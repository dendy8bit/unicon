using BEMN.Forms;

namespace BEMN.UDZT
{
    partial class MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasuringForm));
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct3 = new BEMN.Devices.Structures.DateTimeStruct();
            this._images = new System.Windows.Forms.ImageList(this.components);
            this._diagTab = new System.Windows.Forms.TabControl();
            this._analogPage = new System.Windows.Forms.TabPage();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.label278 = new System.Windows.Forms.Label();
            this._frequency = new System.Windows.Forms.TextBox();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._Uc = new System.Windows.Forms.TextBox();
            this._Ub = new System.Windows.Forms.TextBox();
            this._Ua = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._I30 = new System.Windows.Forms.TextBox();
            this._I3c = new System.Windows.Forms.TextBox();
            this._I3b = new System.Windows.Forms.TextBox();
            this._I3a = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._I20 = new System.Windows.Forms.TextBox();
            this._I2c = new System.Windows.Forms.TextBox();
            this._I2b = new System.Windows.Forms.TextBox();
            this._I2a = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._I0 = new System.Windows.Forms.TextBox();
            this._Ic = new System.Windows.Forms.TextBox();
            this._Ib = new System.Windows.Forms.TextBox();
            this._Ia = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._Un = new System.Windows.Forms.TextBox();
            this._U2 = new System.Windows.Forms.TextBox();
            this._U0 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this._Uca = new System.Windows.Forms.TextBox();
            this._Ubc = new System.Windows.Forms.TextBox();
            this._Uab = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._Id2c = new System.Windows.Forms.TextBox();
            this._Id2b = new System.Windows.Forms.TextBox();
            this._Id2a = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._percentIdcFromIbas = new System.Windows.Forms.TextBox();
            this._percentIdbFromIbas = new System.Windows.Forms.TextBox();
            this._percentIdaFromIbas = new System.Windows.Forms.TextBox();
            this._Idc = new System.Windows.Forms.TextBox();
            this._Idb = new System.Windows.Forms.TextBox();
            this._Ida = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._Is32 = new System.Windows.Forms.TextBox();
            this.label277 = new System.Windows.Forms.Label();
            this._Is30 = new System.Windows.Forms.TextBox();
            this._Is3n = new System.Windows.Forms.TextBox();
            this._Is3c = new System.Windows.Forms.TextBox();
            this._Is3b = new System.Windows.Forms.TextBox();
            this._Is3a = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._Is22 = new System.Windows.Forms.TextBox();
            this.label276 = new System.Windows.Forms.Label();
            this._Is20 = new System.Windows.Forms.TextBox();
            this._Is2n = new System.Windows.Forms.TextBox();
            this._Is2c = new System.Windows.Forms.TextBox();
            this._Is2b = new System.Windows.Forms.TextBox();
            this._Is2a = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._percentIbcFromIbas = new System.Windows.Forms.TextBox();
            this._percentIbbFromIbas = new System.Windows.Forms.TextBox();
            this._percentIbaFromIbas = new System.Windows.Forms.TextBox();
            this._Ibc = new System.Windows.Forms.TextBox();
            this._Ibb = new System.Windows.Forms.TextBox();
            this._Iba = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label275 = new System.Windows.Forms.Label();
            this._Is12 = new System.Windows.Forms.TextBox();
            this._Is10 = new System.Windows.Forms.TextBox();
            this._Is1n = new System.Windows.Forms.TextBox();
            this._Is1c = new System.Windows.Forms.TextBox();
            this._Is1b = new System.Windows.Forms.TextBox();
            this._Is1a = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._Id5c = new System.Windows.Forms.TextBox();
            this._Id5b = new System.Windows.Forms.TextBox();
            this._Id5a = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this._diskretPage = new System.Windows.Forms.TabPage();
            this.v_1_11_Panel = new System.Windows.Forms.Panel();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label257 = new System.Windows.Forms.Label();
            this.SFL32 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this.SFL31 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this.SFL30 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this.SFL29 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this.SFL28 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this.SFL27 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this.SFL26 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this.SFL25 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this.SFL16 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this.SFL15 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this.SFL14 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.SFL13 = new BEMN.Forms.LedControl();
            this.label269 = new System.Windows.Forms.Label();
            this.SFL12 = new BEMN.Forms.LedControl();
            this.label270 = new System.Windows.Forms.Label();
            this.SFL11 = new BEMN.Forms.LedControl();
            this.label271 = new System.Windows.Forms.Label();
            this.SFL10 = new BEMN.Forms.LedControl();
            this.label272 = new System.Windows.Forms.Label();
            this.SFL9 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this.SFL24 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this.SFL23 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this.SFL22 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this.SFL21 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this.SFL20 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this.SFL19 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this.SFL18 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this.SFL17 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this.SFL8 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this.SFL7 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this.SFL6 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this.SFL5 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this.SFL4 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this.SFL3 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this.SFL2 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this.SFL1 = new BEMN.Forms.LedControl();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label226 = new System.Windows.Forms.Label();
            this._Fmin4 = new BEMN.Forms.LedControl();
            this._Fmin4IO = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.label232 = new System.Windows.Forms.Label();
            this.label233 = new System.Windows.Forms.Label();
            this._Fmin3 = new BEMN.Forms.LedControl();
            this._Fmin2 = new BEMN.Forms.LedControl();
            this._Fmin1 = new BEMN.Forms.LedControl();
            this.label234 = new System.Windows.Forms.Label();
            this._Fmin3IO = new BEMN.Forms.LedControl();
            this._Fmin2IO = new BEMN.Forms.LedControl();
            this._Fmin1IO = new BEMN.Forms.LedControl();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label235 = new System.Windows.Forms.Label();
            this._Fmax4 = new BEMN.Forms.LedControl();
            this._Fmax4IO = new BEMN.Forms.LedControl();
            this.label236 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this._Fmax3 = new BEMN.Forms.LedControl();
            this._Fmax2 = new BEMN.Forms.LedControl();
            this._Fmax1 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._Fmax3IO = new BEMN.Forms.LedControl();
            this._Fmax2IO = new BEMN.Forms.LedControl();
            this._Fmax1IO = new BEMN.Forms.LedControl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label191 = new System.Windows.Forms.Label();
            this._Osc = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._Jalm = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._Jsys = new BEMN.Forms.LedControl();
            this.label194 = new System.Windows.Forms.Label();
            this._Passwust = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this._Groupust = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this._Ust = new BEMN.Forms.LedControl();
            this.label211 = new System.Windows.Forms.Label();
            this._Mod5 = new BEMN.Forms.LedControl();
            this.label212 = new System.Windows.Forms.Label();
            this._Mod4 = new BEMN.Forms.LedControl();
            this.label213 = new System.Windows.Forms.Label();
            this._Mod3 = new BEMN.Forms.LedControl();
            this.label214 = new System.Windows.Forms.Label();
            this._Mod2 = new BEMN.Forms.LedControl();
            this.label215 = new System.Windows.Forms.Label();
            this._Mod1 = new BEMN.Forms.LedControl();
            this.label216 = new System.Windows.Forms.Label();
            this._Contr = new BEMN.Forms.LedControl();
            this.label217 = new System.Windows.Forms.Label();
            this._Switch3 = new BEMN.Forms.LedControl();
            this.label218 = new System.Windows.Forms.Label();
            this._Switch2 = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this._Switch1 = new BEMN.Forms.LedControl();
            this.label220 = new System.Windows.Forms.Label();
            this._Meas = new BEMN.Forms.LedControl();
            this.label221 = new System.Windows.Forms.Label();
            this._Soft = new BEMN.Forms.LedControl();
            this.label222 = new System.Windows.Forms.Label();
            this._Hard = new BEMN.Forms.LedControl();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label195 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this.label198 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this.label200 = new System.Windows.Forms.Label();
            this.label201 = new System.Windows.Forms.Label();
            this.label202 = new System.Windows.Forms.Label();
            this.label203 = new System.Windows.Forms.Label();
            this.label204 = new System.Windows.Forms.Label();
            this.label205 = new System.Windows.Forms.Label();
            this.label206 = new System.Windows.Forms.Label();
            this._Ind12 = new BEMN.Forms.LedControl();
            this._Ind11 = new BEMN.Forms.LedControl();
            this._Ind10 = new BEMN.Forms.LedControl();
            this._Ind9 = new BEMN.Forms.LedControl();
            this._Ind8 = new BEMN.Forms.LedControl();
            this._Ind7 = new BEMN.Forms.LedControl();
            this._Ind6 = new BEMN.Forms.LedControl();
            this._Ind5 = new BEMN.Forms.LedControl();
            this._Ind4 = new BEMN.Forms.LedControl();
            this._Ind3 = new BEMN.Forms.LedControl();
            this._Ind2 = new BEMN.Forms.LedControl();
            this._Ind1 = new BEMN.Forms.LedControl();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label207 = new System.Windows.Forms.Label();
            this._Rele18 = new BEMN.Forms.LedControl();
            this.label208 = new System.Windows.Forms.Label();
            this._Rele17 = new BEMN.Forms.LedControl();
            this.label175 = new System.Windows.Forms.Label();
            this._Rele16 = new BEMN.Forms.LedControl();
            this.label176 = new System.Windows.Forms.Label();
            this._Rele15 = new BEMN.Forms.LedControl();
            this.label177 = new System.Windows.Forms.Label();
            this._Rele14 = new BEMN.Forms.LedControl();
            this.label178 = new System.Windows.Forms.Label();
            this._Rele13 = new BEMN.Forms.LedControl();
            this.label179 = new System.Windows.Forms.Label();
            this._Rele12 = new BEMN.Forms.LedControl();
            this.label180 = new System.Windows.Forms.Label();
            this._Rele11 = new BEMN.Forms.LedControl();
            this.label181 = new System.Windows.Forms.Label();
            this._Rele10 = new BEMN.Forms.LedControl();
            this.label182 = new System.Windows.Forms.Label();
            this._Rele9 = new BEMN.Forms.LedControl();
            this.label183 = new System.Windows.Forms.Label();
            this._Rele8 = new BEMN.Forms.LedControl();
            this.label184 = new System.Windows.Forms.Label();
            this._Rele7 = new BEMN.Forms.LedControl();
            this.label185 = new System.Windows.Forms.Label();
            this._Rele6 = new BEMN.Forms.LedControl();
            this.label186 = new System.Windows.Forms.Label();
            this._Rele5 = new BEMN.Forms.LedControl();
            this.label187 = new System.Windows.Forms.Label();
            this._Rele4 = new BEMN.Forms.LedControl();
            this.label188 = new System.Windows.Forms.Label();
            this._Rele3 = new BEMN.Forms.LedControl();
            this.label189 = new System.Windows.Forms.Label();
            this._Rele2 = new BEMN.Forms.LedControl();
            this.label190 = new System.Windows.Forms.Label();
            this._Rele1 = new BEMN.Forms.LedControl();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label174 = new System.Windows.Forms.Label();
            this._Alarm = new BEMN.Forms.LedControl();
            this.label171 = new System.Windows.Forms.Label();
            this._GroupRez = new BEMN.Forms.LedControl();
            this.label170 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this._GroopMain = new BEMN.Forms.LedControl();
            this._Disrepair = new BEMN.Forms.LedControl();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label150 = new System.Windows.Forms.Label();
            this._EXT16 = new BEMN.Forms.LedControl();
            this.label155 = new System.Windows.Forms.Label();
            this._EXT15 = new BEMN.Forms.LedControl();
            this.label156 = new System.Windows.Forms.Label();
            this._EXT14 = new BEMN.Forms.LedControl();
            this.label157 = new System.Windows.Forms.Label();
            this._EXT13 = new BEMN.Forms.LedControl();
            this.label158 = new System.Windows.Forms.Label();
            this._EXT12 = new BEMN.Forms.LedControl();
            this.label159 = new System.Windows.Forms.Label();
            this._EXT11 = new BEMN.Forms.LedControl();
            this.label160 = new System.Windows.Forms.Label();
            this._EXT10 = new BEMN.Forms.LedControl();
            this.label161 = new System.Windows.Forms.Label();
            this._EXT9 = new BEMN.Forms.LedControl();
            this.label162 = new System.Windows.Forms.Label();
            this._EXT8 = new BEMN.Forms.LedControl();
            this.label163 = new System.Windows.Forms.Label();
            this._EXT7 = new BEMN.Forms.LedControl();
            this.label164 = new System.Windows.Forms.Label();
            this._EXT6 = new BEMN.Forms.LedControl();
            this.label165 = new System.Windows.Forms.Label();
            this._EXT5 = new BEMN.Forms.LedControl();
            this.label166 = new System.Windows.Forms.Label();
            this._EXT4 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this._EXT3 = new BEMN.Forms.LedControl();
            this.label168 = new System.Windows.Forms.Label();
            this._EXT2 = new BEMN.Forms.LedControl();
            this.label169 = new System.Windows.Forms.Label();
            this._EXT1 = new BEMN.Forms.LedControl();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label144 = new System.Windows.Forms.Label();
            this._Umin4 = new BEMN.Forms.LedControl();
            this._Umin4IO = new BEMN.Forms.LedControl();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this._Umin3 = new BEMN.Forms.LedControl();
            this._Umin2 = new BEMN.Forms.LedControl();
            this._Umin1 = new BEMN.Forms.LedControl();
            this.label149 = new System.Windows.Forms.Label();
            this._Umin3IO = new BEMN.Forms.LedControl();
            this._Umin2IO = new BEMN.Forms.LedControl();
            this._Umin1IO = new BEMN.Forms.LedControl();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label143 = new System.Windows.Forms.Label();
            this._Umax4 = new BEMN.Forms.LedControl();
            this._Umax4IO = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this._Umax3 = new BEMN.Forms.LedControl();
            this._Umax2 = new BEMN.Forms.LedControl();
            this._Umax1 = new BEMN.Forms.LedControl();
            this.label141 = new System.Windows.Forms.Label();
            this._Umax3IO = new BEMN.Forms.LedControl();
            this._Umax2IO = new BEMN.Forms.LedControl();
            this._Umax1IO = new BEMN.Forms.LedControl();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this._In3 = new BEMN.Forms.LedControl();
            this._In2 = new BEMN.Forms.LedControl();
            this._In1 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this._In3IO = new BEMN.Forms.LedControl();
            this._In2IO = new BEMN.Forms.LedControl();
            this._In1IO = new BEMN.Forms.LedControl();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this._I03 = new BEMN.Forms.LedControl();
            this._I02 = new BEMN.Forms.LedControl();
            this._I01 = new BEMN.Forms.LedControl();
            this.label135 = new System.Windows.Forms.Label();
            this._I03IO = new BEMN.Forms.LedControl();
            this._I02IO = new BEMN.Forms.LedControl();
            this._I01IO = new BEMN.Forms.LedControl();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label123 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._I8 = new BEMN.Forms.LedControl();
            this._I7 = new BEMN.Forms.LedControl();
            this._I6 = new BEMN.Forms.LedControl();
            this._I5 = new BEMN.Forms.LedControl();
            this._I4 = new BEMN.Forms.LedControl();
            this._I3 = new BEMN.Forms.LedControl();
            this._I2 = new BEMN.Forms.LedControl();
            this._I1 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._I8IO = new BEMN.Forms.LedControl();
            this._I7IO = new BEMN.Forms.LedControl();
            this._I6IO = new BEMN.Forms.LedControl();
            this._I5IO = new BEMN.Forms.LedControl();
            this._I4IO = new BEMN.Forms.LedControl();
            this._I3IO = new BEMN.Forms.LedControl();
            this._I2IO = new BEMN.Forms.LedControl();
            this._I1IO = new BEMN.Forms.LedControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._Id0Max3 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._Id0Max3IO = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._Id0Max2 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._Id0Max2IO = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._Id0Max1 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._Id0Max1IO = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._IdMax1 = new BEMN.Forms.LedControl();
            this._IdMax1IO = new BEMN.Forms.LedControl();
            this._IdMax2 = new BEMN.Forms.LedControl();
            this._IdMax2IO = new BEMN.Forms.LedControl();
            this._IdMax2Mgn = new BEMN.Forms.LedControl();
            this._IdMax1L = new System.Windows.Forms.Label();
            this._IdMax1IOL = new System.Windows.Forms.Label();
            this._IdMax2L = new System.Windows.Forms.Label();
            this._IdMax2IOL = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label108 = new System.Windows.Forms.Label();
            this._VLS16 = new BEMN.Forms.LedControl();
            this.label107 = new System.Windows.Forms.Label();
            this._VLS15 = new BEMN.Forms.LedControl();
            this.label106 = new System.Windows.Forms.Label();
            this._VLS14 = new BEMN.Forms.LedControl();
            this.label105 = new System.Windows.Forms.Label();
            this._VLS13 = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this._VLS12 = new BEMN.Forms.LedControl();
            this.label103 = new System.Windows.Forms.Label();
            this._VLS11 = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._VLS10 = new BEMN.Forms.LedControl();
            this.label101 = new System.Windows.Forms.Label();
            this._VLS9 = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._VLS8 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._VLS7 = new BEMN.Forms.LedControl();
            this.label98 = new System.Windows.Forms.Label();
            this._VLS6 = new BEMN.Forms.LedControl();
            this.label97 = new System.Windows.Forms.Label();
            this._VLS5 = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._VLS4 = new BEMN.Forms.LedControl();
            this.label95 = new System.Windows.Forms.Label();
            this._VLS3 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this._VLS2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._VLS1 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label92 = new System.Windows.Forms.Label();
            this._LS16 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._LS15 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._LS14 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._LS13 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._LS12 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._LS11 = new BEMN.Forms.LedControl();
            this.label86 = new System.Windows.Forms.Label();
            this._LS10 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._LS9 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._LS8 = new BEMN.Forms.LedControl();
            this.label83 = new System.Windows.Forms.Label();
            this._LS7 = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._LS6 = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._LS5 = new BEMN.Forms.LedControl();
            this.label80 = new System.Windows.Forms.Label();
            this._LS4 = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._LS3 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this._LS2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._LS1 = new BEMN.Forms.LedControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._D24 = new BEMN.Forms.LedControl();
            this._D23 = new BEMN.Forms.LedControl();
            this._D22 = new BEMN.Forms.LedControl();
            this._D21 = new BEMN.Forms.LedControl();
            this._D20 = new BEMN.Forms.LedControl();
            this._D19 = new BEMN.Forms.LedControl();
            this._D18 = new BEMN.Forms.LedControl();
            this._D17 = new BEMN.Forms.LedControl();
            this._D16 = new BEMN.Forms.LedControl();
            this._D15 = new BEMN.Forms.LedControl();
            this._D14 = new BEMN.Forms.LedControl();
            this._D13 = new BEMN.Forms.LedControl();
            this._D12 = new BEMN.Forms.LedControl();
            this._D10 = new BEMN.Forms.LedControl();
            this._D9 = new BEMN.Forms.LedControl();
            this._D11 = new BEMN.Forms.LedControl();
            this._D8 = new BEMN.Forms.LedControl();
            this._D7 = new BEMN.Forms.LedControl();
            this._D6 = new BEMN.Forms.LedControl();
            this._D5 = new BEMN.Forms.LedControl();
            this._D4 = new BEMN.Forms.LedControl();
            this._D3 = new BEMN.Forms.LedControl();
            this._D2 = new BEMN.Forms.LedControl();
            this._D1 = new BEMN.Forms.LedControl();
            this._uprPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.indJourSysLed = new BEMN.Forms.LedControl();
            this.indDefectRelay = new BEMN.Forms.LedControl();
            this.GROOPREZERVE_BUT = new System.Windows.Forms.Button();
            this.indJourAlarmLed = new BEMN.Forms.LedControl();
            this.GROOPMAIN_BUT = new System.Windows.Forms.Button();
            this.label229 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this._bdGroupRez = new BEMN.Forms.LedControl();
            this._bdGroupMain = new BEMN.Forms.LedControl();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this.SwitchOff = new System.Windows.Forms.Button();
            this.BDC_MESS_DISPSYS = new BEMN.Forms.LedControl();
            this.label274 = new System.Windows.Forms.Label();
            this.BDC_MESS_OSC = new BEMN.Forms.LedControl();
            this.SWITCH_ON = new BEMN.Forms.LedControl();
            this.BDC_MESS_ALARM = new BEMN.Forms.LedControl();
            this.SwitchOn = new System.Windows.Forms.Button();
            this.BDC_MESS_SYS = new BEMN.Forms.LedControl();
            this.label273 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.SWITCH_OFF = new BEMN.Forms.LedControl();
            this.ResetJS_Error = new System.Windows.Forms.Button();
            this.ResetJO = new System.Windows.Forms.Button();
            this.ResetJA = new System.Windows.Forms.Button();
            this.ResetJS = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this.label223 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label14_releLed9Label0 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.KgroupBox = new System.Windows.Forms.GroupBox();
            this._k1 = new BEMN.Forms.LedControl();
            this.label151 = new System.Windows.Forms.Label();
            this._k2 = new BEMN.Forms.LedControl();
            this.label152 = new System.Windows.Forms.Label();
            this._diagTab.SuspendLayout();
            this._analogPage.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this._diskretPage.SuspendLayout();
            this.v_1_11_Panel.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._uprPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.KgroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _images
            // 
            this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.TransparentColor = System.Drawing.Color.Transparent;
            this._images.Images.SetKeyName(0, "analog.bmp");
            this._images.Images.SetKeyName(1, "diskret.bmp");
            // 
            // _diagTab
            // 
            this._diagTab.Controls.Add(this._analogPage);
            this._diagTab.Controls.Add(this._diskretPage);
            this._diagTab.Controls.Add(this._uprPage);
            this._diagTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._diagTab.ImageList = this._images;
            this._diagTab.Location = new System.Drawing.Point(0, 0);
            this._diagTab.Name = "_diagTab";
            this._diagTab.SelectedIndex = 0;
            this._diagTab.Size = new System.Drawing.Size(773, 612);
            this._diagTab.TabIndex = 1;
            // 
            // _analogPage
            // 
            this._analogPage.AllowDrop = true;
            this._analogPage.Controls.Add(this.groupBox);
            this._analogPage.Controls.Add(this._dateTimeControl);
            this._analogPage.Controls.Add(this.groupBox18);
            this._analogPage.Controls.Add(this.groupBox19);
            this._analogPage.Controls.Add(this.groupBox20);
            this._analogPage.Controls.Add(this.groupBox21);
            this._analogPage.Controls.Add(this.groupBox1);
            this._analogPage.Controls.Add(this.groupBox2);
            this._analogPage.Controls.Add(this.groupBox3);
            this._analogPage.Controls.Add(this.groupBox4);
            this._analogPage.Controls.Add(this.groupBox5);
            this._analogPage.Controls.Add(this.groupBox6);
            this._analogPage.Controls.Add(this.groupBox7);
            this._analogPage.Controls.Add(this.groupBox8);
            this._analogPage.ImageIndex = 0;
            this._analogPage.Location = new System.Drawing.Point(4, 23);
            this._analogPage.Name = "_analogPage";
            this._analogPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogPage.Size = new System.Drawing.Size(765, 585);
            this._analogPage.TabIndex = 0;
            this._analogPage.Text = "���������� �� ";
            this._analogPage.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.label278);
            this.groupBox.Controls.Add(this._frequency);
            this.groupBox.Location = new System.Drawing.Point(450, 6);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(102, 41);
            this.groupBox.TabIndex = 56;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "�������";
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Location = new System.Drawing.Point(4, 16);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(13, 13);
            this.label278.TabIndex = 41;
            this.label278.Text = "F";
            // 
            // _frequency
            // 
            this._frequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._frequency.Location = new System.Drawing.Point(28, 14);
            this._frequency.Name = "_frequency";
            this._frequency.ReadOnly = true;
            this._frequency.Size = new System.Drawing.Size(68, 20);
            this._frequency.TabIndex = 57;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct3;
            this._dateTimeControl.Location = new System.Drawing.Point(558, 6);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 55;
            this._dateTimeControl.TimeChanged += new System.Action(this._dateTimeControl_TimeChanged);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._Uc);
            this.groupBox18.Controls.Add(this._Ub);
            this.groupBox18.Controls.Add(this._Ua);
            this.groupBox18.Controls.Add(this.label43);
            this.groupBox18.Controls.Add(this.label44);
            this.groupBox18.Controls.Add(this.label45);
            this.groupBox18.Location = new System.Drawing.Point(328, 264);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(113, 92);
            this.groupBox18.TabIndex = 54;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "����� U";
            // 
            // _Uc
            // 
            this._Uc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uc.Location = new System.Drawing.Point(36, 67);
            this._Uc.Name = "_Uc";
            this._Uc.ReadOnly = true;
            this._Uc.Size = new System.Drawing.Size(68, 20);
            this._Uc.TabIndex = 40;
            // 
            // _Ub
            // 
            this._Ub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ub.Location = new System.Drawing.Point(36, 43);
            this._Ub.Name = "_Ub";
            this._Ub.ReadOnly = true;
            this._Ub.Size = new System.Drawing.Size(68, 20);
            this._Ub.TabIndex = 39;
            // 
            // _Ua
            // 
            this._Ua.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ua.Location = new System.Drawing.Point(36, 20);
            this._Ua.Name = "_Ua";
            this._Ua.ReadOnly = true;
            this._Ua.Size = new System.Drawing.Size(68, 20);
            this._Ua.TabIndex = 38;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 70);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(21, 13);
            this.label43.TabIndex = 28;
            this.label43.Text = "Uc";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(21, 13);
            this.label44.TabIndex = 26;
            this.label44.Text = "Ub";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 23);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 13);
            this.label45.TabIndex = 17;
            this.label45.Text = "Ua";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this._I30);
            this.groupBox19.Controls.Add(this._I3c);
            this.groupBox19.Controls.Add(this._I3b);
            this.groupBox19.Controls.Add(this._I3a);
            this.groupBox19.Controls.Add(this.label47);
            this.groupBox19.Controls.Add(this.label48);
            this.groupBox19.Controls.Add(this.label49);
            this.groupBox19.Controls.Add(this.label50);
            this.groupBox19.Location = new System.Drawing.Point(221, 264);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(101, 115);
            this.groupBox19.TabIndex = 53;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "����� L3";
            // 
            // _I30
            // 
            this._I30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I30.Location = new System.Drawing.Point(27, 90);
            this._I30.Name = "_I30";
            this._I30.ReadOnly = true;
            this._I30.Size = new System.Drawing.Size(68, 20);
            this._I30.TabIndex = 41;
            // 
            // _I3c
            // 
            this._I3c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I3c.Location = new System.Drawing.Point(27, 67);
            this._I3c.Name = "_I3c";
            this._I3c.ReadOnly = true;
            this._I3c.Size = new System.Drawing.Size(68, 20);
            this._I3c.TabIndex = 40;
            // 
            // _I3b
            // 
            this._I3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I3b.Location = new System.Drawing.Point(27, 43);
            this._I3b.Name = "_I3b";
            this._I3b.ReadOnly = true;
            this._I3b.Size = new System.Drawing.Size(68, 20);
            this._I3b.TabIndex = 39;
            // 
            // _I3a
            // 
            this._I3a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I3a.Location = new System.Drawing.Point(27, 20);
            this._I3a.Name = "_I3a";
            this._I3a.ReadOnly = true;
            this._I3a.Size = new System.Drawing.Size(68, 20);
            this._I3a.TabIndex = 38;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 92);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(16, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "I0";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(7, 70);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(16, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "Ic";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(16, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "Ib";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 23);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(16, 13);
            this.label50.TabIndex = 17;
            this.label50.Text = "Ia";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._I20);
            this.groupBox20.Controls.Add(this._I2c);
            this.groupBox20.Controls.Add(this._I2b);
            this.groupBox20.Controls.Add(this._I2a);
            this.groupBox20.Controls.Add(this.label52);
            this.groupBox20.Controls.Add(this.label53);
            this.groupBox20.Controls.Add(this.label54);
            this.groupBox20.Controls.Add(this.label56);
            this.groupBox20.Location = new System.Drawing.Point(114, 264);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(101, 115);
            this.groupBox20.TabIndex = 52;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "����� L2";
            // 
            // _I20
            // 
            this._I20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I20.Location = new System.Drawing.Point(28, 90);
            this._I20.Name = "_I20";
            this._I20.ReadOnly = true;
            this._I20.Size = new System.Drawing.Size(68, 20);
            this._I20.TabIndex = 41;
            // 
            // _I2c
            // 
            this._I2c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2c.Location = new System.Drawing.Point(28, 67);
            this._I2c.Name = "_I2c";
            this._I2c.ReadOnly = true;
            this._I2c.Size = new System.Drawing.Size(68, 20);
            this._I2c.TabIndex = 40;
            // 
            // _I2b
            // 
            this._I2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2b.Location = new System.Drawing.Point(28, 43);
            this._I2b.Name = "_I2b";
            this._I2b.ReadOnly = true;
            this._I2b.Size = new System.Drawing.Size(68, 20);
            this._I2b.TabIndex = 39;
            // 
            // _I2a
            // 
            this._I2a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2a.Location = new System.Drawing.Point(28, 20);
            this._I2a.Name = "_I2a";
            this._I2a.ReadOnly = true;
            this._I2a.Size = new System.Drawing.Size(68, 20);
            this._I2a.TabIndex = 38;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 92);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 13);
            this.label52.TabIndex = 30;
            this.label52.Text = "I0";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 70);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(16, 13);
            this.label53.TabIndex = 28;
            this.label53.Text = "Ic";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 46);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(16, 13);
            this.label54.TabIndex = 26;
            this.label54.Text = "Ib";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 23);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(16, 13);
            this.label56.TabIndex = 17;
            this.label56.Text = "Ia";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._I0);
            this.groupBox21.Controls.Add(this._Ic);
            this.groupBox21.Controls.Add(this._Ib);
            this.groupBox21.Controls.Add(this._Ia);
            this.groupBox21.Controls.Add(this.label59);
            this.groupBox21.Controls.Add(this.label60);
            this.groupBox21.Controls.Add(this.label61);
            this.groupBox21.Controls.Add(this.label62);
            this.groupBox21.Location = new System.Drawing.Point(6, 264);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(102, 115);
            this.groupBox21.TabIndex = 51;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "����� L1";
            // 
            // _I0
            // 
            this._I0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I0.Location = new System.Drawing.Point(28, 90);
            this._I0.Name = "_I0";
            this._I0.ReadOnly = true;
            this._I0.Size = new System.Drawing.Size(68, 20);
            this._I0.TabIndex = 36;
            // 
            // _Ic
            // 
            this._Ic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ic.Location = new System.Drawing.Point(28, 67);
            this._Ic.Name = "_Ic";
            this._Ic.ReadOnly = true;
            this._Ic.Size = new System.Drawing.Size(68, 20);
            this._Ic.TabIndex = 35;
            // 
            // _Ib
            // 
            this._Ib.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib.Location = new System.Drawing.Point(28, 43);
            this._Ib.Name = "_Ib";
            this._Ib.ReadOnly = true;
            this._Ib.Size = new System.Drawing.Size(68, 20);
            this._Ib.TabIndex = 34;
            // 
            // _Ia
            // 
            this._Ia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ia.Location = new System.Drawing.Point(28, 20);
            this._Ia.Name = "_Ia";
            this._Ia.ReadOnly = true;
            this._Ia.Size = new System.Drawing.Size(68, 20);
            this._Ia.TabIndex = 33;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 92);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(16, 13);
            this.label59.TabIndex = 30;
            this.label59.Text = "I0";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 70);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(16, 13);
            this.label60.TabIndex = 28;
            this.label60.Text = "Ic";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 46);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(16, 13);
            this.label61.TabIndex = 26;
            this.label61.Text = "Ib";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 23);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(16, 13);
            this.label62.TabIndex = 17;
            this.label62.Text = "Ia";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._Un);
            this.groupBox1.Controls.Add(this._U2);
            this.groupBox1.Controls.Add(this._U0);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this._Uca);
            this.groupBox1.Controls.Add(this._Ubc);
            this.groupBox1.Controls.Add(this._Uab);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(330, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(111, 153);
            this.groupBox1.TabIndex = 50;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "U ���������";
            // 
            // _Un
            // 
            this._Un.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Un.Location = new System.Drawing.Point(36, 81);
            this._Un.Name = "_Un";
            this._Un.ReadOnly = true;
            this._Un.Size = new System.Drawing.Size(68, 20);
            this._Un.TabIndex = 41;
            // 
            // _U2
            // 
            this._U2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2.Location = new System.Drawing.Point(36, 127);
            this._U2.Name = "_U2";
            this._U2.ReadOnly = true;
            this._U2.Size = new System.Drawing.Size(68, 20);
            this._U2.TabIndex = 42;
            // 
            // _U0
            // 
            this._U0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U0.Location = new System.Drawing.Point(36, 104);
            this._U0.Name = "_U0";
            this._U0.ReadOnly = true;
            this._U0.Size = new System.Drawing.Size(68, 20);
            this._U0.TabIndex = 41;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 85);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 13);
            this.label42.TabIndex = 30;
            this.label42.Text = "Un";
            // 
            // _Uca
            // 
            this._Uca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uca.Location = new System.Drawing.Point(36, 58);
            this._Uca.Name = "_Uca";
            this._Uca.ReadOnly = true;
            this._Uca.Size = new System.Drawing.Size(68, 20);
            this._Uca.TabIndex = 40;
            // 
            // _Ubc
            // 
            this._Ubc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ubc.Location = new System.Drawing.Point(36, 35);
            this._Ubc.Name = "_Ubc";
            this._Ubc.ReadOnly = true;
            this._Ubc.Size = new System.Drawing.Size(68, 20);
            this._Ubc.TabIndex = 39;
            // 
            // _Uab
            // 
            this._Uab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Uab.Location = new System.Drawing.Point(36, 12);
            this._Uab.Name = "_Uab";
            this._Uab.ReadOnly = true;
            this._Uab.Size = new System.Drawing.Size(68, 20);
            this._Uab.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "U2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "U0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Uca";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Ubc";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Uab";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._Id2c);
            this.groupBox2.Controls.Add(this._Id2b);
            this.groupBox2.Controls.Add(this._Id2a);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(2, 408);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(134, 99);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "���. ��� 2 ���������";
            this.groupBox2.Visible = false;
            // 
            // _Id2c
            // 
            this._Id2c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id2c.Location = new System.Drawing.Point(28, 74);
            this._Id2c.Name = "_Id2c";
            this._Id2c.ReadOnly = true;
            this._Id2c.Size = new System.Drawing.Size(68, 20);
            this._Id2c.TabIndex = 38;
            this._Id2c.Visible = false;
            // 
            // _Id2b
            // 
            this._Id2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id2b.Location = new System.Drawing.Point(28, 45);
            this._Id2b.Name = "_Id2b";
            this._Id2b.ReadOnly = true;
            this._Id2b.Size = new System.Drawing.Size(68, 20);
            this._Id2b.TabIndex = 37;
            this._Id2b.Visible = false;
            // 
            // _Id2a
            // 
            this._Id2a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id2a.Location = new System.Drawing.Point(28, 16);
            this._Id2a.Name = "_Id2a";
            this._Id2a.ReadOnly = true;
            this._Id2a.Size = new System.Drawing.Size(68, 20);
            this._Id2a.TabIndex = 36;
            this._Id2a.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Ic";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Ib";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Ia";
            this.label8.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._percentIdcFromIbas);
            this.groupBox3.Controls.Add(this._percentIdbFromIbas);
            this.groupBox3.Controls.Add(this._percentIdaFromIbas);
            this.groupBox3.Controls.Add(this._Idc);
            this.groupBox3.Controls.Add(this._Idb);
            this.groupBox3.Controls.Add(this._Ida);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 100);
            this.groupBox3.TabIndex = 48;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "���. ��� ���. ���������";
            // 
            // _percentIdcFromIbas
            // 
            this._percentIdcFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIdcFromIbas.Location = new System.Drawing.Point(116, 74);
            this._percentIdcFromIbas.Name = "_percentIdcFromIbas";
            this._percentIdcFromIbas.ReadOnly = true;
            this._percentIdcFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIdcFromIbas.TabIndex = 39;
            // 
            // _percentIdbFromIbas
            // 
            this._percentIdbFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIdbFromIbas.Location = new System.Drawing.Point(116, 45);
            this._percentIdbFromIbas.Name = "_percentIdbFromIbas";
            this._percentIdbFromIbas.ReadOnly = true;
            this._percentIdbFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIdbFromIbas.TabIndex = 38;
            // 
            // _percentIdaFromIbas
            // 
            this._percentIdaFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIdaFromIbas.Location = new System.Drawing.Point(116, 16);
            this._percentIdaFromIbas.Name = "_percentIdaFromIbas";
            this._percentIdaFromIbas.ReadOnly = true;
            this._percentIdaFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIdaFromIbas.TabIndex = 37;
            // 
            // _Idc
            // 
            this._Idc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Idc.Location = new System.Drawing.Point(28, 74);
            this._Idc.Name = "_Idc";
            this._Idc.ReadOnly = true;
            this._Idc.Size = new System.Drawing.Size(68, 20);
            this._Idc.TabIndex = 36;
            // 
            // _Idb
            // 
            this._Idb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Idb.Location = new System.Drawing.Point(28, 45);
            this._Idb.Name = "_Idb";
            this._Idb.ReadOnly = true;
            this._Idb.Size = new System.Drawing.Size(68, 20);
            this._Idb.TabIndex = 35;
            // 
            // _Ida
            // 
            this._Ida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ida.Location = new System.Drawing.Point(28, 16);
            this._Ida.Name = "_Ida";
            this._Ida.ReadOnly = true;
            this._Ida.Size = new System.Drawing.Size(68, 20);
            this._Ida.TabIndex = 34;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 13);
            this.label17.TabIndex = 28;
            this.label17.Text = "Ic";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Ib";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(16, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Ia";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._Is32);
            this.groupBox4.Controls.Add(this.label277);
            this.groupBox4.Controls.Add(this._Is30);
            this.groupBox4.Controls.Add(this._Is3n);
            this.groupBox4.Controls.Add(this._Is3c);
            this.groupBox4.Controls.Add(this._Is3b);
            this.groupBox4.Controls.Add(this._Is3a);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Location = new System.Drawing.Point(221, 112);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(103, 153);
            this.groupBox4.TabIndex = 47;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "��� ��. 3";
            // 
            // _Is32
            // 
            this._Is32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is32.Location = new System.Drawing.Point(28, 127);
            this._Is32.Name = "_Is32";
            this._Is32.ReadOnly = true;
            this._Is32.Size = new System.Drawing.Size(68, 20);
            this._Is32.TabIndex = 43;
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(7, 129);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(16, 13);
            this.label277.TabIndex = 43;
            this.label277.Text = "I2";
            // 
            // _Is30
            // 
            this._Is30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is30.Location = new System.Drawing.Point(28, 104);
            this._Is30.Name = "_Is30";
            this._Is30.ReadOnly = true;
            this._Is30.Size = new System.Drawing.Size(68, 20);
            this._Is30.TabIndex = 42;
            // 
            // _Is3n
            // 
            this._Is3n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3n.Location = new System.Drawing.Point(28, 81);
            this._Is3n.Name = "_Is3n";
            this._Is3n.ReadOnly = true;
            this._Is3n.Size = new System.Drawing.Size(68, 20);
            this._Is3n.TabIndex = 41;
            // 
            // _Is3c
            // 
            this._Is3c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3c.Location = new System.Drawing.Point(28, 58);
            this._Is3c.Name = "_Is3c";
            this._Is3c.ReadOnly = true;
            this._Is3c.Size = new System.Drawing.Size(68, 20);
            this._Is3c.TabIndex = 40;
            // 
            // _Is3b
            // 
            this._Is3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3b.Location = new System.Drawing.Point(28, 35);
            this._Is3b.Name = "_Is3b";
            this._Is3b.ReadOnly = true;
            this._Is3b.Size = new System.Drawing.Size(68, 20);
            this._Is3b.TabIndex = 39;
            // 
            // _Is3a
            // 
            this._Is3a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3a.Location = new System.Drawing.Point(28, 12);
            this._Is3a.Name = "_Is3a";
            this._Is3a.ReadOnly = true;
            this._Is3a.Size = new System.Drawing.Size(68, 20);
            this._Is3a.TabIndex = 38;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 107);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "I0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 85);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "In";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 61);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 28;
            this.label22.Text = "Ic";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 38);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "Ib";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "Ia";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._Is22);
            this.groupBox5.Controls.Add(this.label276);
            this.groupBox5.Controls.Add(this._Is20);
            this.groupBox5.Controls.Add(this._Is2n);
            this.groupBox5.Controls.Add(this._Is2c);
            this.groupBox5.Controls.Add(this._Is2b);
            this.groupBox5.Controls.Add(this._Is2a);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(114, 112);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(101, 153);
            this.groupBox5.TabIndex = 46;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "��� ��. 2";
            // 
            // _Is22
            // 
            this._Is22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is22.Location = new System.Drawing.Point(28, 126);
            this._Is22.Name = "_Is22";
            this._Is22.ReadOnly = true;
            this._Is22.Size = new System.Drawing.Size(68, 20);
            this._Is22.TabIndex = 40;
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(6, 129);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(16, 13);
            this.label276.TabIndex = 40;
            this.label276.Text = "I2";
            // 
            // _Is20
            // 
            this._Is20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is20.Location = new System.Drawing.Point(28, 104);
            this._Is20.Name = "_Is20";
            this._Is20.ReadOnly = true;
            this._Is20.Size = new System.Drawing.Size(68, 20);
            this._Is20.TabIndex = 42;
            // 
            // _Is2n
            // 
            this._Is2n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2n.Location = new System.Drawing.Point(28, 82);
            this._Is2n.Name = "_Is2n";
            this._Is2n.ReadOnly = true;
            this._Is2n.Size = new System.Drawing.Size(68, 20);
            this._Is2n.TabIndex = 41;
            // 
            // _Is2c
            // 
            this._Is2c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2c.Location = new System.Drawing.Point(28, 58);
            this._Is2c.Name = "_Is2c";
            this._Is2c.ReadOnly = true;
            this._Is2c.Size = new System.Drawing.Size(68, 20);
            this._Is2c.TabIndex = 40;
            // 
            // _Is2b
            // 
            this._Is2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2b.Location = new System.Drawing.Point(28, 35);
            this._Is2b.Name = "_Is2b";
            this._Is2b.ReadOnly = true;
            this._Is2b.Size = new System.Drawing.Size(68, 20);
            this._Is2b.TabIndex = 39;
            // 
            // _Is2a
            // 
            this._Is2a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2a.Location = new System.Drawing.Point(28, 12);
            this._Is2a.Name = "_Is2a";
            this._Is2a.ReadOnly = true;
            this._Is2a.Size = new System.Drawing.Size(68, 20);
            this._Is2a.TabIndex = 38;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "I0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 85);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 13);
            this.label26.TabIndex = 30;
            this.label26.Text = "In";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 61);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(16, 13);
            this.label27.TabIndex = 28;
            this.label27.Text = "Ic";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 38);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(16, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "Ib";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 15);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(16, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "Ia";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._percentIbcFromIbas);
            this.groupBox6.Controls.Add(this._percentIbbFromIbas);
            this.groupBox6.Controls.Add(this._percentIbaFromIbas);
            this.groupBox6.Controls.Add(this._Ibc);
            this.groupBox6.Controls.Add(this._Ibb);
            this.groupBox6.Controls.Add(this._Iba);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Location = new System.Drawing.Point(221, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(220, 100);
            this.groupBox6.TabIndex = 45;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "��������� ���";
            // 
            // _percentIbcFromIbas
            // 
            this._percentIbcFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIbcFromIbas.Location = new System.Drawing.Point(116, 74);
            this._percentIbcFromIbas.Name = "_percentIbcFromIbas";
            this._percentIbcFromIbas.ReadOnly = true;
            this._percentIbcFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIbcFromIbas.TabIndex = 40;
            // 
            // _percentIbbFromIbas
            // 
            this._percentIbbFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIbbFromIbas.Location = new System.Drawing.Point(116, 45);
            this._percentIbbFromIbas.Name = "_percentIbbFromIbas";
            this._percentIbbFromIbas.ReadOnly = true;
            this._percentIbbFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIbbFromIbas.TabIndex = 39;
            // 
            // _percentIbaFromIbas
            // 
            this._percentIbaFromIbas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIbaFromIbas.Location = new System.Drawing.Point(116, 16);
            this._percentIbaFromIbas.Name = "_percentIbaFromIbas";
            this._percentIbaFromIbas.ReadOnly = true;
            this._percentIbaFromIbas.Size = new System.Drawing.Size(68, 20);
            this._percentIbaFromIbas.TabIndex = 38;
            // 
            // _Ibc
            // 
            this._Ibc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibc.Location = new System.Drawing.Point(28, 74);
            this._Ibc.Name = "_Ibc";
            this._Ibc.ReadOnly = true;
            this._Ibc.Size = new System.Drawing.Size(68, 20);
            this._Ibc.TabIndex = 37;
            // 
            // _Ibb
            // 
            this._Ibb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibb.Location = new System.Drawing.Point(28, 45);
            this._Ibb.Name = "_Ibb";
            this._Ibb.ReadOnly = true;
            this._Ibb.Size = new System.Drawing.Size(68, 20);
            this._Ibb.TabIndex = 36;
            // 
            // _Iba
            // 
            this._Iba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Iba.Location = new System.Drawing.Point(28, 16);
            this._Iba.Name = "_Iba";
            this._Iba.ReadOnly = true;
            this._Iba.Size = new System.Drawing.Size(68, 20);
            this._Iba.TabIndex = 35;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 77);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(16, 13);
            this.label30.TabIndex = 28;
            this.label30.Text = "Ic";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(16, 13);
            this.label31.TabIndex = 26;
            this.label31.Text = "Ib";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 18);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(16, 13);
            this.label32.TabIndex = 17;
            this.label32.Text = "Ia";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label275);
            this.groupBox7.Controls.Add(this._Is12);
            this.groupBox7.Controls.Add(this._Is10);
            this.groupBox7.Controls.Add(this._Is1n);
            this.groupBox7.Controls.Add(this._Is1c);
            this.groupBox7.Controls.Add(this._Is1b);
            this.groupBox7.Controls.Add(this._Is1a);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.label35);
            this.groupBox7.Controls.Add(this.label36);
            this.groupBox7.Controls.Add(this.label37);
            this.groupBox7.Location = new System.Drawing.Point(6, 112);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(102, 153);
            this.groupBox7.TabIndex = 44;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "��� ��. 1";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(6, 129);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(16, 13);
            this.label275.TabIndex = 39;
            this.label275.Text = "I2";
            // 
            // _Is12
            // 
            this._Is12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is12.Location = new System.Drawing.Point(28, 127);
            this._Is12.Name = "_Is12";
            this._Is12.ReadOnly = true;
            this._Is12.Size = new System.Drawing.Size(68, 20);
            this._Is12.TabIndex = 38;
            // 
            // _Is10
            // 
            this._Is10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is10.Location = new System.Drawing.Point(28, 105);
            this._Is10.Name = "_Is10";
            this._Is10.ReadOnly = true;
            this._Is10.Size = new System.Drawing.Size(68, 20);
            this._Is10.TabIndex = 37;
            // 
            // _Is1n
            // 
            this._Is1n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1n.Location = new System.Drawing.Point(28, 82);
            this._Is1n.Name = "_Is1n";
            this._Is1n.ReadOnly = true;
            this._Is1n.Size = new System.Drawing.Size(68, 20);
            this._Is1n.TabIndex = 36;
            // 
            // _Is1c
            // 
            this._Is1c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1c.Location = new System.Drawing.Point(28, 58);
            this._Is1c.Name = "_Is1c";
            this._Is1c.ReadOnly = true;
            this._Is1c.Size = new System.Drawing.Size(68, 20);
            this._Is1c.TabIndex = 35;
            // 
            // _Is1b
            // 
            this._Is1b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1b.Location = new System.Drawing.Point(28, 35);
            this._Is1b.Name = "_Is1b";
            this._Is1b.ReadOnly = true;
            this._Is1b.Size = new System.Drawing.Size(68, 20);
            this._Is1b.TabIndex = 34;
            // 
            // _Is1a
            // 
            this._Is1a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1a.Location = new System.Drawing.Point(28, 12);
            this._Is1a.Name = "_Is1a";
            this._Is1a.ReadOnly = true;
            this._Is1a.Size = new System.Drawing.Size(68, 20);
            this._Is1a.TabIndex = 33;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 107);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(16, 13);
            this.label33.TabIndex = 32;
            this.label33.Text = "I0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 85);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(16, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "In";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 61);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(16, 13);
            this.label35.TabIndex = 28;
            this.label35.Text = "Ic";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 38);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 13);
            this.label36.TabIndex = 26;
            this.label36.Text = "Ib";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 15);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(16, 13);
            this.label37.TabIndex = 17;
            this.label37.Text = "Ia";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._Id5c);
            this.groupBox8.Controls.Add(this._Id5b);
            this.groupBox8.Controls.Add(this._Id5a);
            this.groupBox8.Controls.Add(this.label38);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Location = new System.Drawing.Point(142, 408);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(138, 99);
            this.groupBox8.TabIndex = 43;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "���. ��� 5 ���������";
            this.groupBox8.Visible = false;
            // 
            // _Id5c
            // 
            this._Id5c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id5c.Location = new System.Drawing.Point(28, 74);
            this._Id5c.Name = "_Id5c";
            this._Id5c.ReadOnly = true;
            this._Id5c.Size = new System.Drawing.Size(68, 20);
            this._Id5c.TabIndex = 40;
            this._Id5c.Visible = false;
            // 
            // _Id5b
            // 
            this._Id5b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id5b.Location = new System.Drawing.Point(28, 45);
            this._Id5b.Name = "_Id5b";
            this._Id5b.ReadOnly = true;
            this._Id5b.Size = new System.Drawing.Size(68, 20);
            this._Id5b.TabIndex = 39;
            this._Id5b.Visible = false;
            // 
            // _Id5a
            // 
            this._Id5a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Id5a.Location = new System.Drawing.Point(28, 16);
            this._Id5a.Name = "_Id5a";
            this._Id5a.ReadOnly = true;
            this._Id5a.Size = new System.Drawing.Size(68, 20);
            this._Id5a.TabIndex = 37;
            this._Id5a.Visible = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 77);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(16, 13);
            this.label38.TabIndex = 28;
            this.label38.Text = "Ic";
            this.label38.Visible = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 48);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(16, 13);
            this.label39.TabIndex = 26;
            this.label39.Text = "Ib";
            this.label39.Visible = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 18);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(16, 13);
            this.label40.TabIndex = 17;
            this.label40.Text = "Ia";
            this.label40.Visible = false;
            // 
            // _diskretPage
            // 
            this._diskretPage.Controls.Add(this.KgroupBox);
            this._diskretPage.Controls.Add(this.v_1_11_Panel);
            this._diskretPage.Controls.Add(this.groupBox15);
            this._diskretPage.Controls.Add(this.groupBox26);
            this._diskretPage.Controls.Add(this.groupBox25);
            this._diskretPage.Controls.Add(this.groupBox24);
            this._diskretPage.Controls.Add(this.groupBox23);
            this._diskretPage.Controls.Add(this.groupBox17);
            this._diskretPage.Controls.Add(this.groupBox16);
            this._diskretPage.Controls.Add(this.groupBox14);
            this._diskretPage.Controls.Add(this.groupBox13);
            this._diskretPage.Controls.Add(this.groupBox12);
            this._diskretPage.Controls.Add(this.groupBox11);
            this._diskretPage.Controls.Add(this.groupBox10);
            this._diskretPage.Controls.Add(this.groupBox9);
            this._diskretPage.ImageIndex = 1;
            this._diskretPage.Location = new System.Drawing.Point(4, 23);
            this._diskretPage.Name = "_diskretPage";
            this._diskretPage.Padding = new System.Windows.Forms.Padding(3);
            this._diskretPage.Size = new System.Drawing.Size(765, 585);
            this._diskretPage.TabIndex = 1;
            this._diskretPage.Text = "���������� ��";
            this._diskretPage.UseVisualStyleBackColor = true;
            // 
            // v_1_11_Panel
            // 
            this.v_1_11_Panel.Controls.Add(this.groupBox31);
            this.v_1_11_Panel.Controls.Add(this.groupBox29);
            this.v_1_11_Panel.Controls.Add(this.groupBox30);
            this.v_1_11_Panel.Location = new System.Drawing.Point(537, 0);
            this.v_1_11_Panel.Name = "v_1_11_Panel";
            this.v_1_11_Panel.Size = new System.Drawing.Size(222, 486);
            this.v_1_11_Panel.TabIndex = 63;
            this.v_1_11_Panel.Visible = false;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label257);
            this.groupBox31.Controls.Add(this.SFL32);
            this.groupBox31.Controls.Add(this.label258);
            this.groupBox31.Controls.Add(this.SFL31);
            this.groupBox31.Controls.Add(this.label259);
            this.groupBox31.Controls.Add(this.SFL30);
            this.groupBox31.Controls.Add(this.label260);
            this.groupBox31.Controls.Add(this.SFL29);
            this.groupBox31.Controls.Add(this.label261);
            this.groupBox31.Controls.Add(this.SFL28);
            this.groupBox31.Controls.Add(this.label262);
            this.groupBox31.Controls.Add(this.SFL27);
            this.groupBox31.Controls.Add(this.label263);
            this.groupBox31.Controls.Add(this.SFL26);
            this.groupBox31.Controls.Add(this.label264);
            this.groupBox31.Controls.Add(this.SFL25);
            this.groupBox31.Controls.Add(this.label265);
            this.groupBox31.Controls.Add(this.SFL16);
            this.groupBox31.Controls.Add(this.label266);
            this.groupBox31.Controls.Add(this.SFL15);
            this.groupBox31.Controls.Add(this.label267);
            this.groupBox31.Controls.Add(this.SFL14);
            this.groupBox31.Controls.Add(this.label268);
            this.groupBox31.Controls.Add(this.SFL13);
            this.groupBox31.Controls.Add(this.label269);
            this.groupBox31.Controls.Add(this.SFL12);
            this.groupBox31.Controls.Add(this.label270);
            this.groupBox31.Controls.Add(this.SFL11);
            this.groupBox31.Controls.Add(this.label271);
            this.groupBox31.Controls.Add(this.SFL10);
            this.groupBox31.Controls.Add(this.label272);
            this.groupBox31.Controls.Add(this.SFL9);
            this.groupBox31.Controls.Add(this.label241);
            this.groupBox31.Controls.Add(this.SFL24);
            this.groupBox31.Controls.Add(this.label242);
            this.groupBox31.Controls.Add(this.SFL23);
            this.groupBox31.Controls.Add(this.label243);
            this.groupBox31.Controls.Add(this.SFL22);
            this.groupBox31.Controls.Add(this.label244);
            this.groupBox31.Controls.Add(this.SFL21);
            this.groupBox31.Controls.Add(this.label245);
            this.groupBox31.Controls.Add(this.SFL20);
            this.groupBox31.Controls.Add(this.label246);
            this.groupBox31.Controls.Add(this.SFL19);
            this.groupBox31.Controls.Add(this.label247);
            this.groupBox31.Controls.Add(this.SFL18);
            this.groupBox31.Controls.Add(this.label248);
            this.groupBox31.Controls.Add(this.SFL17);
            this.groupBox31.Controls.Add(this.label249);
            this.groupBox31.Controls.Add(this.SFL8);
            this.groupBox31.Controls.Add(this.label250);
            this.groupBox31.Controls.Add(this.SFL7);
            this.groupBox31.Controls.Add(this.label251);
            this.groupBox31.Controls.Add(this.SFL6);
            this.groupBox31.Controls.Add(this.label252);
            this.groupBox31.Controls.Add(this.SFL5);
            this.groupBox31.Controls.Add(this.label253);
            this.groupBox31.Controls.Add(this.SFL4);
            this.groupBox31.Controls.Add(this.label254);
            this.groupBox31.Controls.Add(this.SFL3);
            this.groupBox31.Controls.Add(this.label255);
            this.groupBox31.Controls.Add(this.SFL2);
            this.groupBox31.Controls.Add(this.label256);
            this.groupBox31.Controls.Add(this.SFL1);
            this.groupBox31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox31.Location = new System.Drawing.Point(3, 97);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(150, 329);
            this.groupBox31.TabIndex = 59;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "��������� ������";
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label257.Location = new System.Drawing.Point(78, 309);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 138;
            this.label257.Text = "���32";
            // 
            // SFL32
            // 
            this.SFL32.Location = new System.Drawing.Point(130, 308);
            this.SFL32.Name = "SFL32";
            this.SFL32.Size = new System.Drawing.Size(13, 13);
            this.SFL32.State = BEMN.Forms.LedState.Off;
            this.SFL32.TabIndex = 137;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label258.Location = new System.Drawing.Point(78, 290);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 136;
            this.label258.Text = "���31";
            // 
            // SFL31
            // 
            this.SFL31.Location = new System.Drawing.Point(130, 289);
            this.SFL31.Name = "SFL31";
            this.SFL31.Size = new System.Drawing.Size(13, 13);
            this.SFL31.State = BEMN.Forms.LedState.Off;
            this.SFL31.TabIndex = 135;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label259.Location = new System.Drawing.Point(78, 271);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 134;
            this.label259.Text = "���30";
            // 
            // SFL30
            // 
            this.SFL30.Location = new System.Drawing.Point(130, 270);
            this.SFL30.Name = "SFL30";
            this.SFL30.Size = new System.Drawing.Size(13, 13);
            this.SFL30.State = BEMN.Forms.LedState.Off;
            this.SFL30.TabIndex = 133;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label260.Location = new System.Drawing.Point(78, 252);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(41, 13);
            this.label260.TabIndex = 132;
            this.label260.Text = "���29";
            // 
            // SFL29
            // 
            this.SFL29.Location = new System.Drawing.Point(130, 251);
            this.SFL29.Name = "SFL29";
            this.SFL29.Size = new System.Drawing.Size(13, 13);
            this.SFL29.State = BEMN.Forms.LedState.Off;
            this.SFL29.TabIndex = 131;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label261.Location = new System.Drawing.Point(78, 233);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(41, 13);
            this.label261.TabIndex = 130;
            this.label261.Text = "���28";
            // 
            // SFL28
            // 
            this.SFL28.Location = new System.Drawing.Point(130, 232);
            this.SFL28.Name = "SFL28";
            this.SFL28.Size = new System.Drawing.Size(13, 13);
            this.SFL28.State = BEMN.Forms.LedState.Off;
            this.SFL28.TabIndex = 129;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label262.Location = new System.Drawing.Point(78, 214);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(41, 13);
            this.label262.TabIndex = 128;
            this.label262.Text = "���27";
            // 
            // SFL27
            // 
            this.SFL27.Location = new System.Drawing.Point(130, 213);
            this.SFL27.Name = "SFL27";
            this.SFL27.Size = new System.Drawing.Size(13, 13);
            this.SFL27.State = BEMN.Forms.LedState.Off;
            this.SFL27.TabIndex = 127;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label263.Location = new System.Drawing.Point(78, 195);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(41, 13);
            this.label263.TabIndex = 126;
            this.label263.Text = "���26";
            // 
            // SFL26
            // 
            this.SFL26.Location = new System.Drawing.Point(130, 194);
            this.SFL26.Name = "SFL26";
            this.SFL26.Size = new System.Drawing.Size(13, 13);
            this.SFL26.State = BEMN.Forms.LedState.Off;
            this.SFL26.TabIndex = 125;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label264.Location = new System.Drawing.Point(78, 176);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(41, 13);
            this.label264.TabIndex = 124;
            this.label264.Text = "���25";
            // 
            // SFL25
            // 
            this.SFL25.Location = new System.Drawing.Point(130, 175);
            this.SFL25.Name = "SFL25";
            this.SFL25.Size = new System.Drawing.Size(13, 13);
            this.SFL25.State = BEMN.Forms.LedState.Off;
            this.SFL25.TabIndex = 123;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label265.Location = new System.Drawing.Point(6, 308);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(41, 13);
            this.label265.TabIndex = 122;
            this.label265.Text = "���16";
            // 
            // SFL16
            // 
            this.SFL16.Location = new System.Drawing.Point(58, 308);
            this.SFL16.Name = "SFL16";
            this.SFL16.Size = new System.Drawing.Size(13, 13);
            this.SFL16.State = BEMN.Forms.LedState.Off;
            this.SFL16.TabIndex = 121;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label266.Location = new System.Drawing.Point(6, 289);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(41, 13);
            this.label266.TabIndex = 120;
            this.label266.Text = "���15";
            // 
            // SFL15
            // 
            this.SFL15.Location = new System.Drawing.Point(58, 289);
            this.SFL15.Name = "SFL15";
            this.SFL15.Size = new System.Drawing.Size(13, 13);
            this.SFL15.State = BEMN.Forms.LedState.Off;
            this.SFL15.TabIndex = 119;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label267.Location = new System.Drawing.Point(6, 270);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(41, 13);
            this.label267.TabIndex = 118;
            this.label267.Text = "���14";
            // 
            // SFL14
            // 
            this.SFL14.Location = new System.Drawing.Point(58, 270);
            this.SFL14.Name = "SFL14";
            this.SFL14.Size = new System.Drawing.Size(13, 13);
            this.SFL14.State = BEMN.Forms.LedState.Off;
            this.SFL14.TabIndex = 117;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label268.Location = new System.Drawing.Point(6, 251);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(41, 13);
            this.label268.TabIndex = 116;
            this.label268.Text = "���13";
            // 
            // SFL13
            // 
            this.SFL13.Location = new System.Drawing.Point(58, 251);
            this.SFL13.Name = "SFL13";
            this.SFL13.Size = new System.Drawing.Size(13, 13);
            this.SFL13.State = BEMN.Forms.LedState.Off;
            this.SFL13.TabIndex = 115;
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label269.Location = new System.Drawing.Point(6, 232);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(41, 13);
            this.label269.TabIndex = 114;
            this.label269.Text = "���12";
            // 
            // SFL12
            // 
            this.SFL12.Location = new System.Drawing.Point(58, 232);
            this.SFL12.Name = "SFL12";
            this.SFL12.Size = new System.Drawing.Size(13, 13);
            this.SFL12.State = BEMN.Forms.LedState.Off;
            this.SFL12.TabIndex = 113;
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label270.Location = new System.Drawing.Point(6, 213);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(41, 13);
            this.label270.TabIndex = 112;
            this.label270.Text = "���11";
            // 
            // SFL11
            // 
            this.SFL11.Location = new System.Drawing.Point(58, 213);
            this.SFL11.Name = "SFL11";
            this.SFL11.Size = new System.Drawing.Size(13, 13);
            this.SFL11.State = BEMN.Forms.LedState.Off;
            this.SFL11.TabIndex = 111;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label271.Location = new System.Drawing.Point(6, 194);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(41, 13);
            this.label271.TabIndex = 110;
            this.label271.Text = "���10";
            // 
            // SFL10
            // 
            this.SFL10.Location = new System.Drawing.Point(58, 194);
            this.SFL10.Name = "SFL10";
            this.SFL10.Size = new System.Drawing.Size(13, 13);
            this.SFL10.State = BEMN.Forms.LedState.Off;
            this.SFL10.TabIndex = 109;
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label272.Location = new System.Drawing.Point(6, 175);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(35, 13);
            this.label272.TabIndex = 108;
            this.label272.Text = "���9";
            // 
            // SFL9
            // 
            this.SFL9.Location = new System.Drawing.Point(58, 175);
            this.SFL9.Name = "SFL9";
            this.SFL9.Size = new System.Drawing.Size(13, 13);
            this.SFL9.State = BEMN.Forms.LedState.Off;
            this.SFL9.TabIndex = 107;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label241.Location = new System.Drawing.Point(78, 157);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 106;
            this.label241.Text = "���24";
            // 
            // SFL24
            // 
            this.SFL24.Location = new System.Drawing.Point(130, 156);
            this.SFL24.Name = "SFL24";
            this.SFL24.Size = new System.Drawing.Size(13, 13);
            this.SFL24.State = BEMN.Forms.LedState.Off;
            this.SFL24.TabIndex = 105;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label242.Location = new System.Drawing.Point(78, 138);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 104;
            this.label242.Text = "���23";
            // 
            // SFL23
            // 
            this.SFL23.Location = new System.Drawing.Point(130, 137);
            this.SFL23.Name = "SFL23";
            this.SFL23.Size = new System.Drawing.Size(13, 13);
            this.SFL23.State = BEMN.Forms.LedState.Off;
            this.SFL23.TabIndex = 103;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label243.Location = new System.Drawing.Point(78, 119);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 102;
            this.label243.Text = "���22";
            // 
            // SFL22
            // 
            this.SFL22.Location = new System.Drawing.Point(130, 118);
            this.SFL22.Name = "SFL22";
            this.SFL22.Size = new System.Drawing.Size(13, 13);
            this.SFL22.State = BEMN.Forms.LedState.Off;
            this.SFL22.TabIndex = 101;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label244.Location = new System.Drawing.Point(78, 100);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 100;
            this.label244.Text = "���21";
            // 
            // SFL21
            // 
            this.SFL21.Location = new System.Drawing.Point(130, 99);
            this.SFL21.Name = "SFL21";
            this.SFL21.Size = new System.Drawing.Size(13, 13);
            this.SFL21.State = BEMN.Forms.LedState.Off;
            this.SFL21.TabIndex = 99;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label245.Location = new System.Drawing.Point(78, 81);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 98;
            this.label245.Text = "���20";
            // 
            // SFL20
            // 
            this.SFL20.Location = new System.Drawing.Point(130, 80);
            this.SFL20.Name = "SFL20";
            this.SFL20.Size = new System.Drawing.Size(13, 13);
            this.SFL20.State = BEMN.Forms.LedState.Off;
            this.SFL20.TabIndex = 97;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label246.Location = new System.Drawing.Point(78, 62);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 96;
            this.label246.Text = "���19";
            // 
            // SFL19
            // 
            this.SFL19.Location = new System.Drawing.Point(130, 61);
            this.SFL19.Name = "SFL19";
            this.SFL19.Size = new System.Drawing.Size(13, 13);
            this.SFL19.State = BEMN.Forms.LedState.Off;
            this.SFL19.TabIndex = 95;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label247.Location = new System.Drawing.Point(78, 43);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 94;
            this.label247.Text = "���18";
            // 
            // SFL18
            // 
            this.SFL18.Location = new System.Drawing.Point(130, 42);
            this.SFL18.Name = "SFL18";
            this.SFL18.Size = new System.Drawing.Size(13, 13);
            this.SFL18.State = BEMN.Forms.LedState.Off;
            this.SFL18.TabIndex = 93;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label248.Location = new System.Drawing.Point(78, 24);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 92;
            this.label248.Text = "���17";
            // 
            // SFL17
            // 
            this.SFL17.Location = new System.Drawing.Point(130, 23);
            this.SFL17.Name = "SFL17";
            this.SFL17.Size = new System.Drawing.Size(13, 13);
            this.SFL17.State = BEMN.Forms.LedState.Off;
            this.SFL17.TabIndex = 91;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label249.Location = new System.Drawing.Point(6, 156);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(35, 13);
            this.label249.TabIndex = 90;
            this.label249.Text = "���8";
            // 
            // SFL8
            // 
            this.SFL8.Location = new System.Drawing.Point(58, 156);
            this.SFL8.Name = "SFL8";
            this.SFL8.Size = new System.Drawing.Size(13, 13);
            this.SFL8.State = BEMN.Forms.LedState.Off;
            this.SFL8.TabIndex = 89;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label250.Location = new System.Drawing.Point(6, 137);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(35, 13);
            this.label250.TabIndex = 88;
            this.label250.Text = "���7";
            // 
            // SFL7
            // 
            this.SFL7.Location = new System.Drawing.Point(58, 137);
            this.SFL7.Name = "SFL7";
            this.SFL7.Size = new System.Drawing.Size(13, 13);
            this.SFL7.State = BEMN.Forms.LedState.Off;
            this.SFL7.TabIndex = 87;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label251.Location = new System.Drawing.Point(6, 118);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(35, 13);
            this.label251.TabIndex = 86;
            this.label251.Text = "���6";
            // 
            // SFL6
            // 
            this.SFL6.Location = new System.Drawing.Point(58, 118);
            this.SFL6.Name = "SFL6";
            this.SFL6.Size = new System.Drawing.Size(13, 13);
            this.SFL6.State = BEMN.Forms.LedState.Off;
            this.SFL6.TabIndex = 85;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label252.Location = new System.Drawing.Point(6, 99);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(35, 13);
            this.label252.TabIndex = 84;
            this.label252.Text = "���5";
            // 
            // SFL5
            // 
            this.SFL5.Location = new System.Drawing.Point(58, 99);
            this.SFL5.Name = "SFL5";
            this.SFL5.Size = new System.Drawing.Size(13, 13);
            this.SFL5.State = BEMN.Forms.LedState.Off;
            this.SFL5.TabIndex = 83;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label253.Location = new System.Drawing.Point(6, 80);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(35, 13);
            this.label253.TabIndex = 82;
            this.label253.Text = "���4";
            // 
            // SFL4
            // 
            this.SFL4.Location = new System.Drawing.Point(58, 80);
            this.SFL4.Name = "SFL4";
            this.SFL4.Size = new System.Drawing.Size(13, 13);
            this.SFL4.State = BEMN.Forms.LedState.Off;
            this.SFL4.TabIndex = 81;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label254.Location = new System.Drawing.Point(6, 61);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(35, 13);
            this.label254.TabIndex = 80;
            this.label254.Text = "���3";
            // 
            // SFL3
            // 
            this.SFL3.Location = new System.Drawing.Point(58, 61);
            this.SFL3.Name = "SFL3";
            this.SFL3.Size = new System.Drawing.Size(13, 13);
            this.SFL3.State = BEMN.Forms.LedState.Off;
            this.SFL3.TabIndex = 79;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label255.Location = new System.Drawing.Point(6, 42);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(35, 13);
            this.label255.TabIndex = 78;
            this.label255.Text = "���2";
            // 
            // SFL2
            // 
            this.SFL2.Location = new System.Drawing.Point(58, 42);
            this.SFL2.Name = "SFL2";
            this.SFL2.Size = new System.Drawing.Size(13, 13);
            this.SFL2.State = BEMN.Forms.LedState.Off;
            this.SFL2.TabIndex = 77;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label256.Location = new System.Drawing.Point(6, 23);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(35, 13);
            this.label256.TabIndex = 76;
            this.label256.Text = "���1";
            // 
            // SFL1
            // 
            this.SFL1.Location = new System.Drawing.Point(58, 23);
            this.SFL1.Name = "SFL1";
            this.SFL1.Size = new System.Drawing.Size(13, 13);
            this.SFL1.State = BEMN.Forms.LedState.Off;
            this.SFL1.TabIndex = 75;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label226);
            this.groupBox29.Controls.Add(this._Fmin4);
            this.groupBox29.Controls.Add(this._Fmin4IO);
            this.groupBox29.Controls.Add(this.label230);
            this.groupBox29.Controls.Add(this.label231);
            this.groupBox29.Controls.Add(this.label232);
            this.groupBox29.Controls.Add(this.label233);
            this.groupBox29.Controls.Add(this._Fmin3);
            this.groupBox29.Controls.Add(this._Fmin2);
            this.groupBox29.Controls.Add(this._Fmin1);
            this.groupBox29.Controls.Add(this.label234);
            this.groupBox29.Controls.Add(this._Fmin3IO);
            this.groupBox29.Controls.Add(this._Fmin2IO);
            this.groupBox29.Controls.Add(this._Fmin1IO);
            this.groupBox29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox29.Location = new System.Drawing.Point(102, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(93, 88);
            this.groupBox29.TabIndex = 58;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "F <";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(6, 71);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(31, 13);
            this.label226.TabIndex = 118;
            this.label226.Text = "F < 4";
            // 
            // _Fmin4
            // 
            this._Fmin4.Location = new System.Drawing.Point(67, 71);
            this._Fmin4.Name = "_Fmin4";
            this._Fmin4.Size = new System.Drawing.Size(13, 13);
            this._Fmin4.State = BEMN.Forms.LedState.Off;
            this._Fmin4.TabIndex = 117;
            // 
            // _Fmin4IO
            // 
            this._Fmin4IO.Location = new System.Drawing.Point(41, 71);
            this._Fmin4IO.Name = "_Fmin4IO";
            this._Fmin4IO.Size = new System.Drawing.Size(13, 13);
            this._Fmin4IO.State = BEMN.Forms.LedState.Off;
            this._Fmin4IO.TabIndex = 116;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label230.Location = new System.Drawing.Point(6, 57);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(31, 13);
            this.label230.TabIndex = 115;
            this.label230.Text = "F < 3";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(6, 43);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(31, 13);
            this.label231.TabIndex = 114;
            this.label231.Text = "F < 2";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label232.ForeColor = System.Drawing.Color.Red;
            this.label232.Location = new System.Drawing.Point(57, 12);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(38, 13);
            this.label232.TabIndex = 113;
            this.label232.Text = "����.";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label233.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label233.Location = new System.Drawing.Point(37, 12);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(23, 13);
            this.label233.TabIndex = 112;
            this.label233.Text = "��";
            // 
            // _Fmin3
            // 
            this._Fmin3.Location = new System.Drawing.Point(67, 57);
            this._Fmin3.Name = "_Fmin3";
            this._Fmin3.Size = new System.Drawing.Size(13, 13);
            this._Fmin3.State = BEMN.Forms.LedState.Off;
            this._Fmin3.TabIndex = 98;
            // 
            // _Fmin2
            // 
            this._Fmin2.Location = new System.Drawing.Point(67, 43);
            this._Fmin2.Name = "_Fmin2";
            this._Fmin2.Size = new System.Drawing.Size(13, 13);
            this._Fmin2.State = BEMN.Forms.LedState.Off;
            this._Fmin2.TabIndex = 97;
            // 
            // _Fmin1
            // 
            this._Fmin1.Location = new System.Drawing.Point(67, 29);
            this._Fmin1.Name = "_Fmin1";
            this._Fmin1.Size = new System.Drawing.Size(13, 13);
            this._Fmin1.State = BEMN.Forms.LedState.Off;
            this._Fmin1.TabIndex = 96;
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label234.Location = new System.Drawing.Point(6, 29);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(31, 13);
            this.label234.TabIndex = 89;
            this.label234.Text = "F < 1";
            // 
            // _Fmin3IO
            // 
            this._Fmin3IO.Location = new System.Drawing.Point(41, 57);
            this._Fmin3IO.Name = "_Fmin3IO";
            this._Fmin3IO.Size = new System.Drawing.Size(13, 13);
            this._Fmin3IO.State = BEMN.Forms.LedState.Off;
            this._Fmin3IO.TabIndex = 82;
            // 
            // _Fmin2IO
            // 
            this._Fmin2IO.Location = new System.Drawing.Point(41, 43);
            this._Fmin2IO.Name = "_Fmin2IO";
            this._Fmin2IO.Size = new System.Drawing.Size(13, 13);
            this._Fmin2IO.State = BEMN.Forms.LedState.Off;
            this._Fmin2IO.TabIndex = 81;
            // 
            // _Fmin1IO
            // 
            this._Fmin1IO.BackColor = System.Drawing.Color.Transparent;
            this._Fmin1IO.Location = new System.Drawing.Point(41, 29);
            this._Fmin1IO.Name = "_Fmin1IO";
            this._Fmin1IO.Size = new System.Drawing.Size(13, 13);
            this._Fmin1IO.State = BEMN.Forms.LedState.Off;
            this._Fmin1IO.TabIndex = 80;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label235);
            this.groupBox30.Controls.Add(this._Fmax4);
            this.groupBox30.Controls.Add(this._Fmax4IO);
            this.groupBox30.Controls.Add(this.label236);
            this.groupBox30.Controls.Add(this.label237);
            this.groupBox30.Controls.Add(this.label238);
            this.groupBox30.Controls.Add(this.label239);
            this.groupBox30.Controls.Add(this._Fmax3);
            this.groupBox30.Controls.Add(this._Fmax2);
            this.groupBox30.Controls.Add(this._Fmax1);
            this.groupBox30.Controls.Add(this.label240);
            this.groupBox30.Controls.Add(this._Fmax3IO);
            this.groupBox30.Controls.Add(this._Fmax2IO);
            this.groupBox30.Controls.Add(this._Fmax1IO);
            this.groupBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox30.Location = new System.Drawing.Point(3, 3);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(93, 88);
            this.groupBox30.TabIndex = 57;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "F >";
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label235.Location = new System.Drawing.Point(6, 71);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(31, 13);
            this.label235.TabIndex = 118;
            this.label235.Text = "F > 4";
            // 
            // _Fmax4
            // 
            this._Fmax4.Location = new System.Drawing.Point(67, 71);
            this._Fmax4.Name = "_Fmax4";
            this._Fmax4.Size = new System.Drawing.Size(13, 13);
            this._Fmax4.State = BEMN.Forms.LedState.Off;
            this._Fmax4.TabIndex = 117;
            // 
            // _Fmax4IO
            // 
            this._Fmax4IO.Location = new System.Drawing.Point(41, 71);
            this._Fmax4IO.Name = "_Fmax4IO";
            this._Fmax4IO.Size = new System.Drawing.Size(13, 13);
            this._Fmax4IO.State = BEMN.Forms.LedState.Off;
            this._Fmax4IO.TabIndex = 116;
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label236.Location = new System.Drawing.Point(6, 57);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(31, 13);
            this.label236.TabIndex = 115;
            this.label236.Text = "F > 3";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label237.Location = new System.Drawing.Point(6, 43);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(31, 13);
            this.label237.TabIndex = 114;
            this.label237.Text = "F > 2";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label238.ForeColor = System.Drawing.Color.Red;
            this.label238.Location = new System.Drawing.Point(57, 12);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(38, 13);
            this.label238.TabIndex = 113;
            this.label238.Text = "����.";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label239.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label239.Location = new System.Drawing.Point(37, 12);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(23, 13);
            this.label239.TabIndex = 112;
            this.label239.Text = "��";
            // 
            // _Fmax3
            // 
            this._Fmax3.Location = new System.Drawing.Point(67, 57);
            this._Fmax3.Name = "_Fmax3";
            this._Fmax3.Size = new System.Drawing.Size(13, 13);
            this._Fmax3.State = BEMN.Forms.LedState.Off;
            this._Fmax3.TabIndex = 98;
            // 
            // _Fmax2
            // 
            this._Fmax2.Location = new System.Drawing.Point(67, 43);
            this._Fmax2.Name = "_Fmax2";
            this._Fmax2.Size = new System.Drawing.Size(13, 13);
            this._Fmax2.State = BEMN.Forms.LedState.Off;
            this._Fmax2.TabIndex = 97;
            // 
            // _Fmax1
            // 
            this._Fmax1.Location = new System.Drawing.Point(67, 29);
            this._Fmax1.Name = "_Fmax1";
            this._Fmax1.Size = new System.Drawing.Size(13, 13);
            this._Fmax1.State = BEMN.Forms.LedState.Off;
            this._Fmax1.TabIndex = 96;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label240.Location = new System.Drawing.Point(6, 29);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(31, 13);
            this.label240.TabIndex = 89;
            this.label240.Text = "F > 1";
            // 
            // _Fmax3IO
            // 
            this._Fmax3IO.Location = new System.Drawing.Point(41, 57);
            this._Fmax3IO.Name = "_Fmax3IO";
            this._Fmax3IO.Size = new System.Drawing.Size(13, 13);
            this._Fmax3IO.State = BEMN.Forms.LedState.Off;
            this._Fmax3IO.TabIndex = 82;
            // 
            // _Fmax2IO
            // 
            this._Fmax2IO.Location = new System.Drawing.Point(41, 43);
            this._Fmax2IO.Name = "_Fmax2IO";
            this._Fmax2IO.Size = new System.Drawing.Size(13, 13);
            this._Fmax2IO.State = BEMN.Forms.LedState.Off;
            this._Fmax2IO.TabIndex = 81;
            // 
            // _Fmax1IO
            // 
            this._Fmax1IO.BackColor = System.Drawing.Color.Transparent;
            this._Fmax1IO.Location = new System.Drawing.Point(41, 29);
            this._Fmax1IO.Name = "_Fmax1IO";
            this._Fmax1IO.Size = new System.Drawing.Size(13, 13);
            this._Fmax1IO.State = BEMN.Forms.LedState.Off;
            this._Fmax1IO.TabIndex = 80;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label191);
            this.groupBox15.Controls.Add(this._Osc);
            this.groupBox15.Controls.Add(this.label192);
            this.groupBox15.Controls.Add(this._Jalm);
            this.groupBox15.Controls.Add(this.label193);
            this.groupBox15.Controls.Add(this._Jsys);
            this.groupBox15.Controls.Add(this.label194);
            this.groupBox15.Controls.Add(this._Passwust);
            this.groupBox15.Controls.Add(this.label209);
            this.groupBox15.Controls.Add(this._Groupust);
            this.groupBox15.Controls.Add(this.label210);
            this.groupBox15.Controls.Add(this._Ust);
            this.groupBox15.Controls.Add(this.label211);
            this.groupBox15.Controls.Add(this._Mod5);
            this.groupBox15.Controls.Add(this.label212);
            this.groupBox15.Controls.Add(this._Mod4);
            this.groupBox15.Controls.Add(this.label213);
            this.groupBox15.Controls.Add(this._Mod3);
            this.groupBox15.Controls.Add(this.label214);
            this.groupBox15.Controls.Add(this._Mod2);
            this.groupBox15.Controls.Add(this.label215);
            this.groupBox15.Controls.Add(this._Mod1);
            this.groupBox15.Controls.Add(this.label216);
            this.groupBox15.Controls.Add(this._Contr);
            this.groupBox15.Controls.Add(this.label217);
            this.groupBox15.Controls.Add(this._Switch3);
            this.groupBox15.Controls.Add(this.label218);
            this.groupBox15.Controls.Add(this._Switch2);
            this.groupBox15.Controls.Add(this.label219);
            this.groupBox15.Controls.Add(this._Switch1);
            this.groupBox15.Controls.Add(this.label220);
            this.groupBox15.Controls.Add(this._Meas);
            this.groupBox15.Controls.Add(this.label221);
            this.groupBox15.Controls.Add(this._Soft);
            this.groupBox15.Controls.Add(this.label222);
            this.groupBox15.Controls.Add(this._Hard);
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox15.Location = new System.Drawing.Point(256, 354);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(275, 127);
            this.groupBox15.TabIndex = 62;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "�������������";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label191.Location = new System.Drawing.Point(198, 106);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(44, 13);
            this.label191.TabIndex = 108;
            this.label191.Text = "��� - �";
            // 
            // _Osc
            // 
            this._Osc.Location = new System.Drawing.Point(255, 106);
            this._Osc.Name = "_Osc";
            this._Osc.Size = new System.Drawing.Size(13, 13);
            this._Osc.State = BEMN.Forms.LedState.Off;
            this._Osc.TabIndex = 107;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label192.Location = new System.Drawing.Point(198, 91);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 106;
            this.label192.Text = "��";
            // 
            // _Jalm
            // 
            this._Jalm.Location = new System.Drawing.Point(255, 90);
            this._Jalm.Name = "_Jalm";
            this._Jalm.Size = new System.Drawing.Size(13, 13);
            this._Jalm.State = BEMN.Forms.LedState.Off;
            this._Jalm.TabIndex = 105;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label193.Location = new System.Drawing.Point(198, 75);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(25, 13);
            this.label193.TabIndex = 104;
            this.label193.Text = "��";
            // 
            // _Jsys
            // 
            this._Jsys.Location = new System.Drawing.Point(255, 74);
            this._Jsys.Name = "_Jsys";
            this._Jsys.Size = new System.Drawing.Size(13, 13);
            this._Jsys.State = BEMN.Forms.LedState.Off;
            this._Jsys.TabIndex = 103;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label194.Location = new System.Drawing.Point(198, 59);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(45, 13);
            this.label194.TabIndex = 102;
            this.label194.Text = "������";
            // 
            // _Passwust
            // 
            this._Passwust.Location = new System.Drawing.Point(255, 58);
            this._Passwust.Name = "_Passwust";
            this._Passwust.Size = new System.Drawing.Size(13, 13);
            this._Passwust.State = BEMN.Forms.LedState.Off;
            this._Passwust.TabIndex = 101;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label209.Location = new System.Drawing.Point(198, 43);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(55, 13);
            this.label209.TabIndex = 100;
            this.label209.Text = "����. ���.";
            // 
            // _Groupust
            // 
            this._Groupust.Location = new System.Drawing.Point(255, 42);
            this._Groupust.Name = "_Groupust";
            this._Groupust.Size = new System.Drawing.Size(13, 13);
            this._Groupust.State = BEMN.Forms.LedState.Off;
            this._Groupust.TabIndex = 99;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label210.Location = new System.Drawing.Point(198, 26);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(50, 13);
            this.label210.TabIndex = 98;
            this.label210.Text = "�������";
            // 
            // _Ust
            // 
            this._Ust.Location = new System.Drawing.Point(255, 26);
            this._Ust.Name = "_Ust";
            this._Ust.Size = new System.Drawing.Size(13, 13);
            this._Ust.State = BEMN.Forms.LedState.Off;
            this._Ust.TabIndex = 97;
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label211.Location = new System.Drawing.Point(112, 106);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(54, 13);
            this.label211.TabIndex = 96;
            this.label211.Text = "������ 5";
            // 
            // _Mod5
            // 
            this._Mod5.Location = new System.Drawing.Point(174, 106);
            this._Mod5.Name = "_Mod5";
            this._Mod5.Size = new System.Drawing.Size(13, 13);
            this._Mod5.State = BEMN.Forms.LedState.Off;
            this._Mod5.TabIndex = 95;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label212.Location = new System.Drawing.Point(112, 90);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(54, 13);
            this.label212.TabIndex = 94;
            this.label212.Text = "������ 4";
            // 
            // _Mod4
            // 
            this._Mod4.Location = new System.Drawing.Point(174, 90);
            this._Mod4.Name = "_Mod4";
            this._Mod4.Size = new System.Drawing.Size(13, 13);
            this._Mod4.State = BEMN.Forms.LedState.Off;
            this._Mod4.TabIndex = 93;
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label213.Location = new System.Drawing.Point(112, 74);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(54, 13);
            this.label213.TabIndex = 92;
            this.label213.Text = "������ 3";
            // 
            // _Mod3
            // 
            this._Mod3.Location = new System.Drawing.Point(174, 74);
            this._Mod3.Name = "_Mod3";
            this._Mod3.Size = new System.Drawing.Size(13, 13);
            this._Mod3.State = BEMN.Forms.LedState.Off;
            this._Mod3.TabIndex = 91;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label214.Location = new System.Drawing.Point(112, 58);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(54, 13);
            this.label214.TabIndex = 90;
            this.label214.Text = "������ 2";
            // 
            // _Mod2
            // 
            this._Mod2.Location = new System.Drawing.Point(174, 58);
            this._Mod2.Name = "_Mod2";
            this._Mod2.Size = new System.Drawing.Size(13, 13);
            this._Mod2.State = BEMN.Forms.LedState.Off;
            this._Mod2.TabIndex = 89;
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label215.Location = new System.Drawing.Point(112, 42);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(54, 13);
            this.label215.TabIndex = 88;
            this.label215.Text = "������ 1";
            // 
            // _Mod1
            // 
            this._Mod1.Location = new System.Drawing.Point(174, 42);
            this._Mod1.Name = "_Mod1";
            this._Mod1.Size = new System.Drawing.Size(13, 13);
            this._Mod1.State = BEMN.Forms.LedState.Off;
            this._Mod1.TabIndex = 87;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label216.Location = new System.Drawing.Point(112, 26);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(62, 13);
            this.label216.TabIndex = 86;
            this.label216.Text = "����� ���.";
            // 
            // _Contr
            // 
            this._Contr.Location = new System.Drawing.Point(174, 26);
            this._Contr.Name = "_Contr";
            this._Contr.Size = new System.Drawing.Size(13, 13);
            this._Contr.State = BEMN.Forms.LedState.Off;
            this._Contr.TabIndex = 85;
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label217.Location = new System.Drawing.Point(3, 107);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(85, 13);
            this.label217.TabIndex = 84;
            this.label217.Text = "����������� 3";
            // 
            // _Switch3
            // 
            this._Switch3.Location = new System.Drawing.Point(89, 106);
            this._Switch3.Name = "_Switch3";
            this._Switch3.Size = new System.Drawing.Size(13, 13);
            this._Switch3.State = BEMN.Forms.LedState.Off;
            this._Switch3.TabIndex = 83;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label218.Location = new System.Drawing.Point(3, 91);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(85, 13);
            this.label218.TabIndex = 82;
            this.label218.Text = "����������� 2";
            // 
            // _Switch2
            // 
            this._Switch2.Location = new System.Drawing.Point(89, 90);
            this._Switch2.Name = "_Switch2";
            this._Switch2.Size = new System.Drawing.Size(13, 13);
            this._Switch2.State = BEMN.Forms.LedState.Off;
            this._Switch2.TabIndex = 81;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label219.Location = new System.Drawing.Point(3, 75);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(85, 13);
            this.label219.TabIndex = 80;
            this.label219.Text = "����������� 1";
            // 
            // _Switch1
            // 
            this._Switch1.Location = new System.Drawing.Point(89, 74);
            this._Switch1.Name = "_Switch1";
            this._Switch1.Size = new System.Drawing.Size(13, 13);
            this._Switch1.State = BEMN.Forms.LedState.Off;
            this._Switch1.TabIndex = 79;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label220.Location = new System.Drawing.Point(3, 59);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(65, 13);
            this.label220.TabIndex = 78;
            this.label220.Text = "���������";
            // 
            // _Meas
            // 
            this._Meas.Location = new System.Drawing.Point(89, 58);
            this._Meas.Name = "_Meas";
            this._Meas.Size = new System.Drawing.Size(13, 13);
            this._Meas.State = BEMN.Forms.LedState.Off;
            this._Meas.TabIndex = 77;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label221.Location = new System.Drawing.Point(3, 43);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(78, 13);
            this.label221.TabIndex = 76;
            this.label221.Text = "�����������";
            // 
            // _Soft
            // 
            this._Soft.Location = new System.Drawing.Point(89, 42);
            this._Soft.Name = "_Soft";
            this._Soft.Size = new System.Drawing.Size(13, 13);
            this._Soft.State = BEMN.Forms.LedState.Off;
            this._Soft.TabIndex = 75;
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label222.Location = new System.Drawing.Point(3, 27);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(67, 13);
            this.label222.TabIndex = 74;
            this.label222.Text = "����������";
            // 
            // _Hard
            // 
            this._Hard.Location = new System.Drawing.Point(89, 26);
            this._Hard.Name = "_Hard";
            this._Hard.Size = new System.Drawing.Size(13, 13);
            this._Hard.State = BEMN.Forms.LedState.Off;
            this._Hard.TabIndex = 0;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label195);
            this.groupBox26.Controls.Add(this.label196);
            this.groupBox26.Controls.Add(this.label197);
            this.groupBox26.Controls.Add(this.label198);
            this.groupBox26.Controls.Add(this.label199);
            this.groupBox26.Controls.Add(this.label200);
            this.groupBox26.Controls.Add(this.label201);
            this.groupBox26.Controls.Add(this.label202);
            this.groupBox26.Controls.Add(this.label203);
            this.groupBox26.Controls.Add(this.label204);
            this.groupBox26.Controls.Add(this.label205);
            this.groupBox26.Controls.Add(this.label206);
            this.groupBox26.Controls.Add(this._Ind12);
            this.groupBox26.Controls.Add(this._Ind11);
            this.groupBox26.Controls.Add(this._Ind10);
            this.groupBox26.Controls.Add(this._Ind9);
            this.groupBox26.Controls.Add(this._Ind8);
            this.groupBox26.Controls.Add(this._Ind7);
            this.groupBox26.Controls.Add(this._Ind6);
            this.groupBox26.Controls.Add(this._Ind5);
            this.groupBox26.Controls.Add(this._Ind4);
            this.groupBox26.Controls.Add(this._Ind3);
            this.groupBox26.Controls.Add(this._Ind2);
            this.groupBox26.Controls.Add(this._Ind1);
            this.groupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox26.Location = new System.Drawing.Point(146, 354);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(104, 127);
            this.groupBox26.TabIndex = 61;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "����������";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label195.Location = new System.Drawing.Point(54, 108);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(19, 13);
            this.label195.TabIndex = 116;
            this.label195.Text = "12";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label196.Location = new System.Drawing.Point(54, 91);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(19, 13);
            this.label196.TabIndex = 115;
            this.label196.Text = "11";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label197.Location = new System.Drawing.Point(54, 74);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(19, 13);
            this.label197.TabIndex = 114;
            this.label197.Text = "10";
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label198.Location = new System.Drawing.Point(54, 57);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(13, 13);
            this.label198.TabIndex = 113;
            this.label198.Text = "9";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label199.Location = new System.Drawing.Point(54, 40);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(13, 13);
            this.label199.TabIndex = 112;
            this.label199.Text = "8";
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label200.Location = new System.Drawing.Point(54, 23);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(13, 13);
            this.label200.TabIndex = 111;
            this.label200.Text = "7";
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label201.Location = new System.Drawing.Point(11, 108);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(13, 13);
            this.label201.TabIndex = 110;
            this.label201.Text = "6";
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label202.Location = new System.Drawing.Point(11, 91);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(13, 13);
            this.label202.TabIndex = 109;
            this.label202.Text = "5";
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label203.Location = new System.Drawing.Point(11, 74);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(13, 13);
            this.label203.TabIndex = 108;
            this.label203.Text = "4";
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label204.Location = new System.Drawing.Point(11, 57);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(13, 13);
            this.label204.TabIndex = 107;
            this.label204.Text = "3";
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label205.Location = new System.Drawing.Point(11, 40);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(13, 13);
            this.label205.TabIndex = 106;
            this.label205.Text = "2";
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label206.Location = new System.Drawing.Point(11, 23);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(13, 13);
            this.label206.TabIndex = 105;
            this.label206.Text = "1";
            // 
            // _Ind12
            // 
            this._Ind12.Location = new System.Drawing.Point(79, 108);
            this._Ind12.Name = "_Ind12";
            this._Ind12.Size = new System.Drawing.Size(13, 13);
            this._Ind12.State = BEMN.Forms.LedState.Off;
            this._Ind12.TabIndex = 95;
            // 
            // _Ind11
            // 
            this._Ind11.Location = new System.Drawing.Point(79, 91);
            this._Ind11.Name = "_Ind11";
            this._Ind11.Size = new System.Drawing.Size(13, 13);
            this._Ind11.State = BEMN.Forms.LedState.Off;
            this._Ind11.TabIndex = 93;
            // 
            // _Ind10
            // 
            this._Ind10.Location = new System.Drawing.Point(79, 74);
            this._Ind10.Name = "_Ind10";
            this._Ind10.Size = new System.Drawing.Size(13, 13);
            this._Ind10.State = BEMN.Forms.LedState.Off;
            this._Ind10.TabIndex = 91;
            // 
            // _Ind9
            // 
            this._Ind9.Location = new System.Drawing.Point(79, 57);
            this._Ind9.Name = "_Ind9";
            this._Ind9.Size = new System.Drawing.Size(13, 13);
            this._Ind9.State = BEMN.Forms.LedState.Off;
            this._Ind9.TabIndex = 89;
            // 
            // _Ind8
            // 
            this._Ind8.Location = new System.Drawing.Point(79, 40);
            this._Ind8.Name = "_Ind8";
            this._Ind8.Size = new System.Drawing.Size(13, 13);
            this._Ind8.State = BEMN.Forms.LedState.Off;
            this._Ind8.TabIndex = 87;
            // 
            // _Ind7
            // 
            this._Ind7.Location = new System.Drawing.Point(79, 23);
            this._Ind7.Name = "_Ind7";
            this._Ind7.Size = new System.Drawing.Size(13, 13);
            this._Ind7.State = BEMN.Forms.LedState.Off;
            this._Ind7.TabIndex = 85;
            // 
            // _Ind6
            // 
            this._Ind6.Location = new System.Drawing.Point(30, 108);
            this._Ind6.Name = "_Ind6";
            this._Ind6.Size = new System.Drawing.Size(13, 13);
            this._Ind6.State = BEMN.Forms.LedState.Off;
            this._Ind6.TabIndex = 83;
            // 
            // _Ind5
            // 
            this._Ind5.Location = new System.Drawing.Point(30, 91);
            this._Ind5.Name = "_Ind5";
            this._Ind5.Size = new System.Drawing.Size(13, 13);
            this._Ind5.State = BEMN.Forms.LedState.Off;
            this._Ind5.TabIndex = 81;
            // 
            // _Ind4
            // 
            this._Ind4.Location = new System.Drawing.Point(30, 74);
            this._Ind4.Name = "_Ind4";
            this._Ind4.Size = new System.Drawing.Size(13, 13);
            this._Ind4.State = BEMN.Forms.LedState.Off;
            this._Ind4.TabIndex = 79;
            // 
            // _Ind3
            // 
            this._Ind3.Location = new System.Drawing.Point(30, 57);
            this._Ind3.Name = "_Ind3";
            this._Ind3.Size = new System.Drawing.Size(13, 13);
            this._Ind3.State = BEMN.Forms.LedState.Off;
            this._Ind3.TabIndex = 77;
            // 
            // _Ind2
            // 
            this._Ind2.Location = new System.Drawing.Point(30, 40);
            this._Ind2.Name = "_Ind2";
            this._Ind2.Size = new System.Drawing.Size(13, 13);
            this._Ind2.State = BEMN.Forms.LedState.Off;
            this._Ind2.TabIndex = 75;
            // 
            // _Ind1
            // 
            this._Ind1.Location = new System.Drawing.Point(30, 23);
            this._Ind1.Name = "_Ind1";
            this._Ind1.Size = new System.Drawing.Size(13, 13);
            this._Ind1.State = BEMN.Forms.LedState.Off;
            this._Ind1.TabIndex = 0;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.label207);
            this.groupBox25.Controls.Add(this._Rele18);
            this.groupBox25.Controls.Add(this.label208);
            this.groupBox25.Controls.Add(this._Rele17);
            this.groupBox25.Controls.Add(this.label175);
            this.groupBox25.Controls.Add(this._Rele16);
            this.groupBox25.Controls.Add(this.label176);
            this.groupBox25.Controls.Add(this._Rele15);
            this.groupBox25.Controls.Add(this.label177);
            this.groupBox25.Controls.Add(this._Rele14);
            this.groupBox25.Controls.Add(this.label178);
            this.groupBox25.Controls.Add(this._Rele13);
            this.groupBox25.Controls.Add(this.label179);
            this.groupBox25.Controls.Add(this._Rele12);
            this.groupBox25.Controls.Add(this.label180);
            this.groupBox25.Controls.Add(this._Rele11);
            this.groupBox25.Controls.Add(this.label181);
            this.groupBox25.Controls.Add(this._Rele10);
            this.groupBox25.Controls.Add(this.label182);
            this.groupBox25.Controls.Add(this._Rele9);
            this.groupBox25.Controls.Add(this.label183);
            this.groupBox25.Controls.Add(this._Rele8);
            this.groupBox25.Controls.Add(this.label184);
            this.groupBox25.Controls.Add(this._Rele7);
            this.groupBox25.Controls.Add(this.label185);
            this.groupBox25.Controls.Add(this._Rele6);
            this.groupBox25.Controls.Add(this.label186);
            this.groupBox25.Controls.Add(this._Rele5);
            this.groupBox25.Controls.Add(this.label187);
            this.groupBox25.Controls.Add(this._Rele4);
            this.groupBox25.Controls.Add(this.label188);
            this.groupBox25.Controls.Add(this._Rele3);
            this.groupBox25.Controls.Add(this.label189);
            this.groupBox25.Controls.Add(this._Rele2);
            this.groupBox25.Controls.Add(this.label190);
            this.groupBox25.Controls.Add(this._Rele1);
            this.groupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox25.Location = new System.Drawing.Point(6, 354);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(134, 127);
            this.groupBox25.TabIndex = 60;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "����";
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label207.Location = new System.Drawing.Point(85, 107);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(19, 13);
            this.label207.TabIndex = 108;
            this.label207.Text = "18";
            // 
            // _Rele18
            // 
            this._Rele18.Location = new System.Drawing.Point(110, 106);
            this._Rele18.Name = "_Rele18";
            this._Rele18.Size = new System.Drawing.Size(13, 13);
            this._Rele18.State = BEMN.Forms.LedState.Off;
            this._Rele18.TabIndex = 107;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label208.Location = new System.Drawing.Point(85, 91);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(19, 13);
            this.label208.TabIndex = 106;
            this.label208.Text = "17";
            // 
            // _Rele17
            // 
            this._Rele17.Location = new System.Drawing.Point(110, 90);
            this._Rele17.Name = "_Rele17";
            this._Rele17.Size = new System.Drawing.Size(13, 13);
            this._Rele17.State = BEMN.Forms.LedState.Off;
            this._Rele17.TabIndex = 105;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label175.Location = new System.Drawing.Point(85, 75);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(19, 13);
            this.label175.TabIndex = 104;
            this.label175.Text = "16";
            // 
            // _Rele16
            // 
            this._Rele16.Location = new System.Drawing.Point(110, 74);
            this._Rele16.Name = "_Rele16";
            this._Rele16.Size = new System.Drawing.Size(13, 13);
            this._Rele16.State = BEMN.Forms.LedState.Off;
            this._Rele16.TabIndex = 103;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label176.Location = new System.Drawing.Point(85, 59);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(19, 13);
            this.label176.TabIndex = 102;
            this.label176.Text = "15";
            // 
            // _Rele15
            // 
            this._Rele15.Location = new System.Drawing.Point(110, 58);
            this._Rele15.Name = "_Rele15";
            this._Rele15.Size = new System.Drawing.Size(13, 13);
            this._Rele15.State = BEMN.Forms.LedState.Off;
            this._Rele15.TabIndex = 101;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label177.Location = new System.Drawing.Point(85, 43);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 100;
            this.label177.Text = "14";
            // 
            // _Rele14
            // 
            this._Rele14.Location = new System.Drawing.Point(110, 42);
            this._Rele14.Name = "_Rele14";
            this._Rele14.Size = new System.Drawing.Size(13, 13);
            this._Rele14.State = BEMN.Forms.LedState.Off;
            this._Rele14.TabIndex = 99;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label178.Location = new System.Drawing.Point(85, 27);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 98;
            this.label178.Text = "13";
            // 
            // _Rele13
            // 
            this._Rele13.Location = new System.Drawing.Point(110, 26);
            this._Rele13.Name = "_Rele13";
            this._Rele13.Size = new System.Drawing.Size(13, 13);
            this._Rele13.State = BEMN.Forms.LedState.Off;
            this._Rele13.TabIndex = 97;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label179.Location = new System.Drawing.Point(41, 107);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 96;
            this.label179.Text = "12";
            // 
            // _Rele12
            // 
            this._Rele12.Location = new System.Drawing.Point(66, 107);
            this._Rele12.Name = "_Rele12";
            this._Rele12.Size = new System.Drawing.Size(13, 13);
            this._Rele12.State = BEMN.Forms.LedState.Off;
            this._Rele12.TabIndex = 95;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label180.Location = new System.Drawing.Point(41, 91);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(19, 13);
            this.label180.TabIndex = 94;
            this.label180.Text = "11";
            // 
            // _Rele11
            // 
            this._Rele11.Location = new System.Drawing.Point(66, 91);
            this._Rele11.Name = "_Rele11";
            this._Rele11.Size = new System.Drawing.Size(13, 13);
            this._Rele11.State = BEMN.Forms.LedState.Off;
            this._Rele11.TabIndex = 93;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label181.Location = new System.Drawing.Point(41, 75);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(19, 13);
            this.label181.TabIndex = 92;
            this.label181.Text = "10";
            // 
            // _Rele10
            // 
            this._Rele10.Location = new System.Drawing.Point(66, 75);
            this._Rele10.Name = "_Rele10";
            this._Rele10.Size = new System.Drawing.Size(13, 13);
            this._Rele10.State = BEMN.Forms.LedState.Off;
            this._Rele10.TabIndex = 91;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label182.Location = new System.Drawing.Point(47, 59);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 90;
            this.label182.Text = "9";
            // 
            // _Rele9
            // 
            this._Rele9.Location = new System.Drawing.Point(66, 59);
            this._Rele9.Name = "_Rele9";
            this._Rele9.Size = new System.Drawing.Size(13, 13);
            this._Rele9.State = BEMN.Forms.LedState.Off;
            this._Rele9.TabIndex = 89;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label183.Location = new System.Drawing.Point(47, 43);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(13, 13);
            this.label183.TabIndex = 88;
            this.label183.Text = "8";
            // 
            // _Rele8
            // 
            this._Rele8.Location = new System.Drawing.Point(66, 43);
            this._Rele8.Name = "_Rele8";
            this._Rele8.Size = new System.Drawing.Size(13, 13);
            this._Rele8.State = BEMN.Forms.LedState.Off;
            this._Rele8.TabIndex = 87;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label184.Location = new System.Drawing.Point(47, 27);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 86;
            this.label184.Text = "7";
            // 
            // _Rele7
            // 
            this._Rele7.Location = new System.Drawing.Point(66, 27);
            this._Rele7.Name = "_Rele7";
            this._Rele7.Size = new System.Drawing.Size(13, 13);
            this._Rele7.State = BEMN.Forms.LedState.Off;
            this._Rele7.TabIndex = 85;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label185.Location = new System.Drawing.Point(6, 107);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 84;
            this.label185.Text = "6";
            // 
            // _Rele6
            // 
            this._Rele6.Location = new System.Drawing.Point(25, 107);
            this._Rele6.Name = "_Rele6";
            this._Rele6.Size = new System.Drawing.Size(13, 13);
            this._Rele6.State = BEMN.Forms.LedState.Off;
            this._Rele6.TabIndex = 83;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label186.Location = new System.Drawing.Point(6, 91);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 82;
            this.label186.Text = "5";
            // 
            // _Rele5
            // 
            this._Rele5.Location = new System.Drawing.Point(25, 91);
            this._Rele5.Name = "_Rele5";
            this._Rele5.Size = new System.Drawing.Size(13, 13);
            this._Rele5.State = BEMN.Forms.LedState.Off;
            this._Rele5.TabIndex = 81;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label187.Location = new System.Drawing.Point(6, 75);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 80;
            this.label187.Text = "4";
            // 
            // _Rele4
            // 
            this._Rele4.Location = new System.Drawing.Point(25, 75);
            this._Rele4.Name = "_Rele4";
            this._Rele4.Size = new System.Drawing.Size(13, 13);
            this._Rele4.State = BEMN.Forms.LedState.Off;
            this._Rele4.TabIndex = 79;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label188.Location = new System.Drawing.Point(6, 59);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 78;
            this.label188.Text = "3";
            // 
            // _Rele3
            // 
            this._Rele3.Location = new System.Drawing.Point(25, 59);
            this._Rele3.Name = "_Rele3";
            this._Rele3.Size = new System.Drawing.Size(13, 13);
            this._Rele3.State = BEMN.Forms.LedState.Off;
            this._Rele3.TabIndex = 77;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label189.Location = new System.Drawing.Point(6, 43);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 76;
            this.label189.Text = "2";
            // 
            // _Rele2
            // 
            this._Rele2.Location = new System.Drawing.Point(25, 43);
            this._Rele2.Name = "_Rele2";
            this._Rele2.Size = new System.Drawing.Size(13, 13);
            this._Rele2.State = BEMN.Forms.LedState.Off;
            this._Rele2.TabIndex = 75;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label190.Location = new System.Drawing.Point(6, 27);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(13, 13);
            this.label190.TabIndex = 74;
            this.label190.Text = "1";
            // 
            // _Rele1
            // 
            this._Rele1.Location = new System.Drawing.Point(25, 27);
            this._Rele1.Name = "_Rele1";
            this._Rele1.Size = new System.Drawing.Size(13, 13);
            this._Rele1.State = BEMN.Forms.LedState.Off;
            this._Rele1.TabIndex = 0;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.label174);
            this.groupBox24.Controls.Add(this._Alarm);
            this.groupBox24.Controls.Add(this.label171);
            this.groupBox24.Controls.Add(this._GroupRez);
            this.groupBox24.Controls.Add(this.label170);
            this.groupBox24.Controls.Add(this.label173);
            this.groupBox24.Controls.Add(this._GroopMain);
            this.groupBox24.Controls.Add(this._Disrepair);
            this.groupBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox24.Location = new System.Drawing.Point(431, 184);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(100, 87);
            this.groupBox24.TabIndex = 59;
            this.groupBox24.TabStop = false;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label174.Location = new System.Drawing.Point(6, 65);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(64, 13);
            this.label174.TabIndex = 120;
            this.label174.Text = "����. ����.";
            // 
            // _Alarm
            // 
            this._Alarm.Location = new System.Drawing.Point(80, 65);
            this._Alarm.Name = "_Alarm";
            this._Alarm.Size = new System.Drawing.Size(13, 13);
            this._Alarm.State = BEMN.Forms.LedState.Off;
            this._Alarm.TabIndex = 119;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label171.Location = new System.Drawing.Point(6, 48);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(68, 13);
            this.label171.TabIndex = 116;
            this.label171.Text = "���. ��. ���.";
            // 
            // _GroupRez
            // 
            this._GroupRez.Location = new System.Drawing.Point(80, 48);
            this._GroupRez.Name = "_GroupRez";
            this._GroupRez.Size = new System.Drawing.Size(13, 13);
            this._GroupRez.State = BEMN.Forms.LedState.Off;
            this._GroupRez.TabIndex = 115;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label170.Location = new System.Drawing.Point(6, 31);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(69, 13);
            this.label170.TabIndex = 114;
            this.label170.Text = "���. ��. ���.";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label173.Location = new System.Drawing.Point(6, 14);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(65, 13);
            this.label173.TabIndex = 89;
            this.label173.Text = "������-���";
            // 
            // _GroopMain
            // 
            this._GroopMain.Location = new System.Drawing.Point(80, 31);
            this._GroopMain.Name = "_GroopMain";
            this._GroopMain.Size = new System.Drawing.Size(13, 13);
            this._GroopMain.State = BEMN.Forms.LedState.Off;
            this._GroopMain.TabIndex = 81;
            // 
            // _Disrepair
            // 
            this._Disrepair.BackColor = System.Drawing.Color.Transparent;
            this._Disrepair.Location = new System.Drawing.Point(80, 14);
            this._Disrepair.Name = "_Disrepair";
            this._Disrepair.Size = new System.Drawing.Size(13, 13);
            this._Disrepair.State = BEMN.Forms.LedState.Off;
            this._Disrepair.TabIndex = 80;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label150);
            this.groupBox23.Controls.Add(this._EXT16);
            this.groupBox23.Controls.Add(this.label155);
            this.groupBox23.Controls.Add(this._EXT15);
            this.groupBox23.Controls.Add(this.label156);
            this.groupBox23.Controls.Add(this._EXT14);
            this.groupBox23.Controls.Add(this.label157);
            this.groupBox23.Controls.Add(this._EXT13);
            this.groupBox23.Controls.Add(this.label158);
            this.groupBox23.Controls.Add(this._EXT12);
            this.groupBox23.Controls.Add(this.label159);
            this.groupBox23.Controls.Add(this._EXT11);
            this.groupBox23.Controls.Add(this.label160);
            this.groupBox23.Controls.Add(this._EXT10);
            this.groupBox23.Controls.Add(this.label161);
            this.groupBox23.Controls.Add(this._EXT9);
            this.groupBox23.Controls.Add(this.label162);
            this.groupBox23.Controls.Add(this._EXT8);
            this.groupBox23.Controls.Add(this.label163);
            this.groupBox23.Controls.Add(this._EXT7);
            this.groupBox23.Controls.Add(this.label164);
            this.groupBox23.Controls.Add(this._EXT6);
            this.groupBox23.Controls.Add(this.label165);
            this.groupBox23.Controls.Add(this._EXT5);
            this.groupBox23.Controls.Add(this.label166);
            this.groupBox23.Controls.Add(this._EXT4);
            this.groupBox23.Controls.Add(this.label167);
            this.groupBox23.Controls.Add(this._EXT3);
            this.groupBox23.Controls.Add(this.label168);
            this.groupBox23.Controls.Add(this._EXT2);
            this.groupBox23.Controls.Add(this.label169);
            this.groupBox23.Controls.Add(this._EXT1);
            this.groupBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox23.Location = new System.Drawing.Point(301, 184);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(121, 164);
            this.groupBox23.TabIndex = 58;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "����. ������";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label150.Location = new System.Drawing.Point(63, 142);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(36, 13);
            this.label150.TabIndex = 104;
            this.label150.Text = "��-16";
            // 
            // _EXT16
            // 
            this._EXT16.Location = new System.Drawing.Point(101, 142);
            this._EXT16.Name = "_EXT16";
            this._EXT16.Size = new System.Drawing.Size(13, 13);
            this._EXT16.State = BEMN.Forms.LedState.Off;
            this._EXT16.TabIndex = 103;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label155.Location = new System.Drawing.Point(63, 125);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(36, 13);
            this.label155.TabIndex = 102;
            this.label155.Text = "��-15";
            // 
            // _EXT15
            // 
            this._EXT15.Location = new System.Drawing.Point(101, 125);
            this._EXT15.Name = "_EXT15";
            this._EXT15.Size = new System.Drawing.Size(13, 13);
            this._EXT15.State = BEMN.Forms.LedState.Off;
            this._EXT15.TabIndex = 101;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label156.Location = new System.Drawing.Point(63, 108);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(36, 13);
            this.label156.TabIndex = 100;
            this.label156.Text = "��-14";
            // 
            // _EXT14
            // 
            this._EXT14.Location = new System.Drawing.Point(101, 108);
            this._EXT14.Name = "_EXT14";
            this._EXT14.Size = new System.Drawing.Size(13, 13);
            this._EXT14.State = BEMN.Forms.LedState.Off;
            this._EXT14.TabIndex = 99;
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label157.Location = new System.Drawing.Point(63, 91);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(36, 13);
            this.label157.TabIndex = 98;
            this.label157.Text = "��-13";
            // 
            // _EXT13
            // 
            this._EXT13.Location = new System.Drawing.Point(101, 91);
            this._EXT13.Name = "_EXT13";
            this._EXT13.Size = new System.Drawing.Size(13, 13);
            this._EXT13.State = BEMN.Forms.LedState.Off;
            this._EXT13.TabIndex = 97;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label158.Location = new System.Drawing.Point(63, 74);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(36, 13);
            this.label158.TabIndex = 96;
            this.label158.Text = "��-12";
            // 
            // _EXT12
            // 
            this._EXT12.Location = new System.Drawing.Point(101, 74);
            this._EXT12.Name = "_EXT12";
            this._EXT12.Size = new System.Drawing.Size(13, 13);
            this._EXT12.State = BEMN.Forms.LedState.Off;
            this._EXT12.TabIndex = 95;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label159.Location = new System.Drawing.Point(63, 57);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(36, 13);
            this.label159.TabIndex = 94;
            this.label159.Text = "��-11";
            // 
            // _EXT11
            // 
            this._EXT11.Location = new System.Drawing.Point(101, 57);
            this._EXT11.Name = "_EXT11";
            this._EXT11.Size = new System.Drawing.Size(13, 13);
            this._EXT11.State = BEMN.Forms.LedState.Off;
            this._EXT11.TabIndex = 93;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label160.Location = new System.Drawing.Point(63, 40);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(36, 13);
            this.label160.TabIndex = 92;
            this.label160.Text = "��-10";
            // 
            // _EXT10
            // 
            this._EXT10.Location = new System.Drawing.Point(101, 40);
            this._EXT10.Name = "_EXT10";
            this._EXT10.Size = new System.Drawing.Size(13, 13);
            this._EXT10.State = BEMN.Forms.LedState.Off;
            this._EXT10.TabIndex = 91;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label161.Location = new System.Drawing.Point(63, 23);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(30, 13);
            this.label161.TabIndex = 90;
            this.label161.Text = "��-9";
            // 
            // _EXT9
            // 
            this._EXT9.Location = new System.Drawing.Point(101, 23);
            this._EXT9.Name = "_EXT9";
            this._EXT9.Size = new System.Drawing.Size(13, 13);
            this._EXT9.State = BEMN.Forms.LedState.Off;
            this._EXT9.TabIndex = 89;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label162.Location = new System.Drawing.Point(6, 142);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(30, 13);
            this.label162.TabIndex = 88;
            this.label162.Text = "��-8";
            // 
            // _EXT8
            // 
            this._EXT8.Location = new System.Drawing.Point(44, 142);
            this._EXT8.Name = "_EXT8";
            this._EXT8.Size = new System.Drawing.Size(13, 13);
            this._EXT8.State = BEMN.Forms.LedState.Off;
            this._EXT8.TabIndex = 87;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label163.Location = new System.Drawing.Point(6, 125);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(30, 13);
            this.label163.TabIndex = 86;
            this.label163.Text = "��-7";
            // 
            // _EXT7
            // 
            this._EXT7.Location = new System.Drawing.Point(44, 125);
            this._EXT7.Name = "_EXT7";
            this._EXT7.Size = new System.Drawing.Size(13, 13);
            this._EXT7.State = BEMN.Forms.LedState.Off;
            this._EXT7.TabIndex = 85;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label164.Location = new System.Drawing.Point(6, 108);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(30, 13);
            this.label164.TabIndex = 84;
            this.label164.Text = "��-6";
            // 
            // _EXT6
            // 
            this._EXT6.Location = new System.Drawing.Point(44, 108);
            this._EXT6.Name = "_EXT6";
            this._EXT6.Size = new System.Drawing.Size(13, 13);
            this._EXT6.State = BEMN.Forms.LedState.Off;
            this._EXT6.TabIndex = 83;
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label165.Location = new System.Drawing.Point(6, 91);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(30, 13);
            this.label165.TabIndex = 82;
            this.label165.Text = "��-5";
            // 
            // _EXT5
            // 
            this._EXT5.Location = new System.Drawing.Point(44, 91);
            this._EXT5.Name = "_EXT5";
            this._EXT5.Size = new System.Drawing.Size(13, 13);
            this._EXT5.State = BEMN.Forms.LedState.Off;
            this._EXT5.TabIndex = 81;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label166.Location = new System.Drawing.Point(6, 74);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(30, 13);
            this.label166.TabIndex = 80;
            this.label166.Text = "��-4";
            // 
            // _EXT4
            // 
            this._EXT4.Location = new System.Drawing.Point(44, 74);
            this._EXT4.Name = "_EXT4";
            this._EXT4.Size = new System.Drawing.Size(13, 13);
            this._EXT4.State = BEMN.Forms.LedState.Off;
            this._EXT4.TabIndex = 79;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label167.Location = new System.Drawing.Point(6, 57);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(30, 13);
            this.label167.TabIndex = 78;
            this.label167.Text = "��-3";
            // 
            // _EXT3
            // 
            this._EXT3.Location = new System.Drawing.Point(44, 57);
            this._EXT3.Name = "_EXT3";
            this._EXT3.Size = new System.Drawing.Size(13, 13);
            this._EXT3.State = BEMN.Forms.LedState.Off;
            this._EXT3.TabIndex = 77;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label168.Location = new System.Drawing.Point(6, 40);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(30, 13);
            this.label168.TabIndex = 76;
            this.label168.Text = "��-2";
            // 
            // _EXT2
            // 
            this._EXT2.Location = new System.Drawing.Point(44, 40);
            this._EXT2.Name = "_EXT2";
            this._EXT2.Size = new System.Drawing.Size(13, 13);
            this._EXT2.State = BEMN.Forms.LedState.Off;
            this._EXT2.TabIndex = 75;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label169.Location = new System.Drawing.Point(6, 23);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(30, 13);
            this.label169.TabIndex = 74;
            this.label169.Text = "��-1";
            // 
            // _EXT1
            // 
            this._EXT1.Location = new System.Drawing.Point(44, 23);
            this._EXT1.Name = "_EXT1";
            this._EXT1.Size = new System.Drawing.Size(13, 13);
            this._EXT1.State = BEMN.Forms.LedState.Off;
            this._EXT1.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label144);
            this.groupBox17.Controls.Add(this._Umin4);
            this.groupBox17.Controls.Add(this._Umin4IO);
            this.groupBox17.Controls.Add(this.label145);
            this.groupBox17.Controls.Add(this.label146);
            this.groupBox17.Controls.Add(this.label147);
            this.groupBox17.Controls.Add(this.label148);
            this.groupBox17.Controls.Add(this._Umin3);
            this.groupBox17.Controls.Add(this._Umin2);
            this.groupBox17.Controls.Add(this._Umin1);
            this.groupBox17.Controls.Add(this.label149);
            this.groupBox17.Controls.Add(this._Umin3IO);
            this.groupBox17.Controls.Add(this._Umin2IO);
            this.groupBox17.Controls.Add(this._Umin1IO);
            this.groupBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox17.Location = new System.Drawing.Point(202, 260);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(93, 88);
            this.groupBox17.TabIndex = 56;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "U <";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label144.Location = new System.Drawing.Point(6, 71);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(33, 13);
            this.label144.TabIndex = 118;
            this.label144.Text = "U < 4";
            // 
            // _Umin4
            // 
            this._Umin4.Location = new System.Drawing.Point(67, 71);
            this._Umin4.Name = "_Umin4";
            this._Umin4.Size = new System.Drawing.Size(13, 13);
            this._Umin4.State = BEMN.Forms.LedState.Off;
            this._Umin4.TabIndex = 117;
            // 
            // _Umin4IO
            // 
            this._Umin4IO.Location = new System.Drawing.Point(41, 71);
            this._Umin4IO.Name = "_Umin4IO";
            this._Umin4IO.Size = new System.Drawing.Size(13, 13);
            this._Umin4IO.State = BEMN.Forms.LedState.Off;
            this._Umin4IO.TabIndex = 116;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label145.Location = new System.Drawing.Point(6, 57);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(33, 13);
            this.label145.TabIndex = 115;
            this.label145.Text = "U < 3";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label146.Location = new System.Drawing.Point(6, 43);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(33, 13);
            this.label146.TabIndex = 114;
            this.label146.Text = "U < 2";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label147.ForeColor = System.Drawing.Color.Red;
            this.label147.Location = new System.Drawing.Point(57, 12);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(38, 13);
            this.label147.TabIndex = 113;
            this.label147.Text = "����.";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label148.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label148.Location = new System.Drawing.Point(37, 12);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(23, 13);
            this.label148.TabIndex = 112;
            this.label148.Text = "��";
            // 
            // _Umin3
            // 
            this._Umin3.Location = new System.Drawing.Point(67, 57);
            this._Umin3.Name = "_Umin3";
            this._Umin3.Size = new System.Drawing.Size(13, 13);
            this._Umin3.State = BEMN.Forms.LedState.Off;
            this._Umin3.TabIndex = 98;
            // 
            // _Umin2
            // 
            this._Umin2.Location = new System.Drawing.Point(67, 43);
            this._Umin2.Name = "_Umin2";
            this._Umin2.Size = new System.Drawing.Size(13, 13);
            this._Umin2.State = BEMN.Forms.LedState.Off;
            this._Umin2.TabIndex = 97;
            // 
            // _Umin1
            // 
            this._Umin1.Location = new System.Drawing.Point(67, 29);
            this._Umin1.Name = "_Umin1";
            this._Umin1.Size = new System.Drawing.Size(13, 13);
            this._Umin1.State = BEMN.Forms.LedState.Off;
            this._Umin1.TabIndex = 96;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label149.Location = new System.Drawing.Point(6, 29);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(33, 13);
            this.label149.TabIndex = 89;
            this.label149.Text = "U < 1";
            // 
            // _Umin3IO
            // 
            this._Umin3IO.Location = new System.Drawing.Point(41, 57);
            this._Umin3IO.Name = "_Umin3IO";
            this._Umin3IO.Size = new System.Drawing.Size(13, 13);
            this._Umin3IO.State = BEMN.Forms.LedState.Off;
            this._Umin3IO.TabIndex = 82;
            // 
            // _Umin2IO
            // 
            this._Umin2IO.Location = new System.Drawing.Point(41, 43);
            this._Umin2IO.Name = "_Umin2IO";
            this._Umin2IO.Size = new System.Drawing.Size(13, 13);
            this._Umin2IO.State = BEMN.Forms.LedState.Off;
            this._Umin2IO.TabIndex = 81;
            // 
            // _Umin1IO
            // 
            this._Umin1IO.BackColor = System.Drawing.Color.Transparent;
            this._Umin1IO.Location = new System.Drawing.Point(41, 29);
            this._Umin1IO.Name = "_Umin1IO";
            this._Umin1IO.Size = new System.Drawing.Size(13, 13);
            this._Umin1IO.State = BEMN.Forms.LedState.Off;
            this._Umin1IO.TabIndex = 80;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label143);
            this.groupBox16.Controls.Add(this._Umax4);
            this.groupBox16.Controls.Add(this._Umax4IO);
            this.groupBox16.Controls.Add(this.label136);
            this.groupBox16.Controls.Add(this.label137);
            this.groupBox16.Controls.Add(this.label138);
            this.groupBox16.Controls.Add(this.label140);
            this.groupBox16.Controls.Add(this._Umax3);
            this.groupBox16.Controls.Add(this._Umax2);
            this.groupBox16.Controls.Add(this._Umax1);
            this.groupBox16.Controls.Add(this.label141);
            this.groupBox16.Controls.Add(this._Umax3IO);
            this.groupBox16.Controls.Add(this._Umax2IO);
            this.groupBox16.Controls.Add(this._Umax1IO);
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox16.Location = new System.Drawing.Point(103, 260);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(93, 88);
            this.groupBox16.TabIndex = 55;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "U >";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label143.Location = new System.Drawing.Point(6, 71);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(33, 13);
            this.label143.TabIndex = 118;
            this.label143.Text = "U > 4";
            // 
            // _Umax4
            // 
            this._Umax4.Location = new System.Drawing.Point(67, 71);
            this._Umax4.Name = "_Umax4";
            this._Umax4.Size = new System.Drawing.Size(13, 13);
            this._Umax4.State = BEMN.Forms.LedState.Off;
            this._Umax4.TabIndex = 117;
            // 
            // _Umax4IO
            // 
            this._Umax4IO.Location = new System.Drawing.Point(41, 71);
            this._Umax4IO.Name = "_Umax4IO";
            this._Umax4IO.Size = new System.Drawing.Size(13, 13);
            this._Umax4IO.State = BEMN.Forms.LedState.Off;
            this._Umax4IO.TabIndex = 116;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label136.Location = new System.Drawing.Point(6, 57);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(33, 13);
            this.label136.TabIndex = 115;
            this.label136.Text = "U > 3";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label137.Location = new System.Drawing.Point(6, 43);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(33, 13);
            this.label137.TabIndex = 114;
            this.label137.Text = "U > 2";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label138.ForeColor = System.Drawing.Color.Red;
            this.label138.Location = new System.Drawing.Point(57, 12);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(38, 13);
            this.label138.TabIndex = 113;
            this.label138.Text = "����.";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label140.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label140.Location = new System.Drawing.Point(37, 12);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(23, 13);
            this.label140.TabIndex = 112;
            this.label140.Text = "��";
            // 
            // _Umax3
            // 
            this._Umax3.Location = new System.Drawing.Point(67, 57);
            this._Umax3.Name = "_Umax3";
            this._Umax3.Size = new System.Drawing.Size(13, 13);
            this._Umax3.State = BEMN.Forms.LedState.Off;
            this._Umax3.TabIndex = 98;
            // 
            // _Umax2
            // 
            this._Umax2.Location = new System.Drawing.Point(67, 43);
            this._Umax2.Name = "_Umax2";
            this._Umax2.Size = new System.Drawing.Size(13, 13);
            this._Umax2.State = BEMN.Forms.LedState.Off;
            this._Umax2.TabIndex = 97;
            // 
            // _Umax1
            // 
            this._Umax1.Location = new System.Drawing.Point(67, 29);
            this._Umax1.Name = "_Umax1";
            this._Umax1.Size = new System.Drawing.Size(13, 13);
            this._Umax1.State = BEMN.Forms.LedState.Off;
            this._Umax1.TabIndex = 96;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label141.Location = new System.Drawing.Point(6, 29);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(33, 13);
            this.label141.TabIndex = 89;
            this.label141.Text = "U > 1";
            // 
            // _Umax3IO
            // 
            this._Umax3IO.Location = new System.Drawing.Point(41, 57);
            this._Umax3IO.Name = "_Umax3IO";
            this._Umax3IO.Size = new System.Drawing.Size(13, 13);
            this._Umax3IO.State = BEMN.Forms.LedState.Off;
            this._Umax3IO.TabIndex = 82;
            // 
            // _Umax2IO
            // 
            this._Umax2IO.Location = new System.Drawing.Point(41, 43);
            this._Umax2IO.Name = "_Umax2IO";
            this._Umax2IO.Size = new System.Drawing.Size(13, 13);
            this._Umax2IO.State = BEMN.Forms.LedState.Off;
            this._Umax2IO.TabIndex = 81;
            // 
            // _Umax1IO
            // 
            this._Umax1IO.BackColor = System.Drawing.Color.Transparent;
            this._Umax1IO.Location = new System.Drawing.Point(41, 29);
            this._Umax1IO.Name = "_Umax1IO";
            this._Umax1IO.Size = new System.Drawing.Size(13, 13);
            this._Umax1IO.State = BEMN.Forms.LedState.Off;
            this._Umax1IO.TabIndex = 80;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label124);
            this.groupBox14.Controls.Add(this.label125);
            this.groupBox14.Controls.Add(this.label126);
            this.groupBox14.Controls.Add(this.label127);
            this.groupBox14.Controls.Add(this._In3);
            this.groupBox14.Controls.Add(this._In2);
            this.groupBox14.Controls.Add(this._In1);
            this.groupBox14.Controls.Add(this.label128);
            this.groupBox14.Controls.Add(this._In3IO);
            this.groupBox14.Controls.Add(this._In2IO);
            this.groupBox14.Controls.Add(this._In1IO);
            this.groupBox14.Controls.Add(this.label129);
            this.groupBox14.Controls.Add(this.label130);
            this.groupBox14.Controls.Add(this.label131);
            this.groupBox14.Controls.Add(this.label134);
            this.groupBox14.Controls.Add(this._I03);
            this.groupBox14.Controls.Add(this._I02);
            this.groupBox14.Controls.Add(this._I01);
            this.groupBox14.Controls.Add(this.label135);
            this.groupBox14.Controls.Add(this._I03IO);
            this.groupBox14.Controls.Add(this._I02IO);
            this.groupBox14.Controls.Add(this._I01IO);
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox14.Location = new System.Drawing.Point(103, 184);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(194, 76);
            this.groupBox14.TabIndex = 53;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "I* >";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label124.Location = new System.Drawing.Point(97, 56);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(32, 13);
            this.label124.TabIndex = 126;
            this.label124.Text = "I* > 6";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label125.Location = new System.Drawing.Point(97, 42);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(32, 13);
            this.label125.TabIndex = 125;
            this.label125.Text = "I* > 5";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label126.ForeColor = System.Drawing.Color.Red;
            this.label126.Location = new System.Drawing.Point(148, 11);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(38, 13);
            this.label126.TabIndex = 124;
            this.label126.Text = "����.";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label127.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label127.Location = new System.Drawing.Point(128, 11);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(23, 13);
            this.label127.TabIndex = 123;
            this.label127.Text = "��";
            // 
            // _In3
            // 
            this._In3.Location = new System.Drawing.Point(158, 56);
            this._In3.Name = "_In3";
            this._In3.Size = new System.Drawing.Size(13, 13);
            this._In3.State = BEMN.Forms.LedState.Off;
            this._In3.TabIndex = 122;
            // 
            // _In2
            // 
            this._In2.Location = new System.Drawing.Point(158, 42);
            this._In2.Name = "_In2";
            this._In2.Size = new System.Drawing.Size(13, 13);
            this._In2.State = BEMN.Forms.LedState.Off;
            this._In2.TabIndex = 121;
            // 
            // _In1
            // 
            this._In1.Location = new System.Drawing.Point(158, 28);
            this._In1.Name = "_In1";
            this._In1.Size = new System.Drawing.Size(13, 13);
            this._In1.State = BEMN.Forms.LedState.Off;
            this._In1.TabIndex = 120;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label128.Location = new System.Drawing.Point(97, 28);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(32, 13);
            this.label128.TabIndex = 119;
            this.label128.Text = "I* > 4";
            // 
            // _In3IO
            // 
            this._In3IO.Location = new System.Drawing.Point(132, 56);
            this._In3IO.Name = "_In3IO";
            this._In3IO.Size = new System.Drawing.Size(13, 13);
            this._In3IO.State = BEMN.Forms.LedState.Off;
            this._In3IO.TabIndex = 118;
            // 
            // _In2IO
            // 
            this._In2IO.Location = new System.Drawing.Point(132, 42);
            this._In2IO.Name = "_In2IO";
            this._In2IO.Size = new System.Drawing.Size(13, 13);
            this._In2IO.State = BEMN.Forms.LedState.Off;
            this._In2IO.TabIndex = 117;
            // 
            // _In1IO
            // 
            this._In1IO.BackColor = System.Drawing.Color.Transparent;
            this._In1IO.Location = new System.Drawing.Point(132, 28);
            this._In1IO.Name = "_In1IO";
            this._In1IO.Size = new System.Drawing.Size(13, 13);
            this._In1IO.State = BEMN.Forms.LedState.Off;
            this._In1IO.TabIndex = 116;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label129.Location = new System.Drawing.Point(18, 56);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(32, 13);
            this.label129.TabIndex = 115;
            this.label129.Text = "I* > 3";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label130.Location = new System.Drawing.Point(18, 42);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(32, 13);
            this.label130.TabIndex = 114;
            this.label130.Text = "I* > 2";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label131.ForeColor = System.Drawing.Color.Red;
            this.label131.Location = new System.Drawing.Point(68, 11);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(38, 13);
            this.label131.TabIndex = 113;
            this.label131.Text = "����.";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label134.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label134.Location = new System.Drawing.Point(48, 11);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(23, 13);
            this.label134.TabIndex = 112;
            this.label134.Text = "��";
            // 
            // _I03
            // 
            this._I03.Location = new System.Drawing.Point(79, 56);
            this._I03.Name = "_I03";
            this._I03.Size = new System.Drawing.Size(13, 13);
            this._I03.State = BEMN.Forms.LedState.Off;
            this._I03.TabIndex = 98;
            // 
            // _I02
            // 
            this._I02.Location = new System.Drawing.Point(79, 42);
            this._I02.Name = "_I02";
            this._I02.Size = new System.Drawing.Size(13, 13);
            this._I02.State = BEMN.Forms.LedState.Off;
            this._I02.TabIndex = 97;
            // 
            // _I01
            // 
            this._I01.Location = new System.Drawing.Point(79, 28);
            this._I01.Name = "_I01";
            this._I01.Size = new System.Drawing.Size(13, 13);
            this._I01.State = BEMN.Forms.LedState.Off;
            this._I01.TabIndex = 96;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label135.Location = new System.Drawing.Point(18, 28);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(32, 13);
            this.label135.TabIndex = 89;
            this.label135.Text = "I* > 1";
            // 
            // _I03IO
            // 
            this._I03IO.Location = new System.Drawing.Point(53, 56);
            this._I03IO.Name = "_I03IO";
            this._I03IO.Size = new System.Drawing.Size(13, 13);
            this._I03IO.State = BEMN.Forms.LedState.Off;
            this._I03IO.TabIndex = 82;
            // 
            // _I02IO
            // 
            this._I02IO.Location = new System.Drawing.Point(53, 42);
            this._I02IO.Name = "_I02IO";
            this._I02IO.Size = new System.Drawing.Size(13, 13);
            this._I02IO.State = BEMN.Forms.LedState.Off;
            this._I02IO.TabIndex = 81;
            // 
            // _I01IO
            // 
            this._I01IO.BackColor = System.Drawing.Color.Transparent;
            this._I01IO.Location = new System.Drawing.Point(53, 28);
            this._I01IO.Name = "_I01IO";
            this._I01IO.Size = new System.Drawing.Size(13, 13);
            this._I01IO.State = BEMN.Forms.LedState.Off;
            this._I01IO.TabIndex = 80;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label123);
            this.groupBox13.Controls.Add(this.label121);
            this.groupBox13.Controls.Add(this.label120);
            this.groupBox13.Controls.Add(this.label113);
            this.groupBox13.Controls.Add(this.label112);
            this.groupBox13.Controls.Add(this.label111);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.label133);
            this.groupBox13.Controls.Add(this.label132);
            this.groupBox13.Controls.Add(this._I8);
            this.groupBox13.Controls.Add(this._I7);
            this.groupBox13.Controls.Add(this._I6);
            this.groupBox13.Controls.Add(this._I5);
            this.groupBox13.Controls.Add(this._I4);
            this.groupBox13.Controls.Add(this._I3);
            this.groupBox13.Controls.Add(this._I2);
            this.groupBox13.Controls.Add(this._I1);
            this.groupBox13.Controls.Add(this.label122);
            this.groupBox13.Controls.Add(this._I8IO);
            this.groupBox13.Controls.Add(this._I7IO);
            this.groupBox13.Controls.Add(this._I6IO);
            this.groupBox13.Controls.Add(this._I5IO);
            this.groupBox13.Controls.Add(this._I4IO);
            this.groupBox13.Controls.Add(this._I3IO);
            this.groupBox13.Controls.Add(this._I2IO);
            this.groupBox13.Controls.Add(this._I1IO);
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox13.Location = new System.Drawing.Point(6, 184);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(93, 164);
            this.groupBox13.TabIndex = 52;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "I >";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label123.Location = new System.Drawing.Point(6, 145);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(28, 13);
            this.label123.TabIndex = 120;
            this.label123.Text = "I > 8";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label121.Location = new System.Drawing.Point(6, 129);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(28, 13);
            this.label121.TabIndex = 119;
            this.label121.Text = "I > 7";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label120.Location = new System.Drawing.Point(6, 113);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(28, 13);
            this.label120.TabIndex = 118;
            this.label120.Text = "I > 6";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label113.Location = new System.Drawing.Point(6, 97);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(28, 13);
            this.label113.TabIndex = 117;
            this.label113.Text = "I > 5";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label112.Location = new System.Drawing.Point(6, 81);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(28, 13);
            this.label112.TabIndex = 116;
            this.label112.Text = "I > 4";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label111.Location = new System.Drawing.Point(6, 64);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(28, 13);
            this.label111.TabIndex = 115;
            this.label111.Text = "I > 3";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label110.Location = new System.Drawing.Point(6, 47);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(28, 13);
            this.label110.TabIndex = 114;
            this.label110.Text = "I > 2";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label133.ForeColor = System.Drawing.Color.Red;
            this.label133.Location = new System.Drawing.Point(53, 12);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(38, 13);
            this.label133.TabIndex = 113;
            this.label133.Text = "����.";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label132.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label132.Location = new System.Drawing.Point(33, 12);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(23, 13);
            this.label132.TabIndex = 112;
            this.label132.Text = "��";
            // 
            // _I8
            // 
            this._I8.Location = new System.Drawing.Point(63, 145);
            this._I8.Name = "_I8";
            this._I8.Size = new System.Drawing.Size(13, 13);
            this._I8.State = BEMN.Forms.LedState.Off;
            this._I8.TabIndex = 103;
            // 
            // _I7
            // 
            this._I7.Location = new System.Drawing.Point(63, 129);
            this._I7.Name = "_I7";
            this._I7.Size = new System.Drawing.Size(13, 13);
            this._I7.State = BEMN.Forms.LedState.Off;
            this._I7.TabIndex = 102;
            // 
            // _I6
            // 
            this._I6.Location = new System.Drawing.Point(63, 113);
            this._I6.Name = "_I6";
            this._I6.Size = new System.Drawing.Size(13, 13);
            this._I6.State = BEMN.Forms.LedState.Off;
            this._I6.TabIndex = 101;
            // 
            // _I5
            // 
            this._I5.Location = new System.Drawing.Point(63, 97);
            this._I5.Name = "_I5";
            this._I5.Size = new System.Drawing.Size(13, 13);
            this._I5.State = BEMN.Forms.LedState.Off;
            this._I5.TabIndex = 100;
            // 
            // _I4
            // 
            this._I4.Location = new System.Drawing.Point(63, 81);
            this._I4.Name = "_I4";
            this._I4.Size = new System.Drawing.Size(13, 13);
            this._I4.State = BEMN.Forms.LedState.Off;
            this._I4.TabIndex = 99;
            // 
            // _I3
            // 
            this._I3.Location = new System.Drawing.Point(63, 64);
            this._I3.Name = "_I3";
            this._I3.Size = new System.Drawing.Size(13, 13);
            this._I3.State = BEMN.Forms.LedState.Off;
            this._I3.TabIndex = 98;
            // 
            // _I2
            // 
            this._I2.Location = new System.Drawing.Point(63, 47);
            this._I2.Name = "_I2";
            this._I2.Size = new System.Drawing.Size(13, 13);
            this._I2.State = BEMN.Forms.LedState.Off;
            this._I2.TabIndex = 97;
            // 
            // _I1
            // 
            this._I1.Location = new System.Drawing.Point(63, 31);
            this._I1.Name = "_I1";
            this._I1.Size = new System.Drawing.Size(13, 13);
            this._I1.State = BEMN.Forms.LedState.Off;
            this._I1.TabIndex = 96;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label122.Location = new System.Drawing.Point(6, 31);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(28, 13);
            this.label122.TabIndex = 89;
            this.label122.Text = "I > 1";
            // 
            // _I8IO
            // 
            this._I8IO.Location = new System.Drawing.Point(37, 145);
            this._I8IO.Name = "_I8IO";
            this._I8IO.Size = new System.Drawing.Size(13, 13);
            this._I8IO.State = BEMN.Forms.LedState.Off;
            this._I8IO.TabIndex = 87;
            // 
            // _I7IO
            // 
            this._I7IO.Location = new System.Drawing.Point(37, 129);
            this._I7IO.Name = "_I7IO";
            this._I7IO.Size = new System.Drawing.Size(13, 13);
            this._I7IO.State = BEMN.Forms.LedState.Off;
            this._I7IO.TabIndex = 86;
            // 
            // _I6IO
            // 
            this._I6IO.Location = new System.Drawing.Point(37, 113);
            this._I6IO.Name = "_I6IO";
            this._I6IO.Size = new System.Drawing.Size(13, 13);
            this._I6IO.State = BEMN.Forms.LedState.Off;
            this._I6IO.TabIndex = 85;
            // 
            // _I5IO
            // 
            this._I5IO.Location = new System.Drawing.Point(37, 97);
            this._I5IO.Name = "_I5IO";
            this._I5IO.Size = new System.Drawing.Size(13, 13);
            this._I5IO.State = BEMN.Forms.LedState.Off;
            this._I5IO.TabIndex = 84;
            // 
            // _I4IO
            // 
            this._I4IO.Location = new System.Drawing.Point(37, 81);
            this._I4IO.Name = "_I4IO";
            this._I4IO.Size = new System.Drawing.Size(13, 13);
            this._I4IO.State = BEMN.Forms.LedState.Off;
            this._I4IO.TabIndex = 83;
            // 
            // _I3IO
            // 
            this._I3IO.Location = new System.Drawing.Point(37, 64);
            this._I3IO.Name = "_I3IO";
            this._I3IO.Size = new System.Drawing.Size(13, 13);
            this._I3IO.State = BEMN.Forms.LedState.Off;
            this._I3IO.TabIndex = 82;
            // 
            // _I2IO
            // 
            this._I2IO.Location = new System.Drawing.Point(37, 47);
            this._I2IO.Name = "_I2IO";
            this._I2IO.Size = new System.Drawing.Size(13, 13);
            this._I2IO.State = BEMN.Forms.LedState.Off;
            this._I2IO.TabIndex = 81;
            // 
            // _I1IO
            // 
            this._I1IO.BackColor = System.Drawing.Color.Transparent;
            this._I1IO.Location = new System.Drawing.Point(37, 31);
            this._I1IO.Name = "_I1IO";
            this._I1IO.Size = new System.Drawing.Size(13, 13);
            this._I1IO.State = BEMN.Forms.LedState.Off;
            this._I1IO.TabIndex = 80;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._Id0Max3);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._Id0Max3IO);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._Id0Max2);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._Id0Max2IO);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._Id0Max1);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._Id0Max1IO);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._IdMax1);
            this.groupBox12.Controls.Add(this._IdMax1IO);
            this.groupBox12.Controls.Add(this._IdMax2);
            this.groupBox12.Controls.Add(this._IdMax2IO);
            this.groupBox12.Controls.Add(this._IdMax2Mgn);
            this.groupBox12.Controls.Add(this._IdMax1L);
            this.groupBox12.Controls.Add(this._IdMax1IOL);
            this.groupBox12.Controls.Add(this._IdMax2L);
            this.groupBox12.Controls.Add(this._IdMax2IOL);
            this.groupBox12.Controls.Add(this.label109);
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox12.Location = new System.Drawing.Point(431, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(100, 175);
            this.groupBox12.TabIndex = 51;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "I �";
            // 
            // _Id0Max3
            // 
            this._Id0Max3.Location = new System.Drawing.Point(80, 157);
            this._Id0Max3.Name = "_Id0Max3";
            this._Id0Max3.Size = new System.Drawing.Size(13, 13);
            this._Id0Max3.State = BEMN.Forms.LedState.Off;
            this._Id0Max3.TabIndex = 74;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label118.Location = new System.Drawing.Point(6, 157);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(43, 13);
            this.label118.TabIndex = 73;
            this.label118.Text = "I�0 >>>";
            // 
            // _Id0Max3IO
            // 
            this._Id0Max3IO.Location = new System.Drawing.Point(80, 143);
            this._Id0Max3IO.Name = "_Id0Max3IO";
            this._Id0Max3IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max3IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max3IO.TabIndex = 72;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label119.Location = new System.Drawing.Point(6, 143);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(68, 13);
            this.label119.TabIndex = 71;
            this.label119.Text = "I�0 >>>   ��";
            // 
            // _Id0Max2
            // 
            this._Id0Max2.Location = new System.Drawing.Point(80, 129);
            this._Id0Max2.Name = "_Id0Max2";
            this._Id0Max2.Size = new System.Drawing.Size(13, 13);
            this._Id0Max2.State = BEMN.Forms.LedState.Off;
            this._Id0Max2.TabIndex = 70;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label116.Location = new System.Drawing.Point(6, 129);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(37, 13);
            this.label116.TabIndex = 69;
            this.label116.Text = "I�0 >>";
            // 
            // _Id0Max2IO
            // 
            this._Id0Max2IO.Location = new System.Drawing.Point(80, 115);
            this._Id0Max2IO.Name = "_Id0Max2IO";
            this._Id0Max2IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max2IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max2IO.TabIndex = 68;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label117.Location = new System.Drawing.Point(6, 115);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(68, 13);
            this.label117.TabIndex = 67;
            this.label117.Text = "I�0 >>     ��";
            // 
            // _Id0Max1
            // 
            this._Id0Max1.Location = new System.Drawing.Point(80, 101);
            this._Id0Max1.Name = "_Id0Max1";
            this._Id0Max1.Size = new System.Drawing.Size(13, 13);
            this._Id0Max1.State = BEMN.Forms.LedState.Off;
            this._Id0Max1.TabIndex = 66;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label115.Location = new System.Drawing.Point(6, 101);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(31, 13);
            this.label115.TabIndex = 65;
            this.label115.Text = "I�0 >";
            // 
            // _Id0Max1IO
            // 
            this._Id0Max1IO.Location = new System.Drawing.Point(80, 87);
            this._Id0Max1IO.Name = "_Id0Max1IO";
            this._Id0Max1IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max1IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max1IO.TabIndex = 64;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label114.Location = new System.Drawing.Point(6, 87);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(68, 13);
            this.label114.TabIndex = 63;
            this.label114.Text = "I�0 >       ��";
            // 
            // _IdMax1
            // 
            this._IdMax1.Location = new System.Drawing.Point(80, 73);
            this._IdMax1.Name = "_IdMax1";
            this._IdMax1.Size = new System.Drawing.Size(13, 13);
            this._IdMax1.State = BEMN.Forms.LedState.Off;
            this._IdMax1.TabIndex = 62;
            // 
            // _IdMax1IO
            // 
            this._IdMax1IO.Location = new System.Drawing.Point(80, 59);
            this._IdMax1IO.Name = "_IdMax1IO";
            this._IdMax1IO.Size = new System.Drawing.Size(13, 13);
            this._IdMax1IO.State = BEMN.Forms.LedState.Off;
            this._IdMax1IO.TabIndex = 61;
            // 
            // _IdMax2
            // 
            this._IdMax2.Location = new System.Drawing.Point(80, 45);
            this._IdMax2.Name = "_IdMax2";
            this._IdMax2.Size = new System.Drawing.Size(13, 13);
            this._IdMax2.State = BEMN.Forms.LedState.Off;
            this._IdMax2.TabIndex = 60;
            // 
            // _IdMax2IO
            // 
            this._IdMax2IO.Location = new System.Drawing.Point(80, 31);
            this._IdMax2IO.Name = "_IdMax2IO";
            this._IdMax2IO.Size = new System.Drawing.Size(13, 13);
            this._IdMax2IO.State = BEMN.Forms.LedState.Off;
            this._IdMax2IO.TabIndex = 59;
            // 
            // _IdMax2Mgn
            // 
            this._IdMax2Mgn.Location = new System.Drawing.Point(80, 17);
            this._IdMax2Mgn.Name = "_IdMax2Mgn";
            this._IdMax2Mgn.Size = new System.Drawing.Size(13, 13);
            this._IdMax2Mgn.State = BEMN.Forms.LedState.Off;
            this._IdMax2Mgn.TabIndex = 58;
            // 
            // _IdMax1L
            // 
            this._IdMax1L.AutoSize = true;
            this._IdMax1L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax1L.Location = new System.Drawing.Point(6, 73);
            this._IdMax1L.Name = "_IdMax1L";
            this._IdMax1L.Size = new System.Drawing.Size(25, 13);
            this._IdMax1L.TabIndex = 57;
            this._IdMax1L.Text = "I� >";
            // 
            // _IdMax1IOL
            // 
            this._IdMax1IOL.AutoSize = true;
            this._IdMax1IOL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax1IOL.Location = new System.Drawing.Point(6, 59);
            this._IdMax1IOL.Name = "_IdMax1IOL";
            this._IdMax1IOL.Size = new System.Drawing.Size(68, 13);
            this._IdMax1IOL.TabIndex = 56;
            this._IdMax1IOL.Text = "I� >         ��";
            // 
            // _IdMax2L
            // 
            this._IdMax2L.AutoSize = true;
            this._IdMax2L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax2L.Location = new System.Drawing.Point(6, 45);
            this._IdMax2L.Name = "_IdMax2L";
            this._IdMax2L.Size = new System.Drawing.Size(31, 13);
            this._IdMax2L.TabIndex = 55;
            this._IdMax2L.Text = "I� >>";
            // 
            // _IdMax2IOL
            // 
            this._IdMax2IOL.AutoSize = true;
            this._IdMax2IOL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax2IOL.Location = new System.Drawing.Point(6, 31);
            this._IdMax2IOL.Name = "_IdMax2IOL";
            this._IdMax2IOL.Size = new System.Drawing.Size(68, 13);
            this._IdMax2IOL.TabIndex = 54;
            this._IdMax2IOL.Text = "I� >>       ��";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label109.Location = new System.Drawing.Point(6, 17);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(74, 13);
            this.label109.TabIndex = 53;
            this.label109.Text = "I� >>       ���.";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label108);
            this.groupBox11.Controls.Add(this._VLS16);
            this.groupBox11.Controls.Add(this.label107);
            this.groupBox11.Controls.Add(this._VLS15);
            this.groupBox11.Controls.Add(this.label106);
            this.groupBox11.Controls.Add(this._VLS14);
            this.groupBox11.Controls.Add(this.label105);
            this.groupBox11.Controls.Add(this._VLS13);
            this.groupBox11.Controls.Add(this.label104);
            this.groupBox11.Controls.Add(this._VLS12);
            this.groupBox11.Controls.Add(this.label103);
            this.groupBox11.Controls.Add(this._VLS11);
            this.groupBox11.Controls.Add(this.label102);
            this.groupBox11.Controls.Add(this._VLS10);
            this.groupBox11.Controls.Add(this.label101);
            this.groupBox11.Controls.Add(this._VLS9);
            this.groupBox11.Controls.Add(this.label100);
            this.groupBox11.Controls.Add(this._VLS8);
            this.groupBox11.Controls.Add(this.label99);
            this.groupBox11.Controls.Add(this._VLS7);
            this.groupBox11.Controls.Add(this.label98);
            this.groupBox11.Controls.Add(this._VLS6);
            this.groupBox11.Controls.Add(this.label97);
            this.groupBox11.Controls.Add(this._VLS5);
            this.groupBox11.Controls.Add(this.label96);
            this.groupBox11.Controls.Add(this._VLS4);
            this.groupBox11.Controls.Add(this.label95);
            this.groupBox11.Controls.Add(this._VLS3);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Controls.Add(this._VLS2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._VLS1);
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox11.Location = new System.Drawing.Point(295, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(131, 175);
            this.groupBox11.TabIndex = 50;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "�������� ��";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label108.Location = new System.Drawing.Point(67, 156);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(41, 13);
            this.label108.TabIndex = 106;
            this.label108.Text = "���16";
            // 
            // _VLS16
            // 
            this._VLS16.Location = new System.Drawing.Point(109, 156);
            this._VLS16.Name = "_VLS16";
            this._VLS16.Size = new System.Drawing.Size(13, 13);
            this._VLS16.State = BEMN.Forms.LedState.Off;
            this._VLS16.TabIndex = 105;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label107.Location = new System.Drawing.Point(67, 137);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(41, 13);
            this.label107.TabIndex = 104;
            this.label107.Text = "���15";
            // 
            // _VLS15
            // 
            this._VLS15.Location = new System.Drawing.Point(109, 137);
            this._VLS15.Name = "_VLS15";
            this._VLS15.Size = new System.Drawing.Size(13, 13);
            this._VLS15.State = BEMN.Forms.LedState.Off;
            this._VLS15.TabIndex = 103;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label106.Location = new System.Drawing.Point(67, 118);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(41, 13);
            this.label106.TabIndex = 102;
            this.label106.Text = "���14";
            // 
            // _VLS14
            // 
            this._VLS14.Location = new System.Drawing.Point(109, 118);
            this._VLS14.Name = "_VLS14";
            this._VLS14.Size = new System.Drawing.Size(13, 13);
            this._VLS14.State = BEMN.Forms.LedState.Off;
            this._VLS14.TabIndex = 101;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label105.Location = new System.Drawing.Point(67, 99);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(41, 13);
            this.label105.TabIndex = 100;
            this.label105.Text = "���13";
            // 
            // _VLS13
            // 
            this._VLS13.Location = new System.Drawing.Point(109, 99);
            this._VLS13.Name = "_VLS13";
            this._VLS13.Size = new System.Drawing.Size(13, 13);
            this._VLS13.State = BEMN.Forms.LedState.Off;
            this._VLS13.TabIndex = 99;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label104.Location = new System.Drawing.Point(67, 80);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(41, 13);
            this.label104.TabIndex = 98;
            this.label104.Text = "���12";
            // 
            // _VLS12
            // 
            this._VLS12.Location = new System.Drawing.Point(109, 80);
            this._VLS12.Name = "_VLS12";
            this._VLS12.Size = new System.Drawing.Size(13, 13);
            this._VLS12.State = BEMN.Forms.LedState.Off;
            this._VLS12.TabIndex = 97;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label103.Location = new System.Drawing.Point(67, 61);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(41, 13);
            this.label103.TabIndex = 96;
            this.label103.Text = "���11";
            // 
            // _VLS11
            // 
            this._VLS11.Location = new System.Drawing.Point(109, 61);
            this._VLS11.Name = "_VLS11";
            this._VLS11.Size = new System.Drawing.Size(13, 13);
            this._VLS11.State = BEMN.Forms.LedState.Off;
            this._VLS11.TabIndex = 95;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label102.Location = new System.Drawing.Point(67, 42);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(41, 13);
            this.label102.TabIndex = 94;
            this.label102.Text = "���10";
            // 
            // _VLS10
            // 
            this._VLS10.Location = new System.Drawing.Point(109, 42);
            this._VLS10.Name = "_VLS10";
            this._VLS10.Size = new System.Drawing.Size(13, 13);
            this._VLS10.State = BEMN.Forms.LedState.Off;
            this._VLS10.TabIndex = 93;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label101.Location = new System.Drawing.Point(67, 23);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(35, 13);
            this.label101.TabIndex = 92;
            this.label101.Text = "���9";
            // 
            // _VLS9
            // 
            this._VLS9.Location = new System.Drawing.Point(109, 23);
            this._VLS9.Name = "_VLS9";
            this._VLS9.Size = new System.Drawing.Size(13, 13);
            this._VLS9.State = BEMN.Forms.LedState.Off;
            this._VLS9.TabIndex = 91;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label100.Location = new System.Drawing.Point(6, 156);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(35, 13);
            this.label100.TabIndex = 90;
            this.label100.Text = "���8";
            // 
            // _VLS8
            // 
            this._VLS8.Location = new System.Drawing.Point(42, 156);
            this._VLS8.Name = "_VLS8";
            this._VLS8.Size = new System.Drawing.Size(13, 13);
            this._VLS8.State = BEMN.Forms.LedState.Off;
            this._VLS8.TabIndex = 89;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label99.Location = new System.Drawing.Point(6, 137);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(35, 13);
            this.label99.TabIndex = 88;
            this.label99.Text = "���7";
            // 
            // _VLS7
            // 
            this._VLS7.Location = new System.Drawing.Point(42, 137);
            this._VLS7.Name = "_VLS7";
            this._VLS7.Size = new System.Drawing.Size(13, 13);
            this._VLS7.State = BEMN.Forms.LedState.Off;
            this._VLS7.TabIndex = 87;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label98.Location = new System.Drawing.Point(6, 118);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(35, 13);
            this.label98.TabIndex = 86;
            this.label98.Text = "���6";
            // 
            // _VLS6
            // 
            this._VLS6.Location = new System.Drawing.Point(42, 118);
            this._VLS6.Name = "_VLS6";
            this._VLS6.Size = new System.Drawing.Size(13, 13);
            this._VLS6.State = BEMN.Forms.LedState.Off;
            this._VLS6.TabIndex = 85;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label97.Location = new System.Drawing.Point(6, 99);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(35, 13);
            this.label97.TabIndex = 84;
            this.label97.Text = "���5";
            // 
            // _VLS5
            // 
            this._VLS5.Location = new System.Drawing.Point(42, 99);
            this._VLS5.Name = "_VLS5";
            this._VLS5.Size = new System.Drawing.Size(13, 13);
            this._VLS5.State = BEMN.Forms.LedState.Off;
            this._VLS5.TabIndex = 83;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label96.Location = new System.Drawing.Point(6, 80);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(35, 13);
            this.label96.TabIndex = 82;
            this.label96.Text = "���4";
            // 
            // _VLS4
            // 
            this._VLS4.Location = new System.Drawing.Point(42, 80);
            this._VLS4.Name = "_VLS4";
            this._VLS4.Size = new System.Drawing.Size(13, 13);
            this._VLS4.State = BEMN.Forms.LedState.Off;
            this._VLS4.TabIndex = 81;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label95.Location = new System.Drawing.Point(6, 61);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 13);
            this.label95.TabIndex = 80;
            this.label95.Text = "���3";
            // 
            // _VLS3
            // 
            this._VLS3.Location = new System.Drawing.Point(42, 61);
            this._VLS3.Name = "_VLS3";
            this._VLS3.Size = new System.Drawing.Size(13, 13);
            this._VLS3.State = BEMN.Forms.LedState.Off;
            this._VLS3.TabIndex = 79;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label94.Location = new System.Drawing.Point(6, 42);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(35, 13);
            this.label94.TabIndex = 78;
            this.label94.Text = "���2";
            // 
            // _VLS2
            // 
            this._VLS2.Location = new System.Drawing.Point(42, 42);
            this._VLS2.Name = "_VLS2";
            this._VLS2.Size = new System.Drawing.Size(13, 13);
            this._VLS2.State = BEMN.Forms.LedState.Off;
            this._VLS2.TabIndex = 77;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label93.Location = new System.Drawing.Point(6, 23);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(35, 13);
            this.label93.TabIndex = 76;
            this.label93.Text = "���1";
            // 
            // _VLS1
            // 
            this._VLS1.Location = new System.Drawing.Point(42, 23);
            this._VLS1.Name = "_VLS1";
            this._VLS1.Size = new System.Drawing.Size(13, 13);
            this._VLS1.State = BEMN.Forms.LedState.Off;
            this._VLS1.TabIndex = 75;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label92);
            this.groupBox10.Controls.Add(this._LS16);
            this.groupBox10.Controls.Add(this.label91);
            this.groupBox10.Controls.Add(this._LS15);
            this.groupBox10.Controls.Add(this.label90);
            this.groupBox10.Controls.Add(this._LS14);
            this.groupBox10.Controls.Add(this.label89);
            this.groupBox10.Controls.Add(this._LS13);
            this.groupBox10.Controls.Add(this.label88);
            this.groupBox10.Controls.Add(this._LS12);
            this.groupBox10.Controls.Add(this.label87);
            this.groupBox10.Controls.Add(this._LS11);
            this.groupBox10.Controls.Add(this.label86);
            this.groupBox10.Controls.Add(this._LS10);
            this.groupBox10.Controls.Add(this.label85);
            this.groupBox10.Controls.Add(this._LS9);
            this.groupBox10.Controls.Add(this.label84);
            this.groupBox10.Controls.Add(this._LS8);
            this.groupBox10.Controls.Add(this.label83);
            this.groupBox10.Controls.Add(this._LS7);
            this.groupBox10.Controls.Add(this.label82);
            this.groupBox10.Controls.Add(this._LS6);
            this.groupBox10.Controls.Add(this.label81);
            this.groupBox10.Controls.Add(this._LS5);
            this.groupBox10.Controls.Add(this.label80);
            this.groupBox10.Controls.Add(this._LS4);
            this.groupBox10.Controls.Add(this.label79);
            this.groupBox10.Controls.Add(this._LS3);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Controls.Add(this._LS2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._LS1);
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox10.Location = new System.Drawing.Point(168, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(121, 175);
            this.groupBox10.TabIndex = 49;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "������� ��";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label92.Location = new System.Drawing.Point(63, 156);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(34, 13);
            this.label92.TabIndex = 104;
            this.label92.Text = "��16";
            // 
            // _LS16
            // 
            this._LS16.Location = new System.Drawing.Point(99, 156);
            this._LS16.Name = "_LS16";
            this._LS16.Size = new System.Drawing.Size(13, 13);
            this._LS16.State = BEMN.Forms.LedState.Off;
            this._LS16.TabIndex = 103;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label91.Location = new System.Drawing.Point(63, 137);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(34, 13);
            this.label91.TabIndex = 102;
            this.label91.Text = "��15";
            // 
            // _LS15
            // 
            this._LS15.Location = new System.Drawing.Point(99, 137);
            this._LS15.Name = "_LS15";
            this._LS15.Size = new System.Drawing.Size(13, 13);
            this._LS15.State = BEMN.Forms.LedState.Off;
            this._LS15.TabIndex = 101;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label90.Location = new System.Drawing.Point(63, 118);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(34, 13);
            this.label90.TabIndex = 100;
            this.label90.Text = "��14";
            // 
            // _LS14
            // 
            this._LS14.Location = new System.Drawing.Point(99, 118);
            this._LS14.Name = "_LS14";
            this._LS14.Size = new System.Drawing.Size(13, 13);
            this._LS14.State = BEMN.Forms.LedState.Off;
            this._LS14.TabIndex = 99;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label89.Location = new System.Drawing.Point(63, 99);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(34, 13);
            this.label89.TabIndex = 98;
            this.label89.Text = "��13";
            // 
            // _LS13
            // 
            this._LS13.Location = new System.Drawing.Point(99, 99);
            this._LS13.Name = "_LS13";
            this._LS13.Size = new System.Drawing.Size(13, 13);
            this._LS13.State = BEMN.Forms.LedState.Off;
            this._LS13.TabIndex = 97;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label88.Location = new System.Drawing.Point(63, 80);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(34, 13);
            this.label88.TabIndex = 96;
            this.label88.Text = "��12";
            // 
            // _LS12
            // 
            this._LS12.Location = new System.Drawing.Point(99, 80);
            this._LS12.Name = "_LS12";
            this._LS12.Size = new System.Drawing.Size(13, 13);
            this._LS12.State = BEMN.Forms.LedState.Off;
            this._LS12.TabIndex = 95;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label87.Location = new System.Drawing.Point(63, 61);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(34, 13);
            this.label87.TabIndex = 94;
            this.label87.Text = "��11";
            // 
            // _LS11
            // 
            this._LS11.Location = new System.Drawing.Point(99, 61);
            this._LS11.Name = "_LS11";
            this._LS11.Size = new System.Drawing.Size(13, 13);
            this._LS11.State = BEMN.Forms.LedState.Off;
            this._LS11.TabIndex = 93;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label86.Location = new System.Drawing.Point(63, 42);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(34, 13);
            this.label86.TabIndex = 92;
            this.label86.Text = "��10";
            // 
            // _LS10
            // 
            this._LS10.Location = new System.Drawing.Point(99, 42);
            this._LS10.Name = "_LS10";
            this._LS10.Size = new System.Drawing.Size(13, 13);
            this._LS10.State = BEMN.Forms.LedState.Off;
            this._LS10.TabIndex = 91;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label85.Location = new System.Drawing.Point(63, 23);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 90;
            this.label85.Text = "��9";
            // 
            // _LS9
            // 
            this._LS9.Location = new System.Drawing.Point(99, 23);
            this._LS9.Name = "_LS9";
            this._LS9.Size = new System.Drawing.Size(13, 13);
            this._LS9.State = BEMN.Forms.LedState.Off;
            this._LS9.TabIndex = 89;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label84.Location = new System.Drawing.Point(6, 156);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 88;
            this.label84.Text = "��8";
            // 
            // _LS8
            // 
            this._LS8.Location = new System.Drawing.Point(38, 156);
            this._LS8.Name = "_LS8";
            this._LS8.Size = new System.Drawing.Size(13, 13);
            this._LS8.State = BEMN.Forms.LedState.Off;
            this._LS8.TabIndex = 87;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label83.Location = new System.Drawing.Point(6, 137);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 86;
            this.label83.Text = "��7";
            // 
            // _LS7
            // 
            this._LS7.Location = new System.Drawing.Point(38, 137);
            this._LS7.Name = "_LS7";
            this._LS7.Size = new System.Drawing.Size(13, 13);
            this._LS7.State = BEMN.Forms.LedState.Off;
            this._LS7.TabIndex = 85;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label82.Location = new System.Drawing.Point(6, 118);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(28, 13);
            this.label82.TabIndex = 84;
            this.label82.Text = "��6";
            // 
            // _LS6
            // 
            this._LS6.Location = new System.Drawing.Point(38, 118);
            this._LS6.Name = "_LS6";
            this._LS6.Size = new System.Drawing.Size(13, 13);
            this._LS6.State = BEMN.Forms.LedState.Off;
            this._LS6.TabIndex = 83;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label81.Location = new System.Drawing.Point(6, 99);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 82;
            this.label81.Text = "��5";
            // 
            // _LS5
            // 
            this._LS5.Location = new System.Drawing.Point(38, 99);
            this._LS5.Name = "_LS5";
            this._LS5.Size = new System.Drawing.Size(13, 13);
            this._LS5.State = BEMN.Forms.LedState.Off;
            this._LS5.TabIndex = 81;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label80.Location = new System.Drawing.Point(6, 80);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(28, 13);
            this.label80.TabIndex = 80;
            this.label80.Text = "��4";
            // 
            // _LS4
            // 
            this._LS4.Location = new System.Drawing.Point(38, 80);
            this._LS4.Name = "_LS4";
            this._LS4.Size = new System.Drawing.Size(13, 13);
            this._LS4.State = BEMN.Forms.LedState.Off;
            this._LS4.TabIndex = 79;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label79.Location = new System.Drawing.Point(6, 61);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(28, 13);
            this.label79.TabIndex = 78;
            this.label79.Text = "��3";
            // 
            // _LS3
            // 
            this._LS3.Location = new System.Drawing.Point(38, 61);
            this._LS3.Name = "_LS3";
            this._LS3.Size = new System.Drawing.Size(13, 13);
            this._LS3.State = BEMN.Forms.LedState.Off;
            this._LS3.TabIndex = 77;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label78.Location = new System.Drawing.Point(6, 42);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(28, 13);
            this.label78.TabIndex = 76;
            this.label78.Text = "��2";
            // 
            // _LS2
            // 
            this._LS2.Location = new System.Drawing.Point(38, 42);
            this._LS2.Name = "_LS2";
            this._LS2.Size = new System.Drawing.Size(13, 13);
            this._LS2.State = BEMN.Forms.LedState.Off;
            this._LS2.TabIndex = 75;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label77.Location = new System.Drawing.Point(6, 23);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(28, 13);
            this.label77.TabIndex = 74;
            this.label77.Text = "��1";
            // 
            // _LS1
            // 
            this._LS1.Location = new System.Drawing.Point(38, 23);
            this._LS1.Name = "_LS1";
            this._LS1.Size = new System.Drawing.Size(13, 13);
            this._LS1.State = BEMN.Forms.LedState.Off;
            this._LS1.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label76);
            this.groupBox9.Controls.Add(this.label75);
            this.groupBox9.Controls.Add(this.label74);
            this.groupBox9.Controls.Add(this.label73);
            this.groupBox9.Controls.Add(this.label72);
            this.groupBox9.Controls.Add(this.label71);
            this.groupBox9.Controls.Add(this.label70);
            this.groupBox9.Controls.Add(this.label69);
            this.groupBox9.Controls.Add(this.label68);
            this.groupBox9.Controls.Add(this.label67);
            this.groupBox9.Controls.Add(this.label66);
            this.groupBox9.Controls.Add(this.label65);
            this.groupBox9.Controls.Add(this.label64);
            this.groupBox9.Controls.Add(this.label63);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.label16);
            this.groupBox9.Controls.Add(this.label14);
            this.groupBox9.Controls.Add(this.label12);
            this.groupBox9.Controls.Add(this.label10);
            this.groupBox9.Controls.Add(this._D24);
            this.groupBox9.Controls.Add(this._D23);
            this.groupBox9.Controls.Add(this._D22);
            this.groupBox9.Controls.Add(this._D21);
            this.groupBox9.Controls.Add(this._D20);
            this.groupBox9.Controls.Add(this._D19);
            this.groupBox9.Controls.Add(this._D18);
            this.groupBox9.Controls.Add(this._D17);
            this.groupBox9.Controls.Add(this._D16);
            this.groupBox9.Controls.Add(this._D15);
            this.groupBox9.Controls.Add(this._D14);
            this.groupBox9.Controls.Add(this._D13);
            this.groupBox9.Controls.Add(this._D12);
            this.groupBox9.Controls.Add(this._D10);
            this.groupBox9.Controls.Add(this._D9);
            this.groupBox9.Controls.Add(this._D11);
            this.groupBox9.Controls.Add(this._D8);
            this.groupBox9.Controls.Add(this._D7);
            this.groupBox9.Controls.Add(this._D6);
            this.groupBox9.Controls.Add(this._D5);
            this.groupBox9.Controls.Add(this._D4);
            this.groupBox9.Controls.Add(this._D3);
            this.groupBox9.Controls.Add(this._D2);
            this.groupBox9.Controls.Add(this._D1);
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox9.Location = new System.Drawing.Point(6, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(156, 175);
            this.groupBox9.TabIndex = 48;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "��������";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label76.Location = new System.Drawing.Point(107, 157);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(27, 13);
            this.label76.TabIndex = 95;
            this.label76.Text = "D24";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label75.Location = new System.Drawing.Point(107, 138);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(27, 13);
            this.label75.TabIndex = 94;
            this.label75.Text = "D23";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label74.Location = new System.Drawing.Point(107, 119);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(27, 13);
            this.label74.TabIndex = 93;
            this.label74.Text = "D22";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label73.Location = new System.Drawing.Point(107, 100);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(27, 13);
            this.label73.TabIndex = 92;
            this.label73.Text = "D21";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label72.Location = new System.Drawing.Point(107, 81);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(27, 13);
            this.label72.TabIndex = 91;
            this.label72.Text = "D20";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label71.Location = new System.Drawing.Point(107, 62);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(27, 13);
            this.label71.TabIndex = 90;
            this.label71.Text = "D19";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label70.Location = new System.Drawing.Point(107, 43);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(27, 13);
            this.label70.TabIndex = 89;
            this.label70.Text = "D18";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label69.Location = new System.Drawing.Point(107, 24);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(27, 13);
            this.label69.TabIndex = 88;
            this.label69.Text = "D17";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label68.Location = new System.Drawing.Point(55, 157);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(27, 13);
            this.label68.TabIndex = 87;
            this.label68.Text = "D16";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label67.Location = new System.Drawing.Point(55, 138);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(27, 13);
            this.label67.TabIndex = 86;
            this.label67.Text = "D15";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label66.Location = new System.Drawing.Point(55, 119);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(27, 13);
            this.label66.TabIndex = 85;
            this.label66.Text = "D14";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label65.Location = new System.Drawing.Point(55, 100);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(27, 13);
            this.label65.TabIndex = 84;
            this.label65.Text = "D13";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label64.Location = new System.Drawing.Point(55, 81);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(27, 13);
            this.label64.TabIndex = 83;
            this.label64.Text = "D12";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label63.Location = new System.Drawing.Point(55, 62);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(27, 13);
            this.label63.TabIndex = 82;
            this.label63.Text = "D11";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label58.Location = new System.Drawing.Point(55, 43);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(27, 13);
            this.label58.TabIndex = 81;
            this.label58.Text = "D10";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label57.Location = new System.Drawing.Point(55, 24);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(21, 13);
            this.label57.TabIndex = 80;
            this.label57.Text = "D9";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label55.Location = new System.Drawing.Point(8, 157);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(21, 13);
            this.label55.TabIndex = 79;
            this.label55.Text = "D8";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label51.Location = new System.Drawing.Point(8, 138);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(21, 13);
            this.label51.TabIndex = 78;
            this.label51.Text = "D7";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(8, 119);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(21, 13);
            this.label46.TabIndex = 77;
            this.label46.Text = "D6";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(8, 100);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 13);
            this.label41.TabIndex = 76;
            this.label41.Text = "D5";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(8, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 13);
            this.label16.TabIndex = 75;
            this.label16.Text = "D4";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(8, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 13);
            this.label14.TabIndex = 74;
            this.label14.Text = "D3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(8, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 73;
            this.label12.Text = "D1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(8, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 72;
            this.label10.Text = "D2";
            // 
            // _D24
            // 
            this._D24.Location = new System.Drawing.Point(137, 157);
            this._D24.Name = "_D24";
            this._D24.Size = new System.Drawing.Size(13, 13);
            this._D24.State = BEMN.Forms.LedState.Off;
            this._D24.TabIndex = 71;
            // 
            // _D23
            // 
            this._D23.Location = new System.Drawing.Point(137, 138);
            this._D23.Name = "_D23";
            this._D23.Size = new System.Drawing.Size(13, 13);
            this._D23.State = BEMN.Forms.LedState.Off;
            this._D23.TabIndex = 70;
            // 
            // _D22
            // 
            this._D22.Location = new System.Drawing.Point(137, 119);
            this._D22.Name = "_D22";
            this._D22.Size = new System.Drawing.Size(13, 13);
            this._D22.State = BEMN.Forms.LedState.Off;
            this._D22.TabIndex = 69;
            // 
            // _D21
            // 
            this._D21.Location = new System.Drawing.Point(137, 100);
            this._D21.Name = "_D21";
            this._D21.Size = new System.Drawing.Size(13, 13);
            this._D21.State = BEMN.Forms.LedState.Off;
            this._D21.TabIndex = 68;
            // 
            // _D20
            // 
            this._D20.Location = new System.Drawing.Point(137, 81);
            this._D20.Name = "_D20";
            this._D20.Size = new System.Drawing.Size(13, 13);
            this._D20.State = BEMN.Forms.LedState.Off;
            this._D20.TabIndex = 67;
            // 
            // _D19
            // 
            this._D19.Location = new System.Drawing.Point(137, 62);
            this._D19.Name = "_D19";
            this._D19.Size = new System.Drawing.Size(13, 13);
            this._D19.State = BEMN.Forms.LedState.Off;
            this._D19.TabIndex = 66;
            // 
            // _D18
            // 
            this._D18.Location = new System.Drawing.Point(137, 43);
            this._D18.Name = "_D18";
            this._D18.Size = new System.Drawing.Size(13, 13);
            this._D18.State = BEMN.Forms.LedState.Off;
            this._D18.TabIndex = 65;
            // 
            // _D17
            // 
            this._D17.Location = new System.Drawing.Point(137, 24);
            this._D17.Name = "_D17";
            this._D17.Size = new System.Drawing.Size(13, 13);
            this._D17.State = BEMN.Forms.LedState.Off;
            this._D17.TabIndex = 64;
            // 
            // _D16
            // 
            this._D16.Location = new System.Drawing.Point(85, 157);
            this._D16.Name = "_D16";
            this._D16.Size = new System.Drawing.Size(13, 13);
            this._D16.State = BEMN.Forms.LedState.Off;
            this._D16.TabIndex = 63;
            // 
            // _D15
            // 
            this._D15.Location = new System.Drawing.Point(85, 138);
            this._D15.Name = "_D15";
            this._D15.Size = new System.Drawing.Size(13, 13);
            this._D15.State = BEMN.Forms.LedState.Off;
            this._D15.TabIndex = 62;
            // 
            // _D14
            // 
            this._D14.Location = new System.Drawing.Point(85, 119);
            this._D14.Name = "_D14";
            this._D14.Size = new System.Drawing.Size(13, 13);
            this._D14.State = BEMN.Forms.LedState.Off;
            this._D14.TabIndex = 61;
            // 
            // _D13
            // 
            this._D13.Location = new System.Drawing.Point(85, 100);
            this._D13.Name = "_D13";
            this._D13.Size = new System.Drawing.Size(13, 13);
            this._D13.State = BEMN.Forms.LedState.Off;
            this._D13.TabIndex = 60;
            // 
            // _D12
            // 
            this._D12.Location = new System.Drawing.Point(85, 81);
            this._D12.Name = "_D12";
            this._D12.Size = new System.Drawing.Size(13, 13);
            this._D12.State = BEMN.Forms.LedState.Off;
            this._D12.TabIndex = 59;
            // 
            // _D10
            // 
            this._D10.Location = new System.Drawing.Point(85, 43);
            this._D10.Name = "_D10";
            this._D10.Size = new System.Drawing.Size(13, 13);
            this._D10.State = BEMN.Forms.LedState.Off;
            this._D10.TabIndex = 58;
            // 
            // _D9
            // 
            this._D9.Location = new System.Drawing.Point(85, 24);
            this._D9.Name = "_D9";
            this._D9.Size = new System.Drawing.Size(13, 13);
            this._D9.State = BEMN.Forms.LedState.Off;
            this._D9.TabIndex = 57;
            // 
            // _D11
            // 
            this._D11.Location = new System.Drawing.Point(85, 62);
            this._D11.Name = "_D11";
            this._D11.Size = new System.Drawing.Size(13, 13);
            this._D11.State = BEMN.Forms.LedState.Off;
            this._D11.TabIndex = 56;
            // 
            // _D8
            // 
            this._D8.Location = new System.Drawing.Point(31, 157);
            this._D8.Name = "_D8";
            this._D8.Size = new System.Drawing.Size(13, 13);
            this._D8.State = BEMN.Forms.LedState.Off;
            this._D8.TabIndex = 55;
            // 
            // _D7
            // 
            this._D7.Location = new System.Drawing.Point(31, 138);
            this._D7.Name = "_D7";
            this._D7.Size = new System.Drawing.Size(13, 13);
            this._D7.State = BEMN.Forms.LedState.Off;
            this._D7.TabIndex = 54;
            // 
            // _D6
            // 
            this._D6.Location = new System.Drawing.Point(31, 119);
            this._D6.Name = "_D6";
            this._D6.Size = new System.Drawing.Size(13, 13);
            this._D6.State = BEMN.Forms.LedState.Off;
            this._D6.TabIndex = 53;
            // 
            // _D5
            // 
            this._D5.Location = new System.Drawing.Point(31, 100);
            this._D5.Name = "_D5";
            this._D5.Size = new System.Drawing.Size(13, 13);
            this._D5.State = BEMN.Forms.LedState.Off;
            this._D5.TabIndex = 52;
            // 
            // _D4
            // 
            this._D4.Location = new System.Drawing.Point(31, 81);
            this._D4.Name = "_D4";
            this._D4.Size = new System.Drawing.Size(13, 13);
            this._D4.State = BEMN.Forms.LedState.Off;
            this._D4.TabIndex = 51;
            // 
            // _D3
            // 
            this._D3.Location = new System.Drawing.Point(31, 62);
            this._D3.Name = "_D3";
            this._D3.Size = new System.Drawing.Size(13, 13);
            this._D3.State = BEMN.Forms.LedState.Off;
            this._D3.TabIndex = 50;
            // 
            // _D2
            // 
            this._D2.Location = new System.Drawing.Point(31, 43);
            this._D2.Name = "_D2";
            this._D2.Size = new System.Drawing.Size(13, 13);
            this._D2.State = BEMN.Forms.LedState.Off;
            this._D2.TabIndex = 49;
            // 
            // _D1
            // 
            this._D1.Location = new System.Drawing.Point(31, 24);
            this._D1.Name = "_D1";
            this._D1.Size = new System.Drawing.Size(13, 13);
            this._D1.State = BEMN.Forms.LedState.Off;
            this._D1.TabIndex = 48;
            // 
            // _uprPage
            // 
            this._uprPage.Controls.Add(this.groupBox28);
            this._uprPage.Controls.Add(this.groupBox27);
            this._uprPage.ImageIndex = 1;
            this._uprPage.Location = new System.Drawing.Point(4, 23);
            this._uprPage.Name = "_uprPage";
            this._uprPage.Padding = new System.Windows.Forms.Padding(3);
            this._uprPage.Size = new System.Drawing.Size(765, 585);
            this._uprPage.TabIndex = 2;
            this._uprPage.Text = "����������� �������";
            this._uprPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.indJourSysLed);
            this.groupBox28.Controls.Add(this.indDefectRelay);
            this.groupBox28.Controls.Add(this.GROOPREZERVE_BUT);
            this.groupBox28.Controls.Add(this.indJourAlarmLed);
            this.groupBox28.Controls.Add(this.GROOPMAIN_BUT);
            this.groupBox28.Controls.Add(this.label229);
            this.groupBox28.Controls.Add(this.label228);
            this.groupBox28.Controls.Add(this._bdGroupRez);
            this.groupBox28.Controls.Add(this._bdGroupMain);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(359, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(191, 83);
            this.groupBox28.TabIndex = 22;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "������ ������� �� ����������";
            // 
            // indJourSysLed
            // 
            this.indJourSysLed.Location = new System.Drawing.Point(41, 19);
            this.indJourSysLed.Name = "indJourSysLed";
            this.indJourSysLed.Size = new System.Drawing.Size(14, 13);
            this.indJourSysLed.State = BEMN.Forms.LedState.Off;
            this.indJourSysLed.TabIndex = 31;
            this.indJourSysLed.Visible = false;
            // 
            // indDefectRelay
            // 
            this.indDefectRelay.Location = new System.Drawing.Point(41, 19);
            this.indDefectRelay.Name = "indDefectRelay";
            this.indDefectRelay.Size = new System.Drawing.Size(14, 13);
            this.indDefectRelay.State = BEMN.Forms.LedState.Off;
            this.indDefectRelay.TabIndex = 32;
            this.indDefectRelay.Visible = false;
            // 
            // GROOPREZERVE_BUT
            // 
            this.GROOPREZERVE_BUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GROOPREZERVE_BUT.Location = new System.Drawing.Point(99, 56);
            this.GROOPREZERVE_BUT.Name = "GROOPREZERVE_BUT";
            this.GROOPREZERVE_BUT.Size = new System.Drawing.Size(86, 23);
            this.GROOPREZERVE_BUT.TabIndex = 30;
            this.GROOPREZERVE_BUT.Text = "�����������";
            this.GROOPREZERVE_BUT.UseVisualStyleBackColor = true;
            this.GROOPREZERVE_BUT.Click += new System.EventHandler(this.GROOPREZERVE_Click);
            // 
            // indJourAlarmLed
            // 
            this.indJourAlarmLed.Location = new System.Drawing.Point(41, 19);
            this.indJourAlarmLed.Name = "indJourAlarmLed";
            this.indJourAlarmLed.Size = new System.Drawing.Size(14, 13);
            this.indJourAlarmLed.State = BEMN.Forms.LedState.Off;
            this.indJourAlarmLed.TabIndex = 33;
            this.indJourAlarmLed.Visible = false;
            // 
            // GROOPMAIN_BUT
            // 
            this.GROOPMAIN_BUT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GROOPMAIN_BUT.Location = new System.Drawing.Point(6, 56);
            this.GROOPMAIN_BUT.Name = "GROOPMAIN_BUT";
            this.GROOPMAIN_BUT.Size = new System.Drawing.Size(86, 23);
            this.GROOPMAIN_BUT.TabIndex = 29;
            this.GROOPMAIN_BUT.Text = "�����������";
            this.GROOPMAIN_BUT.UseVisualStyleBackColor = true;
            this.GROOPMAIN_BUT.Click += new System.EventHandler(this.GROOPMAIN_Click);
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label229.Location = new System.Drawing.Point(110, 39);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(62, 13);
            this.label229.TabIndex = 28;
            this.label229.Text = "���������";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label228.Location = new System.Drawing.Point(21, 39);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(57, 13);
            this.label228.TabIndex = 27;
            this.label228.Text = "��������";
            // 
            // _bdGroupRez
            // 
            this._bdGroupRez.Location = new System.Drawing.Point(136, 19);
            this._bdGroupRez.Name = "_bdGroupRez";
            this._bdGroupRez.Size = new System.Drawing.Size(13, 13);
            this._bdGroupRez.State = BEMN.Forms.LedState.Off;
            this._bdGroupRez.TabIndex = 24;
            // 
            // _bdGroupMain
            // 
            this._bdGroupMain.Location = new System.Drawing.Point(41, 19);
            this._bdGroupMain.Name = "_bdGroupMain";
            this._bdGroupMain.Size = new System.Drawing.Size(14, 13);
            this._bdGroupMain.State = BEMN.Forms.LedState.Off;
            this._bdGroupMain.TabIndex = 23;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this.SwitchOff);
            this.groupBox27.Controls.Add(this.BDC_MESS_DISPSYS);
            this.groupBox27.Controls.Add(this.label274);
            this.groupBox27.Controls.Add(this.BDC_MESS_OSC);
            this.groupBox27.Controls.Add(this.SWITCH_ON);
            this.groupBox27.Controls.Add(this.BDC_MESS_ALARM);
            this.groupBox27.Controls.Add(this.SwitchOn);
            this.groupBox27.Controls.Add(this.BDC_MESS_SYS);
            this.groupBox27.Controls.Add(this.label273);
            this.groupBox27.Controls.Add(this.button5);
            this.groupBox27.Controls.Add(this.SWITCH_OFF);
            this.groupBox27.Controls.Add(this.ResetJS_Error);
            this.groupBox27.Controls.Add(this.ResetJO);
            this.groupBox27.Controls.Add(this.ResetJA);
            this.groupBox27.Controls.Add(this.ResetJS);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label224);
            this.groupBox27.Controls.Add(this.label223);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(347, 262);
            this.groupBox27.TabIndex = 0;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "����������� �������";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(18, 158);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 55;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "�������� ��������������� ������";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "����������";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.stopLogic_Click);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "���������";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.startLogic_Click);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "��������� ������ ������";
            // 
            // SwitchOff
            // 
            this.SwitchOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.SwitchOff.Location = new System.Drawing.Point(266, 129);
            this.SwitchOff.Name = "SwitchOff";
            this.SwitchOff.Size = new System.Drawing.Size(75, 23);
            this.SwitchOff.TabIndex = 18;
            this.SwitchOff.Text = "���������";
            this.SwitchOff.UseVisualStyleBackColor = true;
            this.SwitchOff.Click += new System.EventHandler(this.SwitchOff_Click);
            // 
            // BDC_MESS_DISPSYS
            // 
            this.BDC_MESS_DISPSYS.Location = new System.Drawing.Point(6, 88);
            this.BDC_MESS_DISPSYS.Name = "BDC_MESS_DISPSYS";
            this.BDC_MESS_DISPSYS.Size = new System.Drawing.Size(13, 13);
            this.BDC_MESS_DISPSYS.State = BEMN.Forms.LedState.Off;
            this.BDC_MESS_DISPSYS.TabIndex = 13;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label274.Location = new System.Drawing.Point(30, 134);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(122, 13);
            this.label274.TabIndex = 17;
            this.label274.Text = "����������� �������";
            // 
            // BDC_MESS_OSC
            // 
            this.BDC_MESS_OSC.Location = new System.Drawing.Point(6, 65);
            this.BDC_MESS_OSC.Name = "BDC_MESS_OSC";
            this.BDC_MESS_OSC.Size = new System.Drawing.Size(13, 13);
            this.BDC_MESS_OSC.State = BEMN.Forms.LedState.Off;
            this.BDC_MESS_OSC.TabIndex = 12;
            // 
            // SWITCH_ON
            // 
            this.SWITCH_ON.Location = new System.Drawing.Point(6, 134);
            this.SWITCH_ON.Name = "SWITCH_ON";
            this.SWITCH_ON.Size = new System.Drawing.Size(13, 13);
            this.SWITCH_ON.State = BEMN.Forms.LedState.Off;
            this.SWITCH_ON.TabIndex = 16;
            // 
            // BDC_MESS_ALARM
            // 
            this.BDC_MESS_ALARM.Location = new System.Drawing.Point(6, 42);
            this.BDC_MESS_ALARM.Name = "BDC_MESS_ALARM";
            this.BDC_MESS_ALARM.Size = new System.Drawing.Size(13, 13);
            this.BDC_MESS_ALARM.State = BEMN.Forms.LedState.Off;
            this.BDC_MESS_ALARM.TabIndex = 11;
            // 
            // SwitchOn
            // 
            this.SwitchOn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.SwitchOn.Location = new System.Drawing.Point(266, 106);
            this.SwitchOn.Name = "SwitchOn";
            this.SwitchOn.Size = new System.Drawing.Size(75, 23);
            this.SwitchOn.TabIndex = 15;
            this.SwitchOn.Text = "��������";
            this.SwitchOn.UseVisualStyleBackColor = true;
            this.SwitchOn.Click += new System.EventHandler(this.SwitchOn_Click);
            // 
            // BDC_MESS_SYS
            // 
            this.BDC_MESS_SYS.Location = new System.Drawing.Point(6, 19);
            this.BDC_MESS_SYS.Name = "BDC_MESS_SYS";
            this.BDC_MESS_SYS.Size = new System.Drawing.Size(13, 13);
            this.BDC_MESS_SYS.State = BEMN.Forms.LedState.Off;
            this.BDC_MESS_SYS.TabIndex = 10;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label273.Location = new System.Drawing.Point(30, 111);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(205, 13);
            this.label273.TabIndex = 0;
            this.label273.Text = "����������� �������� �� ����������";
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.Location = new System.Drawing.Point(2, 229);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(335, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "����� ���������";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // SWITCH_OFF
            // 
            this.SWITCH_OFF.Location = new System.Drawing.Point(6, 111);
            this.SWITCH_OFF.Name = "SWITCH_OFF";
            this.SWITCH_OFF.Size = new System.Drawing.Size(13, 13);
            this.SWITCH_OFF.State = BEMN.Forms.LedState.Off;
            this.SWITCH_OFF.TabIndex = 14;
            // 
            // ResetJS_Error
            // 
            this.ResetJS_Error.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetJS_Error.Location = new System.Drawing.Point(266, 83);
            this.ResetJS_Error.Name = "ResetJS_Error";
            this.ResetJS_Error.Size = new System.Drawing.Size(75, 23);
            this.ResetJS_Error.TabIndex = 8;
            this.ResetJS_Error.Text = "��������";
            this.ResetJS_Error.UseVisualStyleBackColor = true;
            this.ResetJS_Error.Click += new System.EventHandler(this.ResetJS_Error_Click);
            // 
            // ResetJO
            // 
            this.ResetJO.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetJO.Location = new System.Drawing.Point(266, 60);
            this.ResetJO.Name = "ResetJO";
            this.ResetJO.Size = new System.Drawing.Size(75, 23);
            this.ResetJO.TabIndex = 7;
            this.ResetJO.Text = "��������";
            this.ResetJO.UseVisualStyleBackColor = true;
            this.ResetJO.Click += new System.EventHandler(this.ResetJO_Click);
            // 
            // ResetJA
            // 
            this.ResetJA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetJA.Location = new System.Drawing.Point(266, 37);
            this.ResetJA.Name = "ResetJA";
            this.ResetJA.Size = new System.Drawing.Size(75, 23);
            this.ResetJA.TabIndex = 6;
            this.ResetJA.Text = "��������";
            this.ResetJA.UseVisualStyleBackColor = true;
            this.ResetJA.Click += new System.EventHandler(this.ResetJA_Click);
            // 
            // ResetJS
            // 
            this.ResetJS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetJS.Location = new System.Drawing.Point(266, 14);
            this.ResetJS.Name = "ResetJS";
            this.ResetJS.Size = new System.Drawing.Size(75, 23);
            this.ResetJS.TabIndex = 5;
            this.ResetJS.Text = "��������";
            this.ResetJS.UseVisualStyleBackColor = true;
            this.ResetJS.Click += new System.EventHandler(this.ResetJS_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 88);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "������� ������������� �� ��";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 65);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "����� ������ ������� ������������";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label224.Location = new System.Drawing.Point(30, 42);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(163, 13);
            this.label224.TabIndex = 1;
            this.label224.Text = "����� ������ ������� ������";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label223.Location = new System.Drawing.Point(30, 19);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(172, 13);
            this.label223.TabIndex = 0;
            this.label223.Text = "����� ������ ������� �������";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(27, 150);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 35;
            this.label139.Text = "10";
            // 
            // label14_releLed9Label0
            // 
            this.label14_releLed9Label0.AutoSize = true;
            this.label14_releLed9Label0.Location = new System.Drawing.Point(27, 165);
            this.label14_releLed9Label0.Name = "label14_releLed9Label0";
            this.label14_releLed9Label0.Size = new System.Drawing.Size(19, 13);
            this.label14_releLed9Label0.TabIndex = 37;
            this.label14_releLed9Label0.Text = "11";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(27, 195);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 41;
            this.label142.Text = "13";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "4";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "6";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 120);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 31;
            this.label9.Text = "8";
            // 
            // KgroupBox
            // 
            this.KgroupBox.Controls.Add(this._k2);
            this.KgroupBox.Controls.Add(this._k1);
            this.KgroupBox.Controls.Add(this.label151);
            this.KgroupBox.Controls.Add(this.label152);
            this.KgroupBox.Location = new System.Drawing.Point(428, 277);
            this.KgroupBox.Name = "KgroupBox";
            this.KgroupBox.Size = new System.Drawing.Size(103, 71);
            this.KgroupBox.TabIndex = 64;
            this.KgroupBox.TabStop = false;
            this.KgroupBox.Text = "����� �������� ����� ����������";
            // 
            // _k1
            // 
            this._k1.Location = new System.Drawing.Point(32, 52);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(13, 13);
            this._k1.State = BEMN.Forms.LedState.Off;
            this._k1.TabIndex = 0;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label151.Location = new System.Drawing.Point(9, 52);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(20, 13);
            this.label151.TabIndex = 74;
            this.label151.Text = "�1";
            // 
            // _k2
            // 
            this._k2.Location = new System.Drawing.Point(83, 52);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(13, 13);
            this._k2.State = BEMN.Forms.LedState.Off;
            this._k2.TabIndex = 89;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label152.Location = new System.Drawing.Point(60, 52);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(20, 13);
            this.label152.TabIndex = 90;
            this.label152.Text = "�2";
            // 
            // MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 612);
            this.Controls.Add(this._diagTab);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(789, 650);
            this.Name = "MeasuringForm";
            this.Text = "���������";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._diagTab.ResumeLayout(false);
            this._analogPage.ResumeLayout(false);
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this._diskretPage.ResumeLayout(false);
            this.v_1_11_Panel.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._uprPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.KgroupBox.ResumeLayout(false);
            this.KgroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList _images;
        private System.Windows.Forms.TabControl _diagTab;
        private System.Windows.Forms.TabPage _analogPage;
        private System.Windows.Forms.TabPage _diskretPage;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label14_releLed9Label0;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _U2;
        private System.Windows.Forms.TextBox _U0;
        private System.Windows.Forms.TextBox _Uca;
        private System.Windows.Forms.TextBox _Ubc;
        private System.Windows.Forms.TextBox _Uab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox _Is30;
        private System.Windows.Forms.TextBox _Is3n;
        private System.Windows.Forms.TextBox _Is3c;
        private System.Windows.Forms.TextBox _Is3b;
        private System.Windows.Forms.TextBox _Is3a;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox _Is20;
        private System.Windows.Forms.TextBox _Is2n;
        private System.Windows.Forms.TextBox _Is2c;
        private System.Windows.Forms.TextBox _Is2b;
        private System.Windows.Forms.TextBox _Is2a;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox _Is10;
        private System.Windows.Forms.TextBox _Is1n;
        private System.Windows.Forms.TextBox _Is1c;
        private System.Windows.Forms.TextBox _Is1b;
        private System.Windows.Forms.TextBox _Is1a;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox _Id2c;
        private System.Windows.Forms.TextBox _Id2b;
        private System.Windows.Forms.TextBox _Id2a;
        private System.Windows.Forms.TextBox _Idc;
        private System.Windows.Forms.TextBox _Idb;
        private System.Windows.Forms.TextBox _Ida;
        private System.Windows.Forms.TextBox _Ibc;
        private System.Windows.Forms.TextBox _Ibb;
        private System.Windows.Forms.TextBox _Iba;
        private System.Windows.Forms.TextBox _Id5c;
        private System.Windows.Forms.TextBox _Id5b;
        private System.Windows.Forms.TextBox _Id5a;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.TextBox _Un;
        private System.Windows.Forms.TextBox _Uc;
        private System.Windows.Forms.TextBox _Ub;
        private System.Windows.Forms.TextBox _Ua;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox _I30;
        private System.Windows.Forms.TextBox _I3c;
        private System.Windows.Forms.TextBox _I3b;
        private System.Windows.Forms.TextBox _I3a;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.TextBox _I20;
        private System.Windows.Forms.TextBox _I2c;
        private System.Windows.Forms.TextBox _I2b;
        private System.Windows.Forms.TextBox _I2a;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox _I0;
        private System.Windows.Forms.TextBox _Ic;
        private System.Windows.Forms.TextBox _Ib;
        private System.Windows.Forms.TextBox _Ia;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _D24;
        private BEMN.Forms.LedControl _D23;
        private BEMN.Forms.LedControl _D22;
        private BEMN.Forms.LedControl _D21;
        private BEMN.Forms.LedControl _D20;
        private BEMN.Forms.LedControl _D19;
        private BEMN.Forms.LedControl _D18;
        private BEMN.Forms.LedControl _D17;
        private BEMN.Forms.LedControl _D16;
        private BEMN.Forms.LedControl _D15;
        private BEMN.Forms.LedControl _D14;
        private BEMN.Forms.LedControl _D13;
        private BEMN.Forms.LedControl _D12;
        private BEMN.Forms.LedControl _D10;
        private BEMN.Forms.LedControl _D9;
        private BEMN.Forms.LedControl _D11;
        private BEMN.Forms.LedControl _D8;
        private BEMN.Forms.LedControl _D7;
        private BEMN.Forms.LedControl _D6;
        private BEMN.Forms.LedControl _D5;
        private BEMN.Forms.LedControl _D4;
        private BEMN.Forms.LedControl _D3;
        private BEMN.Forms.LedControl _D2;
        private BEMN.Forms.LedControl _D1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label108;
        private BEMN.Forms.LedControl _VLS16;
        private System.Windows.Forms.Label label107;
        private BEMN.Forms.LedControl _VLS15;
        private System.Windows.Forms.Label label106;
        private BEMN.Forms.LedControl _VLS14;
        private System.Windows.Forms.Label label105;
        private BEMN.Forms.LedControl _VLS13;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _VLS12;
        private System.Windows.Forms.Label label103;
        private BEMN.Forms.LedControl _VLS11;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _VLS10;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _VLS9;
        private System.Windows.Forms.Label label100;
        private BEMN.Forms.LedControl _VLS8;
        private System.Windows.Forms.Label label99;
        private BEMN.Forms.LedControl _VLS7;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _VLS6;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _VLS5;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _VLS4;
        private System.Windows.Forms.Label label95;
        private BEMN.Forms.LedControl _VLS3;
        private System.Windows.Forms.Label label94;
        private BEMN.Forms.LedControl _VLS2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _VLS1;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _LS16;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _LS15;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _LS14;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _LS13;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _LS12;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _LS11;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _LS10;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _LS9;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _LS8;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _LS7;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _LS6;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _LS5;
        private System.Windows.Forms.Label label80;
        private BEMN.Forms.LedControl _LS4;
        private System.Windows.Forms.Label label79;
        private BEMN.Forms.LedControl _LS3;
        private System.Windows.Forms.Label label78;
        private BEMN.Forms.LedControl _LS2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _LS1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _IdMax2L;
        private System.Windows.Forms.Label _IdMax2IOL;
        private System.Windows.Forms.Label label109;
        private BEMN.Forms.LedControl _IdMax1;
        private BEMN.Forms.LedControl _IdMax1IO;
        private BEMN.Forms.LedControl _IdMax2;
        private BEMN.Forms.LedControl _IdMax2IO;
        private BEMN.Forms.LedControl _IdMax2Mgn;
        private System.Windows.Forms.Label _IdMax1L;
        private System.Windows.Forms.Label _IdMax1IOL;
        private BEMN.Forms.LedControl _Id0Max1;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _Id0Max1IO;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _Id0Max3;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _Id0Max3IO;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _Id0Max2;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _Id0Max2IO;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _I8;
        private BEMN.Forms.LedControl _I7;
        private BEMN.Forms.LedControl _I6;
        private BEMN.Forms.LedControl _I5;
        private BEMN.Forms.LedControl _I4;
        private BEMN.Forms.LedControl _I3;
        private BEMN.Forms.LedControl _I2;
        private BEMN.Forms.LedControl _I1;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _I8IO;
        private BEMN.Forms.LedControl _I7IO;
        private BEMN.Forms.LedControl _I6IO;
        private BEMN.Forms.LedControl _I5IO;
        private BEMN.Forms.LedControl _I4IO;
        private BEMN.Forms.LedControl _I3IO;
        private BEMN.Forms.LedControl _I2IO;
        private BEMN.Forms.LedControl _I1IO;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _I03;
        private BEMN.Forms.LedControl _I02;
        private BEMN.Forms.LedControl _I01;
        private System.Windows.Forms.Label label135;
        private BEMN.Forms.LedControl _I03IO;
        private BEMN.Forms.LedControl _I02IO;
        private BEMN.Forms.LedControl _I01IO;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label144;
        private BEMN.Forms.LedControl _Umin4;
        private BEMN.Forms.LedControl _Umin4IO;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private BEMN.Forms.LedControl _Umin3;
        private BEMN.Forms.LedControl _Umin2;
        private BEMN.Forms.LedControl _Umin1;
        private System.Windows.Forms.Label label149;
        private BEMN.Forms.LedControl _Umin3IO;
        private BEMN.Forms.LedControl _Umin2IO;
        private BEMN.Forms.LedControl _Umin1IO;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label143;
        private BEMN.Forms.LedControl _Umax4;
        private BEMN.Forms.LedControl _Umax4IO;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label140;
        private BEMN.Forms.LedControl _Umax3;
        private BEMN.Forms.LedControl _Umax2;
        private BEMN.Forms.LedControl _Umax1;
        private System.Windows.Forms.Label label141;
        private BEMN.Forms.LedControl _Umax3IO;
        private BEMN.Forms.LedControl _Umax2IO;
        private BEMN.Forms.LedControl _Umax1IO;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label150;
        private BEMN.Forms.LedControl _EXT16;
        private System.Windows.Forms.Label label155;
        private BEMN.Forms.LedControl _EXT15;
        private System.Windows.Forms.Label label156;
        private BEMN.Forms.LedControl _EXT14;
        private System.Windows.Forms.Label label157;
        private BEMN.Forms.LedControl _EXT13;
        private System.Windows.Forms.Label label158;
        private BEMN.Forms.LedControl _EXT12;
        private System.Windows.Forms.Label label159;
        private BEMN.Forms.LedControl _EXT11;
        private System.Windows.Forms.Label label160;
        private BEMN.Forms.LedControl _EXT10;
        private System.Windows.Forms.Label label161;
        private BEMN.Forms.LedControl _EXT9;
        private System.Windows.Forms.Label label162;
        private BEMN.Forms.LedControl _EXT8;
        private System.Windows.Forms.Label label163;
        private BEMN.Forms.LedControl _EXT7;
        private System.Windows.Forms.Label label164;
        private BEMN.Forms.LedControl _EXT6;
        private System.Windows.Forms.Label label165;
        private BEMN.Forms.LedControl _EXT5;
        private System.Windows.Forms.Label label166;
        private BEMN.Forms.LedControl _EXT4;
        private System.Windows.Forms.Label label167;
        private BEMN.Forms.LedControl _EXT3;
        private System.Windows.Forms.Label label168;
        private BEMN.Forms.LedControl _EXT2;
        private System.Windows.Forms.Label label169;
        private BEMN.Forms.LedControl _EXT1;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label label174;
        private BEMN.Forms.LedControl _Alarm;
        private System.Windows.Forms.Label label171;
        private BEMN.Forms.LedControl _GroupRez;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label173;
        private BEMN.Forms.LedControl _GroopMain;
        private BEMN.Forms.LedControl _Disrepair;
        private System.Windows.Forms.GroupBox groupBox26;
        private BEMN.Forms.LedControl _Ind12;
        private BEMN.Forms.LedControl _Ind11;
        private BEMN.Forms.LedControl _Ind10;
        private BEMN.Forms.LedControl _Ind9;
        private BEMN.Forms.LedControl _Ind8;
        private BEMN.Forms.LedControl _Ind7;
        private BEMN.Forms.LedControl _Ind6;
        private BEMN.Forms.LedControl _Ind5;
        private BEMN.Forms.LedControl _Ind4;
        private BEMN.Forms.LedControl _Ind3;
        private BEMN.Forms.LedControl _Ind2;
        private BEMN.Forms.LedControl _Ind1;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label175;
        private BEMN.Forms.LedControl _Rele16;
        private System.Windows.Forms.Label label176;
        private BEMN.Forms.LedControl _Rele15;
        private System.Windows.Forms.Label label177;
        private BEMN.Forms.LedControl _Rele14;
        private System.Windows.Forms.Label label178;
        private BEMN.Forms.LedControl _Rele13;
        private System.Windows.Forms.Label label179;
        private BEMN.Forms.LedControl _Rele12;
        private System.Windows.Forms.Label label180;
        private BEMN.Forms.LedControl _Rele11;
        private System.Windows.Forms.Label label181;
        private BEMN.Forms.LedControl _Rele10;
        private System.Windows.Forms.Label label182;
        private BEMN.Forms.LedControl _Rele9;
        private System.Windows.Forms.Label label183;
        private BEMN.Forms.LedControl _Rele8;
        private System.Windows.Forms.Label label184;
        private BEMN.Forms.LedControl _Rele7;
        private System.Windows.Forms.Label label185;
        private BEMN.Forms.LedControl _Rele6;
        private System.Windows.Forms.Label label186;
        private BEMN.Forms.LedControl _Rele5;
        private System.Windows.Forms.Label label187;
        private BEMN.Forms.LedControl _Rele4;
        private System.Windows.Forms.Label label188;
        private BEMN.Forms.LedControl _Rele3;
        private System.Windows.Forms.Label label189;
        private BEMN.Forms.LedControl _Rele2;
        private System.Windows.Forms.Label label190;
        private BEMN.Forms.LedControl _Rele1;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.Label label207;
        private BEMN.Forms.LedControl _Rele18;
        private System.Windows.Forms.Label label208;
        private BEMN.Forms.LedControl _Rele17;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _In3;
        private BEMN.Forms.LedControl _In2;
        private BEMN.Forms.LedControl _In1;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _In3IO;
        private BEMN.Forms.LedControl _In2IO;
        private BEMN.Forms.LedControl _In1IO;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label191;
        private BEMN.Forms.LedControl _Osc;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _Jalm;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _Jsys;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _Passwust;
        private System.Windows.Forms.Label label209;
        private BEMN.Forms.LedControl _Groupust;
        private System.Windows.Forms.Label label210;
        private BEMN.Forms.LedControl _Ust;
        private System.Windows.Forms.Label label211;
        private BEMN.Forms.LedControl _Mod5;
        private System.Windows.Forms.Label label212;
        private BEMN.Forms.LedControl _Mod4;
        private System.Windows.Forms.Label label213;
        private BEMN.Forms.LedControl _Mod3;
        private System.Windows.Forms.Label label214;
        private BEMN.Forms.LedControl _Mod2;
        private System.Windows.Forms.Label label215;
        private BEMN.Forms.LedControl _Mod1;
        private System.Windows.Forms.Label label216;
        private BEMN.Forms.LedControl _Contr;
        private System.Windows.Forms.Label label217;
        private BEMN.Forms.LedControl _Switch3;
        private System.Windows.Forms.Label label218;
        private BEMN.Forms.LedControl _Switch2;
        private System.Windows.Forms.Label label219;
        private BEMN.Forms.LedControl _Switch1;
        private System.Windows.Forms.Label label220;
        private BEMN.Forms.LedControl _Meas;
        private System.Windows.Forms.Label label221;
        private BEMN.Forms.LedControl _Soft;
        private System.Windows.Forms.Label label222;
        private BEMN.Forms.LedControl _Hard;
        private System.Windows.Forms.TabPage _uprPage;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button ResetJS_Error;
        private System.Windows.Forms.Button ResetJO;
        private System.Windows.Forms.Button ResetJA;
        private System.Windows.Forms.Button ResetJS;
        private BEMN.Forms.LedControl BDC_MESS_DISPSYS;
        private BEMN.Forms.LedControl BDC_MESS_OSC;
        private BEMN.Forms.LedControl BDC_MESS_ALARM;
        private BEMN.Forms.LedControl BDC_MESS_SYS;
        private System.Windows.Forms.GroupBox groupBox28;
        private BEMN.Forms.LedControl _bdGroupRez;
        private BEMN.Forms.LedControl _bdGroupMain;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.Button GROOPREZERVE_BUT;
        private System.Windows.Forms.Button GROOPMAIN_BUT;
        private System.Windows.Forms.Panel v_1_11_Panel;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.Label label226;
        private BEMN.Forms.LedControl _Fmin4;
        private BEMN.Forms.LedControl _Fmin4IO;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.Label label233;
        private BEMN.Forms.LedControl _Fmin3;
        private BEMN.Forms.LedControl _Fmin2;
        private BEMN.Forms.LedControl _Fmin1;
        private System.Windows.Forms.Label label234;
        private BEMN.Forms.LedControl _Fmin3IO;
        private BEMN.Forms.LedControl _Fmin2IO;
        private BEMN.Forms.LedControl _Fmin1IO;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label235;
        private BEMN.Forms.LedControl _Fmax4;
        private BEMN.Forms.LedControl _Fmax4IO;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.Label label239;
        private BEMN.Forms.LedControl _Fmax3;
        private BEMN.Forms.LedControl _Fmax2;
        private BEMN.Forms.LedControl _Fmax1;
        private System.Windows.Forms.Label label240;
        private BEMN.Forms.LedControl _Fmax3IO;
        private BEMN.Forms.LedControl _Fmax2IO;
        private BEMN.Forms.LedControl _Fmax1IO;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl SFL32;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl SFL31;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl SFL30;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl SFL29;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl SFL28;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl SFL27;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl SFL26;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl SFL25;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl SFL16;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl SFL15;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl SFL14;
        private System.Windows.Forms.Label label268;
        private BEMN.Forms.LedControl SFL13;
        private System.Windows.Forms.Label label269;
        private BEMN.Forms.LedControl SFL12;
        private System.Windows.Forms.Label label270;
        private BEMN.Forms.LedControl SFL11;
        private System.Windows.Forms.Label label271;
        private BEMN.Forms.LedControl SFL10;
        private System.Windows.Forms.Label label272;
        private BEMN.Forms.LedControl SFL9;
        private System.Windows.Forms.Label label241;
        private BEMN.Forms.LedControl SFL24;
        private System.Windows.Forms.Label label242;
        private BEMN.Forms.LedControl SFL23;
        private System.Windows.Forms.Label label243;
        private BEMN.Forms.LedControl SFL22;
        private System.Windows.Forms.Label label244;
        private BEMN.Forms.LedControl SFL21;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl SFL20;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl SFL19;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl SFL18;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl SFL17;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl SFL8;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl SFL7;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl SFL6;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl SFL5;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl SFL4;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl SFL3;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl SFL2;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl SFL1;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.Button SwitchOff;
        private System.Windows.Forms.Label label274;
        private BEMN.Forms.LedControl SWITCH_ON;
        private System.Windows.Forms.Button SwitchOn;
        private System.Windows.Forms.Label label273;
        private BEMN.Forms.LedControl SWITCH_OFF;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Label label278;
        private System.Windows.Forms.TextBox _frequency;
        private System.Windows.Forms.TextBox _Is32;
        private System.Windows.Forms.Label label277;
        private System.Windows.Forms.TextBox _Is22;
        private System.Windows.Forms.Label label276;
        private System.Windows.Forms.Label label275;
        private System.Windows.Forms.TextBox _Is12;
        private System.Windows.Forms.TextBox _percentIdcFromIbas;
        private System.Windows.Forms.TextBox _percentIdbFromIbas;
        private System.Windows.Forms.TextBox _percentIdaFromIbas;
        private System.Windows.Forms.TextBox _percentIbcFromIbas;
        private System.Windows.Forms.TextBox _percentIbbFromIbas;
        private System.Windows.Forms.TextBox _percentIbaFromIbas;
        private LedControl indJourSysLed;
        private LedControl indDefectRelay;
        private LedControl indJourAlarmLed;
        private System.Windows.Forms.GroupBox groupBox40;
        private LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.GroupBox KgroupBox;
        private LedControl _k2;
        private LedControl _k1;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
    }
}