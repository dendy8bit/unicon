﻿namespace BEMN.UDZT
{
    public enum ConstraintKoefficient
    {
        K_100 = 100,
        K_1000 = 1000,
        K_100000 = 100000,
        K_1000000 = 1000000,
        K_4000 = 4000,
        K_4001 = 4001,
    };
}