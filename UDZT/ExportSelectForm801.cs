﻿using System;
using System.Windows.Forms;
using BEMN.Forms.Export;

namespace BEMN.UDZT
{
    public partial class ExportSelectForm801 : Form
    {
        public ExportGroup Group { get; private set; }

        public ExportSelectForm801()
        {
            this.InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Group = ExportGroup.MAIN;
            DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Group = ExportGroup.RESERVE;
            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
