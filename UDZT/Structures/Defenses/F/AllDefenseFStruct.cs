﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.UDZT.Structures.Defenses.F
{
    public class AllDefenseFStruct : StructBase, IDgvRowsContainer<DefenseFStruct>
    {
        [Layout(0, Count = 4)]
        private DefenseFStruct[] _allDefenseF;

        [XmlArray(ElementName = "Все_защиты_F")]
        public DefenseFStruct[] Rows
        {
            get { return this._allDefenseF; }
            set { this._allDefenseF = value; }
        }
    }
}
