﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.UDZT.Structures.Defenses.DiffZero
{
    public class AllDefenceDiffZeroStruct : StructBase, IDgvRowsContainer<DefenceDiffZeroStruct>
    {
        public const int DEF_COUNT = 3;
        [Layout(0, Count = DEF_COUNT)] private DefenceDiffZeroStruct[] _deffZero;

        public DefenceDiffZeroStruct[] Rows
        {
            get => _deffZero;
            set => _deffZero = value;
        } 
    }
}
