﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.UDZT.Structures.Defenses.DiffZero
{
    public class DefenceDiffZeroStruct : StructBase
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short Ib1;
        public short K1;
        public short Ib2;
        public short K2;
        private short rez;
    }
}
