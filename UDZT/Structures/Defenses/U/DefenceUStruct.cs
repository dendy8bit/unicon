﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;

namespace BEMN.UDZT.Structures.Defenses.U
{
    public class DefenceUStruct : StructBase 
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short u;
        public short tu;
        private short rez;
    }
}
