﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.UDZT.Structures.Defenses.I
{
    public class AllMtzMainStruct : StructBase, IDgvRowsContainer<MtzMainStruct>
    {
        [Layout(1, Count = 8)] private MtzMainStruct[] _mtzmain;

        [XmlArray(ElementName = "Все_МТЗ")]
        public MtzMainStruct[] Rows
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
    }
}
