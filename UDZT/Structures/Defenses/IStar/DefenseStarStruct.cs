﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BEMN.Devices.Structures;

namespace BEMN.UDZT.Structures.Defenses.IStar
{
    public class DefenseStarStruct :  StructBase
    {
        public short config;
        public short config1;
        public short block;
        public short ust;
        public short time;
        public short k;
        public short u;
        public short tu;
        public short I21;
        private short rez;
    }
}
