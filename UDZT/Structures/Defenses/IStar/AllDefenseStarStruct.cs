﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.UDZT.Structures.Defenses.IStar
{
    public class AllDefenseStarStruct :  StructBase, IDgvRowsContainer<DefenseStarStruct>
    {
        public const int DEF_COUNT = 6;
        [Layout(0, Count = DEF_COUNT)] private DefenseStarStruct[] _mtzmaini0; //мтз I*

        [XmlArray(ElementName = "Все_I*")]
        public DefenseStarStruct[] Rows
        {
            get { return this._mtzmaini0; }
            set { this._mtzmaini0 = value; }
        }
    }
}
