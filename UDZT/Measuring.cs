﻿using System;

namespace BEMN.UDZT
{
    public class Measuring
    {
        public static double GetU(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)value / 0x100 * (double)koeff / 1000;

            return Math.Round(temp, 2);
        }

        public static double GetU(ushort value, double koeff)
        {
            return Math.Floor(value*koeff)/256;
        }

        public static double GetF(ushort value)
        {
            return Math.Round((double) ((double) value/(double) 0x100), 2);
        }

        public static double SetU(double value, ConstraintKoefficient koeff)
        {
            return (double)((double)value * 1000 / (double)koeff) * (double)0x100;
        }

        public static double GetTH(ushort value)
        {
            double temp = (double)value / 256;
            return Math.Round(temp, 2);
        }

        public static double SetTH(double value)
        {
            if (value == 0x100)
                return 0xffff;
            double f = value * 0x100;
            return f;
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)value * (int)koeff / 65535 / 100;
            return Math.Round(temp, 2);
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            ushort t = (ushort)value;
            return (ushort)(value * 65535 * 100 / (int)koeff);
        }


    }
}