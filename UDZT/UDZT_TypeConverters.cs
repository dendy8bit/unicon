using System.ComponentModel;

namespace BEMN.UDZT
{
    public class AllSignalsTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override TypeConverter.StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new TypeConverter.StandardValuesCollection(Strings.All);
        }
    }
    public class BlinkerTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(new string[] { "�����������", "�������" });
        }
    }

    public class ModeLightTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ModesLight);
        }
    }
    public class BlockingTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Blocking);
        }
    }
}
