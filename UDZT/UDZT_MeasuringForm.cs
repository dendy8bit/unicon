using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.UDZT
{
    public partial class MeasuringForm : Form, IFormView
    {
        #region ����������
        private UDZT _device;
        private LedControl[] _diskretLeds;
        private LedControl[] _inputSignalsLeds;
        private LedControl[] _outputSignalsLeds;
        private LedControl[] _idImaxSignalsLeds;
        private LedControl[] _imaxI0maxSignalsLeds;
        private LedControl[] _i0InUmaxSignalsLeds;
        private LedControl[] _qEXTSignalsLeds;
        private LedControl[] _eXTSmeshSignalsLeds;
        private LedControl[] _releSignalsLeds;
        private LedControl[] _indicatorsSignalsLeds;
        private LedControl[] _bugsSignalsLeds;
        private LedControl[] _controlSignalsLeds;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;

        Timer _timer = new Timer();
        #endregion

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._dateTime = device.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                this._device.RefreshAdrSpace1_11();
                this.v_1_11_Panel.Visible = true;
                this.label218.Visible = false;
                this.label217.Visible = false;
                this.label219.Text = "�����������";
                this._Switch2.Visible = false;
                this._Switch3.Visible = false;
                this._qEXTSignalsLeds = new LedControl[]
                {
                    this._Umin1, this._Umin2IO, this._Umin2, this._Umin3IO, this._Umin3, this._Umin4IO, this._Umin4,
                    this._Fmax1IO,
                    this._Fmax1, this._Fmax2IO, this._Fmax2, this._Fmax3IO, this._Fmax3, this._Fmax4IO, this._Fmax4,
                    this._Fmin1IO,
                    this._Fmin1, this._Fmin2IO, this._Fmin2, this._Fmin3IO, this._Fmin3, this._Fmin4IO, this._Fmin4,
                    new LedControl(), new LedControl(), new LedControl(), new LedControl(), 
                    this._EXT1, this._EXT2, this._EXT3, this._EXT4, this._EXT5
                };
                this._eXTSmeshSignalsLeds = new LedControl[]
                {
                    this._EXT6, this._EXT7, this._EXT8, this._EXT9, this._EXT10, this._EXT11, this._EXT12, this._EXT13,
                    this._EXT14, this._EXT15, this._EXT16,
                    this.SFL1, this.SFL2, this.SFL3, this.SFL4, this.SFL5, this.SFL6, this.SFL7, this.SFL8,
                    this.SFL9, this.SFL10, this.SFL11, this.SFL12, this.SFL13, this.SFL14, this.SFL15, this.SFL16,
                    this.SFL17, this.SFL18, this.SFL19, this.SFL20, this.SFL21, this.SFL22, this.SFL23, this.SFL24,
                    this.SFL25, this.SFL26, this.SFL27, this.SFL28, this.SFL29, this.SFL30, this.SFL31, this.SFL32,
                    this._Disrepair, this._GroopMain, this._GroupRez, this._k1, this._Alarm
                };
            }
            else
            {
                this.v_1_11_Panel.Visible = false;
                this.label218.Visible = true;
                this.label217.Visible = true;
                this._Switch2.Visible = true;
                this._Switch3.Visible = true;
                this._qEXTSignalsLeds = new LedControl[]
                {
                    this._Umin1, this._Umin2IO, this._Umin2, this._Umin3IO, this._Umin3, this._Umin4IO, this._Umin4,
                    new LedControl(), new LedControl(), new LedControl(), new LedControl(),
                    this._EXT1, this._EXT2, this._EXT3, this._EXT4, this._EXT5
                };
                this._eXTSmeshSignalsLeds = new LedControl[]
                {
                    this._EXT6, this._EXT7, this._EXT8, this._EXT9, this._EXT10, this._EXT11, this._EXT12, this._EXT13,
                    this._EXT14, this._EXT15, this._EXT16, this._Disrepair, this._GroopMain, this._GroupRez,
                    this._k1, this._Alarm
                };
            }

            this._diskretLeds = new LedControl[]
            {
                this._D1, this._D2, this._D3, this._D4, this._D5, this._D6, this._D7, this._D8,
                this._D9, this._D10, this._D11, this._D12, this._D13, this._D14, this._D15, this._D16,
                this._D17, this._D18, this._D19, this._D20, this._D21, this._D22, this._D23, this._D24
            };
            this._inputSignalsLeds = new LedControl[]
            {
                this._LS1, this._LS2, this._LS3, this._LS4, this._LS5, this._LS6, this._LS7, this._LS8,
                this._LS9, this._LS10, this._LS11, this._LS12, this._LS13, this._LS14, this._LS15, this._LS16
            };
            this._outputSignalsLeds = new LedControl[]
            {
                this._VLS1, this._VLS2, this._VLS3, this._VLS4, this._VLS5, this._VLS6, this._VLS7, this._VLS8,
                this._VLS9, this._VLS10, this._VLS11, this._VLS12, this._VLS13, this._VLS14, this._VLS15, this._VLS16
            };
            this._idImaxSignalsLeds = new LedControl[]
            {
                this._IdMax2Mgn, this._IdMax2IO, this._IdMax2, this._IdMax1IO, this._IdMax1, this._Id0Max1IO,
                this._Id0Max1, this._Id0Max2IO, this._Id0Max2, this._Id0Max3IO, this._Id0Max3, this._I1IO,
                this._I1, this._I2IO, this._I2, this._I3IO
            };
            this._imaxI0maxSignalsLeds = new LedControl[]
            {
                this._I3, this._I4IO, this._I4, this._I5IO, this._I5, this._I6IO, this._I6, this._I7IO, this._I7,
                this._I8IO, this._I8,
                this._I01IO, this._I01, this._I02IO, this._I02, this._I03IO
            };
            this._i0InUmaxSignalsLeds = new LedControl[]
            {
                this._I03, this._In1IO, this._In1, this._In2IO, this._In2, this._In3IO, this._In3, this._Umax1IO,
                this._Umax1, this._Umax2IO, this._Umax2, this._Umax3IO, this._Umax3, this._Umax4IO, this._Umax4,
                this._Umin1IO
            };

            this._releSignalsLeds = new LedControl[]
            {
                new LedControl(), new LedControl(), this._k2, this._Rele1, this._Rele2, this._Rele3, this._Rele4, this._Rele5,
                this._Rele6, this._Rele7, this._Rele8, this._Rele9, this._Rele10, this._Rele11, this._Rele12,
                this._Rele13
            };
            this._indicatorsSignalsLeds = new LedControl[]
            {
                this._Rele14, this._Rele15, this._Rele16, this._Rele17, this._Rele18, this._Ind1, this._Ind2, this._Ind3,
                this._Ind4, this._Ind5, this._Ind6, this._Ind7, this._Ind8, this._Ind9, this._Ind10, this._Ind11
            };

            this._controlSignalsLeds = new LedControl[]
            {
                this._Ind12, this.indJourSysLed, this.indJourAlarmLed, this.BDC_MESS_SYS, this.BDC_MESS_ALARM,
                this.BDC_MESS_OSC, this.BDC_MESS_DISPSYS, this.indDefectRelay, this.SWITCH_OFF, this.SWITCH_ON
            };

            this._bugsSignalsLeds = new LedControl[]
            {
                this._Hard, this._Soft, this._Meas, this._Switch1, this._Switch2, this._Switch3, this._Contr, this._Mod1,
                this._Mod2, this._Mod3, this._Mod4, this._Mod5, this._Ust, this._Groupust, this._Passwust, this._Jsys,
                this._Jalm, this._Osc
            };
            if (Common.VersionConverter(this._device.DeviceVersion) < 2.08)
            {
                this.KgroupBox.Visible = false;
            }

            this._device.AnalogBDLoadOK += HandlerHelper.CreateHandler(this, this.OnAnalogBDLoadOk);
            this._device.AnalogBDLoadFail += HandlerHelper.CreateHandler(this, this.OnAnalogBDLoadFail);
            this._device.BitBDLoadOK += HandlerHelper.CreateHandler(this, this.OnBitBDLoadOK);
            this._device.BitBDLoadFail += HandlerHelper.CreateHandler(this, this.OnBitBDLoadFail);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(UDZT); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        /// <summary>
        /// ���������� �����
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._device.MakeAnalogBDQuick();
            this._device.MakeBitBDQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._device.MakeAnalogBDSlow();
            this._device.MakeBitBDSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveAnalogBD();
            this._device.RemoveBitBD();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.LoadMeasureTrans();
                this._device.LoadPowerTrans();
                this._device.LoadAnalogBDCycle();
                this._device.LoadBitBDCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._device.RemoveAnalogBD();
                this._device.RemoveBitBD();
                this._dateTime.RemoveStructQueries();
                this.OnAnalogBDLoadFail();
                this.OnBitBDLoadFail();
            }
        }

        private void OnBitBDLoadFail()
        {
            LedManager.TurnOffLeds(this._diskretLeds);
            LedManager.TurnOffLeds(this._inputSignalsLeds);
            LedManager.TurnOffLeds(this._outputSignalsLeds);
            LedManager.TurnOffLeds(this._idImaxSignalsLeds);
            LedManager.TurnOffLeds(this._imaxI0maxSignalsLeds);
            LedManager.TurnOffLeds(this._i0InUmaxSignalsLeds);
            LedManager.TurnOffLeds(this._qEXTSignalsLeds);
            LedManager.TurnOffLeds(this._eXTSmeshSignalsLeds);
            LedManager.TurnOffLeds(this._releSignalsLeds);
            LedManager.TurnOffLeds(this._indicatorsSignalsLeds);
            LedManager.TurnOffLeds(this._bugsSignalsLeds);
            LedManager.TurnOffLeds(this._controlSignalsLeds);

            this._bdGroupMain.State = this._GroopMain.State;
            this._bdGroupRez.State = this._GroupRez.State;
            this._logicState.State = LedState.Off;
        }

        private void OnBitBDLoadOK()
        {
            LedManager.SetLeds(this._diskretLeds, this._device.DiskretSignals);
            LedManager.SetLeds(this._inputSignalsLeds, this._device.LSSignals);
            LedManager.SetLeds(this._outputSignalsLeds, this._device.VLSSignals);
            LedManager.SetLeds(this._idImaxSignalsLeds, this._device.IdImaxSignals);
            LedManager.SetLeds(this._imaxI0maxSignalsLeds, this._device.ImaxI0Signals);
            LedManager.SetLeds(this._i0InUmaxSignalsLeds, this._device.I0InUmaxSignals);
            LedManager.SetLeds(this._qEXTSignalsLeds, this._device.QEXTSignals);
            LedManager.SetLeds(this._eXTSmeshSignalsLeds, this._device.EXTSmeshSignals);
            LedManager.SetLeds(this._releSignalsLeds, this._device.ReleSignals);
            LedManager.SetLeds(this._indicatorsSignalsLeds, this._device.IndSignals);
            LedManager.SetLeds(this._bugsSignalsLeds, this._device.BugsSignals);
            LedManager.SetLeds(this._controlSignalsLeds,this._device.ControlSignals);

            this._bdGroupMain.State = this._GroopMain.State;
            this._bdGroupRez.State = this._GroupRez.State;
            
            bool enableLogic = this._device.ControlSignals[10] && !this._device.FaultLogicError;
            this._logicState.State = enableLogic ? LedState.NoSignaled : LedState.Signaled;
        }

        private void OnAnalogBDLoadFail()
        {
            #region ���������
            //�� ����� �� ��������� �����

            this._Ida.Text = "XXXXXX";
            this._Idb.Text = "XXXXXX";
            this._Idc.Text = "XXXXXX";

            this._percentIdaFromIbas.Text = "XXXXXX";
            this._percentIdbFromIbas.Text = "XXXXXX";
            this._percentIdcFromIbas.Text = "XXXXXX";

            //_Id2a.Text = "XXXXXX";
            //_Id2b.Text = "XXXXXX";
            //_Id2c.Text = "XXXXXX";

            //_Id5a.Text = "XXXXXX";
            //_Id5b.Text = "XXXXXX";
            //_Id5c.Text = "XXXXXX"; 
            #endregion

            this._Iba.Text = "XXXXXX";
            this._Ibb.Text = "XXXXXX";
            this._Ibc.Text = "XXXXXX";

            this._percentIbaFromIbas.Text = "XXXXXX";
            this._percentIbbFromIbas.Text = "XXXXXX";
            this._percentIbcFromIbas.Text = "XXXXXX";

            this._Is1a.Text = "XXXXXX";
            this._Is1b.Text = "XXXXXX";
            this._Is1c.Text = "XXXXXX";
            this._Is1n.Text = "XXXXXX";
            this._Is10.Text = "XXXXXX";
            this._Is12.Text = "XXXXXX";

            this._Is2a.Text = "XXXXXX";
            this._Is2b.Text = "XXXXXX";
            this._Is2c.Text = "XXXXXX";
            this._Is2n.Text = "XXXXXX";
            this._Is20.Text = "XXXXXX";
            this._Is22.Text = "XXXXXX";

            this._Is3a.Text = "XXXXXX";
            this._Is3b.Text = "XXXXXX";
            this._Is3c.Text = "XXXXXX";
            this._Is3n.Text = "XXXXXX";
            this._Is30.Text = "XXXXXX";
            this._Is32.Text = "XXXXXX";

            this._Uab.Text = "XXXXXX";
            this._Ubc.Text = "XXXXXX";
            this._Uca.Text = "XXXXXX";
            this._U0.Text = "XXXXXX";
            this._U2.Text = "XXXXXX";

            this._Ia.Text = "XXXXXX";
            this._Ib.Text = "XXXXXX";
            this._Ic.Text = "XXXXXX";
            this._I0.Text = "XXXXXX";

            this._I2a.Text = "XXXXXX";
            this._I2b.Text = "XXXXXX";
            this._I2c.Text = "XXXXXX";
            this._I20.Text = "XXXXXX";

            this._I3a.Text = "XXXXXX";
            this._I3b.Text = "XXXXXX";
            this._I3c.Text = "XXXXXX";
            this._I30.Text = "XXXXXX";

            this._Ua.Text = "XXXXXX";
            this._Ub.Text = "XXXXXX";
            this._Uc.Text = "XXXXXX";
            this._Un.Text = "XXXXXX";

            this._frequency.Text = "XXXXXX";
        }

        private void OnAnalogBDLoadOk()
        {
            #region ���������
            //�� ����� �� ��������� �����

            this._Ida.Text = this._device.Ida;
            this._Idb.Text = this._device.Idb;
            this._Idc.Text = this._device.Idc;

            this._percentIdaFromIbas.Text = this._device.PercentIda;
            this._percentIdbFromIbas.Text = this._device.PercentIdb;
            this._percentIdcFromIbas.Text = this._device.PercentIdc;
            #endregion

            this._Iba.Text = this._device.Iba;
            this._Ibb.Text = this._device.Ibb;
            this._Ibc.Text = this._device.Ibc;

            this._percentIbaFromIbas.Text = this._device.PercentIba;
            this._percentIbbFromIbas.Text = this._device.PercentIbb;
            this._percentIbcFromIbas.Text = this._device.PercentIbc;

            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                this._Is1a.Text = this.Znak(this._device.ZnakiPart1, 2) + this._device.Is1a;
                this._Is1b.Text = this.Znak(this._device.ZnakiPart1, 4) + this._device.Is1b;
                this._Is1c.Text = this.Znak(this._device.ZnakiPart1, 6) + this._device.Is1c;
                this._Is1n.Text = this.Znak(this._device.ZnakiPart1, 0) + this._device.Is1n;
                this._Is10.Text = this.Znak(this._device.ZnakiPart1, 8) + this._device.Is10;
                this._Is12.Text = this.Znak(this._device.ZnakiPart1, 10) + this._device.Is12;
                //12 - 10

                this._Is2a.Text = this.Znak(this._device.ZnakiPart1, 14) + this._device.Is2a;
                this._Is2b.Text = this.Znak(this._device.ZnakiPart1, 16) + this._device.Is2b;
                this._Is2c.Text = this.Znak(this._device.ZnakiPart1, 18) + this._device.Is2c;
                this._Is2n.Text = this.Znak(this._device.ZnakiPart1, 12) + this._device.Is2n;
                this._Is20.Text = this.Znak(this._device.ZnakiPart1, 20) + this._device.Is20;
                this._Is22.Text = this.Znak(this._device.ZnakiPart1, 22) + this._device.Is22;
                //22 - 22

                this._Is3a.Text = this.Znak(this._device.ZnakiPart1, 26) + this._device.Is3a;
                this._Is3b.Text = this.Znak(this._device.ZnakiPart1, 28) + this._device.Is3b;
                this._Is3c.Text = this.Znak(this._device.ZnakiPart1, 30) + this._device.Is3c;
                this._Is3n.Text = this.Znak(this._device.ZnakiPart1, 24) + this._device.Is3n;
                this._Is30.Text = this.Znak(this._device.ZnakiPart1, 32) + this._device.Is30;
                this._Is32.Text = this.Znak(this._device.ZnakiPart1, 34) + this._device.Is32;
            }
            else
            {
                this._Is1a.Text = this.Znak(this._device.ZnakiPart1, 0) + this._device.Is1a;
                this._Is1b.Text = this.Znak(this._device.ZnakiPart1, 2) + this._device.Is1b;
                this._Is1c.Text = this.Znak(this._device.ZnakiPart1, 4) + this._device.Is1c;
                this._Is1n.Text = this.Znak(this._device.ZnakiPart1, 6) + this._device.Is1n;
                this._Is10.Text = this.Znak(this._device.ZnakiPart1, 8) + this._device.Is10;

                this._Is2a.Text = this.Znak(this._device.ZnakiPart1, 10) + this._device.Is2a;
                this._Is2b.Text = this.Znak(this._device.ZnakiPart1, 12) + this._device.Is2b;
                this._Is2c.Text = this.Znak(this._device.ZnakiPart1, 14) + this._device.Is2c;
                this._Is2n.Text = this.Znak(this._device.ZnakiPart1, 16) + this._device.Is2n;
                this._Is20.Text = this.Znak(this._device.ZnakiPart1, 18) + this._device.Is20;

                this._Is3a.Text = this.Znak(this._device.ZnakiPart1, 20) + this._device.Is3a;
                this._Is3b.Text = this.Znak(this._device.ZnakiPart1, 22) + this._device.Is3b;
                this._Is3c.Text = this.Znak(this._device.ZnakiPart1, 24) + this._device.Is3c;
                this._Is3n.Text = this.Znak(this._device.ZnakiPart1, 26) + this._device.Is3n;
                this._Is30.Text = this.Znak(this._device.ZnakiPart1, 28) + this._device.Is30;
            }
            this._Uab.Text = this._device.Uab;
            this._Ubc.Text = this._device.Ubc;
            this._Uca.Text = this._device.Uca;
            this._U0.Text = this._device.U0;
            this._U2.Text = this._device.U2;

            this._Ia.Text = this._device.Ia;
            this._Ib.Text = this._device.Ib;
            this._Ic.Text = this._device.Ic;
            this._I0.Text = this._device.I0;

            this._I2a.Text = this._device.I2a;
            this._I2b.Text = this._device.I2b;
            this._I2c.Text = this._device.I2c;
            this._I20.Text = this._device.I20;

            this._I3a.Text = this._device.I3a;
            this._I3b.Text = this._device.I3b;
            this._I3c.Text = this._device.I3c;
            this._I30.Text = this._device.I30;

            this._Ua.Text = this._device.Ua;
            this._Ub.Text = this._device.Ub;
            this._Uc.Text = this._device.Uc;
            this._Un.Text = this._device.Un;

            this._frequency.Text = this._device.F;
        }

        private string Znak(BitArray BdZnakov, int ZnakIndex) 
        {
            string rez = "";
            if (!BdZnakov[ZnakIndex + 1]) 
            {
                if (!BdZnakov[ZnakIndex])
                    rez = "+";
                else
                    rez = "-";
            }
            return rez;
        }
        
        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������-��801", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber,this._device);
            }
        }

        private void GROOPMAIN_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D09, "����������� �� �������� ������");
        }

        private void GROOPREZERVE_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D0A, "����������� �� ��������� ������");
        }

        private void ResetJS_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D01, "�������� ������ �������");
        }

        private void ResetJA_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D02, "�������� ������ ������");
        }

        private void ResetJO_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D03, "�������� ������ ������������");
        }

        private void ResetJS_Error_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D04, "�������� ������� ������������� �� ��");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D05, "�������� ���������");
        }

        private void SwitchOn_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D0C, "�������� �����������");
        }

        private void SwitchOff_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x0D0B, "��������� �����������");
        }

        private void _dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }
        
        private void startLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D10, true, "������ ���", this._device);
        }

        private void stopLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0F, true, "������� ���", this._device);
        }
    }
}