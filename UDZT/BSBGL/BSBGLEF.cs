using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Compressor;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BMTCD.HelperClasses;
using BMTCD.UDZT;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using Crownwood.Magic.Menus;
using SchemeEditorSystem;

namespace BEMN.UDZT.BSBGL
{
    public partial class BSBGLEF :  Form, IFormView
    {
        #region Variables

        private const string MR801_NAME = "MR801";
        private const int COUNT_EXCHANGES_PROGRAM = 64; //��� ���������� �������� �����
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private MemoryEntity<ProgramPageStruct> _currentProgramPage;
        private MessageBoxForm _formCheck;
        private UDZT _device;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private Mr801Compiler _compiller;
        private bool _isOnSimulateMode;
        private LibraryBox _libraryWindow;
	    private OutputWindow _outputWindow;
		private ToolStrip _mainToolStrip;
        private Content _libraryContent;
	    private Content _outputContent;
		private bool _isSaveLogic;
        private ushort[] _binFile;
        private ushort[] _binFile1;
        private ushort _pageIndex;
        private VisualStyle _style;
        private StatusBar _statusBar;
        private Crownwood.Magic.Controls.TabControl _filler;
        private ToolStrip MainToolStrip;
        private ToolStripButton newToolStripButton;
        private ToolStripButton openToolStripButton;
        private ToolStripButton openProjToolStripButton;
        private ToolStripButton saveToolStripButton;
        private ToolStripButton saveProjToolStripButton;
        private ToolStripButton printToolStripButton;
        private ToolStripButton undoToolStripButton;
        private ToolStripButton redoToolStripButton;
        private ToolStripButton cutToolStripButton;
        private ToolStripButton copyToolStripButton;
        private ToolStripButton pasteToolStripButton;
        private ToolStripButton CompilToolStripButton;
        private ToolStripButton StopToolStripButton;
        private ToolStripButton openFromDeviceToolStripButton;
        private ToolStripButton drawConnectionBtn;
	    private ToolStripButton CompilOfflineToolStripButton;

		#endregion

		#region Constructor
		public BSBGLEF()
        {
            this.InitForm();
        }
        public BSBGLEF(BEMN.UDZT.UDZT device)
        {
            this._device = device;
            this.InitForm();
            double vers = Common.VersionConverter(this._device.DeviceVersion);
            this._compiller = new Mr801Compiler(this._device.DeviceType, vers);
            this._currentSourceProgramStruct = this._device.SourceProgramStruct;
            this._currentStartProgramStruct = this._device.ProgramStartStruct;
            this._currentSignalsStruct = this._device.ProgramSignalsStruct;
            this._currentProgramPage = this._device.ProgramPage;
            //�������� ���������
            this._device.ProgramPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveOk);
            this._device.ProgramPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveFail);
            //���������
            this._device.SourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteOk);
            this._device.SourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteFail);
            this._device.SourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructReadOk);
            this._device.SourceProgramStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructReadFail);
            this._device.SourceProgramStruct.ReadOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructReadFail();
            });
            this._device.SourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructWriteFail();
            });
            //����� ���������
            this._device.ProgramStartStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveOk);
            this._device.ProgramStartStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveFail);
            this._device.StopSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
               MessageBox.Show("���������� �������� ��������������� ������ �����������", "������� ���", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.StopSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� �������", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.StartSpl.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.StateSpl.LoadStruct);
            this._device.StartSpl.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� �������", "", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.StateSpl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                bool res = Common.GetBit(this._device.StateSpl.Value.Word, 6) || Common.GetBit(this._device.StateSpl.Value.Word, 7)
                           || Common.GetBit(this._device.StateSpl.Value.Word, 8) || Common.GetBit(this._device.StateSpl.Value.Word, 9)
                           || Common.GetBit(this._device.StateSpl.Value.Word, 10);
                if (res)
                {
                    MessageBox.Show("��������� ������: ������", "������ ������", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("��������� ������: ��������", "������ ������", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            });
            this._device.StateSpl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("���������� ��������� ��������� ������", "������ ������", MessageBoxButtons.OK, MessageBoxIcon.Error));
            //�������� ��������
            this._device.ProgramSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignalsLoadOk);
            this._device.ProgramSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadFail);
        }
        
        void InitForm()
        {
            this.InitializeComponent();
            this._style = VisualStyle.IDE;
            this._manager = new DockingManager(this,this._style);
            this._newForm = new NewSchematicForm();
            this._formCheck = new MessageBoxForm();
        } 
        #endregion

        #region DeviceEventHandlers
        private void ProgramPageSaveFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (Exception)
            {
            }
        }
        private void ProgramPageSaveOk()
        {
            try
            {
                if (this._pageIndex >= 4) return;
                if (this._isSaveLogic)
                {
                    SourceProgramStruct ps = new SourceProgramStruct();
                    ushort[] program = new ushort[1024];
                    Array.ConstrainedCopy(this._binFile, this._pageIndex * 1024, program, 0, 1024);
                    ps.InitStruct(Common.TOBYTES(program, false));
                    this._currentSourceProgramStruct.Value = ps;
                    this._currentSourceProgramStruct.SaveStruct();
                }
                else
                {
                    this._currentSourceProgramStruct.LoadStruct();
                }
            }
            catch
            { }
        }
        private void ExchangeOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(() => this._formCheck.ProgramExchangeOk()));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramStructWriteFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramSaveFail()
        {
            if (this._isSaveLogic)
            {
	            this.OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
				this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
                this.OnStop();
            }
            else
            {
	            this.OutMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
				this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
            }
        }
        private void ProgramStructReadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramReadFail));
            }
            catch (InvalidOperationException)
            { }
        }
        private void ProgramReadFail()
        {
	        this.OutMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
			this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_UPLOADING_PROGRAM_IN_DEVICE);
        }

        private void ProgramStructWriteOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.WriteOk));
            }
            catch (InvalidOperationException)
            { }
        }


        private void StartLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.StartSpl.Value.Word = 0x00FF;
            this._device.StartSpl.SaveStruct5();
        }

        private void StopLogicProgram(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.StopSpl.Value.Word = 0x00FF;
            this._device.StopSpl.SaveStruct5();
        }


        private void WriteOk()
        {
            this._pageIndex++;
            if (this._pageIndex < 4)
            {
                ushort[] values = new ushort[1];
                values[0] = this._pageIndex;
                ProgramPageStruct pp = new ProgramPageStruct();
                pp.InitStruct(Common.TOBYTES(values, false));
                this._currentProgramPage.Value = pp;
                this._currentProgramPage.SaveStruct();
            }
            else
            {
                this._formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
                var values = new ushort[1];
                values[0] = 0x00FF;
                StartStruct ss = new StartStruct();
                ss.InitStruct(Common.TOBYTES(values, false));
                this._currentStartProgramStruct.Value = ss;
                this._currentStartProgramStruct.SaveStruct5();
            }
        }
        private void ProgramStructReadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ReadOk()
        {
            try
            {
                ushort[] program = this._currentSourceProgramStruct.Value.ProgramStorage;
                Array.ConstrainedCopy(program, 0, this._binFile1, this._pageIndex * 1024, 1024);
                this._pageIndex++;
                if (this._pageIndex < 4)
                {
                    ushort[] values = new ushort[1];
                    values[0] = this._pageIndex;
                    ProgramPageStruct pp = new ProgramPageStruct();
                    pp.InitStruct(Common.TOBYTES(values, false));
                    this._currentProgramPage.Value = pp;
                    this._currentProgramPage.SaveStruct();
                }
                else
                {
                    BSBGLTab tab = new BSBGLTab();
                    this._compiller.ResetCompiller();
                    tab.InitializeBSBGLSheet("MR801Logic", SheetFormat.A0_P, this._device, this._manager);
                    this._compiller.SetSchemeOnBin(tab.Schematic, this._binFile1);
                    this._compiller.SaveLogicInFile(Path.GetDirectoryName(Application.ExecutablePath) +
                        string.Format("\\LogicProg_{0}_v{1}.asm", this._device.DeviceType,this._device.DeviceVersion));
                    this._filler.TabPages.Add(tab);
                    this._formCheck.ShowResultMessage("��������� ��������� �������");
                }
            }
            catch (Exception e)
            {
                this._formCheck.Fail = true;
                this._formCheck.ShowResultMessage(e.Message);
            }
        }

        private void SignalsLoadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramSignalsLoadOk()
        {
            if (this._isOnSimulateMode)
            {
                this._compiller.UpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
	            this.OutMessage(InformationMessages.VARIABLES_UPDATED);
			}
        }

        private void ProgramStartSaveFail()
        {
	        this.OutMessage(InformationMessages.ERROR_PROGRAM_START);
			this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            this.OnStop();
        }

        private void ProgramSignalsLoadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.SignalsLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }

        void SignalsLoadFail()
        {
            //������������� ���������
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
	        if (this._isOnSimulateMode)
	        {
		        this.OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
	        }
		}

        private void ProgramStartSaveOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.StartSaveOk));
            }
            catch (InvalidOperationException) { }
        }

        private void StartSaveOk()
        {
	        this.OutMessage(InformationMessages.PROGRAM_START_OK);
			ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0x4100);
            this._currentSignalsStruct.LoadStructCycle();
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
        }

        private void OnFileOpenFromDevice(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� ���������� ��������� �� ����������?", "", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) != DialogResult.OK) return;
            this._isSaveLogic = false;
            this._pageIndex = 0;
            this._binFile1 = new ushort[4 * 1024];
            ushort[] values = new ushort[1];
            values[0] = this._pageIndex;
            ProgramPageStruct pp = new ProgramPageStruct();
            pp.InitStruct(Common.TOBYTES(values, false));
            this._currentProgramPage.Value = pp;
            this._currentProgramPage.SaveStruct();
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowDialog("�������� ���������� ��������� �� ����������");
        }

        private void OnEditDrawLines(object sender, EventArgs e)
        {
            if (!(this._filler.SelectedTab is BSBGLTab)) return;
            BSBGLTab sTab = (BSBGLTab)this._filler.SelectedTab;
            sTab.Schematic.DrawLines();
        }
        #endregion

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            this._filler = new Crownwood.Magic.Controls.TabControl
            {
                //��������� ��������� ��������� ���� ������������, ������� ����� ������������� ���������
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = this._style,
                IDEPixelBorder = true
            };
            this.Controls.Add(this._filler);
            this._filler.ClosePressed += this.OnFileClose;    
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            this._manager = new DockingManager(this, this._style) {InnerControl = this._filler};
            // Create and setup the StatusBar object
            this._statusBar = new StatusBar {Dock = DockStyle.Bottom, ShowPanels = true};
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel {AutoSize = StatusBarPanelAutoSize.Spring};
            this._statusBar.Panels.Add(statusBarPanel);
            this.Controls.Add(this._statusBar);
            this._mainToolStrip = this.CreateToolStrip();
            this.Controls.Add(this._mainToolStrip);
            this.CreateMenus();
            // Ensure that docking occurs after the menu control and status bar controls
            this._manager.OuterControl = this._statusBar;
	        this.CreateOutputWindow();
			this.CreateLibraryWindow();
            this.Width = 800;
            this.Height = 600;
        }

		private void OutMessage(string str)
		{
			this._outputWindow.AddMessage(str + "\r\n");
		}

		private void DefineContentState(Content c)
        {
            c.CaptionBar = true;
            c.CloseButton = true;
            //c.DisplaySize = new Size(200,100);
            //c.FloatingSize = new Size(200,400);
        }

		#region Docking Forms Code

		private void CreateOutputWindow()
		{
			this._outputWindow = new OutputWindow();
			this._outputContent = this._manager.Contents.Add(this._outputWindow, "���������");
			this.DefineContentState(this._outputContent);
			this._manager.AddContentWithState(this._outputContent, State.DockBottom);
		}

		private void CreateLibraryWindow()
        {
            this._libraryWindow = new LibraryBox(Resources.BlockLib);
            this._libraryContent = this._manager.Contents.Add(this._libraryWindow, "����������");
            this.DefineContentState(this._libraryContent);
            this._manager.AddContentWithState(this._libraryContent, State.DockRight);
        }

        #endregion
   
        #region Toolbox 
        private ToolStrip CreateToolStrip()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(BSBGLEF));
            List<System.Windows.Forms.ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 9;i++ )
            {
                ToolStripSeparator tls = new ToolStripSeparator();
                if (i >= 6) tls.Margin = new Padding(20, 0, 0, 0);
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator"+i;
                tls.Size = new System.Drawing.Size(6, 25);
            }

            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFromDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.drawConnectionBtn = new ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
            
            this.undoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.redoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            
            this.CompilToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.StopToolStripButton = new System.Windows.Forms.ToolStripButton();

	        this.CompilOfflineToolStripButton = new ToolStripButton();

			ToolStripButton startLogicBtn = new ToolStripButton()
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.startSpl,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "��������� ��� � ����������",
            };
            startLogicBtn.Click += this.StartLogicProgram;

            ToolStripButton stopLogicBtn = new ToolStripButton()
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                Image = Resources.stopSpl,
                ImageTransparentColor = System.Drawing.Color.Magenta,
                Size = new System.Drawing.Size(23, 22),
                Text = "���������� ���������� ��� � ����������",
            };
            stopLogicBtn.Click += this.StopLogicProgram;


            this.MainToolStrip.Items.Add(this.newToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[0]);
            this.MainToolStrip.Items.Add(this.openToolStripButton);
            this.MainToolStrip.Items.Add(this.openProjToolStripButton);
            this.MainToolStrip.Items.Add(this.drawConnectionBtn);
            this.MainToolStrip.Items.Add(toolStripSepList[1]);
            this.MainToolStrip.Items.Add(this.saveToolStripButton);
            this.MainToolStrip.Items.Add(this.saveProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[2]);
            this.MainToolStrip.Items.Add(this.printToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[3]);
            this.MainToolStrip.Items.Add(this.undoToolStripButton);
            this.MainToolStrip.Items.Add(this.redoToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[4]);
            this.MainToolStrip.Items.Add(this.cutToolStripButton);
            this.MainToolStrip.Items.Add(this.copyToolStripButton);
            this.MainToolStrip.Items.Add(this.pasteToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[5]);
            this.MainToolStrip.Items.Add(this.CompilToolStripButton);
            this.MainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[6]);
            this.MainToolStrip.Items.Add(this.StopToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[7]);
            this.MainToolStrip.Items.Add(startLogicBtn);
            this.MainToolStrip.Items.Add(stopLogicBtn);
	        this.MainToolStrip.Items.Add(toolStripSepList[8]);
	        this.MainToolStrip.Items.Add(this.CompilOfflineToolStripButton);

			this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(816, 25);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "toolStrip1";

            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filenew");
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "����� �����";
            this.newToolStripButton.Click += this.OnFileNew;

            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = (System.Drawing.Image)resources.GetObject("fileopen");
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "������� ��������";
            this.openToolStripButton.Click += this.OnFileOpen;

            this.openProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = (System.Drawing.Image)resources.GetObject("fileopenproject");
            this.openProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openProjToolStripButton.Text = "������� ������";
            this.openProjToolStripButton.Click += this.OnFileOpenProject;

            this.openFromDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openFromDeviceToolStripButton.Image = (System.Drawing.Image)resources.GetObject("openfromdevice");
            this.openFromDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openFromDeviceToolStripButton.Name = "openFromDeviceToolStripButton";
            this.openFromDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openFromDeviceToolStripButton.Text = "��������� ���������� ��������� �� ����������";
            this.openFromDeviceToolStripButton.Click += this.OnFileOpenFromDevice;

            this.drawConnectionBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.drawConnectionBtn.Image = Resources.Connect;
            this.drawConnectionBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.drawConnectionBtn.Name = "drawConnectionBtn";
            this.drawConnectionBtn.Size = new System.Drawing.Size(23, 22);
            this.drawConnectionBtn.Text = "���������� �����";
            this.drawConnectionBtn.Click += this.OnEditDrawLines;

            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filesave");
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "��������� ��������";
            this.saveToolStripButton.Click += this.OnFileSave;

            this.saveProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = (System.Drawing.Image)resources.GetObject("filesaveproject");
            this.saveProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveProjToolStripButton.Text = "��������� ������";
            this.saveProjToolStripButton.Click += this.OnFileSaveProject;

            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = (System.Drawing.Image)resources.GetObject("print");
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "������";
            this.printToolStripButton.Click += this.printToolStripButton_Click;

            this.undoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = (System.Drawing.Image)resources.GetObject("undo");
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.undoToolStripButton.Text = "������";
            this.undoToolStripButton.Click+=this.OnUndo; 

            this.redoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = (System.Drawing.Image)resources.GetObject("redo");
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.redoToolStripButton.Text = "������������ ���������";
            this.redoToolStripButton.Click+=this.OnRedo;

            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = (System.Drawing.Image)resources.GetObject("cut");
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "��������";
            this.cutToolStripButton.Click += this.OnEditCut;

            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = (System.Drawing.Image)resources.GetObject("copy");
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "����������";
            this.copyToolStripButton.Click +=this.OnEditCopy;

            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = (System.Drawing.Image)resources.GetObject("paste");
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "��������";
            this.pasteToolStripButton.Click +=this.OnEditPaste;
            
            this.CompilToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompilToolStripButton.Image = (System.Drawing.Image)resources.GetObject("go");
            this.CompilToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompilToolStripButton.Name = "CompilToolStripButton";
            this.CompilToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CompilToolStripButton.Text = "��������� ���������� ��������� � ����������, ��������� ��������";
            this.CompilToolStripButton.Click += this.OnCompile;

            this.StopToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = (System.Drawing.Image)resources.GetObject("stop");
            this.StopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StopToolStripButton.Text = "��������� ��������";
            this.StopToolStripButton.Click += this.OnStop;

	        this.CompilOfflineToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
	        this.CompilOfflineToolStripButton.Image = Resources.calculator.ToBitmap();
	        this.CompilOfflineToolStripButton.ImageTransparentColor = Color.Magenta;
	        this.CompilOfflineToolStripButton.Name = "CompilOfflineToolStripButton";
	        this.CompilOfflineToolStripButton.Size = new Size(23, 22);
	        this.CompilOfflineToolStripButton.Text = "�������� ���������� �����";
	        this.CompilOfflineToolStripButton.Click += this.OnCompileOffline;

			return this.MainToolStrip;
        }

  
        #endregion

        #region MAIN MENU
        private void CreateMenus()
        {
            MenuControl topMenu = new MenuControl {Style = this._style, MultiLine = false};
            MenuCommand topFile = new MenuCommand("&����");
            MenuCommand topEdit = new MenuCommand("������");
            MenuCommand topView = new MenuCommand("���");
            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView});
            //File
            MenuCommand topFileNew   = new MenuCommand("�����", this.OnFileNew);
            MenuCommand topFileOpen  = new MenuCommand("������� ��������", this.OnFileOpen);
            MenuCommand topFileOpenProject = new MenuCommand("������� ������", this.OnFileOpenProject);
            MenuCommand topFileClose = new MenuCommand("������� ��������", this.OnFileClose);
            MenuCommand topFileCloseProject = new MenuCommand("������� ������", this.OnFileCloseProject);
            MenuCommand topFileSave  = new MenuCommand("��������� ��������", this.OnFileSave);
            MenuCommand topFileSaveProject = new MenuCommand("��������� ������", this.OnFileSaveProject);
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("����������", this.OnEditCopy);
            MenuCommand topEditPaste = new MenuCommand("��������", this.OnEditPaste);
            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste});
            //View
            MenuCommand topViewToolWindow = new MenuCommand("����������", this.OnViewToolWindow);
            topView.MenuCommands.Add(topViewToolWindow);
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
        }

        private void OnFileNew(object sender, EventArgs e)
        {
            this._newForm.ShowDialog();
            if (DialogResult.OK == this._newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this._newForm.NameOfSchema, this._newForm.sheetFormat, this._device, this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
	            this.OutMessage("������ ����� ���� �����: " + this._newForm.NameOfSchema);
				myTab.Schematic.Focus();
            }
        }
        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            this.pageSetupDialog.PageSettings = new System.Drawing.Printing.PageSettings();
            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            this.pageSetupDialog.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
            //Do not show the network in the printer dialog.
            this.pageSetupDialog.ShowNetwork = false;
            //Show the dialog storing the result.
            DialogResult result = this.pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = this.pageSetupDialog.PageSettings;
                this.printPreviewDialog.Document = this.printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                this.printPreviewDialog.ShowDialog();
                DialogResult result1 = this.printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    this.printDocument.Print();
                }
            }
        }
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                float scale = (float)e.PageBounds.Width / (float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics,scale);
            }           
        }

        private void OnUndo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        private void OnRedo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        private void OnEditCut(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        private void OnEditCopy(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        private void OnEditPaste(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)this._filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }
        
        private void OnFileOpen(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this.openFileDialog.FileName, SheetFormat.A0_L, this._device, this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
         
                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
	            this.OutMessage("�������� ���� �����.");
				myTab.Schematic.Focus();
            }
        }
        private void OnFileOpenProject(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (this._filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("��������� ������� ������ �� ����� ?", 
                        "�������� �������", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (this.SaveProjectDoc())
                            {
                                this._filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No: this._filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }
                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        string name = reader.GetAttribute("name") ?? reader.GetAttribute("pinName");
                        myTab.InitializeBSBGLSheet(name, SheetFormat.A0_L, this._device, this._manager);
                        myTab.Selected = true;
                        this._filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
	            this.OutMessage("�������� ������.");
				reader.Close();
            }
        }

        private void OnFileClose(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch(MessageBox.Show("��������� �������� �� ����� ?", "�������� ���������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    {
                        if (SaveActiveDoc())
                        {
                            BSBGLTab tab = (BSBGLTab)_filler.SelectedTab;
                            _filler.TabPages.Remove(tab);
                            tab.Dispose();
                        }
                        break;
                    }
                case DialogResult.No:
                    {
                        BSBGLTab tab = (BSBGLTab)_filler.SelectedTab;
                        _filler.TabPages.Remove(tab);
                        tab.Dispose();
                        break;
                    }
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileCloseProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� ������ �� ����� ?", "�������� �������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    this.SaveProjectDoc();                    
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();                    
                    break;
                case DialogResult.No:
                {
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();
                }
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        private void OnFileSave(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveActiveDoc();
        }
        private void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            this.SaveProjectDoc();
        }
        private void OnViewToolWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._libraryContent);
        }

        private void CompileProject()
        {
			//if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
			//if (MessageBox.Show("�������� ���������� ��������� � ����������?", "", MessageBoxButtons.OKCancel,
			//    MessageBoxIcon.Question) != DialogResult.OK) return;

			//this._compiller.ResetCompiller();
			//try
			//{
			//    foreach (BSBGLTab src in this._filler.TabPages)
			//    {
			//        this._compiller.AddSource(src.TabName, src.Schematic);
			//    }
			//}
			//catch (Exception exc)
			//{
			//    MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
			//    this.OnStop();
			//    return;
			//}
			//this._binFile = this._compiller.Make();
			//if (this._compiller.BinarySize > 4096)
			//{
			//    MessageBox.Show("��������� ������� ������ ! ", "������ ����������", MessageBoxButtons.OK);
			//    this.OnStop();
			//    return;
			//}
			//if (this._binFile.Length == 0)
			//{
			//    this.OnStop();
			//    return;
			//}
			//foreach (BSBGLTab tabPage in this._filler.TabPages)
			//{
			//    tabPage.Schematic.StartDebugMode();
			//}
			//this._pageIndex = 0;
			//ushort[] values = new ushort[1];
			//values[0] = this._pageIndex;
			//ProgramPageStruct pp = new ProgramPageStruct();
			//pp.InitStruct(Common.TOBYTES(values, false));
			//this._currentProgramPage.Value = pp;
			//this._currentProgramPage.SaveStruct();
			//this._isOnSimulateMode = true;
			//this._isSaveLogic = true;
			//this._formCheck = new MessageBoxForm();
			//this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
			//this._formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);

			if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
			if (this._filler.TabPages.Count == 0) return;
			this._compiller.ResetCompiller();
			try
			{
				foreach (BSBGLTab src in this._filler.TabPages)
				{
					this._compiller.AddSource(src.TabName, src.Schematic);
					this.OutMessage("���������� �����: " + src.TabName);
				}
			}
			catch (Exception exc)
			{
				MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
		        this.OnStop();
		        return;
	        }

	        this._binFile = this._compiller.Make();

	        double percent = (float)this._compiller.BinarySize * 100 / 2047;
	        string outMessage = "�p����� ���������� �����: " + myRound(percent, 1) + "%.";
	        this.OutMessage("�p����� ���������� �����: " + myRound(percent, 1) + "%.");

	        if (percent > 95)
	        {
		        DialogResult dialogResult = MessageBox.Show(outMessage + "\n�������� ����������� ����� ������ (95%)!\n ������ �������� ������ � ����������?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

		        if (dialogResult == DialogResult.Yes)
		        {
			        this.CompileProjectDone();
		        }
		        else if (dialogResult == DialogResult.No)
		        {
			        this.OnStop();
			        return;
		        }
		        return;
	        }

	        if (MessageBox.Show(outMessage + "\n������ �������� ������ � ����������?", "�������� ���������� ��������� � ����������", MessageBoxButtons.OKCancel,
		            MessageBoxIcon.Question) != DialogResult.OK) return;

	        this.CompileProjectDone();
		}

	    private void CompileProjectDone()
	    {
		    if (this._binFile.Length == 0)
		    {
			    this.OnStop();
			    return;
		    }

		    foreach (BSBGLTab tabPage in this._filler.TabPages)
		    {
			    tabPage.Schematic.StartDebugMode();
		    }

		    this._pageIndex = 0;
		    ushort[] values = new ushort[1];
		    values[0] = this._pageIndex;
		    ProgramPageStruct pp = new ProgramPageStruct();
		    pp.InitStruct(Common.TOBYTES(values, false));
		    this._currentProgramPage.Value = pp;
		    this._currentProgramPage.SaveStruct();
		    this._isOnSimulateMode = true;
		    this._isSaveLogic = true;
		    this._formCheck = new MessageBoxForm();
		    this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
		    this._formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
	    }

	    static double myRound(double x, int precision)
	    {
		    return ((int)(x * Math.Pow(10.0, precision)) / Math.Pow(10.0, precision));
	    }

	    private void CompileProjectOffline()
	    {
		    if (this._filler.TabPages.Count == 0) return;
		    this._compiller.ResetCompiller();
		    try
		    {
			    foreach (BSBGLTab src in this._filler.TabPages)
			    {
				    this._compiller.AddSource(src.TabName, src.Schematic);
				    this.OutMessage("���������� �����: " + src.TabName);
			    }
		    }
		    catch (Exception exc)
		    {
			    MessageBox.Show(exc.Message, "������ ����������", MessageBoxButtons.OK, MessageBoxIcon.Error);
			    this.OnStop();
			    return;
		    }

		    this._binFile = this._compiller.Make();

		    double percent = (float)this._compiller.BinarySize * 100 / 2047;
		    string outMessage = "�p����� ���������� �����: " + myRound(percent, 1) + "%.";
		    this.OutMessage("�p����� ���������� �����: " + myRound(percent, 1) + "%.");

		    if (/*this._compiller.BinarySize > 2047*/ percent > 95)
		    {
			    MessageBox.Show(outMessage + "\n�������� ����������� ����� ������ (95%)!", "��������!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			    this.OnStop();
			    return;
		    }

		    MessageBox.Show(outMessage, "��������� ���������� �����", MessageBoxButtons.OK, MessageBoxIcon.Information);

	    }

		private void OnCompileOffline(object sender, EventArgs e)
		{
			this.CompileProjectOffline();
		}

		private void OnCompile(object sender, EventArgs e)
        {
            this.CompileProject();
        }

        void OnStop(object sender, EventArgs e)
        {
            this.OnStop();
        }

        void OnStop()
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }

        private bool SaveActiveDoc()
        {
            BSBGLTab tabPage = (BSBGLTab)this._filler.SelectedTab;
            if (tabPage.Schematic.LinesIsLoaded && !tabPage.Schematic.IsDrawnLoadedLines)
            {
                string msg =
                    string.Format("�� ����������� ����� \"{0}\" �� ������������ �����. ����������� ����� � ����������?",
                        tabPage.TabName);
                DialogResult dr = MessageBox.Show(msg, "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    tabPage.Schematic.DrawLines();
                }
                else
                {
                    return false;
                }
            }
            ZIPCompressor compr = new ZIPCompressor();
            System.Windows.Forms.DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = System.Xml.Formatting.Indented;
                memwriter.WriteStartDocument();
                tabPage.Schematic.WriteXml(memwriter);
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName,
                    FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length, false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this.saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length, false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }

        private bool SaveProjectDoc()
        {
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                if (tabPage.Schematic.LinesIsLoaded && !tabPage.Schematic.IsDrawnLoadedLines)
                {
                    string msg =
                    string.Format("�� ����������� ����� \"{0}\" �� ������������ �����. ����������� ����� � ����������?",
                        tabPage.TabName);
                    DialogResult dr = MessageBox.Show(msg, "��������!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dr == DialogResult.Yes)
                    {
                        tabPage.Schematic.DrawLines();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            ZIPCompressor compr = new ZIPCompressor();
            System.Windows.Forms.DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length, false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this.saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length, false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }

        #endregion
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(BEMN.UDZT.UDZT); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get 
            {
                return Resources.programming.ToBitmap();//(Image)BEMN.��801.Properties.Resources.programming1.ToBitmap(); 
            }
        }

        public string NodeName
        {
            get { return "����������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._currentSignalsStruct.RemoveStructQueries();
            if (this._filler.SelectedTab == null) return;
            if (MessageBox.Show("��������� ������� ������ �� ����� ?", "�������� �������", MessageBoxButtons.YesNo) ==
                DialogResult.Yes)
            {
                this.SaveProjectDoc();
            }
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Dispose();
            }
            this._filler.TabPages.Clear();
        }
    }
}
