namespace BEMN.UDZT
{
    partial class AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this._openAlarmJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._journalCntLabel = new System.Windows.Forms.Label();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1nCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is10Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2nCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is20Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is22Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3nCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is30Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is32Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1d0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2d0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3d0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1T0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2T0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3T0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UnCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _openAlarmJounralDlg
            // 
            this._openAlarmJounralDlg.DefaultExt = "xml";
            this._openAlarmJounralDlg.Filter = "(*.xml)|*.xml|(*.bin)|*.bin";
            this._openAlarmJounralDlg.RestoreDirectory = true;
            this._openAlarmJounralDlg.Title = "������� ������  ������ ��� ��801";
            // 
            // _saveAlarmJournalDlg
            // 
            this._saveAlarmJournalDlg.DefaultExt = "xml";
            this._saveAlarmJournalDlg.FileName = "��801_������_������";
            this._saveAlarmJournalDlg.Filter = "(*.xml)|*.xml";
            this._saveAlarmJournalDlg.Title = "���������  ������ ������ ��� ��801";
            // 
            // _journalCntLabel
            // 
            this._journalCntLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._journalCntLabel.AutoSize = true;
            this._journalCntLabel.Location = new System.Drawing.Point(66, 360);
            this._journalCntLabel.Name = "_journalCntLabel";
            this._journalCntLabel.Size = new System.Drawing.Size(13, 13);
            this._journalCntLabel.TabIndex = 23;
            this._journalCntLabel.Text = "0";
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deserializeBut.Location = new System.Drawing.Point(607, 324);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 21;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._deserializeBut_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._serializeBut.Location = new System.Drawing.Point(347, 324);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(111, 23);
            this._serializeBut.TabIndex = 20;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._serializeBut_Click);
            // 
            // _readBut
            // 
            this._readBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBut.Location = new System.Drawing.Point(5, 324);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(75, 23);
            this._readBut.TabIndex = 19;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._IaCol,
            this._IdaCol,
            this._IdbCol,
            this._IdcCol,
            this._ItaCol,
            this._ItbCol,
            this._ItcCol,
            this._Is1aCol,
            this._Is1bCol,
            this._Is1cCol,
            this._Is2aCol,
            this._Is2bCol,
            this._Is2cCol,
            this._Is3aCol,
            this._Is3bCol,
            this._Is3cCol,
            this._Is1nCol,
            this._Is10Col,
            this._Is12Col,
            this._Is2nCol,
            this._Is20Col,
            this._Is22Col,
            this._Is3nCol,
            this._Is30Col,
            this._Is32Col,
            this._Is1d0Col,
            this._Is2d0Col,
            this._Is3d0Col,
            this._Is1T0Col,
            this._Is2T0Col,
            this._Is3T0Col,
            this._UaCol,
            this._UbCol,
            this._UcCol,
            this._UabCol,
            this._UbcCol,
            this._UcaCol,
            this._UnCol,
            this._U0Col,
            this._U2Col,
            this._FCol,
            this._D1Col,
            this._D2Col,
            this._D3Col});
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(737, 318);
            this._journalGrid.TabIndex = 18;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "�";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "�����";
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "���������";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle1;
            this._msg1Col.HeaderText = "���������";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "�������";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._msgCol.HeaderText = "�������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 120;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "�������� ������������";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._codeCol.HeaderText = "�������� ������������";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "�������� ��������� ������������";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._typeCol.HeaderText = "�������� ��������� ������������";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IaCol
            // 
            this._IaCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._IaCol.DataPropertyName = "������ �������";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IaCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._IaCol.HeaderText = "������ �������";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IdaCol
            // 
            this._IdaCol.DataPropertyName = "Ia ����.";
            this._IdaCol.HeaderText = "Ia ����.";
            this._IdaCol.Name = "_IdaCol";
            this._IdaCol.ReadOnly = true;
            this._IdaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdaCol.Width = 50;
            // 
            // _IdbCol
            // 
            this._IdbCol.DataPropertyName = "Ib ����.";
            this._IdbCol.HeaderText = "Ib ����.";
            this._IdbCol.Name = "_IdbCol";
            this._IdbCol.ReadOnly = true;
            this._IdbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdbCol.Width = 50;
            // 
            // _IdcCol
            // 
            this._IdcCol.DataPropertyName = "Ic ����.";
            this._IdcCol.HeaderText = "Ic ����.";
            this._IdcCol.Name = "_IdcCol";
            this._IdcCol.ReadOnly = true;
            this._IdcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdcCol.Width = 50;
            // 
            // _ItaCol
            // 
            this._ItaCol.DataPropertyName = "ITa";
            this._ItaCol.HeaderText = "ITa";
            this._ItaCol.Name = "_ItaCol";
            this._ItaCol.ReadOnly = true;
            this._ItaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItaCol.Width = 29;
            // 
            // _ItbCol
            // 
            this._ItbCol.DataPropertyName = "ITb";
            this._ItbCol.HeaderText = "ITb";
            this._ItbCol.Name = "_ItbCol";
            this._ItbCol.ReadOnly = true;
            this._ItbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItbCol.Width = 29;
            // 
            // _ItcCol
            // 
            this._ItcCol.DataPropertyName = "ITc";
            this._ItcCol.HeaderText = "ITc";
            this._ItcCol.Name = "_ItcCol";
            this._ItcCol.ReadOnly = true;
            this._ItcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItcCol.Width = 29;
            // 
            // _Is1aCol
            // 
            this._Is1aCol.DataPropertyName = "Is1a";
            this._Is1aCol.HeaderText = "Is1a";
            this._Is1aCol.Name = "_Is1aCol";
            this._Is1aCol.ReadOnly = true;
            this._Is1aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1aCol.Width = 33;
            // 
            // _Is1bCol
            // 
            this._Is1bCol.DataPropertyName = "Is1b";
            this._Is1bCol.HeaderText = "Is1b";
            this._Is1bCol.Name = "_Is1bCol";
            this._Is1bCol.ReadOnly = true;
            this._Is1bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1bCol.Width = 33;
            // 
            // _Is1cCol
            // 
            this._Is1cCol.DataPropertyName = "Is1c";
            this._Is1cCol.HeaderText = "Is1c";
            this._Is1cCol.Name = "_Is1cCol";
            this._Is1cCol.ReadOnly = true;
            this._Is1cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1cCol.Width = 33;
            // 
            // _Is2aCol
            // 
            this._Is2aCol.DataPropertyName = "Is2a";
            this._Is2aCol.HeaderText = "Is2a";
            this._Is2aCol.Name = "_Is2aCol";
            this._Is2aCol.ReadOnly = true;
            this._Is2aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2aCol.Width = 33;
            // 
            // _Is2bCol
            // 
            this._Is2bCol.DataPropertyName = "Is2b";
            this._Is2bCol.HeaderText = "Is2b";
            this._Is2bCol.Name = "_Is2bCol";
            this._Is2bCol.ReadOnly = true;
            this._Is2bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2bCol.Width = 33;
            // 
            // _Is2cCol
            // 
            this._Is2cCol.DataPropertyName = "Is2c";
            this._Is2cCol.HeaderText = "Is2c";
            this._Is2cCol.Name = "_Is2cCol";
            this._Is2cCol.ReadOnly = true;
            this._Is2cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2cCol.Width = 33;
            // 
            // _Is3aCol
            // 
            this._Is3aCol.DataPropertyName = "Is3a";
            this._Is3aCol.HeaderText = "Is3a";
            this._Is3aCol.Name = "_Is3aCol";
            this._Is3aCol.ReadOnly = true;
            this._Is3aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3aCol.Width = 33;
            // 
            // _Is3bCol
            // 
            this._Is3bCol.DataPropertyName = "Is3b";
            this._Is3bCol.HeaderText = "Is3b";
            this._Is3bCol.Name = "_Is3bCol";
            this._Is3bCol.ReadOnly = true;
            this._Is3bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3bCol.Width = 33;
            // 
            // _Is3cCol
            // 
            this._Is3cCol.DataPropertyName = "Is3c";
            this._Is3cCol.HeaderText = "Is3c";
            this._Is3cCol.Name = "_Is3cCol";
            this._Is3cCol.ReadOnly = true;
            this._Is3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3cCol.Width = 33;
            // 
            // _Is1nCol
            // 
            this._Is1nCol.DataPropertyName = "Is1n";
            this._Is1nCol.HeaderText = "Is1n";
            this._Is1nCol.Name = "_Is1nCol";
            this._Is1nCol.ReadOnly = true;
            this._Is1nCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1nCol.Width = 33;
            // 
            // _Is10Col
            // 
            this._Is10Col.DataPropertyName = "Is10";
            this._Is10Col.HeaderText = "Is10";
            this._Is10Col.Name = "_Is10Col";
            this._Is10Col.ReadOnly = true;
            this._Is10Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is10Col.Width = 33;
            // 
            // _Is12Col
            // 
            this._Is12Col.DataPropertyName = "Is12";
            this._Is12Col.HeaderText = "Is12";
            this._Is12Col.Name = "_Is12Col";
            this._Is12Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is12Col.Width = 33;
            // 
            // _Is2nCol
            // 
            this._Is2nCol.DataPropertyName = "Is2n";
            this._Is2nCol.HeaderText = "Is2n";
            this._Is2nCol.Name = "_Is2nCol";
            this._Is2nCol.ReadOnly = true;
            this._Is2nCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2nCol.Width = 33;
            // 
            // _Is20Col
            // 
            this._Is20Col.DataPropertyName = "Is20";
            this._Is20Col.HeaderText = "Is20";
            this._Is20Col.Name = "_Is20Col";
            this._Is20Col.ReadOnly = true;
            this._Is20Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is20Col.Width = 33;
            // 
            // _Is22Col
            // 
            this._Is22Col.DataPropertyName = "Is22";
            this._Is22Col.HeaderText = "Is22";
            this._Is22Col.Name = "_Is22Col";
            this._Is22Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is22Col.Width = 33;
            // 
            // _Is3nCol
            // 
            this._Is3nCol.DataPropertyName = "Is3n";
            this._Is3nCol.HeaderText = "Is3n";
            this._Is3nCol.Name = "_Is3nCol";
            this._Is3nCol.ReadOnly = true;
            this._Is3nCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3nCol.Width = 33;
            // 
            // _Is30Col
            // 
            this._Is30Col.DataPropertyName = "Is30";
            this._Is30Col.HeaderText = "Is30";
            this._Is30Col.Name = "_Is30Col";
            this._Is30Col.ReadOnly = true;
            this._Is30Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is30Col.Width = 33;
            // 
            // _Is32Col
            // 
            this._Is32Col.DataPropertyName = "Is32";
            this._Is32Col.HeaderText = "Is32";
            this._Is32Col.Name = "_Is32Col";
            this._Is32Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is32Col.Width = 33;
            // 
            // _Is1d0Col
            // 
            this._Is1d0Col.DataPropertyName = "Is1d0";
            this._Is1d0Col.HeaderText = "Is1d0";
            this._Is1d0Col.Name = "_Is1d0Col";
            this._Is1d0Col.ReadOnly = true;
            this._Is1d0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1d0Col.Width = 39;
            // 
            // _Is2d0Col
            // 
            this._Is2d0Col.DataPropertyName = "Is2d0";
            this._Is2d0Col.HeaderText = "Is2d0";
            this._Is2d0Col.Name = "_Is2d0Col";
            this._Is2d0Col.ReadOnly = true;
            this._Is2d0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2d0Col.Width = 39;
            // 
            // _Is3d0Col
            // 
            this._Is3d0Col.DataPropertyName = "Is3d0";
            this._Is3d0Col.HeaderText = "Is3d0";
            this._Is3d0Col.Name = "_Is3d0Col";
            this._Is3d0Col.ReadOnly = true;
            this._Is3d0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3d0Col.Width = 39;
            // 
            // _Is1T0Col
            // 
            this._Is1T0Col.DataPropertyName = "Is1T0";
            this._Is1T0Col.HeaderText = "Is1T0";
            this._Is1T0Col.Name = "_Is1T0Col";
            this._Is1T0Col.ReadOnly = true;
            this._Is1T0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1T0Col.Width = 40;
            // 
            // _Is2T0Col
            // 
            this._Is2T0Col.DataPropertyName = "Is2T0";
            this._Is2T0Col.HeaderText = "Is2T0";
            this._Is2T0Col.Name = "_Is2T0Col";
            this._Is2T0Col.ReadOnly = true;
            this._Is2T0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2T0Col.Width = 40;
            // 
            // _Is3T0Col
            // 
            this._Is3T0Col.DataPropertyName = "Is3T0";
            this._Is3T0Col.HeaderText = "Is3T0";
            this._Is3T0Col.Name = "_Is3T0Col";
            this._Is3T0Col.ReadOnly = true;
            this._Is3T0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3T0Col.Width = 40;
            // 
            // _UaCol
            // 
            this._UaCol.DataPropertyName = "Ua";
            this._UaCol.HeaderText = "Ua";
            this._UaCol.Name = "_UaCol";
            this._UaCol.ReadOnly = true;
            this._UaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UaCol.Width = 27;
            // 
            // _UbCol
            // 
            this._UbCol.DataPropertyName = "Ub";
            this._UbCol.HeaderText = "Ub";
            this._UbCol.Name = "_UbCol";
            this._UbCol.ReadOnly = true;
            this._UbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbCol.Width = 27;
            // 
            // _UcCol
            // 
            this._UcCol.DataPropertyName = "Uc";
            this._UcCol.HeaderText = "Uc";
            this._UcCol.Name = "_UcCol";
            this._UcCol.ReadOnly = true;
            this._UcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcCol.Width = 27;
            // 
            // _UabCol
            // 
            this._UabCol.DataPropertyName = "Uab";
            this._UabCol.HeaderText = "Uab";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 33;
            // 
            // _UbcCol
            // 
            this._UbcCol.DataPropertyName = "Ubc";
            this._UbcCol.HeaderText = "Ubc";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 33;
            // 
            // _UcaCol
            // 
            this._UcaCol.DataPropertyName = "Uca";
            this._UcaCol.HeaderText = "Uca";
            this._UcaCol.Name = "_UcaCol";
            this._UcaCol.ReadOnly = true;
            this._UcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcaCol.Width = 33;
            // 
            // _UnCol
            // 
            this._UnCol.DataPropertyName = "Un";
            this._UnCol.HeaderText = "Un";
            this._UnCol.Name = "_UnCol";
            this._UnCol.ReadOnly = true;
            this._UnCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UnCol.Width = 27;
            // 
            // _U0Col
            // 
            this._U0Col.DataPropertyName = "U0";
            this._U0Col.HeaderText = "U0";
            this._U0Col.Name = "_U0Col";
            this._U0Col.ReadOnly = true;
            this._U0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U0Col.Width = 27;
            // 
            // _U2Col
            // 
            this._U2Col.DataPropertyName = "U2";
            this._U2Col.HeaderText = "U2";
            this._U2Col.Name = "_U2Col";
            this._U2Col.ReadOnly = true;
            this._U2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U2Col.Width = 27;
            // 
            // _FCol
            // 
            this._FCol.DataPropertyName = "F";
            this._FCol.HeaderText = "F";
            this._FCol.MinimumWidth = 60;
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 60;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "D[1-8]";
            this._D1Col.HeaderText = "D[1-8]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 42;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "D[9-16]";
            this._D2Col.HeaderText = "D[9-16]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 48;
            // 
            // _D3Col
            // 
            this._D3Col.DataPropertyName = "D[17-24]";
            this._D3Col.HeaderText = "D[17-24]";
            this._D3Col.Name = "_D3Col";
            this._D3Col.ReadOnly = true;
            this._D3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D3Col.Width = 54;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "������ :";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(464, 324);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(137, 23);
            this._exportButton.TabIndex = 25;
            this._exportButton.Text = "��������� � Html";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "������ ������ �� 801";
            this._saveJournalHtmlDialog.Filter = "������ ������ �� 801 | *.html";
            this._saveJournalHtmlDialog.Title = "���������  ������ ������ ��� ��801";
            // 
            // AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 379);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._journalCntLabel);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.Controls.Add(this._journalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(753, 417);
            this.Name = "AlarmJournalForm";
            this.Text = "������ ������";
            this.Activated += new System.EventHandler(this.AlarmJournalForm_Activated);
            this.Deactivate += new System.EventHandler(this.AlarmJournalForm_Deactivate);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openAlarmJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDlg;
        private System.Windows.Forms.Label _journalCntLabel;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1nCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is10Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2nCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is20Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is22Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3nCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is30Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is32Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1d0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2d0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3d0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1T0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2T0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3T0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D3Col;
    }
}