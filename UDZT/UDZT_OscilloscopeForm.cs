using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Interfaces;

namespace BEMN.UDZT
{
    public partial class OscilloscopeForm : Form, IFormView
    {
        #region Fields
        private UDZT _device;
        private int _recordIndex;
        private List<ushort[]> _oscilloscopeJournalList = new List<ushort[]>();
        private List<int[]> _avaryStartEndList = new List<int[]>();
        private List<int[]> _oscilloscopeStartEndList = new List<int[]>();
        private List<string[]> _oscilloscopeDateTimeList = new List<string[]>();
        private List<int> _avaryBeginList = new List<int>();
        private List<List<int>> _avaryBeginOnOscList = new List<List<int>>();
        private StartEndOscilloscope _osc;
        private List<StartEndOscilloscope> _oscilloscopeList = new List<StartEndOscilloscope>();
        private int _currentOscilloscope;
        private int _start;
        private int _end;
        private bool _readOsc = true;
        private FileStream _WriteDATFile;
        private FileStream _WriteCFGFile;
        private FileStream _WriteHDRFile;
        private FileStream _ReadDATFile;
        private FileStream _ReadCFGFile;
        private FileStream _ReadHDRFile;
        #endregion

        #region C'tor
        public OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public OscilloscopeForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.OscJournalSaveOK += HandlerHelper.CreateHandler(this, () => this._device.LoadOscilloscopeJournal(this._recordIndex));
            this._device.OscJournalSaveFail += HandlerHelper.CreateHandler(this, this.ReadJournalFail);
            this._device.OscJournalLoadOK += HandlerHelper.CreateHandler(this, this.ReadJournalRecord);
            this._device.OscJournalLoadFail += HandlerHelper.CreateHandler(this, this.ReadJournalFail);

            this._device.OscilloscopeLoadOK += HandlerHelper.CreateHandler(this, this.OnLoadOsc);
            this._device.OscilloscopeLoadFail += HandlerHelper.CreateHandler(this, this.OscilloscopeLoadFail);
            this._device.OscilloscopeSaveOK += HandlerHelper.CreateHandler(this, this.OscilloscopeSaveOK);
            this._device.OscilloscopeSaveFail += HandlerHelper.CreateHandler(this, this.OscilloscopeLoadFail);
        }

        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this.ReadJournal(); 
        }

        #endregion

        public void ReadJournal() 
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscilloscopeCountLabel.Visible = false;
            this._oscilloscopeCountCB.Visible = false;
            this._oscJournalReadButton.Enabled = false;
            this._oscilloscopeCountCB.Items.Clear();
            this._oscilloscopeJournalList.Clear();
            this._avaryStartEndList.Clear();
            this._avaryBeginOnOscList.Clear();
            this._recordIndex = 0;
            this._oscJournalDataGrid.Rows.Clear();
            this._oscStatus.Text = "���� ������ ������� ������������...";
            this._device.SaveOscilloscopeJournal((ushort)this._recordIndex);
        }

        private void ReadJournalFail()
        {
            this._oscStatus.Text = "������ ������ ������� ������������";
            this._oscJournalReadButton.Enabled = true;
            this._oscLoadButton.Enabled = true;
        }

        public void ReadJournalRecord() 
        {
            if (this._device.OscExist)
            {
                this._oscilloscopeJournalList.Add(this._device.OscJournalValues);
                this._device.OscJournalRecord = this._device.OscJournalValues;

                #region ����-�����
                string _datetime = this._device.OscDay + "-" + this._device.OscMonth + "-" + this._device.OscYear + ", " + this._device.OscHours + ":" + this._device.OscMinutes + ":" + this._device.OscSeconds + "." + this._device.OscMiliseconds;
                #endregion

                #region ����� ����� ��� ������� � �������������
                int _oscEnd = this._device.OscPoint + (this._device.OscLen + this._device.OscAfter) *this._device.OscRez;
                string _oscEndSTR = string.Empty;
                string _oscEndSTRList = string.Empty;
                if (_oscEnd > 0x78000)
                {
                    _oscEndSTR = _oscEnd / 18 + " [" + (_oscEnd - 0x78000 ) / 18 + "]";
                    _oscEndSTRList = (_oscEnd - 0x78000 ).ToString();
                }
                else
                {
                    _oscEndSTR = (_oscEnd / 18).ToString();
                    _oscEndSTRList = _oscEnd.ToString();
                }
                #endregion

                this._avaryStartEndList.Add(new int[] { Convert.ToInt32(this._device.OscPoint), Convert.ToInt32(_oscEndSTRList)});

                #region ������ -> ������
                string str = "";
                if (this._recordIndex == 0) { str = "����."; }
                this._oscJournalDataGrid.Rows.Add(new object[] { this._recordIndex + 1 + " / [" + this._recordIndex +"] "+ str,
                                                            _datetime, this._device.OscStage,   //�������
                    this._device.OscReady,
                                                            (this._device.OscPoint / 18).ToString(),
                                                            _oscEndSTR, this._device.OscLen, this._device.OscAfter, this._device.OscAlm, this._device.OscRez
                                                          });
                #endregion

                this._recordIndex++;
                this._device.LoadOscilloscopeJournal(this._recordIndex);
            }
            else 
            {
                this._oscStatus.Text = "������ ������� ������������ ���������...";
                this._oscilloscopeStartEndList.Clear();
                this._oscilloscopeDateTimeList.Clear();
                this._start = 0;
                this._end = 0;
                if (this._avaryStartEndList.Count != 0)
                {
                    this._oscReadButton.Enabled = true;
                    this._oscJournalReadButton.Enabled = true;
                    this._oscilloscopeCountLabel.Visible = true;
                    this._oscilloscopeCountCB.Visible = true;
                    for (int i = 0; i < this._avaryStartEndList.Count; i++)
                    {
                        this.PrepareOscArray(i);
                    }

                    int s = 0;
                    if (this._avaryBeginOnOscList.Count != 0) 
                    {   
                        for (int i = 0; i < this._avaryBeginOnOscList.Count; i++)
                        {
                            if (i != this._avaryBeginOnOscList.Count - 1)
                            {
                                s = this._oscilloscopeStartEndList[i][0] / 18;
                                for (int j = 0; j < this._avaryBeginOnOscList[i].Count; j++)
                                {
                                    this._avaryBeginOnOscList[i][j] -= s;
                                }
                            }
                        }
                    }

                    if(this._oscilloscopeStartEndList.Count != 0) 
                    {
                        for (int i = 0; i < this._oscilloscopeStartEndList.Count; i++)
                            this._oscilloscopeCountCB.Items.Add(i+1);

                        this._oscilloscopeCountCB.SelectedIndex = 0;
                    }
                }
                else 
                {
                    MessageBox.Show("������ ������������ ����.", "��������!");
                }
            }
        }

        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ReadJournal(); 
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.ShowOldOsc();
        }
        
        private void ShowOldOsc()
        {
            try
            {
                OscilloscopeResultForm _resOscForm;
                if (this._currentOscilloscope <= this._avaryBeginOnOscList.Count - 1)
                {
                    _resOscForm = new OscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                }
                else
                {
                    this._currentOscilloscope = 0;
                    this._avaryBeginOnOscList.Clear();
                    this._avaryBeginOnOscList.Add(new List<int>() { 0 });
                    _resOscForm = new OscilloscopeResultForm(this._osc, this._avaryBeginOnOscList[this._currentOscilloscope]);
                }
                _resOscForm.Show();
            }
            catch 
            {
                OscilloscopeResultForm _resOscForm = new OscilloscopeResultForm();
                _resOscForm.Show();
            }
        }


        public void PrepareOscArray(int avaryIndex) 
        {
            try
            {
                if (avaryIndex > 0 && this._avaryStartEndList.Count > 1)
                {
                    if (this._avaryStartEndList[avaryIndex][1] > this._avaryStartEndList[avaryIndex - 1][0])
                    {
                        this._start = this._avaryStartEndList[avaryIndex][0];
                        this._avaryBeginList.Add(this._start / 18 + (int)this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value);
                    }
                    else
                    {
                        if ((this._avaryStartEndList[avaryIndex][1] < this._avaryStartEndList[avaryIndex - 1][1]) && (this._avaryStartEndList[avaryIndex][1] < this._avaryStartEndList[avaryIndex][0]) && (this._avaryStartEndList[avaryIndex - 1][1] < this._avaryStartEndList[avaryIndex - 1][0]))
                        {
                            this._start = this._avaryStartEndList[avaryIndex][0];
                            this._avaryBeginList.Add(this._start / 18 + (int)this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value);
                        }
                        else
                        {
                            this._oscilloscopeStartEndList.Add(new int[] { this._start, this._end, Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[3].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[8].Value.ToString()) });
                            this._oscilloscopeDateTimeList.Add(new string[] { this._oscJournalDataGrid.Rows[avaryIndex].Cells[1].Value.ToString(), this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString() });
                            this._avaryBeginOnOscList.Add(this._avaryBeginList);
                            this._avaryBeginList = new List<int>();
                            this._start = this._avaryStartEndList[avaryIndex][0];
                            this._end = this._avaryStartEndList[avaryIndex][1];
                            this._avaryBeginList.Add(this._start / 18 + (int)this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value);
                        }
                    }
                }
                else
                {
                    if (this._avaryStartEndList.Count == 1)
                    {
                        this._start = this._avaryStartEndList[avaryIndex][0];
                        this._end = this._avaryStartEndList[avaryIndex][1];
                        this._oscilloscopeStartEndList.Add(new int[] { this._start, this._end, Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[3].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[8].Value.ToString()) });
                        this._oscilloscopeDateTimeList.Add(new string[] { this._oscJournalDataGrid.Rows[avaryIndex].Cells[1].Value.ToString(), this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString() });
                        this._avaryBeginList.Add(this._start / 18 + (int)this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value);
                        this._avaryBeginOnOscList.Add(this._avaryBeginList);
                        this._avaryBeginList = new List<int>();
                    }
                    else
                    {
                        this._start = this._avaryStartEndList[avaryIndex][0];
                        this._end = this._avaryStartEndList[avaryIndex][1];
                        this._avaryBeginList.Add(this._start / 18 + (int)this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value);
                    }
                }
                if (this._avaryStartEndList.Count - 1 == avaryIndex && this._avaryStartEndList.Count != 1)
                {
                    this._oscilloscopeStartEndList.Add(new int[] { this._start, this._end, Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[3].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[6].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[7].Value.ToString()), Convert.ToInt32(this._oscJournalDataGrid.Rows[avaryIndex].Cells[8].Value.ToString()) });
                    this._oscilloscopeDateTimeList.Add(new string[] { this._oscJournalDataGrid.Rows[avaryIndex].Cells[1].Value.ToString(), this._oscJournalDataGrid.Rows[avaryIndex].Cells[5].Value.ToString() });
                    this._avaryBeginOnOscList.Add(this._avaryBeginList);
                    this._avaryBeginList = new List<int>();
                }
            }
            catch 
            {
            }
        }

        private void OscilloscopeSaveOK()
        {
            try
            {
                if (!this._osc.OscilloscopeLoaded)
                {
                    this._device.LoadOscilloscopePage(this._osc.Index);
                }
            }
            catch { }
        }

        private void OscilloscopeLoadFail()
        {
            if (MessageBox.Show("������������� ��������� � ��������!", "��������!", MessageBoxButtons.OK) == DialogResult.OK)
            {
                this.Exception(this.ShowOscButtonEnaibled);
            }
        }

        private void OnLoadOsc()
        {
            if (this._readOsc)
            {
                this._osc.ArrayToValues();

                if (this._osc.PageLoaded)
                {
                    this._osc.Index++;
                    this._device.SaveOscilloscopePage(this._osc.CurrentPage);
                    this._osc.PageLoaded = false;
                }

                if (this._osc.OscilloscopeLoaded)
                {
                    this._oscilloscopeList.Add(this._osc);
                    this._oscStatus.Text = "�������� ������������� ���������";
                    this._oscStatus.Text = "���� �������������� ������";
                    this._osc.InitArrays();
                    for (int i = this._osc.FirstByteIndex; i < this._osc.LastByteIndex; i += 18)
                    {
                        this._osc.CalculateOscParams(i);
                    }
                    this._osc.CalculateOscKoeffs();
                    this._oscStatus.Text = "�������������� ������ ���������";

                    if (MessageBox.Show("������������� ������.", "������ �������������", MessageBoxButtons.OK) == DialogResult.OK)
                    {
                        this.Exception(this.ShowOscButtonEnaibled);
                    }
                }
                this.Exception(this.ProgressBarIncriment);
            }
        }

        private void Exception(OnDeviceEventHandler _func) 
        {
            try
            {
                Invoke(_func);
            }
            catch
            {
                MessageBox.Show("��������� ����������� ������\n���������� � ������������", "��������!");
            }
        }

        private void ProgressBarClear()
        {
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Step = 1;
            this._oscProgressBar.Maximum = this._osc.Values.Length / this._osc.OtschetLengs;
            this._oscProgressBar.Minimum = 0;
        }

        public void ProgressBarIncriment() 
        {
            this._oscProgressBar.PerformStep();
        }

        public void ShowOscButtonEnaibled()
        {
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscJournalReadButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
        }

        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._oscJournalReadButton.Enabled = false;
            this._readOsc = true;
            this._oscilloscopeList.Clear();
            this._currentOscilloscope = this._oscilloscopeCountCB.SelectedIndex;
            this._device.OscJournalRecord = this._oscilloscopeJournalList[this._currentOscilloscope];
            try
            {
                this._osc = new StartEndOscilloscope(this._device,
                    this._oscilloscopeStartEndList[this._oscilloscopeCountCB.SelectedIndex][0],
                    this._oscilloscopeStartEndList[this._oscilloscopeCountCB.SelectedIndex][1],
                    this._oscilloscopeDateTimeList[this._oscilloscopeCountCB.SelectedIndex][0],
                    this._oscilloscopeDateTimeList[this._oscilloscopeCountCB.SelectedIndex][1]);

                this._osc.PageLoaded = false;
                this._osc.OscilloscopeLoaded = false;
                this._oscProgressBar.Value = 0;
                int temp = 0;
                if (this._device.OscPageSize%this._device.EnableOscPageSize != 0)
                {
                    temp = 1;
                }
                this._oscProgressBar.Maximum = this._osc.PagesCount*
                                               (this._device.OscPageSize/this._device.EnableOscPageSize + temp);
                this._oscProgressBar.Step = 1;
                this._oscStatus.Text = "���� �������� �������������";
                this._device.SaveOscilloscopePage(this._osc.StartPage);
                this._stopReadOsc.Enabled = true;
                this._oscJournalReadButton.Enabled = false;
                this.ProgressBarClear();
            }
            catch
            {
            }
        }
        
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._osc.InitArrays();
            for (int i = this._osc.FirstByteIndex; i < this._osc.LastByteIndex; i += 18)
            {
                this._osc.CalculateOscParams(i);
            }
            this._osc.CalculateOscKoeffs();
            this._readOsc = false;
            this._oscReadButton.Enabled = true;
            this._oscJournalReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
        }

        private void OscilloscopeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._readOsc = false;
        }

        public void ShowSaveWindow() 
        {
            IDeviceView idevice = (IDeviceView) this._device;
            this._saveOscilloscopeDlg.FileName = idevice.NodeName + "_�������������";
            this._saveOscilloscopeDlg.Title = "��������� ������������� " + idevice.NodeName;
            if (DialogResult.OK == this._saveOscilloscopeDlg.ShowDialog())
            {
                this.PrepareWriteComTradeFiles(this._saveOscilloscopeDlg.FileName, idevice.NodeName + "_�������������");
            }  
        }

        public void ShowLoadWindow()
        {
            IDeviceView idevice = (IDeviceView) this._device;
            this._openOscilloscopeDlg.FileName = idevice.NodeName + "_�������������";
            this._openOscilloscopeDlg.Filter = "(*.hdr) | *.hdr";
            this._openOscilloscopeDlg.Title = "������� ������������� " + idevice.NodeName;
            if (DialogResult.OK == this._openOscilloscopeDlg.ShowDialog())
            {
                this.PrepareReadComTradeFiles(this._openOscilloscopeDlg.FileName);
                this._oscStatus.Text = string.Format("������������ ��������� �� ����� {0}", this._openOscilloscopeDlg.FileName);
            }
        }

        public void SaveComTradeFiles() 
        {
            if (this.SaveHDR() && this.SaveCFG() && this.SaveDAT())
            {
                MessageBox.Show("����� ComTrade ������� ���������.");
            }
        }

        public string ConvertToComtradeIndex1(string val) 
        {
            if(val.Length < 6)
            {
                string nul = "";
                for (int i = 0; i < 6 - val.Length; i++) 
                {
                    nul += "0";
                }
                val = nul + val;
            }
            return val;
        }

        public string ConvertToComtradeIndex2(string val)
        {
            if (val.Length < 8)
            {
                string nul = "";
                for (int i = 0; i < 8 - val.Length; i++)
                {
                    nul += "0";
                }
                val = nul + val;
            }
            return val;
        }

        public bool SaveDAT() 
        {
            bool res = true;
            StreamWriter _strimDAT = new StreamWriter(this._WriteDATFile);
            try
            {
                for (int i = 0; i < this._osc.OscilloscopeLengs; i++) 
                {
                    _strimDAT.Write(this.ConvertToComtradeIndex1(i.ToString()) + ",");
                    _strimDAT.Write(this.ConvertToComtradeIndex2(i.ToString() + "000"));
                    

                    _strimDAT.Write("," + this._osc.S1PageIaValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S1PageIbValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S1PageIcValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S1PageInValues[i].ToString());

                    _strimDAT.Write("," + this._osc.S2PageIaValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S2PageIbValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S2PageIcValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S2PageInValues[i].ToString());

                    _strimDAT.Write("," + this._osc.S3PageIaValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S3PageIbValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S3PageIcValues[i].ToString());
                    _strimDAT.Write("," + this._osc.S3PageInValues[i].ToString());

                    _strimDAT.Write("," + this._osc.UaValues[i].ToString());
                    _strimDAT.Write("," + this._osc.UbValues[i].ToString());
                    _strimDAT.Write("," + this._osc.UcValues[i].ToString());
                    _strimDAT.Write("," + this._osc.UnValues[i].ToString());
                        
                    for (int j = 0; j < this._osc.DiskretData.Count; j++)
                    {
                        if (this._osc.DiskretData[j][i])
                            _strimDAT.Write(",1");
                        else
                            _strimDAT.Write(",0");
                    }
                    _strimDAT.Write("\r\n");
                }
            }
            catch
            {
                MessageBox.Show("���� " + this._WriteDATFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally
            {
                _strimDAT.Close();
            }
            return res;
        }

        public bool SaveCFG()
        {
            bool res = true;
            StreamWriter _strimCFG = new StreamWriter(this._WriteCFGFile);
            try
            {
                _strimCFG.Write(this._device.NodeName.ToString() + "," + this._device.DeviceNumber.ToString() + "\r\n");
                _strimCFG.Write((this._osc.DiskretData.Count + 16).ToString() + "," + 16.ToString() + "A, " + this._osc.DiskretData.Count.ToString() + " D" + "\r\n");
                _strimCFG.Write("1,S1 Ia,a,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("2,S1 Ib,b,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("3,S1 Ic,c,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[0]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("4,S1 In,n,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[1]) + ",0,0,-32768,32767,1,1,P\r\n");

                _strimCFG.Write("5,S2 Ia,a,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[2]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("6,S2 Ib,b,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[2]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("7,S2 Ic,c,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[2]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("8,S2 In,n,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[3]) + ",0,0,-32768,32767,1,1,P\r\n");

                _strimCFG.Write("9,S3 Ia,a,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[4]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("10,S3 Ib,b,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[4]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("11,S3 Ic,c,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[4]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("12,S3 In,n,,A," + this.ConvertCommaToPoint(this._osc.Koeffs[5]) + ",0,0,-32768,32767,1,1,P\r\n");

                _strimCFG.Write("13,Ua,a,,V," + this.ConvertCommaToPoint(this._osc.Koeffs[6]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("14,Ub,b,,V," + this.ConvertCommaToPoint(this._osc.Koeffs[6]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write ("15,Uc,c,,V," + this.ConvertCommaToPoint(this._osc.Koeffs[6]) + ",0,0,-32768,32767,1,1,P\r\n");
                _strimCFG.Write("16,Un,n,,V," + this.ConvertCommaToPoint(this._osc.Koeffs[7]) + ",0,0,-32768,32767,1,1,P\r\n");

                _strimCFG.Write("17,D1,0" + "\r\n");
                _strimCFG.Write("18,D2,0" + "\r\n");
                _strimCFG.Write("19,D3,0" + "\r\n");
                _strimCFG.Write("20,D4,0" + "\r\n");
                _strimCFG.Write("21,D5,0" + "\r\n");
                _strimCFG.Write("22,D6,0" + "\r\n");
                _strimCFG.Write("23,D7,0" + "\r\n");
                _strimCFG.Write("24,D8,0" + "\r\n");
                _strimCFG.Write("25,D9,0" + "\r\n");
                _strimCFG.Write("26,D10,0" + "\r\n");
                _strimCFG.Write("27,D11,0" + "\r\n");
                _strimCFG.Write("28,D12,0" + "\r\n");
                _strimCFG.Write("29,D13,0" + "\r\n");
                _strimCFG.Write("30,D14,0" + "\r\n");
                _strimCFG.Write("31,D15,0" + "\r\n");
                _strimCFG.Write("32,D16,0" + "\r\n");
                _strimCFG.Write("33,D17,0" + "\r\n");
                _strimCFG.Write("34,D18,0" + "\r\n");
                _strimCFG.Write("35,D19,0" + "\r\n");
                _strimCFG.Write("36,D20,0" + "\r\n");
                _strimCFG.Write("37,D21,0" + "\r\n");
                _strimCFG.Write("38,D22,0" + "\r\n");
                _strimCFG.Write("39,D23,0" + "\r\n");
                _strimCFG.Write("40,D24,0" + "\r\n");
                _strimCFG.Write("41,D25,0" + "\r\n");
                _strimCFG.Write("42,D26,0" + "\r\n");
                _strimCFG.Write("43,D27,0" + "\r\n");
                _strimCFG.Write("44,D28,0" + "\r\n");
                _strimCFG.Write("45,D29,0" + "\r\n");
                _strimCFG.Write("46,D30,0" + "\r\n");
                _strimCFG.Write("47,D31,0" + "\r\n");
                _strimCFG.Write("48,D32,0" + "\r\n");
                _strimCFG.Write("50\r\n");
                _strimCFG.Write("1\r\n");
                _strimCFG.Write("1000," + this._osc.OscilloscopeLengs + "\r\n");
                _strimCFG.Write(this._osc.Time + "\r\n");
                _strimCFG.Write(this._osc.AvaryTime + "\r\n");
                _strimCFG.Write("ASCII\r\n");
            }
            catch
            {
                MessageBox.Show("���� " + this._WriteCFGFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally 
            {
                _strimCFG.Close();
            }
            return res;
        }

        public bool SaveHDR()
        {
            bool res = true;
            StreamWriter _strimHDR = new StreamWriter(this._WriteHDRFile);
            try
            {
                _strimHDR.Write("����� ������������ : " + this._osc.Time + "\r\n");
                _strimHDR.Write("������ ������������� = " + this._osc.OscilloscopeLengs +  "\r\n");
                _strimHDR.Write("K�� = " + "\r\n");
                _strimHDR.Write("K���� = " + "\r\n");
                _strimHDR.Write("K�� = " + "\r\n");
                _strimHDR.Write("K���� = " + "\r\n");
                for (int i = 0; i < this._avaryBeginOnOscList[this._currentOscilloscope].Count; i++)
                {
                    if (i != this._avaryBeginOnOscList[this._currentOscilloscope].Count - 1)
                    {
                        _strimHDR.Write(this._avaryBeginOnOscList[this._currentOscilloscope][i].ToString() + ",");
                    }
                    else 
                    {
                        _strimHDR.Write(this._avaryBeginOnOscList[this._currentOscilloscope][i].ToString() + "\r\n");
                    }
                }
                _strimHDR.Write("BEMN" + "\r\n");

            }
            catch
            {
                MessageBox.Show("���� " + this._WriteHDRFile + " �� ��������!", "ERROR!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                res = false;
            }
            finally 
            {
                _strimHDR.Close();
            }
            return res;
        }

        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this.ShowSaveWindow();  
        }

        private string ConvertCommaToPoint(double val)
        {
            try
            {
                string[] res = val.ToString("0.000000").Split(',');

                return res[0] + "." + res[1];
            }
            catch { return "ERROR!"; }
        }

        private void PrepareWriteComTradeFiles(string path, string filename)
        {
            try
            {
                this._WriteDATFile = new FileStream(path + ".dat", FileMode.CreateNew);
                this._WriteCFGFile = new FileStream(path + ".cfg", FileMode.CreateNew);
                this._WriteHDRFile = new FileStream(path + ".hdr", FileMode.CreateNew);

                this.SaveComTradeFiles();
            }
            catch 
            {
                if (MessageBox.Show("���� " + filename + " ��� ����������.\n\n��������?", "WARNING!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    this._WriteDATFile = new FileStream(path + ".dat", FileMode.Create);
                    this._WriteCFGFile = new FileStream(path + ".cfg", FileMode.Create);
                    this._WriteHDRFile = new FileStream(path + ".hdr", FileMode.Create);

                    this.SaveComTradeFiles();
                }
                else 
                {
                    this.ShowSaveWindow();
                }
            }
            finally
            {
                if (this._WriteDATFile != null)
                {
                    this._WriteDATFile.Close();
                }
                if (this._WriteCFGFile != null)
                {
                    this._WriteCFGFile.Close();
                }
                if (this._WriteHDRFile != null)
                {
                    this._WriteHDRFile.Close();
                }
            }
        }

        private void PrepareReadComTradeFiles(string path)
        {
            try
            {
                this._avaryBeginOnOscList.Clear();
                string[] _prepath = path.Split('.');
                path = _prepath[0];
                this._ReadDATFile = new FileStream(path + ".dat", FileMode.Open);
                this._ReadCFGFile = new FileStream(path + ".cfg", FileMode.Open);
                this._ReadHDRFile = new FileStream(path + ".hdr", FileMode.Open);

                List<string[]> _datFileLines = new List<string[]>();
                string[] _avaryList = new string[] { "0" };
                string _msg = string.Empty;

                bool res = this.PrepareAllFiles(out _avaryList, out _datFileLines, out _msg);

                if (res)
                {
                    this._osc = new StartEndOscilloscope(this._device, _datFileLines);
                    this._currentOscilloscope = 0;
                    List<int> _avaryListInt = new List<int>();
                    for (int i = 0; i < _avaryList.Length; i++)
                    {
                        _avaryListInt.Add(Convert.ToInt32(_avaryList[i]));
                    }
                    this._avaryBeginOnOscList.Add(_avaryListInt);
                    MessageBox.Show("������������� ���������", "!!!");
                    this._oscShowButton.Enabled = true;
                }
                else 
                {
                    MessageBox.Show(_msg, "������");
                }
            }
            catch
            {

            }
            finally
            {
                if (this._ReadDATFile != null)
                {
                    this._ReadDATFile.Close();
                }
                if (this._ReadCFGFile != null)
                {
                    this._ReadCFGFile.Close();
                }
                if (this._ReadHDRFile != null)
                {
                    this._ReadHDRFile.Close();
                }
            }
        }

        private bool PrepareAllFiles(out string[] _avaryList, out List<string[]> _datFileLines, out string _msg)
        {
            bool res = true;
            _avaryList = null;
            _datFileLines = null;
            _msg = string.Empty;
            

            try
            {
                if (this.ParseHDR(out _avaryList, out _msg))
                {
                    if (_msg != string.Empty) 
                    {
                        MessageBox.Show(_msg,"��������!");
                    }
                    res = this.ParseCFG(out _msg);
                    res = this.ParseDAT(out _datFileLines, out _msg);
                }
                else { res = false; }
            }
            catch { res = false; }
            return res;
        }

        private bool ParseHDR(out string[] _avaryList, out string _msg) 
        {
            bool _parsed = true;
            StreamReader _strimReadHDR = null;
            _avaryList = null;
            _msg = string.Empty;

            try
            {
                _strimReadHDR = new StreamReader(this._ReadHDRFile);
                List<string> _hdrFileLines = new List<string>();
                bool _endHdr = false;

                do
                {
                    string _readHdrLine = _strimReadHDR.ReadLine();
                    if (_readHdrLine == null)
                    {
                        _endHdr = true;
                    }
                    else
                    {
                        _hdrFileLines.Add(_readHdrLine);
                    }
                } while (!_endHdr);

                if (_hdrFileLines[7] != "BEMN") 
                {
                    _parsed = false;
                    _avaryList = new string[] { "0" };
                    _msg = "��� ������������� �� �������������� ������'��.";
                }

                if (_hdrFileLines.Count >= 8 && _parsed)
                {
                    _avaryList = _hdrFileLines[6].Split(',');
                }
                try
                {
                    foreach (string t in _avaryList)
                    {
                        int _temp = Convert.ToInt32(t);
                    }
                }
                catch
                {
                    _msg = "������ ������ ������������� �� ��������!";
                    _avaryList = new string[] {"0"};
                }
            }
            catch
            {
                _parsed = false;
                _avaryList = new string[] { "0" };
                _msg = "���� .cfg ��������� � �� ����� ���� ������.";
            }
            finally
            {
                if (_strimReadHDR != null)
                {
                    _strimReadHDR.Close();
                }
            }

            return _parsed;
        }

        private bool ParseDAT(out List<string[]> _datFileLines, out string _msg) 
        {
            bool _parsed = true;
            StreamReader _strimReadDAT = null;
            _msg = string.Empty;
            _datFileLines = null;

            try
            {
                _strimReadDAT = new StreamReader(this._ReadDATFile);
                _datFileLines = new List<string[]>();
                bool _endDat = false;
                do
                {
                    string _startMas = _strimReadDAT.ReadLine();
                    if (_startMas == null)
                    {
                        _endDat = true;
                    }
                    else
                    {
                        string[] _firstMas = _startMas.Split(',');
                        string[] _clearMas = new string[_firstMas.Length - 2];
                        Array.Copy(_firstMas, 2, _clearMas, 0, _clearMas.Length);
                        _datFileLines.Add(_clearMas);
                    }
                } while (!_endDat);
            }
            catch
            {
                _parsed = false;
                _msg = "���� .dat ��������� � �� ����� ���� ������.";
            }
            finally 
            {
                if (_strimReadDAT != null)
                {
                    _strimReadDAT.Close();
                }
            }
            return _parsed;
        }

        private bool ParseCFG(out string _msg)
        {
            bool _parsed = true;
            StreamReader _strimReadCFG = null;
            _msg = string.Empty;

            try
            {
                _strimReadCFG = new StreamReader(this._ReadCFGFile);
                List<string> _cfgFileLines = new List<string>();
                bool _endCfg = false;
                do
                {
                    string _startMas = _strimReadCFG.ReadLine();
                    if (_startMas == null)
                    {
                        _endCfg = true;
                    }
                    else
                    {
                        _cfgFileLines.Add(_startMas);
                    }
                } while (!_endCfg);
            }
            catch
            {
                _parsed = false;
                _msg = "���� .cfg ��������� � �� ����� ���� ������.";
            }
            finally
            {
                if (_strimReadCFG != null)
                {
                    _strimReadCFG.Close();
                }
            }
            return _parsed;
        }

        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            this._oscShowButton.Enabled = false;
            this.ShowLoadWindow();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(UDZT); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.oscilloscope.ToBitmap();

            }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

    }
}