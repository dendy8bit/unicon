﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.UDZT.Structures.Defenses.DiffZero;
using BEMN.UDZT.Structures.Defenses.External;
using BEMN.UDZT.Structures.Defenses.F;
using BEMN.UDZT.Structures.Defenses.I;
using BEMN.UDZT.Structures.Defenses.IStar;
using BEMN.UDZT.Structures.Defenses.U;

namespace BEMN.UDZT
{
    public partial class ConfigurationForm : Form, IFormView
    {
        private const string WRITE_OSC_ERROR = "Произошла ошибка записи конфигурации осциллограммы";
        private string _ust = string.Empty;
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(UDZT); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Fields

        private UDZT _device;
        private double _vers;
        private MaskedTextBox[] _ULONGpowerTransMaskedTextBoxes,
            _DOUBLEpowerTransMaskedTextBoxes,
            _ULONGmeasureTransMaskedTextBoxes,
            _DOUBLEmeasureTransMaskedTextBoxes,
            _USHORTconfigEthernetMaskedTextBoxes,
            _ULONGDifPowerMaskedTextBoxes,
            _DOUBLEDifPowerMaskedTextBoxes,
            _ULONGapvTransMaskedTextBoxes,
            _ULONGavrTransMaskedTextBoxes,
            _DOUBLElzhTransMaskedTextBoxes,
            _ULONGswitchMaskedTextBoxes,
            _DOUBLEswitchMaskedTextBoxes,
            _ulongrRelayNeisprTImpTextBoxes;

        private ComboBox[] _oscopeChannels;
        private CheckedListBox[] _VLSChectboxlistArray;
        private DataGridView[] _inpSygnalsArray;

        private bool _validatingOk = true;
        private bool _validatingGb = true;
        private bool _connectingErrors;
        private bool _messagePower;
        private bool _messageMeasure;
        private bool _messageDifPower;
        private int[] _errorCoord;
        private TabPage _errorPage;
        private TabPage _errorMainPage;
        private DataGridView _errorGrid;
        private string _errorMes;
        private int _oscMemorySize;

        private bool firstMain;
        private bool firstReserve;

        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;

        #endregion

        #region Конструкторы

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(UDZT device)
        {
            this.InitializeComponent();
            this._device = device;
            this._vers = Common.VersionConverter(this._device.DeviceVersion);
            Strings.Version = this._vers;
            this._connectingErrors = false;
            this._messagePower = false;
            this._messageMeasure = false;
            this._messageDifPower = false;

            if (_vers <= 2.07)
            {
                this._configurationTabControl.TabPages.Remove(this._configEthernetPage);
            }

            NewDgwValidatior<AllDefenceDiffZeroStruct, DefenceDiffZeroStruct> _diffZeroData = new NewDgwValidatior<AllDefenceDiffZeroStruct, DefenceDiffZeroStruct>(
                _dif0DataGreed,
                3,
                _toolTip,
                new ColumnInfoCombo(Strings.DiffZeroStape, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.ModesLightMode),
                new ColumnInfoCombo(Strings.DiffBlocking),
                new ColumnInfoText(new CustomDoubleRule(0, 40)),
                new ColumnInfoCombo(Strings.MeasuringLight),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(new CustomDoubleRule(0, 40)),
                new ColumnInfoText(new CustomIntRule(0, 89)),
                new ColumnInfoText(new CustomDoubleRule(0, 40)),
                new ColumnInfoText(new CustomIntRule(0, 89)),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight)
            )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                    (
                        this._dif0DataGreed,
                        new TurnOffRule(1, Strings.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                    )
                }
            };

            NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> _mtzDeff =
                new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>(
                    _difensesIDataGrid, 8, _toolTip,
                    new ColumnInfoCombo(Strings.DiffMtzStape, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ModesLightMode),
                    new ColumnInfoText(new CustomDoubleRule(0, 40)),
                    new ColumnInfoCombo(Strings.MeasuringLight),
                    new ColumnInfoText(new CustomDoubleRule(0, 256)),
                    new ColumnInfoCombo(Strings.YesNo),
                    new ColumnInfoCombo(Strings.BusDirection),
                    new ColumnInfoCombo(Strings.UnDirection),
                    new ColumnInfoCombo(Strings.TokParameter),
                    new ColumnInfoCombo(Strings.Characteristic),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(new CustomIntRule(0, 4000)),
                    new ColumnInfoCombo(Strings.Blocking),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.ModesLight)
                );

                //{
                //    TurnOff = new[]
                //    {
                //        new TurnOffDgv
                //        (
                //            this._difensesIDataGrid,
                //            new TurnOffRule(1, Strings.ModesLightMode[0], true,
                //                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                //        )
                //    }
                //};

            NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct> _deffIStar = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>(
                _difensesI0DataGrid,
                6,
                _toolTip,
                new ColumnInfoCombo(Strings.DiffIStarStape, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.ModesLightMode),
                new ColumnInfoText(new CustomDoubleRule(0, 40)),
                new ColumnInfoCombo(Strings.MeasuringLight),
                new ColumnInfoText(new CustomDoubleRule(0, 256)),
                new ColumnInfoCombo(Strings.YesNo),
                new ColumnInfoCombo(Strings.BusDirection),
                new ColumnInfoCombo(Strings.UnDirection),
                new ColumnInfoCombo(Strings.I0Modes),
                new ColumnInfoCombo(Strings.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(new CustomIntRule(0, 4000)),
                new ColumnInfoCombo(Strings.Blocking),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight)
                )

                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difensesI0DataGrid,
                            new TurnOffRule(1, Strings.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        )
                    }
                };
            
            NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> uBValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>(
                    _difensesUBDataGrid,
                    4,
                    _toolTip,
                    new ColumnInfoCombo(Strings.UStages, ColumnsType.NAME),
                    new ColumnInfoCombo(Strings.ModesLightMode),
                    new ColumnInfoCombo(Strings.UType),
                    new ColumnInfoText(RulesContainer.Ustavka256),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.Ustavka256),
                    new ColumnInfoCheck(),
                    new ColumnInfoCombo(Strings.YesNo),
                    new ColumnInfoCombo(Strings.DiffBlocking),
                    new ColumnInfoCombo(Strings.ModesLightOsc),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.ModesLight),
                    new ColumnInfoCombo(Strings.YesNo)
                )

                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difensesUBDataGrid,
                            new TurnOffRule(1, Strings.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
                        )
                    }
                };

            NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> uMValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>(
                _difensesUMDataGrid,
                   4,
                   _toolTip,
                   new ColumnInfoCombo(Strings.UMStages, ColumnsType.NAME),
                   new ColumnInfoCombo(Strings.ModesLightMode),
                   new ColumnInfoCombo(Strings.UMType),
                   new ColumnInfoText(RulesContainer.Ustavka256),
                   new ColumnInfoText(RulesContainer.TimeRule),
                   new ColumnInfoText(RulesContainer.TimeRule),
                   new ColumnInfoText(RulesContainer.Ustavka256),
                   new ColumnInfoCheck(),
                   new ColumnInfoCombo(Strings.YesNo),
                   new ColumnInfoCombo(Strings.DiffBlocking),
                   new ColumnInfoCombo(Strings.ModesLightOsc),
                   new ColumnInfoCombo(Strings.ModesLight),
                   new ColumnInfoCombo(Strings.ModesLight),
                   new ColumnInfoCombo(Strings.ModesLight),
                   new ColumnInfoCombo(Strings.ModesLight),
                   new ColumnInfoCombo(Strings.YesNo)
               )

                {
                    TurnOff = new[]
                   {
                                 new TurnOffDgv
                                 (
                                     this._difensesUMDataGrid,
                                     new TurnOffRule(1, Strings.ModesLightMode[0], true,
                                         2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
                                 )
                             }
                };

            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> defFValidatior = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>(
                _difensesFBDataGrid,
                4,
                _toolTip,
                new ColumnInfoCombo(Strings.FBStages, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.ModesLightMode),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.DiffBlocking),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.YesNo)
                )

                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._difensesFBDataGrid,
                            new TurnOffRule(1, Strings.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                    }
                };

            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> deffFMValidatior = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>(
                _difensesFMDataGrid,
                4,
                _toolTip,
                new ColumnInfoCombo(Strings.FMStages, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.ModesLightMode),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.DiffBlocking),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.YesNo)
            )

            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                    (
                        this._difensesFMDataGrid,
                        new TurnOffRule(1, Strings.ModesLightMode[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                    )
                }
            };

            NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> deffExtValidatior = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>(
                _externalDifensesDataGrid,
                16,
                _toolTip,
                new ColumnInfoCombo(Strings.ExternalStages, ColumnsType.NAME),
                new ColumnInfoCombo(Strings.ModesLightMode),
                new ColumnInfoCombo(Strings.SygnalSrab),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCombo(Strings.SygnalSrab),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(Strings.SygnalSrab),
                new ColumnInfoCombo(Strings.ModesLightOsc),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.ModesLight),
                new ColumnInfoCombo(Strings.YesNo)
                )

                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._externalDifensesDataGrid,
                            new TurnOffRule(1, Strings.ModesLightMode[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                    }
                };

            if (this._vers > 1)
            {
                this._windingRatedPowerS1TextBox.Visible = false;
                this._windingRatedPowerS2TextBox.Visible = false;
                this._windingRatedPowerS3TextBox.Visible = false;
                this._windingRatedPowerS1TextBoxDOUBLE.Visible = true;
                this._windingRatedPowerS2TextBoxDOUBLE.Visible = true;
                this._windingRatedPowerS3TextBoxDOUBLE.Visible = true;
            }
            else
            {
                this._windingRatedPowerS1TextBox.Visible = true;
                this._windingRatedPowerS2TextBox.Visible = true;
                this._windingRatedPowerS3TextBox.Visible = true;
                this._windingRatedPowerS1TextBoxDOUBLE.Visible = false;
                this._windingRatedPowerS2TextBoxDOUBLE.Visible = false;
                this._windingRatedPowerS3TextBoxDOUBLE.Visible = false;
            }

            if (this._vers >= 1.11)
            {
                this._fault1Label.Text = "1. Аппаратная неисправность";
                this._fault1Label.Location = new Point(6, 25);
                this._fault2Label.Text = "2. Программная неисправность";
                this._fault2Label.Location = new Point(6, 47);
                this._fault3Label.Text = "3. Неисправность измерений";
                this._fault3Label.Location = new Point(6, 69);
                this._fault4Label.Text = "4. Неисправность выключателя";
                this._fault4Label.Location = new Point(6, 91);
                this.tabPage26.Text = "Защ. F>";
                this.tabPage27.Text = "Защ. F<";
                this._device.RefreshAdrSpace1_11();
                this._i0I0Column.HeaderText = "I*";
                this.label77.Visible = true;
                this.label52.Visible = true;
                this.label128.Visible = true;
                this.label129.Visible = true;
                this.label130.Visible = true;
                this.label131.Visible = true;
                this._UROVDTOBTComboBox.Visible = true;
                this._UROVDTZComboBox.Visible = true;
                this._APVDTOBTComboBox.Visible = true;
                this._APVDTZComboBox.Visible = true;
                this._AVRDTOBTComboBox.Visible = true;
                this._AVRDTZComboBox.Visible = true;
                this._dif0UrovColumn.Visible = true;
                this._dif0APVColumn.Visible = true;
                this._dif0AVRColumn.Visible = true;
                this._iUROVModeColumn.Visible = true;
                this._iAPVModeColumn.Visible = true;
                this._iAVRModeColumn.Visible = true;
                this._i0UROVColumn.Visible = true;
                this._i0APVColumn.Visible = true;
                this._i0AVRColumn.Visible = true;
                this._uBUROVColumn.Visible = true;
                this._uBAPVColumn.Visible = true;
                this._uBAVRColumn.Visible = true;
                this._uBAPVRetColumn.Visible = true;
                this._uBSbrosColumn.Visible = true;
                this._uMUROVColumn.Visible = true;
                this._uMAPVColumn.Visible = true;
                this._uMAVRColumn.Visible = true;
                this._uMAPVRetColumn.Visible = true;
                this._uMSbrosColumn.Visible = true;
                this._fBUROVColumn.Visible = true;
                this._fBAPVColumn.Visible = true;
                this._fBAVRColumn.Visible = true;
                this._fBAPVRetColumn.Visible = true;
                this._fBSbrosColumn.Visible = true;
                this._fMUROVColumn.Visible = true;
                this._fMAPVColumn.Visible = true;
                this._fMAVRColumn.Visible = true;
                this._fMAPVRetColumn.Visible = true;
                this._fMSbrosColumn.Visible = true;
                this._externalDifUROVColumn.Visible = true;
                this._externalDifAPVColumn.Visible = true;
                this._externalDifAVRColumn.Visible = true;
                this._externalDifAPVRetColumn.Visible = true;
                this._externalDifSbrosColumn.Visible = true;
                this.v1_11Panel.Visible = true;

                this._neispr4CB.Visible = true;
                this._fault4Label.Visible = true;

                this.oscopePanelVer1_11.Visible = true;
            }
            else
            {
                this.groupBox27.Visible = false;
                this.groupBox28.Visible = false;
                this.oscopePanelVer1.Visible = true;
                this._difensesTC.TabPages.Remove(this.tabPage26);
                this._difensesTC.TabPages.Remove(this.tabPage27);
            }
            
            this._outputIndicatorsGrid.Click += new EventHandler(this._outputIndicatorsGrid_Click);

            this.SubscribeLoadConfig();
            this.SubscribeOnSaveHandlers();

            if (this._vers > 1.10)
            {
                this.PrepareSwich();
                this.PrepareApv();
                this.PrepareAvr();
                this.PrepareLZH();
            }


            this.PrepareVLSSygnals();
            this.PreparePowerTransCombos();
            this.PrepareOutputSignals();
            this.PrepareConfigEthernet();
            this.PrepareMeasuringTransCombos();
            this.PrepareCurrentProtAll();
            this.PrepareCornersGrid();
            this.PrepareInpSygnals();
            this.PrepareInputSignals();
            this.PrepareOscilloscope();
        }

        private void SubscribeLoadConfig()
        {
            this._device.PowerTransLoadOK += new Handler(this._device_PowerTransLoadOK);
            this._device.PowerTransLoadFail += new Handler(this._device_PowerTransLoadFail);
            this._device.ConfigEthernetLoadOK += new Handler(this._device_ConfigEthernetLoadOK);
            this._device.ConfigEthernetLoadFail += new Handler(this._device_ConfigEthernetLoadFail);
            this._device.MeasureTransLoadOK += new Handler(this._device_MeasureTransLoadOK);
            this._device.MeasureTransLoadFail += new Handler(this._device_MeasureTransLoadFail);
            this._device.ParamAutomatLoadOK += new Handler(this._device_ParamAutomatLoadOK);
            this._device.ParamAutomatLoadFail += new Handler(this._device_ParamAutomatLoadFail);
            this._device.ElsSygnalLoadOK4 += new Handler(this._device_ElsSygnalLoadOK4);
            this._device.ElsSygnalSaveOK1 += new Handler(this._device_ElsSygnalSaveOK1);
            this._device.ElsSygnalSaveOK2 += new Handler(this._device_ElsSygnalSaveOK2);
            this._device.ElsSygnalSaveOK3 += new Handler(this._device_ElsSygnalSaveOK3);
            this._device.ElsSygnalSaveOK4 += new Handler(this._device_ElsSygnalSaveOK4);
            this._device.ElsSygnalSaveFail1 += new Handler(this._device_ElsSygnalSaveFail1);
            this._device.ElsSygnalSaveFail2 += new Handler(this._device_ElsSygnalSaveFail2);
            this._device.ElsSygnalSaveFail3 += new Handler(this._device_ElsSygnalSaveFail3);
            this._device.ElsSygnalSaveFail4 += new Handler(this._device_ElsSygnalSaveFail4);
            this._device.InpSygnalLoadOK += new Handler(this._device_InpSygnalLoadOK);
            this._device.InpSygnalLoadFail += new Handler(this._device_InpSygnalLoadFail);
            this._device.InpSygnalSaveOK += new Handler(this._device_InpSygnalSaveOK);
            this._device.InpSygnalSaveFail += new Handler(this._device_InpSygnalSaveFail);
            this._device.InputSygnalLoadOK += new Handler(this._device_InputSygnalLoadOK);
            this._device.InputSygnalLoadFail += new Handler(this._device_InputSygnalLoadFail);
            this._device.InputSygnalSaveOK += new Handler(this._device_InputSygnalSaveOK);
            this._device.InputSygnalSaveFail += new Handler(this._device_InputSygnalSaveFail);
            this._device.CurrentProtLastLoadOK += new Handler(this._device_CurrentProtAllLoadOK);
            this._device.CurrentProtLastLoadFail += new Handler(this._device_CurrentProtAllLoadFail);
            this._device.CurrentProtLastSaveOK += new Handler(this._device_CurrentProtAllSaveOK);
            this._device.CurrentProtLastSaveFail += new Handler(this._device_CurrentProtAllSaveFail);
            this._device.CurrentProtAllSaveOK += new Handler(this._device_CurrentProtAllSaveOK);
            this._device.CurrentProtAllLoadFail += new Handler(this._device_CurrentProtAllLoadFail);
            this._device.OscopeLoadOK += new Handler(this._device_OscopeLoadOK);
            this._device.OscopeLoadFail += new Handler(this._device_OscopeLoadFail);
            this._device.OscopeSaveOK += new Handler(this._device_OscopeSaveOK);
            this._device.OscopeSaveFail += new Handler(this._device_OscopeSaveFail);
            this._device.SwitchLoadOK += new Handler(this._device_SwitchLoadOK);
            this._device.SwitchLoadFail += new Handler(this._device_SwitchLoadFail);
            this._device.SwitchSaveOK += new Handler(this._device_SwitchSaveOK);
            this._device.SwitchSaveFail += new Handler(this._device_SwitchSaveFail);
            this._device.APVLoadOK += new Handler(this._device_APVLoadOK);
            this._device.APVLoadFail += new Handler(this._device_APVLoadFail);
            this._device.APVSaveOK += new Handler(this._device_APVSaveOK);
            this._device.APVSaveFail += new Handler(this._device_APVSaveFail);
            this._device.AVRLoadOK += new Handler(this._device_AVRLoadOK);
            this._device.AVRLoadFail += new Handler(this._device_AVRLoadFail);
            this._device.AVRSaveOK += new Handler(this._device_AVRSaveOK);
            this._device.AVRSaveFail += new Handler(this._device_AVRSaveFail);
            this._device.LPBLoadOK += new Handler(this._device_LPBLoadOK);
            this._device.LPBLoadFail += new Handler(this._device_LPBLoadFail);
            this._device.LPBSaveOK += new Handler(this._device_LPBSaveOK);
            this._device.LPBSaveFail += new Handler(this._device_LPBSaveFail);
            this._device.OscLengthLoadOK += this._device_OscLengthLoadOK;
        }
        private void UnsubscribeLoad()
        {
            this._device.PowerTransLoadOK -= this._device_PowerTransLoadOK;
            this._device.PowerTransLoadFail -= this._device_PowerTransLoadFail;
            this._device.ConfigEthernetLoadOK -= this._device_ConfigEthernetLoadOK;
            this._device.ConfigEthernetLoadFail -= this._device_ConfigEthernetLoadFail;
            this._device.MeasureTransLoadOK -= this._device_MeasureTransLoadOK;
            this._device.MeasureTransLoadFail -= this._device_MeasureTransLoadFail;
            this._device.ParamAutomatLoadOK -= this._device_ParamAutomatLoadOK;
            this._device.ParamAutomatLoadFail -= this._device_ParamAutomatLoadFail;
            this._device.ElsSygnalLoadOK4 -= this._device_ElsSygnalLoadOK4;
            this._device.ElsSygnalSaveOK1 -= this._device_ElsSygnalSaveOK1;
            this._device.ElsSygnalSaveOK2 -= this._device_ElsSygnalSaveOK2;
            this._device.ElsSygnalSaveOK3 -= this._device_ElsSygnalSaveOK3;
            this._device.ElsSygnalSaveOK4 -= this._device_ElsSygnalSaveOK4;
            this._device.ElsSygnalSaveFail1 -= this._device_ElsSygnalSaveFail1;
            this._device.ElsSygnalSaveFail2 -= this._device_ElsSygnalSaveFail2;
            this._device.ElsSygnalSaveFail3 -= this._device_ElsSygnalSaveFail3;
            this._device.ElsSygnalSaveFail4 -= this._device_ElsSygnalSaveFail4;
            this._device.InpSygnalLoadOK -= this._device_InpSygnalLoadOK;
            this._device.InpSygnalLoadFail -= this._device_InpSygnalLoadFail;
            this._device.InpSygnalSaveOK -= this._device_InpSygnalSaveOK;
            this._device.InpSygnalSaveFail -= this._device_InpSygnalSaveFail;
            this._device.InputSygnalLoadOK -= this._device_InputSygnalLoadOK;
            this._device.InputSygnalLoadFail -= this._device_InputSygnalLoadFail;
            this._device.InputSygnalSaveOK -= new Handler(this._device_InputSygnalSaveOK);
            this._device.InputSygnalSaveOK -= new Handler(this._device_InputSygnalSaveOK);
            this._device.InputSygnalSaveFail -= new Handler(this._device_InputSygnalSaveFail);
            this._device.CurrentProtLastLoadOK -= new Handler(this._device_CurrentProtAllLoadOK);
            this._device.CurrentProtLastLoadFail -= new Handler(this._device_CurrentProtAllLoadFail);
            this._device.CurrentProtLastSaveOK -= new Handler(this._device_CurrentProtAllSaveOK);
            this._device.CurrentProtLastSaveFail -= new Handler(this._device_CurrentProtAllSaveFail);
            this._device.CurrentProtAllSaveOK -= new Handler(this._device_CurrentProtAllSaveOK);
            this._device.CurrentProtAllLoadFail -= new Handler(this._device_CurrentProtAllLoadFail);
            this._device.OscopeLoadOK -= new Handler(this._device_OscopeLoadOK);
            this._device.OscopeLoadFail -= new Handler(this._device_OscopeLoadFail);
            this._device.OscopeSaveOK -= new Handler(this._device_OscopeSaveOK);
            this._device.OscopeSaveFail -= new Handler(this._device_OscopeSaveFail);
            this._device.SwitchLoadOK -= new Handler(this._device_SwitchLoadOK);
            this._device.SwitchLoadFail -= new Handler(this._device_SwitchLoadFail);
            this._device.SwitchSaveOK -= new Handler(this._device_SwitchSaveOK);
            this._device.SwitchSaveFail -= new Handler(this._device_SwitchSaveFail);
            this._device.APVLoadOK -= new Handler(this._device_APVLoadOK);
            this._device.APVLoadFail -= new Handler(this._device_APVLoadFail);
            this._device.APVSaveOK -= new Handler(this._device_APVSaveOK);
            this._device.APVSaveFail -= new Handler(this._device_APVSaveFail);
            this._device.AVRLoadOK -= new Handler(this._device_AVRLoadOK);
            this._device.AVRLoadFail -= new Handler(this._device_AVRLoadFail);
            this._device.AVRSaveOK -= new Handler(this._device_AVRSaveOK);
            this._device.AVRSaveFail -= new Handler(this._device_AVRSaveFail);
            this._device.LPBLoadOK -= new Handler(this._device_LPBLoadOK);
            this._device.LPBLoadFail -= new Handler(this._device_LPBLoadFail);
            this._device.LPBSaveOK -= new Handler(this._device_LPBSaveOK);
            this._device.LPBSaveFail -= new Handler(this._device_LPBSaveFail);
            this._device.OscLengthLoadOK -= this._device_OscLengthLoadOK;
        }

        private void SubscribeOnSaveHandlers()
        {
            this._device.PowerTransSaveOK += this.SaveOk;
            this._device.PowerTransSaveFail += this.SaveFail;

            this._device.ElsSygnalSaveOK4 += this.SaveOk;
            this._device.ElsSygnalSaveFail4 += this.SaveFail;

            this._device.MeasureTransSaveOK += this.SaveOk;
            this._device.MeasureTransSaveFail += this.SaveFail;

            this._device.ConfirmConstraintSaveOK += this.SaveComplete;
            this._device.ConfirmConstraintSaveFail += this.SaveComplete;

            this._device.ConfigEthernetSaveOK += this.SaveOk;
            this._device.ConfigEthernetSaveFail += this.SaveFail;
        }
        private void UnsubscriptOnSaveHandlers()
        {
            this._device.PowerTransSaveOK -= this.SaveOk;
            this._device.PowerTransSaveFail -= this.SaveFail;

            this._device.ElsSygnalSaveOK4 -= this.SaveOk;
            this._device.ElsSygnalSaveFail4 -= this.SaveFail;

            this._device.MeasureTransSaveOK -= this.SaveOk;
            this._device.MeasureTransSaveFail -= this.SaveFail;

            this._device.ConfirmConstraintSaveOK -= this.SaveComplete;
            this._device.ConfirmConstraintSaveFail -= this.SaveComplete;

            this._device.ConfigEthernetSaveOK += this.SaveOk;
            this._device.ConfigEthernetSaveFail += this.SaveFail;
        }

        #endregion


        #region Силовой трансформатор

        private void PreparePowerTransCombos()
        {
            ComboBox[] combos = new ComboBox[]
            {
                this._windingTypeS1ComboBox, this._measuringS1ComboBox, this._windingTypeS2ComboBox,
                this._connectionGroupS2ComboBox, this._measuringS2ComboBox, this._windingTypeS3ComboBox,
                this._connectionGroupS3ComboBox, this._measuringS3ComboBox
            };

            this._ULONGpowerTransMaskedTextBoxes = new MaskedTextBox[]
            {
                this._windingRatedPowerS1TextBox, this._windingRatedPowerS2TextBox, this._windingRatedPowerS3TextBox
            };
            this._DOUBLEpowerTransMaskedTextBoxes = new MaskedTextBox[]
            {
                this._windingRatedVoltageS1TextBox, this._windingRatedVoltageS2TextBox,
                this._windingRatedVoltageS3TextBox, this._windingRatedPowerS1TextBoxDOUBLE,
                this._windingRatedPowerS2TextBoxDOUBLE, this._windingRatedPowerS3TextBoxDOUBLE
            };

            this.ClearCombos(combos);
            this.FillPowerTransCombos(combos);

            this.PrepareMaskedBoxes(this._ULONGpowerTransMaskedTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLEpowerTransMaskedTextBoxes, typeof(double));
        }

        private void FillPowerTransCombos(ComboBox[] combos)
        {
            #region S1 Side Combos

            this._windingTypeS1ComboBox.Items.AddRange(Strings.WindingType.ToArray());
            this._measuringS1ComboBox.Items.AddRange(Strings.Measuring.ToArray());
            #endregion

            #region S2 Side Combos

            this._windingTypeS2ComboBox.Items.AddRange(Strings.WindingType.ToArray());
            this._connectionGroupS2ComboBox.Items.AddRange(Strings.ConnectionGroup.ToArray());
            this._measuringS2ComboBox.Items.AddRange(Strings.Measuring.ToArray());
            #endregion

            #region S3 Side Combos

            this._windingTypeS3ComboBox.Items.AddRange(Strings.WindingType.ToArray());
            this._connectionGroupS3ComboBox.Items.AddRange(Strings.ConnectionGroup.ToArray());
            this._measuringS3ComboBox.Items.AddRange(Strings.Measuring.ToArray());
            #endregion

            foreach (ComboBox combo in combos)
            {
                combo.SelectedIndex = 0;
            }
        }

        private void _device_PowerTransLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_PowerTransLoadOK(object sender)
        {
            try
            {

                Invoke(new OnDeviceEventHandler(this.ReadPowerTrans));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception ee)
            {
               
            }
        }

        private void _device_ConfigEthernetLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadConfigEthernet));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception e)
            {

            }
        }

        private void _device_ConfigEthernetLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void ReadRele()
        {
            var relays = this._device.Relays;
            for (int i = 0; i < UDZT.OUTPUT_RELAY_COUNT; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    this._outputReleGrid[j + 1, i].Value = relays[i][j];
                }
            }
        }

        private void ResetRele()
        {
            this._outputReleGrid.Rows.Clear();
            for (int i = 0; i < UDZT.OUTPUT_RELAY_COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(i + 1,"Повторитель","Нет",0);
            }
        }

        private void WriteRele()
        {
            var res = new string[UDZT.OUTPUT_RELAY_COUNT][];
            for (int i = 0; i < UDZT.OUTPUT_RELAY_COUNT; i++)
            {
                res[i] = new string[3];
                for (int j = 0; j < 3; j++)
                {
                   res[i][j]= this._outputReleGrid[j + 1, i].Value.ToString();
                }
            }
            this._device.Relays = res;
        }

        private void ReadIndicators()
        {
            var indicators = this._device.Indicators;
            for (int i = 0; i < UDZT.INDICATORS_COUNT; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (j == 2)
                    {
                        this._outputIndicatorsGrid[3, i].Style.BackColor = Color.FromName(indicators[i][j]);
                        this._outputIndicatorsGrid[3, i].Style.SelectionBackColor =
                            this._outputIndicatorsGrid[3, i].Style.BackColor;
                    }
                    else
                    {
                        this._outputIndicatorsGrid[j + 1, i].Value = indicators[i][j];
                    }

                }
            }
        }

        private void ResetIndicators()
        {
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < UDZT.INDICATORS_COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(i + 1, "Повторитель", "Нет", "");
                this._outputIndicatorsGrid.Rows[i].Cells["_outIndColorCol"].Style.BackColor = Color.Red;
            }
        }

        private void WriteIndicator()
        {
            var res = new string[UDZT.INDICATORS_COUNT][];
            for (int i = 0; i < UDZT.INDICATORS_COUNT; i++)
            {
                res[i] = new string[3];
                for (int j = 0; j < 3; j++)
                {
                    if (j == 2)
                    {
                        res[i][j] = this._outputIndicatorsGrid[3, i].Style.BackColor.ToKnownColor().ToString();
                    }
                    else
                    {
                       res[i][j] = this._outputIndicatorsGrid[j + 1, i].Value.ToString(); 
                    }
                    
                }
            }
            this._device.Indicators = res;
        }

        private void ReadNeispr() 
        {
            try
            {
                this._neispr1CB.SelectedItem = this._device.OutputN1;
                this._neispr2CB.SelectedItem = this._device.OutputN2;
                this._neispr3CB.SelectedItem = this._device.OutputN3;
                this._neispr4CB.SelectedItem = this._device.OutputN4;
                this._impTB.Text = this._device.OutputImp.ToString();
            }catch
            {

            }
        }

        private void ResetNeispr()
        {
            this._neispr1CB.SelectedIndex = 0;
            this._neispr2CB.SelectedIndex = 0;
            this._neispr3CB.SelectedIndex = 0;
            this._neispr4CB.SelectedIndex = 0;
            this._impTB.Text = 0.ToString();
        }

        private void ReadConfigEthernet()
        {
            this._ipLo1.Text = this._device.IpLo1.ToString();
            this._ipLo2.Text = this._device.IpLo2.ToString();
            this._ipHi1.Text = this._device.IpHi1.ToString();
            this._ipHi2.Text = this._device.IpHi2.ToString();
        }

        private void ReadPowerTrans()
        {
            if (!this._messagePower)
            {
                #region Сторона S1
                try
                {
                    
                    if (this._vers > 1)
                    {
                        this._windingRatedPowerS1TextBoxDOUBLE.Text = this._device.S1WindingRatedPowerDOUBLE.ToString("F1");
                    }
                    else
                    {
                        this._windingRatedPowerS1TextBox.Text = this._device.S1WindingRatedPower.ToString();
                    }
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Номинальная мощность обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingRatedVoltageS1TextBox.Text = this._device.S1WindingRatedVoltage.ToString(CultureInfo.CurrentCulture);
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingTypeS1ComboBox.SelectedItem = this._device.S1WindingType;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Тип обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._measuringS1ComboBox.SelectedItem = this._device.S1Measuring;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Измерение земля.", "Ошибка чтения");
                }
                #endregion

                #region Сторона S2
                try
                {
                    if (this._vers > 1)
                    {
                        this._windingRatedPowerS2TextBoxDOUBLE.Text = this._device.S2WindingRatedPowerDOUBLE.ToString("F1");
                    }
                    else
                    {
                        this._windingRatedPowerS2TextBox.Text = this._device.S2WindingRatedPower.ToString();
                    }
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Номинальная мощность обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingRatedVoltageS2TextBox.Text = this._device.S2WindingRatedVoltage.ToString(CultureInfo.CurrentCulture);
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingTypeS2ComboBox.SelectedItem = this._device.S2WindingType;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Тип обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._connectionGroupS2ComboBox.SelectedItem = this._device.S2ConnectionGroup;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Группа соединения.", "Ошибка чтения");
                }
                try
                {
                    this._measuringS2ComboBox.SelectedItem = this._device.S2Measuring;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Измерение земля.", "Ошибка чтения");
                }
                #endregion

                #region Сторона S3
                try
                {
                    if (this._vers > 1)
                    {
                        this._windingRatedPowerS3TextBoxDOUBLE.Text = this._device.S3WindingRatedPowerDOUBLE.ToString("F1");
                    }
                    else
                    {
                        this._windingRatedPowerS3TextBox.Text = this._device.S3WindingRatedPower.ToString();
                    }
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Номинальная мощность обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingRatedVoltageS3TextBox.Text = this._device.S3WindingRatedVoltage.ToString(CultureInfo.CurrentCulture);
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._windingTypeS3ComboBox.SelectedItem = this._device.S3WindingType;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Тип обмотки.", "Ошибка чтения");
                }
                try
                {
                    this._connectionGroupS3ComboBox.SelectedItem = this._device.S3ConnectionGroup;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Группа соединения.", "Ошибка чтения");
                }
                try
                {
                    this._measuringS3ComboBox.SelectedItem = this._device.S3Measuring;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._powTransPage);
                    MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Измерение земля.", "Ошибка чтения");
                }
                #endregion

                this._messagePower = true;
            }
        }
        
        private void ResetPowerTrans()
        {
            #region Сторона S1

            if (this._vers > 1)
            {
                this._windingRatedPowerS1TextBoxDOUBLE.Text = 0.ToString("F1");
            }
            else
            {
                this._windingRatedPowerS1TextBox.Text = 0.ToString();
            }
            this._windingRatedVoltageS1TextBox.Text = 0.ToString(CultureInfo.CurrentCulture);
            this._windingTypeS1ComboBox.SelectedIndex = 0;
            this._measuringS1ComboBox.SelectedIndex = 0;
            #endregion
            
            #region Сторона S2

            if (this._vers > 1)
            {
                this._windingRatedPowerS2TextBoxDOUBLE.Text = 0.ToString("F1");
            }
            else
            {
                this._windingRatedPowerS2TextBox.Text = 0.ToString();
            }
            this._windingRatedVoltageS2TextBox.Text = 0.ToString(CultureInfo.CurrentCulture);
            this._windingTypeS2ComboBox.SelectedIndex = 0;
            this._connectionGroupS2ComboBox.SelectedIndex = 0;
            this._measuringS2ComboBox.SelectedIndex = 0;
            #endregion
            
            #region Сторона S3

            if (this._vers > 1)
            {
                this._windingRatedPowerS3TextBoxDOUBLE.Text = 0.ToString("F1");
            }
            else
            {
                this._windingRatedPowerS3TextBox.Text = 0.ToString();
            }
            this._windingRatedVoltageS3TextBox.Text = 0.ToString(CultureInfo.CurrentCulture);
            this._windingTypeS3ComboBox.SelectedIndex = 0;
            this._connectionGroupS3ComboBox.SelectedIndex = 0;
            this._measuringS3ComboBox.SelectedIndex = 0;

            #endregion
        }

        private void WriteNeispr() 
        {
            try
            {
                this._device.OutputN1 = this._neispr1CB.SelectedItem.ToString();
                this._device.OutputN2 = this._neispr2CB.SelectedItem.ToString();
                this._device.OutputN3 = this._neispr3CB.SelectedItem.ToString();
                this._device.OutputN4 = this._neispr4CB.SelectedItem.ToString();
                this._device.OutputImp = Convert.ToInt32(this._impTB.Text);
            }
            catch
            {

            }
        }

        private bool WriteConfigEthernet()
        {
            bool ret = true;

            try
            {
                this._device.IpLo1 = Convert.ToInt32(this._ipLo1.Text);
                this._device.IpLo2 = Convert.ToInt32(this._ipLo2.Text);
                this._device.IpHi1 = Convert.ToInt32(this._ipHi1.Text);
                this._device.IpHi2 = Convert.ToInt32(this._ipHi2.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка записи конфигурации ethernet");
            }

            return ret;
        }

        private bool WriteParamAutomat()
        {
            bool ret = true;
            try
            {
                this.WriteNeispr();
                this.WriteRele();
                this.WriteIndicator();
            }
            catch (Exception rr)
            {
                ret = false;
            }
            return ret;
        }

        private bool WritePowerTrans()
        {
            bool ret = true;
            #region Сторона S1
            try
            {
                if (this._vers > 1)
                {
                    this._device.S1WindingRatedPowerDOUBLE = Convert.ToDouble(this._windingRatedPowerS1TextBoxDOUBLE.Text);
                }
                else 
                {
                    this._device.S1WindingRatedPower = Convert.ToInt32(this._windingRatedPowerS1TextBox.Text);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Номинальная мощность обмотки.", "Ошибка записи");
            }

            try
            {
                this._device.S1WindingRatedVoltage = Convert.ToDouble(this._windingRatedVoltageS1TextBox.Text);
               
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S1WindingType = this._windingTypeS1ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Тип обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S1Measuring = this._measuringS1ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S1: Некорректное значение : Измерение земля.", "Ошибка записи");
            }
            #endregion

            #region Сторона S2
            try
            {
                if (this._vers > 1)
                {
                    this._device.S2WindingRatedPowerDOUBLE = Convert.ToDouble(this._windingRatedPowerS2TextBoxDOUBLE.Text);
                }
                else
                {
                    this._device.S2WindingRatedPower = Convert.ToInt32(this._windingRatedPowerS2TextBox.Text);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Номинальная мощность обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S2WindingRatedVoltage = Convert.ToDouble(this._windingRatedVoltageS2TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S2WindingType = this._windingTypeS2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Тип обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S2ConnectionGroup = this._connectionGroupS2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Группа соединения.", "Ошибка записи");
            }
            try
            {
                this._device.S2Measuring = this._measuringS2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S2: Некорректное значение : Измерение земля.", "Ошибка записи");
            }
            #endregion

            #region Сторона S3
            try
            {
                if (this._vers > 1)
                {
                    this._device.S3WindingRatedPowerDOUBLE = Convert.ToDouble(this._windingRatedPowerS3TextBoxDOUBLE.Text);
                }
                else
                {
                    this._device.S3WindingRatedPower = Convert.ToInt32(this._windingRatedPowerS3TextBox.Text);
                }
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Номинальная мощность обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S3WindingRatedVoltage = Convert.ToDouble(this._windingRatedVoltageS3TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Номинальное напряжение обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S3WindingType = this._windingTypeS3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Тип обмотки.", "Ошибка записи");
            }
            try
            {
                this._device.S3ConnectionGroup = this._connectionGroupS3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Группа соединения.", "Ошибка записи");
            }
            try
            {
                this._device.S3Measuring = this._measuringS3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nСиловой трансформатор:\nСторона S3: Некорректное значение : Измерение земля.", "Ошибка записи");
            }
            #endregion

            bool measuringGroundOk1 = this._device.S1Measuring != this._device.S2Measuring                // нельзя на Измерение земля завести два
                                      || (this._device.S1Measuring == Strings.Measuring[0] &&        // одинаковых тока нулевой последовательность
                                          this._device.S2Measuring == Strings.Measuring[0]);
            bool measuringGroundOk2 = this._device.S1Measuring != this._device.S3Measuring
                                      || (this._device.S1Measuring == Strings.Measuring[0] && this._device.S3Measuring == Strings.Measuring[0]);
            bool measuringGroundOk3 = this._device.S2Measuring != this._device.S3Measuring
                                      || (this._device.S3Measuring == Strings.Measuring[0] && this._device.S2Measuring == Strings.Measuring[0]);
            if (!measuringGroundOk1 || !measuringGroundOk2 || !measuringGroundOk3)
            {
                ret = false;
                MessageBox.Show(
                    "В конфигурации силового трансформатора в полях 'Измерение земля' не может быть одинаковых значений, кроме значения 'Нет'",
                    "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            return ret;
        }

        #endregion

        #region Выходные сигналы

        #region ВЛС
        private void PrepareVLSSygnals()
        {
            this._VLSChectboxlistArray = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8,
                this.VLScheckedListBox9, this.VLScheckedListBox10, this.VLScheckedListBox11, this.VLScheckedListBox12,
                this.VLScheckedListBox13, this.VLScheckedListBox14, this.VLScheckedListBox15, this.VLScheckedListBox16
            };

            foreach (CheckedListBox box in this._VLSChectboxlistArray.Where(box => box.Items.Count == 0))
            {
                box.Items.AddRange(Strings.VLSSygnals.ToArray());
            }
        }

        private bool WriteVLSSygnals()
        {
            bool res = true;
            try
            {
                var result = new bool[16][];
                for (int i = 0; i < 16; i++)
                {
                    result[i] = new bool[Strings.VLSSygnals.Count];
                    for (int j = 0; j < Strings.VLSSygnals.Count; j++)
                    {
                        result[i][j] = this._VLSChectboxlistArray[i].GetItemChecked(j);
                    }
                }
                this._device.Vls = result;
            }
            catch (Exception)
            {
                res = false;
            }
            return res;
        }

        private void ReadVLSSygnals()
        {
            bool[][] vls = this._device.Vls;
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < Strings.VLSSygnals.Count; j++)
                {
                    this._VLSChectboxlistArray[i].SetItemChecked(j, vls[i][j]);
                }
            }
        }

        private void ResetVLSSygnals()
        {
            foreach (CheckedListBox checkedListBox in this._VLSChectboxlistArray)
            {
                for (int i = 0; i < checkedListBox.Items.Count; i++)
                {
                    checkedListBox.SetItemChecked(i, false);
                }
            }
        }

        private void _device_ElsSygnalLoadOK4(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadVLSSygnals));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception)
            {

            }
        }
        
        private void _device_ElsSygnalSaveOK4(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveOK3(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveOK2(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveOK1(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }
        

        private void _device_ElsSygnalSaveFail4(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveFail3(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveFail2(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ElsSygnalSaveFail1(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }
        #endregion

        private void PrepareOutputSignals()
        {
            this._ulongrRelayNeisprTImpTextBoxes = new MaskedTextBox[]{this._impTB};
            this.PrepareMaskedBoxes(this._ulongrRelayNeisprTImpTextBoxes, typeof(ulong));
            this._outputReleGrid.Rows.Clear();
            this._outputIndicatorsGrid.Rows.Clear();
            this._releSignalCol.Items.Clear();
            this._releTypeCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            this._outIndTypeCol.Items.Clear();

            this._releSignalCol.Items.AddRange(Strings.Sygnal.ToArray());
            this._releTypeCol.Items.AddRange(Strings.SygnalType.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.Sygnal.ToArray());
            this._outIndTypeCol.Items.AddRange(Strings.SygnalType.ToArray());

            for (int i = 0; i < UDZT.OUTPUT_RELAY_COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(i + 1, "Повторитель", "Нет", 0);
            }
            this._outputReleGrid.CellBeginEdit += this.OnGridCellBeginEdit;
            for (int i = 0; i < UDZT.INDICATORS_COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(i + 1, "Повторитель", "Нет", "");
                this._outputIndicatorsGrid.Rows[i].Cells["_outIndColorCol"].Style.BackColor = Color.Red;
            }
            this._outputIndicatorsGrid.CellBeginEdit += this.OnGridCellBeginEdit;
            this._neispr1CB.SelectedIndex = 0;
            this._neispr2CB.SelectedIndex = 0;
            this._neispr3CB.SelectedIndex = 0;
            this._neispr4CB.SelectedIndex = 0;
        }

        private void _outputIndicatorsGrid_Click(object sender, EventArgs e)
        {
            if (this._outputIndicatorsGrid.CurrentCell.ColumnIndex == 3)
            {
                if (this._outputIndicatorsGrid.CurrentCell.Style.BackColor == Color.Red)
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Green;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
                else
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Red;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
            }
        }
        #endregion

        #region Входные сигналы

        #region И Или

        private void PrepareInpSygnals()
        {
            this._inpSygnalsArray = new DataGridView[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals5,
                this._inputSignals6, this._inputSignals7, this._inputSignals8, this._inputSignals9, this._inputSignals10,
                this._inputSignals11, this._inputSignals12, this._inputSignals13, this._inputSignals14,
                this._inputSignals15, this._inputSignals16
            };

            for (int j = 0; j < this._inpSygnalsArray.Length; j++)
            {
                if (this._inpSygnalsArray[j].Rows.Count == 0)
                {
                    for (int i = 0; i < Strings.LogycSygnals.Count; i++)
                    {

                        if (this._signalValueCol.Items.Count == 0)
                        {
                            this._signalValueCol.Items.AddRange(Strings.LogycValues.ToArray());
                        }
                        this._inpSygnalsArray[j].Rows.Add("Д" + (i + 1), Strings.LogycValues[0]);
                    }
                }
                this._inpSygnalsArray[j].CellBeginEdit += this.OnGridCellBeginEdit;
            }
        }

        private void ReadLogycSygnals()
        {
         
           var inpSygnals = this._device.InpSygnals;
            for (int j = 0; j < this._inpSygnalsArray.Length; j++)
            {
            
                for (int i = 0; i < 24; i++)
                {
                    this._inpSygnalsArray[j][1, i].Value = inpSygnals[j][i];
                }
                      
            }
        }

        private void ResetLogycSygnals()
        {
            foreach (DataGridView grid in this._inpSygnalsArray)
            {
                grid.Rows.Clear();
                for (int i = 0; i < Strings.LogycSygnals.Count; i++)
                {

                    if (this._signalValueCol.Items.Count == 0)
                    {
                        this._signalValueCol.Items.AddRange(Strings.LogycValues);
                    }
                    grid.Rows.Add("Д" + (i + 1), Strings.LogycValues[0]);
                }
            }
        }

        private bool WriteLogycSygnals()
        {
            bool ret = true;
            try
            {
                var res = new string[16][];
                for (int j = 0; j < this._inpSygnalsArray.Length; j++)
                {
                 res[j] = new string[24];
                    for (int i = 0; i < 24; i++)
                    {
                      res[j][i] = this._inpSygnalsArray[j][1, i].Value.ToString() ;
                    }

                }
                this._device.InpSygnals = res;
            }
            catch (Exception ee)
            {
                ret = false;
            }
            return ret;
        }

        private void _device_InpSygnalSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_InpSygnalSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_InpSygnalLoadFail(object sender)
        {
        }

        private void _device_InpSygnalLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadLogycSygnals));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception ee) { }
        }
        #endregion

        #region Группа уставок и Сброс индикации
        private void PrepareInputSignals()
        {
            if (this._grUstComboBox.Items.Count == 0)
            {
                this._grUstComboBox.Items.AddRange(Strings.SygnalInputSignals.ToArray());
                this._grUstComboBox.SelectedIndex = 0;
            }
            else
            {
                this._grUstComboBox.SelectedIndex = 0;
            }
            if (this._indComboBox.Items.Count == 0)
            {
                this._indComboBox.Items.AddRange(Strings.SygnalInputSignals.ToArray());
                this._indComboBox.SelectedIndex = 0;
            }
            else
            {
                this._indComboBox.SelectedIndex = 0;
            }
        }

        private void ReadInputSignals()
        {
            this._grUstComboBox.Text = this._device.GrUst;
            this._indComboBox.Text = this._device.SbInd;
        }

        private void ResetInputSignals()
        {
            this._grUstComboBox.SelectedIndex = 0;
            this._indComboBox.SelectedIndex = 0;
        }

        private bool WriteInputSignals()
        {
            bool ret = true;
            try
            {
                this._device.GrUst = this._grUstComboBox.SelectedItem.ToString();
                this._device.SbInd = this._indComboBox.SelectedItem.ToString();
            }
            catch (Exception ee)
            {
                ret = false;
            }
            return ret;
        }

        private void _device_InputSygnalSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee) { }
        }

        private void _device_InputSygnalSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee) { }
        }

        private void _device_InputSygnalLoadFail(object sender)
        {
        }

        private void _device_InputSygnalLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadInputSignals));
            }
            catch (Exception ee) { }
        }
        #endregion

        #endregion Входные сигналы

        private void PrepareConfigEthernet()
        {
            this._USHORTconfigEthernetMaskedTextBoxes = new MaskedTextBox[]
            {
                this._ipLo1, this._ipLo2, this._ipHi1, this._ipHi2
            };
            this.PrepareMaskedBoxes(this._USHORTconfigEthernetMaskedTextBoxes, typeof(ulong));
        }

        #region Данные измерительных трансформаторов

        private void PrepareMeasuringTransCombos()
        {
            ComboBox[] combos = new ComboBox[]
            {
                this._bindingsL1ComboBox, this._polarityOfConnectionL1ComboBox, this._polarityOfConnectionX1ComboBox,
                this._bindingsL2ComboBox, this._polarityOfConnectionL2ComboBox, this._polarityOfConnectionX2ComboBox,
                this._bindingsL3ComboBox, this._polarityOfConnectionL3ComboBox, this._polarityOfConnectionX3ComboBox,
                this._bindingsTHLXComboBox, this._TH_LKoeff_ComboBox, this._TH_XKoeff_ComboBox, this._polarityLCB,
                this._polarityXCB
            };
            this._ULONGmeasureTransMaskedTextBoxes = new MaskedTextBox[] {this._TT_L1_TextBox, this._TT_X1_TextBox, this._TT_L2_TextBox, this._TT_X2_TextBox, this._TT_L3_TextBox, this._TT_X3_TextBox };
            this._DOUBLEmeasureTransMaskedTextBoxes = new MaskedTextBox[] {this._TH_L_TextBox, this._TH_X_TextBox };

            this.ClearCombos(combos);
            this.FillMeasuringTransCombos(combos);

            this.PrepareMaskedBoxes(this._ULONGmeasureTransMaskedTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLEmeasureTransMaskedTextBoxes, typeof(double));
        }

        private void FillMeasuringTransCombos(ComboBox[] combos)
        {
            #region L1,X1 Combos

            this._bindingsL1ComboBox.Items.AddRange(Strings.Bindings.ToArray());
            this._polarityOfConnectionL1ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            this._polarityOfConnectionX1ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            #endregion

            #region L2,X2 Combos

            this._bindingsL2ComboBox.Items.AddRange(Strings.Bindings.ToArray());
            this._polarityOfConnectionL2ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            this._polarityOfConnectionX2ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            #endregion

            #region L3,X3 Combos

            this._bindingsL3ComboBox.Items.AddRange(Strings.Bindings.ToArray());
            this._polarityOfConnectionL3ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            this._polarityOfConnectionX3ComboBox.Items.AddRange(Strings.PolarityOfConnection.ToArray());
            #endregion

            #region L,X Combos

            this._TH_LKoeff_ComboBox.Items.AddRange(new object[] { "1", "1000" });
            this._TH_XKoeff_ComboBox.Items.AddRange(new object[] { "1", "1000" });
            this._polarityLCB.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._polarityXCB.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._bindingsTHLXComboBox.Items.AddRange(Strings.BindingLX.ToArray());
            #endregion

            foreach (ComboBox cb in combos)
            {
                cb.SelectedIndex = 0;
            }
        }

        private void _device_MeasureTransLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_MeasureTransLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadMeasureTrans));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception ee)
            {

            }
        }

        private void ReadMeasureTrans()
        {
            if (!this._messageMeasure)
            {
                try
                {
                    this._TT_L1_TextBox.Text = this._device.TT_L1.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L1.", "Ошибка чтения");
                }
                try
                {
                    this._bindingsL1ComboBox.SelectedItem = this._device.TT_L1_Binding;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L1.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionL1ComboBox.SelectedItem = this._device.TT_L1_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L1.", "Ошибка чтения");
                }
                try
                {
                    this._TT_X1_TextBox.Text = this._device.TT_X1.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X1.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionX1ComboBox.SelectedItem = this._device.TT_X1_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X1.", "Ошибка чтения");
                }

                try
                {
                    this._TT_L2_TextBox.Text = this._device.TT_L2.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L2.", "Ошибка чтения");
                }
                try
                {
                    this._bindingsL2ComboBox.SelectedItem = this._device.TT_L2_Binding;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L2.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionL2ComboBox.SelectedItem = this._device.TT_L2_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L2.", "Ошибка чтения");
                }
                try
                {
                    this._TT_X2_TextBox.Text = this._device.TT_X2.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X2.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionX2ComboBox.SelectedItem = this._device.TT_X2_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X2.", "Ошибка чтения");
                }

                try
                {
                    this._TT_L3_TextBox.Text = this._device.TT_L3.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L3.", "Ошибка чтения");
                }
                try
                {
                    this._bindingsL3ComboBox.SelectedItem = this._device.TT_L3_Binding;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L3.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionL3ComboBox.SelectedItem = this._device.TT_L3_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L3.", "Ошибка чтения");
                }
                try
                {
                    this._TT_X3_TextBox.Text = this._device.TT_X3.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X3.", "Ошибка чтения");
                }
                try
                {
                    this._polarityOfConnectionX3ComboBox.SelectedItem = this._device.TT_X3_Polarity;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X3.", "Ошибка чтения");
                }
                try
                {
                    this._TH_L_TextBox.Text = this._device.TH_L.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH L.", "Ошибка чтения");
                }
                try
                {
                    this._TH_LKoeff_ComboBox.SelectedItem = this._device.TH_LKoef.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH L.", "Ошибка чтения");
                }
                try
                {
                    this._TH_X_TextBox.Text = this._device.TH_X.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH X.", "Ошибка чтения");
                }
                try
                {
                    this._TH_XKoeff_ComboBox.SelectedItem = this._device.TH_XKoef.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH X.", "Ошибка чтения");
                }
                try
                {
                    this._polarityLCB.SelectedItem = this._device.PolarityL;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Неисправность L.", "Ошибка чтения");
                }
                try
                {
                    this._polarityXCB.SelectedItem = this._device.PolarityX;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Неисправность X.", "Ошибка чтения");
                }
                try
                {
                    this._bindingsTHLXComboBox.SelectedItem = this._device.TH_LX_Binding;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._measureTransPage);
                    MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Uo.", "Ошибка чтения");
                }

                this._messageMeasure = true;
            }
        }

        private void ResetMeasureTrans()
        {
            this._TT_L1_TextBox.Text = 0.ToString();
            this._bindingsL1ComboBox.SelectedIndex = 0;
            this._polarityOfConnectionL1ComboBox.SelectedIndex = 0;
            this._TT_X1_TextBox.Text = 0.ToString();
            this._polarityOfConnectionX1ComboBox.SelectedIndex = 0;
            this._TT_L2_TextBox.Text = 0.ToString();
            this._bindingsL2ComboBox.SelectedIndex = 0;
            this._polarityOfConnectionL2ComboBox.SelectedIndex = 0;
            this._TT_X2_TextBox.Text = 0.ToString();
            this._polarityOfConnectionX2ComboBox.SelectedIndex = 0;
            this._TT_L3_TextBox.Text = 0.ToString();
            this._bindingsL3ComboBox.SelectedIndex = 0;
            this._polarityOfConnectionL3ComboBox.SelectedIndex = 0;
            this._TT_X3_TextBox.Text = 0.ToString();
            this._polarityOfConnectionX3ComboBox.SelectedIndex = 0;
            this._TH_L_TextBox.Text = 0.ToString();
            this._TH_LKoeff_ComboBox.SelectedIndex = 0;
            this._TH_X_TextBox.Text = 0.ToString();
            this._TH_XKoeff_ComboBox.SelectedIndex = 0;
            this._polarityLCB.SelectedIndex = 0;
            this._polarityXCB.SelectedIndex = 0;
            this._bindingsTHLXComboBox.SelectedIndex = 0;
        }

        private bool WriteMeasureTrans()
        {
            bool ret = true;

            try
            {
                this._device.TT_L1 = Convert.ToInt32(this._TT_L1_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L1.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L1_Binding = this._bindingsL1ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L1.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L1_Polarity = this._polarityOfConnectionL1ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L1.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X1 = Convert.ToInt32(this._TT_X1_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X1.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X1_Polarity = this._polarityOfConnectionX1ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X1.", "Ошибка записи");
            }

            try
            {
                this._device.TT_L2 = Convert.ToInt32(this._TT_L2_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L2.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L2_Binding = this._bindingsL2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L2.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L2_Polarity = this._polarityOfConnectionL2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L2.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X2 = Convert.ToInt32(this._TT_X2_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X2.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X2_Polarity = this._polarityOfConnectionX2ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X2.", "Ошибка записи");
            }

            try
            {
                this._device.TT_L3 = Convert.ToInt32(this._TT_L3_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT L3.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L3_Binding = this._bindingsL3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Привязка TT L3.", "Ошибка записи");
            }
            try
            {
                this._device.TT_L3_Polarity = this._polarityOfConnectionL3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT L3.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X3 = Convert.ToInt32(this._TT_X3_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Номинальный первичный ток TT X3.", "Ошибка записи");
            }
            try
            {
                this._device.TT_X3_Polarity = this._polarityOfConnectionX3ComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Полярность подключения TT X3.", "Ошибка записи");
            }
            try
            {
                this._device.TH_L = Convert.ToDouble(this._TH_L_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH L.", "Ошибка записи");
            }
            try
            {
                this._device.TH_LKoef = Convert.ToInt32(this._TH_LKoeff_ComboBox.SelectedItem.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH L.", "Ошибка записи");
            }
            try
            {
                this._device.TH_X = Convert.ToDouble(this._TH_X_TextBox.Text);
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH X.", "Ошибка записи");
            }
            try
            {
                this._device.TH_XKoef = Convert.ToInt32(this._TH_XKoeff_ComboBox.SelectedItem.ToString());
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Коэффициент трансформации TH X.", "Ошибка записи");
            }
            try
            {
                this._device.PolarityL = this._polarityLCB.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Неисправность L.", "Ошибка записи");
            }
            try
            {
                this._device.PolarityX = this._polarityXCB.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Неисправность X.", "Ошибка записи");
            }
            try
            {
                this._device.TH_LX_Binding = this._bindingsTHLXComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show(ex.Message + "\n\nДанные измерительных трансформаторов:\nНекорректное значение : Uo.", "Ошибка записи");
            }

            return ret;
        }

        #endregion

        #region Все защиты

        #region Обработчики событий

        private void _device_CurrentProtAllSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
                Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_CurrentProtAllSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_CurrentProtAllLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (Exception) { }
        }

        private void _device_CurrentProtAllLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadCurrentProtAll));
                Invoke(new OnDeviceEventHandler(this.OnLoadComplete));
            }
            catch (Exception) { }
        }

        private void OnCellValueChanged(object sender, DataGridViewCellEventArgs args)
        {
            if (args.ColumnIndex == 1)
            {
                DataGridView dgv = sender as DataGridView;
                int[] indexes = new int[dgv.Columns.Count - 2];
                for (int i = 2; i < dgv.Columns.Count; i++)
                {
                    indexes[i - 2] = i;
                }
                GridManager.ChangeCellDisabling(dgv, args.RowIndex, Strings.ModesLightMode[0], args.ColumnIndex, indexes);
            }
        }
        #endregion

        private void PrepareCurrentProtAll()
        {
            this.PrepareCornersGrid();
            this.PrepareDiffPowerCombos();
            this.PrepareDif0Grid();
            this.PrepareIGrid();
            this.PrepareI0Grid();
            this.PrepareUBGrid();
            this.PrepareUMGrid();
            this.PrepareFBGrid();
            this.PrepareFMGrid();
            this.PrepareExternalDifensesGrid();
        }

        private void ReadCurrentProtAll()
        {
            #region основные/резервные уставки
            ushort[] curProtAll;
            if (this._mainRadioButton.Checked)
            {
                this._ust = "Основные уставки";
                curProtAll = this._device.CurrentProtAllMainProperty;
            }
            else
            {
                this._ust = "Резервные уставки";
                curProtAll = this._device.CurrentProtAllReservProperty;
            }
            #endregion

            #region Углы
            int index = 0;
            for (int i = 0; i < 3; i++)
            {
                this._cornersGridView.Rows[i].Cells[0].Value = "S" + (i + 1);
                for (int j = 0; j < 4; j++)
                {
                    this._cornersGridView.Rows[i].Cells[j + 1].Value = curProtAll[index].ToString();
                    index++;
                }
                //  index++;
            }
            #endregion

            int _currentIndex = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE)) / 2;

            #region Диф Защиты
            ushort[] _DifArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFMAIN)) / 2 + System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFCUTOFF)) / 2];
            Array.ConstrainedCopy(curProtAll, _currentIndex, _DifArray, 0, _DifArray.Length);

            this._device.ArrayDifMainCutOf = _DifArray;

            this._messageDifPower = false;
            this.ReadDifPower();
            #endregion

            _currentIndex += _DifArray.Length;

            #region Dif0
            for (int dif0Ind = 1; dif0Ind <= 3; dif0Ind++)
            {
                ushort[] dif0Array = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFI0)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, dif0Array, 0, dif0Array.Length);
                this._device.Dif0Array = dif0Array;
                _currentIndex += dif0Array.Length;
                this.ReadDif0(dif0Ind);
            }
            #endregion

            //_currentIndex уже посчитан

            #region Защиты I
            for (int _difIInd = 1; _difIInd <= 8; _difIInd++)
            {
                ushort[] _DifIArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, _DifIArray, 0, _DifIArray.Length);
                this._device.DifIArray = _DifIArray;
                _currentIndex += _DifIArray.Length;
                this.ReadDifI(_difIInd);
            }
            #endregion

            //_currentIndex уже посчитан

            #region Защиты I0
            for (int _difI0Ind = 1; _difI0Ind <= 6; _difI0Ind++)
            {
                ushort[] _DifI0Array = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, _DifI0Array, 0, _DifI0Array.Length);
                this._device.DifI0Array = _DifI0Array;
                _currentIndex += _DifI0Array.Length;
                this.ReadDifI0(_difI0Ind);
            }
            #endregion

            //_currentIndex уже посчитан

            #region Защиты U>
            for (int _difUBInd = 1; _difUBInd <= 4; _difUBInd++)
            {
                ushort[] _DifUBArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, _DifUBArray, 0, _DifUBArray.Length);
                this._device.DifUBArray = _DifUBArray;
                _currentIndex += _DifUBArray.Length;
                this.ReadDifUB(_difUBInd);
            }
            #endregion

            //_currentIndex уже посчитан

            #region Защиты U<
            for (int _difUMInd = 1; _difUMInd <= 4; _difUMInd++)
            {
                ushort[] _DifUMArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, _DifUMArray, 0, _DifUMArray.Length);
                this._device.DifUMArray = _DifUMArray;
                _currentIndex += _DifUMArray.Length;
                this.ReadDifUM(_difUMInd);
            }
            #endregion

            if (this._vers >= 1.11) 
            {
                //_currentIndex уже посчитан

                #region Защиты F>
                for (int _difFBInd = 1; _difFBInd <= 4; _difFBInd++)
                {
                    ushort[] _DifFBArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
                    Array.ConstrainedCopy(curProtAll, _currentIndex, _DifFBArray, 0, _DifFBArray.Length);
                    this._device.DifFBArray = _DifFBArray;
                    _currentIndex += _DifFBArray.Length;
                    this.ReadDifFB(_difFBInd);
                }
                #endregion

                //_currentIndex уже посчитан

                #region Защиты F<
                for (int _difFMInd = 1; _difFMInd <= 4; _difFMInd++)
                {
                    ushort[] _DifFMArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
                    Array.ConstrainedCopy(curProtAll, _currentIndex, _DifFMArray, 0, _DifFMArray.Length);
                    this._device.DifFMArray = _DifFMArray;
                    _currentIndex += _DifFMArray.Length;
                    this.ReadDifFM(_difFMInd);
                }
                #endregion
            }


            _currentIndex += System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ)) / 2 * 2;

            #region Защиты внешние
            for (int _extDifInd = 1; _extDifInd <= 16; _extDifInd++)
            {
                ushort[] _ExtDifArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
                Array.ConstrainedCopy(curProtAll, _currentIndex, _ExtDifArray, 0, _ExtDifArray.Length);
                this._device.ExtDifArray = _ExtDifArray;
                _currentIndex += _ExtDifArray.Length;
                this.ReadExtDif(_extDifInd);
            }
            #endregion
        }

        private void ResetCurrentProtAll()
        {
            this._mainRadioButton.Checked = true;
            this._ust = "Основные уставки";
            
            #region Углы
            this._cornersGridView.Rows.Clear();
            if (this._cornersGridView.Rows.Count == 0)
            {
                this._cornersGridView.Rows.Add(3);
            }
            for (int i = 0; i < 3; i++)
            {
                this._cornersGridView.Rows[i].Cells[0].Value = "S" + (i + 1);
                for (int j = 0; j < 4; j++)
                {
                    this._cornersGridView.Rows[i].Cells[j + 1].Value = 0.ToString();
                }
            }
            #endregion
            
            #region Диф Защиты
            this._modeDTOBTComboBox.SelectedIndex = 0;
            this._stepOnInstantValuesDTOBTComboBox.SelectedIndex = 0;
            this._constraintDTOBTTextBox.Text = 0.ToString();
            this._timeEnduranceDTOBTTextBox.Text = this._device.DTOBTTimeEndurance.ToString();
            this._blockingDTOBTComboBox.SelectedIndex = 0;
            this._UROVDTOBTComboBox.SelectedIndex = 0;
            this._oscDTOBTComboBox.SelectedIndex = 0;
            this._APVDTOBTComboBox.SelectedIndex = 0;
            this._AVRDTOBTComboBox.SelectedIndex = 0;
            this._modeDTZComboBox.SelectedIndex = 0;
            this._constraintDTZTextBox.Text = 0.ToString();
            this._timeEnduranceDTZTextBox.Text = 0.ToString();
            this._Ib1BeginTextBox.Text = 0.ToString();
            this._K1AngleOfSlopeTextBox.Text = 0.ToString();
            this._Ib2BeginTextBox.Text = 0.ToString();
            this._K2TangensTextBox.Text = 0.ToString();
            this._I2I1TextBox.Text = 0.ToString();
            this._modeI2I1CB.Text = 0.ToString();
            this._perBlockI2I1.Text = 0.ToString();
            this._I5I1TextBox.Text = 0.ToString();
            this._modeI5I1CB.Text = 0.ToString();
            this._perBlockI5I1.Text = 0.ToString();
            this._blockingDTZComboBox.SelectedIndex = 0;
            this._UROVDTZComboBox.SelectedIndex = 0;
            this._oscDTZComboBox.SelectedIndex = 0;
            this._APVDTZComboBox.SelectedIndex = 0;
            this._AVRDTZComboBox.SelectedIndex = 0;
            #endregion

            #region Dif0

            this._dif0DataGreed.Rows.Clear();
            for (int i = 0; i < 3; i++)
            {
                this._dif0DataGreed.Rows.Add(
                    "Ступень Iд0> " + (i + 1),
                    Strings.ModesLightMode[0],
                    Strings.DiffBlocking[0],
                    0,
                    Strings.MeasuringLight[0],
                    0,
                    0,
                    0,
                    0,
                    0,
                    Strings.ModesLightOsc[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0]
                );
                this.OnCellValueChanged(this._dif0DataGreed, new DataGridViewCellEventArgs(1, i));
            }
            #endregion

            #region Защиты I
            this._difensesIDataGrid.Rows.Clear();
            for (int i = 0; i < 8; i++)
            {
                this._difensesIDataGrid.Rows.Add(
                    "Ступень I> " + (i + 1),
                    Strings.ModesLightMode[0],
                    0,
                    Strings.MeasuringLight[0],
                    0,
                    Strings.YesNo[0],
                    Strings.BusDirection[0],
                    Strings.UnDirection[0],
                    Strings.TokParameter[0],
                    Strings.Characteristic[0],
                    0,
                    0,
                    Strings.Blocking[0],
                    Strings.ModesLightOsc[0],
                    false,
                    0,
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0]
                    );
                this.OnCellValueChanged(this._difensesIDataGrid, new DataGridViewCellEventArgs(1, i));
            }
            #endregion

            #region Защиты I0
            string name = "Ступень I0> ";
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                name = "Ступень I*> ";
            }
            this._difensesI0DataGrid.Rows.Clear();
            for (int i = 0; i < 6; i++)
            {
                this._difensesI0DataGrid.Rows.Add(

                    name + (i + 1),
                    Strings.ModesLightMode[0],
                    0,
                    Strings.MeasuringLight[0],
                    0,
                    Strings.YesNo[0],
                    Strings.BusDirection[0],
                    Strings.UnDirection[0],
                    Strings.I0Modes[0],
                    Strings.Characteristic[0],
                    0,
                    0,
                    Strings.Blocking[0],
                    Strings.ModesLightOsc[0],
                    false,
                    0,
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0]
                    );
                this.OnCellValueChanged(this._difensesI0DataGrid, new DataGridViewCellEventArgs(1, i));
            }

            #endregion

            #region Защиты U>
            this._difensesUBDataGrid.Rows.Clear();
            for (int i = 0; i < 4; i++)
            {
                this._difensesUBDataGrid.Rows.Add(
                    "Ступень U> " + (i + 1),
                    Strings.ModesLightMode[0],
                    Strings.UType[0],
                    0,
                    0,
                    0,
                    0,
                    false,
                    Strings.YesNo[0],
                    Strings.DiffBlocking[0],
                    Strings.ModesLightOsc[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.YesNo[0]
                    );
                this.OnCellValueChanged(this._difensesUBDataGrid, new DataGridViewCellEventArgs(1, i));
            }
            #endregion

            #region Защиты U<

            this._difensesUMDataGrid.Rows.Clear();
            for (int i = 0; i < 4; i++)
            {
                this._difensesUMDataGrid.Rows.Add(
                    "Ступень U< " + (i + 1),
                    Strings.ModesLightMode[0],
                    Strings.UMType[0],
                    0,
                    0,
                    0,
                    0,
                    false,
                    Strings.YesNo[0],
                    Strings.DiffBlocking[0],
                    Strings.ModesLightOsc[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.YesNo[0]
                    );
                this.OnCellValueChanged(this._difensesUMDataGrid, new DataGridViewCellEventArgs(1, i));
            }
            #endregion

            if (this._vers >= 1.11)
            {
                #region Защиты F>

                this._difensesFBDataGrid.Rows.Clear();
                for (int i = 0; i < 4; i++)
                {
                    this._difensesFBDataGrid.Rows.Add(
                        "Ступень F> " + (i + 1),
                        Strings.ModesLightMode[0],
                        40,
                        0,
                        0,
                        40,
                        false,
                        Strings.DiffBlocking[0],
                        Strings.ModesLightOsc[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.YesNo[0]
                        );
                    this.OnCellValueChanged(this._difensesFBDataGrid, new DataGridViewCellEventArgs(1, i));
                }
                #endregion
                
                #region Защиты F<

                this._difensesFMDataGrid.Rows.Clear();
                for (int i = 0; i < 4; i++)
                {
                    this._difensesFMDataGrid.Rows.Add(
                    "Ступень F< " + (i + 1),
                        Strings.ModesLightMode[0],
                        40,
                        0,
                        0,
                        40,
                        false,
                        Strings.DiffBlocking[0],
                        Strings.ModesLightOsc[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.ModesLight[0],
                        Strings.YesNo[0]
                    );
                    this.OnCellValueChanged(this._difensesFMDataGrid, new DataGridViewCellEventArgs(1, i));
                }
                #endregion
            }

            #region Защиты внешние
            for (int i = 0; i < 16; i++)
            {
                this._externalDifensesDataGrid.Rows.Add(
                    "Внешняя " + (i + 1),
                    Strings.ModesLightMode[0],
                    Strings.SygnalSrab[0],
                    0,
                    0,
                    Strings.SygnalSrab[0],
                    false,
                    Strings.SygnalSrab[0],
                    Strings.ModesLightOsc[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.ModesLight[0],
                    Strings.YesNo[0]
                    );
                this.OnCellValueChanged(this._externalDifensesDataGrid, new DataGridViewCellEventArgs(1, i));
            }
            #endregion
        }

        private void WriteCurrentProtAll()
        {
            try
            {
                ushort[] _curProtAll = this._vers >= 1.11 
                    ? new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST1_11)) / 2] 
                    : new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST)) / 2];
                int _currentIndex;

                #region Углы
                int index = 0;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {

                        _curProtAll[index] = Convert.ToUInt16(this._cornersGridView.Rows[i].Cells[j + 1].Value.ToString());
                        index++;
                    }
                }
                _currentIndex = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE)) / 2;
                #endregion

                //_currentIndex уже посчитан

                #region ДифЗащиты

                this.WriteDifPower();
                Array.ConstrainedCopy(this._device.ArrayDifMainCutOf, 0, _curProtAll, _currentIndex, this._device.ArrayDifMainCutOf.Length);
                _currentIndex += this._device.ArrayDifMainCutOf.Length;
                #endregion

                //_currentIndex уже посчитан

                #region Dif0
                string[][] dif0Xml = new string[3][];

                for (int dif0Ind = 1; dif0Ind <= 3; dif0Ind++)
                {
                    this.WriteDif0(dif0Ind);
                    dif0Xml[dif0Ind-1] = this._device.PrepareDif0ToXml();
                    Array.ConstrainedCopy(this._device.Dif0Array, 0, _curProtAll, _currentIndex, this._device.Dif0Array.Length);
                    _currentIndex += this._device.Dif0Array.Length;
                }
                this._device.Dif0Xml = dif0Xml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты I
                string[][] difIXml = new string[8][];
                for (int difIInd = 1; difIInd <= 8; difIInd++)
                {
                    this.WriteDifI(difIInd);
                    difIXml[difIInd - 1] = this._device.PrepareDifIToXml();
                    Array.ConstrainedCopy(this._device.DifIArray, 0, _curProtAll, _currentIndex, this._device.DifIArray.Length);
                    _currentIndex += this._device.DifIArray.Length;
                }
                this._device.DifIXml = difIXml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты I0
                string[][] difI0Xml = new string[6][];
                for (int difI0Ind = 1; difI0Ind <= 6; difI0Ind++)
                {
                    this.WriteDifI0(difI0Ind);
                    difI0Xml[difI0Ind-1] = this._device.PrepareDifI0ToXml();
                    Array.ConstrainedCopy(this._device.DifI0Array, 0, _curProtAll, _currentIndex, this._device.DifI0Array.Length);
                    _currentIndex += this._device.DifI0Array.Length;
                }
                this._device.DifI0Xml = difI0Xml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты U>
                string[][] difBUXml = new string[4][];
                for (int difUbInd = 1; difUbInd <= 4; difUbInd++)
                {
                    this.WriteDifUB(difUbInd);
                    difBUXml[difUbInd - 1] = this._device.PrepareDifUBToXml();
                    Array.ConstrainedCopy(this._device.DifUBArray, 0, _curProtAll, _currentIndex, this._device.DifUBArray.Length);
                    _currentIndex += this._device.DifUBArray.Length;
                }
                this._device.DifUBXml = difBUXml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты U<
                string[][] difMUXml = new string[4][];
                for (int difUmInd = 1; difUmInd <= 4; difUmInd++)
                {
                    this.WriteDifUM(difUmInd);
                    difMUXml[difUmInd - 1] = this._device.PrepareDifUMToXml();
                    Array.ConstrainedCopy(this._device.DifUMArray, 0, _curProtAll, _currentIndex, this._device.DifUMArray.Length);
                    _currentIndex += this._device.DifUMArray.Length;
                }
                this._device.DifUMXml = difMUXml;
                #endregion

                if (this._vers >= 1.11)
                {

                    //_currentIndex уже посчитан

                    #region Защиты F>
                    string[][] difFBXml = new string[4][];
                    for (int difFbInd = 1; difFbInd <= 4; difFbInd++)
                    {
                        this.WriteDifFB(difFbInd);
                        difFBXml[difFbInd - 1] = this._device.PrepareDifFBToXml();
                        Array.ConstrainedCopy(this._device.DifFBArray, 0, _curProtAll, _currentIndex, this._device.DifFBArray.Length);
                        _currentIndex += this._device.DifFBArray.Length;
                    }
                    this._device.DifFBXml = difFBXml;
                    #endregion

                    //_currentIndex уже посчитан
                   
                    #region Защиты F< 
                    string[][] difFMXml = new string[4][];
                    for (int difFmInd = 1; difFmInd <= 4; difFmInd++)
                    {
                        this.WriteDifFM(difFmInd);
                        difFMXml[difFmInd - 1] = this._device.PrepareDifFMToXml();
                        Array.ConstrainedCopy(this._device.DifFMArray, 0, _curProtAll, _currentIndex, this._device.DifFMArray.Length);
                        _currentIndex += this._device.DifFMArray.Length;
                    }
                    this._device.DifFMXml = difFMXml;

                    #endregion
                }
                //_currentIndex уже посчитан

                #region Защиты Q>
                _currentIndex += System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ)) / 2 * 2;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты внешние
                UDZT.ExtClass[] extXml =new UDZT.ExtClass[16];
                for (int extDifInd = 1; extDifInd <= 16; extDifInd++)
                {
                    this.WriteExtDif(extDifInd);
                    extXml[extDifInd - 1] = new UDZT.ExtClass(this._device.PrepareExtToXml()); 
                    Array.ConstrainedCopy(this._device.ExtDifArray, 0, _curProtAll, _currentIndex, this._device.ExtDifArray.Length);
                    _currentIndex += this._device.ExtDifArray.Length;
                }
                this._device.ExtXml = extXml;
                #endregion

                #region Сохранение изменений защит
                if (!this._mainRadioButton.Checked)
                {
                    this._ust = "Основные уставки";
                    this._device.CurrentProtAllMainProperty = _curProtAll;
                }
                else
                {
                    this._ust = "Резервные уставки";
                    this._device.CurrentProtAllReservProperty = _curProtAll;
                }
                #endregion
            }
            catch
            {
            }
        }

        private bool WriteLastCurrentProtAll()
        {
            bool res = true;
            try
            {
                ushort[] _curProtAll;
                if (this._vers >= 1.11)
                {
                    _curProtAll = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST1_11)) / 2];
                }
                else
                {
                    _curProtAll = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST)) / 2];
                }
                int _currentIndex = 0;

                #region Углы
                int index = 0;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 4; j++)
                    {

                        _curProtAll[index] = Convert.ToUInt16(this._cornersGridView.Rows[i].Cells[j + 1].Value.ToString());
                        index++;
                    }
                  //  index++;
                }
                _currentIndex = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE)) / 2;
                #endregion

                //_currentIndex уже посчитан

                #region ДифЗащиты

                this.WriteDifPower();
                Array.ConstrainedCopy(this._device.ArrayDifMainCutOf, 0, _curProtAll, _currentIndex, this._device.ArrayDifMainCutOf.Length);
                _currentIndex += this._device.ArrayDifMainCutOf.Length;
                #endregion

                //_currentIndex уже посчитан

                #region Dif0
                string[][] dif0Xml = new string[3][];

                for (int dif0Ind = 1; dif0Ind <= 3; dif0Ind++)
                {
                    this.WriteDif0(dif0Ind);
                    dif0Xml[dif0Ind-1] = this._device.PrepareDif0ToXml();
                    Array.ConstrainedCopy(this._device.Dif0Array, 0, _curProtAll, _currentIndex, this._device.Dif0Array.Length);
                    _currentIndex += this._device.Dif0Array.Length;
                }
                this._device.Dif0Xml = dif0Xml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты I
                string[][] difIXml = new string[8][];
                for (int difIInd = 1; difIInd <= 8; difIInd++)
                {
                    this.WriteDifI(difIInd);
                    difIXml[difIInd - 1] = this._device.PrepareDifIToXml();
                    Array.ConstrainedCopy(this._device.DifIArray, 0, _curProtAll, _currentIndex, this._device.DifIArray.Length);
                    _currentIndex += this._device.DifIArray.Length;
                }
                this._device.DifIXml = difIXml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты I0
                string[][] difI0Xml = new string[6][];
                for (int difI0Ind = 1; difI0Ind <= 6; difI0Ind++)
                {
                    this.WriteDifI0(difI0Ind);
                    difI0Xml[difI0Ind - 1] = this._device.PrepareDifI0ToXml();
                    Array.ConstrainedCopy(this._device.DifI0Array, 0, _curProtAll, _currentIndex, this._device.DifI0Array.Length);
                    _currentIndex += this._device.DifI0Array.Length;
                }
                this._device.DifI0Xml = difI0Xml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты U>
                string[][] difBUXml = new string[4][];
                for (int difUbInd = 1; difUbInd <= 4; difUbInd++)
                {
                    this.WriteDifUB(difUbInd);
                    difBUXml[difUbInd - 1] = this._device.PrepareDifUBToXml();
                    Array.ConstrainedCopy(this._device.DifUBArray, 0, _curProtAll, _currentIndex, this._device.DifUBArray.Length);
                    _currentIndex += this._device.DifUBArray.Length;
                }
                this._device.DifUBXml = difBUXml;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты U<
                string[][] difMUXml = new string[4][];
                for (int difUmInd = 1; difUmInd <= 4; difUmInd++)
                {
                    this.WriteDifUM(difUmInd);
                    difMUXml[difUmInd - 1] = this._device.PrepareDifUMToXml();
                    Array.ConstrainedCopy(this._device.DifUMArray, 0, _curProtAll, _currentIndex, this._device.DifUMArray.Length);
                    _currentIndex += this._device.DifUMArray.Length;
                }
                this._device.DifUMXml = difMUXml;
                #endregion

                if (this._vers >= 1.11)
                {

                    //_currentIndex уже посчитан

                    #region Защиты F>
                    string[][] difFBXml = new string[4][];
                    for (int difFbInd = 1; difFbInd <= 4; difFbInd++)
                    {
                        this.WriteDifFB(difFbInd);
                        difFBXml[difFbInd - 1] = this._device.PrepareDifFBToXml();
                        Array.ConstrainedCopy(this._device.DifFBArray, 0, _curProtAll, _currentIndex, this._device.DifFBArray.Length);
                        _currentIndex += this._device.DifFBArray.Length;
                    }
                    this._device.DifFBXml = difFBXml;
                    #endregion

                    //_currentIndex уже посчитан

                    #region Защиты F<
                    string[][] difFMXml = new string[4][];
                    for (int difFmInd = 1; difFmInd <= 4; difFmInd++)
                    {
                        this.WriteDifFM(difFmInd);
                        difFMXml[difFmInd - 1] = this._device.PrepareDifFMToXml();
                        Array.ConstrainedCopy(this._device.DifFMArray, 0, _curProtAll, _currentIndex, this._device.DifFMArray.Length);
                        _currentIndex += this._device.DifFMArray.Length;
                    }
                    this._device.DifFMXml = difFMXml;
                    #endregion
                }


                //_currentIndex уже посчитан

                #region Защиты Q>
                _currentIndex += System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ)) / 2 * 2;
                #endregion

                //_currentIndex уже посчитан

                #region Защиты внешние
                UDZT.ExtClass[] extXml = new UDZT.ExtClass[16];
                for (int extDifInd = 1; extDifInd <= 16; extDifInd++)
                {
                    this.WriteExtDif(extDifInd);
                    extXml[extDifInd - 1] = new UDZT.ExtClass(this._device.PrepareExtToXml()); 
                    Array.ConstrainedCopy(this._device.ExtDifArray, 0, _curProtAll, _currentIndex, this._device.ExtDifArray.Length);
                    _currentIndex += this._device.ExtDifArray.Length;
                }
                this._device.ExtXml = extXml;
                #endregion

                #region Сохранение изменений защит
                if (this._mainRadioButton.Checked)
                {
                    this._ust = "Основные уставки";
                    this._device.CurrentProtAllMainProperty = _curProtAll;
                }
                else
                {
                    this._ust = "Резервные уставки";
                    this._device.CurrentProtAllReservProperty = _curProtAll;
                }
                #endregion
            }
            catch
            {
                res = false;
            }
            return res;
        }

        #region Углы
        private void PrepareCornersGrid()
        {
            if (this._cornersGridView.Rows.Count == 0)
            {
                this._cornersGridView.Rows.Add(3);
            }
            for (int i = 0; i < 3; i++)
            {
                this._cornersGridView.Rows[i].Cells[0].Value = "S" + (i + 1);
                for (int j = 0; j < 4; j++)
                {
                    this._cornersGridView.Rows[i].Cells[j + 1].Value = 0.ToString();
                }
            }
            this._cornersGridView.CellBeginEdit += this.OnGridCellBeginEdit;
        }

        #endregion

        #region Дифференциальные защиты
        private void PrepareDiffPowerCombos()
        {
            ComboBox[] combos = {
                this._modeDTZComboBox, this._blockingDTZComboBox, this._UROVDTZComboBox, this._APVDTZComboBox,
                this._AVRDTZComboBox, this._oscDTZComboBox, this._modeDTOBTComboBox,
                this._stepOnInstantValuesDTOBTComboBox, this._blockingDTOBTComboBox, this._UROVDTOBTComboBox,
                this._APVDTOBTComboBox, this._AVRDTOBTComboBox, this._oscDTOBTComboBox, this._modeI2I1CB,
                this._modeI5I1CB, this._perBlockI2I1, this._perBlockI5I1
            };
            this._DOUBLEDifPowerMaskedTextBoxes = new MaskedTextBox[]
            {this._constraintDTZTextBox, this._constraintDTOBTTextBox, this._Ib1BeginTextBox, this._Ib2BeginTextBox};
            this._ULONGDifPowerMaskedTextBoxes = new MaskedTextBox[] {this._timeEnduranceDTZTextBox, this._timeEnduranceDTOBTTextBox, this._K1AngleOfSlopeTextBox, this._K2TangensTextBox, this._I2I1TextBox, this._I5I1TextBox };

            this.ClearCombos(combos);
            this.FillDiffPowerCombos(combos);

            this.PrepareMaskedBoxes(this._DOUBLEDifPowerMaskedTextBoxes, typeof(double));
            this.PrepareMaskedBoxes(this._ULONGDifPowerMaskedTextBoxes, typeof(ulong));
        }

        private void FillDiffPowerCombos(ComboBox[] combos)
        {
            #region Дифференциальная токовая защита

            this._modeDTZComboBox.Items.AddRange(Strings.ModesLightMode.ToArray());
            this._modeI2I1CB.Items.AddRange(Strings.YesNo.ToArray());
            this._modeI5I1CB.Items.AddRange(Strings.YesNo.ToArray());
            this._perBlockI2I1.Items.AddRange(Strings.YesNo.ToArray());
            this._perBlockI5I1.Items.AddRange(Strings.YesNo.ToArray());
            this._blockingDTZComboBox.Items.AddRange(Strings.Blocking.ToArray());
            this._UROVDTZComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._oscDTZComboBox.Items.AddRange(Strings.ModesLightOsc.ToArray());
            this._APVDTZComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._AVRDTZComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            #endregion

            #region Дифференциальная токовая отсечка без торможения

            this._modeDTOBTComboBox.Items.AddRange(Strings.ModesLightMode.ToArray());
            this._stepOnInstantValuesDTOBTComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._blockingDTOBTComboBox.Items.AddRange(Strings.Blocking.ToArray());
            this._UROVDTOBTComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._oscDTOBTComboBox.Items.AddRange(Strings.ModesLightOsc.ToArray());
            this._APVDTOBTComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            this._AVRDTOBTComboBox.Items.AddRange(Strings.ModesLight.ToArray());
            #endregion

            foreach (ComboBox box in combos)
            {
                box.SelectedIndex = 0;
            }
        }

        private void _device_ParamAutomatLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_ParamAutomatLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadNeispr));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
                Invoke(new OnDeviceEventHandler(this.ReadRele));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
                Invoke(new OnDeviceEventHandler(this.ReadIndicators));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception ee)
            {

            }
        }

        private void ReadDifPower()
        {
            if (!this._messageDifPower)
            {
                try
                {
                    this._modeDTOBTComboBox.SelectedItem = this._device.DTOBTMode;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Состояние.", "Ошибка чтения");
                }
                try
                {
                    this._stepOnInstantValuesDTOBTComboBox.SelectedItem = this._device.DTOBTStepOnInstantValues;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Ступень по мгновенным значениям.", "Ошибка чтения");
                }
                try
                {
                    this._constraintDTOBTTextBox.Text = this._device.DTOBTConstraint.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Уставка Iд>>.", "Ошибка чтения");
                }
                try
                {
                    this._timeEnduranceDTOBTTextBox.Text = this._device.DTOBTTimeEndurance.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Выдержка времени Tд>>.", "Ошибка чтения");
                }
                try
                {
                    this._blockingDTOBTComboBox.SelectedItem = this._device.DTOBTblocking;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Блокировка.", "Ошибка чтения");
                }

                try
                {
                    this._UROVDTOBTComboBox.SelectedItem = this._device.DTOBTUROV;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : УРОВ.", "Ошибка чтения");
                }
                try
                {
                    if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                    {
                        this._oscDTOBTComboBox.SelectedItem = this._device.DTOBTOsc;
                    }
                    else 
                    {
                        this._oscDTOBTComboBox.SelectedItem = this._device.DTOBTOsc_1_11;
                    }
                    
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Осциллограф.", "Ошибка чтения");
                }
                try
                {
                    this._APVDTOBTComboBox.SelectedItem = this._device.DTOBTAPV;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : АПВ.", "Ошибка чтения");
                }
                try
                {
                    this._AVRDTOBTComboBox.SelectedItem = this._device.DTOBTAVR;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : АВР.", "Ошибка чтения");
                }
                try
                {
                    this._modeDTZComboBox.SelectedItem = this._device.DTZMode;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Состояние.", "Ошибка чтения");
                }
                try
                {
                    this._constraintDTZTextBox.Text = this._device.DTZConstraint.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Уставка Iд>.", "Ошибка чтения");
                }
                try
                {
                    this._timeEnduranceDTZTextBox.Text = this._device.DTZTimeEndurance.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Выдержка времени Tд>.", "Ошибка чтения");
                }

                try
                {
                    this._Ib1BeginTextBox.Text = this._device.DTZIb1.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Iб1 - начало.", "Ошибка чтения");
                }
                try
                {
                    this._K1AngleOfSlopeTextBox.Text = this._device.DTZK1.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : К1 - угол наклона.", "Ошибка чтения");
                }
                try
                {
                    this._Ib2BeginTextBox.Text = this._device.DTZIb2.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Iб2 - начало.", "Ошибка чтения");
                }
                try
                {
                    this._K2TangensTextBox.Text = this._device.DTZK2.ToString();
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : К2 - тангенс.", "Ошибка чтения");
                }
                try
                {
                    this._I2I1TextBox.Text = this._device.DTZI2I1.ToString();
                    this._modeI2I1CB.Text = this._device.I2I1Mode;
                    this._perBlockI2I1.Text = this._device.I2I1PerBlock;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : I2/I1 (намагничивание).", "Ошибка чтения");
                }
                try
                {
                    this._I5I1TextBox.Text = this._device.DTZI5I1.ToString();
                    this._modeI5I1CB.Text = this._device.I5I1Mode;
                    this._perBlockI5I1.Text = this._device.I5I1PerBlock;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : I5/I1 (перегрузка).", "Ошибка чтения");
                }
                try
                {
                    this._blockingDTZComboBox.SelectedItem = this._device.DTZblocking;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Блокировка.", "Ошибка чтения");
                }
                try
                {
                    this._UROVDTZComboBox.SelectedItem = this._device.DTZUROV;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : УРОВ.", "Ошибка чтения");
                }
                try
                {
                    if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                    {
                        this._oscDTZComboBox.SelectedItem = this._device.DTZOsc;
                    }
                    else
                    {
                        this._oscDTZComboBox.SelectedItem = this._device.DTZOsc_1_11;
                    }
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Осциллограф.", "Ошибка чтения");
                }
                try
                {
                    this._APVDTZComboBox.SelectedItem = this._device.DTZAPV;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : АПВ.", "Ошибка чтения");
                }
                try
                {
                    this._AVRDTZComboBox.SelectedItem = this._device.DTZAVR;
                }
                catch (Exception ex)
                {
                    this._configurationTabControl.SelectTab(this._allDefensesPage);
                    MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : АВР.", "Ошибка чтения");
                }
                this._messageDifPower = true;
            }
        }

        private void WriteDifPower()
        {
            try
            {
                this._device.DTOBTMode = this._modeDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Состояние.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTStepOnInstantValues = this._stepOnInstantValuesDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Ступень по мгновенным значениям.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTConstraint = Convert.ToDouble(this._constraintDTOBTTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Уставка Iд>>.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTTimeEndurance = Convert.ToInt32(this._timeEnduranceDTOBTTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Выдержка времени Tд>>.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTblocking = this._blockingDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Блокировка.", "Ошибка записи");
            }

            try
            {
                this._device.DTOBTUROV = this._UROVDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : УРОВ.", "Ошибка записи");
            }
            try
            {
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._device.DTOBTOsc = this._oscDTOBTComboBox.SelectedItem.ToString();
                }
                else
                {
                    this._device.DTOBTOsc_1_11 = this._oscDTOBTComboBox.SelectedItem.ToString();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : Осциллограф.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTAPV = this._APVDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : АПВ.", "Ошибка записи");
            }
            try
            {
                this._device.DTOBTAVR = this._AVRDTOBTComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая отсечка без торможения\nНекорректное значение : АВР.", "Ошибка записи");
            }
            try
            {
                this._device.DTZMode = this._modeDTZComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Состояние.", "Ошибка записи");
            }
            try
            {
                this._device.DTZConstraint = Convert.ToDouble(this._constraintDTZTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Уставка Iд>.", "Ошибка записи");
            }
            try
            {
                this._device.DTZTimeEndurance = Convert.ToInt32(this._timeEnduranceDTZTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Выдержка времени Tд>.", "Ошибка записи");
            }

            try
            {
                this._device.DTZIb1 = Convert.ToDouble(this._Ib1BeginTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Iб1 - начало.", "Ошибка записи");
            }
            try
            {
                this._device.DTZK1 = Convert.ToInt32(this._K1AngleOfSlopeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : К1 - угол наклона.", "Ошибка записи");
            }
            try
            {
                this._device.DTZIb2 = Convert.ToDouble(this._Ib2BeginTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Iб2 - начало.", "Ошибка записи");
            }
            try
            {
                this._device.DTZK2 = Convert.ToInt32(this._K2TangensTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : К2 - тангенс.", "Ошибка записи");
            }
            try
            {
                this._device.DTZI2I1 = Convert.ToInt32(this._I2I1TextBox.Text);
                this._device.I2I1Mode = this._modeI2I1CB.SelectedItem.ToString();
                this._device.I2I1PerBlock = this._perBlockI2I1.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : I2/I1 (намагничивание).", "Ошибка записи");
            }
            try
            {
                this._device.DTZI5I1 = Convert.ToInt32(this._I5I1TextBox.Text);
                this._device.I5I1Mode = this._modeI5I1CB.SelectedItem.ToString();
                this._device.I5I1PerBlock = this._perBlockI5I1.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : I5/I1 (перегрузка).", "Ошибка записи");
            }
            try
            {
                this._device.DTZblocking = this._blockingDTZComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Блокировка.", "Ошибка записи");
            }
            try
            {
                this._device.DTZUROV = this._UROVDTZComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : УРОВ.", "Ошибка записи");
            }
            try
            {
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._device.DTZOsc = this._oscDTZComboBox.SelectedItem.ToString();
                }
                else
                {
                    this._device.DTZOsc_1_11 = this._oscDTZComboBox.SelectedItem.ToString();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : Осциллограф.", "Ошибка записи");
            }
            try
            {
                this._device.DTZAPV = this._APVDTZComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : АПВ.", "Ошибка записи");
            }
            try
            {
                this._device.DTZAVR = this._AVRDTZComboBox.SelectedItem.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\nДифференциальные защиты:\nДиф. токовая защита\nНекорректное значение : АВР.", "Ошибка записи");
            }
        }

        #endregion

        #region Диф 0
        private void PrepareDif0Grid()
        {
            this.FillDif0Grid();
            if (this._dif0DataGreed.Rows.Count == 0)
            {
                this._dif0DataGreed.CellValueChanged += this.OnCellValueChanged;
                string a = ">";
                for (int i = 0; i < 3; i++)
                {
                    this._dif0DataGreed.Rows.Add("Ступень Iд0> " + (i + 1), Strings.ModesLightMode[0], Strings.DiffBlocking[0], 0, Strings.MeasuringLight[0], 0, 0, 0, 0, 0, Strings.ModesLightOsc[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0]);
                    a += ">";
                    this.OnCellValueChanged(this._dif0DataGreed, new DataGridViewCellEventArgs(1, i));
                }
            }
            this._dif0DataGreed.CellBeginEdit += this.OnGridCellBeginEdit;
        }

        private void FillDif0Grid()
        {
            if (this._dif0ModeColumn.Items.Count == 0)
            {
                this._dif0ModeColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._dif0BlockingColumn.Items.Count == 0)
            {
                this._dif0BlockingColumn.Items.AddRange(Strings.DiffBlocking.ToArray());
            }
            if (this._dif0InColumn.Items.Count == 0)
            {
                this._dif0InColumn.Items.AddRange(Strings.MeasuringLight.ToArray());
            }
            if (this._dif0OscColumn.Items.Count == 0)
            {
                this._dif0OscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._dif0UrovColumn.Items.Count == 0)
            {
                this._dif0UrovColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._dif0APVColumn.Items.Count == 0)
            {
                this._dif0APVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._dif0AVRColumn.Items.Count == 0)
            {
                this._dif0AVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
        }

        private void ReadDif0(int index)
        {
            try
            {
                this._dif0DataGreed["_dif0ModeColumn", index - 1].Value = this._device.Dif0Mode;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр Состояние.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0BlockingColumn", index - 1].Value = this._device.Dif0blocking;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр Блок-ка.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0IdColumn", index - 1].Value = this._device.Dif0Constraint;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр I.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0InColumn", index - 1].Value = this._device.Dif0Side;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр In.\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0TdColumn", index - 1].Value = this._device.Dif0TimeEndurance;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр t.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0Ib1Column", index - 1].Value = this._device.Dif0Ib1;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр Iб1.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0Intg1Column", index - 1].Value = this._device.Dif0K1;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр К1.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0Ib2Column", index - 1].Value = this._device.Dif0Ib2;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр Iб2.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0Intg2Column", index - 1].Value = this._device.Dif0K2;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр К2.\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._dif0DataGreed["_dif0OscColumn", index - 1].Value = this._device.Dif0Osc;
                }
                else
                {
                    this._dif0DataGreed["_dif0OscColumn", index - 1].Value = this._device.Dif0Osc_1_11;
                }
                
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр Осц. .\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0UROVColumn", index - 1].Value = this._device.Dif0UROV;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр УРОВ. .\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0APVColumn", index - 1].Value = this._device.Dif0APV;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр АПВ. .\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._dif0DataGreed["_dif0AVRColumn", index - 1].Value = this._device.Dif0AVR;
            }
            catch
            {
                MessageBox.Show("В дифф. защите нулевой последовательности неверно задан параметр АВР. .\nCтупень > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
        }

        private void WriteDif0(int index)
        {
            try
            {
                this._device.Dif0Mode = this._dif0DataGreed["_dif0ModeColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
            try
            {
                this._device.Dif0blocking = this._dif0DataGreed["_dif0BlockingColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
            try
            {
                this._device.Dif0Constraint = Convert.ToDouble(this._dif0DataGreed["_dif0IdColumn", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                this._device.Dif0Side = this._dif0DataGreed["_dif0InColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
            try
            {
                this._device.Dif0TimeEndurance = Convert.ToInt32(this._dif0DataGreed["_dif0TdColumn", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                this._device.Dif0Ib1 = Convert.ToDouble(this._dif0DataGreed["_dif0Ib1Column", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                this._device.Dif0K1 = Convert.ToInt32(this._dif0DataGreed["_dif0Intg1Column", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                this._device.Dif0Ib2 = Convert.ToDouble(this._dif0DataGreed["_dif0Ib2Column", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                this._device.Dif0K2 = Convert.ToInt32(this._dif0DataGreed["_dif0Intg2Column", index - 1].Value.ToString());
            }
            catch
            {
            }
            try
            {
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._device.Dif0Osc = this._dif0DataGreed["_dif0OscColumn", index - 1].Value.ToString();
                }
                else
                {
                    this._device.Dif0Osc_1_11 = this._dif0DataGreed["_dif0OscColumn", index - 1].Value.ToString();
                }
            }
            catch
            {
            }
            try
            {
                this._device.Dif0UROV = this._dif0DataGreed["_dif0UROVColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
            try
            {
                this._device.Dif0APV = this._dif0DataGreed["_dif0APVColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
            try
            {
                this._device.Dif0AVR = this._dif0DataGreed["_dif0AVRColumn", index - 1].Value.ToString();
            }
            catch
            {
            }
        }
        #endregion

        #region Защиты I

        private void PrepareIGrid()
        {
            this._difensesIDataGrid.CellValueChanged += this.OnCellValueChanged;
            
            for (int i = 0; i < 8; i++)
            {
                this.OnCellValueChanged(this._difensesIDataGrid, new DataGridViewCellEventArgs(1, i));
            }

            this._difensesIDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }

        private void ReadDifI(int index)
        {
            this._difensesIDataGrid["_iTyColumn", index - 1].Value = this._device.DifITy;
            this._difensesIDataGrid["_iTyBoolColumn", index - 1].Value = this._device.DifIBoolTy;
            try
            {
                this._difensesIDataGrid["_iModesColumn", index - 1].Value = this._device.DifI_Mode;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Состояние. .\nCтупень I > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iIColumn", index - 1].Value = this._device.DifI_IConstraint;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iInColumn", index - 1].Value = this._device.DifI_Side;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iUStartColumn", index - 1].Value = this._device.DifI_UStartConstraint;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iUstartYNColumn", index - 1].Value = this._device.DifI_UYNMode;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iDirectColumn", index - 1].Value = this._device.DifI_Direction;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iUnDirectColumn", index - 1].Value = this._device.DifI_Undirection;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesIDataGrid["_iLogicColumn", index - 1].Value = this._device.DifI_Logic;
                }
                else 
                {
                    this._difensesIDataGrid["_iLogicColumn", index - 1].Value = this._device.DifI_Logic_v_1_11;
                }
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iCharColumn", index - 1].Value = this._device.DifI_Charakteristic;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iTColumn", index - 1].Value = this._device.DifI_T;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iKColumn", index - 1].Value = this._device.DifI_K;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                this._difensesIDataGrid["_iBlockingColumn", index - 1].Value = this._device.DifI_Blocking;
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesIDataGrid["_iOscModeColumn", index - 1].Value = this._device.DifI_Osc;
                }
                else 
                {
                    this._difensesIDataGrid["_iOscModeColumn", index - 1].Value = this._device.DifI_Osc_v_1_11;
                }
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр Осц. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesIDataGrid["_iUROVModeColumn", index - 1].Value = this._device.DifI_UROV;
                }
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр УРОВ. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesIDataGrid["_iAPVModeColumn", index - 1].Value = this._device.DifI_APV;
                }
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр АПВ. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
            try
            {
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesIDataGrid["_iAVRModeColumn", index - 1].Value = this._device.DifI_AVR;
                }
            }
            catch
            {
                MessageBox.Show("В защите I неверно задан параметр АВР. .\nCтупень Iд > " + index + "\n" + this._ust + ". \n\nИсправте его вручную.", "Внимание");
            }
        }

        private void WriteDifI(int index)
        {
            this._device.DifITy = Convert.ToInt32(this._difensesIDataGrid["_iTyColumn", index - 1].Value);
            this._device.DifIBoolTy = Convert.ToBoolean(this._difensesIDataGrid["_iTyBoolColumn", index - 1].Value);
            
            this._device.DifI_Mode = this._difensesIDataGrid["_iModesColumn", index - 1].Value.ToString();
            this._device.DifI_IConstraint = Convert.ToDouble(this._difensesIDataGrid["_iIColumn", index - 1].Value.ToString());
            this._device.DifI_Side = this._difensesIDataGrid["_iInColumn", index - 1].Value.ToString();
            this._device.DifI_UStartConstraint = Convert.ToDouble(this._difensesIDataGrid["_iUStartColumn", index - 1].Value.ToString());

            this._device.DifI_UYNMode = this._difensesIDataGrid["_iUstartYNColumn", index - 1].Value.ToString();
            this._device.DifI_Direction = this._difensesIDataGrid["_iDirectColumn", index - 1].Value.ToString();
            this._device.DifI_Undirection = this._difensesIDataGrid["_iUnDirectColumn", index - 1].Value.ToString();
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifI_Logic = this._difensesIDataGrid["_iLogicColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifI_Logic_v_1_11 = this._difensesIDataGrid["_iLogicColumn", index - 1].Value.ToString();
            }
            this._device.DifI_Charakteristic = this._difensesIDataGrid["_iCharColumn", index - 1].Value.ToString();
            this._device.DifI_T = Convert.ToDouble(this._difensesIDataGrid["_iTColumn", index - 1].Value.ToString());
            this._device.DifI_K = Convert.ToDouble(this._difensesIDataGrid["_iKColumn", index - 1].Value.ToString());
            this._device.DifI_Blocking = this._difensesIDataGrid["_iBlockingColumn", index - 1].Value.ToString();
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifI_Osc = this._difensesIDataGrid["_iOscModeColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifI_Osc_v_1_11 = this._difensesIDataGrid["_iOscModeColumn", index - 1].Value.ToString();
            }

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifI_UROV = this._difensesIDataGrid["_iUROVModeColumn", index - 1].Value.ToString();
            }

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifI_APV = this._difensesIDataGrid["_iAPVModeColumn", index - 1].Value.ToString();
            }

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifI_AVR = this._difensesIDataGrid["_iAVRModeColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #region Защиты I0
        private void PrepareI0Grid()
        {
            this._difensesI0DataGrid.CellValueChanged += this.OnCellValueChanged;
            //string name = "Ступень I0> ";
            //if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            //{
            //    name = "Ступень I*> ";
            //}
            for (int i = 0; i < 6; i++)
            {
                this.OnCellValueChanged(this._difensesI0DataGrid, new DataGridViewCellEventArgs(1, i));
            }

            this._difensesI0DataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }
        
        private void ReadDifI0(int index)
        {
            try
            {
                this._difensesI0DataGrid["_i0TyColumn", index - 1].Value = this._device.DifI0Ty;
                this._difensesI0DataGrid["_i0TyBoolColumn", index - 1].Value = this._device.DifI0BoolTy;
                
                this._difensesI0DataGrid["_i0ModesColumn", index - 1].Value = this._device.DifI0_Mode;
                this._difensesI0DataGrid["_i0IColumn", index - 1].Value = this._device.DifI0_IConstraint;
                this._difensesI0DataGrid["_i0InColumn", index - 1].Value = this._device.DifI0_Side;
                this._difensesI0DataGrid["_i0UstartColumn", index - 1].Value = this._device.DifI0_UStartConstraint;
                this._difensesI0DataGrid["_i0UsYNColumn", index - 1].Value = this._device.DifI0_UYNMode;
                this._difensesI0DataGrid["_i0DirColumn", index - 1].Value = this._device.DifI0_Direction;
                this._difensesI0DataGrid["_i0UndirColumn", index - 1].Value = this._device.DifI0_Undirection;
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesI0DataGrid["_i0I0Column", index - 1].Value = this._device.DifI0_IO;
                }
                else 
                {
                    this._difensesI0DataGrid["_i0I0Column", index - 1].Value = this._device.DifI0_IO_v_1_11;
                }
                this._difensesI0DataGrid["_i0CharColumn", index - 1].Value = this._device.DifI0_Charakteristic;
                this._difensesI0DataGrid["_i0TColumn", index - 1].Value = this._device.DifI0_T;
                this._difensesI0DataGrid["_i0kColumn", index - 1].Value = this._device.DifI0_K;
                this._difensesI0DataGrid["_i0BlockingColumn", index - 1].Value = this._device.DifI0_Blocking;

                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesI0DataGrid["_i0OscColumn", index - 1].Value = this._device.DifI0_Osc;
                }
                else
                {
                    this._difensesI0DataGrid["_i0OscColumn", index - 1].Value = this._device.DifI0_Osc_v_1_11;
                }

                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesI0DataGrid["_i0UROVColumn", index - 1].Value = this._device.DifI0_UROV;
                    this._difensesI0DataGrid["_i0APVColumn", index - 1].Value = this._device.DifI0_APV;
                    this._difensesI0DataGrid["_i0AVRColumn", index - 1].Value = this._device.DifI0_AVR;
                }
            }
            catch
            {
                int f = 5;
            }
        }

        private void WriteDifI0(int index)
        {
            this._device.DifI0Ty  = Convert.ToInt32(this._difensesI0DataGrid["_i0TyColumn", index - 1].Value);
            this._device.DifI0BoolTy = Convert.ToBoolean(this._difensesI0DataGrid["_i0TyBoolColumn", index - 1].Value);
            
            this._device.DifI0_Mode = this._difensesI0DataGrid["_i0ModesColumn", index - 1].Value.ToString();
            this._device.DifI0_IConstraint = Convert.ToDouble(this._difensesI0DataGrid["_i0IColumn", index - 1].Value.ToString());
            this._device.DifI0_Side = this._difensesI0DataGrid["_i0InColumn", index - 1].Value.ToString();
            this._device.DifI0_UStartConstraint = Convert.ToDouble(this._difensesI0DataGrid["_i0UstartColumn", index - 1].Value.ToString());
            this._device.DifI0_UYNMode = this._difensesI0DataGrid["_i0UsYNColumn", index - 1].Value.ToString();
            this._device.DifI0_Direction = this._difensesI0DataGrid["_i0DirColumn", index - 1].Value.ToString();
            this._device.DifI0_Undirection = this._difensesI0DataGrid["_i0UndirColumn", index - 1].Value.ToString();
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifI0_IO = this._difensesI0DataGrid["_i0I0Column", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifI0_IO_v_1_11 = this._difensesI0DataGrid["_i0I0Column", index - 1].Value.ToString();
            }
            this._device.DifI0_Charakteristic = this._difensesI0DataGrid["_i0CharColumn", index - 1].Value.ToString();
            this._device.DifI0_T = Convert.ToDouble(this._difensesI0DataGrid["_i0TColumn", index - 1].Value.ToString());
            this._device.DifI0_K = Convert.ToDouble(this._difensesI0DataGrid["_i0kColumn", index - 1].Value.ToString());
            this._device.DifI0_Blocking = this._difensesI0DataGrid["_i0BlockingColumn", index - 1].Value.ToString();
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifI0_Osc = this._difensesI0DataGrid["_i0OscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifI0_Osc_v_1_11 = this._difensesI0DataGrid["_i0OscColumn", index - 1].Value.ToString();
            }

            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifI0_UROV = this._difensesI0DataGrid["_i0UROVColumn", index - 1].Value.ToString();
                this._device.DifI0_APV = this._difensesI0DataGrid["_i0APVColumn", index - 1].Value.ToString();
                this._device.DifI0_AVR = this._difensesI0DataGrid["_i0AVRColumn", index - 1].Value.ToString();
            }

        }

        #endregion

        #region Защиты U>

        private void PrepareUBGrid()
        {
            this._difensesUBDataGrid.CellValueChanged += this.OnCellValueChanged;

            for (int i = 0; i < 4; i++)
            {
                this.OnCellValueChanged(this._difensesUBDataGrid, new DataGridViewCellEventArgs(1, i));
            }

            this._difensesUBDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }
        
        private void ReadDifUB(int index)
        {
            try
            {
                this._difensesUBDataGrid["_uBModesColumn", index - 1].Value = this._device.DifUB_Mode;
                this._difensesUBDataGrid["_uBTypeColumn", index - 1].Value = this._device.DifUB_Type;
                this._difensesUBDataGrid["_uBUsrColumn", index - 1].Value = this._device.DifUB_Usr;
                this._difensesUBDataGrid["_uBTsrColumn", index - 1].Value = this._device.DifUB_Tsr;
                this._difensesUBDataGrid["_uBTvzColumn", index - 1].Value = this._device.DifUB_Tvz;
                this._difensesUBDataGrid["_uBUvzColumn", index - 1].Value = this._device.DifUB_Uvz;
                this._difensesUBDataGrid["_uBUvzYNColumn", index - 1].Value = this._device.DifUB_UvzYN;
                this._difensesUBDataGrid["_uBBlockingUMColumn", index - 1].Value = this._device.DifUB_BlockingUM;
                this._difensesUBDataGrid["_uBBlockingColumn", index - 1].Value = this._device.DifUB_Blocking;
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesUBDataGrid["_uBOscColumn", index - 1].Value = this._device.DifUB_Osc;
                }
                else
                {
                    this._difensesUBDataGrid["_uBOscColumn", index - 1].Value = this._device.DifUB_Osc_v_1_11;
                }

                
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesUBDataGrid["_uBUROVColumn", index - 1].Value = this._device.DifUB_Urov;
                    this._difensesUBDataGrid["_uBAPVColumn", index - 1].Value = this._device.DifUB_APV;
                    this._difensesUBDataGrid["_uBAVRColumn", index - 1].Value = this._device.DifUB_AVR;
                    this._difensesUBDataGrid["_uBAPVRetColumn", index - 1].Value = this._device.DifUB_APVRet;
                    this._difensesUBDataGrid["_uBSbrosColumn", index - 1].Value = this._device.DifUB_Sbros;
                }
            }
            catch
            {
                int f = 5;
            }
        }

        private void WriteDifUB(int index)
        {
            this._device.DifUB_Mode = this._difensesUBDataGrid["_uBModesColumn", index - 1].Value.ToString();
            this._device.DifUB_Type = this._difensesUBDataGrid["_uBTypeColumn", index - 1].Value.ToString();
            this._device.DifUB_Usr = Convert.ToDouble(this._difensesUBDataGrid["_uBUsrColumn", index - 1].Value.ToString());
            this._device.DifUB_Tsr = Convert.ToDouble(this._difensesUBDataGrid["_uBTsrColumn", index - 1].Value.ToString());
            this._device.DifUB_Tvz = Convert.ToDouble(this._difensesUBDataGrid["_uBTvzColumn", index - 1].Value.ToString());
            this._device.DifUB_Uvz = Convert.ToDouble(this._difensesUBDataGrid["_uBUvzColumn", index - 1].Value.ToString());
            this._device.DifUB_UvzYN = Convert.ToBoolean(this._difensesUBDataGrid["_uBUvzYNColumn", index - 1].Value);
            this._device.DifUB_BlockingUM = this._difensesUBDataGrid["_uBBlockingUMColumn", index - 1].Value.ToString();
            this._device.DifUB_Blocking = this._difensesUBDataGrid["_uBBlockingColumn", index - 1].Value.ToString();
            
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifUB_Osc = this._difensesUBDataGrid["_uBOscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifUB_Osc_v_1_11 = this._difensesUBDataGrid["_uBOscColumn", index - 1].Value.ToString();
            }
            
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifUB_Urov = this._difensesUBDataGrid["_uBUROVColumn", index - 1].Value.ToString();
                this._device.DifUB_APV = this._difensesUBDataGrid["_uBAPVColumn", index - 1].Value.ToString();
                this._device.DifUB_AVR = this._difensesUBDataGrid["_uBAVRColumn", index - 1].Value.ToString();
                this._device.DifUB_APVRet = this._difensesUBDataGrid["_uBAPVRetColumn", index - 1].Value.ToString();
                this._device.DifUB_Sbros = this._difensesUBDataGrid["_uBSbrosColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #region Защиты U<

        private void PrepareUMGrid()
        {
            this._difensesUMDataGrid.CellValueChanged += this.OnCellValueChanged;

            for (int i = 0; i < 4; i++)
            {
                this.OnCellValueChanged(this._difensesUMDataGrid, new DataGridViewCellEventArgs(1, i));
            }

            this._difensesUMDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }

        private void ReadDifUM(int index)
        {
            try
            {
                this._difensesUMDataGrid["_uMModesColumn", index - 1].Value = this._device.DifUM_Mode;
                this._difensesUMDataGrid["_uMTypeColumn", index - 1].Value = this._device.DifUM_Type;
                this._difensesUMDataGrid["_uMUsrColumn", index - 1].Value = this._device.DifUM_Usr;
                this._difensesUMDataGrid["_uMTsrColumn", index - 1].Value = this._device.DifUM_Tsr;
                this._difensesUMDataGrid["_uMTvzColumn", index - 1].Value = this._device.DifUM_Tvz;
                this._difensesUMDataGrid["_uMUvzColumn", index - 1].Value = this._device.DifUM_Uvz;
                this._difensesUMDataGrid["_uMUvzYNColumn", index - 1].Value = this._device.DifUM_UvzYN;
                this._difensesUMDataGrid["_uMBlockingUMColumn", index - 1].Value = this._device.DifUM_BlockingUM;
                this._difensesUMDataGrid["_uMBlockingColumn", index - 1].Value = this._device.DifUM_Blocking;
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesUMDataGrid["_uMOscColumn", index - 1].Value = this._device.DifUM_Osc;
                }
                else
                {
                    this._difensesUMDataGrid["_uMOscColumn", index - 1].Value = this._device.DifUM_Osc_v_1_11;
                }
                
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesUMDataGrid["_uMUROVColumn", index - 1].Value = this._device.DifUM_UROV;
                    this._difensesUMDataGrid["_uMAPVColumn", index - 1].Value = this._device.DifUM_APV;
                    this._difensesUMDataGrid["_uMAVRColumn", index - 1].Value = this._device.DifUM_AVR;
                    this._difensesUMDataGrid["_uMAPVRetColumn", index - 1].Value = this._device.DifUM_APVRet;
                    this._difensesUMDataGrid["_uMSbrosColumn", index - 1].Value = this._device.DifUM_Sbros;
                }
            
            }
            catch
            {
                int f = 5;
            }
        }

        private void WriteDifUM(int index)
        {
            this._device.DifUM_Mode = this._difensesUMDataGrid["_uMModesColumn", index - 1].Value.ToString();
            this._device.DifUM_Type = this._difensesUMDataGrid["_uMTypeColumn", index - 1].Value.ToString();
            this._device.DifUM_Usr = Convert.ToDouble(this._difensesUMDataGrid["_uMUsrColumn", index - 1].Value.ToString());
            this._device.DifUM_Tsr = Convert.ToDouble(this._difensesUMDataGrid["_uMTsrColumn", index - 1].Value.ToString());
            this._device.DifUM_Tvz = Convert.ToDouble(this._difensesUMDataGrid["_uMTvzColumn", index - 1].Value.ToString());
            this._device.DifUM_Uvz = Convert.ToDouble(this._difensesUMDataGrid["_uMUvzColumn", index - 1].Value.ToString());
            this._device.DifUM_UvzYN = Convert.ToBoolean(this._difensesUMDataGrid["_uMUvzYNColumn", index - 1].Value);
            this._device.DifUM_BlockingUM = this._difensesUMDataGrid["_uMBlockingUMColumn", index - 1].Value.ToString();
            this._device.DifUM_Blocking = this._difensesUMDataGrid["_uMBlockingColumn", index - 1].Value.ToString();
            
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifUM_Osc = this._difensesUMDataGrid["_uMOscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifUM_Osc_v_1_11 = this._difensesUMDataGrid["_uMOscColumn", index - 1].Value.ToString();
            }
            
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifUM_UROV = this._difensesUMDataGrid["_uMUROVColumn", index - 1].Value.ToString();
                this._device.DifUM_APV = this._difensesUMDataGrid["_uMAPVColumn", index - 1].Value.ToString();
                this._device.DifUM_AVR = this._difensesUMDataGrid["_uMAVRColumn", index - 1].Value.ToString();
                this._device.DifUM_APVRet = this._difensesUMDataGrid["_uMAPVRetColumn", index - 1].Value.ToString();
                this._device.DifUM_Sbros = this._difensesUMDataGrid["_uMSbrosColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #region Защиты F>
        private void PrepareFBGrid()
        {
            //this.FillFBGrid();
            this._difensesFBDataGrid.CellValueChanged += this.OnCellValueChanged;
            //if (this._difensesFBDataGrid.Rows.Count == 0)
            //{
                //string a = ">";
                for (int i = 0; i < 4; i++)
                {
                    //this._difensesFBDataGrid.Rows.Add("Ступень F> " + (i + 1), Strings.ModesLightMode[0], 40, 0, 0, 40, false, Strings.DiffBlocking[0], Strings.ModesLightOsc[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.YesNo[0]);
                    //a += ">";
                    this.OnCellValueChanged(this._difensesFBDataGrid, new DataGridViewCellEventArgs(1, i));
                }
            //}
            this._difensesFBDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }
        
        private void ReadDifFB(int index)
        {
            try
            {
                this._difensesFBDataGrid["_fBModesColumn", index - 1].Value = this._device.DifFB_Mode;
                this._difensesFBDataGrid["_fBUsrColumn", index - 1].Value = this._device.DifFB_Usr;
                this._difensesFBDataGrid["_fBTsrColumn", index - 1].Value = this._device.DifFB_Tsr;
                this._difensesFBDataGrid["_fBTvzColumn", index - 1].Value = this._device.DifFB_Tvz;
                this._difensesFBDataGrid["_fBUvzColumn", index - 1].Value = this._device.DifFB_Uvz;
                this._difensesFBDataGrid["_fBUvzYNColumn", index - 1].Value = this._device.DifFB_UvzYN;
                this._difensesFBDataGrid["_fBBlockingColumn", index - 1].Value = this._device.DifFB_Blocking;
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesFBDataGrid["_fBOscColumn", index - 1].Value = this._device.DifFB_Osc;
                }
                else
                {
                    this._difensesFBDataGrid["_fBOscColumn", index - 1].Value = this._device.DifFB_Osc_v_1_11;
                }
                
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesFBDataGrid["_fBUROVColumn", index - 1].Value = this._device.DifFB_UROV;
                    this._difensesFBDataGrid["_fBAPVColumn", index - 1].Value = this._device.DifFB_APV;
                    this._difensesFBDataGrid["_fBAVRColumn", index - 1].Value = this._device.DifFB_AVR;
                    this._difensesFBDataGrid["_fBAPVRetColumn", index - 1].Value = this._device.DifFB_APVRet;
                    this._difensesFBDataGrid["_fBSbrosColumn", index - 1].Value = this._device.DifFB_Sbros;
                }
            }
            catch{}
        }

        private void WriteDifFB(int index)
        {
            this._device.DifFB_Mode = this._difensesFBDataGrid["_fBModesColumn", index - 1].Value.ToString();
            this._device.DifFB_Usr = Convert.ToDouble(this._difensesFBDataGrid["_fBUsrColumn", index - 1].Value.ToString());
            this._device.DifFB_Tsr = Convert.ToDouble(this._difensesFBDataGrid["_fBTsrColumn", index - 1].Value.ToString());
            this._device.DifFB_Tvz = Convert.ToDouble(this._difensesFBDataGrid["_fBTvzColumn", index - 1].Value.ToString());
            this._device.DifFB_Uvz = Convert.ToDouble(this._difensesFBDataGrid["_fBUvzColumn", index - 1].Value.ToString());
            this._device.DifFB_UvzYN = Convert.ToBoolean(this._difensesFBDataGrid["_fBUvzYNColumn", index - 1].Value);
            this._device.DifFB_Blocking = this._difensesFBDataGrid["_fBBlockingColumn", index - 1].Value.ToString();
            
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifFB_Osc = this._difensesFBDataGrid["_fBOscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifFB_Osc_v_1_11 = this._difensesFBDataGrid["_fBOscColumn", index - 1].Value.ToString();
            }
            
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifFB_UROV = this._difensesFBDataGrid["_fBUROVColumn", index - 1].Value.ToString();
                this._device.DifFB_APV = this._difensesFBDataGrid["_fBAPVColumn", index - 1].Value.ToString();
                this._device.DifFB_AVR = this._difensesFBDataGrid["_fBAVRColumn", index - 1].Value.ToString();
                this._device.DifFB_APVRet = this._difensesFBDataGrid["_fBAPVRetColumn", index - 1].Value.ToString();
                this._device.DifFB_Sbros = this._difensesFBDataGrid["_fBSbrosColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #region Защиты F<
        private void PrepareFMGrid()
        {
            //this.FillFMGrid();
            this._difensesFMDataGrid.CellValueChanged += this.OnCellValueChanged;
            //if (this._difensesFMDataGrid.Rows.Count == 0)
            //{
            //    string a = ">";
                for (int i = 0; i < 4; i++)
                {
                    //this._difensesFMDataGrid.Rows.Add("Ступень F< " + (i + 1), Strings.ModesLightMode[0], 40, 0, 0, 40, false, Strings.DiffBlocking[0], Strings.ModesLightOsc[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.YesNo[0]);
                    //a += ">";
                    this.OnCellValueChanged(this._difensesFMDataGrid, new DataGridViewCellEventArgs(1, i));
                }
            //}
            this._difensesFMDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }
        
        private void ReadDifFM(int index)
        {
            try
            {
                this._difensesFMDataGrid["_fMModesColumn", index - 1].Value = this._device.DifFM_Mode;
                this._difensesFMDataGrid["_fMUsrColumn", index - 1].Value = this._device.DifFM_Usr;
                this._difensesFMDataGrid["_fMTsrColumn", index - 1].Value = this._device.DifFM_Tsr;
                this._difensesFMDataGrid["_fMTvzColumn", index - 1].Value = this._device.DifFM_Tvz;
                this._difensesFMDataGrid["_fMUvzColumn", index - 1].Value = this._device.DifFM_Uvz;
                this._difensesFMDataGrid["_fMUvzYNColumn", index - 1].Value = this._device.DifFM_UvzYN;
                this._difensesFMDataGrid["_fMBlockingColumn", index - 1].Value = this._device.DifFM_Blocking;
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._difensesFMDataGrid["_fMOscColumn", index - 1].Value = this._device.DifFM_Osc;
                }
                else
                {
                    this._difensesFMDataGrid["_fMOscColumn", index - 1].Value = this._device.DifFM_Osc_v_1_11;
                }
                
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._difensesFMDataGrid["_fMUROVColumn", index - 1].Value = this._device.DifFM_UROV;
                    this._difensesFMDataGrid["_fMAPVColumn", index - 1].Value = this._device.DifFM_APV;
                    this._difensesFMDataGrid["_fMAVRColumn", index - 1].Value = this._device.DifFM_AVR;
                    this._difensesFMDataGrid["_fMAPVRetColumn", index - 1].Value = this._device.DifFM_APVRet;
                    this._difensesFMDataGrid["_fMSbrosColumn", index - 1].Value = this._device.DifFM_Sbros;
                }
            }
            catch
            {
            }
        }

        private void WriteDifFM(int index)
        {
            this._device.DifFM_Mode = this._difensesFMDataGrid["_fMModesColumn", index - 1].Value.ToString();
            this._device.DifFM_Usr = Convert.ToDouble(this._difensesFMDataGrid["_fMUsrColumn", index - 1].Value.ToString());
            this._device.DifFM_Tsr = Convert.ToDouble(this._difensesFMDataGrid["_fMTsrColumn", index - 1].Value.ToString());
            this._device.DifFM_Tvz = Convert.ToDouble(this._difensesFMDataGrid["_fMTvzColumn", index - 1].Value.ToString());
            this._device.DifFM_Uvz = Convert.ToDouble(this._difensesFMDataGrid["_fMUvzColumn", index - 1].Value.ToString());
            this._device.DifFM_UvzYN = Convert.ToBoolean(this._difensesFMDataGrid["_fMUvzYNColumn", index - 1].Value);
            this._device.DifFM_Blocking = this._difensesFMDataGrid["_fMBlockingColumn", index - 1].Value.ToString();
            
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.DifFM_Osc = this._difensesFMDataGrid["_fMOscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.DifFM_Osc_v_1_11 = this._difensesFMDataGrid["_fMOscColumn", index - 1].Value.ToString();
            }
            
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.DifFM_UROV = this._difensesFMDataGrid["_fMUROVColumn", index - 1].Value.ToString();
                this._device.DifFM_APV = this._difensesFMDataGrid["_fMAPVColumn", index - 1].Value.ToString();
                this._device.DifFM_AVR = this._difensesFMDataGrid["_fMAVRColumn", index - 1].Value.ToString();
                this._device.DifFM_APVRet = this._difensesFMDataGrid["_fMAPVRetColumn", index - 1].Value.ToString();
                this._device.DifFM_Sbros = this._difensesFMDataGrid["_fMSbrosColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #region Внешние
        private void PrepareExternalDifensesGrid()
        {
            //this.FillExternalDifensesGrid();
            this._externalDifensesDataGrid.CellValueChanged += this.OnCellValueChanged;
            //if (this._externalDifensesDataGrid.Rows.Count == 0)
            //{
            //    string a = ">";
                for (int i = 0; i < 16; i++)
                {
                    //this._externalDifensesDataGrid.Rows.Add("Внешняя " + (i + 1), Strings.ModesLightMode[0], Strings.SygnalSrab[0], 0, 0, Strings.SygnalSrab[0], false, Strings.SygnalSrab[0], Strings.ModesLightOsc[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.ModesLight[0], Strings.YesNo[0]);
                    //a += ">";
                    this.OnCellValueChanged(this._externalDifensesDataGrid, new DataGridViewCellEventArgs(1,i));
                }
            //}
            this._externalDifensesDataGrid.CellBeginEdit += this.OnGridCellBeginEdit;
        }

        private void ReadExtDif(int index)
        {
            try
            {
                this._externalDifensesDataGrid["_externalDifModesColumn", index - 1].Value = this._device.ExtDif_Modes;
                this._externalDifensesDataGrid["_externalDifSrabColumn", index - 1].Value = this._device.ExtDif_Srab;
                this._externalDifensesDataGrid["_externalDifTsrColumn", index - 1].Value = this._device.ExtDif_Tsr;
                this._externalDifensesDataGrid["_externalDifTvzColumn", index - 1].Value = this._device.ExtDif_Tvz;
                this._externalDifensesDataGrid["_externalDifVozvrColumn", index - 1].Value = this._device.ExtDif_Vozvr;
                this._externalDifensesDataGrid["_externalDifVozvrYNColumn", index - 1].Value = this._device.ExtDif_VozvrYN;
                this._externalDifensesDataGrid["_externalDifBlockingColumn", index - 1].Value = this._device.ExtDif_Blocking;
                
                if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
                {
                    this._externalDifensesDataGrid["_externalDifOscColumn", index - 1].Value = this._device.ExtDif_Osc;
                }
                else
                {
                    this._externalDifensesDataGrid["_externalDifOscColumn", index - 1].Value = this._device.ExtDif_Osc_v_1_11;
                }
                
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
                {
                    this._externalDifensesDataGrid["_externalDifUROVColumn", index - 1].Value = this._device.ExtDif_UROV;
                    this._externalDifensesDataGrid["_externalDifAPVColumn", index - 1].Value = this._device.ExtDif_APV;
                    this._externalDifensesDataGrid["_externalDifAVRColumn", index - 1].Value = this._device.ExtDif_AVR;
                    this._externalDifensesDataGrid["_externalDifAPVRetColumn", index - 1].Value = this._device.ExtDif_APVRet;
                    this._externalDifensesDataGrid["_externalDifSbrosColumn", index - 1].Value = this._device.ExtDif_Sbros;
                }
            }
            catch
            {
            }
        }

        private void WriteExtDif(int index)
        {
            this._device.ExtDif_Modes = this._externalDifensesDataGrid["_externalDifModesColumn", index - 1].Value.ToString();
            this._device.ExtDif_Srab = this._externalDifensesDataGrid["_externalDifSrabColumn", index - 1].Value.ToString();
            this._device.ExtDif_Tsr = Convert.ToDouble(this._externalDifensesDataGrid["_externalDifTsrColumn", index - 1].Value.ToString());
            this._device.ExtDif_Tvz = Convert.ToDouble(this._externalDifensesDataGrid["_externalDifTvzColumn", index - 1].Value.ToString());
            this._device.ExtDif_Vozvr = this._externalDifensesDataGrid["_externalDifVozvrColumn", index - 1].Value.ToString();
            this._device.ExtDif_VozvrYN = Convert.ToBoolean(this._externalDifensesDataGrid["_externalDifVozvrYNColumn", index - 1].Value);
            this._device.ExtDif_Blocking = this._externalDifensesDataGrid["_externalDifBlockingColumn", index - 1].Value.ToString();
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.11)
            {
                this._device.ExtDif_Osc = this._externalDifensesDataGrid["_externalDifOscColumn", index - 1].Value.ToString();
            }
            else
            {
                this._device.ExtDif_Osc_v_1_11 = this._externalDifensesDataGrid["_externalDifOscColumn", index - 1].Value.ToString();
            }
            
            if (Common.VersionConverter(this._device.DeviceVersion) >= 1.11)
            {
                this._device.ExtDif_UROV = this._externalDifensesDataGrid["_externalDifUROVColumn", index - 1].Value.ToString();
                this._device.ExtDif_APV = this._externalDifensesDataGrid["_externalDifAPVColumn", index - 1].Value.ToString();
                this._device.ExtDif_AVR = this._externalDifensesDataGrid["_externalDifAVRColumn", index - 1].Value.ToString();
                this._device.ExtDif_APVRet = this._externalDifensesDataGrid["_externalDifAPVRetColumn", index - 1].Value.ToString();
                this._device.ExtDif_Sbros = this._externalDifensesDataGrid["_externalDifSbrosColumn", index - 1].Value.ToString();
            }
        }
        #endregion

        #endregion

        #region Осциллограф
        private void PrepareOscilloscope()
        {
            if (this._vers >= 1.11 && this._vers < 2.0)
            {
                this._oscMemorySize = 26168;
            }
            else if (this._vers >= 2.0)
            {
                this._oscMemorySize = 48469;
            }
            if (this._vers < 1.1)
            {
                this._oscConfig.Items.Clear();
                this._oscConfig.Items.AddRange(Strings.OscConfig.ToArray());
                this._oscConfig.SelectedIndex = 0;
            }
            else
            {
                this._oscopeChannels = new ComboBox[]
                    {
                        this._oscopeChannelComboBox1, 
                        this._oscopeChannelComboBox2, 
                        this._oscopeChannelComboBox3, 
                        this._oscopeChannelComboBox4, 
                        this._oscopeChannelComboBox5, 
                        this._oscopeChannelComboBox6, 
                        this._oscopeChannelComboBox7, 
                        this._oscopeChannelComboBox8
                    };
                this._oscopeCountComboBox.Items.Clear();
                this._oscopeCountComboBox.Items.AddRange(Strings.OscopeCount1_11.ToArray());
                this._oscopeCountComboBox.SelectedIndex = 0;

                this._oscopeFixationComboBox.Items.Clear();
                this._oscopeFixationComboBox.Items.AddRange(Strings.OscopeFixation1_11.ToArray());
                this._oscopeFixationComboBox.SelectedIndex = 0;

                this._oscopePreRecordingLenghtTextBox.Text = "0";
                foreach (var oscopeChannel in this._oscopeChannels)
                {
                    this.PrepareChannelComboBox(oscopeChannel);
                }           
            }
        }
        private void PrepareChannelComboBox(ComboBox channelComboBox)
        {
            channelComboBox.Items.Clear();
            channelComboBox.Items.AddRange(Strings.Sygnal.ToArray());
            channelComboBox.Items.Remove("ХХХХХ");
            channelComboBox.SelectedIndex = 0;
        }

        private void ReadOscilloscope()
        {
            if (this._vers < 1.1)
                this._oscConfig.SelectedItem = this._device.OscConfig;
            else
            {
                var result = this._device.OscConfig1_11;
                this._oscopeFixationComboBox.SelectedIndex = result[0];
                this._oscopeCountComboBox.SelectedIndex = result[1]-1;
                this._oscopePreRecordingLenghtTextBox.Text = result[2].ToString(CultureInfo.InvariantCulture);
                for (var i = 0; i < this._oscopeChannels.Length; i++)
                {
                    this._oscopeChannels[i].SelectedIndex = result[3 + i];
                }
            }
        }

        private void ResetOscilloscope()
        {
            if (this._vers < 1.1)
                this._oscConfig.SelectedIndex = 0;
            else
            {
                this._oscopeFixationComboBox.SelectedIndex = 0;
                this._oscopeCountComboBox.SelectedIndex = 0;
                this._oscopePreRecordingLenghtTextBox.Text = 1.ToString();
                foreach (ComboBox cb in this._oscopeChannels)
                {
                    cb.SelectedIndex = 0;
                }
            }
        }

        private bool WriteOscilloscope()
        {
            bool rez = false;
            if (this._vers < 1.1)
            {
                try
                {
                    this._device.OscConfig = this._oscConfig.SelectedItem.ToString();
                    rez = true;
                }
                catch
                {
                    MessageBox.Show(WRITE_OSC_ERROR);
                }
            }
            else
            {
                try
                {
                    var result = new ushort[11];
                    result[0] = (ushort) this._oscopeFixationComboBox.SelectedIndex;
                    result[1] = (ushort) (this._oscopeCountComboBox.SelectedIndex+1);
                    var preRecording = ushort.Parse(this._oscopePreRecordingLenghtTextBox.Text);
                    if (preRecording <= 100)
                        result[2] = preRecording;
                    else
                        return false;

                    for (var i = 0; i < this._oscopeChannels.Length; i++)
                    {
                        result[3 + i] = (ushort) this._oscopeChannels[i].SelectedIndex;
                    }
                    this._device.OscConfig1_11 = result;
                    rez = true;
                }
                catch
                {
                    MessageBox.Show(WRITE_OSC_ERROR);
                }

            }

            return rez;
        }

        private void _device_OscopeSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_OscopeSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_OscopeLoadFail(object sender)
        {
            //throw new NotImplementedException();
        }

        private void _device_OscopeLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadOscilloscope));
                Invoke(new OnDeviceEventHandler(this.OnLoadOK));
            }
            catch (Exception ee) { }
        }

        private void _device_OscLengthLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OscLenghtLoad));
            }
            catch 
            {
            }
        }

        private void OscLenghtLoad()
        {
            if(this._device.LoadedFullOscSize / this._device.LoadedOscPoint > 0)
                this._oscMemorySize = this._device.LoadedFullOscSize / this._device.LoadedOscPoint;
        }
        #endregion

        #region Other

        private void SaveOk(object o)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void SaveFail(object o)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void SaveComplete(object o)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
            }
            catch (InvalidOperationException)
            { }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;
                try
                {
                    if (!e.IsValidInput)
                    {
                        this.ShowToolTip(box);
                    }
                    else
                    {
                        if (box.ValidatingType == typeof (ulong) &&
                            ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                        {
                            this.ShowToolTip(box);
                        }
                        if (box.ValidatingType == typeof (double) && double.Parse(box.Text) > double.Parse(box.Tag.ToString()) || (double.Parse(box.Text) < 0))
                        {
                            this.ShowToolTip(box);
                        }
                    }
                }
                catch (Exception)
                {
                    this.ShowToolTip(box);
                }
            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    if (box.Parent.Parent.Parent.Parent.Parent is TabPage)
                    {
                        this._configurationTabControl.SelectedTab = box.Parent.Parent.Parent.Parent.Parent as TabPage;
                        this._difensesTC.SelectedTab = box.Parent.Parent as TabPage;
                    }
                    else
                    {
                        this._configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                    }
                }
                switch (box.ValidatingType.Name)
                {
                    case "Double":
                        this._toolTip.Show("Введите число в диапазоне [0-" + box.Tag + "]\n" +
                                      "или измените десятичный разделитель вводимого числа", box, 2000);
                        break;
                    case "UInt64":
                        this._toolTip.Show("Введите целое число в диапазоне [0-" + box.Tag + "]", box, 2000);
                        break;
                    default: break;
                }
                box.Focus();
                box.SelectAll();
            this._validatingOk = false;
            //}
        }

        private void ValidateAll()
        {
            this._validatingOk = true;
            //Проверка MaskedTextBox
            this.ValidatePowerTrans();
            this.ValidateMeasuringTrans();
            this.ValidateDifPower();
            this.ValidateReleNeispr();
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                this.ValidateSwitch();
                this.ValidateAVR();
                this.ValidateAPV();
                this.ValidateLZH();
            }
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteSwich()
                                              && this.WriteApv()
                                              && this.WriteAvr()
                                              && this.WriteLzh()
                                              && this.WritePowerTrans()
                                              && this.WriteConfigEthernet()
                                              && this.WriteLogycSygnals()
                                              && this.WriteParamAutomat()
                                              && this.WriteVLSSygnals()
                                              && this.WriteInputSignals()
                                              && this.WriteMeasureTrans()
                                              && this.WriteOscilloscope()
                                              && this.WriteLastCurrentProtAll();
            }
        }

        private void ValidatePowerTrans()
        {
            this.ValidateMaskedBoxes(this._ULONGpowerTransMaskedTextBoxes);
            this.ValidateMaskedBoxes(this._DOUBLEpowerTransMaskedTextBoxes);
        }

        private void ValidateMeasuringTrans()
        {
            this.ValidateMaskedBoxes(this._ULONGmeasureTransMaskedTextBoxes);
            this.ValidateMaskedBoxes(this._DOUBLEmeasureTransMaskedTextBoxes);
        }

        private void ValidateDifPower()
        {
            this.ValidateMaskedBoxes(this._ULONGDifPowerMaskedTextBoxes);
            this.ValidateMaskedBoxes(this._DOUBLEDifPowerMaskedTextBoxes);
        }

        private void ValidateSwitch()
        {
            this.ValidateMaskedBoxes(this._ULONGswitchMaskedTextBoxes);
            this.ValidateMaskedBoxes(this._DOUBLEswitchMaskedTextBoxes);
        }

        private void ValidateAVR()
        {
            this.ValidateMaskedBoxes(this._ULONGavrTransMaskedTextBoxes);
        }

        private void ValidateAPV()
        {
            this.ValidateMaskedBoxes(this._ULONGapvTransMaskedTextBoxes);
        }

        private void ValidateLZH()
        {
            this.ValidateMaskedBoxes(this._DOUBLElzhTransMaskedTextBoxes);
        }

        private void ValidateReleNeispr()
        {
            this.ValidateMaskedBoxes(this._ulongrRelayNeisprTImpTextBoxes);
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void OnLoadOK()
        {
            this._exchangeProgressBar.PerformStep();
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("Невозможно прочитать конфигурацию. Проверьте связь", "MR801. Ошибка чтения", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("Невозможно записать конфигурацию. Проверьте связь", "Уникон-MR801. Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "Чтение завершено неуспешно";
            }
            else
            {
                this._processLabel.Text = "Чтение успешно завершено";
            }
        }

        private void OnSaveComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "Запись завершена неуспешно";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "Запись успешно завершена";
            }
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
        }

        private void ConfigurationFormClosing(object sender, FormClosingEventArgs e)
        {
            this.UnsubscribeLoad();
            this.UnsubscriptOnSaveHandlers();
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Maximum = 55;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "Идет чтение...";
            this._connectingErrors = false;
            this._messagePower = false;
            this._messageMeasure = false;
            this._messageDifPower = false;

            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                this._device.LoadSwitch();
                this._device.LoadAPV();
                this._device.LoadAVR();
                this._device.LoadLPB();
            }
            this._device.LoadAutoblower();
            this._device.LoadTermal();
            this._device.LoadInputSygnal();
            this._device.LoadOscope();
            this._device.LoadPowerTrans();
            this._device.LoadMeasureTrans();
            this._device.LoadInpSygnal();
            this._device.LoadElsSygnal();
            this._device.LoadProtAll();
            this._device.LoadParamAutomat();
            this._device.LoadConfigEthernet();
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._connectingErrors = false;
            this._exchangeProgressBar.Maximum = 100;
            this._exchangeProgressBar.Value = 0;
            this._validatingOk = true;

            this.ValidateAll();

            if (!this._validatingGb)
            {
                this._validatingOk = false;
                this.ShowError();
            }

            if (this._validatingOk)
            {
                if (_vers > 2.07)
                {
                    DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 801 №{0}? " +
                                                                        "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                            this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                        "Запись", MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                else
                {
                    DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 801 №{0}? ", this._device.DeviceNumber),
                        "Запись", MessageBoxButtons.YesNo);
                    if (result != DialogResult.Yes) return;
                }
                
                this._processLabel.Text = "Идет запись";
                if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
                {
                    this._device.SaveSwitch();
                    this._device.SaveAPV();
                    this._device.SaveAVR();
                    this._device.SaveLPB();
                }
                this._device.SaveInpSygnal();
                this._device.SaveElsSygnal();
                this._device.SaveInputSygnal();
                this._device.SaveParamAutomat();
                this._device.SavePowerTrans();
                this._device.SaveConfigEthernet();
                this._device.SaveOscope();
                this._device.SaveMeasureTrans();
                this._device.SaveProtAll();
                this._device.ConfirmConstraint();
                
            }
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ResetSetpointBtnClick(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
        }

        private void ResetSetpoints(bool isDialog)
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show(@"Загрузить базовые уставки?", @"Базовые уставки",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.No) return;
            }

            string path = this._vers >= 2.08
                ? Path.GetDirectoryName(Application.ExecutablePath) + "\\MR801\\MR801_BaseConfig_v2.08.bin"
                : Path.GetDirectoryName(Application.ExecutablePath) + "\\MR801\\MR801_BaseConfig_v2.07.bin";

            if (File.Exists(path) && Common.VersionConverter(this._device.DeviceVersion) >= 1.20)
            {
                this.ReadFromFile(path);
                this._processLabel.Text = "Базовые ставки успешно загружены";
            }
            else
            {
                MessageBox.Show("Невозможно загрузить базовые уставки. Файл базовых уставок не существует. Уставки будут обнулены.",
                    "Базовые уставки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
                {
                    this.ResetSwich();
                    this.ResetApv();
                    this.ResetAvr();
                    this.ResetLZH();
                }
                this.ResetNeispr();
                this.ResetIndicators();
                this.ResetMeasureTrans();
                this.ResetPowerTrans();
                this.ResetRele();
                this.ResetVLSSygnals();
                this.ResetLogycSygnals();
                this.ResetInputSignals();
                this.ResetCurrentProtAll();
                this.ResetOscilloscope();
            }
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.ReadFromFile(this._openConfigurationDlg.FileName);
            }
        }

        private void ReadFromFile(string filename)
        {
            this.firstReserve = true;
            this.firstMain = true;
            this._processLabel.Text = string.Empty;
            this._exchangeProgressBar.Value = 0;
            try
            {
                this._device.Deserialize(filename);
                this._messagePower = false;
                this._messageMeasure = false;
                this._messageDifPower = false;
                if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
                {
                    this.ReadSwich();
                    this.ReadApv();
                    this.ReadAvr();
                    this.ReadLZH();
                }
                this.ReadNeispr();
                this.ReadIndicators();
                this.ReadMeasureTrans();
                this.ReadPowerTrans();
                if (_vers >= 2.07)
                {
                    this.ReadConfigEthernet();
                }
                this.ReadRele();
                this.ReadVLSSygnals();
                this.ReadLogycSygnals();
                this.ReadInputSignals();
                this.ReadCurrentProtAll();
                this.ReadOscilloscope();
            }
            catch (FileLoadException exc)
            {
                MessageBox.Show(string.Format("Файл {0} не является файлом уставок МР801 или поврежден",
                    Path.GetFileName(exc.FileName)), "Ошибка чтения", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this._exchangeProgressBar.Value = this._exchangeProgressBar.Maximum;
            this._processLabel.Text = "Файл " + this._openConfigurationDlg.FileName + " загружен";
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            this.ValidateAll();
            this._processLabel.Text = string.Empty;
            this._exchangeProgressBar.Value = 0;
            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("МР801_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._device.Serialize(this._saveConfigurationDlg.FileName);
                    this._exchangeProgressBar.Value = this._exchangeProgressBar.Maximum;
                    this._processLabel.Text = "Файл " + this._saveConfigurationDlg.FileName + " сохранен";
                }
            }
        }

        private void OnGridCellBeginEdit(object sender, DataGridViewCellCancelEventArgs cellCancelEventArgs)
        {
            this.contextMenu.Tag = sender;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == resetSetpointContext)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
        #endregion

        #region Выключатель и Управление

        private void _device_SwitchLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_SwitchLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadSwich));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_SwitchSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_SwitchSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void PrepareSwich()
        {
            ComboBox[] combos = new ComboBox[]
            {
                this._switch, this._switchOff, this._switchOn, this._switchError, this._switchBlock,
                this._switchKontCep, this._switchKeyOn, this._switchKeyOff, this._switchVneshOn, this._switchVneshOff,
                this._switchButtons,this._switchKey, this._switchVnesh, this._switchSDTU, this._switchBinding
            };

            this._ULONGswitchMaskedTextBoxes = new MaskedTextBox[] {this._switchImp, this._switchTUskor, this._switchTUrov };
            this._DOUBLEswitchMaskedTextBoxes = new MaskedTextBox[] {this._switchIUrov };

            this.ClearCombos(combos);
            this.FillSwitchCombos(combos);

            this.PrepareMaskedBoxes(this._ULONGswitchMaskedTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLEswitchMaskedTextBoxes, typeof(double));
        }

        private void FillSwitchCombos(ComboBox[] combos)
        {
            this._switch.Items.AddRange(Strings.YesNo.ToArray());
            this._switchOff.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchOn.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchError.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchBlock.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchKontCep.Items.AddRange(Strings.ModesLight.ToArray());
            this._switchKeyOn.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchKeyOff.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchVneshOn.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchVneshOff.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._switchButtons.Items.AddRange(Strings.Forbidden.ToArray());
            this._switchKey.Items.AddRange(Strings.Forbidden2.ToArray());
            this._switchVnesh.Items.AddRange(Strings.Forbidden2.ToArray());
            this._switchSDTU.Items.AddRange(Strings.Forbidden.ToArray());
            this._switchBinding.Items.AddRange(Strings.Bindings2.ToArray());
            foreach (ComboBox box in combos)
            {
                box.SelectedIndex = 0;
            }
        }

        private void ReadSwich()
        {
            this._switch.SelectedItem = this._device.Switch;
            this._switchOff.SelectedItem = this._device.SwitchOff;
            this._switchOn.SelectedItem = this._device.SwitchOn;
            this._switchError.SelectedItem = this._device.SwitchError;
            this._switchBlock.SelectedItem = this._device.SwitchBlock;
            this._switchTUrov.Text = this._device.SwitchTUrov.ToString();
            this._switchIUrov.Text = this._device.SwitchIUrov.ToString();
            this._switchImp.Text = this._device.SwitchImpuls.ToString();
            this._switchTUskor.Text = this._device.SwitchTUskor.ToString();
            this._switchKontCep.SelectedItem = this._device.SwitchKontCep;

            this._switchKeyOn.SelectedItem = this._device.SwitchKeyOn;
            this._switchKeyOff.SelectedItem = this._device.SwitchKeyOff;
            this._switchVneshOn.SelectedItem = this._device.SwitchVneshOn;
            this._switchVneshOff.SelectedItem = this._device.SwitchVneshOff;
            this._switchButtons.SelectedItem = this._device.SwitchButtons;
            this._switchKey.SelectedItem = this._device.SwitchKey;
            this._switchVnesh.SelectedItem = this._device.SwitchVnesh;
            this._switchSDTU.SelectedItem = this._device.SwitchSDTU;
            this._switchBinding.SelectedItem = this._device.SwitchBindings;
        }

        private void ResetSwich()
        {
            this._switch.SelectedIndex = 0;
            this._switchOff.SelectedIndex = 0;
            this._switchOn.SelectedIndex = 0;
            this._switchError.SelectedIndex = 0;
            this._switchBlock.SelectedIndex = 0;
            this._switchTUrov.Text = "0";
            this._switchIUrov.Text = "0";
            this._switchImp.Text = "0";
            this._switchTUskor.Text = "0";
            this._switchKontCep.SelectedIndex = 0;

            this._switchKeyOn.SelectedIndex = 0;
            this._switchKeyOff.SelectedIndex = 0;
            this._switchVneshOn.SelectedIndex = 0;
            this._switchVneshOff.SelectedIndex = 0;
            this._switchButtons.SelectedIndex = 0;
            this._switchKey.SelectedIndex = 0;
            this._switchVnesh.SelectedIndex = 0;
            this._switchSDTU.SelectedIndex = 0;
            this._switchBinding.SelectedIndex = 0;
        }

        private bool WriteSwich()
        {
            bool ret = true;
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                try
                {
                    this._device.Switch = this._switch.SelectedItem.ToString();
                    this._device.SwitchOff = this._switchOff.SelectedItem.ToString();
                    this._device.SwitchOn = this._switchOn.SelectedItem.ToString();
                    this._device.SwitchError = this._switchError.SelectedItem.ToString();
                    this._device.SwitchBlock = this._switchBlock.SelectedItem.ToString();
                    this._device.SwitchTUrov = Convert.ToInt32(this._switchTUrov.Text);

                    //проверка значения
                    var iUrov = Convert.ToDouble(this._switchIUrov.Text);
                    if ((iUrov< 0)|(iUrov> 40))
                        throw new ArgumentException();

                    this._device.SwitchIUrov = iUrov;


                    this._device.SwitchImpuls = Convert.ToInt32(this._switchImp.Text);
                    this._device.SwitchTUskor = Convert.ToInt32(this._switchTUskor.Text);
                    this._device.SwitchKontCep = this._switchKontCep.SelectedItem.ToString();

                    this._device.SwitchKeyOn = this._switchKeyOn.SelectedItem.ToString();
                    this._device.SwitchKeyOff = this._switchKeyOff.SelectedItem.ToString();
                    this._device.SwitchVneshOn = this._switchVneshOn.SelectedItem.ToString();
                    this._device.SwitchVneshOff = this._switchVneshOff.SelectedItem.ToString();
                    this._device.SwitchButtons = this._switchButtons.SelectedItem.ToString();
                    this._device.SwitchKey = this._switchKey.SelectedItem.ToString();
                    this._device.SwitchVnesh = this._switchVnesh.SelectedItem.ToString();
                    this._device.SwitchSDTU = this._switchSDTU.SelectedItem.ToString();
                    this._device.SwitchBindings = this._switchBinding.SelectedItem.ToString();
                }
                catch (Exception)
                {
                    ret = false;
                    MessageBox.Show("Произошла ошибка во время записи блока Выключатель.");
                }
            }
            return ret;
        }
        #endregion

        #region АПВ

        private void _device_APVSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_APVSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_APVLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_APVLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadApv));
            }
            catch (Exception ee)
            {

            }
        }

        private void PrepareApv() 
        {
            ComboBox[] combos = new ComboBox[] {this._apvModes, this._apvBlocking, this._apvOff };

            this._ULONGapvTransMaskedTextBoxes = new MaskedTextBox[] {this._apvTBlock, this._apvTReady, this._apv1Krat, this._apv2Krat };

            this.ClearCombos(combos);
            this.FillApvCombos();

            this.PrepareMaskedBoxes(this._ULONGapvTransMaskedTextBoxes, typeof(ulong));
        }

        private void FillApvCombos() 
        {
            this._apvModes.Items.AddRange(Strings.ApvTypes.ToArray());
            this._apvModes.SelectedIndex = 0;
            this._apvBlocking.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._apvBlocking.SelectedIndex = 0;
            this._apvOff.Items.AddRange(Strings.APVModes.ToArray());
            this._apvOff.SelectedIndex = 0;
        }

        private void ReadApv() 
        {
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                this._apvModes.SelectedItem = this._device.APVModes;
                this._apvBlocking.SelectedItem = this._device.APVBlocking;
                this._apvTBlock.Text = this._device.APVTBlock.ToString();
                this._apvTReady.Text = this._device.APVTReady.ToString();
                this._apv1Krat.Text = this._device.APV1Krat.ToString();
                this._apv2Krat.Text = this._device.APV2Krat.ToString();
                this._apvOff.SelectedItem = this._device.APVOff;
            }
        }

        private void ResetApv()
        {
            this._apvModes.SelectedIndex = 0;
            this._apvBlocking.SelectedIndex = 0;
            this._apvTBlock.Text = 0.ToString();
            this._apvTReady.Text = 0.ToString();
            this._apv1Krat.Text = 0.ToString();
            this._apv2Krat.Text = 0.ToString();
            this._apvOff.SelectedIndex = 0;
        }

        private bool WriteApv()
        {
            bool ret = true;
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                try
                {
                    this._device.APVModes = this._apvModes.SelectedItem.ToString();
                    this._device.APVBlocking = this._apvBlocking.SelectedItem.ToString();
                    this._device.APVTBlock = Convert.ToInt32(this._apvTBlock.Text);
                    this._device.APVTReady = Convert.ToInt32(this._apvTReady.Text);
                    this._device.APV1Krat = Convert.ToInt32(this._apv1Krat.Text);
                    this._device.APV2Krat = Convert.ToInt32(this._apv2Krat.Text);
                    this._device.APVOff = this._apvOff.SelectedItem.ToString();
                }
                catch (Exception ee)
                {
                    ret = false;
                    MessageBox.Show("Произошла ошибка во время записи блока АПВ.");
                }
            }
            return ret;
        }
        #endregion

        #region АВР

        private void _device_AVRSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_AVRSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_AVRLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_AVRLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadAvr));
            }
            catch (Exception ee)
            {

            }
        }

        private void PrepareAvr()
        {
            ComboBox[] combos = new ComboBox[]
            {
                this._avrBySignal, this._avrByOff, this._avrBySelfOff, this._avrByDiff, this._avrSIGNOn,
                this._avrBlocking, this._avrBlockClear, this._avrResolve, this._avrBack, this._avrClear
            };

            this._ULONGavrTransMaskedTextBoxes = new MaskedTextBox[] {this._avrTSr, this._avrTBack, this._avrTOff };

            this.ClearCombos(combos);
            this.FillAvrCombos(combos);

            this.PrepareMaskedBoxes(this._ULONGavrTransMaskedTextBoxes, typeof(ulong));
        }

        private void FillAvrCombos(ComboBox[] combos)
        {
            this._avrBySignal.Items.AddRange(Strings.APVModes.ToArray());
            this._avrByOff.Items.AddRange(Strings.APVModes.ToArray());
            this._avrBySelfOff.Items.AddRange(Strings.APVModes.ToArray());
            this._avrByDiff.Items.AddRange(Strings.APVModes.ToArray());
            this._avrSIGNOn.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._avrBlocking.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._avrBlockClear.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._avrResolve.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._avrBack.Items.AddRange(Strings.DiffBlocking.ToArray());
            this._avrClear.Items.AddRange(Strings.Forbidden.ToArray());
            foreach (ComboBox combo in combos)
            {
                combo.SelectedIndex = 0;
            }
        }

        private void ReadAvr()
        {
            this._avrBySignal.SelectedItem = this._device.AVRBySignal;
                this._avrByOff.SelectedItem = this._device.AVRByOff;
                this._avrBySelfOff.SelectedItem = this._device.AVRBySelfOff;
                this._avrByDiff.SelectedItem = this._device.AVRByDiff;
                this._avrSIGNOn.SelectedItem = this._device.AVRSIGNOn;
                this._avrBlocking.SelectedItem = this._device.AVRBlocking;
                this._avrBlockClear.SelectedItem = this._device.AVRBlockClear;
                this._avrResolve.SelectedItem = this._device.AVRResolve;
                this._avrTSr.Text = this._device.AVRTimeOn.ToString();
                this._avrBack.SelectedItem = this._device.AVRBack;
                this._avrTBack.Text = this._device.AVRTimeBack.ToString();
                this._avrTOff.Text = this._device.AVRTimeOtkl.ToString();
                this._avrClear.SelectedItem = this._device.AVRClear;
        }

        private void ResetAvr()
        {
            this._avrBySignal.SelectedIndex = 0;
            this._avrByOff.SelectedIndex = 0;
            this._avrBySelfOff.SelectedIndex = 0;
            this._avrByDiff.SelectedIndex = 0;
            this._avrSIGNOn.SelectedIndex = 0;
            this._avrBlocking.SelectedIndex = 0;
            this._avrBlockClear.SelectedIndex = 0;
            this._avrResolve.SelectedIndex = 0;
            this._avrTSr.Text = 0.ToString();
            this._avrBack.SelectedIndex = 0;
            this._avrTBack.Text = 0.ToString();
            this._avrTOff.Text = 0.ToString();
            this._avrClear.SelectedIndex = 0;
        }

        private bool WriteAvr()
        {
            bool ret = true;
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                try
                {
                    this._device.AVRBySignal = this._avrBySignal.SelectedItem.ToString();
                    this._device.AVRByOff = this._avrByOff.SelectedItem.ToString();
                    this._device.AVRBySelfOff = this._avrBySelfOff.SelectedItem.ToString();
                    this._device.AVRByDiff = this._avrByDiff.SelectedItem.ToString();
                    this._device.AVRSIGNOn = this._avrSIGNOn.SelectedItem.ToString();
                    this._device.AVRBlocking = this._avrBlocking.SelectedItem.ToString();
                    this._device.AVRBlockClear = this._avrBlockClear.SelectedItem.ToString();
                    this._device.AVRResolve = this._avrResolve.SelectedItem.ToString();
                    this._device.AVRTimeOn = Convert.ToInt32(this._avrTSr.Text);
                    this._device.AVRBack = this._avrBack.SelectedItem.ToString();
                    this._device.AVRTimeBack = Convert.ToInt32(this._avrTBack.Text);
                    this._device.AVRTimeOtkl = Convert.ToInt32(this._avrTOff.Text);
                    this._device.AVRClear = this._avrClear.SelectedItem.ToString();
                }
                catch (Exception ee)
                {
                    ret = false;
                    MessageBox.Show("Произошла ошибка во время записи блока АВР.");
                }
            }
            return ret;
        }
        #endregion

        #region ЛЗШ

        private void _device_LPBSaveFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveFail));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_LPBSaveOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnSaveOk));
            }
            catch (Exception ee)
            {

            }
        }

        private void _device_LPBLoadFail(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.OnLoadFail));
            }
            catch (Exception ee) { }
        }

        private void _device_LPBLoadOK(object sender)
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ReadLZH));
            }
            catch (Exception ee)
            {

            }
        }

        private void PrepareLZH()
        {
            this._DOUBLElzhTransMaskedTextBoxes = new MaskedTextBox[] {this._lzhVal };

            this._lzhModes.Items.Clear();
            this.FillLZHCombos();
            
            this.PrepareMaskedBoxes(this._DOUBLElzhTransMaskedTextBoxes, typeof(double));
 
        }

        private void FillLZHCombos()
        {
            this._lzhModes.Items.AddRange(Strings.LZHModes.ToArray());
            this._lzhModes.SelectedIndex = 0;
        }

        private void ReadLZH()
        {
            this._lzhModes.SelectedItem = this._device.LZHMode;
            this._lzhVal.Text = this._device.LZHUstavka.ToString();
        }

        private void ResetLZH()
        {
            this._lzhModes.SelectedIndex = 0;
            this._lzhVal.Text = 0.ToString();
        }

        private bool WriteLzh()
        {
            bool ret = true;
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.10)
            {
                try
                {
                    this._device.LZHMode = this._lzhModes.SelectedItem.ToString();

                    //проверка значения
                    var lzh = Convert.ToDouble(this._lzhVal.Text);
                    if ((lzh < 0) | (lzh > 40))
                        throw new ArgumentException();
                    this._device.LZHUstavka = lzh;
                }
                catch (Exception ee)
                {
                    ret = false;
                    MessageBox.Show("Произошла ошибка во время записи блока ЛЗШ.");
                }
            }
            return ret;
        }
        #endregion
        
        private void _reserveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.firstMain)
            {
                this.firstReserve = true;
            }
            else
            {
                this.firstMain = false;
            }
            if (this.firstReserve)
            {
                this.WriteCurrentProtAll();
            }
            this.copySetpointBtn.Text = "Копировать резервные в основные";
            this.ReadCurrentProtAll();
        }

        private void _mainRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.firstReserve)
            {
                this.firstMain = true;
            }
            else
            {
                this.firstReserve = false;
            }
            if (this.firstMain)
            {
                this.WriteCurrentProtAll();
            }
            this.copySetpointBtn.Text = "Копировать основные в резервные";
            this.ReadCurrentProtAll();
        }

        private void CopyBtnClick(object sender, EventArgs e)
        {
            string mes = this.firstMain
                ? "Копировать основные уставки в резервные?"
                : "Копировать резервные уставки в основные?";
            DialogResult res = MessageBox.Show(mes, "Уставки", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(res != DialogResult.Yes) return;
            this.WriteCurrentProtAll();
            if (this.firstMain)
            {
                ushort[] setpoints = this._device.CurrentProtAllMainProperty;
                setpoints.CopyTo(this._device.CurrentProtAllReservProperty, 0);
            }
            else
            {
                ushort[] setpoints = this._device.CurrentProtAllReservProperty;
                setpoints.CopyTo(this._device.CurrentProtAllMainProperty, 0);
            }
        }

        #region Валидация гридов
        private void ValidateC(DataGridView data, int ColumnIndex, int minVal, int maxVal, int ColErrorInd, int RowErrorInd, Type _type)
        {
            this._validatingGb = true;
            try
            {
                if (ColumnIndex != 0)
                {
                    if (typeof(int) == _type)
                    {
                        if (ColErrorInd == ColumnIndex && Convert.ToInt32(data[ColumnIndex, RowErrorInd].Value) > maxVal)
                        {
                            this._errorCoord = new int[3];
                            this._errorPage = new TabPage();
                            this._errorMainPage = new TabPage();

                            int left = 0;
                            int top = 0;
                            Control obj = data;
                            do
                            {
                                left += obj.Left;
                                top += obj.Top;
                                obj = obj.Parent;

                            } while (obj.Parent.GetType() != GetType());

                            this._toolTip.Show("Введите целое число в диапазоне [" + minVal + "-" + maxVal + "]", this, left, top, 2000);
                            this._validatingGb = false;
                            this._errorCoord[0] = RowErrorInd;
                            this._errorCoord[1] = ColErrorInd;
                            this._errorCoord[2] = maxVal;
                            this._errorPage = this._difensesTC.SelectedTab;
                            this._errorMainPage = this._configurationTabControl.SelectedTab;
                            this._errorGrid = data;
                            this._errorMes = "целое";
                        }
                    }
                    else if (typeof(double) == _type)
                    {
                        var value = Convert.ToDouble(data[ColumnIndex, RowErrorInd].Value, CultureInfo.InvariantCulture);
                        if (ColErrorInd == ColumnIndex && ((value > maxVal)|(value < minVal)))
                        {
                            this._errorCoord = new int[3];

                            int left = 0;
                            int top = 0;
                            Control obj = data;
                            do
                            {
                                left += obj.Left;
                                top += obj.Top;
                                obj = obj.Parent;

                            } while (obj.Parent.GetType() != GetType());
                            string message = string.Format("Введите число в диапазоне [{0}{1}0-{2}{3}0]", minVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, maxVal,
                                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
                            this._toolTip.Show(message, this, left, top, 2000);
                            this._validatingGb = false;
                            this._errorCoord[0] = RowErrorInd;
                            this._errorCoord[1] = ColErrorInd;
                            this._errorCoord[2] = maxVal;
                            this._errorPage = this._difensesTC.SelectedTab;
                            this._errorMainPage = this._configurationTabControl.SelectedTab;
                            this._errorGrid = data;
                            this._errorMes = string.Empty;
                        }
                    }
                }
            }
            catch
            {
                string _temp = string.Empty;
                if (typeof(int) == _type)
                {
                    _temp = "целое";
                }
                this._errorCoord = new int[3];
                this._errorPage = new TabPage();
                this._errorMainPage = new TabPage();

                int left = 0;
                int top = 0;
                Control obj = data;
                do
                {
                    left += obj.Left;
                    top += obj.Top;
                    obj = obj.Parent;

                } while (obj.Parent.GetType() != GetType());

                this._toolTip.Show("Введите " + _temp + " число в диапазоне [" + minVal + "-" + maxVal + "]", this, left, top, 2000);
                this._validatingGb = false;
                this._errorCoord[0] = RowErrorInd;
                this._errorCoord[1] = ColErrorInd;
                this._errorCoord[2] = maxVal;
                this._errorPage = this._difensesTC.SelectedTab;
                this._errorMainPage = this._configurationTabControl.SelectedTab;
                this._errorGrid = data;
                this._errorMes = string.Empty;
            }
        }

        private void SelectedC(DataGridView _data)
        {
            if (!this._validatingGb)
            {
                try
                {
                    for (int i = 0; i < _data.SelectedCells.Count; i++)
                    {
                        if (_data.SelectedCells[i].RowIndex != this._errorCoord[0] || _data.SelectedCells[i].ColumnIndex != this._errorCoord[1])
                        {
                            _data.SelectedCells[i].Selected = false;
                        }
                        if (_data.CurrentCell == _data.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]])
                        {
                            _data.CurrentCell.Selected = true;
                        }
                    }
                }
                catch { }
                if (this._errorGrid.CurrentCell != this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]])
                {
                    this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]].Selected = true;
                    this._errorGrid.CurrentCell = this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]];
                }
            }
        }

        private void ShowError()
        {
            try
            {
                this._difensesTC.SelectedTab = this._errorPage;
                this._configurationTabControl.SelectedTab = this._errorMainPage;
                this._toolTip.Show("Введите " + this._errorMes + " число в диапазоне [0-" + this._errorCoord[2] + "]", this, this._errorGrid.Left + this._errorGrid.Parent.Left + this._errorGrid.Parent.Parent.Left + this._errorGrid.Parent.Parent.Parent.Left + this._errorGrid.Parent.Parent.Parent.Parent.Left + this._errorGrid.Parent.Parent.Parent.Parent.Parent.Left, this._errorGrid.Top + this._errorGrid.Parent.Top + this._errorGrid.Parent.Parent.Top + this._errorGrid.Parent.Parent.Parent.Top + this._errorGrid.Parent.Parent.Parent.Parent.Top + this._errorGrid.Parent.Parent.Parent.Parent.Parent.Top, 2000);
                this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]].Selected = true;
                this._errorGrid.CurrentCell = this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]];
            }
            catch { }
        }
        #endregion

        private void _difensesIDataGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridView _grid = sender as DataGridView;
            //switch (e.ColumnIndex)
            //{
            //    case 2:
            //    {
            //        ValidateC(_grid, 2, 0, 40, e.ColumnIndex, e.RowIndex, typeof(double));
            //        break;
            //    }
            //    case 4:
            //    {
            //        ValidateC(_grid, 4, 0, 256, e.ColumnIndex, e.RowIndex, typeof(double));
            //        break;
            //    }
            //    case 10:
            //    {
            //        ValidateC(_grid, 10, 0, 3276700, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //    case 11:
            //    {
            //        ValidateC(_grid, 11, 0, 4000, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //}

        }

        private void _cornersGridView_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            this.ValidateC(this._cornersGridView, e.ColumnIndex, 0, 360, e.ColumnIndex, e.RowIndex, typeof(int));
        }

        private void _dif0DataGreed_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //var grid = sender as DataGridView;
            //switch (e.ColumnIndex)
            //{
            //    case 3:
            //    case 6:
            //    case 8:
            //    {
            //        this.ValidateC(grid, e.ColumnIndex, 0, 40, e.ColumnIndex, e.RowIndex, typeof(double));
            //        break;
            //    }
            //    case 5:
            //    {
            //        this.ValidateC(grid, 5, 0, 3276700, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //    case 7:
            //    case 9:
            //    {
            //        this.ValidateC(grid, e.ColumnIndex, 0, 89, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //}

        }

        private void _difensesUDataGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //var _grid = sender as DataGridView;
            //switch (e.ColumnIndex)
            //{
            //    case 3:
            //    case 6:
            //    {
            //        this.ValidateC(_grid, e.ColumnIndex, 0, 256, e.ColumnIndex, e.RowIndex, typeof(double));
            //        break;
            //    }
            //    case 4:
            //    case 5:
            //    {
            //        this.ValidateC(_grid, e.ColumnIndex, 0, 3276700, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //}

        }

        private void _externalDifensesDataGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //var _grid = sender as DataGridView;
            //switch (e.ColumnIndex)
            //{
            //    case 3:
            //    case 4:
            //    {
            //        this.ValidateC(_grid, e.ColumnIndex, 0, 3276700, e.ColumnIndex, e.RowIndex, typeof(int));
            //        break;
            //    }
            //}

        }


        private void _outputReleGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
                var _grid = sender as DataGridView;
                switch (e.ColumnIndex)
                {
                    case 3:
                        {
                            this.ValidateC(_grid, e.ColumnIndex, 0, 3276700, e.ColumnIndex, e.RowIndex, typeof(int));
                            break;
                        }
                }

        }

        private void ConfigurationForm_Activated(object sender, EventArgs e)
        {
            Strings.Version = this._vers;
        }

        private void DataGrid_SelectionChanged(object sender, EventArgs e)
        {
            var _grid = sender as DataGridView;
            this.SelectedC(_grid);
        }

        private void DataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (!this._validatingGb)
            {
                try
                {
                    for (int i = 0; i < this._errorGrid.SelectedCells.Count; i++)
                    {
                        if (this._errorGrid.SelectedCells[i].RowIndex != this._errorCoord[0] || this._errorGrid.SelectedCells[i].ColumnIndex != this._errorCoord[1])
                        {
                            this._errorGrid.SelectedCells[i].Selected = false;
                        }
                        this._errorGrid.CurrentCell.Selected = true;
                    }
                }
                catch { }
                if (this._errorGrid.CurrentCell != this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]])
                {
                    this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]].Selected = true;
                    BeginInvoke(new MethodInvoker(delegate {
                                                               this._errorGrid.CurrentCell = this._errorGrid.Rows[this._errorCoord[0]].Cells[this._errorCoord[1]];
                    })); 
                }
            }
        }

        private void tabControl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._validatingGb)
            {
                this.ShowError();
            }
        }

        private void _configurationTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this._validatingGb)
            {
                this.ShowError();
            }
        }

        private void _oscopePreRecordingLenghtTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && e.KeyChar != '\b')
                e.Handled = true;
        }

        private void _oscopePreRecordingLenghtTextBox_TextChanged(object sender, EventArgs e)
        {
            ushort value;
            var valid = false;
            if (ushort.TryParse(this._oscopePreRecordingLenghtTextBox.Text, out value))
            {
                if (value <= 100)
                    valid = true;
            }
            this._oscopePreRecordingLenghtTextBox.BackColor = valid ? SystemColors.Window : Color.Red;
        }

        private void _oscopeCountComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int oscLenght = this._oscMemorySize * 2/(2 + this._oscopeCountComboBox.SelectedIndex);
            this._oscopeLenghtLabel.Text = string.Format("{0} мс", oscLenght);
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        #region Вывод HTML

        private string Export801(string mainShema, string reserveShema, object config, string deviceType, string version)
        {
            bool toMain = false;
            bool toReserve = false;
            try
            {
                ExportSelectForm801 exportSelect = new ExportSelectForm801();
                string shema = string.Empty;
                HtmlExport._saveDialog.FileName = string.Format("{0} Уставки версия {1}", deviceType, version);
                if (exportSelect.ShowDialog() == DialogResult.OK)
                {
                    if (_reserveRadioButton.Checked && (exportSelect.Group == ExportGroup.MAIN))
                    {
                        this._mainRadioButton.Checked = true;
                        toReserve = true;
                        shema = mainShema;
                    }

                    if (_reserveRadioButton.Checked && (exportSelect.Group == ExportGroup.RESERVE))
                    {
                        this._reserveRadioButton.Checked = true;
                        toReserve = true;
                        shema = reserveShema;
                    }

                    if (_mainRadioButton.Checked && (exportSelect.Group == ExportGroup.MAIN))
                    {
                        this._mainRadioButton.Checked = true;
                        toMain = true;
                        shema = mainShema;
                    }
                    if (_mainRadioButton.Checked && (exportSelect.Group == ExportGroup.RESERVE))
                    {
                        this._reserveRadioButton.Checked = true;
                        toMain = true;
                        shema = reserveShema;
                    }
                }
                else
                {
                    return string.Empty;
                }
                bool res = false;
                if (HtmlExport._saveDialog.ShowDialog() == DialogResult.OK)
                {
                    string extension = Path.GetExtension(HtmlExport._saveDialog.FileName);
                    if (string.Compare(extension, ".xml", true) == 0)
                    {
                        res = HtmlExport.ExportXml(HtmlExport._saveDialog.FileName, config);
                    }
                    if (string.Compare(extension, ".html", true) == 0)
                    {
                        res = HtmlExport.ExportHtml(HtmlExport._saveDialog.FileName, shema, config);
                    }
                }
                if (toMain)
                {
                    this._mainRadioButton.Checked = true;
                }
                if (toReserve)
                {
                    this._reserveRadioButton.Checked = true;
                }
                if (res)
                {
                    return string.Format("Файл {0} успешно сохранён", HtmlExport._saveDialog.FileName);
                }
                return string.Empty;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка сохранения", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "Ошибка сохранения";
            }

        }
        /// <summary>
        /// Не доделано! Доработать
        /// </summary>
        private void SaveToHtml()
        {
            this.ValidateAll();
            this._processLabel.Text = string.Empty;
            this._exchangeProgressBar.Value = 0;
            if (!this._validatingGb)
            {
                this._validatingOk = false;
                this.ShowError();
            }
            if (this._validatingOk)
            {
                try
                {
                    _processLabel.Text = Export801(Resources.MR801, Resources.MR801RES, _device, "МР801", _device.DeviceVersion);
                    this._exchangeProgressBar.Value = this._exchangeProgressBar.Maximum;
                }
                catch (Exception)
                {
                    MessageBox.Show("Ошибка сохранения");
                }

            }
        }
        
        #endregion


        private void UDZTConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
