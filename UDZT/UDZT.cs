﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Collections;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.Devices;
using BEMN.MBServer.Queries;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;
using BEMN.UDZT.BSBGL;
using BEMN.UDZT.NewOsc;
using AssemblyResources;
using BEMN.Framework;

namespace BEMN.UDZT
{
    public class UDZT : Device, IDeviceView, IDeviceVersion
    {
        #region Адреса
        private ushort _startAdress;
        private ushort _startSWITCHALL; // < 1.11
        private ushort _sizeSWITCHALL;  // < 1.11
        private ushort _endSWITCHALL;   // < 1.11

        private ushort _startUROVALL;   // < 1.11
        private ushort _sizeUROVALL;    // < 1.11
        private ushort _endUROVALL;     // < 1.11

        private ushort _startSWITCH;    // >= 1.11
        private ushort _sizeSWITCH;     // >= 1.11
        private ushort _endSWITCH;      // >= 1.11

        private ushort _startAPV;       // >= 1.11
        private ushort _sizeAPV;        // >= 1.11
        private ushort _endAPV;         // >= 1.11

        private ushort _startAVR;       // >= 1.11
        private ushort _sizeAVR;        // >= 1.11
        private ushort _endAVR;         // >= 1.11

        private ushort _startLPB;       // >= 1.11
        private ushort _sizeLPB;        // >= 1.11
        private ushort _endLPB;         // >= 1.11

        private ushort _startAUTOBLOWER;
        private ushort _sizeAUTOBLOWER;
        private ushort _endAUTOBLOWER;

        private ushort _startTERMAL;
        private ushort _sizeTERMAL;
        private ushort _endTERMAL;

        private ushort _startINPUTSIGNAL;
        private ushort _sizeINPUTSIGNAL;
        private ushort _endINPUTSIGNAL;

        private ushort _startOSCOPE;
        private ushort _sizeOSCOPE;
        private ushort _endOSCOPE;

        private ushort _startEthernetAddress;
        private ushort _startEthernet;
        private ushort _sizeEthernet;
        private ushort _endEthernet;

        private ushort _startPOWERTRANS;
        private ushort _sizePOWERTRANS;
        private ushort _endPOWERTRANS;

        private ushort _startMEASURETRANS;
        private ushort _sizeMEASURETRANS;
        private ushort _endMEASURETRANS;

        private ushort _startINPSYGNAL;
        private ushort _sizeINPSYGNAL;
        private ushort _endINPSYGNAL;

        private ushort _startELSSYGNAL1;
        private ushort _sizeELSSYGNAL1;
        private ushort _endELSSYGNAL1;

        private ushort _startELSSYGNAL2;
        private ushort _sizeELSSYGNAL2;
        private ushort _endELSSYGNAL2;

        private ushort _startELSSYGNAL3;
        private ushort _sizeELSSYGNAL3;
        private ushort _endELSSYGNAL3;

        private ushort _startELSSYGNAL4;
        private ushort _sizeELSSYGNAL4;
        private ushort _endELSSYGNAL4;

        private ushort _startCURRENTPROTALL;
        private ushort _sizeCURRENTPROTALL;
        private ushort _endCURRENTPROTALL;

        private ushort _startPARAMAUTOMAT;
        private ushort _sizePARAMAUTOMAT;
        private ushort _endPARAMAUTOMAT;

        private ushort _startSysJournalAdress;
        private ushort _startSysJournal;
        private ushort _sizeSysJournal;
        private ushort _endSysJournal;

        private ushort _startAlarmJournalAdress;
        private ushort _startAlarmJournal;
        private ushort _sizeAlarmJournal;
        private ushort _endAlarmJournal;

        private ushort _startOscJournalAdress;
        private ushort _startOscJournal;
        private ushort _sizeOscJournal;
        private ushort _endOscJournal;

        private ushort _startOscAdress;
        private ushort _startOscilloscope;
        private ushort _sizeOscilloscope;//Слова
        private ushort _endOscilloscope;

        private ushort _startAnalogBDAdress;
        private ushort _startAnalogBD;
        private ushort _sizeAnalogBD;
        private ushort _endAnalogBD;


        private ushort _startBitBDAdress;
        private ushort _startBitBD;
        private ushort _sizeBitBD;
        private ushort _endBitBD;
        #endregion

        #region Поля
        private double _version;

        private MemoryEntity<DateTimeStruct> _dateTime;
        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }
        private MemoryEntity<OscopeChannelStruct> _oscChannels;
        public MemoryEntity<OscopeChannelStruct> OscChannels
        {
            get { return this._oscChannels; }
        }
        private MemoryEntity<OscOptionsStruct> _oscOptionsStruct;
        public MemoryEntity<OscOptionsStruct> OscOptionsStruct
        {
            get { return this._oscOptionsStruct; }
        }
        private MemoryEntity<TransStructAll> _transStruct;
        public MemoryEntity<TransStructAll> TransStruct
        {
            get { return this._transStruct; }
        }

        private MemoryEntity<OscJournalStruct> _oscJournal;
        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        private MemoryEntity<OscPage> _oscPage;
        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }
        private MemoryEntity<OneWordStruct> _setOscStartPage;
        public MemoryEntity<OneWordStruct> SetOscStartPage
        {
            get { return this._setOscStartPage; }
        }
        
        private slot _programStorage = new slot(0xC000, 0xE000);              //Архив схемы
        private slot _programSignals = new slot(0x4100, 0x4300);              //Рабочая база данны
        private slot _oscLength = new slot(0x05A0, 0x05A4);

        //Конфигурация всех выключателей
        private slot _switchAll = new slot(0, 1);

        //Конфигурация всех УРОВ
        private slot _urovAll = new slot(0, 1);

        private slot _switch = new slot(0, 1);
        private slot _apv = new slot(0, 1);
        private slot _avr = new slot(0, 1);
        private slot _lpb = new slot(0, 1);

        //Конфигурация всей автоматики обдува
        private slot _autoblower = new slot(0, 1);

        //Конфигурация тепловой модели
        private slot _termal = new slot(0, 1);

        //Конфигурация входных сигналов
        private slot _inputsygnal = new slot(0, 1);

        //Конфигурация осцилографа
        private slot _oscope = new slot(0, 1);
        
        //Структура силового транформатора
        private slot _powerTrans = new slot(0, 1);

        //Структура измерительного трансформатора
        private slot _measureTrans = new slot(0, 1);

        //Структура входных логических сигналов
        private slot _inpSygnal = new slot(0, 1);

        //Структура выходных логических сигналов
        private slot _elsSygnal1 = new slot(0, 1);
        private slot _elsSygnal2 = new slot(0, 1);
        private slot _elsSygnal3 = new slot(0, 1);
        private slot _elsSygnal4 = new slot(0, 1);
        private ushort[] _elsSygnal = new ushort[0];

        //Все защиты
        private List<slot> CurrentProtAll = new List<slot>();
        private slot _currentProtAll = new slot(0, 1);//разбить на 5
        private ushort[] _curProtAll = new ushort[0];
        //Параметры автоматики
        private slot _paramAutomat = new slot(0, 1);
        
        //Конфигурация Ethernet
        private slot _configEthernet = new slot(0, 1);

        //Конфигурация системы
        private slot _confConstraint = new slot(0, 1);

        //Журнал осциллограммы
        private slot _oscilloscopeJournalReadSlot = new slot(0, 1);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopeJournalWriteSlot = new slot(0, 1);

        //Журнал осциллограммы
        private List<slot> CurrentOscilloscope = new List<slot>();
        private slot _oscilloscopeRead = new slot(0, 1);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopePageWrite = new slot(0, 1);

        //Системный журнал
        private List<slot> CurrentSystemJournal = new List<slot>();
        private slot _systemJournalRead = new slot(0, 1);

        //Номер страницы системного журнала
        private slot _systemJournalWrite = new slot(0, 1);

        //Журнал аварий
        private List<slot> CurrentAlarmJournal = new List<slot>();
        private slot _alarmJournalRead = new slot(0, 1);

        //Номер страницы журнала аварий
        private slot _alarmJournalWrite = new slot(0, 1);

        private slot _analogBDSlot = new slot(0, 1);

        private slot _bitBDSlot = new slot(0, 1);
        #endregion

        #region Конструкторы и инициализация

        public UDZT()
        {
            HaveVersion = true;
        }

        public UDZT(Modbus mb)
        {
            this.MB = mb;
            this.Init();
            this.InitAdr();
            this.InitSlots();
        }

        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [XmlIgnore]
        public override Modbus MB
        {
            get { return mb; }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        private void Init()
        {
            HaveVersion = true;
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._oscOptionsStruct = new MemoryEntity<OscOptionsStruct>("Конфигурация осциллографа", this, 0x05A0);
            this._oscChannels = new MemoryEntity<OscopeChannelStruct>("Конфигурация каналов осциллографа", this, 0x103B);
            this._transStruct = new MemoryEntity<TransStructAll>("AllTrans", this, 0x1048);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x800);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллограммы", this, 0x900);
            this._setOscStartPage = new MemoryEntity<OneWordStruct>("Установка страницы осциллограммы", this, 0x900);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);
            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0F);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D10);
            this._stateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x0D12);
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние задачи логики", this, 0x0D0E);
        }

        private void InitAdr()
        {
            this._startAdress = 0x1000;
            this._startSWITCHALL = this._startAdress;
            this._sizeSWITCHALL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCHALL)) / 2);
            this._endSWITCHALL = (ushort)(this._startSWITCHALL + this._sizeSWITCHALL);

            this._startUROVALL = this._endSWITCHALL;
            this._sizeUROVALL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(UROVALL)) / 2);
            this._endUROVALL = (ushort)(this._startUROVALL + this._sizeUROVALL);

            this._startAUTOBLOWER = this._endUROVALL;
            this._sizeAUTOBLOWER = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AUTOBLOWER)) / 2);
            this._endAUTOBLOWER = (ushort)(this._startAUTOBLOWER + this._sizeAUTOBLOWER);

            this._startTERMAL = this._endAUTOBLOWER;
            this._sizeTERMAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(TERMAL)) / 2);
            this._endTERMAL = (ushort)(this._startTERMAL + this._sizeTERMAL);

            this._startINPUTSIGNAL = this._endTERMAL;
            this._sizeINPUTSIGNAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPUTSIGNAL)) / 2);
            this._endINPUTSIGNAL = (ushort)(this._startINPUTSIGNAL + this._sizeINPUTSIGNAL);

            this._startOSCOPE = this._endINPUTSIGNAL;
            this._sizeOSCOPE = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OSCOPE)) / 2);
            this._endOSCOPE = (ushort)(this._startOSCOPE + this._sizeOSCOPE);
            
            this._startPOWERTRANS = this._endOSCOPE;
            this._sizePOWERTRANS = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(POWERTRANS)) / 2);
            this._endPOWERTRANS = (ushort)(this._startPOWERTRANS + this._sizePOWERTRANS);

            this._startMEASURETRANS = this._endPOWERTRANS;
            this._sizeMEASURETRANS = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MEASURETRANS)) / 2);
            this._endMEASURETRANS = (ushort)(this._startMEASURETRANS + this._sizeMEASURETRANS);

            this._startINPSYGNAL = this._endMEASURETRANS;
            this._sizeINPSYGNAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPSYGNAL)) / 2);
            this._endINPSYGNAL = (ushort)(this._startINPSYGNAL + this._sizeINPSYGNAL);

            this._startELSSYGNAL1 = this._endINPSYGNAL;
            this._sizeELSSYGNAL1 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL1 = (ushort)(this._startELSSYGNAL1 + this._sizeELSSYGNAL1);

            this._startELSSYGNAL2 = this._endELSSYGNAL1;
            this._sizeELSSYGNAL2 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL2 = (ushort)(this._startELSSYGNAL2 + this._sizeELSSYGNAL2);

            this._startELSSYGNAL3 = this._endELSSYGNAL2;
            this._sizeELSSYGNAL3 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL3 = (ushort)(this._startELSSYGNAL3 + this._sizeELSSYGNAL3);

            this._startELSSYGNAL4 = this._endELSSYGNAL3;
            this._sizeELSSYGNAL4 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL4 = (ushort)(this._startELSSYGNAL4 + this._sizeELSSYGNAL4);

            this._startCURRENTPROTALL = this._endELSSYGNAL4;
            this._sizeCURRENTPROTALL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTALL)) / 2);
            this._endCURRENTPROTALL = (ushort)(this._startCURRENTPROTALL + this._sizeCURRENTPROTALL);

            this._startPARAMAUTOMAT = this._endCURRENTPROTALL;
            this._sizePARAMAUTOMAT = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(PARAMAUTOMAT)) / 2);
            this._endPARAMAUTOMAT = (ushort)(this._startPARAMAUTOMAT + this._sizePARAMAUTOMAT);

            this._startEthernetAddress = 0x15C2;
            this._startEthernet = _startEthernetAddress;
            this._sizeEthernet = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGNETETHERNET)) / 2);
            this._endEthernet = (ushort)(this._startEthernet + this._sizeEthernet);
            this._configEthernet = new slot(this._startEthernet, this._endEthernet);

            this._startSysJournalAdress = 0x0600;
            this._startSysJournal = this._startSysJournalAdress;
            this._sizeSysJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SystemJournal)) / 2);
            this._endSysJournal = (ushort)(this._startSysJournal + this._sizeSysJournal);

            this._startAlarmJournalAdress = 0x0700;
            this._startAlarmJournal = this._startAlarmJournalAdress;
            this._sizeAlarmJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AlarmJournal)) / 2);
            this._endAlarmJournal = (ushort)(this._startAlarmJournal + this._sizeAlarmJournal);

            this._startOscJournalAdress = 0x0800;
            this._startOscJournal = this._startOscJournalAdress;
            this._sizeOscJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OscJournal)) / 2 - 2);
            this._endOscJournal = (ushort)(this._startOscJournal + this._sizeOscJournal);

            this._startOscAdress = 0x0900;
            this._startOscilloscope = this._startOscAdress;
            this._sizeOscilloscope = (ushort)0x7c;//Слова
            this._endOscilloscope = (ushort)(this._startOscilloscope + this._sizeOscilloscope);

            this._startAnalogBDAdress = 0x0E00;
            this._startAnalogBD = this._startAnalogBDAdress;
            this._sizeAnalogBD = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AnalogBDStruct)) / 2);
            this._endAnalogBD = (ushort)(this._startAnalogBD + this._sizeAnalogBD);

            this._startBitBDAdress = 0x0D00;
            this._startBitBD = this._startBitBDAdress;
            this._sizeBitBD = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(BDBIT_CTRL)) / 2);
            this._endBitBD = (ushort)(this._startBitBD + this._sizeBitBD);
        }

        public void RefreshAdrSpace1_11()
        {
            this._startAdress = 0x1000;
            this._startSWITCH = this._startAdress;
            this._sizeSWITCH = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH_v_1_11)) / 2);
            this._endSWITCH = (ushort)(this._startSWITCH + this._sizeSWITCH);

            this._startAPV = this._endSWITCH;
            this._sizeAPV = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(APV_v_1_11)) / 2);
            this._endAPV = (ushort)(this._startAPV + this._sizeAPV);

            this._startAVR = this._endAPV;
            this._sizeAVR = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AVR_v_1_11)) / 2);
            this._endAVR = (ushort)(this._startAVR + this._sizeAVR);

            this._startLPB = this._endAVR;
            this._sizeLPB = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(LPB_v_1_11)) / 2);
            this._endLPB = (ushort)(this._startLPB + this._sizeLPB);

            this._startAUTOBLOWER = this._endLPB;
            this._sizeAUTOBLOWER = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AUTOBLOWER)) / 2);
            this._endAUTOBLOWER = (ushort)(this._startAUTOBLOWER + this._sizeAUTOBLOWER);

            this._startTERMAL = this._endAUTOBLOWER;
            this._sizeTERMAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(TERMAL)) / 2);
            this._endTERMAL = (ushort)(this._startTERMAL + this._sizeTERMAL);

            this._startINPUTSIGNAL = this._endTERMAL;
            this._sizeINPUTSIGNAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPUTSIGNAL)) / 2);
            this._endINPUTSIGNAL = (ushort)(this._startINPUTSIGNAL + this._sizeINPUTSIGNAL);

            this._startOSCOPE = this._endINPUTSIGNAL;
            this._sizeOSCOPE = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OSCOPE)) / 2);
            this._endOSCOPE = (ushort)(this._startOSCOPE + this._sizeOSCOPE);
            
            this._startPOWERTRANS = this._endOSCOPE;
            this._sizePOWERTRANS = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(POWERTRANS)) / 2);
            this._endPOWERTRANS = (ushort)(this._startPOWERTRANS + this._sizePOWERTRANS);

            this._startMEASURETRANS = this._endPOWERTRANS;
            this._sizeMEASURETRANS = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MEASURETRANS)) / 2);
            this._endMEASURETRANS = (ushort)(this._startMEASURETRANS + this._sizeMEASURETRANS);

            this._startINPSYGNAL = this._endMEASURETRANS;
            this._sizeINPSYGNAL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPSYGNAL)) / 2);
            this._endINPSYGNAL = (ushort)(this._startINPSYGNAL + this._sizeINPSYGNAL);

            this._startELSSYGNAL1 = this._endINPSYGNAL;
            this._sizeELSSYGNAL1 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL1 = (ushort)(this._startELSSYGNAL1 + this._sizeELSSYGNAL1);

            this._startELSSYGNAL2 = this._endELSSYGNAL1;
            this._sizeELSSYGNAL2 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL2 = (ushort)(this._startELSSYGNAL2 + this._sizeELSSYGNAL2);

            this._startELSSYGNAL3 = this._endELSSYGNAL2;
            this._sizeELSSYGNAL3 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL3 = (ushort)(this._startELSSYGNAL3 + this._sizeELSSYGNAL3);

            this._startELSSYGNAL4 = this._endELSSYGNAL3;
            this._sizeELSSYGNAL4 = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2 / 4);
            this._endELSSYGNAL4 = (ushort)(this._startELSSYGNAL4 + this._sizeELSSYGNAL4);

            this._startCURRENTPROTALL = this._endELSSYGNAL4;
            this._sizeCURRENTPROTALL = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTALL1_11)) / 2);
            this._endCURRENTPROTALL = (ushort)(this._startCURRENTPROTALL + this._sizeCURRENTPROTALL);

            this._startPARAMAUTOMAT = this._endCURRENTPROTALL;
            this._sizePARAMAUTOMAT = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(PARAMAUTOMAT)) / 2);
            this._endPARAMAUTOMAT = (ushort)(this._startPARAMAUTOMAT + this._sizePARAMAUTOMAT);
            
            this._switch = new slot(this._startSWITCH, this._endSWITCH);
            this._apv = new slot(this._startAPV, this._endAPV);
            this._avr = new slot(this._startAVR, this._endAVR);
            this._lpb = new slot(this._startLPB, this._endLPB);

            this._autoblower = new slot(this._startAUTOBLOWER, this._endAUTOBLOWER);
            this._termal = new slot(this._startTERMAL, this._endTERMAL);
            this._inputsygnal = new slot(this._startINPUTSIGNAL, this._endINPUTSIGNAL);
            this._oscope = new slot(this._startOSCOPE, (ushort)(this._startOSCOPE + 11));
            this._powerTrans = new slot(this._startPOWERTRANS, this._endPOWERTRANS);
            this._measureTrans = new slot(this._startMEASURETRANS, this._endMEASURETRANS);
            this._inpSygnal = new slot(this._startINPSYGNAL, this._endINPSYGNAL);
            this._elsSygnal1 = new slot(this._startELSSYGNAL1, this._endELSSYGNAL1);
            this._elsSygnal2 = new slot(this._startELSSYGNAL2, this._endELSSYGNAL2);
            this._elsSygnal3 = new slot(this._startELSSYGNAL3, this._endELSSYGNAL3);
            this._elsSygnal4 = new slot(this._startELSSYGNAL4, this._endELSSYGNAL4);
            this._elsSygnal = new ushort[this._endELSSYGNAL4 - this._startELSSYGNAL1];
            this.CurrentProtAll = new List<slot>();
            this._currentProtAll = new slot(this._startCURRENTPROTALL, this._endCURRENTPROTALL);//разбить на 5
            this._curProtAll = new ushort[this._currentProtAll.Size];
            this._paramAutomat = new slot(this._startPARAMAUTOMAT, this._endPARAMAUTOMAT);

            this._startEthernetAddress = 0x15C2;
            this._startEthernet = _startEthernetAddress;
            this._sizeEthernet = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGNETETHERNET)) / 2);
            this._endEthernet = (ushort)(this._startEthernet + this._sizeEthernet);
            this._configEthernet = new slot(this._startEthernet, this._endEthernet);

            this._startAnalogBDAdress = 0x0E00;
            this._startAnalogBD = this._startAnalogBDAdress;
            this._sizeAnalogBD = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AnalogBDStruct_v_1_11)) / 2);
            this._endAnalogBD = (ushort)(this._startAnalogBD + this._sizeAnalogBD);
            this._analogBDSlot = new slot(this._startAnalogBD, this._endAnalogBD);

            this.main = new ushort[this._curProtAll.Length / 2];
            this.reserve = new ushort[this._curProtAll.Length / 2];

            this._startAlarmJournalAdress = 0x0700;
            this._startAlarmJournal = this._startAlarmJournalAdress;
            this._sizeAlarmJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AlarmJournal_v_1_11)) / 2);
            this._endAlarmJournal = (ushort)(this._startAlarmJournal + this._sizeAlarmJournal);
        }

        public void RefreshAdrSpace1_16()
        {
            this._startAlarmJournalAdress = 0x0700;
            this._startAlarmJournal = this._startAlarmJournalAdress;
            this._sizeAlarmJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AlarmJournal_v_1_16)) / 2);
            this._endAlarmJournal = (ushort)(this._startAlarmJournal + this._sizeAlarmJournal);

            this.CurrentAlarmJournal = new List<slot>();
            this._alarmJournalRead = new slot(this._startAlarmJournal, this._endAlarmJournal);
        }

        void InitSlots()
        {
            this._switchAll = new slot(this._startSWITCHALL, this._endSWITCHALL);
            this._urovAll = new slot(this._startUROVALL, this._endUROVALL);
            this._autoblower = new slot(this._startAUTOBLOWER, this._endAUTOBLOWER);
            this._termal = new slot(this._startTERMAL, this._endTERMAL);
            this._inputsygnal = new slot(this._startINPUTSIGNAL, this._endINPUTSIGNAL);
            this._oscope = new slot(this._startOSCOPE, (ushort)(this._startOSCOPE + 11));
            this._powerTrans = new slot(this._startPOWERTRANS, this._endPOWERTRANS);
            this._measureTrans = new slot(this._startMEASURETRANS, this._endMEASURETRANS);
            this._inpSygnal = new slot(this._startINPSYGNAL, this._endINPSYGNAL);
            this._elsSygnal1 = new slot(this._startELSSYGNAL1, this._endELSSYGNAL1);
            this._elsSygnal2 = new slot(this._startELSSYGNAL2, this._endELSSYGNAL2);
            this._elsSygnal3 = new slot(this._startELSSYGNAL3, this._endELSSYGNAL3);
            this._elsSygnal4 = new slot(this._startELSSYGNAL4, this._endELSSYGNAL4);
            this._elsSygnal = new ushort[this._endELSSYGNAL4 - this._startELSSYGNAL1];
            this.CurrentProtAll = new List<slot>();
            this._currentProtAll = new slot(this._startCURRENTPROTALL, this._endCURRENTPROTALL);//разбить на 5
            this._curProtAll = new ushort[this._currentProtAll.Size];
            this._paramAutomat = new slot(this._startPARAMAUTOMAT, this._endPARAMAUTOMAT);
            this._configEthernet = new slot(this._startEthernet, this._endEthernet);

            this._confConstraint = new slot(0x3103, 0x3104);
            this._oscilloscopeJournalReadSlot = new slot(this._startOscJournal, this._endOscJournal);
            this._oscilloscopeJournalWriteSlot = new slot(this._startOscJournal, (ushort)(this._startOscJournal + 1));
            this.CurrentOscilloscope = new List<slot>();
            this._oscilloscopeRead = new slot(this._startOscilloscope, this._endOscilloscope);
            this._oscilloscopePageWrite = new slot(this._startOscilloscope, (ushort)(this._startOscilloscope + 1));
            this.CurrentSystemJournal = new List<slot>();
            this._systemJournalRead = new slot(this._startSysJournal, this._endSysJournal);
            this._systemJournalWrite = new slot(this._startSysJournal, (ushort)(this._startSysJournal + 1));
            this.CurrentAlarmJournal = new List<slot>();
            this._alarmJournalRead = new slot(this._startAlarmJournal, this._endAlarmJournal);
            this._alarmJournalWrite = new slot(this._startAlarmJournal, (ushort)(this._startAlarmJournal + 1));
            this._analogBDSlot = new slot(this._startAnalogBD, this._endAnalogBD);
            this._bitBDSlot = new slot(this._startBitBD, this._endBitBD);

            this._oscilloscopeJournalRecord = new ushort[this._sizeOscJournal];

            this.main = new ushort[this._curProtAll.Length / 2];
            this.reserve = new ushort[this._curProtAll.Length / 2];
        }
        #endregion

        #region IDeviceVersion Members
        public Type[] Forms
        {
            get
            {
                var ver = Common.VersionConverter(DeviceVersion);
                this._version = ver;
                Strings.Version = ver;
                if (ver <= 1.19 && DeviceType == "MR801")
                {
                    MessageBox.Show("Внимание! Вы подключились к МР801 устаревшей версии " + ver +
                                    ". Программное обеспечение МР801 нуждается в обновлении. " +
                                    "Обратитесь, пожалуйста, по телефону +375-44-710-31-40 для бесплатного оснащения " +
                                    "МР801 новым программным обеспечением, повышающим надежность работы.",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (ver <= 1.10)
                {
                    return new[]
                        {
                            typeof (BSBGLEF),
                            typeof (AlarmJournalForm),
                            typeof (ConfigurationForm),
                            typeof (MeasuringForm),
                            typeof (SystemJournalForm),
                            typeof (OscilloscopeForm)
                        };
                }
                else
                {
                    return new[]
                        {
                            typeof (BSBGLEF),
                            typeof (AlarmJournalForm),
                            typeof (ConfigurationForm),
                            typeof (MeasuringForm),
                            typeof (SystemJournalForm),
                            typeof (VOscilloscopeForm)
                        };
                }
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                    {
                        "1.00",
                        "1.01",
                        "1.10",
                        "1.11",
                        "1.15",
                        "1.16",
                        "1.17",
                        "1.18",
                        "1.19",
                        "1.20",
                        "1.21",
                        "1.22",
                        "1.23",
                        "2.00",
                        "2.01",
                        "2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "2.07",
                        "2.08"
                    };
            }
        }
        #endregion

        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;
        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStruct; }
        }
        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return this._isSplOn; }
        }


        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }

        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return this._stateSpl; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }
        #endregion

        #region События
        public event Handler ProgramSaveOk;
        public event Handler ProgramSaveFail;
        public event Handler ProgramStartSaveOk;
        public event Handler ProgramStartSaveFail;
        public event Handler ProgramStorageSaveOk;
        public event Handler ProgramStorageSaveFail;
        public event Handler ProgramStorageLoadOk;
        public event Handler ProgramStorageLoadFail;
        public event Handler ProgramSignalsLoadOk;
        public event Handler ProgramSignalsLoadFail;
        public event Handler ProgramPageSaveOk;
        public event Handler ProgramPageSaveFail;

        public event Handler OscLengthLoadOK;
        public event Handler OscLengthLoadFail;

        public event Handler SwitchAllLoadOK;
        public event Handler SwitchAllLoadFail;
        public event Handler SwitchAllSaveOK;
        public event Handler SwitchAllSaveFail;

        public event Handler UrovAllLoadOK;
        public event Handler UrovAllLoadFail;
        public event Handler UrovAllSaveOK;
        public event Handler UrovAllSaveFail;

        public event Handler SwitchLoadOK;
        public event Handler SwitchLoadFail;
        public event Handler SwitchSaveOK;
        public event Handler SwitchSaveFail;

        public event Handler APVLoadOK;
        public event Handler APVLoadFail;
        public event Handler APVSaveOK;
        public event Handler APVSaveFail;

        public event Handler AVRLoadOK;
        public event Handler AVRLoadFail;
        public event Handler AVRSaveOK;
        public event Handler AVRSaveFail;

        public event Handler LPBLoadOK;
        public event Handler LPBLoadFail;
        public event Handler LPBSaveOK;
        public event Handler LPBSaveFail;

        public event Handler AutoblowerLoadOK;
        public event Handler AutoblowerLoadFail;
        public event Handler AutoblowerSaveOK;
        public event Handler AutoblowerSaveFail;

        public event Handler TermalLoadOK;
        public event Handler TermalLoadFail;
        public event Handler TermalSaveOK;
        public event Handler TermalSaveFail;

        public event Handler InputSygnalLoadOK;
        public event Handler InputSygnalLoadFail;
        public event Handler InputSygnalSaveOK;
        public event Handler InputSygnalSaveFail;

        public event Handler OscopeLoadOK;
        public event Handler OscopeLoadFail;
        public event Handler OscopeSaveOK;
        public event Handler OscopeSaveFail;

        public event Handler ConfigEthernetLoadOK;
        public event Handler ConfigEthernetLoadFail;
        public event Handler ConfigEthernetSaveOK;
        public event Handler ConfigEthernetSaveFail;

        public event Handler PowerTransLoadOK;
        public event Handler PowerTransLoadFail;
        public event Handler PowerTransSaveOK;
        public event Handler PowerTransSaveFail;

        public event Handler MeasureTransLoadOK;
        public event Handler MeasureTransLoadFail;
        public event Handler MeasureTransSaveOK;
        public event Handler MeasureTransSaveFail;

        public event Handler InpSygnalLoadOK;
        public event Handler InpSygnalLoadFail;
        public event Handler InpSygnalSaveOK;
        public event Handler InpSygnalSaveFail;

        public event Handler ElsSygnalLoadOK1;
        public event Handler ElsSygnalLoadFail1;
        public event Handler ElsSygnalSaveOK1;
        public event Handler ElsSygnalSaveFail1;

        public event Handler ElsSygnalLoadOK2;
        public event Handler ElsSygnalLoadFail2;
        public event Handler ElsSygnalSaveOK2;
        public event Handler ElsSygnalSaveFail2;

        public event Handler ElsSygnalLoadOK3;
        public event Handler ElsSygnalLoadFail3;
        public event Handler ElsSygnalSaveOK3;
        public event Handler ElsSygnalSaveFail3;

        public event Handler ElsSygnalLoadOK4;
        public event Handler ElsSygnalLoadFail4;
        public event Handler ElsSygnalSaveOK4;
        public event Handler ElsSygnalSaveFail4;

        public event Handler CurrentProtLastLoadOK;
        public event Handler CurrentProtLastLoadFail;
        public event Handler CurrentProtLastSaveOK;
        public event Handler CurrentProtLastSaveFail;

        public event Handler CurrentProtAllLoadOK;
        public event Handler CurrentProtAllLoadFail;
        public event Handler CurrentProtAllSaveOK;
        public event Handler CurrentProtAllSaveFail;

        public event Handler ParamAutomatLoadOK;
        public event Handler ParamAutomatLoadFail;
        public event Handler ParamAutomatSaveOK;
        public event Handler ParamAutomatSaveFail;

        public event Handler ConfirmConstraintSaveOK;
        public event Handler ConfirmConstraintSaveFail;

        public event Handler OscJournalLoadOK;
        public event Handler OscJournalLoadFail;
        public event Handler OscJournalSaveOK;
        public event Handler OscJournalSaveFail;

        public event Handler OscilloscopeLoadOK;
        public event Handler OscilloscopeLoadFail;
        public event Handler OscilloscopeSaveOK;
        public event Handler OscilloscopeSaveFail;

        public event Handler SysJournalLoadOK;
        public event Handler SysJournalLoadFail;
        public event Handler SysJournalSaveOK;
        public event Handler SysJournalSaveFail;

        public event Handler AlarmJournalLoadOK;
        public event Handler AlarmJournalLoadFail;
        public event Handler AlarmJournalSaveOK;
        public event Handler AlarmJournalSaveFail;


        public event Handler AnalogBDLoadOK;
        public event Handler AnalogBDLoadFail;

        public event Handler BitBDLoadOK;
        public event Handler BitBDLoadFail;
        #endregion

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(UDZT); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr801; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР801 (до в.2.08)"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        #region Конфигурация Ethrenet
        
        public int IpLo1
        {
            get { return Common.GetBits(this._configEthernet.Value[0], 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._configEthernet.Value[0] = Common.SetBits(this._configEthernet.Value[0], (ushort)value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public int IpLo2
        {
            get { return (ushort)(Common.GetBits(this._configEthernet.Value[0], 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._configEthernet.Value[0] = Common.SetBits(this._configEthernet.Value[0], (ushort)value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        public int IpHi1
        {
            get { return Common.GetBits(this._configEthernet.Value[1], 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this._configEthernet.Value[1] = Common.SetBits(this._configEthernet.Value[1], (ushort)value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        public int IpHi2
        {
            get { return (ushort)(Common.GetBits(this._configEthernet.Value[1], 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._configEthernet.Value[1] = Common.SetBits(this._configEthernet.Value[1], (ushort)value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        #endregion

        #region Силовой трансформатор
        public slot PowerTransformator { get { return this._powerTrans; } }

        #region Сторона S1

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public int S1WindingRatedPower
        {
            get { return this._powerTrans.Value[1]; }
            set { this._powerTrans.Value[1] = (ushort)value; }
        }

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public double S1WindingRatedPowerDOUBLE
        {
            get
            {
                if (this._powerTrans.Value[1] == 65535)
                    return 1024;
                else
                    return this._powerTrans.Value[1] / (double)64;
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[1] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[1] = (ushort)(value * 64);
                }
            }
        }

        /// <summary>
        /// Номинальное напряжение обмотки
        /// </summary>
        public double S1WindingRatedVoltage
        {
            get
            {
                if (this._powerTrans.Value[0] == 65535)
                    return 1024;
                else
                {
                    return Measuring.GetU(this._powerTrans.Value[0], ConstraintKoefficient.K_4000);
                }
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[0] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[0] = (ushort)Measuring.SetU((double)value, ConstraintKoefficient.K_4000);
                }
            }
        }

        /// <summary>
        /// Тип обмотки
        /// </summary>
        public string S1WindingType
        {
            get { return Strings.WindingType[this._powerTrans.Value[2]]; }
            set { this._powerTrans.Value[2] = (ushort)Strings.WindingType.IndexOf(value); }
        }

        /// <summary>
        /// Группа соединения
        /// </summary>
        public string S1ConnectionGroup
        {
            get { return Strings.ConnectionGroup[this._powerTrans.Value[3]]; }
            set
            {
                this._powerTrans.Value[3] = 0; // (ushort)Strings.ConnectionGroup.IndexOf(value);
            }
        }

        /// <summary>
        /// Измерение земля
        /// </summary>
        public string S1Measuring
        {
            get { return Strings.Measuring[this._powerTrans.Value[4]]; }
            set { this._powerTrans.Value[4] = (ushort)Strings.Measuring.IndexOf(value); }
        }

        #endregion

        #region Сторона S2

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public int S2WindingRatedPower
        {
            get { return this._powerTrans.Value[7]; }
            set { this._powerTrans.Value[7] = (ushort)value; }

        }

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public double S2WindingRatedPowerDOUBLE
        {
            get
            {
                if (this._powerTrans.Value[7] == 65535)
                    return 1024;
                else
                    return this._powerTrans.Value[7] / (double)64;
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[7] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[7] = (ushort)(value * 64);
                }
            }
        }

        /// <summary>
        /// Номинальное напряжение обмотки
        /// </summary>
        public double S2WindingRatedVoltage
        {
            get
            {
                if (this._powerTrans.Value[6] == 65535)
                    return 1024;
                else
                {
                    return Measuring.GetU(this._powerTrans.Value[6], ConstraintKoefficient.K_4000);
                }
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[6] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[6] = (ushort)Measuring.SetU((double)value, ConstraintKoefficient.K_4000);
                }
            }
        }

        /// <summary>
        /// Тип обмотки
        /// </summary>
        public string S2WindingType
        {
            get { return Strings.WindingType[this._powerTrans.Value[8]]; }
            set { this._powerTrans.Value[8] = (ushort)Strings.WindingType.IndexOf(value); }
        }

        /// <summary>
        /// Группа соединения
        /// </summary>
        public string S2ConnectionGroup
        {
            get
            {
                return Strings.ConnectionGroup[this._powerTrans.Value[9]];
            }
            set
            {
                this._powerTrans.Value[9] = (ushort)Strings.ConnectionGroup.IndexOf(value);
            }
        }

        /// <summary>
        /// Измерение земля
        /// </summary>
        public string S2Measuring
        {
            get { return Strings.Measuring[this._powerTrans.Value[10]]; }
            set { this._powerTrans.Value[10] = (ushort)Strings.Measuring.IndexOf(value); }
        }

        #endregion

        #region Сторона S3

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public int S3WindingRatedPower
        {
            get { return this._powerTrans.Value[13]; }
            set { this._powerTrans.Value[13] = (ushort)value; }
        }

        /// <summary>
        /// Номинальная мощьность обмотки
        /// </summary>
        public double S3WindingRatedPowerDOUBLE
        {
            get
            {
                if (this._powerTrans.Value[13] == 65535)
                    return 1024;
                else
                    return this._powerTrans.Value[13] / (double)64;
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[13] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[13] = (ushort)(value * 64);
                }
            }
        }

        /// <summary>
        /// Номинальное напряжение обмотки
        /// </summary>
        public double S3WindingRatedVoltage
        {
            get
            {
                if (this._powerTrans.Value[12] == 65535)
                    return 1024;
                else
                {
                    return Measuring.GetU(this._powerTrans.Value[12], ConstraintKoefficient.K_4000);
                }
            }
            set
            {
                if (value == 1024)
                {
                    this._powerTrans.Value[12] = (ushort)65535;
                }
                else
                {
                    this._powerTrans.Value[12] = (ushort)Measuring.SetU((double)value, ConstraintKoefficient.K_4000);
                }
            }
        }

        /// <summary>
        /// Тип обмотки
        /// </summary>
        public string S3WindingType
        {
            get { return Strings.WindingType[this._powerTrans.Value[14]]; }
            set { this._powerTrans.Value[14] = (ushort)Strings.WindingType.IndexOf(value); }
        }

        /// <summary>
        /// Группа соединения
        /// </summary>
        public string S3ConnectionGroup
        {
            get { return Strings.ConnectionGroup[this._powerTrans.Value[15]]; }
            set { this._powerTrans.Value[15] = (ushort)Strings.ConnectionGroup.IndexOf(value); }
        }

        /// <summary>
        /// Измерение земля
        /// </summary>
        public string S3Measuring
        {
            get { return Strings.Measuring[this._powerTrans.Value[16]]; }
            set { this._powerTrans.Value[16] = (ushort)Strings.Measuring.IndexOf(value); }
        }

        #endregion
        #endregion

        #region Выходные сигналы
        #region ВЛС

        [XmlIgnore]
        public ushort[] VlsValues
        {
            get
            {
                Array.ConstrainedCopy(this._elsSygnal1.Value, 0, this._elsSygnal, 0, this._elsSygnal1.Size);
                Array.ConstrainedCopy(this._elsSygnal2.Value, 0, this._elsSygnal, this._elsSygnal1.Size, this._elsSygnal2.Size);
                Array.ConstrainedCopy(this._elsSygnal3.Value, 0, this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size, this._elsSygnal3.Size);
                Array.ConstrainedCopy(this._elsSygnal4.Value, 0, this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size + this._elsSygnal3.Size, this._elsSygnal4.Size);
                return this._elsSygnal;
            }
            set
            {
                this._elsSygnal = value;
                Array.ConstrainedCopy(this._elsSygnal, 0, this._elsSygnal1.Value, 0, this._elsSygnal1.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size, this._elsSygnal2.Value, 0, this._elsSygnal2.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size, this._elsSygnal3.Value, 0, this._elsSygnal3.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size + this._elsSygnal3.Size, this._elsSygnal4.Value, 0, this._elsSygnal4.Size);
            }
        }

        public string[][] VlsXml
        {
            get
            {
                bool[][] vls = this.Vls;
                string[][] result = new string[16][];
                for (int i = 0; i < 16; i++)
                {
                    var temp = new List<string>();
                    for (int j = 0; j < Strings.VLSSygnals.Count; j++)
                    {
                        if (vls[i][j])
                        {
                            temp.Add(Strings.VLSSygnals[j]);
                        }

                    }
                    result[i] = temp.ToArray();
                }
                return result;
            }
            set { }
        }

        [XmlIgnore]
        public bool[][] Vls
        {
            get
            {
                bool[][] res = new bool[16][];
                ushort[] vls = this.VlsValues;

                int byteIndex = 0;

                for (int i = 0; i < 16; i++) //Счетчик закладок ВЛС 1-16
                {
                    byteIndex = 16 * i; //индекс массива 1ВЛС = 16 байт
                    res[i] = new bool[Strings.VLSSygnals.Count];
                    for (int j = 0; j < Strings.VLSSygnals.Count; j += 16) //Счетчик элементов одного ВЛС 1-125
                    {
                        int itemIndex = j; //индекс элемента списка
                        for (int bitIndex = 0; bitIndex < 16; bitIndex++)//перебираем биты слова
                        {
                            if (itemIndex < Strings.VLSSygnals.Count)//Чтоб индекс элемента списка не зашкалил за длинну списка
                            {
                                res[i][itemIndex] = Common.GetBit(vls[byteIndex], bitIndex);

                                itemIndex++;//переходим к сл. элементу списка
                            }
                        }
                        byteIndex++;//переходим к следующему байту массива данного ВЛС
                    }
                }
                return res;
            }

            set
            {
                ushort[] vls = new ushort[16 * 16];

                int byteIndex = 0;

                for (int i = 0; i < 16; i++) //Счетчик закладок ВЛС 1-16
                {
                    byteIndex = 16 * i; //индекс массива 1ВЛС = 16 байт

                    for (int j = 0; j < Strings.VLSSygnals.Count; j += 16) //Счетчик элементов одного ВЛС 1-125
                    {
                        int itemIndex = j; //индекс элемента списка
                        for (int bitIndex = 0; bitIndex < 16; bitIndex++)//перебираем биты слова
                        {
                            if (itemIndex < Strings.VLSSygnals.Count)//Чтоб индекс элемента списка не зашкалил за длинну списка
                            {
                                vls[byteIndex] = Common.SetBit(vls[byteIndex], bitIndex, value[i][itemIndex]);
                                itemIndex++;//переходим к сл. элементу списка
                            }
                        }
                        byteIndex++;//переходим к следующему байту массива данного ВЛС
                    }
                }
                this.VlsValues = vls;
            }
        }

        #endregion

        #region Выходные реле

        private ushort[] OutputReleValues
        {
            get
            {
                return this._paramAutomat.Value;
            }
            set
            {
                this._paramAutomat.Value = value;
            }
        }


        public string[][] Relays
        {
            get
            {
                var res = new string[OUTPUT_RELAY_COUNT][];
                for (int i = 0; i < OUTPUT_RELAY_COUNT; i++)
                {
                    res[i] = new string[3];
                    int indSygnal = this.OutputReleValues[i * 4];
                    res[i][1] = Strings.Sygnal.Count > indSygnal
                            ? Strings.Sygnal[indSygnal]
                            : Strings.Sygnal[Strings.Sygnal.Count - 1];
                    res[i][0] = Strings.SygnalType[this.OutputReleValues[i * 4 + 1]];

                    int val = Common.GetBits(this.OutputReleValues[i * 4 + 2], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                    int ind = Common.GetBits(this.OutputReleValues[i * 4 + 2], 15);

                    if (ind == 0)
                    {
                        val *= 10;
                    }
                    else
                    {
                        val *= 100;
                    }

                    res[i][2] = val.ToString();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < OUTPUT_RELAY_COUNT; i++)
                {
                    this.OutputReleValues[i * 4] = (ushort)Strings.Sygnal.IndexOf(value[i][1]);
                    this.OutputReleValues[i * 4 + 1] = (ushort)Strings.SygnalType.IndexOf(value[i][0]);

                    ushort ind = 0;
                    ushort val = (ushort)Convert.ToInt32(value[i][2]);
                    if (Convert.ToInt32(value[i][2]) > 32767 * 10)
                    {
                        ind = 1;
                        val = (ushort)(Convert.ToInt32(value[i][2]) / 100);
                    }
                    else
                    {
                        val = (ushort)(val / 10);
                    }
                    if (Convert.ToInt32(value[i][2]) > 32767 && Convert.ToInt32(value[i][2]) < 32767 * 10)
                    {
                        val = (ushort)(Convert.ToInt32(value[i][2]) / 10);
                    }
                    if (val > 32767)
                    {
                        val = 32767;
                    }
                    this.OutputReleValues[i * 4 + 2] = Common.SetBits(this.OutputReleValues[i * 4 + 2], ind, 15);
                    this.OutputReleValues[i * 4 + 2] = Common.SetBits(this.OutputReleValues[i * 4 + 2], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                }
            }
        }




        public string OutputN1
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 0);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = value == "Есть";
                this._paramAutomat.Value[this._paramAutomat.Value.Length - 2] = Common.SetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 0, rez);
            }
        }

        public string OutputN2
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 1);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = value == "Есть";

                this._paramAutomat.Value[this._paramAutomat.Value.Length - 2] = Common.SetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 1, rez);
            }
        }

        public string OutputN3
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 2);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = value == "Есть";

                this._paramAutomat.Value[this._paramAutomat.Value.Length - 2] = Common.SetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 2, rez);
            }
        }

        public string OutputN4
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 3);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = value == "Есть";

                this._paramAutomat.Value[this._paramAutomat.Value.Length - 2] = Common.SetBit(this._paramAutomat.Value[this._paramAutomat.Value.Length - 2], 3, rez);
            }
        }
        public int OutputImp
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._paramAutomat.Value[this._paramAutomat.Value.Length - 1]);
            }
            set
            {
                this._paramAutomat.Value[this._paramAutomat.Value.Length - 1] = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public const int OUTPUT_RELAY_COUNT = 18;





        #endregion


        #region Выходные индикаторы
        public const int INDICATORS_COUNT = 12;

        public string[][] Indicators
        {
            get
            {
                var res = new string[INDICATORS_COUNT][];
                int index = 0;
                for (int i = 0; i < INDICATORS_COUNT; i++)
                {
                    res[i] = new string[3];
                    int indSygnal = this.OutputReleValues[index + 72];
                    res[i][1] = Strings.Sygnal.Count > indSygnal
                        ? Strings.Sygnal[indSygnal]
                        : Strings.Sygnal[Strings.Sygnal.Count - 1];
                    bool type = Common.GetBit(this.OutputReleValues[index + 72 + 1], 0);
                    int typeInt;
                    bool color = Common.GetBit(this.OutputReleValues[index + 72 + 1], 8);
                    if (type)
                    {
                        typeInt = 1;
                    }
                    else
                    {
                        typeInt = 0;
                    }
                    if (color)
                    {
                        res[i][2] = Color.Green.ToKnownColor().ToString();

                    }
                    else
                    {
                        res[i][2] = Color.Red.ToKnownColor().ToString();

                    }
                    res[i][0] = Strings.SygnalType[typeInt];
                    index += 2;
                }
                return res;
            }
            set
            {

                int index = 0;
                for (int i = 0; i < INDICATORS_COUNT; i++)
                {
                    this.OutputReleValues[index + 72] =
                        (ushort)Strings.Sygnal.IndexOf(value[i][1]);

                    bool modeFlag = value[i][0] == "Блинкер";
                    this.OutputReleValues[index + 72 + 1] = Common.SetBit(this.OutputReleValues[index + 72 + 1], 0,
                                                                                   modeFlag);


                    bool colorFlag = value[i][2] == Color.Green.ToKnownColor().ToString();
                    this.OutputReleValues[index + 72 + 1] = Common.SetBit(this.OutputReleValues[index + 72 + 1], 8,
                                                                                   colorFlag);

                    index += 2;
                }
            }
        }



        #endregion
        #endregion

        #region Входные Логические сигналы
        public string[][] InpSygnals
        {
            get
            {
                var res = new string[16][];
                int _inpSygnalsIndex = 0;
                ushort[] inpSygnals = this._inpSygnal.Value;
                for (int j = 0; j < 16; j++)
                {
                    res[j] = new string[24];
                    _inpSygnalsIndex = j * 4;
                    int indexLs = 0;
                    do
                    {
                        for (int i = 0; i < 16; i += 2)
                        {
                            int IND = Common.GetBits(inpSygnals[_inpSygnalsIndex], i, i + 1) >> i;
                            res[j][indexLs] = Strings.LogycValues[IND];
                            indexLs++;
                        }
                        _inpSygnalsIndex++;
                    } while (indexLs < 24);
                }

                return res;
                // return _inpSygnal.Value;
            }
            set
            {
                int _inpSygnalsIndex = 0;
                ushort[] _inpSygnals = new ushort[8 * 16 / 2];
                for (int j = 0; j < value.Length; j++)
                {
                    _inpSygnalsIndex = j * 4;
                    int indexLs = 0;
                    do
                    {
                        for (int i = 0; i < 16; i += 2)
                        {
                            _inpSygnals[_inpSygnalsIndex] = Common.SetBits(_inpSygnals[_inpSygnalsIndex], (ushort)Strings.LogycValues.IndexOf(value[j][indexLs]), i, i + 1);
                            indexLs++;
                        }
                        _inpSygnalsIndex++;
                    } while (indexLs < 24);
                }
                this._inpSygnal.Value = _inpSygnals;
            }
        }

        public string GrUst
        {
            get
            {
                int index = this._inputsygnal.Value[0];
                return Strings.SygnalInputSignals[index];
            }
            set
            {
                this._inputsygnal.Value[0] = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string SbInd
        {
            get
            {
                int index = this._inputsygnal.Value[1];
                return Strings.SygnalInputSignals[index];
            }
            set
            {
                this._inputsygnal.Value[1] = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        #region Выключатель и Управление
        #region Выключатель
        public string Switch
        {
            get { return Strings.YesNo[Common.GetBits(this._switch.Value[0], 7) >> 7]; }
            set { this._switch.Value[0] = (ushort)Common.SetBits(this._switch.Value[0], (ushort)Strings.YesNo.IndexOf(value), 7); }
        }

        public string SwitchOff
        {
            get { return Strings.DiffBlocking[this._switch.Value[2]]; }
            set { this._switch.Value[2] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchOn
        {
            get { return Strings.DiffBlocking[this._switch.Value[1]]; }
            set { this._switch.Value[1] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchError
        {
            get { return Strings.DiffBlocking[this._switch.Value[3]]; }
            set { this._switch.Value[3] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchBlock
        {
            get { return Strings.DiffBlocking[this._switch.Value[4]]; }
            set { this._switch.Value[4] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public int SwitchTUrov
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.Value[5]); }
            set { this._switch.Value[5] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public double SwitchIUrov
        {
            get { return Measuring.GetConstraintOnly(this._switch.Value[6], ConstraintKoefficient.K_4000); }
            set { this._switch.Value[6] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000); }
        }

        public int SwitchImpuls
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.Value[7]); }
            set { this._switch.Value[7] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int SwitchTUskor
        {
            get { return ValuesConverterCommon.GetWaitTime(this._switch.Value[8]); }
            set { this._switch.Value[8] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string SwitchKontCep
        {
            get { return Strings.ModesLight[this._switch.Value[9]]; }
            set { this._switch.Value[9] = (ushort)Strings.ModesLight.IndexOf(value); }
        }
        #endregion

        #region Управление
        public string SwitchKeyOn
        {
            get { return Strings.DiffBlocking[this._switch.Value[10]]; }
            set { this._switch.Value[10] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchKeyOff
        {
            get { return Strings.DiffBlocking[this._switch.Value[11]]; }
            set { this._switch.Value[11] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchVneshOn
        {
            get { return Strings.DiffBlocking[this._switch.Value[12]]; }
            set { this._switch.Value[12] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchVneshOff
        {
            get { return Strings.DiffBlocking[this._switch.Value[13]]; }
            set { this._switch.Value[13] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string SwitchButtons
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.Value[0], 0))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.Value[0] = Common.SetBit(this._switch.Value[0], 0, res);
            }
        }

        public string SwitchKey
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.Value[0], 1))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.Value[0] = Common.SetBit(this._switch.Value[0], 1, res);
            }
        }

        public string SwitchVnesh
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.Value[0], 2))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.Value[0] = Common.SetBit(this._switch.Value[0], 2, res);
            }
        }

        public string SwitchSDTU
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._switch.Value[0], 3))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                this._switch.Value[0] = Common.SetBit(this._switch.Value[0], 3, res);
            }
        }

        public string SwitchBindings
        {
            get
            {
                return Strings.Bindings2[Common.GetBits(this._switch.Value[0], 14, 15) >> 14];
            }
            set
            {
                this._switch.Value[0] = Common.SetBits(this._switch.Value[0], (ushort)Strings.Bindings2.IndexOf(value), 14, 15);
            }
        }
        #endregion

        #endregion

        #region АВР
        public string AVRBySignal
        {
            get
            {
                return Strings.APVModes[Common.GetBits(this._avr.Value[0], 0) >> 0];
            }
            set
            {
                this._avr.Value[0] = Common.SetBits(this._avr.Value[0], (ushort)Strings.APVModes.IndexOf(value), 0);
            }
        }

        public string AVRByOff
        {
            get
            {
                return Strings.APVModes[Common.GetBits(this._avr.Value[0], 2) >> 2];
            }
            set
            {
                this._avr.Value[0] = Common.SetBits(this._avr.Value[0], (ushort)Strings.APVModes.IndexOf(value), 2);
            }
        }

        public string AVRBySelfOff
        {
            get { return Strings.APVModes[Common.GetBits(this._avr.Value[0], 1) >> 1]; }
            set { this._avr.Value[0] = Common.SetBits(this._avr.Value[0], (ushort)Strings.APVModes.IndexOf(value), 1); }
        }

        public string AVRByDiff
        {
            get
            {
                return Strings.APVModes[Common.GetBits(this._avr.Value[0], 3) >> 3];
            }
            set
            {
                this._avr.Value[0] = Common.SetBits(this._avr.Value[0], (ushort)Strings.APVModes.IndexOf(value), 3);
            }
        }

        public string AVRSIGNOn
        {
            get { return Strings.DiffBlocking[this._avr.Value[3]]; }
            set { this._avr.Value[3] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string AVRBlocking
        {
            get { return Strings.DiffBlocking[this._avr.Value[1]]; }
            set { this._avr.Value[1] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string AVRBlockClear
        {
            get { return Strings.DiffBlocking[this._avr.Value[2]]; }
            set { this._avr.Value[2] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public string AVRResolve
        {
            get { return Strings.DiffBlocking[this._avr.Value[4]]; }
            set { this._avr.Value[4] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public int AVRTimeOn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.Value[5]); }
            set { this._avr.Value[5] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRBack
        {
            get { return Strings.DiffBlocking[this._avr.Value[6]]; }
            set { this._avr.Value[6] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public int AVRTimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.Value[7]); }
            set { this._avr.Value[7] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int AVRTimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this._avr.Value[8]); }
            set { this._avr.Value[8] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRClear
        {
            get
            {
                int index = 1;
                if (Common.GetBit(this._avr.Value[0], 7))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = Strings.Forbidden.IndexOf(value) == 0;
                this._avr.Value[0] = Common.SetBit(this._avr.Value[0], 7, res);
            }
        }

        #endregion

        #region АПВ
        public string APVModes
        {
            get
            {
                return Strings.ApvTypes[Common.GetBits(this._apv.Value[0], 0, 1) >> 0];
            }
            set
            {
                this._apv.Value[0] = Common.SetBits(this._apv.Value[0], (ushort)Strings.ApvTypes.IndexOf(value), 0, 1);
            }
        }

        public string APVBlocking
        {
            get { return Strings.DiffBlocking[this._apv.Value[1]]; }
            set { this._apv.Value[1] = (ushort)Strings.DiffBlocking.IndexOf(value); }
        }

        public int APVTBlock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.Value[2]); }
            set { this._apv.Value[2] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APVTReady
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.Value[3]); }
            set { this._apv.Value[3] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APV1Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.Value[4]); }
            set { this._apv.Value[4] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APV2Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._apv.Value[5]); }
            set { this._apv.Value[5] = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string APVOff
        {
            get
            {
                return Strings.APVModes[Common.GetBits(this._apv.Value[0], 8) >> 8];
            }
            set
            {
                this._apv.Value[0] = Common.SetBits(this._apv.Value[0], (ushort)Strings.APVModes.IndexOf(value), 8);
            }
        }
        #endregion

        #region ЛЗШ
        public string LZHMode
        {
            get
            {
                int index = this._lpb.Value[0] < Strings.LZHModes.Count ? this._lpb.Value[0] : 0;
                return Strings.LZHModes[index];
            }
            set
            {
                this._lpb.Value[0] = (ushort)Strings.LZHModes.IndexOf(value);
            }
        }

        public double LZHUstavka
        {
            get
            {
                return Measuring.GetConstraintOnly(this._lpb.Value[1], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._lpb.Value[1] = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }
        #endregion

        #endregion

        #region Данные измерительных трансформаторов
        /// <summary>
        /// Номинальный первичный ток TT L1
        /// </summary>
        public int TT_L1
        {
            get
            {
                return this._measureTrans.Value[0];
            }
            set
            {
                this._measureTrans.Value[0] = (ushort)value;
            }
        }

        /// <summary>
        /// Номинальный первичный ток TT X1
        /// </summary>
        public int TT_X1
        {
            get
            {
                return this._measureTrans.Value[1];
            }
            set
            {
                this._measureTrans.Value[1] = (ushort)value;
            }
        }

        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_L1_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[2]];
            }
            set
            {
                this._measureTrans.Value[2] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }



        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_X1_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[3]];
            }
            set
            {
                this._measureTrans.Value[3] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        public string TT_L1_Binding
        {
            get
            {
                return Strings.Bindings[this._measureTrans.Value[4]];
            }
            set
            {
                this._measureTrans.Value[4] = (ushort)Strings.Bindings.IndexOf(value);
            }
        }



        /// <summary>
        /// Номинальный первичный ток TT L2
        /// </summary>
        public int TT_L2
        {
            get
            {
                return this._measureTrans.Value[6];
            }
            set
            {
                this._measureTrans.Value[6] = (ushort)value;
            }
        }


        /// <summary>
        /// Номинальный первичный ток TT X2
        /// </summary>
        public int TT_X2
        {
            get
            {
                return this._measureTrans.Value[7];
            }
            set
            {
                this._measureTrans.Value[7] = (ushort)value;
            }
        }


        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_L2_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[8]];
            }
            set
            {
                this._measureTrans.Value[8] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }


        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_X2_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[9]];
            }
            set
            {
                this._measureTrans.Value[9] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        public string TT_L2_Binding
        {
            get
            {
                return Strings.Bindings[this._measureTrans.Value[10]];
            }
            set
            {
                this._measureTrans.Value[10] = (ushort)Strings.Bindings.IndexOf(value);
            }
        }




        /// <summary>
        /// Номинальный первичный ток TT L3
        /// </summary>
        public int TT_L3
        {
            get
            {
                return this._measureTrans.Value[12];
            }
            set
            {
                this._measureTrans.Value[12] = (ushort)value;
            }
        }


        /// <summary>
        /// Номинальный первичный ток TT X3
        /// </summary>
        public int TT_X3
        {
            get
            {
                return this._measureTrans.Value[13];
            }
            set
            {
                this._measureTrans.Value[13] = (ushort)value;
            }
        }


        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_L3_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[14]];
            }
            set
            {
                this._measureTrans.Value[14] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }


        /// <summary>
        /// Полярность подключения
        /// </summary>
        public string TT_X3_Polarity
        {
            get
            {
                return Strings.PolarityOfConnection[this._measureTrans.Value[15]];
            }
            set
            {
                this._measureTrans.Value[15] = (ushort)Strings.PolarityOfConnection.IndexOf(value);
            }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        public string TT_L3_Binding
        {
            get
            {
                return Strings.Bindings[this._measureTrans.Value[16]];
            }
            set
            {
                this._measureTrans.Value[16] = (ushort)Strings.Bindings.IndexOf(value);
            }
        }


        /// <summary>
        /// Коэффициент трансформации TH L
        /// </summary>
        public double TH_L
        {
            get
            {
                return Measuring.GetTH(Common.GetBits(this._measureTrans.Value[18], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14));
            }
            set
            {
                ushort val = (ushort)Measuring.SetTH(value);
                if (val > 32767)
                {
                    val = 32767;
                }
                this._measureTrans.Value[18] = Common.SetBits(this._measureTrans.Value[18], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Коэффициент трансформации домножения TH L
        /// </summary>
        public int TH_LKoef
        {
            get
            {
                return Common.GetBit(this._measureTrans.Value[18], 15) ? 1000 : 1;
            }
            set
            {
                if (value == 1000)
                    this._measureTrans.Value[18] = Common.SetBit(this._measureTrans.Value[18], 15, true);
                else
                    this._measureTrans.Value[18] = Common.SetBit(this._measureTrans.Value[18], 15, false);
            }
        }

        /// <summary>
        /// Коэффициент трансформации ток TT X
        /// </summary>
        public double TH_X
        {
            get
            {
                ushort val = Common.GetBits(this._measureTrans.Value[19], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Measuring.GetTH(val);
            }
            set
            {
                ushort val = (ushort)Measuring.SetTH(value);
                if (val > 32767)
                {
                    val = 32767;
                }
                this._measureTrans.Value[19] = Common.SetBits(this._measureTrans.Value[19], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        public double THXprecision
        {
            get
            {
                return (double)Common.GetBits(this._measureTrans.Value[19], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) / 256;
            }
        }

        /// <summary>
        /// Коэффициент трансформации домножения TH L
        /// </summary>
        public int TH_XKoef
        {
            get
            {
                int index;
                if (Common.GetBit(this._measureTrans.Value[19], 15))
                {
                    index = 1000;
                }
                else
                {
                    index = 1;
                }

                return index;
            }
            set
            {
                if (value == 1000)
                    this._measureTrans.Value[19] = Common.SetBit(this._measureTrans.Value[19], 15, true);
                else
                    this._measureTrans.Value[19] = Common.SetBit(this._measureTrans.Value[19], 15, false);
            }
        }

        /// <summary>
        /// Полярность L
        /// </summary>
        public string PolarityL
        {
            get
            {
                int index = this._measureTrans.Value[20];
                return Strings.SygnalInputSignals[index];
            }
            set
            {
                this._measureTrans.Value[20] = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        /// <summary>
        /// Полярность X
        /// </summary>
        public string PolarityX
        {
            get
            {
                int index = this._measureTrans.Value[21];
                return Strings.SygnalInputSignals[index];
            }
            set
            {
                this._measureTrans.Value[21] = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        public string TH_LX_Binding
        {
            get
            {
                int index = this._measureTrans.Value[22];
                return Strings.BindingLX[index];
            }
            set
            {
                this._measureTrans.Value[22] = (ushort)Strings.BindingLX.IndexOf(value);
            }
        }

        #endregion

        #region Все защиты

        private void InitProtAllList()
        {
            int _fulcount = 0;
            int _ost = 0;
            ushort step = 120;
            if (this._currentProtAll.Value.Length > 255)
            {
                _fulcount = this._currentProtAll.Value.Length / step;
                _ost = this._currentProtAll.Value.Length % step;
            }
            if (_ost != 0)
            {
                _fulcount++;
            }
            ushort start = this._currentProtAll.Start;
            for (int i = 0; i < _fulcount; i++)
            {
                if (i + 1 == _fulcount)
                {
                    slot curProtAll = new slot(start, (ushort)(start + (ushort)_ost));
                    this.CurrentProtAll.Add(curProtAll);
                }
                else
                {
                    slot curProtAll = new slot(start, (ushort)(start + step));
                    this.CurrentProtAll.Add(curProtAll);
                    start = this.CurrentProtAll[i].End;
                }
            }
        }
        #region Резервные/Основные уставки
        ushort[] main = new ushort[0];
        //[XmlIgnore]
        public ushort[] CurrentProtAllMainProperty
        {
            get
            {
                this.GetCurrentProtAll();
                Array.ConstrainedCopy(this._curProtAll, 0, this.main, 0, this.main.Length);
                return this.main;
            }
            set
            {
                Array.ConstrainedCopy(value, 0, this._curProtAll, 0, value.Length);
                this.SetCurrentProtAll();
                this._currentProtAll.Value = this._curProtAll;
            }
        }



        ushort[] reserve = new ushort[0];
        //[XmlIgnore]
        public ushort[] CurrentProtAllReservProperty
        {
            get
            {
                this.GetCurrentProtAll();

                Array.ConstrainedCopy(this._curProtAll, this._curProtAll.Length / 2, this.reserve, 0, this.reserve.Length);
                return this.reserve;
            }
            set
            {
                Array.ConstrainedCopy(value, 0, this._curProtAll, this._curProtAll.Length / 2, value.Length);
                this.SetCurrentProtAll();
                this._currentProtAll.Value = this._curProtAll;
            }
        }

        private void GetCurrentProtAll()
        {
            ushort destinationindex = 0;
            for (int i = 0; i < this.CurrentProtAll.Count; i++)
            {
                Array.ConstrainedCopy(this.CurrentProtAll[i].Value, 0, this._curProtAll, destinationindex, this.CurrentProtAll[i].Size);
                destinationindex += this.CurrentProtAll[i].Size;
            }
        }

        private void SetCurrentProtAll()
        {
            ushort destinationindex = 0;
            for (int i = 0; i < this.CurrentProtAll.Count; i++)
            {
                Array.ConstrainedCopy(this._curProtAll, destinationindex, this.CurrentProtAll[i].Value, 0, this.CurrentProtAll[i].Size);
                destinationindex += this.CurrentProtAll[i].Size;
            }
        }
        #endregion

        #region Диф. токовая защита

        private ushort[] _arrayDifMainCutOf = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFMAIN)) / 2 + System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFCUTOFF)) / 2];

        public ushort[] ArrayDifMainCutOf
        {
            get
            {
                return this._arrayDifMainCutOf;
            }
            set
            {
                this._arrayDifMainCutOf = value;
            }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DTZMode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_arrayDifMainCutOf[0], 0))
                //{
                //    index = 1;
                //}
                //else 
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._arrayDifMainCutOf[0], 0, 1)];
            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else 
                //{
                //    bit = false;
                //}
                //_arrayDifMainCutOf[0] = Common.SetBit(_arrayDifMainCutOf[0], 0, bit);

                this._arrayDifMainCutOf[0] = Common.SetBits(this._arrayDifMainCutOf[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }


        /// <summary>
        /// Блокировка
        /// </summary>
        public string DTZblocking
        {
            get
            {
                return Strings.Blocking[this._arrayDifMainCutOf[2]];
            }
            set
            {
                this._arrayDifMainCutOf[2] = (ushort)Strings.Blocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Уставка Iд>
        /// </summary>
        public double DTZConstraint
        {
            get
            {
                return Measuring.GetConstraintOnly(this._arrayDifMainCutOf[3], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._arrayDifMainCutOf[3] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// Выдержка Tд>
        /// </summary>
        public int DTZTimeEndurance
        {
            get
            {
                int val = Common.GetBits(this._arrayDifMainCutOf[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._arrayDifMainCutOf[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._arrayDifMainCutOf[4] = Common.SetBits(this._arrayDifMainCutOf[4], ind, 15);
                this._arrayDifMainCutOf[4] = Common.SetBits(this._arrayDifMainCutOf[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Iб1 - начало
        /// </summary>
        public double DTZIb1
        {
            get
            {
                return Measuring.GetConstraintOnly(this._arrayDifMainCutOf[5], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._arrayDifMainCutOf[5] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// К1 - угол наклона
        /// </summary>
        public int DTZK1
        {
            get
            {
                return this._arrayDifMainCutOf[6];
            }
            set
            {
                this._arrayDifMainCutOf[6] = (ushort)value;
            }
        }

        /// <summary>
        /// Iб2 - начало
        /// </summary>
        public double DTZIb2
        {
            get
            {
                return Measuring.GetConstraintOnly(this._arrayDifMainCutOf[7], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._arrayDifMainCutOf[7] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// К2 - тангенс
        /// </summary>
        public int DTZK2
        {
            get
            {
                return this._arrayDifMainCutOf[8];
            }
            set
            {
                this._arrayDifMainCutOf[8] = (ushort)value;
            }
        }

        /// <summary>
        /// I2/I1
        /// </summary>
        public int DTZI2I1
        {
            get
            {
                return this._arrayDifMainCutOf[9];
            }
            set
            {
                this._arrayDifMainCutOf[9] = (ushort)value;
            }
        }

        /// <summary>
        /// I2/I1 Блокировка
        /// </summary>
        public string I2I1Mode
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 10))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 10, bit);
            }
        }

        /// <summary>
        /// I2/I1 Перекрестная блокировка
        /// </summary>
        public string I2I1PerBlock
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 7))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 7, bit);
            }
        }

        /// <summary>
        /// I5/I1
        /// </summary>
        public int DTZI5I1
        {
            get
            {
                return this._arrayDifMainCutOf[10];
            }
            set
            {
                this._arrayDifMainCutOf[10] = (ushort)value;
            }
        }

        /// <summary>
        /// I5/I1 Блокировка
        /// </summary>
        public string I5I1Mode
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 9))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 9, bit);
            }
        }

        /// <summary>
        /// I5/I1 Перекрестная Блокировка
        /// </summary>
        public string I5I1PerBlock
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 6))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 6, bit);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DTZUROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DTZAPV
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[1] = Common.SetBit(this._arrayDifMainCutOf[1], 0, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DTZAVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[1] = Common.SetBit(this._arrayDifMainCutOf[1], 1, bit);
            }
        }

        /// <summary>
        /// Осциллограф
        /// </summary>
        public string DTZOsc
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLightOsc[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLightOsc.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[0] = Common.SetBit(this._arrayDifMainCutOf[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DTZOsc_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._arrayDifMainCutOf[1], 4, 5) >> 4];
            }
            set
            {
                this._arrayDifMainCutOf[1] = Common.SetBits(this._arrayDifMainCutOf[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }
        #endregion

        #region Диф. токовая отсечка без торможения
        /// <summary>
        /// Состояние
        /// </summary>
        public string DTOBTMode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_arrayDifMainCutOf[12], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._arrayDifMainCutOf[12], 0, 1)];


            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_arrayDifMainCutOf[12] = Common.SetBit(_arrayDifMainCutOf[12], 0, bit);
                this._arrayDifMainCutOf[12] = Common.SetBits(this._arrayDifMainCutOf[12], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DTOBTblocking
        {
            get
            {
                return Strings.Blocking[this._arrayDifMainCutOf[14]];
            }
            set
            {
                this._arrayDifMainCutOf[14] = (ushort)Strings.Blocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Уставка Iд>>
        /// </summary>
        public double DTOBTConstraint
        {
            get
            {
                return Measuring.GetConstraintOnly(this._arrayDifMainCutOf[15], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._arrayDifMainCutOf[15] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// Выдержка Tд>>
        /// </summary>
        public int DTOBTTimeEndurance
        {
            get
            {
                int val = Common.GetBits(this._arrayDifMainCutOf[16], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._arrayDifMainCutOf[16], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._arrayDifMainCutOf[16] = Common.SetBits(this._arrayDifMainCutOf[16], ind, 15);
                this._arrayDifMainCutOf[16] = Common.SetBits(this._arrayDifMainCutOf[16], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Ступень по мгновенным значениям
        /// </summary>
        public string DTOBTStepOnInstantValues
        {
            get
            {
                int index;
                bool e = Common.GetBit(this._arrayDifMainCutOf[12], 14);
                if (Common.GetBit(this._arrayDifMainCutOf[12], 3))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[12] = Common.SetBit(this._arrayDifMainCutOf[12], 3, bit);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DTOBTUROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[12], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[12] = Common.SetBit(this._arrayDifMainCutOf[12], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DTOBTAPV
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[13], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[13] = Common.SetBit(this._arrayDifMainCutOf[13], 0, bit);
            }
        }


        /// <summary>
        /// АВР
        /// </summary>
        public string DTOBTAVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[13], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[13] = Common.SetBit(this._arrayDifMainCutOf[13], 1, bit);
            }
        }

        /// <summary>
        /// Осциллограф
        /// </summary>
        public string DTOBTOsc
        {
            get
            {
                int index;
                if (Common.GetBit(this._arrayDifMainCutOf[12], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLightOsc[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLightOsc.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._arrayDifMainCutOf[12] = Common.SetBit(this._arrayDifMainCutOf[12], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DTOBTOsc_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._arrayDifMainCutOf[13], 4, 5) >> 4];
            }
            set
            {
                this._arrayDifMainCutOf[13] = Common.SetBits(this._arrayDifMainCutOf[13], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }
        #endregion

        #region Диф 0
        private ushort[] _Dif0Array = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(DIFFI0)) / 2];
        [XmlIgnore]
        public ushort[] Dif0Array
        {
            set { this._Dif0Array = value; }
            get { return this._Dif0Array; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string Dif0Mode
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(this._Dif0Array[0], 0, 1)];
            }
            set
            {
                this._Dif0Array[0] = Common.SetBits(this._Dif0Array[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Привязка к стороне Iд>
        /// </summary>
        public string Dif0Side
        {
            get
            {
                int index = Common.GetBits(this._Dif0Array[0], 4, 5) >> 4;
                return Strings.MeasuringLight[index];
            }
            set
            {
                this._Dif0Array[0] = Common.SetBits(this._Dif0Array[0], (ushort)Strings.MeasuringLight.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string Dif0UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._Dif0Array[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._Dif0Array[0] = Common.SetBit(this._Dif0Array[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string Dif0APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._Dif0Array[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._Dif0Array[1] = Common.SetBit(this._Dif0Array[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string Dif0AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._Dif0Array[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._Dif0Array[1] = Common.SetBit(this._Dif0Array[1], 1, bit);
            }
        }

        /// <summary>
        /// Осциллограф
        /// </summary>
        public string Dif0Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._Dif0Array[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLightOsc[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLightOsc.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._Dif0Array[0] = Common.SetBit(this._Dif0Array[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string Dif0Osc_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._Dif0Array[1], 4, 5) >> 4];
            }
            set
            {
                this._Dif0Array[1] = Common.SetBits(this._Dif0Array[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string Dif0blocking
        {
            get
            {
                return Strings.DiffBlocking[this._Dif0Array[2]];
            }
            set
            {
                this._Dif0Array[2] = (ushort)Strings.DiffBlocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Уставка Iд>
        /// </summary>
        public double Dif0Constraint
        {
            get
            {
                return Measuring.GetConstraintOnly(this._Dif0Array[3], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._Dif0Array[3] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// Выдержка Tд>
        /// </summary>
        public int Dif0TimeEndurance
        {
            get
            {
                int val = Common.GetBits(this._Dif0Array[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._Dif0Array[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._Dif0Array[4] = Common.SetBits(this._Dif0Array[4], ind, 15);
                this._Dif0Array[4] = Common.SetBits(this._Dif0Array[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }
        private string[][] _dif0Xml;
        public string[][] Dif0Xml
        {
            get { return this._dif0Xml; }
            set { this._dif0Xml = value; }
        }
        public string[] PrepareDif0ToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.Dif0Osc : this.Dif0Osc_1_11;

            return new string[]
                {
                    this.Dif0Mode, this.Dif0blocking, this.Dif0Constraint.ToString(), this.Dif0Side, this.Dif0TimeEndurance.ToString(), this.Dif0Ib1.ToString(), this.Dif0K1.ToString(), this.Dif0Ib2.ToString(), this.Dif0K2.ToString(), this.Dif0UROV, this.Dif0APV, this.Dif0AVR,
                    osc
                };


        }


        private string[][] _difIXml;
        public string[][] DifIXml
        {
            get { return this._difIXml; }
            set { this._difIXml = value; }
        }
        public string[] PrepareDifIToXml()
        {
            string logic = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifI_Logic : this.DifI_Logic_v_1_11;
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifI_Osc : this.DifI_Osc_v_1_11;

            return new string[]
                {
                    this.DifITy.ToString(), this.DifIBoolTy.ToString(), this.DifI_Mode, this.DifI_IConstraint.ToString(), this.DifI_Side, this.DifI_UStartConstraint.ToString(), this.DifI_UYNMode, this.DifI_Direction, this.DifI_Undirection, this.DifI_Charakteristic, this.DifI_T.ToString(), this.DifI_K.ToString(), this.DifI_Blocking, this.DifI_UROV, this.DifI_APV, this.DifI_AVR,
                    logic,
                    osc
                };


        }


        private string[][] _difI0Xml;
        public string[][] DifI0Xml
        {
            get { return this._difI0Xml; }
            set { this._difI0Xml = value; }
        }

        public string[] PrepareDifI0ToXml()
        {
            string dif0I0 = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifI0_IO : this.DifI0_IO_v_1_11;
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifI0_Osc : this.DifI0_Osc_v_1_11;

            return new string[]
                {
                    this.DifI0Ty.ToString(), this.DifI0BoolTy.ToString(), this.DifI0_Mode, this.DifI0_IConstraint.ToString(), this.DifI0_Side, this.DifI0_UStartConstraint.ToString(), this.DifI0_UYNMode, this.DifI0_Direction, this.DifI0_Undirection, this.DifI0_Charakteristic, this.DifI0_T.ToString(), this.DifI0_K.ToString(), this.DifI0_Blocking, this.DifI0_UROV, this.DifI0_APV, this.DifI0_AVR,
                    dif0I0,
                    osc
                };
        }

        /// <summary>
        /// Iб1 - начало
        /// </summary>
        public double Dif0Ib1
        {
            get
            {
                return Measuring.GetConstraintOnly(this._Dif0Array[5], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._Dif0Array[5] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// К1 - угол наклона
        /// </summary>
        public int Dif0K1
        {
            get
            {
                return this._Dif0Array[6];
            }
            set
            {
                this._Dif0Array[6] = (ushort)value;
            }
        }

        /// <summary>
        /// Iб2 - начало
        /// </summary>
        public double Dif0Ib2
        {
            get
            {
                return Measuring.GetConstraintOnly(this._Dif0Array[7], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._Dif0Array[7] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// К2 - тангенс
        /// </summary>
        public int Dif0K2
        {
            get
            {
                return this._Dif0Array[8];
            }
            set
            {
                this._Dif0Array[8] = (ushort)value;
            }
        }

        #endregion

        #region Защиты I
        private ushort[] _DifIArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2];
        [XmlIgnore]
        public ushort[] DifIArray
        {
            set
            {
                this._DifIArray = value;
            }
            get { return this._DifIArray; }
        }

        /// <summary>
        /// ввод уставки на ускорение
        /// </summary>
        public bool DifIBoolTy
        {
            get { return Common.GetBit(this._DifIArray[0], 13); }
            set { this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 13, value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        public int DifITy
        {
            get { return ValuesConverterCommon.GetWaitTime(this._DifIArray[7]); }
            set { this._DifIArray[7] = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifI_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifIArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._DifIArray[0], 0, 1)];

            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_DifIArray[0] = Common.SetBit(_DifIArray[0], 0, bit);
                this._DifIArray[0] = Common.SetBits(this._DifIArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Привязка к стороне I
        /// </summary>
        public string DifI_Side
        {
            get
            {
                int index = Common.GetBits(this._DifIArray[0], 4, 5) >> 4;
                return Strings.MeasuringLight[index];
            }
            set
            {
                this._DifIArray[0] = Common.SetBits(this._DifIArray[0], (ushort)Strings.MeasuringLight.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// Направление
        /// </summary>
        public string DifI_Direction
        {
            get
            {
                int index = Common.GetBits(this._DifIArray[0], 6, 7) >> 6;
                return Strings.BusDirection[index];
            }
            set
            {
                this._DifIArray[0] = Common.SetBits(this._DifIArray[0], (ushort)Strings.BusDirection.IndexOf(value), 6, 7);
            }
        }


        /// <summary>
        /// Нет направления
        /// </summary>
        public string DifI_Undirection
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 8))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.UnDirection[index];
            }
            set
            {
                bool bit;
                if (Strings.UnDirection.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 8, bit);
            }
        }

        /// <summary>
        /// Логика
        /// </summary>
        public string DifI_Logic
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 11))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.TokParameter[index];
            }
            set
            {
                bool bit;
                if (Strings.TokParameter.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 11, bit);
            }
        }

        /// <summary>
        /// Логика 1.11
        /// </summary>
        public string DifI_Logic_v_1_11
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.TokParameter[index];
            }
            set
            {
                bool bit;
                if (Strings.TokParameter.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        public string DifI_Charakteristic
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 12))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.Characteristic[index];
            }
            set
            {
                bool bit;
                if (Strings.Characteristic.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 12, bit);
            }
        }

        /// <summary>
        /// Ускорение - Есть/Нет
        /// </summary>
        public string DifI_TyYNMode
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 13))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 13, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifI_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[1] = Common.SetBit(this._DifIArray[1], 0, bit);
            }
        }


        /// <summary>
        /// АВР
        /// </summary>
        public string DifI_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[1] = Common.SetBit(this._DifIArray[1], 1, bit);
            }
        }


        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifI_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 2, bit);
            }
        }

        /// <summary>
        /// Uпуск - Есть/Нет
        /// </summary>
        public string DifI_UYNMode
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifIArray[0], 3))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 3, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifI_Blocking
        {
            get
            {
                return Strings.Blocking[this._DifIArray[2]];
            }
            set
            {
                this._DifIArray[2] = (ushort)Strings.Blocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограф
        /// </summary>
        public string DifI_Osc
        {
            get
            {
                int index;

                if (Common.GetBit(this._DifIArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLightOsc[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLightOsc.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifIArray[0] = Common.SetBit(this._DifIArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        //public string DifI_Osc_v_1_11
        //{
        //    get
        //    {
        //        int index;
        //        if (Common.GetBit(_DifIArray[0], 11))
        //        {
        //            index = 1;
        //        }
        //        else
        //        {
        //            index = 0;
        //        }
        //        return Strings.ModesLightOsc[index];
        //    }
        //    set
        //    {
        //        bool bit;
        //        if (Strings.ModesLightOsc.IndexOf(value) == 1)
        //        {
        //            bit = true;
        //        }
        //        else
        //        {
        //            bit = false;
        //        }
        //        _DifIArray[0] = Common.SetBit(_DifIArray[0], 11, bit);
        //    }
        //}

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifI_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifIArray[1], 4, 5) >> 4];
            }
            set
            {
                this._DifIArray[1] = Common.SetBits(this._DifIArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// Уставка I
        /// </summary>
        public double DifI_IConstraint
        {
            get
            {
                return Measuring.GetConstraintOnly(this._DifIArray[3], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._DifIArray[3] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// Уставка Uпуск
        /// </summary>
        public double DifI_UStartConstraint
        {
            get
            {
                return Measuring.GetTH(this._DifIArray[6]);
            }
            set
            {
                double val = Measuring.SetTH(value);
                if (val > 65535)
                {
                    val = 65535;
                }
                this._DifIArray[6] = (ushort)val;
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifI_T
        {
            get
            {
                int val = Common.GetBits(this._DifIArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifIArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifIArray[4] = Common.SetBits(this._DifIArray[4], ind, 15);
                this._DifIArray[4] = Common.SetBits(this._DifIArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Коэфф. зависимой хар-ки
        /// </summary>
        public double DifI_K
        {
            get
            {
                return this._DifIArray[5];
            }
            set
            {
                this._DifIArray[5] = (ushort)value;
            }
        }

        /// <summary>
        /// Время ускорения
        /// </summary>
        public double DifI_Ty
        {
            get
            {
                return Measuring.GetConstraintOnly(this._DifIArray[7], ConstraintKoefficient.K_4001);
            }
            set
            {
                this._DifIArray[7] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
            }
        }

        #endregion

        #region Защиты I0

        private ushort[] _DifI0Array = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2];
        [XmlIgnore]
        public ushort[] DifI0Array
        {
            set
            {
                this._DifI0Array = value;
            }
            get { return this._DifI0Array; }
        }

        /// <summary>
        /// ввод уставки на ускорение
        /// </summary>
        public bool DifI0BoolTy
        {
            get { return Common.GetBit(this._DifI0Array[0], 13); }
            set { this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 13, value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        public int DifI0Ty
        {
            get { return ValuesConverterCommon.GetWaitTime(this._DifI0Array[7]); }
            set { this._DifI0Array[7] = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifI0_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifI0Array[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._DifI0Array[0], 0, 1)];
            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_DifI0Array[0] = Common.SetBit(_DifI0Array[0], 0, bit);
                this._DifI0Array[0] = Common.SetBits(this._DifI0Array[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Привязка к стороне I
        /// </summary>
        public string DifI0_Side
        {
            get
            {
                int index = Common.GetBits(this._DifI0Array[0], 4, 5) >> 4;
                return Strings.MeasuringLight[index];
            }
            set
            {
                this._DifI0Array[0] = Common.SetBits(this._DifI0Array[0], (ushort)Strings.MeasuringLight.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// Направление
        /// </summary>
        public string DifI0_Direction
        {
            get
            {
                int index = Common.GetBits(this._DifI0Array[0], 6, 7) >> 6;
                return Strings.BusDirection[index];
            }
            set
            {
                this._DifI0Array[0] = Common.SetBits(this._DifI0Array[0], (ushort)Strings.BusDirection.IndexOf(value), 6, 7);
            }
        }


        /// <summary>
        /// Нет направления
        /// </summary>
        public string DifI0_Undirection
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 8))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.UnDirection[index];
            }
            set
            {
                bool bit;
                if (Strings.UnDirection.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 8, bit);
            }
        }

        /// <summary>
        /// IO
        /// </summary>
        public string DifI0_IO
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 11))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.I0Modes[index];
            }
            set
            {
                bool bit;
                if (Strings.I0Modes.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 11, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifI0_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[1] = Common.SetBit(this._DifI0Array[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string DifI0_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[1] = Common.SetBit(this._DifI0Array[1], 1, bit);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifI0_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 2, bit);
            }
        }

        /// <summary>
        /// IO 1.11
        /// </summary>
        public string DifI0_IO_v_1_11
        {
            get
            {
                int index = Common.GetBits(this._DifI0Array[0], 14, 15) >> 14;

                return Strings.I0Modes[index];
            }
            set
            {
                this._DifI0Array[0] = Common.SetBits(this._DifI0Array[0], (ushort)Strings.I0Modes.IndexOf(value), 14, 15);
            }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        public string DifI0_Charakteristic
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 12))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.Characteristic[index];
            }
            set
            {
                bool bit;
                if (Strings.Characteristic.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 12, bit);
            }
        }

        /// <summary>
        /// Ускорение - Есть/Нет
        /// </summary>
        public string DifI0_TyYNMode
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 13))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 13, bit);
            }
        }

        /// <summary>
        /// Uпуск - Есть/Нет
        /// </summary>
        public string DifI0_UYNMode
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifI0Array[0], 3))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 3, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifI0_Blocking
        {
            get
            {
                return Strings.Blocking[this._DifI0Array[2]];
            }
            set
            {
                this._DifI0Array[2] = (ushort)Strings.Blocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограф
        /// </summary>
        public string DifI0_Osc
        {
            get
            {
                int index;

                if (Common.GetBit(this._DifI0Array[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLightOsc[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLightOsc.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifI0Array[0] = Common.SetBit(this._DifI0Array[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        //public string DifI0_Osc_v_1_11
        //{
        //    get
        //    {
        //        int index;
        //        if (Common.GetBit(_DifI0Array[0], 11))
        //        {
        //            index = 1;
        //        }
        //        else
        //        {
        //            index = 0;
        //        }
        //        return Strings.ModesLightOsc[index];
        //    }
        //    set
        //    {
        //        bool bit;
        //        if (Strings.ModesLightOsc.IndexOf(value) == 1)
        //        {
        //            bit = true;
        //        }
        //        else
        //        {
        //            bit = false;
        //        }
        //        _DifI0Array[0] = Common.SetBit(_DifI0Array[0], 11, bit);
        //    }
        //}

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifI0_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifI0Array[1], 4, 5) >> 4];
            }
            set
            {
                this._DifI0Array[1] = Common.SetBits(this._DifI0Array[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// Уставка I
        /// </summary>
        public double DifI0_IConstraint
        {
            get
            {
                return Measuring.GetConstraintOnly(this._DifI0Array[3], ConstraintKoefficient.K_4000);
            }
            set
            {
                this._DifI0Array[3] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        /// <summary>
        /// Уставка Uпуск
        /// </summary>
        public double DifI0_UStartConstraint
        {
            get
            {
                return Measuring.GetTH(this._DifI0Array[6]);
            }
            set
            {
                this._DifI0Array[6] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifI0_T
        {
            get
            {
                int val = Common.GetBits(this._DifI0Array[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifI0Array[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifI0Array[4] = Common.SetBits(this._DifI0Array[4], ind, 15);
                this._DifI0Array[4] = Common.SetBits(this._DifI0Array[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Коэфф. зависимой хар-ки
        /// </summary>
        public double DifI0_K
        {
            get
            {
                return this._DifI0Array[5];
            }
            set
            {
                this._DifI0Array[5] = (ushort)value;
            }
        }

        /// <summary>
        /// Время ускорения
        /// </summary>
        public double DifI0_Ty
        {
            get
            {
                return Measuring.GetConstraintOnly(this._DifI0Array[7], ConstraintKoefficient.K_4001);
            }
            set
            {
                this._DifI0Array[7] = (ushort)Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);
            }
        }

        #endregion

        #region Защиты U>

        private string[][] _difUBXml;
        public string[][] DifUBXml
        {
            get { return this._difUBXml; }
            set { this._difUBXml = value; }
        }

        public string[] PrepareDifUBToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifUB_Osc : this.DifUB_Osc_v_1_11;
            return new string[]
                {
                    this.DifUB_Mode, this.DifUB_Type, this.DifUB_Usr.ToString(), this.DifUB_Tsr.ToString(), this.DifUB_Tvz.ToString(), this.DifUB_Uvz.ToString(), this.DifUB_UvzYN.ToString(), this.DifUB_BlockingUM.ToString(), this.DifUB_Blocking.ToString(), this.DifUB_Urov.ToString(), this.DifUB_APV.ToString(), this.DifUB_AVR, this.DifUB_APVRet, this.DifUB_Sbros,
                    osc
                };
        }


        private ushort[] _DifUBArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
        [XmlIgnore]
        public ushort[] DifUBArray
        {
            set
            {
                this._DifUBArray = value;
            }
            get { return this._DifUBArray; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifUB_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifUBArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._DifUBArray[0], 0, 1)];

            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_DifUBArray[0] = Common.SetBit(_DifUBArray[0], 0, bit);
                this._DifUBArray[0] = Common.SetBits(this._DifUBArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Логика
        /// </summary>
        public string DifUB_Type
        {
            get
            {
                int index = Common.GetBits(this._DifUBArray[0], 5, 6, 7) >> 5;
                return Strings.UType[index];
            }
            set
            {
                this._DifUBArray[0] = Common.SetBits(this._DifUBArray[0], (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }


        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double DifUB_Usr
        {
            get
            {
                return Measuring.GetTH(this._DifUBArray[3]);
            }
            set
            {
                this._DifUBArray[3] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifUB_Tsr
        {
            get
            {
                int val = Common.GetBits(this._DifUBArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifUBArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifUBArray[4] = Common.SetBits(this._DifUBArray[4], ind, 15);
                this._DifUBArray[4] = Common.SetBits(this._DifUBArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double DifUB_Tvz
        {
            get
            {
                int val = Common.GetBits(this._DifUBArray[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifUBArray[6], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifUBArray[6] = Common.SetBits(this._DifUBArray[6], ind, 15);
                this._DifUBArray[6] = Common.SetBits(this._DifUBArray[6], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Время возврата
        /// </summary>
        public double DifUB_Uvz
        {
            get
            {
                return Measuring.GetTH(this._DifUBArray[5]);
            }
            set
            {
                this._DifUBArray[5] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// UВремя возврата - Есть/Нет
        /// </summary>
        public bool DifUB_UvzYN
        {
            get { return Common.GetBit(this._DifUBArray[0], 3); }
            set { this._DifUBArray[0] = Common.SetBit(this._DifUBArray[0], 3, value); }
        }

        /// <summary>
        /// Блокировка по U 5в
        /// </summary>
        public string DifUB_BlockingUM
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[0], 4))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[0] = Common.SetBit(this._DifUBArray[0], 4, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifUB_Blocking
        {
            get
            {
                return Strings.DiffBlocking[this._DifUBArray[2]];
            }
            set
            {
                this._DifUBArray[2] = (ushort)Strings.DiffBlocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограмма
        /// </summary>
        public string DifUB_Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[0] = Common.SetBit(this._DifUBArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifUB_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifUBArray[1], 4, 5) >> 4];
            }
            set
            {
                this._DifUBArray[1] = Common.SetBits(this._DifUBArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifUB_Urov
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[0] = Common.SetBit(this._DifUBArray[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifUB_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[1] = Common.SetBit(this._DifUBArray[1], 0, bit);
            }
        }


        /// <summary>
        /// АВР
        /// </summary>
        public string DifUB_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[1] = Common.SetBit(this._DifUBArray[1], 1, bit);
            }
        }

        /// <summary>
        /// АПВ по возврату
        /// </summary>
        public string DifUB_APVRet
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[1], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[1] = Common.SetBit(this._DifUBArray[1], 2, bit);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string DifUB_Sbros
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUBArray[0], 15))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUBArray[0] = Common.SetBit(this._DifUBArray[0], 15, bit);
            }
        }

        #endregion

        #region Защиты U<
        private string[][] _difUMXml;
        public string[][] DifUMXml
        {
            get { return this._difUMXml; }
            set { this._difUMXml = value; }
        }

        public string[] PrepareDifUMToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifUM_Osc : this.DifUM_Osc_v_1_11;
            return new string[]
                {
                    this.DifUM_Mode, this.DifUM_Type, this.DifUM_Usr.ToString(), this.DifUM_Tsr.ToString(), this.DifUM_Tvz.ToString(), this.DifUM_Uvz.ToString(), this.DifUM_UvzYN.ToString(), this.DifUM_BlockingUM, this.DifUM_Blocking, this.DifUM_UROV, this.DifUM_APV, this.DifUM_AVR, this.DifUM_APVRet, this.DifUM_Sbros,
                    osc
                };
        }

        private ushort[] _DifUMArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
        [XmlIgnore]
        public ushort[] DifUMArray
        {
            set
            {
                this._DifUMArray = value;
            }
            get { return this._DifUMArray; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifUM_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifUMArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._DifUMArray[0], 0, 1)];

            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_DifUMArray[0] = Common.SetBit(_DifUMArray[0], 0, bit);
                this._DifUMArray[0] = Common.SetBits(this._DifUMArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Логика
        /// </summary>
        public string DifUM_Type
        {
            get
            {
                int index = Common.GetBits(this._DifUMArray[0], 5, 6, 7) >> 5;
                return Strings.UMType[index];
            }
            set
            {
                this._DifUMArray[0] = Common.SetBits(this._DifUMArray[0], (ushort)Strings.UMType.IndexOf(value), 5, 6, 7);
            }
        }


        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double DifUM_Usr
        {
            get
            {
                return Measuring.GetTH(this._DifUMArray[3]);
            }
            set
            {
                this._DifUMArray[3] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifUM_Tsr
        {
            get
            {
                int val = Common.GetBits(this._DifUMArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifUMArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifUMArray[4] = Common.SetBits(this._DifUMArray[4], ind, 15);
                this._DifUMArray[4] = Common.SetBits(this._DifUMArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double DifUM_Tvz
        {
            get
            {
                int val = Common.GetBits(this._DifUMArray[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifUMArray[6], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifUMArray[6] = Common.SetBits(this._DifUMArray[6], ind, 15);
                this._DifUMArray[6] = Common.SetBits(this._DifUMArray[6], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Время возврата
        /// </summary>
        public double DifUM_Uvz
        {
            get
            {
                return Measuring.GetTH(this._DifUMArray[5]);
            }
            set
            {
                this._DifUMArray[5] = (ushort)Measuring.SetTH(value);
            }

        }

        /// <summary>
        /// UВремя возврата - Есть/Нет
        /// </summary>
        public bool DifUM_UvzYN
        {
            get { return Common.GetBit(this._DifUMArray[0], 3); }
            set { this._DifUMArray[0] = Common.SetBit(this._DifUMArray[0], 3, value); }
        }

        /// <summary>
        /// Блокировка по U 5в
        /// </summary>
        public string DifUM_BlockingUM
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[0], 4))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[0] = Common.SetBit(this._DifUMArray[0], 4, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifUM_Blocking
        {
            get
            {
                return Strings.DiffBlocking[this._DifUMArray[2]];
            }
            set
            {
                this._DifUMArray[2] = (ushort)Strings.DiffBlocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограмма
        /// </summary>
        public string DifUM_Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[0] = Common.SetBit(this._DifUMArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifUM_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifUMArray[1], 4, 5) >> 4];
            }
            set
            {
                this._DifUMArray[1] = Common.SetBits(this._DifUMArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifUM_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[0] = Common.SetBit(this._DifUMArray[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifUM_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[1] = Common.SetBit(this._DifUMArray[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string DifUM_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[1] = Common.SetBit(this._DifUMArray[1], 1, bit);
            }
        }


        /// <summary>
        /// АПВ по возврату
        /// </summary>
        public string DifUM_APVRet
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[1], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[1] = Common.SetBit(this._DifUMArray[1], 2, bit);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string DifUM_Sbros
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifUMArray[0], 15))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifUMArray[0] = Common.SetBit(this._DifUMArray[0], 15, bit);
            }
        }
        #endregion

        #region Защиты F>
        private string[][] _difFBXml;
        public string[][] DifFBXml
        {
            get { return this._difFBXml; }
            set { this._difFBXml = value; }
        }

        public string[] PrepareDifFBToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifFB_Osc : this.DifFB_Osc_v_1_11;
            return new string[]
                {
                    this.DifFB_Mode, this.DifFB_Usr.ToString(), this.DifFB_Tsr.ToString(), this.DifFB_Tvz.ToString(), this.DifFB_Uvz.ToString(), this.DifFB_UvzYN.ToString(), this.DifFB_Blocking, this.DifFB_UROV, this.DifFB_APV, this.DifFB_AVR, this.DifFB_APVRet, this.DifFB_Sbros,
                    osc
                };
        }


        private ushort[] _DifFBArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
        [XmlIgnore]
        public ushort[] DifFBArray
        {
            set
            {
                this._DifFBArray = value;
            }
            get { return this._DifFBArray; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifFB_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifFBArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                return Strings.ModesLightMode[Common.GetBits(this._DifFBArray[0], 0, 1)];
            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                this._DifFBArray[0] = Common.SetBits(this._DifFBArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Логика
        /// </summary>
        public string DifFB_Type
        {
            get
            {
                int index = Common.GetBits(this._DifFBArray[0], 5, 6, 7) >> 8;
                return Strings.UType[index];
            }
            set
            {
                this._DifFBArray[0] = Common.SetBits(this._DifFBArray[0], (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }


        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double DifFB_Usr
        {
            get
            {
                return Measuring.GetTH(this._DifFBArray[3]);
            }
            set
            {
                this._DifFBArray[3] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifFB_Tsr
        {
            get
            {
                int val = Common.GetBits(this._DifFBArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifFBArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifFBArray[4] = Common.SetBits(this._DifFBArray[4], ind, 15);
                this._DifFBArray[4] = Common.SetBits(this._DifFBArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double DifFB_Tvz
        {
            get
            {
                int val = Common.GetBits(this._DifFBArray[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifFBArray[6], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifFBArray[6] = Common.SetBits(this._DifFBArray[6], ind, 15);
                this._DifFBArray[6] = Common.SetBits(this._DifFBArray[6], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Время возврата
        /// </summary>
        public double DifFB_Uvz
        {
            get
            {
                return Measuring.GetTH(this._DifFBArray[5]);
            }
            set
            {
                this._DifFBArray[5] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// UВремя возврата - Есть/Нет
        /// </summary>
        public bool DifFB_UvzYN
        {
            get { return Common.GetBit(this._DifFBArray[0], 3); }
            set { this._DifFBArray[0] = Common.SetBit(this._DifFBArray[0], 3, value); }
        }

        /// <summary>
        /// Блокировка по U 5в
        /// </summary>
        public string DifFB_BlockingUM
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[0], 4))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[0] = Common.SetBit(this._DifFBArray[0], 4, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifFB_Blocking
        {
            get
            {
                return Strings.DiffBlocking[this._DifFBArray[2]];
            }
            set
            {
                this._DifFBArray[2] = (ushort)Strings.DiffBlocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограмма
        /// </summary>
        public string DifFB_Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[0] = Common.SetBit(this._DifFBArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifFB_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifFBArray[1], 4, 5) >> 4];
            }
            set
            {
                this._DifFBArray[1] = Common.SetBits(this._DifFBArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifFB_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[0] = Common.SetBit(this._DifFBArray[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifFB_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[1] = Common.SetBit(this._DifFBArray[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string DifFB_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[1] = Common.SetBit(this._DifFBArray[1], 1, bit);
            }
        }


        /// <summary>
        /// АПВ по возврату
        /// </summary>
        public string DifFB_APVRet
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[1], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[1] = Common.SetBit(this._DifFBArray[1], 2, bit);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string DifFB_Sbros
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFBArray[0], 15))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFBArray[0] = Common.SetBit(this._DifFBArray[0], 15, bit);
            }
        }
        #endregion

        #region Защиты F<
        private string[][] _difFMXml;
        public string[][] DifFMXml
        {
            get { return this._difFMXml; }
            set { this._difFMXml = value; }
        }

        public string[] PrepareDifFMToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.DifFM_Osc : this.DifFM_Osc_v_1_11;
            return new string[]
                {
                    this.DifFM_Mode, this.DifFM_Usr.ToString(), this.DifFM_Tsr.ToString(), this.DifFM_Tvz.ToString(), this.DifFM_Uvz.ToString(), this.DifFM_UvzYN.ToString(), this.DifFM_Blocking.ToString(), this.DifFM_UROV, this.DifFM_APV, this.DifFM_AVR, this.DifFM_APVRet, this.DifFM_Sbros,
                    osc
                };
        }

        private ushort[] _DifFMArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
        [XmlIgnore]
        public ushort[] DifFMArray
        {
            set
            {
                this._DifFMArray = value;
            }
            get { return this._DifFMArray; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string DifFM_Mode
        {
            get
            {
                //int index;
                //if (Common.GetBit(_DifFMArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._DifFMArray[0], 0, 1)];
            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_DifFMArray[0] = Common.SetBit(_DifFMArray[0], 0, bit);
                this._DifFMArray[0] = Common.SetBits(this._DifFMArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Логика
        /// </summary>
        public string DifFM_Type
        {
            get
            {
                int index = Common.GetBits(this._DifFMArray[0], 5, 6, 7) >> 5;
                return Strings.UMType[index];
            }
            set
            {
                this._DifFMArray[0] = Common.SetBits(this._DifFMArray[0], (ushort)Strings.UMType.IndexOf(value), 5, 6, 7);
            }
        }


        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double DifFM_Usr
        {
            get
            {
                return Measuring.GetTH(this._DifFMArray[3]);
            }
            set
            {
                this._DifFMArray[3] = (ushort)Measuring.SetTH(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double DifFM_Tsr
        {
            get
            {
                int val = Common.GetBits(this._DifFMArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifFMArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifFMArray[4] = Common.SetBits(this._DifFMArray[4], ind, 15);
                this._DifFMArray[4] = Common.SetBits(this._DifFMArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double DifFM_Tvz
        {
            get
            {
                int val = Common.GetBits(this._DifFMArray[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._DifFMArray[6], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._DifFMArray[6] = Common.SetBits(this._DifFMArray[6], ind, 15);
                this._DifFMArray[6] = Common.SetBits(this._DifFMArray[6], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Время возврата
        /// </summary>
        public double DifFM_Uvz
        {
            get
            {
                return Measuring.GetTH(this._DifFMArray[5]);
            }
            set
            {
                this._DifFMArray[5] = (ushort)Measuring.SetTH(value);
            }

        }

        /// <summary>
        /// UВремя возврата - Есть/Нет
        /// </summary>
        public bool DifFM_UvzYN
        {
            get { return Common.GetBit(this._DifFMArray[0], 3); }
            set { this._DifFMArray[0] = Common.SetBit(this._DifFMArray[0], 3, value); }
        }

        /// <summary>
        /// Блокировка по U 5в
        /// </summary>
        public string DifFM_BlockingUM
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[0], 4))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[0] = Common.SetBit(this._DifFMArray[0], 4, bit);
            }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string DifFM_Blocking
        {
            get
            {
                return Strings.DiffBlocking[this._DifFMArray[2]];
            }
            set
            {
                this._DifFMArray[2] = (ushort)Strings.DiffBlocking.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограмма
        /// </summary>
        public string DifFM_Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[0] = Common.SetBit(this._DifFMArray[0], 14, bit);
            }
        }

        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string DifFM_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._DifFMArray[1], 4, 5) >> 4];
            }
            set
            {
                this._DifFMArray[1] = Common.SetBits(this._DifFMArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string DifFM_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[0] = Common.SetBit(this._DifFMArray[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string DifFM_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[1] = Common.SetBit(this._DifFMArray[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string DifFM_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[1] = Common.SetBit(this._DifFMArray[1], 1, bit);
            }
        }

        /// <summary>
        /// АПВ по возврату
        /// </summary>
        public string DifFM_APVRet
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[1], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[1] = Common.SetBit(this._DifFMArray[1], 2, bit);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string DifFM_Sbros
        {
            get
            {
                int index;
                if (Common.GetBit(this._DifFMArray[0], 15))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._DifFMArray[0] = Common.SetBit(this._DifFMArray[0], 15, bit);
            }
        }
        #endregion

        #region Защиты внешние

        public class ExtClass : IXmlSerializable
        {
            private string[] _str;

            public ExtClass() { }
            public ExtClass(string[] str)
            {
                this._str = str;
            }
            public System.Xml.Schema.XmlSchema GetSchema()
            {
                throw new NotImplementedException();
            }

            public void ReadXml(XmlReader reader)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlWriter writer)
            {
                for (int i = 0; i < this._str.Length; i++)
                {

                    writer.WriteElementString("Pos" + i, this._str[i]);
                }
            }
        }
        private ExtClass[] _extXml;
        public ExtClass[] ExtXml
        {
            get { return this._extXml; }
            set { this._extXml = value; }
        }

        public string[] PrepareExtToXml()
        {
            string osc = Common.VersionConverter(DeviceVersion) < 1.11 ? this.ExtDif_Osc : this.ExtDif_Osc_v_1_11;
            return new string[]
                {
                    this.ExtDif_Modes, this.ExtDif_Srab, this.ExtDif_Tsr.ToString(), this.ExtDif_Tvz.ToString(), this.ExtDif_Vozvr, this.ExtDif_VozvrYN.ToString(), this.ExtDif_Blocking, this.ExtDif_UROV, this.ExtDif_APV, this.ExtDif_AVR, this.ExtDif_APVRet, this.ExtDif_Sbros,
                    osc
                };





        }
        private ushort[] _ExtDifArray = new ushort[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2];
        [XmlIgnore]
        public ushort[] ExtDifArray
        {
            set
            {
                this._ExtDifArray = value;
            }
            get { return this._ExtDifArray; }
        }

        /// <summary>
        /// Состояние
        /// </summary>
        public string ExtDif_Modes
        {
            get
            {
                //int index;
                //if (Common.GetBit(_ExtDifArray[0], 0))
                //{
                //    index = 1;
                //}
                //else
                //{
                //    index = 0;
                //}
                //return Strings.ModesLight[index];
                return Strings.ModesLightMode[Common.GetBits(this._ExtDifArray[0], 0, 1)];

            }
            set
            {
                //bool bit;
                //if (Strings.ModesLight.IndexOf(value) == 1)
                //{
                //    bit = true;
                //}
                //else
                //{
                //    bit = false;
                //}
                //_ExtDifArray[0] = Common.SetBit(_ExtDifArray[0], 0, bit);
                this._ExtDifArray[0] = Common.SetBits(this._ExtDifArray[0], (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public string ExtDif_Srab
        {
            get
            {
                return Strings.SygnalSrab[this._ExtDifArray[2 + 1]];
            }
            set
            {
                this._ExtDifArray[2 + 1] = (ushort)Strings.SygnalSrab.IndexOf(value);
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        public double ExtDif_Tsr
        {
            get
            {
                int val = Common.GetBits(this._ExtDifArray[4], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._ExtDifArray[4], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._ExtDifArray[4] = Common.SetBits(this._ExtDifArray[4], ind, 15);
                this._ExtDifArray[4] = Common.SetBits(this._ExtDifArray[4], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }


        /// <summary>
        /// Время возврата
        /// </summary>
        public double ExtDif_Tvz
        {
            get
            {
                int val = Common.GetBits(this._ExtDifArray[6], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                int ind = Common.GetBits(this._ExtDifArray[6], 15);

                if (ind == 0)
                {
                    val *= 10;
                }
                else
                {
                    val *= 100;
                }

                return val;
            }
            set
            {
                ushort ind = 0;
                ushort val = (ushort)value;
                if (value > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(value / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (value > 32767 && value < 32767 * 10)
                {
                    val = (ushort)(value / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                this._ExtDifArray[6] = Common.SetBits(this._ExtDifArray[6], ind, 15);
                this._ExtDifArray[6] = Common.SetBits(this._ExtDifArray[6], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        /// <summary>
        /// Уставка возврата
        /// </summary>
        public string ExtDif_Vozvr
        {
            get
            {
                return Strings.SygnalSrab[this._ExtDifArray[5]];
            }
            set
            {
                this._ExtDifArray[5] = (ushort)Strings.SygnalSrab.IndexOf(value);

            }
        }


        /// <summary>
        /// UВремя возврата - Есть/Нет
        /// </summary>
        public bool ExtDif_VozvrYN
        {
            get { return Common.GetBit(this._ExtDifArray[0], 3); }
            set { this._ExtDifArray[0] = Common.SetBit(this._ExtDifArray[0], 3, value); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        public string ExtDif_Blocking
        {
            get
            {
                return Strings.SygnalSrab[this._ExtDifArray[2]];
            }
            set
            {
                this._ExtDifArray[2] = (ushort)Strings.SygnalSrab.IndexOf(value);
            }
        }

        /// <summary>
        /// Осциллограмма
        /// </summary>
        public string ExtDif_Osc
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[0], 14))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[0] = Common.SetBit(this._ExtDifArray[0], 14, bit);
            }
        }


        /// <summary>
        /// Осциллограф 1.11
        /// </summary>
        public string ExtDif_Osc_v_1_11
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(this._ExtDifArray[1], 4, 5) >> 4];
            }
            set
            {
                this._ExtDifArray[1] = Common.SetBits(this._ExtDifArray[1], (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        public string ExtDif_UROV
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[0], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[0] = Common.SetBit(this._ExtDifArray[0], 2, bit);
            }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        public string ExtDif_APV
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[1], 0))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[1] = Common.SetBit(this._ExtDifArray[1], 0, bit);
            }
        }

        /// <summary>
        /// АВР
        /// </summary>
        public string ExtDif_AVR
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[1], 1))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[1] = Common.SetBit(this._ExtDifArray[1], 1, bit);
            }
        }

        /// <summary>
        /// АПВ по возврату
        /// </summary>
        public string ExtDif_APVRet
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[1], 2))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.ModesLight[index];
            }
            set
            {
                bool bit;
                if (Strings.ModesLight.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[1] = Common.SetBit(this._ExtDifArray[1], 2, bit);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public string ExtDif_Sbros
        {
            get
            {
                int index;
                if (Common.GetBit(this._ExtDifArray[0], 15))
                {
                    index = 1;
                }
                else
                {
                    index = 0;
                }
                return Strings.YesNo[index];
            }
            set
            {
                bool bit;
                if (Strings.YesNo.IndexOf(value) == 1)
                {
                    bit = true;
                }
                else
                {
                    bit = false;
                }
                this._ExtDifArray[0] = Common.SetBit(this._ExtDifArray[0], 15, bit);
            }
        }
        #endregion

        #endregion

        #region Осциллограф

        #region Конфигурация
        public string[] OscXml
        {
            get
            {
                return new string[]
                    {
                        Strings.OscopeFixation1_11[this.OscConfig1_11[0]], this.OscConfig1_11[1].ToString(), this.OscConfig1_11[2].ToString(),

                        Strings.Sygnal[this.OscConfig1_11[3]],
                        Strings.Sygnal[this.OscConfig1_11[4]],
                        Strings.Sygnal[this.OscConfig1_11[5]],
                        Strings.Sygnal[this.OscConfig1_11[6]],
                        Strings.Sygnal[this.OscConfig1_11[7]],
                        Strings.Sygnal[this.OscConfig1_11[8]],
                        Strings.Sygnal[this.OscConfig1_11[9]],
                        Strings.Sygnal[this.OscConfig1_11[10]],
                    };
            }
            set { }
        }
        [XmlIgnore]
        public ushort[] OscConfig1_11
        {
            get { return this._oscope.Value; }
            set { this._oscope.Value = value; }
        }

        public string OscConfig
        {
            get
            {
                return Strings.OscConfig[this._oscope.Value[0]];
            }
            set
            {
                this._oscope.Value[0] = (ushort)Strings.OscConfig.IndexOf(value);
            }
        }
        #endregion

        #region Журнал
        ushort[] _oscilloscopeJournalRecord;
        public ushort[] OscJournalValues
        {
            get
            {
                return this._oscilloscopeJournalReadSlot.Value;
            }
        }

        public bool OscExist
        {
            get
            {
                if (this._oscilloscopeJournalReadSlot.Value[19] != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        [XmlIgnore]
        public ushort[] OscJournalRecord
        {
            get
            {
                return this._oscilloscopeJournalRecord;
            }
            set
            {
                this._oscilloscopeJournalRecord = value;
            }
        }

        public string OscYear
        {
            get
            {
                if (this._oscilloscopeJournalRecord[2] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[2].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[2].ToString();
                }
            }
        }

        public string OscMonth
        {
            get
            {
                if (this._oscilloscopeJournalRecord[1] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[1].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[1].ToString();
                }
            }
        }

        public string OscDay
        {
            get
            {
                if (this._oscilloscopeJournalRecord[0] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[0].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[0].ToString();
                }
            }
        }

        public string OscHours
        {
            get
            {
                if (this._oscilloscopeJournalRecord[3] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[3].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[3].ToString();
                }
            }
        }

        public string OscMinutes
        {
            get
            {
                if (this._oscilloscopeJournalRecord[4] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[4].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[4].ToString();
                }
            }
        }

        public string OscSeconds
        {
            get
            {
                if (this._oscilloscopeJournalRecord[5] < 10)
                {
                    return "0" + this._oscilloscopeJournalRecord[5].ToString();
                }
                else
                {
                    return this._oscilloscopeJournalRecord[5].ToString();
                }
            }
        }
        /// <summary>
        /// Ступень в осц.
        /// </summary>
        public string OscStage
        {
            get
            {
                return Strings.AlarmJournalSteps[this._oscilloscopeJournalRecord[18]];
            }
        }

        public string OscMiliseconds
        {
            get
            {
                if (this._oscilloscopeJournalRecord[6] < 10)
                {
                    return "0" + (this._oscilloscopeJournalRecord[6] * 10).ToString();
                }
                else
                {
                    return (this._oscilloscopeJournalRecord[6] * 10).ToString();
                }
            }
        }
        // В 7м слове выведено не группа уставок, а что-то другое 
        //public string OscGroupSetpoint
        //{
        //    get
        //    {
        //        return Common.GetBits(_oscilloscopeJournalRecord[7],0,1,2,3) == 0 ? "Осн. группа" : "Рез. группа";
        //    }
        //}

        public int OscReady
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[9], this._oscilloscopeJournalRecord[8]);
            }
        }

        public int OscPoint
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[11], this._oscilloscopeJournalRecord[10]);
            }
        }

        public int OscBegin
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[13], this._oscilloscopeJournalRecord[12]);
            }
        }

        public int OscLen
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[15], this._oscilloscopeJournalRecord[14]);
            }
        }

        public int OscAfter
        {
            get
            {
                return Common.UshortUshortToInt(this._oscilloscopeJournalRecord[17], this._oscilloscopeJournalRecord[16]);
            }
        }

        public ushort OscAlm
        {
            get
            {
                return this._oscilloscopeJournalRecord[18];
            }
        }

        public ushort OscRez
        {
            get
            {
                return this._oscilloscopeJournalRecord[19];
            }
            set
            {
                this._oscilloscopeJournalRecord[19] = value;
            }
        }
        #endregion

        #region Осциллогрмма
        private static int _enableOscPageSize = 0x7c;//0x64;//

        public int EnableOscPageSize
        {
            get { return _enableOscPageSize; }
            set { _enableOscPageSize = value; }
        }

        private static int _oscPageSize = 0x400;

        public int OscPageSize
        {
            get { return _oscPageSize; }
            set { _oscPageSize = value; }
        }

        private static int _fullOscSize = 0x78000;

        public int FullOscSize
        {
            get { return _fullOscSize; }
            set { _fullOscSize = value; }
        }

        private static int _oscSize = 0x77FF4;

        public int OscSize
        {
            get { return _oscSize; }
            set { _oscSize = value; }
        }

        public ushort[] Oscilloscope
        {
            get
            {
                return this._oscilloscopeRead.Value;
            }
        }
        #endregion

        #region Настройки
        public int LoadedOscPoint
        {
            get { return this._oscLength.Value[3]; }
        }
        public int LoadedFullOscSize
        {
            get { return Common.UshortUshortToInt(this._oscLength.Value[1], this._oscLength.Value[0]); }
        }
        #endregion

        #endregion

        public string ConvertTime(string startTime)
        {
            string res = startTime;

            if (startTime.Length < 2)
            {
                res = "0" + startTime;
            }

            return res;
        }

        #region Системный журнал
        public string SysJournalRecTime
        {
            get
            {
                return this.ConvertTime(this._systemJournalRead.Value[2].ToString()) + "-" + this.ConvertTime(this._systemJournalRead.Value[1].ToString()) + "-" + this.ConvertTime(this._systemJournalRead.Value[0].ToString()) + ","
                       + this.ConvertTime(this._systemJournalRead.Value[3].ToString()) + ":" + this.ConvertTime(this._systemJournalRead.Value[4].ToString()) + ":" + this.ConvertTime(this._systemJournalRead.Value[5].ToString()) + "."
                       + this.ConvertTime(this._systemJournalRead.Value[6].ToString());
            }
        }

        public string SysJournalMessage
        {
            get
            {
                string res = string.Empty;
                if (this._version < 1.11)
                {
                    try
                    {
                        res = Strings.SysJournalMessages[this._systemJournalRead.Value[8]];
                    }
                    catch
                    {
                        res = "ХХХХХХ";
                    }
                }
                else
                {
                    try
                    {
                        res = Strings.SysJournalMessages[this._systemJournalRead.Value[8]];
                    }
                    catch
                    {
                        res = "ХХХХХХ";
                    }
                    if (this._systemJournalRead.Value[8] >= 500 && this._systemJournalRead.Value[8] < 600)
                    {
                        res = "СООБЩЕНИЕ СПЛ №" + (this._systemJournalRead.Value[8] - 499).ToString();
                    }
                }
                return res;
            }
        }

        public int SysJournalMessageIndex
        {
            get
            {
                return this._systemJournalRec;
            }
            set { _systemJournalRec = value; }
        }

        public void SetSystemJurnalSlotValue(ushort[] value)
        {
            _systemJournalRead.Value = value;
        }

        #endregion

        #region Журнал аварий

        public void SetAlaramJurnalSlotValue(ushort[] value)
        {
            _alarmJournalRead.Value = value;
        }

        public int AlarmJournalPageIndex
        {
            get { return this._alarmJournalRec; }
            set { this._alarmJournalRec = value; }
        }

        public string AlarmJournalRecTime
        {
            get
            {
                string _time = "FAIL";
                int timeVal = this._alarmJournalRead.Value[0] + this._alarmJournalRead.Value[1] +
                              this._alarmJournalRead.Value[2] + this._alarmJournalRead.Value[3] +
                              this._alarmJournalRead.Value[4] + this._alarmJournalRead.Value[5] +
                              this._alarmJournalRead.Value[6];

                if (timeVal != 0)
                {
                    _time = this.ConvertTime(this._alarmJournalRead.Value[2].ToString()) + "-" + this.ConvertTime(this._alarmJournalRead.Value[1].ToString()) + "-" + this.ConvertTime(this._alarmJournalRead.Value[0].ToString()) + ","
                           + this.ConvertTime(this._alarmJournalRead.Value[3].ToString()) + ":" + this.ConvertTime(this._alarmJournalRead.Value[4].ToString()) + ":" + this.ConvertTime(this._alarmJournalRead.Value[5].ToString()) + "."
                           + this.ConvertTime(this._alarmJournalRead.Value[6].ToString());
                }

                return _time;
            }
        }

        public string AlarmJournalMessage
        {
            get
            {
                try
                {
                    string res = Strings.AlarmJournalRecords[this._alarmJournalRead.Value[7]];

                    if (res == "ОМП") return "";
                    return res;
                }
                catch (Exception)
                {
                    return "ХХХХХХ";
                }
            }
        }

        public string AlarmJournalDifIndex
        {
            get
            {
                try
                {
                    return Strings.AlarmJournalSteps[this._alarmJournalRead.Value[8]];
                }
                catch (Exception)
                {
                    return "ХХХХХХ";
                    throw;
                }

            }
        }

        public string AlarmJournalParam
        {
            get
            {
                return Strings.Params[this._alarmJournalRead.Value[9]];
            }
        }

        public string AlarmJournalParamValue
        {
            get
            {
                string res = string.Empty;
                switch (this.AlarmJournalParam)
                {
                    case "Ia дифф.":
                    case "Ib дифф.":
                    case "Ic дифф.":
                    case "Ia торм.":
                    case "Ib торм.":
                    case "Ic торм.":
                    case "Ст.1 Ia":
                    case "Ст.1 Ib":
                    case "Ст.1 Ic":
                    case "Ст.1 In":
                    case "Ст.1 I0":
                    case "Ст.1 Iдифф.0":
                    case "Ст.1 Iторм.0":
                        {
                            res = this.ShowI(this._alarmJournalRead.Value[10], this.S1WindingRatedVoltage);
                            break;
                        }
                    case "Ст.2 Ia":
                    case "Ст.2 Ib":
                    case "Ст.2 Ic":
                    case "Ст.2 In":
                    case "Ст.2 I0":
                    case "Ст.2 Iдифф.0":
                    case "Ст.2 Iторм.0":
                        {
                            res = this.ShowI(this._alarmJournalRead.Value[10], this.S2WindingRatedVoltage);
                            break;
                        }
                    case "Ст.3 Ia":
                    case "Ст.3 Ib":
                    case "Ст.3 Ic":
                    case "Ст.3 In":
                    case "Ст.3 I0":
                    case "Ст.3 Iдифф.0":
                    case "Ст.3 Iторм.0":
                        {
                            res = this.ShowI(this._alarmJournalRead.Value[10], this.S2WindingRatedVoltage);
                            break;
                        }
                    case "Ua":
                    case "Ub":
                    case "Uc":
                    case "Uab":
                    case "Ubc":
                    case "Uca":
                    case "U0":
                    case "U2":
                        {
                            res = this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[10], this.TH_L * this.TH_LKoef));
                            break;
                        }
                    case "Un":
                        {
                            res = this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[10], this.TH_X * this.TH_XKoef));
                            break;
                        }
                    default:
                        {
                            if (this._alarmJournalRead.Value[9] == 42)
                            {
                                res = "СПЛ " + this._alarmJournalRead.Value[10].ToString();
                            }
                            break;
                        }
                }
                return res;
            }
        }

        public string AlarmJournalGroupUst
        {
            get
            {
                string _group = string.Empty;
                switch (this._alarmJournalRead.Value[11])
                {
                    case 0:
                        {
                            _group = "Основная";
                            break;
                        }
                    case 1:
                        {
                            _group = "Резервная";
                            break;
                        }
                    default:
                        {
                            _group = "Ошибка";
                            break;
                        }
                }

                return _group;
            }
        }


        public string AlarmJournalIDa
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[12], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalIDb
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[13], this.S1WindingRatedVoltage); ;
            }
        }

        public string AlarmJournalIDc
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[14], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalITa
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[15], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalITb
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[16], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalITc
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[17], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs1a
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[18], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs1b
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[19], this.S1WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs1c
        {
            get { return this.ShowI(this._alarmJournalRead.Value[20], this.S1WindingRatedVoltage); }
        }

        public string AlarmJournalIs2a
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[21], this.S2WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs2b
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[22], this.S2WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs2c
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[23], this.S2WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs3a
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[24], this.S3WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs3b
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[25], this.S3WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs3c
        {
            get
            {
                return this.ShowI(this._alarmJournalRead.Value[26], this.S3WindingRatedVoltage);
            }
        }

        public string AlarmJournalIs1n
        {
            get { return this.ShowI(this._alarmJournalRead.Value[27], this.S1WindingRatedVoltage); }
        }

        public string AlarmJournalIs10
        {
            get { return this.ShowI(this._alarmJournalRead.Value[28], this.S1WindingRatedVoltage); }
        }


        public string AlarmJournalIs12
        {
            get { return this.ShowI(this._alarmJournalRead.Value[29], this.S1WindingRatedVoltage); }
        }

        public string AlarmJournalIs2n
        {
            get { return this.ShowI(this._alarmJournalRead.Value[30], this.S2WindingRatedVoltage); }
        }

        public string AlarmJournalIs20
        {
            get { return this.ShowI(this._alarmJournalRead.Value[31], this.S2WindingRatedVoltage); }
        }


        public string AlarmJournalIs22
        {
            get { return this.ShowI(this._alarmJournalRead.Value[32], this.S2WindingRatedVoltage); }
        }

        public string AlarmJournalIs3n
        {
            get { return this.ShowI(this._alarmJournalRead.Value[33], this.S3WindingRatedVoltage); }
        }

        public string AlarmJournalIs30
        {
            get { return this.ShowI(this._alarmJournalRead.Value[34], this.S3WindingRatedVoltage); }
        }


        public string AlarmJournalIs32
        {
            get { return this.ShowI(this._alarmJournalRead.Value[35], this.S3WindingRatedVoltage); }
        }

        public string AlarmJournalIs1d0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[36], this.S1WindingRatedVoltage); }
        }

        public string AlarmJournalIs2d0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[37], this.S2WindingRatedVoltage); }
        }

        public string AlarmJournalIs3d0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[38], this.S3WindingRatedVoltage); }
        }

        public string AlarmJournalIs1T0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[39], this.S1WindingRatedVoltage); }
        }

        public string AlarmJournalIs2T0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[40], this.S2WindingRatedVoltage); }
        }

        public string AlarmJournalIs3T0
        {
            get { return this.ShowI(this._alarmJournalRead.Value[41], this.S3WindingRatedVoltage); }
        }

        public string AlarmJournalUa
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[42], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUb
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[43], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUc
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[44], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUab
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[45], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUbc
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[46], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUca
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[47], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalUn
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[48], this.TH_X * this.TH_XKoef)); }
        }

        public string AlarmJournalU0
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[49], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalU2
        {
            get { return this.ConvertU(Measuring.GetU(this._alarmJournalRead.Value[50], this.TH_L * this.TH_LKoef)); }
        }

        public string AlarmJournalF
        {
            get { return this.ConvertF(Measuring.GetF(this._alarmJournalRead.Value[51])); }
        }

        public string AlarmJournalD1
        {
            get { return Common.ByteToMask(Common.LOBYTE(this._alarmJournalRead.Value[52]), true); }
        }

        public string AlarmJournalD2
        {
            get { return Common.ByteToMask(Common.HIBYTE(this._alarmJournalRead.Value[52]), true); }
        }

        public string AlarmJournalD3
        {
            get { return Common.ByteToMask(Common.LOBYTE(this._alarmJournalRead.Value[53]), true); }
        }
        #endregion

        #region Получение первичных значений из относительных единиц
        public string ConvertU(double u)
        {
            string param = "В";
            if (u > 1000)
            {
                u = u / 1000;
                param = "кВ";
            }
            return Math.Round(u, 2) + " {" + param + "}";
        }

        public string ConvertF(double f)
        {
            string param = "Гц";
            if (f > 1000)
            {
                f = f / 1000;
                param = "кГц";
            }
            return f + " {" + param + "}";
        }

        public string ShowI(ushort value, double U)
        {
            string param = "A";
            if (value == 0 && U == 0)
            {
                return 0 + " {" + param + "}";
            }
            double i;
            double vers = Common.VersionConverter(DeviceVersion);
            if (vers > 1)
            {
                double tmp = Math.Sqrt(3);
                i = value * 40 / (double)65536 * (this.S1WindingRatedPowerDOUBLE / (tmp * U)) * 1000;
            }
            else
            {
                double tmp = Math.Sqrt(3);
                i = value * 40 / (double)65536 * (this.S1WindingRatedPower / (tmp * U)) * 1000;
            }
            int dec = 2;
            if (i > 1000)
            {
                i = i / 1000;
                param = "кA";
                dec = 2;
            }

            return Math.Round(i, dec) + " {" + param + "}";
        }

        public string ShowPercent(ushort value, double u)
        {
            double percent = value * 4000.0 / 65536.0;
            string param = "%Iб";
            return Math.Round(percent, 2) + " {" + param + "}";
        }

        public string ShowIDiagnostik(ushort value)
        {
            double i = value * 40 * 5 / (double)65536;
            string param = "A";
            int dec = 2;
            if (i > 1000)
            {
                i = i / 1000;
                param = "кA";
                dec = 2;
            }
            return Math.Round(i, dec) + " {" + param + "}";
        }
        #endregion

        #region Аналоговая БД

        public string Ida
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[0], this.S1WindingRatedVoltage);
            }
        }
        public string Idb
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[1], this.S1WindingRatedVoltage);
            }
        }
        public string Idc
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[2], this.S1WindingRatedVoltage);
            }
        }

        public string PercentIda
        {
            get
            {
                return this.ShowPercent(this._analogBDSlot.Value[0], this.S1WindingRatedVoltage);
            }
        }

        public string PercentIdb
        {
            get
            {
                return this.ShowPercent(this._analogBDSlot.Value[1], this.S1WindingRatedVoltage);
            }
        }

        public string PercentIdc
        {
            get
            {
                return this.ShowPercent(this._analogBDSlot.Value[2], this.S1WindingRatedVoltage);
            }
        }

        public string Iba
        {
            get
            {
                return this.ShowI(Common.VersionConverter(DeviceVersion) > 1.10 
                    ? this._analogBDSlot.Value[9]
                    : this._analogBDSlot.Value[12], this.S1WindingRatedVoltage);
            }
        }
        public string Ibb
        {
            get
            {
                return this.ShowI(Common.VersionConverter(DeviceVersion) > 1.10 
                    ? this._analogBDSlot.Value[10] :
                    this._analogBDSlot.Value[13], this.S1WindingRatedVoltage);
            }
        }
        public string Ibc
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[11], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[14], this.S1WindingRatedVoltage);
                }
            }
        }

        public string PercentIba
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowPercent(this._analogBDSlot.Value[9], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowPercent(this._analogBDSlot.Value[12], this.S1WindingRatedVoltage);
                }
            }
        }

        public string PercentIbb
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowPercent(this._analogBDSlot.Value[10], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowPercent(this._analogBDSlot.Value[13], this.S1WindingRatedVoltage);
                }
            }
        }

        public string PercentIbc
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowPercent(this._analogBDSlot.Value[11], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowPercent(this._analogBDSlot.Value[14], this.S1WindingRatedVoltage);
                }
            }
        }

        public string Is1a
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[13], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[15], this.S1WindingRatedVoltage);
                }
            }
        }
        public string Is1b
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[14], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[16], this.S1WindingRatedVoltage);
                }
            }
        }
        public string Is1c
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[15], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[17], this.S1WindingRatedVoltage);
                }
            }
        }
        public string Is1n
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[12], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[18], this.S1WindingRatedVoltage);
                }
            }
        }

        public string Is10
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[16], this.S1WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[19], this.S1WindingRatedVoltage);
                }
            }
        }

        public string Is12
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[17], this.S1WindingRatedVoltage);
            }
        }

        public string Is1d0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[18], this.S1WindingRatedVoltage);
            }
        }

        public string Is1b0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[19], this.S1WindingRatedVoltage);
            }
        }

        public string Is2a
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[21], this.S2WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[20], this.S2WindingRatedVoltage);
                }
            }
        }
        public string Is2b
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[22], this.S2WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[21], this.S2WindingRatedVoltage);
                }
            }
        }
        public string Is2c
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[23], this.S2WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[22], this.S2WindingRatedVoltage);
                }
            }
        }
        public string Is2n
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[20], this.S2WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[23], this.S2WindingRatedVoltage);
                }
            }
        }

        public string Is20
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[24], this.S2WindingRatedVoltage);
            }
        }

        public string Is22
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[25], this.S2WindingRatedVoltage);
            }
        }

        public string Is2d0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[26], this.S2WindingRatedVoltage);
            }
        }

        public string Is2b0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[27], this.S2WindingRatedVoltage);
            }
        }

        public string Is3a
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[29], this.S3WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[25], this.S3WindingRatedVoltage);
                }
            }
        }
        public string Is3b
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[30], this.S3WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[26], this.S3WindingRatedVoltage);
                }
            }
        }
        public string Is3c
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[31], this.S3WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[27], this.S3WindingRatedVoltage);
                }
            }
        }
        public string Is3n
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[28], this.S3WindingRatedVoltage);
            }
        }
        public string Is30
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowI(this._analogBDSlot.Value[32], this.S3WindingRatedVoltage);
                }
                else
                {
                    return this.ShowI(this._analogBDSlot.Value[29], this.S3WindingRatedVoltage);
                }
            }
        }
        public string Is32
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[33], this.S3WindingRatedVoltage);
            }
        }
        public string Is3d0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[34], this.S3WindingRatedVoltage);
            }
        }
        public string Is3b0
        {
            get
            {
                return this.ShowI(this._analogBDSlot.Value[35], this.S3WindingRatedVoltage);
            }
        }
        public string Uab
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[40], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[30], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string Ubc
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[41], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[31], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string Uca
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[42], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[32], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string U0
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[43], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[33], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string U2
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[44], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[34], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string I0
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[45]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[35]);
                }
            }
        }
        public string Ia
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[46]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[36]);
                }
            }
        }
        public string Ib
        {
            get
            {

                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[47]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[37]);
                }
            }
        }
        public string Ic
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[48]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[38]);
                }
            }
        }
        public string I20
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[49]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[39]);
                }
            }
        }
        public string I2a
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[50]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[40]);
                }
            }
        }
        public string I2b
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[51]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[41]);
                }
            }
        }
        public string I2c
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[52]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[42]);
                }
            }
        }
        public string I30
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[53]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[43]);
                }
            }
        }
        public string I3a
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[54]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[44]);
                }
            }
        }
        public string I3b
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[55]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[45]);
                }
            }
        }
        public string I3c
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[56]);
                }
                else
                {
                    return this.ShowIDiagnostik(this._analogBDSlot.Value[46]);
                }
            }
        }
        public string Un
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[36], this.TH_X * this.TH_XKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[47], this.TH_X * this.TH_XKoef));
                }
            }
        }
        public string Ua
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[37], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[48], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string Ub
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[38], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[49], this.TH_L * this.TH_LKoef));
                }
            }
        }
        public string Uc
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[39], this.TH_L * this.TH_LKoef));
                }
                else
                {
                    return this.ConvertU(Measuring.GetU(this._analogBDSlot.Value[50], this.TH_L * this.TH_LKoef));
                }
            }
        }

        public string F
        {
            get
            {
                return this.ConvertF(Common.VersionConverter(DeviceVersion) > 1.10
                    ? Measuring.GetF(this._analogBDSlot.Value[57])
                    : Measuring.GetF(this._analogBDSlot.Value[51]));
            }
        }

        public string D1
        {
            get { return Common.ByteToMask(Common.LOBYTE(this._alarmJournalRead.Value[52]), true); }
        }

        public string D2
        {
            get { return Common.ByteToMask(Common.HIBYTE(this._alarmJournalRead.Value[52]), true); }
        }

        public string D3
        {
            get { return Common.ByteToMask(Common.LOBYTE(this._alarmJournalRead.Value[53]), true); }
        }
        
        #endregion

        #region Дискретная БД
        [XmlIgnore]
        public BitArray DiskretSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(this._bitBDSlot.Value[0]),
                                                 Common.HIBYTE(this._bitBDSlot.Value[0]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[1])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray LSSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[1]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[2])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray VLSSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[2]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[3])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray IdImaxSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[3]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[4])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray ImaxI0Signals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[4]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[5])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray I0InUmaxSignals
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[5]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[6])
                                                });
            }
        }

        [XmlIgnore]
        public BitArray QEXTSignals
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[6]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[7]),
                                                     Common.HIBYTE(this._bitBDSlot.Value[7]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[8])
                                                });

                }
                else
                {
                    return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[6]),
                                                    Common.LOBYTE(this._bitBDSlot.Value[7])
                                                });
                }
            }
        }

        [XmlIgnore]
        public BitArray EXTSmeshSignals
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[8]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[9]),
                                                     Common.HIBYTE(this._bitBDSlot.Value[9]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[10]),
                                                     Common.HIBYTE(this._bitBDSlot.Value[10]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[11])
                                                });
                }
                else
                {
                    return new BitArray(new byte[] { Common.HIBYTE(this._bitBDSlot.Value[7]),
                                                     Common.LOBYTE(this._bitBDSlot.Value[8])
                                                });
                }
            }
        }

        [XmlIgnore]
        public BitArray ReleSignals
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return new BitArray(new byte[] { Common.LOBYTE(this._bitBDSlot.Value[12]),
                                                     Common.HIBYTE(this._bitBDSlot.Value[12])
                                                });

                }
                else
                {
                    return new BitArray(new byte[] { Common.LOBYTE(this._bitBDSlot.Value[10]),
                                                     Common.HIBYTE(this._bitBDSlot.Value[10])
                                                });
                }
            }
        }

        [XmlIgnore]
        public BitArray IndSignals
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return new BitArray(new []
                    {
                        Common.LOBYTE(this._bitBDSlot.Value[13]),
                        Common.HIBYTE(this._bitBDSlot.Value[13])
                    });

                }
                else
                {
                    return new BitArray(new []
                    {
                        Common.LOBYTE(this._bitBDSlot.Value[11]),
                        Common.HIBYTE(this._bitBDSlot.Value[11]),
                        Common.LOBYTE(this._bitBDSlot.Value[12])
                    });
                }

            }
        }

        [XmlIgnore]
        public BitArray ControlSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._bitBDSlot.Value[14]),
                    Common.HIBYTE(this._bitBDSlot.Value[14])
                });
            }
        }

        [XmlIgnore]
        public BitArray BugsSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._bitBDSlot.Value[16]),
                    Common.HIBYTE(this._bitBDSlot.Value[16]),
                    Common.LOBYTE(this._bitBDSlot.Value[17])
                });
            }
        }

        [XmlIgnore]
        public BitArray FaultLogicSignals
        {
            get
            {
                return new BitArray(new byte[]
                {
                    Common.LOBYTE(this._bitBDSlot.Value[18]),
                    Common.HIBYTE(this._bitBDSlot.Value[18])
                });
            }
        }



        [XmlIgnore]
        public BitArray ZnakiPart1
        {
            get
            {
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    return new BitArray(new byte[] { Common.LOBYTE(this._bitBDSlot.Value[32]),
                                                 Common.HIBYTE(this._bitBDSlot.Value[32]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[33]),
                                                 Common.HIBYTE(this._bitBDSlot.Value[33]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[34])
                                                });
                }
                else
                {
                    return new BitArray(new byte[] { Common.LOBYTE(this._bitBDSlot.Value[32]),
                                                 Common.HIBYTE(this._bitBDSlot.Value[32]),
                                                 Common.LOBYTE(this._bitBDSlot.Value[33]),
                                                 Common.HIBYTE(this._bitBDSlot.Value[33])
                                                });
                }
            }
        }

        public bool FaultLogicError
        {
            get
            {
                return this.FaultLogicSignals[6] || this.FaultLogicSignals[7] || this.FaultLogicSignals[8] || this.FaultLogicSignals[9] || this.FaultLogicSignals[10];
            }
        }


        #endregion

        #region Подтверждение изменений
        /// <summary>
        /// Подтверждение изменений
        /// </summary>
        public int ConfirmConstraintF
        {
            set
            {
                this._confConstraint.Value[0] = (ushort)value;
            }
        }
        #endregion

        #region Функции чтения из устройства

        public void LoadOscLength()
        {
            LoadSlot(DeviceNumber, this._oscLength, "LoadOscLength" + DeviceNumber, this);
        }

        //Конфигурация всех выключателей
        public void LoadSwitchAll()
        {
            LoadSlot(DeviceNumber, this._switchAll, "LoadSwitchAll" + DeviceNumber, this);
        }

        public void LoadSwitch()
        {

            LoadSlot(DeviceNumber, this._switch, "LoadSwitch" + DeviceNumber, this);

        }

        public void LoadAPV()
        {

            LoadSlot(DeviceNumber, this._apv, "LoadAPV" + DeviceNumber, this);

        }

        public void LoadAVR()
        {

            LoadSlot(DeviceNumber, this._avr, "LoadAVR" + DeviceNumber, this);

        }

        public void LoadLPB()
        {

            LoadSlot(DeviceNumber, this._lpb, "LoadLPB" + DeviceNumber, this);

        }

        //Конфигурация всех УРОВ
        public void LoadUrovAll()
        {
            LoadSlot(DeviceNumber, this._urovAll, "LoadUrovAll" + DeviceNumber, this);
        }

        //Конфигурация всей автоматики обдува
        public void LoadAutoblower()
        {
            LoadSlot(DeviceNumber, this._autoblower, "LoadAutoblower" + DeviceNumber, this);
        }

        //Конфигурация тепловой модели
        public void LoadTermal()
        {
            LoadSlot(DeviceNumber, this._termal, "LoadTermal" + DeviceNumber, this);
        }

        //Конфигурация входных сигналов
        public void LoadInputSygnal()
        {

            LoadSlot(DeviceNumber, this._inputsygnal, "LoadInputSygnal" + DeviceNumber, this);
        }

        //Конфигурация осцилографа
        public void LoadOscope()
        {
            LoadSlot(DeviceNumber, this._oscope, "LoadOscope" + DeviceNumber, this);
        }

        //Конфигурация Ethernet
        public void LoadConfigEthernet()
        {
            LoadSlot(DeviceNumber, this._configEthernet, "LoadConfigEthernet" + DeviceNumber, this);
        }

        //Структура силового транформатора
        public void LoadPowerTrans()
        {
            LoadSlot(DeviceNumber, this._powerTrans, "LoadPowerTrans" + DeviceNumber, this);
        }

        //Структура измерительного трансформатора
        public void LoadMeasureTrans()
        {
            LoadSlot(DeviceNumber, this._measureTrans, "LoadMeasureTrans" + DeviceNumber, this);
        }

        //Структура входных логических сигналов
        public void LoadInpSygnal()
        {
            LoadSlot(DeviceNumber, this._inpSygnal, "LoadInpSygnal" + DeviceNumber, this);
        }

        //Структура выходных логических сигналов
        public void LoadElsSygnal()
        {
            LoadSlot(DeviceNumber, this._elsSygnal1, "LoadElsSygnal1" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._elsSygnal2, "LoadElsSygnal2" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._elsSygnal3, "LoadElsSygnal3" + DeviceNumber, this);
            LoadSlot(DeviceNumber, this._elsSygnal4, "LoadElsSygnal4" + DeviceNumber, this);
        }

        //Все защиты
        public void LoadProtAll()
        {
            this.CurrentProtAll.Clear();
            this.InitProtAllList();
            for (int j = 0; j < this.CurrentProtAll.Count; j++)
            {
                LoadSlot(DeviceNumber, this.CurrentProtAll[j], "LoadProtAll" + j.ToString() + DeviceNumber, this);
            }
        }

        //Параметры автоматики
        public void LoadParamAutomat()
        {
            LoadSlot(DeviceNumber, this._paramAutomat, "LoadParamAutomat" + DeviceNumber, this);
        }

        //Журнал осциллографа
        int _recordIndex = 0;
        public void LoadOscilloscopeJournal(int recordIndex)
        {
            this._recordIndex = recordIndex;
            LoadSlot(DeviceNumber, this._oscilloscopeJournalReadSlot, "LoadOscilloscopeJournal" + recordIndex + DeviceNumber, this);
        }

        //Осциллограмма
        private int _oscIndexRead = 0;
        int _temp = 0;
        public void LoadOscilloscopePage(int oscIndexRead)
        {
            if (_oscPageSize % _enableOscPageSize != 0)
            {
                this._temp = 1;
            }
            this._oscilloscopeRead = new slot(this._startOscilloscope, this._endOscilloscope);
            this.CurrentOscilloscope.Clear();
            this._oscIndexRead = oscIndexRead;
            for (int i = 0; i < _oscPageSize / _enableOscPageSize + this._temp; i++)
            {

                LoadSlot(DeviceNumber, this._oscilloscopeRead, "LoadOscilloscope" + oscIndexRead + i + DeviceNumber, this);
                this.CurrentOscilloscope.Add(this._oscilloscopeRead);
                if (i + 1 < _oscPageSize / _enableOscPageSize || (_oscPageSize % _enableOscPageSize == 0))
                {
                    this._oscilloscopeRead = new slot((ushort)(this._oscilloscopeRead.Start + _enableOscPageSize), (ushort)(this._oscilloscopeRead.End + _enableOscPageSize));
                }
                else
                {
                    if (i + 1 == _oscPageSize / _enableOscPageSize)
                    {
                        this._oscilloscopeRead = new slot((ushort)(this._oscilloscopeRead.Start + _enableOscPageSize), (ushort)(this._oscilloscopeRead.End + _oscPageSize % _enableOscPageSize));
                    }
                }
            }
        }

        //Чтение записи системного журнала
        int _systemJournalRec = 0;
        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, this._systemJournalRead, "LoadSystemJournalRecord" + this._systemJournalRec + DeviceNumber, this);
        }

        //Чтение записи журнала аварий
        int _alarmJournalRec = 0;
        public void LoadAlarmJournal()
        {
            LoadSlot(DeviceNumber, this._alarmJournalRead, "LoadAlarmJournalRecord" + this._alarmJournalRec + DeviceNumber, this);
        }

        public void LoadAnalogBDCycle()
        {
            LoadSlotCycle(DeviceNumber, this._analogBDSlot, "LoadAnalogBD" + DeviceNumber, new TimeSpan(5000000), 10, this);
        }

        public void RemoveAnalogBD()
        {
            this.MB.RemoveQuery("LoadAnalogBD" + DeviceNumber);
        }

        public void MakeAnalogBDQuick()
        {
            MakeQuerySpan("LoadAnalogBD" + DeviceNumber, new TimeSpan(5000000));
        }

        public void MakeAnalogBDSlow()
        {
            MakeQuerySlow("LoadAnalogBD" + DeviceNumber);
        }

        public void LoadBitBDCycle()
        {
            LoadSlotCycle(DeviceNumber, this._bitBDSlot, "LoadBitBD" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        public void RemoveBitBD()
        {
            this.MB.RemoveQuery("LoadBitBD" + DeviceNumber);
        }

        public void MakeBitBDQuick()
        {
            MakeQuerySpan("LoadBitBD" + DeviceNumber, new TimeSpan(1000));
        }

        public void MakeBitBDSlow()
        {
            MakeQuerySlow("LoadBitBD" + DeviceNumber);
        }

        #endregion

        #region Функции записи в устройство
        //Конфигурация всех выключателей
        public void SaveSwitchAll()
        {
            SaveSlot(DeviceNumber, this._switchAll, "SaveSwitchAll" + DeviceNumber, this);
        }

        public void SaveSwitch()
        {
            SaveSlot(DeviceNumber, this._switch, "SaveSwitch" + DeviceNumber, this);
        }

        public void SaveAPV()
        {

            SaveSlot(DeviceNumber, this._apv, "SaveAPV" + DeviceNumber, this);

        }

        public void SaveAVR()
        {

            SaveSlot(DeviceNumber, this._avr, "SaveAVR" + DeviceNumber, this);

        }

        public void SaveLPB()
        {

            SaveSlot(DeviceNumber, this._lpb, "SaveLPB" + DeviceNumber, this);

        }

        //Конфигурация всех УРОВ
        public void SaveUrovAll()
        {
            SaveSlot(DeviceNumber, this._urovAll, "SaveUrovAll" + DeviceNumber, this);
        }

        //Конфигурация всей автоматики обдува
        public void SaveAutoblower()
        {
            SaveSlot(DeviceNumber, this._autoblower, "SaveAutoblower" + DeviceNumber, this);
        }

        //Конфигурация тепловой модели
        public void SaveTermal()
        {
            LoadSlot(DeviceNumber, this._termal, "SaveTermal" + DeviceNumber, this);
        }

        //Конфигурация входных сигналов
        public void SaveInputSygnal()
        {
            SaveSlot(DeviceNumber, this._inputsygnal, "SaveInputSygnal" + DeviceNumber, this);
        }

        //Конфигурация осцилографа
        public void SaveOscope()
        {
            SaveSlot(DeviceNumber, this._oscope, "SaveOscope" + DeviceNumber, this);
        }

        //Конфигурация Ethernet
        public void SaveConfigEthernet()
        {
            SaveSlot(DeviceNumber, this._configEthernet, "SaveConfigEthernet" + DeviceNumber, this);
        }

        //Структура силового транформатора
        public void SavePowerTrans()
        {
            SaveSlot(DeviceNumber, this._powerTrans, "SavePowerTrans" + DeviceNumber, this);
        }

        //Структура измерительного трансформатора
        public void SaveMeasureTrans()
        {
            SaveSlot(DeviceNumber, this._measureTrans, "SaveMeasureTrans" + DeviceNumber, this);
        }

        //Структура входных логических сигналов
        public void SaveInpSygnal()
        {
            SaveSlot(DeviceNumber, this._inpSygnal, "SaveInpSygnal" + DeviceNumber, this);
        }

        //Структура выходных логических сигналов
        public void SaveElsSygnal()
        {
            SaveSlot(DeviceNumber, this._elsSygnal1, "SaveElsSygnal1" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._elsSygnal2, "SaveElsSygnal2" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._elsSygnal3, "SaveElsSygnal3" + DeviceNumber, this);
            SaveSlot(DeviceNumber, this._elsSygnal4, "SaveElsSygnal4" + DeviceNumber, this);
        }

        //Все защиты
        public void SaveProtAll()
        {
            if (this.CurrentProtAll.Count == 0)
            {
                this.InitProtAllList();
                this.SetCurrentProtAll();
            }
            for (int i = 0; i < this.CurrentProtAll.Count; i++)
            {
                SaveSlot(DeviceNumber, this.CurrentProtAll[i], "SaveProtAll" + DeviceNumber + i.ToString(), this);
            }
        }

        //Параметры автоматики
        public void SaveParamAutomat()
        {
            SaveSlot(DeviceNumber, this._paramAutomat, "SaveParamAutomat" + DeviceNumber, this);
        }

        //Номер журнала осциллографа
        public void SaveOscilloscopeJournal(ushort index)
        {
            this._oscilloscopeJournalWriteSlot.Value[0] = index;
            SaveSlot(DeviceNumber, this._oscilloscopeJournalWriteSlot, "SaveOscilloscopeJournal" + DeviceNumber, this);
        }

        private int _oscIndexWrite = 0;
        public void SaveOscilloscopePage(int oscIndexWrite)
        {
            this._oscIndexWrite = oscIndexWrite;
            this._oscilloscopePageWrite.Value[0] = (ushort)oscIndexWrite;
            SaveSlot(DeviceNumber, this._oscilloscopePageWrite, "SaveOscilloscope" + oscIndexWrite + DeviceNumber, this);
        }

        public void SaveSystemJournalIndex()
        {
            this._systemJournalRec = 0;
            this._systemJournalWrite.Value[0] = 0;
            SaveSlot(DeviceNumber, this._systemJournalWrite, "SaveSystemJournal" + DeviceNumber, this);
        }

        public void SaveAlarmJournalIndex()
        {
            this._alarmJournalRec = 0;
            this._alarmJournalWrite.Value[0] = 0;
            SaveSlot(DeviceNumber, this._alarmJournalWrite, "SaveAlarmJournal" + DeviceNumber, this);
        }

        public void ConfirmConstraintFunc()
        {
            this.ConfirmConstraintF = 1;
            SaveSlot(DeviceNumber, this._confConstraint, "ConfirmConstraint" + DeviceNumber, this);
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0x0D00/*0x1800*/, true, "UDZT запрос подтверждения", this);
        }
        #endregion


        #region Сериализация
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_всех_выключателей", this._switchAll);
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    this.DeserializeAutoSlot(doc, "/УДЗТ_уставки/Конфигурация_всех_выключателей1", this._switch);
                    this.DeserializeAutoSlot(doc, "/УДЗТ_уставки/Конфигурация_АПВ", this._apv);
                    this.DeserializeAutoSlot(doc, "/УДЗТ_уставки/Конфигурация_АВР", this._avr);
                    this.DeserializeAutoSlot(doc, "/УДЗТ_уставки/Конфигурация_ЛЗШ", this._lpb);
                }
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_всех_УРОВ", this._urovAll);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_всей_автоматики_обдува", this._autoblower);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_тепловой_модели", this._termal);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_входных_сигналов", this._inputsygnal);
                this.DeserializeOscSlot(doc, "/УДЗТ_уставки/Конфигурация_осцилографа", this._oscope);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_силового_транформатора", this._powerTrans);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_измерительного_трансформатора", this._measureTrans);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_входных_логических_сигналов", this._inpSygnal);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_выходных_логических_сигналов1", this._elsSygnal1);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_выходных_логических_сигналов2", this._elsSygnal2);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_выходных_логических_сигналов3", this._elsSygnal3);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Структура_выходных_логических_сигналов4", this._elsSygnal4);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Все_защиты", this._currentProtAll);
                this.DeserializeSlot(doc, "/УДЗТ_уставки/Параметры_автоматики", this._paramAutomat);
                if (doc.SelectSingleNode("/УДЗТ_уставки/Конфигурация_ethernet") != null)
                {
                    if (Common.VersionConverter(DeviceVersion) > 2.07)
                    {
                        this.DeserializeSlot(doc, "/УДЗТ_уставки/Конфигурация_ethernet", this._configEthernet);
                    }
                }
                Array.ConstrainedCopy(this._elsSygnal1.Value, 0, this._elsSygnal, 0, this._elsSygnal1.Size);
                Array.ConstrainedCopy(this._elsSygnal2.Value, 0, this._elsSygnal, this._elsSygnal1.Size, this._elsSygnal2.Size);
                Array.ConstrainedCopy(this._elsSygnal3.Value, 0, this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size, this._elsSygnal3.Size);
                Array.ConstrainedCopy(this._elsSygnal4.Value, 0, this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size + this._elsSygnal3.Size, this._elsSygnal4.Size);
                this._curProtAll = this._currentProtAll.Value;
                ushort destinationindex = 0;
                for (int i = 0; i < this.CurrentProtAll.Count; i++)
                {
                    Array.ConstrainedCopy(this._curProtAll, destinationindex, this.CurrentProtAll[i].Value, 0, this.CurrentProtAll[i].Size);
                    destinationindex += this.CurrentProtAll[i].Size;
                }
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("Файл уставок УДЗТ поврежден", binFileName);
            }
        }

        void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }

        void DeserializeAutoSlot(XmlDocument doc, string nodePath, slot slot)
        {
            try
            {
                slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
            }
            catch
            {
                slot.Value = new ushort[14];
            }
        }

        void DeserializeOscSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
            if (slot.Value.Length < 11 && Common.VersionConverter(DeviceVersion) < 1.11)
            {
                ushort[] buf = slot.Value;
                slot.Value = new ushort[11];
                for (int i = 0; i < buf.Length; i++)
                {
                    slot.Value[i] = buf[i];
                }
            }
        }

        public void Serialize(string binFileName)
        {
            //string xmlFileName = Path.ChangeExtension(binFileName, ".xml");

            //Type r = GetType();
            //try
            //{
            //    XmlSerializer ser = new XmlSerializer(r);
            //    TextWriter writer = new StreamWriter(xmlFileName, false, Encoding.UTF8);
            //    ser.Serialize(writer, this);
            //    writer.Close();
            //}
            //catch (Exception)
            //{
            //    //MessageBox.Show("Ошибка Сериализации", "Внимание!");
            //}

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("УДЗТ_уставки"));

                Array.ConstrainedCopy(this._elsSygnal, 0, this._elsSygnal1.Value, 0, this._elsSygnal1.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size, this._elsSygnal2.Value, 0, this._elsSygnal2.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size, this._elsSygnal3.Value, 0, this._elsSygnal3.Size);
                Array.ConstrainedCopy(this._elsSygnal, this._elsSygnal1.Size + this._elsSygnal2.Size + this._elsSygnal3.Size, this._elsSygnal4.Value, 0, this._elsSygnal4.Size);

                this.SerializeSlot(doc, "Конфигурация_всех_выключателей", this._switchAll);
                if (Common.VersionConverter(DeviceVersion) > 1.10)
                {
                    this.SerializeSlot(doc, "Конфигурация_всех_выключателей1", this._switch);
                    this.SerializeSlot(doc, "Конфигурация_АПВ", this._apv);
                    this.SerializeSlot(doc, "Конфигурация_АВР", this._avr);
                    this.SerializeSlot(doc, "Конфигурация_ЛЗШ", this._lpb);
                }
                this.SerializeSlot(doc, "Конфигурация_всех_УРОВ", this._urovAll);
                this.SerializeSlot(doc, "Конфигурация_всей_автоматики_обдува", this._autoblower);
                this.SerializeSlot(doc, "Конфигурация_тепловой_модели", this._termal);
                this.SerializeSlot(doc, "Конфигурация_входных_сигналов", this._inputsygnal);
                this.SerializeSlot(doc, "Конфигурация_осцилографа", this._oscope);
                this.SerializeSlot(doc, "Структура_силового_транформатора", this._powerTrans);
                this.SerializeSlot(doc, "Структура_измерительного_трансформатора", this._measureTrans);
                this.SerializeSlot(doc, "Структура_входных_логических_сигналов", this._inpSygnal);
                this.SerializeSlot(doc, "Структура_выходных_логических_сигналов1", this._elsSygnal1);
                this.SerializeSlot(doc, "Структура_выходных_логических_сигналов2", this._elsSygnal2);
                this.SerializeSlot(doc, "Структура_выходных_логических_сигналов3", this._elsSygnal3);
                this.SerializeSlot(doc, "Структура_выходных_логических_сигналов4", this._elsSygnal4);
                this.SerializeSlot(doc, "Все_защиты", this._currentProtAll);
                this.SerializeSlot(doc, "Параметры_автоматики", this._paramAutomat);
                if (Common.VersionConverter(DeviceVersion) > 2.07)
                {
                    this.SerializeSlot(doc, "Конфигурация_ethernet", this._configEthernet);
                }
                
                doc.Save(binFileName);

                MessageBox.Show("Уставки успешно сохранены.", "Сохранение уставок",
                    MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch
            {
                MessageBox.Show("Уставки не удалось сохранить. Обратитесь к разработчикам.", "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }
        #endregion

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "UDZT запрос подтверждения")
            {
                if (query.fail == 0)
                {
                    MessageBox.Show("Конфигурация записана успешно", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Конфигурация не сохранена", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            #region Конфигурация всех выключателей
            if ("LoadSwitchAll" + DeviceNumber == query.name)
            {
                Raise(query, this.SwitchAllLoadOK, this.SwitchAllLoadFail, ref this._switchAll);
            }
            if ("SaveSwitchAll" + DeviceNumber == query.name)
            {
                Raise(query, this.SwitchAllSaveOK, this.SwitchAllSaveFail);
            }
            #endregion

            #region Конфигурация всех УРОВ
            if ("LoadUrovAll" + DeviceNumber == query.name)
            {
                Raise(query, this.UrovAllLoadOK, this.UrovAllLoadFail, ref this._urovAll);
            }
            if ("SaveUrovAll" + DeviceNumber == query.name)
            {
                Raise(query, this.UrovAllSaveOK, this.UrovAllSaveFail);
            }
            #endregion

            #region Конфигурация всей автоматики обдува
            if ("LoadAutoblower" + DeviceNumber == query.name)
            {
                Raise(query, this.AutoblowerLoadOK, this.AutoblowerLoadFail, ref this._autoblower);
            }
            if ("SaveAutoblower" + DeviceNumber == query.name)
            {
                Raise(query, this.AutoblowerSaveOK, this.AutoblowerSaveFail);
            }
            #endregion

            #region Конфигурация тепловой модели
            if ("LoadTermal" + DeviceNumber == query.name)
            {
                Raise(query, this.TermalLoadOK, this.TermalLoadFail, ref this._termal);
            }
            if ("SaveTermal" + DeviceNumber == query.name)
            {
                Raise(query, this.TermalSaveOK, this.TermalSaveFail);
            }
            #endregion

            #region Конфигурация входных сигналов
            if ("LoadInputSygnal" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSygnalLoadOK, this.InputSygnalLoadFail, ref this._inputsygnal);
            }
            if ("SaveInputSygnal" + DeviceNumber == query.name)
            {
                Raise(query, this.InputSygnalSaveOK, this.InputSygnalSaveFail);
            }
            #endregion

            #region Конфигурация осцилографа
            if ("LoadOscope" + DeviceNumber == query.name)
            {
                Raise(query, this.OscopeLoadOK, this.OscopeLoadFail, ref this._oscope);
            }
            if ("SaveOscope" + DeviceNumber == query.name)
            {
                Raise(query, this.OscopeSaveOK, this.OscopeSaveFail);
            }
            #endregion

            #region Конфигурация ethernet

            if ("LoadConfigEthernet" + DeviceNumber == query.name)
            {
                Raise(query,this.ConfigEthernetLoadOK, this.ConfigEthernetLoadFail, ref this._configEthernet);
            }

            if ("SaveConfigEthernet" + DeviceNumber == query.name)
            {
                Raise(query, this.ConfigEthernetSaveOK, this. ConfigEthernetSaveFail);
            }
            #endregion

            #region Силовой трансформатор
            if ("LoadPowerTrans" + DeviceNumber == query.name)
            {
                Raise(query, this.PowerTransLoadOK, this.PowerTransLoadFail, ref this._powerTrans);
            }
            if ("SavePowerTrans" + DeviceNumber == query.name)
            {
                Raise(query, this.PowerTransSaveOK, this.PowerTransSaveFail);
            }
            #endregion

            #region Данные измерительных трансформаторов
            if ("LoadMeasureTrans" + DeviceNumber == query.name)
            {
                Raise(query, this.MeasureTransLoadOK, this.MeasureTransLoadFail, ref this._measureTrans);
            }
            if ("SaveMeasureTrans" + DeviceNumber == query.name)
            {
                Raise(query, this.MeasureTransSaveOK, this.MeasureTransSaveFail);
            }
            #endregion

            #region Данные входных логических сигналов
            if ("LoadInpSygnal" + DeviceNumber == query.name)
            {
                Raise(query, this.InpSygnalLoadOK, this.InpSygnalLoadFail, ref this._inpSygnal);
            }
            if ("SaveInpSygnal" + DeviceNumber == query.name)
            {
                Raise(query, this.InpSygnalSaveOK, this.InpSygnalSaveFail);
            }

            if ("LoadSwitch" + DeviceNumber == query.name)
            {
                Raise(query, this.SwitchLoadOK, this.SwitchLoadFail, ref this._switch);
            }

            if ("SaveSwitch" + DeviceNumber == query.name)
            {
                Raise(query, this.SwitchSaveOK, this.SwitchSaveFail);
            }

            if ("LoadAPV" + DeviceNumber == query.name)
            {
                Raise(query, this.APVLoadOK, this.APVLoadFail, ref this._apv);
            }

            if ("SaveAPV" + DeviceNumber == query.name)
            {
                Raise(query, this.APVSaveOK, this.APVSaveFail);
            }

            if ("LoadAVR" + DeviceNumber == query.name)
            {
                Raise(query, this.AVRLoadOK, this.AVRLoadFail, ref this._avr);
            }

            if ("SaveAVR" + DeviceNumber == query.name)
            {
                Raise(query, this.AVRSaveOK, this.AVRSaveFail);
            }

            if ("LoadLPB" + DeviceNumber == query.name)
            {
                Raise(query, this.LPBLoadOK, this.LPBLoadFail, ref this._lpb);
            }

            if ("SaveLPB" + DeviceNumber == query.name)
            {
                Raise(query, this.LPBSaveOK, this.LPBSaveFail);
            }
            #endregion

            #region Данные выходных логических сигналов
            if ("LoadElsSygnal1" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalLoadOK1, this.ElsSygnalLoadFail1, ref this._elsSygnal1);
            }
            if ("SaveElsSygnal1" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalSaveOK1, this.ElsSygnalSaveFail1);
            }

            if ("LoadElsSygnal2" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalLoadOK2, this.ElsSygnalLoadFail2, ref this._elsSygnal2);
            }
            if ("SaveElsSygnal2" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalSaveOK2, this.ElsSygnalSaveFail2);
            }

            if ("LoadElsSygnal3" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalLoadOK3, this.ElsSygnalLoadFail3, ref this._elsSygnal3);
            }
            if ("SaveElsSygnal3" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalSaveOK3, this.ElsSygnalSaveFail3);
            }

            if ("LoadElsSygnal4" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalLoadOK4, this.ElsSygnalLoadFail4, ref this._elsSygnal4);
            }
            if ("SaveElsSygnal4" + DeviceNumber == query.name)
            {
                Raise(query, this.ElsSygnalSaveOK4, this.ElsSygnalSaveFail4);
            }
            #endregion

            #region Все защиты

            for (int i = 0; i < this.CurrentProtAll.Count; i++)
            {
                if ("LoadProtAll" + i.ToString() + DeviceNumber == query.name)
                {
                    slot _curProtAll = this.CurrentProtAll[i];
                    if (i == this.CurrentProtAll.Count - 1)
                    {
                        Raise(query, this.CurrentProtLastLoadOK, this.CurrentProtLastLoadFail, ref _curProtAll);
                    }
                    else
                    {
                        Raise(query, this.CurrentProtAllLoadOK, this.CurrentProtAllLoadFail, ref _curProtAll);
                    }
                }
            }
            if ("SaveProtAll" + DeviceNumber + (this.CurrentProtAll.Count - 1).ToString() == query.name)
            {
                Raise(query, this.CurrentProtAllSaveOK, this.CurrentProtAllSaveFail);
            }
            #endregion

            #region Осциллограф
            #region Журнал
            if ("LoadOscilloscopeJournal" + this._recordIndex + DeviceNumber == query.name)
            {
                Raise(query, this.OscJournalLoadOK, this.OscJournalLoadFail, ref this._oscilloscopeJournalReadSlot);
            }
            if ("SaveOscilloscopeJournal" + DeviceNumber == query.name)
            {
                Raise(query, this.OscJournalSaveOK, this.OscJournalSaveFail);
            }
            #endregion

            #region Осцилограмма
            for (int i = 0; i < _oscPageSize / _enableOscPageSize + this._temp; i++)
                if ("LoadOscilloscope" + this._oscIndexRead + i + DeviceNumber == query.name)
                {
                    this._oscilloscopeRead = this.CurrentOscilloscope[i];
                    Raise(query, this.OscilloscopeLoadOK, this.OscilloscopeLoadFail, ref this._oscilloscopeRead/* _oscilloscopeRead*/);
                }

            if ("SaveOscilloscope" + this._oscIndexWrite + DeviceNumber == query.name)
            {
                Raise(query, this.OscilloscopeSaveOK, this.OscilloscopeSaveFail);
            }
            #endregion

            #region Настройки
            if ("LoadOscLength" + DeviceNumber == query.name)
            {
                Raise(query, this.OscLengthLoadOK, this.OscLengthLoadFail, ref this._oscLength);
            }
            #endregion
            #endregion

            #region Системный журнал
            if ("LoadSystemJournalRecord" + this._systemJournalRec + DeviceNumber == query.name)
            {
                this._systemJournalRec++;
                Raise(query, this.SysJournalLoadOK, this.SysJournalLoadFail, ref this._systemJournalRead);
                this.CurrentSystemJournal.Add(this._systemJournalRead);
            }

            if ("SaveSystemJournal" + DeviceNumber == query.name)
            {
                Raise(query, this.SysJournalSaveOK, this.SysJournalSaveFail);
            }
            #endregion

            #region Журнал аварий
            if ("LoadAlarmJournalRecord" + this._alarmJournalRec + DeviceNumber == query.name)
            {
                this._alarmJournalRec++;
                Raise(query, this.AlarmJournalLoadOK, this.AlarmJournalLoadFail, ref this._alarmJournalRead);
                this.CurrentAlarmJournal.Add(this._alarmJournalRead);
            }

            if ("SaveAlarmJournal" + DeviceNumber == query.name)
            {
                Raise(query, this.AlarmJournalSaveOK, this.AlarmJournalSaveFail);
            }
            #endregion

            #region Параметры автоматики
            if ("LoadParamAutomat" + DeviceNumber == query.name)
            {
                Raise(query, this.ParamAutomatLoadOK, this.ParamAutomatLoadFail, ref this._paramAutomat);
            }
            if ("SavePowerTrans" + DeviceNumber == query.name)
            {
                Raise(query, this.ParamAutomatSaveOK, this.ParamAutomatSaveFail);
            }
            #endregion

            #region Дифференциальные защиты
            //if ("LoadDifPower" + DeviceNumber == query.pinName)
            //{
            //    Raise(query, DifPowerLoadOK, DifPowerLoadFail, ref _difPower);
            //}
            //if ("SaveDifPower" + DeviceNumber == query.pinName)
            //{
            //    Raise(query, DifPowerSaveOK, DifPowerSaveFail);
            //}
            #endregion

            #region Измерения
            if ("LoadAnalogBD" + DeviceNumber == query.name)
            {
                Raise(query, this.AnalogBDLoadOK, this.AnalogBDLoadFail, ref this._analogBDSlot);
            }
            if ("LoadBitBD" + DeviceNumber == query.name)
            {
                Raise(query, this.BitBDLoadOK, this.BitBDLoadFail, ref this._bitBDSlot);
            }
            #endregion

            #region Подтверждение изменений
            if ("ConfirmConstraint" + DeviceNumber == query.name)
            {
                Raise(query, this.ConfirmConstraintSaveOK, this.ConfirmConstraintSaveFail);
            }
            #endregion

            #region  Сохранение и старт программы
            if ("SaveProgram_" + DeviceNumber == query.name)
            {
                if (0 != query.fail)
                {
                    if (null != this.ProgramSaveFail)
                    {
                        this.ProgramSaveFail(this);
                    }
                }
                else
                {
                    if (null != this.ProgramSaveOk && query.fail == 0)
                    {
                        this.ProgramSaveOk(this);
                    }
                }
            }
            if ("SaveProgrammPage" + DeviceNumber == query.name)
            {
                if (0 != query.fail)
                {
                    if (null != this.ProgramPageSaveFail)
                    {
                        this.ProgramPageSaveFail(this);
                    }
                }
                else
                {
                    if (null != this.ProgramPageSaveOk)
                    {
                        this.ProgramPageSaveOk(this);
                    }
                }
            }
            if ("SaveProgramStart_" + DeviceNumber == query.name)
            {
                if (0 != query.fail)
                {
                    if (null != this.ProgramStartSaveFail)
                    {
                        this.ProgramStartSaveFail(this);
                    }

                }
                else
                {
                    if (null != this.ProgramStartSaveOk && query.fail == 0)
                    {
                        this.ProgramStartSaveOk(this);
                    }
                }
            }
            #endregion

            #region  Сохранение Исходника
            if ("SaveProgramStorage_" + DeviceNumber == query.name)
            {
                if (0 != query.fail)
                {
                    if (null != this.ProgramStorageSaveFail)
                    {
                        this.ProgramStorageSaveFail(this);
                    }
                }
                else
                {
                    if (null != this.ProgramStorageSaveOk && query.fail == 0)
                    {
                        this.ProgramStorageSaveOk(this);
                    }
                }
            }

            #endregion

            #region  Загрузка исходника
            if ("LoadProgramStorage_" + DeviceNumber == query.name)
            {

                Raise(query, this.ProgramStorageLoadOk, this.ProgramStorageLoadFail, ref this._programStorage);
            }

            #endregion

            #region  Загрузка сигналов
            if ("LoadProgramSignals_" + DeviceNumber == query.name)
            {
                Raise(query, this.ProgramSignalsLoadOk, this.ProgramSignalsLoadFail, ref this._programSignals);
            }

            #endregion

            base.mb_CompleteExchange(sender, query);
        }
    }
}
