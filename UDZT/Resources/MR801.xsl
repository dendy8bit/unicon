<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
<script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "True")
  			result = "��";
 		else if(value.toString().toLowerCase() == "False")
  			result = "���";
		else
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result;
}
</script>
<script type="text/javascript">
	function translateColor(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "Green")
  			result = "�������";
 		else if(value.toString().toLowerCase() == "Red")
  			result = "�������";
		else
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result;
}
</script>
</head>
  <body>
 <h3>���������� ��801. ������ �� <xsl:value-of select="UDZT/Info/Version"/>. ����� <xsl:value-of select="UDZT/DeviceNumber"/>. �������� ������ �������</h3>
  <p></p>

  <h3>������  ��������� ����������������</h3>
  <table border="1" cellspacing="0">
  <td>
	<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT L1, �</th>
	<th><xsl:value-of select="UDZT/TT_L1"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">��������</th>
	<th><xsl:value-of select="UDZT/TT_L1_Binding"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_L1_Polarity"/></th>
	</tr>
	</table>
	<p></p>
   <table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT X1, �</th>
	<th><xsl:value-of select="UDZT/TT_X1"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_X1_Polarity"/></th>
	</tr>
   </table>
	<p></p>
<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT L2, �</th>
	<th><xsl:value-of select="UDZT/TT_L2"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">��������</th>
	<th><xsl:value-of select="UDZT/TT_L2_Binding"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_L2_Polarity"/></th>
	</tr>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT X2, �</th>
	<th><xsl:value-of select="UDZT/TT_X2"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_X2_Polarity"/></th>
	</tr>
	</table>
	<p></p>
<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT L3, �</th>
	<th><xsl:value-of select="UDZT/TT_L3"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">��������</th>
	<th><xsl:value-of select="UDZT/TT_L3_Binding"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_L3_Polarity"/></th>
	</tr>
	</table>
	<p></p>
		<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� ��������� ��� TT X3, �</th>
	<th><xsl:value-of select="UDZT/TT_X3"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">���������� �����������</th>
	<th><xsl:value-of select="UDZT/TT_X3_Polarity"/></th>
	</tr>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">����������� �������������� TH L</th>
	<th><xsl:value-of select="UDZT/TH_L"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">����������� �������������� TH X</th>
	<th><xsl:value-of select="UDZT/TH_X"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">U0</th>
	<th><xsl:value-of select="UDZT/TH_LX_Binding"/></th>
	</tr>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFDEAD">������������� L</th>
	<th><xsl:value-of select="UDZT/PolarityL"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFDEAD">������������� X</th>
	<th><xsl:value-of select="UDZT/PolarityX"/></th>
	</tr>
	</table>
</td>
</table>
	<p></p>
	<h3>������� �������������</h3>
	<table border="1" cellspacing="0">
	<td>
	<b>������� S1</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="32CD32">����������� �������� �������, ���</th>
		<th><xsl:value-of select="UDZT/S1WindingRatedPowerDOUBLE"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">����������� ���������� �������, ��</th>
		<th><xsl:value-of select="UDZT/S1WindingRatedVoltage"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">��� �������</th>
		<th><xsl:value-of select="UDZT/S1WindingType"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">��������� �����</th>
		<th><xsl:value-of select="UDZT/S1Measuring"/></th>
	</tr>
	</table>
	<th>
	<b>������� S2</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="32CD32">����������� �������� �������, ���</th>
		<th><xsl:value-of select="UDZT/S2WindingRatedPowerDOUBLE"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">����������� ���������� �������, ��</th>
		<th><xsl:value-of select="UDZT/S2WindingRatedVoltage"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">��� �������</th>
		<th><xsl:value-of select="UDZT/S2WindingType"/></th>
	</tr>
	<tr>
	  <th bgcolor="32CD32">������ ����������</th>
	  <th><xsl:value-of select="UDZT/S2ConnectionGroup"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">��������� �����</th>
		<th><xsl:value-of select="UDZT/S2Measuring"/></th>
	</tr>
	</table>
	</th>
	<th>
	<b>������� S3</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="32CD32">����������� �������� �������, ���</th>
		<th><xsl:value-of select="UDZT/S3WindingRatedPowerDOUBLE"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">����������� ���������� �������, ��</th>
		<th><xsl:value-of select="UDZT/S3WindingRatedVoltage"/></th>
	</tr>
	<tr>
		<th bgcolor="32CD32">��� �������</th>
		<th><xsl:value-of select="UDZT/S3WindingType"/></th>
	</tr>
	  <tr>
	    <th bgcolor="32CD32">������ ����������</th>
	    <th><xsl:value-of select="UDZT/S3ConnectionGroup"/></th>
	  </tr>
	<tr>
		<th bgcolor="32CD32">��������� �����</th>
		<th><xsl:value-of select="UDZT/S3Measuring"/></th>
	</tr>
	</table>
	</th>
	</td>
	</table>
	<p></p>
	<h3>������� �������</h3>
	<p></p>
	<b>������� ���������� �������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>���������� ������� �</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>

   <xsl:for-each select="UDZT/InpSygnals/ArrayOfString">
     <xsl:if test="position() &lt; 9 ">
   	  <tr>
         <td><xsl:value-of  select="position()"/></td>
			 <td>
			 <xsl:for-each select="string">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>
    </xsl:for-each>
    </table>
	<th>
		<b>���������� ������� ���</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>

   <xsl:for-each select="UDZT/InpSygnals/ArrayOfString">
     <xsl:if test="position() &gt; 8 ">
   	  <tr>
         <td><xsl:value-of  select="position()"/></td>
			 <td>
			 <xsl:for-each select="string">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>
    </xsl:for-each>
    </table>
	</th>
	</td>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<td>
	<table border="1" cellspacing="0">
	<tr bgcolor="FFFFCC">
		 <th>��������� ������ �������</th>
    </tr>
	<xsl:for-each select="UDZT">
		<tr align="center">
			<td><xsl:value-of select="GrUst"/></td>
		 </tr>
	</xsl:for-each>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<tr bgcolor="FFFFCC">
		 <th>����� ���������</th>
    </tr>
	<xsl:for-each select="UDZT">
		<tr align="center">
			<td><xsl:value-of select="SbInd"/></td>
		 </tr>
	</xsl:for-each>
	</table>
	</td>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
	<td>
	<b>�����������. </b><b><xsl:value-of select="UDZT/Switch"/></b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="FFFFCC">����������</th>
		<th><xsl:value-of select="UDZT/SwitchOff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">���������</th>
		<th><xsl:value-of select="UDZT/SwitchOn"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�������������</th>
		<th><xsl:value-of select="UDZT/SwitchError"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����������</th>
		<th><xsl:value-of select="UDZT/SwitchBlock"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t ����.</th>
		<th><xsl:value-of select="UDZT/SwitchTUrov"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">l ����.</th>
		<th><xsl:value-of select="UDZT/SwitchIUrov"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�������</th>
		<th><xsl:value-of select="UDZT/SwitchImpuls"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t �����.</th>
		<th><xsl:value-of select="UDZT/SwitchTUskor"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����. ���.</th>
		<th><xsl:value-of select="UDZT/SwitchKontCep"/></th>
	</tr>
	</table>
	<th>
	<b>����������</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="FFFFCC">���� ���.</th>
		<th><xsl:value-of select="UDZT/SwitchKeyOn"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">���� ����.</th>
		<th><xsl:value-of select="UDZT/SwitchKeyOff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����. ���.</th>
		<th><xsl:value-of select="UDZT/SwitchVneshOn"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">���� ����.</th>
		<th><xsl:value-of select="UDZT/SwitchVneshOff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">������</th>
		<th><xsl:value-of select="UDZT/SwitchButtons"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����</th>
		<th><xsl:value-of select="UDZT/SwitchKey"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�������</th>
		<th><xsl:value-of select="UDZT/SwitchVnesh"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">��������</th>
		<th><xsl:value-of select="UDZT/SwitchSDTU"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����</th>
		<th><xsl:value-of select="UDZT/SwitchBindings"/></th>
	</tr>
	</table>
	</th>
	</td>
	</table>
	<p></p>

	<b>���</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="FFFFCC">�����</th>
		<th><xsl:value-of select="UDZT/APVModes"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����������</th>
		<th><xsl:value-of select="UDZT/APVBlocking"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t ����</th>
		<th><xsl:value-of select="UDZT/APVTBlock"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t �����</th>
		<th><xsl:value-of select="UDZT/APVTReady"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">1 ����</th>
		<th><xsl:value-of select="UDZT/APV1Krat"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">2 ����</th>
		<th><xsl:value-of select="UDZT/APV2Krat"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">��������������</th>
		<th><xsl:value-of select="UDZT/APVOff"/></th>
	</tr>
	</table>
	<p></p>
	<b>���</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="FFFFCC">�� �������</th>
		<th><xsl:value-of select="UDZT/AVRBySignal"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�� ����.</th>
		<th><xsl:value-of select="UDZT/AVRByOff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�� ��������.</th>
		<th><xsl:value-of select="UDZT/AVRBySelfOff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�� ������</th>
		<th><xsl:value-of select="UDZT/AVRByDiff"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">���� ����</th>
		<th><xsl:value-of select="UDZT/AVRSIGNOn"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">����-��</th>
		<th><xsl:value-of select="UDZT/AVRBlocking"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�����</th>
		<th><xsl:value-of select="UDZT/AVRBlockClear"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">��� ������.</th>
		<th><xsl:value-of select="UDZT/AVRResolve"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t ��</th>
		<th><xsl:value-of select="UDZT/AVRTimeOn"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">������</th>
		<th><xsl:value-of select="UDZT/AVRBack"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t ���</th>
		<th><xsl:value-of select="UDZT/AVRTimeBack"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">t ����.</th>
		<th><xsl:value-of select="UDZT/AVRTimeOtkl"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�����</th>
		<th><xsl:value-of select="UDZT/AVRClear"/></th>
	</tr>
	</table>
	<p></p>
	<b>���</b>
	<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="FFFFCC">�����</th>
		<th><xsl:value-of select="UDZT/LZHMode"/></th>
	</tr>
	<tr>
		<th bgcolor="FFFFCC">�������</th>
		<th><xsl:value-of select="UDZT/LZHUstavka"/></th>
	</tr>
	</table>
	<p></p>

	<h3>�������� �������</h3>

	<b>�������� ����</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#00FFFF">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>������, ��</th>
    </tr>

		 <tr align="center">
			<td>1</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>2</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>3</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>4</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>5</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>6</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>7</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>8</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>9</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>10</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>11</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>12</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>13</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>14</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>15</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =45"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>16</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>17</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>
		 <tr align="center">
			<td>18</td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =52"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =53"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Relays/ArrayOfString/string"><xsl:if test="position() =54"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
		 </tr>

	</table>
	<p></p>
	<b>����������</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#00FFFF">
		 <th>�����</th>
		 <th>���</th>
		 <th>������</th>
		 <th>����</th>
    </tr>
	<tr align="center">
			<td>1</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =3">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
	<tr align="center">
			<td>2</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =6">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>3</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =9">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>4</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =12">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
     <tr align="center">
			<td>5</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =15">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>6</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =18">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>7</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =21">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>8</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =24">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>9</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =27">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>10</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =30">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
     <tr align="center">
			<td>11</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =33">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
    <tr align="center">
			<td>12</td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="UDZT/Indicators/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
      <td>
      <xsl:for-each select="UDZT/Indicators/ArrayOfString/string">
        <xsl:if test="position() =36">
          <xsl:if test="current() ='Red'">
            �������<xsl:if test="current() ='Green'">�������</xsl:if>
          </xsl:if>
        </xsl:if>
      </xsl:for-each>
      </td>
	</tr>
	</table>
	<p></p>
	<b>���� �������������</b>
    <table border="1" cellspacing="0">
		<tr>
			<th bgcolor="#00FFFF" align="left">1. ���������� �������������</th>
			<th><xsl:for-each select="UDZT"><xsl:value-of select="OutputN1"/></xsl:for-each></th>
		</tr>
		<tr>
			<th bgcolor="#00FFFF" align="left">2. ����������� �������������</th>
			<th><xsl:for-each select="UDZT"><xsl:value-of select="OutputN2"/></xsl:for-each></th>
		</tr>
		<tr>
			<th bgcolor="#00FFFF" align="left">3. ������������� ���������</th>
			<th><xsl:for-each select="UDZT"><xsl:value-of select="OutputN3"/></xsl:for-each></th>
		</tr>
		<tr>
			<th bgcolor="#00FFFF" align="left">4. ������������� �����������</th>
			<th><xsl:for-each select="UDZT"><xsl:value-of select="OutputN4"/></xsl:for-each></th>
		</tr>
		<tr>
			<th bgcolor="#00FFFF" align="center">������, ��</th>
			<th><xsl:for-each select="UDZT"><xsl:value-of select="OutputImp"/></xsl:for-each></th>
		</tr>
	 </table>
		<p></p>
		<b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="#00FFFF">
         <th>�����</th>
         <th>������������</th>
      </tr>

		 <xsl:for-each select="UDZT/VlsXml/ArrayOfString">
		 <tr>
       <td><xsl:value-of select="position()"/></td>
		   <td>
         <xsl:for-each select="string">
           <xsl:value-of select="current()"/>
        |</xsl:for-each>
       </td>
		 </tr>
		 </xsl:for-each>
	 </table>
		<h3>������.</h3>
		<b>��������� ����</b>
		<table border="1" cellspacing="0">
		<td>
		<b>���� ��</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500" align="center">
		 <th></th>
		 <th>I</th>
		 <th>ln</th>
		 <th>l0</th>
		 <th>l2</th>
    </tr>
	 <tr align="center">
   <td>S1</td>
    <td><xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort"><xsl:if test="position() = 1"><xsl:value-of select= "current()" /></xsl:if></xsl:for-each></td>
    <td><xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort"><xsl:if test="position() = 2"><xsl:value-of select= "current()" /></xsl:if></xsl:for-each></td>
    <td><xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort"><xsl:if test="position() = 3"><xsl:value-of select= "current()" /></xsl:if></xsl:for-each></td>
    <td><xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort"><xsl:if test="position() = 4"><xsl:value-of select= "current()" /></xsl:if></xsl:for-each></td>
  </tr>
  <tr align="center">
    <td>S2</td>

    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 5">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 6">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 7">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 8">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>

  </tr>
  <tr align="center">
    <td>S3</td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 9">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 10">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 11">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
    <td>
      <xsl:for-each select="UDZT/CurrentProtAllMainProperty/unsignedShort">
        <xsl:if test="position() = 12">
          <xsl:value-of select= "current()" />
        </xsl:if>
      </xsl:for-each>
    </td>
  </tr>
	</table>
    </td>
    </table>
    <p></p>
		<b>����. ������</b>
		<table border="1" cellspacing="0">
		<td>
		<b>���. ������� ������</b>
		<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FFA500">���������</th>
	<th><xsl:value-of select="UDZT/DTZMode"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">����������</th>
	<th><xsl:value-of select="UDZT/DTZblocking"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">������� l�></th>
	<th><xsl:value-of select="UDZT/DTZConstraint"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">�������� ������� T�></th>
	<th><xsl:value-of select="UDZT/DTZTimeEndurance"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">����</th>
	<th><xsl:value-of select="UDZT/DTZUROV"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">�����������</th>
	<th><xsl:value-of select="UDZT/DTZOsc_1_11"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">���</th>
	<th><xsl:value-of select="UDZT/DTZAPV"/></th>
	</tr>
	<tr>
	<th bgcolor="#FFA500">���</th>
	<th><xsl:value-of select="UDZT/DTZAVR"/></th>
	</tr>
	</table>
	<b>���������� l2/l1. </b><b><xsl:value-of select="UDZT/I2I1Mode"/></b>
		<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="#FFA500">������.����.</th>
		<th><xsl:value-of select="UDZT/I2I1PerBlock"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">l2/l1 (�� ���)</th>
		<th><xsl:value-of select="UDZT/DTZI2I1"/></th>
	</tr>
	</table>
	<b>���������� l5/l1. </b><b><xsl:value-of select="UDZT/I5I1Mode"/></b>
		<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="#FFA500">������.����.</th>
		<th><xsl:value-of select="UDZT/I5I1PerBlock"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">l5/l1 (�� ���)</th>
		<th><xsl:value-of select="UDZT/DTZI5I1"/></th>
	</tr>
	</table>
	<b>�������������� ����������</b>
		<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="#FFA500">l�1 - ������</th>
		<th><xsl:value-of select="UDZT/DTZIb1"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">f1 - ���� �������</th>
		<th><xsl:value-of select="UDZT/DTZK1"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">l�2 - ������</th>
		<th><xsl:value-of select="UDZT/DTZIb2"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">f2 - ���� �������</th>
		<th><xsl:value-of select="UDZT/DTZK2"/></th>
	</tr>
	</table>
		</td>
		</table>
	<p></p>
	<b>����. �������</b>
		<p></p>
		<b>���. ������� ������� ��� ����������</b>
		<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="#FFA500">���������</th>
		<th><xsl:value-of select="UDZT/DTOBTMode"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">����������</th>
		<th><xsl:value-of select="UDZT/DTOBTblocking"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">������� l�>></th>
		<th><xsl:value-of select="UDZT/DTOBTConstraint"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">�������� ������� T�>></th>
		<th><xsl:value-of select="UDZT/DTOBTTimeEndurance"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">������� �� ���������� ���������</th>
		<th><xsl:value-of select="UDZT/DTOBTStepOnInstantValues"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">����</th>
		<th><xsl:value-of select="UDZT/DTOBTUROV"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">�����������</th>
		<th><xsl:value-of select="UDZT/DTOBTOsc_1_11"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">���</th>
		<th><xsl:value-of select="UDZT/DTOBTAPV"/></th>
	</tr>
	<tr>
		<th bgcolor="#FFA500">���</th>
		<th><xsl:value-of select="UDZT/DTOBTAVR"/></th>
	</tr>
	</table>
	<p></p>

		<b>���. ���. ������������������</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
		 <th>�������</th>
		 <th>���������</th>
		 <th>����������</th>
		 <th>l� [l� �������]</th>
		 <th>�������</th>
		 <th>t�, ��</th>
		 <th>l�1 [l� �������]</th>
		 <th>���� f1</th>
		 <th>l�2 [l� �������]</th>
		 <th>���� f2</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
    </tr>
	<tr align="center">
	<td>������� I�0> 1</td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I�0> 2</td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I�0> 3</td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/Dif0Xml/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ I</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
		 <th>�������</th>
		 <th>���������</th>
		 <th>l,l� ��</th>
		 <th>�������</th>
		 <th>U����, B</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������. ����.</th>
		 <th>������</th>
		 <th>��������������</th>
		 <th>t, ��</th>
		 <th>k �����. ���-��</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>���������</th>
		 <th>����� ��������� ty, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
    </tr>
	<tr align="center">
	<td>������� I> 1</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =2">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 2</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
     <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =20">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 3</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =45"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =53"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =54"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =38">
           <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =52"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 4</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =57"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =58"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =59"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =60"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =61"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =62"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =63"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =71"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =64"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =65"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =66"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =67"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =72"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =56">
           <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =55"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =68"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =69"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =70"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 5</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =75"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =76"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =77"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =78"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =79"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =80"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =81"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =89"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =82"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =83"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =84"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =85"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =90"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =74">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =73"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =86"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =87"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =88"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 6</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =93"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =94"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =95"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =96"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =97"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =98"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =99"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =107"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =100"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =101"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =102"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =103"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =108"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =92">
         <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =91"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =104"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =105"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =106"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 7</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =111"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =112"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =113"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =114"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =115"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =116"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =117"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =125"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =118"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =119"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =120"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =121"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =126"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =110">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =109"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =122"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =123"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =124"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I> 8</td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =129"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =130"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =131"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =132"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =133"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =134"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =135"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =143"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =136"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =137"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =138"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =139"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =144"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifIXml/ArrayOfString/string">
        <xsl:if test="position() =128">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =127"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =140"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =141"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifIXml/ArrayOfString/string"><xsl:if test="position() =142"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ I*</b>
		<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
		 <th>�������</th>
		 <th>���������</th>
		 <th>l,l� ��</th>
		 <th>�������</th>
		 <th>U����, B</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������. ����.</th>
		 <th>I*</th>
		 <th>��������������</th>
		 <th>t, ��</th>
		 <th>k �����. ���-��</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>���������</th>
		 <th>����� ��������� ty, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
    </tr>
	<tr align="center">
	<td>������� I*> 1</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =2">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I*> 2</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =20">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I*> 3</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =45"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =53"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =54"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =38">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =52"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I*> 4</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =57"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =58"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =59"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =60"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =61"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =62"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =63"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =71"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =64"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =65"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =66"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =67"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =72"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =56">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =55"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =68"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =69"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =70"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I*> 5</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =75"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =76"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =77"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =78"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =79"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =80"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =81"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =89"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =82"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =83"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =84"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =85"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =90"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =74">
         <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =73"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =86"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =87"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =88"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� I*> 6</td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =93"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =94"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =95"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =96"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =97"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =98"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =99"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =107"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =100"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =101"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =102"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =103"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =108"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string">
        <xsl:if test="position() =92">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =91"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =104"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =105"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifI0Xml/ArrayOfString/string"><xsl:if test="position() =106"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ U></b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
	<th>�������</th>
	<th>���������</th>
	<th>���</th>
	<th>U��, B</th>
	<th>t��, ��</th>
	<th>t��,vc</th>
	<th>U��, �</th>
	<th>�������</th>
	<th>���������� U&#60;5�</th>
	<th>����������</th>
	<th>�����������</th>
	<th>����</th>
	<th>���</th>
	<th>���</th>
	<th>��� ����.</th>
	<th>�����</th>
	</tr>
	<tr align="center">
	<td>������� U> 1</td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string">
        <xsl:if test="position() =7">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U> 2</td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string">
        <xsl:if test="position() =22">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U> 3</td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string">
        <xsl:if test="position() =37">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =45"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U> 4</td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string">
        <xsl:if test="position() =52">
         <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =53"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =54"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =60"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =55"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =56"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =57"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =58"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUBXml/ArrayOfString/string"><xsl:if test="position() =59"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ U&#60;</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
	<th>�������</th>
	<th>���������</th>
	<th>���</th>
	<th>U��, B</th>
	<th>t��, ��</th>
	<th>t��,vc</th>
	<th>U��, �</th>
	<th>�������</th>
	<th>���������� U&#60;5�</th>
	<th>����������</th>
	<th>�����������</th>
	<th>����</th>
	<th>���</th>
	<th>���</th>
	<th>��� ����.</th>
	<th>�����</th>
	</tr>
	<tr align="center">
	<td>������� U&#60; 1</td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string">
        <xsl:if test="position() =7">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U&#60; 2</td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string">
        <xsl:if test="position() =22">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U&#60; 3</td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
    <td>
      <xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string">
        <xsl:if test="position() =37">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =45"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� U&#60; 4</td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string">
        <xsl:if test="position() =52">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =53"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =54"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =60"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =55"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =56"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =57"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =58"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifUMXml/ArrayOfString/string"><xsl:if test="position() =59"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ F></b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
	<th>�������</th>
	<th>���������</th>
	<th>F��, ��</th>
	<th>t��, ��</th>
	<th>t��, ��</th>
	<th>F��, ��</th>
	<th>�������</th>
	<th>����������</th>
	<th>�����������</th>
	<th>����</th>
	<th>���</th>
	<th>���</th>
	<th>��� �����.</th>
	<th>�����</th>
	</tr>
	<tr align="center">
	<td>������� F> 1</td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string">
        <xsl:if test="position() =6">
         <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
  <td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F> 2</td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string">
        <xsl:if test="position() =19">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F> 3</td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string">
        <xsl:if test="position() =32">
          <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F> 4</td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td>
      <xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string">
        <xsl:if test="position() =45">
         <xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if>
        </xsl:if>
      </xsl:for-each>
    </td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =52"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFBXml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������ F&#60;</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
	<th>�������</th>
	<th>���������</th>
	<th>F��, ��</th>
	<th>t��, ��</th>
	<th>t��, ��</th>
	<th>F��, ��</th>
	<th>�������</th>
	<th>����������</th>
	<th>�����������</th>
	<th>����</th>
	<th>���</th>
	<th>���</th>
	<th>��� �����.</th>
	<th>�����</th>
	</tr>
	<tr align="center">
	<td>������� F&#60; 1</td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =6"><xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =13"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =12"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F&#60; 2</td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =14"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =15"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =16"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =17"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =18"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =19"><xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =20"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =26"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =21"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =22"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =23"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =24"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =25"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F&#60; 3</td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =27"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =28"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =29"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =30"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =31"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =32"><xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =33"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =39"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =34"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =35"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =36"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =37"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =38"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	<tr align="center">
	<td>������� F&#60; 4</td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =40"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =41"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =42"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =43"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =44"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
  <td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =45"><xsl:if test="current() ='False'">��� </xsl:if><xsl:if test="current() ='True'">��</xsl:if></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =46"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =52"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =47"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =48"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =49"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =50"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	<td><xsl:for-each select="UDZT/DifFMXml/ArrayOfString/string"><xsl:if test="position() =51"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></td>
	</tr>
	</table>
	<p></p>
	<b>������� ������</b>
	<table border="1" cellspacing="0">
	<tr bgcolor="#FFA500">
	<th>�������</th>
	<th>���������</th>
	<th>����.</th>
	<th>t��, ��</th>
	<th>t��, ��</th>
	<th>����.</th>
	<th>�������</th>
	<th>����-��</th>
	<th>�����������</th>
	<th>����</th>
	<th>���</th>
	<th>���</th>
	<th>��� �����.</th>
	<th>�����</th>
	</tr>
	<xsl:for-each select="UDZT/ExtXml/ExtClass">
	<tr align="center">
	<td>������� <xsl:value-of select="position()"/> </td>
	<td><xsl:value-of select="Pos0"/></td>
	<td><xsl:value-of select="Pos1"/></td>
	<td><xsl:value-of select="Pos2"/></td>
	<td><xsl:value-of select="Pos3"/></td>
	<td><xsl:value-of select="Pos4"/></td>
  <td><xsl:for-each select="Pos5"> <xsl:if test="current() ='False'">���	</xsl:if><xsl:if test="current() ='True'">��</xsl:if></xsl:for-each></td>
	<td><xsl:value-of select="Pos6"/></td>
	<td><xsl:value-of select="Pos12"/></td>
	<td><xsl:value-of select="Pos7"/></td>
	<td><xsl:value-of select="Pos8"/></td>
	<td><xsl:value-of select="Pos9"/></td>
	<td><xsl:value-of select="Pos10"/></td>
	<td><xsl:value-of select="Pos11"/></td>
	</tr>
	</xsl:for-each>
	</table>
	<h3>�����������</h3>
	<p></p>
		<table border="1" cellspacing="0">
	<tr>
		<th bgcolor="#FF69B4">����� � ������������ ������������</th>
		<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =2"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
		<th bgcolor="#FF69B4">�������� ������������</th>
		<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =1"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
		<th bgcolor="#FF69B4">������������ ����������, %</th>
		<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =3"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	</table>
		<p></p>
		<b>��������������� ���������� ������</b>
		<table border="1" cellspacing="0">
	<tr>
	<th bgcolor="#FF69B4">����� 1</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =4"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 2</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =5"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 3</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =6"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 4</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =7"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 5</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =8"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 6</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =9"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 7</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =10"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	<tr>
	<th bgcolor="#FF69B4">����� 8</th>
	<th><xsl:for-each select="UDZT/OscXml/string"><xsl:if test="position() =11"><xsl:value-of select="current()"/></xsl:if></xsl:for-each></th>
	</tr>
	</table>
    <p></p>

    <xsl:if test="UDZT/Info/Version &gt; 2.07">
      <b>������������ Ethernet</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="B0C4DE">
        <th>IP-�����</th>
      </tr>
      <xsl:for-each select="UDZT">
        <tr align="center">
          <td>
            <xsl:value-of select="IpLo1"/>.
            <xsl:value-of select="IpLo2"/>.
            <xsl:value-of select="IpHi1"/>.
            <xsl:value-of select="IpHi2"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    </xsl:if>

  </body>
  </html>
</xsl:template>
</xsl:stylesheet>