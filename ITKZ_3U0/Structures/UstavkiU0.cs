﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class UstavkiU0 : StructBase
    {
        [Layout(0)] private ushort _u0Limit; // (0x002D)
        [Layout(1)] private ushort _u0Time;  // (0x002E)
        [Layout(2)] private ushort _res;     // (0x002F)
        [Layout(3)] private ushort _phase;   // (0x0030)

        [BindingProperty(0)]
        public ushort U0
        {
            get { return this._u0Limit; }
            set { this._u0Limit = value; }
        }

        [BindingProperty(1)]
        public ushort Time
        {
            get { return this._u0Time; }
            set { this._u0Time = value; }
        }

        [BindingProperty(2)]
        public ushort Phase
        {
            get { return this._phase; }
            set { this._phase = value; }
        }
    }
}
