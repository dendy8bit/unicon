﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0.Structures
{
    public class ReleConfig : StructBase
    {
        [Layout(0)] private ushort _status; // (0x000D)

        /// <summary>
        /// Режим выключателя. Если 1 - режим выходного реле, 0 - режим блинкера
        /// </summary>
        public bool ConfigRele
        {
            get { return Common.GetBit(this._status, 12); }
        }
    }
}
