﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class TimeUstavka : StructBase
    {
        [Layout(0)] private ushort _iaAbsLimit; // (0x001D)
        
        [BindingProperty(0)]
        public ushort IaAbsLimit
        {
            get { return this._iaAbsLimit; }
            set { this._iaAbsLimit = value; }
        }
    }
}
