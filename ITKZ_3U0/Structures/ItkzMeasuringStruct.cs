﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class ItkzMeasuringStruct : StructBase
    {
        [Layout(0)] private ushort _status;              // (0x000D)
        [Layout(1)] private ushort _iaValue;             // (0x000E)
        [Layout(2)] private ushort _ibValue;             // (0x000F)
        [Layout(3)] private ushort _icValue;             // (0x0010)
        [Layout(4)] private ushort _iNValue;             // (0x0011)
        [Layout(5)] private ushort _iN5Value;            // (0x0012)
        [Layout(6)] private ushort _iaLastexcessValue;   // (0x0013)
        [Layout(7)] private ushort _ibLastexcessValue;   // (0x0014)
        [Layout(8)] private ushort _icLastexcessValue;   // (0x0015)
        [Layout(9)] private ushort _iNLastexcessValue;   // (0x0016)
        [Layout(10)] private ushort _iN5LastexcessValue; // (0x0017)

        public BitArray Status => new BitArray(BitConverter.GetBytes(this._status));

        public ushort IaValue => this._iaValue;
        public ushort IbValue => this._ibValue;
        public ushort IcValue => this._icValue;
        public ushort InValue => this._iNValue;
        public ushort In5Value => this._iN5Value;
        public ushort IaLastexcessValue => this._iaLastexcessValue;
        public ushort IbLastexcessValue => this._ibLastexcessValue;
        public ushort IcLastexcessValue => this._icLastexcessValue;
        public ushort InLastexcessValue => this._iNLastexcessValue;
        public ushort In5LastexcessValue => this._iN5LastexcessValue;
    }
}
