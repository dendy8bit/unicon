﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class VersionStruct : StructBase
    {
        [Layout(0)] private ushort _v1;
        [Layout(1)] private ushort _v2;
        [Layout(2)] private ushort _v3;

        [BindingProperty(0)]
        public ushort Version1 => this._v1;
        public ushort Version2 => this._v2;
        public ushort Version3 => this._v3;

        public void SetVersion(ushort v1, ushort v2, ushort v3)
        {
            this._v1 = v1;
            this._v2 = v2;
            this._v3 = v3;
        }

        public string VersionFull => string.Format("{0}.{1}.{2}", this._v1, this._v2, this._v3);

    }
}
