﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.ITKZ_3U0.Structures
{
    public class ConfigStruct : StructBase
    {
        [Layout(0)] private ushort _number; // (0x000B)
        [Layout(1)] private ushort _rate;   // (0x000C)
        [BindingProperty(0)]
        public ushort DvNum
        {
            get { return this._number; }
            set { this._number = value; }
        }

        [BindingProperty(1)]
        public string BaundRate
        {
            get
            {
                ushort val = (ushort)(this._rate - 1);
                return Validator.Get(val, Rate);
            }
            set
            {
                ushort val = Validator.Set(value, Rate);
                this._rate = (ushort)(val + 1);
            }
        }

        public static List<string> Rate
        {
            get
            {
                return new List<string>
                {
                    "9600",
                    "19200",
                    "38400",
                    "57600",
                    "115200"
                };
            }
        }

        public static List<ushort> ComandRateList
        {
            get
            {
                return new List<ushort>
                {
                   0x0601,
                   0x0602,
                   0x0603,
                   0x0604,
                   0x0605
                };
            }
        }
    }
}
