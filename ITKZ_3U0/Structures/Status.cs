﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0.Structures
{
    public class Status : StructBase
    {
        [Layout(0)] private ushort _status; // (0x002С)

        /// <summary>
        /// 0 - Состояние ИО напряжения U0
        /// 1 - Состояние СИ напряжения U0
        /// 2 - Режим СИ напряжения: 0 - выведена, 1 - введена
        /// 3 - флаг направления мо=щности при последнем срабатывании ступени 
        /// 4 - флаг достоверности направления мощности In
        /// 5 - флаг направления мо=щности при последнем срабатывании ступени In5
        /// 6 - флаг достоверности направления мощности In5
        /// 7 - флаг направления мо=щности при последнем срабатывании ступени U0
        /// 8 - флаг достоверности направления мощности U0
        /// 9 - последнее значение при квитировании: 0 - очищать, 1 - не очищать
        /// 10 - резерв
        /// 11 - резерв
        /// 12 - Бит автоквитирования В0
        /// 13 - Бит автоквитирования В1
        /// 14 - флаг уставки направленного срабатывания: 0 - к шинам, 1 - от шин
        /// 15 - флаг режима направленного срабатывания: 0 - ненаправленное срабатывание, 1 - направленное срабатывание
        /// </summary>
        public bool[] Stat => Common.GetBitsArray(this._status);

        [BindingProperty(0)]
        public bool UpdateAlarmParam
        {
            get { return !Common.GetBit(this._status, 9); }
            set { this._status = Common.SetBit(this._status, 9, value); }
        }

        [BindingProperty(1)]
        public string StatusAngel
        {
            get { return Validator.Get(this._status, AngelStatus, 10, 11); }
            set { this._status = Validator.Set(value, AngelStatus, this._status, 10, 11); }
        }

        [BindingProperty(2)]
        public string AutoKvitir
        {
            get { return Validator.Get(this._status, AutoKvitTime, 12, 13); }
            set { this._status = Validator.Set(value, AutoKvitTime, this._status, 12, 13); }
        }

        public static List<string> AutoKvitTime
        {
            get
            {
                return new List<string>
                {
                    "Выключено",
                    "10 минут",
                    "1 час",
                    "2 часа"
                };
            }
        }

        public static List<string> AngelStatus
        {
            get
            {
                return new List<string>
                {
                    "180",
                    "240",
                    "250",
                    "260"
                };
            }
        }

        public static List<ushort> ComandAutoKvitList
        {
            get
            {
                return new List<ushort>
                {
                    0xaa00,
                    0xaa01,
                    0xaa02,
                    0xaa03
                };
            }
        }

        public static List<ushort> ComandStatusAnglesList
        {
            get
            {
                return new List<ushort>
                {
                    0x30aa,
                    0x31aa,
                    0x32aa,
                    0x33aa
                };
            }
        }

        public static List<ushort> CommandUpdateParam
        {
            get
            {
                return new List<ushort>
                {
                    0xaa10,
                    0xaa11
                };
            }
        }
    }
}
