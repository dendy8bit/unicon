﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class ItkzMeasuringU0 : StructBase
    {
        [Layout(0)] private ushort _u0Val;        // (0x0031)
        [Layout(1)] private ushort _inu0Phase;    // (0x0032)

        [Layout(2)] private ushort _u0LastValIn;  // (0x0033)
        [Layout(3)] private ushort _u0LastValIn5; // (0x0034)
        [Layout(4)] private ushort _u0LastValU0;  // (0x0035)

        [Layout(5)] private ushort _inLastValIn5; // (0x0036)
        [Layout(6)] private ushort _inLastValU0;  // (0x0037)

        [Layout(7)] private ushort _in5LastIn;    // (0x0038)
        [Layout(8)] private ushort _in5LastU0;    // (0x0039)

        [Layout(9)] private ushort _phIn;         // (0x003A)
        [Layout(10)] private ushort _phIn5;       // (0x003B)
        [Layout(11)] private ushort _phU0;        // (0x003C)
        
        public ushort U0Val
        {
            get { return this._u0Val; }
        }

        public ushort InU0Phase
        {
            get { return this._inu0Phase; }
        }

        public ushort U0LastValIn
        {
            get { return this._u0LastValIn; }
        }

        public ushort U0LastValIn5
        {
            get { return this._u0LastValIn5; }
        }

        public ushort U0LastValU0
        {
            get { return this._u0LastValU0; }
        }

        public ushort InLastValIn5
        {
            get { return this._inLastValIn5; }
        }

        public ushort InLastValU0
        {
            get { return this._inLastValU0; }
        }

        public ushort In5LastIn
        {
            get { return this._in5LastIn; }
        }

        public ushort In5LastU0
        {
            get { return this._in5LastU0; }
        }

        public ushort PhIn
        {
            get { return this._phIn; }
        }

        public ushort PhIn5
        {
            get { return this._phIn5; }
        }

        public ushort PhU0
        {
            get { return this._phU0; }
        }
    }
}
