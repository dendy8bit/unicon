﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.ITKZ_3U0.Structures
{
    public class Blinker : StructBase
    {
        [Layout(0)] private ushort _blinker; // (0x001E)

        public BitArray Status => new BitArray(BitConverter.GetBytes(this._blinker));
    }
}
