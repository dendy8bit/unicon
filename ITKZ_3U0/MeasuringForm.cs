﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Interfaces;

namespace BEMN.ITKZ_3U0
{
    public partial class ItkzMeasuringForm : Form, IFormView
    {
        private readonly ItkzDeviceU0 _device;
        private readonly LedControl[] _statusLeds;
        private readonly LedControl[] _statesU0;
        private readonly LedControl[] _blinkerLeds;

        private string _message = "";
        private string _caption = "";
        private bool _bugKirilla;

        private enum Command
        {
            NONE,
            KVITIROVANIE,
            ENTER_IN,
            REMOVE_IN,
            ENTER_IN5,
            REMOVE_IN5,
            ENTER_U0,
            REMOVE_U0,
            SET_DIR_ON,
            SET_DIR_OFF,
            SET_DIR_1,
            SET_DIR_0
        }

        private Command _currentCmd;

        public ItkzMeasuringForm()
        {
            this.Multishow = false;
            this.InitializeComponent();
        }

        public ItkzMeasuringForm(ItkzDeviceU0 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this.Multishow = false;

            if (_device.Version1 < 5)
            {
                this._statesU0 = new[]
                {
                    this._statusLedU0, this._stupenU0, this._modU0, this._directionIn, this._dostoverIn,
                    this._directionIn5, this._dostoverIn5, this._directionU0, this._dostoverU0, new LedControl(),
                    new LedControl(), new LedControl(), new LedControl(), new LedControl()
                };
            }
            else
            {
                this._statesU0 = new[]
                {
                    this._statusLedU0, this._stupenU0, this._modU0, this._directionIn, this._dostoverIn,
                    this._directionIn5, this._dostoverIn5, this._directionU0, this._dostoverU0, new LedControl(),
                    new LedControl(), new LedControl(), new LedControl(), new LedControl(), this._modUstDir, this._modInOutDir
                };
            }
            
            this._statusLeds = new[]
            {
                this._statusLed1, this._statusLed2, this._statusLed3, this._statusLedN,
                this._statusLedN5, this._stupen1, this._stupen2, this._stupen3, this._stupenN, this._stupenN5,
                this._modN, this._modN5, new LedControl(), this._relayLed
            };

            this._blinkerLeds = new[]
            {
                this._blinkerLed1, this._blinkerLed2, this._blinkerLed3, this._blinkerLed4, this._blinkerLed5, this._blinkerLed6
            };

            this._currentCmd = Command.NONE;
            this._device.Measuring.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadOk);
            this._device.Measuring.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadFail);

            this._device.MeasuringU0.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringU0ReadOk);
            this._device.MeasuringU0.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringReadU0Fail);

            this._device.StatusU0.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => LedManager.SetLeds(this._statesU0, this._device.StatusU0.Value.Stat));
            this._device.StatusU0.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => LedManager.TurnOffLeds(this._statesU0));

            this._device.Command.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandOk);
            this._device.Command.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandFail);

            if (this._device.VersionInIntType >= 530)
            {
                this._device.Blinker.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.BlinkerReadOk);
                this._device.Blinker.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.BlinkerReadFail);
            }

            if (this._device.Version1 != 5 || this._device.Version == null)
            {
                groupBox5.Enabled = false;
                groupBox6.Enabled = false;
            }
        }

        private void SendCommandOk()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Квитирование выполнено успешно", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.ENTER_IN:
                    MessageBox.Show(@"Режим ступени индикации In введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.REMOVE_IN:
                    MessageBox.Show(@"Режим ступени индикации In выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.ENTER_IN5:
                    MessageBox.Show(@"Режим ступени индикации In5 введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.REMOVE_IN5:
                    MessageBox.Show(@"Режим ступени индикации In5 выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.ENTER_U0:
                    MessageBox.Show(@"Режим ступени индикации U0 введен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.REMOVE_U0:
                    MessageBox.Show(@"Режим ступени индикации U0 выведен успешно", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_ON:

                    if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5) && this._bugKirilla)
                    {
                        //this._currentCmd = Command.KVITIROVANIE;
                        //SendCmd(0xAAAA);
                        MessageBox.Show(@"Режим направленного срабатывания введен успешно", 
                                        @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //this._bugKirilla = false;
                    }

                    else
                    {
                        MessageBox.Show(@"Режим направленного срабатывания введен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    break;
                case Command.SET_DIR_OFF:

                    if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5) && this._bugKirilla)
                    {
                        //this._currentCmd = Command.KVITIROVANIE;
                        //SendCmd(0xAAAA);
                        MessageBox.Show(@"Режим направленного срабатывания выведен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                    }

                    else
                    {
                        MessageBox.Show(@"Режим направленного срабатывания выведен успешно", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    break;
                case Command.SET_DIR_1:

                    if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5))
                    {
                        //this._currentCmd = Command.KVITIROVANIE;
                        //SendCmd(0xAAAA);
                        this._bugKirilla = true;
                        MessageBox.Show(@"Установленно направление ""От шин"" ", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }

                    else
                    {
                        MessageBox.Show(@"Установленно направление ""От шин"" ", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    break;
                case Command.SET_DIR_0:

                    if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5))
                    {
                        //this._currentCmd = Command.KVITIROVANIE;
                        //SendCmd(0xAAAA);
                        this._bugKirilla = true;
                        MessageBox.Show(@"Установленно направление ""К шинам"" ", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                    }

                    else
                    {
                        MessageBox.Show(@"Установленно направление ""К шинам"" ", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    
                    break;
            }
            this.Process = false;
        }

        private void SendCommandFail()
        {
            switch (this._currentCmd)
            {
                case Command.KVITIROVANIE:
                    MessageBox.Show(@"Невозможно выполнить квитирование", @"Квитирование", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.ENTER_IN:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.REMOVE_IN:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.ENTER_IN5:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.REMOVE_IN5:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации In5!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.ENTER_U0:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима ступени индикации U0!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.REMOVE_U0:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима ступени индикации U0!", @"Режим ступени индикации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.SET_DIR_ON:
                    MessageBox.Show(@"Невозможно выполнить команду ввода режима направленного срабатывания", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_OFF:
                    MessageBox.Show(@"Невозможно выполнить команду вывода режима направленного срабатывания", @"Режим направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_1:
                    MessageBox.Show(@"Невозможно выполнить команду установки режима уставки направленного срабатывания", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.SET_DIR_0:
                    MessageBox.Show(@"Невозможно выполнить команду обнуления режима уставки направленного срабатывания", @"Режим уставки направленного срабатывания", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            this.Process = false;
        }
    
        private bool Process
        {
            set
            {
                this._btnModINInput.Enabled = !value;
                this._btnModINOutput.Enabled = !value;
                this._btnModIN5Input.Enabled = !value;
                this._btnModIN5Output.Enabled = !value;
                this._modU0inBtn.Enabled = !value;
                this._modU0outBtn.Enabled = !value;
                this._kvitBtn.Enabled = !value;
                this._btnModDirTrigInp.Enabled = !value;
                this._btnModDirTrigOut.Enabled = !value;
                this._btnModDirTrigOne.Enabled = !value;
                this._btnModDirTrigZero.Enabled = !value;
            }
        }

        private void BlinkerReadOk()
        {
            if (this._device.ReleConfig.Value.ConfigRele) LedManager.SetLeds(this._blinkerLeds, this._device.Blinker.Value.Status); 
            else this.BlinkerReadFail(); // если режим блинкера не выводим значения ledов
        }

        private void BlinkerReadFail()
        {
            LedManager.TurnOffLeds(this._blinkerLeds);
        }



        private void MeasuringReadOk()
        {
            LedManager.SetLeds(this._statusLeds, this._device.Measuring.Value.Status);
            this._curFazaA.Text = this._device.Measuring.Value.IaValue.ToString();
            this._curFazaB.Text = this._device.Measuring.Value.IbValue.ToString();
            this._curFazaC.Text = this._device.Measuring.Value.IcValue.ToString();
            this._curFazaN.Text = this._device.Measuring.Value.InValue.ToString();
            this._curFazaN5.Text = this._device.Measuring.Value.In5Value.ToString();
            this._lastFazaA.Text = this._device.Measuring.Value.IaLastexcessValue.ToString();
            this._lastFazaB.Text = this._device.Measuring.Value.IbLastexcessValue.ToString();
            this._lastFazaC.Text = this._device.Measuring.Value.IcLastexcessValue.ToString();
            this._lastFazaN.Text = this._device.Measuring.Value.InLastexcessValue.ToString();
            this._lastFazaN5.Text = this._device.Measuring.Value.In5LastexcessValue.ToString();
            if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5) && this._bugKirilla)
            {
                this._currentCmd = Command.KVITIROVANIE;
                SendCmd(0xAAAA);
                this._bugKirilla = false;
            }
        }

        private void MeasuringReadFail()
        {
            LedManager.TurnOffLeds(this._statusLeds);
            this._curFazaA.Text = string.Empty;
            this._curFazaB.Text = string.Empty;
            this._curFazaC.Text = string.Empty;
            this._curFazaN.Text = string.Empty;
            this._curFazaN5.Text = string.Empty;
            this._lastFazaA.Text = string.Empty;
            this._lastFazaB.Text = string.Empty;
            this._lastFazaC.Text = string.Empty;
            this._lastFazaN.Text = string.Empty;
            this._lastFazaN5.Text = string.Empty;
        }

        private void MeasuringU0ReadOk()
        {
            LedManager.SetLeds(this._statesU0, this._device.StatusU0.Value.Stat);
            
            if (_modUstDir.State == LedState.Signaled)
            {
                this.stateUstNaprSrabTB.Text = "к шинам";
            }

            if (_modUstDir.State == LedState.NoSignaled)
            {
                this.stateUstNaprSrabTB.Text = "от шин";
            }

            this._curU0.Text = this._device.MeasuringU0.Value.U0Val.ToString();
            this._currentPh.Text = this._device.MeasuringU0.Value.InU0Phase.ToString();

            this._inlastValU0.Text = this._device.MeasuringU0.Value.U0LastValIn.ToString();
            this._in5lastValU0.Text = this._device.MeasuringU0.Value.U0LastValIn5.ToString();
            this._u0lastValU0.Text = this._device.MeasuringU0.Value.U0LastValU0.ToString();

            this._in5lastValIn.Text = this._device.MeasuringU0.Value.InLastValIn5.ToString();
            this._u0lastValn.Text = this._device.MeasuringU0.Value.InLastValU0.ToString();

            this._inlastValIn5.Text = this._device.MeasuringU0.Value.In5LastIn.ToString();
            this._u0lastValIn5.Text = this._device.MeasuringU0.Value.In5LastU0.ToString();

            this._angleIn.Text = this._device.MeasuringU0.Value.PhIn.ToString();
            this._angleIn5.Text = this._device.MeasuringU0.Value.PhIn5.ToString();
            this._angleU0.Text = this._device.MeasuringU0.Value.PhU0.ToString();
        }
        private void MeasuringReadU0Fail()
        {
            LedManager.TurnOffLeds(this._statesU0);

            if (_modUstDir.State == LedState.Off)
            {
                this.stateUstNaprSrabTB.Text = string.Empty;
            }

            this._curU0.Text = string.Empty;
            this._currentPh.Text = string.Empty;

            this._inlastValU0.Text = string.Empty;
            this._in5lastValU0.Text = string.Empty;
            this._u0lastValU0.Text = string.Empty;

            this._in5lastValIn.Text = string.Empty;
            this._u0lastValn.Text = string.Empty;

            this._inlastValIn5.Text = string.Empty;
            this._u0lastValIn5.Text = string.Empty;

            this._angleIn.Text = string.Empty;
            this._angleIn5.Text = string.Empty;
            this._angleU0.Text = string.Empty;
        }

        private void ItkzMeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void ItkzMeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.Measuring.RemoveStructQueries();
            this._device.MeasuringU0.RemoveStructQueries();
            this._device.StatusU0.RemoveStructQueries();
            this._device.Blinker.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Measuring.LoadStructCycle();
                this._device.MeasuringU0.LoadStructCycle();
                this._device.StatusU0.LoadStructCycle();
                if (this._device.VersionInIntType >= 530)
                {
                    this._device.Blinker.LoadStructCycle();
                }              
            }
            else
            {
                this._device.Measuring.RemoveStructQueries();
                this._device.MeasuringU0.RemoveStructQueries();
                this._device.StatusU0.RemoveStructQueries();
                this._device.Blinker.RemoveStructQueries();
                this.MeasuringReadFail();
                this.MeasuringReadU0Fail();
                this.BlinkerReadFail();
            }
        }

        private void _btnModINInput_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.ENTER_IN;
            this.SendCmd(0x04AA);
        }

        private void _btnModINOutput_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.REMOVE_IN;
            this.SendCmd(0x0455);
        }

        private void _btnModIN5Input_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.ENTER_IN5;
            this.SendCmd(0x05AA);
        }

        private void _btnModIN5Output_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.REMOVE_IN5;
            this.SendCmd(0x0555);
        }

        private void KvitirovanieClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.KVITIROVANIE;
            this._message = @"Квитирование выполнено успешно";
            this._caption = @"Квитирование";
            this.SendCmd(0xAAAA);
        }

        private void _modU0inBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.ENTER_U0;
            this.SendCmd(0x08AA);
        }

        private void _modU0outBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.REMOVE_U0;
            this.SendCmd(0x0855);
        }
        
        private void _btnModDirTrigZero_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_0;
            this._message = @"Установленно направление ""К шинам"" ";
            this._caption = @"Режим уставки направленного срабатывания";
            this.SendCmd(0x2455);
        }

        private void _btnModDirTrigOne_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.SET_DIR_1;
            this._message = @"Установленно направление ""От шин"" ";
            this._caption = @"Режим уставки направленного срабатывания";
            this.SendCmd(0x24AA);
        }

        private void _btnModDirTrigInp_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._message = @"Режим направленного срабатывания введен успешно";
            this._caption = @"Режим направленного срабатывания";
            this._bugKirilla = true;
            this._currentCmd = Command.SET_DIR_ON;
            this.SendCmd(0x14AA);
        }

        private void _btnModDirTrigOut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._bugKirilla = true;
            this._message = @"Режим направленного срабатывания выведен успешно";
            this._caption = @"Режим направленного срабатывания";
            this._currentCmd = Command.SET_DIR_OFF;
            this.SendCmd(0x1455);
        }

        private void SendCmd(ushort command)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.Process = true;
            this._device.Command.Value.Word = command;

            if (this._device.Version1 == 5 && this._device.Version2 == 2 && (this._device.Version3 == 6 || this._device.Version3 == 5) && this._bugKirilla)
            {
                this._device.Command.SaveStruct6();
            }
            else
            {
                this._device.Command.SaveStruct6();
            }
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof (ItkzDeviceU0); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (ItkzMeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion      
    }
}
