﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Byte;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.ITKZ_3U0.Structures;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0
{
    public partial class ItkzConfigurationForm : Form, IFormView
    {
        #region [Feilds]

        private ItkzDeviceU0 _device;

        private readonly NewStructValidator<UstavkiStruct> _ustavkiValidator; // 1 - A
        private readonly NewStructValidator<ConfigStruct> _devNumValidator;   // B - C

        private readonly NewStructValidator<TimeUstavka> _timeValidator;      // 1D - с версии 5.3.0

        private readonly NewStructValidator<UstavkiU0> _ustavkiU0Validator;   // 2D - 30
        private readonly NewStructValidator<Status> _statusValidator;         // 2C
        
        private readonly MemoryEntity<OneWordStruct> _version;
        private const string VERSION_PO = "Версия ПО - ";

        #endregion [Fields]

        private enum Command
        {
            NONE, //              0.
            CMD_RELAY_BLINKER, // 1. 0xaa12 - изменение состояния бита уставки режима реле аварии (режим блинкера)
            CMD_RELAY_OUT //      2. 0xaa13 - изменение состояния бита уставки режима реле аварии (импульсный режим)
        }

        private Command _currentCmd;


        #region [Constructor's]

        public ItkzConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ItkzConfigurationForm(ItkzDeviceU0 device)
        {
            this.InitializeComponent();

            this._device = device;
            this._version = _device.Version;


            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadVersionComplite);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Ошибка чтения версии!", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            });


            if (this._version != null)
            {
                this._device.StatusU0Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadStatus);
                this._device.StatusU0Config.AllWriteOk +=
                    HandlerHelper.CreateReadArrayHandler(this, this._device.StatusU0Config.SaveStruct);
                this._device.StatusU0Config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                    MessageBox.Show("Невозможно прочитать статус из устройства", "Внимание", MessageBoxButtons.OK,
                        MessageBoxIcon.Error));
            }


            this._device.Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadDevNumOk);
            this._device.Config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно прочитать номер и скорость устройства", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));
            this._device.Config.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.Accept.Value.Word = 0x5555;
                this._device.Accept.SaveStruct6();
            });


            this._device.ConfigurationCommand.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandOk);
            this._device.ConfigurationCommand.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.SendCommandFail);


            this._currentCmd = Command.NONE;


            this._device.Accept.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show(
                    "Запись в устройство прошла успешно.\nЧтобы изменения вступили в силу, требуется отключить питание",
                    "Запись в устройство", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
            this._device.Accept.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Невозможно выполнить команду подтверждения записи", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            });
            this._device.AutoKvit.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Невозможно выполнить установку автоквитирования", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            });


            this._device.Ustavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._device.Ustavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Конфигурация записана", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Information));
            this._device.Ustavki.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения конфигурации", "Чтение", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.Ustavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка записи конфигурации", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Error));


            this._device.UstavkiU0.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadUstaviU0Ok);
            this._device.UstavkiU0.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._device.Ustavki.SaveStruct);
            this._device.UstavkiU0.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка чтения уставок U0", "Чтение", MessageBoxButtons.OK, MessageBoxIcon.Error));
            this._device.UstavkiU0.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Ошибка записи уставок U0", "Запись", MessageBoxButtons.OK, MessageBoxIcon.Error));


            if (this._device.VersionInIntType >= 530)
            {
                this._device.ReleConfig.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReleConfigReadOk);
                this._device.ReleConfig.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReleConfigReadFail);


                this._device.TimeUstavka.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.TimeUstavkaReadOk);
                this._device.TimeUstavka.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                    MessageBox.Show("Невозможно прочитать уставку управления выклюлчателем", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error));
            }
            

            ToolTip toolTip = new ToolTip();
            this._devNumValidator = new NewStructValidator<ConfigStruct>
            (
                toolTip,
                new ControlInfoText(this._devNumBox, new CustomByteRule(1, 247)),
                new ControlInfoCombo(this._baundRateCmb, ConfigStruct.Rate)
            );

            this._statusValidator = new NewStructValidator<Status>(
                toolTip,
                new ControlInfoCheck(this._updateParamCheckBox),
                new ControlInfoCombo(this._sectorAngleCB, Status.AngelStatus),
                new ControlInfoCombo(this._autoKvitCmb, Status.AutoKvitTime)
            );

            this._ustavkiValidator = new NewStructValidator<UstavkiStruct>
            (
                toolTip,
                new ControlInfoText(this._fazaA, new CustomDoubleRule(0.02, 1)),
                new ControlInfoText(this._fazaB, new CustomDoubleRule(0.02, 1)),
                new ControlInfoText(this._fazaC, new CustomDoubleRule(0.02, 1)),
                new ControlInfoText(this._fazaN, new CustomDoubleRule(0.02, 1)),
                new ControlInfoText(this._fazaN5, new CustomDoubleRule(0.02, 1)),
                new ControlInfoText(this._fazatA, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatB, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatC, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatN, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._fazatN5, new CustomUshortRule(10, 30000))
            );

            this._ustavkiU0Validator = new NewStructValidator<UstavkiU0>
            (
                toolTip,
                new ControlInfoText(this._u0Limit, new CustomUshortRule(0, ushort.MaxValue)),
                new ControlInfoText(this._tu0, new CustomUshortRule(10, 30000)),
                new ControlInfoText(this._phase, new CustomUshortRule(0, 360))
            );

            this._timeValidator = new NewStructValidator<TimeUstavka> // С версии 5.3.0
            (
                toolTip,
                new ControlInfoText(this._time, new CustomUshortRule(100, 5000))
            );

            this.HideElements();
        }

        #endregion [Constructor's]

        #region [Handlers]

        private void ReadDevNumOk()
        {
            this._devNumValidator.Set(this._device.Config.Value);
        }

        private void ReadConfigOk()
        {
            this._ustavkiValidator.Set(this._device.Ustavki.Value);
        }

        private void ReadUstaviU0Ok()
        {
            this._ustavkiU0Validator.Set(this._device.UstavkiU0.Value);
        }

        private void ReadStatus()
        {
            this._statusValidator.Set(this._device.StatusU0Config.Value);
        }

        public void ReadVersionComplite()
        {

            if (_device.Version1 < 5 && _device.Version1 < 4)
            {
                this._versionLabel.Text = VERSION_PO + "5.2.3 и ниже";
                return;
            }
            this._versionLabel.Text = VERSION_PO + _device.DeviceVersion;
        }

        private void HideElements()
        {
            if (this._device.DeviceVersion != "5.3.0") // дописывать, если появятся новые версии через ||
            {
                this.ClientSize = new Size(587, 355);
                this.groupBox1.Size = new Size(467, 331);
                this.groupBox1.Location = new Point(12, 12);
                this.groupBoxConfigReleAlarm.Visible = false;
                this.groupBoxBreakerControl.Visible = false;
            } 
            
            if (_device.DeviceVersion == "5.2.4" || _device.DeviceVersion == "5.2.5" ||
                _device.DeviceVersion == "5.2.6" || _device.DeviceVersion == "5.2.7")
            {
                this.GroupBox9.Visible = false;
                this.groupBox1.Size = new Size(467, 286);
                this._aoutoKvitLabel.Location = new Point(213, 193);
                this._autoKvitCmb.Location = new Point(353, 191);
                this.label11.Location = new Point(7, 37);
                this._u0Limit.Location = new Point(91, 34);
                this.label12.Location = new Point(7, 56);
                this._tu0.Location = new Point(91, 53);

                this.groupBox7.Size = new Size(200, 82);
                this.groupBox8.Location = new Point(259, 107);
                this.groupBox8.Size = new Size(200, 78);
                this.label13.Location = new Point(7, 45);
                this._phase.Location = new Point(91, 42);

                this._updateKvitLabel.Visible = false;
                this._updateParamCheckBox.Visible = false;

                this.Size = new Size(603, 344);
            }
            else if (_device.DeviceVersion == "до 5.2.5")
            {
                this.GroupBox9.Visible = false;
                this.groupBox1.Size = new Size(467, 286);
                this._aoutoKvitLabel.Visible = false;
                this._autoKvitCmb.Visible = false;
                this._updateKvitLabel.Visible = false;
                this._updateParamCheckBox.Visible = false;
                this.label11.Location = new Point(7, 37);
                this._u0Limit.Location = new Point(91, 34);
                this.label12.Location = new Point(7, 56);
                this._tu0.Location = new Point(91, 53);

                this.groupBox7.Size = new Size(200, 82);
                this.groupBox8.Location = new Point(259, 107);
                this.groupBox8.Size = new Size(200, 78);
                this.label13.Location = new Point(7, 45);
                this._phase.Location = new Point(91, 42);

                this.Size = new Size(603, 344);
            }
            else if (_device.DeviceVersion == "5.2.8")
            {
                this._updateKvitLabel.Visible = false;
                this._updateParamCheckBox.Visible = false;
                this.groupBox1.Size = new Size(467, 305);

                this.Size = new Size(603, 362);
            }
        }

        private void ItkzConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
        }


        private void ReleConfigReadOk()
        {
            LedManager.SetLed(this._modLedRele, this._device.ReleConfig.Value.ConfigRele);
            if (this._modLedRele.State == LedState.Signaled)
            {
                this._modeReleAlarm.Text = @"Блинкера";
            }
            if (this._modLedRele.State == LedState.NoSignaled)
            {
                this._modeReleAlarm.Text = @"Импульсный";
            }
        }

        private void ReleConfigReadFail()
        {
            LedManager.TurnOffLed(this._modLedRele);
            this._modeReleAlarm.Text = string.Empty;
        }

        private void TimeUstavkaReadOk()
        {
            this._timeValidator.Set(this._device.TimeUstavka.Value);
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Config.LoadStruct();
            this._device.Ustavki.LoadStruct();
            this._device.UstavkiU0.LoadStruct();
            this._device.TimeUstavka.LoadStruct();
            if (this._device.Version1 >= 5)
            {
                this._device.StatusU0Config.LoadStruct();
            }
            this._device.ReleConfig.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 300));
        }

        private void _readBtn_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeBtn_Click(object sender, EventArgs e)
        {
            this.WriteSetpoints();
        }

        private void WriteSetpoints()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string str;
            if (this._device.VersionInIntType >= 530)
            {
                if (this._ustavkiValidator.Check(out str, true) && this._ustavkiU0Validator.Check(out str, true) &&
                    this._timeValidator.Check(out str, true))
                {
                    this._device.Ustavki.Value = this._ustavkiValidator.Get();
                    this._device.UstavkiU0.Value = this._ustavkiU0Validator.Get();
                    this._device.StatusU0Config.Value = this._statusValidator.Get();
                    this._device.TimeUstavka.Value = this._timeValidator.Get();
                    this._device.UstavkiU0.SaveStruct();
                    this._device.StatusU0Config.SaveStruct();
                    this._device.TimeUstavka.SaveStruct(); // с версии 5.3.0

                    if (this._device.Version1 >= 5)
                    {
                        int updateParam = this._updateParamCheckBox.Checked ? 0 : 1;

                        this._device.AutoKvit.Value.Word = Status.ComandAutoKvitList[this._autoKvitCmb.SelectedIndex];
                        this._device.AutoKvit.SaveStruct();
                        this._device.StatusAngles.Value.Word =
                            Status.ComandStatusAnglesList[this._sectorAngleCB.SelectedIndex];
                        this._device.StatusAngles.SaveStruct();
                        this._device.UpdateParamKvit.Value.Word = Status.CommandUpdateParam[updateParam];
                        this._device.UpdateParamKvit.SaveStruct();
                    }
                }
                else
                {
                    MessageBox.Show("Введены неверные уставки. Невозможно записать конфигурацию", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (this._ustavkiValidator.Check(out str, true) && this._ustavkiU0Validator.Check(out str, true))
                {
                    this._device.Ustavki.Value = this._ustavkiValidator.Get();
                    this._device.UstavkiU0.Value = this._ustavkiU0Validator.Get();
                    this._device.StatusU0Config.Value = this._statusValidator.Get();
                    this._device.UstavkiU0.SaveStruct();
                    this._device.StatusU0Config.SaveStruct();

                    if (this._device.Version1 >= 5)
                    {
                        int updateParam = this._updateParamCheckBox.Checked ? 0 : 1;

                        this._device.AutoKvit.Value.Word = Status.ComandAutoKvitList[this._autoKvitCmb.SelectedIndex];
                        this._device.AutoKvit.SaveStruct();
                        this._device.StatusAngles.Value.Word =
                            Status.ComandStatusAnglesList[this._sectorAngleCB.SelectedIndex];
                        this._device.StatusAngles.SaveStruct();
                        this._device.UpdateParamKvit.Value.Word = Status.CommandUpdateParam[updateParam];
                        this._device.UpdateParamKvit.SaveStruct();
                    }
                }
                else
                {
                    MessageBox.Show("Введены неверные уставки. Невозможно записать конфигурацию", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }           
        }

        private void _readDevNumBtn_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.Config.LoadStruct();
        }

        private void _acceptBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._devNumValidator.Check())
            {
                this._device.Config.SaveOneWord(Convert.ToUInt16(this._devNumBox.Text));
            }
            else
            {
                MessageBox.Show("Неверно задан номер устройства", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        private void _acceptRateBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Accept.Value.Word = ConfigStruct.ComandRateList[this._baundRateCmb.SelectedIndex];
            this._device.Accept.SaveStruct();
        }

        private void readFromFileBtn_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (this.openFileDialog.ShowDialog() != DialogResult.OK) return;
            this.Deserialize(this.openFileDialog.FileName, "ITKZ");
        }

        private void writeToFileBtn_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            if (this._device.VersionInIntType < 530) // 5.3.0
            {
                if (this.saveFileDialog.ShowDialog() != DialogResult.OK) return;
                if (this._ustavkiValidator.Check(out string message, true) &&
                    this._devNumValidator.Check(out message, true) &&
                    this._ustavkiU0Validator.Check(out message, true) &&
                    this._statusValidator.Check(out message, true))
                {
                    StructBase ustavki = this._ustavkiValidator.Get();
                    StructBase devNum = this._devNumValidator.Get();
                    StructBase ustavkiU0 = this._ustavkiU0Validator.Get();
                    StructBase autoKvit = this._statusValidator.Get();
                    this.Serialize(this.saveFileDialog.FileName, ustavki, devNum, ustavkiU0, autoKvit, "ITKZ");
                }
                else
                {
                    MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть сохранена.",
                        "Сохранение уставок", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else // >= 5.3.0
            {
                if (this.saveFileDialog.ShowDialog() != DialogResult.OK) return;
                if (this._ustavkiValidator.Check(out string message, true) &&
                    this._devNumValidator.Check(out message, true) &&
                    this._ustavkiU0Validator.Check(out message, true) &&
                    this._statusValidator.Check(out message, true) &&
                    this._timeValidator.Check(out message, true))
                {
                    StructBase ustavki = this._ustavkiValidator.Get();
                    StructBase devNum = this._devNumValidator.Get();
                    StructBase ustavkiU0 = this._ustavkiU0Validator.Get();
                    StructBase autoKvit = this._statusValidator.Get();
                    StructBase time = this._timeValidator.Get(); // Используется с версии 5.3.0
  
                    this.Serialize(this.saveFileDialog.FileName, ustavki, devNum, ustavkiU0, autoKvit, time, "ITKZ");
                }
                else
                {
                    MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть сохранена.",
                        "Сохранение уставок", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }            
        }

        /// <summary>
        /// Для версии >= 5.3.0
        /// </summary>
        private void Serialize(string binFileName, StructBase config, StructBase devNum, StructBase ustavkiU0,
            StructBase status, StructBase time, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                List<ushort> values = new List<ushort>(config.GetValues());
                values.AddRange(devNum.GetValues());
                values.AddRange(ustavkiU0.GetValues());
                values.AddRange(status.GetValues());
                values.AddRange(time.GetValues());

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                MessageBox.Show("Запись конфигурации прошла успешно", "Запись конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка записи конфигурации", "Сохранение крнфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Для версий ниже 5.3.0
        /// </summary>
        private void Serialize(string binFileName, StructBase config, StructBase devNum, StructBase ustavkiU0,
            StructBase status, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));

                List<ushort> values = new List<ushort>(config.GetValues());
                values.AddRange(devNum.GetValues());
                values.AddRange(ustavkiU0.GetValues());
                values.AddRange(status.GetValues());

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                MessageBox.Show("Запись конфигурации прошла успешно", "Запись конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Ошибка записи конфигурации", "Сохранение крнфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Для версии ниже 5.3.0
        /// </summary>
        private void Deserialize(string binFileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                int ustLen = this._ustavkiValidator.Get().GetValues().Length * 2;
                int devNumLen = this._devNumValidator.Get().GetValues().Length * 2;
                int ustU0Len = this._ustavkiU0Validator.Get().GetValues().Length * 2;
                int statusLen = this._statusValidator.Get().GetValues().Length * 2;

                byte[] ustavkiValues = new byte[ustLen];
                byte[] devNumValues = new byte[devNumLen];
                byte[] ustavkiU0Values = new byte[ustU0Len];
                byte[] statusValues = new byte[statusLen];

                Array.ConstrainedCopy(values, 0, ustavkiValues, 0, ustLen);
                Array.ConstrainedCopy(values, ustLen, devNumValues, 0, devNumLen);
                Array.ConstrainedCopy(values, ustLen + devNumLen, ustavkiU0Values, 0, ustU0Len);
                Array.ConstrainedCopy(values, ustLen + devNumLen + ustU0Len, statusValues, 0, statusLen);

                UstavkiStruct ustavki = new UstavkiStruct();
                ustavki.InitStruct(ustavkiValues);
                this._ustavkiValidator.Set(ustavki);

                ConfigStruct devNum = new ConfigStruct();
                devNum.InitStruct(devNumValues);
                this._devNumValidator.Set(devNum);

                UstavkiU0 ustU0 = new UstavkiU0();
                ustU0.InitStruct(ustavkiU0Values);
                this._ustavkiU0Validator.Set(ustU0);

                Status status = new Status();
                status.InitStruct(statusValues);
                this._statusValidator.Set(status);

                if (this._device.VersionInIntType >= 530) // с версии 5.3.0
                {
                    int timeLen = this._timeValidator.Get().GetValues().Length * 2;
                    byte[] timeValues = new byte[timeLen];
                    Array.ConstrainedCopy(values, ustLen + devNumLen + ustU0Len + statusLen, timeValues, 0, timeLen);
                    TimeUstavka time = new TimeUstavka();
                    time.InitStruct(timeValues);
                    this._timeValidator.Set(time);
                }
                
                MessageBox.Show("Файл конфигурации загружен успешно", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Невозможно загрузить файл конфигурации", "Загрузка конфигурации", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region [IFormView Members]
        public Type FormDevice => typeof(ItkzDeviceU0);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(ItkzConfigurationForm);
        public bool ForceShow => false;
        public Image NodeImage => Properties.Resources.config.ToBitmap();
        public string NodeName => "Конфигурация";
        public INodeView[] ChildNodes => new INodeView[] {};
        public bool Deletable => false;
        #endregion [IFormView Members]

        private void ITKZConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteSetpoints();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip) sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteSetpoints();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
            }

        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void ItkzConfigurationForm_Activated(object sender, EventArgs e)
        {
            this._versionLabel.Text = VERSION_PO + this._device.DeviceVersion;
        }

        private void _modBlinkerBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.CMD_RELAY_BLINKER;
            this.SendCmd(0xaa12);
            //this._modeReleAlarm.Text = @"Блинкера";
        }

        private void _modReleOutputBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._currentCmd = Command.CMD_RELAY_OUT;
            this.SendCmd(0xaa13);
            //this._modeReleAlarm.Text = @"Выходного реле";
        }

        private void SendCommandOk()
        {
            switch (this._currentCmd)
            {
                case Command.CMD_RELAY_BLINKER:
                    MessageBox.Show(@"Включен режим блинкера!", @"Конфигурация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case Command.CMD_RELAY_OUT:
                    MessageBox.Show(@"Включен импульсный режим", @"Конфигурация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private void SendCommandFail()
        {
            switch (this._currentCmd)
            {
                case Command.CMD_RELAY_BLINKER:
                    MessageBox.Show(@"Невозможно включить режим блинкера!", @"Конфигурация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case Command.CMD_RELAY_OUT:
                    MessageBox.Show(@"Невозможно включить импульсный режим!", @"Конфигурация", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void SendCmd(ushort command)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.ConfigurationCommand.Value.Word = command;
            this._device.ConfigurationCommand.SaveStruct6();
        }

        private void ItkzConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.ReleConfig.RemoveStructQueries();
        }
    }
}