﻿namespace BEMN.ITKZ_3U0
{
    partial class ItkzMeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label48 = new System.Windows.Forms.Label();
            this._blinkerLed6 = new BEMN.Forms.LedControl();
            this._blinkerLed5 = new BEMN.Forms.LedControl();
            this._blinkerLed4 = new BEMN.Forms.LedControl();
            this._blinkerLed3 = new BEMN.Forms.LedControl();
            this._blinkerLed2 = new BEMN.Forms.LedControl();
            this._blinkerLed1 = new BEMN.Forms.LedControl();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._in5lastValU0 = new System.Windows.Forms.TextBox();
            this._angleIn5 = new System.Windows.Forms.TextBox();
            this._lastFazaN5 = new System.Windows.Forms.TextBox();
            this._in5lastValIn = new System.Windows.Forms.TextBox();
            this._directionIn5 = new BEMN.Forms.LedControl();
            this._dostoverIn5 = new BEMN.Forms.LedControl();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._inlastValU0 = new System.Windows.Forms.TextBox();
            this._angleIn = new System.Windows.Forms.TextBox();
            this._inlastValIn5 = new System.Windows.Forms.TextBox();
            this._lastFazaN = new System.Windows.Forms.TextBox();
            this._dostoverIn = new BEMN.Forms.LedControl();
            this._directionIn = new BEMN.Forms.LedControl();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this._lastFazaC = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this._lastFazaB = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this._lastFazaA = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this._curFazaN5 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this._curFazaN = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this._curFazaC = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this._curFazaB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._statusLed3 = new BEMN.Forms.LedControl();
            this._statusLed2 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._statusLedN = new BEMN.Forms.LedControl();
            this._statusLedN5 = new BEMN.Forms.LedControl();
            this._stupen1 = new BEMN.Forms.LedControl();
            this._stupen2 = new BEMN.Forms.LedControl();
            this._stupen3 = new BEMN.Forms.LedControl();
            this._stupenN = new BEMN.Forms.LedControl();
            this._stupenN5 = new BEMN.Forms.LedControl();
            this.label10 = new System.Windows.Forms.Label();
            this._statusLed1 = new BEMN.Forms.LedControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this._curFazaA = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this._u0lastValU0 = new System.Windows.Forms.TextBox();
            this._angleU0 = new System.Windows.Forms.TextBox();
            this._u0lastValIn5 = new System.Windows.Forms.TextBox();
            this._u0lastValn = new System.Windows.Forms.TextBox();
            this._directionU0 = new BEMN.Forms.LedControl();
            this._dostoverU0 = new BEMN.Forms.LedControl();
            this._statusLedU0 = new BEMN.Forms.LedControl();
            this._stupenU0 = new BEMN.Forms.LedControl();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this._curU0 = new System.Windows.Forms.TextBox();
            this._btnModINInput = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this._btnModINOutput = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._modN = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._modU0outBtn = new System.Windows.Forms.Button();
            this._modU0inBtn = new System.Windows.Forms.Button();
            this._modU0 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._btnModIN5Output = new System.Windows.Forms.Button();
            this._btnModIN5Input = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._relayLed = new BEMN.Forms.LedControl();
            this._kvitBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._modN5 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._currentPh = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._btnModDirTrigOut = new System.Windows.Forms.Button();
            this._btnModDirTrigInp = new System.Windows.Forms.Button();
            this._modInOutDir = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.stateUstNaprSrabTB = new System.Windows.Forms.TextBox();
            this._btnModDirTrigOne = new System.Windows.Forms.Button();
            this._btnModDirTrigZero = new System.Windows.Forms.Button();
            this._modUstDir = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "(срабатывание ступени)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Реле срабатывания";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 60);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ia";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Каналы";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 39);
            this.label6.TabIndex = 0;
            this.label6.Text = "Состояние измер. органа";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label48, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed6, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed5, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed4, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed3, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed2, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this._blinkerLed1, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel10, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel9, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel8, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLed3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this._statusLed2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedN, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this._statusLedN5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this._stupen1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._stupen2, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this._stupen3, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this._stupenN, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this._stupenN5, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._statusLed1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label26, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label7, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel11, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this._statusLedU0, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this._stupenU0, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 4, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.30604F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.828253F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.363297F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.363297F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.79669F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.17122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.17122F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(886, 379);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // label48
            // 
            this.label48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(218, 11);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(50, 26);
            this.label48.TabIndex = 30;
            this.label48.Text = "Блинкер аварий";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _blinkerLed6
            // 
            this._blinkerLed6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed6.Location = new System.Drawing.Point(236, 333);
            this._blinkerLed6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed6.Name = "_blinkerLed6";
            this._blinkerLed6.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed6.State = BEMN.Forms.LedState.Off;
            this._blinkerLed6.TabIndex = 30;
            // 
            // _blinkerLed5
            // 
            this._blinkerLed5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed5.Location = new System.Drawing.Point(236, 256);
            this._blinkerLed5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed5.Name = "_blinkerLed5";
            this._blinkerLed5.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed5.State = BEMN.Forms.LedState.Off;
            this._blinkerLed5.TabIndex = 30;
            // 
            // _blinkerLed4
            // 
            this._blinkerLed4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed4.Location = new System.Drawing.Point(236, 182);
            this._blinkerLed4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed4.Name = "_blinkerLed4";
            this._blinkerLed4.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed4.State = BEMN.Forms.LedState.Off;
            this._blinkerLed4.TabIndex = 30;
            // 
            // _blinkerLed3
            // 
            this._blinkerLed3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed3.Location = new System.Drawing.Point(236, 128);
            this._blinkerLed3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed3.Name = "_blinkerLed3";
            this._blinkerLed3.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed3.State = BEMN.Forms.LedState.Off;
            this._blinkerLed3.TabIndex = 30;
            // 
            // _blinkerLed2
            // 
            this._blinkerLed2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed2.Location = new System.Drawing.Point(236, 93);
            this._blinkerLed2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed2.Name = "_blinkerLed2";
            this._blinkerLed2.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed2.State = BEMN.Forms.LedState.Off;
            this._blinkerLed2.TabIndex = 30;
            // 
            // _blinkerLed1
            // 
            this._blinkerLed1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._blinkerLed1.Location = new System.Drawing.Point(236, 58);
            this._blinkerLed1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._blinkerLed1.Name = "_blinkerLed1";
            this._blinkerLed1.Size = new System.Drawing.Size(13, 13);
            this._blinkerLed1.State = BEMN.Forms.LedState.Off;
            this._blinkerLed1.TabIndex = 30;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label41);
            this.panel10.Controls.Add(this.label24);
            this.panel10.Controls.Add(this.label31);
            this.panel10.Controls.Add(this.label32);
            this.panel10.Controls.Add(this.label33);
            this.panel10.Controls.Add(this.label34);
            this.panel10.Controls.Add(this._in5lastValU0);
            this.panel10.Controls.Add(this._angleIn5);
            this.panel10.Controls.Add(this._lastFazaN5);
            this.panel10.Controls.Add(this._in5lastValIn);
            this.panel10.Controls.Add(this._directionIn5);
            this.panel10.Controls.Add(this._dostoverIn5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(435, 228);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(446, 67);
            this.panel10.TabIndex = 38;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(198, 46);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(104, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "Бит достоверности";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(198, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Бит направления";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 13);
            this.label31.TabIndex = 34;
            this.label31.Text = "3U0, В";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(198, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(64, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Угол, град.";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(5, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(71, 13);
            this.label33.TabIndex = 36;
            this.label33.Text = "In5, 0.01Imax";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 9);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(65, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "In, 0.01Imax";
            // 
            // _in5lastValU0
            // 
            this._in5lastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5lastValU0.Location = new System.Drawing.Point(81, 42);
            this._in5lastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5lastValU0.Name = "_in5lastValU0";
            this._in5lastValU0.ReadOnly = true;
            this._in5lastValU0.Size = new System.Drawing.Size(55, 20);
            this._in5lastValU0.TabIndex = 29;
            this._in5lastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _angleIn5
            // 
            this._angleIn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._angleIn5.Location = new System.Drawing.Point(380, 4);
            this._angleIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._angleIn5.Name = "_angleIn5";
            this._angleIn5.ReadOnly = true;
            this._angleIn5.Size = new System.Drawing.Size(55, 20);
            this._angleIn5.TabIndex = 30;
            this._angleIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _lastFazaN5
            // 
            this._lastFazaN5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaN5.Location = new System.Drawing.Point(81, 23);
            this._lastFazaN5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaN5.Name = "_lastFazaN5";
            this._lastFazaN5.ReadOnly = true;
            this._lastFazaN5.Size = new System.Drawing.Size(55, 20);
            this._lastFazaN5.TabIndex = 31;
            this._lastFazaN5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _in5lastValIn
            // 
            this._in5lastValIn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._in5lastValIn.Location = new System.Drawing.Point(81, 4);
            this._in5lastValIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._in5lastValIn.Name = "_in5lastValIn";
            this._in5lastValIn.ReadOnly = true;
            this._in5lastValIn.Size = new System.Drawing.Size(55, 20);
            this._in5lastValIn.TabIndex = 32;
            this._in5lastValIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _directionIn5
            // 
            this._directionIn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._directionIn5.Location = new System.Drawing.Point(422, 26);
            this._directionIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionIn5.Name = "_directionIn5";
            this._directionIn5.Size = new System.Drawing.Size(13, 13);
            this._directionIn5.State = BEMN.Forms.LedState.Off;
            this._directionIn5.TabIndex = 38;
            // 
            // _dostoverIn5
            // 
            this._dostoverIn5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._dostoverIn5.Location = new System.Drawing.Point(422, 44);
            this._dostoverIn5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverIn5.Name = "_dostoverIn5";
            this._dostoverIn5.Size = new System.Drawing.Size(13, 13);
            this._dostoverIn5.State = BEMN.Forms.LedState.Off;
            this._dostoverIn5.TabIndex = 28;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label40);
            this.panel9.Controls.Add(this.label30);
            this.panel9.Controls.Add(this.label28);
            this.panel9.Controls.Add(this.label29);
            this.panel9.Controls.Add(this.label27);
            this.panel9.Controls.Add(this.label23);
            this.panel9.Controls.Add(this._inlastValU0);
            this.panel9.Controls.Add(this._angleIn);
            this.panel9.Controls.Add(this._inlastValIn5);
            this.panel9.Controls.Add(this._lastFazaN);
            this.panel9.Controls.Add(this._dostoverIn);
            this.panel9.Controls.Add(this._directionIn);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(435, 155);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(446, 65);
            this.panel9.TabIndex = 37;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(198, 48);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(104, 13);
            this.label40.TabIndex = 3;
            this.label40.Text = "Бит достоверности";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(198, 28);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Бит направления";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(5, 47);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(40, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "3U0, В";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(198, 8);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Угол, град.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 13);
            this.label27.TabIndex = 3;
            this.label27.Text = "In5, 0.01Imax";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "In, 0.01Imax";
            // 
            // _inlastValU0
            // 
            this._inlastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._inlastValU0.Location = new System.Drawing.Point(81, 42);
            this._inlastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._inlastValU0.Name = "_inlastValU0";
            this._inlastValU0.ReadOnly = true;
            this._inlastValU0.Size = new System.Drawing.Size(55, 20);
            this._inlastValU0.TabIndex = 2;
            this._inlastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _angleIn
            // 
            this._angleIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._angleIn.Location = new System.Drawing.Point(380, 5);
            this._angleIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._angleIn.Name = "_angleIn";
            this._angleIn.ReadOnly = true;
            this._angleIn.Size = new System.Drawing.Size(55, 20);
            this._angleIn.TabIndex = 2;
            this._angleIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inlastValIn5
            // 
            this._inlastValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._inlastValIn5.Location = new System.Drawing.Point(81, 23);
            this._inlastValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._inlastValIn5.Name = "_inlastValIn5";
            this._inlastValIn5.ReadOnly = true;
            this._inlastValIn5.Size = new System.Drawing.Size(55, 20);
            this._inlastValIn5.TabIndex = 2;
            this._inlastValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _lastFazaN
            // 
            this._lastFazaN.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaN.Location = new System.Drawing.Point(81, 4);
            this._lastFazaN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaN.Name = "_lastFazaN";
            this._lastFazaN.ReadOnly = true;
            this._lastFazaN.Size = new System.Drawing.Size(55, 20);
            this._lastFazaN.TabIndex = 2;
            this._lastFazaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _dostoverIn
            // 
            this._dostoverIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._dostoverIn.Location = new System.Drawing.Point(422, 48);
            this._dostoverIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverIn.Name = "_dostoverIn";
            this._dostoverIn.Size = new System.Drawing.Size(13, 13);
            this._dostoverIn.State = BEMN.Forms.LedState.Off;
            this._dostoverIn.TabIndex = 28;
            // 
            // _directionIn
            // 
            this._directionIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._directionIn.Location = new System.Drawing.Point(422, 29);
            this._directionIn.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionIn.Name = "_directionIn";
            this._directionIn.Size = new System.Drawing.Size(13, 13);
            this._directionIn.State = BEMN.Forms.LedState.Off;
            this._directionIn.TabIndex = 28;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label22);
            this.panel8.Controls.Add(this._lastFazaC);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(435, 120);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(446, 27);
            this.panel8.TabIndex = 36;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(142, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 3;
            this.label22.Text = "А";
            // 
            // _lastFazaC
            // 
            this._lastFazaC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaC.Location = new System.Drawing.Point(81, 3);
            this._lastFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaC.Name = "_lastFazaC";
            this._lastFazaC.ReadOnly = true;
            this._lastFazaC.Size = new System.Drawing.Size(55, 20);
            this._lastFazaC.TabIndex = 2;
            this._lastFazaC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label21);
            this.panel7.Controls.Add(this._lastFazaB);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(435, 85);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(446, 27);
            this.panel7.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(142, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "А";
            // 
            // _lastFazaB
            // 
            this._lastFazaB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lastFazaB.Location = new System.Drawing.Point(81, 3);
            this._lastFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaB.Name = "_lastFazaB";
            this._lastFazaB.ReadOnly = true;
            this._lastFazaB.Size = new System.Drawing.Size(55, 20);
            this._lastFazaB.TabIndex = 2;
            this._lastFazaB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this._lastFazaA);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(435, 51);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(446, 26);
            this.panel6.TabIndex = 34;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(142, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "А";
            // 
            // _lastFazaA
            // 
            this._lastFazaA.Location = new System.Drawing.Point(81, 3);
            this._lastFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._lastFazaA.Name = "_lastFazaA";
            this._lastFazaA.ReadOnly = true;
            this._lastFazaA.Size = new System.Drawing.Size(55, 20);
            this._lastFazaA.TabIndex = 2;
            this._lastFazaA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this._curFazaN5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(283, 228);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(144, 67);
            this.panel5.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(69, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 13);
            this.label19.TabIndex = 3;
            this.label19.Text = "0.001Imax";
            // 
            // _curFazaN5
            // 
            this._curFazaN5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaN5.Location = new System.Drawing.Point(8, 22);
            this._curFazaN5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaN5.Name = "_curFazaN5";
            this._curFazaN5.ReadOnly = true;
            this._curFazaN5.Size = new System.Drawing.Size(55, 20);
            this._curFazaN5.TabIndex = 2;
            this._curFazaN5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this._curFazaN);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(283, 155);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(144, 65);
            this.panel4.TabIndex = 32;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(69, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "0.001Imax";
            // 
            // _curFazaN
            // 
            this._curFazaN.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaN.Location = new System.Drawing.Point(8, 21);
            this._curFazaN.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaN.Name = "_curFazaN";
            this._curFazaN.ReadOnly = true;
            this._curFazaN.Size = new System.Drawing.Size(55, 20);
            this._curFazaN.TabIndex = 2;
            this._curFazaN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this._curFazaC);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(283, 120);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(144, 27);
            this.panel3.TabIndex = 31;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(69, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "А";
            // 
            // _curFazaC
            // 
            this._curFazaC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaC.Location = new System.Drawing.Point(8, 3);
            this._curFazaC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaC.Name = "_curFazaC";
            this._curFazaC.ReadOnly = true;
            this._curFazaC.Size = new System.Drawing.Size(55, 20);
            this._curFazaC.TabIndex = 2;
            this._curFazaC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this._curFazaB);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(283, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(144, 27);
            this.panel2.TabIndex = 30;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(69, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "А";
            // 
            // _curFazaB
            // 
            this._curFazaB.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curFazaB.Location = new System.Drawing.Point(8, 3);
            this._curFazaB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaB.Name = "_curFazaB";
            this._curFazaB.ReadOnly = true;
            this._curFazaB.Size = new System.Drawing.Size(55, 20);
            this._curFazaB.TabIndex = 2;
            this._curFazaB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 257);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "In5";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 94);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Ib";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 129);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(16, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Ic";
            // 
            // _statusLed3
            // 
            this._statusLed3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed3.Location = new System.Drawing.Point(92, 128);
            this._statusLed3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed3.Name = "_statusLed3";
            this._statusLed3.Size = new System.Drawing.Size(13, 13);
            this._statusLed3.State = BEMN.Forms.LedState.Off;
            this._statusLed3.TabIndex = 6;
            // 
            // _statusLed2
            // 
            this._statusLed2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed2.Location = new System.Drawing.Point(92, 93);
            this._statusLed2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed2.Name = "_statusLed2";
            this._statusLed2.Size = new System.Drawing.Size(13, 13);
            this._statusLed2.State = BEMN.Forms.LedState.Off;
            this._statusLed2.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(308, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущ. значение";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 183);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "In";
            // 
            // _statusLedN
            // 
            this._statusLedN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedN.Location = new System.Drawing.Point(92, 182);
            this._statusLedN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedN.Name = "_statusLedN";
            this._statusLedN.Size = new System.Drawing.Size(13, 13);
            this._statusLedN.State = BEMN.Forms.LedState.Off;
            this._statusLedN.TabIndex = 18;
            // 
            // _statusLedN5
            // 
            this._statusLedN5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedN5.Location = new System.Drawing.Point(92, 256);
            this._statusLedN5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedN5.Name = "_statusLedN5";
            this._statusLedN5.Size = new System.Drawing.Size(13, 13);
            this._statusLedN5.State = BEMN.Forms.LedState.Off;
            this._statusLedN5.TabIndex = 17;
            // 
            // _stupen1
            // 
            this._stupen1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen1.Location = new System.Drawing.Point(164, 58);
            this._stupen1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen1.Name = "_stupen1";
            this._stupen1.Size = new System.Drawing.Size(13, 13);
            this._stupen1.State = BEMN.Forms.LedState.Off;
            this._stupen1.TabIndex = 24;
            // 
            // _stupen2
            // 
            this._stupen2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen2.Location = new System.Drawing.Point(164, 93);
            this._stupen2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen2.Name = "_stupen2";
            this._stupen2.Size = new System.Drawing.Size(13, 13);
            this._stupen2.State = BEMN.Forms.LedState.Off;
            this._stupen2.TabIndex = 25;
            // 
            // _stupen3
            // 
            this._stupen3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupen3.Location = new System.Drawing.Point(164, 128);
            this._stupen3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupen3.Name = "_stupen3";
            this._stupen3.Size = new System.Drawing.Size(13, 13);
            this._stupen3.State = BEMN.Forms.LedState.Off;
            this._stupen3.TabIndex = 26;
            // 
            // _stupenN
            // 
            this._stupenN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenN.Location = new System.Drawing.Point(164, 182);
            this._stupenN.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenN.Name = "_stupenN";
            this._stupenN.Size = new System.Drawing.Size(13, 13);
            this._stupenN.State = BEMN.Forms.LedState.Off;
            this._stupenN.TabIndex = 27;
            // 
            // _stupenN5
            // 
            this._stupenN5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenN5.Location = new System.Drawing.Point(164, 256);
            this._stupenN5.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenN5.Name = "_stupenN5";
            this._stupenN5.Size = new System.Drawing.Size(13, 13);
            this._stupenN5.State = BEMN.Forms.LedState.Off;
            this._stupenN5.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(140, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 39);
            this.label10.TabIndex = 23;
            this.label10.Text = "Состояние ступени индикации";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _statusLed1
            // 
            this._statusLed1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLed1.Location = new System.Drawing.Point(92, 58);
            this._statusLed1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLed1.Name = "_statusLed1";
            this._statusLed1.Size = new System.Drawing.Size(13, 13);
            this._statusLed1.State = BEMN.Forms.LedState.Off;
            this._statusLed1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this._curFazaA);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(283, 51);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(144, 26);
            this.panel1.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(69, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "А";
            // 
            // _curFazaA
            // 
            this._curFazaA.Location = new System.Drawing.Point(8, 3);
            this._curFazaA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curFazaA.Name = "_curFazaA";
            this._curFazaA.ReadOnly = true;
            this._curFazaA.Size = new System.Drawing.Size(55, 20);
            this._curFazaA.TabIndex = 2;
            this._curFazaA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(18, 334);
            this.label26.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "3U0";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(552, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(211, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Значение при последнем срабатывании";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label42);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.label36);
            this.panel11.Controls.Add(this.label37);
            this.panel11.Controls.Add(this.label38);
            this.panel11.Controls.Add(this.label39);
            this.panel11.Controls.Add(this._u0lastValU0);
            this.panel11.Controls.Add(this._angleU0);
            this.panel11.Controls.Add(this._u0lastValIn5);
            this.panel11.Controls.Add(this._u0lastValn);
            this.panel11.Controls.Add(this._directionU0);
            this.panel11.Controls.Add(this._dostoverU0);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(435, 303);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(446, 71);
            this.panel11.TabIndex = 38;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(198, 47);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(104, 13);
            this.label42.TabIndex = 3;
            this.label42.Text = "Бит достоверности";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(198, 27);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(94, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "Бит направления";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 47);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(40, 13);
            this.label36.TabIndex = 34;
            this.label36.Text = "3U0, В";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(198, 7);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 13);
            this.label37.TabIndex = 35;
            this.label37.Text = "Угол, град.";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 28);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "In5, 0.01Imax";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 8);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 13);
            this.label39.TabIndex = 37;
            this.label39.Text = "In, 0.01Imax";
            // 
            // _u0lastValU0
            // 
            this._u0lastValU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0lastValU0.Location = new System.Drawing.Point(81, 42);
            this._u0lastValU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0lastValU0.Name = "_u0lastValU0";
            this._u0lastValU0.ReadOnly = true;
            this._u0lastValU0.Size = new System.Drawing.Size(55, 20);
            this._u0lastValU0.TabIndex = 29;
            this._u0lastValU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _angleU0
            // 
            this._angleU0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._angleU0.Location = new System.Drawing.Point(380, 5);
            this._angleU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._angleU0.Name = "_angleU0";
            this._angleU0.ReadOnly = true;
            this._angleU0.Size = new System.Drawing.Size(55, 20);
            this._angleU0.TabIndex = 30;
            this._angleU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _u0lastValIn5
            // 
            this._u0lastValIn5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0lastValIn5.Location = new System.Drawing.Point(81, 23);
            this._u0lastValIn5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0lastValIn5.Name = "_u0lastValIn5";
            this._u0lastValIn5.ReadOnly = true;
            this._u0lastValIn5.Size = new System.Drawing.Size(55, 20);
            this._u0lastValIn5.TabIndex = 31;
            this._u0lastValIn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _u0lastValn
            // 
            this._u0lastValn.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._u0lastValn.Location = new System.Drawing.Point(81, 4);
            this._u0lastValn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._u0lastValn.Name = "_u0lastValn";
            this._u0lastValn.ReadOnly = true;
            this._u0lastValn.Size = new System.Drawing.Size(55, 20);
            this._u0lastValn.TabIndex = 32;
            this._u0lastValn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _directionU0
            // 
            this._directionU0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._directionU0.Location = new System.Drawing.Point(422, 27);
            this._directionU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._directionU0.Name = "_directionU0";
            this._directionU0.Size = new System.Drawing.Size(13, 13);
            this._directionU0.State = BEMN.Forms.LedState.Off;
            this._directionU0.TabIndex = 38;
            // 
            // _dostoverU0
            // 
            this._dostoverU0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._dostoverU0.Location = new System.Drawing.Point(422, 46);
            this._dostoverU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._dostoverU0.Name = "_dostoverU0";
            this._dostoverU0.Size = new System.Drawing.Size(13, 13);
            this._dostoverU0.State = BEMN.Forms.LedState.Off;
            this._dostoverU0.TabIndex = 28;
            // 
            // _statusLedU0
            // 
            this._statusLedU0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._statusLedU0.Location = new System.Drawing.Point(92, 333);
            this._statusLedU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._statusLedU0.Name = "_statusLedU0";
            this._statusLedU0.Size = new System.Drawing.Size(13, 13);
            this._statusLedU0.State = BEMN.Forms.LedState.Off;
            this._statusLedU0.TabIndex = 17;
            // 
            // _stupenU0
            // 
            this._stupenU0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this._stupenU0.Location = new System.Drawing.Point(164, 333);
            this._stupenU0.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this._stupenU0.Name = "_stupenU0";
            this._stupenU0.Size = new System.Drawing.Size(13, 13);
            this._stupenU0.State = BEMN.Forms.LedState.Off;
            this._stupenU0.TabIndex = 28;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label43);
            this.panel12.Controls.Add(this._curU0);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(283, 303);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(144, 71);
            this.panel12.TabIndex = 33;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(69, 30);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(14, 13);
            this.label43.TabIndex = 3;
            this.label43.Text = "В";
            // 
            // _curU0
            // 
            this._curU0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._curU0.Location = new System.Drawing.Point(8, 24);
            this._curU0.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._curU0.Name = "_curU0";
            this._curU0.ReadOnly = true;
            this._curU0.Size = new System.Drawing.Size(55, 20);
            this._curU0.TabIndex = 2;
            this._curU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _btnModINInput
            // 
            this._btnModINInput.Location = new System.Drawing.Point(9, 30);
            this._btnModINInput.Name = "_btnModINInput";
            this._btnModINInput.Size = new System.Drawing.Size(67, 26);
            this._btnModINInput.TabIndex = 16;
            this._btnModINInput.Text = "Ввести";
            this._btnModINInput.UseVisualStyleBackColor = true;
            this._btnModINInput.Click += new System.EventHandler(this._btnModINInput_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 14);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Режим ступени индикации In";
            // 
            // _btnModINOutput
            // 
            this._btnModINOutput.Location = new System.Drawing.Point(82, 30);
            this._btnModINOutput.Name = "_btnModINOutput";
            this._btnModINOutput.Size = new System.Drawing.Size(67, 26);
            this._btnModINOutput.TabIndex = 17;
            this._btnModINOutput.Text = "Вывести";
            this._btnModINOutput.UseVisualStyleBackColor = true;
            this._btnModINOutput.Click += new System.EventHandler(this._btnModINOutput_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._modN);
            this.groupBox1.Controls.Add(this._btnModINOutput);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this._btnModINInput);
            this.groupBox1.Location = new System.Drawing.Point(5, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(186, 60);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            // 
            // _modN
            // 
            this._modN.Location = new System.Drawing.Point(167, 14);
            this._modN.Name = "_modN";
            this._modN.Size = new System.Drawing.Size(13, 13);
            this._modN.State = BEMN.Forms.LedState.Off;
            this._modN.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._modU0outBtn);
            this.groupBox2.Controls.Add(this._modU0inBtn);
            this.groupBox2.Controls.Add(this._modU0);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(12, 492);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(191, 73);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            // 
            // _modU0outBtn
            // 
            this._modU0outBtn.Location = new System.Drawing.Point(82, 35);
            this._modU0outBtn.Name = "_modU0outBtn";
            this._modU0outBtn.Size = new System.Drawing.Size(67, 26);
            this._modU0outBtn.TabIndex = 19;
            this._modU0outBtn.Text = "Вывести";
            this._modU0outBtn.UseVisualStyleBackColor = true;
            this._modU0outBtn.Click += new System.EventHandler(this._modU0outBtn_Click);
            // 
            // _modU0inBtn
            // 
            this._modU0inBtn.Location = new System.Drawing.Point(9, 35);
            this._modU0inBtn.Name = "_modU0inBtn";
            this._modU0inBtn.Size = new System.Drawing.Size(67, 26);
            this._modU0inBtn.TabIndex = 18;
            this._modU0inBtn.Text = "Ввести";
            this._modU0inBtn.UseVisualStyleBackColor = true;
            this._modU0inBtn.Click += new System.EventHandler(this._modU0inBtn_Click);
            // 
            // _modU0
            // 
            this._modU0.Location = new System.Drawing.Point(171, 19);
            this._modU0.Name = "_modU0";
            this._modU0.Size = new System.Drawing.Size(13, 13);
            this._modU0.State = BEMN.Forms.LedState.Off;
            this._modU0.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 19);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(159, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Режим ступени индикации U0";
            // 
            // _btnModIN5Output
            // 
            this._btnModIN5Output.Location = new System.Drawing.Point(82, 35);
            this._btnModIN5Output.Name = "_btnModIN5Output";
            this._btnModIN5Output.Size = new System.Drawing.Size(67, 26);
            this._btnModIN5Output.TabIndex = 17;
            this._btnModIN5Output.Text = "Вывести";
            this._btnModIN5Output.UseVisualStyleBackColor = true;
            this._btnModIN5Output.Click += new System.EventHandler(this._btnModIN5Output_Click);
            // 
            // _btnModIN5Input
            // 
            this._btnModIN5Input.Location = new System.Drawing.Point(9, 35);
            this._btnModIN5Input.Name = "_btnModIN5Input";
            this._btnModIN5Input.Size = new System.Drawing.Size(67, 26);
            this._btnModIN5Input.TabIndex = 16;
            this._btnModIN5Input.Text = "Ввести";
            this._btnModIN5Input.UseVisualStyleBackColor = true;
            this._btnModIN5Input.Click += new System.EventHandler(this._btnModIN5Input_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._relayLed);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(406, 492);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(178, 73);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            // 
            // _relayLed
            // 
            this._relayLed.Location = new System.Drawing.Point(153, 19);
            this._relayLed.Name = "_relayLed";
            this._relayLed.Size = new System.Drawing.Size(13, 13);
            this._relayLed.State = BEMN.Forms.LedState.Off;
            this._relayLed.TabIndex = 8;
            // 
            // _kvitBtn
            // 
            this._kvitBtn.Location = new System.Drawing.Point(777, 397);
            this._kvitBtn.Name = "_kvitBtn";
            this._kvitBtn.Size = new System.Drawing.Size(121, 27);
            this._kvitBtn.TabIndex = 17;
            this._kvitBtn.Text = "Квитирование";
            this._kvitBtn.UseVisualStyleBackColor = true;
            this._kvitBtn.Click += new System.EventHandler(this.KvitirovanieClick);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._modN5);
            this.groupBox4.Controls.Add(this._btnModIN5Output);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this._btnModIN5Input);
            this.groupBox4.Location = new System.Drawing.Point(209, 492);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(191, 73);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            // 
            // _modN5
            // 
            this._modN5.Location = new System.Drawing.Point(172, 19);
            this._modN5.Name = "_modN5";
            this._modN5.Size = new System.Drawing.Size(13, 13);
            this._modN5.State = BEMN.Forms.LedState.Off;
            this._modN5.TabIndex = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 19);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(160, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "Режим ступени индикации In5";
            // 
            // _currentPh
            // 
            this._currentPh.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._currentPh.Location = new System.Drawing.Point(345, 392);
            this._currentPh.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this._currentPh.Name = "_currentPh";
            this._currentPh.ReadOnly = true;
            this._currentPh.Size = new System.Drawing.Size(55, 20);
            this._currentPh.TabIndex = 2;
            this._currentPh.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(406, 396);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 13);
            this.label44.TabIndex = 3;
            this.label44.Text = "градусы";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(223, 396);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(116, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Разность фаз U0 и In";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._btnModDirTrigOut);
            this.groupBox5.Controls.Add(this._btnModDirTrigInp);
            this.groupBox5.Controls.Add(this._modInOutDir);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Location = new System.Drawing.Point(197, 14);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(191, 60);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            // 
            // _btnModDirTrigOut
            // 
            this._btnModDirTrigOut.Location = new System.Drawing.Point(82, 30);
            this._btnModDirTrigOut.Name = "_btnModDirTrigOut";
            this._btnModDirTrigOut.Size = new System.Drawing.Size(67, 26);
            this._btnModDirTrigOut.TabIndex = 19;
            this._btnModDirTrigOut.Text = "Вывести";
            this._btnModDirTrigOut.UseVisualStyleBackColor = true;
            this._btnModDirTrigOut.Click += new System.EventHandler(this._btnModDirTrigOut_Click);
            // 
            // _btnModDirTrigInp
            // 
            this._btnModDirTrigInp.Location = new System.Drawing.Point(9, 30);
            this._btnModDirTrigInp.Name = "_btnModDirTrigInp";
            this._btnModDirTrigInp.Size = new System.Drawing.Size(67, 26);
            this._btnModDirTrigInp.TabIndex = 18;
            this._btnModDirTrigInp.Text = "Ввести";
            this._btnModDirTrigInp.UseVisualStyleBackColor = true;
            this._btnModDirTrigInp.Click += new System.EventHandler(this._btnModDirTrigInp_Click);
            // 
            // _modInOutDir
            // 
            this._modInOutDir.Location = new System.Drawing.Point(172, 14);
            this._modInOutDir.Name = "_modInOutDir";
            this._modInOutDir.Size = new System.Drawing.Size(13, 13);
            this._modInOutDir.State = BEMN.Forms.LedState.Off;
            this._modInOutDir.TabIndex = 10;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 14);
            this.label46.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(152, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Режим направленного сраб.";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.stateUstNaprSrabTB);
            this.groupBox6.Controls.Add(this._btnModDirTrigOne);
            this.groupBox6.Controls.Add(this._btnModDirTrigZero);
            this.groupBox6.Controls.Add(this._modUstDir);
            this.groupBox6.Controls.Add(this.label47);
            this.groupBox6.Location = new System.Drawing.Point(394, 14);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(247, 60);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            // 
            // stateUstNaprSrabTB
            // 
            this.stateUstNaprSrabTB.Location = new System.Drawing.Point(176, 11);
            this.stateUstNaprSrabTB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.stateUstNaprSrabTB.Name = "stateUstNaprSrabTB";
            this.stateUstNaprSrabTB.ReadOnly = true;
            this.stateUstNaprSrabTB.Size = new System.Drawing.Size(65, 20);
            this.stateUstNaprSrabTB.TabIndex = 20;
            this.stateUstNaprSrabTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _btnModDirTrigOne
            // 
            this._btnModDirTrigOne.Location = new System.Drawing.Point(82, 30);
            this._btnModDirTrigOne.Name = "_btnModDirTrigOne";
            this._btnModDirTrigOne.Size = new System.Drawing.Size(67, 26);
            this._btnModDirTrigOne.TabIndex = 19;
            this._btnModDirTrigOne.Text = "От шин";
            this._btnModDirTrigOne.UseVisualStyleBackColor = true;
            this._btnModDirTrigOne.Click += new System.EventHandler(this._btnModDirTrigOne_Click);
            // 
            // _btnModDirTrigZero
            // 
            this._btnModDirTrigZero.Location = new System.Drawing.Point(9, 30);
            this._btnModDirTrigZero.Name = "_btnModDirTrigZero";
            this._btnModDirTrigZero.Size = new System.Drawing.Size(67, 26);
            this._btnModDirTrigZero.TabIndex = 18;
            this._btnModDirTrigZero.Text = "К шинам";
            this._btnModDirTrigZero.UseVisualStyleBackColor = true;
            this._btnModDirTrigZero.Click += new System.EventHandler(this._btnModDirTrigZero_Click);
            // 
            // _modUstDir
            // 
            this._modUstDir.Location = new System.Drawing.Point(153, 41);
            this._modUstDir.Name = "_modUstDir";
            this._modUstDir.Size = new System.Drawing.Size(13, 13);
            this._modUstDir.State = BEMN.Forms.LedState.Off;
            this._modUstDir.TabIndex = 10;
            this._modUstDir.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 14);
            this.label47.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(160, 13);
            this.label47.TabIndex = 3;
            this.label47.Text = "Уставка направленного сраб.";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox1);
            this.groupBox7.Controls.Add(this.groupBox6);
            this.groupBox7.Controls.Add(this.groupBox5);
            this.groupBox7.Location = new System.Drawing.Point(12, 412);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(647, 80);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ступень In";
            // 
            // ItkzMeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 567);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this._kvitBtn);
            this.Controls.Add(this._currentPh);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ItkzMeasuringForm";
            this.Text = "Measuring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzMeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzMeasuringForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _relayLed;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _statusLed3;
        private BEMN.Forms.LedControl _statusLed2;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _statusLed1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private BEMN.Forms.LedControl _statusLedN;
        private BEMN.Forms.LedControl _statusLedN5;
        private System.Windows.Forms.Label label10;
        private BEMN.Forms.LedControl _stupen1;
        private BEMN.Forms.LedControl _stupen2;
        private BEMN.Forms.LedControl _stupen3;
        private BEMN.Forms.LedControl _stupenN;
        private BEMN.Forms.LedControl _stupenN5;
        private System.Windows.Forms.Button _btnModINInput;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button _btnModINOutput;
        private BEMN.Forms.LedControl _modN;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private BEMN.Forms.LedControl _modN5;
        private System.Windows.Forms.Button _btnModIN5Output;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _btnModIN5Input;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox _lastFazaN;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox _lastFazaC;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox _lastFazaB;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox _lastFazaA;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _curFazaN5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox _curFazaN;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _curFazaC;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox _curFazaB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _curFazaA;
        private System.Windows.Forms.Button _kvitBtn;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox4;
        private Forms.LedControl _modU0;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox _inlastValU0;
        private System.Windows.Forms.TextBox _angleIn;
        private System.Windows.Forms.TextBox _inlastValIn5;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox _in5lastValU0;
        private System.Windows.Forms.TextBox _angleIn5;
        private System.Windows.Forms.TextBox _lastFazaN5;
        private System.Windows.Forms.TextBox _in5lastValIn;
        private Forms.LedControl _directionIn5;
        private Forms.LedControl _directionIn;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox _u0lastValU0;
        private System.Windows.Forms.TextBox _angleU0;
        private System.Windows.Forms.TextBox _u0lastValIn5;
        private System.Windows.Forms.TextBox _u0lastValn;
        private Forms.LedControl _directionU0;
        private Forms.LedControl _statusLedU0;
        private Forms.LedControl _stupenU0;
        private System.Windows.Forms.Button _modU0outBtn;
        private System.Windows.Forms.Button _modU0inBtn;
        private System.Windows.Forms.Label label40;
        private Forms.LedControl _dostoverIn;
        private System.Windows.Forms.Label label41;
        private Forms.LedControl _dostoverIn5;
        private System.Windows.Forms.Label label42;
        private Forms.LedControl _dostoverU0;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox _currentPh;
        private System.Windows.Forms.TextBox _curU0;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _btnModDirTrigOut;
        private System.Windows.Forms.Button _btnModDirTrigInp;
        private Forms.LedControl _modInOutDir;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button _btnModDirTrigOne;
        private System.Windows.Forms.Button _btnModDirTrigZero;
        private Forms.LedControl _modUstDir;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox stateUstNaprSrabTB;
        private System.Windows.Forms.Label label48;
        private Forms.LedControl _blinkerLed6;
        private Forms.LedControl _blinkerLed5;
        private Forms.LedControl _blinkerLed4;
        private Forms.LedControl _blinkerLed3;
        private Forms.LedControl _blinkerLed2;
        private Forms.LedControl _blinkerLed1;
    }
}