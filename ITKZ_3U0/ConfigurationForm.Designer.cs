﻿namespace BEMN.ITKZ_3U0
{
    partial class ItkzConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._writeBtn = new System.Windows.Forms.Button();
            this.readFromFileBtn = new System.Windows.Forms.Button();
            this.writeToFileBtn = new System.Windows.Forms.Button();
            this._readBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._acceptRateBtn = new System.Windows.Forms.Button();
            this._baundRateCmb = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._devNumBox = new System.Windows.Forms.MaskedTextBox();
            this._acceptBtn = new System.Windows.Forms.Button();
            this._readDevNumBtn = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._updateParamCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBoxBreakerControl = new System.Windows.Forms.GroupBox();
            this._time = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBoxConfigReleAlarm = new System.Windows.Forms.GroupBox();
            this._modeReleAlarm = new System.Windows.Forms.MaskedTextBox();
            this._modReleOutputBtn = new System.Windows.Forms.Button();
            this._modBlinkerBtn = new System.Windows.Forms.Button();
            this._modLedRele = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._updateKvitLabel = new System.Windows.Forms.Label();
            this.GroupBox9 = new System.Windows.Forms.GroupBox();
            this._sectorAngleCB = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._autoKvitCmb = new System.Windows.Forms.ComboBox();
            this._aoutoKvitLabel = new System.Windows.Forms.Label();
            this._versionLabel = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._phase = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._u0Limit = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this._tu0 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this._fazatC = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._fazatB = new System.Windows.Forms.MaskedTextBox();
            this._fazatN5 = new System.Windows.Forms.MaskedTextBox();
            this._fazatA = new System.Windows.Forms.MaskedTextBox();
            this._fazatN = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._fazaN5 = new System.Windows.Forms.MaskedTextBox();
            this._fazaN = new System.Windows.Forms.MaskedTextBox();
            this._fazaC = new System.Windows.Forms.MaskedTextBox();
            this._fazaB = new System.Windows.Forms.MaskedTextBox();
            this._fazaA = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxBreakerControl.SuspendLayout();
            this.groupBoxConfigReleAlarm.SuspendLayout();
            this.GroupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // _writeBtn
            // 
            this._writeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeBtn.Location = new System.Drawing.Point(6, 382);
            this._writeBtn.Name = "_writeBtn";
            this._writeBtn.Size = new System.Drawing.Size(194, 23);
            this._writeBtn.TabIndex = 1;
            this._writeBtn.Text = "Записать уставки в устройство";
            this.toolTip1.SetToolTip(this._writeBtn, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeBtn.UseVisualStyleBackColor = true;
            this._writeBtn.Click += new System.EventHandler(this._writeBtn_Click);
            // 
            // readFromFileBtn
            // 
            this.readFromFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.readFromFileBtn.Location = new System.Drawing.Point(259, 353);
            this.readFromFileBtn.Name = "readFromFileBtn";
            this.readFromFileBtn.Size = new System.Drawing.Size(200, 23);
            this.readFromFileBtn.TabIndex = 6;
            this.readFromFileBtn.Text = "Загрузить конфигурацию из файла";
            this.toolTip1.SetToolTip(this.readFromFileBtn, "Загрузить конфигурацию из файла (CTRL+O)");
            this.readFromFileBtn.UseVisualStyleBackColor = true;
            this.readFromFileBtn.Click += new System.EventHandler(this.readFromFileBtn_Click);
            // 
            // writeToFileBtn
            // 
            this.writeToFileBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.writeToFileBtn.Location = new System.Drawing.Point(259, 382);
            this.writeToFileBtn.Name = "writeToFileBtn";
            this.writeToFileBtn.Size = new System.Drawing.Size(200, 23);
            this.writeToFileBtn.TabIndex = 6;
            this.writeToFileBtn.Text = "Сохранить конфигурацию в файл";
            this.toolTip1.SetToolTip(this.writeToFileBtn, "Сохранить конфигурацию в файл (CTRL+S)");
            this.writeToFileBtn.UseVisualStyleBackColor = true;
            this.writeToFileBtn.Click += new System.EventHandler(this.writeToFileBtn_Click);
            // 
            // _readBtn
            // 
            this._readBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBtn.Location = new System.Drawing.Point(6, 353);
            this._readBtn.Name = "_readBtn";
            this._readBtn.Size = new System.Drawing.Size(194, 23);
            this._readBtn.TabIndex = 6;
            this._readBtn.Text = "Прочитать уставки из устройства";
            this.toolTip1.SetToolTip(this._readBtn, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readBtn.UseVisualStyleBackColor = true;
            this._readBtn.Click += new System.EventHandler(this._readBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this._readDevNumBtn);
            this.groupBox2.Location = new System.Drawing.Point(485, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(93, 222);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Параметры";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._acceptRateBtn);
            this.groupBox4.Controls.Add(this._baundRateCmb);
            this.groupBox4.Location = new System.Drawing.Point(6, 103);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(81, 72);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Скорость";
            // 
            // _acceptRateBtn
            // 
            this._acceptRateBtn.Location = new System.Drawing.Point(3, 43);
            this._acceptRateBtn.Name = "_acceptRateBtn";
            this._acceptRateBtn.Size = new System.Drawing.Size(75, 23);
            this._acceptRateBtn.TabIndex = 6;
            this._acceptRateBtn.Text = "Установить";
            this._acceptRateBtn.UseVisualStyleBackColor = true;
            this._acceptRateBtn.Click += new System.EventHandler(this._acceptRateBtn_Click);
            // 
            // _baundRateCmb
            // 
            this._baundRateCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._baundRateCmb.FormattingEnabled = true;
            this._baundRateCmb.Location = new System.Drawing.Point(6, 19);
            this._baundRateCmb.Name = "_baundRateCmb";
            this._baundRateCmb.Size = new System.Drawing.Size(69, 21);
            this._baundRateCmb.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._devNumBox);
            this.groupBox3.Controls.Add(this._acceptBtn);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(81, 79);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Номер";
            // 
            // _devNumBox
            // 
            this._devNumBox.Location = new System.Drawing.Point(6, 19);
            this._devNumBox.Name = "_devNumBox";
            this._devNumBox.Size = new System.Drawing.Size(66, 20);
            this._devNumBox.TabIndex = 1;
            // 
            // _acceptBtn
            // 
            this._acceptBtn.Location = new System.Drawing.Point(3, 44);
            this._acceptBtn.Name = "_acceptBtn";
            this._acceptBtn.Size = new System.Drawing.Size(75, 23);
            this._acceptBtn.TabIndex = 0;
            this._acceptBtn.Text = "Установить";
            this._acceptBtn.UseVisualStyleBackColor = true;
            this._acceptBtn.Click += new System.EventHandler(this._acceptBtn_Click);
            // 
            // _readDevNumBtn
            // 
            this._readDevNumBtn.Location = new System.Drawing.Point(9, 191);
            this._readDevNumBtn.Name = "_readDevNumBtn";
            this._readDevNumBtn.Size = new System.Drawing.Size(75, 23);
            this._readDevNumBtn.TabIndex = 0;
            this._readDevNumBtn.Text = "Прочитать";
            this._readDevNumBtn.UseVisualStyleBackColor = true;
            this._readDevNumBtn.Click += new System.EventHandler(this._readDevNumBtn_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "ИТКЗ уставки";
            this.openFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "ИТКЗ уставки";
            this.saveFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._updateParamCheckBox);
            this.groupBox1.Controls.Add(this.groupBoxBreakerControl);
            this.groupBox1.Controls.Add(this.groupBoxConfigReleAlarm);
            this.groupBox1.Controls.Add(this._updateKvitLabel);
            this.groupBox1.Controls.Add(this.GroupBox9);
            this.groupBox1.Controls.Add(this._autoKvitCmb);
            this.groupBox1.Controls.Add(this._aoutoKvitLabel);
            this.groupBox1.Controls.Add(this._versionLabel);
            this.groupBox1.Controls.Add(this.groupBox8);
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.readFromFileBtn);
            this.groupBox1.Controls.Add(this.writeToFileBtn);
            this.groupBox1.Controls.Add(this._writeBtn);
            this.groupBox1.Controls.Add(this._readBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(467, 411);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Уставки";
            // 
            // _updateParamCheckBox
            // 
            this._updateParamCheckBox.AutoSize = true;
            this._updateParamCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._updateParamCheckBox.Location = new System.Drawing.Point(238, 243);
            this._updateParamCheckBox.Name = "_updateParamCheckBox";
            this._updateParamCheckBox.Size = new System.Drawing.Size(15, 14);
            this._updateParamCheckBox.TabIndex = 15;
            this._updateParamCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBoxBreakerControl
            // 
            this.groupBoxBreakerControl.Controls.Add(this._time);
            this.groupBoxBreakerControl.Controls.Add(this.label16);
            this.groupBoxBreakerControl.Controls.Add(this.label15);
            this.groupBoxBreakerControl.Location = new System.Drawing.Point(293, 275);
            this.groupBoxBreakerControl.Name = "groupBoxBreakerControl";
            this.groupBoxBreakerControl.Size = new System.Drawing.Size(138, 69);
            this.groupBoxBreakerControl.TabIndex = 31;
            this.groupBoxBreakerControl.TabStop = false;
            this.groupBoxBreakerControl.Text = "Уставка управление выключателем";
            // 
            // _time
            // 
            this._time.Location = new System.Drawing.Point(55, 41);
            this._time.Mask = "CCCC";
            this._time.Name = "_time";
            this._time.PromptChar = ' ';
            this._time.Size = new System.Drawing.Size(49, 20);
            this._time.TabIndex = 32;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(110, 44);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(21, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "мс";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 44);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Время";
            // 
            // groupBoxConfigReleAlarm
            // 
            this.groupBoxConfigReleAlarm.Controls.Add(this._modeReleAlarm);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modReleOutputBtn);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modBlinkerBtn);
            this.groupBoxConfigReleAlarm.Controls.Add(this._modLedRele);
            this.groupBoxConfigReleAlarm.Controls.Add(this.label59);
            this.groupBoxConfigReleAlarm.Location = new System.Drawing.Point(10, 275);
            this.groupBoxConfigReleAlarm.Name = "groupBoxConfigReleAlarm";
            this.groupBoxConfigReleAlarm.Size = new System.Drawing.Size(277, 69);
            this.groupBoxConfigReleAlarm.TabIndex = 30;
            this.groupBoxConfigReleAlarm.TabStop = false;
            this.groupBoxConfigReleAlarm.Text = "Режим работы реле аварий";
            // 
            // _modeReleAlarm
            // 
            this._modeReleAlarm.Location = new System.Drawing.Point(159, 11);
            this._modeReleAlarm.Name = "_modeReleAlarm";
            this._modeReleAlarm.PromptChar = ' ';
            this._modeReleAlarm.Size = new System.Drawing.Size(93, 20);
            this._modeReleAlarm.TabIndex = 20;
            // 
            // _modReleOutputBtn
            // 
            this._modReleOutputBtn.Location = new System.Drawing.Point(147, 37);
            this._modReleOutputBtn.Name = "_modReleOutputBtn";
            this._modReleOutputBtn.Size = new System.Drawing.Size(126, 26);
            this._modReleOutputBtn.TabIndex = 19;
            this._modReleOutputBtn.Text = "Импульсный";
            this._modReleOutputBtn.UseVisualStyleBackColor = true;
            this._modReleOutputBtn.Click += new System.EventHandler(this._modReleOutputBtn_Click);
            // 
            // _modBlinkerBtn
            // 
            this._modBlinkerBtn.Location = new System.Drawing.Point(6, 37);
            this._modBlinkerBtn.Name = "_modBlinkerBtn";
            this._modBlinkerBtn.Size = new System.Drawing.Size(126, 26);
            this._modBlinkerBtn.TabIndex = 18;
            this._modBlinkerBtn.Text = "Блинкера";
            this._modBlinkerBtn.UseVisualStyleBackColor = true;
            this._modBlinkerBtn.Click += new System.EventHandler(this._modBlinkerBtn_Click);
            // 
            // _modLedRele
            // 
            this._modLedRele.Location = new System.Drawing.Point(258, 11);
            this._modLedRele.Name = "_modLedRele";
            this._modLedRele.Size = new System.Drawing.Size(13, 13);
            this._modLedRele.State = BEMN.Forms.LedState.Off;
            this._modLedRele.TabIndex = 10;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(108, 14);
            this.label59.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(0, 13);
            this.label59.TabIndex = 3;
            // 
            // _updateKvitLabel
            // 
            this._updateKvitLabel.AutoSize = true;
            this._updateKvitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._updateKvitLabel.Location = new System.Drawing.Point(7, 241);
            this._updateKvitLabel.Name = "_updateKvitLabel";
            this._updateKvitLabel.Size = new System.Drawing.Size(173, 26);
            this._updateKvitLabel.TabIndex = 14;
            this._updateKvitLabel.Text = "Обнуление аварийных значений \r\nпри квитировании";
            // 
            // GroupBox9
            // 
            this.GroupBox9.Controls.Add(this._sectorAngleCB);
            this.GroupBox9.Controls.Add(this.label14);
            this.GroupBox9.Location = new System.Drawing.Point(259, 154);
            this.GroupBox9.Name = "GroupBox9";
            this.GroupBox9.Size = new System.Drawing.Size(200, 63);
            this.GroupBox9.TabIndex = 13;
            this.GroupBox9.TabStop = false;
            this.GroupBox9.Text = "Уставка ширины сектора направленного срабатывания";
            // 
            // _sectorAngleCB
            // 
            this._sectorAngleCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sectorAngleCB.FormattingEnabled = true;
            this._sectorAngleCB.Location = new System.Drawing.Point(93, 34);
            this._sectorAngleCB.Name = "_sectorAngleCB";
            this._sectorAngleCB.Size = new System.Drawing.Size(69, 21);
            this._sectorAngleCB.TabIndex = 17;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 37);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Угол, градусы";
            // 
            // _autoKvitCmb
            // 
            this._autoKvitCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._autoKvitCmb.FormattingEnabled = true;
            this._autoKvitCmb.Location = new System.Drawing.Point(147, 216);
            this._autoKvitCmb.Name = "_autoKvitCmb";
            this._autoKvitCmb.Size = new System.Drawing.Size(106, 21);
            this._autoKvitCmb.TabIndex = 12;
            // 
            // _aoutoKvitLabel
            // 
            this._aoutoKvitLabel.AutoSize = true;
            this._aoutoKvitLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this._aoutoKvitLabel.Location = new System.Drawing.Point(7, 216);
            this._aoutoKvitLabel.Name = "_aoutoKvitLabel";
            this._aoutoKvitLabel.Size = new System.Drawing.Size(134, 16);
            this._aoutoKvitLabel.TabIndex = 11;
            this._aoutoKvitLabel.Text = "Автоквитирование";
            // 
            // _versionLabel
            // 
            this._versionLabel.AutoSize = true;
            this._versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._versionLabel.Location = new System.Drawing.Point(6, 193);
            this._versionLabel.Name = "_versionLabel";
            this._versionLabel.Size = new System.Drawing.Size(78, 16);
            this._versionLabel.TabIndex = 8;
            this._versionLabel.Text = "Версия ПО";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._phase);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Location = new System.Drawing.Point(259, 91);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(200, 62);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Уставка угла положительного направления мощности";
            // 
            // _phase
            // 
            this._phase.Location = new System.Drawing.Point(91, 32);
            this._phase.Mask = "CCCCC";
            this._phase.Name = "_phase";
            this._phase.PromptChar = ' ';
            this._phase.Size = new System.Drawing.Size(50, 20);
            this._phase.TabIndex = 16;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "Угол, градусы";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._u0Limit);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this._tu0);
            this.groupBox7.Location = new System.Drawing.Point(259, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(200, 72);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Уставки срабатывания ИО по значению напряжения U0";
            // 
            // _u0Limit
            // 
            this._u0Limit.Location = new System.Drawing.Point(91, 27);
            this._u0Limit.Mask = "CCCCC";
            this._u0Limit.Name = "_u0Limit";
            this._u0Limit.PromptChar = ' ';
            this._u0Limit.Size = new System.Drawing.Size(50, 20);
            this._u0Limit.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Уставка U0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Т, мс";
            // 
            // _tu0
            // 
            this._tu0.Location = new System.Drawing.Point(91, 46);
            this._tu0.Mask = "CCCCC";
            this._tu0.Name = "_tu0";
            this._tu0.PromptChar = ' ';
            this._tu0.Size = new System.Drawing.Size(50, 20);
            this._tu0.TabIndex = 13;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this._fazatC);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this._fazatB);
            this.groupBox5.Controls.Add(this._fazatN5);
            this.groupBox5.Controls.Add(this._fazatA);
            this.groupBox5.Controls.Add(this._fazatN);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(120, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(133, 166);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Уставки времени срабатывания ступени индикации по току фазы, мс";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "IN5";
            // 
            // _fazatC
            // 
            this._fazatC.Location = new System.Drawing.Point(37, 96);
            this._fazatC.Mask = "CCCCC";
            this._fazatC.Name = "_fazatC";
            this._fazatC.PromptChar = ' ';
            this._fazatC.Size = new System.Drawing.Size(53, 20);
            this._fazatC.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "IN";
            // 
            // _fazatB
            // 
            this._fazatB.Location = new System.Drawing.Point(37, 74);
            this._fazatB.Mask = "CCCCC";
            this._fazatB.Name = "_fazatB";
            this._fazatB.PromptChar = ' ';
            this._fazatB.Size = new System.Drawing.Size(53, 20);
            this._fazatB.TabIndex = 13;
            // 
            // _fazatN5
            // 
            this._fazatN5.Location = new System.Drawing.Point(37, 140);
            this._fazatN5.Mask = "CCCCC";
            this._fazatN5.Name = "_fazatN5";
            this._fazatN5.PromptChar = ' ';
            this._fazatN5.Size = new System.Drawing.Size(53, 20);
            this._fazatN5.TabIndex = 14;
            // 
            // _fazatA
            // 
            this._fazatA.Location = new System.Drawing.Point(37, 52);
            this._fazatA.Mask = "CCCCC";
            this._fazatA.Name = "_fazatA";
            this._fazatA.PromptChar = ' ';
            this._fazatA.Size = new System.Drawing.Size(53, 20);
            this._fazatA.TabIndex = 10;
            // 
            // _fazatN
            // 
            this._fazatN.Location = new System.Drawing.Point(37, 118);
            this._fazatN.Mask = "CCCCC";
            this._fazatN.Name = "_fazatN";
            this._fazatN.PromptChar = ' ';
            this._fazatN.Size = new System.Drawing.Size(53, 20);
            this._fazatN.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Ic";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Ib";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Ia";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this._fazaN5);
            this.groupBox6.Controls.Add(this._fazaN);
            this.groupBox6.Controls.Add(this._fazaC);
            this.groupBox6.Controls.Add(this._fazaB);
            this.groupBox6.Controls.Add(this._fazaA);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(6, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(108, 166);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Уставки срабатывания от макс. значения, (0,02...1)Imax";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "IN5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "IN";
            // 
            // _fazaN5
            // 
            this._fazaN5.Location = new System.Drawing.Point(37, 140);
            this._fazaN5.Mask = "CCCC";
            this._fazaN5.Name = "_fazaN5";
            this._fazaN5.PromptChar = ' ';
            this._fazaN5.Size = new System.Drawing.Size(53, 20);
            this._fazaN5.TabIndex = 10;
            // 
            // _fazaN
            // 
            this._fazaN.Location = new System.Drawing.Point(37, 118);
            this._fazaN.Mask = "CCCC";
            this._fazaN.Name = "_fazaN";
            this._fazaN.PromptChar = ' ';
            this._fazaN.Size = new System.Drawing.Size(53, 20);
            this._fazaN.TabIndex = 9;
            // 
            // _fazaC
            // 
            this._fazaC.Location = new System.Drawing.Point(37, 96);
            this._fazaC.Mask = "CCCC";
            this._fazaC.Name = "_fazaC";
            this._fazaC.PromptChar = ' ';
            this._fazaC.Size = new System.Drawing.Size(53, 20);
            this._fazaC.TabIndex = 8;
            // 
            // _fazaB
            // 
            this._fazaB.Location = new System.Drawing.Point(37, 74);
            this._fazaB.Mask = "CCCC";
            this._fazaB.Name = "_fazaB";
            this._fazaB.PromptChar = ' ';
            this._fazaB.Size = new System.Drawing.Size(53, 20);
            this._fazaB.TabIndex = 7;
            // 
            // _fazaA
            // 
            this._fazaA.Location = new System.Drawing.Point(37, 52);
            this._fazaA.Mask = "CCCC";
            this._fazaA.Name = "_fazaA";
            this._fazaA.PromptChar = ' ';
            this._fazaA.Size = new System.Drawing.Size(53, 20);
            this._fazaA.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ic";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ib";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файл";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Записать в файл";
            // 
            // ItkzConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 434);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItkzConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Activated += new System.EventHandler(this.ItkzConfigurationForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ItkzConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ItkzConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ITKZConfigurationForm_KeyUp);
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxBreakerControl.ResumeLayout(false);
            this.groupBoxBreakerControl.PerformLayout();
            this.groupBoxConfigReleAlarm.ResumeLayout(false);
            this.groupBoxConfigReleAlarm.PerformLayout();
            this.GroupBox9.ResumeLayout(false);
            this.GroupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _readBtn;
        private System.Windows.Forms.Button _writeBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _acceptBtn;
        private System.Windows.Forms.MaskedTextBox _devNumBox;
        private System.Windows.Forms.Button _readDevNumBtn;
        private System.Windows.Forms.Button readFromFileBtn;
        private System.Windows.Forms.Button writeToFileBtn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _baundRateCmb;
        private System.Windows.Forms.Button _acceptRateBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox _fazatC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox _fazatB;
        private System.Windows.Forms.MaskedTextBox _fazatN5;
        private System.Windows.Forms.MaskedTextBox _fazatA;
        private System.Windows.Forms.MaskedTextBox _fazatN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _fazaN5;
        private System.Windows.Forms.MaskedTextBox _fazaN;
        private System.Windows.Forms.MaskedTextBox _fazaC;
        private System.Windows.Forms.MaskedTextBox _fazaB;
        private System.Windows.Forms.MaskedTextBox _fazaA;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.MaskedTextBox _u0Limit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox _tu0;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.MaskedTextBox _phase;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label _versionLabel;
        private System.Windows.Forms.ComboBox _autoKvitCmb;
        private System.Windows.Forms.Label _aoutoKvitLabel;
        private System.Windows.Forms.GroupBox GroupBox9;
        private System.Windows.Forms.ComboBox _sectorAngleCB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox _updateParamCheckBox;
        private System.Windows.Forms.Label _updateKvitLabel;
        private System.Windows.Forms.GroupBox groupBoxConfigReleAlarm;
        private System.Windows.Forms.Button _modReleOutputBtn;
        private System.Windows.Forms.Button _modBlinkerBtn;
        private Forms.LedControl _modLedRele;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox _modeReleAlarm;
        private System.Windows.Forms.GroupBox groupBoxBreakerControl;
        private System.Windows.Forms.MaskedTextBox _time;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
    }
}