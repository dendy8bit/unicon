﻿namespace BEMN.ITKZ_3U0.Calibrate
{
    partial class MeasuringPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.iaBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ibBox = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.icBox = new System.Windows.Forms.MaskedTextBox();
            this.parameretsGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.timeBox = new System.Windows.Forms.MaskedTextBox();
            this.measureCBtn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.iaa = new System.Windows.Forms.Label();
            this.iab = new System.Windows.Forms.Label();
            this.iac = new System.Windows.Forms.Label();
            this.iba = new System.Windows.Forms.Label();
            this.ibb = new System.Windows.Forms.Label();
            this.ibc = new System.Windows.Forms.Label();
            this.ica = new System.Windows.Forms.Label();
            this.icb = new System.Windows.Forms.Label();
            this.icc = new System.Windows.Forms.Label();
            this.measureBBtn = new System.Windows.Forms.Button();
            this.measureABtn = new System.Windows.Forms.Button();
            this.parameretsGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // infoLabel
            // 
            this.infoLabel.Location = new System.Drawing.Point(3, 168);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(201, 34);
            this.infoLabel.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Уставка Ia";
            // 
            // iaBox
            // 
            this.iaBox.Location = new System.Drawing.Point(75, 19);
            this.iaBox.Name = "iaBox";
            this.iaBox.Size = new System.Drawing.Size(40, 20);
            this.iaBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Уставка Ib";
            // 
            // ibBox
            // 
            this.ibBox.Location = new System.Drawing.Point(75, 38);
            this.ibBox.Name = "ibBox";
            this.ibBox.Size = new System.Drawing.Size(40, 20);
            this.ibBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Уставка Ic";
            // 
            // icBox
            // 
            this.icBox.Location = new System.Drawing.Point(75, 57);
            this.icBox.Name = "icBox";
            this.icBox.Size = new System.Drawing.Size(40, 20);
            this.icBox.TabIndex = 2;
            // 
            // parameretsGroup
            // 
            this.parameretsGroup.Controls.Add(this.groupBox2);
            this.parameretsGroup.Controls.Add(this.label1);
            this.parameretsGroup.Controls.Add(this.icBox);
            this.parameretsGroup.Controls.Add(this.iaBox);
            this.parameretsGroup.Controls.Add(this.label3);
            this.parameretsGroup.Controls.Add(this.label2);
            this.parameretsGroup.Controls.Add(this.ibBox);
            this.parameretsGroup.Location = new System.Drawing.Point(3, 3);
            this.parameretsGroup.Name = "parameretsGroup";
            this.parameretsGroup.Size = new System.Drawing.Size(201, 92);
            this.parameretsGroup.TabIndex = 3;
            this.parameretsGroup.TabStop = false;
            this.parameretsGroup.Text = "Параметры калибровки";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.timeBox);
            this.groupBox2.Location = new System.Drawing.Point(121, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(74, 71);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Время замера токов, с";
            // 
            // timeBox
            // 
            this.timeBox.Location = new System.Drawing.Point(6, 42);
            this.timeBox.Name = "timeBox";
            this.timeBox.Size = new System.Drawing.Size(40, 20);
            this.timeBox.TabIndex = 2;
            // 
            // measureCBtn
            // 
            this.measureCBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.measureCBtn.Location = new System.Drawing.Point(147, 205);
            this.measureCBtn.Name = "measureCBtn";
            this.measureCBtn.Size = new System.Drawing.Size(57, 23);
            this.measureCBtn.TabIndex = 4;
            this.measureCBtn.Tag = "C";
            this.measureCBtn.Text = "Изм. Kc";
            this.measureCBtn.UseVisualStyleBackColor = true;
            this.measureCBtn.Click += new System.EventHandler(this.measureBtn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Ia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Ib";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ic";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Канал a";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(78, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Канал b";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(131, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "Канал c";
            // 
            // iaa
            // 
            this.iaa.AutoSize = true;
            this.iaa.Location = new System.Drawing.Point(25, 114);
            this.iaa.Name = "iaa";
            this.iaa.Size = new System.Drawing.Size(13, 13);
            this.iaa.TabIndex = 5;
            this.iaa.Text = "0";
            // 
            // iab
            // 
            this.iab.AutoSize = true;
            this.iab.Location = new System.Drawing.Point(25, 127);
            this.iab.Name = "iab";
            this.iab.Size = new System.Drawing.Size(13, 13);
            this.iab.TabIndex = 5;
            this.iab.Text = "0";
            // 
            // iac
            // 
            this.iac.AutoSize = true;
            this.iac.Location = new System.Drawing.Point(25, 140);
            this.iac.Name = "iac";
            this.iac.Size = new System.Drawing.Size(13, 13);
            this.iac.TabIndex = 5;
            this.iac.Text = "0";
            // 
            // iba
            // 
            this.iba.AutoSize = true;
            this.iba.Location = new System.Drawing.Point(78, 114);
            this.iba.Name = "iba";
            this.iba.Size = new System.Drawing.Size(13, 13);
            this.iba.TabIndex = 5;
            this.iba.Text = "0";
            // 
            // ibb
            // 
            this.ibb.AutoSize = true;
            this.ibb.Location = new System.Drawing.Point(78, 127);
            this.ibb.Name = "ibb";
            this.ibb.Size = new System.Drawing.Size(13, 13);
            this.ibb.TabIndex = 5;
            this.ibb.Text = "0";
            // 
            // ibc
            // 
            this.ibc.AutoSize = true;
            this.ibc.Location = new System.Drawing.Point(78, 140);
            this.ibc.Name = "ibc";
            this.ibc.Size = new System.Drawing.Size(13, 13);
            this.ibc.TabIndex = 5;
            this.ibc.Text = "0";
            // 
            // ica
            // 
            this.ica.AutoSize = true;
            this.ica.Location = new System.Drawing.Point(131, 114);
            this.ica.Name = "ica";
            this.ica.Size = new System.Drawing.Size(13, 13);
            this.ica.TabIndex = 5;
            this.ica.Text = "0";
            // 
            // icb
            // 
            this.icb.AutoSize = true;
            this.icb.Location = new System.Drawing.Point(131, 127);
            this.icb.Name = "icb";
            this.icb.Size = new System.Drawing.Size(13, 13);
            this.icb.TabIndex = 5;
            this.icb.Text = "0";
            // 
            // icc
            // 
            this.icc.AutoSize = true;
            this.icc.Location = new System.Drawing.Point(131, 140);
            this.icc.Name = "icc";
            this.icc.Size = new System.Drawing.Size(13, 13);
            this.icc.TabIndex = 5;
            this.icc.Text = "0";
            // 
            // measureBBtn
            // 
            this.measureBBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.measureBBtn.Location = new System.Drawing.Point(84, 205);
            this.measureBBtn.Name = "measureBBtn";
            this.measureBBtn.Size = new System.Drawing.Size(57, 23);
            this.measureBBtn.TabIndex = 4;
            this.measureBBtn.Tag = "B";
            this.measureBBtn.Text = "Изм. Kb";
            this.measureBBtn.UseVisualStyleBackColor = true;
            this.measureBBtn.Click += new System.EventHandler(this.measureBtn_Click);
            // 
            // measureABtn
            // 
            this.measureABtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.measureABtn.Location = new System.Drawing.Point(21, 205);
            this.measureABtn.Name = "measureABtn";
            this.measureABtn.Size = new System.Drawing.Size(57, 23);
            this.measureABtn.TabIndex = 4;
            this.measureABtn.Tag = "A";
            this.measureABtn.Text = "Изм. Ka";
            this.measureABtn.UseVisualStyleBackColor = true;
            this.measureABtn.Click += new System.EventHandler(this.measureBtn_Click);
            // 
            // MeasuringPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.icc);
            this.Controls.Add(this.ibc);
            this.Controls.Add(this.iac);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.icb);
            this.Controls.Add(this.ibb);
            this.Controls.Add(this.iab);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ica);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.iba);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.iaa);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.measureABtn);
            this.Controls.Add(this.measureBBtn);
            this.Controls.Add(this.measureCBtn);
            this.Controls.Add(this.parameretsGroup);
            this.Controls.Add(this.infoLabel);
            this.Name = "MeasuringPanel";
            this.Size = new System.Drawing.Size(207, 231);
            this.parameretsGroup.ResumeLayout(false);
            this.parameretsGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox iaBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox ibBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox icBox;
        private System.Windows.Forms.GroupBox parameretsGroup;
        private System.Windows.Forms.Button measureCBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox timeBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label iaa;
        private System.Windows.Forms.Label iab;
        private System.Windows.Forms.Label iac;
        private System.Windows.Forms.Label iba;
        private System.Windows.Forms.Label ibb;
        private System.Windows.Forms.Label ibc;
        private System.Windows.Forms.Label ica;
        private System.Windows.Forms.Label icb;
        private System.Windows.Forms.Label icc;
        private System.Windows.Forms.Button measureBBtn;
        private System.Windows.Forms.Button measureABtn;
    }
}
