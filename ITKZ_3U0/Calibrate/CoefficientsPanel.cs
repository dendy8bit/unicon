﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0.Calibrate
{
    public partial class CoefficientsPanel : UserControl
    {
        private MemoryEntity<CalibrateCoefficients> _calibrationCoef;
        private NewStructValidator<CalibrateCoefficients> _validator;
        private MaskedTextBox[] _boxes;
        private Action<bool> _callBack;

        public CoefficientsPanel(ItkzDeviceU0 device)
        {
            this.InitializeComponent();
            this._boxes = new[]
            {
                this._ka1, this._ka2, this._ka3, this._kb1, this._kb2, this._kb3, this._kc1, this._kc2, this._kc3
            };
            this._calibrationCoef = new MemoryEntity<CalibrateCoefficients>("Калибровочные коэффициенты", device, 0x23);
            this._calibrationCoef.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnReadCoefOk);
            this._calibrationCoef.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(false));
            this._calibrationCoef.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(true));
            this._calibrationCoef.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => this._callBack?.Invoke(false));

            this._validator = new NewStructValidator<CalibrateCoefficients>(new ToolTip(),
                new ControlInfoText(this._ka1, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._ka2, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._ka3, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kb1, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kb2, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kb3, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kc1, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kc2, new CustomDoubleRule(-7.99, 7.99, 3)),
                new ControlInfoText(this._kc3, new CustomDoubleRule(-7.99, 7.99, 3)));
        }

        public void SetStartState()
        {
            this._infoLabel.Text = Info.PANEL1_INFO1;
        }

        public void SetReadCoefficients(CalibrateCoefficients str)
        {
            this._validator.Set(str);
        }

        public void ReadCoeff(Action<bool> callBack)
        {
            this._callBack = callBack;
            this._calibrationCoef.LoadStruct();
        }

        private void OnReadCoefOk()
        {
            this._validator.Set(this._calibrationCoef.Value);
            this._callBack?.Invoke(true);
        }

        public void WriteDefaultCoefficients(Action<bool> callBack)
        {
            this._callBack = callBack;
            CalibrateCoefficients str = new CalibrateCoefficients
            {
                Ka1 = 1,
                Kb2 = 1,
                Kc3 = 1,
                Ka2 = 0,
                Ka3 = 0,
                Kb1 = 0,
                Kb3 = 0,
                Kc1 = 0,
                Kc2 = 0
            };
            this._calibrationCoef.Value = str;
            this._calibrationCoef.SaveStruct();
        }

        public void WriteCoeffInDevice(Action<bool> callBack)
        {
            this._callBack = callBack;
            this._calibrationCoef.Value = this._validator.Get();
            this._calibrationCoef.SaveStruct();
        }

        public bool IsDefault
        {
            get
            {
                CalibrateCoefficients str = this._calibrationCoef.Value;
                return str.Ka1 == 1 && str.Kb2 == 1 && str.Kc3 == 1
                       && str.Ka2 == 0 && str.Ka3 == 0
                       && str.Kb1 == 0 && str.Kb3 == 0
                       && str.Kc1 == 0 && str.Kc2 == 0;
            }
        }


        public void SaveInFile(string fileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("Калибровка_каналов"));

                var koefsStruct = this._validator.Get();
                List<ushort> values = new List<ushort>(koefsStruct.GetValues());

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values.ToArray(), false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(fileName);

                MessageBox.Show($"Калибровочные коэффициенты успешно сохранены в файл {fileName}", "Сохранение", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения калибровочных коэффициентов", "Сохранение", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        public void LoadFromFile(string fileName, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                var str = this._calibrationCoef.Value;
                str.InitStruct(values);
                this._validator.Set(str);

                MessageBox.Show($"Калибровочные коэффициенты успешно загружены из файла {fileName}", "Сохранение", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка загрузки калибровочных коэффициентов", "Сохранение", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
