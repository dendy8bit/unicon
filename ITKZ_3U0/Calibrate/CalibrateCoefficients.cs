﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0.Calibrate
{
    public class CalibrateCoefficients : StructBase
    {
        [Layout(0)] private ushort _ka1;
        [Layout(1)] private ushort _ka2;
        [Layout(2)] private ushort _ka3;
        [Layout(3)] private ushort _kb1;
        [Layout(4)] private ushort _kb2;
        [Layout(5)] private ushort _kb3;
        [Layout(6)] private ushort _kc1;
        [Layout(7)] private ushort _kc2;
        [Layout(8)] private ushort _kc3;

        [BindingProperty(0)]
        public double Ka1
        {
            get
            {
                int integerPart = Common.GetBits(this._ka1, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._ka1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)/4096;
                return Math.Round(Common.GetBit(this._ka1, 15) ? -1*(integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._ka1 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort) Math.Round(fractionalPartD*4096);
                this._ka1 += fractionalPart;
                this._ka1 = value < 0 ? Common.SetBit(this._ka1, 15, true) : Common.SetBit(this._ka1, 15, false);
            }
        }

        [BindingProperty(1)]
        public double Ka2
        {
            get
            {
                int integerPart = Common.GetBits(this._ka2, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._ka2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._ka2, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._ka2 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._ka2 += fractionalPart;
                this._ka2 = value < 0 ? Common.SetBit(this._ka2, 15, true) : Common.SetBit(this._ka2, 15, false);
            }
        }

        [BindingProperty(2)]
        public double Ka3
        {
            get
            {
                int integerPart = Common.GetBits(this._ka3, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._ka3, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._ka3, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._ka3 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._ka3 += fractionalPart;
                this._ka3 = value < 0 ? Common.SetBit(this._ka3, 15, true) : Common.SetBit(this._ka3, 15, false);
            }
        }

        [BindingProperty(3)]
        public double Kb1
        {
            get
            {
                int integerPart = Common.GetBits(this._kb1, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kb1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kb1, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kb1 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kb1 += fractionalPart;
                this._kb1 = value < 0 ? Common.SetBit(this._kb1, 15, true) : Common.SetBit(this._kb1, 15, false);
            }
        }

        [BindingProperty(4)]
        public double Kb2
        {
            get
            {
                int integerPart = Common.GetBits(this._kb2, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kb2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kb2, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kb2 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kb2 += fractionalPart;
                this._kb2 = value < 0 ? Common.SetBit(this._kb2, 15, true) : Common.SetBit(this._kb2, 15, false);
            }
        }

        [BindingProperty(5)]
        public double Kb3
        {
            get
            {
                int integerPart = Common.GetBits(this._kb3, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kb3, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kb3, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kb3 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kb3 += fractionalPart;
                this._kb3 = value < 0 ? Common.SetBit(this._kb3, 15, true) : Common.SetBit(this._kb3, 15, false);
            }
        }

        [BindingProperty(6)]
        public double Kc1
        {
            get
            {
                int integerPart = Common.GetBits(this._kc1, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kc1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kc1, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kc1 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kc1 += fractionalPart;
                this._kc1 = value < 0 ? Common.SetBit(this._kc1, 15, true) : Common.SetBit(this._kc1, 15, false);
            }
        }

        [BindingProperty(7)]
        public double Kc2
        {
            get
            {
                int integerPart = Common.GetBits(this._kc2, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kc2, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kc2, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kc2 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kc2 += fractionalPart;
                this._kc2 = value < 0 ? Common.SetBit(this._kc2, 15, true) : Common.SetBit(this._kc2, 15, false);
            }
        }

        [BindingProperty(8)]
        public double Kc3
        {
            get
            {
                int integerPart = Common.GetBits(this._kc3, 12, 13, 14) >> 12;
                double fractionalPart = (double)Common.GetBits(this._kc3, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) / 4096;
                return Math.Round(Common.GetBit(this._kc3, 15) ? -1 * (integerPart + fractionalPart) : integerPart + fractionalPart, 3);
            }
            set
            {
                ushort integerPart = (ushort)Math.Abs(value);
                this._kc3 = (ushort)(integerPart << 12);
                double fractionalPartD = Math.Abs(value) - integerPart;
                ushort fractionalPart = (ushort)Math.Round(fractionalPartD * 4096);
                this._kc3 += fractionalPart;
                this._kc3 = value < 0 ? Common.SetBit(this._kc3, 15, true) : Common.SetBit(this._kc3, 15, false);
            }
        }
    }
}
