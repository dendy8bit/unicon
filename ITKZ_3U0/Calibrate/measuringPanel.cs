﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.ITKZ_3U0.Structures;

namespace BEMN.ITKZ_3U0.Calibrate
{
    public partial class MeasuringPanel : UserControl
    {
        private MemoryEntity<ItkzMeasuringStruct> _measuring;
        private Timer _timer;
        private List<ushort> _ia;
        private List<ushort> _ib;
        private List<ushort> _ic;
        private Channel _channel;
        private bool _measuredA;
        private bool _measuredB;
        private bool _measuredC;
        private bool _repeat;

        internal enum Channel
        {
            A,B,C
        }

        public CalibrateStruct Coefficients { get; }

        private Action<bool> _callBack;

        public MeasuringPanel(ItkzDeviceU0 device)
        {
            this.InitializeComponent();

            this._measuring = new MemoryEntity<ItkzMeasuringStruct>("Измерения (калибровка)", device, 0x000D);
            this._measuring.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnReadCurrentsOk);
            this._measuring.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringFailure);

            this._timer = new Timer();
            this._timer.Tick += this.TimerOnTick;
            
            this.Coefficients = new CalibrateStruct();
            this._ia = new List<ushort>();
            this._ib = new List<ushort>();
            this._ic = new List<ushort>();
            //Для валидации вводимых значений пользователем
            NewStructValidator<CalibrateStruct> coefValid = new NewStructValidator<CalibrateStruct>(new ToolTip(),
                new ControlInfoText(this.iaBox, new CustomUshortRule(50, 2000)),
                new ControlInfoText(this.ibBox, new CustomUshortRule(50, 2000)),
                new ControlInfoText(this.icBox, new CustomUshortRule(50, 2000)),
                new ControlInfoText(this.timeBox, new CustomUshortRule(3, 5)));
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            this._timer.Stop();
            this._measuring.RemoveStructQueries();
            
            switch (this._channel)
            {
                case Channel.A:
                    this.measureABtn.Enabled = true;
                    this._measuredA = true;
                    this.Coefficients.Iaa = this.AverageValue(this._ia);
                    this.Coefficients.Iab = this.AverageValue(this._ib);
                    this.Coefficients.Iac = this.AverageValue(this._ic);
                    this.iaa.Text = this.Coefficients.Iaa.ToString("F2");
                    this.iab.Text = this.Coefficients.Iab.ToString("F2");
                    this.iac.Text = this.Coefficients.Iac.ToString("F2");
                    if (this.Coefficients.Iaa == 0 || this.Coefficients.Iab == 0 || this.Coefficients.Iac == 0)
                    {
                        MessageBox.Show("Один или более фазных токов равен нулю. Расчет калибровочных коэффициентов не возможен.",
                            "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.MeasuringFailure();
                        return;
                    }
                    this.infoLabel.Text = Info.MEASURING_IA_COMPLETE;
                    break;
                case Channel.B:
                    this.measureBBtn.Enabled = true;
                    this._measuredB = true;
                    this.Coefficients.Iba = this.AverageValue(this._ia);
                    this.Coefficients.Ibb = this.AverageValue(this._ib);
                    this.Coefficients.Ibc = this.AverageValue(this._ic);
                    this.iba.Text = this.Coefficients.Iba.ToString("F2");
                    this.ibb.Text = this.Coefficients.Ibb.ToString("F2");
                    this.ibc.Text = this.Coefficients.Ibc.ToString("F2");
                    if (this.Coefficients.Iba == 0 || this.Coefficients.Ibb == 0 || this.Coefficients.Ibc == 0)
                    {
                        MessageBox.Show("Один или более фазных токов равен нулю. Расчет калибровочных коэффициентов не возможен.",
                            "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.MeasuringFailure();
                        return;
                    }
                    this.infoLabel.Text = Info.MEASURING_IB_COMPLETE;
                    break;
                case Channel.C:
                    this.measureCBtn.Enabled = true;
                    this._measuredC = true;
                    this.Coefficients.Ica = this.AverageValue(this._ia);
                    this.Coefficients.Icb = this.AverageValue(this._ib);
                    this.Coefficients.Icc = this.AverageValue(this._ic);
                    this.ica.Text = this.Coefficients.Ica.ToString("F2");
                    this.icb.Text = this.Coefficients.Icb.ToString("F2");
                    this.icc.Text = this.Coefficients.Icc.ToString("F2");
                    if (this.Coefficients.Ica == 0 || this.Coefficients.Icb == 0 || this.Coefficients.Icc == 0)
                    {
                        MessageBox.Show("Один или более фазных токов равен нулю. Расчет калибровочных коэффициентов не возможен.",
                            "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.MeasuringFailure();
                        return;
                    }
                    this.infoLabel.Text = Info.MEASURING_IC_COMPLETE;
                    break;
                default:
                    MessageBox.Show("Ошибка калибровки устройства.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this._callBack?.Invoke(false);
                    return;
            }
            this._ia.Clear();
            this._ib.Clear();
            this._ic.Clear();
            if (this._measuredA && this._measuredB && this._measuredC)
            {
                this.infoLabel.Text = Info.MEASURING_COMPLETE;
                if(!this._repeat)
                    this._callBack?.Invoke(true);
                this._repeat = true;
            }
        }

        public void SetStartParametersMeasuring(Action<bool> callBack)
        {
            this._callBack = callBack;
            this.measureABtn.Enabled = true;
            this.measureBBtn.Enabled = true;
            this.measureCBtn.Enabled = true;
            this._repeat = this._measuredA = this._measuredB = this._measuredC = false;
            this.parameretsGroup.Enabled = true;
            this.infoLabel.Text = string.Empty;
            this.iaa.Text = this.iab.Text = this.iac.Text = "0";
            this.iba.Text = this.ibb.Text = this.ibc.Text = "0";
            this.ica.Text = this.icb.Text = this.icc.Text = "0";
        }

        public void CalculateCoefficients()
        {
            this.Coefficients.IaUst = Convert.ToUInt16(this.iaBox.Text);
            this.Coefficients.IbUst = Convert.ToUInt16(this.ibBox.Text);
            this.Coefficients.IcUst = Convert.ToUInt16(this.icBox.Text);
            this.Coefficients.Calculate();
        }

        public void StopMeasuring()
        {
            this._measuring.RemoveStructQueries();
        }

        private void OnReadCurrentsOk()
        {
            this._ia.Add(this._measuring.Value.IaValue);
            this._ib.Add(this._measuring.Value.IbValue);
            this._ic.Add(this._measuring.Value.IcValue);
        }

        private void MeasuringFailure()
        {
            this._measuring.RemoveStructQueries();
            this._timer.Stop();
            this._callBack?.Invoke(false);
        }

        private double AverageValue(List<ushort> currents)
        {
            return currents.Aggregate<ushort, double>(0, (current, val) => current + val) / currents.Count;
        }

        private void measureBtn_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if(b == null) return;
            this._channel = (Channel) Enum.Parse(typeof (Channel), b.Tag.ToString());
            switch (this._channel)
            {
                case Channel.A:
                    this.infoLabel.Text = Info.MEASURING_IA;
                    break;
                case Channel.B:
                    this.infoLabel.Text = Info.MEASURING_IB;
                    break;
                case Channel.C:
                    this.infoLabel.Text = Info.MEASURING_IC;
                    break;
            }
            b.Enabled = false;
            this._timer.Interval = Convert.ToInt32(this.timeBox.Text) * 1000;
            this._measuring.LoadStructCycle();
            this._timer.Start();
        }
    }

    public class CalibrateStruct : StructBase
    {
        public CalibrateStruct()
        {
            this.Coefficients = new CalibrateCoefficients();
        }

        public double Iaa { get; set; }
        public double Iab { get; set; }
        public double Iac { get; set; }

        public double Iba { get; set; }
        public double Ibb { get; set; }
        public double Ibc { get; set; }

        public double Ica { get; set; }
        public double Icb { get; set; }
        public double Icc { get; set; }

        // BindingProperty только для создания валидатора
        [BindingProperty(0)]
        public ushort IaUst { get; set; }

        [BindingProperty(1)]
        public ushort IbUst { get; set; }

        [BindingProperty(2)]
        public ushort IcUst { get; set; }

        [BindingProperty(3)]
        public int Time { get; set; }

        public CalibrateCoefficients Coefficients { get; }

        public void Calculate()
        {
            double Kaa = this.IaUst/this.Iaa;
            double Kbb = this.IbUst/this.Ibb;
            double Kcc = this.IcUst/this.Icc;

            double Kab = Kbb*this.Iab/this.IaUst;
            double Kac = Kcc*this.Iac/this.IaUst;

            double Kba = Kaa*this.Iba/this.IbUst;
            double Kbc = Kcc*this.Ibc/this.IbUst;

            double Kca = Kaa*this.Ica/this.IcUst;
            double Kcb = Kbb*this.Icb/this.IcUst;

            double D0 = 1 + Kba*Kcb*Kac + Kca*Kab*Kbc - Kca*Kac - Kab*Kba - Kbc*Kcb ;
            
            this.Coefficients.Ka1 = Kaa/D0*(1 - Kcb*Kbc);
            this.Coefficients.Ka2 = Kbb/D0*(Kca*Kbc - Kba);
            this.Coefficients.Ka3 = Kcc/D0*(Kba*Kcb - Kca);

            this.Coefficients.Kb1 = Kaa/D0*(Kcb*Kac-Kab);
            this.Coefficients.Kb2 = Kbb/D0*(1 - Kca*Kac);
            this.Coefficients.Kb3 = Kcc/D0*(Kca*Kab - Kcb);

            this.Coefficients.Kc1 = Kaa/D0*(Kab*Kbc - Kac);
            this.Coefficients.Kc2 = Kbb/D0*(Kba*Kac - Kbc);
            this.Coefficients.Kc3 = Kcc/D0*(1 - Kba*Kab);
        }
    }
}
