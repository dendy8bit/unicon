﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.BusinessLogic;
using BEMN.Interfaces;
using BEMN.ITKZ_3U0.Calibrate;
using BEMN.ITKZ_3U0.Structures;
using BEMN.MBServer;

namespace BEMN.ITKZ_3U0
{
    public class ItkzDeviceU0: Device, IDeviceView, IDeviceVersion
    {
        #region Fields
        #endregion

        #region Constructor
        public ItkzDeviceU0() { HaveVersion = true; }

        public ItkzDeviceU0(Modbus mb)
        {
            HaveVersion = true;
            this.MB = mb;
            this._version = new MemoryEntity<OneWordStruct>("version", this, 0x003F);
            this._version.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.OnVersionReadOk);
            this._version.AllReadFail += HandlerHelper.CreateReadArrayHandler(() =>
            {
                TreeManager.OnDeviceVersionLoadFail(this);
            });
            this.Config = new MemoryEntity<ConfigStruct>("Конфигурация усройства", this, 0x000B);
            this.Accept = new MemoryEntity<OneWordStruct>("Команда подтверждение", this, 0x0000);
            this.AutoKvit = new MemoryEntity<OneWordStruct>("Команда автоквитирование", this, 0x0000);
            this.StatusAngles = new MemoryEntity<OneWordStruct>("Команда ширины сектора", this, 0x0000);
            this.UpdateParamKvit = new MemoryEntity<OneWordStruct>("Команда обновления аварийных параметров при квитировании", this, 0x0000);
            this.Ustavki = new MemoryEntity<UstavkiStruct>("ИТКЗ уставки", this, 0x0001);
            this.UstavkiU0 = new MemoryEntity<UstavkiU0>("ИТКЗ уставки U0", this, 0x0002D);
            this.Measuring = new MemoryEntity<ItkzMeasuringStruct>("ИТКЗ измерения", this, 0x000D);

            this.Blinker = new MemoryEntity<Blinker>("Блинкер аварий", this, 0x001E); // Добавлено с версии 5.3.0
            this.ReleConfig = new MemoryEntity<ReleConfig>("Конфигурация реле аварий", this, 0x000D); // Добавлено с версии 5.3.0
            this.ReleConfigMeasuring = new MemoryEntity<ReleConfig>("Конфигурация реле аварий", this, 0x000D); // Добавлено с версии 5.3.0
            this.TimeUstavka = new MemoryEntity<TimeUstavka>("Уставка управления выключателем", this, 0x001D); // Добавлено с версии 5.3.0
            this.ConfigurationCommand = new MemoryEntity<OneWordStruct>("Команды конфигурации", this, 0x0000); // Добавлено с версии 5.3.0

            this.Command = new MemoryEntity<OneWordStruct>("Команда ввода/вывода ступеней", this, 0x0000);
            this.StatusU0 = new MemoryEntity<Status>("Состояние напряжения U0", this, 0x002C);
            this.StatusU0Config = new MemoryEntity<Status>("Состояние напряжения U0 конфигурации", this, 0x002C);
            this.MeasuringU0 = new MemoryEntity<ItkzMeasuringU0>("Измерения дополнительные", this, 0x0031);          
        }

        #endregion [Constructor]

        #region [Properties]
        private MemoryEntity<OneWordStruct> _version;
        public MemoryEntity<OneWordStruct> Version => this._version;

        public MemoryEntity<Blinker> Blinker { get; }
        public MemoryEntity<UstavkiStruct> Ustavki { get; }
        public MemoryEntity<UstavkiU0> UstavkiU0 { get; }
        public MemoryEntity<ConfigStruct> Config { get; }
        public MemoryEntity<OneWordStruct> Accept { get; }
        public MemoryEntity<ItkzMeasuringStruct> Measuring { get; }
        public MemoryEntity<OneWordStruct> Command { get; }
        public MemoryEntity<OneWordStruct> AutoKvit { get; }
        public MemoryEntity<OneWordStruct> StatusAngles { get; }
        public MemoryEntity<OneWordStruct> UpdateParamKvit { get; }
        public MemoryEntity<Status> StatusU0 { get; }
        public MemoryEntity<Status> StatusU0Config { get; }
        public MemoryEntity<ItkzMeasuringU0> MeasuringU0 { get; }
        public MemoryEntity<ReleConfig> ReleConfig { get; } // Добавлено с версии 5.3.0 
        public MemoryEntity<ReleConfig> ReleConfigMeasuring { get; } // Добавлено с версии 5.3.0 
        public MemoryEntity<TimeUstavka> TimeUstavka { get; } // Добавлено с версии 5.3.0
        public MemoryEntity<OneWordStruct> ConfigurationCommand { get; set; } // Добавлено с версии 5.3.0
        

        private void OnVersionReadOk()
        {
            string value = this._version.Value.Word.ToString("X");
            int[] valueToArray = value.Select(x => int.Parse(x.ToString()))
                .ToArray();
            if (value == "0")
            {
                this.Version1 = 1;
                this.Version2 = 0;
                this.Version3 = 0;
                DeviceVersion = Versions[0];
            }
            else
            {
                this.Version1 = valueToArray[0];
                this.Version2 = valueToArray[1];
                this.Version3 = valueToArray[2];
                DeviceVersion = $"{this.Version1}.{this.Version2}.{this.Version3}";
            }
            
            DeviceType = "ITKZ_3U0";
            LoadVersionOk();
        }

        public int Version1 { get; private set; }
        public int Version2 { get; private set; }
        public int Version3 { get; private set; }


        public int VersionInIntType
        {
            get
            {
                string str = new string(DeviceVersion.Where(char.IsDigit).ToArray());
                return int.Parse(str);
            }
        }

        public override void LoadVersion(object deviceObj)
        {
            this._version.LoadStruct();
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "до 5.2.5",
                    "5.2.5",
                    "5.2.6",
                    "5.2.7",
                    "5.2.8",
                    "5.2.9",
                    "5.3.0"
                };
            }
        }


        [XmlIgnore]
        public override Modbus MB
        {
            get => mb;
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }

        #endregion Properties

        public Type[] Forms => new[]
        {
            typeof (ItkzConfigurationForm),
            typeof (CalibrateForm),
            typeof (ItkzMeasuringForm)
        };

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(ItkzDeviceU0);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Framework.Properties.Resources.itkz;

        [Browsable(false)]
        public string NodeName => "ИТКЗ исполнение 2 (с U0)";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion
    }
}
