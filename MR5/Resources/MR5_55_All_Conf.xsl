<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <h2>���������� <xsl:value-of select="��5_V55/���_����������"/>. ������ �� <xsl:value-of select="��5_V55/������"/>. ����� <xsl:value-of select="��5_V55/�����_����������"/> </h2>
		<!--�������  �������-->
		 <h3><b>������� ������� </b></h3>
    
    <b>��������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>ITT�, A</th>
        <th>ITTn, A</th>
        <th>Im, In</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5_V55/�������������_�������������/������������_��"/></td>
        <td><xsl:value-of select="��5_V55/�������������_�������������/������������_����"/></td>
        <td><xsl:value-of select="��5_V55/�������������_�������������/I�"/></td>
      </tr>
    </table>
    <p></p>
    
    <b>������� �������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���� ���������</th>
        <th>���� ��������</th>
        <th>������� ���������</th>
        <th>������� ��������</th>
        <th>����� ������������</th>
        <th>������. ������� ��.1</th>
        <th>������. ������� ��.2</th>
        <th>������. ������� ��.3</th>
        <th>������. ������� ��.4</th>
        <th>������. ������� ��.5</th>
        <th>������. ������� ��.6</th>		
        <th>���������� ����</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5_V55/�������_�������/����_����"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/����_���"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/����_����_���������"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/����_����_��������"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/�����_������������"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������1"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������2"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������3"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������4"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������5"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������6"/></td>
        <td><xsl:value-of select="��5_V55/�������_�������/������_x0020_������_x0020_��_x0020_����"/></td>
      </tr>
    </table>
	<p></p>
    <b>��������� �������</b>
    <table border="1" cellspacing="0">
      <tr>
        <th  bgcolor="FFFFCC">�������� U��, �</th> 
        <th><xsl:value-of select="��5_V55/�������_�������/������_x0020_�����_x0020_������������"/></th>
      </tr>
    </table>
	<p></p>
	<b>������� ���������� �������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>���������� ������� �</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="��5_V55/�������_����������_�������/��">  
     <xsl:if test="position() &lt; 5 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	
		<b>���������� ������� ���</b>
	<table border="1" cellspacing="0">

      <tr bgcolor="FFFFCC">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="��5_V55/�������_����������_�������/��">  
     <xsl:if test="position() &gt; 4 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
	  </tr>
	</xsl:if>	
    </xsl:for-each>
    </table>
	</td>
	</table>
	<p></p>
	<!--
	<b>�����</b>&#32;
			<xsl:for-each select="��5_V55/�����/����">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					<xsl:value-of select="position()"/>&#32;
					</xsl:if>
					
				</xsl:if>
			 </xsl:for-each>
	<p></p>	
	-->
    <p></p>	
    <b>�����������</b>
    <table border="1" cellspacing="0">
	  <tr bgcolor="FFFFCC">
         <th>���������� �����������</th>
         <th>������������ ������� ���., ��</th>
         <th>�������� ���.</th>
         <th>����. ���������� ���., %</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��5_V55/���/Mode"/></td>
         <td><xsl:value-of select="��5_V55/���/Size"/></td>
         <td><xsl:value-of select="��5_V55/���/Fixation"/></td>
         <td><xsl:value-of select="��5_V55/���/Percent"/></td>

      </tr>
	</table>
  
  <!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->	
   <h3><b>�������� ������� </b></h3>

	   <b>����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
         <th>����� ���.</th>
         <th>����� ��</th>
         <th>����� ��</th>
      </tr>
     
      
		 <xsl:for-each select="��5_V55/����������/���_����������/����_���������">
		 <tr align ="center">
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td>
           <xsl:for-each select="@�����_���������">
             <xsl:if test="current() ='false'">���	</xsl:if>
             <xsl:if test="current() ='true'">����</xsl:if>
           </xsl:for-each>
         </td>
         <td>
           <xsl:for-each select="@�����_��">
             <xsl:if test="current() ='false'">	���	</xsl:if>
             <xsl:if test="current() ='true'">	����	</xsl:if>
           </xsl:for-each>
         </td>
         <td>
           <xsl:for-each select="@�����_��">
             <xsl:if test="current() ='false'">	���	</xsl:if>
             <xsl:if test="current() ='true'">	����	</xsl:if>
           </xsl:for-each>
         </td>
		 </tr>
		 </xsl:for-each>

    </table>   
	<p></p>
	
		
		<b>���� �������������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>T�����., ��</th>
         <th>���������� (������. 1)</th>
		     <th>������ (������. 2)</th>
         <th>������ (������. 3)</th>
		     <th>������ (������. 4)</th>
		     <th>����������� (������. 5)</th>
		     <th>������ (������. 6)</th>
		     <th>������ (������. 7)</th>
		     <th>������ (������. 8)</th>        	 
      </tr>
     
      <tr align="center">
		<td><xsl:value-of select="��5_V55/����_�������������/@�������_����_�������������"/></td>
			<xsl:for-each select="��5_V55/����_�������������/Signals/�������������">
				<td><xsl:value-of select="current()"/></td>
			</xsl:for-each>
       </tr>
    </table>
	<p></p>
	
	
    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>������������</th>
      </tr>
     
      
		 <xsl:for-each select="��5_V55/���_���/���">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
		 <td>
		 <xsl:for-each select="�������">
         <xsl:value-of select="current()"/>|
         </xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
	 </table>   
  
   <!-- |||||||||||||||||||||||||||���������� � ����������||||||||||||||||||||||||||||||||||||||||||| -->	
 <h3><b>���������� � ����������</b></h3>
	
	<b>������������ �����������</b>
	<table border="1" cellspacing="0">
	<td>
	<b>������� ����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC">
         <th>�� ������</th>
         <th>�� �����</th>
		 <th>�������</th>
         <th>����</th>
      </tr>
      <tr align="center">
		<td><xsl:value-of select="��5_V55/������������_�����������/����"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/����"/></td>
		<td><xsl:value-of select="��5_V55/������������_�����������/�������"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/����"/></td>
       </tr>
    </table>
	
	<b>�����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC">
         <th>��������� ���������</th>
         <th>��������� ��������</th>
		 <th>�������������</th>
         <th>����������</th>
		 <th>����� ����, ��</th>
         <th>��� ����, In</th>
		 <th>������� ��, ��</th>
         <th>����. �����, ��</th>
      </tr>
      <tr align="center">
		<td><xsl:value-of select="��5_V55/������������_�����������/���������"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/��������"/></td>
		<td><xsl:value-of select="��5_V55/������������_�����������/������"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/����������"/></td>
		<td><xsl:value-of select="��5_V55/������������_�����������/t����"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/���_����"/></td>
		<td><xsl:value-of select="��5_V55/������������_�����������/�������"/></td>
        <td><xsl:value-of select="��5_V55/������������_�����������/���������"/></td>
       </tr>
    </table>	
	</td>
	</table>
	<p></p>

   <b>���</b>
		    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>�������</th>
      </tr>
 		  <tr>
         <td><xsl:value-of select="��5_V55/���/�����"/></td>
         <td><xsl:value-of select="��5_V55/���/�������_���"/></td>
		 </tr>
    </table> 
		<p></p>
  <b>���</b>
		    <table border="1" cellspacing="0">
			
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>����������</th>
		 <th>t ����, ��</th>
		 <th>t �����, ��</th>
		 <th>1 ����</th>
		 <th>2 ����</th>
		 <th>3 ����</th>
		 <th>4 ����</th>
		 <th>������ �� ��������.</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��5_V55/���/�����"/></td>
         <td><xsl:value-of select="��5_V55/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��5_V55/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��5_V55/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��5_V55/���/����1"/></td>
		 <td><xsl:value-of select="��5_V55/���/����2"/></td>
		 <td><xsl:value-of select="��5_V55/���/����3"/></td>
		 <td><xsl:value-of select="��5_V55/���/����4"/></td>
		 <td><xsl:for-each select="��5_V55/���/������_���_��_�����������������_����������_�����������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 
		 </tr>
    </table> 	
   	<p></p>
   <b>���</b>
		    <table border="1" cellspacing="0">			
			
      <tr bgcolor="9966CC">
         <th>�� �������</th>
         <th>�� ����������</th>
		 <th>�� ��������.</th>
		 <th>�� ������</th>
		 <th>����. ����</th>
		 <th>����������</th>
		 <th>�����</th>
		 <th>��� ������.</th>
		 <th>t ��, ��</th>
		 <th>�������</th>
		 <th>t ���, ��</th>
		 <th>t ����, ��</th>
		 <th>�����</th>
      </tr>
     
 		  <tr>
		  <td><xsl:for-each select="��5_V55/���/��_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��5_V55/���/��_����������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��5_V55/���/��_��������������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		  <td><xsl:for-each select="��5_V55/���/��_������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 <td><xsl:value-of select="��5_V55/���/����"/></td>
		 <td><xsl:value-of select="��5_V55/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��5_V55/���/����_�����_����������_���"/></td>
		 <td><xsl:value-of select="��5_V55/���/����_���_������������"/></td>
		 <td><xsl:value-of select="��5_V55/���/�����_���_������������"/></td>
		 <td><xsl:value-of select="��5_V55/���/����_���_�������"/></td>
		 <td><xsl:value-of select="��5_V55/���/�����_���_�������"/></td>
		 <td><xsl:value-of select="��5_V55/���/��������_����������_�������"/></td>
		  <td><xsl:for-each select="��5_V55/���/�����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
    </table>	
<!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->

<h3><b>������� ������</b></h3>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		     <th>����������</th>
         <th>����</th>
		     <th>��������, ��</th>
		     <th>�������</th>
		     <th>��� ��</th>
		     <th>���� ��</th>
		     <th>������� ��, ��</th>
		     <th>����</th>
		     <th>���</th>
		     <th>���</th>
         <th>���.</th>
		</tr>
		 <xsl:for-each select="��5_V55/�������/���/ExternalDefenseStructV55">
		 <tr align="center">
		    <td>��- <xsl:value-of select="position()"/> </td>
			  <td><center><xsl:value-of select="Mode"/></center></td>
			  <td><center><xsl:value-of select="����������"/></center></td>
			  <td><center><xsl:value-of select="����_������������"/></center></td>
			  <td><center><xsl:value-of select="�������_������������"/></center></td>
			  <td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			  <td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			  <td><center><xsl:value-of select="����_��"/></center></td>			
			  <td><center><xsl:value-of select="�������_��"/></center></td>
			  <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			  <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			  <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
        <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>

		 </tr>
		 </xsl:for-each>
		</table>
    <p></p>
    <h3>������� ������</h3>
    <p></p>
    <div name="groups">
      <xsl:for-each select="��5_V55/�������/������">
        <xsl:variable name="index" select="position()"/>
        
      <h3 style="color:#FF0000FF">������ ������� <xsl:value-of select="position()"/></h3>

          <table border="1" cellspacing="0">
            <td>
              <h3>
                <b>������ I</b>
              </h3>

              <b>������ I> ������������� ����</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>���. ������������, In</th>
                  <th>��������������</th>
                  <th>t����, ��/����</th>
                  <th>���������</th>
                  <th>t���, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>


                <xsl:for-each select="CurrentDefenses/Rows/CurrentDefenseStructV55">
                  <xsl:if test="position() &lt; 5">
                    <tr align="center">
                      <td>
                        I&#62;  <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="Mode"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������_������������"/>
                      </td>
                      <td>
                        <xsl:value-of select="��������������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <p></p>
              <b>������ I2</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>���. ������������, In</th>
                  <th>t����, ��</th>
                  <th>���������</th>
                  <th>t���, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>


                <xsl:for-each select="CurrentDefenses/Rows/CurrentDefenseStructV55">
                  <xsl:if test="position() &gt; 4">
                    <xsl:if test="position() &lt; 7">
                      <tr align="center">
                        <xsl:if test="position() = 5">
                          <td>I2&#62; </td>
                        </xsl:if>
                        <xsl:if test="position() = 6">
                          <td>I2&#62;&#62; </td>
                        </xsl:if>
                        <td>
                          <xsl:value-of select="Mode"/>
                        </td>
                        <td>
                          <xsl:value-of select="����������"/>
                        </td>
                        <td>
                          <xsl:value-of select="��������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�������_������������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�����_������������"/>
                        </td>
                        <td>
                          <xsl:for-each select="���������">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="�������_���������"/>
                        </td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="���"/>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>
              <b>������ I0</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>���. ������������, In</th>
                  <th>t����, ��</th>
                  <th>���������</th>
                  <th>t���, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>


                <xsl:for-each select="CurrentDefenses/Rows/CurrentDefenseStructV55">
                  <xsl:if test="position() &gt; 6">
                    <xsl:if test="position() &lt; 9">
                      <tr align="center">
                        <xsl:if test="position() = 7">
                          <td>I0&#62; </td>
                        </xsl:if>
                        <xsl:if test="position() = 8">
                          <td>I0&#62;&#62; </td>
                        </xsl:if>
                        <td>
                          <xsl:value-of select="Mode"/>
                        </td>
                        <td>
                          <xsl:value-of select="����������"/>
                        </td>
                        <td>
                          <xsl:value-of select="��������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�������_������������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�����_������������"/>
                        </td>
                        <td>
                          <xsl:for-each select="���������">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="�������_���������"/>
                        </td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="���"/>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>
              <b>������ In</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>���. ������������, In</th>
                  <th>t����, ��</th>
                  <th>���������</th>
                  <th>t���, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>


                <xsl:for-each select="CurrentDefenses/Rows/CurrentDefenseStructV55">
                  <xsl:if test="position() &gt; 8">
                    <xsl:if test="position() &lt; 11">
                      <tr align="center">
                        <xsl:if test="position() = 9">
                          <td>In&#62; </td>
                        </xsl:if>
                        <xsl:if test="position() = 10">
                          <td>In&#62;&#62; </td>
                        </xsl:if>
                        <td>
                          <xsl:value-of select="Mode"/>
                        </td>
                        <td>
                          <xsl:value-of select="����������"/>
                        </td>
                        <td>
                          <xsl:value-of select="��������"/>
                        </td>
                        <td>
                          <xsl:value-of select="�������_������������In"/>
                        </td>
                        <td>
                          <xsl:value-of select="�����_������������"/>
                        </td>
                        <td>
                          <xsl:for-each select="���������">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:value-of select="�������_���������"/>
                        </td>
                        <td>
                          <xsl:for-each select="����">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                          <xsl:for-each select="���">
                            <xsl:if test="current() ='false'">	���	</xsl:if>
                            <xsl:if test="current() ='true'">	����	</xsl:if>
                          </xsl:for-each>
                        </td>
                        <td>
                            <xsl:value-of select="���"/>
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <p></p>
              <b>������ I�</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���. ������������, In</th>
                  <th>t����, ��</th>
                  <th>���������</th>
                  <th>t���, ��</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>
                <xsl:for-each select="AddCurrentDefenses/Rows/AddCurrentDefenseStructV55">
                  <xsl:if test="position() = 1">
                    <tr align="center">
                      <td>I�</td>
                      <td>
                        <xsl:value-of select="Mode"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������I�"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                          <xsl:value-of select="���"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <p></p>
              <b>������ I2/I1</b>
              <table border="1" cellspacing="0">

                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���. ������������, In</th>
                  <th>t����, ��</th>
                  <!--<th>���������</th>
                  <th>t���, ��</th>-->
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                  <th>���.</th>
                </tr>
                <xsl:for-each select="AddCurrentDefenses/Rows/AddCurrentDefenseStructV55">
                  <xsl:if test="position() = 2">
                    <tr align="center">
                      <td>I2/I1</td>
                      <td>
                        <xsl:value-of select="Mode"/>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <!--<td>
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>-->
                      <!--<td>
                        <xsl:value-of select="�������_���������"/>
                      </td>-->
                      <td>
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                          <xsl:value-of select="���"/>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </td>
          </table>
      </xsl:for-each>
    </div>
  <p></p>

   </body>
  </html>
</xsl:template>
</xsl:stylesheet>
