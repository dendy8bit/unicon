<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <xsl:variable name="vers" select="��5_V50/������"></xsl:variable>
    <xsl:variable name="group" select="��5_V50/�����_������"/>
    <h2>���������� <xsl:value-of select="��5_V50/���_����������"/>. ������ �� <xsl:value-of select="��5_V50/������"/>. ����� <xsl:value-of select="��5_V50/�����_����������"/> </h2>


    <h2>
      <b>������� ������� </b>
    </h2>
    <b>��������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>��������� ��� ��, �</th>
        <th>��������� ��� ����, A</th>
        <th>����. ��� ��������, I�</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/�������������/���������_���_��"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������������/���������_���_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������������/Imax"/>
        </td>
      </tr>
    </table>
    <p></p>

    <b>�����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC" align="center">
        <th>�����</th>
        <th>��������</th>
      </tr>
      <xsl:for-each select="��5_V50/�����/����">
        <tr align="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:value-of select="current()"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>

    <p></p>

    <b>������� �������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���� ���������</th>
        <th>���� ��������</th>
        <th>������� ���������</th>
        <th>������� ��������</th>
        <th>����� ���������</th>
        <th>���������� ����</th>
        <th>������. �� ��. ���.1</th>
        <th>������. �� ��. ���.2</th>
        <th>������. �� ��. ���.3</th>
        <th>������. �� ��. ���.4</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/�������_�������/����_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/����_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/����_����_���������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/����_����_��������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/�����_������������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/����������_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/������_�������_1"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/������_�������_2"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/������_�������_3"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�������_�������/������_�������_4"/>
        </td>
      </tr>
    </table>
    
    <p></p>

    <b>������� ����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>�� ������</th>
        <th>�� �����</th>
        <th>�������</th>
        <th>����</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/�����������/����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/�������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/����"/>
        </td>
      </tr>
    </table>

    <b>�����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>��������� ���������</th>
        <th>��������� ��������</th>
        <th>�������������</th>
        <th>����������</th>
        <th>����� ����, ��</th>
        <th>��� ����, In</th>
        <th>������� ��, ��</th>
        <th>����. �����, ��</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/�����������/���������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/��������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/����������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/t����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/���_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/�������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/�����������/���������"/>
        </td>
      </tr>
    </table>
    <p></p>

    <b>�����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���������� �����������</th>
        <th>������������ ������� ���., ��</th>
        <th>�������� ���.</th>
        <th>����. ���������� ���., %</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/������������_������������/����������_�����������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/������������_������������/������������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/������������_������������/��������"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/������������_������������/����������"/>
        </td>
      </tr>
    </table>

    <h2>������� ���������� �������</h2>
    <table border="1" cellspacing="0">
      <td>
        <b>���������� ������� �</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC" align="center">
            <th>����� ��</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="��5_V50/����������_�������/��">
            <xsl:if test="position() &lt; 5 ">
              <tr>
                <td align="center">
                  <xsl:value-of  select="position()"/>
                </td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:if test="current() !='���'">
                      <xsl:if test="current() ='��'">
                        �<xsl:value-of select="position()"/>
                      </xsl:if>
                      <xsl:if test="current() ='������'">
                        ^�<xsl:value-of select="position()"/>
                      </xsl:if>
                    </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>

        <b>���������� ������� ���</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC" align="center">
            <th>����� ��</th>
            <th>������������</th>
          </tr>
          <xsl:for-each select="��5_V50/����������_�������/��">
            <xsl:if test="position() &gt; 4 ">
              <tr>
                <td align="center">
                  <xsl:value-of  select="position()"/>
                </td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:if test="current() !='���'">
                      <xsl:if test="current() ='��'">
                        �<xsl:value-of select="position()"/>
                      </xsl:if>
                      <xsl:if test="current() ='������'">
                        ^�<xsl:value-of select="position()"/>
                      </xsl:if>
                    </xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
      </td>
    </table>
    <p></p>

    <!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->
    <h2>
      <b>�������� ������� </b>
    </h2>
    <b>�������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC" align="center">
        <th>�����</th>
        <th>���</th>
        <th>������</th>
        <th>T�����., ��</th>
      </tr>
      <xsl:for-each select="��5_V50/����/���_����/����_����">
        <tr align="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:value-of select="@���"/>
          </td>
          <td>
            <xsl:value-of select="@������"/>
          </td>
          <td>
            <xsl:value-of select="@�����"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <p></p>

    <b>����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
        <th>�����</th>
        <th>���</th>
        <th>������</th>
        <th>����� ���.</th>
        <th>����� ��</th>
        <th>����� ��</th>
      </tr>
      <xsl:for-each select="��5_V50/����������/���_����������/����_���������">
        <tr align ="center">
          <td>
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:value-of select="@���"/>
          </td>
          <td>
            <xsl:value-of select="@������"/>
          </td>
          <td>
            <xsl:for-each select="@�����_���������">
              <xsl:if test="current() ='false'">���	</xsl:if>
              <xsl:if test="current() ='true'">����</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="@�����_��">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="@�����_��">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    <p></p>

    <b>���� �������������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>T�����., ��</th>
        <th>���������� (������. 1)</th>
        <th>��� - ������ (������. 2)</th>
        <th>����������� (������. 3)</th>
        <th>������ (������. 4)</th>
        <th>����������� (������. 5)</th>
        <th>������ (������. 6)</th>
        <th>������ (������. 7)</th>
        <th>������ (������. 8)</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/�������������/@�������_����_�������������"/>
        </td>
        <xsl:for-each select="��5_V50/�������������/Signals/�������������">
          <td>
            <xsl:value-of select="current()"/>
          </td>
        </xsl:for-each>
      </tr>
    </table>
    <p></p>

    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
        <th>�����</th>
        <th>������������</th>
      </tr>
      <xsl:for-each select="��5_V50/���/���">
        <tr>
          <td align="center">
            <xsl:value-of select="position()"/>
          </td>
          <td>
            <xsl:for-each select="�������">
              <xsl:value-of select="current()"/>|
            </xsl:for-each>
          </td>
        </tr>
      </xsl:for-each>
    </table>

    <!-- |||||||||||||||||||||||||||����������||||||||||||||||||||||||||||||||||||||||||| -->
    <h2>
      <b>����������</b>
    </h2>

    <b>������������ ���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <th>������������</th>
        <th>����������</th>
        <th>����� ����������, ��</th>
        <th>����� ����������, ��</th>
        <th>����� 1 �����, ��</th>
        <th>����� 2 �����, ��</th>
        <th>����� 3 �����, ��</th>
        <th>����� 4 �����, ��</th>
        <th>������ ��� �� ��������. �����-��</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/���/�����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/�����_����������_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/����1"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/����2"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/����3"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/����4"/>
        </td>
        <td>
          <xsl:for-each select="��5_V50/���/������_���_��_��������������_�����������">
            <xsl:if test="current() ='false'">	���	</xsl:if>
            <xsl:if test="current() ='true'">	����	</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
    </table>
    <p></p>
    <b>������������ ���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <th>�����</th>
        <th>�������, I�</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/���/�����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���/�������_���"/>
        </td>
      </tr>
    </table>
    <p></p>

    <b>������������ ���</b>
    <table border="1" cellspacing="0">
      <td>
        <b>���������� �������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="9966CC" align="center">
            <th>�� �������</th>
            <th>�� ����������</th>
            <th>��������������</th>
            <th>�� ������</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��5_V50/���/��_�������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5_V50/���/��_����������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5_V50/���/��_��������������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5_V50/���/��_������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </table>
        <p></p>
        <table border="1" cellspacing="0">
          <tr bgcolor="9966CC" align="center">
            <th>���������� ������ �� ���. �����������</th>
            <th>������ ��� (�� �������)</th>
            <th>����������</th>
            <th>����� ����������</th>
            <th>������������</th>
            <th>����� ������������, ��</th>
            <th>�������</th>
            <th>����� ��������, ��</th>
            <th>����� ����������, ��</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��5_V50/���/�����">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/����"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/����_�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/����_���_������������"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/�����_���_������������"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/����_���_�������"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/�����_���_�������"/>
            </td>
            <td>
              <xsl:value-of select="��5_V50/���/��������_����������_�������"/>
            </td>
          </tr>
        </table>
      </td>
    </table>
    <p></p>
    <b>������������ ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <td>����</td>
        <td>����������</td>
        <td>��������, ��</td>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/���_����/����_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���_����/����������_����"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���_����/��������_�������_����"/>
        </td>
      </tr>
    </table>
    <p></p>
    <b>������������ ���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <td>����</td>
        <td>����������</td>
        <td>��������, ��</td>
        <td>�����������</td>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5_V50/���_����/����_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���_����/����������_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���_����/��������_�������_���"/>
        </td>
        <td>
          <xsl:value-of select="��5_V50/���_����/�����������_���"/>
        </td>
      </tr>
    </table>

	
<!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->
    <h2>
      <b>������� ������</b>
    </h2>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFCC66">
        <th>�������</th>
        <th>�����</th>
        <th>����������</th>
        <th>������������</th>
        <th>t��., ��</th>
        <th>�������</th>
        <th>��� ��</th>
        <th>���� ��</th>
        <th>t��, ��</th>
        <th>����</th>
        <th>���.</th>
        <th>���</th>
        <th>���</th>
        <th>�����</th>
      </tr>
      <xsl:for-each select="��5_V50/�������_������/���/DefenseExternalStruct">
        <tr align="center">
          <td>
            ��- <xsl:value-of select="position()"/>
          </td>
          <td>
            <center>
              <xsl:value-of select="�����"/>
            </center>
          </td>
          <td>
            <center>
              <xsl:value-of select="����������"/>
            </center>
          </td>
          <td>
            <center>
              <xsl:value-of select="����_������������"/>
            </center>
          </td>
          <td>
            <center>
              <xsl:value-of select="�������_������������"/>
            </center>
          </td>
          <td>
            <xsl:for-each select="�������">
              <xsl:if test="current() ='false'">���	</xsl:if>
              <xsl:if test="current() ='true'">����</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="���_��">
              <xsl:if test="current() ='false'">���	</xsl:if>
              <xsl:if test="current() ='true'">����</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <center>
              <xsl:value-of select="����_��"/>
            </center>
          </td>
          <td>
            <center>
              <xsl:value-of select="�������_��"/>
            </center>
          </td>
          <td>
            <xsl:for-each select="����">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="���">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="���">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="���">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
          <td>
            <xsl:for-each select="�����_�������">
              <xsl:if test="current() ='false'">	���	</xsl:if>
              <xsl:if test="current() ='true'">	����	</xsl:if>
            </xsl:for-each>
          </td>
        </tr>
      </xsl:for-each>
    </table>

      <xsl:for-each select="��5_V50/���_������/������">
        <xsl:if test="position() = $group">

          <h3>
            ������ ������� <xsl:value-of select="$group"/>
          </h3>

          <h3>
            <b>������ I</b>
          </h3>
          <table border="1" cellspacing="0">
            <td>
              <b>������ I> ������������� ����</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>��������</th>
                  <th>I��, I�</th>
                  <th>�����-��</th>
                  <th>t��, ��/����.</th>
                  <th>���.</th>
                  <th>t���, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="�������/Rows/CurrentDefenseStruct">
                  <tr>
                    <xsl:if test="position()=1">
                      <td>I&#62;</td>
                    </xsl:if>
                    <xsl:if test="position()=2">
                      <td>I&#62;&#62;</td>
                    </xsl:if>
                    <xsl:if test="position()=3">
                      <td>I&#62;&#62;&#62;</td>
                    </xsl:if>
                    <xsl:if test="position()=4">
                      <td>I&#62;&#62;&#62;&#62;</td>
                    </xsl:if>
                    <td align="center">
                      <xsl:value-of select="�����"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="����������"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="��������"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="�������_������������"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="��������������"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="t��_�����������"/>
                    </td>
                    <td align="center">
                      <xsl:for-each select="���������">
                        <xsl:if test="current() ='false'">	���	</xsl:if>
                        <xsl:if test="current() ='true'">	����	</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td align="center">
                      <xsl:value-of select="�������_���������"/>
                    </td>
                    <td align="center">
                      <xsl:value-of select="���"/>
                    </td>
                    <td align="center">
                      <xsl:for-each select="����">
                        <xsl:if test="current() ='false'">	���	</xsl:if>
                        <xsl:if test="current() ='true'">	����	</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td align="center">
                      <xsl:for-each select="���">
                        <xsl:if test="current() ='false'">	���	</xsl:if>
                        <xsl:if test="current() ='true'">	����	</xsl:if>
                      </xsl:for-each>
                    </td>
                    <td align="center">
                      <xsl:for-each select="���">
                        <xsl:if test="current() ='false'">	���	</xsl:if>
                        <xsl:if test="current() ='true'">	����	</xsl:if>
                      </xsl:for-each>
                    </td>

                  </tr>
                </xsl:for-each>
              </table>
              <p></p>

              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>I��, I�</th>
                  <th>t��, ��</th>
                  <th>���.</th>
                  <th>t���, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="�������_������/Rows/CurrentDefenseOtherStruct">
                  <xsl:if test="position() &lt; 3">
                    <tr>
                      <xsl:if test="position()=1">
                        <td>I2&#62;</td>
                      </xsl:if>
                      <xsl:if test="position()=2">
                        <td>I2&#62;&#62;</td>
                      </xsl:if>
                      <td align="center">
                        <xsl:value-of select="�����"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="����������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_������������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="���"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>

              <b>������ I0</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>I��, I�</th>
                  <th>t��, ��</th>
                  <th>���.</th>
                  <th>t���, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="�������_������/Rows/CurrentDefenseOtherStruct">
                  <xsl:if test="position() &gt; 2 and position() &lt; 5">
                    <tr>
                      <xsl:if test="position()=3">
                        <td>I0&#62;</td>
                      </xsl:if>
                      <xsl:if test="position()=4">
                        <td>I0&#62;&#62;</td>
                      </xsl:if>
                      <td align="center">
                        <xsl:value-of select="�����"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="����������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_������������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="���"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>

              <b>������ In</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>I��, I�</th>
                  <th>t��, ��</th>
                  <th>���.</th>
                  <th>t���, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="�������_������/Rows/CurrentDefenseOtherStruct">
                  <xsl:if test="position() &gt; 4 and position() &lt; 7">
                    <tr>
                      <xsl:if test="position()=5">
                        <td>In&#62;</td>
                      </xsl:if>
                      <xsl:if test="position()=6">
                        <td>In&#62;&#62;</td>
                      </xsl:if>
                      <td align="center">
                        <xsl:value-of select="�����"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="����������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_������������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="���"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>

              <p></p>

              <b>������ I�</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>I��, I�</th>
                  <th>t��, ��</th>
                  <th>���.</th>
                  <th>t���, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="��5_V50/�������_��������������/��������/Rows/AddCurrentDefenseStruct">
                  <xsl:if test="position() = 1">
                    <tr>
                      <td>I�</td>
                      <td align="center">
                        <xsl:value-of select="Mode"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="����������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������100"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���������">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������_���������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="���"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>

              <b>������ I2/I1</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="FFCC66">
                  <th>�������</th>
                  <th>�����</th>
                  <th>����������</th>
                  <th>���. ������������</th>
                  <th>t����, ��</th>
                  <th>�����������</th>
                  <th>����</th>
                  <th>���</th>
                  <th>���</th>
                </tr>
                <xsl:for-each select="�������_��������������/Rows/AddCurrentDefenseStruct">
                  <xsl:if test="position() = 2">
                    <tr>
                      <td>I2/I1</td>
                      <td align="center">
                        <xsl:value-of select="Mode"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="����������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�������100"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="�����_������������"/>
                      </td>
                      <td align="center">
                        <xsl:value-of select="���"/>
                      </td>
                      <td align="center">
                        <xsl:for-each select="����">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td align="center">
                        <xsl:for-each select="���">
                          <xsl:if test="current() ='false'">	���	</xsl:if>
                          <xsl:if test="current() ='true'">	����	</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
            </td>
          </table>

        </xsl:if>
      </xsl:for-each>

   </body>
  </html>
</xsl:template>
</xsl:stylesheet>
