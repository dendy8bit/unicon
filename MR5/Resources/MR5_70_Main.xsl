<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <xsl:variable name="vers" select="��5V70/DeviceVersion"></xsl:variable>
    <h2>���������� <xsl:value-of select="��5V70/DeviceType"/>. ������ �� <xsl:value-of select="��5V70/DeviceVersion"/>. ����� <xsl:value-of select="��5V70/DeviceNumber"/> </h2>
    
    <h2><b>������� ������� </b></h2>
    <b>��������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>��������� ��� ��, �</th>
        <th>��������� ��� ����, A</th>
        <th>����. ��� ��������, I�</th>
        <th>��� ��</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5V70/�������������_�������������/������������_��"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/������������_����"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/I�"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/���_��"/></td>
      </tr>
    </table>
    <p></p>
    
    <b>��� ��</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>��� ��</th>
        <th>����������� ��</th>
        <th>������������� ��</th>
        <th>����������� ����</th>
        <th>������������� ����</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5V70/�������������_�������������/���_Uo"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/��"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/�������������_��"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/����"/></td>
        <td><xsl:value-of select="��5V70/�������������_�������������/�������������_����"/></td>
      </tr>
    </table>
    <p></p>
    
    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���</th>
        <th>��� (��/��)</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:for-each select="��5V70/���/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each>
        </td>
        <td><xsl:value-of select="��5V70/���/Xyd"/></td>
      </tr>
    </table>
    <p></p>
    
    <b>�����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>�����</th>
        <th>��������</th>
      </tr>
      <xsl:for-each select="��5V70/�����/����">
        <tr>
          <td><xsl:value-of select="position()"/></td>
          <td><xsl:value-of select="current()"/></td>
        </tr>
      </xsl:for-each>
    </table>
    
    <p></p>
    
    <b>������� �������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���� ���������</th>
        <th>���� ��������</th>
        <th>������� ���������</th>
        <th>������� ��������</th>
        <th>����� ������������</th>
        <th>������ �������</th>
        <th>���������� ����</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5V70/�������_�������/����_����"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/����_���"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/����_����_���������"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/����_����_��������"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/�����_������������"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/������_�������"/></td>
        <td><xsl:value-of select="��5V70/�������_�������/����������_����"/></td>
      </tr>
    </table>
    <p></p>
    
        <b>������� ����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>�� ������</th>
            <th>�� �����</th>
            <th>�������</th>
            <th>����</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="��5V70/������������_�����������/����"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/����"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/�������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/����"/></td>
          </tr>
        </table>
        
        <b>�����������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFFFCC">
            <th>��������� ���������</th>
            <th>��������� ��������</th>
            <th>�������������</th>
            <th>����������</th>
            <th>����� ����, ��</th>
            <th>��� ����, In</th>
            <th>������� ��, ��</th>
            <th>����. �����, ��</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="��5V70/������������_�����������/���������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/��������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/����������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/t����"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/���_����"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/�������"/></td>
            <td><xsl:value-of select="��5V70/������������_�����������/���������"/></td>
          </tr>
        </table>	
    <p></p>
    
    <b>������� ���������� �������</b>
	  <table border="1" cellspacing="0">
	  <td>
	  <b>���������� ������� �</b>
	  <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>����� ��</th>
        <th>������������</th>
      </tr>
      <xsl:for-each select="��5V70/�������_����������_�������/��">  
       <xsl:if test="position() &lt; 5 ">
   	    <tr>	
          <td><xsl:value-of  select="position()"/></td>
          <td><xsl:for-each select="�������">
            <xsl:if test="current() !='���'">
              <xsl:if test="current() ='��'">
                �<xsl:value-of select="position()"/>
              </xsl:if>
              <xsl:if test="current() ='������'">
                ^�<xsl:value-of select="position()"/>
              </xsl:if>
            </xsl:if>
          </xsl:for-each>
          </td>
        </tr>
       </xsl:if>	
     </xsl:for-each>
    </table>
	
		<b>���������� ������� ���</b>
	  <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>����� ��</th>
        <th>������������</th>
      </tr>
      <xsl:for-each select="��5V70/�������_����������_�������/��"> 
        <xsl:if test="position() &gt; 4 ">
          <tr>	
            <td><xsl:value-of  select="position()"/></td>
            <td>
              <xsl:for-each select="�������">
                <xsl:if test="current() !='���'">
                  <xsl:if test="current() ='��'">
                    �<xsl:value-of select="position()"/>
                  </xsl:if>
                  <xsl:if test="current() ='������'">
                    ^�<xsl:value-of select="position()"/>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:if>	
      </xsl:for-each>
    </table>
    </td>
    </table>
    <p></p>	
    
    <b>�����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>���������� �����������</th>
        <th>������������ ������� ���., ��</th>
        <th>�������� ���.</th>
        <th>����. ���������� ���., %</th>
      </tr>
      <tr align="center">
        <td><xsl:value-of select="��5V70/���/����������_�����������"/></td>
        <td><xsl:value-of select="��5V70/���/������������"/></td>
        <td><xsl:value-of select="��5V70/���/��������"/></td>
        <td><xsl:value-of select="��5V70/���/����������"/></td>
      </tr>
    </table>
	
<!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->	
   <h2><b>�������� ������� </b></h2>
   <b>�������� ����</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
         <th>T�����., ��</th>
      </tr>
      <xsl:for-each select="��5V70/����/���_����/����_����">
        <xsl:if test="position() &lt; 9 ">
          <tr align="center">
            <td>
              <xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:value-of select="@���"/>
            </td>
            <td>
              <xsl:value-of select="@������"/>
            </td>
            <td>
              <xsl:value-of select="@�����"/>
            </td>
          </tr>
        </xsl:if>
      </xsl:for-each>
    </table>  
    <p></p>
    
    <b>����������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
        <th>�����</th>
        <th>���</th>
        <th>������</th>
        <th>����� ���.</th>
        <th>����� ��</th>
        <th>����� ��</th>
      </tr>
      <xsl:for-each select="��5V70/����������/���_����������/����_���������">
        <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
          <td><xsl:for-each select="@�����_���������">
           <xsl:if test="current() ='false'">	���	</xsl:if>
           <xsl:if test="current() ='true'">	����	</xsl:if>
         </xsl:for-each>
          </td>
          <td> <xsl:for-each select="@�����_��">
           <xsl:if test="current() ='false'">	���	</xsl:if>
           <xsl:if test="current() ='true'">	����	</xsl:if>
         </xsl:for-each>
             </td>
          <td> <xsl:for-each select="@�����_��">
           <xsl:if test="current() ='false'">	���	</xsl:if>
           <xsl:if test="current() ='true'">	����	</xsl:if>
         </xsl:for-each>
        </td>
        
        </tr>
      </xsl:for-each>
    </table>   
    <p></p>

    <b>���� �������������</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFFFCC">
        <th>T�����., ��</th>
        <th>���������� (������. 1)</th>
        <th>���-������ (������. 2)</th>
        <th>����������� (������. 3)</th>
        <th>������ (������. 4)</th>
        <th>����������� (������. 5)</th>
        <th>������ (������. 6)</th>
        <th>���������� (������. 7)</th>
        <th>������� (������. 8</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5V70/����_�������������/@�������_����_�������������"/>
        </td>
        <xsl:for-each select="��5V70/����_�������������/Signals/�������������">
          <td>
            <xsl:value-of select="current()"/>
          </td>
        </xsl:for-each>
      </tr>
    </table>
    <p></p>
	
    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="CCFFCC">
        <th>�����</th>
        <th>������������</th>
      </tr>
      <xsl:for-each select="��5V70/���_���/���">
        <tr>
          <td><xsl:value-of select="position()"/></td>
          <td>
            <xsl:for-each select="�������">
              <xsl:value-of select="current()"/>|
              </xsl:for-each></td>
        </tr>
      </xsl:for-each>
    </table>   

 <!-- |||||||||||||||||||||||||||���������� � ����������||||||||||||||||||||||||||||||||||||||||||| -->	
    <h2><b>����������</b></h2>
    <b>��������� ���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <th>������������</th>
        <th>�������, I�</th>
      </tr>
      <tr align="center">
        <td>
          <xsl:value-of select="��5V70/���/�����"/>
        </td>
        <td>
          <xsl:value-of select="��5V70/���/�������_���"/>
        </td>
      </tr>
    </table> 
		<p></p>
    
    <b>���</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="9966CC" align="center">
        <th>������������</th>
        <th>����������</th>
		    <th>����� ����������, ��</th>
		    <th>����� ����������, ��</th>
		    <th>����� 1 �����, ��</th>
		    <th>����� 2 �����, ��</th>
		    <th>����� 3 �����, ��</th>
		    <th>����� 4 �����, ��</th>
		    <th>������ ��� �� ��������.</th>
      </tr>
     <tr align="center">
       <td>
         <xsl:value-of select="��5V70/���/�����"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/����_����������_���"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/�����_����������_���"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/�����_����������_���"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/����1"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/����2"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/����3"/>
       </td>
       <td>
         <xsl:value-of select="��5V70/���/����4"/>
       </td>
       <td>
         <xsl:for-each select="��5V70/���/������_���_��_�����������������_����������_�����������">
           <xsl:if test="current() ='false'">	���	</xsl:if>
           <xsl:if test="current() ='true'">	����	</xsl:if>
         </xsl:for-each>
       </td>
     </tr>
    </table> 	
    
   	<p></p>
    <b>������������ ���</b>
    <table border="1" cellspacing="0">
      <td>
        <b>���������� �������</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="9966CC" align="center">
            <th>�� �������</th>
            <th>�� ����������</th>
            <th>��������������</th>
            <th>�� ������</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��5V70/���/��_�������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5V70/���/��_����������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5V70/���/��_��������������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:for-each select="��5V70/���/��_������">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </table>
        <p></p>
        <table border="1" cellspacing="0">
          <tr bgcolor="9966CC" align="center">
            <th>���������� ������ �� ���. �����������</th>
            <th>������ ��� (�� �������)</th>
            <th>����������</th>
            <th>����� ����������</th>
            <th>������������</th>
            <th>t��, ��</th>
            <th>�������</th>
            <th>t���, ��</th>
            <th>t����, ��</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:for-each select="��5V70/���/�����">
                <xsl:if test="current() ='false'">	���	</xsl:if>
                <xsl:if test="current() ='true'">	����	</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/����"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/����_�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/����_���_������������"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/�����_���_������������"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/����_���_�������"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/�����_���_�������"/>
            </td>
            <td>
              <xsl:value-of select="��5V70/���/��������_����������_�������"/>
            </td>
          </tr>
        </table>
      </td>	
    </table>

	
<!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->  
    <h2><b>������� ������</b></h2>
    <table border="1" cellspacing="0">
      <tr bgcolor="FFCC66">
        <th>�������</th>
        <th>�����</th>
        <th>����������</th>
        <th>������������</th>
		    <th>����� ����., ��</th>
		    <th>�������</th>
		    <th>��� ��</th>
		    <th>���� ��</th>
		    <th>����� ��������, ��</th>
		    <th>����</th>
		    <th>���</th>
		    <th>���</th>
		    <th>�����������</th>
		    <th>�����</th>
		  </tr>
      <xsl:for-each select="��5V70/�������/���/DefenseExternalStruct">
        <tr align="center">
          <td>��- <xsl:value-of select="position()"/> </td>
          <td><center><xsl:value-of select="�����"/></center></td>
          <td><center><xsl:value-of select="����������"/></center></td>
          <td><center><xsl:value-of select="����_������������"/></center></td>
          <td><center><xsl:value-of select="�������_������������"/></center></td>
          <td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
          <td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
          <td><center><xsl:value-of select="����_��"/></center></td>
          <td><center><xsl:value-of select="�������_��"/></center></td>
          <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			    <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			    <td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		    </tr>
      </xsl:for-each>
    </table>
    
    <h2><b>������. �������� ������ �������</b></h2>
    
    <h3><b>���� ��</b></h3>
    <table border="1" cellspacing="0" >
      <tr bgcolor="FFCC66">
        <th>I</th>
        <th>I0</th>
        <th>I2</th>
        <th>In</th>
      </tr>
 		  <tr>
        <td><xsl:value-of select="��5V70/�������/��������/����/I"/></td>
        <td><xsl:value-of select="��5V70/�������/��������/����/I0"/></td>
        <td><xsl:value-of select="��5V70/�������/��������/����/I2"/></td>
        <td><xsl:value-of select="��5V70/�������/��������/����/In"/></td>
		  </tr>
		</table> 
  
    <h3><b>������ I</b></h3>
		<table border="1" cellspacing="0">
      <td>
        <b>������ I> ������������� ����</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>���� �� U</th>
            <th>U����, �</th>
            <th>�����������</th>
            <th>������.����</th>
            <th>��������</th>
            <th>���. ������������, In</th>
            <th>��������������</th>
            <th>t����, ��/����</th>
            <th>���������</th>
            <th>t���, ��</th>
		        <th>����</th>
		        <th>���</th>
		        <th>���</th>
		        <th>�����������</th>
		      </tr>
          <xsl:for-each select="��5V70/�������/��������/��������_�������/Rows/CurrentDefenseStruct">
            <tr align="center">
              <xsl:if test="position()=1">
                <td>
                  I&#62;
                </td>
              </xsl:if>
              <xsl:if test="position()=2">
                <td>
                  I&#62;&#62;
                </td>
              </xsl:if>
              <xsl:if test="position()=3">
                <td>
                  I&#62;&#62;&#62;
                </td>
              </xsl:if>
              <xsl:if test="position()=4">
                <td>
                  I&#62;&#62;&#62;&#62;
                </td>
              </xsl:if>
              <td><xsl:value-of select="�����"/></td>
              <td><xsl:value-of select="����������"/></td>
			        <td><xsl:for-each select="����_��_U"><xsl:if test="current() ='false'">���</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			        <td><xsl:value-of select="U����"/></td>
			        <td><xsl:value-of select="�����������"/></td>
			        <td><xsl:value-of select="�����������_����������"/></td>
			        <td><xsl:value-of select="��������"/></td>
			        <td><xsl:value-of select="�������_������������"/></td>
			        <td><xsl:value-of select="��������������"/></td>
			        <td><xsl:value-of select="�����_������������"/></td>
			        <td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			        <td><xsl:value-of select="�������_���������"/></td>
			        <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			        <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			        <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			        <td><xsl:value-of select="���"/></td>
		        </tr>
          </xsl:for-each>
        </table>
        <p></p>	
        
				<b>������ I2</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>���� �� U</th>
		        <th>U����, �</th>
		        <th>�����������</th>
		        <th>������.����</th>
		        <th>��������</th>
		        <th>���. ������������, In</th>
		        <th>t����, ��</th>
		        <th>���������</th>
		        <th>t���, ��</th>
		        <th>����</th>
		        <th>���</th>
		        <th>���</th>
		        <th>�����������</th>
		      </tr>
          <xsl:for-each select="��5V70/�������/��������/�������_���������/Rows/CurrentDefenseOtherStruct">
            <xsl:if test="position() &lt; 3">
              <tr align="center">
                <xsl:if test="position()=1">
                  <td>
                    I2&#62;
                  </td>
                </xsl:if>
                <xsl:if test="position()=2">
                  <td>
                    I2&#62;&#62;
                  </td>
                </xsl:if>
                <td><xsl:value-of select="�����"/></td>
			          <td><xsl:value-of select="����������"/></td>
			          <td><xsl:for-each select="����_��_U"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="U����"/></td>
			          <td><xsl:value-of select="�����������"/></td>
			          <td><xsl:value-of select="�����������_����������"/></td>
			          <td><xsl:value-of select="��������"/></td>
			          <td><xsl:value-of select="�������_������������"/></td>
			          <td><xsl:value-of select="�����_������������"/></td>
			          <td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="�������_���������"/></td>
			          <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="���"/></td>
		          </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>	
        
        <b>������ I0</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>���� �� U</th>
            <th>U����, �</th>
            <th>�����������</th>
            <th>������.����</th>
            <th>��������</th>
            <th>���. ������������, In</th>
            <th>t����, ��</th>
            <th>���������</th>
            <th>t���, ��</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
            <th>�����������</th>
          </tr>
          <xsl:for-each select="��5V70/�������/��������/�������_���������/Rows/CurrentDefenseOtherStruct">
            <xsl:if test="position() &gt; 2 and position() &lt; 5">
              <tr align="center">
                <xsl:if test="position()=3">
                  <td>
                    I0&#62;
                  </td>
                </xsl:if>
                <xsl:if test="position()=4">
                  <td>
                    I0&#62;&#62;
                  </td>
                </xsl:if>
                <td><xsl:value-of select="�����"/></td>
			          <td><xsl:value-of select="����������"/></td>
			          <td><xsl:for-each select="����_��_U"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="U����"/></td>
			          <td><xsl:value-of select="�����������"/></td>
			          <td><xsl:value-of select="�����������_����������"/></td>
			          <td><xsl:value-of select="��������"/></td>
			          <td><xsl:value-of select="�������_������������"/></td>
			          <td><xsl:value-of select="�����_������������"/></td>
			          <td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="�������_���������"/></td>
			          <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="���"/></td>
		          </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>

          <b>������ In</b>
          <table border="1" cellspacing="0">
            <tr bgcolor="FFCC66">
              <th>�������</th>
              <th>�����</th>
              <th>����������</th>
              <th>���� �� U</th>
              <th>U����, �</th>
              <th>�����������</th>
              <th>������.����</th>
              <th>��������</th>
              <th>���. ������������, In</th>
              <xsl:if test="$vers &gt; 70.03">
                <th>�������� ����</th>
              </xsl:if>
              <th>t����, ��</th>
              <th>���������</th>
              <th>t���, ��</th>
              <th>����</th>
              <th>���</th>
              <th>���</th>
              <th>�����������</th>
            </tr>
            <xsl:for-each select="��5V70/�������/��������/�������_���������/Rows/CurrentDefenseOtherStruct">
              <xsl:if test="position() &gt; 4 and position() &lt; 7">
                <tr align="center">
                  <xsl:if test="position()=5">
                    <td>
                      In&#62;
                    </td>
                  </xsl:if>
                  <xsl:if test="position()=6">
                    <td>
                      In&#62;&#62;
                    </td>
                  </xsl:if>
                  <td>
                    <xsl:value-of select="�����"/>
                  </td>
                  <td>
                    <xsl:value-of select="����������"/>
                  </td>
                  <td>
                    <xsl:for-each select="����_��_U">
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:value-of select="U����"/>
                  </td>
                  <td>
                    <xsl:value-of select="�����������"/>
                  </td>
                  <td>
                    <xsl:value-of select="�����������_����������"/>
                  </td>
                  <td>
                    <xsl:value-of select="��������"/>
                  </td>
                  <td>
                    <xsl:value-of select="�������_������������"/>
                  </td>
                  <xsl:if test="$vers &gt; 70.03">
                    <td>
                      <xsl:value-of select="��������_����"/>
                    </td>
                  </xsl:if>
                  <td>
                    <xsl:value-of select="�����_������������"/>
                  </td>
                  <td>
                    <xsl:for-each select="���������">
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:value-of select="�������_���������"/>
                  </td>
                  <td>
                    <xsl:for-each select="����">
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:for-each select="���">
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:for-each select="���">
                      <xsl:if test="current() ='false'">	���	</xsl:if>
                      <xsl:if test="current() ='true'">	����	</xsl:if>
                    </xsl:for-each>
                  </td>
                  <td>
                    <xsl:value-of select="���"/>
                  </td>
                </tr>
              </xsl:if>
            </xsl:for-each>
          </table>

        <p></p>

        <b>������ I�</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
            <th>���� �� U</th>
            <th>U����, �</th>
            <th>���. ������������, In</th>
            <th>t����, ��</th>
            <th>���������</th>
            <th>t���, ��</th>
		        <th>����</th>
		        <th>���</th>
		        <th>���</th>
		        <th>�����������</th>
		      </tr>
          <xsl:for-each select="��5V70/�������_��������������/��������/Rows/AddCurrentDefenseStruct">
            <xsl:if test="position() = 1">
              <tr align="center">
                <td>I�</td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="����������"/></td>
			          <td><xsl:for-each select="����_��_U"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="U����"/></td>
			          <td><xsl:value-of select="�������_In"/></td>
			          <td><xsl:value-of select="�����_������������"/></td>
			          <td><xsl:for-each select="���������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="�������_���������"/></td>
			          <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="���"/></td>
		          </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>
        
        <b>������ I2/I1</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>�������</th>
            <th>�����</th>
            <th>����������</th>
		        <th>���. ������������, %</th>
		        <th>t����, ��</th>
		        <th>����</th>
		        <th>���</th>
		        <th>���</th>
		        <th>�����������</th>
		      </tr>
          <xsl:for-each select="��5V70/�������_��������������/��������/Rows/AddCurrentDefenseStruct">
            <xsl:if test="position() = 2">
              <tr align="center">
                <td>I2/I1</td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="����������"/></td>
			          <td><xsl:value-of select="�������"/></td>
			          <td><xsl:value-of select="�����_������������"/></td>
			          <td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="���"/></td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        </td>
      </table>

    <h3><b>������ U</b></h3>	
    <table border="1" cellspacing="0" >
      <td>
        <b>������ U></b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th> 
		</tr>
		
		 <xsl:for-each select="��5V70/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &lt; 3">
		 <tr align="center">
       <xsl:if test="position()=1">
         <td>
           U&#62;
         </td>
       </xsl:if>
       <xsl:if test="position()=2">
         <td>
           U&#62;&#62;
         </td>
       </xsl:if>
       <td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_1"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
		 
		 <b>������ U&#60;</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="FFCC66">
            <th>C������</th>
            <th>�����</th>
            <th>����-��</th>
            <th>��������</th>
            <th>U����, �</th>
            <th>T����, ��</th>
            <th>�������</th>
            <th>��� ��</th>
            <th>U��, �</th>
            <th>T��, ��</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
            <th>�����������</th>
            <th>�����</th>
            <th>���������� U&#60; 5�</th>
          </tr>

          <xsl:for-each select="��5V70/����������/��������/Rows/VoltageDefenseStruct">
            <xsl:if test="position() &gt; 2 and position() &lt; 5">
              <tr align="center">
                <xsl:if test="position()=3">
                  <td>
                    U&#60;
                  </td>
                </xsl:if>
                <xsl:if test="position()=4">
                  <td>
                    U&#60;&#60;
                  </td>
                </xsl:if>
                <td><xsl:value-of select="Mode"/></td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="��������_1"/></td>
                <td><xsl:value-of select="�������_����"/></td>
                <td><xsl:value-of select="�������_������������"/></td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:if test="current() ='false'">���	</xsl:if>
                    <xsl:if test="current() ='true'">����</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���_��">
                    <xsl:if test="current() ='false'">���	</xsl:if>
                    <xsl:if test="current() ='true'">����</xsl:if>
                  </xsl:for-each>
                </td>
                <td><xsl:value-of select="�������_��"/></td>
                <td><xsl:value-of select="�����_��"/></td>
                <td>
                  <xsl:for-each select="����">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="���">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:for-each select="�����_�������">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:for-each select="����������5�">
                    <xsl:if test="current() ='false'">	���	</xsl:if>
                    <xsl:if test="current() ='true'">	����	</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
		 
	<b>������ U2></b>
		<table border="1" cellspacing="0">

		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th>
		</tr>
		
		 <xsl:for-each select="��5V70/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &gt; 4 and position() &lt; 7">
		 <tr align="center">
       <xsl:if test="position()=5">
         <td>
           U2&#62;
         </td>
       </xsl:if>
       <xsl:if test="position()=6">
         <td>
           U2&#62;&#62;
         </td>
       </xsl:if>
			<td><xsl:value-of select="Mode"/></td>
			<td><xsl:value-of select="����������"/></td>
			<td><xsl:value-of select="��������_2"/></td>
			<td><xsl:value-of select="�������_����"/></td>
			<td><xsl:value-of select="�������_������������"/></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="�������_��"/></td>			
			<td><xsl:value-of select="�����_��"/></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
	
	<b>������ U0></b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
	     <th>C������</th>
         <th>�����</th>
         <th>����-��</th>
         <th>��������</th>
         <th>U����, �</th>
         <th>T����, ��</th>
         <th>�������</th>
         <th>��� ��</th>
         <th>U��, �</th>
         <th>T��, ��</th>
         <th>����</th>
         <th>���</th>
         <th>���</th>
		 <th>�����������</th>
        <th>�����</th>
		</tr>
		
		 <xsl:for-each select="��5V70/����������/��������/Rows/VoltageDefenseStruct">
		 <xsl:if test="position() &gt; 6 and position() &lt; 9">
		 <tr align="center">
       <xsl:if test="position()=7">
         <td>
           U0&#62;
         </td>
       </xsl:if>
       <xsl:if test="position()=8">
         <td>
           U0&#62;&#62;
         </td>
       </xsl:if>
       <td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="��������_3"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 </table>
	
	</td>
	</table>

	 <h3><b>������ F</b></h3>
		<table border="1" cellspacing="0">
		<td>
		
		<b>������ F></b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
     <th>�������</th>
     <th>�����</th>
		 <th>����������</th>
     <th>������� ����., ��</th>
		 <th>����� ����., ��</th>
		 <th>�������</th>
		 <th>��� ��</th>
		 <th>������� ��, ��</th>
		 <th>����� ��������, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		 <th>�����</th>
		</tr>
		 <xsl:for-each select="��5V70/���������/��������/Rows/FrequencyDefenseStruct">
		<xsl:if test="position() &lt; 3">
		 <tr align="center">
       <xsl:if test="position()=1">
         <td>
           F&#62;
         </td>
       </xsl:if>
       <xsl:if test="position()=2">
         <td>
           F&#62;&#62;
         </td>
       </xsl:if>
     <td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
		
		<b>������ F&#60;</b>
		<table border="1" cellspacing="0">
		<tr bgcolor="FFCC66">
     <th>�������</th>
     <th>�����</th>
		 <th>����������</th>
     <th>������� ����., ��</th>
		 <th>����� ����., ��</th>
		 <th>�������</th>
		 <th>��� ��</th>
		 <th>������� ��, ��</th>
		 <th>����� ��������, ��</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>�����������</th>
		 <th>�����</th>
		</tr>
		 <xsl:for-each select="��5V70/���������/��������/Rows/FrequencyDefenseStruct">
		<xsl:if test="position() &gt; 2">
		 <tr align="center">
       <xsl:if test="position()=3">
         <td>
           F&#60;
         </td>
       </xsl:if>
       <xsl:if test="position()=4">
         <td>
           F&#60;&#60;
         </td>
       </xsl:if>
       <td><center><xsl:value-of select="Mode"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_����"/></center></td>
			<td><center><xsl:value-of select="�������_������������"/></center></td>
			<td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_��"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�������_��"/></center></td>			
			<td><center><xsl:value-of select="�����_��"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:value-of select="���"/></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		</table>
		
		</td>
		</table>
   </body>
  </html>
</xsl:template>
</xsl:stylesheet>
