using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v70.Configuration.Structures;
using BEMN.MR5.v70.Configuration.Structures.Apv;
using BEMN.MR5.v70.Configuration.Structures.Avr;
using BEMN.MR5.v70.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v70.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v70.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v70.Configuration.Structures.FaultSignal;
using BEMN.MR5.v70.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v70.Configuration.Structures.Indicators;
using BEMN.MR5.v70.Configuration.Structures.Keys;
using BEMN.MR5.v70.Configuration.Structures.Ls;
using BEMN.MR5.v70.Configuration.Structures.Lzsh;
using BEMN.MR5.v70.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v70.Configuration.Structures.OMP;
using BEMN.MR5.v70.Configuration.Structures.OscilloscopeConfig;
using BEMN.MR5.v70.Configuration.Structures.Relay;
using BEMN.MR5.v70.Configuration.Structures.Switch;
using BEMN.MR5.v70.Configuration.Structures.Vls;
using BEMN.MR5.v70.Configuration.Structures.VoltageDefenses;
using BEMN.MR5.v70.Configuration.StructuresNew;

namespace BEMN.MR5.v70.Configuration
{
    public partial class Mr5V70ConfigurationForm : Form, IFormView
    {
        #region Const
        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";
        private const string XML_HEAD = "MR5_SET_POINTS";
        //private const string MR5v70_00_BASE_CONFIG_PATH = "\\MR5\\MR5v70.00_BaseConfig.bin";
        //private const string MR5v70_03_BASE_CONFIG_PATH = "\\MR5\\MR5v70.03_BaseConfig.bin";
        private const string MR5v70_BASE_CONFIG_PATH = "\\MR5\\MR5v{0:F2}_BaseConfig.bin";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";
        private const string GROUP_SETPOIN_OLD_LABEL = "������ �������";
        private static readonly Size GROUPBOX_OLD_SIZE = new Size(203, 152);
        private static readonly Point GROUPBOX_OLD_POINT = new Point(8, 390);

        #endregion


        #region [Private fields]

        private readonly MR5Device _device;
        private readonly MemoryEntity<ConfigurationStructV70> _configuration;
        private readonly MemoryEntity<ConfigurationStructV70New> _configurationNew;
        private RadioButtonSelector _groupSelector;
        private readonly double _version;
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;

        private ConfigurationStructV70 _currentSetpointsStruct;

        #region [Validators]

        private NewStructValidator<MeasureTransStructV70> _measureTransValidator;
        private NewStructValidator<OmpStruct> _ompValidator;
        private NewCheckedListBoxValidator<KeysStruct> _keysValidator;
        private NewStructValidator<ExternalSignalStruct> _externalSignalsValidator;
        private NewStructValidator<FaultStruct> _faultValidator;

        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;

        private NewStructValidator<SwitchStruct> _switchValidator;
        private NewStructValidator<ApvStruct> _apvValidator;
        private NewStructValidator<AvrStruct> _avrValidator;
        private NewStructValidator<LpbStruct> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct> _externalDefenseValidatior;

        private NewStructValidator<CornerStruct> _cornerValidator;
        private NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> _currentDefenseValidatior ;
        private NewDgwValidatior<AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct> _currentDefensesOtherValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> _addCurrentDefenseValidator;
        private SetpointsValidator<AllSetpointsStruct, SetpointStruct> _currentSetpointsValidator;
        private StructUnion<SetpointStruct> _currentSetpointUnion;
        private SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct> _currentAddSetpointsValidator;

        private NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct> _frequencyDefenseValidatior;
        private SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct> _frequencySetpoints;


        private NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct> _voltageDefenseValidatior;
        private SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct> _voltageSetpoints;

        #region [���]

        /// <summary>
        /// ������ ��������� ��� ���
        /// </summary>
        private CheckedListBox[] _vlsBoxes;

        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;

        #endregion [���]

        private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<OscopeConfigStruct> _oscValidator;
        private StructUnion<ConfigurationStructV70> _configurationUnion;

        // ����� ������������
        private StructUnion<GroupSetpoint> _groupSetpoinStructUnion;
        private SetpointsValidator<AllDefensesSetpoint, GroupSetpoint> _alldefensesValidator;
        private StructUnion<ConfigurationStructV70New> _configurationUnionNew;
        private ConfigurationStructV70New _configurationStructV70New;
        private ComboboxSelector _comboBoxSelector;

        #endregion [Validators]

        #endregion [Private fields]

        #region C'tor
        public Mr5V70ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr5V70ConfigurationForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = this._device.Configuration70;
            this._configurationNew = this._device.Configuration70New;
            this._version = Common.VersionConverter(this._device.DeviceVersion);

            if (_version > 70.06)
            {
                this._configurationNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
                this._configurationNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);
                });
                this._configurationNew.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configurationNew.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configurationNew.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);

                });
                this._configurationNew.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {

                    if (_device.MB.NetworkEnabled)
                    {
                        this._device.WriteConfiguration(new TimeSpan(50));
                    }
                    else
                    {
                        this._device.WriteConfiguration();
                    }
                    this.IsProcess = false;
                    this._statusLabel.Text = "������������ ������� ��������";
                });
                this._exchangeProgressBar.Maximum = this._configurationNew.Slots.Count;
            }
            else
            {
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
                this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);

                });
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);

                });
                this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._device.WriteConfiguration();
                    this.IsProcess = false;
                    this._statusLabel.Text = "������������ ��������";
                });
                this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            }
            
            this._currentSetpointsStruct = new ConfigurationStructV70();
            this.Init();

            //this._configurationUnion.Set(this._currentSetpointsStruct);
            //this._configurationUnion.Reset();

            this._groupSelector = new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup,
                this._ChangeGroupButton2);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;

            if (_version < 70.04)
            {
                this._tokDefenseGrid5.Visible = false;
                this._tokDefendTabPage.Location = new System.Drawing.Point(4, 22);
                this._tokDefendTabPage.Size = new System.Drawing.Size(993, 504);
                this._tokDefenseGrid2.Location = new System.Drawing.Point(3, 187);
                this._tokDefenseGrid2.Size = new System.Drawing.Size(987, 180);
                this._tokDefenseGrid3.Location = new System.Drawing.Point(3, 370);
                this._tokDefenseGrid4.Location = new System.Drawing.Point(3, 420);
                this.mainPanel.Size = new System.Drawing.Size(1021, 696);
                this.ClientSize = new System.Drawing.Size(1021, 696);
                this.MinimumSize = new System.Drawing.Size(1015, 690);
            }

            if (this._version < 70.07)
            {
                this.OldCopyGroupBox.Visible = true;
                this.NewCopyGroupBox.Visible = false;
                this.group1label.Text = GROUP_SETPOIN_OLD_LABEL;
                this.groupBox3.Size = GROUPBOX_OLD_SIZE;
                this.groupBox7.Location = GROUPBOX_OLD_POINT;
                this.group2label.Visible = this.group2label.Visible = this.group2label.Visible = false;
                this._constraintGroup2Combo.Visible = this._constraintGroup3Combo.Visible = this._constraintGroup4Combo.Visible = false; 
            }
        }

        private void Init()
        {
            this.copyGroupsCombo.DataSource = StringsConfig.CopyGroups;
            this._lsBoxes = new[]
            {
                this._inputSignals1,
                this._inputSignals2,
                this._inputSignals3,
                this._inputSignals4,
                this._inputSignals9,
                this._inputSignals10,
                this._inputSignals11,
                this._inputSignals12

            };

            ToolTip toolTip = new ToolTip();
            this._measureTransValidator = new NewStructValidator<MeasureTransStructV70>
                (
                toolTip,
                new ControlInfoText(this._TT_Box, new CustomUshortRule(0, 5000)),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo1000),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._TT_typeCombo, StringsConfig.TtType),
                new ControlInfoCombo(this._TN_typeCombo, StringsConfig.TnType),
                new ControlInfoText(this._TN_Box, RulesContainer.Ustavka128000),
                new ControlInfoCombo(this._TN_dispepairCombo, StringsConfig.LogicSignals),
                new ControlInfoText(this._TNNP_Box, RulesContainer.Ustavka128000),
                new ControlInfoCombo(this._TNNP_dispepairCombo, StringsConfig.LogicSignals)
                );

            this._ompValidator = new NewStructValidator<OmpStruct>(
                toolTip, 
                new ControlInfoCheck(this._ompCheckBox),
                new ControlInfoText(this._HUD_Box, RulesContainer.DoubleTo1));

            this._externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._keyOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._extOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._extOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._signalizationCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._constraintGroup1Combo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._blockSDTU,StringsConfig.LogicSignals)
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._disrepairCheckList,
                    StringsConfig.FaultsV2)),
                new ControlInfoText(this._releDispepairBox, RulesContainer.IntTo3M)
                );

            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            foreach (DataGridView gridView in _lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    this._lsBoxes[i],
                    InputLogicStruct.DISCRETS_COUNT,
                    toolTip,
                    new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            this._inputLogicUnion = new StructUnion<InputLogicSignalStruct>(this._inputLogicValidator);

            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfig.LogicSignals),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfig.LogicSignals),
                new ControlInfoText(this._switcherTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherTokBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherDurationBox, RulesContainer.IntTo3M),

                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfig.Zr)
                );

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                toolTip,
                new ControlInfoCombo(this.apv_conf, StringsConfig.ApvModes),
                new ControlInfoCombo(this.apv_blocking, StringsConfig.LogicSignals),
                new ControlInfoText(this.apv_time_blocking, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_ready, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_1krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_2krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_3krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_4krat, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvStartCheckBox)

                );
            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                toolTip,
                new ControlInfoCheck(this.avr_supply_off),
                new ControlInfoCheck(this.avr_switch_off),
                new ControlInfoCheck(this.avr_self_off),
                new ControlInfoCheck(this.avr_abrasion_switch),
                new ControlInfoCombo(this.avr_start, StringsConfig.LogicSignals),
                new ControlInfoCombo(this.avr_blocking, StringsConfig.LogicSignals),
                new ControlInfoCombo(this.avr_reset_blocking, StringsConfig.LogicSignals),
                new ControlInfoCombo(this.avr_abrasion, StringsConfig.LogicSignals),
                new ControlInfoCombo(this.avr_return, StringsConfig.LogicSignals),
                new ControlInfoText(this.avr_time_abrasion, RulesContainer.IntTo3M),
                new ControlInfoText(this.avr_time_return, RulesContainer.IntTo3M),
                new ControlInfoText(this.avr_disconnection, RulesContainer.IntTo3M),
                new ControlInfoCheck(this.avr_permit_reset_switch)
                );
            this._lpbValidator = new NewStructValidator<LpbStruct>
                (
                toolTip,
                new ControlInfoCombo(this.comboBox1, StringsConfig.Lzs),
                new ControlInfoText(this.lzsh_constraint, RulesContainer.Ustavka40)
                );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct>
                (
                this._externalDefenseGrid,
                8,
                toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME), //1
                new ColumnInfoCombo(StringsConfig.Modes), //2
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals), //3
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals), //4
                new ColumnInfoText(RulesContainer.IntTo3M), //5
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(5, false, false, 6, 7, 8)
                        )
                }
            };

            Func<DataGridView, IValidatingRule> currentDefenseFunc = dgv =>
            {
                try
                {
                    DataGridViewCell curCell = dgv.Tag as DataGridViewCell;
                    if (curCell == null)
                    {
                        return RulesContainer.IntTo3M;
                    }
                    DataGridViewCell masterCell = dgv[11, curCell.RowIndex];
                    if (masterCell.Value.ToString() == StringsConfig.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            };

            // ���� � ������� ������
            this._cornerValidator = new NewStructValidator<CornerStruct>
                (
                toolTip,
                new ControlInfoText(this._tokDefenseIbox, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI0box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI2box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseInbox, RulesContainer.UshortTo360)
                );

            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                    this._tokDefenseGrid1, 4, toolTip,
                    new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME), //0
                    new ColumnInfoCombo(StringsConfig.Modes), //1
                    new ColumnInfoCombo(StringsConfig.LogicSignals), //2 
                    new ColumnInfoCheck(), //3
                    new ColumnInfoText(RulesContainer.Ustavka256), //4
                    new ColumnInfoCombo(StringsConfig.Direction), //5
                    new ColumnInfoCombo(StringsConfig.DirectionBlock), //6
                    new ColumnInfoCombo(StringsConfig.TokParameter), //7
                    new ColumnInfoText(RulesContainer.Ustavka40), //8
                    new ColumnInfoCombo(StringsConfig.FeatureLight), //9
                    new ColumnInfoTextDgvDependent(currentDefenseFunc, this._tokDefenseGrid1), //10
                    new ColumnInfoCheck(), //11
                    new ColumnInfoText(RulesContainer.IntTo3M), //12
                    new ColumnInfoCheck(), //13
                    new ColumnInfoCheck(), //14
                    new ColumnInfoCheck(), //15
                    new ColumnInfoCombo(StringsConfig.OscModes) //16
                )
            {
                TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._tokDefenseGrid1,
                            new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                                15, 16),
                            new TurnOffRule(3, false, false, 4),
                            new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                            new TurnOffRule(11, false, false, 12)
                        )
                    }
            };

            if (_version < 70.04)
            {
                this._currentDefensesOtherValidatior =
                    new NewDgwValidatior<AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct>
                    (
                        this._tokDefenseGrid2,
                        6,
                        toolTip,
                        new ColumnInfoCombo(StringsConfig.OtherCurrentDefensesNames, ColumnsType.NAME),//0
                        new ColumnInfoCombo(StringsConfig.Modes), //1
                        new ColumnInfoCombo(StringsConfig.LogicSignals), //2 
                        new ColumnInfoCheck(), //3
                        new ColumnInfoText(RulesContainer.Ustavka256), //4
                        new ColumnInfoCombo(StringsConfig.Direction), //5
                        new ColumnInfoCombo(StringsConfig.DirectionBlock), //6
                        new ColumnInfoCombo(StringsConfig.TokParameter2), //7
                        new ColumnInfoText(RulesContainer.Ustavka40), //8
                        new ColumnInfoCombo(StringsConfig.CurrentSettings),
                        new ColumnInfoText(RulesContainer.IntTo3M),  //9
                        new ColumnInfoCheck(), //10
                        new ColumnInfoText(RulesContainer.IntTo3M), //11
                        new ColumnInfoCheck(), //12
                        new ColumnInfoCheck(), //13
                        new ColumnInfoCheck(), //14
                        new ColumnInfoCombo(StringsConfig.OscModes) //15
                    )
                        {
                            TurnOff = new[]
                            {
                                new TurnOffDgv
                                (
                                    this._tokDefenseGrid2,
                                    new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                                    new TurnOffRule(3, false, false, 4),
                                    new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                                    new TurnOffRule(10, false, false, 11)
                                )
                            },
                            AddRules = new[] { new TextboxCellRule(4, 8, RulesContainer.Ustavka5),
                                new TextboxCellRule(5, 8, RulesContainer.Ustavka5) }
                        };

            }
            else if (_version > 70.06)
            {
                this._currentDefensesOtherValidatior =
                   new NewDgwValidatior<AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct>
                   (
                     new[] { this._tokDefenseGrid2, this._tokDefenseGrid5 },
                     new[] { 4, 2 },
                     toolTip,
                     new ColumnInfoCombo(StringsConfig.OtherCurrentDefensesNames, ColumnsType.NAME), //0
                     new ColumnInfoCombo(StringsConfig.Modes), //1
                     new ColumnInfoCombo(StringsConfig.LogicSignals), //2 
                     new ColumnInfoCheck(), //3
                     new ColumnInfoText(RulesContainer.Ustavka256), //4
                     new ColumnInfoCombo(StringsConfig.Direction), //5
                     new ColumnInfoCombo(StringsConfig.DirectionBlock), //6
                     new ColumnInfoCombo(StringsConfig.TokParameter2), //7
                     new ColumnInfoText(RulesContainer.Ustavka5), //8
                     new ColumnInfoCombo(StringsConfig.CurrentSettings, ColumnsType.COMBO, false, true),
                     new ColumnInfoText(RulesContainer.IntTo3M), //9
                     new ColumnInfoCheck(), //10
                     new ColumnInfoText(RulesContainer.IntTo3M), //11
                     new ColumnInfoCheck(), //12
                     new ColumnInfoCheck(), //13
                     new ColumnInfoCheck(), //14
                     new ColumnInfoCombo(StringsConfig.OscModes) //15
                 )

                   {
                       TurnOff = new[]
                     {
                           new TurnOffDgv
                           (
                               this._tokDefenseGrid2,
                               new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                   14, 15, 16),
                               new TurnOffRule(3, false, false, 4),
                               new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                               new TurnOffRule(11, false, false, 12)
                           ),
                           new TurnOffDgv
                           (
                               this._tokDefenseGrid5,
                               new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                   14, 15, 16),
                               new TurnOffRule(3, false, false, 4),
                               new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                               new TurnOffRule(11, false, false, 12),
                               new TurnOffRule(7, StringsConfig.TokParameter2[1], false, 9)
                           )
                     }
                   };
            }
            else
            {
                this._currentDefensesOtherValidatior =
                    new NewDgwValidatior<AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct>
                    (
                      new[] { this._tokDefenseGrid2, this._tokDefenseGrid5 },
                      new[] { 4, 2 },
                      toolTip,
                      new ColumnInfoCombo(StringsConfig.OtherCurrentDefensesNames, ColumnsType.NAME), //0
                      new ColumnInfoCombo(StringsConfig.Modes), //1
                      new ColumnInfoCombo(StringsConfig.LogicSignals), //2 
                      new ColumnInfoCheck(), //3
                      new ColumnInfoText(RulesContainer.Ustavka256), //4
                      new ColumnInfoCombo(StringsConfig.Direction), //5
                      new ColumnInfoCombo(StringsConfig.DirectionBlock), //6
                      new ColumnInfoCombo(StringsConfig.TokParameter2), //7
                      new ColumnInfoText(RulesContainer.Ustavka40), //8
                      new ColumnInfoCombo(StringsConfig.CurrentSettings, ColumnsType.COMBO, false, true),
                      new ColumnInfoText(RulesContainer.IntTo3M), //9
                      new ColumnInfoCheck(), //10
                      new ColumnInfoText(RulesContainer.IntTo3M), //11
                      new ColumnInfoCheck(), //12
                      new ColumnInfoCheck(), //13
                      new ColumnInfoCheck(), //14
                      new ColumnInfoCombo(StringsConfig.OscModes) //15
                  )

                    {
                        TurnOff = new[]
                      {
                           new TurnOffDgv
                           (
                               this._tokDefenseGrid2,
                               new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                   14, 15, 16),
                               new TurnOffRule(3, false, false, 4),
                               new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                               new TurnOffRule(11, false, false, 12)
                           ),
                           new TurnOffDgv
                           (
                               this._tokDefenseGrid5,
                               new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                                   14, 15, 16),
                               new TurnOffRule(3, false, false, 4),
                               new TurnOffRule(5, StringsConfig.Direction[0], false, 6),
                               new TurnOffRule(11, false, false, 12),
                               new TurnOffRule(7, StringsConfig.TokParameter2[1], false, 9)
                           )
                      }
                    };
            }

            this._addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (
                new [] {this._tokDefenseGrid3, this._tokDefenseGrid4},
                new [] {1,1},
                toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes), //0
                new ColumnInfoCombo(StringsConfig.LogicSignals), //1
                new ColumnInfoCheck(), //2 
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoText(RulesContainer.Ustavka5, true, false),   //4 
                new ColumnInfoText(RulesContainer.Ustavka100, false, true), //4
                new ColumnInfoText(RulesContainer.IntTo3M), //5
                new ColumnInfoCheck(), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCheck(), //9
                new ColumnInfoCheck(), //10
                new ColumnInfoCombo(StringsConfig.OscModes) //11
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid3,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(7, false, false, 8)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid4,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
                        )
                }
            };

           
            this._currentSetpointUnion = new StructUnion<SetpointStruct>
            (
                this._cornerValidator,
                this._currentDefenseValidatior,
                this._currentDefensesOtherValidatior
            );
          
           

            this._currentSetpointsValidator = new SetpointsValidator<AllSetpointsStruct, SetpointStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._currentSetpointUnion
                );

            this._currentAddSetpointsValidator = new SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._addCurrentDefenseValidator
                );
            
            this._frequencyDefenseValidatior = new NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct>
                (
                this._frequenceDefensesGrid,
                4,
                new ToolTip(),//toolTip,
                new ColumnInfoCombo(StringsConfig.FrequencyDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes), //0
                new ColumnInfoCombo(StringsConfig.LogicSignals), //1
                new ColumnInfoText(RulesContainer.Ustavka40To60), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoCheck(), //5
                new ColumnInfoText(RulesContainer.Ustavka40To60), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7        
                new ColumnInfoCheck(), //8              
                new ColumnInfoCheck(), //9
                new ColumnInfoCheck(), //10
                new ColumnInfoCombo(StringsConfig.OscModes), //11
                new ColumnInfoCheck() //12
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._frequenceDefensesGrid,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(5, false, false, 6, 7, 8)
                        )
                }
            };

            this._frequencySetpoints = new SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),this._frequencyDefenseValidatior
                );

            this._voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct>
                (
                new[] {this._voltageDefensesGrid1, this._voltageDefensesGrid11, this._voltageDefensesGrid2, this._voltageDefensesGrid3},
                new[] {2, 2, 2, 2},
                toolTip,
                new ColumnInfoCombo(StringsConfig.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes), //0
                new ColumnInfoCombo(StringsConfig.LogicSignals), //1
                
                new ColumnInfoCombo(StringsConfig.VoltageParameterU, ColumnsType.COMBO, true, true, false, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter1, ColumnsType.COMBO, false, false, true, false), //1
                new ColumnInfoCombo(StringsConfig.VoltageParameter2, ColumnsType.COMBO, false, false, false, true), //1
                
                new ColumnInfoText(RulesContainer.Ustavka256), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoCheck(), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7        
                new ColumnInfoCheck(), //8              
                new ColumnInfoCheck(), //9
                new ColumnInfoCheck(), //10
                new ColumnInfoCombo(StringsConfig.OscModes), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(false, true, false, false)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid1,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                            15, 16, 17),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid11,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                            15, 16, 17),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid2,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                            15, 16),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid3,
                        new TurnOffRule(1, StringsConfig.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                            15, 16),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        )
                }
            };

            this._voltageSetpoints = new SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._voltageDefenseValidatior

                );

            #region [���]

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1,
                this.VLScheckedListBox2,
                this.VLScheckedListBox3,
                this.VLScheckedListBox4,
                this.VLScheckedListBox5,
                this.VLScheckedListBox6,
                this.VLScheckedListBox7,
                this.VLScheckedListBox8
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i],
                    StringsConfig.VlsSignals);
            }
            this._vlsUnion = new StructUnion<OutputLogicSignalStruct>(this._vlsValidator);

            #endregion [���]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.IntTo3M)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._oscValidator = new NewStructValidator<OscopeConfigStruct>
                (
                toolTip,
                new ControlInfoText(this.oscPercent, new CustomUshortRule(1, 100)),
                new ControlInfoCombo(this.oscFix, StringsConfig.OscFixation),
                new ControlInfoCombo(this.oscLengthMode, StringsConfig.OscSize.ToList())
                );

            this._keysValidator = new NewCheckedListBoxValidator<KeysStruct>(this._keysCheckedListBox, StringsConfig.KeyNumbers);

            if (this._version > 70.06)
            {
                NewStructValidator<ExternalSignalsNew> externalSignalsValidator = new NewStructValidator
                    <ExternalSignalsNew>
                    (
                    toolTip,
                    new ControlInfoCombo(this._keyOffCombo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._keyOnCombo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._extOffCombo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._extOnCombo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._signalizationCombo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup1Combo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._blockSDTU, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup2Combo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup3Combo, StringsConfig.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup4Combo, StringsConfig.LogicSignals)
                    );

                this._groupSetpoinStructUnion = new StructUnion<GroupSetpoint>
                    (
                    this._cornerValidator,
                    this._currentDefenseValidatior,
                    this._currentDefensesOtherValidatior,
                    this._addCurrentDefenseValidator,
                    this._frequencyDefenseValidatior,
                    this._voltageDefenseValidatior
                    );

                this._alldefensesValidator = new SetpointsValidator<AllDefensesSetpoint, GroupSetpoint>
                    (
                        this._comboBoxSelector = new ComboboxSelector(this.currentGroupCombo, StringsConfig.Groups), this._groupSetpoinStructUnion
                    );
                this._comboBoxSelector.OnSelect += ReadAllData;

                this._configurationUnionNew = new StructUnion<ConfigurationStructV70New>
                    (
                        this._measureTransValidator, 
                        this._ompValidator, 
                        externalSignalsValidator, 
                        this._faultValidator,
                        this._inputLogicUnion, 
                        this._switchValidator, 
                        this._apvValidator, 
                        this._avrValidator,
                        this._lpbValidator, 
                        this._externalDefenseValidatior,
                        this._alldefensesValidator,
                        this._vlsUnion,
                        this._releyValidator,
                        this._indicatorValidator, 
                        this._oscValidator, 
                        this._keysValidator
                    );
            }

            else
            {
                this._configurationUnion = new StructUnion<ConfigurationStructV70>
                (
                    this._measureTransValidator,
                    this._ompValidator,
                    this._externalSignalsValidator,
                    this._faultValidator,
                    this._inputLogicUnion,
                    this._switchValidator,
                    this._apvValidator,
                    this._avrValidator,
                    this._lpbValidator,
                    this._externalDefenseValidatior,
                    this._currentSetpointsValidator,
                    this._currentAddSetpointsValidator,
                    this._frequencySetpoints,
                    this._voltageSetpoints,
                    this._vlsUnion,
                    this._releyValidator,
                    this._indicatorValidator,
                    this._oscValidator,
                    this._keysValidator
                );
            }
            
        }
        #endregion C'tor

        #region Misc
        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        private void _groupSelector_NeedCopyGroup()
        {
            this.WriteConfiguration();
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this._currentSetpointsStruct.AllAddSetpoints.Reserve =
                    this._currentSetpointsStruct.AllAddSetpoints.Main.Clone<AllAddCurrentDefensesStruct>();

                this._currentSetpointsStruct.AllSetpoints.Reserv =
                    this._currentSetpointsStruct.AllSetpoints.Main.Clone<SetpointStruct>();

                this._currentSetpointsStruct.AllVoltage.Reserve =
                    this._currentSetpointsStruct.AllVoltage.Main.Clone<AllVoltageDefensesStruct>();

                this._currentSetpointsStruct.AllFrequency.Reserve =
                    this._currentSetpointsStruct.AllFrequency.Main.Clone<AllFrequencyDefensesStruct>();
            }
            else
            {
                this._currentSetpointsStruct.AllAddSetpoints.Main =
                    this._currentSetpointsStruct.AllAddSetpoints.Reserve.Clone<AllAddCurrentDefensesStruct>();

                this._currentSetpointsStruct.AllSetpoints.Main =
                    this._currentSetpointsStruct.AllSetpoints.Reserv.Clone<SetpointStruct>();

                this._currentSetpointsStruct.AllVoltage.Main =
                    this._currentSetpointsStruct.AllVoltage.Reserve.Clone<AllVoltageDefensesStruct>();

                this._currentSetpointsStruct.AllFrequency.Main =
                    this._currentSetpointsStruct.AllFrequency.Reserve.Clone<AllFrequencyDefensesStruct>();
            }
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            string message;
            bool message_b;

            if (_version > 70.06)
            {
                this._configurationStructV70New = this._configurationNew.Value.Clone<ConfigurationStructV70New>();
                this._configurationUnionNew.Set(this._configurationStructV70New);
                message_b = this._configurationUnionNew.Check(out message, false);
            }
            else
            {
                this._currentSetpointsStruct = this._configuration.Value;
                this._configurationUnion.Set(this._currentSetpointsStruct);
                message_b = this._configurationUnion.Check(out message, false);
            }

            if (!message_b)
            {
                MessageBox.Show("� ���������� ���������� ������������ �������");
            }

        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            
            if (this._device.MB.NetworkEnabled)
            {
                this._device.ReadConfiguration(new TimeSpan(50));
            }
            else
            {
                this._device.ReadConfiguration();
            }


            if (!this._device.MB.IsDisconnect)
            {
                this.IsProcess = true;
                if (_version > 70.06)
                {
                    if (this._device.MB.NetworkEnabled)
                    {
                        this._configurationNew.LoadStruct(new TimeSpan(50));
                    }
                    else
                    {
                        this._configurationNew.LoadStruct();
                    }
                }
                else
                {
                    this._configuration.LoadStruct();
                }
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (_version > 70.06)
            {
                if (this._configurationUnionNew.Check(out message, true))
                {
                    this._configurationStructV70New = this._configurationUnionNew.Get();
                    return true;
                }
            }
            else
            {
                if (this._configurationUnion.Check(out message, true))
                {
                    this._currentSetpointsStruct = this._configurationUnion.Get();
                    return true;
                }
            }
            
            MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
            return false;
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
           this.ReadFromFile(); 
        }

        private void ReadFromFile()
        {
            try
            {
                if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
                {
                    this.Deserialize(this._openConfigurationDlg.FileName);
                }
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(binFileName);
            List<byte> values = new List<byte>();

            XmlNode a = doc.FirstChild.SelectSingleNode(string.Format(XML_HEAD));
            if (a == null)
                throw new NullReferenceException();
            values.AddRange(Convert.FromBase64String(a.InnerText));

            if (_version > 70.06)
            {
                if (values.Count > 2016) // ��� ������������� ������������ �� ����, ��� ��� ����� ���� Ethernet.
                {                        // �� ������ ���������������� �������� ������� ���������� �����(RS485 ��� Ethernet)
                    values.RemoveAt(46);
                    values.RemoveAt(46);
                }
                this._configurationStructV70New = this._configurationStructV70New ?? new ConfigurationStructV70New();
                this._configurationStructV70New.InitStruct(values.ToArray());
                this._configurationUnionNew.Set(this._configurationStructV70New);
            }
            else
            {
                if (Common.VersionConverter(this._device.DeviceVersion) < 70.03 && values.Count > 1248) // ��� ������������� ������������ �� ����, ��� ��� ����� ���� Ethernet.
                {                                                                                       // �� ������ ���������������� �������� ������� ���������� �����(RS485 ��� Ethernet)
                    values.RemoveAt(46);
                    values.RemoveAt(46);
                }
                this._currentSetpointsStruct = this._currentSetpointsStruct ?? new ConfigurationStructV70();
                this._currentSetpointsStruct.InitStruct(values.ToArray());
                this._configurationUnion.Set(this._currentSetpointsStruct);
            }
            this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    StructBase config = _version > 70.06
                        ? (StructBase)this._configurationStructV70New
                        : (StructBase)this._currentSetpointsStruct;
                    this._saveConfigurationDlg.FileName = string.Format("��5_�������_������_{0}.bin", this._device.DeviceVersion);
                    if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                    {
                        this.Serialize(this._saveConfigurationDlg.FileName, config);
                    }
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ���������.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ���������� � ����!", "������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }
        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Serialize(string binFileName, StructBase config)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR5"));

                ushort[] values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format(XML_HEAD));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void ShowToolTipBtn()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            toolTip.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            toolTip.SetToolTip(this._resetConfigBut, "��������� ������� �������");
            toolTip.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            toolTip.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            toolTip.SetToolTip(this._saveToXmlButton, "��������� ������������ � HTML ����");
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DialogResult result = MessageBox.Show(string.Format("�������� ������������ ��5 v{0} �{1}?", this._device.DeviceVersion,
                this._device.DeviceNumber), "������", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    if (this.WriteConfiguration())
                    {
                        this._statusLabel.Text = "��� ������ ������������";
                        this.IsProcess = true;
                        this._exchangeProgressBar.Value = 0;

                        if (_version > 70.06)
                        {
                            this._configurationNew.Value = this._configurationStructV70New;
                            if (this._device.MB.NetworkEnabled)
                            {
                                this._configurationNew.SaveStruct(new TimeSpan(50));
                            }
                            else
                            {
                                this._configurationNew.SaveStruct();
                            }
                        }
                        else
                        {
                            this._configuration.Value = this._currentSetpointsStruct;
                            this._configuration.SaveStruct();
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("���������� �������� ������������", "������", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.ResetSetpoints(true);
                this.StartReadConfiguration();
            }
            this.ShowToolTipBtn();
        }

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
        }

        //�������� ������� ������� �� �����, isDialog - ���������� ����� �� ������
        private void ResetSetpoints(bool isDialog)   
        {
            try
            {
                if (!isDialog)
                {
                    DialogResult res = MessageBox.Show(@"��������� ������� �������?", @"������� �������",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (res == DialogResult.No) return;
                }

                XmlDocument doc = new XmlDocument();

                //string fileName = Common.VersionConverter(this._device.DeviceVersion) >= 70.03
                //    ? Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR5v70_03_BASE_CONFIG_PATH)
                //    : Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR5v70_00_BASE_CONFIG_PATH);

                string fileName = Path.GetDirectoryName(Application.ExecutablePath) + string.Format(CultureInfo.InvariantCulture, MR5v70_BASE_CONFIG_PATH, this._device.DeviceVersion);

                Deserialize(fileName);
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch (Exception ex)
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                        "��������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this._configurationUnion.Reset();
                }
                else
                {
                    this._configurationUnion.Reset();
                }
            }
        }

        private void ConfigurationFormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
            if (_version > 70.06)
            {
                this._configurationNew.RemoveStructQueries();
            }
            else
            {
                this._configuration.RemoveStructQueries();
            }
            
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    if (_version < 70.07)
                    {
                        this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                        this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();

                        this._statusLabel.Text = HtmlExport.Export(Resources.MR5_70_Main, Resources.MR5_70_Res,
                            this._currentSetpointsStruct, this._currentSetpointsStruct.DeviceType,
                            this._currentSetpointsStruct.DeviceVersion);
                    }
                    else
                    {
                        ExportGroupForm exportGroup = new ExportGroupForm(4);

                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }

                        this._configurationStructV70New.DeviceVersion = this._device.DeviceVersion;
                        this._configurationStructV70New.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._configurationStructV70New.Group = (int)exportGroup.SelectedGroup;

                        this.SaveToHtml(this._configurationStructV70New, exportGroup.SelectedGroup);
                        exportGroup.IsExport = false;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("������ ���������� �����!", "������!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} ������� ������ {1} ��� ������", "��5", this._device.DeviceVersion);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_70_All_Configuration, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} ������� ������ {1} ������ {2}", "��5", this._device.DeviceVersion, (int)group);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_70_Configuration, fileName, str);
            }
        }

        private void _disrepairCheckList_SelectedValueChanged(object sender, EventArgs e)
        {
            CheckedListBox list = sender as CheckedListBox;
            int index = list.SelectedIndex;
            if (list.Items[index].ToString()
                    .IndexOf("������", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                (sender as CheckedListBox).SetItemChecked(index, false);
            }
        }

        private void _disrepairCheckList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CheckedListBox list = sender as CheckedListBox;
            int index = list.SelectedIndex;
            if (list.Items[index].ToString()
                    .IndexOf("������", StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                (sender as CheckedListBox).SetItemChecked(index, false);
            }
        }

        private void oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int oscLen = OscopeConfigStruct.ONE_OSC_MAX_LEN*2/(2 + this.oscLengthMode.SelectedIndex);
            this.oscLenText.Text = string.Format("{0} ��", oscLen);
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(false);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
          
        }
        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void copyBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string message;
                if (this._groupSetpoinStructUnion.Check(out message, true))
                {
                    GroupSetpoint[] allSetpoints =
                        this.CopySetpoints<AllDefensesSetpoint, GroupSetpoint>(this._groupSetpoinStructUnion, this._alldefensesValidator);

                    this._configurationStructV70New.AllDefensesSetpoint.Setpoints = allSetpoints;
                    this._alldefensesValidator.Set(this._configurationStructV70New.AllDefensesSetpoint);
                    MessageBox.Show("����������� ���������");
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator) where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this.copyGroupsCombo.SelectedItem == StringsConfig.CopyGroups.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this.copyGroupsCombo.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this.currentGroupCombo.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        private void ReadAllData()
        {
            this._configurationNew.Value = this._configurationUnionNew.Get();
        }


        #endregion


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr5V70ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Properties.Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

       
    }
}