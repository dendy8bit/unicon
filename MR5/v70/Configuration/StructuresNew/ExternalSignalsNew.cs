﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v70.Configuration.StructuresNew
{
    public class ExternalSignalsNew : StructBase
    {

        [Layout(0)] private ushort _keyOff;
        [Layout(1)] private ushort _keyOn;
        [Layout(2)] private ushort _extOff;
        [Layout(3)] private ushort _extOn;
        [Layout(4)] private ushort _inpClear;
        [Layout(5)] private ushort _inpGroup1;
        [Layout(6)] private ushort _rez1;
        [Layout(7, Ignore = true)] private ushort _portInterface;
        [Layout(8)] private ushort _blockSDTU;
        [Layout(9)] private ushort _inpGroup2;
        [Layout(10)] private ushort _inpGroup3;
        [Layout(11)] private ushort _inpGroup4;
        [Layout(12)] private ushort _rez;

        [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOff
        {
            get { return Validator.Get(this._keyOff, StringsConfig.LogicSignals); }
            set { this._keyOff = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ключ_ВКЛ")]
        public string KeyOn
        {
            get { return Validator.Get(this._keyOn, StringsConfig.LogicSignals); }
            set { this._keyOn = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string ExtOff
        {
            get { return Validator.Get(this._extOff, StringsConfig.LogicSignals); }
            set { this._extOff = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_внеш_включить")]
        public string ExtOn
        {
            get { return Validator.Get(this._extOn, StringsConfig.LogicSignals); }
            set { this._extOn = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "сброс_сигнализации")]
        public string InpClear
        {
            get { return Validator.Get(this._inpClear, StringsConfig.LogicSignals); }
            set { this._inpClear = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "группа_уставок_1")]
        public string InpGroup
        {
            get { return Validator.Get(this._inpGroup1, StringsConfig.LogicSignals); }
            set { this._inpGroup1 = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        
        [BindingProperty(6)]
        [XmlElement(ElementName = "Запрет команд по СДТУ")]
        public string Disable
        {
            get { return Validator.Get(this._blockSDTU, StringsConfig.LogicSignals); }
            set { this._blockSDTU = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        
        [BindingProperty(7)]
        [XmlElement(ElementName = "группа_уставок_2")]
        public string InpGroup2
        {
            get { return Validator.Get(this._inpGroup2, StringsConfig.LogicSignals); }
            set { this._inpGroup2 = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "группа_уставок_3")]
        public string InpGroup3
        {
            get { return Validator.Get(this._inpGroup3, StringsConfig.LogicSignals); }
            set { this._inpGroup3 = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "группа_уставок_4")]
        public string InpGroup4
        {
            get { return Validator.Get(this._inpGroup4, StringsConfig.LogicSignals); }
            set { this._inpGroup4 = Validator.Set(value, StringsConfig.LogicSignals); }
        }
    }
}
