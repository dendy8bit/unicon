﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.MR5.v70.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v70.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v70.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR5.v70.Configuration.StructuresNew
{
    // то, как располагаются структуры в устройстве
    public class AllDefensesSetpoint : StructBase, ISetpointContainer<GroupSetpoint> // GroupSetpoint - структура уставок для одной группы
    {
        // Группы 1 и 2
        [Layout(0)] private CornerStruct _corner1;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses1;
        [Layout(2)] private AllCurrentDefensesOtherStruct _currentOtherDefenses1;
        [Layout(3)] private CornerStruct _corner2;
        [Layout(4)] private AllCurrentDefensesStruct _currentDefenses2;
        [Layout(5)] private AllCurrentDefensesOtherStruct _currentOtherDefenses2;
        [Layout(6)] public AllAddCurrentDefensesStruct _addCurrentDefenses1;
        [Layout(7, Count = 16)] private ushort[] _res1;
        [Layout(8)] public AllAddCurrentDefensesStruct _addCurrentDefenses2;
        [Layout(9, Count = 16)] private ushort[] _res2;
        [Layout(10)] private AllFrequencyDefensesStruct _frequencyDefenses1;
        [Layout(11)] private AllFrequencyDefensesStruct _frequencyDefenses2;
        [Layout(12)] private AllVoltageDefensesStruct _voltageDefenses1;
        [Layout(13)] private AllVoltageDefensesStruct _voltageDefenses2;
        // Группы 3 и 4        
        [Layout(14)] private CornerStruct _corner3;
        [Layout(15)] private AllCurrentDefensesStruct _currentDefenses3;
        [Layout(16)] private AllCurrentDefensesOtherStruct _currentOtherDefenses3;
        [Layout(17)] private CornerStruct _corner4;
        [Layout(18)] private AllCurrentDefensesStruct _currentDefenses4;
        [Layout(19)] private AllCurrentDefensesOtherStruct _currentOtherDefenses4;
        [Layout(20)] public AllAddCurrentDefensesStruct _addCurrentDefenses3;
        [Layout(21, Count = 16)] private ushort[] _res3;
        [Layout(22)] public AllAddCurrentDefensesStruct _addCurrentDefenses4;
        [Layout(23, Count = 16)] private ushort[] _res4;
        [Layout(24)] private AllFrequencyDefensesStruct _frequencyDefenses3;
        [Layout(25)] private AllFrequencyDefensesStruct _frequencyDefenses4;
        [Layout(26)] private AllVoltageDefensesStruct _voltageDefenses3;
        [Layout(27)] private AllVoltageDefensesStruct _voltageDefenses4;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint[] Setpoints
        {
            get
            {
                return new []
                {
                    this.GetGroupsetpoint(this._corner1, this._currentDefenses1, this._currentOtherDefenses1, this._addCurrentDefenses1, this._frequencyDefenses1, this._voltageDefenses1),
                    this.GetGroupsetpoint(this._corner2, this._currentDefenses2, this._currentOtherDefenses2, this._addCurrentDefenses2, this._frequencyDefenses2, this._voltageDefenses2),
                    this.GetGroupsetpoint(this._corner3, this._currentDefenses3, this._currentOtherDefenses3, this._addCurrentDefenses3, this._frequencyDefenses3, this._voltageDefenses3),
                    this.GetGroupsetpoint(this._corner4, this._currentDefenses4, this._currentOtherDefenses4, this._addCurrentDefenses4, this._frequencyDefenses4, this._voltageDefenses4)
                };
            }
            set
            {
                this._corner1 = value[0].Corners;
                this._currentDefenses1 = value[0].CurrentDefenses;
                this._currentOtherDefenses1 = value[0].CurrentDefensesOther;
                this._addCurrentDefenses1 = value[0].AddCurrentDefenses;
                this._frequencyDefenses1 = value[0].FrequencyDefenses;
                this._voltageDefenses1 = value[0].VoltageDefenses;

                this._corner2 = value[1].Corners;
                this._currentDefenses2 = value[1].CurrentDefenses;
                this._currentOtherDefenses2 = value[1].CurrentDefensesOther;
                this._addCurrentDefenses2 = value[1].AddCurrentDefenses;
                this._frequencyDefenses2 = value[1].FrequencyDefenses;
                this._voltageDefenses2 = value[1].VoltageDefenses;

                this._corner3 = value[2].Corners;
                this._currentDefenses3 = value[2].CurrentDefenses;
                this._currentOtherDefenses3 = value[2].CurrentDefensesOther;
                this._addCurrentDefenses3 = value[2].AddCurrentDefenses;
                this._frequencyDefenses3 = value[2].FrequencyDefenses;
                this._voltageDefenses3 = value[2].VoltageDefenses;

                this._corner4 = value[3].Corners;
                this._currentDefenses4 = value[3].CurrentDefenses;
                this._currentOtherDefenses4 = value[3].CurrentDefensesOther;
                this._addCurrentDefenses4 = value[3].AddCurrentDefenses;
                this._frequencyDefenses4 = value[3].FrequencyDefenses;
                this._voltageDefenses4 = value[3].VoltageDefenses;
            }
        }

        private GroupSetpoint GetGroupsetpoint(CornerStruct corners, AllCurrentDefensesStruct currentDefenses, AllCurrentDefensesOtherStruct currentDefensesOther, AllAddCurrentDefensesStruct addCurrentDefense, 
            AllFrequencyDefensesStruct frequencyDefenses, AllVoltageDefensesStruct voltageDefenses)
        {
            return new GroupSetpoint
            {
                Corners = corners.Clone<CornerStruct>(),
                CurrentDefenses = currentDefenses.Clone<AllCurrentDefensesStruct>(),
                CurrentDefensesOther = currentDefensesOther.Clone<AllCurrentDefensesOtherStruct>(),
                AddCurrentDefenses = addCurrentDefense.Clone<AllAddCurrentDefensesStruct>(),
                FrequencyDefenses = frequencyDefenses.Clone<AllFrequencyDefensesStruct>(),
                VoltageDefenses = voltageDefenses.Clone<AllVoltageDefensesStruct>()
            };
        }
    }
}
