namespace BEMN.MR5.v70.Configuration
{
    partial class Mr5V70ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param pinName="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle193 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle194 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle195 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle196 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle197 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle198 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle199 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle200 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle201 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle202 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle203 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle204 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle205 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle206 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle207 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle208 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle209 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle211 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle212 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle213 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle210 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle214 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle219 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle220 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle221 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle215 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle216 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle217 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle218 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle222 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle227 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle228 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle229 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle223 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle224 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle225 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle226 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle230 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle234 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle235 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle236 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle231 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle232 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle233 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle237 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle242 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle243 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle244 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle238 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle239 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle240 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle241 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle245 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle250 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle251 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle252 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle246 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle247 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle248 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle249 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle253 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle257 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle258 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle259 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle254 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle255 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle256 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle260 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle264 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle265 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle266 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle261 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle262 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle263 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle267 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle271 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle272 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle273 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle268 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle269 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle270 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle274 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle278 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle279 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle280 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle275 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle276 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle277 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle281 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle287 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle288 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle282 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle283 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle284 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle285 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle286 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mainPanel = new System.Windows.Forms.Panel();
            this._resetConfigBut = new System.Windows.Forms.Button();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.oscLengthMode = new System.Windows.Forms.ComboBox();
            this.oscLenText = new System.Windows.Forms.MaskedTextBox();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.oscFix = new System.Windows.Forms.ComboBox();
            this.gb3 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.oscPercent = new System.Windows.Forms.MaskedTextBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this._keysCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._ompCheckBox = new System.Windows.Forms.CheckBox();
            this._HUD_Box = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._constraintGroup4Combo = new System.Windows.Forms.ComboBox();
            this.group4label = new System.Windows.Forms.Label();
            this._constraintGroup3Combo = new System.Windows.Forms.ComboBox();
            this.group3label = new System.Windows.Forms.Label();
            this._constraintGroup2Combo = new System.Windows.Forms.ComboBox();
            this.group2label = new System.Windows.Forms.Label();
            this._constraintGroup1Combo = new System.Windows.Forms.ComboBox();
            this.group1label = new System.Windows.Forms.Label();
            this._blockSDTU = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._TN_typeCombo = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this._TNNP_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this._TN_dispepairCombo = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._TN_Box = new System.Windows.Forms.MaskedTextBox();
            this._TNNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._TT_typeCombo = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._disrepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefOSCcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefResetcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.avr_time_abrasion = new System.Windows.Forms.MaskedTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.avr_return = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.avr_abrasion = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.avr_permit_reset_switch = new System.Windows.Forms.CheckBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.avr_abrasion_switch = new System.Windows.Forms.CheckBox();
            this.avr_supply_off = new System.Windows.Forms.CheckBox();
            this.avr_self_off = new System.Windows.Forms.CheckBox();
            this.avr_switch_off = new System.Windows.Forms.CheckBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.avr_start = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._apvStartCheckBox = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.NewCopyGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.copyBtn = new System.Windows.Forms.Button();
            this.copyGroupsCombo = new System.Windows.Forms.ComboBox();
            this.currentGroupCombo = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this._tokDefendTabPage = new System.Windows.Forms.TabPage();
            this._tokDefenseGrid5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseGrid3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseGrid4 = new System.Windows.Forms.DataGridView();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._tokDefenseInbox = new System.Windows.Forms.MaskedTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._tokDefenseI2box = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this._tokDefenseI0box = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._tokDefenseIbox = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._tokDefenseGrid2 = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UpuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3PuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3DirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3ParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseGrid1 = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseU_PuskCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefensePuskConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseDirectionCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockExistCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseOSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefendTabPage = new System.Windows.Forms.TabPage();
            this._voltageDefensesGrid11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid2 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesParameterCol2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSC�11Col2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetCol2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid1 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesParameterCol1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesWorkTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVReturlCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetCol1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._u5vColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesGrid3 = new System.Windows.Forms.DataGridView();
            this._voltageDefensesNameCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesModeCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesBlockingNumberCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesParameterCol3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesWorkConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesTimeConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVreturnCol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesReturnConstraintCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesReturnTimeCol3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._voltageDefensesUROVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAPVcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesAVRcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._voltageDefensesOSCv11Col3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._voltageDefensesResetcol3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequencyDefendTabPage = new System.Windows.Forms.TabPage();
            this._frequenceDefensesGrid = new System.Windows.Forms.DataGridView();
            this._frequenceDefensesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesMode = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesBlockingNumber = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesWorkConstraint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesWorkTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPVReturn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesConstraintAPV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesReturnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._frequenceDefensesUROV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAPV = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesAVR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._frequenceDefensesOSCv11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._frequenceDefensesReset = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.OldCopyGroupBox = new System.Windows.Forms.GroupBox();
            this._reserveRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._mainRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._ChangeGroupButton2 = new System.Windows.Forms.Button();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this.mainPanel.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._inSignalsPage.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.gb1.SuspendLayout();
            this.gb2.SuspendLayout();
            this.gb3.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.groupBox27.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.NewCopyGroupBox.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this._tokDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).BeginInit();
            this._voltageDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).BeginInit();
            this._frequencyDefendTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).BeginInit();
            this.OldCopyGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.AutoSize = true;
            this.mainPanel.Controls.Add(this._resetConfigBut);
            this.mainPanel.Controls.Add(this._saveToXmlButton);
            this.mainPanel.Controls.Add(this._saveConfigBut);
            this.mainPanel.Controls.Add(this._loadConfigBut);
            this.mainPanel.Controls.Add(this._writeConfigBut);
            this.mainPanel.Controls.Add(this._statusStrip);
            this.mainPanel.Controls.Add(this._readConfigBut);
            this.mainPanel.Controls.Add(this._tabControl);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1021, 750);
            this.mainPanel.TabIndex = 0;
            // 
            // _resetConfigBut
            // 
            this._resetConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetConfigBut.Location = new System.Drawing.Point(312, 702);
            this._resetConfigBut.Name = "_resetConfigBut";
            this._resetConfigBut.Size = new System.Drawing.Size(149, 23);
            this._resetConfigBut.TabIndex = 39;
            this._resetConfigBut.Text = "��������� ���. �������";
            this._resetConfigBut.UseVisualStyleBackColor = true;
            this._resetConfigBut.Click += new System.EventHandler(this._resetConfigBut_Click);
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(898, 702);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(120, 23);
            this._saveToXmlButton.TabIndex = 40;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveConfigBut.Location = new System.Drawing.Point(772, 702);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(120, 23);
            this._saveConfigBut.TabIndex = 37;
            this._saveConfigBut.Text = "��������� � ����";
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._loadConfigBut.Location = new System.Drawing.Point(646, 702);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(120, 23);
            this._loadConfigBut.TabIndex = 36;
            this._loadConfigBut.Text = "��������� �� �����";
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(157, 702);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 35;
            this._writeConfigBut.Text = "�������� � ����������";
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._statusLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 728);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(1021, 22);
            this._statusStrip.TabIndex = 38;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 80;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(3, 702);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(149, 23);
            this._readConfigBut.TabIndex = 34;
            this._readConfigBut.Text = "��������� �� ����������";
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _tabControl
            // 
            this._tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this.tabPage5);
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(1021, 696);
            this._tabControl.TabIndex = 33;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 136);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(212, 22);
            this.clearSetpointsItem.Text = "��������� ���. �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox17);
            this._inSignalsPage.Controls.Add(this.groupBox29);
            this._inSignalsPage.Controls.Add(this.groupBox24);
            this._inSignalsPage.Controls.Add(this.groupBox27);
            this._inSignalsPage.Controls.Add(this.groupBox18);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox2);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(1013, 670);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.gb1);
            this.groupBox17.Controls.Add(this.gb2);
            this.groupBox17.Controls.Add(this.gb3);
            this.groupBox17.Location = new System.Drawing.Point(217, 258);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(203, 180);
            this.groupBox17.TabIndex = 36;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "������������ ������������";
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.oscLengthMode);
            this.gb1.Controls.Add(this.oscLenText);
            this.gb1.Location = new System.Drawing.Point(6, 19);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(181, 46);
            this.gb1.TabIndex = 21;
            this.gb1.TabStop = false;
            this.gb1.Text = "����. ������� ���.";
            // 
            // oscLengthMode
            // 
            this.oscLengthMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscLengthMode.FormattingEnabled = true;
            this.oscLengthMode.Location = new System.Drawing.Point(6, 19);
            this.oscLengthMode.Name = "oscLengthMode";
            this.oscLengthMode.Size = new System.Drawing.Size(50, 21);
            this.oscLengthMode.TabIndex = 0;
            this.oscLengthMode.SelectedIndexChanged += new System.EventHandler(this.oscLength_SelectedIndexChanged);
            // 
            // oscLenText
            // 
            this.oscLenText.Location = new System.Drawing.Point(62, 19);
            this.oscLenText.Name = "oscLenText";
            this.oscLenText.ReadOnly = true;
            this.oscLenText.Size = new System.Drawing.Size(106, 20);
            this.oscLenText.TabIndex = 0;
            this.oscLenText.Tag = "1,99";
            this.oscLenText.Text = "0";
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.oscFix);
            this.gb2.Location = new System.Drawing.Point(6, 71);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(181, 46);
            this.gb2.TabIndex = 22;
            this.gb2.TabStop = false;
            this.gb2.Text = "�������� ���.";
            // 
            // oscFix
            // 
            this.oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscFix.FormattingEnabled = true;
            this.oscFix.Location = new System.Drawing.Point(6, 19);
            this.oscFix.Name = "oscFix";
            this.oscFix.Size = new System.Drawing.Size(147, 21);
            this.oscFix.TabIndex = 0;
            // 
            // gb3
            // 
            this.gb3.Controls.Add(this.label28);
            this.gb3.Controls.Add(this.oscPercent);
            this.gb3.Location = new System.Drawing.Point(6, 123);
            this.gb3.Name = "gb3";
            this.gb3.Size = new System.Drawing.Size(181, 46);
            this.gb3.TabIndex = 23;
            this.gb3.TabStop = false;
            this.gb3.Text = "����. ���������� ���.";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(123, 23);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "%";
            // 
            // oscPercent
            // 
            this.oscPercent.Location = new System.Drawing.Point(6, 19);
            this.oscPercent.Name = "oscPercent";
            this.oscPercent.Size = new System.Drawing.Size(110, 20);
            this.oscPercent.TabIndex = 0;
            this.oscPercent.Tag = "1,99";
            this.oscPercent.Text = "1";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this._keysCheckedListBox);
            this.groupBox29.Location = new System.Drawing.Point(824, 8);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(144, 276);
            this.groupBox29.TabIndex = 35;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "�����";
            // 
            // _keysCheckedListBox
            // 
            this._keysCheckedListBox.CheckOnClick = true;
            this._keysCheckedListBox.FormattingEnabled = true;
            this._keysCheckedListBox.Location = new System.Drawing.Point(6, 19);
            this._keysCheckedListBox.Name = "_keysCheckedListBox";
            this._keysCheckedListBox.ScrollAlwaysVisible = true;
            this._keysCheckedListBox.Size = new System.Drawing.Size(130, 244);
            this._keysCheckedListBox.TabIndex = 7;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.tabControl2);
            this.groupBox24.Location = new System.Drawing.Point(625, 6);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(193, 400);
            this.groupBox24.TabIndex = 33;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "���������� ������� ���";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 376);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 347);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "��5";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle193.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle193;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 344);
            this._inputSignals9.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "�";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "��������";
            this._signalValueColILI.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 25);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 347);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "��6";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle194.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle194;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 344);
            this._inputSignals10.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "�";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "��������";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 25);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 347);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "��7";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle195.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle195;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 344);
            this._inputSignals11.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "�";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "��������";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 25);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 347);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "��8";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle196.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle196;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 344);
            this._inputSignals12.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "�";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "��������";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.tabControl1);
            this.groupBox27.Location = new System.Drawing.Point(426, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(193, 400);
            this.groupBox27.TabIndex = 32;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "���������� ������� �";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 376);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 347);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "��1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle197.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle197;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 344);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "�";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "��������";
            this._signalValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 347);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "��2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle198.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle198;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 344);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "�";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "��������";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 347);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "��3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle199.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle199;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 344);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "�";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "��������";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 347);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "��4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle200.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle200;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 344);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "�";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "��������";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._ompCheckBox);
            this.groupBox18.Controls.Add(this._HUD_Box);
            this.groupBox18.Controls.Add(this.label26);
            this.groupBox18.Controls.Add(this.label27);
            this.groupBox18.Location = new System.Drawing.Point(217, 183);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(203, 69);
            this.groupBox18.TabIndex = 17;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "����������� ����� �����������";
            // 
            // _ompCheckBox
            // 
            this._ompCheckBox.AutoSize = true;
            this._ompCheckBox.Location = new System.Drawing.Point(116, 21);
            this._ompCheckBox.Name = "_ompCheckBox";
            this._ompCheckBox.Size = new System.Drawing.Size(15, 14);
            this._ompCheckBox.TabIndex = 21;
            this._ompCheckBox.UseVisualStyleBackColor = true;
            // 
            // _HUD_Box
            // 
            this._HUD_Box.Location = new System.Drawing.Point(116, 42);
            this._HUD_Box.Name = "_HUD_Box";
            this._HUD_Box.Size = new System.Drawing.Size(81, 20);
            this._HUD_Box.TabIndex = 20;
            this._HUD_Box.Tag = "0,1";
            this._HUD_Box.Text = "0";
            this._HUD_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 45);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(70, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = "��� (��/��)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "���";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(8, 449);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 101);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(71, 75);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(71, 56);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(71, 37);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(71, 17);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(217, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 171);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������, ��";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(0, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, I�";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(125, 147);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(71, 20);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "0,3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(125, 107);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "0,40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(0, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(0, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(125, 87);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "0,3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(125, 127);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "0,3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switcherBlockCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switcherBlockCombo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(125, 68);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switcherErrorCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(125, 49);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switcherStateOnCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(125, 30);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(0, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switcherStateOffCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(125, 11);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._constraintGroup4Combo);
            this.groupBox3.Controls.Add(this.group4label);
            this.groupBox3.Controls.Add(this._constraintGroup3Combo);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.group3label);
            this.groupBox3.Controls.Add(this._constraintGroup2Combo);
            this.groupBox3.Controls.Add(this.group2label);
            this.groupBox3.Controls.Add(this._constraintGroup1Combo);
            this.groupBox3.Controls.Add(this.group1label);
            this.groupBox3.Controls.Add(this._blockSDTU);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 227);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 216);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _constraintGroup4Combo
            // 
            this._constraintGroup4Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroup4Combo.FormattingEnabled = true;
            this._constraintGroup4Combo.Location = new System.Drawing.Point(124, 188);
            this._constraintGroup4Combo.Name = "_constraintGroup4Combo";
            this._constraintGroup4Combo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroup4Combo.TabIndex = 29;
            // 
            // group4label
            // 
            this.group4label.AutoSize = true;
            this.group4label.Location = new System.Drawing.Point(5, 190);
            this.group4label.Name = "group4label";
            this.group4label.Size = new System.Drawing.Size(111, 13);
            this.group4label.TabIndex = 28;
            this.group4label.Text = "������. �� ��. ���. 4";
            // 
            // _constraintGroup3Combo
            // 
            this._constraintGroup3Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroup3Combo.FormattingEnabled = true;
            this._constraintGroup3Combo.Location = new System.Drawing.Point(124, 168);
            this._constraintGroup3Combo.Name = "_constraintGroup3Combo";
            this._constraintGroup3Combo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroup3Combo.TabIndex = 27;
            // 
            // group3label
            // 
            this.group3label.AutoSize = true;
            this.group3label.Location = new System.Drawing.Point(5, 170);
            this.group3label.Name = "group3label";
            this.group3label.Size = new System.Drawing.Size(111, 13);
            this.group3label.TabIndex = 26;
            this.group3label.Text = "������. �� ��. ���. 3";
            // 
            // _constraintGroup2Combo
            // 
            this._constraintGroup2Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroup2Combo.FormattingEnabled = true;
            this._constraintGroup2Combo.Location = new System.Drawing.Point(124, 148);
            this._constraintGroup2Combo.Name = "_constraintGroup2Combo";
            this._constraintGroup2Combo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroup2Combo.TabIndex = 25;
            // 
            // group2label
            // 
            this.group2label.AutoSize = true;
            this.group2label.Location = new System.Drawing.Point(5, 150);
            this.group2label.Name = "group2label";
            this.group2label.Size = new System.Drawing.Size(111, 13);
            this.group2label.TabIndex = 24;
            this.group2label.Text = "������. �� ��. ���. 2";
            // 
            // _constraintGroup1Combo
            // 
            this._constraintGroup1Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroup1Combo.FormattingEnabled = true;
            this._constraintGroup1Combo.Location = new System.Drawing.Point(124, 128);
            this._constraintGroup1Combo.Name = "_constraintGroup1Combo";
            this._constraintGroup1Combo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroup1Combo.TabIndex = 23;
            // 
            // group1label
            // 
            this.group1label.AutoSize = true;
            this.group1label.Location = new System.Drawing.Point(5, 130);
            this.group1label.Name = "group1label";
            this.group1label.Size = new System.Drawing.Size(111, 13);
            this.group1label.TabIndex = 22;
            this.group1label.Text = "������. �� ��. ���. 1";
            // 
            // _blockSDTU
            // 
            this._blockSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockSDTU.FormattingEnabled = true;
            this._blockSDTU.Location = new System.Drawing.Point(124, 109);
            this._blockSDTU.Name = "_blockSDTU";
            this._blockSDTU.Size = new System.Drawing.Size(71, 21);
            this._blockSDTU.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 111);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(102, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "���������� ����";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(124, 89);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(71, 21);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ������������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(124, 50);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(124, 69);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 72);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(124, 12);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(124, 31);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._TN_typeCombo);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this._TNNP_dispepairCombo);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this._TN_dispepairCombo);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._TN_Box);
            this.groupBox2.Controls.Add(this._TNNP_Box);
            this.groupBox2.Location = new System.Drawing.Point(8, 104);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 122);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��� ��";
            // 
            // _TN_typeCombo
            // 
            this._TN_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_typeCombo.FormattingEnabled = true;
            this._TN_typeCombo.Location = new System.Drawing.Point(56, 16);
            this._TN_typeCombo.Name = "_TN_typeCombo";
            this._TN_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TN_typeCombo.TabIndex = 12;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 19);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(44, 13);
            this.label42.TabIndex = 11;
            this.label42.Text = "��� ��";
            // 
            // _TNNP_dispepairCombo
            // 
            this._TNNP_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TNNP_dispepairCombo.FormattingEnabled = true;
            this._TNNP_dispepairCombo.Location = new System.Drawing.Point(93, 98);
            this._TNNP_dispepairCombo.Name = "_TNNP_dispepairCombo";
            this._TNNP_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TNNP_dispepairCombo.TabIndex = 10;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(5, 102);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "������. ����";
            // 
            // _TN_dispepairCombo
            // 
            this._TN_dispepairCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TN_dispepairCombo.FormattingEnabled = true;
            this._TN_dispepairCombo.Location = new System.Drawing.Point(94, 57);
            this._TN_dispepairCombo.Name = "_TN_dispepairCombo";
            this._TN_dispepairCombo.Size = new System.Drawing.Size(101, 21);
            this._TN_dispepairCombo.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 61);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "������. ��";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "����������� ����";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "����������� ��";
            // 
            // _TN_Box
            // 
            this._TN_Box.Location = new System.Drawing.Point(134, 37);
            this._TN_Box.Name = "_TN_Box";
            this._TN_Box.Size = new System.Drawing.Size(60, 20);
            this._TN_Box.TabIndex = 2;
            this._TN_Box.Tag = "0,128000";
            this._TN_Box.Text = "0";
            this._TN_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TNNP_Box
            // 
            this._TNNP_Box.Location = new System.Drawing.Point(134, 78);
            this._TNNP_Box.Name = "_TNNP_Box";
            this._TNNP_Box.Size = new System.Drawing.Size(60, 20);
            this._TNNP_Box.TabIndex = 5;
            this._TNNP_Box.Tag = "0,128000";
            this._TNNP_Box.Text = "0";
            this._TNNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._TT_typeCombo);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 95);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // _TT_typeCombo
            // 
            this._TT_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeCombo.FormattingEnabled = true;
            this._TT_typeCombo.Location = new System.Drawing.Point(58, 71);
            this._TT_typeCombo.Name = "_TT_typeCombo";
            this._TT_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TT_typeCombo.TabIndex = 14;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "��� �T";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(145, 52);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(50, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "0,40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, A";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(145, 32);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(50, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "0,1000";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(145, 12);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(50, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "0,1500";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox16);
            this._outputSignalsPage.Controls.Add(this.groupBox6);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(1013, 670);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.VLSTabControl);
            this.groupBox16.Location = new System.Drawing.Point(436, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(196, 490);
            this.groupBox16.TabIndex = 33;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "�������� ���������� �������";
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Location = new System.Drawing.Point(6, 19);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(186, 467);
            this.VLSTabControl.TabIndex = 31;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(178, 414);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "��� 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(178, 414);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "��� 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox2.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(178, 414);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "��� 3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox3.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(178, 414);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "��� 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox4.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(178, 414);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "��� 5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(178, 414);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "��� 6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox6.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(178, 414);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "��� 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox7.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(178, 414);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "��� 8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(4, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(174, 394);
            this.VLScheckedListBox8.TabIndex = 6;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox22);
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Location = new System.Drawing.Point(638, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 204);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label11);
            this.groupBox22.Controls.Add(this._releDispepairBox);
            this.groupBox22.Location = new System.Drawing.Point(6, 12);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(190, 38);
            this.groupBox22.TabIndex = 8;
            this.groupBox22.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "T�����., ��";
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(110, 11);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(74, 20);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "0,3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._disrepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(6, 50);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(190, 148);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������ �������������";
            // 
            // _disrepairCheckList
            // 
            this._disrepairCheckList.CheckOnClick = true;
            this._disrepairCheckList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._disrepairCheckList.FormattingEnabled = true;
            this._disrepairCheckList.Items.AddRange(new object[] {
            "������������� 1",
            "������������� 2",
            "������������� 3",
            "������������� 4",
            "������������� 5",
            "������������� 6",
            "������������� 7",
            "������������� 8"});
            this._disrepairCheckList.Location = new System.Drawing.Point(3, 16);
            this._disrepairCheckList.Name = "_disrepairCheckList";
            this._disrepairCheckList.Size = new System.Drawing.Size(184, 129);
            this._disrepairCheckList.TabIndex = 5;
            this._disrepairCheckList.SelectedValueChanged += new System.EventHandler(this._disrepairCheckList_SelectedValueChanged);
            this._disrepairCheckList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this._disrepairCheckList_MouseDoubleClick);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(6, 246);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 250);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle201.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle201.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle201.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle201.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle201.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle201.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle201.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle201;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            dataGridViewCellStyle202.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle202.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle202.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle202.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle202.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle202.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle202.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputIndicatorsGrid.DefaultCellStyle = dataGridViewCellStyle202;
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            dataGridViewCellStyle203.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle203.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle203.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle203.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle203.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle203.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle203.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle203;
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle204.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle204;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(418, 231);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(424, 234);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle205.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle205.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle205.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle205.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle205.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle205.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle205.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle205;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            dataGridViewCellStyle206.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle206.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle206.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle206.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle206.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle206.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle206.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputReleGrid.DefaultCellStyle = dataGridViewCellStyle206;
            this._outputReleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputReleGrid.Location = new System.Drawing.Point(3, 16);
            this._outputReleGrid.Name = "_outputReleGrid";
            dataGridViewCellStyle207.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle207.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle207.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle207.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle207.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle207.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle207.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle207;
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle208.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle208;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(418, 215);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "T�����., ��";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(1013, 670);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._externalDefenseGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._externalDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle209.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle209.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle209.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle209.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle209.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle209.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle209.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle209;
            this._externalDefenseGrid.ColumnHeadersHeight = 30;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefUROVcol,
            this._exDefAPVcol,
            this._exDefAVRcol,
            this._exDefOSCcol,
            this._exDefResetcol});
            dataGridViewCellStyle211.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle211.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle211.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle211.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle211.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle211.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle211.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDefenseGrid.DefaultCellStyle = dataGridViewCellStyle211;
            this._externalDefenseGrid.Location = new System.Drawing.Point(3, 3);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            dataGridViewCellStyle212.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle212.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle212.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle212.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle212.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle212.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle212.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle212;
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle213.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle213;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(1007, 230);
            this._externalDefenseGrid.TabIndex = 0;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.Frozen = true;
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 43;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 48;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 74;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle210.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle210;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 87;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "����� ������., ��";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 107;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 41;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 52;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 87;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "����� ��������, ��";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 116;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 34;
            // 
            // _exDefOSCcol
            // 
            this._exDefOSCcol.HeaderText = "���.";
            this._exDefOSCcol.Name = "_exDefOSCcol";
            this._exDefOSCcol.Width = 36;
            // 
            // _exDefResetcol
            // 
            this._exDefResetcol.HeaderText = "�����";
            this._exDefResetcol.Name = "_exDefResetcol";
            this._exDefResetcol.Width = 44;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox11);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(1013, 670);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.avr_disconnection);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.avr_time_return);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.avr_time_abrasion);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.avr_return);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.avr_abrasion);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.avr_reset_blocking);
            this.groupBox11.Controls.Add(this.avr_permit_reset_switch);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.avr_blocking);
            this.groupBox11.Controls.Add(this.avr_start);
            this.groupBox11.Location = new System.Drawing.Point(265, 1);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(256, 326);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������������ ���";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(9, 303);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(123, 13);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������, ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(9, 158);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(131, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "������ ��� (�� �������)";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(152, 300);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(85, 20);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "0,3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(9, 283);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(110, 13);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������, ��";
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(152, 280);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(85, 20);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "0,3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(9, 263);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(49, 13);
            this.label62.TabIndex = 40;
            this.label62.Text = "�������";
            // 
            // avr_time_abrasion
            // 
            this.avr_time_abrasion.Location = new System.Drawing.Point(152, 239);
            this.avr_time_abrasion.Name = "avr_time_abrasion";
            this.avr_time_abrasion.Size = new System.Drawing.Size(85, 20);
            this.avr_time_abrasion.TabIndex = 29;
            this.avr_time_abrasion.Tag = "0,3000000";
            this.avr_time_abrasion.Text = "0";
            this.avr_time_abrasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(9, 242);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(136, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "����� ������������, ��";
            // 
            // avr_return
            // 
            this.avr_return.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_return.FormattingEnabled = true;
            this.avr_return.Location = new System.Drawing.Point(152, 259);
            this.avr_return.Name = "avr_return";
            this.avr_return.Size = new System.Drawing.Size(87, 21);
            this.avr_return.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(9, 222);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 38;
            this.label64.Text = "������������";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 201);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // avr_abrasion
            // 
            this.avr_abrasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_abrasion.FormattingEnabled = true;
            this.avr_abrasion.Location = new System.Drawing.Point(152, 218);
            this.avr_abrasion.Name = "avr_abrasion";
            this.avr_abrasion.Size = new System.Drawing.Size(87, 21);
            this.avr_abrasion.TabIndex = 26;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 180);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(152, 197);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // avr_permit_reset_switch
            // 
            this.avr_permit_reset_switch.Location = new System.Drawing.Point(12, 117);
            this.avr_permit_reset_switch.Name = "avr_permit_reset_switch";
            this.avr_permit_reset_switch.Size = new System.Drawing.Size(227, 34);
            this.avr_permit_reset_switch.TabIndex = 22;
            this.avr_permit_reset_switch.Text = "���������� ������ �� ���. \r\n�����������";
            this.avr_permit_reset_switch.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.avr_abrasion_switch);
            this.groupBox12.Controls.Add(this.avr_supply_off);
            this.groupBox12.Controls.Add(this.avr_self_off);
            this.groupBox12.Controls.Add(this.avr_switch_off);
            this.groupBox12.Location = new System.Drawing.Point(6, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(233, 100);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "���������� �������";
            // 
            // avr_abrasion_switch
            // 
            this.avr_abrasion_switch.Location = new System.Drawing.Point(6, 72);
            this.avr_abrasion_switch.Name = "avr_abrasion_switch";
            this.avr_abrasion_switch.Size = new System.Drawing.Size(186, 24);
            this.avr_abrasion_switch.TabIndex = 21;
            this.avr_abrasion_switch.Text = "�� ������";
            this.avr_abrasion_switch.UseVisualStyleBackColor = true;
            // 
            // avr_supply_off
            // 
            this.avr_supply_off.Location = new System.Drawing.Point(6, 15);
            this.avr_supply_off.Name = "avr_supply_off";
            this.avr_supply_off.Size = new System.Drawing.Size(139, 24);
            this.avr_supply_off.TabIndex = 18;
            this.avr_supply_off.Text = "�� �������";
            this.avr_supply_off.UseVisualStyleBackColor = true;
            // 
            // avr_self_off
            // 
            this.avr_self_off.Location = new System.Drawing.Point(6, 53);
            this.avr_self_off.Name = "avr_self_off";
            this.avr_self_off.Size = new System.Drawing.Size(186, 24);
            this.avr_self_off.TabIndex = 20;
            this.avr_self_off.Text = "��������������";
            this.avr_self_off.UseVisualStyleBackColor = true;
            // 
            // avr_switch_off
            // 
            this.avr_switch_off.Location = new System.Drawing.Point(6, 34);
            this.avr_switch_off.Name = "avr_switch_off";
            this.avr_switch_off.Size = new System.Drawing.Size(221, 24);
            this.avr_switch_off.TabIndex = 19;
            this.avr_switch_off.Text = "�� ����������";
            this.avr_switch_off.UseVisualStyleBackColor = true;
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(152, 176);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_blocking.TabIndex = 3;
            // 
            // avr_start
            // 
            this.avr_start.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_start.FormattingEnabled = true;
            this.avr_start.Location = new System.Drawing.Point(152, 155);
            this.avr_start.Name = "avr_start";
            this.avr_start.Size = new System.Drawing.Size(87, 21);
            this.avr_start.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.comboBox1);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(3, 228);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(256, 99);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(11, 48);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(87, 21);
            this.comboBox1.TabIndex = 29;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(157, 31);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(65, 13);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������, I�";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(145, 48);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(85, 20);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "0,40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(16, 30);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(80, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._apvStartCheckBox);
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(3, 1);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(256, 221);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // _apvStartCheckBox
            // 
            this._apvStartCheckBox.AutoSize = true;
            this._apvStartCheckBox.Location = new System.Drawing.Point(146, 185);
            this._apvStartCheckBox.Name = "_apvStartCheckBox";
            this._apvStartCheckBox.Size = new System.Drawing.Size(15, 14);
            this._apvStartCheckBox.TabIndex = 35;
            this._apvStartCheckBox.UseVisualStyleBackColor = true;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 186);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(132, 13);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� �������.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 26);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 166);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(101, 13);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����, ��";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(146, 161);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "0,3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 146);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����, ��";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(146, 141);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "0,3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(8, 126);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����, ��";
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(146, 121);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "0,3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 106);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����, ��";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(146, 101);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(85, 20);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "0,3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 86);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(120, 13);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������, ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 66);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(123, 13);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������, ��";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(146, 81);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(85, 20);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "0,3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 46);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(146, 61);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(85, 20);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "0,3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(146, 40);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(87, 21);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(146, 19);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(87, 21);
            this.apv_conf.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.NewCopyGroupBox);
            this.tabPage5.Controls.Add(this.groupBox10);
            this.tabPage5.Controls.Add(this.OldCopyGroupBox);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1013, 670);
            this.tabPage5.TabIndex = 8;
            this.tabPage5.Text = "������";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // NewCopyGroupBox
            // 
            this.NewCopyGroupBox.Controls.Add(this.groupBox19);
            this.NewCopyGroupBox.Controls.Add(this.currentGroupCombo);
            this.NewCopyGroupBox.Location = new System.Drawing.Point(9, 6);
            this.NewCopyGroupBox.Name = "NewCopyGroupBox";
            this.NewCopyGroupBox.Size = new System.Drawing.Size(393, 51);
            this.NewCopyGroupBox.TabIndex = 2;
            this.NewCopyGroupBox.TabStop = false;
            this.NewCopyGroupBox.Text = "������ �������";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.copyBtn);
            this.groupBox19.Controls.Add(this.copyGroupsCombo);
            this.groupBox19.Location = new System.Drawing.Point(132, 4);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(255, 42);
            this.groupBox19.TabIndex = 1;
            this.groupBox19.TabStop = false;
            // 
            // copyBtn
            // 
            this.copyBtn.Location = new System.Drawing.Point(6, 13);
            this.copyBtn.Name = "copyBtn";
            this.copyBtn.Size = new System.Drawing.Size(115, 23);
            this.copyBtn.TabIndex = 3;
            this.copyBtn.Text = "���������� �";
            this.copyBtn.UseVisualStyleBackColor = true;
            this.copyBtn.Click += new System.EventHandler(this.copyBtn_Click);
            // 
            // copyGroupsCombo
            // 
            this.copyGroupsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.copyGroupsCombo.FormattingEnabled = true;
            this.copyGroupsCombo.Location = new System.Drawing.Point(129, 14);
            this.copyGroupsCombo.Name = "copyGroupsCombo";
            this.copyGroupsCombo.Size = new System.Drawing.Size(120, 21);
            this.copyGroupsCombo.TabIndex = 2;
            // 
            // currentGroupCombo
            // 
            this.currentGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentGroupCombo.FormattingEnabled = true;
            this.currentGroupCombo.Location = new System.Drawing.Point(6, 19);
            this.currentGroupCombo.Name = "currentGroupCombo";
            this.currentGroupCombo.Size = new System.Drawing.Size(120, 21);
            this.currentGroupCombo.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.tabControl3);
            this.groupBox10.Location = new System.Drawing.Point(3, 64);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1007, 603);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "������";
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this._tokDefendTabPage);
            this.tabControl3.Controls.Add(this._voltageDefendTabPage);
            this.tabControl3.Controls.Add(this._frequencyDefendTabPage);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(3, 16);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(1001, 584);
            this.tabControl3.TabIndex = 0;
            // 
            // _tokDefendTabPage
            // 
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid5);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid3);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid4);
            this._tokDefendTabPage.Controls.Add(this.groupBox15);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid2);
            this._tokDefendTabPage.Controls.Add(this._tokDefenseGrid1);
            this._tokDefendTabPage.Location = new System.Drawing.Point(4, 22);
            this._tokDefendTabPage.Name = "_tokDefendTabPage";
            this._tokDefendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._tokDefendTabPage.Size = new System.Drawing.Size(993, 558);
            this._tokDefendTabPage.TabIndex = 0;
            this._tokDefendTabPage.Text = "������� ������";
            this._tokDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _tokDefenseGrid5
            // 
            this._tokDefenseGrid5.AllowUserToAddRows = false;
            this._tokDefenseGrid5.AllowUserToDeleteRows = false;
            this._tokDefenseGrid5.AllowUserToResizeColumns = false;
            this._tokDefenseGrid5.AllowUserToResizeRows = false;
            this._tokDefenseGrid5.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle214.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle214.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle214.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle214.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle214.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle214.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle214.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle214;
            this._tokDefenseGrid5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn13,
            this.dataGridViewComboBoxColumn14,
            this.dataGridViewCheckBoxColumn13,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn15,
            this.dataGridViewComboBoxColumn16,
            this.dataGridViewComboBoxColumn18,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewCheckBoxColumn14,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewCheckBoxColumn15,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewComboBoxColumn19});
            dataGridViewCellStyle219.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle219.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle219.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle219.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle219.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle219.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle219.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid5.DefaultCellStyle = dataGridViewCellStyle219;
            this._tokDefenseGrid5.Location = new System.Drawing.Point(2, 349);
            this._tokDefenseGrid5.MultiSelect = false;
            this._tokDefenseGrid5.Name = "_tokDefenseGrid5";
            dataGridViewCellStyle220.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle220.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle220.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle220.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle220.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle220.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle220.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid5.RowHeadersDefaultCellStyle = dataGridViewCellStyle220;
            this._tokDefenseGrid5.RowHeadersVisible = false;
            this._tokDefenseGrid5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle221.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid5.RowsDefaultCellStyle = dataGridViewCellStyle221;
            this._tokDefenseGrid5.RowTemplate.Height = 24;
            this._tokDefenseGrid5.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid5.Size = new System.Drawing.Size(987, 104);
            this._tokDefenseGrid5.TabIndex = 14;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.Frozen = true;
            this.dataGridViewTextBoxColumn16.HeaderText = "";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 35;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn13.HeaderText = "�����";
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            this.dataGridViewComboBoxColumn13.Width = 48;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn14.HeaderText = "����������";
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            this.dataGridViewComboBoxColumn14.Width = 74;
            // 
            // dataGridViewCheckBoxColumn13
            // 
            this.dataGridViewCheckBoxColumn13.HeaderText = "���� �� U";
            this.dataGridViewCheckBoxColumn13.Name = "dataGridViewCheckBoxColumn13";
            this.dataGridViewCheckBoxColumn13.Width = 40;
            // 
            // dataGridViewTextBoxColumn20
            // 
            dataGridViewCellStyle215.NullValue = null;
            this.dataGridViewTextBoxColumn20.DefaultCellStyle = dataGridViewCellStyle215;
            this.dataGridViewTextBoxColumn20.HeaderText = "U����, �";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 50;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn15.HeaderText = "�������.";
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            this.dataGridViewComboBoxColumn15.Width = 60;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn16.HeaderText = "������. �������.";
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            this.dataGridViewComboBoxColumn16.Width = 91;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn18.HeaderText = "��������";
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            this.dataGridViewComboBoxColumn18.Width = 64;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle216.NullValue = null;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle216;
            this.dataGridViewTextBoxColumn21.HeaderText = "���.��., I�(��)";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 45;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn20.HeaderText = "�������";
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            this.dataGridViewComboBoxColumn20.Width = 56;
            // 
            // dataGridViewTextBoxColumn22
            // 
            dataGridViewCellStyle217.NullValue = null;
            this.dataGridViewTextBoxColumn22.DefaultCellStyle = dataGridViewCellStyle217;
            this.dataGridViewTextBoxColumn22.HeaderText = "t����, ��";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 55;
            // 
            // dataGridViewCheckBoxColumn14
            // 
            this.dataGridViewCheckBoxColumn14.HeaderText = "���.";
            this.dataGridViewCheckBoxColumn14.Name = "dataGridViewCheckBoxColumn14";
            this.dataGridViewCheckBoxColumn14.Width = 36;
            // 
            // dataGridViewTextBoxColumn23
            // 
            dataGridViewCellStyle218.NullValue = null;
            this.dataGridViewTextBoxColumn23.DefaultCellStyle = dataGridViewCellStyle218;
            this.dataGridViewTextBoxColumn23.HeaderText = "t���, ��";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 50;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "����";
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Width = 43;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            this.dataGridViewCheckBoxColumn16.HeaderText = "���";
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Width = 35;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            this.dataGridViewCheckBoxColumn17.HeaderText = "���";
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Width = 34;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn19.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            this.dataGridViewComboBoxColumn19.Width = 82;
            // 
            // _tokDefenseGrid3
            // 
            this._tokDefenseGrid3.AllowUserToAddRows = false;
            this._tokDefenseGrid3.AllowUserToDeleteRows = false;
            this._tokDefenseGrid3.AllowUserToResizeColumns = false;
            this._tokDefenseGrid3.AllowUserToResizeRows = false;
            this._tokDefenseGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle222.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle222.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle222.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle222.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle222.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle222.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle222.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle222;
            this._tokDefenseGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn11,
            this.dataGridViewComboBoxColumn12,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn17,
            this.Column9,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn12,
            this.dataGridViewComboBoxColumn17});
            dataGridViewCellStyle227.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle227.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle227.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle227.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle227.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle227.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid3.DefaultCellStyle = dataGridViewCellStyle227;
            this._tokDefenseGrid3.Location = new System.Drawing.Point(3, 449);
            this._tokDefenseGrid3.MultiSelect = false;
            this._tokDefenseGrid3.Name = "_tokDefenseGrid3";
            dataGridViewCellStyle228.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle228.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle228.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle228.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle228.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle228.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle228.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle228;
            this._tokDefenseGrid3.RowHeadersVisible = false;
            this._tokDefenseGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle229.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid3.RowsDefaultCellStyle = dataGridViewCellStyle229;
            this._tokDefenseGrid3.RowTemplate.Height = 24;
            this._tokDefenseGrid3.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid3.Size = new System.Drawing.Size(987, 55);
            this._tokDefenseGrid3.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.Frozen = true;
            this.dataGridViewTextBoxColumn14.HeaderText = "";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 35;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "�����";
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Width = 48;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn12.HeaderText = "����������";
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Width = 74;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "���� �� U";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Width = 65;
            // 
            // dataGridViewTextBoxColumn15
            // 
            dataGridViewCellStyle223.NullValue = null;
            this.dataGridViewTextBoxColumn15.DefaultCellStyle = dataGridViewCellStyle223;
            this.dataGridViewTextBoxColumn15.HeaderText = "U����, �";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 70;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle224.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle224;
            this.dataGridViewTextBoxColumn17.HeaderText = "���.��., I�";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "Column9";
            this.Column9.Name = "Column9";
            this.Column9.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            dataGridViewCellStyle225.NullValue = null;
            this.dataGridViewTextBoxColumn18.DefaultCellStyle = dataGridViewCellStyle225;
            this.dataGridViewTextBoxColumn18.HeaderText = "t��, ��";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 70;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "���.";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Width = 40;
            // 
            // dataGridViewTextBoxColumn19
            // 
            dataGridViewCellStyle226.NullValue = null;
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle226;
            this.dataGridViewTextBoxColumn19.HeaderText = "t���, ��";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 70;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "����";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Width = 43;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "���";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Width = 35;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            this.dataGridViewCheckBoxColumn12.HeaderText = "���";
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Width = 34;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn17.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            this.dataGridViewComboBoxColumn17.Width = 82;
            // 
            // _tokDefenseGrid4
            // 
            this._tokDefenseGrid4.AllowUserToAddRows = false;
            this._tokDefenseGrid4.AllowUserToDeleteRows = false;
            this._tokDefenseGrid4.AllowUserToResizeColumns = false;
            this._tokDefenseGrid4.AllowUserToResizeRows = false;
            this._tokDefenseGrid4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid4.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle230.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle230.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle230.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle230.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle230.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle230.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle230.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle230;
            this._tokDefenseGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this._tokDefense4UpuskCol,
            this._tokDefense4PuskConstraintCol,
            this.Column10,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4AVRCol,
            this._tokDefense4OSCv11Col});
            dataGridViewCellStyle234.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle234.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle234.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle234.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle234.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle234.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle234.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid4.DefaultCellStyle = dataGridViewCellStyle234;
            this._tokDefenseGrid4.Location = new System.Drawing.Point(3, 502);
            this._tokDefenseGrid4.MultiSelect = false;
            this._tokDefenseGrid4.Name = "_tokDefenseGrid4";
            dataGridViewCellStyle235.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle235.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle235.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle235.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle235.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle235.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle235.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid4.RowHeadersDefaultCellStyle = dataGridViewCellStyle235;
            this._tokDefenseGrid4.RowHeadersVisible = false;
            this._tokDefenseGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle236.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid4.RowsDefaultCellStyle = dataGridViewCellStyle236;
            this._tokDefenseGrid4.RowTemplate.Height = 24;
            this._tokDefenseGrid4.Size = new System.Drawing.Size(987, 56);
            this._tokDefenseGrid4.TabIndex = 12;
            // 
            // _tokDefense4NameCol
            // 
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 35;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 48;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 74;
            // 
            // _tokDefense4UpuskCol
            // 
            this._tokDefense4UpuskCol.HeaderText = "���� �� U";
            this._tokDefense4UpuskCol.Name = "_tokDefense4UpuskCol";
            this._tokDefense4UpuskCol.Visible = false;
            this._tokDefense4UpuskCol.Width = 65;
            // 
            // _tokDefense4PuskConstraintCol
            // 
            dataGridViewCellStyle231.NullValue = null;
            this._tokDefense4PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle231;
            this._tokDefense4PuskConstraintCol.HeaderText = "U����, �";
            this._tokDefense4PuskConstraintCol.Name = "_tokDefense4PuskConstraintCol";
            this._tokDefense4PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4PuskConstraintCol.Visible = false;
            this._tokDefense4PuskConstraintCol.Width = 70;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Column10";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle232.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle232;
            this._tokDefense4WorkConstraintCol.HeaderText = "���. ��.,%";
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 70;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle233.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle233;
            this._tokDefense4WorkTimeCol.HeaderText = "t��, ��";
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 70;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Visible = false;
            this._tokDefense4SpeedupCol.Width = 40;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "t���., ��";
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Visible = false;
            this._tokDefense4SpeedupTimeCol.Width = 70;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 43;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 35;
            // 
            // _tokDefense4AVRCol
            // 
            this._tokDefense4AVRCol.HeaderText = "���";
            this._tokDefense4AVRCol.Name = "_tokDefense4AVRCol";
            this._tokDefense4AVRCol.Width = 34;
            // 
            // _tokDefense4OSCv11Col
            // 
            this._tokDefense4OSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4OSCv11Col.HeaderText = "�����������";
            this._tokDefense4OSCv11Col.Name = "_tokDefense4OSCv11Col";
            this._tokDefense4OSCv11Col.Width = 82;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._tokDefenseInbox);
            this.groupBox15.Controls.Add(this.label24);
            this.groupBox15.Controls.Add(this._tokDefenseI2box);
            this.groupBox15.Controls.Add(this.label23);
            this.groupBox15.Controls.Add(this._tokDefenseI0box);
            this.groupBox15.Controls.Add(this.label22);
            this.groupBox15.Controls.Add(this._tokDefenseIbox);
            this.groupBox15.Controls.Add(this.label21);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(261, 41);
            this.groupBox15.TabIndex = 5;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "������������ ���� (���� ��)";
            // 
            // _tokDefenseInbox
            // 
            this._tokDefenseInbox.Location = new System.Drawing.Point(216, 15);
            this._tokDefenseInbox.Name = "_tokDefenseInbox";
            this._tokDefenseInbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseInbox.TabIndex = 7;
            this._tokDefenseInbox.Tag = "0,360";
            this._tokDefenseInbox.Text = "0";
            this._tokDefenseInbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(200, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "In";
            // 
            // _tokDefenseI2box
            // 
            this._tokDefenseI2box.Location = new System.Drawing.Point(152, 15);
            this._tokDefenseI2box.Name = "_tokDefenseI2box";
            this._tokDefenseI2box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI2box.TabIndex = 5;
            this._tokDefenseI2box.Tag = "0,360";
            this._tokDefenseI2box.Text = "0";
            this._tokDefenseI2box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(136, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "I2";
            // 
            // _tokDefenseI0box
            // 
            this._tokDefenseI0box.Location = new System.Drawing.Point(91, 15);
            this._tokDefenseI0box.Name = "_tokDefenseI0box";
            this._tokDefenseI0box.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseI0box.TabIndex = 3;
            this._tokDefenseI0box.Tag = "0,360";
            this._tokDefenseI0box.Text = "0";
            this._tokDefenseI0box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(75, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "I0";
            // 
            // _tokDefenseIbox
            // 
            this._tokDefenseIbox.Location = new System.Drawing.Point(31, 15);
            this._tokDefenseIbox.Name = "_tokDefenseIbox";
            this._tokDefenseIbox.Size = new System.Drawing.Size(37, 20);
            this._tokDefenseIbox.TabIndex = 1;
            this._tokDefenseIbox.Tag = "0,360";
            this._tokDefenseIbox.Text = "0";
            this._tokDefenseIbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(10, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "I";
            // 
            // _tokDefenseGrid2
            // 
            this._tokDefenseGrid2.AllowUserToAddRows = false;
            this._tokDefenseGrid2.AllowUserToDeleteRows = false;
            this._tokDefenseGrid2.AllowUserToResizeColumns = false;
            this._tokDefenseGrid2.AllowUserToResizeRows = false;
            this._tokDefenseGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle237.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle237.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle237.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle237.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle237.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle237.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle237.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle237;
            this._tokDefenseGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3UpuskCol,
            this._tokDefense3PuskConstraintCol,
            this._tokDefense3DirectionCol,
            this._tokDefense3BlockingExistCol,
            this._tokDefense3ParameterCol,
            this._tokDefense3WorkConstraintCol,
            this.Column12,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3AVRCol,
            this._tokDefense3OSCv11Col});
            dataGridViewCellStyle242.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle242.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle242.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle242.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle242.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle242.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle242.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid2.DefaultCellStyle = dataGridViewCellStyle242;
            this._tokDefenseGrid2.Location = new System.Drawing.Point(3, 206);
            this._tokDefenseGrid2.MultiSelect = false;
            this._tokDefenseGrid2.Name = "_tokDefenseGrid2";
            dataGridViewCellStyle243.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle243.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle243.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle243.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle243.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle243.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle243.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle243;
            this._tokDefenseGrid2.RowHeadersVisible = false;
            this._tokDefenseGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle244.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid2.RowsDefaultCellStyle = dataGridViewCellStyle244;
            this._tokDefenseGrid2.RowTemplate.Height = 24;
            this._tokDefenseGrid2.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid2.Size = new System.Drawing.Size(987, 144);
            this._tokDefenseGrid2.TabIndex = 11;
            // 
            // _tokDefense3NameCol
            // 
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 35;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 48;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 74;
            // 
            // _tokDefense3UpuskCol
            // 
            this._tokDefense3UpuskCol.HeaderText = "���� �� U";
            this._tokDefense3UpuskCol.Name = "_tokDefense3UpuskCol";
            this._tokDefense3UpuskCol.Width = 40;
            // 
            // _tokDefense3PuskConstraintCol
            // 
            dataGridViewCellStyle238.NullValue = null;
            this._tokDefense3PuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle238;
            this._tokDefense3PuskConstraintCol.HeaderText = "U����, �";
            this._tokDefense3PuskConstraintCol.Name = "_tokDefense3PuskConstraintCol";
            this._tokDefense3PuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3PuskConstraintCol.Width = 50;
            // 
            // _tokDefense3DirectionCol
            // 
            this._tokDefense3DirectionCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3DirectionCol.HeaderText = "�������.";
            this._tokDefense3DirectionCol.Name = "_tokDefense3DirectionCol";
            this._tokDefense3DirectionCol.Width = 60;
            // 
            // _tokDefense3BlockingExistCol
            // 
            this._tokDefense3BlockingExistCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3BlockingExistCol.HeaderText = "������. �������.";
            this._tokDefense3BlockingExistCol.Name = "_tokDefense3BlockingExistCol";
            this._tokDefense3BlockingExistCol.Width = 91;
            // 
            // _tokDefense3ParameterCol
            // 
            this._tokDefense3ParameterCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3ParameterCol.HeaderText = "��������";
            this._tokDefense3ParameterCol.Name = "_tokDefense3ParameterCol";
            this._tokDefense3ParameterCol.Width = 64;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle239.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle239;
            this._tokDefense3WorkConstraintCol.HeaderText = "���.��., I�(��)";
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 45;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Column12";
            this.Column12.Name = "Column12";
            this.Column12.Visible = false;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle240.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle240;
            this._tokDefense3WorkTimeCol.HeaderText = "t����, ��";
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 55;
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 36;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle241.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle241;
            this._tokDefense3SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 50;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 35;
            // 
            // _tokDefense3AVRCol
            // 
            this._tokDefense3AVRCol.HeaderText = "���";
            this._tokDefense3AVRCol.Name = "_tokDefense3AVRCol";
            this._tokDefense3AVRCol.Width = 34;
            // 
            // _tokDefense3OSCv11Col
            // 
            this._tokDefense3OSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3OSCv11Col.HeaderText = "�����������";
            this._tokDefense3OSCv11Col.Name = "_tokDefense3OSCv11Col";
            this._tokDefense3OSCv11Col.Width = 82;
            // 
            // _tokDefenseGrid1
            // 
            this._tokDefenseGrid1.AllowUserToAddRows = false;
            this._tokDefenseGrid1.AllowUserToDeleteRows = false;
            this._tokDefenseGrid1.AllowUserToResizeColumns = false;
            this._tokDefenseGrid1.AllowUserToResizeRows = false;
            this._tokDefenseGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tokDefenseGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle245.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle245.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle245.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle245.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle245.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle245.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle245.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle245;
            this._tokDefenseGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._tokDefenseGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseU_PuskCol,
            this._tokDefensePuskConstraintCol,
            this._tokDefenseDirectionCol,
            this._tokDefenseBlockExistCol,
            this._tokDefenseParameterCol,
            this._tokDefenseWorkConstraintCol,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseAVRCol,
            this._tokDefenseOSCv11Col});
            dataGridViewCellStyle250.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle250.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle250.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle250.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle250.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle250.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle250.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._tokDefenseGrid1.DefaultCellStyle = dataGridViewCellStyle250;
            this._tokDefenseGrid1.Location = new System.Drawing.Point(3, 52);
            this._tokDefenseGrid1.MultiSelect = false;
            this._tokDefenseGrid1.Name = "_tokDefenseGrid1";
            dataGridViewCellStyle251.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle251.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle251.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle251.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle251.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle251.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle251.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._tokDefenseGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle251;
            this._tokDefenseGrid1.RowHeadersVisible = false;
            this._tokDefenseGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle252.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._tokDefenseGrid1.RowsDefaultCellStyle = dataGridViewCellStyle252;
            this._tokDefenseGrid1.RowTemplate.Height = 24;
            this._tokDefenseGrid1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._tokDefenseGrid1.Size = new System.Drawing.Size(987, 156);
            this._tokDefenseGrid1.TabIndex = 10;
            // 
            // _tokDefenseNameCol
            // 
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 40;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 74;
            // 
            // _tokDefenseU_PuskCol
            // 
            this._tokDefenseU_PuskCol.HeaderText = "���� �� U";
            this._tokDefenseU_PuskCol.Name = "_tokDefenseU_PuskCol";
            this._tokDefenseU_PuskCol.Width = 40;
            // 
            // _tokDefensePuskConstraintCol
            // 
            dataGridViewCellStyle246.NullValue = null;
            this._tokDefensePuskConstraintCol.DefaultCellStyle = dataGridViewCellStyle246;
            this._tokDefensePuskConstraintCol.HeaderText = "U����, �";
            this._tokDefensePuskConstraintCol.Name = "_tokDefensePuskConstraintCol";
            this._tokDefensePuskConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefensePuskConstraintCol.Width = 50;
            // 
            // _tokDefenseDirectionCol
            // 
            this._tokDefenseDirectionCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseDirectionCol.HeaderText = "�������.";
            this._tokDefenseDirectionCol.Name = "_tokDefenseDirectionCol";
            this._tokDefenseDirectionCol.Width = 60;
            // 
            // _tokDefenseBlockExistCol
            // 
            this._tokDefenseBlockExistCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseBlockExistCol.HeaderText = "������. �������.";
            this._tokDefenseBlockExistCol.Name = "_tokDefenseBlockExistCol";
            this._tokDefenseBlockExistCol.Width = 91;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle247.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle247;
            this._tokDefenseWorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 40;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseFeatureCol.HeaderText = "������-��";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 64;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle248.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle248;
            this._tokDefenseWorkTimeCol.HeaderText = "t����, ��/����.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 60;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle249.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle249;
            this._tokDefenseSpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 48;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 43;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 35;
            // 
            // _tokDefenseAVRCol
            // 
            this._tokDefenseAVRCol.HeaderText = "���";
            this._tokDefenseAVRCol.Name = "_tokDefenseAVRCol";
            this._tokDefenseAVRCol.Width = 34;
            // 
            // _tokDefenseOSCv11Col
            // 
            this._tokDefenseOSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseOSCv11Col.HeaderText = "����������";
            this._tokDefenseOSCv11Col.Name = "_tokDefenseOSCv11Col";
            this._tokDefenseOSCv11Col.Width = 76;
            // 
            // _voltageDefendTabPage
            // 
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid11);
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid2);
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid1);
            this._voltageDefendTabPage.Controls.Add(this._voltageDefensesGrid3);
            this._voltageDefendTabPage.Location = new System.Drawing.Point(4, 22);
            this._voltageDefendTabPage.Name = "_voltageDefendTabPage";
            this._voltageDefendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._voltageDefendTabPage.Size = new System.Drawing.Size(993, 558);
            this._voltageDefendTabPage.TabIndex = 1;
            this._voltageDefendTabPage.Text = "������ �� ����������";
            this._voltageDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _voltageDefensesGrid11
            // 
            this._voltageDefensesGrid11.AllowUserToAddRows = false;
            this._voltageDefensesGrid11.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid11.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid11.AllowUserToResizeRows = false;
            this._voltageDefensesGrid11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid11.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle253.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle253.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle253.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle253.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle253.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle253.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle253.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid11.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle253;
            this._voltageDefensesGrid11.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7});
            dataGridViewCellStyle257.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle257.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle257.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle257.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle257.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle257.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle257.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid11.DefaultCellStyle = dataGridViewCellStyle257;
            this._voltageDefensesGrid11.Location = new System.Drawing.Point(3, 110);
            this._voltageDefensesGrid11.MultiSelect = false;
            this._voltageDefensesGrid11.Name = "_voltageDefensesGrid11";
            dataGridViewCellStyle258.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle258.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle258.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle258.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle258.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle258.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle258.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid11.RowHeadersDefaultCellStyle = dataGridViewCellStyle258;
            this._voltageDefensesGrid11.RowHeadersVisible = false;
            this._voltageDefensesGrid11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle259.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid11.RowsDefaultCellStyle = dataGridViewCellStyle259;
            this._voltageDefensesGrid11.RowTemplate.Height = 24;
            this._voltageDefensesGrid11.Size = new System.Drawing.Size(987, 80);
            this._voltageDefensesGrid11.TabIndex = 14;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle254.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle254;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 35;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "�����";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn4.Width = 48;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn5.HeaderText = "����������";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn5.Width = 74;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle255.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.dataGridViewComboBoxColumn6.DefaultCellStyle = dataGridViewCellStyle255;
            this.dataGridViewComboBoxColumn6.HeaderText = "��������";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn6.Width = 64;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Column1";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            this.dataGridViewTextBoxColumn5.Width = 73;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Column2";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            this.dataGridViewTextBoxColumn6.Width = 73;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "U��, �";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle256.NullValue = "0";
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle256;
            this.dataGridViewTextBoxColumn11.HeaderText = "t��, ��";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 50;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "����.";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 41;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "��� ��";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn2.Width = 52;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "U��, �";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 50;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "t��, ��";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "����";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn3.Width = 43;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "���";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 35;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "���";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 34;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn7.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn7.Width = 82;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "�����";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn6.Width = 44;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "U<5B";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn7.Width = 40;
            // 
            // _voltageDefensesGrid2
            // 
            this._voltageDefensesGrid2.AllowUserToAddRows = false;
            this._voltageDefensesGrid2.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid2.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid2.AllowUserToResizeRows = false;
            this._voltageDefensesGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid2.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle260.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle260.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle260.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle260.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle260.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle260.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle260.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle260;
            this._voltageDefensesGrid2.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol2,
            this._voltageDefensesModeCol2,
            this._voltageDefensesBlockingNumberCol2,
            this.Column3,
            this._voltageDefensesParameterCol2,
            this.Column4,
            this._voltageDefensesWorkConstraintCol2,
            this._voltageDefensesWorkTimeCol2,
            this._voltageDefensesReturnCol2,
            this._voltageDefensesAPVreturnCol2,
            this._voltageDefensesReturnConstraintCol2,
            this._voltageDefensesReturnTimeCol2,
            this._voltageDefensesUROVcol2,
            this._voltageDefensesAPVcol2,
            this._voltageDefensesAVRcol2,
            this._voltageDefensesOSC�11Col2,
            this._voltageDefensesResetCol2,
            this.Column7});
            dataGridViewCellStyle264.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle264.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle264.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle264.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle264.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle264.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle264.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid2.DefaultCellStyle = dataGridViewCellStyle264;
            this._voltageDefensesGrid2.Location = new System.Drawing.Point(3, 214);
            this._voltageDefensesGrid2.MultiSelect = false;
            this._voltageDefensesGrid2.Name = "_voltageDefensesGrid2";
            dataGridViewCellStyle265.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle265.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle265.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle265.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle265.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle265.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle265.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle265;
            this._voltageDefensesGrid2.RowHeadersVisible = false;
            this._voltageDefensesGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle266.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid2.RowsDefaultCellStyle = dataGridViewCellStyle266;
            this._voltageDefensesGrid2.RowTemplate.Height = 24;
            this._voltageDefensesGrid2.Size = new System.Drawing.Size(987, 80);
            this._voltageDefensesGrid2.TabIndex = 12;
            // 
            // _voltageDefensesNameCol2
            // 
            dataGridViewCellStyle261.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol2.DefaultCellStyle = dataGridViewCellStyle261;
            this._voltageDefensesNameCol2.Frozen = true;
            this._voltageDefensesNameCol2.HeaderText = "";
            this._voltageDefensesNameCol2.Name = "_voltageDefensesNameCol2";
            this._voltageDefensesNameCol2.ReadOnly = true;
            this._voltageDefensesNameCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesNameCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol2.Width = 35;
            // 
            // _voltageDefensesModeCol2
            // 
            this._voltageDefensesModeCol2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesModeCol2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol2.HeaderText = "�����";
            this._voltageDefensesModeCol2.Name = "_voltageDefensesModeCol2";
            this._voltageDefensesModeCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesModeCol2.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol2
            // 
            this._voltageDefensesBlockingNumberCol2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesBlockingNumberCol2.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol2.Name = "_voltageDefensesBlockingNumberCol2";
            this._voltageDefensesBlockingNumberCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesBlockingNumberCol2.Width = 74;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            this.Column3.Width = 73;
            // 
            // _voltageDefensesParameterCol2
            // 
            this._voltageDefensesParameterCol2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle262.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol2.DefaultCellStyle = dataGridViewCellStyle262;
            this._voltageDefensesParameterCol2.HeaderText = "��������";
            this._voltageDefensesParameterCol2.Name = "_voltageDefensesParameterCol2";
            this._voltageDefensesParameterCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesParameterCol2.Width = 64;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            this.Column4.Width = 73;
            // 
            // _voltageDefensesWorkConstraintCol2
            // 
            this._voltageDefensesWorkConstraintCol2.HeaderText = "U��, �";
            this._voltageDefensesWorkConstraintCol2.Name = "_voltageDefensesWorkConstraintCol2";
            this._voltageDefensesWorkConstraintCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesWorkConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol2.Width = 50;
            // 
            // _voltageDefensesWorkTimeCol2
            // 
            dataGridViewCellStyle263.NullValue = "0";
            this._voltageDefensesWorkTimeCol2.DefaultCellStyle = dataGridViewCellStyle263;
            this._voltageDefensesWorkTimeCol2.HeaderText = "t��, ��";
            this._voltageDefensesWorkTimeCol2.Name = "_voltageDefensesWorkTimeCol2";
            this._voltageDefensesWorkTimeCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesWorkTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol2.Width = 50;
            // 
            // _voltageDefensesReturnCol2
            // 
            this._voltageDefensesReturnCol2.HeaderText = "����.";
            this._voltageDefensesReturnCol2.Name = "_voltageDefensesReturnCol2";
            this._voltageDefensesReturnCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnCol2.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol2
            // 
            this._voltageDefensesAPVreturnCol2.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol2.Name = "_voltageDefensesAPVreturnCol2";
            this._voltageDefensesAPVreturnCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAPVreturnCol2.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol2
            // 
            this._voltageDefensesReturnConstraintCol2.HeaderText = "U��, �";
            this._voltageDefensesReturnConstraintCol2.Name = "_voltageDefensesReturnConstraintCol2";
            this._voltageDefensesReturnConstraintCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnConstraintCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol2.Width = 50;
            // 
            // _voltageDefensesReturnTimeCol2
            // 
            this._voltageDefensesReturnTimeCol2.HeaderText = "t��, ��";
            this._voltageDefensesReturnTimeCol2.Name = "_voltageDefensesReturnTimeCol2";
            this._voltageDefensesReturnTimeCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnTimeCol2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol2.Width = 50;
            // 
            // _voltageDefensesUROVcol2
            // 
            this._voltageDefensesUROVcol2.HeaderText = "����";
            this._voltageDefensesUROVcol2.Name = "_voltageDefensesUROVcol2";
            this._voltageDefensesUROVcol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesUROVcol2.Width = 43;
            // 
            // _voltageDefensesAPVcol2
            // 
            this._voltageDefensesAPVcol2.HeaderText = "���";
            this._voltageDefensesAPVcol2.Name = "_voltageDefensesAPVcol2";
            this._voltageDefensesAPVcol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAPVcol2.Width = 35;
            // 
            // _voltageDefensesAVRcol2
            // 
            this._voltageDefensesAVRcol2.HeaderText = "���";
            this._voltageDefensesAVRcol2.Name = "_voltageDefensesAVRcol2";
            this._voltageDefensesAVRcol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAVRcol2.Width = 34;
            // 
            // _voltageDefensesOSC�11Col2
            // 
            this._voltageDefensesOSC�11Col2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesOSC�11Col2.HeaderText = "�����������";
            this._voltageDefensesOSC�11Col2.Name = "_voltageDefensesOSC�11Col2";
            this._voltageDefensesOSC�11Col2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesOSC�11Col2.Width = 82;
            // 
            // _voltageDefensesResetCol2
            // 
            this._voltageDefensesResetCol2.HeaderText = "�����";
            this._voltageDefensesResetCol2.Name = "_voltageDefensesResetCol2";
            this._voltageDefensesResetCol2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesResetCol2.Width = 50;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            this.Column7.Width = 54;
            // 
            // _voltageDefensesGrid1
            // 
            this._voltageDefensesGrid1.AllowUserToAddRows = false;
            this._voltageDefensesGrid1.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid1.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid1.AllowUserToResizeRows = false;
            this._voltageDefensesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle267.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle267.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle267.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle267.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle267.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle267.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle267.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle267;
            this._voltageDefensesGrid1.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol1,
            this._voltageDefensesModeCol1,
            this._voltageDefensesBlockingNumberCol1,
            this._voltageDefensesParameterCol1,
            this.Column1,
            this.Column2,
            this._voltageDefensesWorkConstraintCol1,
            this._voltageDefensesWorkTimeCol1,
            this._voltageDefensesReturnCol1,
            this._voltageDefensesAPVReturlCol1,
            this._voltageDefensesReturnConstraintCol1,
            this._voltageDefensesReturnTimeCol1,
            this._voltageDefensesUROVCol1,
            this._voltageDefensesAPVCol1,
            this._voltageDefensesAVRCol1,
            this._voltageDefensesOSCv11Col1,
            this._voltageDefensesResetCol1,
            this._u5vColumn});
            dataGridViewCellStyle271.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle271.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle271.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle271.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle271.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle271.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle271.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid1.DefaultCellStyle = dataGridViewCellStyle271;
            this._voltageDefensesGrid1.Location = new System.Drawing.Point(3, 6);
            this._voltageDefensesGrid1.MultiSelect = false;
            this._voltageDefensesGrid1.Name = "_voltageDefensesGrid1";
            dataGridViewCellStyle272.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle272.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle272.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle272.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle272.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle272.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle272.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle272;
            this._voltageDefensesGrid1.RowHeadersVisible = false;
            this._voltageDefensesGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle273.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid1.RowsDefaultCellStyle = dataGridViewCellStyle273;
            this._voltageDefensesGrid1.RowTemplate.Height = 24;
            this._voltageDefensesGrid1.Size = new System.Drawing.Size(987, 80);
            this._voltageDefensesGrid1.TabIndex = 11;
            // 
            // _voltageDefensesNameCol1
            // 
            dataGridViewCellStyle268.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._voltageDefensesNameCol1.DefaultCellStyle = dataGridViewCellStyle268;
            this._voltageDefensesNameCol1.Frozen = true;
            this._voltageDefensesNameCol1.HeaderText = "";
            this._voltageDefensesNameCol1.Name = "_voltageDefensesNameCol1";
            this._voltageDefensesNameCol1.ReadOnly = true;
            this._voltageDefensesNameCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesNameCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol1.Width = 35;
            // 
            // _voltageDefensesModeCol1
            // 
            this._voltageDefensesModeCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesModeCol1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol1.HeaderText = "�����";
            this._voltageDefensesModeCol1.Name = "_voltageDefensesModeCol1";
            this._voltageDefensesModeCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesModeCol1.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol1
            // 
            this._voltageDefensesBlockingNumberCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesBlockingNumberCol1.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol1.Name = "_voltageDefensesBlockingNumberCol1";
            this._voltageDefensesBlockingNumberCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesBlockingNumberCol1.Width = 74;
            // 
            // _voltageDefensesParameterCol1
            // 
            this._voltageDefensesParameterCol1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle269.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol1.DefaultCellStyle = dataGridViewCellStyle269;
            this._voltageDefensesParameterCol1.HeaderText = "��������";
            this._voltageDefensesParameterCol1.Name = "_voltageDefensesParameterCol1";
            this._voltageDefensesParameterCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesParameterCol1.Width = 64;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 73;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            this.Column2.Width = 73;
            // 
            // _voltageDefensesWorkConstraintCol1
            // 
            this._voltageDefensesWorkConstraintCol1.HeaderText = "U��, �";
            this._voltageDefensesWorkConstraintCol1.Name = "_voltageDefensesWorkConstraintCol1";
            this._voltageDefensesWorkConstraintCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesWorkConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol1.Width = 50;
            // 
            // _voltageDefensesWorkTimeCol1
            // 
            dataGridViewCellStyle270.NullValue = "0";
            this._voltageDefensesWorkTimeCol1.DefaultCellStyle = dataGridViewCellStyle270;
            this._voltageDefensesWorkTimeCol1.HeaderText = "t��, ��";
            this._voltageDefensesWorkTimeCol1.Name = "_voltageDefensesWorkTimeCol1";
            this._voltageDefensesWorkTimeCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesWorkTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkTimeCol1.Width = 50;
            // 
            // _voltageDefensesReturnCol1
            // 
            this._voltageDefensesReturnCol1.HeaderText = "����.";
            this._voltageDefensesReturnCol1.Name = "_voltageDefensesReturnCol1";
            this._voltageDefensesReturnCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnCol1.Width = 40;
            // 
            // _voltageDefensesAPVReturlCol1
            // 
            this._voltageDefensesAPVReturlCol1.HeaderText = "��� ��";
            this._voltageDefensesAPVReturlCol1.Name = "_voltageDefensesAPVReturlCol1";
            this._voltageDefensesAPVReturlCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAPVReturlCol1.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol1
            // 
            this._voltageDefensesReturnConstraintCol1.HeaderText = "U��, �";
            this._voltageDefensesReturnConstraintCol1.Name = "_voltageDefensesReturnConstraintCol1";
            this._voltageDefensesReturnConstraintCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnConstraintCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol1.Width = 50;
            // 
            // _voltageDefensesReturnTimeCol1
            // 
            this._voltageDefensesReturnTimeCol1.HeaderText = "t��, ��";
            this._voltageDefensesReturnTimeCol1.Name = "_voltageDefensesReturnTimeCol1";
            this._voltageDefensesReturnTimeCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesReturnTimeCol1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol1.Width = 50;
            // 
            // _voltageDefensesUROVCol1
            // 
            this._voltageDefensesUROVCol1.HeaderText = "����";
            this._voltageDefensesUROVCol1.Name = "_voltageDefensesUROVCol1";
            this._voltageDefensesUROVCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesUROVCol1.Width = 43;
            // 
            // _voltageDefensesAPVCol1
            // 
            this._voltageDefensesAPVCol1.HeaderText = "���";
            this._voltageDefensesAPVCol1.Name = "_voltageDefensesAPVCol1";
            this._voltageDefensesAPVCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAPVCol1.Width = 35;
            // 
            // _voltageDefensesAVRCol1
            // 
            this._voltageDefensesAVRCol1.HeaderText = "���";
            this._voltageDefensesAVRCol1.Name = "_voltageDefensesAVRCol1";
            this._voltageDefensesAVRCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesAVRCol1.Width = 34;
            // 
            // _voltageDefensesOSCv11Col1
            // 
            this._voltageDefensesOSCv11Col1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesOSCv11Col1.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col1.Name = "_voltageDefensesOSCv11Col1";
            this._voltageDefensesOSCv11Col1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesOSCv11Col1.Width = 82;
            // 
            // _voltageDefensesResetCol1
            // 
            this._voltageDefensesResetCol1.HeaderText = "�����";
            this._voltageDefensesResetCol1.Name = "_voltageDefensesResetCol1";
            this._voltageDefensesResetCol1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesResetCol1.Width = 44;
            // 
            // _u5vColumn
            // 
            this._u5vColumn.HeaderText = "U<5B";
            this._u5vColumn.Name = "_u5vColumn";
            this._u5vColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._u5vColumn.Visible = false;
            this._u5vColumn.Width = 40;
            // 
            // _voltageDefensesGrid3
            // 
            this._voltageDefensesGrid3.AllowUserToAddRows = false;
            this._voltageDefensesGrid3.AllowUserToDeleteRows = false;
            this._voltageDefensesGrid3.AllowUserToResizeColumns = false;
            this._voltageDefensesGrid3.AllowUserToResizeRows = false;
            this._voltageDefensesGrid3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._voltageDefensesGrid3.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle274.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle274.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle274.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle274.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle274.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle274.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle274.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle274;
            this._voltageDefensesGrid3.ColumnHeadersHeight = 30;
            this._voltageDefensesGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._voltageDefensesGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._voltageDefensesNameCol3,
            this._voltageDefensesModeCol3,
            this._voltageDefensesBlockingNumberCol3,
            this.Column5,
            this.Column6,
            this._voltageDefensesParameterCol3,
            this._voltageDefensesWorkConstraintCol3,
            this._voltageDefensesTimeConstraintCol3,
            this._voltageDefensesReturnCol3,
            this._voltageDefensesAPVreturnCol3,
            this._voltageDefensesReturnConstraintCol3,
            this._voltageDefensesReturnTimeCol3,
            this._voltageDefensesUROVcol3,
            this._voltageDefensesAPVcol3,
            this._voltageDefensesAVRcol3,
            this._voltageDefensesOSCv11Col3,
            this._voltageDefensesResetcol3,
            this.Column8});
            dataGridViewCellStyle278.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle278.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle278.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle278.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle278.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle278.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle278.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._voltageDefensesGrid3.DefaultCellStyle = dataGridViewCellStyle278;
            this._voltageDefensesGrid3.Location = new System.Drawing.Point(3, 321);
            this._voltageDefensesGrid3.MultiSelect = false;
            this._voltageDefensesGrid3.Name = "_voltageDefensesGrid3";
            dataGridViewCellStyle279.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle279.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle279.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle279.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle279.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle279.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle279.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle279;
            this._voltageDefensesGrid3.RowHeadersVisible = false;
            this._voltageDefensesGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle280.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._voltageDefensesGrid3.RowsDefaultCellStyle = dataGridViewCellStyle280;
            this._voltageDefensesGrid3.RowTemplate.Height = 24;
            this._voltageDefensesGrid3.Size = new System.Drawing.Size(987, 80);
            this._voltageDefensesGrid3.TabIndex = 13;
            // 
            // _voltageDefensesNameCol3
            // 
            dataGridViewCellStyle275.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._voltageDefensesNameCol3.DefaultCellStyle = dataGridViewCellStyle275;
            this._voltageDefensesNameCol3.Frozen = true;
            this._voltageDefensesNameCol3.HeaderText = "";
            this._voltageDefensesNameCol3.Name = "_voltageDefensesNameCol3";
            this._voltageDefensesNameCol3.ReadOnly = true;
            this._voltageDefensesNameCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesNameCol3.Width = 35;
            // 
            // _voltageDefensesModeCol3
            // 
            this._voltageDefensesModeCol3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesModeCol3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._voltageDefensesModeCol3.HeaderText = "�����";
            this._voltageDefensesModeCol3.Name = "_voltageDefensesModeCol3";
            this._voltageDefensesModeCol3.Width = 48;
            // 
            // _voltageDefensesBlockingNumberCol3
            // 
            this._voltageDefensesBlockingNumberCol3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesBlockingNumberCol3.HeaderText = "����������";
            this._voltageDefensesBlockingNumberCol3.Name = "_voltageDefensesBlockingNumberCol3";
            this._voltageDefensesBlockingNumberCol3.Width = 74;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            this.Column5.Width = 73;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            this.Column6.Width = 73;
            // 
            // _voltageDefensesParameterCol3
            // 
            this._voltageDefensesParameterCol3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle276.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._voltageDefensesParameterCol3.DefaultCellStyle = dataGridViewCellStyle276;
            this._voltageDefensesParameterCol3.HeaderText = "��������";
            this._voltageDefensesParameterCol3.Name = "_voltageDefensesParameterCol3";
            this._voltageDefensesParameterCol3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._voltageDefensesParameterCol3.Width = 64;
            // 
            // _voltageDefensesWorkConstraintCol3
            // 
            this._voltageDefensesWorkConstraintCol3.HeaderText = "U��, �";
            this._voltageDefensesWorkConstraintCol3.Name = "_voltageDefensesWorkConstraintCol3";
            this._voltageDefensesWorkConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesWorkConstraintCol3.Width = 50;
            // 
            // _voltageDefensesTimeConstraintCol3
            // 
            dataGridViewCellStyle277.NullValue = "0";
            this._voltageDefensesTimeConstraintCol3.DefaultCellStyle = dataGridViewCellStyle277;
            this._voltageDefensesTimeConstraintCol3.HeaderText = "t��, ��";
            this._voltageDefensesTimeConstraintCol3.Name = "_voltageDefensesTimeConstraintCol3";
            this._voltageDefensesTimeConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesTimeConstraintCol3.Width = 50;
            // 
            // _voltageDefensesReturnCol3
            // 
            this._voltageDefensesReturnCol3.HeaderText = "����.";
            this._voltageDefensesReturnCol3.Name = "_voltageDefensesReturnCol3";
            this._voltageDefensesReturnCol3.Width = 41;
            // 
            // _voltageDefensesAPVreturnCol3
            // 
            this._voltageDefensesAPVreturnCol3.HeaderText = "��� ��";
            this._voltageDefensesAPVreturnCol3.Name = "_voltageDefensesAPVreturnCol3";
            this._voltageDefensesAPVreturnCol3.Width = 52;
            // 
            // _voltageDefensesReturnConstraintCol3
            // 
            this._voltageDefensesReturnConstraintCol3.HeaderText = "U��, �";
            this._voltageDefensesReturnConstraintCol3.Name = "_voltageDefensesReturnConstraintCol3";
            this._voltageDefensesReturnConstraintCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnConstraintCol3.Width = 50;
            // 
            // _voltageDefensesReturnTimeCol3
            // 
            this._voltageDefensesReturnTimeCol3.HeaderText = "t��, ��";
            this._voltageDefensesReturnTimeCol3.Name = "_voltageDefensesReturnTimeCol3";
            this._voltageDefensesReturnTimeCol3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._voltageDefensesReturnTimeCol3.Width = 50;
            // 
            // _voltageDefensesUROVcol3
            // 
            this._voltageDefensesUROVcol3.HeaderText = "����";
            this._voltageDefensesUROVcol3.Name = "_voltageDefensesUROVcol3";
            this._voltageDefensesUROVcol3.Width = 43;
            // 
            // _voltageDefensesAPVcol3
            // 
            this._voltageDefensesAPVcol3.HeaderText = "���";
            this._voltageDefensesAPVcol3.Name = "_voltageDefensesAPVcol3";
            this._voltageDefensesAPVcol3.Width = 35;
            // 
            // _voltageDefensesAVRcol3
            // 
            this._voltageDefensesAVRcol3.HeaderText = "���";
            this._voltageDefensesAVRcol3.Name = "_voltageDefensesAVRcol3";
            this._voltageDefensesAVRcol3.Width = 34;
            // 
            // _voltageDefensesOSCv11Col3
            // 
            this._voltageDefensesOSCv11Col3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._voltageDefensesOSCv11Col3.HeaderText = "�����������";
            this._voltageDefensesOSCv11Col3.Name = "_voltageDefensesOSCv11Col3";
            this._voltageDefensesOSCv11Col3.Width = 82;
            // 
            // _voltageDefensesResetcol3
            // 
            this._voltageDefensesResetcol3.HeaderText = "�����";
            this._voltageDefensesResetcol3.Name = "_voltageDefensesResetcol3";
            this._voltageDefensesResetcol3.Width = 50;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Column8";
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            this.Column8.Width = 54;
            // 
            // _frequencyDefendTabPage
            // 
            this._frequencyDefendTabPage.Controls.Add(this._frequenceDefensesGrid);
            this._frequencyDefendTabPage.Location = new System.Drawing.Point(4, 22);
            this._frequencyDefendTabPage.Name = "_frequencyDefendTabPage";
            this._frequencyDefendTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._frequencyDefendTabPage.Size = new System.Drawing.Size(993, 558);
            this._frequencyDefendTabPage.TabIndex = 2;
            this._frequencyDefendTabPage.Text = "������ �� �������";
            this._frequencyDefendTabPage.UseVisualStyleBackColor = true;
            // 
            // _frequenceDefensesGrid
            // 
            this._frequenceDefensesGrid.AllowUserToAddRows = false;
            this._frequenceDefensesGrid.AllowUserToDeleteRows = false;
            this._frequenceDefensesGrid.AllowUserToResizeColumns = false;
            this._frequenceDefensesGrid.AllowUserToResizeRows = false;
            this._frequenceDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._frequenceDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle281.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle281.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle281.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle281.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle281.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle281.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle281.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle281;
            this._frequenceDefensesGrid.ColumnHeadersHeight = 30;
            this._frequenceDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._frequenceDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._frequenceDefensesName,
            this._frequenceDefensesMode,
            this._frequenceDefensesBlockingNumber,
            this._frequenceDefensesWorkConstraint,
            this._frequenceDefensesWorkTime,
            this._frequenceDefensesReturn,
            this._frequenceDefensesAPVReturn,
            this._frequenceDefensesConstraintAPV,
            this._frequenceDefensesReturnTime,
            this._frequenceDefensesUROV,
            this._frequenceDefensesAPV,
            this._frequenceDefensesAVR,
            this._frequenceDefensesOSCv11,
            this._frequenceDefensesReset});
            this._frequenceDefensesGrid.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle287.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle287.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle287.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle287.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle287.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle287.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle287.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesGrid.DefaultCellStyle = dataGridViewCellStyle287;
            this._frequenceDefensesGrid.Location = new System.Drawing.Point(3, 6);
            this._frequenceDefensesGrid.MultiSelect = false;
            this._frequenceDefensesGrid.Name = "_frequenceDefensesGrid";
            dataGridViewCellStyle288.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle288.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle288.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle288.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle288.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle288.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle288.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._frequenceDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle288;
            this._frequenceDefensesGrid.RowHeadersVisible = false;
            this._frequenceDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._frequenceDefensesGrid.RowTemplate.Height = 24;
            this._frequenceDefensesGrid.Size = new System.Drawing.Size(987, 128);
            this._frequenceDefensesGrid.TabIndex = 6;
            // 
            // _frequenceDefensesName
            // 
            this._frequenceDefensesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle282.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._frequenceDefensesName.DefaultCellStyle = dataGridViewCellStyle282;
            this._frequenceDefensesName.Frozen = true;
            this._frequenceDefensesName.HeaderText = "";
            this._frequenceDefensesName.MaxInputLength = 1;
            this._frequenceDefensesName.Name = "_frequenceDefensesName";
            this._frequenceDefensesName.ReadOnly = true;
            this._frequenceDefensesName.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesName.Width = 40;
            // 
            // _frequenceDefensesMode
            // 
            this._frequenceDefensesMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesMode.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._frequenceDefensesMode.HeaderText = "�����";
            this._frequenceDefensesMode.Name = "_frequenceDefensesMode";
            this._frequenceDefensesMode.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesMode.Width = 48;
            // 
            // _frequenceDefensesBlockingNumber
            // 
            this._frequenceDefensesBlockingNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesBlockingNumber.HeaderText = "����������";
            this._frequenceDefensesBlockingNumber.Name = "_frequenceDefensesBlockingNumber";
            this._frequenceDefensesBlockingNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesBlockingNumber.Width = 74;
            // 
            // _frequenceDefensesWorkConstraint
            // 
            dataGridViewCellStyle283.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesWorkConstraint.DefaultCellStyle = dataGridViewCellStyle283;
            this._frequenceDefensesWorkConstraint.HeaderText = "F��, ��";
            this._frequenceDefensesWorkConstraint.Name = "_frequenceDefensesWorkConstraint";
            this._frequenceDefensesWorkConstraint.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkConstraint.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkConstraint.Width = 50;
            // 
            // _frequenceDefensesWorkTime
            // 
            dataGridViewCellStyle284.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle284.NullValue = "0";
            this._frequenceDefensesWorkTime.DefaultCellStyle = dataGridViewCellStyle284;
            this._frequenceDefensesWorkTime.HeaderText = "t��, ��";
            this._frequenceDefensesWorkTime.Name = "_frequenceDefensesWorkTime";
            this._frequenceDefensesWorkTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesWorkTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesWorkTime.Width = 50;
            // 
            // _frequenceDefensesReturn
            // 
            this._frequenceDefensesReturn.HeaderText = "����.";
            this._frequenceDefensesReturn.Name = "_frequenceDefensesReturn";
            this._frequenceDefensesReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturn.Width = 41;
            // 
            // _frequenceDefensesAPVReturn
            // 
            this._frequenceDefensesAPVReturn.HeaderText = "��� ��";
            this._frequenceDefensesAPVReturn.Name = "_frequenceDefensesAPVReturn";
            this._frequenceDefensesAPVReturn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPVReturn.Width = 52;
            // 
            // _frequenceDefensesConstraintAPV
            // 
            dataGridViewCellStyle285.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesConstraintAPV.DefaultCellStyle = dataGridViewCellStyle285;
            this._frequenceDefensesConstraintAPV.HeaderText = "F��, ��";
            this._frequenceDefensesConstraintAPV.Name = "_frequenceDefensesConstraintAPV";
            this._frequenceDefensesConstraintAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesConstraintAPV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesConstraintAPV.Width = 50;
            // 
            // _frequenceDefensesReturnTime
            // 
            dataGridViewCellStyle286.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._frequenceDefensesReturnTime.DefaultCellStyle = dataGridViewCellStyle286;
            this._frequenceDefensesReturnTime.HeaderText = "t��, ��";
            this._frequenceDefensesReturnTime.Name = "_frequenceDefensesReturnTime";
            this._frequenceDefensesReturnTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesReturnTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._frequenceDefensesReturnTime.Width = 50;
            // 
            // _frequenceDefensesUROV
            // 
            this._frequenceDefensesUROV.HeaderText = "����";
            this._frequenceDefensesUROV.Name = "_frequenceDefensesUROV";
            this._frequenceDefensesUROV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesUROV.Width = 43;
            // 
            // _frequenceDefensesAPV
            // 
            this._frequenceDefensesAPV.HeaderText = "���";
            this._frequenceDefensesAPV.Name = "_frequenceDefensesAPV";
            this._frequenceDefensesAPV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAPV.Width = 35;
            // 
            // _frequenceDefensesAVR
            // 
            this._frequenceDefensesAVR.HeaderText = "���";
            this._frequenceDefensesAVR.Name = "_frequenceDefensesAVR";
            this._frequenceDefensesAVR.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._frequenceDefensesAVR.Width = 34;
            // 
            // _frequenceDefensesOSCv11
            // 
            this._frequenceDefensesOSCv11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._frequenceDefensesOSCv11.HeaderText = "�����������";
            this._frequenceDefensesOSCv11.Name = "_frequenceDefensesOSCv11";
            this._frequenceDefensesOSCv11.Width = 82;
            // 
            // _frequenceDefensesReset
            // 
            this._frequenceDefensesReset.HeaderText = "�����";
            this._frequenceDefensesReset.Name = "_frequenceDefensesReset";
            this._frequenceDefensesReset.Width = 44;
            // 
            // OldCopyGroupBox
            // 
            this.OldCopyGroupBox.Controls.Add(this._reserveRadioButtonGroup);
            this.OldCopyGroupBox.Controls.Add(this._mainRadioButtonGroup);
            this.OldCopyGroupBox.Controls.Add(this._ChangeGroupButton2);
            this.OldCopyGroupBox.Location = new System.Drawing.Point(9, 7);
            this.OldCopyGroupBox.Name = "OldCopyGroupBox";
            this.OldCopyGroupBox.Size = new System.Drawing.Size(382, 51);
            this.OldCopyGroupBox.TabIndex = 0;
            this.OldCopyGroupBox.TabStop = false;
            this.OldCopyGroupBox.Text = "������ �������";
            // 
            // _reserveRadioButtonGroup
            // 
            this._reserveRadioButtonGroup.AutoSize = true;
            this._reserveRadioButtonGroup.Location = new System.Drawing.Point(99, 20);
            this._reserveRadioButtonGroup.Name = "_reserveRadioButtonGroup";
            this._reserveRadioButtonGroup.Size = new System.Drawing.Size(82, 17);
            this._reserveRadioButtonGroup.TabIndex = 2;
            this._reserveRadioButtonGroup.Text = "���������";
            this._reserveRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _mainRadioButtonGroup
            // 
            this._mainRadioButtonGroup.AutoSize = true;
            this._mainRadioButtonGroup.Checked = true;
            this._mainRadioButtonGroup.Location = new System.Drawing.Point(16, 20);
            this._mainRadioButtonGroup.Name = "_mainRadioButtonGroup";
            this._mainRadioButtonGroup.Size = new System.Drawing.Size(77, 17);
            this._mainRadioButtonGroup.TabIndex = 1;
            this._mainRadioButtonGroup.TabStop = true;
            this._mainRadioButtonGroup.Text = "��������";
            this._mainRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _ChangeGroupButton2
            // 
            this._ChangeGroupButton2.Location = new System.Drawing.Point(187, 17);
            this._ChangeGroupButton2.Name = "_ChangeGroupButton2";
            this._ChangeGroupButton2.Size = new System.Drawing.Size(192, 23);
            this._ChangeGroupButton2.TabIndex = 0;
            this._ChangeGroupButton2.Text = "�������� --> ���������";
            this._ChangeGroupButton2.UseVisualStyleBackColor = true;
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��5 v70";
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��5 v70 �������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��5 v70";
            // 
            // Mr5V70ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1021, 750);
            this.Controls.Add(this.mainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1015, 690);
            this.Name = "Mr5V70ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationFormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConfigurationForm_KeyUp);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.gb1.ResumeLayout(false);
            this.gb1.PerformLayout();
            this.gb2.ResumeLayout(false);
            this.gb3.ResumeLayout(false);
            this.gb3.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.NewCopyGroupBox.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this._tokDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid4)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tokDefenseGrid1)).EndInit();
            this._voltageDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._voltageDefensesGrid3)).EndInit();
            this._frequencyDefendTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._frequenceDefensesGrid)).EndInit();
            this.OldCopyGroupBox.ResumeLayout(false);
            this.OldCopyGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button _resetConfigBut;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.CheckedListBox _keysCheckedListBox;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals9;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.GroupBox gb3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox oscPercent;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.ComboBox oscFix;
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.ComboBox oscLengthMode;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.CheckBox _ompCheckBox;
        private System.Windows.Forms.MaskedTextBox _HUD_Box;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _TN_typeCombo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox _TNNP_dispepairCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox _TN_dispepairCombo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox _TN_Box;
        private System.Windows.Forms.MaskedTextBox _TNNP_Box;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _TT_typeCombo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox avr_time_abrasion;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox avr_return;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox avr_abrasion;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.CheckBox avr_permit_reset_switch;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox avr_abrasion_switch;
        private System.Windows.Forms.CheckBox avr_supply_off;
        private System.Windows.Forms.CheckBox avr_self_off;
        private System.Windows.Forms.CheckBox avr_switch_off;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.ComboBox avr_start;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox _apvStartCheckBox;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage _tokDefendTabPage;
        private System.Windows.Forms.DataGridView _tokDefenseGrid3;
        private System.Windows.Forms.DataGridView _tokDefenseGrid4;
        private System.Windows.Forms.DataGridView _tokDefenseGrid2;
        private System.Windows.Forms.DataGridView _tokDefenseGrid1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.MaskedTextBox _tokDefenseInbox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI2box;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.MaskedTextBox _tokDefenseI0box;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MaskedTextBox _tokDefenseIbox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage _voltageDefendTabPage;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid11;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid2;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid1;
        private System.Windows.Forms.DataGridView _voltageDefensesGrid3;
        private System.Windows.Forms.TabPage _frequencyDefendTabPage;
        private System.Windows.Forms.DataGridView _frequenceDefensesGrid;
        private System.Windows.Forms.GroupBox OldCopyGroupBox;
        private System.Windows.Forms.RadioButton _reserveRadioButtonGroup;
        private System.Windows.Forms.RadioButton _mainRadioButtonGroup;
        private System.Windows.Forms.Button _ChangeGroupButton2;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.MaskedTextBox oscLenText;
        private System.Windows.Forms.ComboBox _blockSDTU;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefOSCcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefResetcol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckedListBox _disrepairCheckList;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.DataGridView _tokDefenseGrid5;
        private System.Windows.Forms.GroupBox NewCopyGroupBox;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Button copyBtn;
        private System.Windows.Forms.ComboBox copyGroupsCombo;
        private System.Windows.Forms.ComboBox currentGroupCombo;
        private System.Windows.Forms.ComboBox _constraintGroup4Combo;
        private System.Windows.Forms.Label group4label;
        private System.Windows.Forms.ComboBox _constraintGroup3Combo;
        private System.Windows.Forms.Label group3label;
        private System.Windows.Forms.ComboBox _constraintGroup2Combo;
        private System.Windows.Forms.Label group2label;
        private System.Windows.Forms.ComboBox _constraintGroup1Combo;
        private System.Windows.Forms.Label group1label;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4PuskConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4AVRCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4OSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UpuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3PuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3DirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3AVRCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3OSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseU_PuskCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefensePuskConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseDirectionCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockExistCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAVRCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseOSCv11Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSC�11Col2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVReturlCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRCol1;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetCol1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _u5vColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesNameCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesModeCol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesBlockingNumberCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesParameterCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesWorkConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesTimeConstraintCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesReturnCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVreturnCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnConstraintCol3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _voltageDefensesReturnTimeCol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesUROVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAPVcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesAVRcol3;
        private System.Windows.Forms.DataGridViewComboBoxColumn _voltageDefensesOSCv11Col3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _voltageDefensesResetcol3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesName;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesMode;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesBlockingNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkConstraint;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesWorkTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReturn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesConstraintAPV;
        private System.Windows.Forms.DataGridViewTextBoxColumn _frequenceDefensesReturnTime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesUROV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAPV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesAVR;
        private System.Windows.Forms.DataGridViewComboBoxColumn _frequenceDefensesOSCv11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _frequenceDefensesReset;
    }
}