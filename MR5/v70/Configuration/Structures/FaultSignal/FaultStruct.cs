﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR5.v70.Configuration.Structures.FaultSignal
{
    public class FaultStruct : StructBase
    {
        [Layout(0)] private FaultSignalStruct _signals;
        [Layout(1)] private ushort _time;
        [Layout(2, Count = 3)] private ushort[] _res;

        [BindingProperty(0)]
        public FaultSignalStruct Signals
        {
            get { return this._signals; }
            set { this._signals = value; }
        }
        [XmlAttribute(AttributeName = "Импульс_реле_неисправность")]
        [BindingProperty(1)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time) ; }
            set { this._time =ValuesConverterCommon.SetWaitTime(value) ; }
        }
    }
}