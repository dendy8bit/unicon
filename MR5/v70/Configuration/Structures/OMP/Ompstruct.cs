﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR5.v70.Configuration.Structures.OMP
{
    public class OmpStruct : StructBase
    {
        [Layout(0)] private ushort _opmMode;
        [Layout(1)] private ushort _rOpm;

        [XmlElement(ElementName = "ОМП")]
        [BindingProperty(0)]
        public bool Omp
        {
            get { return Common.GetBit(this._opmMode, 0); }
            set { this._opmMode = Common.SetBit(this._opmMode, 0, value); }
        }

        [XmlElement(ElementName = "Xyd")]
        [BindingProperty(1)]
        public double Xyd
        {
            get { return this._rOpm / 1000.0; }
            set { this._rOpm = (ushort)(value * 1000); }
        }
    }
}
