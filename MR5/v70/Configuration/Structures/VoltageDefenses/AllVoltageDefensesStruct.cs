﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v70.Configuration.Structures.VoltageDefenses
{
    public class AllVoltageDefensesStruct : StructBase, IDgvRowsContainer<VoltageDefenseStruct>
    {
        [Layout(0, Count = 8)]
        private VoltageDefenseStruct[] _externalDefenses;



        public VoltageDefenseStruct[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
