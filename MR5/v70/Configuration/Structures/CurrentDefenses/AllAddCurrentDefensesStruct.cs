﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v70.Configuration.Structures.CurrentDefenses
{
    public class AllAddCurrentDefensesStruct : StructBase, IDgvRowsContainer<AddCurrentDefenseStruct>
    {
        [Layout(0, Count = 2)] private AddCurrentDefenseStruct[] _addCurrentDefenses;

        public AddCurrentDefenseStruct[] Rows
        {
            get
            {
                this._addCurrentDefenses[0].Index = 0;
                this._addCurrentDefenses[1].Index = 1;
                return this._addCurrentDefenses;
            }
            set
            {
                this._addCurrentDefenses = value;
                this._addCurrentDefenses[0].Index = 0;
                this._addCurrentDefenses[1].Index = 1;
            }
        }
    }
}
