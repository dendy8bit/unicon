﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v70.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesOtherStruct : StructBase, IDgvRowsContainer<CurrentDefenseOtherStruct>
    {
        [Layout(0, Count = 6)]
        private CurrentDefenseOtherStruct[] _currentDefenses;

        public CurrentDefenseOtherStruct[] Rows
        {
            get
            {
                for (int i = 0; i < this._currentDefenses.Length; i++)
                {
                    this._currentDefenses[i].In = i > 3;
                }
                return this._currentDefenses;
            }
            set
            {
                this._currentDefenses = value;
                for (int i = 0; i < this._currentDefenses.Length; i++)
                {
                    this._currentDefenses[i].In = i > 3;
                }
            }
        }
    }
}
