﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v70.Configuration.Structures.CurrentDefenses
{
    public class AddCurrentDefenseStruct : StructBase
    {
        public int Index;
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _uUst; //U ustavka
        [Layout(5)] private ushort _u; //уставка возврата
        [Layout(6)] private ushort _res1; 
        [Layout(7)] private ushort _res2; 

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.Modes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.Modes, this._config, 0, 1); }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.LogicSignals); }
            set { this._block = Validator.Set(value, StringsConfig.LogicSignals); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Пуск_по_U")]
        public bool Ustart
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Uпуск")]
        public double Uustavka
        {
            get { return ValuesConverterCommon.GetU(this._uUst); }
            set { this._uUst = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Уставка в In
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Уставка_In")]
        public double Srab5
        {
            get { return ValuesConverterCommon.GetUstavka5(this._srab); }
            set { this._srab = ValuesConverterCommon.SetUstavka5(value); }
        }

        /// <summary>
        /// Уставка в In
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Уставка")]
        public double Srab100
        {
            get { return ValuesConverterCommon.GetUstavka100(this._srab); }
            set { this._srab = ValuesConverterCommon.SetUstavka100(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "время_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ust); }
            set { this._ust = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Speedup
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Уставка_ускорения")]
        public int Tspeedup
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }
        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }
        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {

            get { return Validator.Get(this._config, StringsConfig.OscModes, 3, 4); }
            set { this._config = Validator.Set(value, StringsConfig.OscModes, this._config, 3, 4); }
        }
    }
}
