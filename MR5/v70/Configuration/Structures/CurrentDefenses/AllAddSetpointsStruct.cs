﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR5.v70.Configuration.Structures.CurrentDefenses
{
    public class AllAddSetpointsStruct : StructBase, ISetpointContainer<AllAddCurrentDefensesStruct>
    {
        [Layout(0)] public AllAddCurrentDefensesStruct _addCurrentDefenses1;
        [Layout(1, Count = 16)] private ushort[] _res1;
        [Layout(2)] public AllAddCurrentDefensesStruct _addCurrentDefenses2;
        [Layout(3, Count = 16)] private ushort[] _res2;

        [XmlIgnore]
        public AllAddCurrentDefensesStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        this._addCurrentDefenses1.Clone<AllAddCurrentDefensesStruct>(),
                        this._addCurrentDefenses2.Clone<AllAddCurrentDefensesStruct>()
                    };
            }
            set
            {
                this._addCurrentDefenses1 = value[0];
                this._addCurrentDefenses2 = value[1];
            }
        }

        [XmlElement(ElementName = "Основная")]
        public AllAddCurrentDefensesStruct Main
        {
            get { return this._addCurrentDefenses1; }
            set { this._addCurrentDefenses1 = value; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllAddCurrentDefensesStruct Reserve
        {
            get { return this._addCurrentDefenses2; }
            set { this._addCurrentDefenses2 = value; }
        }
    }
}
