﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR5.v70.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {
        [Layout(0)] private CornerStruct _corner1;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses1;
        [Layout(2)] private AllCurrentDefensesOtherStruct _currentOtherDefenses1;
        [Layout(3)] private CornerStruct _corner2;
        [Layout(4)] private AllCurrentDefensesStruct _currentDefense2;
        [Layout(5)] private AllCurrentDefensesOtherStruct _currentOtherDefenses2;

        [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                var result = new[]
                {
                    new SetpointStruct
                    {
                        Corners = this._corner1.Clone<CornerStruct>(),
                        CurrentDefenses = this._currentDefenses1.Clone<AllCurrentDefensesStruct>(),
                        CurrentDefensesOther = this._currentOtherDefenses1.Clone<AllCurrentDefensesOtherStruct>()
                    },
                    new SetpointStruct
                    {
                        Corners = this._corner2.Clone<CornerStruct>(),
                        CurrentDefenses = this._currentDefense2.Clone<AllCurrentDefensesStruct>(),
                        CurrentDefensesOther = this._currentOtherDefenses2.Clone<AllCurrentDefensesOtherStruct>()
                    }
                };

                return result;
            }
            set
            {
                this._corner1 = value[0].Corners;
                this._currentDefenses1 = value[0].CurrentDefenses;
                this._currentOtherDefenses1 = value[0].CurrentDefensesOther;

                this._corner2 = value[1].Corners;
                this._currentDefense2 = value[1].CurrentDefenses;
                this._currentOtherDefenses2 = value[1].CurrentDefensesOther;
            }
        }

        [XmlElement(ElementName = "Основная")]
        public SetpointStruct Main
        {
            get
            {
                return new SetpointStruct
                {
                    Corners = this._corner1.Clone<CornerStruct>(),
                    CurrentDefenses = this._currentDefenses1.Clone<AllCurrentDefensesStruct>(),
                    CurrentDefensesOther = this._currentOtherDefenses1.Clone<AllCurrentDefensesOtherStruct>()
                };
            }
            set
            {
                this._corner1 = value.Corners;
                this._currentDefenses1 = value.CurrentDefenses;
                this._currentOtherDefenses1 = value.CurrentDefensesOther;
            }
        }

        [XmlElement(ElementName = "Резервная")]
        public SetpointStruct Reserv
        {
            get
            {
                return new SetpointStruct
                {
                    Corners = this._corner2.Clone<CornerStruct>(),
                    CurrentDefenses = this._currentDefense2.Clone<AllCurrentDefensesStruct>(),
                    CurrentDefensesOther = this._currentOtherDefenses2.Clone<AllCurrentDefensesOtherStruct>()
                };
            }
            set
            {
                this._corner2 = value.Corners;
                this._currentDefense2 = value.CurrentDefenses;
                this._currentOtherDefenses2 = value.CurrentDefensesOther;
            }
        }
    }
}
