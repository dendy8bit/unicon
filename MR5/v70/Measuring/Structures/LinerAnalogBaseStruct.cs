﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v70.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v70.Measuring.Structures
{
    public class LinerAnalogBaseStruct : StructBase
    {
        [Layout(0)] private int _uab; // ток N
        [Layout(1)] private int _ubc; // ток A
        [Layout(2)] private int _uca; // ток B

        private int GetMean(List<LinerAnalogBaseStruct> list, Func<LinerAnalogBaseStruct, int> func)
        {
            int count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (int)(sum / (double)count);
        }

        public string GetUab(List<LinerAnalogBaseStruct> list, MeasureTransStructV70 measure)
        {
            int value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU801(value, measure.Tn);
        }

        public string GetUbc(List<LinerAnalogBaseStruct> list, MeasureTransStructV70 measure)
        {
            int value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU801(value, measure.Tn);
        }

        public string GetUca(List<LinerAnalogBaseStruct> list, MeasureTransStructV70 measure)
        {
            int value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU801(value, measure.Tn);
        }

    }
}
