﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v70.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v70.Measuring.Structures
{
    /// <summary>
    /// МР 550 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStructV70 :StructBase
    {
        #region [Private fields]
        
        [Layout(0)] private ushort _in; // ток N
        [Layout(1)] private ushort _ia; // ток A
        [Layout(2)] private ushort _ib; // ток B
        [Layout(3)] private ushort _ic; // ток C
        [Layout(4)] private ushort _i0; // ток 0
        [Layout(5)] private ushort _i1; // ток 2
        [Layout(6)] private ushort _i2; // ток 2
        [Layout(7)] private ushort _ig; // ток Iг
        [Layout(8)] private ushort _un;
        [Layout(9)] private ushort _ua;
        [Layout(10)] private ushort _ub;
        [Layout(11)] private ushort _uc;
        [Layout(12)] private ushort _uab;
        [Layout(13)] private ushort _ubc;
        [Layout(14)] private ushort _uca;
        [Layout(15)] private ushort _u0;
        [Layout(16)] private ushort _u1;
        [Layout(17)] private ushort _u2;
        [Layout(18)] private ushort _f;
        [Layout(19)] private ushort _cos;
        [Layout(20)] private ushort _p;
        [Layout(21)] private ushort _q;
        
        #endregion [Private fields]
        
        #region [Public members]

        private ushort GetMean(List<AnalogDataBaseStructV70> list,Func<AnalogDataBaseStructV70, ushort> func)
        {
            int count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort) (sum/(double) count);
        }

        private int GetMean(List<AnalogDataBaseStructV70> list, Func<AnalogDataBaseStructV70, int> func)
        {
            int count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (int)(sum / (double)count);
        }

        public string GetIa(List<AnalogDataBaseStructV70> list,MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIb(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIc(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI1(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI2(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI0(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIn(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }
        public string GetIg(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }

        public string GetCosF(List<AnalogDataBaseStructV70> list)
        {
            ushort value = this.GetMean(list, o => o._cos);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            var value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQMr575(value, 1000);
            /*
            ushort value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQMr575(value, measure.Tt * measure.Tn);
            */
        }

        public string GetP(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            var value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetPMr575(value, 1000);
            /*
            ushort value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.Tt * measure.Tn);
            */
        }

        public string GetF(List<AnalogDataBaseStructV70> list)
        {
            ushort value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF(value);
        }
        public string GetUa(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUb(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUc(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUab(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUbc(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUca(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetU1(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetU2(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetU0(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._u0);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUn(List<AnalogDataBaseStructV70> list, MeasureTransStructV70 measure)
        {
            ushort value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tnnp);
        } 

     
        #endregion [Public members]
        

    }
}
