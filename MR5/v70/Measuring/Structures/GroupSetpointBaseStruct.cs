﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR5.v70.Configuration;

namespace BEMN.MR5.v70.Measuring.Structures
{
    public class GroupSetpointBaseStructV70 : StructBase
    {
        [Layout(0)] private ushort _group;

        public string GetGroup(double version)
        {
            if (version > 70.06)
            {
                return Validator.Get(this._group, StringsConfig.Groups);
            }
            return Validator.Get(this._group, StringsConfig.SetpointsNamesV1);
        }

        public void SetGroup(double version, string value)
        {
            this._group = Validator.Set(value, version > 70.06 ? StringsConfig.Groups : StringsConfig.SetpointsNamesV1);
        }
    }
}
