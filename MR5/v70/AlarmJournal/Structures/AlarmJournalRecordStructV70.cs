﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR5.v70.Configuration;
using BEMN.MR5.v70.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v70.AlarmJournal.Structures
{
    public class AlarmJournalRecordStructV70 : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]
        
        #region [Private fields]

        [Layout(0)] private ushort _message; //1
        [Layout(1)] private ushort _year; //2
        [Layout(2)] private ushort _month; //3
        [Layout(3)] private ushort _date; //4
        [Layout(4)] private ushort _hour; //5
        [Layout(5)] private ushort _minute; //6
        [Layout(6)] private ushort _second; //7
        [Layout(7)] private ushort _millisecond; //8

        [Layout(8)] private ushort _stage; // 2 Код повреждения** 9
        [Layout(9)] private ushort _type; //3 Тип повреждения*** 10
        [Layout(10)] private ushort _value; //4 Значение повреждения //Знач. сраб пар 11
        [Layout(11)] private ushort _ia; //5 Значение  //F 12
        [Layout(12)] private ushort _ib; //6 Значение   //Ua 13 
        [Layout(13)] private ushort _ic; //7 Значение   //Ub 14 
        [Layout(14)] private ushort _i0; //8 Значение //Uc 15
        [Layout(15)] private ushort _i1; //9 Значение //Uab 16
        [Layout(16)] private ushort _i2; //10 Значение  //Ubc 17 
        [Layout(17)] private ushort _in; //11 Значение //Uca 18
        [Layout(18)] private ushort _ig; //12 Значение //U0 19
        [Layout(19)] private ushort _f; //13 Значение //U1 20
        [Layout(20)] private ushort _uab; //14 Значение //U2 21
        [Layout(21)] private ushort _ubc; //15 Значение //Un 22
        [Layout(22)] private ushort _uca; //17 рез 24
        [Layout(23)] private ushort _u0; //16 Значение //Дискрет 23
        [Layout(24)] private ushort _u1; //18 рез 25
        [Layout(25)] private ushort _u2; //19 рез 26
        [Layout(26)] private ushort _un; //20 рез 27
        [Layout(27)] private ushort _discrets; //21 рез 28 

        #endregion [Private fields]

        #region [Properties]
        public bool IsEmpty
        {
            get
            {
                return (this._date + this._month + this._year + this._hour + this._minute + this._second +
                        this._millisecond + this._stage + this._type + this._value) == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        public string TypeDamage
        {
            get
            {
                string res = string.Empty;
                res +=   Common.GetBit(this._type, 3) ? "_" : "";
                res += Common.GetBit(this._type, 0) ? "A" : "";
                res += Common.GetBit(this._type, 1) ? "B" : "";
                res += Common.GetBit(this._type, 2) ? "C" : "";
                return res;
            }
        }

        public string Stage
        {
            get { return Validator.Get(this._stage,AjStrings.Stage,0,1,2,3,4,5) ; }
        }

        public string GroupOfSetpoints(double version)
        {
            return version > 70.06
                ? Validator.GetJornal(this._stage, StringsConfig.Groups, 8, 9, 10)
                : Validator.GetJornal(this._stage, StringsConfig.SetpointsNamesV1, 7);
            //return Validator.GetJornal(this._stage, StringsConfig.SetpointsNamesV1, 8, 9, 10);
        }
        public string WorkParametr
        {
            get { return Validator.Get(this._type, AjStrings.Parametr, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        public string ValueParametr(MeasureTransStructV70 measure)
        {
            var index = Common.GetBits(this._type, 8, 9, 10, 11, 12, 13, 14, 15) >> 8;
            
            //Iг,In, Iем, Iак
            if (index == 1||index == 2|| index == 29 || index == 30)
            {
                return ValuesConverterCommon.Analog.GetI(this._value, measure.Ttnp * 5);
            }
            //Ia,Ib... I2
            if (index >= 3&&index <= 8)
            {
                return ValuesConverterCommon.Analog.GetISmall(this._value, measure.Tt*40);
            }
            //Pn
            if (index == 9) 
            {
                return ValuesConverterCommon.Analog.GetP(this._value, 5);
            }

            //Pa...P2
            if ((index >= 10) & (index <= 15))
            {
                return ValuesConverterCommon.Analog.GetP(this._value,  40);
            }
            //F
            if (index == 16)
            {
                return ValuesConverterCommon.Analog.GetF(this._value);
            }
            //Un
            if (index == 17)
            {
                return ValuesConverterCommon.Analog.GetU(this._value, measure.Tnnp);
            }
            //Ua,Ub... U2
            if ((index >= 18) & (index <= 26))
            {
                return ValuesConverterCommon.Analog.GetU(this._value, measure.Tn);
            }
            if (index == 27) 
            {
                return ValuesConverterCommon.Analog.GetISmall(this._value, 100).Replace("А", "%");
            }
            if (index == 28)
            {
                return ValuesConverterCommon.Analog.GetISmall(this._value, 256).Replace("А", "км");
            }
                return string.Empty;
            
        }

        public ushort Value
        {
            get { return this._value; }
        }

        public ushort Ia
        {
            get { return this._ia; }
        }

        public ushort Ib
        {
            get { return this._ib; }
        }

        public ushort Ic
        {
            get { return this._ic; }
        }

        public ushort I0
        {
            get { return this._i0; }
        }

        public ushort I1
        {
            get { return this._i1; }
        }

        public ushort I2
        {
            get { return this._i2; }
        }

        public ushort In
        {
            get { return this._in; }
        }

        public ushort Ig
        {
            get { return this._ig; }
        }

        public ushort F
        {
            get { return this._f; }
        }

        public ushort Uab
        {
            get { return this._uab; }
        }

        public ushort Ubc
        {
            get { return this._ubc; }
        }

        public string Discret
        {
            get { return  "  " + this.GetMask(this._discrets); }
        }

        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(ushort value)
        {
            char[] chars = Convert.ToString(value, 2).PadLeft(16, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }
        
        public string Message
        {
            get { return Validator.Get(this._message, AjStrings.Message); }
        }

        public ushort Uca
        {
            get { return this._uca; }
        }

        public ushort U0
        {
            get { return this._u0; }
        }

        public ushort U1
        {
            get { return this._u1; }
        }

        public ushort U2
        {
            get { return this._u2; }
        }

        public ushort Un
        {
            get { return this._un; }
        }

        #endregion [Properties]
    }
}
