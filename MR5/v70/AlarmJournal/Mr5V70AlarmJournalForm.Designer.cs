﻿namespace BEMN.MR5.v70.AlarmJournal
{
    partial class Mr5V70AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stageCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeDamage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._trigParamCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._valueCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uabColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ubcColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ucaColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u0Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._u2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._unColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._DCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._configProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(1, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(103, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this.Start_Click);
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._stageCol,
            this._typeDamage,
            this._trigParamCol,
            this._valueCol,
            this._groupCol,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I0Col,
            this._I1Col,
            this._I2Col,
            this._InCol,
            this._IgCol,
            this._fColumn,
            this._uabColumn,
            this._ubcColumn,
            this._ucaColumn,
            this._u0Column,
            this._u1Column,
            this._u2Column,
            this._unColumn,
            this._DCol});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(717, 528);
            this._alarmJournalGrid.TabIndex = 19;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "№";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Дата/Время";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "Сообщение";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle1;
            this._msgCol.HeaderText = "Сообщение";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _stageCol
            // 
            this._stageCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._stageCol.DataPropertyName = "Ступень";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._stageCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._stageCol.HeaderText = "Ступень";
            this._stageCol.Name = "_stageCol";
            this._stageCol.ReadOnly = true;
            this._stageCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._stageCol.Width = 120;
            // 
            // _typeDamage
            // 
            this._typeDamage.DataPropertyName = "Тип повреждения";
            this._typeDamage.HeaderText = "Тип повреждения";
            this._typeDamage.Name = "_typeDamage";
            this._typeDamage.ReadOnly = true;
            this._typeDamage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeDamage.Width = 93;
            // 
            // _trigParamCol
            // 
            this._trigParamCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._trigParamCol.DataPropertyName = "Параметр срабатывания";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._trigParamCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._trigParamCol.HeaderText = "Параметр срабатывания";
            this._trigParamCol.Name = "_trigParamCol";
            this._trigParamCol.ReadOnly = true;
            this._trigParamCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _valueCol
            // 
            this._valueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._valueCol.DataPropertyName = "Значение параметра срабатывания";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._valueCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._valueCol.HeaderText = "Значение параметра срабатывания";
            this._valueCol.Name = "_valueCol";
            this._valueCol.ReadOnly = true;
            this._valueCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._groupCol.DataPropertyName = "Группа уставок";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IaCol
            // 
            this._IaCol.DataPropertyName = "Ia";
            this._IaCol.HeaderText = "Ia";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 22;
            // 
            // _IbCol
            // 
            this._IbCol.DataPropertyName = "Ib";
            this._IbCol.HeaderText = "Ib";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 22;
            // 
            // _IcCol
            // 
            this._IcCol.DataPropertyName = "Ic";
            this._IcCol.HeaderText = "Ic";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 22;
            // 
            // _I0Col
            // 
            this._I0Col.DataPropertyName = "I0";
            this._I0Col.HeaderText = "I0";
            this._I0Col.Name = "_I0Col";
            this._I0Col.ReadOnly = true;
            this._I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I0Col.Width = 22;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "I1";
            this._I1Col.HeaderText = "I1";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 22;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "I2";
            this._I2Col.HeaderText = "I2";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 22;
            // 
            // _InCol
            // 
            this._InCol.DataPropertyName = "In";
            this._InCol.HeaderText = "In";
            this._InCol.Name = "_InCol";
            this._InCol.ReadOnly = true;
            this._InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InCol.Width = 22;
            // 
            // _IgCol
            // 
            this._IgCol.DataPropertyName = "Iг";
            this._IgCol.HeaderText = "Iг";
            this._IgCol.Name = "_IgCol";
            this._IgCol.ReadOnly = true;
            this._IgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IgCol.Width = 21;
            // 
            // _fColumn
            // 
            this._fColumn.DataPropertyName = "F";
            this._fColumn.HeaderText = "F";
            this._fColumn.Name = "_fColumn";
            this._fColumn.ReadOnly = true;
            this._fColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fColumn.Width = 19;
            // 
            // _uabColumn
            // 
            this._uabColumn.DataPropertyName = "Uab";
            this._uabColumn.HeaderText = "Uab";
            this._uabColumn.Name = "_uabColumn";
            this._uabColumn.ReadOnly = true;
            this._uabColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uabColumn.Width = 33;
            // 
            // _ubcColumn
            // 
            this._ubcColumn.DataPropertyName = "Ubc";
            this._ubcColumn.HeaderText = "Ubc";
            this._ubcColumn.Name = "_ubcColumn";
            this._ubcColumn.ReadOnly = true;
            this._ubcColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ubcColumn.Width = 33;
            // 
            // _ucaColumn
            // 
            this._ucaColumn.DataPropertyName = "Uca";
            this._ucaColumn.HeaderText = "Uca";
            this._ucaColumn.Name = "_ucaColumn";
            this._ucaColumn.ReadOnly = true;
            this._ucaColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ucaColumn.Width = 33;
            // 
            // _u0Column
            // 
            this._u0Column.DataPropertyName = "U0";
            this._u0Column.HeaderText = "U0";
            this._u0Column.Name = "_u0Column";
            this._u0Column.ReadOnly = true;
            this._u0Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u0Column.Width = 27;
            // 
            // _u1Column
            // 
            this._u1Column.DataPropertyName = "U1";
            this._u1Column.HeaderText = "U1";
            this._u1Column.Name = "_u1Column";
            this._u1Column.ReadOnly = true;
            this._u1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u1Column.Width = 27;
            // 
            // _u2Column
            // 
            this._u2Column.DataPropertyName = "U2";
            this._u2Column.HeaderText = "U2";
            this._u2Column.Name = "_u2Column";
            this._u2Column.ReadOnly = true;
            this._u2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._u2Column.Width = 27;
            // 
            // _unColumn
            // 
            this._unColumn.DataPropertyName = "Un";
            this._unColumn.HeaderText = "Un";
            this._unColumn.Name = "_unColumn";
            this._unColumn.ReadOnly = true;
            this._unColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._unColumn.Width = 27;
            // 
            // _DCol
            // 
            this._DCol.DataPropertyName = "Входные сигналы 1-16";
            this._DCol.HeaderText = "Входные сигналы 1-16";
            this._DCol.Name = "_DCol";
            this._DCol.ReadOnly = true;
            this._DCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._DCol.Width = 95;
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(471, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(118, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(347, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(118, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР5 v70";
            this._openAlarmJournalDialog.Filter = "МР5 v70 ЖА(*.xml)|*.xml|МР5 v70 ЖА(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР5 v70";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР5 v70";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР5 v70) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР5 v70";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._configProgressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(717, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _configProgressBar
            // 
            this._configProgressBar.Name = "_configProgressBar";
            this._configProgressBar.Size = new System.Drawing.Size(100, 16);
            this._configProgressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(108, 17);
            this._statusLabel.Text = "Аварий в журнале";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(595, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(118, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР5 v70";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР5 v70 | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР5 v70";
            // 
            // Mr5V70AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 593);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(733, 632);
            this.Name = "Mr5V70AlarmJournalForm";
            this.Text = "Журнал аварий";
            this.Load += new System.EventHandler(this.Mr51AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.ToolStripProgressBar _configProgressBar;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stageCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeDamage;
        private System.Windows.Forms.DataGridViewTextBoxColumn _trigParamCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _valueCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ubcColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ucaColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u0Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _u2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _unColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _DCol;
    }
}