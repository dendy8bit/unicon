﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v70.SystemJournal.Structures;

namespace BEMN.MR5.v70.SystemJournal
{
    public partial class Mr5V70SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР5_V70_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string JOURNAL_READDING = "Идёт чтение журнала";

        #endregion [Constants]


        #region [Private fields]

        private MR5Device _device;
        private DataTable _dataTable;
        private int _recordNumber;
        #endregion [Private fields]

        public Mr5V70SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr5V70SystemJournalForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            
            this._device.SystemJournal70.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._device.SystemJournal70.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._device.SystemJournal70.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);

            this._configProgressBar.Maximum = this._device.SystemJournal70.Slots.Count;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(v50.SystemJournal.Mr5V50SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Устанавливает св-во Enabled для всех кнопок
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readJournalButton.Enabled = enabled;
            this._saveJournalButton.Enabled = enabled;
            this._loadJournalButton.Enabled = enabled;
            this._exportButton.Enabled = enabled;
        }

        private void FailReadJournal()
        {               
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void ReadRecord()
        {
            int i = 0;
            foreach (SystemJournalRecordStructV70 record in this._device.SystemJournal70.Value.Records.Where(record => !record.IsEmpty))
            {
                string message;
                if (Common.VersionConverter(this._device.DeviceVersion) > 70.06)
                {
                    message = StringsSj.SystemJournalMessagesNew.ContainsKey(record.RecordMessageNumber)
                        ? StringsSj.SystemJournalMessagesNew[record.RecordMessageNumber]
                        : record.RecordMessageNumber.ToString();
                }
                else
                {
                    message = StringsSj.SystemJournalMessages.ContainsKey(record.RecordMessageNumber)
                        ? StringsSj.SystemJournalMessages[record.RecordMessageNumber]
                        : record.RecordMessageNumber.ToString();
                }

                this._dataTable.Rows.Add(i + 1,record.GetRecordTime, message);
                i++;
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
            this.SetButtonsState(true);
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStructV70 journal = new SystemJournalStructV70();
                    int size = journal.GetSize();
                    List<byte> journalBytes = new List<byte>();
                    journalBytes.AddRange(Common.SwapArrayItems(file));
                    if (journalBytes.Count != size)
                    {
                        journalBytes.AddRange(new byte[size - journalBytes.Count]);
                    }
                    journal.InitStruct(journalBytes.ToArray());
                    this._device.SystemJournal70.Value = journal;
                    this.ReadRecord();
                }
                else
                {
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    this._systemJournalGrid.Refresh();
                }
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void Mr5SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            this.StartReadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._configProgressBar.Value = 0;
            this.RecordNumber = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._device.SystemJournal70.LoadStruct();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR5_70_SJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
