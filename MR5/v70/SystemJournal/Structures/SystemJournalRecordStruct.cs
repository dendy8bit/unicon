﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v70.SystemJournal.Structures
{
    public class SystemJournalRecordStructV70 : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _message;
        [Layout(1)] private ushort _year;
        [Layout(2)] private ushort _month;
        [Layout(3)] private ushort _date;
        [Layout(4)] private ushort _hour;
        [Layout(5)] private ushort _minute;
        [Layout(6)] private ushort _second;
        [Layout(7)] private ushort _millisecond;
        
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                int sum = this._year + this._month + this._date + this._hour + this._minute 
                    + this._second + this._millisecond + this._message;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this._date,
                        this._month,
                        this._year,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        ///// <summary>
        ///// Текст сообщения
        ///// </summary>
        //public string GetRecordMessage
        //{
        //    get
        //    {
        //       return StringsSj.SystemJournalMessages.ContainsKey(this._message)
        //           ? StringsSj.SystemJournalMessages[this._message] 
        //           : this._message.ToString();
        //    }
        //}
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public ushort RecordMessageNumber
        {
            get { return this._message; }
        }
        #endregion [Properties]
    }
}
