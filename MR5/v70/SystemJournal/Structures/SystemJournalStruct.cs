﻿using BEMN.Devices;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v70.SystemJournal.Structures
{
    public class SystemJournalStructV70 : StructBase
    {
        [Layout(0, Count = 128)] private SystemJournalRecordStructV70[] _records;

        public SystemJournalRecordStructV70[] Records
        {
            get { return this._records; }
        }

        public override object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            const int slotLenght = 8;
            int arrayLength = 128;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + slotLenght));
                start += slotLenght * 2;
            }
            return slots;
        }
    }
}
