﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v55.Osc.Structures;

namespace BEMN.MR5.v55.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader55
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructV55> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructV55> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        /// <summary>
        /// Весь журнал успешно прочитан
        /// </summary>
        public event Action AllJournalReadOk;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoader55(MemoryEntity<OscJournalStructV55> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStructV55>();
            
            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        } 
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructV55> OscRecords
        {
            get { return this._oscRecords; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void UpdateNumber()
        {
            this._refreshOscJournal.Value.Word = (ushort) this._recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructV55.RecordIndex = this.RecordNumber;
                OscJournalStructV55 record = this._oscJournal.Value.Clone<OscJournalStructV55>();
                this.OscRecords.Add(record);
                this._recordNumber = this.RecordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
                this.UpdateNumber();
            }
            else
            {
                if (this.AllJournalReadOk != null)
                    this.AllJournalReadOk.Invoke();
            }
        }
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            this.UpdateNumber();
        }
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}
