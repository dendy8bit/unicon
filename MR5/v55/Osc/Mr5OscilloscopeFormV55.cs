using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Osc.HelpClasses;
using BEMN.MR5.v55.Osc.Loaders;
using BEMN.MR5.v55.Osc.Structures;

namespace BEMN.MR5.v55.Osc
{
    public partial class Mr5OscilloscopeFormV55 : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoaderV55 _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader55 _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �������������� ��������������
        /// </summary>
        private readonly MemoryEntity<MeasureTransStructV55> _currentOptions;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV55 _countingList;
        private OscJournalStructV55 _journalStruct;
        private readonly DataTable _table;
        private MR5Device _device;
        #endregion [Private fields]
        
        #region [Ctor's]
        public Mr5OscilloscopeFormV55()
        {
            this.InitializeComponent();
        }

        public Mr5OscilloscopeFormV55(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            //��������� �������
            this._pageLoader = new OscPageLoaderV55(device.SetOscPageV55, device.OscPageV55);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
          
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader55(device.OscJournalV55, device.RefreshOscJournalV55);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //��������� ������� �����
            this._currentOptions = device.TransformerOscV55;
            this._currentOptions.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoader.StartReadJournal);
            this._currentOptions.AllReadFail +=HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            
            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscProgressBar.Value = 0;
            this.EnableButtons = true;
        }

        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text = "������ ����";
            }
            this.EnableButtons = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoader.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingList = new CountingListV55(this._pageLoader.ResultArray, this._journalStruct, this._currentOptions.Value); 
            }
               catch (Exception e)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this._oscReadButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
            this.EnableButtons = true;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV55 CountingList
        {
            get { return this._countingList ?? new CountingListV55(0); }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��5_55_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartLoad();
        }

        private void StartLoad()
        {
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._currentOptions.LoadStruct();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                return;
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��5 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                // TODO ������ ����������� ����������
            }
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            if(this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartLoad();
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this._oscSaveButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this.EnableButtons = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = this.CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }
        
        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        #endregion [Event Handlers]

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr5OscilloscopeFormV55); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]
    }
}