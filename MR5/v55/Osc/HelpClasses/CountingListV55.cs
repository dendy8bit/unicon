using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR5.v55.Configuration;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Osc.Structures;

namespace BEMN.MR5.v55.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListV55
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 12;

        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 128;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 4;

        public static readonly string[] iNames = { "Ia", "Ib", "Ic", "In" };
        #endregion [Constants]


        #region [Private fields]
        private readonly ushort[][] _countingArray;
        private ushort[][] _discrets;
        private int _count;
        private int _alarm;
        private string _dateTime;
        private OscJournalStructV55 _oscJournalStruct;
        private string _stage;
        #endregion [Private fields]

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseCurrents;
        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;
        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }
        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]
        
        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        public string Stage
        {
            get { return this._stage; }
        }
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        #region [Ctor's]
        public CountingListV55(ushort[] pageValue, OscJournalStructV55 oscJournalStruct, MeasureTransStructV55 measure)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();
                double factor = i != CURRENTS_COUNT - 1 ? 40 * measure.Tt : 5 * measure.Ttnp;
                currentsKoefs[i] = factor * 2 * Math.Sqrt(2) / 65536.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;

            ushort[][] d1To16 = this.DiscretArrayInit(this._countingArray[4]);
            ushort[][] d17To32 = this.DiscretArrayInit(this._countingArray[5]);
            ushort[][] d33To48 = this.DiscretArrayInit(this._countingArray[6]);
            ushort[][] d49To64 = this.DiscretArrayInit(this._countingArray[7]);
            ushort[][] d65To80 = this.DiscretArrayInit(this._countingArray[8]);
            ushort[][] d81To96 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d97To112 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d113To128 = this.DiscretArrayInit(this._countingArray[11]);


            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To16);
            dicrets.AddRange(d17To32);
            dicrets.AddRange(d33To48);
            dicrets.AddRange(d49To64);
            dicrets.AddRange(d65To80);
            dicrets.AddRange(d81To96);
            dicrets.AddRange(d97To112);
            dicrets.AddRange(d113To128);
            this._discrets = dicrets.ToArray();
        }

        public CountingListV55(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            this._count = count;
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("��5 ver.55 {0} {1} ������� - {2}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath,false,Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP5,1");
                cgfFile.WriteLine("132,4A,128D");
                int index = 1;
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, iNames[i],
                        this.CurrentsKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < StringsConfig.OscSignals.Count; i++)
                {
                    cgfFile.WriteLine("{0},{1},0", index, StringsConfig.OscSignals[i]);
                    index++;
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public CountingListV55 Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string[] buff = hdrStrings[0].Split(' ');
            string timeString = string.Format("{0} {1}", buff[2], buff[3]);
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));
            string stage = hdrStrings[3];

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath,Encoding.GetEncoding(1251));

            double[] factors = new double[CURRENTS_COUNT];
            Regex factorRegex = new Regex(@"\d+\,[I]\w+\,\,\,[A]\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2 + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[2 + CURRENTS_COUNT + DISCRETS_COUNT + 2].Replace("1000,", string.Empty));
            CountingListV55 result = new CountingListV55(counts) {_alarm = alarm};

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[CURRENTS_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }
            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            
            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j];
                }
                
                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + CURRENTS_COUNT], CultureInfo.InvariantCulture);
                }
            }

            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }

            result.Currents = currents;
            result.Discrets = discrets;
            result._dateTime = timeString;
            result._stage = stage;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
