﻿using System;
using System.Globalization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR5.v55.Configuration;

namespace BEMN.MR5.v55.Osc.Structures
{
    public class OscJournalStructV55 : StructBase
    {
        #region [Constants and static]

        private const string DATE_PATTERN = "{0:d2}.{1:d2}.{2:d2}";
        private const string TIME_PATTERN = "{0:d2}:{1:d2}:{2:d2}.{3:d2}";
        private const string NUMBER_PATTERN = "{0}";
        /// <summary>
        /// Размер одной страницы 
        /// </summary>
        private const int PAGE_LEN = 1024;
        private const int OscSize = 507900;
        public static int RecordIndex;
        #endregion [Constants]

        #region [Private fields]
        
        [Layout(0)] private ushort _reserv;
        [Layout(1)] private ushort _year;
        [Layout(2)] private ushort _month;
        [Layout(3)] private ushort _date;
        [Layout(4)] private ushort _hour;
        [Layout(5)] private ushort _minute;
        [Layout(6)] private ushort _second;
        [Layout(7)] private ushort _millisecond;
        
        /// <summary>
        /// "READY" если 0 - осциллограмма готова 
        /// </summary>
        [Layout(8)] private int _ready;
        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        [Layout(9)] private int _point;
        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        [Layout(10)] private int _begin;
        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        [Layout(11)] private int _len;
        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        [Layout(12)] private int _after;
        /// <summary>
        /// "ALM" Номер (последней) сработавшей защиты 
        /// </summary>
        [Layout(13)] private ushort _numberDefence;
        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        [Layout(14)] private ushort _sizeReference;

        #endregion [Private fields]

        #region [Properties]

        public string Stage
        {
            get
            {
                return Validator.GetJornal(this._numberDefence, StringsConfig.StageDictionary);
            }
        }

        /// <summary>
        /// Запись журнала осциллографа
        /// </summary>
        public object[] GetRecord
        {
            get
            {
                return new object[]
                    {
                        this.GetNumber,     //Номер
                        this.GetDate,       //Дата
                        this.GetTime,       //Время
                        this.Stage,         //№ сработавшей защиты
                        this.Len,           //Размер
                        this._ready,        //Готовность
                        this.Point,         //Адрес начала
                        this.GetEnd,        //Адрес конца
                        this.Begin,         //Начало
                        this.After,         //После аварии
                        this.SizeReference  //Длина отсчета
                    };
            }
        }

        public bool IsEmpty
        {
            get
            {
                //пустая запись
                return this.SizeReference == 0 |
                       //осциллограмма не готова, тоже признак конца журнала
                       this._ready != 0;
            }
        }
        /// <summary>
        /// Начальная страница осциллограммы
        /// </summary>
        public ushort OscStartIndex
        {
            get { return (ushort) (this.Point/PAGE_LEN); }
        }
        
        /// <summary>
        /// Номер соощения
        /// </summary>
        private string GetNumber
        {
            get
            {
                string result = string.Format(NUMBER_PATTERN, RecordIndex + 1);
                if (RecordIndex == 0)
                {
                    result += "(посл.)";
                }
                return result;
            }
        }

        /// <summary>
        /// Время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        TIME_PATTERN,
                        this._hour,
                        this._minute,
                        this._second,
                        this._millisecond
                    );
            }
        }

        /// <summary>
        /// Дата сообщения
        /// </summary>
        public string GetDate
        {
            get
            {
                return string.Format
                    (
                        DATE_PATTERN,
                        this._date,
                        this._month,
                        this._year
                    );
            }
        }

        public string GetFormattedDateTimeAlarm(int alarm)
        {
            try
            {
                DateTime a = new DateTime(this._year, this._month, this._date, this._hour, this._minute, this._second, this._millisecond * 10);
                DateTime result = a.AddMilliseconds(-alarm);
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     result.Month,
                                     result.Day,
                                     result.Year,
                                     result.Hour,
                                     result.Minute,
                                     result.Second,
                                     result.Millisecond);
            }
            catch (Exception)
            {

                return this.GetFormattedDateTime;
            }

        }

        public string GetFormattedDateTime
        {
            get
            {
                return string.Format("{0:D2}/{1:D2}/{2:D2},{3:D2}:{4:D2}:{5:D2}.{6:D3}",
                                     this._month,
                                     this._date,
                                     this._year,
                                     this._hour,
                                     this._minute,
                                     this._second,
                                     this._millisecond * 10);
            }
        }

        /// <summary>
        /// Адрес конца
        /// </summary>
        private string GetEnd
        {
            get
            {
                int currentEnd = this.Point + this.Len * this.SizeReference;
                return currentEnd > OscSize ? string.Format("{0} [{1}]", currentEnd, currentEnd - OscSize) : currentEnd.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// "LEN" Размер осциллограммы (в отсчётах)
        /// </summary>
        public int Len
        {
            get { return this._len; }
        }

        /// <summary>
        /// "AFTER" Размер после аварии (в отсчётах)
        /// </summary>
        public int After
        {
            get { return this._after; }
        }

        /// <summary>
        /// "BEGIN" Адрес аварии в массиве данных (в словах)
        /// </summary>
        public int Begin
        {
            get { return this._begin; }
        }

        /// <summary>
        /// "POINT" Адрес начала блока текущей осциллограммы в массиве данных (в словах)
        /// </summary>
        public int Point
        {
            get { return this._point; }
        }

        /// <summary>
        /// "REZ" Размер одного отсчёта (в словах)
        /// </summary>
        public ushort SizeReference
        {
            get { return this._sizeReference; }
        }
        #endregion [Private Properties]
    }
}
