﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v55.Measuring.Structures
{
    /// <summary>
    /// МР 550 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStructV55 :StructBase
    {
        #region [Private fields]
        //ток

        [Layout(0)] private ushort _in; // ток N
        [Layout(1)] private ushort _ia; // ток A
        [Layout(2)] private ushort _ib; // ток B
        [Layout(3)] private ushort _ic; // ток C
        [Layout(4)] private ushort _i0; // ток 0
        [Layout(5)] private ushort _i1; // ток 2
        [Layout(6)] private ushort _i2; // ток 2
        [Layout(7)] private ushort _ig; // ток Iг
        [Layout(8, Count = 15)] private ushort[] _res; //напряжения, частота, мощности, ОМП
        [Layout(9)] private ushort _uacc;
        #endregion [Private fields]
        
        #region [Public members]    

        private ushort GetMean(List<AnalogDataBaseStructV55> list,Func<AnalogDataBaseStructV55, ushort> func)
        {
            var count = list.Count();

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort) (sum/(double) count);
        }
        
      

        public string GetIa(List<AnalogDataBaseStructV55> list,MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIb(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIc(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI1(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI2(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI0(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIn(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }
        public string GetIg(List<AnalogDataBaseStructV55> list, MeasureTransStructV55 measure)
        {
            var value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }

        public string Uacc(List<AnalogDataBaseStructV55> list)
        {
            var value = this.GetMean(list, o => o._uacc);
            return ValuesConverterCommon.GetU(value).ToString();
        }
        #endregion [Public members]
    }
}
