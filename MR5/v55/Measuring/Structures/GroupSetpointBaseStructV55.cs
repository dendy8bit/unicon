﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR5.v55.Configuration;

namespace BEMN.MR5.v55.Measuring.Structures
{
    public class GroupSetpointBaseStructV55 : StructBase
    {
        [Layout(0)] private ushort _group;

        public string GetGroup(double version)
        {
            return Validator.Get(this._group, StringsConfig.SetpointsNames);
        }

        public void SetGroup(double version, string value)
        {
            this._group = Validator.Set(value, StringsConfig.SetpointsNames);
        }
    }
}
