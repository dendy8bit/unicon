﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v55.Configuration;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Measuring.Structures;

namespace BEMN.MR5.v55.Measuring
{
    public partial class Mr5V55MeasuringForm : Form, IFormView
    {
        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить индикацию";
        private const string MEASURE_TRANS_READ_FAIL = "Параметры измерений не были загружены";
        private const string MEASURE_TRANS_READ_OK = "Параметры измерений загружены";

        #region [Private fields]
        private readonly MemoryEntity<AnalogDataBaseStructV55> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseStructV55> _discretDataBase;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<MeasureTransStructV55> _measureTrans;
        private MeasureTransStructV55 _measureTransStruct;
        private readonly AveragerTime<AnalogDataBaseStructV55> _averagerTime;
        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;
        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;
        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;
        /// <summary>
        /// Защиты I(I*, I2/I1, Ig)
        /// </summary>
        private LedControl[] _currents;
        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;

        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;
        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _faultSignals;
        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _indicators;
        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faults;
        /// <summary>
        /// Автоматика
        /// </summary>
        private LedControl[] _automatics;
        
        private LedControl[] _manage;
        private MemoryEntity<GroupSetpointBaseStructV55> _groupSetpointBase;
        private MR5Device _device;
        #endregion [Private fields]

        public Mr5V55MeasuringForm()
        {
            this.InitializeComponent();
        }

        public Mr5V55MeasuringForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this._dateTime = device.DateTime55;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._discretDataBase = device.DiscretV55;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._analogDataBase = device.AnalogV55;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._groupSetpointBase = device.GroupSetpointBaseV55;
            this._groupSetpointBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                    this._setpointLabel.Text =
                        this._groupSetpointBase.Value.GetGroup(Common.VersionConverter(this._device.DeviceVersion)));
            this._groupSetpointBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,
                () => this._setpointLabel.Text = "не определена");


            this._measureTrans = device.MeasuringV55;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);
            
            device.ConfigurationV55.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                    this._measureTransStruct = new MeasureTransStructV55
                    {
                        Tt = device.ConfigurationV55.Value.MeasureTrans.Tt,
                        Ttnp = device.ConfigurationV55.Value.MeasureTrans.Ttnp
                    }
                );
            this._averagerTime = new AveragerTime<AnalogDataBaseStructV55>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;

            this.Init();
        }

        private void AnalogBdReadFail()
        {
            const string errorValue = "0";
    
            this._iaTextBox.Text = errorValue;
            this._ibTextBox.Text = errorValue;
            this._icTextBox.Text = errorValue;
            this._i1TextBox.Text = errorValue; 
            this._i2TextBox.Text = errorValue; 
            this._i0TextBox.Text = errorValue; 
            this._inTextBox.Text = errorValue;
            this._igTextBox.Text = errorValue;
        }

        private void AveragerTimeTick()
        {
            try
            {
                this._iaTextBox.Text = this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._measureTransStruct);
                this._ibTextBox.Text = this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._measureTransStruct);
                this._icTextBox.Text = this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._measureTransStruct);
                this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._measureTransStruct);
                this._i2TextBox.Text = this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._measureTransStruct);
                this._i0TextBox.Text = this._analogDataBase.Value.GetI0(this._averagerTime.ValueList, this._measureTransStruct);
                this._inTextBox.Text = this._analogDataBase.Value.GetIn(this._averagerTime.ValueList, this._measureTransStruct);
                this._igTextBox.Text = this._analogDataBase.Value.GetIg(this._averagerTime.ValueList, this._measureTransStruct);
                this._UaccBox.Text = this._analogDataBase.Value.Uacc(this._averagerTime.ValueList);
            }
            catch (Exception)
            {
            }
        }

        private void MeasureTransReadFail()
        {
            MessageBox.Show(MEASURE_TRANS_READ_FAIL);
        }

        private void MeasureTransReadOk()
        {
            this._measureTransStruct = this._measureTrans.Value;
            MessageBox.Show(MEASURE_TRANS_READ_OK);
            this._analogDataBase.LoadStructCycle();
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(Mr5V55MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion [INodeView Members]


        #region [Help members]
        private void Init()
        {
            this._manage = new[]
                {
                    this._manageLed1,
                    this._manageLed2,
                    this._manageLed3,
                    this._manageLed4,
                    this._manageLed6
                };
            this._automatics = new[]
                {
                   this._autoLed1,this._autoLed2,this._autoLed3,this._autoLed4,
                   this._autoLed5,this._autoLed6,this._autoLed7,this._autoLed8
                };
          
            this._freeLogic = new[]
                {
                    this._ssl1,this._ssl2,this._ssl3,this._ssl4,
                    this._ssl5,this._ssl6,this._ssl7,this._ssl8,
                    this._ssl9,this._ssl10,this._ssl11,this._ssl12,
                    this._ssl13,this._ssl14,this._ssl15,this._ssl16,
                    this._ssl17,this._ssl18,this._ssl19,this._ssl20,
                    this._ssl21,this._ssl22,this._ssl23,this._ssl24
                };
            this._currents = new[]
                {
                    this._i5Io,this._i5,this._i6Io,this._i6,
                    this._i7Io,this._i7,this._i8Io,this._i8,
                    this._iS1Io,this._iS1,this._iS2Io,this._iS2,
                    this._iS3Io,this._iS3,this._iS4Io,this._iS4,
                    this._i1Io,this._i1,this._i2Io,this._i2,
                    this._i3Io,this._i3,this._i4Io,this._i4
                };
           
            this._indicators = new[]
                {
                    this._indicator1,this._indicator2,this._indicator3,this._indicator4,
                    this._indicator5,this._indicator6,this._indicator7,this._indicator8
                };
            this._faultSignals = new[]
                {
                    this._faultSignalLed1,this._faultSignalLed2,this._faultSignalLed3,this._faultSignalLed4,
                    this._faultSignalLed5,this._faultSignalLed6,this._faultSignalLed7,this._faultSignalLed8,
                    this._faultSignalLed9,this._faultSignalLed10,this._faultSignalLed11,this._faultSignalLed12,
                    this._faultSignalLed13,this._faultSignalLed14,this._faultSignalLed15,this._faultSignalLed16
                };

            this._externalDefenses = new[]
                {
                    this._vz1,this._vz2,this._vz3,this._vz4,
                    this._vz5,this._vz6,this._vz7,this._vz8
                };

            this._outputLogicSignals = new[]
                {
                    this._vls1,this._vls2,this._vls3,
                    this._vls4,this._vls5,this._vls6,
                    this._vls7,this._vls8
                };
            this._inputsLogicSignals = new[]
                {
                    this._ls1,this._ls2,this._ls3,this._ls4,
                    this._ls5,this._ls6,this._ls7,this._ls8
                };
         
            this._faults = new[]
                {
                    this._faultStateLed1,this._faultStateLed2,this._faultStateLed3,this._faultStateLed4,
                    this._faultStateLed5,this._faultStateLed6,this._faultStateLed7,this._faultStateLed8
                };

            this._discretInputs = new[]
                {
                    this._d1,this._d2,this._d3,this._d4,
                    this._d5,this._d6,this._d7,this._d8
                };

            this._setpointsComboBox.DataSource = StringsConfig.SetpointsNames;
        }
        #endregion [Help members]

        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);    
            LedManager.TurnOffLeds(this._currents);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._faultSignals);
            LedManager.TurnOffLeds(this._indicators);
            LedManager.TurnOffLeds(this._faults);
            LedManager.TurnOffLeds(this._automatics);
            LedManager.TurnOffLeds(this._manage);  
            this._spl.State = LedState.Off;
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
          //  this._symbols = this._discretDataBase.Value.CurrentsSymbols;
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, this._discretDataBase.Value.DiscretInputs);
            //Индикаторы
            LedManager.SetLeds(this._indicators, this._discretDataBase.Value.Indicators);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, this._discretDataBase.Value.InputsLogicSignals);
            //Выходные ЛС
            LedManager.SetLeds(this._outputLogicSignals, this._discretDataBase.Value.OutputLogicSignals);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, this._discretDataBase.Value.ExternalDefenses);
            //Защиты I(I*, I2/I1, Ig)
            LedManager.SetLeds(this._currents, this._discretDataBase.Value.MaximumCurrent);
            //Автоматика
            LedManager.SetLeds(this._automatics, this._discretDataBase.Value.Automatics);
            //Неисправности
            LedManager.SetLeds(this._faults, this._discretDataBase.Value.Faults);
            //Реле
            LedManager.SetLeds(this._faultSignals, this._discretDataBase.Value.FaultSignals);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, this._discretDataBase.Value.FreeLogic);
            //Управление
            LedManager.SetLeds(this._manage, this._discretDataBase.Value.Manage);

            this._spl.State = this._discretDataBase.Value.WorkingLogic ? LedState.NoSignaled : LedState.Signaled;
        }
        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        private void dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void Mr550MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void Mr550MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._discretDataBase.RemoveStructQueries();
            this._groupSetpointBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._discretDataBase.LoadStructCycle();
                this._groupSetpointBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
                this._measureTrans.LoadStruct();
            }
            else
            {
                this._discretDataBase.RemoveStructQueries();
                this._groupSetpointBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1806, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1807, RESET_AJ);
        }
    

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1805, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1804, RESET_INDICATION);
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            this._groupSetpointBase.Value.SetGroup(Common.VersionConverter(this._device.DeviceVersion),this._setpointsComboBox.SelectedItem.ToString());
            this._groupSetpointBase.SaveStruct();
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1801, "Включить выключатель");
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._discretDataBase.SetBitByAdress(0x1800, "Отключить выключатель");
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "Запуск СПЛ", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "Останов СПЛ", this._device);
        }
    }
}
