﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v55.SystemJournal.Structures;

namespace BEMN.MR5.v55.SystemJournal
{
    public partial class Mr5V55SystemJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР5_v55_журнал_системы";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string JOURNAL_READDING = "Идёт чтение журнала";
        private const string FILE_NAME_PATTERN = "МР5 Журнал Системы v{0}";
        #endregion [Constants]

        #region [Private fields]
        private readonly MemoryEntity<SystemJournalStructV55> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;

        private MR5Device _device;
        #endregion [Private fields]

        public Mr5V55SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr5V55SystemJournalForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._systemJournal = device.SystemJournal55;
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;

            this._systemJournal.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._configProgressBar.Maximum = this._systemJournal.Slots.Count;
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr5V55SystemJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Устанавливает св-во Enabled для всех кнопок
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readJournalButton.Enabled = enabled;
            this._saveJournalButton.Enabled = enabled;
            this._loadJournalButton.Enabled = enabled;
            this._exportButton.Enabled = enabled;
        }

        private void FailReadJournal()
        {               
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }
        
        private void ReadRecord()
        {
            int i = 0;
            foreach (SystemJournalRecordStructV55 record in this._systemJournal.Value.Records.Where(record => !record.IsEmpty))
            {
                this._dataTable.Rows.Add(i + 1, record.GetRecordTime, record.GetRecordMessage);
                i++;
            }
            this._systemJournalGrid.Refresh();
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
            this.SetButtonsState(true);
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME_SYS);
            for (int j = 0; j < this._systemJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._systemJournalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        private void SaveJournalToFile()
        {
            this._saveJournalDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {
            this._openJournalDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (DialogResult.OK == this._openJournalDialog.ShowDialog() && !string.IsNullOrEmpty(this._openJournalDialog.FileName))
            {
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStructV55 journal = new SystemJournalStructV55();
                    int size = journal.GetSize();
                    List<byte> journalBytes = new List<byte>();
                    journalBytes.AddRange(Common.SwapArrayItems(file));
                    if (journalBytes.Count != size)
                    {
                        journalBytes.AddRange(new byte[size - journalBytes.Count]);
                    }
                    journal.InitStruct(journalBytes.ToArray());
                    this._systemJournal.Value = journal;
                    this.ReadRecord();
                }
                else
                {
                    this._dataTable.Clear();
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    this._systemJournalGrid.Refresh();
                }
            }
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._configProgressBar.Value = 0;
            this.RecordNumber = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._systemJournal.LoadStruct();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            this._saveJournalHtmlDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR5_55_SJ);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
