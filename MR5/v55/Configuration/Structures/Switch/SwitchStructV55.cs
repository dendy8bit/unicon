﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v55.Configuration.Structures.Switch
{
   public class SwitchStructV55:StructBase
   {
       [Layout(0)]  private ushort _off;
       [Layout(1)]  private ushort _on;
       [Layout(2)]  private ushort _fault;
       [Layout(3)]  private ushort _block;
       [Layout(4)]  private ushort _urovTime;
       [Layout(5)]  private ushort _urovUstavka;
       [Layout(6)]  private ushort _timeSignal;
       [Layout(7)]  private ushort _timeOn;
       [Layout(8)]  private ushort _res;
       [Layout(9)]  private ushort _manageSignals;

       [BindingProperty(0)]
       [XmlElement(ElementName = "Отключено")]
       public string Off
       {
           get { return Validator.Get(this._off, StringsConfig.ExternalSignals); }
           set { this._off = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(1)]
       [XmlElement(ElementName = "Включено")]
       public string On
       {
           get { return Validator.Get(this._on, StringsConfig.ExternalSignals); }
           set { this._on = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(2)]
       [XmlElement(ElementName = "Ошибка")]
       public string Fault
       {
           get { return Validator.Get(this._fault, StringsConfig.ExternalSignals); }
           set { this._fault = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(3)]
       [XmlElement(ElementName = "Блокировка")]
       public string Block
       {
           get { return Validator.Get(this._block, StringsConfig.ExternalSignals); }
           set { this._block = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(4)]
       [XmlElement(ElementName = "tуров")]
       public int UrovTime
       {
           get { return ValuesConverterCommon.GetWaitTime(this._urovTime) ; }
           set { this._urovTime = ValuesConverterCommon.SetWaitTime(value); }
       }

       [BindingProperty(5)]
       [XmlElement(ElementName = "ток_УРОВ")]
       public double UrovUstavka
       {
           get
           {
               return ValuesConverterCommon.GetIn(this._urovUstavka);
           }
           set { this._urovUstavka = ValuesConverterCommon.SetIn(value); }
       }

       [BindingProperty(6)]
       [XmlElement(ElementName = "Импульс")]
       public int TimeSignal
       {
           get { return ValuesConverterCommon.GetWaitTime(this._timeSignal); }
           set { this._timeSignal = ValuesConverterCommon.SetWaitTime(value); }
       }

       [BindingProperty(7)]
       [XmlElement(ElementName = "Ускорение")]
       public int TimeOn
       {
           get { return ValuesConverterCommon.GetWaitTime(this._timeOn); }
           set { this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
       }
       
       [BindingProperty(8)]
       [XmlElement(ElementName = "Меню")]
       public string FromButton
       {
           get { return Validator.Get(this._manageSignals, StringsConfig.Zr, 0); }
           set { this._manageSignals = Validator.Set(value, StringsConfig.Zr, this._manageSignals, 0); }
       }

       [BindingProperty(9)]
       [XmlElement(ElementName = "Ключ")]
       public string FromKey
       {
           get { return Validator.Get(this._manageSignals, StringsConfig.Cr,1); }
           set { this._manageSignals = Validator.Set(value, StringsConfig.Cr, this._manageSignals, 1); }
       }

       [BindingProperty(10)]
       [XmlElement(ElementName = "Внешнее")]
       public string External
       {
           get { return Validator.Get(this._manageSignals, StringsConfig.Cr,2); }
           set { this._manageSignals = Validator.Set(value, StringsConfig.Cr, this._manageSignals, 2); }
       }

       [BindingProperty(11)]
       [XmlElement(ElementName = "СДТУ")]
       public string Sdtu
       {
           get { return Validator.Get(this._manageSignals, StringsConfig.Zr,3); }
           set { this._manageSignals = Validator.Set(value, StringsConfig.Zr, this._manageSignals, 3); }
       }
   }
}
