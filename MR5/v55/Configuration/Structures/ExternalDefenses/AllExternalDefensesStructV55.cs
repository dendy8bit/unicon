﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v55.Configuration.Structures.ExternalDefenses
{
    public class AllExternalDefensesStruct : StructBase, IDgvRowsContainer<ExternalDefenseStructV55>
    {
        [Layout(1, Count = 8)]
        private ExternalDefenseStructV55[] _externalDefenses;
        [XmlArray(ElementName = "Все")]
        public ExternalDefenseStructV55[] Rows
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
