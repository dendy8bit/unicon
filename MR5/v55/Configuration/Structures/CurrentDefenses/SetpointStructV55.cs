﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v55.Configuration.Structures.CurrentDefenses
{
  public  class SetpointStructV55 : StructBase
    {
     [Layout(0)]   private AllCurrentDefensesStructV55 _currentDefenses;
      [Layout(1)] private AllAddCurrentDefensesStructV55 _addCurrentDefenses;

      [BindingProperty(0)]
      public AllCurrentDefensesStructV55 CurrentDefenses
      {
          get { return this._currentDefenses; }
          set { this._currentDefenses = value; }
      }
      [BindingProperty(1)]
      public AllAddCurrentDefensesStructV55 AddCurrentDefenses
      {
          get { return this._addCurrentDefenses; }
          set { this._addCurrentDefenses = value; }
      }
    }
}
