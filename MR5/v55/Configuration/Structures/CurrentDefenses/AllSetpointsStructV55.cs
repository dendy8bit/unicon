﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR5.v55.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStructV55 : StructBase, ISetpointContainer<SetpointStructV55>
    {
        private const int DEF_COUNT = 6;

        [Layout(0, Count = DEF_COUNT)]
        private AllCurrentDefensesStructV55[] _currentDefenses;
         [Layout(1, Count = DEF_COUNT)]
         private AllAddCurrentDefensesStructV55[] _addCurrentDefenses;

        [XmlElement(ElementName = "Группы")]
        public SetpointStructV55[] Setpoints
        {
            get
            {
                var result = new SetpointStructV55[DEF_COUNT];
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    result[i] = new SetpointStructV55();
                    result[i].AddCurrentDefenses =  this._addCurrentDefenses[i].Clone<AllAddCurrentDefensesStructV55>();
                    result[i].CurrentDefenses =  this._currentDefenses[i].Clone<AllCurrentDefensesStructV55>();
                }
                return result;
            }
            set
            {
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    this._addCurrentDefenses[i] = value[i].AddCurrentDefenses;
                    this._currentDefenses[i] = value[i].CurrentDefenses;
                }
            }
        }

        [XmlElement(ElementName = "Группа1")]
        public SetpointStructV55 Group1
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[0].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[0].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа2")]
        public SetpointStructV55 Group2
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[1].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[1].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа3")]
        public SetpointStructV55 Group3
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[2].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[2].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа4")]
        public SetpointStructV55 Group4
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[3].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[3].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа5")]
        public SetpointStructV55 Group5
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[4].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[4].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа6")]
        public SetpointStructV55 Group6
        {
            get
            {
                SetpointStructV55 result = new SetpointStructV55();
                result.AddCurrentDefenses = this._addCurrentDefenses[5].Clone<AllAddCurrentDefensesStructV55>();
                result.CurrentDefenses = this._currentDefenses[5].Clone<AllCurrentDefensesStructV55>();
                return result;
            }
            set { }
        }


    }
}
