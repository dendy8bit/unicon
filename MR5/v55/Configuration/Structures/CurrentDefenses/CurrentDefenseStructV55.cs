﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v55.Configuration.Structures.CurrentDefenses
{
    public class CurrentDefenseStructV55 : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//уставка срабатывания
        [Layout(3)] private ushort _ust; //Время срабатывания или коэффициент
        [Layout(4)] private ushort _reserve;
        [Layout(5)] private ushort _u; //уставка возврата
      

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.Modes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.Modes, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalSignals); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Параметр")]
        public string Parameter
        {
            get
            {
                return Validator.Get(this._config, StringsConfig.TokParameter, 8);
               
            }
            set
            {
                this._config = Validator.Set(value, StringsConfig.TokParameter, this._config, 8);
            }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public double Srab
        {
            get { return ValuesConverterCommon.GetIn(this._srab); }
            set { this._srab = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Уставка_срабатыванияIn")]
        public double SrabIn
        {
            get { return ValuesConverterCommon.GetUstavka5(this._srab); }
            set { this._srab = ValuesConverterCommon.SetUstavka5(value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Характеристика")]
        public string Feature
        {
            get { return Validator.Get(this._config, StringsConfig.FeatureLight, 12); }
            set
            {
                this._config = Validator.Set(value, StringsConfig.FeatureLight, this._config, 12);
            }
        }

        /// <summary>
        /// Время срабатывания или коэффициент
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "время_срабатывания_коэффициент")]
        public int TimeSrabKoef
        {
            get
            {
                if (Common.GetBit(this._config, 12))
                {
                    return this._ust;
                }
                else
                {
                    return ValuesConverterCommon.GetWaitTime(this._ust);
                }
            }
            set
            {
                if (Common.GetBit(this._config, 12))
                {
                    this._ust = (ushort) value;
                }
                else
                {
                    this._ust = ValuesConverterCommon.SetWaitTime(value);
                }
            }
        }

        /// <summary>
        /// Время срабатывания
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "время_срабатывания")]
        public int TimeSrab
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(this._ust);
            }
            set
            {
                this._ust = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Speedup
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Уставка_ускорения")]
        public int Tspeedup
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }



        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "ОСЦ")]
        public string Osc
        {
            get { return Validator.Get(this._config, StringsConfig.OscV111, 3,4); }
            set { this._config = Validator.Set(value, StringsConfig.OscV111, this._config, 3,4); }
        }
    }
}
