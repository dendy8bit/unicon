﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v55.Configuration.Structures.CurrentDefenses
{
    public class AllAddCurrentDefensesStructV55 : StructBase, IDgvRowsContainer<AddCurrentDefenseStructV55>
    {
        [Layout(0, Count = 2)] private AddCurrentDefenseStructV55[] _addCurrentDefenses;

        public AddCurrentDefenseStructV55[] Rows
        {
            get { return this._addCurrentDefenses; }
            set { this._addCurrentDefenses = value; }
        }
    }
}
