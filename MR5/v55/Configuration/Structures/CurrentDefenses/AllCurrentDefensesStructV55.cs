﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v55.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesStructV55 : StructBase, IDgvRowsContainer<CurrentDefenseStructV55>
    {
        [Layout(0, Count = 4)] private ushort[] _res;
        [Layout(1, Count = 10)] private CurrentDefenseStructV55[] _currentDefenses;

        public CurrentDefenseStructV55[] Rows
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }
    }
}
