﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v55.Configuration.Structures.OscConfig
{
    public class OscConfigStruct55 : StructBase
    {
        [Layout(0)] private ushort _config;

        public const int ONE_OSC_MAX_LEN = 21162;

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.OscCount, 0, 1, 2, 3, 4); }
            set { this._config = Validator.Set(value, StringsConfig.OscCount, this._config, 0, 1, 2, 3, 4); }
        }
        [BindingProperty(1)]
        public string Fixation
        {
            get { return Validator.Get(this._config, StringsConfig.OscFixation, 7); }
            set { this._config = Validator.Set(value, StringsConfig.OscFixation, this._config, 7); }
        }
        [BindingProperty(2)]
        public ushort Percent
        {
            get { return (ushort) (Common.GetBits(this._config, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._config = Common.SetBits(this._config, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
        public int Size
        {
            get { return ONE_OSC_MAX_LEN * 2 / (2 + Common.GetBits(this._config, 0, 1, 2, 3, 4, 5, 6)); }
            set { }
        }
    }
}
