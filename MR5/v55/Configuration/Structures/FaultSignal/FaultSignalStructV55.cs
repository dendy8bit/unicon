﻿using System;
using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR5.v55.Configuration.Structures.FaultSignal
{
    public class FaultSignalStructV55 : StructBase, IBitField, IXmlSerializable
    {
        [Layout(0)] private ushort _signals;

        [XmlIgnore]
        public BitArray Bits
        {
            get
            {
                var arr = new bool[8];
                for (int i = 0; i < 8; i++)
                {
                    arr[i] = Common.GetBit(this._signals, i);
                }
                return new BitArray(arr);
            }
            set
            {
               
                for (int i = 0; i < 8; i++)
                {
                    this._signals = Common.SetBit(this._signals, i, value[i]);
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < this.Bits.Count; i++)
            {
                writer.WriteElementString("Неисправность", this.Bits[i] ? "Да" : "Нет");
            }
        }
    }
}
