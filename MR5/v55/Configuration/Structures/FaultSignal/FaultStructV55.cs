﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR5.v55.Configuration.Structures.FaultSignal
{
    public class FaultStructV55 : StructBase
    {
        [Layout(0)] private FaultSignalStructV55 _signals;
        [Layout(1)] private ushort _time;
        [Layout(2, Count = 2)] private ushort[] _res;

        [BindingProperty(0)]
        public FaultSignalStructV55 Signals
        {
            get { return this._signals; }
            set { this._signals = value; }
        }
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Импульс_реле_неисправность")]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time) ; }
            set { this._time =ValuesConverterCommon.SetWaitTime(value) ; }
        }
    }
}