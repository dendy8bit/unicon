﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v55.Configuration.Structures.Apv;
using BEMN.MR5.v55.Configuration.Structures.Avr;
using BEMN.MR5.v55.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v55.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v55.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v55.Configuration.Structures.FaultSignal;
using BEMN.MR5.v55.Configuration.Structures.Indicators;
using BEMN.MR5.v55.Configuration.Structures.Ls;
using BEMN.MR5.v55.Configuration.Structures.Lzsh;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Configuration.Structures.OscConfig;
using BEMN.MR5.v55.Configuration.Structures.Relay;
using BEMN.MR5.v55.Configuration.Structures.Switch;
using BEMN.MR5.v55.Configuration.Structures.Vls;

namespace BEMN.MR5.v55.Configuration.Structures
{
    [XmlRoot(ElementName = "МР5_V55")]
    public class ConfigurationStructV55 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР5"; } set { } }
        public int Group { get; set; }
        [XmlElement(ElementName = "Время_возврата_полное")]
        public string TimeRet { get; set; }

        #region [Private fields]

        [Layout(0)] private MeasureTransStructV55 _measureTrans;
        [Layout(1)] private ExternalSignalStructV55 _externalSignal;
        [Layout(2)] private FaultStructV55 _fault;
        [Layout(3, Ignore = true)] private ushort _portInterface;
        [Layout(4)] private InputLogicSignalStructV55 _inputLogicSignal;
        [Layout(5)] private SwitchStructV55 _switch;
        [Layout(6)] private ApvStructV55 _apv;
        [Layout(7)] private AvrStructV55 _avr;
        [Layout(8)] private LpbStructV55 _lzsh;
        [Layout(9)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(10)] private AllSetpointsStructV55 _allSetpoints;
        [Layout(11, Count = 32, Ignore = true)] private ushort[] _res;
        [Layout(12)] private OutputLogicSignalStructV55 _vls;
        [Layout(13)] private AllReleOutputStructV55 _reley;
        [Layout(14)] private AllIndicatorsStructV55 _indicators;
        [Layout(15, Count = 4, Ignore = true)] private ushort[] _res2;
        [Layout(16)] private OscConfigStruct55 _oscConfig; 
        #endregion [Private fields]

        [BindingProperty(0)]
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        public MeasureTransStructV55 MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Внешние_сигналы")]
        public ExternalSignalStructV55 ExternalSignal
        {
            get { return this._externalSignal; }
            set { this._externalSignal = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Реле_неисправности")]
        public FaultStructV55 Fault
        {
            get { return  this._fault; }
            set { this._fault = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        public InputLogicSignalStructV55 InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        public SwitchStructV55 Switch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "АПВ")]
        public ApvStructV55 Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "АВР")]
        public AvrStructV55 Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "ЛЗШ")]
        public LpbStructV55 Lzsh
        {
            get { return this._lzsh; }
            set { this._lzsh = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "Токовые")]
        public AllSetpointsStructV55 AllSetpoints
        {
            get { return this._allSetpoints; }
            set { this._allSetpoints = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Все_ВЛС")]
        public OutputLogicSignalStructV55 Vls
        {
            get { return this._vls; }
            set { this._vls = value; }
        }

         [BindingProperty(11)]
         [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStructV55 Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        [XmlElement(ElementName = "Осц")]
        [BindingProperty(12)]
        public OscConfigStruct55 OscConfig
        {
            get { return this._oscConfig; }
            set { this._oscConfig = value; }

        }
    }
}
