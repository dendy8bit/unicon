﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v55.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    public class InputLogicSignalStructV55 : StructBase, IXmlSerializable
    {
        public const int LOGIC_COUNT = 8;

        [Layout(0, Count = LOGIC_COUNT)] private InputLogicStructV55[] _logic;

        [BindingProperty(0)]
        [XmlIgnore]
        public InputLogicStructV55 this[int index]
        {
            get { return this._logic[index]; }
            set { this._logic[index] = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < LOGIC_COUNT; i++)
            {

                writer.WriteStartElement(string.Format("ЛС"));
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}
