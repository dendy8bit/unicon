﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v55.Configuration.Structures.Relay
{
    /// <summary>
    /// параметры выходных реле
    /// </summary>
    [XmlType(TypeName = "Одно_реле")]
    public class ReleOutputStructV55 : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _type;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeXml
        {
            get { return Validator.Get(this._type, StringsConfig.ReleyType); }
            set { this._type = Validator.Set(value, StringsConfig.ReleyType); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string SignalXml
        {
            get { return Validator.Get(this._signal, StringsConfig.RelaySignals); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        #endregion [Properties]
    }
}
