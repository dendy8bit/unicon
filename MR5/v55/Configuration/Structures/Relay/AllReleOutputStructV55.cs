using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v55.Configuration.Structures.Relay
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStructV55 : StructBase, IDgvRowsContainer<ReleOutputStructV55>
    {
        public const int RELAY_COUNT = 16;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT)]
        private ReleOutputStructV55[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]
    
       public ReleOutputStructV55[] Rows
        {
            get
            {
                return this._relays;
            }
            set
            {
                this._relays = value;
            }
        }
    }
}