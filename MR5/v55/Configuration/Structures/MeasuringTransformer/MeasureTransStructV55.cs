﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStructV55 : StructBase
    {
        #region [Public field]

        [Layout(0)] private ushort _ttMode; //res
        [Layout(1)] private ushort _tt;
        [Layout(2)] private ushort _ttnp;
        [Layout(3)] private ushort _maxI;
        [Layout(4)] private ushort _startI; //res
        [Layout(5)] private ushort _res1;
        [Layout(6)] private ushort _res2;
        [Layout(7)] private ushort _res3;
        
        #endregion [Public field]

        [BindingProperty(0)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Tt
        {
            get { return this._tt; }
            set { this._tt = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public ushort Ttnp
        {
            get { return this._ttnp; }
            set { this._ttnp = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Iм")]
        public double MaxI
        {
            get
            {
                return ValuesConverterCommon.GetIn(this._maxI);
            }
            set
            {
                this._maxI = ValuesConverterCommon.SetIn(value);
            }
        }
    }
}
