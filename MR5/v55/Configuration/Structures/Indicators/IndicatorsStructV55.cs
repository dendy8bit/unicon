﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v55.Configuration.Structures.Indicators
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsStructV55 : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _type;
        [Layout(1)] private ushort _signal;


        #endregion [Private fields]


        #region [Properties]


        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string Type
        {
            get { return Validator.Get(this._type, StringsConfig.ReleyType, 15); }
            set { this._type = Validator.Set(value, StringsConfig.ReleyType, this._type, 15); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string Signal
        {
            get { return Validator.Get(this._type, StringsConfig.RelaySignals, 0, 1, 2, 3, 4, 5, 6, 7, 8); }
            set
            {
                this._type = Validator.Set(value, StringsConfig.RelaySignals, this._type, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            }
        }


        /// <summary>
        /// Сброс_индикации
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сброс_индикации")]
        public bool ResetInd
        {
            get { return Common.GetBit(this._signal, 0); }
            set { this._signal = Common.SetBit(this._signal, 0, value); }
        }

        /// <summary>
        /// Сброс_ЖА
        /// </summary>
        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "Сброс_ЖА")]
        public bool ResetAj
        {
            get { return Common.GetBit(this._signal, 1); }
            set { this._signal = Common.SetBit(this._signal, 1, value); }
        }

        /// <summary>
        /// Сброс_ЖС
        /// </summary>
        [BindingProperty(4)]
        [XmlAttribute(AttributeName = "Сброс_ЖС")]
        public bool ResetSj
        {
            get { return Common.GetBit(this._signal, 2); }
            set { this._signal = Common.SetBit(this._signal, 2, value); }
        }

        #endregion [Properties]
    }
}
