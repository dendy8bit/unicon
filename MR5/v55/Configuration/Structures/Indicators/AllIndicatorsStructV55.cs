using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v55.Configuration.Structures.Indicators
{
    /// <summary>
    /// ��� ����������
    /// </summary>
    public class AllIndicatorsStructV55 : StructBase, IDgvRowsContainer<IndicatorsStructV55>
    {
        public const int INDICATORS_COUNT = 8;

        /// <summary>
        /// ����������
        /// </summary>
        [Layout(0, Count = INDICATORS_COUNT)]
        private IndicatorsStructV55[] _indicators;

        /// <summary>
        /// ����������
        /// </summary>
        [XmlArray(ElementName = "���_����������")]
      public IndicatorsStructV55[] Rows
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }
    }
}