using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v55.Configuration.Structures;
using BEMN.MR5.v55.Configuration.Structures.Apv;
using BEMN.MR5.v55.Configuration.Structures.Avr;
using BEMN.MR5.v55.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v55.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v55.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v55.Configuration.Structures.FaultSignal;
using BEMN.MR5.v55.Configuration.Structures.Indicators;
using BEMN.MR5.v55.Configuration.Structures.Ls;
using BEMN.MR5.v55.Configuration.Structures.Lzsh;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Configuration.Structures.OscConfig;
using BEMN.MR5.v55.Configuration.Structures.Switch;
using BEMN.MR5.v55.Configuration.Structures.Vls;

namespace BEMN.MR5.v55.Configuration
{
    public partial class Mr5V55ConfigurationForm : Form, IFormView
    {
        #region [Const]
        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string MR5 = "MR5";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";
        private const string XML_HEAD = "MR5_SET_POINTS";
        private const string MR5v55_BASE_CONFIG_PATH = "\\MR5\\MR5v55_BaseConfig.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";
        #endregion

        #region [Private fields]
        private readonly MR5Device _device;
        private readonly MemoryEntity<ConfigurationStructV55> _configuration;
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;
        private ConfigurationStructV55 _currentSetpointsStruct;

        #region [Validators]
        private NewStructValidator<MeasureTransStructV55> _measureTransValidator;
        private NewStructValidator<ExternalSignalStructV55> _externalSignalsValidator;
        private NewStructValidator<FaultStructV55> _faultValidator;

        private NewDgwValidatior<InputLogicStructV55>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStructV55> _inputLogicUnion;

        private NewStructValidator<SwitchStructV55> _switchValidator;
        private NewStructValidator<ApvStructV55> _apvValidator;
        private NewStructValidator<AvrStructV55> _avrValidator;
        private NewStructValidator<LpbStructV55> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStructV55> _externalDefenseValidatior;
        private NewDgwValidatior<AllCurrentDefensesStructV55, CurrentDefenseStructV55> _currentDefenseValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStructV55, AddCurrentDefenseStructV55> _addCurrentDefenseValidator;
        private SetpointsValidator< AllSetpointsStructV55, SetpointStructV55> _setpointsValidator;
        private StructUnion<SetpointStructV55> _setpointUnion;

        #region [���]
        /// <summary>
        /// ������ ��������� ��� ���
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        private NewCheckedListBoxValidator<OutputLogicStructV55>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStructV55> _vlsUnion;
        #endregion [���]
        private NewDgwValidatior<AllIndicatorsStructV55, IndicatorsStructV55> _indicatorValidator;
        private NewStructValidator<OscConfigStruct55> _oscValidator;
        private StructUnion<ConfigurationStructV55> _configurationUnion;
        #endregion [Validators] 

        #endregion [Private fields]


        #region [Ctor's]
        public Mr5V55ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr5V55ConfigurationForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._configuration = this._device.ConfigurationV55;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);
                });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_WRITE_CONFIG;
                MessageBox.Show(ERROR_WRITE_CONFIG);

            });
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {

                this._device.WriteConfiguration();
                this.IsProcess = false;
                this._statusLabel.Text = "������������ ������� ��������";

            });
            this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStruct = new ConfigurationStructV55();
            this.Init();
            this._configurationUnion.Set(this._currentSetpointsStruct);
        } 
        private void Init()
        {
            this._copySetpoinsGroupComboBox.DataSource = StringsConfig.CopyGroupsNames;

            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals9,
                this._inputSignals10, this._inputSignals11, this._inputSignals12
            };

            this._measureTransValidator = new NewStructValidator<MeasureTransStructV55>
                (
                this._toolTip,
                new ControlInfoText(this._TT_Box, RulesContainer.UshortTo1500),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo100),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40)
                );

            this._externalSignalsValidator = new NewStructValidator<ExternalSignalStructV55>
                (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._keyOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._extOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._extOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._indicationCombo, StringsConfig.ExternalSignals)  ,
                new ControlInfoCombo(this._constraintGroupCombo1, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo2, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo3, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo4, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo5, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo6, StringsConfig.ExternalSignals),
                new ControlInfoText(this._UaccBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._inpDisableCombo, StringsConfig.ExternalSignals)
                );

            this._faultValidator = new NewStructValidator<FaultStructV55>
                (
                this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStructV55>(this._dispepairCheckList)),
                new ControlInfoText(this._dispepairReleDurationBox, RulesContainer.IntTo3M)
                );

            this._inputLogicValidator = new NewDgwValidatior<InputLogicStructV55>[InputLogicSignalStructV55.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStructV55.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStructV55>
              (
              this._lsBoxes[i],
              InputLogicStructV55.DISCRETS_COUNT,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            this._inputLogicUnion = new StructUnion<InputLogicSignalStructV55>(this._inputLogicValidator);

            this._switchValidator = new NewStructValidator<SwitchStructV55>
                (
                this._toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(this._switcherUrovTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherUrovBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherAccelerationBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfig.Zr)
                );

            this._apvValidator = new NewStructValidator<ApvStructV55>
                (
                this._toolTip,
                new ControlInfoCombo(this._APV_ConfigCombo, StringsConfig.ApvModes),
                new ControlInfoCombo(this._APV_BlockingCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(this._APV_BlockingTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_ReadyTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat1TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat2TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat3TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat4TimeBox, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvStartCheckBox)

                );
            this._avrValidator = new NewStructValidator<AvrStructV55>
                (
                this._toolTip,
                new ControlInfoCheck(this._avrBySignalCheckBox),
                new ControlInfoCheck(this._avrByOffCheckBox),
                new ControlInfoCheck(this._avrBySelfOffCheckBox),
                new ControlInfoCheck(this._avrByDiffCheckBox),
                new ControlInfoCombo(this._avrSIGNOn, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlocking, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlockClear, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrResolve, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBack, StringsConfig.ExternalSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTBack, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTOff, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._avrClearCheckBox)
                );
            this._lpbValidator = new NewStructValidator<LpbStructV55>
                  (
                  this._toolTip,
                  new ControlInfoCombo(this._lzhModes, StringsConfig.BeNo),
                  new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40)
                  );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStructV55>
                (this._externalDefenseGrid,
                8,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff =new[]{new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.Modes[0],true,2,3,4,5,6,7,8,9,10,11,12), 
                        new TurnOffRule(5, false,false, 6, 7, 8)
                        )} 
                };

            Func<IValidatingRule> currentDefenseFunc = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._tokDefenseGrid1[5, this._tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                        StringsConfig.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            });


            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStructV55, CurrentDefenseStructV55>
                (
                new[] {this._tokDefenseGrid1, this._tokDefenseGrid2, this._tokDefenseGrid3 },
                new[] {4, 4, 2},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),
                new ColumnInfoCombo(StringsConfig.TokParameter, ColumnsType.COMBO, true, false, false),
                new ColumnInfoText(RulesContainer.Ustavka40,true,true,false),
                new ColumnInfoText(RulesContainer.Ustavka5,false,false,true),
                new ColumnInfoCombo(StringsConfig.FeatureLight, ColumnsType.COMBO, true, false, false),
                new ColumnInfoTextDependent(currentDefenseFunc, true, false, false),
                new ColumnInfoText(RulesContainer.IntTo3M, false, true, true),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscV111)
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(this._tokDefenseGrid1,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10)),
                            new TurnOffDgv(this._tokDefenseGrid2,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10)),
                            new TurnOffDgv(this._tokDefenseGrid3,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10))
                        }
                };

            this._addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStructV55, AddCurrentDefenseStructV55>
                (new [] { this._tokDefenseGrid4, this._tokDefenseGrid5},
                new [] {1, 1},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),
                new ColumnInfoText(RulesContainer.Ustavka5, true, false),
                new ColumnInfoText(RulesContainer.Ustavka100, false, true),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscV111)
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(this._tokDefenseGrid4,
                                new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11), 
                                new TurnOffRule(5, false, false, 6)),

                            new TurnOffDgv(this._tokDefenseGrid5,
                                new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                                new TurnOffRule(5, false, false, 6))
                        }
            };


            this._setpointUnion = new StructUnion<SetpointStructV55>
                (
                this._currentDefenseValidatior, this._addCurrentDefenseValidator
                );

            this._setpointsValidator = new SetpointsValidator<AllSetpointsStructV55,SetpointStructV55>
                (
                new ComboboxSelector(this._setpointsComboBox,StringsConfig.SetpointsNames), this._setpointUnion
                );
            
            #region [���] 

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStructV55>[OutputLogicSignalStructV55.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStructV55.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStructV55>(this._vlsBoxes[i], StringsConfig.VlsSignals);
            }
            this._vlsUnion = new StructUnion<OutputLogicSignalStructV55>(this._vlsValidator);
            #endregion [���]

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStructV55, IndicatorsStructV55>
                (this._outputIndicatorsGrid,
                AllIndicatorsStructV55.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._oscValidator = new NewStructValidator<OscConfigStruct55>
                (
                this._toolTip,
                new ControlInfoCombo(this._oscLengthCmb, StringsConfig.OscLenMode),
                new ControlInfoCombo(this._oscFixationCombo, StringsConfig.OscFixation),
                new ControlInfoText(this._oscWriteLength, RulesContainer.Ushort1To100)
                );

            this._configurationUnion = new StructUnion<ConfigurationStructV55>
                (
                this._measureTransValidator, 
                this._externalSignalsValidator,
                this._faultValidator,
                this._inputLogicUnion,
                this._switchValidator, 
                this._apvValidator, 
                this._avrValidator,
                this._lpbValidator, 
                this._externalDefenseValidatior, 
                this._setpointsValidator,
                this._vlsUnion,
                this._indicatorValidator, 
                this._oscValidator
                );
        }
        #endregion [Ctor's]

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this.Deserialize(this._openConfigurationDlg.FileName);
                    this._statusLabel.Text = string.Format("���� {0} ������� ��������", this._openConfigurationDlg.FileName);
                }
                catch
                {
                    MessageBox.Show(FILE_LOAD_FAIL);
                }
            }
        }

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(binFileName);

            XmlNode a = doc.FirstChild.SelectSingleNode(string.Format(XML_HEAD));
            if (a == null)
                throw new NullReferenceException();

            List<byte> values = new List<byte>(Convert.FromBase64String(a.InnerText));

            if (values.Count > 1440)
            {
                values.RemoveAt(66);
                values.RemoveAt(66);
            }

            this._currentSetpointsStruct.InitStruct(values.ToArray());
            this._configurationUnion.Set(this._currentSetpointsStruct);
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            string message;
            if (this._configurationUnion.Check(out message, true))
            {
                ConfigurationStructV55 currentStruct = this._configurationUnion.Get();
                this._saveConfigurationDlg.FileName = string.Format("��5_v55_�������_������ {0}.xml", this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName, currentStruct, MR5);
                }

            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ���������.");
            }  
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }


        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Serialize(string binFileName,StructBase config, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));


                ushort[] values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS",head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this._device.MB.IsPortInvalid)
            {
                if (MessageBox.Show("�������� ������������ �� 5.55?", "������ ������������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this._exchangeProgressBar.Value = 0;
                    if (this.WriteConfiguration())
                    {
                        this._configuration.Value = this._currentSetpointsStruct;
                        this._statusLabel.Text = "��� ������ ������������";
                        this.IsProcess = true;
                        this._configuration.SaveStruct();
                    }
                }
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }  
        }
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }
    
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartReadConfiguration();
            this.ResetSetpoints(false);
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            this._configurationUnion.Set(this._configuration.Value);
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            if (!this._device.MB.IsPortInvalid)
            {
                this.IsProcess = true;
                this._configuration.LoadStruct();
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }
        
        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(true);
        }

        private void ResetSetpoints(bool isDialog)   //�������� ������� ������� �� �����, isDialog - ���������� ����� �� ������
        {
            try
            {
                if (isDialog)
                {
                    DialogResult res = MessageBox.Show(@"��������� ������� �������?", @"������� �������",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (res == DialogResult.No) return;
                }

                this.Deserialize(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR5v55_BASE_CONFIG_PATH));
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                        "��������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this._configurationUnion.Reset();
                }
                else
                {
                    this._configurationUnion.Reset();
                }
            }
        }
        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            if (this.WriteConfiguration())
            {
                this._statusLabel.Text = "���� ������ ������������ � HTML";
                ExportGroupForm exportGroup = new ExportGroupForm();
                if (exportGroup.ShowDialog() != DialogResult.OK)
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                this._currentSetpointsStruct.DeviceVersion = this._device.DeviceVersion;
                this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                this._currentSetpointsStruct.TimeRet = this._dispepairReleDurationBox.Text;
                this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                string fileName;

                if (exportGroup.SelectedGroup == ExportStruct.ALL)
                {
                    fileName = string.Format("{0} ������� ������ {1} ��� ������", "��5", this._device.DeviceVersion);
                    this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_55_All_Conf, fileName, this._currentSetpointsStruct);
                }
                else
                {
                    fileName = string.Format("{0} ������� ������ {1} ������ {2}", "��5", this._device.DeviceVersion, this._currentSetpointsStruct.Group);
                    this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_55_Conf, fileName, this._currentSetpointsStruct);
                }
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            this._statusLabel.Text = "�������� �������";
            this._statusStrip.Update();
            if (this._configurationUnion.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationUnion.Get();
                return true;
            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                return false;
            }
        }

        private void oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.oscLenBox.Text = (21162 * 2 / (this._oscLengthCmb.SelectedIndex + 2)).ToString();
        }
        #endregion Misc

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr5V55ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(false);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
            }
        }

        private void Mr550ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;
            if (this._setpointUnion.Check(out message, true))
            {
                SetpointStructV55[] allSetpoints = this.CopySetpoints<AllSetpointsStructV55, SetpointStructV55>(this._setpointUnion, this._setpointsValidator);
                this._currentSetpointsStruct.AllSetpoints.Setpoints = allSetpoints;
                this._setpointsValidator.Set(this._currentSetpointsStruct.AllSetpoints);
                MessageBox.Show("����������� ���������");
            }
            else
            {
                MessageBox.Show("���������� ������������ �������");
            }
        }
        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this._copySetpoinsGroupComboBox.SelectedItem == StringsConfig.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }
    }
}
