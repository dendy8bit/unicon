﻿using BEMN.Devices;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v55.AlarmJournal.Structures
{
    public class AlarmJournalStructV55 : StructBase
    {
        [Layout(0, Count = 32)] private AlarmJournalRecordStructV55[] _records;

        public AlarmJournalRecordStructV55[] Records
        {
            get { return this._records; }
        }

        public override object GetSlots(ushort start, bool slotArray, int len)
        {
            int arrayLength = 32;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + 28));
                start += 64;
            }

            return slots;
        }
    }
}
