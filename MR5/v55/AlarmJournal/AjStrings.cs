﻿using System.Collections.Generic;

namespace BEMN.MR5.v55.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Основная",
                        "Резервная"
                    };
            }
        }
        /// <summary>
        /// Сработавшая ступень
        /// </summary>
        public static List<string> Stage
        {
            get
            {
                return new List<string>
                    {
                        "",
                        "I>",
                        "I>>",
                        "I>>>",
                        "I>>>>",
                        "I2>",
                        "I2>>",
                        "I0>",
                        "I0>>",
                        "In>",
                        "In>>",
                        "Iг>",
                        "I2/I1", //12
                        "13",
                        "14",
                        "15",
                        "16",
                        "17",
                        "18",
                        "19",
                        "20",
                        "21",
                        "22",
                        "23",
                        "24",
                        "ВЗ-1",
                        "ВЗ-2",
                        "ВЗ-3",
                        "ВЗ-4",
                        "ВЗ-5",
                        "ВЗ-6",
                        "ВЗ-7",
                        "ВЗ-8",
                    };
            }
        }
        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "Журнал пуст",
                        "Сигнализация",
                        "Отключение",
                        "Работа",
                        "Неуспешное АПВ",
                        "Возврат",
                        "Включение",
                        "Резерв",
                    
                    };
            }
        }

        /// <summary>
        /// Параметр срабатывания
        /// </summary>
        public static List<string> Parametr
        {
           get
           {
               return new List<string>
                   {
                       "", // 0
                       "Iг", // 1
                       "In", // 2
                       "Ia", // 3
                       "Ib", // 4
                       "Ic", // 5
                       "I0", // 6
                       "I1", // 7
                       "I2", // 8
                       "9", // 9
                       "10", // 10
                       "11", // 11
                       "12", // 12
                       "13", // 13
                       "14", // 14
                       "15", // 15
                       "16", // 16
                       "17", // 17
                       "18 ", // 18
                       "19 ", // 19
                       "20 ", // 20
                       "21 ", // 21
                       "22", // 22
                       "23", // 23
                       "24", // 24
                       "25", // 25
                       "26", // 26
                       "I2/I1", // 27
                       "28",
                       "29"
                   };
           }
        }

    }
}
