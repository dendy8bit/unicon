﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v55.AlarmJournal.Structures;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v55.AlarmJournal
{
    public partial class Mr5V55AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР5_v55_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        private const string FILE_NAME_PATTERN = "Журнал аварий МР5 v{0}";
        #endregion [Constants]


        #region [Private fields]
        private DataTable _table;
        private readonly MemoryEntity<AlarmJournalStructV55> _alarmRecord;
        private readonly MemoryEntity<MeasureTransStructV55> _measuringChannel;
        private MR5Device _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr5V55AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr5V55AlarmJournalForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmRecord = device.AlarmJournal55;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            
            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.MeasuringAlarmV55;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);

            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            this._configProgressBar.Maximum = this._alarmRecord.Slots.Count;
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
        }
        private void JournalReadFail()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
        }
        private void ReadJournalOk()
        {
            int i = 1;
            var measure = this._measuringChannel.Value;
            var version = Common.VersionConverter(this._device.DeviceVersion);
            foreach (var record in this._alarmRecord.Value.Records)
            {
                if (record.IsEmpty)
                {
                    continue;
                }
              
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Stage,
                        record.TypeDamage,
                        record.WorkParametr,
                        record.ValueParametr(measure),
                        record.GroupOfSetpoints(version),
                        ValuesConverterCommon.Analog.GetISmall(record.Ia, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ib, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ic, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I0, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I1, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I2, measure.Tt * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.In, measure.Ttnp * 5),
                        ValuesConverterCommon.Analog.GetISmall(record.Ig, measure.Ttnp * 5),
             record.Discret
                        
                    );
                i++;
            }
            this._alarmJournalGrid.DataSource = this._table;
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._alarmJournalGrid.Update();
            this.Reading = false;
        }

        private DataTable GetJournalDataTable()
         {
             var table = new DataTable(TABLE_NAME);
             for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
             {
                 table.Columns.Add(this._alarmJournalGrid.Columns[j].Name);
             }
             return table;
         }
        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr5V55AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this.Reading = true;
            this._measuringChannel.LoadStruct();
        }

        private bool Reading
        {
            set
            {
                this._readAlarmJournalButton.Enabled = !value;
                this._saveAlarmJournalButton.Enabled = !value;
                this._loadAlarmJournalButton.Enabled = !value;
                this._exportButton.Enabled = !value;
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this._openAlarmJournalDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                    AlarmJournalStructV55 journal = new AlarmJournalStructV55();
                    MeasureTransStructV55 measuring = new MeasureTransStructV55();
                    int measSize = measuring.GetSize();
                    int journalSize = journal.GetSize();
                    byte[] buffer = file.Take(file.Length - measSize).ToArray();
                    List<byte> journalBytes = new List<byte>(Common.SwapArrayItems(buffer));
                    if (journalBytes.Count != journalSize)
                    {
                        journalBytes.AddRange(new byte[journalSize - journalBytes.Count]);
                    }
                    journal.InitStruct(journalBytes.ToArray());
                    this._alarmRecord.Value = journal;

                    buffer = file.Skip(file.Length - measSize).ToArray();
                    List<byte> measuringBytes = new List<byte>(Common.SwapArrayItems(buffer));
                    if (measuringBytes.Count != measSize)
                    {
                        measuringBytes.AddRange(new byte[measSize - measuringBytes.Count]);
                    }
                    measuring.InitStruct(measuringBytes.ToArray());
                    this._measuringChannel.Value = measuring;
                    this.ReadJournalOk();
                }
                else
                {
                    this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
                }
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            this._saveAlarmJournalDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        #endregion [Event Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            this._saveJournalHtmlDialog.FileName = string.Format(FILE_NAME_PATTERN, this._device.DeviceVersion);
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR5_55_AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
