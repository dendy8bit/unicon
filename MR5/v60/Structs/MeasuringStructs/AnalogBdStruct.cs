﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;

namespace BEMN.MR5.v60.Structs.MeasuringStructs
{
    public class AnalogBdStruct60 : StructBase
    {
        [Layout(0, Count = 11)]public ushort[] _analogValues;

        private ushort GetMean(List<AnalogBdStruct60> list, Func<AnalogBdStruct60, ushort> func)
        {
            int count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }


        public string GetF(List<AnalogBdStruct60> list)
        {
            ushort value = this.GetMean(list, o => o._analogValues[10]);
            return ValuesConverterCommon.Analog.GetF(value);
        }
        public string GetUa(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[1]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUb(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[2]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUc(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[3]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUab(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[4]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUbc(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[5]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUca(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[6]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }
        public string GetU0(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[7]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetU1(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[8]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetU2(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[9]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorU);
        }

        public string GetUn(List<AnalogBdStruct60> list, RomMeasuringStruct60 measure)
        {
            ushort value = this.GetMean(list, o => o._analogValues[0]);
            return ValuesConverterCommon.Analog.GetU(value, measure.RomFactorUo);
        } 
    }
}
