﻿using System.Collections;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR5.v60.Structs.MeasuringStructs
{
    public class DiscretBdStruct60 : StructBase
    {
        [Layout(0, Count = 15)]private ushort[] _array; //15
        
        /// <summary>
        /// Индикаторы
        /// </summary>
        public BitArray Indicators
        {
            get
            {
                bool[] values = new[]
                {
                    Common.GetBit(this._array[2], 8),
                    Common.GetBit(this._array[2], 9),
                    Common.GetBit(this._array[2], 10),
                    Common.GetBit(this._array[2], 11),
                    Common.GetBit(this._array[2], 12),
                    Common.GetBit(this._array[2], 13),
                    Common.GetBit(this._array[2], 14),
                    Common.GetBit(this._array[2], 15),
                    Common.GetBit(this._array[2], 5),
                    Common.GetBit(this._array[2], 4),
                    Common.GetBit(this._array[2], 2), // реле аварии
                    Common.GetBit(this._array[2], 3)  // реле сигнал-ции
                };
                return new BitArray(values);
            }
        }
        /// <summary>
        /// Реле
        /// </summary>
        public BitArray Relay
        {
            get { return new BitArray(Common.TOBYTES(new[]{this._array[3]}, false)); }
        }

        /// <summary>
        /// Состояние неисправности 1
        /// </summary>
        public BitArray Fault1
        {
            get
            {
                var array = new[]
                    {
                        Common.GetBit(this._array[4], 0),
                        Common.GetBit(this._array[4], 2),
                        Common.GetBit(this._array[4], 6),
                        Common.GetBit(this._array[4], 7)
                    };
                return new BitArray(array);
            }
        }

        /// <summary>
        /// Состояние неисправности 2
        /// </summary>
        public BitArray Fault2
        {
            get
            {
                var array = new[]
                    {
                        Common.GetBit(this._array[5], 0),
                        Common.GetBit(this._array[5], 1),
                        Common.GetBit(this._array[5], 2),
                        Common.GetBit(this._array[5], 3),
                        Common.GetBit(this._array[5], 4),
                        Common.GetBit(this._array[5], 5),
                        Common.GetBit(this._array[5], 6),
                        Common.GetBit(this._array[5], 7),
                        Common.GetBit(this._array[5], 8),
                        Common.GetBit(this._array[5], 9),
                        Common.GetBit(this._array[5], 11),
                        Common.GetBit(this._array[5], 12),
                        Common.GetBit(this._array[5], 13),
                        Common.GetBit(this._array[5], 14),
                        Common.GetBit(this._array[5], 15)
                    };
                return new BitArray(array);
            }
        }
        /// <summary>
        /// Д1-Д8
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                var array = new[]
                    {
                        Common.GetBit(this._array[8], 8),
                        Common.GetBit(this._array[8], 9),
                        Common.GetBit(this._array[8], 10),
                        Common.GetBit(this._array[8], 11),
                        Common.GetBit(this._array[8], 12),
                        Common.GetBit(this._array[8], 13),
                        Common.GetBit(this._array[8], 14),
                        Common.GetBit(this._array[8], 15)
                    };
                return new BitArray(array);
            }
        }
        /// <summary>
        /// Л1-Л8, ВЛС1 -ВЛС8
        /// </summary>
        public BitArray InputLogicSignals
        {
            get
            {
                 return new BitArray(Common.TOBYTES(new[]{this._array[9]}, true)); 
            }
        }

        /// <summary>
        /// Umax, Umin
        /// </summary>
        public BitArray USignals
        {
            get
            {
                return new BitArray(Common.TOBYTES(new[] { this._array[10] }, true));
            }
        }

        /// <summary>
        /// U0, U1, U2
        /// </summary>
        public BitArray U012Signals
        {
            get
            {
                return new BitArray(Common.TOBYTES(new[] { this._array[11] }, true));
            }
        }

        /// <summary>
        /// Fmax, Fmin
        /// </summary>
        public BitArray FSignals
        {
            get
            {
                return new BitArray(Common.TOBYTES(new[] { this._array[12] }, true));
            }
        }

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public BitArray ExternalDefenses
        {
            get
            {
                var array = new[]
                    {
                        Common.GetBit(this._array[13], 0),
                        Common.GetBit(this._array[13], 1),
                        Common.GetBit(this._array[13], 2),
                        Common.GetBit(this._array[13], 3),
                        Common.GetBit(this._array[13], 4),
                        Common.GetBit(this._array[13], 5),
                        Common.GetBit(this._array[13], 6),
                        Common.GetBit(this._array[13], 7)
                    };
                return new BitArray(array);
            }
        }

        /// <summary>
        /// Управляющие сигналы(СДТУ)
        /// </summary>
        public BitArray ManageSignals
        {
            get
            {
                var array = new[]
                    {
                        !Common.GetBit(this._array[0], 3), //основная группа уставок
                        Common.GetBit(this._array[0], 3), //Резервная группа уставок 
                        Common.GetBit(this._array[0], 5), //Наличие неисправностей
                        Common.GetBit(this._array[0], 6), //Новая запись ЖС
                        Common.GetBit(this._array[0], 7),  //Новая запись ЖА
                        Common.GetBit(this._array[7], 3),  //Uabc < 5V
                        Common.GetBit(this._array[0], 9),  //Un < 5V
                    };
                return new BitArray(array);
            }
        }
        /// <summary>
        /// Сигналы ССЛ
        /// </summary>
        public BitArray SPL
        {
            get
            {
                List<byte> mas = new List<byte>();
                mas.Add(Common.HIBYTE(_array[13]));
                mas.Add(Common.TOBYTE(_array[14])[1]);
                mas.Add(Common.TOBYTE(_array[14])[0]);
                return new BitArray(mas.ToArray());
            }
        }
    }
}
