﻿using BEMN.Devices;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v60.Structs.JournalStructs
{
    public class AlarmJournalStruct60 : StructBase
    {
        [Layout(0, Count = 32)] private AlarmJournalRecordStruct60[] _records;

        public AlarmJournalRecordStruct60[] Records
        {
            get { return this._records; }
        }

        #region [IStruct Members]
        
        public override object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            int arrayLength = 32;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + 28));
                start += 64;
            }

            return slots;
        }
        #endregion
    }
}
