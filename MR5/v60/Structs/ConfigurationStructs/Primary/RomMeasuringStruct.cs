﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs
{
    /// <summary>
    /// измерительный канал 12 байт (1)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RomMeasuringStruct : IStruct, IStructInit
    {
        #region [Private fields]
        private ushort _romConfigU; //конфигурация ТН       РЕЗЕРВ    2 байта
        private ushort _romFactorU; //коэфициент ТН                  2 байта
        private ushort _romErrorsU; //вн.неисправность ТН            2 байта
        private ushort _romFactorUo; //коэфициент ТННП                2 байта
        private ushort _romErrorsUo; //вн.неисправность ТННП          2 байта
        //ключи свободно программируемой логики
        private ushort _romFreeLogicKey; //ключи для задачи свободно программируемой логики 2 байта 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// коэфициент ТН 
        /// </summary>
        public double RomFactorU
        {
           /* get { return Math.Round(_romFactorU /(double) 256,2) ; }
            set { _romFactorU = (ushort) (value * 256); }*/
            get { return ValuesConverterCommon.GetFactor(_romFactorU); }
            set { _romFactorU = ValuesConverterCommon.SetFactor(value); }
        }
        /// <summary>
        /// вн.неисправность ТН 
        /// </summary>
        public ushort RomErrorsU
        {
            get { return _romErrorsU; }
            set { _romErrorsU = value; }
        }

        /// <summary>
        /// коэфициент ТННП
        /// </summary>
        public double RomFactorUo
        {
            get { return ValuesConverterCommon.GetFactor(_romFactorUo); /*Math.Round(_romFactorUo/ (double) 256,2);*/ }
            set { _romFactorUo = ValuesConverterCommon.SetFactor(value); /*(ushort)(value * 256);*/ }
        }

        /// <summary>
        /// вн.неисправность ТННП 
        /// </summary>
        public ushort RomErrorsUo
        {
            get { return _romErrorsUo; }
            set { _romErrorsUo = value; }
        }
        /// <summary>
        /// ключи для задачи свободно программируемой логики
        /// </summary>
        public string RomFreeLogicKey
        {
            get
            {
                return LoadKeys(new ushort[]{_romFreeLogicKey});
            }
            set
            {
                _romFreeLogicKey = SetKeys(value);
            }
        }
        
        #endregion [Properties]


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._romConfigU = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFactorU = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romErrorsU = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFactorUo = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romErrorsUo = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFreeLogicKey = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._romConfigU,
                    this._romFactorU,
                    this._romErrorsU,
                    this._romFactorUo,
                    this._romErrorsUo,
                    this._romFreeLogicKey

                };


        }

        #endregion [IStructInit Members]

        public string LoadKeys(ushort[] masInputSignals)
        {
            byte[] buffer1 = Common.TOBYTES(masInputSignals, false);
            byte[] b1 = new byte[] { buffer1[0] };
            byte[] b2 = new byte[] { buffer1[1] };
            string r1 = Common.BitsToString(new BitArray(b1));
            string r2 = Common.BitsToString(new BitArray(b2));
            string res = r1 + r2;
            return res;
        }
        public ushort SetKeys(string masInputSignals)
        {
            return Common.BitsToUshort(Common.StringToBits(masInputSignals));
        }
    }

}
