﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// конфигурации защит частоты обе группы уставок (7)
    /// </summary>

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AllFrequencyDefenseConfigurationStruct : IStruct, IStructInit
    {
        #region [Private fields]
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
        public FrequencyDefenseConfigurationStruct[] FrequencyDefenseConfiguration; 
        #endregion [Private fields]


        #region [Ctor's]
        public AllFrequencyDefenseConfigurationStruct(bool a)
        {
            this.FrequencyDefenseConfiguration = new FrequencyDefenseConfigurationStruct[2];
            for (int i = 0; i < this.FrequencyDefenseConfiguration.Length; i++)
            {
                this.FrequencyDefenseConfiguration[i] = new FrequencyDefenseConfigurationStruct(a);
            }
        } 
        #endregion [Ctor's]


        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.FrequencyDefenseConfiguration = new FrequencyDefenseConfigurationStruct[2];
            for (int i = 0; i < this.FrequencyDefenseConfiguration.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(FrequencyDefenseConfigurationStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.FrequencyDefenseConfiguration[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(FrequencyDefenseConfigurationStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var defense in FrequencyDefenseConfiguration)
            {
                result.AddRange(defense.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}
