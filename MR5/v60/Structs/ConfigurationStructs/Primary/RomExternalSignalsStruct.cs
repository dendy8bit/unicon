﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MBServer;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// внешние сигналы 8 байт (2)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RomExternalSignalsStruct : IStruct, IStructInit
    {
        private ushort _romInpClear; //вн.сигн.сброса сигнализации    2 байта
        private ushort _romInpGroup; //вн.сигн.группы уставок         2 байта
        public FaultSygnalStruct RomDispepair; //сигнал НЕИСПРАВНОСТЬ           4 байт

        /// <summary>
        /// Сброс индикации
        /// </summary>
        public ushort ResetIndication
        {
            get { return this._romInpClear; }
            set { this._romInpClear = value; }
        }
        /// <summary>
        /// Переключение на резервные уставки
        /// </summary>
        public ushort ReservSetpoints
        {
            get { return this._romInpGroup; }
            set { this._romInpGroup  = value; }
        }


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._romInpClear = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romInpGroup = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this.RomDispepair = new FaultSygnalStruct();
            var oneStruct = new byte[Marshal.SizeOf(typeof(FaultSygnalStruct))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.RomDispepair.InitStruct(oneStruct);
          //  index += Marshal.SizeOf(typeof(FaultSygnalStruct));

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>
                {
                    this._romInpClear,
                     this._romInpGroup
                };

            result.AddRange(this.RomDispepair.GetValues());
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}
