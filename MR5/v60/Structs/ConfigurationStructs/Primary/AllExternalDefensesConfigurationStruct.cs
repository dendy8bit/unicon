﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MR600.New.Structs.ConfigurationStructs.Second;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Primary
{
    /// <summary>
    /// конфигурации внешних защит, обе группы уставок (5)
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct AllExternalDefensesConfigurationStruct
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)] 
        public ExternalDefensesConfigurationStruct[] ExternalDefensesConfiguration;

        public AllExternalDefensesConfigurationStruct(bool a)
        {
            this.ExternalDefensesConfiguration = new ExternalDefensesConfigurationStruct[2];
            for (int i = 0; i < this.ExternalDefensesConfiguration.Length; i++)
            {
                this.ExternalDefensesConfiguration[i] = new ExternalDefensesConfigurationStruct(a);
            }
        }

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.ExternalDefensesConfiguration = new ExternalDefensesConfigurationStruct[2];
            for (int i = 0; i < this.ExternalDefensesConfiguration.Length; i++)
            {
                var oneStruct = new byte[Marshal.SizeOf(typeof(ExternalDefensesConfigurationStruct))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.ExternalDefensesConfiguration[i].InitStruct(oneStruct);
                index += Marshal.SizeOf(typeof(ExternalDefensesConfigurationStruct));
            }

        }

        public ushort[] GetValues()
        {
            var result = new List<ushort>();
            foreach (var defense in ExternalDefensesConfiguration)
            {
                result.AddRange(defense.GetValues());
            }
            return result.ToArray();
        }

        #endregion [IStructInit Members]
    }
}