﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    /// <summary>
    /// конфигурации напряженьческих защит 
    /// </summary>
     [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VoltageDefenseConfigurationStruct : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)] 
        public VoltageDefenseStruct[] VoltageDefenses;

         public VoltageDefenseConfigurationStruct(bool a)
         {
             this.VoltageDefenses = new VoltageDefenseStruct[16];
         }

         #region [IStruct Members]
         public StructInfo GetStructInfo()
         {
             return StructHelper.GetStructInfo(this.GetType());
         }

         public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
         {
             return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
         }
         #endregion [IStruct Members]


         #region [IStructInit Members]
         public void InitStruct(byte[] array)
         {
             int index = 0;
             this.VoltageDefenses = new VoltageDefenseStruct[16];
             for (int i = 0; i < this.VoltageDefenses.Length; i++)
             {
                 var oneStruct = new byte[Marshal.SizeOf(typeof(VoltageDefenseStruct))];
                 Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                 this.VoltageDefenses[i].InitStruct(oneStruct);
                 index += Marshal.SizeOf(typeof(VoltageDefenseStruct));
             }

         }

         public ushort[] GetValues()
         {
             var result = new List<ushort>();
             foreach (var defense in VoltageDefenses)
             {
                 result.AddRange(defense.GetValues());
             }
             return result.ToArray();
         }

         #endregion [IStructInit Members]
    }
}
