﻿using System;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MR600.New.Structs.ConfigurationStructs.Second
{
    public struct LogicSignalStruct : IStruct, IStructInit
    {
        private ushort _logicValues;
        private ushort _reserv;

        public int[] LogicValues
        {
            get {
                int temp = this._logicValues;
                var result = new int[8];
                for (int i = 0; i < 8; i++)
                {
                    result[i] = (Common.GetBit(this._logicValues, i) ? 1 : 0) + (Common.GetBit(this._logicValues, i+8) ? 1 : 0);
               
                }
                return result;
            }
            set
            {
                ushort result = 0; 
                for (int i = 0; i < 8; i++)
                {
                    result = Common.SetBit(result, i, value[i] > 0);
                    result = Common.SetBit(result, i+8, value[i] > 1);
                }
                this._logicValues = result;
            }
        }

        #region [IStruct Members]

        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }

        #endregion [IStruct Members]


        #region [IStructInit Members]

        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._logicValues = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof (UInt16);
            this._reserv = Common.TOWORD(array[index + 1], array[index]);

        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._logicValues,
                    this._reserv
                };
        }

        #endregion [IStructInit Members]
    }
}