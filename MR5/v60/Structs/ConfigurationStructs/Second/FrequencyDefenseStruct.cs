﻿using System;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using BEMN.MR600.New.HelpClasses;

namespace BEMN.MR600.New.Structs.ConfigurationStructs
{
    /// <summary>
    /// защиты частоты 16 байт
    /// </summary>
    public struct FrequencyDefenseStruct : IStruct, IStructInit
    {
        private ushort _romFConfig; //EQU     0       ;конфигурация                                   2 байта
        private ushort _romFBlock; //EQU     2       ;номер входа блокировки                         2 байта
        private ushort _romFValue; //EQU     4       ;уставка срабатывания                           2 байта
        private ushort _romFWait; //EQU     6       ;выдержка времени срабатывания                  2 байта
        private ushort _romFValueAdd; //EQU     8       ;уставка возврата                               2 байта
        private ushort _romFWaitAdd; //EQU     10      ;выдержка времени возврата                      2 байта
        private ushort _reserv1;
        private ushort _reserv2;

        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        public string Mode
        {
            get
            {
                var index = Common.GetBits(this._romFConfig, 0, 1);
                return StringsV2.VoltageDefenseMode[index];
            }
            set
            {
                var index = (ushort)StringsV2.VoltageDefenseMode.IndexOf(value);
                this._romFConfig = Common.SetBits(this._romFConfig, index, 0, 1);
            }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        public string RomExtBlock
        {
            get { return StringsV2.ExternalDefensesSignals[this._romFBlock]; }
            set { this._romFBlock = (ushort)StringsV2.ExternalDefensesSignals.IndexOf(value); }
        }
        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        public double RomExtInput
        {
            get
            {
                var result =Math.Round(this._romFValue / (double)256, 2);
                if (result < 40)
                {
                    return 40;
                }
                  if (result > 60)
                {
                    return 60;
                }
                return result;
            }
            set { _romFValue = (ushort)Math.Round(value * 256); }
        }
        /// <summary>
        /// Время срабатывания
        /// </summary>
        public int RomExtWait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._romFWait); }
            set { this._romFWait = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        public bool Recovery
        {
            get { return Common.GetBit(this._romFConfig, 14); }
            set { this._romFConfig = Common.SetBit(this._romFConfig, 14, value); }
        }
        /// <summary>
        /// Уставка возврата
        /// </summary>
        public double RomExtInputAdd
        {
            get
            {
                var result = Math.Round(this._romFValueAdd / (double)256, 2);
                if (result < 40)
                {
                    return 40;
                }
                if (result > 60)
                {
                    return 60;
                }
                return result;
            }
            set { _romFValueAdd = (ushort)Math.Round(value * 256); }
        }
        /// <summary>
        /// Время возврата
        /// </summary>
        public int RomExtWaitAdd
        {
            get { return ValuesConverterCommon.GetWaitTime(_romFWaitAdd); }
            set { this._romFWaitAdd = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Осциллограф
        /// </summary>
        public string Osc
        {
            get { return StringsV2.VoltageDefenseOsc[(Common.GetBits(this._romFConfig, 3, 4) >> 3)]; }
            set
            {
                var osc = (ushort)StringsV2.VoltageDefenseOsc.IndexOf(value);
                this._romFConfig = Common.SetBits(this._romFConfig, osc, 3, 4);
            }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        public bool Reset
        {
            get { return Common.GetBit(this._romFConfig, 13); }
            set { this._romFConfig = Common.SetBit(this._romFConfig, 13, value); }
        }
        #endregion [Properties]


        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool SlotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, SlotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]


        #region [IStructInit Members]
        public void InitStruct(byte[] array)
        {
            int index = 0;

            this._romFConfig = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFBlock = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFValue = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFWait = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFValueAdd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._romFWaitAdd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._reserv1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(UInt16);

            this._reserv2 = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            return new[]
                {
                    this._romFConfig,
                    this._romFBlock,
                    this._romFValue,
                    this._romFWait,
                    this._romFValueAdd,
                    this._romFWaitAdd,
                    this._reserv1,
                    this._reserv2
                };


        }

        #endregion [IStructInit Members]
    }
}