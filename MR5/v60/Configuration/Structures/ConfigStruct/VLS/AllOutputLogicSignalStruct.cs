﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.VLS
{
    /// <summary>
    /// все выходные логические сигналы
    /// </summary>
    public class AllOutputLogicSignalStruct60 : StructBase, IXmlSerializable
    {
        public const int VLS_COUNT = 8;

        [Layout(0, Count = VLS_COUNT)]
        private OutputLogicSignalStruct60[] _vlsStruct;

        [BindingProperty(0)]
        [XmlIgnore]
        public OutputLogicSignalStruct60 this[int index]
        {
            get { return _vlsStruct[index]; }
            set { _vlsStruct[index] = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < VLS_COUNT; i++)
            {

                writer.WriteStartElement(string.Format("ВЛС"));
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}