﻿using System;
using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.VLS
{
    /// <summary>
    /// выходные логические сигналы
    /// </summary>
    public class OutputLogicSignalStruct60 : StructBase, IBitField, IXmlSerializable
    {
        #region [Constants]
        public const int LOGIC_COUNT = 8;
        #endregion [Constants]

        [Layout(0, Count = LOGIC_COUNT)] private ushort[] _logicSignals;
        public  BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this._logicSignals, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i/16;
                    int y = i%16;
                    this._logicSignals[x] = Common.SetBit(this._logicSignals[x], y, value[i]);
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            bool flag = false;
            for (int i = 0; i < ListStrings.OutputLogicSignals.Count; i++)
            {
                if (this.Bits[i])
                {
                    flag = true;
                    writer.WriteElementString("Выбрано", ListStrings.OutputLogicSignals[i]);
                }

            }
            if (!flag)
            {
                writer.WriteElementString("Выбрано", "НЕТ");
            }
        }
    }

}