﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay
{
    /// <summary>
    /// Выходные реле
    /// </summary>
    public class OutputRelayStruct60 : StructBase
    {
        [Layout(0, Count = 16)] private RelayStruct60[] _relays;

        public RelayStruct60[] Relays
        {
            get { return _relays; }
            set { _relays = value; }
        }
    }
}
