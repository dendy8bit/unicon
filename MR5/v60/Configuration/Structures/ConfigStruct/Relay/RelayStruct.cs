﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay
{
    /// <summary>
    /// Структура одного реле
    /// </summary>
    public class RelayStruct60 : StructBase
    {
        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _waitTime;

        /// <summary>
        /// Тип реле
        /// </summary>
        [BindingProperty(0)]
        public string RelayType
        {
            get { return Validator.Get(this._signal, ListStrings.OutputSignalsType, 15); }
            set { this._signal = Validator.Set(value, ListStrings.OutputSignalsType, this._signal, 15); }
        }
        /// <summary>
        /// Сигнал реле
        /// </summary>
        [BindingProperty(1)]
        public string Signal
        {
            get
            {
                return Validator.Get(this._signal, ListStrings.RelayIndSignals, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                    14);
            }
            set
            {
                this._signal = Validator.Set(value, ListStrings.RelayIndSignals, this._signal, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
                    10, 11, 12, 13, 14);
            }
        }
        /// <summary>
        /// Импульс
        /// </summary>
        [BindingProperty(2)]
        public int WaitTime
        {
            get { return ValuesConverterCommon.GetWaitTime(this._waitTime); }
            set { this._waitTime = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
