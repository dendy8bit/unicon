﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay
{
    public class AllRelaysStruct60 : StructBase, IDgvRowsContainer<RelayStruct60>
    {
        #region Fields
        private OutputAddedRelaysStruct60 _addRelays;
        private OutputRelayStruct60 _relays;
        #endregion

        #region Constructors

        public AllRelaysStruct60() 
        {
            this._addRelays = new OutputAddedRelaysStruct60();
            this._relays = new OutputRelayStruct60();
        }
        public AllRelaysStruct60(OutputAddedRelaysStruct60 aR, OutputRelayStruct60 r)
        {
            this._addRelays = aR.Clone<OutputAddedRelaysStruct60>();
            this._relays = r.Clone<OutputRelayStruct60>();
        }
        #endregion

        #region Properties
        public RelayStruct60[] Rows
        {
            get
            {
                List<RelayStruct60> list = new List<RelayStruct60>();
                list.AddRange(this.AddRelays.Relay);
                list.AddRange(this.Relays.Relays);
                return list.ToArray();
            }
            set
            {
                this.AddRelays.Relay = new[] {value[0], value[1]};
                this.Relays.Relays = value.Skip(2).ToArray();
            }
        }

        public OutputAddedRelaysStruct60 AddRelays
        {
            get { return this._addRelays; }
        }

        public OutputRelayStruct60 Relays
        {
            get { return this._relays; }
        }
        #endregion
    }
}
