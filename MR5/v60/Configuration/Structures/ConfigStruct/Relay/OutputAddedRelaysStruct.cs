﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay
{
    /// <summary>
    /// выходные дополнительные реле
    /// </summary>
    public class OutputAddedRelaysStruct60 : StructBase
    {
        [Layout(0, Count = 2)] private RelayStruct60[] _relay;

        public RelayStruct60[] Relay
        {
            get { return _relay; }
            set { _relay = value; }
        }
    }
}