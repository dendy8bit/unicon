﻿using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Indicators
{
    /// <summary>
    /// индикаторы
    /// </summary>
    public class AllIndicatorsStruct60 : StructBase, IDgvRowsContainer<IndicatorsStruct60>
    {
        public const int COUNT_INDICATORS = 10;
        [Layout(0, Count = COUNT_INDICATORS)]
        private IndicatorsStruct60[] _indicators;

        public IndicatorsStruct60[] Rows
        {
            get
            {
                List<IndicatorsStruct60>ret = new List<IndicatorsStruct60>();
                ret.AddRange(this._indicators.Skip(2));
                ret.AddRange(this._indicators.Take(2));
                return ret.ToArray();
            }
            set
            {
                _indicators = value;
            }
        }
    }
}
