﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    /// <summary>
    /// внешние сигналы
    /// </summary>
    public class RomExternalSignalsStruct60 : StructBase
    {
        [Layout(0)] private ushort _romInpClear; //вн.сигн.сброса сигнализации
        [Layout(1)] private ushort _romInpGroup; //вн.сигн.группы уставок

        /// <summary>
        /// Сброс индикации
        /// </summary>
        [BindingProperty(0)]
        public string ResetIndication
        {
            get { return Validator.Get(_romInpClear, ListStrings.InputSignals); }
            set { _romInpClear = Validator.Set(value, ListStrings.InputSignals); }
        }

        /// <summary>
        /// Переключение на резервные уставки
        /// </summary>
        [BindingProperty(1)]
        public string ReservSetpoints
        {
            get { return Validator.Get(_romInpGroup, ListStrings.InputSignals); }
            set { _romInpGroup = Validator.Set(value, ListStrings.InputSignals); }
        }
    }
}
