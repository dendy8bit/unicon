﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    public class OscilloscopeConfigStruct60 : StructBase
    {
        [Layout(0)] private ushort _oscillConfig;

        [BindingProperty(0)]
        public string Code
        {
            get { return Validator.Get(_oscillConfig, StringsV2.OsLenMode, 0, 1, 2); }
            set { _oscillConfig = Validator.Set(value, StringsV2.OsLenMode,_oscillConfig, 0, 1, 2); }
        }
        [BindingProperty(1)]
        public string Fixation
        {
            get { return Validator.Get(_oscillConfig, StringsV2.OscFicsation, 7); }
            set { _oscillConfig = Validator.Set(value, StringsV2.OscFicsation,_oscillConfig, 7); }
        }
        [BindingProperty(2)]
        public ushort PeriodPercent
        {
            get { return (ushort) (Common.GetBits(_oscillConfig, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { _oscillConfig = Common.SetBits(_oscillConfig, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
    }
}
