﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.FaultSignals;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.ILS;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.VLS;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Indicators;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.OscConfig;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    public class RamMemoryStruct60 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР5"; } set { } }


        [Layout(0)] private RomMeasuringStruct60 _romMeasuring;
        [Layout(1)] private KeysStruct60 _keys;
        [Layout(2)] private RomExternalSignalsStruct60 _romExternalSignals;
        [Layout(3)] private AllFaultSignalsStruct60 _faultSignal;
        [Layout(4)] private InputLogicParametresStruct60 _inputLogicParametres;
        [Layout(5)] private OutputAddedRelaysStruct60 _outputAddedRelays;
        [Layout(6)] private ResetStepStruct60 _resetStep;
        [Layout(7, Ignore = true)] private ushort _portInterface;
        [Layout(8)] private AllSetpointStruct60 _allSetpoint;
        [Layout(9)] private AllOutputLogicSignalStruct60 _allOutputLogicSignal;
        [Layout(10)] private OutputRelayStruct60 _outputRelays;
        [Layout(11)] private AllIndicatorsStruct60 _allIndicators;
        [Layout(12)] private OscConfigStruct60 _oscConfiguration;
        
        [BindingProperty(0)]
        public RomMeasuringStruct60 RomMeasuring
        {
            get { return _romMeasuring; }
            set { _romMeasuring = value; }
        }

        [BindingProperty(1)]
        public KeysStruct60 Keys
        {
            get { return _keys; }
            set { _keys = value; }
        }

        [BindingProperty(2)]
        public RomExternalSignalsStruct60 RomExternalSignals
        {
            get { return _romExternalSignals; }
            set { _romExternalSignals = value; }
        }
        [BindingProperty(3)]
        public AllFaultSignalsStruct60 AllFaultSignal
        {
            get { return _faultSignal; }
            set { _faultSignal = value; }
        }
        [BindingProperty(4)]
        public InputLogicParametresStruct60 InputLogicParametres
        {
            get { return _inputLogicParametres; }
            set { _inputLogicParametres = value; }
        }

        [BindingProperty(5)]
        public ResetStepStruct60 ResetStep
        {
            get { return _resetStep; }
            set { _resetStep = value; }
        }
        [BindingProperty(6)]
        public AllSetpointStruct60 AllSetpoint
        {
            get { return _allSetpoint; }
            set { _allSetpoint = value; }
        }
        [BindingProperty(7)]
        public AllOutputLogicSignalStruct60 AllOutputLogicSignal
        {
            get { return _allOutputLogicSignal; }
            set { _allOutputLogicSignal = value; }
        }
        [BindingProperty(8)]
        public AllRelaysStruct60 AllOutputRelays
        {
            get { return new AllRelaysStruct60(_outputAddedRelays, _outputRelays); }
            set
            {
                _outputAddedRelays = value.AddRelays;
                _outputRelays = value.Relays;
            }
        }
        [BindingProperty(9)]
        public AllIndicatorsStruct60 AllIndicators
        {
            get { return _allIndicators; }
            set { _allIndicators = value; }
        }
        [BindingProperty(10)]
        public OscConfigStruct60 OscConfiguration
        {
            get { return _oscConfiguration; }
            set { _oscConfiguration = value; }
        }
    }
}
