﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    public class RomMeasuringStruct60 : StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _romConfigU; //конфигурация ТН       РЕЗЕРВ   2 байта
        [Layout(1)]
        private ushort _romFactorU; //коэфициент ТН                  2 байта
        [Layout(2)]
        private ushort _romErrorsU; //вн.неисправность ТН            2 байта
        [Layout(3)]
        private ushort _romFactorUo; //коэфициент ТННП               2 байта
        [Layout(4)]
        private ushort _romErrorsUo; //вн.неисправность ТННП         2 байта
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// коэфициент ТН 
        /// </summary>
        [BindingProperty(0)]
        public double RomFactorU
        {
           /* get { return Math.Round(_romFactorU /(double) 256,2) ; }
            set { _romFactorU = (ushort) (value * 256); }*/
            get { return ValuesConverterCommon.GetFactor(_romFactorU); }
            set { _romFactorU = ValuesConverterCommon.SetFactor(value); }
        }
        /// <summary>
        /// вн.неисправность ТН 
        /// </summary>
        [BindingProperty(1)]
        public string RomErrorsU
        {
            get { return Validator.Get(_romErrorsU, ListStrings.InputSignals); }
            set { _romErrorsU = Validator.Set(value, ListStrings.InputSignals); }
        }

        /// <summary>
        /// коэфициент ТННП
        /// </summary>
        [BindingProperty(2)]
        public double RomFactorUo
        {
            get { return ValuesConverterCommon.GetFactor(_romFactorUo); /*Math.Round(_romFactorUo/ (double) 256,2);*/ }
            set { _romFactorUo = ValuesConverterCommon.SetFactor(value); /*(ushort)(value * 256);*/ }
        }

        /// <summary>
        /// вн.неисправность ТННП 
        /// </summary>
        [BindingProperty(3)]
        public string RomErrorsUo
        {
            get { return Validator.Get(_romErrorsUo, ListStrings.InputSignals); }
            set { _romErrorsUo = Validator.Set(value, ListStrings.InputSignals); }
        } 
        #endregion [Properties]
    }

}
