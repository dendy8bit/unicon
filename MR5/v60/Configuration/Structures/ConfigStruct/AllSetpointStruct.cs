﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.External;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Voltage;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Frequency;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    public class AllSetpointStruct60 : StructBase, ISetpointContainer<DefensesSetpointStruct60>
    {
        [Layout(0)] private AllExternalDefensesStruct60 _extMain;
        [Layout(1)] private AllExternalDefensesStruct60 _extRes;
        [Layout(2)] private AllVoltageDefensesStruct60 _voltageMain;
        [Layout(3)] private AllVoltageDefensesStruct60 _voltageRes;
        [Layout(4)] private AllFrequencyDefensesStruct60 _frequencyMain;
        [Layout(5)] private AllFrequencyDefensesStruct60 _frequencyRes;

        /// <summary>
        /// Основная группа уставок
        /// </summary>
        //[Layout(0)]
        public DefensesSetpointStruct60 MainSetpoints
        {
            get
            {
                var ret = new DefensesSetpointStruct60();
                ret.ExternalDefenses = _extMain;
                ret.VoltageDefenses = _voltageMain;
                ret.FrequencyDefenses = _frequencyMain;
                return ret;
            }
            set
            {
                _extMain = value.ExternalDefenses;
                _voltageMain = value.VoltageDefenses;
                _frequencyMain = value.FrequencyDefenses;
            }
        }

        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        //[Layout(1)]
        public DefensesSetpointStruct60 ReserveSetpoints
        {
            get
            {
                var ret = new DefensesSetpointStruct60();
                ret.ExternalDefenses = _extRes;
                ret.VoltageDefenses = _voltageRes;
                ret.FrequencyDefenses = _frequencyRes;
                return ret;
            }
            set
            {
                _extRes = value.ExternalDefenses;
                _voltageRes = value.VoltageDefenses;
                _frequencyRes = value.FrequencyDefenses;
            }
        }
    [XmlIgnore]
        public DefensesSetpointStruct60[] Setpoints
        {
            get
            {
                return new[]
                    {
                        MainSetpoints.Clone<DefensesSetpointStruct60>(),
                        ReserveSetpoints.Clone<DefensesSetpointStruct60>()
                    };
            }
            set
            {
                MainSetpoints = value[0].Clone<DefensesSetpointStruct60>();
                ReserveSetpoints = value[1].Clone<DefensesSetpointStruct60>();
            }
        }
    }
}
