﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct
{
    public class ResetStepStruct60:StructBase
    {
        [Layout(0)] private ushort _reset;

        [BindingProperty(0)]
        public string Reset
        {
            get { return Validator.Get(_reset, ListStrings.InputSignals); }
            set { this._reset = Validator.Set(value, ListStrings.InputSignals); }
        }
    }
}
