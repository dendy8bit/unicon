﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Voltage
{
    /// <summary>
    /// защиты напряжения
    /// </summary>
    public class VoltageDefensesStruct60 : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block;  //вход блокировки
        [Layout(2)] private ushort _srab;   //уставка срабатывания
        [Layout(3)] private ushort _tSrab;  //время срабатывания
        [Layout(4)] private ushort _return; //уставка возврата
        [Layout(5)] private ushort _tReturn;//время срабатывания
        [Layout(6)] private ushort _res1;
        [Layout(7)] private ushort _res2;

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, ListStrings.ExternalDefenseMode, 0, 1); }
            set { this._config = Validator.Set(value, ListStrings.ExternalDefenseMode, this._config, 0, 1); }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        public string Block
        {
            get { return Validator.Get(this._block, ListStrings.ExternalDefensesSignals); }
            set { this._block = Validator.Set(value, ListStrings.ExternalDefensesSignals); }
        }
        /// <summary>
        /// Параметр U
        /// </summary>
        [BindingProperty(2)]
        public string ParametrUB
        {
            get { return Validator.Get(this._config, ListStrings.ParametrU, 8, 9, 10); }
            set { this._config = Validator.Set(value, ListStrings.ParametrU, this._config, 8, 9, 10); }
        }
        /// <summary>
        /// Параметр U
        /// </summary>
        [BindingProperty(3)]
        public string ParametrUM
        {
            get { return Validator.Get(this._config, ListStrings.ParametrU, 8, 9, 10); }
            set { this._config = Validator.Set(value, ListStrings.ParametrU, this._config, 8, 9, 10); }
        }
        /// <summary>
        /// Параметр U0
        /// </summary>
        [BindingProperty(4)]
        public string ParametrU0
        {
            get { return Validator.Get(this._config, ListStrings.ParametrU0, 8, 9, 10); }
            set { this._config = Validator.Set(value, ListStrings.ParametrU0, this._config, 8, 9, 10); }
        }

        [BindingProperty(5)]
        public string ParametrU2
        {
            get { return Validator.Get(this._config, ListStrings.ParametrU2, 8, 9, 10); }
            set { this._config = Validator.Set(value, ListStrings.ParametrU2, this._config, 8, 9, 10); }
        }
        [BindingProperty(6)]
        public string ParametrU1
        {
            get { return Validator.Get(this._config, ListStrings.ParametrU1, 8, 9, 10); }
            set { this._config = Validator.Set(value, ListStrings.ParametrU1, this._config, 8, 9, 10); }
        }
        /// <summary>
        /// 
        /// </summary>
        [BindingProperty(7)]
        public double Srab
        {
            get { return ValuesConverterCommon.GetU(this._srab); }
            set { this._srab = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(8)]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tSrab); }
            set { this._tSrab = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Возврат
        /// </summary>
        [BindingProperty(9)]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }
        
        [BindingProperty(10)]
        public double UstavkaReturn
        {
            get { return ValuesConverterCommon.GetU(this._return); }
            set { this._return = ValuesConverterCommon.SetU(value); }
        }
        
        [BindingProperty(11)]
        public int TimeReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tReturn); }
            set { this._tReturn = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(12)]
        public bool BlockV5
        {
            get { return Common.GetBit(this._config, 11); }
            set { this._config = Common.SetBit(this._config, 11, value); }
        }
        /// <summary>
        /// Осциллограф
        /// </summary>
        [BindingProperty(13)]
        public string Osc
        {
            get { return Validator.Get(this._config, ListStrings.VoltageDefenseOsc, 3, 4); }
            set { this._config = Validator.Set(value, ListStrings.VoltageDefenseOsc, this._config, 3, 4); }
        }
        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(14)]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 13); }
            set { this._config = Common.SetBit(this._config, 13, value); }
        }
    }
}