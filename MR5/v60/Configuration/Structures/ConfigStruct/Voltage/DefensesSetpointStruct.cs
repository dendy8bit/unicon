﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.External;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Frequency;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Voltage
{
    public class DefensesSetpointStruct60 : StructBase
    {
        [Layout(0)] private AllExternalDefensesStruct60 _externalDefenses;
        [Layout(1)] private AllVoltageDefensesStruct60 _voltageDefenses;
        [Layout(2)] private AllFrequencyDefensesStruct60 _frequencyDefenses;

        [BindingProperty(0)]
        public AllExternalDefensesStruct60 ExternalDefenses
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
        [BindingProperty(1)]
        public AllVoltageDefensesStruct60 VoltageDefenses
        {
            get { return this._voltageDefenses; }
            set { this._voltageDefenses = value; }
        }
        [BindingProperty(2)]
        public AllFrequencyDefensesStruct60 FrequencyDefenses
        {
            get { return this._frequencyDefenses; }
            set { this._frequencyDefenses = value; }
        }
    }
}
