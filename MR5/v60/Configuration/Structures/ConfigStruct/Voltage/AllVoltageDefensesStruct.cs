﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Voltage
{
    /// <summary>
    /// конфигурации защит U обе группы уставок
    /// </summary>
    public class AllVoltageDefensesStruct60 : StructBase, IDgvRowsContainer<VoltageDefensesStruct60>
    {
        [Layout(0, Count = 16)]
        private VoltageDefensesStruct60[] _voltageDefenses;
        
        public VoltageDefensesStruct60[] Rows
        {
            get { return _voltageDefenses; }
            set { _voltageDefenses = value; }
        }
    }
}