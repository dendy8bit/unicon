﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.ILS
{
    /// <summary>
    /// входные логические параметры
    /// </summary>
    public class InputLogicParametresStruct60 : StructBase, IXmlSerializable
    {
        public const int LOGIC_COUNT = 8;
        /// <summary>
        /// 4-И 4-ИЛИ
        /// </summary>
        [Layout(0, Count = LOGIC_COUNT)]
        private LogicSignalStruct60[] _logicSignalsStructs;

        /// <summary>
        /// 4-И 4-ИЛИ
        /// </summary>
        [BindingProperty(0)]
        [XmlIgnore]
        public LogicSignalStruct60 this[int index]
        {
            get { return _logicSignalsStructs[index]; }
            set { _logicSignalsStructs[index] = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < LOGIC_COUNT; i++)
            {

                writer.WriteStartElement(string.Format("ЛС"));
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}