﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.External
{
    /// <summary>
    /// конфигурации внешних защит
    /// </summary>
    public class AllExternalDefensesStruct60 : StructBase, IDgvRowsContainer<ExternalDefenseStruct60>
    {
        public const int COUNT_EXTERN_DEF = 8;
        [Layout(0, Count = COUNT_EXTERN_DEF)]
        private ExternalDefenseStruct60[] _externalDefenses;

        public ExternalDefenseStruct60[] Rows
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}