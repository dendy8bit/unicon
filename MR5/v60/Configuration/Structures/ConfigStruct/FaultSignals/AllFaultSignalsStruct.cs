﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.FaultSignals
{
    public class AllFaultSignalsStruct60 : StructBase
    {
        [Layout(0)]
        private FaultSignalStruct60 _signals;
        [Layout(1)]
        private ushort _time;

        [BindingProperty(0)]
        public FaultSignalStruct60 Signals
        {
            get { return _signals; }
            set { _signals = value; }
        }
        [BindingProperty(1)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(_time); }
            set { _time = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
