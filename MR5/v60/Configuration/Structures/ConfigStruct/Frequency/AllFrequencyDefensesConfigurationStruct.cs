﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Frequency
{
    /// <summary>
    /// конфигурации защит частоты обе группы уставок (7)
    /// </summary>
    public class AllFrequencyDefensesStruct60 : StructBase, IDgvRowsContainer<FrequencyDefensesStruct60>
    {
        [Layout(0,Count = 8)]
        private FrequencyDefensesStruct60[] _frequencyDefenses;
        
        public FrequencyDefensesStruct60[] Rows
        {
            get { return _frequencyDefenses; }
            set { _frequencyDefenses = value; }
        }
    }
}
