﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v60.Structs.JournalStructs;

namespace BEMN.MR5.v60.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader60
    {
        #region [Private fields]

        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct60> _oscJournal;

        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;

        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct60> _oscRecords;

        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]

        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;

        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;

        /// <summary>
        /// Весь журнал успешно прочитан
        /// </summary>
        public event Action AllJournalReadOk;

        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        /// <param name="oscOptions">Объект параметров журнала</param>
        public OscJournalLoader60(MemoryEntity<OscJournalStruct60> oscJournal,
            MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStruct60>();

            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            //Сброс журнала на нулевую запись
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(_oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);


        }

        #endregion [Ctor's]


        #region [Properties]

        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }

        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct60> OscRecords
        {
            get { return this._oscRecords; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]

        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void UpdateNumber()
        {
            _refreshOscJournal.Value.Word = (ushort) _recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct60.RecordIndex = this.RecordNumber;
                OscJournalStruct60 record = this._oscJournal.Value.Clone<OscJournalStruct60>();
                this.OscRecords.Add(record);
                this._recordNumber = this.RecordNumber + 1;
                this.UpdateNumber();
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk != null)
                    this.AllJournalReadOk.Invoke();
            }
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]

        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            UpdateNumber();
        }

        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}