﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.OscConfig;
using BEMN.MR5.v60.Structs.JournalStructs;

namespace BEMN.MR5.v60.Osc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoaderV60
    {
        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<SetOscStartPageStruct> _setPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список страниц
        /// </summary>
        private List<OscPage> _pages;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct60 _journalStructV2;
        /// <summary>
        /// Параматры осцилографа
        /// </summary>
        private OscConfigStruct60 _oscOptions;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        #endregion [Events]


        #region [Ctor's]
        public OscPageLoaderV60(MemoryEntity<SetOscStartPageStruct> setStartPage, MemoryEntity<OscPage> oscPage)
        {
            this._setPage = setStartPage;
            this._oscPage = oscPage;

            //Установка начальной страницы осциллограммы
            this._setPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscPage.LoadStruct);

            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
        }
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStructV2">Запись журнала о осцилограмме</param>
        /// <param name="oscOptions">Параматры осцилографа</param>
        public void StartRead(OscJournalStruct60 journalStructV2, OscConfigStruct60 oscOptions)
        {
            this._needStop = false;
            this._oscOptions = oscOptions;
            this._journalStructV2 = journalStructV2;
            this._startPage = journalStructV2.OscStartIndex;

            this._startWordIndex = this._journalStructV2.Point % this._oscOptions.PageSize;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStructV2.Len * this._journalStructV2.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / this._oscOptions.PageSize);

            this._endPage = this._startPage + this._pageCount;
            this._pages = new List<OscPage>();
            this.WritePageNumber((ushort)this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        }
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();
                return;
            }
            OscPage page = this._oscPage.Value.Clone<OscPage>();
            this._pages.Add(page);
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < this._oscOptions.FullOscSizeInPages)
                {
                    this.WritePageNumber((ushort)this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort)(this._startPage - this._oscOptions.FullOscSizeInPages));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();

        }
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            //Результирующий массив
            ushort[] resultMassiv = new ushort[this._resultLenInWords];
            ushort[] startPageValueArray = this._pages[0].Words;
            int destanationIndex = 0;
            //Копируем часть первой страницы
            Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex,
                startPageValueArray.Length - this._startWordIndex);
            destanationIndex += startPageValueArray.Length - this._startWordIndex;

            for (int i = 1; i < this._pages.Count - 1; i++)
            {
                ushort[] pageValue = this._pages[i].Words;
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }
            OscPage endPage = this._pages[this._pages.Count - 1];
            Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex,
                this._resultLenInWords - destanationIndex);

            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            int b = (this._journalStructV2.Len - this._journalStructV2.After) * this._journalStructV2.SizeReference;
            //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStructV2.Begin < this._journalStructV2.Point) // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStructV2.Begin + this._oscOptions.LoadedFullOscSizeInWords -
                    this._journalStructV2.Point;
            }
            else //Если BEGIN больше POINT, то:
            {
                c = this._journalStructV2.Begin - this._journalStructV2.Point; //c = BEGIN – POINT
            }
            int start = c - b; //START = c – b
            if (start < 0) //Если START меньше 0, то:
            {
                start += this._journalStructV2.Len * this._journalStructV2.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
            //Перевёрнутый массив
            ushort[] invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            this.ResultArray = invertedMass;
        }

        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setPage.Value.PageIndex = pageNumber;
            this._setPage.SaveStruct6();
        }
        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
