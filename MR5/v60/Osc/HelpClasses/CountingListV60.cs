using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR5.v60.Structs.JournalStructs;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;

namespace BEMN.MR5.v60.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListV60
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 12;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 128;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 4;

        public static readonly string[] uNames = { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]


        #region [Private fields]
        private readonly ushort[][] _countingArray;
        private ushort[][] _discrets;
        private int _count;
        private int _alarm;
        private string _dateTime;
        private OscJournalStruct60 _oscJournalStruct;
        private string _stage;
        #endregion [Private fields]

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseVolt;
        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxU;
        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// �������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        } 
        #endregion [����]
        
        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        public string Stage
        {
            get { return this._stage; }
        }
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] VoltKoefs { get; set; }
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        #region [Ctor's]
        public CountingListV60(ushort[] pageValue, OscJournalStruct60 oscJournalStruct, RomMeasuringStruct60 measure)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            this._voltages = new double[CURRENTS_COUNT][];
            this._baseVolt = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];

            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseVolt[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();
                double factor = i == CURRENTS_COUNT - 1 ? measure.RomFactorUo : measure.RomFactorU;
                currentsKoefs[i] = factor * 256 * Math.Sqrt(2) / 32768;
                this._voltages[i] = this._baseVolt[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._maxU, this._voltages[i].Min());
            }
            this.VoltKoefs = currentsKoefs;

            ushort[][] d1To16 = this.DiscretArrayInit(this._countingArray[4]);
            ushort[][] d17To32 = this.DiscretArrayInit(this._countingArray[5]);
            ushort[][] d33To48 = this.DiscretArrayInit(this._countingArray[6]);
            ushort[][] d49To64 = this.DiscretArrayInit(this._countingArray[7]);
            ushort[][] d65To80 = this.DiscretArrayInit(this._countingArray[8]);
            ushort[][] d81To96 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d97To112 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d113To128 = this.DiscretArrayInit(this._countingArray[11]);
            
            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To16);
            dicrets.AddRange(d17To32);
            dicrets.AddRange(d33To48);
            dicrets.AddRange(d49To64);
            dicrets.AddRange(d65To80);
            dicrets.AddRange(d81To96);
            dicrets.AddRange(d97To112);
            dicrets.AddRange(d113To128);
            this._discrets = dicrets.ToArray();
        }

        public CountingListV60(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._voltages = new double[CURRENTS_COUNT][];
            this._baseVolt = new short[CURRENTS_COUNT][];
            this._count = count;
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("��5 �60 {0} {1} ������� - {2}", this._oscJournalStruct.GetDate, this._oscJournalStruct.GetTime,this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP5,1");
                cgfFile.WriteLine("132,4A,128D");
                int index = 1;
                for (int i = 0; i < this.Voltages.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, uNames[i],
                        this.VoltKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},0", index, ListStrings.OscDiscretsList[i]);
                    index++;
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);

                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath/*, false, Encoding.Default*/))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current  in this._baseVolt)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public CountingListV60 Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));
            string stage = hdrStrings[3];

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath,Encoding.GetEncoding(1251));

            double[] factors = new double[CURRENTS_COUNT];
            Regex factorRegex = new Regex(@"\d+\,[U]\w+\,\,\,[V]\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2 + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[2 + CURRENTS_COUNT + DISCRETS_COUNT + 2].Replace("1000,", string.Empty));
            CountingListV60 result = new CountingListV60(counts) {_alarm = alarm};

            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string pattern = @"^\d+\,\d+,";
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                pattern += string.Format(@"(?<I{0}>\-?\d+),", i + 1);
            }
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                if (i < DISCRETS_COUNT - 1)
                {
                    pattern += string.Format(@"(?<D{0}>\d+),", i+1);
                }
                else
                {
                    pattern += string.Format(@"(?<D{0}>\d+)", i+1);
                }
            }
            
            Regex dataRegex = new Regex(pattern);
            
            double[][] currents = new double[CURRENTS_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }
            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            
            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j];
                }
                
                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }
            }

            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, currents[i].Max());
                result._minU = Math.Min(result.MinU, currents[i].Min());                
            }

            result.Voltages = currents;
            result.Discrets = discrets;
            result._dateTime = timeString;
            result._stage = stage;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
