﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.v60.Structs.JournalStructs;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;
using BEMN.MR5.Properties;

namespace BEMN.MR5.v60.Forms
{
    public partial class Mr5AlarmJournalFormV60 : Form, IFormView
    {
        #region Fields
        private readonly MemoryEntity<AlarmJournalStruct60> _alarmRecord;
        private readonly MemoryEntity<RomMeasuringStruct60> _measuringChannel;
        private DataTable _table;
        private MR5Device _device;
        #endregion

        #region Const
        private const string READ_AJ = "Чтение журнала аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string FAULT_READ_JOURNAL = "Невозможно прочитать журнал";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        #endregion
        
        public Mr5AlarmJournalFormV60()
        {
            this.InitializeComponent();
        }

        public Mr5AlarmJournalFormV60(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;

            this._alarmRecord = device.AlarmRecord60;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.MeasuringChannel60Aj;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);
        }
        
        private void JournalReadFail()
        {
            this._statusLabel.Text = FAULT_READ_JOURNAL;
        }

        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
        }

        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }

        private void ReadJournalOk()
        {
            int i = 1;
            double tn = this._measuringChannel.Value.RomFactorU;
            double tnnp = this._measuringChannel.Value.RomFactorUo;
            this._table.Clear();
            foreach (AlarmJournalRecordStruct60 record in this._alarmRecord.Value.Records.Where(r=>!r.IsEmpty))
            {
                string typeAndValue = this.GetTempValue(record);
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Code,
                        typeAndValue,
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetU(record.Ua, tn),
                        ValuesConverterCommon.Analog.GetU(record.Ub, tn),
                        ValuesConverterCommon.Analog.GetU(record.Uc, tn),
                        ValuesConverterCommon.Analog.GetU(record.Uab, tn),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, tn),
                        ValuesConverterCommon.Analog.GetU(record.Uca, tn),
                        ValuesConverterCommon.Analog.GetU(record.U0, tn),
                        ValuesConverterCommon.Analog.GetU(record.U1, tn),
                        ValuesConverterCommon.Analog.GetU(record.U2, tn),
                        ValuesConverterCommon.Analog.GetU(record.Un, tnnp),
                        record.Discret
                    );
                i++;
            }
            this._journalGrid.Refresh();
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._readAlarmJournalButton.Enabled = true;
        }

        /// <summary>
        /// В зависимости от типа повреждения получаем коэффициент пересчета
        /// </summary>
        /// <param name="record">Запись журнала</param>
        /// <returns>Коэффициент пересчета</returns>
        private string GetTempValue(AlarmJournalRecordStruct60 record)
        {
            int type = record.TypeIndex;
            if (type == 0)
            {
                return record.Type;
            }
            if (type == 1)
            {
                return string.Format("{0} = {1}", record.Type, ValuesConverterCommon.Analog.GetF(record.Value));
            }
            if (type == 2)
            {
                return string.Format("{0} = {1}", record.Type,
                    ValuesConverterCommon.Analog.GetU(record.Value, this._measuringChannel.Value.RomFactorUo));
            }
            return string.Format("{0} = {1}", record.Type,
                ValuesConverterCommon.Analog.GetU(record.Value, this._measuringChannel.Value.RomFactorU));
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr5AlarmJournalFormV60); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "Журнал аварий"; }
        }
        #endregion [IFormView Members]

        private void Mr5AlarmJournalFormV60_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._journalGrid.DataSource = this._table;
            this.StartRead();
        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable("МР5_V60_журнал_аварий");
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].Name);
            }
            return table;
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._readAlarmJournalButton.Enabled = false;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this._measuringChannel.LoadStruct();
        }

        private void _readAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                    AlarmJournalStruct60 journal = new AlarmJournalStruct60();
                    RomMeasuringStruct60 measuring = new RomMeasuringStruct60();
                    int measSize = measuring.GetSize();
                    int journalSize = journal.GetSize();
                    byte[] buffer = file.Take(file.Length - measSize).ToArray();
                    List<byte> journalBytes = new List<byte>(Common.SwapArrayItems(buffer));
                    if (journalBytes.Count != journalSize)
                    {
                        journalBytes.AddRange(new byte[journalSize - journalBytes.Count]);
                    }
                    journal.InitStruct(journalBytes.ToArray());
                    this._alarmRecord.Value = journal;
                    
                    buffer = file.Skip(file.Length - measSize).ToArray();
                    List<byte> measuringBytes = new List<byte>(Common.SwapArrayItems(buffer));
                    if (measuringBytes.Count != measSize)
                    {
                        measuringBytes.AddRange(new byte[measSize - measuringBytes.Count]);
                    }
                    measuring.InitStruct(measuringBytes.ToArray());
                    this._measuringChannel.Value = measuring;
                    this.ReadJournalOk();
                }
                else
                {
                    this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
                }
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR5_v60_AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
