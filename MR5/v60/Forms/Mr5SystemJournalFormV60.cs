﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.v60.Structs.JournalStructs;
using BEMN.MR5.Properties;

namespace BEMN.MR5.v60.Forms
{
    public partial class Mr5SystemJournalFormV60 : Form, IFormView
    {
        private MR5Device _device;

        #region [Ctor's]

        public Mr5SystemJournalFormV60()
        {
            this.InitializeComponent();
        }

        public Mr5SystemJournalFormV60(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._systemJournal = device.SystemJournal60;
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            this._systemJournal.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
            this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
        }

        #endregion [Ctor's]


        #region [Constants]

        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР5_V60_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";
        private const string JOURNAL_READDING = "Идёт чтение журнала";

        #endregion [Constants]


        #region [Private fields]

        private readonly MemoryEntity<SystemJournalStruct60> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;

        #endregion [Private fields]


        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public Type ClassType
        {
            get { return typeof(Mr5SystemJournalFormV60); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public System.Drawing.Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }

        #endregion [IFormView Members]


        #region [Properties]

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);

            }
        }

        #endregion [Properties]


        #region [Help members]
        /// <summary>
        /// Устанавливает св-во Enabled для всех кнопок
        /// </summary>
        /// <param name="enabled"></param>
        private void SetButtonsState(bool enabled)
        {
            this._readJournalButton.Enabled = enabled;
            this._saveJournalButton.Enabled = enabled;
            this._loadJournalButton.Enabled = enabled;         
        }

        private void FailReadJournal()
        {
            this.SetButtonsState(true);
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void StartReadJournal()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this.RecordNumber = 0;
            this._configProgressBar.Value = 0;
            this.SetButtonsState(false);
            this._statusLabel.Text = JOURNAL_READDING;
            this._systemJournal.LoadStruct();
        }

        private void ReadRecord()
        {
            int i = 0;
            foreach (var record in this._systemJournal.Value.Records)
            {
                if (record.IsEmpty)
                {

                    continue;
                }
                this._dataTable.Rows.Add(i + 1, record.GetRecordTime, record.GetRecordMessage);
                i++;
            }
            this._systemJournalGrid.Refresh();
            this.RecordNumber = this._dataTable.Rows.Count;
            this.SetButtonsState(true);

        }

        private DataTable GetJournalDataTable()
        {
            DataTable table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;

            }
        }

        private void LoadJournalFromFile()
        {
            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                if (Path.GetExtension(this._openJournalDialog.FileName).ToLower().Contains("bin"))
                {
                    byte[] file = File.ReadAllBytes(this._openJournalDialog.FileName);
                    SystemJournalStruct60 journal = new SystemJournalStruct60();
                    int size = journal.GetSize();
                    List<byte> journalBytes = new List<byte>();
                    journalBytes.AddRange(Common.SwapArrayItems(file));
                    if (journalBytes.Count != size)
                    {
                        journalBytes.AddRange(new byte[size - journalBytes.Count]);
                    }
                    journal.InitStruct(journalBytes.ToArray());
                    this._systemJournal.Value = journal;
                    this.ReadRecord();
                }
                else
                {
                    this._dataTable.ReadXml(this._openJournalDialog.FileName);
                    this.RecordNumber = this._dataTable.Rows.Count;
                    this._systemJournalGrid.Refresh();
                }
            }

        }

        #endregion [Help members]


        #region [Events Handlers]

        private void Mr5SystemJournalFormV60_Load(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            this.StartReadJournal();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }

        private void Mr5SystemJournalFormV60_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._systemJournal.RemoveStructQueries();
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {               
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR5_60_SJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        } 
        
        #endregion [Events Handlers]
    }
}
