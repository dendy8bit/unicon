﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MR5.v60.Structs.MeasuringStructs;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;
using BEMN.MR5.Properties;
using BEMN.MR5.v60.Measuring.Structures;

namespace BEMN.MR5.v60.Forms
{
    public partial class Mr5MeasuringFormV60 : Form, IFormView
    {
        #region [Constants]
        private const string MAIN_GROUP_SETPOINTS_ON = "Включить основную группу уставок ?";
        private const string RESET_SJ_OK = "Сбросить журнал системы?";
        private const string RESET_SJ = "Сбросить журнал системы";
        private const string RESET_FAULT_OK = "Сбросить неисправность?";
        private const string RESET_FAULT = "Сбросить неисправность";
        private const string RESET_AJ_OK = "Сбросить журнал аварий?";
        private const string RESET_AJ = "Сбросить журнал аварий";
        private const string RESERV_GROUP_SETPOINTS_ON = "Включить резервную группу уставок ?";
        private const string RESET_IND_OK = "Сбросить индикацию?";
        private const string RESET_IND = "Сбросить индикацию";

        #endregion [Constants]


        #region [Private fields]

        private readonly MR5Device _device;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<DiscretBdStruct60> _discretBd;
        private readonly MemoryEntity<RomMeasuringStruct60> _measuringChannel;
        private readonly MemoryEntity<AnalogBdStruct60> _analogBd;
        private readonly AveragerTime<AnalogBdStruct60> _averagerTime;
        private readonly AveragerTime<LinerAnalogBaseStruct60> _averagerLinerTime;
        private MemoryEntity<LinerAnalogBaseStruct60> _linerAnalogSignals;
        #region [Массивы индикаторов]
        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _indicators;
        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;
        /// <summary>
        /// Состояние неисправности 1
        /// </summary>
        private LedControl[] _fault1;
        /// <summary>
        /// Состояние неисправности 2
        /// </summary>
        private LedControl[] _fault2;
        /// <summary>
        /// Д1-Д8
        /// </summary>
        private LedControl[] _discrets;
        /// <summary>
        /// Л1-Л8, ВЛС1 -ВЛС8
        /// </summary>
        private LedControl[] _inputLogicSignals;

        /// <summary>
        /// Umax, Umin
        /// </summary>
        private LedControl[] _uSignals;

        /// <summary>
        /// U0, U1, U2
        /// </summary>
        private LedControl[] _u012Signals;

        /// <summary>
        /// Fmax, Fmin
        /// </summary>
        private LedControl[] _fSignals;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;

        /// <summary>
        /// Управляющие сигналы(СДТУ)
        /// </summary>
        private LedControl[] _manageSignals;

        /// <summary>
        /// Сигналы свободно програмируемой логики
        /// </summary>
        private LedControl[] _ssl;
        #endregion [Массивы индикаторов]


        #endregion [Private fields]


        #region [Ctor's]

        public Mr5MeasuringFormV60()
        {
            this.InitializeComponent();
        }

        public Mr5MeasuringFormV60(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            this._dateTime = this._device.DateTime60;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._discretBd = this._device.DiscretBd60;
            this._discretBd.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdLoad);
            this._discretBd.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdLoadFail);

            this._measuringChannel = this._device.MeasuringChannel60;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);

            this._analogBd = this._device.AnalogBd60;
            this._analogBd.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdLoadOk);
            this._analogBd.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdLoadFail);

            this._linerAnalogSignals = device.LinerAnalogSignals60;
            this._linerAnalogSignals.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogLinerSignalsLoadOk);
            this._linerAnalogSignals.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.LinerBdLoadFail);

            this._averagerTime = new AveragerTime<AnalogBdStruct60>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;

            this._averagerLinerTime = new AveragerTime<LinerAnalogBaseStruct60>(500);
            this._averagerLinerTime.Tick += this.AveragerLinerTimeTick;

            this.Prepare();
        }   

        private void MeasuringChannelLoad()
        {
            this._analogBd.LoadStructCycle();
            this._linerAnalogSignals.LoadStructCycle();
        }

        private void OnAnalogLinerSignalsLoadOk()
        {
            this._averagerLinerTime.Add(this._linerAnalogSignals.Value);
        }

        /// <summary>
        /// Загружены аналоговые сигналы
        /// </summary>
        private void DiscretBdLoad()
        {
            LedManager.SetLeds(this._indicators, this._discretBd.Value.Indicators);
            LedManager.SetLeds(this._relays, this._discretBd.Value.Relay);
            LedManager.SetLeds(this._fault1, this._discretBd.Value.Fault1);
            LedManager.SetLeds(this._fault2, this._discretBd.Value.Fault2);
            LedManager.SetLeds(this._discrets, this._discretBd.Value.Discrets);
            LedManager.SetLeds(this._inputLogicSignals, this._discretBd.Value.InputLogicSignals);
            LedManager.SetLeds(this._uSignals, this._discretBd.Value.USignals);
            LedManager.SetLeds(this._u012Signals, this._discretBd.Value.U012Signals);
            LedManager.SetLeds(this._fSignals, this._discretBd.Value.FSignals);
            LedManager.SetLeds(this._externalDefenses, this._discretBd.Value.ExternalDefenses);
            LedManager.SetLeds(this._manageSignals, this._discretBd.Value.ManageSignals);
            LedManager.SetLeds(this._ssl, this._discretBd.Value.SPL);
        }
        private void DiscretBdLoadFail()
        {
            LedManager.TurnOffLeds(this._indicators);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._fault1);
            LedManager.TurnOffLeds(this._fault2);
            LedManager.TurnOffLeds(this._discrets);
            LedManager.TurnOffLeds(this._inputLogicSignals);
            LedManager.TurnOffLeds(this._uSignals);
            LedManager.TurnOffLeds(this._u012Signals);
            LedManager.TurnOffLeds(this._fSignals);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._manageSignals);
            LedManager.TurnOffLeds(this._ssl);
        }

        private void Prepare()
        {
            this._manageSignals = new[]
                {
                    this._mainGroupLed,
                    this._resGroupLed,
                    this._faultLed,
                    this._sjLed,
                    this._ajLed,
                    this._uabcLed,
                    this._splLed
                };
            this._externalDefenses = new[]
                {
                    this._extDefenseLed1,this._extDefenseLed2,this._extDefenseLed3,this._extDefenseLed4,
                    this._extDefenseLed5,this._extDefenseLed6,this._extDefenseLed7,this._extDefenseLed8
                };
            this._fSignals = new[]
                {
                    this._FminLed1,this._FminLed2,this._FminLed3,this._FminLed4,
                    this._FminLed5,this._FminLed6,this._FminLed7,this._FminLed8,
                    this._FmaxLed1,this._FmaxLed2,this._FmaxLed3,this._FmaxLed4,
                    this._FmaxLed5,this._FmaxLed6,this._FmaxLed7,this._FmaxLed8
                };
            this._u012Signals = new[]
                {
                    this._U12Led_1,this._U12Led_2,this._U12Led_3,this._U12Led_4,
                    this._U12Led_5,this._U12Led_6,this._U12Led_7,this._U12Led_8,
                    this._U0maxLed1,this._U0maxLed2,this._U0maxLed3,this._U0maxLed4,
                    this._U0maxLed5,this._U0maxLed6,this._U0maxLed7,this._U0maxLed8
                };
            this._uSignals = new[]
                {
                    this._UminLed1,this._UminLed2,this._UminLed3,this._UminLed4,
                    this._UminLed5,this._UminLed6,this._UminLed7,this._UminLed8,
                    this._UmaxLed1,this._UmaxLed2,this._UmaxLed3,this._UmaxLed4,
                    this._UmaxLed5,this._UmaxLed6,this._UmaxLed7,this._UmaxLed8
                 
                };
            this._inputLogicSignals = new[]
                {
                    this._outLed1,this._outLed2,this._outLed3,this._outLed4,
                    this._outLed5,this._outLed6,this._outLed7,this._outLed8,
                    this._inL_led1,this._inL_led2,this._inL_led3,this._inL_led4,
                    this._inL_led5,this._inL_led6,this._inL_led7,this._inL_led8
                };
            this._discrets = new[]
                {
                    this._inD_led1,this._inD_led2,this._inD_led3,this._inD_led4,
                    this._inD_led5,this._inD_led6,this._inD_led7,this._inD_led8
                };
            this._fault2 = new[]
                {
                    this._faultSignalLed1,this._faultSignalLed2,this._faultSignalLed3,this._faultSignalLed4,
                    this._faultSignalLed5,this._faultSignalLed6,this._faultSignalLed7,this._faultSignalLed8,
                    this._faultSignalLed9,this._faultSignalLed10,this._faultSignalLed11,this._faultSignalLed12,
                    this._faultSignalLed13,this._faultSignalLed14,this._faultSignalLed15
                };
            this._fault1 = new[]
                {
                    this._faultStateLed1,
                    this._faultStateLed2,
                    this._faultStateLed3,
                    this._faultStateLed4
                };
            this._indicators = new[]
                {
                    this._indLed1,this._indLed2,this._indLed3,this._indLed4,this._indLed5,this._indLed6,
                    this._indLed7,this._indLed8,this._indLed9,this._indLed10,this._releFaultLed,this._releAlarmLed
                };
            this._relays = new[]
                {
                    this._releLed1,this._releLed2,this._releLed3,this._releLed4,this._releLed5,this._releLed6,
                    this._releLed7,this._releLed8,this._releLed9,this._releLed10,this._releLed11,this._releLed12,
                    this._releLed13,this._releLed14,this._releLed15,this._releLed16
                };
            this._ssl = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8, 
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16, 
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24
            };
        }
        /// <summary>
        /// Аналоговая БД загружена и добавлена для усреднения
        /// </summary>
        private void AnalogBdLoadOk()
        {
            this._averagerTime.Add(this._analogBd.Value);
        }

        private void AnalogBdLoadFail()
        {
            this._Un.Text = string.Empty;
            this._Ua.Text = string.Empty;
            this._Ub.Text = string.Empty;
            this._Uc.Text = string.Empty;
            this._U0.Text = string.Empty;
            this._U1.Text = string.Empty;
            this._U2.Text = string.Empty;
            this._F.Text = string.Empty;
        }

        private void LinerBdLoadFail()
        {
            this._Uab.Text = string.Empty;
            this._Ubc.Text = string.Empty;
            this._Uca.Text = string.Empty;
        }

        private void AveragerTimeTick()
        {
            this._Un.Text = this._analogBd.Value.GetUn(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._Ua.Text = this._analogBd.Value.GetUa(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._Ub.Text = this._analogBd.Value.GetUb(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._Uc.Text = this._analogBd.Value.GetUc(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._U0.Text = this._analogBd.Value.GetU0(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._U1.Text = this._analogBd.Value.GetU1(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._U2.Text = this._analogBd.Value.GetU2(this._averagerTime.ValueList, this._measuringChannel.Value);
            this._F.Text = this._analogBd.Value.GetF(this._averagerTime.ValueList);
        }

        private void AveragerLinerTimeTick()
        {
            this._Uab.Text = this._linerAnalogSignals.Value.GetUab(this._averagerLinerTime.ValueList, this._measuringChannel.Value);
            this._Ubc.Text = this._linerAnalogSignals.Value.GetUbc(this._averagerLinerTime.ValueList, this._measuringChannel.Value);
            this._Uca.Text = this._linerAnalogSignals.Value.GetUca(this._averagerLinerTime.ValueList, this._measuringChannel.Value);
        }

        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        #endregion [Ctor's]


        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public Type ClassType
        {
            get { return typeof(Mr5MeasuringFormV60); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        #endregion [IFormView Members]

        private void _dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        private void MeasuringFormLoad(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void MeasuringFormFormClosing(object sender, FormClosingEventArgs e)
        {
            this._dateTime.RemoveStructQueries();
            this._analogBd.RemoveStructQueries();
            this._discretBd.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._dateTime.LoadStructCycle();
                this._measuringChannel.LoadStruct();
                this._discretBd.LoadStructCycle();
            }
            else
            {
                this._dateTime.RemoveStructQueries();
                this._analogBd.RemoveStructQueries();
                this._discretBd.RemoveStructQueries();
                this.AnalogBdLoadFail();
                this.DiscretBdLoadFail();
                this.LinerBdLoadFail();
            }
        }

        private void SetBitByAdress(ushort adress, string requestName)
        {
            this._device.SetBit(this._device.DeviceNumber, adress, true, requestName, this);
        }
        
        private void MainGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(MAIN_GROUP_SETPOINTS_ON, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.ChangeGroup.Value.Word = 0;
                this._device.ChangeGroup.SaveStruct();
            }
        }
        /// <summary>
        /// Сбросить журнал системы
        /// </summary>
        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(RESET_SJ_OK, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetBitByAdress(0x1806, RESET_SJ);
            }
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
           if (MessageBox.Show(RESET_FAULT_OK, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
           {
               this.SetBitByAdress(0x1805, RESET_FAULT);
           }
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(RESET_AJ_OK, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetBitByAdress(0x1807, RESET_AJ);
            }
        }

        private void ReserveGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(RESERV_GROUP_SETPOINTS_ON, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._device.ChangeGroup.Value.Word = 1;
                this._device.ChangeGroup.SaveStruct();
            }
        }

        private void _resetInd_But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(RESET_IND_OK, string.Empty, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.SetBitByAdress(0x1804, RESET_IND);
            }
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "Запуск СПЛ", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "Останов СПЛ", this._device);
        }
    }
}
