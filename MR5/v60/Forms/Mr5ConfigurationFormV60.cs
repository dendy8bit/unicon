﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.ILS;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.FaultSignals;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Relay;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Frequency;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Voltage;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.External;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.VLS;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.Indicators;
using BEMN.MR5.Properties;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.OscConfig;

namespace BEMN.MR5.v60.Forms
{
    public partial class Mr5ConfigurationFormV60 : Form, IFormView
    {
        #region [Constants]
        private const string SETPOINTS_READ_OK = "Конфигурация успешно прочитана";
        private const string SETPOINTS_READ_FAIL = "Уставки невозможно прочитать";

        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        
        private const string WRITE_FAIL = "Невозможно записать конфигурацию";
        private const string WRITE_OK = "Конфигурация успешно записана";
        private const string WRITTING_CONFIG = "Записать конфигурацию в устройство?";
        private const string WRITE_CONFIG = "Запись конфигурации";

        private const string START_READ = "Идет чтение";
        private const string START_WRITE = "Идет запись";

        private const string XML_HEAD = "MR5V60_SET_POINTS";
        private const string MR5v60_BASE_CONFIG_PATH = "\\MR5\\MR5v{0:F2}_BaseConfig.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<RamMemoryStruct60> _ramMemory;
        private RamMemoryStruct60 _currentStruct;
        private readonly MR5Device _device;
        private RadioButtonSelector _groupSelector;
        private DataGridView[] _lsBoxes;
        private CheckedListBox[] _vlsBoxes;
        private NewStructValidator<RomMeasuringStruct60> _romMeasuringValidator;
        private NewCheckedListBoxValidator<KeysStruct60> _keysValidator;
        private NewStructValidator<RomExternalSignalsStruct60> _romExternalSignalValidator;
        private NewStructValidator<AllFaultSignalsStruct60> _faultStructValidator;
        private NewDgwValidatior<LogicSignalStruct60>[] _inputLogicValidator;
        private StructUnion<InputLogicParametresStruct60> _inputLogicUnion;
        private NewDgwValidatior<AllRelaysStruct60, RelayStruct60> _relaysValidator;
        private NewStructValidator<ResetStepStruct60> _resetStepValidator;

        private NewDgwValidatior<AllExternalDefensesStruct60, ExternalDefenseStruct60> _externalDefenseValidatior;
        private NewDgwValidatior<AllVoltageDefensesStruct60, VoltageDefensesStruct60> _voltageDefenseValidatior;
        private NewDgwValidatior<AllFrequencyDefensesStruct60, FrequencyDefensesStruct60> _frequencyDefenseValidatior;
        private StructUnion<DefensesSetpointStruct60> _defenseUnion;
        private SetpointsValidator<AllSetpointStruct60, DefensesSetpointStruct60> _defensesValidator;

        private NewCheckedListBoxValidator<OutputLogicSignalStruct60>[] _vlsValidator;
        private StructUnion<AllOutputLogicSignalStruct60> _vlsUnion;
        private NewDgwValidatior<AllIndicatorsStruct60, IndicatorsStruct60> _indicatorsValidatior;
        private NewStructValidator<OscConfigStruct60> _oscValidator; 
        private StructUnion<RamMemoryStruct60> _configUnion;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr5ConfigurationFormV60()
        {
            this.InitializeComponent();
        }

        public Mr5ConfigurationFormV60(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._currentStruct = new RamMemoryStruct60();
            this._ramMemory = device.RamMemory60;
            this._ramMemory.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadSuccessful);
            this._ramMemory.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ReadFail);

            this._ramMemory.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.WriteSuccessful);
            this._ramMemory.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.WriteFail);

            this._ramMemory.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this._ramMemory.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
            this.Prepare();
        }
        /// <summary>
        /// Подготовка
        /// </summary>
        private void Prepare()
        {
            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals5,
                this._inputSignals6, this._inputSignals7, this._inputSignals8
            };

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
            };

            ToolTip toolTip = new ToolTip();
            //  измерительный канал
            this._romMeasuringValidator = new NewStructValidator<RomMeasuringStruct60>(
                toolTip,
                new ControlInfoText(this._TN_Box, new Ustavka128000Rule()),
                new ControlInfoCombo(this._defectTN_CB, ListStrings.InputSignals),
                new ControlInfoText(this._TNNP_Box, new Ustavka128000Rule()),
                new ControlInfoCombo(this._defectTNNP_CB, ListStrings.InputSignals));
            // Ключи
            this._keysValidator = new NewCheckedListBoxValidator<KeysStruct60>(this.keysCheckList, ListStrings.KeyNumbers);
            // Внешние сигналы(Дополнительные сигналы)
            this._romExternalSignalValidator = new NewStructValidator<RomExternalSignalsStruct60>(
                toolTip,
                new ControlInfoCombo(this._inputSignalsIndicationResetCombo, ListStrings.InputSignals),
                new ControlInfoCombo(this._inputSignalsConstraintResetCombo, ListStrings.InputSignals));
            // Сигнал неисправность
            this._faultStructValidator = new NewStructValidator<AllFaultSignalsStruct60>(
                toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct60>(this._dispepairCheckList, ListStrings.FaultSignalsDictionary)),
                new ControlInfoText(this._outputSignalsDispepairTimeBox, new IntTo3MRule()));
            
            //Логические сигналы
            this._inputLogicValidator = new NewDgwValidatior<LogicSignalStruct60>[InputLogicParametresStruct60.LOGIC_COUNT];

            for (int i = 0; i < InputLogicParametresStruct60.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<LogicSignalStruct60>
                    (
                    this._lsBoxes[i],
                    LogicSignalStruct60.DISCRETS_COUNT,
                    toolTip,
                    new ColumnInfoCombo(ListStrings.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(ListStrings.LogicState)
                    );
            }
            this._inputLogicUnion = new StructUnion<InputLogicParametresStruct60>(this._inputLogicValidator);
            // Дополнительные и выходные реле
            this._relaysValidator = new NewDgwValidatior<AllRelaysStruct60, RelayStruct60>
                (this._outputReleGrid,
                18,
                toolTip,
                new ColumnInfoCombo(ListStrings.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(ListStrings.OutputSignalsType),
                new ColumnInfoCombo(ListStrings.RelayIndSignals),
                new ColumnInfoText(RulesContainer.IntTo3M)
                );
            // Сброс шага
            this._resetStepValidator = new NewStructValidator<ResetStepStruct60>(
                toolTip,
                new ControlInfoCombo(this._inputSignalsTN_DispepairCombo, ListStrings.InputSignals));

            // Внешние защиты
            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct60, ExternalDefenseStruct60>(this._externalDefenseGrid,
                8,
                toolTip,
                new ColumnInfoCombo(ListStrings.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(ListStrings.ExternalDefenseMode),
                new ColumnInfoCombo(ListStrings.ExternalDefensesSignals),
                new ColumnInfoCombo(ListStrings.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(ListStrings.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(
                        this._externalDefenseGrid,
                        new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9), 
                        new TurnOffRule(5, false, false, 6, 7))
                }
            };
            // Защиты по напряжению
            this._voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct60, VoltageDefensesStruct60>(
                new[]
                {
                    this._voltageDefensesUBGrid, this._voltageDefensesUMGrid, this._voltageDefensesU0Grid,
                    this._voltageDefensesU2Grid, this._voltageDefensesU1Grid
                },
                new int[] {4, 4, 4, 2, 2},
                toolTip,
                new ColumnInfoCombo(ListStrings.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(ListStrings.ExternalDefenseMode),
                new ColumnInfoCombo(ListStrings.ExternalDefensesSignals),
                new ColumnInfoCombo(ListStrings.ParametrU, ColumnsType.COMBO, true, false, false, false, false),
                new ColumnInfoCombo(ListStrings.ParametrU, ColumnsType.COMBO, false, true, false, false, false),
                new ColumnInfoCombo(ListStrings.ParametrU0, ColumnsType.COMBO, false, false, true, false, false),
                new ColumnInfoCombo(ListStrings.ParametrU2, ColumnsType.COMBO, false, false, false, true, false),
                new ColumnInfoCombo(ListStrings.ParametrU1, ColumnsType.COMBO, false, false, false, false, true),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(ListStrings.VoltageDefenseOsc),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._voltageDefensesUBGrid, new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                                        new TurnOffRule(10, false, false, 11, 12)),
                    new TurnOffDgv(this._voltageDefensesUMGrid, new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15), 
                                        new TurnOffRule(10, false, false, 11, 12)),
                    new TurnOffDgv(this._voltageDefensesU0Grid, new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15), 
                                        new TurnOffRule(10, false, false, 11, 12)),
                    new TurnOffDgv(this._voltageDefensesU2Grid, new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                                        new TurnOffRule(10, false, false, 11, 12)),
                    new TurnOffDgv(this._voltageDefensesU1Grid,new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                                        new TurnOffRule(10, false, false, 11, 12))
                }
            };
            // Защиты по частоте
            this._frequencyDefenseValidatior = new NewDgwValidatior<AllFrequencyDefensesStruct60, FrequencyDefensesStruct60>(this._frequenceDefensesGrid,
                8,
                toolTip,
                new ColumnInfoCombo(ListStrings.FrequenceDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(ListStrings.ExternalDefenseMode),
                new ColumnInfoCombo(ListStrings.ExternalDefensesSignals),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40To60),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCombo(ListStrings.VoltageDefenseOsc),
                new ColumnInfoCheck())
            {
                TurnOff = new[]
                {
                    new TurnOffDgv(this._frequenceDefensesGrid,
                        new TurnOffRule(1, ListStrings.ExternalDefenseMode[0], true, 2, 3, 4, 5, 6, 7, 8, 9),
                        new TurnOffRule(5, false, false, 6, 7))
                }
            };
            // Объединение всех защит
            this._defenseUnion = new StructUnion<DefensesSetpointStruct60>(this._externalDefenseValidatior, this._voltageDefenseValidatior, this._frequencyDefenseValidatior);
            // Переключатель между вставками
            this._groupSelector = new RadioButtonSelector(this._mainRadioBtnGroup, this._reserveRadioBtnGroup, this._btnGroupChange);
            // Общий для всех защит валидатор
            this._defensesValidator = new SetpointsValidator<AllSetpointStruct60, DefensesSetpointStruct60>(this._groupSelector, this._defenseUnion);

            // ВЛС
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicSignalStruct60>[AllOutputLogicSignalStruct60.VLS_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct60.VLS_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicSignalStruct60>(this._vlsBoxes[i], ListStrings.OutputLogicSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct60>(this._vlsValidator); 

            // Индикаторы
            this._indicatorsValidatior = new NewDgwValidatior<AllIndicatorsStruct60, IndicatorsStruct60>(this._indicatorsGrid,
                AllIndicatorsStruct60.COUNT_INDICATORS,
                toolTip,
                new ColumnInfoCombo(ListStrings.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(ListStrings.OutputSignalsType),
                new ColumnInfoCombo(ListStrings.RelayIndSignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            // Конфигурация осциллографа
            this._oscValidator = new NewStructValidator<OscConfigStruct60>(
                toolTip,
                new ControlInfoCombo(this._oscLengthCmb, ListStrings.OscLenMode),
                new ControlInfoCombo(this._oscFixationCombo, ListStrings.OscFicsation),
                new ControlInfoText(this._oscWriteLength, RulesContainer.Ushort1To100));

            // Вся структура конфигурации
            this._configUnion = new StructUnion<RamMemoryStruct60>(this._romMeasuringValidator, this._keysValidator, this._romExternalSignalValidator, this._faultStructValidator, this._inputLogicUnion, this._resetStepValidator, this._defensesValidator, this._vlsUnion, this._relaysValidator, this._indicatorsValidatior, this._oscValidator);
        }
        #endregion [Ctor's]


        #region [Ввод-вывод уставок]

        private void WriteFail()
        {
            MessageBox.Show(WRITE_FAIL, "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void WriteSuccessful()
        {
            this._statusLabel.Text = WRITE_OK;
            this._device.WriteConfiguration();
        }
        /// <summary>
        /// Уставки успешно прочитаны
        /// </summary>
        private void ReadSuccessful()
        {
            this._currentStruct = this._ramMemory.Value;
            this._configUnion.Set(this._currentStruct);
            this._statusLabel.Text = SETPOINTS_READ_OK;
        }
        /// <summary>
        /// Уставки невозможно прочитать
        /// </summary>
        private void ReadFail()
        {
            this._statusLabel.Text = SETPOINTS_READ_FAIL;
        }
        
        #endregion


        #region [Чтение/Запись конфигурации]
        /// <summary>
        /// Запуск чтения конфигурации из устройства
        /// </summary>
        private void StartRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._statusLabel.Text = START_READ;
                this._ramMemory.LoadStruct();
            }
        }

        private void SetConfiguration()
        {
            this._currentStruct = this._configUnion.Get();
            this._ramMemory.Value = this._currentStruct;
        }
        
        /// <summary>
        /// Запись конфигурации в устройство
        /// </summary>
        private void StartWrite()
        {
            this.SetConfiguration();
            this._ramMemory.SaveStruct();}


        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR5"));
                
                ushort[] values = this._currentStruct.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
             
                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }
        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);
                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if(a == null)
                    throw new NullReferenceException();
                List<byte> values = new List<byte>(Convert.FromBase64String(a.InnerText));
                if (values.Count > 1256) // Для совместимости конфигурации до того, как был введе порт Ethernet.
                {                        // Не должно перезаписываться значение уставки выбранного порта(RS485 или Ethernet)
                    values.RemoveAt(46);
                    values.RemoveAt(46);
                }
                this._currentStruct.InitStruct(values.ToArray());
                this._configUnion.Set(this._currentStruct);
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }
        #endregion [Чтение/Запись конфигурации]


        #region [События взаимодействия]
        /// <summary>
        /// Сохранение в файл
        /// </summary>
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР5_Уставки_версия_{0:F1}.xml", this._device.DeviceVersion);
            if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog()) return;
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this.SetConfiguration();
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
            else
            {
                MessageBox.Show("Имеются неверные уставки. Невозможно сохранить в файл", "Ошибка записи", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Загрузить из файла
        /// </summary>
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();       
        }

        private void ShowToolTipBtn()
        {
            ToolTip toolTip = new ToolTip();
            toolTip.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            toolTip.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройства (CTRL+W)");
            toolTip.SetToolTip(this._resetSetpointBtn, "Загрузить базовые уставки");
            toolTip.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            toolTip.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            toolTip.SetToolTip(this._saveToXmlButton, "Сохранить конфигурацию в HTML файл");
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
            }  
        }

        /// <summary>
        /// Прочитать из устройства
        /// </summary>
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this._exchangeProgressBar.Value = 0;
            this.StartRead();
        }

        private void Mr5ConfigurationFormV60_Shown(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.ResetSetpoints(true);
                this.StartRead();
            }
            //this._configUnion.Reset();
            this.ShowToolTipBtn();
        }

        /// <summary>
        /// Записать в устройтво
        /// </summary>
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig(); 
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(WRITTING_CONFIG, WRITE_CONFIG, MessageBoxButtons.YesNo) != DialogResult.Yes) return;
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this._exchangeProgressBar.Value = 0;
                this._statusLabel.Text = START_WRITE;
                this.StartWrite();
            }
            else
            {
                MessageBox.Show("Имеются неверные уставки. Невозможно сохранить в устройство", "Ошибка записи",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }
        private void _resetSetpointBtn_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
        }
        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            try
            {
                if (!isDialog)
                {
                    DialogResult res = MessageBox.Show(@"Загрузить базовые уставки?", @"Базовые уставки",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (res == DialogResult.No) return;
                }
                this.Deserialize(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(CultureInfo.InvariantCulture, MR5v60_BASE_CONFIG_PATH, this._device.DeviceVersion));
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                        "Внимание", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        this._configUnion.Reset();
                }
                else
                {
                    this._configUnion.Reset();
                }
            }
        }
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.oscLenBox.Text = (21162*2/(this._oscLengthCmb.SelectedIndex + 2)).ToString();
        }
        #endregion [События взаимодействия]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MR5Device); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion [IFormView Members]


        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            string message;
            if (this._configUnion.Check(out message, true))
            {
                this.SetConfiguration();
                try
                {
                    this._currentStruct.DeviceVersion = this._device.DeviceVersion;
                    this._currentStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._statusLabel.Text = HtmlExport.Export(Resources.MR5_60_Main, Resources.MR5_60_Res, this._currentStruct, this._currentStruct.DeviceType, this._currentStruct.DeviceVersion);
                }
                catch (Exception)
                {
                    MessageBox.Show("Ошибка сохранения");
                }
            }
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(false);
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
            
        }
        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                   
                    this._exchangeProgressBar.Value = 0;
            this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }
    }
}