﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v50.Configuration.Structures.Transformer;

namespace BEMN.MR5.v50.Measuring
{
    public class AnalogDataBaseV50 : StructBase
    {
        [Layout(0)] private ushort _in;
        [Layout(1)] private ushort _ia;
        [Layout(2)] private ushort _ib;
        [Layout(3)] private ushort _ic;
        [Layout(4)] private ushort _i0;
        [Layout(5)] private ushort _i1;
        [Layout(6)] private ushort _i2;
        [Layout(7)] private ushort _ig;

        private ushort GetMean(List<AnalogDataBaseV50> list, Func<AnalogDataBaseV50, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        public string GetIn(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.TTNP*5);
        }

        public string GetIa(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetIb(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetIc(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetI0(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetI1(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetI2(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.TT*40);
        }

        public string GetIg(List<AnalogDataBaseV50> list, MeasuringTransformerV50 measure)
        {
            ushort value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.TTNP * 5);
        }
    }
}
