﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v50.Measuring
{
    public class GroupSetpointBaseStructV50 : StructBase
    {
        [Layout(0)] private ushort _group;

        public string GetGroup(double version)
        {
            if (version > 50.05)
            {
                return Validator.Get(this._group, ConfigParamsV50.Groups);
            }
            return Validator.Get(this._group, ConfigParamsV50.SetpointsNames);
        }

        public void SetGroup(double version, string value)
        {
            this._group = Validator.Set(value, version > 50.05 ? ConfigParamsV50.Groups : ConfigParamsV50.SetpointsNames);
        }
    }
}
