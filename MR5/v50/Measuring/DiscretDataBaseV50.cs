﻿using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Measuring
{
    public class DiscretDataBaseV50 : StructBase
    {
        [Layout(0, Count = 16)] private ushort[] _diagnostic;
        
        /// <summary>
        /// Управляющие сигналы СДТУ
        /// </summary>
        public BitArray ManageSignals
        {
            get
            {
                return new BitArray(new [] { Common.LOBYTE(this._diagnostic[0]), Common.HIBYTE(this._diagnostic[0]) });
            }
        }
        /// <summary>
        /// Дополнительные сигналы
        /// </summary>
        public BitArray AdditionalSignals
        {
            get
            {
                BitArray temp = new BitArray(new[] { Common.LOBYTE(this._diagnostic[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4; i++)
                {
                    ret[i] = temp[i + 4];
                }
                return ret;
            }
        }

        /// <summary>
        /// Индикаторы
        /// </summary>
        public BitArray Indicators
        {
            get
            {
                return new BitArray(new[] { Common.HIBYTE(this._diagnostic[2]) });
            }
        }
        /// <summary>
        /// Реле
        /// </summary>
        public BitArray Rele
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._diagnostic[3]),
                    Common.HIBYTE(this._diagnostic[3])
                });
            }
        }
        /// <summary>
        /// Состояние неисправности
        /// </summary>
        public BitArray FaultState
        {
            get
            {
                return new BitArray(new[] { Common.LOBYTE(this._diagnostic[4]) });
            }
        }
        /// <summary>
        /// Сигналы неисправности
        /// </summary>
        public BitArray FaultSignals
        {
            get
            {
                return new BitArray(new[] { Common.LOBYTE(this._diagnostic[5]), Common.HIBYTE(this._diagnostic[5]) });
            }
        }

        /// <summary>
        /// Автоматика
        /// </summary>
        public BitArray Automation
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.HIBYTE(this._diagnostic[8]),
                    Common.HIBYTE(this._diagnostic[12])
                });
            }
        }
        /// <summary>
        /// Входные сигналы
        /// </summary>
        public BitArray InputSignals
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._diagnostic[9]),
                    Common.HIBYTE(this._diagnostic[9]),
                    Common.LOBYTE(this._diagnostic[10])
                });
            }
        }
        /// <summary>
        /// Выходные сигналы
        /// </summary>
        public BitArray OutputSignals
        {
            get
            {
                return new BitArray(new[] { Common.HIBYTE(this._diagnostic[10]) });
            }
        }
        /// <summary>
        /// Сигналы пределов
        /// </summary>
        public BitArray LimitSignals
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._diagnostic[11]),
                    Common.HIBYTE(this._diagnostic[11]),
                    Common.LOBYTE(this._diagnostic[12])
                });
            }
        }

        /// <summary>
        /// Сигналы внешних защит
        /// </summary>
        public BitArray External
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.LOBYTE(this._diagnostic[14])
                });
            }
        }

        /// <summary>
        /// Сигналы СП
        /// </summary>
        public BitArray Ssl
        {
            get
            {
                return new BitArray(new[]
                {
                    Common.HIBYTE(this._diagnostic[14]),
                    Common.LOBYTE(this._diagnostic[15]),
                    Common.HIBYTE(this._diagnostic[15])
                });
            }
        }
    }
}
