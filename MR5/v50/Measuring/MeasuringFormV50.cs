using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Measuring
{
    public partial class MeasuringFormV50 : Form, IFormView
    {
        private MR5Device _device;
        private readonly AveragerTime<AnalogDataBaseV50> _averagerTime;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _ssl;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;
        private LedControl[] _externalDef;
        private MemoryEntity<DateTimeStruct> _dateTime;

        public MeasuringFormV50()
        {
            this.InitializeComponent();
        }

        public MeasuringFormV50(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;

            this._dateTime = this._device.DateTimeV50;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._device.AnalogV50.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._device.AnalogV50.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);
            this._device.MeasuringV50.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._device.AnalogV50.LoadStruct);
            this._device.MeasuringV50.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadFail);

            this._device.DiscretV50.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadOk);
            this._device.DiscretV50.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.OnDiagnosticLoadFail);

            this._averagerTime = new AveragerTime<AnalogDataBaseV50>(500);
            this._averagerTime.Tick += this.AveragerTick;

            this._device.Groups50.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                    this._setpointLabel.Text =
                        this._device.Groups50.Value.GetGroup(Common.VersionConverter(this._device.DeviceVersion)));
            this._device.Groups50.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._setpointLabel.Text = "�� ����������");

            this.Init();
        }

        private void Init()
        {
            if (Common.VersionConverter(_device.DeviceVersion) > 50.05)
            {
                this._setpointsComboBox.DataSource = ConfigParamsV50.Groups;
            }
            else
            {
                this._setpointsComboBox.DataSource = ConfigParamsV50.SetpointsNames;
            }
            this._setpointsComboBox.SelectedIndex = 0;

            this._manageLeds = new[]
            {
                this._manageLed1, this._manageLed2, this._manageLed3, this._manageLed4, this._manageLed5,
                this._manageLed6, this._manageLed7, this._manageLed8, new LedControl(), this._manageLed9
            };

            this._additionalLeds = new[]
            {
                this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4
            };

            this._indicatorLeds = new[]
            {
                this._indLed1, this._indLed2, this._indLed3, this._indLed4,
                this._indLed5, this._indLed6, this._indLed7, this._indLed8
            };

            this._inputLeds = new[]
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4,
                this._inD_led5, this._inD_led6, this._inD_led7, this._inD_led8,
                this._inD_led9, this._inD_led10, this._inD_led11, this._inD_led12,
                this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16,
                this._inL_led1, this._inL_led2, this._inL_led3, this._inL_led4,
                this._inL_led5, this._inL_led6, this._inL_led7, this._inL_led8
            };
            this._outputLeds = new[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4,
                this._outLed5, this._outLed6, this._outLed7, this._outLed8
            };

            this._releLeds = new[]
            {
                this._releLed1, this._releLed2, this._releLed3, this._releLed4,
                this._releLed5, this._releLed6, this._releLed7, this._releLed8,
            };

            this._limitLeds = new[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4,
                this._ImaxLed5, this._ImaxLed6, this._ImaxLed7, this._ImaxLed8,
                this._I0maxLed1, this._I0maxLed2, this._I0maxLed3, this._I0maxLed4,
                this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8,

                this._InmaxLed1, this._InmaxLed2, this._InmaxLed3, this._InmaxLed4,
                this._IgLed5, this._IgLed6, this._I21Led7, this._I21Led8
            };

            this._externalDef = new []
            {
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };

            this._faultStateLeds = new[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4,
                this._faultStateLed5, this._faultStateLed6
            };

            this._ssl = new[]
            {
                this.sslLed1, this.sslLed2, this.sslLed3, this.sslLed4, this.sslLed5, this.sslLed6, this.sslLed7, this.sslLed8,
                this.sslLed9, this.sslLed10, this.sslLed11, this.sslLed12, this.sslLed13, this.sslLed14, this.sslLed15, this.sslLed16,
                this.sslLed17, this.sslLed18, this.sslLed19, this.sslLed20, this.sslLed21, this.sslLed22, this.sslLed23, this.sslLed24
            };

            this._faultSignalsLeds = new[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, new LedControl(), 
                this._faultSignalLed5, this._faultSignalLed6, this._faultSignalLed7, this._faultSignalLed8,
                this._faultSignalLed9, this._faultSignalLed10, this._faultSignalLed11, this._faultSignalLed12,
                this._faultSignalLed13, this._faultSignalLed14, this._faultSignalLed15, this._faultSignalLed16
            };

            this._automationLeds = new[]
            {
                this._autoLed1, this._autoLed2, this._autoLed3, this._autoLed4,
                this._autoLed5, this._autoLed6, this._autoLed7, this._autoLed8,
                this._achr, this._chapv
            };
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.MeasuringV50.LoadStructCycle();
                this._device.DiscretV50.LoadStructCycle();
                this._device.DateTimeV50.LoadStructCycle();
                _device.Groups50.LoadStructCycle();
            }
            else
            {
                this._device.MeasuringV50.RemoveStructQueries();
                this._device.DiscretV50.RemoveStructQueries();
                this._device.DateTimeV50.RemoveStructQueries();
                _device.Groups50.RemoveStructQueries();
                this.OnAnalogSignalsLoadFail();
                this.OnDiagnosticLoadFail();
            }
        }
        
        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.MeasuringV50.RemoveStructQueries();
            this._device.DiscretV50.RemoveStructQueries();
            this._device.DateTimeV50.RemoveStructQueries();
            _device.Groups50.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        
        private void OnAnalogSignalsLoadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;
        }

        private void OnAnalogSignalsLoadOk()
        {
            this._averagerTime.Add(this._device.AnalogV50.Value);
        }

        private void AveragerTick()
        {
            this._InBox.Text = this._device.AnalogV50.Value.GetIn(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._IaBox.Text = this._device.AnalogV50.Value.GetIa(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._IbBox.Text = this._device.AnalogV50.Value.GetIb(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._IcBox.Text = this._device.AnalogV50.Value.GetIc(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._I0Box.Text = this._device.AnalogV50.Value.GetI0(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._I1Box.Text = this._device.AnalogV50.Value.GetI1(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._I2Box.Text = this._device.AnalogV50.Value.GetI2(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
            this._IgBox.Text = this._device.AnalogV50.Value.GetIg(this._averagerTime.ValueList, this._device.MeasuringV50.Value);
        }
        
        private void OnDiagnosticLoadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);
            LedManager.TurnOffLeds(this._releLeds);
            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._automationLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._ssl);
            LedManager.TurnOffLeds(this._faultStateLeds);
            LedManager.TurnOffLeds(this._externalDef);
        }

        private void OnDiagnosticLoadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._device.DiscretV50.Value.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._device.DiscretV50.Value.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._device.DiscretV50.Value.Indicators);
            LedManager.SetLeds(this._inputLeds, this._device.DiscretV50.Value.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._device.DiscretV50.Value.OutputSignals);
            LedManager.SetLeds(this._releLeds, this._device.DiscretV50.Value.Rele);
            LedManager.SetLeds(this._limitLeds, this._device.DiscretV50.Value.LimitSignals);
            LedManager.SetLeds(this._automationLeds, this._device.DiscretV50.Value.Automation);
            LedManager.SetLeds(this._faultSignalsLeds, this._device.DiscretV50.Value.FaultSignals);
            LedManager.SetLeds(this._ssl, this._device.DiscretV50.Value.Ssl);
            LedManager.SetLeds(this._faultStateLeds, this._device.DiscretV50.Value.FaultState);
            LedManager.SetLeds(this._externalDef, this._device.DiscretV50.Value.External);
        }

        
        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������ ��5 v50", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber, this._device);
            }
        }

        private void _resetFaultBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "������������ ������� �������");
        }

        private void _resetJA_But_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "������������ ������� ������");
        }
        
        private void _resetIndicatBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ���������");
        }

        private void _breakerOffBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _breakerOnBut_Click_1(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "������ ���", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������", "������� ���",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "������� ���", this._device);
        }

        private void _selectConstraintGroupBut_Click_1(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string msg;
            bool constraintGroup;
            switch (this._manageLed4.State)
            {
                case LedState.NoSignaled:
                    constraintGroup = true;
                    msg = "����������� �� �������� ������";
                    break;
                case LedState.Signaled:
                    msg = "����������� �� ��������� ������";
                    constraintGroup = false;
                    break;
                case LedState.Off:
                    return;
                default:
                    return;
            }
            if (DialogResult.Yes == MessageBox.Show(msg + " ?", "������ ��5 v50", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.ChangeGroup.SaveOneWord((ushort)(constraintGroup ? 0 : 1));
            }
        }

        private void _dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MeasuringFormV50); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR5.Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            this._device.Groups50.Value.SetGroup(Common.VersionConverter(this._device.DeviceVersion), this._setpointsComboBox.SelectedItem.ToString());
            this._device.Groups50.SaveStruct();
        }
    }
}