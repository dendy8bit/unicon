using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v50.Configuration.Structures;
using BEMN.MR5.v50.Configuration.Structures.Achr;
using BEMN.MR5.v50.Configuration.Structures.AddCurrentDefenses;
using BEMN.MR5.v50.Configuration.Structures.Automatic;
using BEMN.MR5.v50.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v50.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v50.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v50.Configuration.Structures.Fault;
using BEMN.MR5.v50.Configuration.Structures.Indicators;
using BEMN.MR5.v50.Configuration.Structures.InputLogicSignals;
using BEMN.MR5.v50.Configuration.Structures.OutputSignals;
using BEMN.MR5.v50.Configuration.Structures.ProgramLogicKeys;
using BEMN.MR5.v50.Configuration.Structures.Relays;
using BEMN.MR5.v50.Configuration.Structures.Switch;
using BEMN.MR5.v50.Configuration.Structures.SystemAndOscConfig;
using BEMN.MR5.v50.Configuration.Structures.Transformer;

namespace BEMN.MR5.v50.Configuration
{
    public partial class MR5V50ConfigurationForm : Form, IFormView
    {
        #region [Const]
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string MR5_NAME = "MR5";
        private const string FILE_NAME = "��5 v50 �������";
        private const string XML_HEAD = "MR5_SET_POINTS";
        private const string MR5v50_BASE_CONFIG_PATH = "\\MR5\\MR5v50_BaseConfig.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";
        #endregion [Const]

        #region [Fields]
        private MR5Device _device;
        private MemoryEntity<ConfigurationStructV50> _config;
        private ConfigurationStructV50 _currentConfig;
        private StructUnion<ConfigurationStructV50> _configValidator;

        private MemoryEntity<ConfigurationStructV50New> _configNew;
        private ConfigurationStructV50New _currentConfigNew;
        private StructUnion<ConfigurationStructV50New> _configValidatorNew;

        private SetpointsValidator<AllDefenceSetpoint, GroupSetpoint> _alldefensesValidator;
        private StructUnion<GroupSetpoint> _groupSetpoinStructUnion;
        private ComboboxSelector _comboBoxSelector;

        private double _ver;
        #endregion [Fields]
        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
                this._resetButton.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
            }
        }

        public MR5V50ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public MR5V50ConfigurationForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;

            _ver = Common.VersionConverter(_device.DeviceVersion);

            if (_ver < 50.06)
            {
                this._config = this._device.ConfigV50;
                this._config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigReadOk);
                this._config.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._processLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);
                });
                this._config.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);

                this._config.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._device.WriteConfiguration();
                    this.IsProcess = false;
                    this._processLabel.Text = string.Empty;
                });
                this._config.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
                this._config.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._processLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);
                });
            }
            else
            {
                this._configNew = this._device.ConfigV50New;
                this._configNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigReadOk);
                this._configNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._processLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);
                });
                this._configNew.ReadOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);

                this._configNew.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._device.WriteConfiguration(new TimeSpan(50));
                    this.IsProcess = false;
                    this._processLabel.Text = string.Empty;
                });
                this._configNew.WriteOk += HandlerHelper.CreateHandler(this, this._exchangeProgressBar.PerformStep);
                this._configNew.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._processLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);
                });
            }

            this.Init();
        }

        ///<Summary>
        /// ����������� ����������� �� ����� � ����������� �� ������
        /// </Summary>
        private void InitVisible()
        {
            if (_ver > 50.05)
            {
                NewCopyGroupBox.Visible = true;
                OldGroupBox.Visible = false;
                this.copyGroupsCombo.DataSource = ConfigParamsV50.CopyGroups;

                // ������������->������� �������->������� ������� 
                this._pereklLabel1.Visible = true;
                this._pereklLabel2.Visible = true;
                this._pereklLabel3.Visible = true;
                this._pereklLabel4.Visible = true;

                this._gr1ComboBox.Visible = true;
                this._gr2ComboBox.Visible = true;
                this._gr3ComboBox.Visible = true;
                this._gr4ComboBox.Visible = true;

                this.label12.Visible = false;
                this._constraintGroupCombo.Visible = false;

                this.groupBox3.Location = new Point(8, 93);
                this.groupBox3.Size = new Size(219, 241);

                // --//-- ������� ���������� 
                this.groupBox7.Location = new Point(8, 340);
                this.groupBox7.Size = new Size(219, 107);
            }
            else
            {
                // ������������->������� �������->������� ������� (�������� ����������)
                this._pereklLabel1.Visible = false;
                this._pereklLabel2.Visible = false;
                this._pereklLabel3.Visible = false;
                this._pereklLabel4.Visible = false;

                this._gr1ComboBox.Visible = false;
                this._gr2ComboBox.Visible = false;
                this._gr3ComboBox.Visible = false;
                this._gr4ComboBox.Visible = false;

                this.label12.Visible = true;
                this._constraintGroupCombo.Visible = true;

                this.groupBox3.Location = new Point(8, 104);
                this.groupBox3.Size = new Size(219, 167);

                // --//-- ������� ���������� (��������� �������� ������)
                this.groupBox7.Location = new Point(8, 280);
                this.groupBox7.Size = new Size(219, 101);
            }
        }

        private void Init()
        {
            InitVisible();

            _currentConfig = new ConfigurationStructV50();
            _currentConfigNew = new ConfigurationStructV50New();
            // ������� �������->������� ������� (������ > 50.05)
            NewStructValidator<ExternalSignalStructNew> externalSignalsValidatorNew = new NewStructValidator<ExternalSignalStructNew>
            (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._keyOnCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._extOffCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._extOnCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._signalizationCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._gr1ComboBox, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._blockSdtu, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._gr2ComboBox, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._gr3ComboBox, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._gr4ComboBox, ConfigParamsV50.SwitchSignals)
            );

            // ������� �������->������� ������� (������ <= 50.05)
            NewStructValidator<ExternalSignalStruct> externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._keyOnCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._extOffCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._extOnCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._signalizationCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._constraintGroupCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._blockSdtu, ConfigParamsV50.SwitchSignals)
            );


            NewStructValidator<MeasuringTransformerV50> transformerValidator = new NewStructValidator<MeasuringTransformerV50>(
                this._toolTip,
                new ControlInfoCombo(this._TT_typeCombo, ConfigParamsV50.TransformerType),
                new ControlInfoText(this._TT_Box, new CustomUshortRule(0, 5000)),
                new ControlInfoText(this._TTNP_Box, new CustomUshortRule(0, 1000)),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40)
            );
            NewStructValidator<AchrAndChapvStruct> achrAndChapvValidator = new NewStructValidator<AchrAndChapvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._achrEnter, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._achrBlock, ConfigParamsV50.SwitchSignals),
                new ControlInfoText(this._achrTime, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._achrOsc, ConfigParamsV50.OscAchr),
                new ControlInfoCombo(this._chapvEnter, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._chapvBlock, ConfigParamsV50.SwitchSignals),
                new ControlInfoText(this._chapvTime, RulesContainer.IntTo3M)
            );

            NewCheckedListBoxValidator<KeysStruct> keysValidator = new NewCheckedListBoxValidator<KeysStruct>
                (this.keysListBox, Enumerable.Range(1, 16).Select(o => o.ToString()).ToList());

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
            (this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList,
                    ConfigParamsV50.Faults)),
                new ControlInfoText(this._releDispepairBox, RulesContainer.IntTo3M)
            );

            NewDgwValidatior<InputLogicStruct>[] inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            DataGridView[] lsBoxes =
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8
            };
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                (
                    lsBoxes[i],
                    InputLogicStruct.DISCRETS_COUNT, this._toolTip,
                    new ColumnInfoCombo(ConfigParamsV50.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(ConfigParamsV50.LsState)
                );
            }
            StructUnion<InputLogicSignalStruct> inputLogicUnion = new StructUnion<InputLogicSignalStruct>(inputLogicValidator);

            NewStructValidator<SwitchStruct> switchValidator = new NewStructValidator<SwitchStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._switcherErrorCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this._switcherBlockCombo, ConfigParamsV50.SwitchSignals),
                new ControlInfoText(this._switcherTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherTokBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherDurationBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._manageSignalsButtonCombo, ConfigParamsV50.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, ConfigParamsV50.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, ConfigParamsV50.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, ConfigParamsV50.Zr)
            );

            NewStructValidator<ApvStruct> apvValidator = new NewStructValidator<ApvStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this.apv_conf, ConfigParamsV50.ApvModes),
                new ControlInfoCombo(this.apv_blocking, ConfigParamsV50.SwitchSignals),
                new ControlInfoText(this.apv_time_blocking, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_ready, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_1krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_2krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_3krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_4krat, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvSwithOff)
            );

            NewStructValidator<AvrStruct> avrValidator = new NewStructValidator<AvrStruct>
            (
                this._toolTip,
                new ControlInfoCheck(this.avr_supply_off),
                new ControlInfoCheck(this.avr_switch_off),
                new ControlInfoCheck(this.avr_self_off),
                new ControlInfoCheck(this.avr_abrasion_switch),
                new ControlInfoCombo(this.avr_start, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this.avr_blocking, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this.avr_reset_blocking, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this.avr_abrasion, ConfigParamsV50.SwitchSignals),
                new ControlInfoCombo(this.avr_return, ConfigParamsV50.SwitchSignals),
                new ControlInfoText(this.avr_time_abrasion, RulesContainer.IntTo3M),
                new ControlInfoText(this.avr_time_return, RulesContainer.IntTo3M),
                new ControlInfoText(this.avr_disconnection, RulesContainer.IntTo3M),
                new ControlInfoCheck(this.avr_permit_reset_switch)
            );

            NewStructValidator<LzshStruct> lzshValidator = new NewStructValidator<LzshStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this.lzshConf, ConfigParamsV50.Lzsh),
                new ControlInfoText(this.lzsh_constraint, RulesContainer.Ustavka40)
            );

            NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct> externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct>
            (
                this._externalDefenseGrid, 8, this._toolTip,
                new ColumnInfoCombo(ConfigParamsV50.ExternalDefensesNames, ColumnsType.NAME), //1
                new ColumnInfoCombo(ConfigParamsV50.Modes), //2
                new ColumnInfoCombo(ConfigParamsV50.ExternalDefenseSignals), //3
                new ColumnInfoCombo(ConfigParamsV50.ExternalDefenseSignals), //4
                new ColumnInfoText(RulesContainer.IntTo3M), //5
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(ConfigParamsV50.ExternalDefenseSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
            )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                    (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13),
                        new TurnOffRule(5, false, false, 6, 7, 8)
                    )
                }
            };

            Func<DataGridView, IValidatingRule> currentDefenseFunc = dgv =>
            {
                try
                {
                    DataGridViewCell curCell = dgv.Tag as DataGridViewCell;
                    if (curCell == null)
                    {
                        return RulesContainer.IntTo3M;
                    }
                    DataGridViewCell masterCell = dgv[5, curCell.RowIndex];
                    if (masterCell.Value.ToString() == ConfigParamsV50.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            };

            NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
            (
                this._currentDefensesGrid, 4, _toolTip,
                new ColumnInfoCombo(ConfigParamsV50.CurrentDefensesNames, ColumnsType.NAME), //0
                new ColumnInfoCombo(ConfigParamsV50.Modes), //1
                new ColumnInfoCombo(ConfigParamsV50.SwitchSignals), //2
                new ColumnInfoCombo(ConfigParamsV50.TokParameter), //3
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(ConfigParamsV50.FeatureLight),
                new ColumnInfoTextDgvDependent(currentDefenseFunc, this._currentDefensesGrid),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCombo(ConfigParamsV50.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()  //12
            )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                    (
                        this._currentDefensesGrid,
                        new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(7, false, false, 8)
                    )
                }
            };

            NewDgwValidatior<AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct> otherCurrentDefenseValidatior = new NewDgwValidatior
                <AllCurrentDefensesOtherStruct, CurrentDefenseOtherStruct>
                (
                    new[] { this._otherCurrentDefensesGrid, this._inDefenseGrid }, new[] { 4, 2 }, _toolTip,
                    new ColumnInfoCombo(ConfigParamsV50.OtherCurrentDefensesNames, ColumnsType.NAME), //0
                    new ColumnInfoCombo(ConfigParamsV50.Modes), //1
                    new ColumnInfoCombo(ConfigParamsV50.SwitchSignals), //2
                    new ColumnInfoText(RulesContainer.Ustavka40, true, false),
                    new ColumnInfoText(RulesContainer.Ustavka5, false, true),
                    new ColumnInfoText(RulesContainer.IntTo3M),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.IntTo3M),
                    new ColumnInfoCombo(ConfigParamsV50.OscModes),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck() //10
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._otherCurrentDefensesGrid,
                            new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                            new TurnOffRule(6, false, false, 7)
                        ),
                        new TurnOffDgv
                        (
                            this._inDefenseGrid,
                            new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                            new TurnOffRule(6, false, false, 7)
                        )
                    }
                };

            StructUnion<SetpointStruct> currentUnion = new StructUnion<SetpointStruct>
                (currentDefenseValidatior, otherCurrentDefenseValidatior);

            SetpointsValidator<AllSetpointsStruct, SetpointStruct> currentSetpointsValidator = new SetpointsValidator
                <AllSetpointsStruct, SetpointStruct>
                (new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup, this._groupChangeButton), currentUnion);

            NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> addCurrentDefenseValidator =
                new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (
                    new[] { this._igDefenseGrid, this._i1i2DefenseGrid }, new[] { 1, 1 }, this._toolTip,
                    new ColumnInfoCombo(ConfigParamsV50.AddDefenseNames, ColumnsType.NAME),
                    new ColumnInfoCombo(ConfigParamsV50.Modes), //0
                    new ColumnInfoCombo(ConfigParamsV50.SwitchSignals), //1
                    new ColumnInfoText(RulesContainer.Ustavka5, true, false), //3
                    new ColumnInfoText(RulesContainer.Ustavka100, false, true),
                    new ColumnInfoText(RulesContainer.IntTo3M),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.IntTo3M),
                    new ColumnInfoCombo(ConfigParamsV50.OscModes),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck()  //11
                )
                {
                    TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._igDefenseGrid,
                            new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                            new TurnOffRule(6, false, false, 7)
                        ),
                        new TurnOffDgv
                        (
                            this._i1i2DefenseGrid,
                            new TurnOffRule(1, ConfigParamsV50.Modes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
                        )
                    }
                };

            SetpointsValidator<AddSetpointsStruct, AllAddCurrentDefensesStruct> currentAddSetpointsValidator =
                new SetpointsValidator<AddSetpointsStruct, AllAddCurrentDefensesStruct>
                (new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup, this._groupChangeButton, true),
                    addCurrentDefenseValidator);

            CheckedListBox[] vlsBoxes =
            {
                this.VLScheckedListBox1,this.VLScheckedListBox2,this.VLScheckedListBox3,this.VLScheckedListBox4,
                this.VLScheckedListBox5,this.VLScheckedListBox6,this.VLScheckedListBox7,this.VLScheckedListBox8
            };

            NewCheckedListBoxValidator<OutputLogicStruct>[] vlsValidator =
                new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(vlsBoxes[i],
                    ConfigParamsV50.VlsSignals);
            }
            StructUnion<OutputLogicSignalStruct> vlsUnion = new StructUnion<OutputLogicSignalStruct>(vlsValidator);

            NewDgwValidatior<AllRelaysStruct, RelayStruct> relayValidator =
                new NewDgwValidatior<AllRelaysStruct, RelayStruct>
                (
                    this._outputReleGrid, AllRelaysStruct.RELAY_COUNT, this._toolTip,
                    new ColumnInfoCombo(Enumerable.Range(1, AllRelaysStruct.RELAY_COUNT).Select(s => s.ToString()).ToList(), ColumnsType.NAME),
                    new ColumnInfoCombo(ConfigParamsV50.ReleyType),
                    new ColumnInfoCombo(ConfigParamsV50.RelaySignals),
                    new ColumnInfoText(RulesContainer.IntTo3M)
                );

            NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator =
                new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                    this._outputIndicatorsGrid, AllIndicatorsStruct.INDICATORS_COUNT, this._toolTip,
                    new ColumnInfoCombo(Enumerable.Range(1, AllIndicatorsStruct.INDICATORS_COUNT).Select(s => s.ToString()).ToList(), ColumnsType.NAME),
                    new ColumnInfoCombo(ConfigParamsV50.ReleyType),
                    new ColumnInfoCombo(ConfigParamsV50.RelaySignals),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck()
                );

            NewStructValidator<SystemAndOscConfigStruct> oscValidator = new NewStructValidator<SystemAndOscConfigStruct>
            (
                this._toolTip,
                new ControlInfoText(this.oscPercent, new CustomUshortRule(1, 100)),
                new ControlInfoCombo(this.oscFix, ConfigParamsV50.OscFixation),
                new ControlInfoCombo(this.oscLenCombo, Enumerable.Range(1, 32).Select(s => s.ToString()).ToList())
            );

            this._groupSetpoinStructUnion = new StructUnion<GroupSetpoint>
            (
                currentDefenseValidatior,
                otherCurrentDefenseValidatior,
                addCurrentDefenseValidator
            );

            this._alldefensesValidator = new SetpointsValidator<AllDefenceSetpoint, GroupSetpoint>
            (
                _comboBoxSelector = new ComboboxSelector(this.currentGroupCombo, ConfigParamsV50.Groups), this._groupSetpoinStructUnion
            );

            _comboBoxSelector.OnSelect += ReadAllData;

            if (_ver < 50.06)
            {
                this._configValidator = new StructUnion<ConfigurationStructV50>(
                    transformerValidator, achrAndChapvValidator, keysValidator, externalSignalsValidator, faultValidator,
                    inputLogicUnion, switchValidator, apvValidator, avrValidator, lzshValidator,
                    externalDefenseValidatior,
                    currentSetpointsValidator, currentAddSetpointsValidator, vlsUnion,
                    relayValidator, indicatorValidator, oscValidator
                );
            }
            else
            {
                this._configValidatorNew = new StructUnion<ConfigurationStructV50New>(
                    transformerValidator, achrAndChapvValidator, keysValidator, externalSignalsValidatorNew, faultValidator,
                    inputLogicUnion, switchValidator, apvValidator, avrValidator, lzshValidator, externalDefenseValidatior,
                    _alldefensesValidator, vlsUnion, relayValidator, indicatorValidator, oscValidator
                );
            }

            
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
            this.ResetSetpoints(false);
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._config?.RemoveStructQueries();
            this._configNew?.RemoveStructQueries();
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }
        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(true);
        }

        private void _resetButton_Click(object sender, EventArgs e)
        {
            this._configValidator?.Reset();
            this._configValidatorNew?.Reset();
        }

        private void ResetSetpoints(bool isDialog)   //�������� ������� ������� �� �����, isDialog - ���������� ����� �� ������
        {
            try
            {
                if (isDialog)
                {
                    DialogResult res = MessageBox.Show(@"��������� ������� �������?", @"������� �������",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (res == DialogResult.No) return;
                }

                this.Deserialize(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR5v50_BASE_CONFIG_PATH));
                this._processLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                            "��������", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        this._configValidator?.Reset();
                        this._configValidatorNew?.Reset();
                    }

                }
                else
                {
                    this._configValidator?.Reset();
                    this._configValidatorNew?.Reset();
                }
            }
        }
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (_ver < 50.06)
            {
                this._exchangeProgressBar.Maximum = this._config.Slots.Count;
            }
            else
            {
                this._exchangeProgressBar.Maximum = this._configNew.Slots.Count;
            }
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������ ������������";
            this.IsProcess = true;
            if (_ver < 50.06)
            {
                this._config.LoadStruct(new TimeSpan(50));
            }
            else
            {
                this._configNew.LoadStruct();
            }
        }

        private void ConfigReadOk()
        {
            if (_ver < 50.06)
            {
                this._currentConfig = this._config.Value;
                this._configValidator.Set(this._currentConfig);
            }
            else
            {
                _currentConfigNew = _configNew.Value;
                _configValidatorNew.Set(_currentConfigNew);
            }
            this.IsProcess = false;
            this._processLabel.Text = "������������ ��������� �������";
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            string mes;

            if (_ver < 50.06)
            {
                if (this._configValidator.Check(out mes, true))
                {
                    if (DialogResult.Yes ==
                        MessageBox.Show("�������� ������������ ��5 v50 �" + this._device.DeviceNumber + " ?", "��������",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        this._exchangeProgressBar.Value = 0;
                        this._processLabel.Text = "���� ������";
                        this.IsProcess = true;

                        this._config.Value = this._configValidator.Get();
                        this._config.SaveStruct(new TimeSpan(50));

                    }
                }
                else
                {
                    MessageBox.Show("���������� �������� ������������. ���������� ������������ �������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (this._configValidatorNew.Check(out mes, true))
                {
                    if (DialogResult.Yes ==
                        MessageBox.Show("�������� ������������ ��5 v50 �" + this._device.DeviceNumber + " ?", "��������",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        this._exchangeProgressBar.Value = 0;
                        this._processLabel.Text = "���� ������";
                        this.IsProcess = true;

                        this._configNew.Value = this._configValidatorNew.Get();
                        this._configNew.SaveStruct(new TimeSpan(50));

                    }
                }
                else
                {
                    MessageBox.Show("���������� �������� ������������. ���������� ������������ �������.", "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void oscLenCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.oscLenBox.Text = (21162 * 2 / (2 + this.oscLenCombo.SelectedIndex)).ToString();
        }

        #region Save/load XML
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            try
            {
                this._openConfigurationDlg.FileName = FILE_NAME;
                if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
                {
                    this.Deserialize(this._openConfigurationDlg.FileName);
                    this._processLabel.Text = string.Format("���� {0} ������� ��������",
                        this._openConfigurationDlg.FileName);
                }
            }
            catch
            {
                this._processLabel.Text = FILE_LOAD_FAIL;
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(binFileName);
            List<byte> values = new List<byte>();
            XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", MR5_NAME));
            if (a == null)
                throw new NullReferenceException();
            values.AddRange(Convert.FromBase64String(a.InnerText));
            if (_ver < 50.06 && values.Count > 864)// ��� ������������� ������������ �� ����, ��� ��� ����� ���� Ethernet.
            {                                                                                      // �� ������ ���������������� �������� ������� ���������� �����(RS485 ��� Ethernet)
                values.RemoveAt(46);
                values.RemoveAt(46);
            }

            if (_ver < 50.06)
            {
                this._currentConfig.InitStruct(values.ToArray());
                this._configValidator.Set(this._currentConfig);
            }
            else
            {
                this._currentConfigNew.InitStruct(values.ToArray());
                this._configValidatorNew.Set(this._currentConfigNew);
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            try
            {
                string mes;

                if (_ver < 50.06)
                {
                    if (this._configValidator.Check(out mes, true) && _configValidator != null)
                    {
                        this._saveConfigurationDlg.FileName = FILE_NAME;
                        if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                        {
                            this._currentConfig = this._configValidator.Get();
                            
                            this.Serialize(this._saveConfigurationDlg.FileName);
                            this._processLabel.Text = "���� " + this._saveConfigurationDlg.FileName + " ��������";
                        }
                    }
                }
                else
                {
                    if (this._configValidatorNew.Check(out mes, true) && _configValidatorNew != null)
                    {
                        this._saveConfigurationDlg.FileName = FILE_NAME;
                        if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                        {
                            _currentConfigNew = _configValidatorNew.Get();
                            
                            this.Serialize(this._saveConfigurationDlg.FileName);
                            this._processLabel.Text = "���� " + this._saveConfigurationDlg.FileName + " ��������";
                        }
                    }
                        
                }
            }
            catch
            {
                this._processLabel.Text = FILE_SAVE_FAIL;
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        public void Serialize(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement(MR5_NAME));
            ushort[] values;
            if (_ver < 50.06)
            {
                values = this._currentConfig.GetValues();
            }
            else
            {
                values = _currentConfigNew.GetValues();
            }
           
            XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", MR5_NAME));
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
            if (doc.DocumentElement == null)
            {
                throw new NullReferenceException();
            }
            doc.DocumentElement.AppendChild(element);
            doc.Save(fileName);
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            string str;

            if (_ver < 50.06)
            {
                if (this._configValidator.Check(out str, true))
                {
                    this._currentConfig = this._configValidator.Get();
                    this._currentConfig.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentConfig.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._processLabel.Text = HtmlExport.Export(Resources.MR5_50_Main, Resources.MR5_50_Res, this._currentConfig,
                        MR5_NAME, this._device.DeviceVersion);
                }
                else
                {
                    MessageBox.Show("���������� ��������� ������������ � HTML. ���������� ������������ �������.",
                        "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            else
            {
                if (this._configValidatorNew.Check(out str, true))
                {
                    ExportGroupForm exportGroup = new ExportGroupForm(4);

                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        this.IsProcess = false;
                        this._processLabel.Text = string.Empty;
                        return;
                    }

                    this._currentConfigNew = this._configValidatorNew.Get();
                    this._currentConfigNew.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentConfigNew.DeviceNumber = this._device.DeviceNumber.ToString();
                    _currentConfigNew.Group = (int)exportGroup.SelectedGroup;

                    this.SaveToHtml(this._currentConfigNew, exportGroup.SelectedGroup);
                    exportGroup.IsExport = false;
                }
                else
                {
                    MessageBox.Show("���������� ��������� ������������ � HTML. ���������� ������������ �������.",
                        "��������", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} ������� ������ {1} ��� ������", "��5", this._device.DeviceVersion);
                this._processLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_50_All_Configuration, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} ������� ������ {1} ������ {2}", "��5", this._device.DeviceVersion, (int)group);
                this._processLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_50_Configuration, fileName, str);
            }
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointStruct)
            {
                this.ResetSetpoints(true);
                return;
            }
            if (e.ClickedItem == this.resetSetpointStruct)
            {
                this._configValidator?.Reset();
                this._configValidatorNew?.Reset();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }
        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled =
                this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }

        #region [IFormView Members]
        public Type FormDevice => typeof(MR5Device);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(MR5V50ConfigurationForm);
        public bool ForceShow => false;
        public Image NodeImage => Resources.config.ToBitmap();
        public string NodeName => "������������";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;
        #endregion [IFormView Members]

        private void copyBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string message;
                if (this._groupSetpoinStructUnion.Check(out message, true))
                {
                    GroupSetpoint[] allSetpoints =
                        this.CopySetpoints<AllDefenceSetpoint, GroupSetpoint>(this._groupSetpoinStructUnion, this._alldefensesValidator);
                   
                    this._currentConfigNew.AllGroup.Setpoints = allSetpoints;
                    this._alldefensesValidator.Set(this._currentConfigNew.AllGroup);
                    
                    MessageBox.Show("����������� ���������");
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator) where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this.copyGroupsCombo.SelectedItem == ConfigParamsV50.CopyGroups.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this.copyGroupsCombo.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this.currentGroupCombo.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }


        private void ReadAllData()
        {
            this._configNew.Value = this._configValidatorNew.Get();
        }
    }
}