﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v50.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesStruct : StructBase, IDgvRowsContainer<CurrentDefenseStruct>
    {
        [Layout(0, Count = 4)]
        private CurrentDefenseStruct[] _currentDefenses;

        public CurrentDefenseStruct[] Rows
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }
    }
}
