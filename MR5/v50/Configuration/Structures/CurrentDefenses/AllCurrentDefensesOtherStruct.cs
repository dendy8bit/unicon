﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v50.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesOtherStruct : StructBase, IDgvRowsContainer<CurrentDefenseOtherStruct>
    {
        [Layout(0, Count = 6)]
        private CurrentDefenseOtherStruct[] _currentDefenses;

        public CurrentDefenseOtherStruct[] Rows
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }
    }
}
