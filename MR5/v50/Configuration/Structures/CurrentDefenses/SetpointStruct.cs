﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v50.Configuration.Structures.CurrentDefenses
{
    public class SetpointStruct : StructBase
    {
        [Layout(0, Count = 4)] private ushort[] _res;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses;
        [Layout(2)] private AllCurrentDefensesOtherStruct _currentDefensesOther;


        [BindingProperty(0)]
        [XmlElement(ElementName = "Основные_токовые")]
        public AllCurrentDefensesStruct CurrentDefenses
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Токовые_остальные")]
        public AllCurrentDefensesOtherStruct CurrentDefensesOther
        {
            get { return this._currentDefensesOther; }
            set { this._currentDefensesOther = value; }
        }
    }
}
