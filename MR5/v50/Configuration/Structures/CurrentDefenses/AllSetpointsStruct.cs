﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR5.v50.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {
        [Layout(0)] private SetpointStruct _main;
        [Layout(1)] private SetpointStruct _reserve;

        [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                SetpointStruct[] result = 
                {
                    new SetpointStruct
                    {
                        CurrentDefenses = this._main.CurrentDefenses.Clone<AllCurrentDefensesStruct>(),
                        CurrentDefensesOther = this._main.CurrentDefensesOther.Clone<AllCurrentDefensesOtherStruct>()
                    },
                    new SetpointStruct
                    {
                        CurrentDefenses = this._reserve.CurrentDefenses.Clone<AllCurrentDefensesStruct>(),
                        CurrentDefensesOther = this._reserve.CurrentDefensesOther.Clone<AllCurrentDefensesOtherStruct>()
                    }
                };
                return result;
            }
            set
            {
                this._main.CurrentDefenses = value[0].CurrentDefenses;
                this._main.CurrentDefensesOther = value[0].CurrentDefensesOther;

                this._reserve.CurrentDefenses = value[1].CurrentDefenses;
                this._reserve.CurrentDefensesOther = value[1].CurrentDefensesOther;
            }
        }

        [XmlElement(ElementName = "Основная")]
        public SetpointStruct Main
        {
            get
            {
                return new SetpointStruct
                {
                    CurrentDefenses = this._main.CurrentDefenses.Clone<AllCurrentDefensesStruct>(),
                    CurrentDefensesOther = this._main.CurrentDefensesOther.Clone<AllCurrentDefensesOtherStruct>()
                };
            }
            set
            {
                this._main.CurrentDefenses = value.CurrentDefenses;
                this._main.CurrentDefensesOther = value.CurrentDefensesOther;
            }
        }

        [XmlElement(ElementName = "Резервная")]
        public SetpointStruct Reserv
        {
            get
            {
                return new SetpointStruct
                {
                    CurrentDefenses = this._reserve.CurrentDefenses.Clone<AllCurrentDefensesStruct>(),
                    CurrentDefensesOther = this._reserve.CurrentDefensesOther.Clone<AllCurrentDefensesOtherStruct>()
                };
            }
            set
            {
                this._reserve.CurrentDefenses = value.CurrentDefenses;
                this._reserve.CurrentDefensesOther = value.CurrentDefensesOther;
            }
        }
    }
}
