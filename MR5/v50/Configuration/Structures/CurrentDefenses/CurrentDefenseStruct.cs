﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.CurrentDefenses
{
    public class CurrentDefenseStruct : StructBase
    {
        [Layout(0)] private ushort _config;     //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block;      //вход блокировки
        [Layout(2)] private ushort _srab;       //уставка срабатывания
        [Layout(3)] private ushort _tsrOrKoef;  //уставка времени срабатывания/кэффициент
        [Layout(4)] private ushort _res;        //уставка пуска по U (резерв)
        [Layout(5)] private ushort _u;          //уставка ускорения

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, ConfigParamsV50.Modes, 0, 1); }
            set { this._config = Validator.Set(value, ConfigParamsV50.Modes, this._config, 0, 1); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, ConfigParamsV50.SwitchSignals); }
            set { this._block = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Параметр")]
        public string Parameter
        {
            get { return Validator.Get(this._config, ConfigParamsV50.TokParameter, 8); }
            set { this._config = Validator.Set(value, ConfigParamsV50.TokParameter, this._config, 8); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public double Srab
        {
            get { return Math.Round((double) this._srab*4001/(65536*100), 2, MidpointRounding.AwayFromZero); }
            set { this._srab = (ushort) (value*65536*100/4001); }
        }
        
        [BindingProperty(4)]
        [XmlElement(ElementName = "Характеристика")]
        public string Feature
        {
            get { return Validator.Get(this._config, ConfigParamsV50.FeatureLight, 12); }
            set { this._config = Validator.Set(value, ConfigParamsV50.FeatureLight, this._config, 12); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "tср_коэффициент")]
        public int TimeSrab
        {
            get
            {
                return Common.GetBit(this._config, 12)
                    ? this._tsrOrKoef
                    : ValuesConverterCommon.GetWaitTime(this._tsrOrKoef);
            }
            set
            {
                this._tsrOrKoef = Common.GetBit(this._config, 12)
                    ? (ushort) value
                    : ValuesConverterCommon.SetWaitTime(value);
            }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Speedup
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Уставка_ускорения")]
        public int Tuskor
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Осциллограф
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config, ConfigParamsV50.OscModes, 3, 4); }
            set { this._config = Validator.Set(value, ConfigParamsV50.OscModes, this._config, 3, 4); }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "УРОВ")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }
        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }
    }
}