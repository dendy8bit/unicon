﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.OscConfig
{
    public class OscConfigStruct50 : StructBase
    {
        private const ushort PAGE_SIZE = 1024;
        private const ushort FULL_OSC_SIZE_IN_PAGES = 496;
        private const int FULL_OSC_SIZE_IN_WORDS = 0xF7FF8/2;
        private const ushort ONE_OSC_SIZE_MAX = 2*21162;

        [Layout(0)] private ushort _config;

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(this._config, ConfigParamsV50.OscCount, 0, 1, 2, 3, 4); }
            set { this._config = Validator.Set(value, ConfigParamsV50.OscCount, this._config, 0, 1, 2, 3, 4); }
        }
        [BindingProperty(1)]
        public string Fixation
        {
            get { return Validator.Get(this._config, ConfigParamsV50.OscFixation, 7); }
            set { this._config = Validator.Set(value, ConfigParamsV50.OscFixation, this._config, 7); }
        }
        [BindingProperty(2)]
        public ushort Percent
        {
            get { return (ushort) (Common.GetBits(this._config, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._config = Common.SetBits(this._config, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
        /// <summary>
        /// Размер одной страницы в словах
        /// </summary>
        public ushort PageSize
        {
            get { return PAGE_SIZE; }
        }
        /// <summary>
        /// Размер всего осциллографа в страницай
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return FULL_OSC_SIZE_IN_PAGES; }
        }
        /// <summary>
        /// Размер всего осциллографа в словах
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get { return FULL_OSC_SIZE_IN_WORDS; }
        }
        /// <summary>
        /// Размер одной осциллограммы в осчетах (мс)
        /// </summary>
        public int OscLen
        {
            get
            {
                int mode = Common.GetBits(this._config, 0, 1, 2);
                return ONE_OSC_SIZE_MAX/(mode + 2);
            }
        }
    }
}
