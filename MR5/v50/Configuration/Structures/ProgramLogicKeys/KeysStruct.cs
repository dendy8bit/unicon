﻿using System;
using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.ProgramLogicKeys
{
    /// <summary>
    /// Конфигурация программных ключей
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_ключей")]
    public class KeysStruct : StructBase, IBitField, IXmlSerializable
    {
        [Layout(0)] private ushort _keys;
        [XmlIgnore]
        public BitArray Bits
        {
            get
            {
                var bytes= Common.TOBYTE(this._keys);
                Common.SwapArrayItems(ref  bytes);
                return new BitArray(bytes);
            }
            set
            {
                for (int i = 0; i < 16; i++)
                {
                    this._keys = Common.SetBit(this._keys, i, value.Get(i));
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < this.Bits.Count; i++)
            {
                writer.WriteElementString("Ключ", this.Bits[i] ? "Да" : "Нет");
            }
        }
    }
}
