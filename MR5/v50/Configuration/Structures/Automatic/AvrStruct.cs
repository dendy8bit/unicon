﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.Automatic
{
    /// <summary>
    /// Конфигурация АВР
    /// </summary>
    public class AvrStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _block; //вход блокировки АВР
        [Layout(2)] private ushort _clear; //вход сброс блокировки АВР
        [Layout(3)] private ushort _start; //вход сигнала запуск АВР
        [Layout(4)] private ushort _on; //вход АВР срабатывания
        [Layout(5)] private ushort _timeOn; //время АВР срабатывания
        [Layout(6)] private ushort _off; //вход АВР возврат
        [Layout(7)] private ushort _timeOff; //время АВР возврат
        [Layout(8)] private ushort _timeOtkl; //задержка отключения резерва
        [Layout(9)] private ushort _rez;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// от сигнала
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "от_сигнала")]
        public bool BySignalXml
        {
            get { return Common.GetBit(this._config,0); }
            set { this._config = Common.SetBit(this._config,0,value) ; }
        }

        /// <summary>
        /// по отключению
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "по_отключению")]
        public bool ByOffXml
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        /// <summary>
        /// по самоотключению
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "по_самоотключению")]
        public bool BySelfOffXml
        {
            get { return Common.GetBit(this._config, 1); }
            set { this._config = Common.SetBit(this._config, 1, value); }
        }

        /// <summary>
        /// по защите
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "по_защите")]
        public bool ByDefenseXml
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }


        /// <summary>
        /// Пуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Пуск")]
        public string StartXml
        {
            get { return Validator.Get(this._start, ConfigParamsV50.SwitchSignals); }
            set { this._start = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }



        /// <summary>
        /// вход блокировки АВР
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "вход_блокировки_АВР")]
        public string BlockingXml
        {
            get { return Validator.Get(this._block, ConfigParamsV50.SwitchSignals); }
            set { this._block = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        /// <summary>
        /// вход сброс блокировки АВР
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "вход_сброс_блокировки_АВР")]
        public string ClearXml
        {
            get { return Validator.Get(this._clear, ConfigParamsV50.SwitchSignals); }
            set { this._clear = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }


        /// <summary>
        /// вход АВР срабатывания
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "вход_АВР_срабатывания")]
        public string OnXml
        {
            get { return Validator.Get(this._on, ConfigParamsV50.SwitchSignals); }
            set { this._on = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        /// <summary>
        /// вход АВР возврат
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "вход_АВР_возврат")]
        public string BackXml
        {
            get { return Validator.Get(this._off, ConfigParamsV50.SwitchSignals); }
            set { this._off = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        /// <summary>
        /// время АВР срабатывания
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "время_АВР_срабатывания")]
        public int TimeOn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOn); }
            set { this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// время АВР возврат
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "время_АВР_возврат")]
        public int TimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOff); }
            set { this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// задержка отключения резерва
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "задержка_отключения_резерва")]
        public int TimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOtkl); }
            set { this._timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Сброс")]
        public bool ResetXml
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }

        #endregion [Properties]



    }
}
