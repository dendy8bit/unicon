﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR5.v70.Configuration;

namespace BEMN.MR5.v50.Configuration.Structures.Automatic
{
    /// <summary>
    /// конфигурация АПВ
    /// </summary>
    public class ApvStruct : StructBase
    {
        #region [Private fields]

        //биты 00FF
        //(0000) - выведено
        //(0001) - 1 крат
        //(0002) - 2 крат
        //(0003) - 3 крат
        //(0004) - 4 крат
        //бит 0100 - запуск АПВ по самопроизвольному отключению выключателя
        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _block; //вход блокировки АПВ
        [Layout(2)] private ushort _timeBlock; //время блокировки АПВ
        [Layout(3)] private ushort _ctrl; //время готовности АПВ
        [Layout(4)] private ushort _step1; //время 1 крата АПВ
        [Layout(5)] private ushort _step2; //время 2 крата АПВ
        [Layout(6)] private ushort _step3; //время 3 крата АПВ
        [Layout(7)] private ushort _step4; //время 4 крата АПВ 

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.ApvModes, 0, 1, 2, 3); }
            set { this._config = Validator.Set(value, StringsConfig.ApvModes, this._config, 0, 1, 2, 3); }
        }

        /// <summary>
        /// вход блокировки АПВ
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "вход_блокировки_АПВ")]
        public string BlockingInputXml
        {
            get { return Validator.Get(this._block, StringsConfig.LogicSignals); }
            set { this._block = Validator.Set(value, StringsConfig.LogicSignals); }
        }

        /// <summary>
        /// время блокировки АПВ
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "время_блокировки_АПВ")]
        public int TimeBlocking
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeBlock); }
            set { this._timeBlock = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// время готовности АПВ
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "время_готовности_АПВ")]
        public int TimeReady
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ctrl); }
            set { this._ctrl = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// 1КРАТ
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "КРАТ1")]
        public int OneCrat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._step1); }
            set { this._step1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// 2КРАТ
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "КРАТ2")]
        public int TwoCrat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._step2); }
            set { this._step2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// 3КРАТ
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "КРАТ3")]
        public int ThreeCrat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._step3); }
            set { this._step3 = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// 4КРАТ
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "КРАТ4")]
        public int FourCrat
        {
            get { return ValuesConverterCommon.GetWaitTime(this._step4); }
            set { this._step4 = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// запуск АПВ по самопроизвольному отключению выключателя
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "запуск_АПВ_по_самоотключению_выключателя")]
        public bool Run
        {
            get { return Common.GetBit(this._config, 8); }
            set { this._config = Common.SetBit(this._config, 8, value); }
        }
        #endregion [Properties]

    }
}
