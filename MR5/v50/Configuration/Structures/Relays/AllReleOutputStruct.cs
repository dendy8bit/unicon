using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v50.Configuration.Structures.Relays
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllRelaysStruct : StructBase, IDgvRowsContainer<RelayStruct>
    {
        public const int RELAY_COUNT = 8;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT)] private RelayStruct[] _relays;

        [Layout(1, Count = RELAY_COUNT)] private RelayStruct[] _reserve;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]

        public RelayStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }
    }
}