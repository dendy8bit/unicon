﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR5.v70.Configuration;

namespace BEMN.MR5.v50.Configuration.Structures.Relays
{
    /// <summary>
    /// параметры выходных реле
    /// </summary>
    [XmlType(TypeName = "Одно_реле")]
    public class RelayStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _pulse;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string TypeXml
        {
            get { return Validator.Get(this._signal, ConfigParamsV50.ReleyType,15); }
            set { this._signal = Validator.Set(value, ConfigParamsV50.ReleyType, this._signal, 15); }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string SignalXml
        {
            get { return Validator.Get(this._signal, ConfigParamsV50.RelaySignals,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14); }
            set { this._signal = Validator.Set(value, ConfigParamsV50.RelaySignals, this._signal, 0, 1, 2, 3, 4, 5, 6, 7,8,9,10,11,12,13,14); }
        }
        
        /// <summary>
        /// Время
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Время")]
        public int Wait
        {
            get { return ValuesConverterCommon.GetWaitTime(this._pulse); }
            set { this._pulse = ValuesConverterCommon.SetWaitTime(value); }
        }
        
        #endregion [Properties]
    }
}
