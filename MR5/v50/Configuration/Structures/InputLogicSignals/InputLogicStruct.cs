﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.InputLogicSignals
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        public const int DISCRETS_COUNT = 16;

        [Layout(0)] private ushort _a1;
        [Layout(1)] private ushort _a2;
        
        [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                {
                    this._a1,
                    this._a2,

                };
            }
            set
            {
                this._a1 = value[0];
                this._a2 = value[1];

            }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort val;
                if (ls< 8)
                {
                    val = this._a1;
                }
                else
                {
                    val = this._a2;
                    ls = ls - 8;
                }
                if (Common.GetBit(val, ls))
                {
                    if (Common.GetBit(val, ls + 8))
                    {
                        return ConfigParamsV50.LsState[2];
                    }
                    else
                    {
                        return ConfigParamsV50.LsState[1];
                    }
                }
                return ConfigParamsV50.LsState[0];
            }
            set
            {
                if (ls < 8)
                {
                    if (value == ConfigParamsV50.LsState[0])
                {
                    this._a1 = Common.SetBit(this._a1, ls, false);
                    this._a1 = Common.SetBit(this._a1, ls+8, false);
                }
                    if (value == ConfigParamsV50.LsState[1])
                {
                    this._a1 = Common.SetBit(this._a1, ls, true);
                    this._a1 = Common.SetBit(this._a1, ls + 8, false);
                }
                    if (value == ConfigParamsV50.LsState[2])
                {
                    this._a1 = Common.SetBit(this._a1, ls, true);
                    this._a1 = Common.SetBit(this._a1, ls + 8, true);
                } 
                }
                else
                {
                    ls = ls - 8;
                    if (value == ConfigParamsV50.LsState[0])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, false);
                        this._a2 = Common.SetBit(this._a2, ls + 8, false);
                    }
                    if (value == ConfigParamsV50.LsState[1])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, true);
                        this._a2 = Common.SetBit(this._a2, ls + 8, false);
                    }
                    if (value == ConfigParamsV50.LsState[2])
                    {
                        this._a2 = Common.SetBit(this._a2, ls, true);
                        this._a2 = Common.SetBit(this._a2, ls + 8, true);
                    } 
                }
            }
        }

        [XmlIgnore]
        public string[] LogicSygnalXml
        {
            get
            {
                var result = new string[16];

                var sourse = this.Mass;
                var enumerator = sourse.GetEnumerator();

                int j = 0;
                while (j < 8)
                {
                    enumerator.MoveNext();
                    var temp = (ushort) (enumerator.Current);

                    for (int i = 0; i < 16; i += 2)
                    {
                        var number = Common.GetBits(temp, i, i + 1) >> i;
                        result[j] = ConfigParamsV50.LsState[number];
                        j++;
                    }
                }
                return result;
            }
            set
            {
                int j = 0;
                var result = new List<ushort>();
                while (j < 8)
                {
                    ushort newValue = 0;

                    for (int i = 0; i < 16; i += 2)
                    {
                        var bits = (ushort)ConfigParamsV50.LsState.IndexOf(value[j]);
                        newValue = Common.SetBits(newValue, bits, i, i + 1);
                        j++;
                    }
                    result.Add(newValue);
                }
                this.Mass = result.ToArray();
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
       
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
           
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
