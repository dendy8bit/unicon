﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v50.Configuration.Structures.SystemAndOscConfig
{
    public class SystemAndOscConfigStruct : StructBase
    {
        [Layout(0,Ignore = true)] private ushort _address ;
        [Layout(1,Ignore = true)] private ushort _speed ;
        [Layout(2,Ignore = true)] private ushort _delay;
        [Layout(3,Ignore = true)] private ushort _status;
        [Layout(4)] private ushort _oscConf;

        public const int ONE_OSC_MAX_LEN = 21162;

        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return (ushort)(Common.GetBits(this._oscConf, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._oscConf = Common.SetBits(this._oscConf, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string FixationXml
        {
            get { return Validator.Get(this._oscConf, ConfigParamsV50.OscFixation, 7); }
            set { this._oscConf = Validator.Set(value, ConfigParamsV50.OscFixation, this._oscConf, 7); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return Validator.Get(this._oscConf, Enumerable.Range(1, 32).Select(o=>o.ToString()).ToList(), 0, 1, 2, 3, 4, 5); }
            set { this._oscConf = Validator.Set(value, Enumerable.Range(1, 32).Select(o => o.ToString()).ToList(), this._oscConf, 0, 1, 2, 3, 4, 5); }
        }
        [XmlElement(ElementName = "Длительность")]
        public int Size
        {
            get { return ONE_OSC_MAX_LEN * 2 / (2 + Common.GetBits(this._oscConf, 0, 1, 2, 3, 4, 5, 6)); }
            set { }
        }
    }
}
