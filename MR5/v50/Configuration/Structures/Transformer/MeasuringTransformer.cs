﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v50.Configuration.Structures.Transformer
{
    public class MeasuringTransformerV50 : StructBase
    {
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _tt;
        [Layout(2)] private ushort _ttnp;
        [Layout(3)] private ushort _iMax;
        [Layout(4, Count = 4)] private ushort[] _reserve;

        [XmlElement(ElementName = "Тип_трансформатора")]
        [BindingProperty(0)]
        public string Config
        {
            get { return Validator.Get(this._config, ConfigParamsV50.TransformerType, 0); }
            set { this._config = Validator.Set(value, ConfigParamsV50.TransformerType, this._config, 0); }
        }

        [XmlElement(ElementName = "Первичный_ток_ТТ")]
        [BindingProperty(1)]
        public ushort TT
        {
            get { return this._tt; }
            set { this._tt = value; }
        }

        [XmlElement(ElementName = "Первичный_ток_ТТНП")]
        [BindingProperty(2)]
        public ushort TTNP
        {
            get { return this._ttnp; }
            set { this._ttnp = value; }
        }

        [XmlElement(ElementName = "Imax")]
        [BindingProperty(3)]
        public double Imax
        {
            get { return Math.Round((double) this._iMax*4001/(65536*100), 2, MidpointRounding.AwayFromZero); }
            set { this._iMax = (ushort) (value*65536*100/4001); }
        }
    }
}
