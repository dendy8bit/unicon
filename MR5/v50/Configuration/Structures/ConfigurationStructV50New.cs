﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v50.Configuration.Structures.Achr;
using BEMN.MR5.v50.Configuration.Structures.Automatic;
using BEMN.MR5.v50.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v50.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v50.Configuration.Structures.Fault;
using BEMN.MR5.v50.Configuration.Structures.Indicators;
using BEMN.MR5.v50.Configuration.Structures.InputLogicSignals;
using BEMN.MR5.v50.Configuration.Structures.OutputSignals;
using BEMN.MR5.v50.Configuration.Structures.ProgramLogicKeys;
using BEMN.MR5.v50.Configuration.Structures.Relays;
using BEMN.MR5.v50.Configuration.Structures.Switch;
using BEMN.MR5.v50.Configuration.Structures.SystemAndOscConfig;
using BEMN.MR5.v50.Configuration.Structures.Transformer;

namespace BEMN.MR5.v50.Configuration.Structures
{
    [Serializable, XmlRoot(ElementName = "МР5_V50")]
    public class ConfigurationStructV50New: StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "МР5"; }
            set { }
        }
        [XmlElement(ElementName = "Номер_группы")]
        public int Group { get; set; }

        [Layout(0)] private MeasuringTransformerV50 _transformer;
        [Layout(1)] private AchrAndChapvStruct _achrAndChapv;
        [Layout(2)] private KeysStruct _keys;
        [Layout(3)] private ExternalSignalStructNew _externalSignalsNew;
        [Layout(4)] private FaultStruct _faultSignal;
        [Layout(5)] private InputLogicSignalStruct _inputLogicSignals;
        [Layout(6)] private SwitchStruct _switch;
        [Layout(7)] private ApvStruct _apv;
        [Layout(8)] private AvrStruct _avr;
        [Layout(9)] private LzshStruct _lzsh;
        [Layout(10)] private AllExternalDefensesStruct _externalDefenses;
        [Layout(11)] private AllDefenceSetpoint _allCurrents; // группы уставок
        [Layout(12)] private OutputLogicSignalStruct _outputLogicSignals;
        [Layout(13)] private AllRelaysStruct _allRelays;
        [Layout(14)] private AllIndicatorsStruct _allIndicators;
        [Layout(15)] private SystemAndOscConfigStruct _oscConfig;
        
        [BindingProperty(0)]
        [XmlElement(ElementName = "Трансформатор")]
        public MeasuringTransformerV50 Transformer
        {
            get { return this._transformer; }
            set { this._transformer = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "АЧР_ЧАПВ")]
        public AchrAndChapvStruct AchrAndChapv
        {
            get { return this._achrAndChapv; }
            set { this._achrAndChapv = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Ключи")]
        public KeysStruct Keys
        {
            get { return this._keys; }
            set { this._keys = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Внешние_сигналы")]
        public ExternalSignalStructNew ExternalSignals
        {
            get { return this._externalSignalsNew; }
            set { this._externalSignalsNew = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Неисправность")]
        public FaultStruct FaultSignal
        {
            get { return this._faultSignal; }
            set { this._faultSignal = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "Логические_сигналы")]
        public InputLogicSignalStruct LogicSignals
        {
            get { return this._inputLogicSignals; }
            set { this._inputLogicSignals = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Выключатель")]
        public SwitchStruct Switch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "АПВ")]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "АВР")]
        public AvrStruct Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "ЛЗШ")]
        public LzshStruct Lzsh
        {
            get { return this._lzsh; }
            set { this._lzsh = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Внешние_защиты")]
        public AllExternalDefensesStruct ExternalDefenses
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
        [BindingProperty(11)]
        [XmlElement(ElementName = "Все_группы")]
        public AllDefenceSetpoint AllGroup
        {
            get { return this._allCurrents; }
            set { this._allCurrents = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "ВЛС")]
        public OutputLogicSignalStruct OutputLogicSignals
        {
            get { return this._outputLogicSignals; }
            set { this._outputLogicSignals = value; }
        }
        [BindingProperty(13)]
        [XmlElement(ElementName = "Реле")]
        public AllRelaysStruct AllRelays
        {
            get { return this._allRelays; }
            set { this._allRelays = value; }
        }
        [BindingProperty(14)]
        [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStruct AllIndicators
        {
            get { return this._allIndicators; }
            set { this._allIndicators = value; }
        }
        [BindingProperty(15)]
        [XmlElement(ElementName = "Конфигурация_осциллографа")]
        public SystemAndOscConfigStruct OscConfig
        {
            get { return this._oscConfig; }
            set { this._oscConfig = value; }
        }
    }
}
