﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.MR5.v50.Configuration.Structures.AddCurrentDefenses;
using BEMN.MR5.v50.Configuration.Structures.CurrentDefenses;

namespace BEMN.MR5.v50.Configuration.Structures
{
    public class AllDefenceSetpoint: StructBase, ISetpointContainer<GroupSetpoint>
    {
        //1-2
        [Layout(0, Count = 4)] private ushort[] _corners1;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses1;
        [Layout(2)] private AllCurrentDefensesOtherStruct _currentDefensesOther1;
        [Layout(3, Count = 4)] private ushort[] _corners2;
        [Layout(4)] private AllCurrentDefensesStruct _currentDefenses2;
        [Layout(5)] private AllCurrentDefensesOtherStruct _currentDefensesOther2;
        [Layout(6)] private AllAddCurrentDefensesStruct _addCurrentDefenses1;
        [Layout(7, Count = 16)] private ushort[] _res1;
        [Layout(8)] private AllAddCurrentDefensesStruct _addCurrentDefenses2;
        [Layout(9, Count = 16)] private ushort[] _res2;
        [Layout(10, Count = 32)] private ushort[] _frequencyDefenses1;
        [Layout(11, Count = 32)] private ushort[] _frequencyDefenses2;
        [Layout(12, Count = 64)] private ushort[] _voltageDefenses1;
        [Layout(13, Count = 64)] private ushort[] _voltageDefenses2;

        //3-4
        [Layout(14, Count = 4)] private ushort[] _corners3;
        [Layout(15)] private AllCurrentDefensesStruct _currentDefenses3;
        [Layout(16)] private AllCurrentDefensesOtherStruct _currentDefensesOther3;
        [Layout(17, Count = 4)] private ushort[] _corners4;
        [Layout(18)] private AllCurrentDefensesStruct _currentDefenses4;
        [Layout(19)] private AllCurrentDefensesOtherStruct _currentDefensesOther4;
        [Layout(20)] private AllAddCurrentDefensesStruct _addCurrentDefenses3;
        [Layout(21, Count = 16)] private ushort[] _res3;
        [Layout(22)] private AllAddCurrentDefensesStruct _addCurrentDefenses4;
        [Layout(23, Count = 16)] private ushort[] _res4;
        [Layout(24, Count = 32)] private ushort[] _frequencyDefenses3;
        [Layout(25, Count = 32)] private ushort[] _frequencyDefenses4;
        [Layout(26, Count = 64)] private ushort[] _voltageDefenses3;
        [Layout(27, Count = 64)] private ushort[] _voltageDefenses4;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint[] Setpoints
        {
            get
            {
                return new[]
                {
                    this.GetGroupsetpoint(this._currentDefenses1, this._currentDefensesOther1, this._addCurrentDefenses1),
                    this.GetGroupsetpoint(this._currentDefenses2, this._currentDefensesOther2, this._addCurrentDefenses2),
                    this.GetGroupsetpoint(this._currentDefenses3, this._currentDefensesOther3, this._addCurrentDefenses3),
                    this.GetGroupsetpoint(this._currentDefenses4, this._currentDefensesOther4, this._addCurrentDefenses4)
                };
            }
            set
            {
                this._currentDefenses1 = value[0].CurrentDefenses;
                this._currentDefensesOther1 = value[0].CurrentDefensesOther;
                this._addCurrentDefenses1 = value[0].AddCurrentDefenses;

                this._currentDefenses2 = value[1].CurrentDefenses;
                this._currentDefensesOther2 = value[1].CurrentDefensesOther;
                this._addCurrentDefenses2 = value[1].AddCurrentDefenses;

                this._currentDefenses3 = value[2].CurrentDefenses;
                this._currentDefensesOther3 = value[2].CurrentDefensesOther;
                this._addCurrentDefenses3 = value[2].AddCurrentDefenses;

                this._currentDefenses4 = value[3].CurrentDefenses;
                this._currentDefensesOther4 = value[3].CurrentDefensesOther;
                this._addCurrentDefenses4 = value[3].AddCurrentDefenses;
            }
        }

        private GroupSetpoint GetGroupsetpoint(AllCurrentDefensesStruct currentDefenses,
            AllCurrentDefensesOtherStruct currentDefensesOther,
            AllAddCurrentDefensesStruct addCurrentDefense)
        {
            return new GroupSetpoint
            {
                CurrentDefenses = currentDefenses.Clone<AllCurrentDefensesStruct>(),
                CurrentDefensesOther = currentDefensesOther.Clone<AllCurrentDefensesOtherStruct>(),
                AddCurrentDefenses = addCurrentDefense.Clone<AllAddCurrentDefensesStruct>()
            };
        }
    }
}
