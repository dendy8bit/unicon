﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v50.Configuration.Structures.AddCurrentDefenses;
using BEMN.MR5.v50.Configuration.Structures.CurrentDefenses;

namespace BEMN.MR5.v50.Configuration.Structures
{
    public class GroupSetpoint : StructBase
    {
        [Layout(0)] private AllCurrentDefensesStruct _currentDefenses;
        [Layout(1)] private AllCurrentDefensesOtherStruct _currentDefensesOther;
        [Layout(2)] private AllAddCurrentDefensesStruct _addCurrentDefenses;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Токовые")]
        public AllCurrentDefensesStruct CurrentDefenses
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }
        
        [BindingProperty(1)]
        [XmlElement(ElementName = "Токовые_другие")]
        public AllCurrentDefensesOtherStruct CurrentDefensesOther
        {
            get { return this._currentDefensesOther; }
            set { this._currentDefensesOther = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Токовые_дополнительные")]
        public AllAddCurrentDefensesStruct AddCurrentDefenses
        {
            get { return this._addCurrentDefenses; }
            set { this._addCurrentDefenses = value; }
        }
    }
}
