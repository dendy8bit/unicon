﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v50.Configuration.Structures.Achr
{
    public class AchrAndChapvStruct:StructBase
    {
        [Layout(0)] private ushort _inpAchr;
        [Layout(1)] private ushort _blockAchr;
        [Layout(2)] private ushort _tAchr;
        [Layout(3)] private ushort _oscAchr;
        [Layout(4)] private ushort _inpChapv;
        [Layout(5)] private ushort _blockChapv;
        [Layout(6)] private ushort _tChapv;
        

        [BindingProperty(0)]
        [XmlElement(ElementName = "Вход_АЧР")]
        public string InpAchr
        {
            get { return Validator.Get(this._inpAchr, ConfigParamsV50.SwitchSignals); }
            set { this._inpAchr = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка_АЧР")]
        public string BlockAchr
        {
            get { return Validator.Get(this._blockAchr, ConfigParamsV50.SwitchSignals); }
            set { this._blockAchr = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Выдержка_времени_АЧР")]
        public int TimeAchr
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tAchr); }
            set { this._tAchr = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Осциллограф_АЧР")]
        public string Osc
        {
            get { return Validator.Get(this._oscAchr, ConfigParamsV50.OscAchr); }
            set { this._oscAchr = Validator.Set(value, ConfigParamsV50.OscAchr); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Вход_ЧАПВ")]
        public string InpChapv
        {
            get { return Validator.Get(this._inpChapv, ConfigParamsV50.SwitchSignals); }
            set { this._inpChapv = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Блокировка_ЧАПВ")]
        public string BlockChapv
        {
            get { return Validator.Get(this._blockChapv, ConfigParamsV50.SwitchSignals); }
            set { this._blockChapv = Validator.Set(value, ConfigParamsV50.SwitchSignals); }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Выдержка_времени_ЧАПВ")]
        public int TimeChapv
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tChapv); }
            set { this._tChapv = ValuesConverterCommon.SetWaitTime(value); }
        }
    }
}
