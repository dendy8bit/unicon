﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v50.Configuration.Structures.AddCurrentDefenses
{
    public class AllAddCurrentDefensesStruct : StructBase, IDgvRowsContainer<AddCurrentDefenseStruct>
    {
        [Layout(0, Count = 2)]
        private AddCurrentDefenseStruct[] _addCurrentDefenses;

        public AddCurrentDefenseStruct[] Rows
        {
            get { return this._addCurrentDefenses; }
            set { this._addCurrentDefenses = value; }
        }
    }
}
