﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v50.Configuration.Structures.ExternalDefenses
{
    public class AllExternalDefensesStruct : StructBase, IDgvRowsContainer<DefenseExternalStruct>
    {
        [Layout(1, Count = 8)]
        private DefenseExternalStruct[] _defenseExternal;
        [XmlArray(ElementName = "Все")]
        public DefenseExternalStruct[] Rows
        {
            get { return this._defenseExternal; }
            set { this._defenseExternal = value; }
        }
    }
}
