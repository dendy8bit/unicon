namespace BEMN.MR5.v50.Configuration
{
    partial class MR5V50ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._groupChangeButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._tokDefensesPage = new System.Windows.Forms.TabPage();
            this.NewCopyGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.copyBtn = new System.Windows.Forms.Button();
            this.copyGroupsCombo = new System.Windows.Forms.ComboBox();
            this.currentGroupCombo = new System.Windows.Forms.ComboBox();
            this.OldGroupBox = new System.Windows.Forms.GroupBox();
            this._mainRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._reserveRadioButtonGroup = new System.Windows.Forms.RadioButton();
            this._igDefenseGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._otherCurrentDefensesGrid = new System.Windows.Forms.DataGridView();
            this._tokDefense3NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3BlockingNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense3OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense3UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense3AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._currentDefensesGrid = new System.Windows.Forms.DataGridView();
            this._tokDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseBlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseParameterCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseFeatureCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseWorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseSpeedUpCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseSpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefenseOSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefenseUROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAPVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefenseAVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._inDefenseGrid = new System.Windows.Forms.DataGridView();
            this._tokDefense4NameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4ModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4BlockNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4WorkTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4SpeedupCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4SpeedupTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tokDefense4OSCv11Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._tokDefense4UROVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4APVCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._tokDefense4AVRCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i1i2DefenseGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this._chapvTime = new System.Windows.Forms.MaskedTextBox();
            this._chapvBlock = new System.Windows.Forms.ComboBox();
            this._chapvEnter = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._achrOsc = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._achrTime = new System.Windows.Forms.MaskedTextBox();
            this._achrBlock = new System.Windows.Forms.ComboBox();
            this._achrEnter = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.avr_disconnection = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.avr_permit_reset_switch = new System.Windows.Forms.CheckBox();
            this.avr_time_return = new System.Windows.Forms.MaskedTextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.avr_time_abrasion = new System.Windows.Forms.MaskedTextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.avr_return = new System.Windows.Forms.ComboBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.avr_abrasion = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.avr_reset_blocking = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.avr_abrasion_switch = new System.Windows.Forms.CheckBox();
            this.avr_supply_off = new System.Windows.Forms.CheckBox();
            this.avr_self_off = new System.Windows.Forms.CheckBox();
            this.avr_switch_off = new System.Windows.Forms.CheckBox();
            this.avr_blocking = new System.Windows.Forms.ComboBox();
            this.avr_start = new System.Windows.Forms.ComboBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lzshConf = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.lzsh_constraint = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._apvSwithOff = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.apv_time_4krat = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.apv_time_3krat = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.apv_time_2krat = new System.Windows.Forms.MaskedTextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.apv_time_1krat = new System.Windows.Forms.MaskedTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.apv_time_ready = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.apv_time_blocking = new System.Windows.Forms.MaskedTextBox();
            this.apv_blocking = new System.Windows.Forms.ComboBox();
            this.apv_conf = new System.Windows.Forms.ComboBox();
            this._externalDefensePage = new System.Windows.Forms.TabPage();
            this._externalDefenseGrid = new System.Windows.Forms.DataGridView();
            this._extDefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefBlockingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefWorkingTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefReturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVreturnCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefReturnNumberCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._exDefReturnTimeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._exDefUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefOSCcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._exDefResetcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._releDispepairBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._dispepairCheckList = new System.Windows.Forms.CheckedListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndResetCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndAlarmCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._outIndSystemCol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releImpulseCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._inSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.gb1 = new System.Windows.Forms.GroupBox();
            this.oscLenCombo = new System.Windows.Forms.ComboBox();
            this.oscLenBox = new System.Windows.Forms.MaskedTextBox();
            this.gb3 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.oscPercent = new System.Windows.Forms.MaskedTextBox();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.oscFix = new System.Windows.Forms.ComboBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.keysListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._manageSignalsSDTU_Combo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this._manageSignalsExternalCombo = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this._manageSignalsKeyCombo = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this._manageSignalsButtonCombo = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this._switcherDurationBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTokBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherImpulseBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._switcherErrorCombo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this._switcherStateOnCombo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this._switcherStateOffCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._gr4ComboBox = new System.Windows.Forms.ComboBox();
            this._gr3ComboBox = new System.Windows.Forms.ComboBox();
            this._gr2ComboBox = new System.Windows.Forms.ComboBox();
            this._gr1ComboBox = new System.Windows.Forms.ComboBox();
            this._pereklLabel4 = new System.Windows.Forms.Label();
            this._pereklLabel3 = new System.Windows.Forms.Label();
            this._pereklLabel2 = new System.Windows.Forms.Label();
            this._pereklLabel1 = new System.Windows.Forms.Label();
            this._blockSdtu = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this._constraintGroupCombo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this._signalizationCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this._extOffCombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._extOnCombo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._keyOffCombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._keyOnCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._TT_typeCombo = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this._maxTok_Box = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetpointStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._logicSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals5 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._resetButton = new System.Windows.Forms.Button();
            this._statusStrip.SuspendLayout();
            this._tokDefensesPage.SuspendLayout();
            this.NewCopyGroupBox.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.OldGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._igDefenseGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._otherCurrentDefensesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._currentDefensesGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._inDefenseGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._i1i2DefenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this._externalDefensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).BeginInit();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._inSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.gb1.SuspendLayout();
            this.gb3.SuspendLayout();
            this.gb2.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._logicSignalsPage.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).BeginInit();
            this.groupBox27.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.SuspendLayout();
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveConfigBut.Location = new System.Drawing.Point(705, 560);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(120, 23);
            this._saveConfigBut.TabIndex = 10;
            this._saveConfigBut.Text = "��������� � ����";
            this._toolTip.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadConfigBut.Location = new System.Drawing.Point(579, 560);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(120, 23);
            this._loadConfigBut.TabIndex = 9;
            this._loadConfigBut.Text = "��������� �� �����";
            this._toolTip.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(143, 16);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(60, 20);
            this._TT_Box.TabIndex = 0;
            this._TT_Box.Tag = "5000";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._toolTip.SetToolTip(this._TT_Box, "��������� ��� ��������������");
            // 
            // _groupChangeButton
            // 
            this._groupChangeButton.Location = new System.Drawing.Point(184, 10);
            this._groupChangeButton.Name = "_groupChangeButton";
            this._groupChangeButton.Size = new System.Drawing.Size(226, 23);
            this._groupChangeButton.TabIndex = 9;
            this._groupChangeButton.Text = "�������� -> ���������";
            this._toolTip.SetToolTip(this._groupChangeButton, "���������� �������");
            this._groupChangeButton.UseVisualStyleBackColor = true;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.Location = new System.Drawing.Point(154, 560);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(148, 23);
            this._writeConfigBut.TabIndex = 8;
            this._writeConfigBut.Text = "�������� � ���������";
            this._toolTip.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.Location = new System.Drawing.Point(4, 560);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(144, 23);
            this._readConfigBut.TabIndex = 7;
            this._readConfigBut.Text = "��������� �� ���������";
            this._toolTip.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(831, 560);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(120, 23);
            this._saveToXmlButton.TabIndex = 34;
            this._saveToXmlButton.Text = "��������� � HTML";
            this._toolTip.SetToolTip(this._saveToXmlButton, "��������� ������������ � HTML ����");
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetConfigBut
            // 
            this._resetConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetConfigBut.Location = new System.Drawing.Point(308, 560);
            this._resetConfigBut.Name = "_resetConfigBut";
            this._resetConfigBut.Size = new System.Drawing.Size(148, 23);
            this._resetConfigBut.TabIndex = 35;
            this._resetConfigBut.Text = "��������� ���. �������";
            this._toolTip.SetToolTip(this._resetConfigBut, "��������� ������� �������");
            this._resetConfigBut.UseVisualStyleBackColor = true;
            this._resetConfigBut.Click += new System.EventHandler(this._resetConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��5 v50";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 1;
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��5 v50";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._exchangeProgressBar,
            this._processLabel});
            this._statusStrip.Location = new System.Drawing.Point(0, 593);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(955, 22);
            this._statusStrip.TabIndex = 11;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _tokDefensesPage
            // 
            this._tokDefensesPage.Controls.Add(this.NewCopyGroupBox);
            this._tokDefensesPage.Controls.Add(this.OldGroupBox);
            this._tokDefensesPage.Controls.Add(this._igDefenseGrid);
            this._tokDefensesPage.Controls.Add(this._otherCurrentDefensesGrid);
            this._tokDefensesPage.Controls.Add(this._currentDefensesGrid);
            this._tokDefensesPage.Controls.Add(this._inDefenseGrid);
            this._tokDefensesPage.Controls.Add(this._i1i2DefenseGrid);
            this._tokDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._tokDefensesPage.Name = "_tokDefensesPage";
            this._tokDefensesPage.Size = new System.Drawing.Size(947, 528);
            this._tokDefensesPage.TabIndex = 4;
            this._tokDefensesPage.Text = "������� ������";
            this._tokDefensesPage.UseVisualStyleBackColor = true;
            // 
            // NewCopyGroupBox
            // 
            this.NewCopyGroupBox.Controls.Add(this.groupBox10);
            this.NewCopyGroupBox.Controls.Add(this.currentGroupCombo);
            this.NewCopyGroupBox.Location = new System.Drawing.Point(3, 3);
            this.NewCopyGroupBox.Name = "NewCopyGroupBox";
            this.NewCopyGroupBox.Size = new System.Drawing.Size(393, 51);
            this.NewCopyGroupBox.TabIndex = 9;
            this.NewCopyGroupBox.TabStop = false;
            this.NewCopyGroupBox.Text = "������ �������";
            this.NewCopyGroupBox.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.copyBtn);
            this.groupBox10.Controls.Add(this.copyGroupsCombo);
            this.groupBox10.Location = new System.Drawing.Point(132, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(255, 42);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            // 
            // copyBtn
            // 
            this.copyBtn.Location = new System.Drawing.Point(6, 13);
            this.copyBtn.Name = "copyBtn";
            this.copyBtn.Size = new System.Drawing.Size(115, 23);
            this.copyBtn.TabIndex = 3;
            this.copyBtn.Text = "���������� �";
            this.copyBtn.UseVisualStyleBackColor = true;
            this.copyBtn.Click += new System.EventHandler(this.copyBtn_Click);
            // 
            // copyGroupsCombo
            // 
            this.copyGroupsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.copyGroupsCombo.FormattingEnabled = true;
            this.copyGroupsCombo.Location = new System.Drawing.Point(129, 14);
            this.copyGroupsCombo.Name = "copyGroupsCombo";
            this.copyGroupsCombo.Size = new System.Drawing.Size(120, 21);
            this.copyGroupsCombo.TabIndex = 2;
            // 
            // currentGroupCombo
            // 
            this.currentGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentGroupCombo.FormattingEnabled = true;
            this.currentGroupCombo.Location = new System.Drawing.Point(6, 19);
            this.currentGroupCombo.Name = "currentGroupCombo";
            this.currentGroupCombo.Size = new System.Drawing.Size(120, 21);
            this.currentGroupCombo.TabIndex = 0;
            // 
            // OldGroupBox
            // 
            this.OldGroupBox.Controls.Add(this._groupChangeButton);
            this.OldGroupBox.Controls.Add(this._mainRadioButtonGroup);
            this.OldGroupBox.Controls.Add(this._reserveRadioButtonGroup);
            this.OldGroupBox.Location = new System.Drawing.Point(13, 6);
            this.OldGroupBox.Name = "OldGroupBox";
            this.OldGroupBox.Size = new System.Drawing.Size(420, 35);
            this.OldGroupBox.TabIndex = 8;
            this.OldGroupBox.TabStop = false;
            this.OldGroupBox.Text = "������ �������";
            // 
            // _mainRadioButtonGroup
            // 
            this._mainRadioButtonGroup.AutoSize = true;
            this._mainRadioButtonGroup.Checked = true;
            this._mainRadioButtonGroup.Location = new System.Drawing.Point(17, 13);
            this._mainRadioButtonGroup.Name = "_mainRadioButtonGroup";
            this._mainRadioButtonGroup.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButtonGroup.TabIndex = 6;
            this._mainRadioButtonGroup.TabStop = true;
            this._mainRadioButtonGroup.Text = "��������";
            this._mainRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioButtonGroup
            // 
            this._reserveRadioButtonGroup.AutoSize = true;
            this._reserveRadioButtonGroup.Location = new System.Drawing.Point(98, 14);
            this._reserveRadioButtonGroup.Name = "_reserveRadioButtonGroup";
            this._reserveRadioButtonGroup.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButtonGroup.TabIndex = 7;
            this._reserveRadioButtonGroup.Text = "���������";
            this._reserveRadioButtonGroup.UseVisualStyleBackColor = true;
            // 
            // _igDefenseGrid
            // 
            this._igDefenseGrid.AllowUserToAddRows = false;
            this._igDefenseGrid.AllowUserToDeleteRows = false;
            this._igDefenseGrid.AllowUserToResizeColumns = false;
            this._igDefenseGrid.AllowUserToResizeRows = false;
            this._igDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._igDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._igDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._igDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._igDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewTextBoxColumn5,
            this.Column3,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._igDefenseGrid.DefaultCellStyle = dataGridViewCellStyle5;
            this._igDefenseGrid.Location = new System.Drawing.Point(3, 425);
            this._igDefenseGrid.MultiSelect = false;
            this._igDefenseGrid.Name = "_igDefenseGrid";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._igDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this._igDefenseGrid.RowHeadersVisible = false;
            this._igDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._igDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._igDefenseGrid.RowTemplate.Height = 24;
            this._igDefenseGrid.Size = new System.Drawing.Size(807, 47);
            this._igDefenseGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "�����";
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Width = 48;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "����������";
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Width = 74;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn5.HeaderText = "I��, I�";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle4.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn6.HeaderText = "t��, ��";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 60;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "���.";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 36;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "t���, ��";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 60;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Width = 82;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "����";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 43;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "���";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 35;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "���";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 34;
            // 
            // _otherCurrentDefensesGrid
            // 
            this._otherCurrentDefensesGrid.AllowUserToAddRows = false;
            this._otherCurrentDefensesGrid.AllowUserToDeleteRows = false;
            this._otherCurrentDefensesGrid.AllowUserToResizeColumns = false;
            this._otherCurrentDefensesGrid.AllowUserToResizeRows = false;
            this._otherCurrentDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._otherCurrentDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._otherCurrentDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this._otherCurrentDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._otherCurrentDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense3NameCol,
            this._tokDefense3ModeCol,
            this._tokDefense3BlockingNumberCol,
            this._tokDefense3WorkConstraintCol,
            this.Column1,
            this._tokDefense3WorkTimeCol,
            this._tokDefense3SpeedupCol,
            this._tokDefense3SpeedupTimeCol,
            this._tokDefense3OSCv11Col,
            this._tokDefense3UROVCol,
            this._tokDefense3APVCol,
            this._tokDefense3AVRCol});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._otherCurrentDefensesGrid.DefaultCellStyle = dataGridViewCellStyle13;
            this._otherCurrentDefensesGrid.Location = new System.Drawing.Point(3, 215);
            this._otherCurrentDefensesGrid.MultiSelect = false;
            this._otherCurrentDefensesGrid.Name = "_otherCurrentDefensesGrid";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._otherCurrentDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this._otherCurrentDefensesGrid.RowHeadersVisible = false;
            this._otherCurrentDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._otherCurrentDefensesGrid.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this._otherCurrentDefensesGrid.RowTemplate.Height = 24;
            this._otherCurrentDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._otherCurrentDefensesGrid.Size = new System.Drawing.Size(807, 127);
            this._otherCurrentDefensesGrid.TabIndex = 2;
            // 
            // _tokDefense3NameCol
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._tokDefense3NameCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._tokDefense3NameCol.Frozen = true;
            this._tokDefense3NameCol.HeaderText = "";
            this._tokDefense3NameCol.Name = "_tokDefense3NameCol";
            this._tokDefense3NameCol.ReadOnly = true;
            this._tokDefense3NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3NameCol.Width = 35;
            // 
            // _tokDefense3ModeCol
            // 
            this._tokDefense3ModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense3ModeCol.HeaderText = "�����";
            this._tokDefense3ModeCol.Name = "_tokDefense3ModeCol";
            this._tokDefense3ModeCol.Width = 48;
            // 
            // _tokDefense3BlockingNumberCol
            // 
            this._tokDefense3BlockingNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3BlockingNumberCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense3BlockingNumberCol.HeaderText = "����������";
            this._tokDefense3BlockingNumberCol.Name = "_tokDefense3BlockingNumberCol";
            this._tokDefense3BlockingNumberCol.Width = 74;
            // 
            // _tokDefense3WorkConstraintCol
            // 
            dataGridViewCellStyle10.NullValue = null;
            this._tokDefense3WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._tokDefense3WorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefense3WorkConstraintCol.Name = "_tokDefense3WorkConstraintCol";
            this._tokDefense3WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkConstraintCol.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 73;
            // 
            // _tokDefense3WorkTimeCol
            // 
            dataGridViewCellStyle11.NullValue = null;
            this._tokDefense3WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle11;
            this._tokDefense3WorkTimeCol.HeaderText = "t��, ��";
            this._tokDefense3WorkTimeCol.Name = "_tokDefense3WorkTimeCol";
            this._tokDefense3WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3WorkTimeCol.Width = 60;
            // 
            // _tokDefense3SpeedupCol
            // 
            this._tokDefense3SpeedupCol.HeaderText = "���.";
            this._tokDefense3SpeedupCol.Name = "_tokDefense3SpeedupCol";
            this._tokDefense3SpeedupCol.Width = 36;
            // 
            // _tokDefense3SpeedupTimeCol
            // 
            dataGridViewCellStyle12.NullValue = null;
            this._tokDefense3SpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._tokDefense3SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense3SpeedupTimeCol.Name = "_tokDefense3SpeedupTimeCol";
            this._tokDefense3SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense3SpeedupTimeCol.Width = 60;
            // 
            // _tokDefense3OSCv11Col
            // 
            this._tokDefense3OSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense3OSCv11Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense3OSCv11Col.HeaderText = "�����������";
            this._tokDefense3OSCv11Col.Name = "_tokDefense3OSCv11Col";
            this._tokDefense3OSCv11Col.Width = 82;
            // 
            // _tokDefense3UROVCol
            // 
            this._tokDefense3UROVCol.HeaderText = "����";
            this._tokDefense3UROVCol.Name = "_tokDefense3UROVCol";
            this._tokDefense3UROVCol.Width = 43;
            // 
            // _tokDefense3APVCol
            // 
            this._tokDefense3APVCol.HeaderText = "���";
            this._tokDefense3APVCol.Name = "_tokDefense3APVCol";
            this._tokDefense3APVCol.Width = 35;
            // 
            // _tokDefense3AVRCol
            // 
            this._tokDefense3AVRCol.HeaderText = "���";
            this._tokDefense3AVRCol.Name = "_tokDefense3AVRCol";
            this._tokDefense3AVRCol.Width = 34;
            // 
            // _currentDefensesGrid
            // 
            this._currentDefensesGrid.AllowUserToAddRows = false;
            this._currentDefensesGrid.AllowUserToDeleteRows = false;
            this._currentDefensesGrid.AllowUserToResizeColumns = false;
            this._currentDefensesGrid.AllowUserToResizeRows = false;
            this._currentDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._currentDefensesGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._currentDefensesGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this._currentDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._currentDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefenseNameCol,
            this._tokDefenseModeCol,
            this._tokDefenseBlockNumberCol,
            this._tokDefenseParameterCol,
            this._tokDefenseWorkConstraintCol,
            this._tokDefenseFeatureCol,
            this._tokDefenseWorkTimeCol,
            this._tokDefenseSpeedUpCol,
            this._tokDefenseSpeedupTimeCol,
            this._tokDefenseOSCv11Col,
            this._tokDefenseUROVCol,
            this._tokDefenseAPVCol,
            this._tokDefenseAVRCol});
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._currentDefensesGrid.DefaultCellStyle = dataGridViewCellStyle21;
            this._currentDefensesGrid.Location = new System.Drawing.Point(3, 60);
            this._currentDefensesGrid.MultiSelect = false;
            this._currentDefensesGrid.Name = "_currentDefensesGrid";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._currentDefensesGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this._currentDefensesGrid.RowHeadersVisible = false;
            this._currentDefensesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._currentDefensesGrid.RowsDefaultCellStyle = dataGridViewCellStyle23;
            this._currentDefensesGrid.RowTemplate.Height = 24;
            this._currentDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._currentDefensesGrid.Size = new System.Drawing.Size(807, 149);
            this._currentDefensesGrid.TabIndex = 0;
            // 
            // _tokDefenseNameCol
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._tokDefenseNameCol.DefaultCellStyle = dataGridViewCellStyle17;
            this._tokDefenseNameCol.Frozen = true;
            this._tokDefenseNameCol.HeaderText = "";
            this._tokDefenseNameCol.Name = "_tokDefenseNameCol";
            this._tokDefenseNameCol.ReadOnly = true;
            this._tokDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseNameCol.Width = 40;
            // 
            // _tokDefenseModeCol
            // 
            this._tokDefenseModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseModeCol.HeaderText = "�����";
            this._tokDefenseModeCol.Name = "_tokDefenseModeCol";
            this._tokDefenseModeCol.Width = 48;
            // 
            // _tokDefenseBlockNumberCol
            // 
            this._tokDefenseBlockNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseBlockNumberCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseBlockNumberCol.HeaderText = "����������";
            this._tokDefenseBlockNumberCol.Name = "_tokDefenseBlockNumberCol";
            this._tokDefenseBlockNumberCol.Width = 74;
            // 
            // _tokDefenseParameterCol
            // 
            this._tokDefenseParameterCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseParameterCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseParameterCol.HeaderText = "��������";
            this._tokDefenseParameterCol.Name = "_tokDefenseParameterCol";
            this._tokDefenseParameterCol.Width = 64;
            // 
            // _tokDefenseWorkConstraintCol
            // 
            dataGridViewCellStyle18.NullValue = null;
            this._tokDefenseWorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle18;
            this._tokDefenseWorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefenseWorkConstraintCol.Name = "_tokDefenseWorkConstraintCol";
            this._tokDefenseWorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkConstraintCol.Width = 45;
            // 
            // _tokDefenseFeatureCol
            // 
            this._tokDefenseFeatureCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseFeatureCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseFeatureCol.HeaderText = "���-��";
            this._tokDefenseFeatureCol.Name = "_tokDefenseFeatureCol";
            this._tokDefenseFeatureCol.Width = 47;
            // 
            // _tokDefenseWorkTimeCol
            // 
            dataGridViewCellStyle19.NullValue = null;
            this._tokDefenseWorkTimeCol.DefaultCellStyle = dataGridViewCellStyle19;
            this._tokDefenseWorkTimeCol.HeaderText = "t��, ��/����.";
            this._tokDefenseWorkTimeCol.Name = "_tokDefenseWorkTimeCol";
            this._tokDefenseWorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseWorkTimeCol.Width = 60;
            // 
            // _tokDefenseSpeedUpCol
            // 
            this._tokDefenseSpeedUpCol.HeaderText = "���.";
            this._tokDefenseSpeedUpCol.Name = "_tokDefenseSpeedUpCol";
            this._tokDefenseSpeedUpCol.Width = 36;
            // 
            // _tokDefenseSpeedupTimeCol
            // 
            dataGridViewCellStyle20.NullValue = null;
            this._tokDefenseSpeedupTimeCol.DefaultCellStyle = dataGridViewCellStyle20;
            this._tokDefenseSpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefenseSpeedupTimeCol.Name = "_tokDefenseSpeedupTimeCol";
            this._tokDefenseSpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefenseSpeedupTimeCol.Width = 50;
            // 
            // _tokDefenseOSCv11Col
            // 
            this._tokDefenseOSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefenseOSCv11Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefenseOSCv11Col.HeaderText = "����������";
            this._tokDefenseOSCv11Col.Name = "_tokDefenseOSCv11Col";
            this._tokDefenseOSCv11Col.Width = 76;
            // 
            // _tokDefenseUROVCol
            // 
            this._tokDefenseUROVCol.HeaderText = "����";
            this._tokDefenseUROVCol.Name = "_tokDefenseUROVCol";
            this._tokDefenseUROVCol.Width = 43;
            // 
            // _tokDefenseAPVCol
            // 
            this._tokDefenseAPVCol.HeaderText = "���";
            this._tokDefenseAPVCol.Name = "_tokDefenseAPVCol";
            this._tokDefenseAPVCol.Width = 35;
            // 
            // _tokDefenseAVRCol
            // 
            this._tokDefenseAVRCol.HeaderText = "���";
            this._tokDefenseAVRCol.Name = "_tokDefenseAVRCol";
            this._tokDefenseAVRCol.Width = 34;
            // 
            // _inDefenseGrid
            // 
            this._inDefenseGrid.AllowUserToAddRows = false;
            this._inDefenseGrid.AllowUserToDeleteRows = false;
            this._inDefenseGrid.AllowUserToResizeColumns = false;
            this._inDefenseGrid.AllowUserToResizeRows = false;
            this._inDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._inDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._inDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this._inDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._tokDefense4NameCol,
            this._tokDefense4ModeCol,
            this._tokDefense4BlockNumberCol,
            this.Column2,
            this._tokDefense4WorkConstraintCol,
            this._tokDefense4WorkTimeCol,
            this._tokDefense4SpeedupCol,
            this._tokDefense4SpeedupTimeCol,
            this._tokDefense4OSCv11Col,
            this._tokDefense4UROVCol,
            this._tokDefense4APVCol,
            this._tokDefense4AVRCol});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._inDefenseGrid.DefaultCellStyle = dataGridViewCellStyle28;
            this._inDefenseGrid.Location = new System.Drawing.Point(3, 348);
            this._inDefenseGrid.MultiSelect = false;
            this._inDefenseGrid.Name = "_inDefenseGrid";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._inDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this._inDefenseGrid.RowHeadersVisible = false;
            this._inDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._inDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this._inDefenseGrid.RowTemplate.Height = 24;
            this._inDefenseGrid.Size = new System.Drawing.Size(807, 71);
            this._inDefenseGrid.TabIndex = 3;
            // 
            // _tokDefense4NameCol
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._tokDefense4NameCol.DefaultCellStyle = dataGridViewCellStyle25;
            this._tokDefense4NameCol.Frozen = true;
            this._tokDefense4NameCol.HeaderText = "";
            this._tokDefense4NameCol.Name = "_tokDefense4NameCol";
            this._tokDefense4NameCol.ReadOnly = true;
            this._tokDefense4NameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4NameCol.Width = 35;
            // 
            // _tokDefense4ModeCol
            // 
            this._tokDefense4ModeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4ModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense4ModeCol.HeaderText = "�����";
            this._tokDefense4ModeCol.Name = "_tokDefense4ModeCol";
            this._tokDefense4ModeCol.Width = 48;
            // 
            // _tokDefense4BlockNumberCol
            // 
            this._tokDefense4BlockNumberCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4BlockNumberCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense4BlockNumberCol.HeaderText = "����������";
            this._tokDefense4BlockNumberCol.Name = "_tokDefense4BlockNumberCol";
            this._tokDefense4BlockNumberCol.Width = 74;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            this.Column2.Width = 73;
            // 
            // _tokDefense4WorkConstraintCol
            // 
            dataGridViewCellStyle26.NullValue = null;
            this._tokDefense4WorkConstraintCol.DefaultCellStyle = dataGridViewCellStyle26;
            this._tokDefense4WorkConstraintCol.HeaderText = "I��, I�";
            this._tokDefense4WorkConstraintCol.Name = "_tokDefense4WorkConstraintCol";
            this._tokDefense4WorkConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkConstraintCol.Width = 50;
            // 
            // _tokDefense4WorkTimeCol
            // 
            dataGridViewCellStyle27.NullValue = null;
            this._tokDefense4WorkTimeCol.DefaultCellStyle = dataGridViewCellStyle27;
            this._tokDefense4WorkTimeCol.HeaderText = "t��, ��";
            this._tokDefense4WorkTimeCol.Name = "_tokDefense4WorkTimeCol";
            this._tokDefense4WorkTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4WorkTimeCol.Width = 60;
            // 
            // _tokDefense4SpeedupCol
            // 
            this._tokDefense4SpeedupCol.HeaderText = "���.";
            this._tokDefense4SpeedupCol.Name = "_tokDefense4SpeedupCol";
            this._tokDefense4SpeedupCol.Width = 36;
            // 
            // _tokDefense4SpeedupTimeCol
            // 
            this._tokDefense4SpeedupTimeCol.HeaderText = "t���, ��";
            this._tokDefense4SpeedupTimeCol.Name = "_tokDefense4SpeedupTimeCol";
            this._tokDefense4SpeedupTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tokDefense4SpeedupTimeCol.Width = 60;
            // 
            // _tokDefense4OSCv11Col
            // 
            this._tokDefense4OSCv11Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._tokDefense4OSCv11Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._tokDefense4OSCv11Col.HeaderText = "�����������";
            this._tokDefense4OSCv11Col.Name = "_tokDefense4OSCv11Col";
            this._tokDefense4OSCv11Col.Width = 82;
            // 
            // _tokDefense4UROVCol
            // 
            this._tokDefense4UROVCol.HeaderText = "����";
            this._tokDefense4UROVCol.Name = "_tokDefense4UROVCol";
            this._tokDefense4UROVCol.Width = 43;
            // 
            // _tokDefense4APVCol
            // 
            this._tokDefense4APVCol.HeaderText = "���";
            this._tokDefense4APVCol.Name = "_tokDefense4APVCol";
            this._tokDefense4APVCol.Width = 35;
            // 
            // _tokDefense4AVRCol
            // 
            this._tokDefense4AVRCol.HeaderText = "���";
            this._tokDefense4AVRCol.Name = "_tokDefense4AVRCol";
            this._tokDefense4AVRCol.Width = 34;
            // 
            // _i1i2DefenseGrid
            // 
            this._i1i2DefenseGrid.AllowUserToAddRows = false;
            this._i1i2DefenseGrid.AllowUserToDeleteRows = false;
            this._i1i2DefenseGrid.AllowUserToResizeColumns = false;
            this._i1i2DefenseGrid.AllowUserToResizeRows = false;
            this._i1i2DefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._i1i2DefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._i1i2DefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this._i1i2DefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._i1i2DefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewComboBoxColumn11,
            this.Column4,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewCheckBoxColumn5,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn12,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewCheckBoxColumn8});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._i1i2DefenseGrid.DefaultCellStyle = dataGridViewCellStyle35;
            this._i1i2DefenseGrid.Location = new System.Drawing.Point(3, 478);
            this._i1i2DefenseGrid.MultiSelect = false;
            this._i1i2DefenseGrid.Name = "_i1i2DefenseGrid";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._i1i2DefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this._i1i2DefenseGrid.RowHeadersVisible = false;
            this._i1i2DefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._i1i2DefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle37;
            this._i1i2DefenseGrid.RowTemplate.Height = 24;
            this._i1i2DefenseGrid.Size = new System.Drawing.Size(807, 47);
            this._i1i2DefenseGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn11.Frozen = true;
            this.dataGridViewTextBoxColumn11.HeaderText = "";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 40;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "�����";
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Width = 48;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "����������";
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Width = 74;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle33.NullValue = null;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn12.HeaderText = "���. ����.";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 65;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle34.NullValue = null;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn13.HeaderText = "t����, ��";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 60;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "���.";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Visible = false;
            this.dataGridViewCheckBoxColumn5.Width = 36;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "t���, ��";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Visible = false;
            this.dataGridViewTextBoxColumn14.Width = 60;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.HeaderText = "�����������";
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Width = 82;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "����";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 43;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "���";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Width = 35;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "���";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Width = 34;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox17);
            this._automaticPage.Controls.Add(this.groupBox2);
            this._automaticPage.Controls.Add(this.groupBox11);
            this._automaticPage.Controls.Add(this.groupBox13);
            this._automaticPage.Controls.Add(this.groupBox14);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(947, 528);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label4);
            this.groupBox17.Controls.Add(this.label26);
            this.groupBox17.Controls.Add(this.label27);
            this.groupBox17.Controls.Add(this._chapvTime);
            this.groupBox17.Controls.Add(this._chapvBlock);
            this.groupBox17.Controls.Add(this._chapvEnter);
            this.groupBox17.Location = new System.Drawing.Point(270, 327);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(256, 114);
            this.groupBox17.TabIndex = 26;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "������������ ����";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "����";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(8, 66);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 13);
            this.label26.TabIndex = 28;
            this.label26.Text = "��������, ��";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(8, 46);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "����������";
            // 
            // _chapvTime
            // 
            this._chapvTime.Location = new System.Drawing.Point(146, 61);
            this._chapvTime.Name = "_chapvTime";
            this._chapvTime.Size = new System.Drawing.Size(87, 20);
            this._chapvTime.TabIndex = 18;
            this._chapvTime.Tag = "3000000";
            this._chapvTime.Text = "0";
            this._chapvTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _chapvBlock
            // 
            this._chapvBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._chapvBlock.FormattingEnabled = true;
            this._chapvBlock.Location = new System.Drawing.Point(146, 40);
            this._chapvBlock.Name = "_chapvBlock";
            this._chapvBlock.Size = new System.Drawing.Size(87, 21);
            this._chapvBlock.TabIndex = 3;
            // 
            // _chapvEnter
            // 
            this._chapvEnter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._chapvEnter.FormattingEnabled = true;
            this._chapvEnter.Location = new System.Drawing.Point(146, 19);
            this._chapvEnter.Name = "_chapvEnter";
            this._chapvEnter.Size = new System.Drawing.Size(87, 21);
            this._chapvEnter.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this._achrOsc);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this._achrTime);
            this.groupBox2.Controls.Add(this._achrBlock);
            this.groupBox2.Controls.Add(this._achrEnter);
            this.groupBox2.Location = new System.Drawing.Point(8, 327);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 114);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "������������ ���";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(8, 84);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 13);
            this.label29.TabIndex = 30;
            this.label29.Text = "�����������";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "����";
            // 
            // _achrOsc
            // 
            this._achrOsc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._achrOsc.FormattingEnabled = true;
            this._achrOsc.Location = new System.Drawing.Point(108, 81);
            this._achrOsc.Name = "_achrOsc";
            this._achrOsc.Size = new System.Drawing.Size(125, 21);
            this._achrOsc.TabIndex = 29;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(8, 66);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(80, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "��������, ��";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 46);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 27;
            this.label33.Text = "����������";
            // 
            // _achrTime
            // 
            this._achrTime.Location = new System.Drawing.Point(146, 61);
            this._achrTime.Name = "_achrTime";
            this._achrTime.Size = new System.Drawing.Size(87, 20);
            this._achrTime.TabIndex = 18;
            this._achrTime.Tag = "3000000";
            this._achrTime.Text = "0";
            this._achrTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _achrBlock
            // 
            this._achrBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._achrBlock.FormattingEnabled = true;
            this._achrBlock.Location = new System.Drawing.Point(146, 40);
            this._achrBlock.Name = "_achrBlock";
            this._achrBlock.Size = new System.Drawing.Size(87, 21);
            this._achrBlock.TabIndex = 3;
            // 
            // _achrEnter
            // 
            this._achrEnter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._achrEnter.FormattingEnabled = true;
            this._achrEnter.Location = new System.Drawing.Point(146, 19);
            this._achrEnter.Name = "_achrEnter";
            this._achrEnter.Size = new System.Drawing.Size(87, 21);
            this._achrEnter.TabIndex = 1;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label58);
            this.groupBox11.Controls.Add(this.label59);
            this.groupBox11.Controls.Add(this.avr_disconnection);
            this.groupBox11.Controls.Add(this.label61);
            this.groupBox11.Controls.Add(this.avr_permit_reset_switch);
            this.groupBox11.Controls.Add(this.avr_time_return);
            this.groupBox11.Controls.Add(this.label62);
            this.groupBox11.Controls.Add(this.avr_time_abrasion);
            this.groupBox11.Controls.Add(this.label63);
            this.groupBox11.Controls.Add(this.avr_return);
            this.groupBox11.Controls.Add(this.label64);
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.avr_abrasion);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.avr_reset_blocking);
            this.groupBox11.Controls.Add(this.groupBox12);
            this.groupBox11.Controls.Add(this.avr_blocking);
            this.groupBox11.Controls.Add(this.avr_start);
            this.groupBox11.Location = new System.Drawing.Point(270, 4);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(256, 317);
            this.groupBox11.TabIndex = 24;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "������������ ���";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(9, 295);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(123, 13);
            this.label58.TabIndex = 42;
            this.label58.Text = "����� ����������, ��";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(9, 150);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(131, 13);
            this.label59.TabIndex = 35;
            this.label59.Text = "������ ��� (�� �������)";
            // 
            // avr_disconnection
            // 
            this.avr_disconnection.Location = new System.Drawing.Point(152, 292);
            this.avr_disconnection.Name = "avr_disconnection";
            this.avr_disconnection.Size = new System.Drawing.Size(87, 20);
            this.avr_disconnection.TabIndex = 31;
            this.avr_disconnection.Tag = "3000000";
            this.avr_disconnection.Text = "0";
            this.avr_disconnection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(9, 274);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(110, 13);
            this.label61.TabIndex = 41;
            this.label61.Text = "����� ��������, ��";
            // 
            // avr_permit_reset_switch
            // 
            this.avr_permit_reset_switch.Location = new System.Drawing.Point(12, 115);
            this.avr_permit_reset_switch.Name = "avr_permit_reset_switch";
            this.avr_permit_reset_switch.Size = new System.Drawing.Size(227, 32);
            this.avr_permit_reset_switch.TabIndex = 22;
            this.avr_permit_reset_switch.Text = "���������� ������ �� ���. \r\n�����������";
            this.avr_permit_reset_switch.UseVisualStyleBackColor = true;
            // 
            // avr_time_return
            // 
            this.avr_time_return.Location = new System.Drawing.Point(152, 272);
            this.avr_time_return.Name = "avr_time_return";
            this.avr_time_return.Size = new System.Drawing.Size(87, 20);
            this.avr_time_return.TabIndex = 30;
            this.avr_time_return.Tag = "3000000";
            this.avr_time_return.Text = "0";
            this.avr_time_return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(9, 254);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(49, 13);
            this.label62.TabIndex = 40;
            this.label62.Text = "�������";
            // 
            // avr_time_abrasion
            // 
            this.avr_time_abrasion.Location = new System.Drawing.Point(152, 231);
            this.avr_time_abrasion.Name = "avr_time_abrasion";
            this.avr_time_abrasion.Size = new System.Drawing.Size(87, 20);
            this.avr_time_abrasion.TabIndex = 29;
            this.avr_time_abrasion.Tag = "3000000";
            this.avr_time_abrasion.Text = "0";
            this.avr_time_abrasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(9, 234);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(136, 13);
            this.label63.TabIndex = 39;
            this.label63.Text = "����� ������������, ��";
            // 
            // avr_return
            // 
            this.avr_return.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_return.FormattingEnabled = true;
            this.avr_return.Location = new System.Drawing.Point(152, 251);
            this.avr_return.Name = "avr_return";
            this.avr_return.Size = new System.Drawing.Size(87, 21);
            this.avr_return.TabIndex = 28;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(9, 214);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 38;
            this.label64.Text = "������������";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(9, 193);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(101, 13);
            this.label65.TabIndex = 37;
            this.label65.Text = "����� ����������";
            // 
            // avr_abrasion
            // 
            this.avr_abrasion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_abrasion.FormattingEnabled = true;
            this.avr_abrasion.Location = new System.Drawing.Point(152, 210);
            this.avr_abrasion.Name = "avr_abrasion";
            this.avr_abrasion.Size = new System.Drawing.Size(87, 21);
            this.avr_abrasion.TabIndex = 26;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 171);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 13);
            this.label66.TabIndex = 36;
            this.label66.Text = "����������";
            // 
            // avr_reset_blocking
            // 
            this.avr_reset_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_reset_blocking.FormattingEnabled = true;
            this.avr_reset_blocking.Location = new System.Drawing.Point(152, 189);
            this.avr_reset_blocking.Name = "avr_reset_blocking";
            this.avr_reset_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_reset_blocking.TabIndex = 24;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.avr_abrasion_switch);
            this.groupBox12.Controls.Add(this.avr_supply_off);
            this.groupBox12.Controls.Add(this.avr_self_off);
            this.groupBox12.Controls.Add(this.avr_switch_off);
            this.groupBox12.Location = new System.Drawing.Point(6, 13);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(233, 100);
            this.groupBox12.TabIndex = 19;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "���������� �������";
            // 
            // avr_abrasion_switch
            // 
            this.avr_abrasion_switch.AutoSize = true;
            this.avr_abrasion_switch.Location = new System.Drawing.Point(6, 72);
            this.avr_abrasion_switch.Name = "avr_abrasion_switch";
            this.avr_abrasion_switch.Size = new System.Drawing.Size(79, 17);
            this.avr_abrasion_switch.TabIndex = 21;
            this.avr_abrasion_switch.Text = "�� ������";
            this.avr_abrasion_switch.UseVisualStyleBackColor = true;
            // 
            // avr_supply_off
            // 
            this.avr_supply_off.AutoSize = true;
            this.avr_supply_off.Location = new System.Drawing.Point(6, 15);
            this.avr_supply_off.Name = "avr_supply_off";
            this.avr_supply_off.Size = new System.Drawing.Size(81, 17);
            this.avr_supply_off.TabIndex = 18;
            this.avr_supply_off.Text = "�� �������";
            this.avr_supply_off.UseVisualStyleBackColor = true;
            // 
            // avr_self_off
            // 
            this.avr_self_off.AutoSize = true;
            this.avr_self_off.Location = new System.Drawing.Point(6, 53);
            this.avr_self_off.Name = "avr_self_off";
            this.avr_self_off.Size = new System.Drawing.Size(112, 17);
            this.avr_self_off.TabIndex = 20;
            this.avr_self_off.Text = "��������������";
            this.avr_self_off.UseVisualStyleBackColor = true;
            // 
            // avr_switch_off
            // 
            this.avr_switch_off.AutoSize = true;
            this.avr_switch_off.Location = new System.Drawing.Point(6, 34);
            this.avr_switch_off.Name = "avr_switch_off";
            this.avr_switch_off.Size = new System.Drawing.Size(103, 17);
            this.avr_switch_off.TabIndex = 19;
            this.avr_switch_off.Text = "�� ����������";
            this.avr_switch_off.UseVisualStyleBackColor = true;
            // 
            // avr_blocking
            // 
            this.avr_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_blocking.FormattingEnabled = true;
            this.avr_blocking.Location = new System.Drawing.Point(152, 168);
            this.avr_blocking.Name = "avr_blocking";
            this.avr_blocking.Size = new System.Drawing.Size(87, 21);
            this.avr_blocking.TabIndex = 3;
            // 
            // avr_start
            // 
            this.avr_start.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avr_start.FormattingEnabled = true;
            this.avr_start.Location = new System.Drawing.Point(152, 147);
            this.avr_start.Name = "avr_start";
            this.avr_start.Size = new System.Drawing.Size(87, 21);
            this.avr_start.TabIndex = 1;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.lzshConf);
            this.groupBox13.Controls.Add(this.label57);
            this.groupBox13.Controls.Add(this.lzsh_constraint);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Location = new System.Drawing.Point(8, 215);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(256, 76);
            this.groupBox13.TabIndex = 23;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "��������� ���";
            // 
            // lzshConf
            // 
            this.lzshConf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lzshConf.FormattingEnabled = true;
            this.lzshConf.Location = new System.Drawing.Point(152, 12);
            this.lzshConf.Name = "lzshConf";
            this.lzshConf.Size = new System.Drawing.Size(87, 21);
            this.lzshConf.TabIndex = 28;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(8, 42);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(65, 13);
            this.label57.TabIndex = 27;
            this.label57.Text = "�������, I�";
            // 
            // lzsh_constraint
            // 
            this.lzsh_constraint.Location = new System.Drawing.Point(152, 39);
            this.lzsh_constraint.Name = "lzsh_constraint";
            this.lzsh_constraint.Size = new System.Drawing.Size(87, 20);
            this.lzsh_constraint.TabIndex = 24;
            this.lzsh_constraint.Tag = "40";
            this.lzsh_constraint.Text = "0";
            this.lzsh_constraint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(8, 15);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(80, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "������������";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._apvSwithOff);
            this.groupBox14.Controls.Add(this.label55);
            this.groupBox14.Controls.Add(this.label47);
            this.groupBox14.Controls.Add(this.label56);
            this.groupBox14.Controls.Add(this.apv_time_4krat);
            this.groupBox14.Controls.Add(this.label52);
            this.groupBox14.Controls.Add(this.apv_time_3krat);
            this.groupBox14.Controls.Add(this.label53);
            this.groupBox14.Controls.Add(this.apv_time_2krat);
            this.groupBox14.Controls.Add(this.label54);
            this.groupBox14.Controls.Add(this.apv_time_1krat);
            this.groupBox14.Controls.Add(this.label50);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.apv_time_ready);
            this.groupBox14.Controls.Add(this.label49);
            this.groupBox14.Controls.Add(this.apv_time_blocking);
            this.groupBox14.Controls.Add(this.apv_blocking);
            this.groupBox14.Controls.Add(this.apv_conf);
            this.groupBox14.Location = new System.Drawing.Point(8, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(256, 206);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "������������ ���";
            // 
            // _apvSwithOff
            // 
            this._apvSwithOff.AutoSize = true;
            this._apvSwithOff.Location = new System.Drawing.Point(224, 180);
            this._apvSwithOff.Name = "_apvSwithOff";
            this._apvSwithOff.Size = new System.Drawing.Size(15, 14);
            this._apvSwithOff.TabIndex = 21;
            this._apvSwithOff.UseVisualStyleBackColor = true;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(8, 180);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(209, 13);
            this.label55.TabIndex = 34;
            this.label55.Text = "������ ��� �� ��������. �����������\r\n";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(8, 20);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 25;
            this.label47.Text = "������������";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(8, 160);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(101, 13);
            this.label56.TabIndex = 33;
            this.label56.Text = "����� 4 �����, ��";
            // 
            // apv_time_4krat
            // 
            this.apv_time_4krat.Location = new System.Drawing.Point(152, 157);
            this.apv_time_4krat.Name = "apv_time_4krat";
            this.apv_time_4krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_4krat.TabIndex = 23;
            this.apv_time_4krat.Tag = "3000000";
            this.apv_time_4krat.Text = "0";
            this.apv_time_4krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(8, 140);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 13);
            this.label52.TabIndex = 32;
            this.label52.Text = "����� 3 �����, ��";
            // 
            // apv_time_3krat
            // 
            this.apv_time_3krat.Location = new System.Drawing.Point(152, 137);
            this.apv_time_3krat.Name = "apv_time_3krat";
            this.apv_time_3krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_3krat.TabIndex = 22;
            this.apv_time_3krat.Tag = "3000000";
            this.apv_time_3krat.Text = "0";
            this.apv_time_3krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(8, 120);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(101, 13);
            this.label53.TabIndex = 31;
            this.label53.Text = "����� 2 �����, ��";
            // 
            // apv_time_2krat
            // 
            this.apv_time_2krat.Location = new System.Drawing.Point(152, 117);
            this.apv_time_2krat.Name = "apv_time_2krat";
            this.apv_time_2krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_2krat.TabIndex = 21;
            this.apv_time_2krat.Tag = "3000000";
            this.apv_time_2krat.Text = "0";
            this.apv_time_2krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(8, 100);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 13);
            this.label54.TabIndex = 30;
            this.label54.Text = "����� 1 �����, ��";
            // 
            // apv_time_1krat
            // 
            this.apv_time_1krat.Location = new System.Drawing.Point(152, 97);
            this.apv_time_1krat.Name = "apv_time_1krat";
            this.apv_time_1krat.Size = new System.Drawing.Size(87, 20);
            this.apv_time_1krat.TabIndex = 20;
            this.apv_time_1krat.Tag = "3000000";
            this.apv_time_1krat.Text = "0";
            this.apv_time_1krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(8, 80);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(120, 13);
            this.label50.TabIndex = 29;
            this.label50.Text = "����� ����������, ��";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(8, 60);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(123, 13);
            this.label51.TabIndex = 28;
            this.label51.Text = "����� ����������, ��";
            // 
            // apv_time_ready
            // 
            this.apv_time_ready.Location = new System.Drawing.Point(152, 77);
            this.apv_time_ready.Name = "apv_time_ready";
            this.apv_time_ready.Size = new System.Drawing.Size(87, 20);
            this.apv_time_ready.TabIndex = 19;
            this.apv_time_ready.Tag = "3000000";
            this.apv_time_ready.Text = "0";
            this.apv_time_ready.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(8, 40);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(68, 13);
            this.label49.TabIndex = 27;
            this.label49.Text = "����������";
            // 
            // apv_time_blocking
            // 
            this.apv_time_blocking.Location = new System.Drawing.Point(152, 57);
            this.apv_time_blocking.Name = "apv_time_blocking";
            this.apv_time_blocking.Size = new System.Drawing.Size(87, 20);
            this.apv_time_blocking.TabIndex = 18;
            this.apv_time_blocking.Tag = "3000000";
            this.apv_time_blocking.Text = "0";
            this.apv_time_blocking.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // apv_blocking
            // 
            this.apv_blocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_blocking.FormattingEnabled = true;
            this.apv_blocking.Location = new System.Drawing.Point(152, 36);
            this.apv_blocking.Name = "apv_blocking";
            this.apv_blocking.Size = new System.Drawing.Size(87, 21);
            this.apv_blocking.TabIndex = 3;
            // 
            // apv_conf
            // 
            this.apv_conf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apv_conf.FormattingEnabled = true;
            this.apv_conf.Location = new System.Drawing.Point(152, 15);
            this.apv_conf.Name = "apv_conf";
            this.apv_conf.Size = new System.Drawing.Size(87, 21);
            this.apv_conf.TabIndex = 1;
            // 
            // _externalDefensePage
            // 
            this._externalDefensePage.Controls.Add(this._externalDefenseGrid);
            this._externalDefensePage.Location = new System.Drawing.Point(4, 22);
            this._externalDefensePage.Name = "_externalDefensePage";
            this._externalDefensePage.Size = new System.Drawing.Size(947, 528);
            this._externalDefensePage.TabIndex = 2;
            this._externalDefensePage.Text = "������� ������";
            this._externalDefensePage.UseVisualStyleBackColor = true;
            // 
            // _externalDefenseGrid
            // 
            this._externalDefenseGrid.AllowUserToAddRows = false;
            this._externalDefenseGrid.AllowUserToDeleteRows = false;
            this._externalDefenseGrid.AllowUserToResizeColumns = false;
            this._externalDefenseGrid.AllowUserToResizeRows = false;
            this._externalDefenseGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDefenseGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this._externalDefenseGrid.ColumnHeadersHeight = 40;
            this._externalDefenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._externalDefenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._extDefNumberCol,
            this._exDefModeCol,
            this._exDefBlockingCol,
            this._exDefWorkingCol,
            this._exDefWorkingTimeCol,
            this._exDefReturnCol,
            this._exDefAPVreturnCol,
            this._exDefReturnNumberCol,
            this._exDefReturnTimeCol,
            this._exDefUROVcol,
            this._exDefOSCcol,
            this._exDefAPVcol,
            this._exDefAVRcol,
            this._exDefResetcol});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDefenseGrid.DefaultCellStyle = dataGridViewCellStyle40;
            this._externalDefenseGrid.Location = new System.Drawing.Point(3, 3);
            this._externalDefenseGrid.Name = "_externalDefenseGrid";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDefenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this._externalDefenseGrid.RowHeadersVisible = false;
            this._externalDefenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDefenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle42;
            this._externalDefenseGrid.RowTemplate.Height = 24;
            this._externalDefenseGrid.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this._externalDefenseGrid.Size = new System.Drawing.Size(941, 270);
            this._externalDefenseGrid.TabIndex = 0;
            // 
            // _extDefNumberCol
            // 
            this._extDefNumberCol.HeaderText = "�";
            this._extDefNumberCol.Name = "_extDefNumberCol";
            this._extDefNumberCol.ReadOnly = true;
            this._extDefNumberCol.Width = 35;
            // 
            // _exDefModeCol
            // 
            this._exDefModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefModeCol.HeaderText = "�����";
            this._exDefModeCol.Name = "_exDefModeCol";
            this._exDefModeCol.Width = 80;
            // 
            // _exDefBlockingCol
            // 
            this._exDefBlockingCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefBlockingCol.HeaderText = "����������";
            this._exDefBlockingCol.Name = "_exDefBlockingCol";
            this._exDefBlockingCol.Width = 110;
            // 
            // _exDefWorkingCol
            // 
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._exDefWorkingCol.DefaultCellStyle = dataGridViewCellStyle39;
            this._exDefWorkingCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefWorkingCol.HeaderText = "������������";
            this._exDefWorkingCol.Name = "_exDefWorkingCol";
            this._exDefWorkingCol.Width = 110;
            // 
            // _exDefWorkingTimeCol
            // 
            this._exDefWorkingTimeCol.HeaderText = "t��, ��";
            this._exDefWorkingTimeCol.Name = "_exDefWorkingTimeCol";
            this._exDefWorkingTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefWorkingTimeCol.Width = 50;
            // 
            // _exDefReturnCol
            // 
            this._exDefReturnCol.HeaderText = "����.";
            this._exDefReturnCol.Name = "_exDefReturnCol";
            this._exDefReturnCol.Width = 40;
            // 
            // _exDefAPVreturnCol
            // 
            this._exDefAPVreturnCol.HeaderText = "��� ��";
            this._exDefAPVreturnCol.Name = "_exDefAPVreturnCol";
            this._exDefAPVreturnCol.Width = 40;
            // 
            // _exDefReturnNumberCol
            // 
            this._exDefReturnNumberCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._exDefReturnNumberCol.HeaderText = "���� ��������";
            this._exDefReturnNumberCol.Name = "_exDefReturnNumberCol";
            this._exDefReturnNumberCol.Width = 110;
            // 
            // _exDefReturnTimeCol
            // 
            this._exDefReturnTimeCol.HeaderText = "t��, ��";
            this._exDefReturnTimeCol.Name = "_exDefReturnTimeCol";
            this._exDefReturnTimeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._exDefReturnTimeCol.Width = 50;
            // 
            // _exDefUROVcol
            // 
            this._exDefUROVcol.HeaderText = "����";
            this._exDefUROVcol.Name = "_exDefUROVcol";
            this._exDefUROVcol.Width = 43;
            // 
            // _exDefOSCcol
            // 
            this._exDefOSCcol.HeaderText = "���.";
            this._exDefOSCcol.Name = "_exDefOSCcol";
            this._exDefOSCcol.Width = 36;
            // 
            // _exDefAPVcol
            // 
            this._exDefAPVcol.HeaderText = "���";
            this._exDefAPVcol.Name = "_exDefAPVcol";
            this._exDefAPVcol.Width = 35;
            // 
            // _exDefAVRcol
            // 
            this._exDefAVRcol.HeaderText = "���";
            this._exDefAVRcol.Name = "_exDefAVRcol";
            this._exDefAVRcol.Width = 34;
            // 
            // _exDefResetcol
            // 
            this._exDefResetcol.HeaderText = "�����";
            this._exDefResetcol.Name = "_exDefResetcol";
            this._exDefResetcol.Width = 44;
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox6);
            this._outputSignalsPage.Controls.Add(this.groupBox5);
            this._outputSignalsPage.Controls.Add(this.groupBox9);
            this._outputSignalsPage.Controls.Add(this.groupBox8);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(947, 528);
            this._outputSignalsPage.TabIndex = 1;
            this._outputSignalsPage.Text = "�������� �������";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox22);
            this.groupBox6.Controls.Add(this.groupBox21);
            this.groupBox6.Location = new System.Drawing.Point(639, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 202);
            this.groupBox6.TabIndex = 34;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���� �������������";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label11);
            this.groupBox22.Controls.Add(this._releDispepairBox);
            this.groupBox22.Location = new System.Drawing.Point(6, 12);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(190, 38);
            this.groupBox22.TabIndex = 8;
            this.groupBox22.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "T�����., ��";
            // 
            // _releDispepairBox
            // 
            this._releDispepairBox.Location = new System.Drawing.Point(110, 11);
            this._releDispepairBox.Name = "_releDispepairBox";
            this._releDispepairBox.Size = new System.Drawing.Size(74, 20);
            this._releDispepairBox.TabIndex = 9;
            this._releDispepairBox.Tag = "3000000";
            this._releDispepairBox.Text = "0";
            this._releDispepairBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._dispepairCheckList);
            this.groupBox21.Location = new System.Drawing.Point(6, 50);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(190, 144);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "������";
            // 
            // _dispepairCheckList
            // 
            this._dispepairCheckList.CheckOnClick = true;
            this._dispepairCheckList.FormattingEnabled = true;
            this._dispepairCheckList.Items.AddRange(new object[] {
            "������������� 1",
            "������������� 2",
            "������������� 3",
            "������������� 4",
            "������������� 5",
            "������������� 6",
            "������������� 7",
            "������������� 8"});
            this._dispepairCheckList.Location = new System.Drawing.Point(9, 14);
            this._dispepairCheckList.Name = "_dispepairCheckList";
            this._dispepairCheckList.Size = new System.Drawing.Size(175, 124);
            this._dispepairCheckList.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.VLSTabControl);
            this.groupBox5.Location = new System.Drawing.Point(438, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(195, 516);
            this.groupBox5.TabIndex = 33;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "�������� ���������� �������";
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VLSTabControl.Location = new System.Drawing.Point(3, 16);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(189, 497);
            this.VLSTabControl.TabIndex = 32;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(181, 444);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "��� 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox1.TabIndex = 6;
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(181, 444);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "��� 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox2.TabIndex = 6;
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(181, 444);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "��� 3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox3.TabIndex = 6;
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(181, 444);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "��� 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox4.TabIndex = 6;
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(181, 444);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "��� 5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox5.TabIndex = 6;
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(181, 444);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "��� 6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox6.TabIndex = 6;
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(181, 444);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "��� 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox7.TabIndex = 6;
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(181, 444);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "��� 8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(0, 0);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(181, 439);
            this.VLScheckedListBox8.TabIndex = 6;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox9.Location = new System.Drawing.Point(8, 245);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(424, 250);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "����������";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndResetCol,
            this._outIndAlarmCol,
            this._outIndSystemCol});
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputIndicatorsGrid.DefaultCellStyle = dataGridViewCellStyle44;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(8, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputIndicatorsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle46;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(410, 230);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "�";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 20;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.HeaderText = "���";
            this._outIndTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.HeaderText = "������";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndResetCol
            // 
            this._outIndResetCol.HeaderText = "����� ���.";
            this._outIndResetCol.Name = "_outIndResetCol";
            this._outIndResetCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndResetCol.Width = 40;
            // 
            // _outIndAlarmCol
            // 
            this._outIndAlarmCol.HeaderText = "����� ��";
            this._outIndAlarmCol.Name = "_outIndAlarmCol";
            this._outIndAlarmCol.Width = 40;
            // 
            // _outIndSystemCol
            // 
            this._outIndSystemCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._outIndSystemCol.HeaderText = "����� ��";
            this._outIndSystemCol.Name = "_outIndSystemCol";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._outputReleGrid);
            this.groupBox8.Location = new System.Drawing.Point(8, 5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(424, 234);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "�������� ����";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle47;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releImpulseCol});
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._outputReleGrid.DefaultCellStyle = dataGridViewCellStyle48;
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._outputReleGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle49;
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle50;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(410, 215);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "�";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "���";
            this._releTypeCol.Items.AddRange(new object[] {
            "�����������",
            "�������",
            "XXXXX"});
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "������";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releImpulseCol
            // 
            this._releImpulseCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._releImpulseCol.HeaderText = "T�����., ��";
            this._releImpulseCol.Name = "_releImpulseCol";
            this._releImpulseCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _inSignalsPage
            // 
            this._inSignalsPage.Controls.Add(this.groupBox18);
            this._inSignalsPage.Controls.Add(this.groupBox19);
            this._inSignalsPage.Controls.Add(this.groupBox7);
            this._inSignalsPage.Controls.Add(this.groupBox4);
            this._inSignalsPage.Controls.Add(this.groupBox3);
            this._inSignalsPage.Controls.Add(this.groupBox1);
            this._inSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inSignalsPage.Name = "_inSignalsPage";
            this._inSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._inSignalsPage.Size = new System.Drawing.Size(947, 528);
            this._inSignalsPage.TabIndex = 0;
            this._inSignalsPage.Text = "������� �������";
            this._inSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.gb1);
            this.groupBox18.Controls.Add(this.gb3);
            this.groupBox18.Controls.Add(this.gb2);
            this.groupBox18.Location = new System.Drawing.Point(233, 189);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(203, 192);
            this.groupBox18.TabIndex = 24;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "������������ ������������";
            // 
            // gb1
            // 
            this.gb1.Controls.Add(this.oscLenCombo);
            this.gb1.Controls.Add(this.oscLenBox);
            this.gb1.Location = new System.Drawing.Point(6, 19);
            this.gb1.Name = "gb1";
            this.gb1.Size = new System.Drawing.Size(190, 46);
            this.gb1.TabIndex = 21;
            this.gb1.TabStop = false;
            this.gb1.Text = "����. ������� ���.";
            // 
            // oscLenCombo
            // 
            this.oscLenCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscLenCombo.FormattingEnabled = true;
            this.oscLenCombo.Items.AddRange(new object[] {
            "1 �������������",
            "1 ���������������� ���.",
            "2 ���������������� ���."});
            this.oscLenCombo.Location = new System.Drawing.Point(6, 19);
            this.oscLenCombo.Name = "oscLenCombo";
            this.oscLenCombo.Size = new System.Drawing.Size(45, 21);
            this.oscLenCombo.TabIndex = 0;
            this.oscLenCombo.SelectedIndexChanged += new System.EventHandler(this.oscLenCombo_SelectedIndexChanged);
            // 
            // oscLenBox
            // 
            this.oscLenBox.Location = new System.Drawing.Point(57, 19);
            this.oscLenBox.Name = "oscLenBox";
            this.oscLenBox.ReadOnly = true;
            this.oscLenBox.Size = new System.Drawing.Size(70, 20);
            this.oscLenBox.TabIndex = 0;
            this.oscLenBox.Tag = "99";
            this.oscLenBox.Text = "1";
            this.oscLenBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gb3
            // 
            this.gb3.Controls.Add(this.label28);
            this.gb3.Controls.Add(this.oscPercent);
            this.gb3.Location = new System.Drawing.Point(6, 123);
            this.gb3.Name = "gb3";
            this.gb3.Size = new System.Drawing.Size(190, 46);
            this.gb3.TabIndex = 23;
            this.gb3.TabStop = false;
            this.gb3.Text = "����. ���������� ���.";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(71, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(15, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "%";
            // 
            // oscPercent
            // 
            this.oscPercent.Location = new System.Drawing.Point(6, 17);
            this.oscPercent.Name = "oscPercent";
            this.oscPercent.Size = new System.Drawing.Size(59, 20);
            this.oscPercent.TabIndex = 0;
            this.oscPercent.Tag = "99";
            this.oscPercent.Text = "1";
            this.oscPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.oscFix);
            this.gb2.Location = new System.Drawing.Point(6, 71);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(190, 46);
            this.gb2.TabIndex = 22;
            this.gb2.TabStop = false;
            this.gb2.Text = "�������� ���.";
            // 
            // oscFix
            // 
            this.oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscFix.FormattingEnabled = true;
            this.oscFix.Items.AddRange(new object[] {
            "1 �������������",
            "1 ���������������� ���.",
            "2 ���������������� ���."});
            this.oscFix.Location = new System.Drawing.Point(6, 16);
            this.oscFix.Name = "oscFix";
            this.oscFix.Size = new System.Drawing.Size(121, 21);
            this.oscFix.TabIndex = 0;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.keysListBox);
            this.groupBox19.Location = new System.Drawing.Point(442, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(93, 268);
            this.groupBox19.TabIndex = 18;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "�����";
            // 
            // keysListBox
            // 
            this.keysListBox.CheckOnClick = true;
            this.keysListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keysListBox.FormattingEnabled = true;
            this.keysListBox.Location = new System.Drawing.Point(3, 16);
            this.keysListBox.Name = "keysListBox";
            this.keysListBox.Size = new System.Drawing.Size(87, 249);
            this.keysListBox.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._manageSignalsSDTU_Combo);
            this.groupBox7.Controls.Add(this.label43);
            this.groupBox7.Controls.Add(this._manageSignalsExternalCombo);
            this.groupBox7.Controls.Add(this.label44);
            this.groupBox7.Controls.Add(this._manageSignalsKeyCombo);
            this.groupBox7.Controls.Add(this.label45);
            this.groupBox7.Controls.Add(this._manageSignalsButtonCombo);
            this.groupBox7.Controls.Add(this.label46);
            this.groupBox7.Location = new System.Drawing.Point(8, 340);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(219, 107);
            this.groupBox7.TabIndex = 16;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� ����������";
            // 
            // _manageSignalsSDTU_Combo
            // 
            this._manageSignalsSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsSDTU_Combo.FormattingEnabled = true;
            this._manageSignalsSDTU_Combo.Location = new System.Drawing.Point(79, 77);
            this._manageSignalsSDTU_Combo.Name = "_manageSignalsSDTU_Combo";
            this._manageSignalsSDTU_Combo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsSDTU_Combo.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 79);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "�� ����";
            // 
            // _manageSignalsExternalCombo
            // 
            this._manageSignalsExternalCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsExternalCombo.FormattingEnabled = true;
            this._manageSignalsExternalCombo.Location = new System.Drawing.Point(79, 58);
            this._manageSignalsExternalCombo.Name = "_manageSignalsExternalCombo";
            this._manageSignalsExternalCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsExternalCombo.TabIndex = 21;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 60);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "�������";
            // 
            // _manageSignalsKeyCombo
            // 
            this._manageSignalsKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsKeyCombo.FormattingEnabled = true;
            this._manageSignalsKeyCombo.Location = new System.Drawing.Point(79, 39);
            this._manageSignalsKeyCombo.Name = "_manageSignalsKeyCombo";
            this._manageSignalsKeyCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsKeyCombo.TabIndex = 19;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 41);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "�� �����";
            // 
            // _manageSignalsButtonCombo
            // 
            this._manageSignalsButtonCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._manageSignalsButtonCombo.FormattingEnabled = true;
            this._manageSignalsButtonCombo.Location = new System.Drawing.Point(79, 19);
            this._manageSignalsButtonCombo.Name = "_manageSignalsButtonCombo";
            this._manageSignalsButtonCombo.Size = new System.Drawing.Size(126, 21);
            this._manageSignalsButtonCombo.TabIndex = 17;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "�� ������";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this._switcherDurationBox);
            this.groupBox4.Controls.Add(this._switcherTokBox);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this._switcherTimeBox);
            this.groupBox4.Controls.Add(this._switcherImpulseBox);
            this.groupBox4.Controls.Add(this._switcherBlockCombo);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._switcherErrorCombo);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._switcherStateOnCombo);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._switcherStateOffCombo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(233, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(203, 172);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "�����������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(0, 148);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "����. ���������, ��";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(0, 110);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 13);
            this.label17.TabIndex = 19;
            this.label17.Text = "��� ����, I�";
            // 
            // _switcherDurationBox
            // 
            this._switcherDurationBox.Location = new System.Drawing.Point(125, 147);
            this._switcherDurationBox.Name = "_switcherDurationBox";
            this._switcherDurationBox.Size = new System.Drawing.Size(71, 20);
            this._switcherDurationBox.TabIndex = 22;
            this._switcherDurationBox.Tag = "3000000";
            this._switcherDurationBox.Text = "0";
            this._switcherDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTokBox
            // 
            this._switcherTokBox.Location = new System.Drawing.Point(125, 107);
            this._switcherTokBox.Name = "_switcherTokBox";
            this._switcherTokBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTokBox.TabIndex = 18;
            this._switcherTokBox.Tag = "40";
            this._switcherTokBox.Text = "0";
            this._switcherTokBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(0, 129);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 13);
            this.label20.TabIndex = 21;
            this.label20.Text = "������� ��, ��";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(0, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(93, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "����� ����, ��";
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(125, 87);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(71, 20);
            this._switcherTimeBox.TabIndex = 16;
            this._switcherTimeBox.Tag = "3000000";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherImpulseBox
            // 
            this._switcherImpulseBox.Location = new System.Drawing.Point(125, 127);
            this._switcherImpulseBox.Name = "_switcherImpulseBox";
            this._switcherImpulseBox.Size = new System.Drawing.Size(71, 20);
            this._switcherImpulseBox.TabIndex = 20;
            this._switcherImpulseBox.Tag = "3000000";
            this._switcherImpulseBox.Text = "0";
            this._switcherImpulseBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(125, 68);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherBlockCombo.TabIndex = 15;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(0, 72);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "����������";
            // 
            // _switcherErrorCombo
            // 
            this._switcherErrorCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherErrorCombo.FormattingEnabled = true;
            this._switcherErrorCombo.Location = new System.Drawing.Point(125, 49);
            this._switcherErrorCombo.Name = "_switcherErrorCombo";
            this._switcherErrorCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherErrorCombo.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(0, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "�������������";
            // 
            // _switcherStateOnCombo
            // 
            this._switcherStateOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOnCombo.FormattingEnabled = true;
            this._switcherStateOnCombo.Location = new System.Drawing.Point(125, 30);
            this._switcherStateOnCombo.Name = "_switcherStateOnCombo";
            this._switcherStateOnCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOnCombo.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(0, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "��������� ��������";
            // 
            // _switcherStateOffCombo
            // 
            this._switcherStateOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherStateOffCombo.FormattingEnabled = true;
            this._switcherStateOffCombo.Location = new System.Drawing.Point(125, 11);
            this._switcherStateOffCombo.Name = "_switcherStateOffCombo";
            this._switcherStateOffCombo.Size = new System.Drawing.Size(71, 21);
            this._switcherStateOffCombo.TabIndex = 9;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(0, 15);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(118, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "��������� ���������";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._gr4ComboBox);
            this.groupBox3.Controls.Add(this._gr3ComboBox);
            this.groupBox3.Controls.Add(this._gr2ComboBox);
            this.groupBox3.Controls.Add(this._gr1ComboBox);
            this.groupBox3.Controls.Add(this._pereklLabel4);
            this.groupBox3.Controls.Add(this._pereklLabel3);
            this.groupBox3.Controls.Add(this._pereklLabel2);
            this.groupBox3.Controls.Add(this._pereklLabel1);
            this.groupBox3.Controls.Add(this._blockSdtu);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this._constraintGroupCombo);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._signalizationCombo);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._extOffCombo);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._extOnCombo);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this._keyOffCombo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this._keyOnCombo);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(8, 93);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(219, 241);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "������� �������";
            // 
            // _gr4ComboBox
            // 
            this._gr4ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gr4ComboBox.FormattingEnabled = true;
            this._gr4ComboBox.Location = new System.Drawing.Point(132, 211);
            this._gr4ComboBox.Name = "_gr4ComboBox";
            this._gr4ComboBox.Size = new System.Drawing.Size(71, 21);
            this._gr4ComboBox.TabIndex = 19;
            // 
            // _gr3ComboBox
            // 
            this._gr3ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gr3ComboBox.FormattingEnabled = true;
            this._gr3ComboBox.Location = new System.Drawing.Point(132, 193);
            this._gr3ComboBox.Name = "_gr3ComboBox";
            this._gr3ComboBox.Size = new System.Drawing.Size(71, 21);
            this._gr3ComboBox.TabIndex = 18;
            // 
            // _gr2ComboBox
            // 
            this._gr2ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gr2ComboBox.FormattingEnabled = true;
            this._gr2ComboBox.Location = new System.Drawing.Point(132, 175);
            this._gr2ComboBox.Name = "_gr2ComboBox";
            this._gr2ComboBox.Size = new System.Drawing.Size(71, 21);
            this._gr2ComboBox.TabIndex = 17;
            // 
            // _gr1ComboBox
            // 
            this._gr1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._gr1ComboBox.FormattingEnabled = true;
            this._gr1ComboBox.Location = new System.Drawing.Point(132, 157);
            this._gr1ComboBox.Name = "_gr1ComboBox";
            this._gr1ComboBox.Size = new System.Drawing.Size(71, 21);
            this._gr1ComboBox.TabIndex = 16;
            // 
            // _pereklLabel4
            // 
            this._pereklLabel4.AutoSize = true;
            this._pereklLabel4.Location = new System.Drawing.Point(6, 214);
            this._pereklLabel4.Name = "_pereklLabel4";
            this._pereklLabel4.Size = new System.Drawing.Size(111, 13);
            this._pereklLabel4.TabIndex = 15;
            this._pereklLabel4.Text = "������. �� ��. ���. 4";
            // 
            // _pereklLabel3
            // 
            this._pereklLabel3.AutoSize = true;
            this._pereklLabel3.Location = new System.Drawing.Point(6, 196);
            this._pereklLabel3.Name = "_pereklLabel3";
            this._pereklLabel3.Size = new System.Drawing.Size(111, 13);
            this._pereklLabel3.TabIndex = 14;
            this._pereklLabel3.Text = "������. �� ��. ���. 3";
            // 
            // _pereklLabel2
            // 
            this._pereklLabel2.AutoSize = true;
            this._pereklLabel2.Location = new System.Drawing.Point(6, 178);
            this._pereklLabel2.Name = "_pereklLabel2";
            this._pereklLabel2.Size = new System.Drawing.Size(111, 13);
            this._pereklLabel2.TabIndex = 13;
            this._pereklLabel2.Text = "������. �� ��. ���. 2";
            // 
            // _pereklLabel1
            // 
            this._pereklLabel1.AutoSize = true;
            this._pereklLabel1.Location = new System.Drawing.Point(6, 160);
            this._pereklLabel1.Name = "_pereklLabel1";
            this._pereklLabel1.Size = new System.Drawing.Size(111, 13);
            this._pereklLabel1.TabIndex = 12;
            this._pereklLabel1.Text = "������. �� ��. ���. 1";
            // 
            // _blockSdtu
            // 
            this._blockSdtu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockSdtu.FormattingEnabled = true;
            this._blockSdtu.Location = new System.Drawing.Point(132, 136);
            this._blockSdtu.Name = "_blockSdtu";
            this._blockSdtu.Size = new System.Drawing.Size(71, 21);
            this._blockSdtu.TabIndex = 11;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 139);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(102, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "���������� ����";
            // 
            // _constraintGroupCombo
            // 
            this._constraintGroupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._constraintGroupCombo.FormattingEnabled = true;
            this._constraintGroupCombo.Location = new System.Drawing.Point(132, 116);
            this._constraintGroupCombo.Name = "_constraintGroupCombo";
            this._constraintGroupCombo.Size = new System.Drawing.Size(71, 21);
            this._constraintGroupCombo.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "������ �������";
            // 
            // _signalizationCombo
            // 
            this._signalizationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._signalizationCombo.FormattingEnabled = true;
            this._signalizationCombo.Location = new System.Drawing.Point(132, 96);
            this._signalizationCombo.Name = "_signalizationCombo";
            this._signalizationCombo.Size = new System.Drawing.Size(71, 21);
            this._signalizationCombo.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 99);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "����� ���������";
            // 
            // _extOffCombo
            // 
            this._extOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOffCombo.FormattingEnabled = true;
            this._extOffCombo.Location = new System.Drawing.Point(132, 76);
            this._extOffCombo.Name = "_extOffCombo";
            this._extOffCombo.Size = new System.Drawing.Size(71, 21);
            this._extOffCombo.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "����. ���������";
            // 
            // _extOnCombo
            // 
            this._extOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._extOnCombo.FormattingEnabled = true;
            this._extOnCombo.Location = new System.Drawing.Point(132, 56);
            this._extOnCombo.Name = "_extOnCombo";
            this._extOnCombo.Size = new System.Drawing.Size(71, 21);
            this._extOnCombo.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 79);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "����. ��������";
            // 
            // _keyOffCombo
            // 
            this._keyOffCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOffCombo.FormattingEnabled = true;
            this._keyOffCombo.Location = new System.Drawing.Point(132, 36);
            this._keyOffCombo.Name = "_keyOffCombo";
            this._keyOffCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOffCombo.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "���� ���������";
            // 
            // _keyOnCombo
            // 
            this._keyOnCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._keyOnCombo.FormattingEnabled = true;
            this._keyOnCombo.Location = new System.Drawing.Point(132, 16);
            this._keyOnCombo.Name = "_keyOnCombo";
            this._keyOnCombo.Size = new System.Drawing.Size(71, 21);
            this._keyOnCombo.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "���� ��������";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._TT_typeCombo);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._maxTok_Box);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._TTNP_Box);
            this.groupBox1.Controls.Add(this._TT_Box);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 85);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "��������� ����";
            // 
            // _TT_typeCombo
            // 
            this._TT_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeCombo.FormattingEnabled = true;
            this._TT_typeCombo.Location = new System.Drawing.Point(66, 95);
            this._TT_typeCombo.Name = "_TT_typeCombo";
            this._TT_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TT_typeCombo.TabIndex = 14;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 98);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "��� �T";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "����. ��� ��������, I�";
            // 
            // _maxTok_Box
            // 
            this._maxTok_Box.Location = new System.Drawing.Point(143, 56);
            this._maxTok_Box.Name = "_maxTok_Box";
            this._maxTok_Box.Size = new System.Drawing.Size(60, 20);
            this._maxTok_Box.TabIndex = 5;
            this._maxTok_Box.Tag = "40";
            this._maxTok_Box.Text = "0";
            this._maxTok_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "��������� ��� ��, A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "��������� ��� ����, A";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(143, 36);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(60, 20);
            this._TTNP_Box.TabIndex = 2;
            this._TTNP_Box.Tag = "1000";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._inSignalsPage);
            this._tabControl.Controls.Add(this._logicSignalsPage);
            this._tabControl.Controls.Add(this._outputSignalsPage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._externalDefensePage);
            this._tabControl.Controls.Add(this._tokDefensesPage);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(955, 554);
            this._tabControl.TabIndex = 6;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointStruct,
            this.resetSetpointStruct,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // clearSetpointStruct
            // 
            this.clearSetpointStruct.Name = "clearSetpointStruct";
            this.clearSetpointStruct.Size = new System.Drawing.Size(212, 22);
            this.clearSetpointStruct.Text = "��������� ���. �������";
            // 
            // resetSetpointStruct
            // 
            this.resetSetpointStruct.Name = "resetSetpointStruct";
            this.resetSetpointStruct.Size = new System.Drawing.Size(212, 22);
            this.resetSetpointStruct.Text = "�������� �������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "��������� � HTML";
            // 
            // _logicSignalsPage
            // 
            this._logicSignalsPage.Controls.Add(this.groupBox24);
            this._logicSignalsPage.Controls.Add(this.groupBox27);
            this._logicSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._logicSignalsPage.Name = "_logicSignalsPage";
            this._logicSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._logicSignalsPage.Size = new System.Drawing.Size(947, 528);
            this._logicSignalsPage.TabIndex = 5;
            this._logicSignalsPage.Text = "������� ���������� �������";
            this._logicSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.tabControl2);
            this.groupBox24.Location = new System.Drawing.Point(207, 6);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(193, 516);
            this.groupBox24.TabIndex = 34;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "���������� ������� ���";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(187, 497);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals5);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(179, 468);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "��5";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals5
            // 
            this._inputSignals5.AllowUserToAddRows = false;
            this._inputSignals5.AllowUserToDeleteRows = false;
            this._inputSignals5.AllowUserToResizeColumns = false;
            this._inputSignals5.AllowUserToResizeRows = false;
            this._inputSignals5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals5.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals5.Location = new System.Drawing.Point(0, 0);
            this._inputSignals5.MultiSelect = false;
            this._inputSignals5.Name = "_inputSignals5";
            this._inputSignals5.RowHeadersVisible = false;
            this._inputSignals5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals5.RowsDefaultCellStyle = dataGridViewCellStyle51;
            this._inputSignals5.RowTemplate.Height = 20;
            this._inputSignals5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals5.ShowCellErrors = false;
            this._inputSignals5.ShowCellToolTips = false;
            this._inputSignals5.ShowEditingIcon = false;
            this._inputSignals5.ShowRowErrors = false;
            this._inputSignals5.Size = new System.Drawing.Size(179, 468);
            this._inputSignals5.TabIndex = 2;
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "�";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "��������";
            this._signalValueColILI.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals6);
            this.tabPage10.Location = new System.Drawing.Point(4, 25);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(179, 468);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "��6";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals6
            // 
            this._inputSignals6.AllowUserToAddRows = false;
            this._inputSignals6.AllowUserToDeleteRows = false;
            this._inputSignals6.AllowUserToResizeColumns = false;
            this._inputSignals6.AllowUserToResizeRows = false;
            this._inputSignals6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals6.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals6.Location = new System.Drawing.Point(0, 0);
            this._inputSignals6.MultiSelect = false;
            this._inputSignals6.Name = "_inputSignals6";
            this._inputSignals6.RowHeadersVisible = false;
            this._inputSignals6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals6.RowsDefaultCellStyle = dataGridViewCellStyle52;
            this._inputSignals6.RowTemplate.Height = 20;
            this._inputSignals6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals6.ShowCellErrors = false;
            this._inputSignals6.ShowCellToolTips = false;
            this._inputSignals6.ShowEditingIcon = false;
            this._inputSignals6.ShowRowErrors = false;
            this._inputSignals6.Size = new System.Drawing.Size(179, 468);
            this._inputSignals6.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "�";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "��������";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals7);
            this.tabPage11.Location = new System.Drawing.Point(4, 25);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(179, 468);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "��7";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals7
            // 
            this._inputSignals7.AllowUserToAddRows = false;
            this._inputSignals7.AllowUserToDeleteRows = false;
            this._inputSignals7.AllowUserToResizeColumns = false;
            this._inputSignals7.AllowUserToResizeRows = false;
            this._inputSignals7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals7.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals7.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inputSignals7.Location = new System.Drawing.Point(0, 0);
            this._inputSignals7.MultiSelect = false;
            this._inputSignals7.Name = "_inputSignals7";
            this._inputSignals7.RowHeadersVisible = false;
            this._inputSignals7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals7.RowsDefaultCellStyle = dataGridViewCellStyle53;
            this._inputSignals7.RowTemplate.Height = 20;
            this._inputSignals7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals7.ShowCellErrors = false;
            this._inputSignals7.ShowCellToolTips = false;
            this._inputSignals7.ShowEditingIcon = false;
            this._inputSignals7.ShowRowErrors = false;
            this._inputSignals7.Size = new System.Drawing.Size(179, 468);
            this._inputSignals7.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "�";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "��������";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals8);
            this.tabPage12.Location = new System.Drawing.Point(4, 25);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(179, 468);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "��8";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals8
            // 
            this._inputSignals8.AllowUserToAddRows = false;
            this._inputSignals8.AllowUserToDeleteRows = false;
            this._inputSignals8.AllowUserToResizeColumns = false;
            this._inputSignals8.AllowUserToResizeRows = false;
            this._inputSignals8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals8.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals8.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inputSignals8.Location = new System.Drawing.Point(0, 0);
            this._inputSignals8.MultiSelect = false;
            this._inputSignals8.Name = "_inputSignals8";
            this._inputSignals8.RowHeadersVisible = false;
            this._inputSignals8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals8.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this._inputSignals8.RowTemplate.Height = 20;
            this._inputSignals8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals8.ShowCellErrors = false;
            this._inputSignals8.ShowCellToolTips = false;
            this._inputSignals8.ShowEditingIcon = false;
            this._inputSignals8.ShowRowErrors = false;
            this._inputSignals8.Size = new System.Drawing.Size(179, 468);
            this._inputSignals8.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "�";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "��������";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.tabControl1);
            this.groupBox27.Location = new System.Drawing.Point(8, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(193, 516);
            this.groupBox27.TabIndex = 33;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "���������� ������� �";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(187, 497);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(179, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "��1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(0, 0);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle55;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(179, 468);
            this._inputSignals1.TabIndex = 2;
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "�";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "��������";
            this._signalValueCol.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(179, 468);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "��2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(0, 0);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(179, 468);
            this._inputSignals2.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "�";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "��������";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(179, 468);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "��3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inputSignals3.Location = new System.Drawing.Point(0, 0);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle57;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(179, 468);
            this._inputSignals3.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "�";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "��������";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(179, 468);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "��4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._inputSignals4.Location = new System.Drawing.Point(0, 0);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle58;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(179, 468);
            this._inputSignals4.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "�";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "��������";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "���",
            "��",
            "������"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _resetButton
            // 
            this._resetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetButton.AutoSize = true;
            this._resetButton.Location = new System.Drawing.Point(462, 560);
            this._resetButton.Name = "_resetButton";
            this._resetButton.Size = new System.Drawing.Size(111, 23);
            this._resetButton.TabIndex = 36;
            this._resetButton.Text = "�������� �������";
            this._resetButton.UseVisualStyleBackColor = true;
            this._resetButton.Click += new System.EventHandler(this._resetButton_Click);
            // 
            // MR5V50ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 615);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this._resetButton);
            this.Controls.Add(this._resetConfigBut);
            this.Controls.Add(this._saveToXmlButton);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(971, 654);
            this.Name = "MR5V50ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ConfigurationForm_KeyUp);
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this._tokDefensesPage.ResumeLayout(false);
            this.NewCopyGroupBox.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.OldGroupBox.ResumeLayout(false);
            this.OldGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._igDefenseGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._otherCurrentDefensesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._currentDefensesGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._inDefenseGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._i1i2DefenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this._externalDefensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenseGrid)).EndInit();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._inSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.gb1.ResumeLayout(false);
            this.gb1.PerformLayout();
            this.gb3.ResumeLayout(false);
            this.gb3.PerformLayout();
            this.gb2.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._logicSignalsPage.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.TabPage _tokDefensesPage;
        private System.Windows.Forms.GroupBox OldGroupBox;
        private System.Windows.Forms.RadioButton _mainRadioButtonGroup;
        private System.Windows.Forms.RadioButton _reserveRadioButtonGroup;
        private System.Windows.Forms.DataGridView _inDefenseGrid;
        private System.Windows.Forms.DataGridView _otherCurrentDefensesGrid;
        private System.Windows.Forms.DataGridView _currentDefensesGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.MaskedTextBox avr_disconnection;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.MaskedTextBox avr_time_return;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox avr_time_abrasion;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox avr_return;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox avr_abrasion;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox avr_reset_blocking;
        private System.Windows.Forms.CheckBox avr_permit_reset_switch;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox avr_abrasion_switch;
        private System.Windows.Forms.CheckBox avr_supply_off;
        private System.Windows.Forms.CheckBox avr_self_off;
        private System.Windows.Forms.CheckBox avr_switch_off;
        private System.Windows.Forms.ComboBox avr_blocking;
        private System.Windows.Forms.ComboBox avr_start;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox lzshConf;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.MaskedTextBox lzsh_constraint;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox apv_time_4krat;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox apv_time_3krat;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.MaskedTextBox apv_time_2krat;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox apv_time_1krat;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.MaskedTextBox apv_time_ready;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.MaskedTextBox apv_time_blocking;
        private System.Windows.Forms.ComboBox apv_blocking;
        private System.Windows.Forms.ComboBox apv_conf;
        private System.Windows.Forms.TabPage _externalDefensePage;
        private System.Windows.Forms.DataGridView _externalDefenseGrid;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndResetCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndAlarmCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _outIndSystemCol;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.TabPage _inSignalsPage;
        private System.Windows.Forms.GroupBox gb3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.MaskedTextBox oscPercent;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.ComboBox oscFix;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _manageSignalsSDTU_Combo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox _manageSignalsExternalCombo;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox _manageSignalsKeyCombo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.ComboBox _manageSignalsButtonCombo;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox _switcherDurationBox;
        private System.Windows.Forms.MaskedTextBox _switcherTokBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseBox;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _switcherErrorCombo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _switcherStateOnCombo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox _switcherStateOffCombo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox _constraintGroupCombo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox _signalizationCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox _extOffCombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox _extOnCombo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _keyOffCombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox _keyOnCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _TT_typeCombo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox _maxTok_Box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox _achrOsc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.MaskedTextBox _chapvTime;
        private System.Windows.Forms.ComboBox _chapvBlock;
        private System.Windows.Forms.ComboBox _chapvEnter;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _achrTime;
        private System.Windows.Forms.ComboBox _achrBlock;
        private System.Windows.Forms.ComboBox _achrEnter;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.Button _groupChangeButton;
        private System.Windows.Forms.GroupBox gb1;
        private System.Windows.Forms.ComboBox oscLenCombo;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.MaskedTextBox oscLenBox;
        private System.Windows.Forms.TabPage _logicSignalsPage;
        private System.Windows.Forms.CheckedListBox keysListBox;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView _inputSignals1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView _inputSignals2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView _inputSignals3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView _inputSignals4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DataGridView _inputSignals5;
        private System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        private System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.DataGridView _inputSignals6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.DataGridView _inputSignals7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.DataGridView _inputSignals8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TabControl VLSTabControl;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        private System.Windows.Forms.ComboBox _blockSdtu;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridView _igDefenseGrid;
        private System.Windows.Forms.CheckBox _apvSwithOff;
        private System.Windows.Forms.DataGridView _i1i2DefenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _extDefNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefBlockingCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefWorkingCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefWorkingTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefReturnCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVreturnCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _exDefReturnNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _exDefReturnTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefUROVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefOSCcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAPVcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefAVRcol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _exDefResetcol;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.Button _resetConfigBut;
        private System.Windows.Forms.Button _resetButton;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointStruct;
        private System.Windows.Forms.ToolStripMenuItem resetSetpointStruct;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releImpulseCol;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox _releDispepairBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckedListBox _dispepairCheckList;
        private System.Windows.Forms.GroupBox NewCopyGroupBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button copyBtn;
        private System.Windows.Forms.ComboBox copyGroupsCombo;
        private System.Windows.Forms.ComboBox currentGroupCombo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3BlockingNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense3SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense3OSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense3AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseBlockNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseParameterCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseFeatureCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseWorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseSpeedUpCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefenseSpeedupTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefenseOSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseUROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAPVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefenseAVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4NameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4ModeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4BlockNumberCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkConstraintCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4WorkTimeCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4SpeedupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tokDefense4SpeedupTimeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _tokDefense4OSCv11Col;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4UROVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4APVCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _tokDefense4AVRCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.Label _pereklLabel1;
        private System.Windows.Forms.ComboBox _gr4ComboBox;
        private System.Windows.Forms.ComboBox _gr3ComboBox;
        private System.Windows.Forms.ComboBox _gr2ComboBox;
        private System.Windows.Forms.ComboBox _gr1ComboBox;
        private System.Windows.Forms.Label _pereklLabel4;
        private System.Windows.Forms.Label _pereklLabel3;
        private System.Windows.Forms.Label _pereklLabel2;
    }
}