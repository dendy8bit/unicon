﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v50.Osc.Structures;

namespace BEMN.MR5.v50.Osc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoaderV50
    {
        #region Const
        private const ushort PAGE_SIZE = 1024;
        private const ushort FULL_OSC_SIZE_IN_PAGES = 496;
        private const int FULL_OSC_SIZE_IN_WORDS = 507900; //507888
        #endregion

        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<SetOscStartPageStruct> _setPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список страниц
        /// </summary>
        private List<ushort[]> _pagesWords;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStructV50 _journalStructV2;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        #endregion [Events]

        
        #region [Ctor's]
        public OscPageLoaderV50(MemoryEntity<SetOscStartPageStruct> setStartPage, MemoryEntity<OscPage> oscPage)
        {
            this._setPage = setStartPage;
            this._oscPage = oscPage;

            //Установка начальной страницы осциллограммы
            this._setPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscPage.LoadStruct);

            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
        } 
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStructV2">Запись журнала о осцилограмме</param>
        public void StartRead(OscJournalStructV50 journalStructV2)
        {
            this._needStop = false;
            this._journalStructV2 = journalStructV2;
            this._startPage = journalStructV2.OscStartIndex;

            this._startWordIndex = this._journalStructV2.Point % PAGE_SIZE;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStructV2.Len * this._journalStructV2.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex ;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / PAGE_SIZE);
            
            this._endPage = this._startPage + this._pageCount;
            this._pagesWords = new List<ushort[]>();//this._pages = new List<OscPage>();
            this.WritePageNumber((ushort) this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        } 
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if(this._pagesWords == null) return;
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();        
                return;
            }
            this._pagesWords.Add(this._startPage == FULL_OSC_SIZE_IN_PAGES - 1
                ? this._oscPage.Value.Words.Take(this._oscPage.Value.Words.Length - 4).ToArray()
                : this._oscPage.Value.Words);
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < FULL_OSC_SIZE_IN_PAGES)
                {
                    this.WritePageNumber((ushort) this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort) (this._startPage - FULL_OSC_SIZE_IN_PAGES));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();

        }
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            //Результирующий массив
            ushort[] resultMassiv = new ushort[this._resultLenInWords];
            ushort[] startPageValueArray = this._pagesWords[0];
            int destanationIndex = 0;
            //Копируем часть первой страницы
            Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
            destanationIndex += startPageValueArray.Length - this._startWordIndex;
            for (int i = 1; i < this._pagesWords.Count - 1; i++)
            {
                ushort[] pageValue = this._pagesWords[i];
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }
            ushort[] endPage = this._pagesWords[this._pagesWords.Count - 1];
            Array.ConstrainedCopy(endPage, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);

            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            int b = (this._journalStructV2.Len - this._journalStructV2.After)*this._journalStructV2.SizeReference;
                //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStructV2.Begin < this._journalStructV2.Point) // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStructV2.Begin + FULL_OSC_SIZE_IN_WORDS - this._journalStructV2.Point;
            }
            else //Если BEGIN больше POINT, то:
            {
                c = this._journalStructV2.Begin - this._journalStructV2.Point; //c = BEGIN – POINT
            }
            int start = c - b; //START = c – b
            if (start < 0) //Если START меньше 0, то:
            {
                start += this._journalStructV2.Len*this._journalStructV2.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
            //Перевёрнутый массив
            ushort[] invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            this.ResultArray = invertedMass;
        }

        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setPage.Value.PageIndex = pageNumber;
            this._setPage.SaveStruct6();
        } 
        #endregion [Help members]


        #region [Properties]

        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }

        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
