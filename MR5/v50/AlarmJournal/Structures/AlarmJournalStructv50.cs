﻿using BEMN.Devices;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;


namespace BEMN.MR5.v50.AlarmJournal.Structures
{
   public class AlarmJournalStructV50: StructBase
    {
        [Layout(0, Count = 32)] private AlarmJournalRecordStructV50[] _records;

        public AlarmJournalRecordStructV50[] Records
        {
            get { return this._records; }
        }

        public override object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            int arrayLength = 32;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + 28));
                start += 64;
            }
            return slots;
        }
    }
}
