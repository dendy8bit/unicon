namespace BEMN.MR5.v50.AlarmJournal
{
    partial class MR5v50AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._openAlarmJounralDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveSysJournalDlg = new System.Windows.Forms.SaveFileDialog();
            this._deserializeBut = new System.Windows.Forms.Button();
            this._serializeBut = new System.Windows.Forms.Button();
            this._readBut = new System.Windows.Forms.Button();
            this._journalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._valueParametr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Dcol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._journalProgress = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLable = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _openAlarmJounralDlg
            // 
            this._openAlarmJounralDlg.DefaultExt = "xml";
            this._openAlarmJounralDlg.Filter = "��5 v50 ��(*.xml)|*.xml|��5 v50 ��(*.bin)|*.bin";
            this._openAlarmJounralDlg.RestoreDirectory = true;
            this._openAlarmJounralDlg.Title = "������� ������  ������ ��� ��500";
            // 
            // _saveSysJournalDlg
            // 
            this._saveSysJournalDlg.DefaultExt = "xml";
            this._saveSysJournalDlg.FileName = "��5 V50 ������ ������";
            this._saveSysJournalDlg.Filter = "(*.xml) | *.xml";
            this._saveSysJournalDlg.Title = "���������  ������ ������ ��� ��5 v50";
            // 
            // _deserializeBut
            // 
            this._deserializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._deserializeBut.Location = new System.Drawing.Point(434, 439);
            this._deserializeBut.Name = "_deserializeBut";
            this._deserializeBut.Size = new System.Drawing.Size(126, 23);
            this._deserializeBut.TabIndex = 21;
            this._deserializeBut.Text = "��������� �� �����";
            this._deserializeBut.UseVisualStyleBackColor = true;
            this._deserializeBut.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _serializeBut
            // 
            this._serializeBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._serializeBut.Location = new System.Drawing.Point(566, 439);
            this._serializeBut.Name = "_serializeBut";
            this._serializeBut.Size = new System.Drawing.Size(126, 23);
            this._serializeBut.TabIndex = 20;
            this._serializeBut.Text = "��������� � ����";
            this._serializeBut.UseVisualStyleBackColor = true;
            this._serializeBut.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _readBut
            // 
            this._readBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readBut.Location = new System.Drawing.Point(2, 439);
            this._readBut.Name = "_readBut";
            this._readBut.Size = new System.Drawing.Size(102, 23);
            this._readBut.TabIndex = 19;
            this._readBut.Text = "���������";
            this._readBut.UseVisualStyleBackColor = true;
            this._readBut.Click += new System.EventHandler(this._readBut_Click);
            // 
            // _journalGrid
            // 
            this._journalGrid.AllowUserToAddRows = false;
            this._journalGrid.AllowUserToDeleteRows = false;
            this._journalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._journalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._journalGrid.BackgroundColor = System.Drawing.Color.White;
            this._journalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._journalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msgCol,
            this._stage,
            this._typeCol,
            this._codeCol,
            this._valueParametr,
            this._group,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I0Col,
            this._I1Col,
            this._I2Col,
            this._InCol,
            this._IgCol,
            this._Dcol});
            this._journalGrid.Location = new System.Drawing.Point(0, 0);
            this._journalGrid.Name = "_journalGrid";
            this._journalGrid.RowHeadersVisible = false;
            this._journalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._journalGrid.Size = new System.Drawing.Size(826, 418);
            this._journalGrid.TabIndex = 18;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "�";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "�";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "�����";
            this._timeCol.HeaderText = "�����";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 46;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this._msgCol.DataPropertyName = "���������";
            this._msgCol.HeaderText = "���������";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 71;
            // 
            // _stage
            // 
            this._stage.DataPropertyName = "�������";
            this._stage.HeaderText = "�������";
            this._stage.Name = "_stage";
            this._stage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._stage.Width = 54;
            // 
            // _typeCol
            // 
            this._typeCol.DataPropertyName = "��� �����������";
            this._typeCol.HeaderText = "��� �����������";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._typeCol.Width = 93;
            // 
            // _codeCol
            // 
            this._codeCol.DataPropertyName = "�������� ������������";
            this._codeCol.HeaderText = "�������� ������������";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._codeCol.Width = 126;
            // 
            // _valueParametr
            // 
            this._valueParametr.DataPropertyName = "�������� ��������� ������������";
            this._valueParametr.HeaderText = "�������� ��������� ������������";
            this._valueParametr.Name = "_valueParametr";
            this._valueParametr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._valueParametr.Width = 176;
            // 
            // _group
            // 
            this._group.DataPropertyName = "������ �������";
            this._group.HeaderText = "������ �������";
            this._group.Name = "_group";
            this._group.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._group.Width = 82;
            // 
            // _IaCol
            // 
            this._IaCol.DataPropertyName = "Ia{A}";
            this._IaCol.HeaderText = "Ia{A}";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 37;
            // 
            // _IbCol
            // 
            this._IbCol.DataPropertyName = "Ib{A}";
            this._IbCol.HeaderText = "Ib{A}";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 37;
            // 
            // _IcCol
            // 
            this._IcCol.DataPropertyName = "Ic{A}";
            this._IcCol.HeaderText = "Ic{A}";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 37;
            // 
            // _I0Col
            // 
            this._I0Col.DataPropertyName = "I0{A}";
            this._I0Col.HeaderText = "I0{A}";
            this._I0Col.Name = "_I0Col";
            this._I0Col.ReadOnly = true;
            this._I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I0Col.Width = 37;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "I1{A}";
            this._I1Col.HeaderText = "I1{A}";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 37;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "I2{A}";
            this._I2Col.HeaderText = "I2{A}";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 37;
            // 
            // _InCol
            // 
            this._InCol.DataPropertyName = "In{A}";
            this._InCol.HeaderText = "In{A}";
            this._InCol.Name = "_InCol";
            this._InCol.ReadOnly = true;
            this._InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._InCol.Width = 37;
            // 
            // _IgCol
            // 
            this._IgCol.DataPropertyName = "Ig{A}";
            this._IgCol.HeaderText = "Ig{A}";
            this._IgCol.Name = "_IgCol";
            this._IgCol.ReadOnly = true;
            this._IgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IgCol.Width = 37;
            // 
            // _Dcol
            // 
            this._Dcol.DataPropertyName = "������� ������� 1-16";
            this._Dcol.HeaderText = "������� ������� 1-16";
            this._Dcol.Name = "_Dcol";
            this._Dcol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Dcol.Width = 95;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._journalProgress,
            this._statusLable});
            this.statusStrip1.Location = new System.Drawing.Point(0, 472);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(826, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _journalProgress
            // 
            this._journalProgress.Name = "_journalProgress";
            this._journalProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // _statusLable
            // 
            this._statusLable.Name = "_statusLable";
            this._statusLable.Size = new System.Drawing.Size(108, 17);
            this._statusLable.Text = "������ � �������";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(698, 439);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(126, 23);
            this._exportButton.TabIndex = 24;
            this._exportButton.Text = "��������� � HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.Filter = "������ ������ ��5 v50 | *.html";
            this._saveJournalHtmlDialog.Title = "���������  ������ ������ ��� ��5 v50";
            // 
            // MR5v50AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 494);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._deserializeBut);
            this.Controls.Add(this._serializeBut);
            this.Controls.Add(this._readBut);
            this.Controls.Add(this._journalGrid);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(842, 533);
            this.Name = "MR5v50AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.Load += new System.EventHandler(this.MR5v50AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._journalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openAlarmJounralDlg;
        private System.Windows.Forms.SaveFileDialog _saveSysJournalDlg;
        private System.Windows.Forms.Button _deserializeBut;
        private System.Windows.Forms.Button _serializeBut;
        private System.Windows.Forms.Button _readBut;
        private System.Windows.Forms.DataGridView _journalGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _journalProgress;
        private System.Windows.Forms.ToolStripStatusLabel _statusLable;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stage;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _valueParametr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _group;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Dcol;
    }
}