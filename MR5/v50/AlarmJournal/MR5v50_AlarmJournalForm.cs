using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v50.AlarmJournal.Structures;
using BEMN.MR5.v50.Configuration.Structures.Transformer;

namespace BEMN.MR5.v50.AlarmJournal
{
    public partial class MR5v50AlarmJournalForm  : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "������ � ������� - {0}";
        private const string READ_AJ_FAIL = "���������� ��������� ������ ������";
        private const string READ_AJ = "������ ������� ������";
        private const string TABLE_NAME = "��5_V50_������_������";
        private const string JOURNAL_IS_EMPTY = "������ ����";
        private const string JOURNAL_SAVED = "������ �������� {0}";
        private const string PARAMETERS_LOADED = "��������� ���������";
        private const string FAULT_LOAD_PARAMETERS = "���������� ��������� ���������";
        private const string RECORDS_IN_JOURNAL_PATTERN = "� ������� {0} ���������";
        private const string ALARM_JOURNAL_FILE_NAME = "������ ������ ��5 ������ {0}";
        #endregion [Constants]

        #region [Private fields]

        private DataTable _table;
        private readonly MemoryEntity<AlarmJournalStructV50> _alarmRecord;
        private readonly MemoryEntity<MeasuringTransformerV50> _measuringChannel;
        private MR5Device _device;
        #endregion [Private fields]

        #region [Ctor's]
        public MR5v50AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public MR5v50AlarmJournalForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmRecord = device.AlarmJurnalV50;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            this._alarmRecord.ReadOk +=HandlerHelper.CreateHandler(this, this._journalProgress.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.MeasuringAlarmV50;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);
            this._table = this.GetJournalDataTable();
            this._journalGrid.DataSource = this._table;
            this._journalProgress.Maximum = this._alarmRecord.Slots.Count;
        }
        #endregion

        #region [Help members]
        private void ReadJournalOk()
        {
            int i = 1;
            var measure = this._measuringChannel.Value;
            var version = Common.VersionConverter(this._device.DeviceVersion);
            foreach (var record in this._alarmRecord.Value.Records)
            {
                if (record.IsEmpty)
                {
                    continue;
                }

                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Stage,
                        record.TypeDamage,
                        record.WorkParametr,
                        record.ValueParametr(measure),
                        record.GroupOfSetpoints(version),
                        ValuesConverterCommon.Analog.GetISmall(record.Ia, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ib, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.Ic, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I0, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I1, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.I2, measure.TT * 40),
                        ValuesConverterCommon.Analog.GetISmall(record.In, measure.TTNP * 5),
                        ValuesConverterCommon.Analog.GetISmall(record.Ig, measure.TTNP * 5),
                        record.Discret
                    );
                i++;
            }
            this._journalGrid.DataSource = this._table;
            this._statusLable.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this._journalGrid.Update();
            this.Reading = false;
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._journalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._journalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        private void JournalReadFail()
        {
            this._statusLable.Text = READ_AJ_FAIL;
            this.Reading = false;
        }

        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
            this.Reading = false;
        }

        #endregion

        #region [Event Handlers]
        private bool Reading
        {
            set
            {
                this._readBut.Enabled = !value;
                this._serializeBut.Enabled = !value;
                this._deserializeBut.Enabled = !value;
                this._exportButton.Enabled = !value;
            }
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._journalProgress.Value = 0;
            this._statusLable.Text = READ_AJ;
            this.Reading = true;
            this._measuringChannel.LoadStruct();
        }

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this._openAlarmJounralDlg.FileName = string.Format(ALARM_JOURNAL_FILE_NAME, this._device.DeviceVersion);
            if (this._openAlarmJounralDlg.ShowDialog() != DialogResult.OK) return;
            this._table.Clear();
            if (Path.GetExtension(this._openAlarmJounralDlg.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJounralDlg.FileName);
                AlarmJournalStructV50 journal = new AlarmJournalStructV50();
                MeasuringTransformerV50 measuring = new MeasuringTransformerV50();
                int measSize = measuring.GetSize();
                int journalSize = journal.GetSize();
                byte[] buffer = file.Take(file.Length - measSize).ToArray();
                List<byte> journalBytes = new List<byte>(Common.SwapArrayItems(buffer));
                if (journalBytes.Count != journalSize)
                {
                    journalBytes.AddRange(new byte[journalSize - journalBytes.Count]);
                }
                journal.InitStruct(journalBytes.ToArray());
                this._alarmRecord.Value = journal;

                buffer = file.Skip(file.Length - measSize).ToArray();
                List<byte> measuringBytes = new List<byte>(Common.SwapArrayItems(buffer));
                if (measuringBytes.Count != measSize)
                {
                    measuringBytes.AddRange(new byte[measSize - measuringBytes.Count]);
                }
                measuring.InitStruct(measuringBytes.ToArray());
                this._measuringChannel.Value = measuring;
                this.ReadJournalOk();
            }
            else
            {
                this._table.ReadXml(this._openAlarmJounralDlg.FileName);
                this._statusLable.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            this._saveSysJournalDlg.FileName = string.Format(ALARM_JOURNAL_FILE_NAME, this._device.DeviceVersion);
            if (this._saveSysJournalDlg.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveSysJournalDlg.FileName);
                this._statusLable.Text = string.Format(JOURNAL_SAVED, this._saveSysJournalDlg.FileName);
            }
        }

        private void SaveToHtml()
        {
            this._saveJournalHtmlDialog.FileName = string.Format(ALARM_JOURNAL_FILE_NAME, this._device.DeviceVersion);
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR5_50AJ);
                this._statusLable.Text = string.Format(JOURNAL_SAVED, this._saveJournalHtmlDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void MR5v50AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this.StartRead();
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(MR5v50AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return BEMN.MR5.Properties.Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }




        #endregion

    }
}
