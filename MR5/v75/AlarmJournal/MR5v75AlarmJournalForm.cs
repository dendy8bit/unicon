﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.Properties;
using BEMN.MR5.v75.AlarmJournal.Structures;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v75.AlarmJournal
{
    public partial class MR5v75AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string DISCRET16 = "Входные сигналы 1. . .16";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР5V75_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string PARAMETERS_LOADED = "Параметры загружены";
        private const string FAULT_LOAD_PARAMETERS = "Невозможно загрузить параметры";
        private const string RECORDS_IN_JOURNAL_PATTERN = "В журнале {0} сообщений";
        private const string ALARM_JOURNAL_FILE_NAME = "Журнал аварий МР5 версия {0}";
        #endregion [Constants]


        #region [Private fields]
        private DataTable _table;
        private readonly MemoryEntity<AlarmJournalStruct75> _alarmRecord;
        private readonly MemoryEntity<MeasureTransStruct75> _measuringChannel;
        private MR5Device _device;
        #endregion [Private fields]


        #region [Ctor's]
        public MR5v75AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public MR5v75AlarmJournalForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._alarmRecord = device.AlarmRecord75;
            this._alarmRecord.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadJournalOk);
            this._alarmRecord.ReadOk += HandlerHelper.CreateHandler(this, this._configProgressBar.PerformStep);
            this._alarmRecord.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.JournalReadFail);

            this._measuringChannel = device.MeasureTransAj75;
            this._measuringChannel.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoad);
            this._measuringChannel.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasuringChannelLoadFail);

            if (Common.VersionConverter(this._device.DeviceVersion) > 75)
            {
                this._D0Col.HeaderText = DISCRET16;
            }
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            this._configProgressBar.Maximum = this._alarmRecord.Slots.Count;
        } 
        #endregion [Ctor's]


        #region [Help members]

        private void MeasuringChannelLoad()
        {
            MessageBox.Show(PARAMETERS_LOADED);
            this._alarmRecord.LoadStruct();
        }
        private void MeasuringChannelLoadFail()
        {
            MessageBox.Show(FAULT_LOAD_PARAMETERS);
            this.Reading = false;
        }
        private void JournalReadFail()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.Reading = false;
        }

        private void ReadJournalOk()
        {
            int i = 1;
            MeasureTransStruct75 measure = this._measuringChannel.Value;
            double version = Common.VersionConverter(this._device.DeviceVersion);
            foreach (AlarmJournalRecordStruct record in this._alarmRecord.Value.Records.Where(record => !record.IsEmpty))
            {
                this._table.Rows.Add
                    (
                        i,
                        record.GetTime,
                        record.Message,
                        record.Stage,
                        record.TypeDamage,
                        record.WorkParametr,
                        record.ValueParametr(measure),
                        record.GroupOfSetpoints(version),
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.Tt*40),
                        ValuesConverterCommon.Analog.GetI(record.In, measure.Ttnp*5),
                        ValuesConverterCommon.Analog.GetI(record.Ig, measure.Ttnp*5),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.Tn),
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.Tnnp),
                        Common.VersionConverter(this._device.DeviceVersion) > 75 ? record.Discret16 : record.Discret8
                    );
                i++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            this.Reading = false;
        }

        private DataTable GetJournalDataTable()
         {
            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(j == this._alarmJournalGrid.Columns.Count - 1
                    ? this._alarmJournalGrid.Columns[j].Name
                    : this._alarmJournalGrid.Columns[j].HeaderText);
            }
            return table;
        }
        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MR5v75AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
          this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._configProgressBar.Value = 0;
            this._statusLabel.Text = READ_AJ;
            this.Reading = true;
            if (this._device.MB.NetworkEnabled)
            {
                this._measuringChannel.LoadStruct(new TimeSpan(50));
            }
            else
            {
                this._measuringChannel.LoadStruct();
            }
        }


        private bool Reading
        {
            set
            {
                this._readAlarmJournalButton.Enabled = !value;
                this._saveAlarmJournalButton.Enabled = !value;
                this._exportButton.Enabled = !value;
                this._loadAlarmJournalButton.Enabled =! value;
            }
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this._openAlarmJournalDialog.FileName = string.Format(ALARM_JOURNAL_FILE_NAME, this._device.DeviceVersion);
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            this._table.Clear();
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalStruct75 journal = new AlarmJournalStruct75();
                MeasureTransStruct75 measuring = new MeasureTransStruct75();
                int measSize = measuring.GetSize();
                int journalSize = journal.GetSize();
                byte[] buffer = file.Take(file.Length - measSize).ToArray();
                List<byte> journalBytes = new List<byte>(Common.SwapArrayItems(buffer));
                if (journalBytes.Count != journalSize)
                {
                    journalBytes.AddRange(new byte[journalSize - journalBytes.Count]);
                }
                journal.InitStruct(journalBytes.ToArray());
                this._alarmRecord.Value = journal;

                buffer = file.Skip(file.Length - measSize).ToArray();
                List<byte> measuringBytes = new List<byte>(Common.SwapArrayItems(buffer));
                if (measuringBytes.Count != measSize)
                {
                    measuringBytes.AddRange(new byte[measSize - measuringBytes.Count]);
                }
                measuring.InitStruct(measuringBytes.ToArray());
                this._measuringChannel.Value = measuring;
                this.ReadJournalOk();
            }
            else
            {
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL_PATTERN, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            this._saveAlarmJournalDialog.FileName = string.Format(ALARM_JOURNAL_FILE_NAME, this._device.DeviceVersion);
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        #endregion [Event Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR5_75AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void MR5v75AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._alarmRecord?.RemoveStructQueries();
            this._measuringChannel?.RemoveStructQueries();
        }
    }
}
