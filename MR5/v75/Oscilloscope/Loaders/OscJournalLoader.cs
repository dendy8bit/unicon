﻿using System;
using System.Collections.Generic;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v75.Oscilloscope.Structures;

namespace BEMN.MR5.v75.Oscilloscope.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructV75> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructV75> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        private bool _isNetworkConected;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        /// <summary>
        /// Весь журнал успешно прочитан
        /// </summary>
        public event Action AllJournalReadOk;

        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        public OscJournalLoader(MR5Device device)
        {
            this._oscRecords = new List<OscJournalStructV75>();
            this._isNetworkConected = device.MB.NetworkEnabled;
            //Записи журнала
            this._oscJournal = device.OscJournal75;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = device.RefreshOscJournalV75;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructV75> OscRecords
        {
            get { return this._oscRecords; }
        }
        
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void UpdateNumber()
        {
            this._refreshOscJournal.Value.Word = (ushort) this._recordNumber;
            if (this._isNetworkConected)
            {
                this._refreshOscJournal.SaveStruct6(new TimeSpan(50));
            }
            else
            {
                this._refreshOscJournal.SaveStruct6();
            }
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructV75.RecordIndex = this.RecordNumber;
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructV75>());
                this._recordNumber = this.RecordNumber + 1;
                this.UpdateNumber();
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk != null)
                    this.AllJournalReadOk.Invoke();
            }
        } 
        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
           this._recordNumber = 0;
           this.UpdateNumber();
        } 
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }

        public void Close()
        {
            this._oscJournal.RemoveStructQueries();
            this._refreshOscJournal.RemoveStructQueries();
        }
    }
}
