﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR5.v75.Oscilloscope.Structures;
using System;
using System.Collections.Generic;
using System.Threading;
using BEMN.MBServer;

namespace BEMN.MR5.v75.Oscilloscope.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoader
    {
        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список страниц
        /// </summary>
        private List<OscPage> _pages;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStructV75 _journalStruct;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;

        private ModbusType _mbType;
        private int _receiveTime;
        private bool _isConnectedTCP;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;

        private readonly MemoryEntityOperationComplite _opCompleteDelegate1;
        private readonly MemoryEntityOperationComplite _opCompleteDelegate2;
        #endregion [Events]


        #region [Ctor's]
        public OscPageLoader(MR5Device device)
        {
            this._mbType = device.MB.ModbusType;
            this._receiveTime = device.MB.ReceiveTimeout;

            this._setStartPage = device.SetStartPage75;
            this._oscPage = device.OscPage75;

            this._isConnectedTCP = device.MB.NetworkEnabled;

            //Установка начальной страницы осциллограммы
            this._opCompleteDelegate1 = HandlerHelper.CreateReadArrayHandler(this.LoadPage);
            this._setStartPage.AllWriteOk += this._opCompleteDelegate1;
            //Страницы осциллограммы
            this._opCompleteDelegate2 = HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
            this._oscPage.AllReadOk += this._opCompleteDelegate2;
        }

        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        public void StartRead(OscJournalStructV75 journalStruct)
        {
            this._needStop = false;
            this._journalStruct = journalStruct;
            this._startPage = journalStruct.OscStartIndex;

            this._startWordIndex = this._journalStruct.Point % this._journalStruct.PageSize;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStruct.Len * this._journalStruct.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex;
            //Количество страниц которые нужно прочитать
            this.PagesCount = (int)Math.Ceiling(lenOscInWords / this._journalStruct.PageSize);

            this._endPage = this._startPage + this.PagesCount;
            this._pages = new List<OscPage>();
            this.WritePageNumber((ushort)this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        }
        #endregion [Event Raise Members]


        #region [Help members]

        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if (this._pages == null) return;
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();
                return;
            }
            OscPage page = this._oscPage.Value.Clone<OscPage>();
            this._pages.Add(page);
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < this._journalStruct.FullOscSizeInPages)
                {
                    this.WritePageNumber((ushort)this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort)(this._startPage - this._journalStruct.FullOscSizeInPages));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();
        }
        
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            //Результирующий массив
            ushort[] resultMassiv = new ushort[this._resultLenInWords];
            //Если осцилограмма начинается с начала страницы
            ushort[] startPageValueArray = this._pages[0].Words;
            int destanationIndex = 0;
            //Копируем часть первой страницы
            Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
            destanationIndex += startPageValueArray.Length - this._startWordIndex;
            for (int i = 1; i < this._pages.Count - 1; i++)
            {
                ushort[] pageValue = this._pages[i].Words;
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }
            OscPage endPage = this._pages[this._pages.Count - 1];
            Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);
            
            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            int b = (this._journalStruct.Len - this._journalStruct.After) * this._journalStruct.SizeReference;
            //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStruct.Begin < this._journalStruct.Point) // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStruct.Begin + this._journalStruct.LoadedFullOscSizeInWords - this._journalStruct.Point;
            }
            else //Если BEGIN больше POINT, то:
            {
                c = this._journalStruct.Begin - this._journalStruct.Point; //c = BEGIN – POINT
            }
            int start = c - b; //START = c – b
            if (start < 0) //Если START меньше 0, то:
            {
                start += this._journalStruct.Len * this._journalStruct.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//

            //Перевёрнутый массив
            ushort[] invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            this.ResultArray = invertedMass;
        }

        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setStartPage.Value.Word = pageNumber;
            if (this._isConnectedTCP)
            {
                this._setStartPage.SaveStruct6(new TimeSpan(50));
            }
            else
            {
                this._setStartPage.SaveStruct6();
            }
        }

        private void LoadPage()
        {
            if (this._mbType == ModbusType.ModbusOnTcp)
            {
                Thread.Sleep(this._receiveTime/10);
            }
            this._oscPage.LoadStruct();
        }
        #endregion [Help members]


        #region [Properties]
        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount { get; private set; }
        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]

        public void Close()
        {
            this._oscPage.RemoveStructQueries();
            this._setStartPage.RemoveStructQueries();
            //ОТПИСКА ОТ МЕТОДОВ
            this._setStartPage.AllWriteOk -= this._opCompleteDelegate1;
            this._oscPage.AllReadOk -= this._opCompleteDelegate2;
        }
    }
}
