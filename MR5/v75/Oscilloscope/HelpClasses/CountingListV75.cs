using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v75.Oscilloscope.Structures;

namespace BEMN.MR5.v75.Oscilloscope.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingListV75
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 16;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 128;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 4;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 4;

        public static readonly string[] INames = new[] { "Ia", "Ib", "Ic", "In" };
        public static readonly string[] UNames = new[] { "Uab", "Ubc", "Uca", "Un" };
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        private OscJournalStructV75 _oscJournalStruct;
        private double _deviceVersion;
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 1-16
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private readonly short[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]


        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }  
        #endregion [����������]
        
        #endregion [Private fields]

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }


        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; }
        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[] VoltageKoefs { get; }

        #region [Ctor's]
        public CountingListV75(ushort[] pageValue, OscJournalStructV75 oscJournalStruct, MeasureTransStruct75 measure, double version)
        {
            this._deviceVersion = version;
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();
                currentsKoefs[i] = (i == 3 ? measure.Ttnp * 5.0 : measure.Tt * 40.0) * Math.Sqrt(2.0) / 32768.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this._maxI, this._currents[i].Max());
                this._minI = Math.Min(this._minI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            double[] voltageKoefs = new double[VOLTAGES_COUNT];
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + CURRENTS_COUNT].Select(v => (short)(v - 32768)).ToArray();
                voltageKoefs[i] = (i == 3 ? measure.Tnnp : measure.Tn) * 256.0 * Math.Sqrt(2.0) / 32768.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i] * a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;

            ushort[][] d1To16 = this.DiscretArrayInit(this._countingArray[8]);
            ushort[][] d17To32 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d33To48 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d49To64 = this.DiscretArrayInit(this._countingArray[11]);
            ushort[][] d65To80 = this.DiscretArrayInit(this._countingArray[12]);
            ushort[][] d81To96 = this.DiscretArrayInit(this._countingArray[13]);
            ushort[][] d97To112 = this.DiscretArrayInit(this._countingArray[14]);
            ushort[][] d113To128 = this.DiscretArrayInit(this._countingArray[15]);
            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To16);
            dicrets.AddRange(d17To32);
            dicrets.AddRange(d33To48);
            dicrets.AddRange(d49To64);
            dicrets.AddRange(d65To80);
            dicrets.AddRange(d81To96);
            dicrets.AddRange(d97To112);
            dicrets.AddRange(d113To128);
            this._discrets = dicrets.ToArray();
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param pinName="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]

        public void Save(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException();
            }
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("��5 �75 {0} {1} ������� - {2}", this._oscJournalStruct.GetDate,
                    this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine(1251);
            }
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            Encoding encoding = Encoding.GetEncoding(1251);
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, encoding))
            {
                cgfFile.WriteLine("MP5,1");
                cgfFile.WriteLine("136,8A,128D");
                int index = 1;
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, INames[i], this.CurrentsKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Voltages.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};

                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, UNames[i], this.VoltageKoefs[i].ToString(format));
                    index++;
                }
                List<string> oscSignals = this._deviceVersion > 75
                    ? StringsConfigV75.OscSignals75_01
                    : StringsConfigV75.OscSignals75;
                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},{1},0", index, oscSignals[i]);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._count);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i*1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (short[] voltage in this._baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingListV75 Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string[] buff = hdrStrings[0].Split(' ');
            string timeString = string.Format("{0} {1}", buff[2], buff[3]);
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath,Encoding.GetEncoding(1251));
            double[,] factors = new double[VOLTAGES_COUNT + CURRENTS_COUNT,2];

            Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,\,\,[AV]\,(?<valueA>[0-9\.-]+)\,(?<valueB>[0-9\.-]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["valueA"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2, 0] = double.Parse(res, format);
                res = factorRegex.Match(cfgStrings[i]).Groups["valueB"].Value;
                factors[i - 2, 1] = double.Parse(res, format);
            }

            int counts = int.Parse(cfgStrings[4 + CURRENTS_COUNT + VOLTAGES_COUNT + DISCRETS_COUNT]
                .Replace("1000,", string.Empty));

            CountingListV75 result = new CountingListV75(counts) {_alarm = alarm};
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            
            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    voltages[j][i] = Convert.ToDouble(means[j + 2 + CURRENTS_COUNT], CultureInfo.InvariantCulture) * factors[j + CURRENTS_COUNT, 0] + factors[j + CURRENTS_COUNT, 1];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + CURRENTS_COUNT + VOLTAGES_COUNT]);
                }
            }

            result._maxI = double.MinValue;
            result._minI = double.MaxValue;
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());                
            }
            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result._maxU, voltages[i].Max());
                result._minU = Math.Min(result._minU, voltages[i].Min());
            }

            result.Currents = currents;
            result.Discrets = discrets;
            result.Voltages = voltages;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;
            return result;
        }

        private CountingListV75(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];

            this._count = count;
        }

    }
}
