﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v75.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v75.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v75.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR5.v75.Configuration.StructuresNew
{
    public class GroupSetpoint : StructBase
    {
        [Layout(0)] private CornerStruct _corners;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses;
        [Layout(2)] private AllAddCurrentDefensesStruct _addCurrentDefenses;
        [Layout(3)] private AllFrequencyDefensesStruct _frequencyDefenses;
        [Layout(4)] private AllVoltageDefensesStruct _voltageDefenses;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornerStruct Corners
        {
            get { return this._corners; }
            set { this._corners = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Основные_токовые")]
        public AllCurrentDefensesStruct CurrentDefenses
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Токовые_дополнительные")]
        public AllAddCurrentDefensesStruct AddCurrentDefenses
        {
            get { return this._addCurrentDefenses; }
            set { this._addCurrentDefenses = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Частотные")]
        public AllFrequencyDefensesStruct FrequencyDefenses
        {
            get { return this._frequencyDefenses; }
            set { this._frequencyDefenses = value; }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Напряжения")]
        public AllVoltageDefensesStruct VoltageDefenses
        {
            get { return this._voltageDefenses; }
            set { this._voltageDefenses = value; }
        }
    }
}
