﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v75.Configuration.Structures.Apv;
using BEMN.MR5.v75.Configuration.Structures.Avr;
using BEMN.MR5.v75.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v75.Configuration.Structures.FaultSignal;
using BEMN.MR5.v75.Configuration.Structures.Indicators;
using BEMN.MR5.v75.Configuration.Structures.Keys;
using BEMN.MR5.v75.Configuration.Structures.Ls;
using BEMN.MR5.v75.Configuration.Structures.Lzsh;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v75.Configuration.Structures.OscConfig;
using BEMN.MR5.v75.Configuration.Structures.Relay;
using BEMN.MR5.v75.Configuration.Structures.Switch;
using BEMN.MR5.v75.Configuration.Structures.Vls;

namespace BEMN.MR5.v75.Configuration.StructuresNew
{
    [XmlRoot(ElementName = "МР5")]
    public class ConfigurationStructV75new : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР5"; } set { } }
        
        [XmlElement(ElementName = "Номер_группы")]
        public int Group { get; set; }

        [Layout(0)] private MeasureTransStruct75 _measureTrans;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private ExternalSignalsNew _externalSignal;
        [Layout(3)] private FaultStruct _fault;
        [Layout(4, Ignore = true)] private ushort _portInterface;
        [Layout(5)] private InputLogicSignalStruct _inputLogicSignal;
        [Layout(6)] private SwitchStruct _switch;
        [Layout(7)] private ApvStruct _apv;
        [Layout(8)] private AvrStruct _avr;
        [Layout(9)] private LpbStruct _lzsh;
        [Layout(10)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(11)] private AllDefensesSetpoint _allDefensesSetpoint;
        [Layout(12)] private OutputLogicSignalStruct _vls;
        [Layout(13)] private AllReleOutputStruct _reley;
        [Layout(14)] private AllIndicatorsStruct _indicators;
        [Layout(15, Count = 4, Ignore = true)] private ushort[] _resSystem;
        [Layout(16)] private OscConfigStruct _oscConfig;

        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(0)]
        public MeasureTransStruct75 MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }
        [XmlElement(ElementName = "Внешние_сигналы")]
        [BindingProperty(1)]
        public ExternalSignalsNew ExternalSignal
        {
            get { return this._externalSignal; }
            set { this._externalSignal = value; }
        }
        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(2)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        [BindingProperty(3)]
        public InputLogicSignalStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(4)]
        public SwitchStruct Switch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }
        [XmlElement(ElementName = "АПВ")]
        [BindingProperty(5)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }
        [XmlElement(ElementName = "АВР")]
        [BindingProperty(6)]
        public AvrStruct Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }
        [XmlElement(ElementName = "ЛЗШ")]
        [BindingProperty(7)]
        public LpbStruct Lzsh
        {
            get { return this._lzsh; }
            set { this._lzsh = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Все_группы_защит")]
        public AllDefensesSetpoint AllDefensesSetpoint
        {
            get { return this._allDefensesSetpoint; }
            set { this._allDefensesSetpoint = value; }
        }

        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(10)]
        public OutputLogicSignalStruct Vls
        {
            get { return this._vls; }
            set { this._vls = value; }
        }

        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(11)]
        public AllIndicatorsStruct Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        [XmlElement(ElementName = "Осц")]
        [BindingProperty(12)]
        public OscConfigStruct OscConfig
        {
            get { return this._oscConfig; }
            set { this._oscConfig = value; }
        }

        [XmlElement(ElementName = "Ключи")]
        [BindingProperty(13)]
        public KeysStruct Keys
        {
            get { return this._keys; }
            set { this._keys = value; }
        }
    }
}
