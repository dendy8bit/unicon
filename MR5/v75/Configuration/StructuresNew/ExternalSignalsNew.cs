﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v75.Configuration.StructuresNew
{
    public class ExternalSignalsNew : StructBase
    {
        [Layout(0)] private ushort _keyOn;
        [Layout(1)] private ushort _keyOff;
        [Layout(2)] private ushort _extOn;
        [Layout(3)] private ushort _extOff;
        [Layout(4)] private ushort _inpClear;
        [Layout(5)] private ushort _inpGroup1; // вход переключения на группу уставок 1
        [Layout(6)] private ushort _uca;
        [Layout(7)] private ushort _f;
        [Layout(8)] private ushort _side1;
        [Layout(9)] private ushort _side2;
        [Layout(10)] private ushort _noUacc;
        [Layout(11)] private ushort _disable; //вход запрет команд управления по СДТУ
        [Layout(12)] private ushort _inpGroup2;     //вход переключения на группу уставок 2

        [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOff
        {
            get { return Validator.Get(this._keyOff, StringsConfigV75.LogicSignals); }
            set { this._keyOff = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ключ_ВКЛ")]
        public string KeyOn
        {
            get { return Validator.Get(this._keyOn, StringsConfigV75.LogicSignals); }
            set { this._keyOn = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string ExtOff
        {
            get { return Validator.Get(this._extOff, StringsConfigV75.LogicSignals); }
            set { this._extOff = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_внеш_включить")]
        public string ExtOn
        {
            get { return Validator.Get(this._extOn, StringsConfigV75.LogicSignals); }
            set { this._extOn = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "сброс_сигнализации")]
        public string InpClear
        {
            get { return Validator.Get(this._inpClear, StringsConfigV75.LogicSignals); }
            set { this._inpClear = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "группа_уставок_1")]
        public string InpGroup
        {
            get { return Validator.Get(this._inpGroup1, StringsConfigV75.LogicSignals); }
            set { this._inpGroup1 = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        
        [BindingProperty(6)]
        [XmlElement(ElementName = "Uca")]
        public string Uca
        {
            get { return Validator.Get(this._uca, StringsConfigV75.Uca); }
            set { this._uca = Validator.Set(value, StringsConfigV75.Uca); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "F")]
        public string F
        {
            get { return Validator.Get(this._f, StringsConfigV75.F); }
            set { this._f = Validator.Set(value, StringsConfigV75.F); }
        }
        

        [BindingProperty(8)]
        [XmlElement(ElementName = "Сторона1")]
        public string Side1
        {
            get { return Validator.Get(this._side1, StringsConfigV75.Side, 0, 1); }
            set { this._side1 = Validator.Set(value, StringsConfigV75.Side, this._side1, 0, 1); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Сторона2")]
        public string Side2
        {
            get { return Validator.Get(this._side2, StringsConfigV75.Side, 0, 1); }
            set { this._side2 = Validator.Set(value, StringsConfigV75.Side, this._side2, 0, 1); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Низкий заряд на аккумуляторе")]
        public double NoUacc
        {
            get { return ValuesConverterCommon.GetU(this._noUacc); }
            set { this._noUacc = ValuesConverterCommon.SetU(value); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Запрет команд по СДТУ")]
        public string Disable
        {
            get { return Validator.Get(this._disable, StringsConfigV75.LogicSignals); }
            set { this._disable = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }
        
        [BindingProperty(12)]
        [XmlElement(ElementName = "группа_уставок_2")]
        public string InpGroup2
        {
            get { return Validator.Get(this._inpGroup2, StringsConfigV75.LogicSignals); }
            set { this._inpGroup2 = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }
    }
}
