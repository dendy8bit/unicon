﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.MR5.v75.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v75.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v75.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR5.v75.Configuration.StructuresNew
{
    // то, как располагаются структуры в устройстве
    public class AllDefensesSetpoint : StructBase, ISetpointContainer<GroupSetpoint> // GroupSetpoint - структура уставок для одной группы
    {
        // Группы 1 и 2
        [Layout(0)] private CornerStruct _corner1;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefense1;
        [Layout(2)] private CornerStruct _corner2;
        [Layout(3)] private AllCurrentDefensesStruct _currentDefense2;
        [Layout(4)] private AllAddCurrentDefensesStruct _addCurrentDefenses1;
        [Layout(5, Count = 16)] private ushort[] _res1;
        [Layout(6)] private AllAddCurrentDefensesStruct _addCurrentDefenses2;
        [Layout(7, Count = 16)] private ushort[] _res2;
        [Layout(8)] private AllFrequencyDefensesStruct _frequencyDefenses1;
        [Layout(9)] private AllFrequencyDefensesStruct _frequencyDefenses2;
        [Layout(10)] private AllVoltageDefensesStruct _voltageDefenses1;
        [Layout(11)] private AllVoltageDefensesStruct _voltageDefenses2;
        // Группы 3 и 4        
        [Layout(12)] private CornerStruct _corner3;
        [Layout(13)] private AllCurrentDefensesStruct _currentDefense3;
        [Layout(14)] private CornerStruct _corner4;
        [Layout(15)] private AllCurrentDefensesStruct _currentDefense4;
        [Layout(16)] private AllAddCurrentDefensesStruct _addCurrentDefenses3;
        [Layout(17, Count = 16)] private ushort[] _res3;
        [Layout(18)] private AllAddCurrentDefensesStruct _addCurrentDefenses4;
        [Layout(19, Count = 16)] private ushort[] _res4;
        [Layout(20)] private AllFrequencyDefensesStruct _frequencyDefenses3;
        [Layout(21)] private AllFrequencyDefensesStruct _frequencyDefenses4;
        [Layout(22)] private AllVoltageDefensesStruct _voltageDefenses3;
        [Layout(23)] private AllVoltageDefensesStruct _voltageDefenses4;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint[] Setpoints
        {
            get
            {
                return new []
                {
                    this.GetGroupsetpoint(this._corner1, this._currentDefense1, this._addCurrentDefenses1, this._frequencyDefenses1, this._voltageDefenses1),
                    this.GetGroupsetpoint(this._corner2, this._currentDefense2, this._addCurrentDefenses2, this._frequencyDefenses2, this._voltageDefenses2),
                    this.GetGroupsetpoint(this._corner3, this._currentDefense3, this._addCurrentDefenses3, this._frequencyDefenses3, this._voltageDefenses3),
                    this.GetGroupsetpoint(this._corner4, this._currentDefense4, this._addCurrentDefenses4, this._frequencyDefenses4, this._voltageDefenses4)
                };
            }
            set
            {
                this._corner1 = value[0].Corners;
                this._currentDefense1 = value[0].CurrentDefenses;
                this._addCurrentDefenses1 = value[0].AddCurrentDefenses;
                this._frequencyDefenses1 = value[0].FrequencyDefenses;
                this._voltageDefenses1 = value[0].VoltageDefenses;

                this._corner2 = value[1].Corners;
                this._currentDefense2 = value[1].CurrentDefenses;
                this._addCurrentDefenses2 = value[1].AddCurrentDefenses;
                this._frequencyDefenses2 = value[1].FrequencyDefenses;
                this._voltageDefenses2 = value[1].VoltageDefenses;

                this._corner3 = value[2].Corners;
                this._currentDefense3 = value[2].CurrentDefenses;
                this._addCurrentDefenses3 = value[2].AddCurrentDefenses;
                this._frequencyDefenses3 = value[2].FrequencyDefenses;
                this._voltageDefenses3 = value[2].VoltageDefenses;

                this._corner4 = value[3].Corners;
                this._currentDefense4 = value[3].CurrentDefenses;
                this._addCurrentDefenses4 = value[3].AddCurrentDefenses;
                this._frequencyDefenses4 = value[3].FrequencyDefenses;
                this._voltageDefenses4 = value[3].VoltageDefenses;
            }
        }

        private GroupSetpoint GetGroupsetpoint(CornerStruct corners, AllCurrentDefensesStruct currentDefenses, AllAddCurrentDefensesStruct addCurrentDefenses, 
            AllFrequencyDefensesStruct frequencyDefenses, AllVoltageDefensesStruct voltageDefenses)
        {
            return new GroupSetpoint
            {
                Corners = corners.Clone<CornerStruct>(),
                CurrentDefenses = currentDefenses.Clone<AllCurrentDefensesStruct>(),
                AddCurrentDefenses = addCurrentDefenses.Clone<AllAddCurrentDefensesStruct>(),
                FrequencyDefenses = frequencyDefenses.Clone<AllFrequencyDefensesStruct>(),
                VoltageDefenses = voltageDefenses.Clone<AllVoltageDefensesStruct>()
            };
        }
    }
}
