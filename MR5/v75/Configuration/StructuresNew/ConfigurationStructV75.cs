﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR5.v75.Configuration.Structures.Apv;
using BEMN.MR5.v75.Configuration.Structures.Avr;
using BEMN.MR5.v75.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v75.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v75.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v75.Configuration.Structures.FaultSignal;
using BEMN.MR5.v75.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v75.Configuration.Structures.Indicators;
using BEMN.MR5.v75.Configuration.Structures.Keys;
using BEMN.MR5.v75.Configuration.Structures.Ls;
using BEMN.MR5.v75.Configuration.Structures.Lzsh;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v75.Configuration.Structures.OscConfig;
using BEMN.MR5.v75.Configuration.Structures.Relay;
using BEMN.MR5.v75.Configuration.Structures.Switch;
using BEMN.MR5.v75.Configuration.Structures.Vls;
using BEMN.MR5.v75.Configuration.Structures.VoltageDefenses;

namespace BEMN.MR5.v75.Configuration.StructuresNew
{
    [XmlRoot(ElementName = "МР5")]
    public class ConfigurationStruct75 : StructBase
    {

        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР5"; } set { } }

        #region [Private fields]
        [Layout(0)] private MeasureTransStruct75 _measureTrans;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private ExternalSignalStruct _externalSignal;
        [Layout(3)] private FaultStruct _fault;
        [Layout(4, Ignore = true)] private ushort _portInterface;
        [Layout(5)] private InputLogicSignalStruct _inputLogicSignal;
        [Layout(6)] private SwitchStruct _switch;
        [Layout(7)] private ApvStruct _apv;
        [Layout(8)] private AvrStruct _avr;
        [Layout(9)] private LpbStruct _lzsh;
        [Layout(10)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(11)] private AllSetpointsStruct _allSetpoints;
        [Layout(12)] private AllAddSetpointsStruct _allAddSetpoints;
        [Layout(13)] private FrequencySetpointsStruct _allFrequency;
        [Layout(14)] private VoltageSetpointsStruct _allVoltage;
        [Layout(15)] private OutputLogicSignalStruct _vls;
        [Layout(16)] private AllReleOutputStruct _reley;
        [Layout(17)] private AllIndicatorsStruct _indicators;
        [Layout(18, Count = 4, Ignore = true)] private ushort[] _resSystem;
        [Layout(19)] private OscConfigStruct _oscConfig; 
        #endregion [Private fields]
        
        #region [Properties]
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        [BindingProperty(0)]
        public MeasureTransStruct75 MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }
        [XmlElement(ElementName = "Внешние_сигналы")]
        [BindingProperty(1)]
        public ExternalSignalStruct ExternalSignal
        {
            get { return this._externalSignal; }
            set { this._externalSignal = value; }
        }
        [XmlElement(ElementName = "Реле_неисправности")]
        [BindingProperty(2)]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        [BindingProperty(3)]
        public InputLogicSignalStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(4)]
        public SwitchStruct Switch
        {
            get { return this._switch; }
            set { this._switch = value; }
        }
        [XmlElement(ElementName = "АПВ")]
        [BindingProperty(5)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }
        [XmlElement(ElementName = "АВР")]
        [BindingProperty(6)]
        public AvrStruct Avr
        {
            get { return this._avr; }
            set { this._avr = value; }
        }
        [XmlElement(ElementName = "ЛЗШ")]
        [BindingProperty(7)]
        public LpbStruct Lzsh
        {
            get { return this._lzsh; }
            set { this._lzsh = value; }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Токовые")]
        public AllSetpointsStruct AllSetpoints
        {
            get { return this._allSetpoints; }
            set { this._allSetpoints = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Токовые_дополнительные")]
        public AllAddSetpointsStruct AllAddSetpoints
        {
            get { return this._allAddSetpoints; }
            set { this._allAddSetpoints = value; }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Частотные")]
        public FrequencySetpointsStruct AllFrequency
        {
            get { return this._allFrequency; }
            set { this._allFrequency = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "Напряжения")]
        public VoltageSetpointsStruct AllVoltage
        {
            get { return this._allVoltage; }
            set { this._allVoltage = value; }
        }
        [XmlElement(ElementName = "Все_ВЛС")]
        [BindingProperty(13)]
        public OutputLogicSignalStruct Vls
        {
            get { return this._vls; }
            set { this._vls = value; }
        }

        [XmlElement(ElementName = "Индикаторы")]
        [BindingProperty(14)]
        public AllIndicatorsStruct Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        [XmlElement(ElementName = "Осц")]
        [BindingProperty(15)]
        public OscConfigStruct OscConfig
        {
            get { return this._oscConfig; }
            set { this._oscConfig = value; }
        }

        [XmlElement(ElementName = "Ключи")]
        [BindingProperty(16)]
        public KeysStruct Keys
        {
            get { return this._keys; }
            set { this._keys = value; }
        } 
        #endregion [Properties]
    }
}
