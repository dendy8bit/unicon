﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v75.Configuration.Structures.CurrentDefenses
{
    public class SetpointStruct : StructBase
    {
        [Layout(0)] private CornerStruct _corners;
        [Layout(1)] private AllCurrentDefensesStruct _currentDefenses;


        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornerStruct Corners
        {
            get { return _corners; }
            set { _corners = value; }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Основные_токовые")]
        public AllCurrentDefensesStruct CurrentDefenses
        {
            get { return _currentDefenses; }
            set { _currentDefenses = value; }
        }




    }
}
