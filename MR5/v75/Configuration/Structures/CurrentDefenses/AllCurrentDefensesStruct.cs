﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR5.v75.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesStruct : StructBase, IDgvRowsContainer<CurrentDefenseStruct>
    {

        [Layout(0, Count = 10)]
        private CurrentDefenseStruct[] _currentDefenses;
      /*  [Layout(1, Count = 64)]
        private ushort[] _res;*/
        public CurrentDefenseStruct[] Rows
        {
            get { return _currentDefenses; }
            set { _currentDefenses = value; }
        }
    }
}
