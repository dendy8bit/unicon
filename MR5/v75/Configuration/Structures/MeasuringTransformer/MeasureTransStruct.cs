﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct75 : StructBase
    {
        #region [Public field]

        [Layout(0)] private ushort _ttMode; //res
        [Layout(1)] private ushort _tt;
        [Layout(2)] private ushort _ttnp;
        [Layout(3)] private ushort _maxI;
        [Layout(4)] private ushort _startI; //res
        [Layout(5)] private ushort _res3;
        [Layout(6)] private ushort _res1;
        [Layout(7)] private ushort _res2;
        [Layout(8)] private ushort _tnMode;
        [Layout(9)] private ushort _tn;
        [Layout(10)] private ushort _tnFault;
        [Layout(11)] private ushort _tnnp;
        [Layout(12)] private ushort _tnnpFault;
        [Layout(13)] private ushort _opmMode;
        [Layout(14)] private ushort _rOpm;
        
        #endregion [Public field]

        [XmlElement(ElementName = "конфигурация_ТТ")]
        [BindingProperty(0)]
        public ushort Tt
        {
            get { return this._tt; }
            set { this._tt = value; }
        }

        [XmlElement(ElementName = "конфигурация_ТТНП")]
        [BindingProperty(1)]
        public ushort Ttnp
        {
            get { return this._ttnp; }
            set { this._ttnp = value; }
        }

        [XmlElement(ElementName = "Iм")]
        [BindingProperty(2)]
        public double MaxI
        {
            get { return ValuesConverterCommon.GetIn(this._maxI); }
            set { this._maxI = ValuesConverterCommon.SetIn(value); }

        }

        [XmlElement(ElementName = "тип_ТТ")]
        [BindingProperty(3)]
        public string TtMode
        {
            get { return Validator.Get(this._ttMode, StringsConfigV75.TtType); }
            set { this._ttMode = Validator.Set(value, StringsConfigV75.TtType); }
        }

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(4)]
        [XmlIgnore]
        public double TnKoef
        {
            get { return ValuesConverterCommon.GetKth(this._tn); }
            set { this._tn = ValuesConverterCommon.SetKth(value); }
        }
        /// <summary>
        /// Множитель коэффициента ТН 
        /// </summary>
        [BindingProperty(5)]
        [XmlIgnore]
        public string TnAmpl
        {
            get { return Validator.Get(this._tn, StringsConfigV75.TnKoef, 15); }
            set { this._tn = Validator.Set(value, StringsConfigV75.TnKoef, this._tn, 15); }
        }

        [XmlElement(ElementName = "Неисправность_ТН")]
        [BindingProperty(6)]
        public string TnFault
        {
            get { return Validator.Get(this._tnFault, StringsConfigV75.LogicSignals); }
            set { this._tnFault = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        /// <summary>
        /// KTHX
        /// </summary>
        [BindingProperty(7)]
        [XmlIgnore]
        public double TnnpKoef
        {
            get { return ValuesConverterCommon.GetKth(this._tnnp); }
            set { this._tnnp = ValuesConverterCommon.SetKth(value); }
        }
        /// <summary>
        /// Множитель коэффициента ТННП
        /// </summary>
        [BindingProperty(8)]
        [XmlIgnore]
        public string TnnpAmpl
        {
            get { return Validator.Get(this._tnnp, StringsConfigV75.TnKoef, 15); }
            set { this._tnnp = Validator.Set(value, StringsConfigV75.TnKoef, this._tnnp, 15); }
        }

        [XmlElement(ElementName = "Неисправность_ТННП")]
        [BindingProperty(9)]
        public string TnnpFault
        {
            get { return Validator.Get(this._tnnpFault, StringsConfigV75.LogicSignals); }
            set { this._tnnpFault = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [XmlElement(ElementName = "ОМП")]
        [BindingProperty(10)]
        public bool Omp
        {
            get { return Common.GetBit(this._opmMode, 0); }
            set { this._opmMode = Common.SetBit(this._opmMode, 0, value); }
        }

        [XmlElement(ElementName = "Xyd")]
        [BindingProperty(11)]
        public double Xyd
        {
            get { return this._rOpm/1000.0; }
            set { this._rOpm = (ushort) (value*1000); }
        }


        [XmlElement(ElementName = "ТН")]
        public double Tn
        {
            get { return ValuesConverterCommon.GetKthFull(this._tn); }
            set { this._tn = ValuesConverterCommon.SetKthFull(value); }
        }

        [XmlElement(ElementName = "ТННП")]
        public double Tnnp
        {
            get { return ValuesConverterCommon.GetKthFull(this._tnnp); }
            set { this._tnnp = ValuesConverterCommon.SetKthFull(value); }
        }
    }
}
