﻿using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v75.Configuration.Structures.FrequencyDefenses
{
    public class FrequencySetpointsStruct : StructBase, ISetpointContainer<AllFrequencyDefensesStruct>
    {

        [Layout(0, Count = 2)] private AllFrequencyDefensesStruct[] _groups;

        [XmlIgnore]
        public AllFrequencyDefensesStruct[] Setpoints
        {
            get { return _groups.Select(o => o.Clone<AllFrequencyDefensesStruct>()).ToArray(); }
            set { _groups = value; }
        }
        [XmlElement(ElementName = "Основная")]
        public AllFrequencyDefensesStruct Main
        {
            get { return _groups[0]; }
            set { _groups[0] = value ; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllFrequencyDefensesStruct Reserve
        {
            get { return _groups[1]; }
            set { _groups[1] = value; }
        }
    }
}
