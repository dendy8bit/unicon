﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR5.v75.Configuration.Structures.SystenConfig
{
    public class SystemConfigStruct : StructBase
    {
        [Layout(0, Ignore = true)]
        private ushort _address;
        [Layout(1, Ignore = true)]
        private ushort _speed;
        [Layout(2, Ignore = true)]
        private ushort _delay;
        [Layout(3, Ignore = true)]
        private ushort _status;
        [Layout(4)]
        private ushort _oscConf;

        public const int ONE_OSC_MAX_LEN = 15872;

        /// <summary>
        /// Длит. предзаписи
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Предзапись")]
        public ushort Percent
        {
            get { return (ushort)(Common.GetBits(this._oscConf, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this._oscConf = Common.SetBits(this._oscConf, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        /// <summary>
        /// Фиксация
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фиксация")]
        public string Fixation
        {
            get { return Validator.Get(this._oscConf, StringsConfigV75.OscFixation, 7); }
            set { this._oscConf = Validator.Set(value, StringsConfigV75.OscFixation, this._oscConf, 7); }
        }

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string Count
        {
            get { return Validator.Get(this._oscConf, StringsConfigV75.OscSize.ToList(), 0, 1, 2, 3, 4, 5, 6); }
            set { this._oscConf = Validator.Set(value, StringsConfigV75.OscSize.ToList(), this._oscConf, 0, 1, 2, 3, 4, 5, 6); }
        }

        [XmlElement(ElementName = "Длительность")]
        public int Size
        {
            get { return ONE_OSC_MAX_LEN * 2 / (2 + Common.GetBits(this._oscConf, 0, 1, 2, 3, 4, 5, 6)); }
            set { }
        }
    }
}
