﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v75.Configuration.Structures.FaultSignal
{
    public class FaultStruct : StructBase
    {
        [Layout(0)] private FaultSignalStruct _signals;
        [Layout(1)] private ushort _time;
        [Layout(2)] private ushort _inpGroup3; // вход переключения на группу уставок 3
        [Layout(3)] private ushort _inpGroup4; // вход переключения на группу уставок 4

        [BindingProperty(0)]
        public FaultSignalStruct Signals
        {
            get { return this._signals; }
            set { this._signals = value; }
        }

        [XmlAttribute(AttributeName = "Импульс_реле_неисправность")]
        [BindingProperty(1)]
        public int Time
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time) ; }
            set { this._time =ValuesConverterCommon.SetWaitTime(value) ; }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "группа_уставок_3")]
        public string InpGroup3
        {
            get { return Validator.Get(this._inpGroup3, StringsConfigV75.LogicSignals); }
            set { this._inpGroup3 = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "группа_уставок_4")]
        public string InpGroup4
        {
            get { return Validator.Get(this._inpGroup4, StringsConfigV75.LogicSignals); }
            set { this._inpGroup4 = Validator.Set(value, StringsConfigV75.LogicSignals); }
        }
    }
}