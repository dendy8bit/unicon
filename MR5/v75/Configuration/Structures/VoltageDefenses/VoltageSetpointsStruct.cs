﻿using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR5.v75.Configuration.Structures.VoltageDefenses
{
    public class VoltageSetpointsStruct : StructBase, ISetpointContainer<AllVoltageDefensesStruct>
    {

        [Layout(0, Count = 2)]
        private AllVoltageDefensesStruct[] _groups;

        [XmlIgnore]
        public AllVoltageDefensesStruct[] Setpoints
        {
            get { return _groups.Select(o => o.Clone<AllVoltageDefensesStruct>()).ToArray(); }
            set { _groups = value; }
        }

        [XmlElement(ElementName = "Основная")]
        public AllVoltageDefensesStruct Main
        {
            get { return _groups[0]; }
            set { _groups[0] = value; }
        }
        [XmlElement(ElementName = "Резервная")]
        public AllVoltageDefensesStruct Reserve
        {
            get { return _groups[1]; }
            set {  _groups[1] = value; }
        }
    }
}
