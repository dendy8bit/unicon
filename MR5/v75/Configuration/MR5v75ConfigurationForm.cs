using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.v75.Configuration.Structures.Apv;
using BEMN.MR5.Properties;
using BEMN.MR5.v75.Configuration.Structures.CurrentDefenses;
using BEMN.MR5.v75.Configuration.Structures.VoltageDefenses;
using BEMN.MR5.v75.Configuration.Structures.FrequencyDefenses;
using BEMN.MR5.v75.Configuration.Structures.Keys;
using BEMN.MR5.v75.Configuration.Structures.Indicators;
using BEMN.MR5.v75.Configuration.Structures.Vls;
using BEMN.MR5.v75.Configuration.Structures.ExternalDefenses;
using BEMN.MR5.v75.Configuration.Structures.Lzsh;
using BEMN.MR5.v75.Configuration.Structures.Avr;
using BEMN.MR5.v75.Configuration.Structures.Ls;
using BEMN.MR5.v75.Configuration.Structures.FaultSignal;
using BEMN.MR5.v75.Configuration.Structures.ExternalSignals;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v75.Configuration.Structures.OscConfig;
using BEMN.MR5.v75.Configuration.Structures.Switch;
using BEMN.MR5.v75.Configuration.StructuresNew;

namespace BEMN.MR5.v75.Configuration
{
    public partial class MR5v75ConfigurationForm : Form, IFormView
    {
        #region [Const]
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string MR5 = "MR5v75";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";
        private const string XML_HEAD = "MR5v75_SET_POINTS";
        private const string MR5v75_BASE_CONFIG_PATH = "\\MR5\\MR5v{0:F2}_BaseConfig.bin";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "������� ������� ������� ���������";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";

        private const string GROUP_SETPOIN_OLD_LABEL = "������ �������";
        private static readonly Size GROUPBOX_OLD_SIZE = new Size(203, 153);

        #endregion

        #region [Private fields]
        private readonly MR5Device _device;
        private readonly double _version;
        private readonly MemoryEntity<ConfigurationStruct75> _configuration;
        private readonly MemoryEntity<ConfigurationStructV75new> _configurationNew; 
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;
        

        #region [Validators]
        private NewStructValidator<MeasureTransStruct75> _measureTransValidator;
        private NewCheckedListBoxValidator<KeysStruct> _keysValidator;
        private NewStructValidator<ExternalSignalStruct> _externalSignalsValidator;
        private NewStructValidator<FaultStruct> _faultValidator;

        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;

        private NewStructValidator<SwitchStruct> _switchValidator;
        private NewStructValidator<ApvStruct> _apvValidator;
        private NewStructValidator<AvrStruct> _avrValidator;
        private NewStructValidator<LpbStruct> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct> _externalDefenseValidatior;

        private NewStructValidator<CornerStruct> _cornerValidator;
        private NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> _currentDefenseValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> _addCurrentDefenseValidator;
        private SetpointsValidator<AllSetpointsStruct, SetpointStruct> _currentSetpointsValidator;
        private StructUnion<SetpointStruct> _currentSetpointUnion;
        private SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct> _currentAddSetpointsValidator;

        private NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct> _frequencyDefenseValidatior;
        private SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct> _frequencySetpoints;

        private NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct> _voltageDefenseValidatior;
        private SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct> _voltageSetpoints;

        private CheckedListBox[] _vlsBoxes;
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<OscConfigStruct> _oscValidator;
        // ����� ������������
        private StructUnion<GroupSetpoint> _groupSetpoinStructUnion;
        private SetpointsValidator<AllDefensesSetpoint, GroupSetpoint> _alldefensesValidator;
        private StructUnion<ConfigurationStructV75new> _configurationUnionNew;
        private ConfigurationStructV75new _configurationStructV75New;
        // ������ ������������
        private StructUnion<ConfigurationStruct75> _configurationUnion;
        private ConfigurationStruct75 _configurationStructV75;
        private RadioButtonSelector _groupSelector;
        #endregion [Validators]

        #endregion [Private fields]

        #region C'tor
        public MR5v75ConfigurationForm()
        {
            this.InitializeComponent();
        }
        
        public MR5v75ConfigurationForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._version = Common.VersionConverter(this._device.DeviceVersion);
            StringsConfigV75.Version = this._version;
            if (this._version > 75)
            {
                this._configurationNew = this._device.Configuration75New;
                this._configurationNew.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
                this._configurationNew.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);
                });
                this._configurationNew.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configurationNew.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configurationNew.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);

                });
                this._configurationNew.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {

                    if (_device.MB.NetworkEnabled)
                    {
                        this._device.WriteConfiguration(new TimeSpan(50));
                    }
                    else
                    {
                        this._device.WriteConfiguration();
                    }
                    this.IsProcess = false;
                    this._statusLabel.Text = "������������ ������� ��������";
                });
                this._exchangeProgressBar.Maximum = this._configurationNew.Slots.Count;
            }
            else
            {
                this._configuration = this._device.Configuration75;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
                this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);

                });
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
                this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_WRITE_CONFIG;
                    MessageBox.Show(ERROR_WRITE_CONFIG);

                });
                this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {

                    this._device.WriteConfiguration();
                    this.IsProcess = false;
                    this._statusLabel.Text = "������������ ������� ��������";

                });
                this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            }
            
            this.Init();
            this._groupSelector = new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup, this._ChangeSetPointsButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
            
            if (this._version < 75.01)
            {
                this.OldCopyGroupBox.Visible = true;
                this.NewCopyGroupBox.Visible = false;
                this.ConnectionParametersGroup.Visible = true;
                this.group1label.Text = GROUP_SETPOIN_OLD_LABEL;
                this.groupBox3.Size = GROUPBOX_OLD_SIZE;
                this.group2label.Visible = this.group2label.Visible = this.group2label.Visible = false;
                this._constraintGroup2Combo.Visible = this._constraintGroup3Combo.Visible = this._constraintGroup4Combo.Visible = false;
            }
        }

        private void Init()
        {
            this.copyGroupsCombo.DataSource = StringsConfigV75.CopyGroups;
            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4,
                this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12
            };

            this._measureTransValidator = new NewStructValidator<MeasureTransStruct75>
                (
                this._toolTip,
                new ControlInfoText(this._TT_Box, new CustomUshortRule(0, 5000)),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo1000),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._TT_typeCombo, StringsConfigV75.TtType),
                new ControlInfoText(this._TN_Box, RulesContainer.Ustavka128),
                new ControlInfoCombo(this.TnAmplCombo, StringsConfigV75.TnKoef),
                new ControlInfoCombo(this._TN_dispepairCombo, StringsConfigV75.LogicSignals),
                new ControlInfoText(this._TNNP_Box, RulesContainer.Ustavka128),
                new ControlInfoCombo(this.TnnpAmplCombo, StringsConfigV75.TnKoef),
                new ControlInfoCombo(this._TNNP_dispepairCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCheck(this._ompCheckBox),
                new ControlInfoText(this._HUD_Box, RulesContainer.DoubleTo1)
                );

            this._externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._keyOnCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._extOffCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._extOnCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._signalizationCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._constraintGroup1Combo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._uca, StringsConfigV75.Uca),
                new ControlInfoCombo(this._f, StringsConfigV75.F),
                new ControlInfoCombo(this._side1, StringsConfigV75.Side),
                new ControlInfoCombo(this._side2, StringsConfigV75.Side),
                new ControlInfoText(this.noUaccBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this.disableComboBox, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this.connectionPort, StringsConfigV75.ConnectionPort)
                );
            
            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList, StringsConfigV75.Faults)),
                new ControlInfoText(this._releDispepairBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._constraintGroup3Combo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._constraintGroup4Combo, StringsConfigV75.LogicSignals)
                );

            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    this._lsBoxes[i],
                    this._version > 75 ? 16 : 8,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfigV75.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfigV75.LsState)
                    );
            }
            this._inputLogicUnion = new StructUnion<InputLogicSignalStruct>(this._inputLogicValidator);

            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfigV75.LogicSignals),
                new ControlInfoText(this._switcherTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherTokBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherDurationBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfigV75.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfigV75.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfigV75.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfigV75.Zr)
                );

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.apv_conf, StringsConfigV75.ApvModes),
                new ControlInfoCombo(this.apv_blocking, StringsConfigV75.LogicSignals),
                new ControlInfoText(this.apv_time_blocking, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_ready, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_1krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_2krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_3krat, RulesContainer.IntTo3M),
                new ControlInfoText(this.apv_time_4krat, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvStartCheckBox)
                );

            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this.avr_mode),
                new ControlInfoCombo(this.avr_blocking, StringsConfigV75.LogicSignals),
                new ControlInfoCombo(this.avr_reset_blocking, StringsConfigV75.LogicSignals),
                new ControlInfoText(this._umin1, RulesContainer.Ustavka256),
                new ControlInfoText(this._umax1, RulesContainer.Ustavka256),
                new ControlInfoText(this._umin2, RulesContainer.Ustavka256),
                new ControlInfoText(this._umax2, RulesContainer.Ustavka256),
                new ControlInfoText(this.avr_time_return, RulesContainer.IntTo3M),
                new ControlInfoText(this.avr_disconnection, RulesContainer.IntTo3M)
                );

            this._lpbValidator = new NewStructValidator<LpbStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.comboBox1, StringsConfigV75.Lzs),
                new ControlInfoText(this.lzsh_constraint, RulesContainer.Ustavka40)
                );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, DefenseExternalStruct>
                (this._externalDefenseGrid,
                    8,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfigV75.ExternalDefensesNames, ColumnsType.NAME), //1
                    new ColumnInfoCombo(StringsConfigV75.CurModesV1), //2
                    new ColumnInfoCombo(StringsConfigV75.ExternalDefenceSignals), //3
                    new ColumnInfoCombo(StringsConfigV75.ExternalDefenceSignals), //4
                    new ColumnInfoText(RulesContainer.IntTo3M), //5
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCombo(StringsConfigV75.ExternalDefenceSignals),
                    new ColumnInfoText(RulesContainer.IntTo3M),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(5, false, false, 6, 7, 8)
                        )
                }
            };

            this._vlsBoxes = new[]
            {
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4,
                this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
            };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i],
                    StringsConfigV75.VlsSignals);
            }
            this._vlsUnion = new StructUnion<OutputLogicSignalStruct>(this._vlsValidator);

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (this._outputIndicatorsGrid,
                    AllIndicatorsStruct.INDICATORS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfigV75.IndicatorNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfigV75.ReleyType),
                    new ColumnInfoCombo(StringsConfigV75.RelaySignals),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck(),
                    new ColumnInfoCheck()
                );

            this._oscValidator = new NewStructValidator<OscConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this.oscPercent, RulesContainer.Ushort1To100),
                new ControlInfoCombo(this.oscFix, StringsConfigV75.OscFixation),
                new ControlInfoCombo(this.oscLength, StringsConfigV75.OscSize.ToList())
                );

            this._keysValidator = new NewCheckedListBoxValidator<KeysStruct>(this._keysCheckedListBox, StringsConfigV75.KeyNumbers);


            #region Defenses
            Func<IValidatingRule> currentDefenseFunc = () =>
            {
                try
                {
                    if (this._tokDefenseGrid1[9, this._tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                        StringsConfigV75.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            };

            this._cornerValidator = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._tokDefenseIbox, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI0box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseI2box, RulesContainer.UshortTo360),
                new ControlInfoText(this._tokDefenseInbox, RulesContainer.UshortTo360)
                );

            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                new[] {this._tokDefenseGrid1, this._tokDefenseGrid2, this._tokDefenseGrid3},
                new[] {4, 4, 2},
                this._toolTip,
                new ColumnInfoCombo(StringsConfigV75.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfigV75.CurModesV1), //0
                new ColumnInfoCombo(StringsConfigV75.LogicSignals), //1
                new ColumnInfoCheck(), //2 
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoCombo(StringsConfigV75.Direction), //4
                new ColumnInfoCombo(StringsConfigV75.DirectionBlock), //5
                new ColumnInfoCombo(StringsConfigV75.TokParameter, ColumnsType.COMBO, true, false, false), //6
                new ColumnInfoCombo(StringsConfigV75.TokParameter2, ColumnsType.COMBO, false, true, true), //6
                new ColumnInfoText(RulesContainer.Ustavka40, true, true, false), //7        
                new ColumnInfoText(RulesContainer.Ustavka5, false, false, true),
                new ColumnInfoCombo(StringsConfigV75.FeatureLight, ColumnsType.COMBO, true, false, false),//8              
                new ColumnInfoTextDependent(currentDefenseFunc), //9
                new ColumnInfoCheck(), //10
                new ColumnInfoText(RulesContainer.IntTo3M), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(), //13
                new ColumnInfoCombo(StringsConfigV75.OscV111) //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid1,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14, 15, 16, 17),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfigV75.Direction[0], false, 6),
                        new TurnOffRule(12, false, false, 13)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid2,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14, 15, 16, 17),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfigV75.Direction[0], false, 6),
                        new TurnOffRule(12, false, false, 13)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid3,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,14, 15, 16, 17),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(5, StringsConfigV75.Direction[0], false, 6),
                        new TurnOffRule(12, false, false, 13)
                        )
                }
            };

            this._addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (
                    new[] {this._tokDefenseGrid4, this._tokDefenseGrid5},
                    new[] {1, 1},
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfigV75.AddCurrentDefensesNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfigV75.CurModesV1), //0
                    new ColumnInfoCombo(StringsConfigV75.LogicSignals), //1
                    new ColumnInfoCheck(), //2 
                    new ColumnInfoText(RulesContainer.Ustavka256), //3
                    new ColumnInfoText(RulesContainer.Ustavka5, true, false), //4 
                    new ColumnInfoText(RulesContainer.Ustavka100, false, true), //4 
                    new ColumnInfoText(RulesContainer.IntTo3M), //5
                    new ColumnInfoCheck(), //6
                    new ColumnInfoText(RulesContainer.IntTo3M), //7
                    new ColumnInfoCheck(), //8
                    new ColumnInfoCheck(), //9
                    new ColumnInfoCombo(StringsConfigV75.OscV111) //10
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid4,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(8, false, false, 9)
                        ),
                    new TurnOffDgv
                        (
                        this._tokDefenseGrid5,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(3, false, false, 4),
                        new TurnOffRule(8, false, false, 9)
                        )
                }
            };

            this._currentSetpointUnion = new StructUnion<SetpointStruct>
                (
                this._cornerValidator,
                this._currentDefenseValidatior
                );
            this._currentSetpointsValidator = new SetpointsValidator<AllSetpointsStruct, SetpointStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._currentSetpointUnion
                );

            this._currentAddSetpointsValidator = new SetpointsValidator<AllAddSetpointsStruct, AllAddCurrentDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._addCurrentDefenseValidator
                );

            this._frequencyDefenseValidatior = new NewDgwValidatior<AllFrequencyDefensesStruct, FrequencyDefenseStruct>
                (this._frequenceDefensesGrid,
                    4,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfigV75.FrequencyDefensesNames, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfigV75.CurModesV1), //0
                    new ColumnInfoCombo(StringsConfigV75.ExternalDefenceSignals), //1
                    new ColumnInfoText(RulesContainer.Ustavka40To60), //2 
                    new ColumnInfoText(RulesContainer.IntTo3M), //3
                    new ColumnInfoCheck(), //4
                    new ColumnInfoCheck(), //5
                    new ColumnInfoText(RulesContainer.Ustavka40To60), //6
                    new ColumnInfoText(RulesContainer.IntTo3M), //7        
                    new ColumnInfoCheck(), //8              
                    new ColumnInfoCheck(), //9
                    new ColumnInfoCombo(StringsConfigV75.OscV111), //11
                    new ColumnInfoCheck() //12
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._frequenceDefensesGrid,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12),
                        new TurnOffRule(5, false, false, 6, 7, 8)
                        )
                }
            };

            this._frequencySetpoints = new SetpointsValidator<FrequencySetpointsStruct, AllFrequencyDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._frequencyDefenseValidatior
                );

            this._voltageDefenseValidatior = new NewDgwValidatior<AllVoltageDefensesStruct, VoltageDefenseStruct>
                (
                new[]
                {
                    this._voltageDefensesGrid1, this._voltageDefensesGrid11, this._voltageDefensesGrid2,
                    this._voltageDefensesGrid3
                },
                new[] {2, 2, 2, 2},
                this._toolTip,
                new ColumnInfoCombo(StringsConfigV75.VoltageDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfigV75.CurModesV1), //0
                new ColumnInfoCombo(StringsConfigV75.LogicSignals), //1
                new ColumnInfoCombo(StringsConfigV75.VoltageParameterU, ColumnsType.COMBO, true, true, false, false),//1
                new ColumnInfoCombo(StringsConfigV75.VoltageParameter1, ColumnsType.COMBO, false, false, false, false),//1
                new ColumnInfoCombo(StringsConfigV75.VoltageParameter2, ColumnsType.COMBO, false, false, false, false),//1
                new ColumnInfoText(RulesContainer.Ustavka256), //2 
                new ColumnInfoText(RulesContainer.IntTo3M), //3
                new ColumnInfoCheck(), //4
                new ColumnInfoCheck(), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoText(RulesContainer.IntTo3M), //7        
                new ColumnInfoCheck(), //8              
                new ColumnInfoCheck(), //9
                new ColumnInfoCombo(StringsConfigV75.OscV111), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCheck(false, true, false, false)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid1,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid11,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid2,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        ),
                    new TurnOffDgv
                        (
                        this._voltageDefensesGrid3,
                        new TurnOffRule(1, StringsConfigV75.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(8, false, false, 9, 10, 11)
                        )
                }
            };

            this._voltageSetpoints = new SetpointsValidator<VoltageSetpointsStruct, AllVoltageDefensesStruct>
                (
                new RadioButtonSelector(this._mainRadioButtonGroup, this._reserveRadioButtonGroup),
                this._voltageDefenseValidatior
                );
            #endregion

            if (this._version > 75)
            {
                NewStructValidator<ExternalSignalsNew> externalSignalsValidator = new NewStructValidator
                    <ExternalSignalsNew>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this._keyOffCombo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._keyOnCombo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._extOffCombo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._extOnCombo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._signalizationCombo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup1Combo, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._uca, StringsConfigV75.Uca),
                    new ControlInfoCombo(this._f, StringsConfigV75.F),
                    new ControlInfoCombo(this._side1, StringsConfigV75.Side),
                    new ControlInfoCombo(this._side2, StringsConfigV75.Side),
                    new ControlInfoText(this.noUaccBox, RulesContainer.Ustavka256),
                    new ControlInfoCombo(this.disableComboBox, StringsConfigV75.LogicSignals),
                    new ControlInfoCombo(this._constraintGroup2Combo, StringsConfigV75.LogicSignals)
                    );

                this._groupSetpoinStructUnion = new StructUnion<GroupSetpoint>
                    (
                    this._cornerValidator,
                    this._currentDefenseValidatior,
                    this._addCurrentDefenseValidator,
                    this._frequencyDefenseValidatior,
                    this._voltageDefenseValidatior
                    );

                this._alldefensesValidator = new SetpointsValidator<AllDefensesSetpoint, GroupSetpoint>
                    (
                    new ComboboxSelector(this.currentGroupCombo, StringsConfigV75.Groups), this._groupSetpoinStructUnion
                    );

                this._configurationUnionNew = new StructUnion<ConfigurationStructV75new>
                    (
                        this._measureTransValidator, externalSignalsValidator, this._faultValidator,
                        this._inputLogicUnion, this._switchValidator, this._apvValidator, this._avrValidator,
                        this._lpbValidator, this._externalDefenseValidatior,
                        this._alldefensesValidator,
                        this._vlsUnion, this._indicatorValidator, this._oscValidator, this._keysValidator
                    );
            }
            else
            {
                this._configurationUnion = new StructUnion<ConfigurationStruct75>
                    (this._measureTransValidator, this._externalSignalsValidator, this._faultValidator,
                        this._inputLogicUnion, this._switchValidator, this._apvValidator, this._avrValidator,
                        this._lpbValidator, this._externalDefenseValidatior, this._currentSetpointsValidator,
                        this._currentAddSetpointsValidator, this._frequencySetpoints, this._voltageSetpoints,
                        this._vlsUnion, this._indicatorValidator, this._oscValidator, this._keysValidator
                    );
            }
        }

        #endregion C'tor

        void _groupSelector_NeedCopyGroup()
        {
            this.WriteConfiguration();
            //�������� � ���������, ����� ��������
            if (this._groupSelector.SelectedGroup == 0)
            {
                this._configurationStructV75.AllAddSetpoints.Reserve = this._configurationStructV75.AllAddSetpoints.Main.Clone<AllAddCurrentDefensesStruct>();
                this._configurationStructV75.AllSetpoints.Reserv = this._configurationStructV75.AllSetpoints.Main.Clone<SetpointStruct>();
                this._configurationStructV75.AllVoltage.Reserve = this._configurationStructV75.AllVoltage.Main.Clone<AllVoltageDefensesStruct>();
                this._configurationStructV75.AllFrequency.Reserve = this._configurationStructV75.AllFrequency.Main.Clone<AllFrequencyDefensesStruct>();
            }
            else
            {
                this._configurationStructV75.AllAddSetpoints.Main = this._configurationStructV75.AllAddSetpoints.Reserve.Clone<AllAddCurrentDefensesStruct>();
                this._configurationStructV75.AllSetpoints.Main = this._configurationStructV75.AllSetpoints.Reserv.Clone<SetpointStruct>();
                this._configurationStructV75.AllVoltage.Main = this._configurationStructV75.AllVoltage.Reserve.Clone<AllVoltageDefensesStruct>();
                this._configurationStructV75.AllFrequency.Main = this._configurationStructV75.AllFrequency.Reserve.Clone<AllFrequencyDefensesStruct>();
            }
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            string message;
            bool message_b;
            if (this._version > 75)
            {
                this._configurationStructV75New = this._configurationNew.Value.Clone<ConfigurationStructV75new>();
                this._configurationUnionNew.Set(this._configurationStructV75New);
                message_b = this._configurationUnionNew.Check(out message, false);
            }
            else
            {
                this._configurationStructV75 = this._configuration.Value.Clone<ConfigurationStruct75>();
                this._configurationUnion.Set(this._configurationStructV75);
                message_b = this._configurationUnion.Check(out message, false);
            }
            if (!message_b)
            {
                MessageBox.Show("� ���������� ���������� ������������ �������");
            }
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this.IsProcess = true;
            if (this._device.MB.NetworkEnabled)
            {
                this._device.ReadConfiguration(new TimeSpan(50));
            }
            else
            {
                this._device.ReadConfiguration();
            }

            if (this._version > 75)
            {
                if (this._device.MB.NetworkEnabled)
                {
                    this._configurationNew.LoadStruct(new TimeSpan(50));
                }
                else
                {
                    this._configurationNew.LoadStruct();
                }
            }
            else
            {
                this._configuration.LoadStruct();
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            if (this._version > 75)
            {
                if (this._configurationUnionNew.Check(out message, true))
                {
                    this._configurationStructV75New = this._configurationUnionNew.Get();
                    return true;
                }
            }
            else
            {
                if (this._configurationUnion.Check(out message, true))
                {
                    this._configurationStructV75 = this._configurationUnion.Get();
                    return true;
                }
            }

            MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
            return false;
        }

        #region Misc

        private void oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int oscLen = OscConfigStruct.ONE_OSC_MAX_LEN * 2 / (2 + this.oscLength.SelectedIndex);
            this.oscLenBox.Text = string.Format("{0} ��", oscLen);
        }

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this.ResetSetpoints(true);
        }
        private void ResetSetpoints(bool isDialog)   //�������� ������� ������� �� �����, isDialog - ���������� ����� �� ������
        {
            try
            {
                if (isDialog)
                {
                    DialogResult res = MessageBox.Show(@"��������� ������� �������?", @"������� �������",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (res == DialogResult.No) return;
                }
                //string path = Path.GetDirectoryName(Application.ExecutablePath) +
                //              string.Format(CultureInfo.InvariantCulture, MR5v75_BASE_CONFIG_PATH, this._version > 75 ? 75.01 : this._version);

                string path = Path.GetDirectoryName(Application.ExecutablePath) + string.Format(CultureInfo.InvariantCulture, MR5v75_BASE_CONFIG_PATH, this._version);

                this.Deserialize(path);
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("���������� ��������� ���� ����������� ������������. �������� �������?",
                        "��������", MessageBoxButtons.YesNo) != DialogResult.Yes)
                        return;
                }
                if (this._version > 75)
                {
                    this._configurationUnionNew.Reset();
                }
                else
                {
                    this._configurationUnion.Reset();
                }
            }
        } 
        private void ConfigurationFormV2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
            if (this._version > 75)
            {
                this._configurationNew.RemoveStructQueries();
            }
            else
            {
                this._configuration.RemoveStructQueries();
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this.Deserialize(this._openConfigurationDlg.FileName);
                    this._statusLabel.Text = string.Format("���� {0} ������� ��������", this._openConfigurationDlg.FileName);
                }
                catch
                {
                    MessageBox.Show(FILE_LOAD_FAIL);
                }
            }
        }

        private const string FILE_LOAD_FAIL = "���������� ��������� ����";

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(binFileName);
            XmlNode a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", MR5));
            if (a == null) throw new Exception();
            List<byte> values = new List<byte>(Convert.FromBase64String(a.InnerText));

            if (this._version > 75)
            {
                if (values.Count > 2016) // ��� ������������� ������������ �� ����, ��� ��� ����� ���� Ethernet.
                {                        // �� ������ ���������������� �������� ������� ���������� �����(RS485 ��� Ethernet)
                    values.RemoveAt(66);
                    values.RemoveAt(66);
                }
                this._configurationStructV75New = this._configurationStructV75New ?? new ConfigurationStructV75new();
                this._configurationStructV75New.InitStruct(values.ToArray());
                this._configurationUnionNew.Set(this._configurationStructV75New);
            }
            else
            {
                if (values.Count > 1248) // ��� ������������� ������������ �� ����, ��� ��� ����� ���� Ethernet.
                {                        // �� ������ ���������������� �������� ������� ���������� �����(RS485 ��� Ethernet)
                    values.RemoveAt(66);
                    values.RemoveAt(66);
                }
                this._configurationStructV75 = this._configurationStructV75 ?? new ConfigurationStruct75();
                this._configurationStructV75.InitStruct(values.ToArray());
                this._configurationUnion.Set(this._configurationStructV75);
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            if (this.WriteConfiguration())
            {
                this._saveConfigurationDlg.FileName = string.Format("��5_V{0}_�������.bin", this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    StructBase config = this._version > 75
                        ? (StructBase)this._configurationStructV75New
                        : this._configurationStructV75;
                    this.Serialize(this._saveConfigurationDlg.FileName, config, MR5);
                }
            }
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        /// <param name="config">����������� ��������� ������</param>
        /// <param name="head">��������� ���</param>
        public void Serialize(string binFileName, StructBase config, string head)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));
                
                ushort[] values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS", head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();  
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            DialogResult result = MessageBox.Show(string.Format("�������� ������������ �� 5 v75 �{0}?", this._device.DeviceNumber), "������",
                  MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                bool res;
                try
                {
                    res = this.WriteConfiguration();
                }
                catch (Exception)
                {
                    return;
                }
                if (res)
                {
                    this._statusLabel.Text = "��� ������ ������������";
                    this.IsProcess = true;
                    this._exchangeProgressBar.Value = 0;
                    if (this._version > 75)
                    {
                        this._configurationNew.Value = this._configurationStructV75New;
                        if (this._device.MB.NetworkEnabled)
                        {
                            this._configurationNew.SaveStruct(new TimeSpan(50));
                        }
                        else
                        {
                            this._configurationNew.SaveStruct();
                        }
                    }
                    else
                    {
                        this._configuration.Value = this._configurationStructV75;
                        this._configuration.SaveStruct();
                    }
                }
            }  
        }
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }
        
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            this.ResetSetpoints(false);
            if (Device.AutoloadConfig)
            {
                this.StartReadConfiguration();
            }
        }
        
        private void SaveToHtmlFile()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    if (this._version > 75)
                    {
                        ExportGroupForm exportGroup = new ExportGroupForm(4);
                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }
                        string fileName;
                        this._configurationStructV75New.DeviceVersion = this._version;
                        this._configurationStructV75New.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._configurationStructV75New.Group = (int)exportGroup.SelectedGroup;
                        if (exportGroup.SelectedGroup == ExportStruct.ALL)
                        {
                            fileName = string.Format("{0} ������� ������ {1} ��� ������", "��5", this._device.DeviceVersion);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_75_AllGroup, fileName, this._configurationStructV75New);
                        }
                        else
                        {
                            fileName = string.Format("{0} ������� ������ {1} ������{2}", "��5", this._device.DeviceVersion, this._configurationStructV75New.Group);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR5_75_Group, fileName, this._configurationStructV75New);
                        }
                    }
                    else
                    {
                        this._configurationStructV75.DeviceVersion = this._device.DeviceVersion;
                        this._configurationStructV75.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._statusLabel.Text = HtmlExport.Export(Resources.MR5_75_Main, Resources.MR5_75_Res, this._configurationStructV75, 
                            this._configurationStructV75.DeviceType, this._configurationStructV75.DeviceVersion);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("������ ����������");
            }
        }

        private void CopySetpoinsButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message;
                if (this._groupSetpoinStructUnion.Check(out message, true))
                {
                    GroupSetpoint[] allSetpoints =
                        this.CopySetpoints<AllDefensesSetpoint, GroupSetpoint>(this._groupSetpoinStructUnion, this._alldefensesValidator);

                    this._configurationStructV75New.AllDefensesSetpoint.Setpoints = allSetpoints;
                    this._alldefensesValidator.Set(this._configurationStructV75New.AllDefensesSetpoint);
                    MessageBox.Show("����������� ���������");
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������");
                }
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator) where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this.copyGroupsCombo.SelectedItem == StringsConfigV75.CopyGroups.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this.copyGroupsCombo.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this.currentGroupCombo.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this.ResetSetpoints(true);
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void MR5v75ConfigurationForm_Activated(object sender, EventArgs e)
        {
            StringsConfigV75.Version = this._version;
        }

        #endregion



        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MR5v75ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.config.ToBitmap();
            }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}