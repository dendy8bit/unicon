using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR5.v75.Measuring.Structures;
using BEMN.MR5.Properties;

namespace BEMN.MR5.v75.Measuring
{
    public partial class MR5v75MeasuringForm : Form, IFormView
    {
        private MR5Device _device;
        private LedControl[] _manageLeds;
        private LedControl[] _additionalLeds;
        private LedControl[] _indicatorLeds;
        private LedControl[] _inputLeds;
        private LedControl[] _outputLeds;
        private LedControl[] _limitLeds;
        private LedControl[] _faultStateLeds;
        private LedControl[] _faultSignalsLeds;
        private LedControl[] _automationLeds;
        private LedControl[] _ssl;
        private MemoryEntity<DiscretDataBaseStruct75> _discretDataBase;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<AnalogDataBaseStruct75> _analogDataBase;
        private readonly AveragerTime<AnalogDataBaseStruct75> _averagerTime;
        private MemoryEntity<Configuration.Structures.MeasuringTransformer.MeasureTransStruct75> _measureTrans;
        private Configuration.Structures.MeasuringTransformer.MeasureTransStruct75 _measureTransStruct;


        public MR5v75MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MR5v75MeasuringForm(MR5Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this._dateTime = device.DateAndTime75;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._discretDataBase = device.DiscretDataBase75;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);

            this._analogDataBase = device.AnalogDataBase75;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.OnAnalogSignalsLoadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._measureTrans = device.MeasureTrans75;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);
            
            this._device.Groups75.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                    this._setpointLabel.Text =
                        this._device.Groups75.Value.GetGroup(Common.VersionConverter(this._device.DeviceVersion)));
            this._device.Groups75.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._setpointLabel.Text = "�� ����������");

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct75>(500);
            this._averagerTime.Tick += this.AveragerTimeTick;
            this.Init();
        }

        private void Init()
        {
            if (Common.VersionConverter(this._device.DeviceVersion) < 75.01)
            {
                LedControl[] invisibleLeds =
                {
                    this._inD_led9, this._inD_led10, this._inD_led11, this._inD_led12,
                    this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16
                };
                foreach (LedControl ledControl in invisibleLeds)
                {
                    ledControl.Visible = false;
                }
                Label[] invisibleLabels =
                {
                    this.d9label, this.d10label, this.d11label, this.d12label, this.d13label, this.d14label,
                    this.d15label, this.d16label
                };
                foreach (Label invisibleLabel in invisibleLabels)
                {
                    invisibleLabel.Visible = false;
                }
                this.discretGroupBox.Size = new Size(144, 155);
                this._setpointsComboBox.DataSource = StringsConfigV75.SetpointsNames;
            }
            else
            {
                 this._setpointsComboBox.DataSource = StringsConfigV75.Groups;
            }
            this._setpointsComboBox.SelectedIndex = 0;
            this._ssl = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24
            };
            this._manageLeds = new[]
            {
                this._manageLed1, this._manageLed2, this._manageLed3, new LedControl(), new LedControl(), 
                this._manageLed6, this._manageLed7, this._manageLed8, this._manageLed9
            };
            this._additionalLeds = new LedControl[]
            {this._addIndLed1, this._addIndLed2, this._addIndLed3, this._addIndLed4};
            this._indicatorLeds = new LedControl[]
            {
                this._indLed1, this._indLed2, this._indLed3, this._indLed4, this._indLed5, this._indLed6, this._indLed7,
                this._indLed8
            };
            this._inputLeds = new LedControl[]
            {
                this._inD_led1, this._inD_led2, this._inD_led3, this._inD_led4,
                this._inD_led5, this._inD_led6, this._inD_led7, this._inD_led8,
                this._inD_led9, this._inD_led10, this._inD_led11, this._inD_led12,
                this._inD_led13, this._inD_led14, this._inD_led15, this._inD_led16,

                this._inL_led1, this._inL_led2, this._inL_led3, this._inL_led4,
                this._inL_led5, this._inL_led6, this._inL_led7, this._inL_led8
            };
            this._outputLeds = new LedControl[]
            {
                this._outLed1, this._outLed2, this._outLed3, this._outLed4, this._outLed5, this._outLed6, this._outLed7,
                this._outLed8
            };

            this._limitLeds = new LedControl[]
            {
                this._ImaxLed1, this._ImaxLed2, this._ImaxLed3, this._ImaxLed4, this._ImaxLed5, this._ImaxLed6,
                this._ImaxLed7, this._ImaxLed8, this._I0maxLed1, this._I0maxLed2, this._I0maxLed3, this._I0maxLed4,
                this._I0maxLed5, this._I0maxLed6, this._I0maxLed7, this._I0maxLed8, this._InmaxLed1, this._InmaxLed2,
                this._InmaxLed3, this._InmaxLed4, this._InmaxLed5, this._InmaxLed6, this._InmaxLed7, this._InmaxLed8,
                this._FmaxLed1, this._FmaxLed2, this._FmaxLed3, this._FmaxLed4, this._FmaxLed5, this._FmaxLed6,
                this._FmaxLed7, this._FmaxLed8, this._UmaxLed1, this._UmaxLed2, this._UmaxLed3, this._UmaxLed4,
                this._UmaxLed5, this._UmaxLed6, this._UmaxLed7, this._UmaxLed8, this._U0maxLed1, this._U0maxLed2,
                this._U0maxLed3, this._U0maxLed4, this._U0maxLed5, this._U0maxLed6, this._U0maxLed7, this._U0maxLed8,
                this._extDefenseLed1, this._extDefenseLed2, this._extDefenseLed3, this._extDefenseLed4,
                this._extDefenseLed5, this._extDefenseLed6, this._extDefenseLed7, this._extDefenseLed8
            };
            this._faultStateLeds = new LedControl[]
            {
                this._faultStateLed1, this._faultStateLed2, this._faultStateLed3, this._faultStateLed4,
                this._faultStateLed5, this._faultStateLed6, this._faultStateLed7, this._faultStateLed8
            };
            this._faultSignalsLeds = new LedControl[]
            {
                this._faultSignalLed1, this._faultSignalLed2, this._faultSignalLed3, this._faultSignalLed4,
                this._faultSignalLed5, new LedControl(), this._faultSignalLed7, this._faultSignalLed8, this._faultSignalLed9,
                this._faultSignalLed10, new LedControl(), this._faultSignalLed12, this._faultSignalLed13,
                this._faultSignalLed14, this._faultSignalLed15
            };
            this._automationLeds = new LedControl[]
            {
                this._autoLed1, this._autoLed2, this._autoLed3, this._autoLed4, this._autoLed5, this._autoLed6,
                this._autoLed7, this._autoLed8
            };
            if (Common.VersionConverter(this._device.DeviceVersion) < 75.01)
            {
                this._faultSignal8Label.Visible = this._faultSignalLed8.Visible = false;
            }
        }

        private void MeasureTransReadOk()
        {
            this._measureTransStruct = this._measureTrans.Value;
            this._analogDataBase.LoadStruct();
        }

        private void AveragerTimeTick()
        {
            this._InBox.Text = this._discretDataBase.Value.InSign + this._analogDataBase.Value.GetIn(this._averagerTime.ValueList, this._measureTransStruct);
            this._IaBox.Text = this._discretDataBase.Value.IaSign + this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._measureTransStruct);
            this._IbBox.Text = this._discretDataBase.Value.IbSign + this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._measureTransStruct);
            this._IcBox.Text = this._discretDataBase.Value.IcSign + this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._measureTransStruct);
            this._I1Box.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._measureTransStruct);
            this._I2Box.Text = this._discretDataBase.Value.I2Sign + this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._measureTransStruct);
            this._I0Box.Text = this._discretDataBase.Value.I0Sign + this._analogDataBase.Value.GetI0(this._averagerTime.ValueList, this._measureTransStruct);
            this._IgBox.Text = this._analogDataBase.Value.GetIg(this._averagerTime.ValueList, this._measureTransStruct);
            
            this._UaccBox.Text = this._analogDataBase.Value.GetUacc(this._averagerTime.ValueList, this._measureTransStruct);
            this._UnBox.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._measureTransStruct);
            this._UabBox.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._measureTransStruct);
            this._UbcBox.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._measureTransStruct);
            this._UcaBox.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._measureTransStruct);
            this._U1Box.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._measureTransStruct);
            this._U2Box.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._measureTransStruct);

            this._PBox.Text = this._discretDataBase.Value.SignP +
                              this._analogDataBase.Value.GetP(this._averagerTime.ValueList, this._measureTransStruct);
            this._QBox.Text = this._discretDataBase.Value.SignQ +
                this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, this._measureTransStruct);
            this._CosBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);
            this._F_Box.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
        }

        private void AnalogBdReadFail()
        {
            this._InBox.Text = string.Empty;
            this._IaBox.Text = string.Empty;
            this._IbBox.Text = string.Empty;
            this._IcBox.Text = string.Empty;
            this._I1Box.Text = string.Empty;
            this._I2Box.Text = string.Empty;
            this._I0Box.Text = string.Empty;
            this._IgBox.Text = string.Empty;

            this._UnBox.Text = string.Empty;
            this._UabBox.Text = string.Empty;
            this._UbcBox.Text = string.Empty;
            this._UcaBox.Text = string.Empty;
            this._U1Box.Text = string.Empty;
            this._U2Box.Text = string.Empty;

            this._PBox.Text = string.Empty;
            this._QBox.Text = string.Empty;
            this._CosBox.Text = string.Empty;
            this._F_Box.Text = string.Empty;
        }

        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._manageLeds);
            LedManager.TurnOffLeds(this._additionalLeds);
            LedManager.TurnOffLeds(this._indicatorLeds);
            LedManager.TurnOffLeds(this._inputLeds);
            LedManager.TurnOffLeds(this._outputLeds);

            LedManager.TurnOffLeds(this._limitLeds);
            LedManager.TurnOffLeds(this._automationLeds);
            LedManager.TurnOffLeds(this._faultSignalsLeds);
            LedManager.TurnOffLeds(this._faultStateLeds);
            LedManager.TurnOffLeds(this._ssl);
            this._manageLed10.State = LedState.Off;
        }

        private void DiscretBdReadOk()
        {
            LedManager.SetLeds(this._manageLeds, this._discretDataBase.Value.ManageSignals);
            LedManager.SetLeds(this._additionalLeds, this._discretDataBase.Value.AdditionalSignals);
            LedManager.SetLeds(this._indicatorLeds, this._discretDataBase.Value.Indicators);
            LedManager.SetLeds(this._inputLeds, this._discretDataBase.Value.InputSignals);
            LedManager.SetLeds(this._outputLeds, this._discretDataBase.Value.OutputSignals);

            LedManager.SetLeds(this._limitLeds, this._discretDataBase.Value.LimitSignals);
            LedManager.SetLeds(this._automationLeds, this._discretDataBase.Value.Automation);
            LedManager.SetLeds(this._faultSignalsLeds, this._discretDataBase.Value.FaultSignals);
            LedManager.SetLeds(this._faultStateLeds, this._discretDataBase.Value.FaultState);
            LedManager.SetLeds(this._ssl, this._discretDataBase.Value.Ssl);
            bool state = this._discretDataBase.Value.ManageSignals[9] && !this._discretDataBase.Value.FaultSignals[15];
            this._manageLed10.State = state ? LedState.NoSignaled : LedState.Signaled;
        }
        
        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._discretDataBase.RemoveStructQueries();
            this._device.Groups75.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._measureTrans.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopRead();
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._discretDataBase.LoadStructCycle();
                this._device.Groups75.LoadStructCycle();
                this._dateTime.LoadStructCycle();
                this._measureTrans.LoadStructCycle();
            }
            else
            {
                this._discretDataBase.RemoveStructQueries();
                this._device.Groups75.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._measureTrans.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void OnAnalogSignalsLoadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1801, "�������� �����������");
        }

        private void ConfirmSDTU(ushort address, string msg)
        {
            if (DialogResult.Yes ==
                MessageBox.Show(msg + " ?", "��5 v75", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                this._device.SetBit(this._device.DeviceNumber, address, true, msg + this._device.DeviceNumber,
                    this._device);
            }
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1800, "��������� �����������");
        }

        private void _resetFaultBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1805, "�������� �������������");
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1806, "�������� ��������� ������� �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1807, "�������� ��������� ������� ������");
        }

        private void _resetIndicatBut_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1804, "�������� ���������");
        }

        private void _selectConstraintGroupBut_Click(object sender, EventArgs e)
        {
            this._device.Groups75.Value.SetGroup(Common.VersionConverter(this._device.DeviceVersion), this._setpointsComboBox.SelectedItem.ToString());
            this._device.Groups75.SaveStruct();
        }

        private void _dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void _blockAvr_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1802, "����������� ���");
        }

        private void _resetAvr_Click(object sender, EventArgs e)
        {
            this.ConfirmSDTU(0x1803, "�������� ����������");
        }

        private void OnSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show("��������� �������� ��������������� ������ � ����������?", "������ ���",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1809, true, "������ ���", this._device);
        }

        private void OffSplBtnClock(object o, EventArgs e)
        {
            if (MessageBox.Show(
                "���������� �������� ��������������� ������ � ����������? ��������! ��� ����� �������� � ������ �� ������ ������ ������� ����������",
                "������� ���",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x1808, true, "������� ���", this._device);
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR5Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(MR5v75MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}