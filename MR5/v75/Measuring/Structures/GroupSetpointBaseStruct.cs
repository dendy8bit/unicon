﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR5.v75.Measuring.Structures
{
    public class GroupSetpointBaseStructV75 : StructBase
    {
        [Layout(0)] private ushort _group;

        public string GetGroup(double version)
        {
            if (version > 75)
            {
                return Validator.Get(this._group, StringsConfigV75.Groups);
            }
            return Validator.Get(this._group, StringsConfigV75.SetpointsNames);
        }

        public void SetGroup(double version, string value)
        {
            this._group = Validator.Set(value, version > 75 ? StringsConfigV75.Groups : StringsConfigV75.SetpointsNames);
        }
    }
}
