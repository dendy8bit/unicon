﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR5.v75.Measuring.Structures
{
    /// <summary>
    /// МР 550 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStruct75 :StructBase
    {
        #region [Private fields]
        //ток

        [Layout(0)]
        private ushort _in; // ток N
        [Layout(1)]
        private ushort _ia; // ток A
        [Layout(2)]
        private ushort _ib; // ток B
        [Layout(3)]
        private ushort _ic; // ток C
        [Layout(4)]
        private ushort _i0; // ток 0
        [Layout(5)]
        private ushort _i1; // ток 2
        [Layout(6)]
        private ushort _i2; // ток 2
        [Layout(7)]
        private ushort _ig; // ток Iг

        [Layout(8)]
        private ushort _un;
        [Layout(9)]
        private ushort _ua;
        [Layout(10)]
        private ushort _ub;
        [Layout(11)]
        private ushort _uc;
        [Layout(12)]
        private ushort _uab;
        [Layout(13)]
        private ushort _ubc;
        [Layout(14)]
        private ushort _uca;
        [Layout(15)]
        private ushort _u0;
        [Layout(16)]
        private ushort _u1;
        [Layout(17)]
        private ushort _u2;


        [Layout(18)]
        private ushort _f;
        [Layout(19)]
        private ushort _cos;
        [Layout(20)]
        private ushort _p;
        [Layout(21)]
        private ushort _q;
        [Layout(22)] 
        private ushort _OMP;
        [Layout(23)]
        private ushort _Uacc;

        #endregion [Private fields]
   
        #region [Public members]
      

        private ushort GetMean(List<AnalogDataBaseStruct75> list,Func<AnalogDataBaseStruct75, ushort> func)
        {
            var count = list.Count;

            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort) (sum/(double) count);
        }


        public string GetIa(List<AnalogDataBaseStruct75> list,MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIb(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIc(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI1(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI2(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetI0(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.Tt * 40);
        }
        public string GetIn(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }
        public string GetIg(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.Ttnp * 5);
        }

        public string GetCosF(List<AnalogDataBaseStruct75> list)
        {
            var value = this.GetMean(list, o => o._cos);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQMr575(value, 1000);
        }

        public string GetP(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetPMr575(value, 1000);
        }

        public string GetF(List<AnalogDataBaseStruct75> list)
        {
            var value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF(value);
        }


        public string GetUab(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUbc(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUca(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetU1(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetU2(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }

        public string GetUn(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tnnp);
        }

        public string GetUacc(List<AnalogDataBaseStruct75> list, MeasureTransStruct75 measure)
        {
            var value = this.GetMean(list, o => o._Uacc);
            return ValuesConverterCommon.Analog.GetU(value, measure.Tn);
        }
        #endregion [Public members]
        

    }
}
