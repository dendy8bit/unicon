﻿using BEMN.Devices;
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MBServer;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;
using BEMN.MR5.v75.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v75.Measuring.Structures;
using BEMN.MR5.v75.AlarmJournal.Structures;
using BEMN.MR5.v75.SystemJournal.Structures;
using BEMN.MBServer.Queries;
using BEMN.MR5.BSBGL;
using BEMN.MR5.v50.AlarmJournal.Structures;
using BEMN.MR5.v50.Configuration.Structures.Transformer;
using BEMN.MR5.v50.Measuring;
using BEMN.MR5.v50.Osc.Structures;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct;
using BEMN.MR5.v60.Configuration.Structures.ConfigStruct.OscConfig;
using BEMN.MR5.v60.Structs.MeasuringStructs;
using BEMN.MR5.v60.Structs.JournalStructs;
using BEMN.MR5.v60.Osc;
using BEMN.MR5.v60.Forms;
using BEMN.MR5.v70.AlarmJournal;
using BEMN.MR5.v70.AlarmJournal.Structures;
using BEMN.MR5.v70.Configuration;
using BEMN.MR5.v70.Configuration.Structures;
using BEMN.MR5.v70.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v70.Measuring;
using BEMN.MR5.v70.Measuring.Structures;
using BEMN.MR5.v70.Oscilloscope;
using BEMN.MR5.v70.Oscilloscope.Structures;
using BEMN.MR5.v70.SystemJournal;
using BEMN.MR5.v70.SystemJournal.Structures;
using BEMN.MR5.v50.AlarmJournal;
using BEMN.MR5.v50.Configuration;
using BEMN.MR5.v50.Configuration.Structures;
using BEMN.MR5.v50.Osc;
using BEMN.MR5.v50.SystemJournal;
using BEMN.MR5.v50.SystemJournal.Structures;
using BEMN.MR5.v55.AlarmJournal;
using BEMN.MR5.v55.AlarmJournal.Structures;
using BEMN.MR5.v55.Configuration;
using BEMN.MR5.v55.Configuration.Structures;
using BEMN.MR5.v55.Configuration.Structures.MeasuringTransformer;
using BEMN.MR5.v55.Measuring;
using BEMN.MR5.v55.Measuring.Structures;
using BEMN.MR5.v55.Osc;
using BEMN.MR5.v55.Osc.Structures;
using BEMN.MR5.v55.SystemJournal;
using BEMN.MR5.v55.SystemJournal.Structures;
using BEMN.MR5.v60.Measuring.Structures;
using BEMN.MR5.v70.Configuration.StructuresNew;
using BEMN.MR5.v75.AlarmJournal;
using BEMN.MR5.v75.Configuration;
using BEMN.MR5.v75.Configuration.StructuresNew;
using BEMN.MR5.v75.Measuring;
using BEMN.MR5.v75.Oscilloscope;
using BEMN.MR5.v75.Oscilloscope.Structures;
using BEMN.MR5.v75.SystemJournal;

namespace BEMN.MR5
{
    public class MR5Device : Device, IDeviceView, IDeviceVersion
    {
        #region [Fields]

        private const string SAVE_COMAND_CONFIRM = "SaveConfigConfirm";
        private const string READ_COMAND_CONFIRM = "ReadConfigConfirm";

        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private MemoryEntity<OneWordStruct> _changeGroup;

        #region MR5 v75
        private MemoryEntity<DateTimeStruct> _dateTime75;
        private MemoryEntity<MeasureTransStruct75> _measureTrans75;
        private MemoryEntity<DiscretDataBaseStruct75> _discretDataBase75;
        private MemoryEntity<GroupSetpointBaseStructV75> _groups75;
        private MemoryEntity<AnalogDataBaseStruct75> _analogDataBase75;
        private MemoryEntity<MeasureTransStruct75> _measureTransAj75;
        private MemoryEntity<AlarmJournalStruct75> _alarmRecord75;
        private MemoryEntity<ConfigurationStruct75> _configuration75;
        private MemoryEntity<ConfigurationStructV75new> _configuration75new;
        private MemoryEntity<SystemJournalStruct75> _systemJournal75;

        private MemoryEntity<MeasureTransStruct75> _oscTrans75;
        private MemoryEntity<OneWordStruct> _refreshOscJournalV75;
        private MemoryEntity<OscJournalStructV75> _oscJournal75;
        private MemoryEntity<OneWordStruct> _setStartPage75;
        private MemoryEntity<OscPage> _oscPage75;
        #endregion MR5 v75

        #region MR5 v50

        private MemoryEntity<AnalogDataBaseV50> _analogV50;
        private MemoryEntity<DiscretDataBaseV50> _discretV50;
        private MemoryEntity<MeasuringTransformerV50> _measuringV50;
        private MemoryEntity<DateTimeStruct> _dateTime50;

        private MemoryEntity<AlarmJournalStructV50> _alarmJournal50;
        private MemoryEntity<MeasuringTransformerV50> _measuringAlarmV50;

        private MemoryEntity<MeasuringTransformerV50> _transformerOscV50;
        private MemoryEntity<OneWordStruct> _refreshOscJournalV50;
        private MemoryEntity<OscJournalStructV50> _oscJournalV50;
        private MemoryEntity<SetOscStartPageStruct> _setOscPageV50;
        private MemoryEntity<OscPage> _oscPageV50;

        private MemoryEntity<SystemJournalStructV50> _systemJournal50;
        private MemoryEntity<ConfigurationStructV50> _configV50;
        private MemoryEntity<ConfigurationStructV50New> _configV50New;
        private MemoryEntity<GroupSetpointBaseStructV50> _groups50;
        #endregion MR5 v50

        #region MR5v55

        private MemoryEntity<AnalogDataBaseStructV55> _analogV55;
        private MemoryEntity<DiscretDataBaseStructV55> _discretV55;
        private MemoryEntity<MeasureTransStructV55> _measuringV55;
        private MemoryEntity<DateTimeStruct> _dateTime55;

        private MemoryEntity<AlarmJournalStructV55> _alarmJournal55;
        private MemoryEntity<MeasureTransStructV55> _measuringAlarmV55;
        private MemoryEntity<GroupSetpointBaseStructV55> _groupSetpointBase;

        private MemoryEntity<MeasureTransStructV55> _transformerOscV55;
        private MemoryEntity<OneWordStruct> _refreshOscJournalV55;
        private MemoryEntity<OscJournalStructV55> _oscJournalV55;
        private MemoryEntity<OneWordStruct> _setOscPageV55;
        private MemoryEntity<OscPage> _oscPageV55;

        private MemoryEntity<SystemJournalStructV55> _systemJournal55;
        private MemoryEntity<ConfigurationStructV55> _configV55;

        #endregion MR5 v50

        #region MR5 v60
        private MemoryEntity<RamMemoryStruct60> _ramMemory60;
        private MemoryEntity<DateTimeStruct> _dateTime60;
        private MemoryEntity<DiscretBdStruct60> _discretBd60;
        private MemoryEntity<RomMeasuringStruct60> _measuringChannel60;
        private MemoryEntity<RomMeasuringStruct60> _measuringChannel60Aj;
        private MemoryEntity<AnalogBdStruct60> _analogBd60;
        private MemoryEntity<SystemJournalStruct60> _systemJournal60;
        private MemoryEntity<AlarmJournalStruct60> _alarmRecord60;
        /// <summary>
        /// Записи журнала
        /// </summary>
        private MemoryEntity<OscJournalStruct60> _oscJournal60;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private MemoryEntity<OneWordStruct> _refreshOscJournal60;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        private MemoryEntity<OscConfigStruct60> _oscOptions60;
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private MemoryEntity<SetOscStartPageStruct> _setStartPage60;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private MemoryEntity<OscPage> _oscPage60;
        private MemoryEntity<RomMeasuringStruct60> _measuringChannel60Osc;
        #endregion

        #region MR5 v70
        private MemoryEntity<MeasureTransStructV70> _transformator70;
        private MemoryEntity<DateTimeStruct> _dateTime70;
        private MemoryEntity<DiscretDataBaseStructV70> _discret70;
        private MemoryEntity<GroupSetpointBaseStructV70> _groups70;
        private MemoryEntity<AnalogDataBaseStructV70> _analog70;
        private MemoryEntity<LinerAnalogBaseStruct> _linerAnalog;
        private MemoryEntity<LinerAnalogBaseStruct60> _linerAnalog60;
        private MemoryEntity<MeasureTransStructV70> _transformatorAj70;
        private MemoryEntity<AlarmJournalStructV70> _alarmJournal70;
        private MemoryEntity<SystemJournalStructV70> _systemJournal70;
        private MemoryEntity<ConfigurationStructV70> _configuration70;
        private MemoryEntity<ConfigurationStructV70New> _configuration70new;

        private MemoryEntity<MeasureTransStructV70> _oscTrans70;
        private MemoryEntity<OneWordStruct> _refreshOscJournalV70;
        private MemoryEntity<OscJournalStructV70> _oscJournal70;
        private MemoryEntity<SetOscStartPageStruct> _setStartPage70;
        private MemoryEntity<OscPage> _oscPage70;
        #endregion
        
        #endregion [Fields]
        
        #region [Properties]
        public MemoryEntity<OneWordStruct> ChangeGroup
        {
            get { return this._changeGroup; }
        }

        #region MR5 v75
        public MemoryEntity<DateTimeStruct> DateAndTime75
        {
            get { return this._dateTime75; }
        }

        public MemoryEntity<GroupSetpointBaseStructV75> Groups75
        {
            get { return this._groups75; }
        }
         
        public MemoryEntity<DiscretDataBaseStruct75> DiscretDataBase75
        {
            get { return this._discretDataBase75; }
        }

        public MemoryEntity<AnalogDataBaseStruct75> AnalogDataBase75
        {
            get { return this._analogDataBase75; }
        }

        public MemoryEntity<MeasureTransStruct75> MeasureTrans75
        {
            get { return this._measureTrans75; }
        }

        public MemoryEntity<MeasureTransStruct75> MeasureTransAj75
        {
            get { return this._measureTransAj75; }
        }

        public MemoryEntity<AlarmJournalStruct75> AlarmRecord75
        {
            get { return this._alarmRecord75; }
        }

        public MemoryEntity<SystemJournalStruct75> SystemJournal75
        {
            get { return this._systemJournal75; }
        }

        public MemoryEntity<ConfigurationStruct75> Configuration75
        {
            get { return this._configuration75; }
        }

        public MemoryEntity<ConfigurationStructV75new> Configuration75New
        {
            get { return this._configuration75new; }
        }

        public MemoryEntity<MeasureTransStruct75> OscTransformator75
        {
            get { return this._oscTrans75; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournalV75
        {
            get { return this._refreshOscJournalV75; }
        }

        public MemoryEntity<OscJournalStructV75> OscJournal75
        {
            get { return this._oscJournal75; }
        }

        public MemoryEntity<OneWordStruct> SetStartPage75
        {
            get { return this._setStartPage75; }
        }

        public MemoryEntity<OscPage> OscPage75
        {
            get { return this._oscPage75; }
        }
        #endregion

        #region MR5 v50

        public MemoryEntity<AnalogDataBaseV50> AnalogV50
        {
            get { return this._analogV50; }
        }

        public MemoryEntity<DiscretDataBaseV50> DiscretV50
        {
            get { return this._discretV50; }
        }

        public MemoryEntity<MeasuringTransformerV50> MeasuringV50
        {
            get { return this._measuringV50; }
        }

        public MemoryEntity<AlarmJournalStructV50> AlarmJurnalV50
        {
            get { return this._alarmJournal50; }
        }
        public MemoryEntity<MeasuringTransformerV50> MeasuringAlarmV50
        {
            get { return this._measuringAlarmV50; }
        }
        
        public MemoryEntity<DateTimeStruct> DateTimeV50
        {
            get { return this._dateTime50; }
        }

        public MemoryEntity<MeasuringTransformerV50> TransformerOscV50
        {
            get { return this._transformerOscV50; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournalV50
        {
            get { return this._refreshOscJournalV50; }
        }

        public MemoryEntity<OscJournalStructV50> OscJournalV50
        {
            get { return this._oscJournalV50; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscPageV50
        {
            get { return this._setOscPageV50; }
        }

        public MemoryEntity<OscPage> OscPageV50
        {
            get { return this._oscPageV50; }
        }
        
        public MemoryEntity<ConfigurationStructV50> ConfigV50
        {
            get { return this._configV50; }
        }

        public MemoryEntity<ConfigurationStructV50New> ConfigV50New
        {
            get { return this._configV50New; }
        }

        public MemoryEntity<SystemJournalStructV50> SystemJournal50
        {
            get { return this._systemJournal50; }
        }

        public MemoryEntity<GroupSetpointBaseStructV50> Groups50
        {
            get { return this._groups50; }
        }
        #endregion

        #region MR5v55

        public MemoryEntity<AnalogDataBaseStructV55> AnalogV55
        {
            get { return this._analogV55; }
        }

        public MemoryEntity<DiscretDataBaseStructV55> DiscretV55
        {
            get { return this._discretV55; }
        }

        public MemoryEntity<MeasureTransStructV55> MeasuringV55
        {
            get { return this._measuringV55; }
        }

        public MemoryEntity<DateTimeStruct> DateTime55
        {
            get { return this._dateTime55; }
        }

        public MemoryEntity<GroupSetpointBaseStructV55> GroupSetpointBaseV55
        {
            get { return this._groupSetpointBase; }
        }

        public MemoryEntity<AlarmJournalStructV55> AlarmJournal55
        {
            get { return this._alarmJournal55; }
        }

        public MemoryEntity<MeasureTransStructV55> MeasuringAlarmV55
        {
            get { return this._measuringAlarmV55; }
        }

        public MemoryEntity<MeasureTransStructV55> TransformerOscV55
        {
            get { return this._transformerOscV55; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournalV55
        {
            get { return this._refreshOscJournalV55; }
        }

        public MemoryEntity<OscJournalStructV55> OscJournalV55
        {
            get { return this._oscJournalV55; }
        }

        public MemoryEntity<OneWordStruct> SetOscPageV55
        {
            get { return this._setOscPageV55; }
        }

        public MemoryEntity<OscPage> OscPageV55
        {
            get { return this._oscPageV55;}
        }

        public MemoryEntity<SystemJournalStructV55> SystemJournal55
        {
            get { return this._systemJournal55; }
        }

        public MemoryEntity<ConfigurationStructV55> ConfigurationV55
        {
            get { return this._configV55; }
        }

        #endregion MR5 v50

        #region MR5 v60

        public MemoryEntity<RamMemoryStruct60> RamMemory60
        {
            get { return _ramMemory60; }
        }

        public MemoryEntity<DateTimeStruct> DateTime60
        {
            get { return _dateTime60; }
        }

        public MemoryEntity<DiscretBdStruct60> DiscretBd60
        {
            get { return _discretBd60; }
        }
        
        public MemoryEntity<RomMeasuringStruct60> MeasuringChannel60
        {
            get { return _measuringChannel60; }
        }

        public MemoryEntity<RomMeasuringStruct60> MeasuringChannel60Aj
        {
            get { return _measuringChannel60Aj; }
        }

        public MemoryEntity<AnalogBdStruct60> AnalogBd60
        {
            get { return _analogBd60; }
        }

        public MemoryEntity<SystemJournalStruct60> SystemJournal60
        {
            get { return _systemJournal60; }
        }

        public MemoryEntity<AlarmJournalStruct60> AlarmRecord60
        {
            get { return _alarmRecord60; }
        }
        
        public MemoryEntity<OscJournalStruct60> OscJournal60
        {
            get { return _oscJournal60; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournal60
        {
            get { return _refreshOscJournal60; }
        }

        public MemoryEntity<OscConfigStruct60> OscOptions60
        {
            get { return _oscOptions60; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetStartPage60
        {
            get { return _setStartPage60; }
        }

        public MemoryEntity<OscPage> OscPage60
        {
            get { return _oscPage60; }
        }

        public MemoryEntity<RomMeasuringStruct60> MeasuringChannel60Osc
        {
            get { return this._measuringChannel60Osc; }
        }


        #endregion

        #region MR5 v70

        public MemoryEntity<MeasureTransStructV70> Transformator70
        {
            get { return this._transformator70; }
        }

        public MemoryEntity<DateTimeStruct> DateTime70
        {
            get { return this._dateTime70; }
        }

        public MemoryEntity<DiscretDataBaseStructV70> Discrets70
        {
            get { return this._discret70; }
        }

        public MemoryEntity<GroupSetpointBaseStructV70> Groups70
        {
            get { return this._groups70; }
        }

        public MemoryEntity<AnalogDataBaseStructV70> Analog70
        {
            get { return this._analog70; }
        }

        public MemoryEntity<LinerAnalogBaseStruct> LinerAnalogSignals
        {
            get { return this._linerAnalog; }
        }

        public MemoryEntity<LinerAnalogBaseStruct60> LinerAnalogSignals60
        {
            get { return this._linerAnalog60; }
        }

        public MemoryEntity<MeasureTransStructV70> TransformatorAJ70
        {
            get { return this._transformatorAj70; }
        }

        public MemoryEntity<AlarmJournalStructV70> AlarmJournalV70
        {
            get { return this._alarmJournal70; }
        }

        public MemoryEntity<SystemJournalStructV70> SystemJournal70
        {
            get { return this._systemJournal70; }
        }

        public MemoryEntity<ConfigurationStructV70> Configuration70
        {
            get { return this._configuration70; }
        }

        public MemoryEntity<ConfigurationStructV70New> Configuration70New
        {
            get { return this._configuration70new; }
        }

        public MemoryEntity<MeasureTransStructV70> OscTransformator70
        {
            get { return this._oscTrans70; }
        }

        public MemoryEntity<OneWordStruct> RefreshOscJournalV70
        {
            get { return this._refreshOscJournalV70; }
        }

        public MemoryEntity<OscJournalStructV70> OscJournal70
        {
            get { return this._oscJournal70; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetStartPage70
        {
            get { return this._setStartPage70; }
        }

        public MemoryEntity<OscPage> OscPage70
        {
            get { return this._oscPage70; }
        }
        #endregion

        #endregion [Properties]

        #region [Constructor]
        public MR5Device()
        {
            HaveVersion = true;
        }

        public MR5Device(Modbus mb)
        {
            HaveVersion = true;
            DefaultTimeSpan = new TimeSpan(0, 0, 0, 0, 10);
            this.MB = mb;
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.OnCompleteExchange;
            }
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Сохранить конфигурацию")
            {
                MessageBox.Show(query.error ? ERROR_SAVE_CONFIG : WRITE_OK);
            }

            mb_CompleteExchange(sender, query);
        }

        #endregion

        #region Programming

        private MemoryEntity<SourceProgramStruct> _sourceProgram;
        private MemoryEntity<StartStruct> _programStart;
        private MemoryEntity<ProgramStorageStruct> _programStorage;
        private MemoryEntity<ProgramSignalsStruct> _programSignals;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return this._programStorage; }
        }

        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStart; }
        }

        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignals; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgram; }
        }

        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return this._stopSpl; }
        }
        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return this._startSpl; }
        }
        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return this._stateSpl; }
        }

        #endregion

        #region [Methods]


        internal void WriteConfiguration()
        {
            SetBit(DeviceNumber, 0x0, true, SAVE_COMAND_CONFIRM+DeviceVersion, this, DefaultTimeSpan);
        }
        internal void WriteConfiguration(TimeSpan span)
        {
            SetBit(DeviceNumber, 0x0, true, SAVE_COMAND_CONFIRM + DeviceVersion, this, span);
        }

        internal void ReadConfiguration()
        {
            SetBit(DeviceNumber, 0x0, false, READ_COMAND_CONFIRM + DeviceVersion, this, DefaultTimeSpan);
        }
        internal void ReadConfiguration(TimeSpan span)
        {
            SetBit(DeviceNumber, 0x0, false, READ_COMAND_CONFIRM + DeviceVersion, this, span);
        }

        private void InitCommonStructs()
        {
            this._changeGroup = new MemoryEntity<OneWordStruct>("Переключение групп уставок", this, 0x400);
            this._sourceProgram = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
            this._programStart = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
            this._programStorage = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this._programSignals = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals", this, 0xA000);
            this._stopSpl = new MemoryEntity<OneWordStruct>("StopSpl", this, 0x1808);
            this._startSpl = new MemoryEntity<OneWordStruct>("StartSpl", this, 0x1809);
            this._stateSpl = new MemoryEntity<OneWordStruct>("StateSpl", this, 0x1805);
        }

        private void InitMemoryV50()
        {
            this._configV50 = new MemoryEntity<ConfigurationStructV50>("Конфигурация v50", this, 0x1000);
            this._configV50New = new MemoryEntity<ConfigurationStructV50New>("Конфигурация v50", this, 0x1000);
            this._analogV50 = new MemoryEntity<AnalogDataBaseV50>("Аналоговые сигналы v50", this, 0x1900);
            this._discretV50 = new MemoryEntity<DiscretDataBaseV50>("Дискретные сигналы v50", this, 0x1800);
            this._measuringV50 = new MemoryEntity<MeasuringTransformerV50>("Измерительный трансформатор (измерения) v50", this, 0x1000);
            this._dateTime50 = new MemoryEntity<DateTimeStruct>("Дата и время v50", this, 0x200);
            this._groups50 = new MemoryEntity<GroupSetpointBaseStructV50>("Группа уставок v50", this, 0x400);
            this._alarmJournal50 = new MemoryEntity<AlarmJournalStructV50>("ЖА V50", this, 0x2800);
            this._measuringAlarmV50 = new MemoryEntity<MeasuringTransformerV50>("Измерительный трансформатор (ЖА) v50", this, 0x1000);
            this._transformerOscV50 = new MemoryEntity<MeasuringTransformerV50>("Измерительный трансформатор (осц-ф) v50", this, 0x1000);
            this._refreshOscJournalV50 = new MemoryEntity<OneWordStruct>("Сброс журнала осциллографа v50", this, 0x800);
            this._oscJournalV50 = new MemoryEntity<OscJournalStructV50>("Журнал осциллографа v50", this, 0x800);
            this._setOscPageV50 = new MemoryEntity<SetOscStartPageStruct>("Номер страницы осциллограммы v50", this, 0x900);
            this._oscPageV50 = new MemoryEntity<OscPage>("Страница осциллограммы v50", this, 0x900);
            this._systemJournal50 = new MemoryEntity<SystemJournalStructV50>("ЖС v50", this, 0x2000);
        }

        private void InitMemoryV55()
        {
            this._configV55 = new MemoryEntity<ConfigurationStructV55>("Конфигурация v55", this, 0x1000);
            this._analogV55 = new MemoryEntity<AnalogDataBaseStructV55>("Аналоговые сигналы v55", this, 0x1900);
            this._discretV55 = new MemoryEntity<DiscretDataBaseStructV55>("Дискретные сигналы v55", this, 0x1800);
            this._groupSetpointBase = new MemoryEntity<GroupSetpointBaseStructV55>("Группа уставок", this, 0x400);
            this._measuringV55 = new MemoryEntity<MeasureTransStructV55>("Измерительный трансформатор (измерения) v55", this, 0x1000);
            this._dateTime55 = new MemoryEntity<DateTimeStruct>("Дата и время v55", this, 0x200);
            this._alarmJournal55 = new MemoryEntity<AlarmJournalStructV55>("ЖА V55", this, 0x2800);
            this._measuringAlarmV55 = new MemoryEntity<MeasureTransStructV55>("Измерительный трансформатор (ЖА) v55", this, 0x1000);
            this._transformerOscV55 = new MemoryEntity<MeasureTransStructV55>("Измерительный трансформатор (осц-ф) v55", this, 0x1000);
            this._refreshOscJournalV55 = new MemoryEntity<OneWordStruct>("Сброс журнала осциллографа v55", this, 0x800);
            this._oscJournalV55 = new MemoryEntity<OscJournalStructV55>("Журнал осциллографа v55", this, 0x800);
            this._setOscPageV55 = new MemoryEntity<OneWordStruct>("Номер страницы осциллограммы v55", this, 0x900);
            this._oscPageV55 = new MemoryEntity<OscPage>("Страница осциллограммы v55", this, 0x900);
            this._systemJournal55 = new MemoryEntity<SystemJournalStructV55>("ЖС v55", this, 0x2000);
        }

        private void InitMemoryV60()
        {
            this._ramMemory60 = new MemoryEntity<RamMemoryStruct60>("Конфигурация v60", this, 0x1000);
            this._dateTime60 = new MemoryEntity<DateTimeStruct>("Дата и время v60", this, 0x200);
            this._discretBd60 = new MemoryEntity<DiscretBdStruct60>("Дискретные сигналы v60", this, 0x1800);
            this._measuringChannel60 = new MemoryEntity<RomMeasuringStruct60>("Измерительный канал (измерения) v60", this, 0x1000);
            this._analogBd60 = new MemoryEntity<AnalogBdStruct60>("Аналоговые сигналы v60", this, 0x1900);
            this._systemJournal60 = new MemoryEntity<SystemJournalStruct60>("ЖС V60", this, 0x2000);
            this._alarmRecord60 = new MemoryEntity<AlarmJournalStruct60>("ЖА V60", this, 0x2800);
            this._measuringChannel60Aj = new MemoryEntity<RomMeasuringStruct60>("Измерительный канал (ЖА) v60", this, 0x1000);
            this._oscJournal60 = new MemoryEntity<OscJournalStruct60>("Журнал Осц v60", this, 0x800);
            this._linerAnalog60 = new MemoryEntity<LinerAnalogBaseStruct60>("Линейная АБД v60", this, 0x1B00);
            this._refreshOscJournal60 = new MemoryEntity<OneWordStruct>("Сброс Журнала Осц v60", this, 0x800);
            this._oscOptions60 = new MemoryEntity<OscConfigStruct60>("Конфигурация осциллографа v60", this, 0x1274);
            this._setStartPage60 = new MemoryEntity<SetOscStartPageStruct>("Установка страницы осциллографа v60", this, 0x900);
            this._oscPage60 = new MemoryEntity<OscPage>("Чтение страницы осциллографа v60", this, 0x900);
            this._measuringChannel60Osc = new MemoryEntity<RomMeasuringStruct60>("Измерительный канал (Осциллограф) v60", this, 0x1000);
        }

        private void InitMemoryV70()
        {
            this._configuration70 = new MemoryEntity<ConfigurationStructV70>("Конфигурация v70", this, 0x1000);
            this._configuration70new = new MemoryEntity<ConfigurationStructV70New>("Конфигурация V70 new", this, 0x1000);
            this._systemJournal70 = new MemoryEntity<SystemJournalStructV70>("ЖС v70", this, 0x2000);
            this._alarmJournal70 = new MemoryEntity<AlarmJournalStructV70>("ЖА V70", this, 0x2800);
            this._transformatorAj70 = new MemoryEntity<MeasureTransStructV70>("Параметры измерений ЖА v70", this, 0x1000);
            this._dateTime70 = new MemoryEntity<DateTimeStruct>("DateAndTime v70", this, 0x200);
            this._groups70 = new MemoryEntity<GroupSetpointBaseStructV70>("Группа уставок v70", this, 0x400);
            this._discret70 = new MemoryEntity<DiscretDataBaseStructV70>("Дискретная БД v70", this, 0x1800);
            this._analog70 = new MemoryEntity<AnalogDataBaseStructV70>("Аналоговая БД v70", this, 0x1900);
            this._transformator70 = new MemoryEntity<MeasureTransStructV70>("Параметры измерений v70", this, 0x1000);
            this._linerAnalog = new MemoryEntity<LinerAnalogBaseStruct>("Линейная АБД v70", this, 0x1b00);
            this._oscTrans70 = new MemoryEntity<MeasureTransStructV70>("Параметры измерений ЖО v70", this, 0x1000);
            this._refreshOscJournalV70 = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа v70", this, 0x800);
            this._oscJournal70 = new MemoryEntity<OscJournalStructV70>("Журнал осциллографа v70", this, 0x800);
            this._setStartPage70 = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы v70", this, 0x900);
            this._oscPage70 = new MemoryEntity<OscPage>("Страница осциллографа v70", this, 0x900);
        }

        private void InitMemoryV75()
        {
            this._configuration75 = new MemoryEntity<ConfigurationStruct75>("Конфигурация v75", this, 0x1000);
            this._configuration75new = new MemoryEntity<ConfigurationStructV75new>("Конфигурация V75 new", this, 0x1000);
            this._systemJournal75 = new MemoryEntity<SystemJournalStruct75>("ЖС v75", this, 0x2000);
            this._alarmRecord75 = new MemoryEntity<AlarmJournalStruct75>("ЖА V75", this, 0x2800);
            this._measureTransAj75 = new MemoryEntity<MeasureTransStruct75>("Параметры измерений ЖА v75", this, 0x1000);
            this._dateTime75 = new MemoryEntity<DateTimeStruct>("DateAndTime v75", this, 0x200);
            this._groups75 = new MemoryEntity<GroupSetpointBaseStructV75>("Группа уставок v75", this, 0x400);
            this._discretDataBase75 = new MemoryEntity<DiscretDataBaseStruct75>("Дискретная БД v75", this, 0x1800);
            this._analogDataBase75 = new MemoryEntity<AnalogDataBaseStruct75>("Аналоговая БД v75", this, 0x1900);
            this._measureTrans75 = new MemoryEntity<MeasureTransStruct75>("Параметры измерений v75", this, 0x1000);

            this._oscTrans75 = new MemoryEntity<MeasureTransStruct75>("Параметры измерений ЖО v75", this, 0x1000);
            this._refreshOscJournalV75 = new MemoryEntity<OneWordStruct>("Обновление журнала осциллографа v75", this, 0x800);
            this._oscJournal75 = new MemoryEntity<OscJournalStructV75>("Журнал осциллографа v75", this, 0x800);
            this._setStartPage75 = new MemoryEntity<OneWordStruct>("Установка стартовой страницы осциллограммы v75", this, 0x900);
            this._oscPage75 = new MemoryEntity<OscPage>("Страница осциллографа v75", this, 0x900);
        }
        
        private void OnCompleteExchange(object sender, Query query)
        {
            if (query.name == SAVE_COMAND_CONFIRM+DeviceVersion)
            {
                if (query.error)
                {
                    MessageBox.Show("Ошибка записи конфигурации", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Запись конфигурации", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            mb_CompleteExchange(sender, query);
        }
        #endregion

        #region IDeviceViewMembers

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(MR5Device); }
        }

        public bool Deletable
        {
            get { return true; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr5; }
        }

        public string NodeName
        {
            get { return "МР5"; }
        }
        #endregion IDeviceViewMembers

        #region [IDeviceVersion]

        public Type[] Forms
        {
            get
            {
                this.InitCommonStructs();

                double ver = Common.VersionConverter(DeviceVersion);
                
                //StringsConfig.Version = Common.VersionConverter(DeviceVersion);

                if (ver >= 50 && ver < 55)
                {
                    this.InitMemoryV50();
                    return new[]
                    {
                        typeof (MeasuringFormV50),
                        typeof (Mr5V50SystemJournalForm),
                        typeof (Mr5OscilloscopeFormV50),
                        typeof (MR5v50AlarmJournalForm),
                        typeof (MR5V50ConfigurationForm),
                        typeof (Bsbglef)
                    };
                }
                if (ver >= 55 && ver < 60)
                {
                    this.InitMemoryV55();
                    return new[]
                    {
                        typeof (Mr5V55ConfigurationForm),
                        typeof (Mr5V55SystemJournalForm),
                        typeof (Mr5OscilloscopeFormV55),
                        typeof (Mr5V55AlarmJournalForm),
                        typeof (Mr5V55MeasuringForm),
                        typeof (Bsbglef)
                    };
                }
                if (ver >= 60 && ver < 70)
                {
                    this.InitMemoryV60();
                    return new[]
                    {
                        typeof (Mr5OscilloscopeFormV60),
                        typeof (Mr5AlarmJournalFormV60),
                        typeof (Mr5SystemJournalFormV60),
                        typeof (Mr5MeasuringFormV60),
                        typeof (Mr5ConfigurationFormV60),
                        typeof (Bsbglef)
                    };
                }
                
                if (ver >= 70 && ver < 75)
                {
                    this.InitMemoryV70();
                    return new[]
                    {
                        typeof (Mr5V70OscilloscopeForm),
                        typeof (Mr5V70AlarmJournalForm),
                        typeof (Mr5V70SystemJournalForm),
                        typeof (Mr5V70MeasuringForm),
                        typeof (Mr5V70ConfigurationForm),
                        typeof (Bsbglef)
                    };
                }
                
                this.InitMemoryV75();
                return new[]
                {
                    typeof (Mr5V75OscilloscopeForm),
                    typeof (MR5v75AlarmJournalForm),
                    typeof (MR5v75SystemJournalForm),
                    typeof (MR5v75MeasuringForm),
                    typeof (MR5v75ConfigurationForm),
                    typeof (Bsbglef)
                };

            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "50.00",
                    "50.01",
                    "50.02",
                    "50.03",
                    "50.04",
                    "50.05",
                    "50.06",
                    "55.00",
                    "60.00",
                    "60.01",
                    "60.02",
                    "60.03",
                    "60.04",
                    "60.05",
                    "70.00",
                    "70.01",
                    "70.02",
                    "70.03",
                    "70.04",
                    "70.05",
                    "70.06",
                    "70.07",
                    "75.00",
                    "75.01",
                    "75.02",
                    "75.03",
                    "75.04"
                };
            }
        }


        #endregion

    }
}
