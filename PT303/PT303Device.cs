﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Interfaces;
using BEMN.MBServer;

namespace PT303
{
    public class PT303Device : Device, IDeviceView
    {
        public PT303Device()
        {
            HaveVersion = false;
        }

        public PT303Device(Modbus mb)
        {
            MB = mb;
        }

        #region IDeviceViewMembers
        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(PT303Device); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return BEMN.Framework.Properties.Resources.tez; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "ПТ303"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }
        #endregion
    }
}
