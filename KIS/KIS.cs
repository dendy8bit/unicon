﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.KIS.Calibration.Structures;
using BEMN.MBServer;

namespace BEMN.KIS
{
    public class KIS : Device, IDeviceView
    {
        public MemoryEntity<AllCoeffStruct> AllCoeff { get; private set; }
        public MemoryEntity<CalibrateStruct> Clear { get; private set; }
        public MemoryEntity<FactorStruct> Channel3 { get; private set; }
        public MemoryEntity<FactorStruct> Channel2 { get; private set; }
        public MemoryEntity<FactorStruct> Channel1 { get; private set; }
        public MemoryEntity<CalibrateStruct> Calibrate { get; private set; }
        public MemoryEntity<SignalsStruct> Signals { get; private set; }
        public MemoryEntity<SignalsStruct> SignalsForShow { get; private set; }
        public MemoryEntity<OneWordStruct> Koeff110 { get; private set; }
        public MemoryEntity<OneWordStruct> CurrentNominal { get; private set; }
        public MemoryEntity<StateStruct> StateKis { get; private set; }


        public KIS(){}

        public KIS(Modbus mb) /*: base(mb)*/
        {
            this.MB = mb;
            InitMemoryEntity();
        }

        private void InitMemoryEntity()
        {
            this.AllCoeff = new MemoryEntity<AllCoeffStruct>("Все коэффициенты", this, 0x110);
            this.Clear = new MemoryEntity<CalibrateStruct>("Очистка коэффициентов", this, 0x110);
            this.Channel3 = new MemoryEntity<FactorStruct>("Канал3", this, 0x110);
            this.Channel2 = new MemoryEntity<FactorStruct>("Канал2", this, 0x113);
            this.Channel1 = new MemoryEntity<FactorStruct>("Канал1", this, 0x116);
            this.Signals = new MemoryEntity<SignalsStruct>("Сигналы", this, 0x0);
            this.SignalsForShow = new MemoryEntity<SignalsStruct>("Отображение показаний прибора", this, 0x0);
            this.Calibrate = new MemoryEntity<CalibrateStruct>("Калибровка", this, 0x110);
            this.Koeff110 = new MemoryEntity<OneWordStruct>("Проверка на новую плату", this, 0x110);
            this.CurrentNominal = new MemoryEntity<OneWordStruct>("Номинальный ток импульса", this, 0x11F);
            this.StateKis = new MemoryEntity<StateStruct>("Состояния", this, 0x5);
        }


        public Type[] Forms
        {
            get
            {            
                return new[]
                {
                    typeof(Calibration.Calibration)
                };
            }
        }

        #region [INodeView Members]

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType => typeof(KIS);

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow => false;

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage => Resources.KIS;

        [Browsable(false)]
        public string NodeName => "КИС";

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes => new INodeView[] { };

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable => true;

        #endregion [INodeView Members]

        [XmlIgnore]
        public sealed override Modbus MB
        {
            get => mb;
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += mb_CompleteExchange;
                }
            }
        }      
    }
}