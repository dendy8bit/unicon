﻿using System.ComponentModel;
using System.Windows.Forms;

namespace BEMN.KIS.Calibration
{
    partial class Calibration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._nullCoeffButtonBut = new System.Windows.Forms.Button();
            this._readCalibrateBut = new System.Windows.Forms.Button();
            this._loadFileBut = new System.Windows.Forms.Button();
            this._saveFileBut = new System.Windows.Forms.Button();
            this._numericSamples = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this._coeffListHex = new System.Windows.Forms.ListBox();
            this._coeffListDec = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._i3TextBox = new System.Windows.Forms.MaskedTextBox();
            this._i2TextBox = new System.Windows.Forms.MaskedTextBox();
            this._i1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._setI3But = new System.Windows.Forms.Button();
            this._setI2But = new System.Windows.Forms.Button();
            this._setI1But = new System.Windows.Forms.Button();
            this._currentLabel = new System.Windows.Forms.Label();
            this._averageValueLabel = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._averageLabel = new System.Windows.Forms.Label();
            this._currentValueLabel = new System.Windows.Forms.Label();
            this._saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this._calibrationProgressBar = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._address2 = new System.Windows.Forms.Label();
            this._address1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._address0 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._writeNominalBut = new System.Windows.Forms.Button();
            this._readNominalBut = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this._nominalCurrentImpuls = new System.Windows.Forms.MaskedTextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._address5 = new System.Windows.Forms.Label();
            this._address6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._address7 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._numericSamples)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._nullCoeffButtonBut);
            this.groupBox4.Controls.Add(this._readCalibrateBut);
            this.groupBox4.Controls.Add(this._loadFileBut);
            this.groupBox4.Controls.Add(this._saveFileBut);
            this.groupBox4.Location = new System.Drawing.Point(265, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(181, 150);
            this.groupBox4.TabIndex = 212;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Панель калибровок";
            // 
            // _nullCoeffButtonBut
            // 
            this._nullCoeffButtonBut.Location = new System.Drawing.Point(9, 19);
            this._nullCoeffButtonBut.Name = "_nullCoeffButtonBut";
            this._nullCoeffButtonBut.Size = new System.Drawing.Size(163, 23);
            this._nullCoeffButtonBut.TabIndex = 2;
            this._nullCoeffButtonBut.Text = "Обнулить коэффициенты";
            this._nullCoeffButtonBut.UseVisualStyleBackColor = true;
            this._nullCoeffButtonBut.Click += new System.EventHandler(this._nullCoeffBut_Click);
            // 
            // _readCalibrateBut
            // 
            this._readCalibrateBut.Location = new System.Drawing.Point(9, 48);
            this._readCalibrateBut.Name = "_readCalibrateBut";
            this._readCalibrateBut.Size = new System.Drawing.Size(163, 23);
            this._readCalibrateBut.TabIndex = 208;
            this._readCalibrateBut.Text = "Прочитать калибровки";
            this._readCalibrateBut.UseVisualStyleBackColor = true;
            this._readCalibrateBut.Click += new System.EventHandler(this._readCalibrateBut_Click);
            // 
            // _loadFileBut
            // 
            this._loadFileBut.Location = new System.Drawing.Point(9, 116);
            this._loadFileBut.Name = "_loadFileBut";
            this._loadFileBut.Size = new System.Drawing.Size(163, 23);
            this._loadFileBut.TabIndex = 2;
            this._loadFileBut.Text = "Загрузить из файла";
            this._loadFileBut.UseVisualStyleBackColor = true;
            this._loadFileBut.Click += new System.EventHandler(this._loadFileBut_Click);
            // 
            // _saveFileBut
            // 
            this._saveFileBut.Location = new System.Drawing.Point(9, 87);
            this._saveFileBut.Name = "_saveFileBut";
            this._saveFileBut.Size = new System.Drawing.Size(163, 23);
            this._saveFileBut.TabIndex = 2;
            this._saveFileBut.Text = "Сохранить в файл";
            this._saveFileBut.UseVisualStyleBackColor = true;
            this._saveFileBut.Click += new System.EventHandler(this._saveFileBut_Click);
            // 
            // _numericSamples
            // 
            this._numericSamples.Location = new System.Drawing.Point(132, 123);
            this._numericSamples.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this._numericSamples.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this._numericSamples.Name = "_numericSamples";
            this._numericSamples.ReadOnly = true;
            this._numericSamples.Size = new System.Drawing.Size(40, 20);
            this._numericSamples.TabIndex = 217;
            this._numericSamples.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Количество выборок:";
            // 
            // _coeffListHex
            // 
            this._coeffListHex.FormattingEnabled = true;
            this._coeffListHex.Location = new System.Drawing.Point(12, 168);
            this._coeffListHex.Name = "_coeffListHex";
            this._coeffListHex.Size = new System.Drawing.Size(247, 95);
            this._coeffListHex.TabIndex = 209;
            // 
            // _coeffListDec
            // 
            this._coeffListDec.FormattingEnabled = true;
            this._coeffListDec.Location = new System.Drawing.Point(265, 168);
            this._coeffListDec.Name = "_coeffListDec";
            this._coeffListDec.Size = new System.Drawing.Size(181, 95);
            this._coeffListDec.TabIndex = 218;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._numericSamples);
            this.groupBox1.Controls.Add(this._i3TextBox);
            this.groupBox1.Controls.Add(this._i2TextBox);
            this.groupBox1.Controls.Add(this._i1TextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._setI3But);
            this.groupBox1.Controls.Add(this._setI2But);
            this.groupBox1.Controls.Add(this._setI1But);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 150);
            this.groupBox1.TabIndex = 200;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Калибровки";
            // 
            // _i3TextBox
            // 
            this._i3TextBox.Location = new System.Drawing.Point(70, 96);
            this._i3TextBox.Mask = "000";
            this._i3TextBox.Name = "_i3TextBox";
            this._i3TextBox.PromptChar = ' ';
            this._i3TextBox.Size = new System.Drawing.Size(73, 20);
            this._i3TextBox.TabIndex = 222;
            this._i3TextBox.Text = "100";
            // 
            // _i2TextBox
            // 
            this._i2TextBox.Location = new System.Drawing.Point(70, 67);
            this._i2TextBox.Mask = "000";
            this._i2TextBox.Name = "_i2TextBox";
            this._i2TextBox.PromptChar = ' ';
            this._i2TextBox.Size = new System.Drawing.Size(73, 20);
            this._i2TextBox.TabIndex = 221;
            this._i2TextBox.Text = "100";
            // 
            // _i1TextBox
            // 
            this._i1TextBox.Location = new System.Drawing.Point(70, 37);
            this._i1TextBox.Mask = "000";
            this._i1TextBox.Name = "_i1TextBox";
            this._i1TextBox.PromptChar = ' ';
            this._i1TextBox.Size = new System.Drawing.Size(73, 20);
            this._i1TextBox.TabIndex = 220;
            this._i1TextBox.Text = "100";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(66, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "% от максимума шкалы";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "КИС - 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "КИС - 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "КИС - 1";
            // 
            // _setI3But
            // 
            this._setI3But.Location = new System.Drawing.Point(165, 94);
            this._setI3But.Name = "_setI3But";
            this._setI3But.Size = new System.Drawing.Size(75, 23);
            this._setI3But.TabIndex = 2;
            this._setI3But.Text = "Установить";
            this._setI3But.UseVisualStyleBackColor = true;
            this._setI3But.Click += new System.EventHandler(this._setI3But_Click);
            // 
            // _setI2But
            // 
            this._setI2But.Location = new System.Drawing.Point(165, 65);
            this._setI2But.Name = "_setI2But";
            this._setI2But.Size = new System.Drawing.Size(75, 23);
            this._setI2But.TabIndex = 2;
            this._setI2But.Text = "Установить";
            this._setI2But.UseVisualStyleBackColor = true;
            this._setI2But.Click += new System.EventHandler(this._setI2But_Click);
            // 
            // _setI1But
            // 
            this._setI1But.Location = new System.Drawing.Point(165, 36);
            this._setI1But.Name = "_setI1But";
            this._setI1But.Size = new System.Drawing.Size(75, 23);
            this._setI1But.TabIndex = 0;
            this._setI1But.Text = "Установить";
            this._setI1But.UseVisualStyleBackColor = true;
            this._setI1But.Click += new System.EventHandler(this._setI1But_Click);
            // 
            // _currentLabel
            // 
            this._currentLabel.AutoSize = true;
            this._currentLabel.Location = new System.Drawing.Point(115, 28);
            this._currentLabel.Name = "_currentLabel";
            this._currentLabel.Size = new System.Drawing.Size(13, 13);
            this._currentLabel.TabIndex = 213;
            this._currentLabel.Text = "0";
            // 
            // _averageValueLabel
            // 
            this._averageValueLabel.AutoSize = true;
            this._averageValueLabel.Location = new System.Drawing.Point(6, 56);
            this._averageValueLabel.Name = "_averageValueLabel";
            this._averageValueLabel.Size = new System.Drawing.Size(103, 13);
            this._averageValueLabel.TabIndex = 214;
            this._averageValueLabel.Text = "Среднее значение:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._averageLabel);
            this.groupBox5.Controls.Add(this._currentValueLabel);
            this.groupBox5.Controls.Add(this._averageValueLabel);
            this.groupBox5.Controls.Add(this._currentLabel);
            this.groupBox5.Location = new System.Drawing.Point(265, 269);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(181, 88);
            this.groupBox5.TabIndex = 216;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Измерения";
            // 
            // _averageLabel
            // 
            this._averageLabel.AutoSize = true;
            this._averageLabel.Location = new System.Drawing.Point(115, 56);
            this._averageLabel.Name = "_averageLabel";
            this._averageLabel.Size = new System.Drawing.Size(13, 13);
            this._averageLabel.TabIndex = 215;
            this._averageLabel.Text = "0";
            // 
            // _currentValueLabel
            // 
            this._currentValueLabel.AutoSize = true;
            this._currentValueLabel.Location = new System.Drawing.Point(51, 28);
            this._currentValueLabel.Name = "_currentValueLabel";
            this._currentValueLabel.Size = new System.Drawing.Size(58, 13);
            this._currentValueLabel.TabIndex = 204;
            this._currentValueLabel.Text = "Значение:";
            // 
            // _saveFileDialog
            // 
            this._saveFileDialog.FileName = "Калибровочные коэффициенты";
            this._saveFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // _openFileDialog
            // 
            this._openFileDialog.Filter = "(*.xml) | *.xml";
            // 
            // _calibrationProgressBar
            // 
            this._calibrationProgressBar.Location = new System.Drawing.Point(12, 364);
            this._calibrationProgressBar.Name = "_calibrationProgressBar";
            this._calibrationProgressBar.Size = new System.Drawing.Size(434, 23);
            this._calibrationProgressBar.TabIndex = 219;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._address2);
            this.groupBox2.Controls.Add(this._address1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._address0);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(12, 269);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 88);
            this.groupBox2.TabIndex = 220;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Приведённые значения на входе";
            // 
            // _address2
            // 
            this._address2.AutoSize = true;
            this._address2.Location = new System.Drawing.Point(60, 66);
            this._address2.Name = "_address2";
            this._address2.Size = new System.Drawing.Size(19, 13);
            this._address2.TabIndex = 5;
            this._address2.Text = "__";
            // 
            // _address1
            // 
            this._address1.AutoSize = true;
            this._address1.Location = new System.Drawing.Point(60, 47);
            this._address1.Name = "_address1";
            this._address1.Size = new System.Drawing.Size(19, 13);
            this._address1.TabIndex = 4;
            this._address1.Text = "__";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "КИС - 3:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "КИС - 2:";
            // 
            // _address0
            // 
            this._address0.AutoSize = true;
            this._address0.Location = new System.Drawing.Point(60, 28);
            this._address0.Name = "_address0";
            this._address0.Size = new System.Drawing.Size(19, 13);
            this._address0.TabIndex = 1;
            this._address0.Text = "__";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "КИС - 1:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._nominalCurrentImpuls);
            this.groupBox3.Controls.Add(this._writeNominalBut);
            this.groupBox3.Controls.Add(this._readNominalBut);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(265, 393);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 81);
            this.groupBox3.TabIndex = 221;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Номинальный ток импульса";
            // 
            // _writeNominalBut
            // 
            this._writeNominalBut.Location = new System.Drawing.Point(88, 48);
            this._writeNominalBut.Name = "_writeNominalBut";
            this._writeNominalBut.Size = new System.Drawing.Size(84, 23);
            this._writeNominalBut.TabIndex = 223;
            this._writeNominalBut.Text = "Записать";
            this._writeNominalBut.UseVisualStyleBackColor = true;
            this._writeNominalBut.Click += new System.EventHandler(this._writeNominalBut_Click);
            // 
            // _readNominalBut
            // 
            this._readNominalBut.Location = new System.Drawing.Point(88, 19);
            this._readNominalBut.Name = "_readNominalBut";
            this._readNominalBut.Size = new System.Drawing.Size(84, 23);
            this._readNominalBut.TabIndex = 222;
            this._readNominalBut.Text = "Прочитать";
            this._readNominalBut.UseVisualStyleBackColor = true;
            this._readNominalBut.Click += new System.EventHandler(this._readNominalBut_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(60, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 222;
            this.label6.Text = "mA";
            // 
            // _nominalCurrentImpuls
            // 
            this._nominalCurrentImpuls.Location = new System.Drawing.Point(10, 19);
            this._nominalCurrentImpuls.Mask = "000";
            this._nominalCurrentImpuls.Name = "_nominalCurrentImpuls";
            this._nominalCurrentImpuls.PromptChar = ' ';
            this._nominalCurrentImpuls.Size = new System.Drawing.Size(44, 20);
            this._nominalCurrentImpuls.TabIndex = 222;
            this._nominalCurrentImpuls.Text = "50";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._address5);
            this.groupBox6.Controls.Add(this._address6);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this._address7);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Location = new System.Drawing.Point(12, 393);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(247, 81);
            this.groupBox6.TabIndex = 222;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Состояния";
            // 
            // _address5
            // 
            this._address5.AutoSize = true;
            this._address5.Location = new System.Drawing.Point(59, 60);
            this._address5.Name = "_address5";
            this._address5.Size = new System.Drawing.Size(19, 13);
            this._address5.TabIndex = 11;
            this._address5.Text = "__";
            // 
            // _address6
            // 
            this._address6.AutoSize = true;
            this._address6.Location = new System.Drawing.Point(59, 41);
            this._address6.Name = "_address6";
            this._address6.Size = new System.Drawing.Size(19, 13);
            this._address6.TabIndex = 10;
            this._address6.Text = "__";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "КИС - 3:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "КИС - 2:";
            // 
            // _address7
            // 
            this._address7.AutoSize = true;
            this._address7.Location = new System.Drawing.Point(59, 22);
            this._address7.Name = "_address7";
            this._address7.Size = new System.Drawing.Size(19, 13);
            this._address7.TabIndex = 7;
            this._address7.Text = "__";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "КИС - 1:";
            // 
            // Calibration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 483);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this._calibrationProgressBar);
            this.Controls.Add(this._coeffListDec);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this._coeffListHex);
            this.Controls.Add(this.groupBox1);
            this.Name = "Calibration";
            this.Text = "Calibration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Calibration_FormClosing);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._numericSamples)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private GroupBox groupBox4;
        private Label label8;
        private ListBox _coeffListHex;
        private Button _readCalibrateBut;
        private Button _nullCoeffButtonBut;
        private GroupBox groupBox1;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Button _loadFileBut;
        private Button _saveFileBut;
        private Button _setI3But;
        private Button _setI2But;
        private Button _setI1But;
        private Label _currentLabel;
        private Label _averageValueLabel;
        private GroupBox groupBox5;
        private Label _currentValueLabel;
        private NumericUpDown _numericSamples;
        private Label _averageLabel;
        private ListBox _coeffListDec;
        private SaveFileDialog _saveFileDialog;
        private OpenFileDialog _openFileDialog;
        private ProgressBar _calibrationProgressBar;
        private MaskedTextBox _i3TextBox;
        private MaskedTextBox _i2TextBox;
        private MaskedTextBox _i1TextBox;
        private GroupBox groupBox2;
        private Label _address2;
        private Label _address1;
        private Label label9;
        private Label label7;
        private Label _address0;
        private Label label5;
        private GroupBox groupBox3;
        private Button _writeNominalBut;
        private Button _readNominalBut;
        private Label label6;
        private MaskedTextBox _nominalCurrentImpuls;
        private GroupBox groupBox6;
        private Label _address5;
        private Label _address6;
        private Label label12;
        private Label label13;
        private Label _address7;
        private Label label15;
    }
}