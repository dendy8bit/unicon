﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.KIS.Calibration.Structures
{
    public class SignalsStruct : StructBase
    {
        [Layout(0)] private ushort _i1;
        [Layout(1)] private ushort _i2;
        [Layout(2)] private ushort _i3;
        
        public ushort I1 => _i1;
        public ushort I2 => _i2;
        public ushort I3 => _i3;

        public double ReducedI1 => Math.Round((double)this._i1 * 4000 / 65535, 1);
        public double ReducedI2 => Math.Round((double)this._i2 * 4000 / 65535, 1);
        public double ReducedI3 => Math.Round((double)this._i3 * 4000 / 65535, 1);


        // BindingProperty используется только в валидаторе для проверки значений (КИС - 1, КИС - 2, КИС - 3)
        [BindingProperty(0)]
        public ushort Validator1 { get; set; }

        [BindingProperty(1)]
        public ushort Validator2 { get; set; }

        [BindingProperty(2)]
        public ushort Validator3 { get; set; }
    }
}
