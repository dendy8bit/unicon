﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.KIS.Calibration.Structures
{
    public class FactorStruct : StructBase
    {
        public static FactorStruct Default = new FactorStruct { _b = 0x0, _a = 0x8000, _pa = 0x1 };

        [Layout(0)] private ushort _b;
        [Layout(1)] private ushort _a;
        [Layout(2)] private ushort _pa;


        public ushort B
        {
            get { return _b; }
            set { _b = value; }
        }

        public ushort A
        {
            get { return _a; }
            set { _a = value; }
        }

        public ushort Pa
        {
            get { return _pa; }
            set { _pa = value; }
        }
        

        public string ToStringHex(int index) // получает сообщение с 118 адреса по 110 (в обратном порядке)
        {
            if (index == 0) return string.Format("B{3} = {0:X4}, A{3} = {1:X4}, Pa{3} = {2:X4}", B, A, Pa, 3);
            if (index == 1) return string.Format("B{3} = {0:X4}, A{3} = {1:X4}, Pa{3} = {2:X4}", B, A, Pa, 2);
            return string.Format("B{3} = {0:X4}, A{3} = {1:X4}, Pa{3} = {2:X4}", B, A, Pa, 1); // если index == 2
        }

        public string ToStringDec(int index) // получает сообщение с 118 адреса по 110 (в обратном порядке)
        {
            if (index == 0) return string.Format("B{3} = {0}, A{3} = {1}, Pa{3} = {2}", B, A, Pa, 3);
            if (index == 1) return string.Format("B{3} = {0}, A{3} = {1}, Pa{3} = {2}", B, A, Pa, 2);
            return string.Format("B{3} = {0}, A{3} = {1}, Pa{3} = {2}", B, A, Pa, 1); // если index == 2
        }
    }
}
