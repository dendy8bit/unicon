﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.KIS.Calibration.Structures
{
    public class AllCoeffStruct : StructBase
    {
        [Layout(0, Count = 15)]
        private ushort[] _data;

        public ushort[] Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = new ushort[15];
                Array.ConstrainedCopy(value, 0, _data, 0, 15);
            }
        }
    }
}
