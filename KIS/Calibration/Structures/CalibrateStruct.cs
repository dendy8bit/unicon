﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.KIS.Calibration.Structures
{
    public class CalibrateStruct : StructBase
    {
        [Layout(0, Count = 3)] private FactorStruct[] _factors;

        public FactorStruct[] Factors
        {
            get { return _factors; }
            set { _factors = value; }
        }

        public static readonly CalibrateStruct Default = new CalibrateStruct
        {
            _factors = new[]
            {
                FactorStruct.Default,
                FactorStruct.Default,
                FactorStruct.Default
            }
        };       
    }
}
