﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.KIS.Calibration.Structures
{
    public class StateStruct : StructBase
    {
        [Layout(0)] private ushort _stateKis3;
        [Layout(1)] private ushort _stateKis2;
        [Layout(2)] private ushort _stateKis1;
        
        public ushort StateKis1 => this._stateKis1;
        public ushort StateKis2 => this._stateKis2;
        public ushort StateKis3 => this._stateKis3;
    }
}
