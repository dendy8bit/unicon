﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.KIS.Calibration.Structures;
using BEMN.MBServer;

namespace BEMN.KIS.Calibration
{
    public partial class Calibration : Form, IFormView
    {
        #region [IFormView]
        public Type FormDevice => typeof(KIS);
        public bool Multishow { get; private set; }
        public Type ClassType => typeof(Calibration);
        public bool ForceShow => false;
        public Image NodeImage => Resources.calibrate; 
        public string NodeName => "Калибровка";
        public INodeView[] ChildNodes => new INodeView[] { };
        public bool Deletable => false;

        #endregion [IFormView]

        private KIS _device;
        private AveragerCount<SignalsStruct> _averager; // (_i1, _i2, _i3)
        private MemoryEntity<FactorStruct> _currentFactor; // текущий факток (b, a, pa)

        private NewStructValidator<SignalsStruct> _signalValidator; // для проверки вводимых значений КИСов
        private NewStructValidator<OneWordStruct> _currentNominalValidator; // для проверки вводимого значения номинального тока импульса

        private int _step; // шаг (этап калибровки)
        private ushort _b; // текущее измеряемое значение 
        private ushort _average; // среднее значение из _b
        private ushort _k; // среднее измеряемое значение this._b c первого шага калибровки 
        private ushort _currentCount; // количество входов в метод _averager_IncIndex()
        private int _sum; // сумма первого шага
        private int _sumMax; // сумма второго шага

        private bool _ok = true; // false - если хотим отменить калибровку, true - по умолчанию

        public Calibration()
        {
            InitializeComponent();
        }

        public Calibration(KIS device)
        {
            InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            this.VisibleLabel(false);

            this._calibrationProgressBar.Value = 0;

            this._device.Koeff110.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadKoeffs); // читает калибровочный коэффициент, если в 110-ом адресе ffff (новая плата) - задает параметры по умолчанию
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.Koeff110.LoadStruct();
            }
            
            //this._device.CurrentNominal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CurrentNominalWriteOk); // по записи структуры номинального тока импульса
            this._device.CurrentNominal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CurrentNominalReadOk); // по прочтению структуры номинального тока импульса

            this._device.Calibrate.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.CalibrateReadOk); // по прочтению структуры калибровки (формирование листа)
            this._device.Calibrate.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.CalibrateWriteOk); // при записи происходит метод чтения калибровки -> формирование листа

            this._device.Channel1.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.ChannelAllWriteOk); 
            this._device.Channel2.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.ChannelAllWriteOk); 
            this._device.Channel3.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.ChannelAllWriteOk);

            this._device.Clear.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("Невозможно очистить текущие коэффициенты, ошибка связи!"));
            this._device.Clear.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.CalibrateWriteOk(); // при записи происходит метод чтения калибровки -> формирование листа
                MessageBox.Show("Заданы параметры по умолчанию!");
            });

            this._device.AllCoeff.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                if (this._saveFileDialog.ShowDialog() != DialogResult.OK) return; 
                this.Serialize(this._saveFileDialog.FileName, this._device.AllCoeff.Values); // сохраняем в файл, читаем коэффициенты
            });
            this._device.AllCoeff.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Ошибка. Не удалось прочитать память!");
            });
            this._device.AllCoeff.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.CalibrateWriteOk);

            this._averager = new AveragerCount<SignalsStruct>(this._device.Signals);
            this._averager.Tick += this._averager_Tick; // метод вызывается после чтения структуры SignalsStruct n-раз (где n = _numericSamples.Text)
            this._averager.IncIndex += this._averager_IncIndex; // метод вызывается каждый раз после чтения структуры SignalsStruct

            this._signalValidator = new NewStructValidator<SignalsStruct>
            (new ToolTip(),
                new ControlInfoText(this._i1TextBox, new CustomUshortRule(1, 100)),
                new ControlInfoText(this._i2TextBox, new CustomUshortRule(1, 100)),
                new ControlInfoText(this._i3TextBox, new CustomUshortRule(1, 100))
            );

            this._currentNominalValidator = new NewStructValidator<OneWordStruct>
            (new ToolTip(),
                new ControlInfoText(this._nominalCurrentImpuls, new CustomUshortRule(40, 70)));

            this._device.SignalsForShow.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignalsReadOk);
            this._device.StateKis.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.StateReadOk); 

            // начальные значения
            _i1TextBox.Text = "100";
            _i2TextBox.Text = "100";
            _i3TextBox.Text = "100";
           
            this.LoadCalibrate(); // прочитать калибровки 

            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.SignalsForShow.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 300));
                this._device.StateKis.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 300));
            }              
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._device.SignalsForShow.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 300));
                this._device.StateKis.LoadStructCycle(new TimeSpan(0, 0, 0, 0, 300));
            }
            else
            {
                this._device.SignalsForShow.RemoveStructQueries();
                this._device.StateKis.RemoveStructQueries();
            }
        }



        private void SignalsReadOk()
        {
            this._address2.Text = this._device.SignalsForShow.Value.ReducedI1.ToString();
            this._address1.Text = this._device.SignalsForShow.Value.ReducedI2.ToString();
            this._address0.Text = this._device.SignalsForShow.Value.ReducedI3.ToString();
        }

        private void StateReadOk()
        {
            this._address7.Text = this._device.StateKis.Value.StateKis1.ToString();
            this._address6.Text = this._device.StateKis.Value.StateKis2.ToString();
            this._address5.Text = this._device.StateKis.Value.StateKis3.ToString();
        }

        public void Serialize(string fileName, ushort[] values) // сохранение в файл
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException();
            }
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("KIS"));
            XmlElement allElements = doc.CreateElement("AllCoeff");
            allElements.InnerText = Convert.ToBase64String(Common.TOBYTES(values, true));
            doc.DocumentElement?.AppendChild(allElements);
            doc.Save(fileName);
            MessageBox.Show("Файл успешно сохранён!");
        }

        private void Deserialize() // загрузка из файла 
        {
            if (DialogResult.Yes != MessageBox.Show("Память прочитанная из файла автоматически запишется в устройство. Продолжить?", "Внимание!", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                return;
            }
            if (DialogResult.OK == this._openFileDialog.ShowDialog())
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(this._openFileDialog.FileName);
                    ushort[] data = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode("/KIS/AllCoeff")?.InnerText), true);
                    this._device.AllCoeff.Value = new AllCoeffStruct { Data = data };
                    this._device.AllCoeff.SaveStruct();
                }
                catch (NullReferenceException)
                {
                    MessageBox.Show("Файл " + this._openFileDialog.FileName + " повреждён!");
                }
            }
        }
        
        private void CalibrateReadOk() // формирование списков 
        {
            var listHex = new List<string>();
            var listDec = new List<string>();
            this._coeffListHex.Items.Clear();
            this._coeffListDec.Items.Clear();
            for (int i = 0; i < this._device.Calibrate.Value.Factors.Length; i++)
            {
                FactorStruct factor = this._device.Calibrate.Value.Factors[i];
                listHex.Add(factor.ToStringHex(i));
                listDec.Add(factor.ToStringDec(i));
            }
            listHex.Reverse();
            listDec.Reverse();
            this._coeffListHex.Items.AddRange(listHex.ToArray());
            this._coeffListDec.Items.AddRange(listDec.ToArray());
        }

        private void CurrentNominalReadOk()
        {
            this._nominalCurrentImpuls.Text = this._device.CurrentNominal.Value.Word.ToString();          
        }



        private void CalibrateWriteOk() // метод при успешном чтении калибровки
        {
            this._device.Calibrate.LoadStruct(new TimeSpan(0, 0, 0, 0, 500));           
        }

        private void _nullCoeffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Вы уверены что хотите стереть все калибровки?", "Внимание", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                this._device.Clear.Value = CalibrateStruct.Default;
                this._device.Clear.SaveStruct();
                _i1TextBox.BackColor = Color.White;
                _i1TextBox.ReadOnly = false;
                _i2TextBox.BackColor = Color.White;
                _i2TextBox.ReadOnly = false;
                _i3TextBox.BackColor = Color.White;
                _i3TextBox.ReadOnly = false;
            }
        }

        private void _readCalibrateBut_Click(object sender, EventArgs e)
        {
            LoadCalibrate();
        }

        private void LoadCalibrate()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.Calibrate.LoadStruct(new TimeSpan(0, 0, 0, 0, 500));
        }

        /// <summary>
        /// Блоуирует элементы управления во время калибровки
        /// </summary>
        private void Proccess(bool value)
        {
            this.groupBox1.Enabled = value;
            this.groupBox4.Enabled = value;
        }

        private void _setI1But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return; // если нет связи с устройством - выходим из метода
            if (!Check()) return; // если введены некорректные значения - выходим из метода
            this.StartSetFactor(this._device.Channel1); // начать калибровку
            if (!this._ok) return; // если передумали калибровать - выходим из метода
            this.Proccess(false); // блокируем элементы управления
            this._calibrationProgressBar.Value = 0;
            this._setI1But.Enabled = false;
            this.VisibleLabel(false);       
        }

        private void _setI2But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this.Check()) return;
            this.StartSetFactor(this._device.Channel2);
            if (!this._ok) return;
            this.Proccess(false);
            this._calibrationProgressBar.Value = 0;
            this._setI2But.Enabled = false;
            this.VisibleLabel(false);           
        }

        private void _setI3But_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this.Check()) return;
            this.StartSetFactor(this._device.Channel3);
            if (!this._ok) return;
            this.Proccess(false);
            this._calibrationProgressBar.Value = 0;
            this._setI3But.Enabled = false;
            this.VisibleLabel(false);           
        }

        /// <summary>
        /// Проверка значений "КИС - 1", "КИС - 2", "КИС - 3" на корректность
        /// </summary>
        private bool Check() 
        {
            string message;
            if (!this._signalValidator.Check(out message, true) || !this._currentNominalValidator.Check(out message, true))
            {
                MessageBox.Show("Найдены неверные значения!","Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }
        
        private void StartSetFactor(MemoryEntity<FactorStruct> currentFactor)
        {
            if (MessageBox.Show("Установите значение 0 на входе канала.", "Внимание!", MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                this._ok = false;
                return;
            }

            this._ok = true;
            this._calibrationProgressBar.Maximum = Convert.ToInt16(this._numericSamples.Text) * 2;
            this._step = 0;
            this._currentFactor = currentFactor;
            this._currentFactor.Value = FactorStruct.Default;
            this._currentFactor.SaveStruct(); // вызовет метод this.ChannelAllWriteOk()
        }
        
        private void ChannelAllWriteOk()
        {
            this._step++;
            if (this._step == 1 || this._step == 2)
            {
                this._averager.Start((int)this._numericSamples.Value);
            }
            if (this._step == 3) 
            {
                if (this._currentFactor == this._device.Channel1) 
                {
                    this._averageLabel.Invoke((MethodInvoker)delegate // выполняем изменение компонентов в другом потоке
                    {
                        Proccess(true);
                        MessageBox.Show("Калибровка канала 1 завершена.");
                        this._i1TextBox.ReadOnly = true;
                        this._i1TextBox.BackColor = Color.Green;
                    });
                }
                if (this._currentFactor == this._device.Channel2)
                {
                    this._averageLabel.Invoke((MethodInvoker) delegate
                    {
                        Proccess(true);
                        this._i2TextBox.BackColor = Color.Green;
                        this._i2TextBox.ReadOnly = true;
                        MessageBox.Show("Калибровка канала 2 завершена.");
                        
                    });
                }
                if (this._currentFactor == this._device.Channel3)
                {
                    this._averageLabel.Invoke((MethodInvoker) delegate
                    {
                        Proccess(true);
                        this._i3TextBox.BackColor = Color.Green;
                        this._i3TextBox.ReadOnly = true;
                        MessageBox.Show("Калибровка канала 3 завершена.");
                    });
                }
                this.LoadCalibrate(); // читаем калибровку -> формируем новый лист
            }
        }

        private void _averager_IncIndex()
        {
            this._currentCount++;
            int index = this._averager.ValueList.Count;
            if (this._currentFactor == this._device.Channel1)
            {
                this._b = this._averager.ValueList[index - 1].I3;
                this._sum += this._averager.ValueList[index - 1].I3;
            }
            if (this._currentFactor == this._device.Channel2)
            {
                this._b = this._averager.ValueList[index - 1].I2;
                this._sum += this._averager.ValueList[index - 1].I2;
            }
            if (this._currentFactor == this._device.Channel3)
            {
                this._b = this._averager.ValueList[index - 1].I1;
                this._sum += this._averager.ValueList[index - 1].I1;
            }

            this._currentLabel.Invoke((MethodInvoker)delegate { this._currentLabel.Text = this._b.ToString(); });

            if (this._currentCount == int.Parse(this._numericSamples.Text))
            {
                this._average = (ushort)(this._sum / this._currentCount); // расчёт среднего значения
                this._averageLabel.Invoke((MethodInvoker)delegate { this._averageLabel.Text = this._average.ToString(); });
                this._currentCount = 0;
                this._sumMax = this._sum;
                this._sum = 0;
            }
            
            this._calibrationProgressBar.Invoke((MethodInvoker)delegate { this._calibrationProgressBar.Increment(1);});
        }

        private void _averager_Tick()
        {
            try
            {
                if (this._step == 1) 
                {
                    FactorStruct temp = FactorStruct.Default.Clone<FactorStruct>();
                    temp.B = _k = this._average;
                    this._currentFactor.Value = temp;
                    MessageBox.Show("Установите значение соответствующее указанному проценту и нажмите ОК.");
                    this._averageLabel.Invoke((MethodInvoker) delegate { this._averageLabel.Visible = true; });
                    this._averageValueLabel.Invoke((MethodInvoker) delegate { this._averageValueLabel.Visible = true; });
                    this._currentFactor.SaveStruct(); // вызовет метод ChannelAllWriteOk()
                }
            }
            catch
            {
                MessageBox.Show("Ошибка на первом этапе калибровки!");
                return;
            }

            try
            {
                if (this._step == 2)
                {
                    int pa = 1;
                    double atmp = 0;
                    double koeff = 0.0;
                    if (this._currentFactor == this._device.Channel1)
                    {
                        atmp = this._sumMax / (ushort) this._numericSamples.Value;
                        koeff = double.Parse(this._i1TextBox.Text);
                    }
                    if (this._currentFactor == this._device.Channel2)
                    {
                        atmp = this._sumMax / (ushort) this._numericSamples.Value;
                        koeff = double.Parse(this._i2TextBox.Text);
                    }
                    if (this._currentFactor == this._device.Channel3)
                    {
                        atmp = this._sumMax / (ushort) this._numericSamples.Value;
                        koeff = double.Parse(this._i3TextBox.Text);
                    }
                    this._averageLabel.Invoke((MethodInvoker) delegate { this._averageLabel.Text = atmp.ToString(); });

                    atmp += _k * koeff / 100; // ОСНОВНАЯ ФОРМУЛА
                    atmp = (ulong) (0x8000 * 0x8000 / atmp * koeff / 100); // ОСНОВНАЯ ФОРМУЛА

                    if (atmp == 0)
                    {
                        atmp = 0x8000; // 32768
                    }

                    while (atmp > 0xFFFF) // 65535
                    {
                        atmp -= 0x7FFF; // 32767
                        pa++;
                    }

                    this._setI1But.Invoke((MethodInvoker) delegate { this._setI1But.Enabled = true; });
                    this._setI2But.Invoke((MethodInvoker) delegate { this._setI2But.Enabled = true; });
                    this._setI3But.Invoke((MethodInvoker) delegate { this._setI3But.Enabled = true; });

                    this._currentFactor.Value.A = (ushort) atmp;
                    this._currentFactor.Value.Pa = (ushort) pa;
                    //this._currentFactor.Value.B = 0;
                    this._currentFactor.SaveStruct();
                }
            }
            catch
            {
                MessageBox.Show("Ошибка на втором этапе калибровки!");
            }
        }

        private void _saveFileBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this.Check()) return;
            this._device.AllCoeff.LoadStruct();
        }

        private void _loadFileBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.Deserialize();
        }


        /// <summary>
        /// Читает коэффициенты, если записано в 110-ом адресе ffff (новая плата) - задаем параметры по умолчанию
        /// </summary>
        private void LoadKoeffs()
        {
            if (this._device.Koeff110.Values[0] != 0xffff) return;
            this._device.Clear.Value = CalibrateStruct.Default;
            this._device.Clear.SaveStruct();
        }

        /// <summary>
        /// Видимость текста - среднее значение
        /// </summary>
        private void VisibleLabel(bool value)
        {
            this._averageLabel.Visible = value;
            this._averageValueLabel.Visible = value;
        }

        private void Calibration_FormClosing(object sender, FormClosingEventArgs e) // при закрытии формы удаляем все запросы
        {
            this._device.AllCoeff.RemoveStructQueries();
            this._device.Koeff110.RemoveStructQueries();
            this._device.Calibrate.RemoveStructQueries();
            this._device.Channel1.RemoveStructQueries();
            this._device.Channel2.RemoveStructQueries();
            this._device.Channel3.RemoveStructQueries();
            this._device.Clear.RemoveStructQueries();

            this._device.ConnectionModeChanged -= this.StartStopLoad;
            this._device.Signals.RemoveStructQueries();
            this._device.SignalsForShow.RemoveStructQueries();

            this._averager.Tick -= this._averager_Tick; // метод вызывается после чтения структуры SignalsStruct n-раз (где n = _numericSamples.Text)
            this._averager.IncIndex -= this._averager_IncIndex; // метод вызывается каждый раз после чтения структуры SignalsStruct

            this._averager.MemoryEntityRemove();
        }

        private void _readNominalBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._device.CurrentNominal.LoadStruct();
        }

        private void _writeNominalBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this.Check()) return;
            this._device.CurrentNominal.Value.Word = ushort.Parse(this._nominalCurrentImpuls.Text);
            this._device.CurrentNominal.SaveStruct();
        }
    }
}