﻿using System.Windows.Forms;

namespace BEMN.Mr762.Version300.Configuration
{
    partial class ResistanceDefTabCtr
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ResistTabControl = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._corner2ForRGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label94 = new System.Windows.Forms.Label();
            this._corner1ForRGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this._swingfGr2 = new System.Windows.Forms.MaskedTextBox();
            this.swPanel2 = new System.Windows.Forms.Panel();
            this._swingXGr2 = new System.Windows.Forms.MaskedTextBox();
            this._swingdzGr2 = new System.Windows.Forms.MaskedTextBox();
            this._swingRGr2 = new System.Windows.Forms.MaskedTextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.swPanel1 = new System.Windows.Forms.Panel();
            this._swingXGr1 = new System.Windows.Forms.MaskedTextBox();
            this._swingdzGr1 = new System.Windows.Forms.MaskedTextBox();
            this._swingRGr1 = new System.Windows.Forms.MaskedTextBox();
            this._l11 = new System.Windows.Forms.Label();
            this._l10 = new System.Windows.Forms.Label();
            this._l9 = new System.Windows.Forms.Label();
            this._swingYesNoTrGr1 = new System.Windows.Forms.CheckBox();
            this._swingTrGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this._swingIGr1 = new System.Windows.Forms.MaskedTextBox();
            this._swingI0Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._swingTGr1 = new System.Windows.Forms.MaskedTextBox();
            this._swingfGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this._swingTypeGr1 = new System.Windows.Forms.ComboBox();
            this._loadedGroup = new System.Windows.Forms.GroupBox();
            this.loadFaz2 = new System.Windows.Forms.GroupBox();
            this._r2FazGr2 = new System.Windows.Forms.MaskedTextBox();
            this.label154 = new System.Windows.Forms.Label();
            this._r1FazGr2 = new System.Windows.Forms.MaskedTextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.maskedTextBox4 = new System.Windows.Forms.MaskedTextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.loadLin2 = new System.Windows.Forms.GroupBox();
            this._r2LinGr2 = new System.Windows.Forms.MaskedTextBox();
            this.label135 = new System.Windows.Forms.Label();
            this._r1LinGr2 = new System.Windows.Forms.MaskedTextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.maskedTextBox3 = new System.Windows.Forms.MaskedTextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.loadFaz1 = new System.Windows.Forms.GroupBox();
            this._r2FazGr1 = new System.Windows.Forms.MaskedTextBox();
            this._l8 = new System.Windows.Forms.Label();
            this._r1FazGr1 = new System.Windows.Forms.MaskedTextBox();
            this._l7 = new System.Windows.Forms.Label();
            this._cornerFazGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.loadLin1 = new System.Windows.Forms.GroupBox();
            this._r2LinGr1 = new System.Windows.Forms.MaskedTextBox();
            this._l6 = new System.Windows.Forms.Label();
            this._r1LinGr1 = new System.Windows.Forms.MaskedTextBox();
            this._l5 = new System.Windows.Forms.Label();
            this._cornerLinGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.calcKoefBtn = new System.Windows.Forms.Button();
            this.fn2groupBox2 = new System.Windows.Forms.GroupBox();
            this._xZ1Step2Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step2Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this._rZ1Step2Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this._rZ0Step2Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.fn4groupBox1 = new System.Windows.Forms.GroupBox();
            this._xZ1Step4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._rZ0Step4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.fn4groupBox2 = new System.Windows.Forms.GroupBox();
            this._xZ1Step4Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step4Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step4Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._rZ0Step4Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.fn5groupBox2 = new System.Windows.Forms.GroupBox();
            this._xZ1Step5Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step5Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step5Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this._rZ0Step5Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.fn5groupBox1 = new System.Windows.Forms.GroupBox();
            this._xZ1Step5Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step5Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step5Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this._rZ0Step5Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.fn3groupBox2 = new System.Windows.Forms.GroupBox();
            this._xZ1Step3Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step3Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step3Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._rZ0Step3Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.fn3groupBox1 = new System.Windows.Forms.GroupBox();
            this._xZ1Step3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._rZ0Step3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fn1groupBox2 = new System.Windows.Forms.GroupBox();
            this._xZ1Step1Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step1Gr2 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step1Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this._rZ0Step1Gr2 = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.fn2groupBox1 = new System.Windows.Forms.GroupBox();
            this._xZ1Step2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._l4 = new System.Windows.Forms.Label();
            this._l3 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._rZ1Step2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this._rZ0Step2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.fn1groupBox1 = new System.Windows.Forms.GroupBox();
            this._xZ1Step1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._xZ0Step1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._rZ1Step1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label82 = new System.Windows.Forms.Label();
            this._rZ0Step1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label81 = new System.Windows.Forms.Label();
            this._l2 = new System.Windows.Forms.Label();
            this._l1 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage1 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage2 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage3 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage4 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage5 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabPage6 = new BEMN.Mr762.Version300.Configuration.ResistanceDefTabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._isMash = new System.Windows.Forms.CheckBox();
            this.saveResistParamsBtn = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.characteristicEnableControl6 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.characteristicEnableControl5 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.characteristicEnableControl4 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.characteristicEnableControl3 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.characteristicEnableControl2 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.characteristicEnableControl1 = new BEMN.Mr762.Version300.Configuration.CharacteristicEnableControl();
            this.button1 = new System.Windows.Forms.Button();
            this.DrawPanel = new System.Windows.Forms.Panel();
            this._tablePage = new System.Windows.Forms.TabPage();
            this._infoGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._header1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._header2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileResistCharacteristic = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.settingsOpfGroupBox = new System.Windows.Forms.GroupBox();
            this._settingsResetCB = new System.Windows.Forms.CheckBox();
            this._settingsUminTB = new System.Windows.Forms.MaskedTextBox();
            this._settingsImaxTB = new System.Windows.Forms.MaskedTextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.ResistTabControl.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.swPanel2.SuspendLayout();
            this.swPanel1.SuspendLayout();
            this._loadedGroup.SuspendLayout();
            this.loadFaz2.SuspendLayout();
            this.loadLin2.SuspendLayout();
            this.loadFaz1.SuspendLayout();
            this.loadLin1.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.fn2groupBox2.SuspendLayout();
            this.fn4groupBox1.SuspendLayout();
            this.fn4groupBox2.SuspendLayout();
            this.fn5groupBox2.SuspendLayout();
            this.fn5groupBox1.SuspendLayout();
            this.fn3groupBox2.SuspendLayout();
            this.fn3groupBox1.SuspendLayout();
            this.fn1groupBox2.SuspendLayout();
            this.fn2groupBox1.SuspendLayout();
            this.fn1groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.panel7.SuspendLayout();
            this._tablePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._infoGrid)).BeginInit();
            this.settingsOpfGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ResistTabControl
            // 
            this.ResistTabControl.Controls.Add(this.tabPage12);
            this.ResistTabControl.Controls.Add(this.tabPage1);
            this.ResistTabControl.Controls.Add(this.tabPage2);
            this.ResistTabControl.Controls.Add(this.tabPage3);
            this.ResistTabControl.Controls.Add(this.tabPage4);
            this.ResistTabControl.Controls.Add(this.tabPage5);
            this.ResistTabControl.Controls.Add(this.tabPage6);
            this.ResistTabControl.Controls.Add(this.tabPage11);
            this.ResistTabControl.Controls.Add(this._tablePage);
            this.ResistTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResistTabControl.Location = new System.Drawing.Point(0, 0);
            this.ResistTabControl.Name = "ResistTabControl";
            this.ResistTabControl.SelectedIndex = 0;
            this.ResistTabControl.Size = new System.Drawing.Size(868, 541);
            this.ResistTabControl.TabIndex = 0;
            this.ResistTabControl.SelectedIndexChanged += new System.EventHandler(this.ResistTabControl_SelectedIndexChanged);
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.settingsOpfGroupBox);
            this.tabPage12.Controls.Add(this.groupBox17);
            this.tabPage12.Controls.Add(this.groupBox47);
            this.tabPage12.Controls.Add(this._loadedGroup);
            this.tabPage12.Controls.Add(this.groupBox11);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(860, 515);
            this.tabPage12.TabIndex = 12;
            this.tabPage12.Text = "Общие настройки";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._corner2ForRGr1);
            this.groupBox17.Controls.Add(this.label94);
            this.groupBox17.Controls.Add(this._corner1ForRGr1);
            this.groupBox17.Controls.Add(this.label87);
            this.groupBox17.Location = new System.Drawing.Point(6, 241);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(444, 46);
            this.groupBox17.TabIndex = 8;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Углы направленной характеристики для ступеней Z";
            // 
            // _corner2ForRGr1
            // 
            this._corner2ForRGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._corner2ForRGr1.Location = new System.Drawing.Point(165, 17);
            this._corner2ForRGr1.Name = "_corner2ForRGr1";
            this._corner2ForRGr1.Size = new System.Drawing.Size(30, 20);
            this._corner2ForRGr1.TabIndex = 23;
            this._corner2ForRGr1.Tag = "1500";
            this._corner2ForRGr1.Text = "0";
            this._corner2ForRGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(114, 19);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(50, 13);
            this.label94.TabIndex = 26;
            this.label94.Text = "у2, град.";
            // 
            // _corner1ForRGr1
            // 
            this._corner1ForRGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._corner1ForRGr1.Location = new System.Drawing.Point(68, 17);
            this._corner1ForRGr1.Name = "_corner1ForRGr1";
            this._corner1ForRGr1.Size = new System.Drawing.Size(30, 20);
            this._corner1ForRGr1.TabIndex = 23;
            this._corner1ForRGr1.Tag = "1500";
            this._corner1ForRGr1.Text = "0";
            this._corner1ForRGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(17, 19);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(50, 13);
            this.label87.TabIndex = 26;
            this.label87.Text = "у1, град.";
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this._swingfGr2);
            this.groupBox47.Controls.Add(this.swPanel2);
            this.groupBox47.Controls.Add(this.swPanel1);
            this.groupBox47.Controls.Add(this._swingYesNoTrGr1);
            this.groupBox47.Controls.Add(this._swingTrGr1);
            this.groupBox47.Controls.Add(this.label65);
            this.groupBox47.Controls.Add(this._swingIGr1);
            this.groupBox47.Controls.Add(this._swingI0Gr1);
            this.groupBox47.Controls.Add(this._swingTGr1);
            this.groupBox47.Controls.Add(this._swingfGr1);
            this.groupBox47.Controls.Add(this.label119);
            this.groupBox47.Controls.Add(this.label118);
            this.groupBox47.Controls.Add(this.label117);
            this.groupBox47.Controls.Add(this.label115);
            this.groupBox47.Controls.Add(this.label112);
            this.groupBox47.Controls.Add(this._swingTypeGr1);
            this.groupBox47.Location = new System.Drawing.Point(456, 6);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(398, 128);
            this.groupBox47.TabIndex = 7;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Качание";
            // 
            // _swingfGr2
            // 
            this._swingfGr2.Location = new System.Drawing.Point(79, 98);
            this._swingfGr2.Name = "_swingfGr2";
            this._swingfGr2.Size = new System.Drawing.Size(47, 20);
            this._swingfGr2.TabIndex = 10;
            this._swingfGr2.Text = "0";
            this._swingfGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._swingfGr2.Visible = false;
            // 
            // swPanel2
            // 
            this.swPanel2.Controls.Add(this._swingXGr2);
            this.swPanel2.Controls.Add(this._swingdzGr2);
            this.swPanel2.Controls.Add(this._swingRGr2);
            this.swPanel2.Controls.Add(this.label159);
            this.swPanel2.Controls.Add(this.label160);
            this.swPanel2.Controls.Add(this.label161);
            this.swPanel2.Location = new System.Drawing.Point(3, 38);
            this.swPanel2.Name = "swPanel2";
            this.swPanel2.Size = new System.Drawing.Size(124, 61);
            this.swPanel2.TabIndex = 9;
            this.swPanel2.Visible = false;
            // 
            // _swingXGr2
            // 
            this._swingXGr2.Location = new System.Drawing.Point(76, 20);
            this._swingXGr2.Name = "_swingXGr2";
            this._swingXGr2.Size = new System.Drawing.Size(47, 20);
            this._swingXGr2.TabIndex = 6;
            this._swingXGr2.Text = "0";
            this._swingXGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingdzGr2
            // 
            this._swingdzGr2.Location = new System.Drawing.Point(76, 40);
            this._swingdzGr2.Name = "_swingdzGr2";
            this._swingdzGr2.Size = new System.Drawing.Size(47, 20);
            this._swingdzGr2.TabIndex = 7;
            this._swingdzGr2.Text = "0";
            this._swingdzGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingRGr2
            // 
            this._swingRGr2.Location = new System.Drawing.Point(76, 0);
            this._swingRGr2.Name = "_swingRGr2";
            this._swingRGr2.Size = new System.Drawing.Size(47, 20);
            this._swingRGr2.TabIndex = 8;
            this._swingRGr2.Text = "0";
            this._swingRGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(3, 43);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(70, 13);
            this.label159.TabIndex = 3;
            this.label159.Text = "dz, Ом перв.";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(3, 23);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(66, 13);
            this.label160.TabIndex = 4;
            this.label160.Text = "X, Ом перв.";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(3, 3);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(67, 13);
            this.label161.TabIndex = 5;
            this.label161.Text = "R, Ом перв.";
            // 
            // swPanel1
            // 
            this.swPanel1.Controls.Add(this._swingXGr1);
            this.swPanel1.Controls.Add(this._swingdzGr1);
            this.swPanel1.Controls.Add(this._swingRGr1);
            this.swPanel1.Controls.Add(this._l11);
            this.swPanel1.Controls.Add(this._l10);
            this.swPanel1.Controls.Add(this._l9);
            this.swPanel1.Location = new System.Drawing.Point(3, 38);
            this.swPanel1.Name = "swPanel1";
            this.swPanel1.Size = new System.Drawing.Size(124, 61);
            this.swPanel1.TabIndex = 6;
            // 
            // _swingXGr1
            // 
            this._swingXGr1.Location = new System.Drawing.Point(76, 20);
            this._swingXGr1.Name = "_swingXGr1";
            this._swingXGr1.Size = new System.Drawing.Size(47, 20);
            this._swingXGr1.TabIndex = 6;
            this._swingXGr1.Text = "0";
            this._swingXGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingdzGr1
            // 
            this._swingdzGr1.Location = new System.Drawing.Point(76, 40);
            this._swingdzGr1.Name = "_swingdzGr1";
            this._swingdzGr1.Size = new System.Drawing.Size(47, 20);
            this._swingdzGr1.TabIndex = 7;
            this._swingdzGr1.Text = "0";
            this._swingdzGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingRGr1
            // 
            this._swingRGr1.Location = new System.Drawing.Point(76, 0);
            this._swingRGr1.Name = "_swingRGr1";
            this._swingRGr1.Size = new System.Drawing.Size(47, 20);
            this._swingRGr1.TabIndex = 8;
            this._swingRGr1.Text = "0";
            this._swingRGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l11
            // 
            this._l11.AutoSize = true;
            this._l11.Location = new System.Drawing.Point(3, 43);
            this._l11.Name = "_l11";
            this._l11.Size = new System.Drawing.Size(69, 13);
            this._l11.TabIndex = 3;
            this._l11.Text = "dz, Ом втор.";
            // 
            // _l10
            // 
            this._l10.AutoSize = true;
            this._l10.Location = new System.Drawing.Point(3, 23);
            this._l10.Name = "_l10";
            this._l10.Size = new System.Drawing.Size(65, 13);
            this._l10.TabIndex = 4;
            this._l10.Text = "X, Ом втор.";
            // 
            // _l9
            // 
            this._l9.AutoSize = true;
            this._l9.Location = new System.Drawing.Point(3, 3);
            this._l9.Name = "_l9";
            this._l9.Size = new System.Drawing.Size(66, 13);
            this._l9.TabIndex = 5;
            this._l9.Text = "R, Ом втор.";
            // 
            // _swingYesNoTrGr1
            // 
            this._swingYesNoTrGr1.AutoSize = true;
            this._swingYesNoTrGr1.Location = new System.Drawing.Point(202, 79);
            this._swingYesNoTrGr1.Name = "_swingYesNoTrGr1";
            this._swingYesNoTrGr1.Size = new System.Drawing.Size(185, 17);
            this._swingYesNoTrGr1.TabIndex = 6;
            this._swingYesNoTrGr1.Text = "Сброс блокировки по времени:";
            this._swingYesNoTrGr1.UseVisualStyleBackColor = true;
            // 
            // _swingTrGr1
            // 
            this._swingTrGr1.Location = new System.Drawing.Point(264, 98);
            this._swingTrGr1.Name = "_swingTrGr1";
            this._swingTrGr1.Size = new System.Drawing.Size(77, 20);
            this._swingTrGr1.TabIndex = 5;
            this._swingTrGr1.Text = "20";
            this._swingTrGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(202, 101);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(40, 13);
            this.label65.TabIndex = 4;
            this.label65.Text = "Tб, мс";
            // 
            // _swingIGr1
            // 
            this._swingIGr1.Location = new System.Drawing.Point(264, 58);
            this._swingIGr1.Name = "_swingIGr1";
            this._swingIGr1.Size = new System.Drawing.Size(47, 20);
            this._swingIGr1.TabIndex = 2;
            this._swingIGr1.Text = "0";
            this._swingIGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingI0Gr1
            // 
            this._swingI0Gr1.Location = new System.Drawing.Point(264, 37);
            this._swingI0Gr1.Name = "_swingI0Gr1";
            this._swingI0Gr1.Size = new System.Drawing.Size(47, 20);
            this._swingI0Gr1.TabIndex = 2;
            this._swingI0Gr1.Text = "0";
            this._swingI0Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingTGr1
            // 
            this._swingTGr1.Location = new System.Drawing.Point(264, 17);
            this._swingTGr1.Name = "_swingTGr1";
            this._swingTGr1.Size = new System.Drawing.Size(77, 20);
            this._swingTGr1.TabIndex = 2;
            this._swingTGr1.Text = "20";
            this._swingTGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _swingfGr1
            // 
            this._swingfGr1.Location = new System.Drawing.Point(79, 98);
            this._swingfGr1.Name = "_swingfGr1";
            this._swingfGr1.Size = new System.Drawing.Size(47, 20);
            this._swingfGr1.TabIndex = 2;
            this._swingfGr1.Text = "0";
            this._swingfGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(202, 61);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(31, 13);
            this.label119.TabIndex = 1;
            this.label119.Text = "Iр, Iн";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(202, 40);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(43, 13);
            this.label118.TabIndex = 1;
            this.label118.Text = "3I0з, Iн";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(202, 20);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(45, 13);
            this.label117.TabIndex = 1;
            this.label117.Text = "Tdz, мс";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 101);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(61, 13);
            this.label115.TabIndex = 1;
            this.label115.Text = "r, Ом втор.";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 20);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(26, 13);
            this.label112.TabIndex = 1;
            this.label112.Text = "Тип";
            // 
            // _swingTypeGr1
            // 
            this._swingTypeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._swingTypeGr1.FormattingEnabled = true;
            this._swingTypeGr1.Location = new System.Drawing.Point(79, 17);
            this._swingTypeGr1.Name = "_swingTypeGr1";
            this._swingTypeGr1.Size = new System.Drawing.Size(116, 21);
            this._swingTypeGr1.TabIndex = 0;
            this._swingTypeGr1.SelectedIndexChanged += new System.EventHandler(this._swingTypeGr1_SelectedIndexChanged);
            // 
            // _loadedGroup
            // 
            this._loadedGroup.Controls.Add(this.loadFaz2);
            this._loadedGroup.Controls.Add(this.loadLin2);
            this._loadedGroup.Controls.Add(this.loadFaz1);
            this._loadedGroup.Controls.Add(this.loadLin1);
            this._loadedGroup.Location = new System.Drawing.Point(6, 293);
            this._loadedGroup.Name = "_loadedGroup";
            this._loadedGroup.Size = new System.Drawing.Size(444, 110);
            this._loadedGroup.TabIndex = 6;
            this._loadedGroup.TabStop = false;
            this._loadedGroup.Text = "Учет нагрузки";
            // 
            // loadFaz2
            // 
            this.loadFaz2.Controls.Add(this._r2FazGr2);
            this.loadFaz2.Controls.Add(this.label154);
            this.loadFaz2.Controls.Add(this._r1FazGr2);
            this.loadFaz2.Controls.Add(this.label155);
            this.loadFaz2.Controls.Add(this.maskedTextBox4);
            this.loadFaz2.Controls.Add(this.label156);
            this.loadFaz2.Location = new System.Drawing.Point(6, 60);
            this.loadFaz2.Name = "loadFaz2";
            this.loadFaz2.Size = new System.Drawing.Size(432, 43);
            this.loadFaz2.TabIndex = 27;
            this.loadFaz2.TabStop = false;
            this.loadFaz2.Text = "Для контуров Ф-N";
            this.loadFaz2.Visible = false;
            // 
            // _r2FazGr2
            // 
            this._r2FazGr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r2FazGr2.Location = new System.Drawing.Point(314, 17);
            this._r2FazGr2.Name = "_r2FazGr2";
            this._r2FazGr2.Size = new System.Drawing.Size(45, 20);
            this._r2FazGr2.TabIndex = 23;
            this._r2FazGr2.Tag = "1500";
            this._r2FazGr2.Text = "0";
            this._r2FazGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(238, 19);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(73, 13);
            this.label154.TabIndex = 26;
            this.label154.Text = "R2, Ом перв.";
            // 
            // _r1FazGr2
            // 
            this._r1FazGr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r1FazGr2.Location = new System.Drawing.Point(179, 17);
            this._r1FazGr2.Name = "_r1FazGr2";
            this._r1FazGr2.Size = new System.Drawing.Size(45, 20);
            this._r1FazGr2.TabIndex = 23;
            this._r1FazGr2.Tag = "1500";
            this._r1FazGr2.Text = "0";
            this._r1FazGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(103, 20);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(73, 13);
            this.label155.TabIndex = 26;
            this.label155.Text = "R1, Ом перв.";
            // 
            // maskedTextBox4
            // 
            this.maskedTextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maskedTextBox4.Location = new System.Drawing.Point(62, 17);
            this.maskedTextBox4.Name = "maskedTextBox4";
            this.maskedTextBox4.Size = new System.Drawing.Size(30, 20);
            this.maskedTextBox4.TabIndex = 23;
            this.maskedTextBox4.Tag = "1500";
            this.maskedTextBox4.Text = "0";
            this.maskedTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(11, 20);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(50, 13);
            this.label156.TabIndex = 26;
            this.label156.Text = "у1, град.";
            // 
            // loadLin2
            // 
            this.loadLin2.Controls.Add(this._r2LinGr2);
            this.loadLin2.Controls.Add(this.label135);
            this.loadLin2.Controls.Add(this._r1LinGr2);
            this.loadLin2.Controls.Add(this.label140);
            this.loadLin2.Controls.Add(this.maskedTextBox3);
            this.loadLin2.Controls.Add(this.label153);
            this.loadLin2.Location = new System.Drawing.Point(6, 15);
            this.loadLin2.Name = "loadLin2";
            this.loadLin2.Size = new System.Drawing.Size(432, 43);
            this.loadLin2.TabIndex = 27;
            this.loadLin2.TabStop = false;
            this.loadLin2.Text = "Для контуров Ф-Ф";
            this.loadLin2.Visible = false;
            // 
            // _r2LinGr2
            // 
            this._r2LinGr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r2LinGr2.Location = new System.Drawing.Point(314, 17);
            this._r2LinGr2.Name = "_r2LinGr2";
            this._r2LinGr2.Size = new System.Drawing.Size(45, 20);
            this._r2LinGr2.TabIndex = 23;
            this._r2LinGr2.Tag = "1500";
            this._r2LinGr2.Text = "0";
            this._r2LinGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(238, 19);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(73, 13);
            this.label135.TabIndex = 26;
            this.label135.Text = "R2, Ом перв.";
            // 
            // _r1LinGr2
            // 
            this._r1LinGr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r1LinGr2.Location = new System.Drawing.Point(179, 17);
            this._r1LinGr2.Name = "_r1LinGr2";
            this._r1LinGr2.Size = new System.Drawing.Size(45, 20);
            this._r1LinGr2.TabIndex = 23;
            this._r1LinGr2.Tag = "1500";
            this._r1LinGr2.Text = "0";
            this._r1LinGr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(103, 19);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(73, 13);
            this.label140.TabIndex = 26;
            this.label140.Text = "R1, Ом перв.";
            // 
            // maskedTextBox3
            // 
            this.maskedTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.maskedTextBox3.Location = new System.Drawing.Point(62, 17);
            this.maskedTextBox3.Name = "maskedTextBox3";
            this.maskedTextBox3.Size = new System.Drawing.Size(30, 20);
            this.maskedTextBox3.TabIndex = 23;
            this.maskedTextBox3.Tag = "1500";
            this.maskedTextBox3.Text = "0";
            this.maskedTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(11, 19);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(50, 13);
            this.label153.TabIndex = 26;
            this.label153.Text = "у1, град.";
            // 
            // loadFaz1
            // 
            this.loadFaz1.Controls.Add(this._r2FazGr1);
            this.loadFaz1.Controls.Add(this._l8);
            this.loadFaz1.Controls.Add(this._r1FazGr1);
            this.loadFaz1.Controls.Add(this._l7);
            this.loadFaz1.Controls.Add(this._cornerFazGr1);
            this.loadFaz1.Controls.Add(this.label110);
            this.loadFaz1.Location = new System.Drawing.Point(6, 60);
            this.loadFaz1.Name = "loadFaz1";
            this.loadFaz1.Size = new System.Drawing.Size(365, 43);
            this.loadFaz1.TabIndex = 1;
            this.loadFaz1.TabStop = false;
            this.loadFaz1.Text = "Для контуров Ф-N";
            // 
            // _r2FazGr1
            // 
            this._r2FazGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r2FazGr1.Location = new System.Drawing.Point(314, 17);
            this._r2FazGr1.Name = "_r2FazGr1";
            this._r2FazGr1.Size = new System.Drawing.Size(45, 20);
            this._r2FazGr1.TabIndex = 23;
            this._r2FazGr1.Tag = "1500";
            this._r2FazGr1.Text = "0";
            this._r2FazGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l8
            // 
            this._l8.AutoSize = true;
            this._l8.Location = new System.Drawing.Point(238, 19);
            this._l8.Name = "_l8";
            this._l8.Size = new System.Drawing.Size(72, 13);
            this._l8.TabIndex = 26;
            this._l8.Text = "R2, Ом втор.";
            // 
            // _r1FazGr1
            // 
            this._r1FazGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r1FazGr1.Location = new System.Drawing.Point(179, 17);
            this._r1FazGr1.Name = "_r1FazGr1";
            this._r1FazGr1.Size = new System.Drawing.Size(45, 20);
            this._r1FazGr1.TabIndex = 23;
            this._r1FazGr1.Tag = "1500";
            this._r1FazGr1.Text = "0";
            this._r1FazGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l7
            // 
            this._l7.AutoSize = true;
            this._l7.Location = new System.Drawing.Point(103, 20);
            this._l7.Name = "_l7";
            this._l7.Size = new System.Drawing.Size(72, 13);
            this._l7.TabIndex = 26;
            this._l7.Text = "R1, Ом втор.";
            // 
            // _cornerFazGr1
            // 
            this._cornerFazGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cornerFazGr1.Location = new System.Drawing.Point(62, 17);
            this._cornerFazGr1.Name = "_cornerFazGr1";
            this._cornerFazGr1.Size = new System.Drawing.Size(30, 20);
            this._cornerFazGr1.TabIndex = 23;
            this._cornerFazGr1.Tag = "1500";
            this._cornerFazGr1.Text = "0";
            this._cornerFazGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(11, 20);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(50, 13);
            this.label110.TabIndex = 26;
            this.label110.Text = "у1, град.";
            // 
            // loadLin1
            // 
            this.loadLin1.Controls.Add(this._r2LinGr1);
            this.loadLin1.Controls.Add(this._l6);
            this.loadLin1.Controls.Add(this._r1LinGr1);
            this.loadLin1.Controls.Add(this._l5);
            this.loadLin1.Controls.Add(this._cornerLinGr1);
            this.loadLin1.Controls.Add(this.label96);
            this.loadLin1.Location = new System.Drawing.Point(6, 15);
            this.loadLin1.Name = "loadLin1";
            this.loadLin1.Size = new System.Drawing.Size(365, 43);
            this.loadLin1.TabIndex = 1;
            this.loadLin1.TabStop = false;
            this.loadLin1.Text = "Для контуров Ф-Ф";
            // 
            // _r2LinGr1
            // 
            this._r2LinGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r2LinGr1.Location = new System.Drawing.Point(314, 17);
            this._r2LinGr1.Name = "_r2LinGr1";
            this._r2LinGr1.Size = new System.Drawing.Size(45, 20);
            this._r2LinGr1.TabIndex = 23;
            this._r2LinGr1.Tag = "1500";
            this._r2LinGr1.Text = "0";
            this._r2LinGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l6
            // 
            this._l6.AutoSize = true;
            this._l6.Location = new System.Drawing.Point(238, 19);
            this._l6.Name = "_l6";
            this._l6.Size = new System.Drawing.Size(72, 13);
            this._l6.TabIndex = 26;
            this._l6.Text = "R2, Ом втор.";
            // 
            // _r1LinGr1
            // 
            this._r1LinGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._r1LinGr1.Location = new System.Drawing.Point(179, 17);
            this._r1LinGr1.Name = "_r1LinGr1";
            this._r1LinGr1.Size = new System.Drawing.Size(45, 20);
            this._r1LinGr1.TabIndex = 23;
            this._r1LinGr1.Tag = "1500";
            this._r1LinGr1.Text = "0";
            this._r1LinGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l5
            // 
            this._l5.AutoSize = true;
            this._l5.Location = new System.Drawing.Point(103, 19);
            this._l5.Name = "_l5";
            this._l5.Size = new System.Drawing.Size(72, 13);
            this._l5.TabIndex = 26;
            this._l5.Text = "R1, Ом втор.";
            // 
            // _cornerLinGr1
            // 
            this._cornerLinGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._cornerLinGr1.Location = new System.Drawing.Point(62, 17);
            this._cornerLinGr1.Name = "_cornerLinGr1";
            this._cornerLinGr1.Size = new System.Drawing.Size(30, 20);
            this._cornerLinGr1.TabIndex = 23;
            this._cornerLinGr1.Tag = "1500";
            this._cornerLinGr1.Text = "0";
            this._cornerLinGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(11, 19);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(50, 13);
            this.label96.TabIndex = 26;
            this.label96.Text = "у1, град.";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.calcKoefBtn);
            this.groupBox11.Controls.Add(this.fn2groupBox2);
            this.groupBox11.Controls.Add(this.fn4groupBox1);
            this.groupBox11.Controls.Add(this.fn4groupBox2);
            this.groupBox11.Controls.Add(this.fn5groupBox2);
            this.groupBox11.Controls.Add(this.fn5groupBox1);
            this.groupBox11.Controls.Add(this.fn3groupBox2);
            this.groupBox11.Controls.Add(this.fn3groupBox1);
            this.groupBox11.Controls.Add(this.fn1groupBox2);
            this.groupBox11.Controls.Add(this.fn2groupBox1);
            this.groupBox11.Controls.Add(this.fn1groupBox1);
            this.groupBox11.Location = new System.Drawing.Point(6, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(444, 229);
            this.groupBox11.TabIndex = 5;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Компенсация НП";
            // 
            // calcKoefBtn
            // 
            this.calcKoefBtn.Location = new System.Drawing.Point(227, 193);
            this.calcKoefBtn.Name = "calcKoefBtn";
            this.calcKoefBtn.Size = new System.Drawing.Size(211, 23);
            this.calcKoefBtn.TabIndex = 28;
            this.calcKoefBtn.Text = "Рассчитать коэффициенты Ko, Z0/Z1\r\n";
            this.calcKoefBtn.UseVisualStyleBackColor = true;
            this.calcKoefBtn.Click += new System.EventHandler(this.CalcKoefClick);
            // 
            // fn2groupBox2
            // 
            this.fn2groupBox2.Controls.Add(this._xZ1Step2Gr2);
            this.fn2groupBox2.Controls.Add(this._xZ0Step2Gr2);
            this.fn2groupBox2.Controls.Add(this.label113);
            this.fn2groupBox2.Controls.Add(this.label114);
            this.fn2groupBox2.Controls.Add(this.label116);
            this.fn2groupBox2.Controls.Add(this._rZ1Step2Gr2);
            this.fn2groupBox2.Controls.Add(this.label121);
            this.fn2groupBox2.Controls.Add(this.label126);
            this.fn2groupBox2.Controls.Add(this.label127);
            this.fn2groupBox2.Controls.Add(this._rZ0Step2Gr2);
            this.fn2groupBox2.Location = new System.Drawing.Point(227, 15);
            this.fn2groupBox2.Name = "fn2groupBox2";
            this.fn2groupBox2.Size = new System.Drawing.Size(211, 65);
            this.fn2groupBox2.TabIndex = 27;
            this.fn2groupBox2.TabStop = false;
            this.fn2groupBox2.Text = "Зона Ф-N2";
            this.fn2groupBox2.Visible = false;
            // 
            // _xZ1Step2Gr2
            // 
            this._xZ1Step2Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step2Gr2.Location = new System.Drawing.Point(105, 39);
            this._xZ1Step2Gr2.Name = "_xZ1Step2Gr2";
            this._xZ1Step2Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step2Gr2.TabIndex = 23;
            this._xZ1Step2Gr2.Tag = "1500";
            this._xZ1Step2Gr2.Text = "0";
            this._xZ1Step2Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step2Gr2
            // 
            this._xZ0Step2Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step2Gr2.Location = new System.Drawing.Point(105, 19);
            this._xZ0Step2Gr2.Name = "_xZ0Step2Gr2";
            this._xZ0Step2Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step2Gr2.TabIndex = 23;
            this._xZ0Step2Gr2.Tag = "1500";
            this._xZ0Step2Gr2.Text = "0";
            this._xZ0Step2Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(156, 41);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(53, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "Ом перв.";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(156, 21);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(53, 13);
            this.label114.TabIndex = 26;
            this.label114.Text = "Ом перв.";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 21);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(26, 13);
            this.label116.TabIndex = 26;
            this.label116.Text = "Z0=";
            // 
            // _rZ1Step2Gr2
            // 
            this._rZ1Step2Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step2Gr2.Location = new System.Drawing.Point(36, 39);
            this._rZ1Step2Gr2.Name = "_rZ1Step2Gr2";
            this._rZ1Step2Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step2Gr2.TabIndex = 23;
            this._rZ1Step2Gr2.Tag = "1500";
            this._rZ1Step2Gr2.Text = "0";
            this._rZ1Step2Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(87, 21);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(15, 13);
            this.label121.TabIndex = 26;
            this.label121.Text = "+j";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(87, 41);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(15, 13);
            this.label126.TabIndex = 26;
            this.label126.Text = "+j";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 41);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(26, 13);
            this.label127.TabIndex = 26;
            this.label127.Text = "Z1=";
            // 
            // _rZ0Step2Gr2
            // 
            this._rZ0Step2Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step2Gr2.Location = new System.Drawing.Point(36, 19);
            this._rZ0Step2Gr2.Name = "_rZ0Step2Gr2";
            this._rZ0Step2Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step2Gr2.TabIndex = 23;
            this._rZ0Step2Gr2.Tag = "1500";
            this._rZ0Step2Gr2.Text = "0";
            this._rZ0Step2Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fn4groupBox1
            // 
            this.fn4groupBox1.Controls.Add(this._xZ1Step4Gr1);
            this.fn4groupBox1.Controls.Add(this._xZ0Step4Gr1);
            this.fn4groupBox1.Controls.Add(this._rZ1Step4Gr1);
            this.fn4groupBox1.Controls.Add(this.label13);
            this.fn4groupBox1.Controls.Add(this._rZ0Step4Gr1);
            this.fn4groupBox1.Controls.Add(this.label14);
            this.fn4groupBox1.Controls.Add(this.label15);
            this.fn4groupBox1.Controls.Add(this.label16);
            this.fn4groupBox1.Controls.Add(this.label17);
            this.fn4groupBox1.Controls.Add(this.label18);
            this.fn4groupBox1.Location = new System.Drawing.Point(227, 86);
            this.fn4groupBox1.Name = "fn4groupBox1";
            this.fn4groupBox1.Size = new System.Drawing.Size(211, 65);
            this.fn4groupBox1.TabIndex = 27;
            this.fn4groupBox1.TabStop = false;
            this.fn4groupBox1.Text = "Зона Ф-N4";
            // 
            // _xZ1Step4Gr1
            // 
            this._xZ1Step4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step4Gr1.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step4Gr1.Name = "_xZ1Step4Gr1";
            this._xZ1Step4Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step4Gr1.TabIndex = 23;
            this._xZ1Step4Gr1.Tag = "1500";
            this._xZ1Step4Gr1.Text = "0";
            this._xZ1Step4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step4Gr1
            // 
            this._xZ0Step4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step4Gr1.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step4Gr1.Name = "_xZ0Step4Gr1";
            this._xZ0Step4Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step4Gr1.TabIndex = 23;
            this._xZ0Step4Gr1.Tag = "1500";
            this._xZ0Step4Gr1.Text = "0";
            this._xZ0Step4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step4Gr1
            // 
            this._rZ1Step4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step4Gr1.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step4Gr1.Name = "_rZ1Step4Gr1";
            this._rZ1Step4Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step4Gr1.TabIndex = 23;
            this._rZ1Step4Gr1.Tag = "1500";
            this._rZ1Step4Gr1.Text = "0";
            this._rZ1Step4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(90, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "+j";
            // 
            // _rZ0Step4Gr1
            // 
            this._rZ0Step4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step4Gr1.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step4Gr1.Name = "_rZ0Step4Gr1";
            this._rZ0Step4Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step4Gr1.TabIndex = 23;
            this._rZ0Step4Gr1.Tag = "1500";
            this._rZ0Step4Gr1.Text = "0";
            this._rZ0Step4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 41);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Z1=";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(159, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Ом втор.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(159, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(52, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "Ом втор.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(90, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(15, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "+j";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "Z0=";
            // 
            // fn4groupBox2
            // 
            this.fn4groupBox2.Controls.Add(this._xZ1Step4Gr2);
            this.fn4groupBox2.Controls.Add(this._xZ0Step4Gr2);
            this.fn4groupBox2.Controls.Add(this._rZ1Step4Gr2);
            this.fn4groupBox2.Controls.Add(this.label19);
            this.fn4groupBox2.Controls.Add(this._rZ0Step4Gr2);
            this.fn4groupBox2.Controls.Add(this.label20);
            this.fn4groupBox2.Controls.Add(this.label21);
            this.fn4groupBox2.Controls.Add(this.label22);
            this.fn4groupBox2.Controls.Add(this.label23);
            this.fn4groupBox2.Controls.Add(this.label24);
            this.fn4groupBox2.Location = new System.Drawing.Point(227, 86);
            this.fn4groupBox2.Name = "fn4groupBox2";
            this.fn4groupBox2.Size = new System.Drawing.Size(211, 65);
            this.fn4groupBox2.TabIndex = 26;
            this.fn4groupBox2.TabStop = false;
            this.fn4groupBox2.Text = "Зона Ф-N4";
            this.fn4groupBox2.Visible = false;
            // 
            // _xZ1Step4Gr2
            // 
            this._xZ1Step4Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step4Gr2.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step4Gr2.Name = "_xZ1Step4Gr2";
            this._xZ1Step4Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step4Gr2.TabIndex = 23;
            this._xZ1Step4Gr2.Tag = "1500";
            this._xZ1Step4Gr2.Text = "0";
            this._xZ1Step4Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step4Gr2
            // 
            this._xZ0Step4Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step4Gr2.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step4Gr2.Name = "_xZ0Step4Gr2";
            this._xZ0Step4Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step4Gr2.TabIndex = 23;
            this._xZ0Step4Gr2.Tag = "1500";
            this._xZ0Step4Gr2.Text = "0";
            this._xZ0Step4Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step4Gr2
            // 
            this._rZ1Step4Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step4Gr2.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step4Gr2.Name = "_rZ1Step4Gr2";
            this._rZ1Step4Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step4Gr2.TabIndex = 23;
            this._rZ1Step4Gr2.Tag = "1500";
            this._rZ1Step4Gr2.Text = "0";
            this._rZ1Step4Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(90, 41);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(15, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "+j";
            // 
            // _rZ0Step4Gr2
            // 
            this._rZ0Step4Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step4Gr2.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step4Gr2.Name = "_rZ0Step4Gr2";
            this._rZ0Step4Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step4Gr2.TabIndex = 23;
            this._rZ0Step4Gr2.Tag = "1500";
            this._rZ0Step4Gr2.Text = "0";
            this._rZ0Step4Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 41);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(26, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Z1=";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(159, 41);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "Ом перв.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(159, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "Ом перв.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(90, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(15, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "+j";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(9, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(26, 13);
            this.label24.TabIndex = 26;
            this.label24.Text = "Z0=";
            // 
            // fn5groupBox2
            // 
            this.fn5groupBox2.Controls.Add(this._xZ1Step5Gr2);
            this.fn5groupBox2.Controls.Add(this._xZ0Step5Gr2);
            this.fn5groupBox2.Controls.Add(this._rZ1Step5Gr2);
            this.fn5groupBox2.Controls.Add(this.label33);
            this.fn5groupBox2.Controls.Add(this._rZ0Step5Gr2);
            this.fn5groupBox2.Controls.Add(this.label34);
            this.fn5groupBox2.Controls.Add(this.label35);
            this.fn5groupBox2.Controls.Add(this.label36);
            this.fn5groupBox2.Controls.Add(this.label37);
            this.fn5groupBox2.Controls.Add(this.label38);
            this.fn5groupBox2.Location = new System.Drawing.Point(6, 157);
            this.fn5groupBox2.Name = "fn5groupBox2";
            this.fn5groupBox2.Size = new System.Drawing.Size(215, 65);
            this.fn5groupBox2.TabIndex = 27;
            this.fn5groupBox2.TabStop = false;
            this.fn5groupBox2.Text = "Зона Ф-N5";
            this.fn5groupBox2.Visible = false;
            // 
            // _xZ1Step5Gr2
            // 
            this._xZ1Step5Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step5Gr2.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step5Gr2.Name = "_xZ1Step5Gr2";
            this._xZ1Step5Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step5Gr2.TabIndex = 23;
            this._xZ1Step5Gr2.Tag = "1500";
            this._xZ1Step5Gr2.Text = "0";
            this._xZ1Step5Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step5Gr2
            // 
            this._xZ0Step5Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step5Gr2.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step5Gr2.Name = "_xZ0Step5Gr2";
            this._xZ0Step5Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step5Gr2.TabIndex = 23;
            this._xZ0Step5Gr2.Tag = "1500";
            this._xZ0Step5Gr2.Text = "0";
            this._xZ0Step5Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step5Gr2
            // 
            this._rZ1Step5Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step5Gr2.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step5Gr2.Name = "_rZ1Step5Gr2";
            this._rZ1Step5Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step5Gr2.TabIndex = 23;
            this._rZ1Step5Gr2.Tag = "1500";
            this._rZ1Step5Gr2.Text = "0";
            this._rZ1Step5Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(90, 41);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(15, 13);
            this.label33.TabIndex = 26;
            this.label33.Text = "+j";
            // 
            // _rZ0Step5Gr2
            // 
            this._rZ0Step5Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step5Gr2.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step5Gr2.Name = "_rZ0Step5Gr2";
            this._rZ0Step5Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step5Gr2.TabIndex = 23;
            this._rZ0Step5Gr2.Tag = "1500";
            this._rZ0Step5Gr2.Text = "0";
            this._rZ0Step5Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(9, 41);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 13);
            this.label34.TabIndex = 26;
            this.label34.Text = "Z1=";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(159, 41);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 13);
            this.label35.TabIndex = 26;
            this.label35.Text = "Ом перв.";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(159, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 13);
            this.label36.TabIndex = 26;
            this.label36.Text = "Ом перв.";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(90, 21);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(15, 13);
            this.label37.TabIndex = 26;
            this.label37.Text = "+j";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(9, 21);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 13);
            this.label38.TabIndex = 26;
            this.label38.Text = "Z0=";
            // 
            // fn5groupBox1
            // 
            this.fn5groupBox1.Controls.Add(this._xZ1Step5Gr1);
            this.fn5groupBox1.Controls.Add(this._xZ0Step5Gr1);
            this.fn5groupBox1.Controls.Add(this._rZ1Step5Gr1);
            this.fn5groupBox1.Controls.Add(this.label25);
            this.fn5groupBox1.Controls.Add(this._rZ0Step5Gr1);
            this.fn5groupBox1.Controls.Add(this.label26);
            this.fn5groupBox1.Controls.Add(this.label27);
            this.fn5groupBox1.Controls.Add(this.label29);
            this.fn5groupBox1.Controls.Add(this.label31);
            this.fn5groupBox1.Controls.Add(this.label32);
            this.fn5groupBox1.Location = new System.Drawing.Point(6, 157);
            this.fn5groupBox1.Name = "fn5groupBox1";
            this.fn5groupBox1.Size = new System.Drawing.Size(215, 65);
            this.fn5groupBox1.TabIndex = 27;
            this.fn5groupBox1.TabStop = false;
            this.fn5groupBox1.Text = "Зона Ф-N5";
            // 
            // _xZ1Step5Gr1
            // 
            this._xZ1Step5Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step5Gr1.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step5Gr1.Name = "_xZ1Step5Gr1";
            this._xZ1Step5Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step5Gr1.TabIndex = 23;
            this._xZ1Step5Gr1.Tag = "1500";
            this._xZ1Step5Gr1.Text = "0";
            this._xZ1Step5Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step5Gr1
            // 
            this._xZ0Step5Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step5Gr1.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step5Gr1.Name = "_xZ0Step5Gr1";
            this._xZ0Step5Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step5Gr1.TabIndex = 23;
            this._xZ0Step5Gr1.Tag = "1500";
            this._xZ0Step5Gr1.Text = "0";
            this._xZ0Step5Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step5Gr1
            // 
            this._rZ1Step5Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step5Gr1.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step5Gr1.Name = "_rZ1Step5Gr1";
            this._rZ1Step5Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step5Gr1.TabIndex = 23;
            this._rZ1Step5Gr1.Tag = "1500";
            this._rZ1Step5Gr1.Text = "0";
            this._rZ1Step5Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(90, 41);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 13);
            this.label25.TabIndex = 26;
            this.label25.Text = "+j";
            // 
            // _rZ0Step5Gr1
            // 
            this._rZ0Step5Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step5Gr1.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step5Gr1.Name = "_rZ0Step5Gr1";
            this._rZ0Step5Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step5Gr1.TabIndex = 23;
            this._rZ0Step5Gr1.Tag = "1500";
            this._rZ0Step5Gr1.Text = "0";
            this._rZ0Step5Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 41);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(26, 13);
            this.label26.TabIndex = 26;
            this.label26.Text = "Z1=";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(159, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(52, 13);
            this.label27.TabIndex = 26;
            this.label27.Text = "Ом втор.";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(159, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 26;
            this.label29.Text = "Ом втор.";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(90, 21);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(15, 13);
            this.label31.TabIndex = 26;
            this.label31.Text = "+j";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(9, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 13);
            this.label32.TabIndex = 26;
            this.label32.Text = "Z0=";
            // 
            // fn3groupBox2
            // 
            this.fn3groupBox2.Controls.Add(this._xZ1Step3Gr2);
            this.fn3groupBox2.Controls.Add(this._xZ0Step3Gr2);
            this.fn3groupBox2.Controls.Add(this._rZ1Step3Gr2);
            this.fn3groupBox2.Controls.Add(this.label7);
            this.fn3groupBox2.Controls.Add(this._rZ0Step3Gr2);
            this.fn3groupBox2.Controls.Add(this.label8);
            this.fn3groupBox2.Controls.Add(this.label9);
            this.fn3groupBox2.Controls.Add(this.label10);
            this.fn3groupBox2.Controls.Add(this.label11);
            this.fn3groupBox2.Controls.Add(this.label12);
            this.fn3groupBox2.Location = new System.Drawing.Point(6, 86);
            this.fn3groupBox2.Name = "fn3groupBox2";
            this.fn3groupBox2.Size = new System.Drawing.Size(215, 65);
            this.fn3groupBox2.TabIndex = 27;
            this.fn3groupBox2.TabStop = false;
            this.fn3groupBox2.Text = "Зона Ф-N3";
            this.fn3groupBox2.Visible = false;
            // 
            // _xZ1Step3Gr2
            // 
            this._xZ1Step3Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step3Gr2.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step3Gr2.Name = "_xZ1Step3Gr2";
            this._xZ1Step3Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step3Gr2.TabIndex = 23;
            this._xZ1Step3Gr2.Tag = "1500";
            this._xZ1Step3Gr2.Text = "0";
            this._xZ1Step3Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step3Gr2
            // 
            this._xZ0Step3Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step3Gr2.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step3Gr2.Name = "_xZ0Step3Gr2";
            this._xZ0Step3Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step3Gr2.TabIndex = 23;
            this._xZ0Step3Gr2.Tag = "1500";
            this._xZ0Step3Gr2.Text = "0";
            this._xZ0Step3Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step3Gr2
            // 
            this._rZ1Step3Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step3Gr2.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step3Gr2.Name = "_rZ1Step3Gr2";
            this._rZ1Step3Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step3Gr2.TabIndex = 23;
            this._rZ1Step3Gr2.Tag = "1500";
            this._rZ1Step3Gr2.Text = "0";
            this._rZ1Step3Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(90, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "+j";
            // 
            // _rZ0Step3Gr2
            // 
            this._rZ0Step3Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step3Gr2.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step3Gr2.Name = "_rZ0Step3Gr2";
            this._rZ0Step3Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step3Gr2.TabIndex = 23;
            this._rZ0Step3Gr2.Tag = "1500";
            this._rZ0Step3Gr2.Text = "0";
            this._rZ0Step3Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Z1=";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(159, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Ом перв.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(159, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Ом перв.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(90, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "+j";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Z0=";
            // 
            // fn3groupBox1
            // 
            this.fn3groupBox1.Controls.Add(this._xZ1Step3Gr1);
            this.fn3groupBox1.Controls.Add(this._xZ0Step3Gr1);
            this.fn3groupBox1.Controls.Add(this._rZ1Step3Gr1);
            this.fn3groupBox1.Controls.Add(this.label1);
            this.fn3groupBox1.Controls.Add(this._rZ0Step3Gr1);
            this.fn3groupBox1.Controls.Add(this.label2);
            this.fn3groupBox1.Controls.Add(this.label3);
            this.fn3groupBox1.Controls.Add(this.label4);
            this.fn3groupBox1.Controls.Add(this.label5);
            this.fn3groupBox1.Controls.Add(this.label6);
            this.fn3groupBox1.Location = new System.Drawing.Point(6, 86);
            this.fn3groupBox1.Name = "fn3groupBox1";
            this.fn3groupBox1.Size = new System.Drawing.Size(215, 65);
            this.fn3groupBox1.TabIndex = 27;
            this.fn3groupBox1.TabStop = false;
            this.fn3groupBox1.Text = "Зона Ф-N3";
            // 
            // _xZ1Step3Gr1
            // 
            this._xZ1Step3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step3Gr1.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step3Gr1.Name = "_xZ1Step3Gr1";
            this._xZ1Step3Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step3Gr1.TabIndex = 23;
            this._xZ1Step3Gr1.Tag = "1500";
            this._xZ1Step3Gr1.Text = "0";
            this._xZ1Step3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step3Gr1
            // 
            this._xZ0Step3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step3Gr1.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step3Gr1.Name = "_xZ0Step3Gr1";
            this._xZ0Step3Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step3Gr1.TabIndex = 23;
            this._xZ0Step3Gr1.Tag = "1500";
            this._xZ0Step3Gr1.Text = "0";
            this._xZ0Step3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step3Gr1
            // 
            this._rZ1Step3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step3Gr1.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step3Gr1.Name = "_rZ1Step3Gr1";
            this._rZ1Step3Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step3Gr1.TabIndex = 23;
            this._rZ1Step3Gr1.Tag = "1500";
            this._rZ1Step3Gr1.Text = "0";
            this._rZ1Step3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "+j";
            // 
            // _rZ0Step3Gr1
            // 
            this._rZ0Step3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step3Gr1.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step3Gr1.Name = "_rZ0Step3Gr1";
            this._rZ0Step3Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step3Gr1.TabIndex = 23;
            this._rZ0Step3Gr1.Tag = "1500";
            this._rZ0Step3Gr1.Text = "0";
            this._rZ0Step3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Z1=";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(159, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Ом втор.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(159, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Ом втор.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(90, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "+j";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Z0=";
            // 
            // fn1groupBox2
            // 
            this.fn1groupBox2.Controls.Add(this._xZ1Step1Gr2);
            this.fn1groupBox2.Controls.Add(this._xZ0Step1Gr2);
            this.fn1groupBox2.Controls.Add(this._rZ1Step1Gr2);
            this.fn1groupBox2.Controls.Add(this.label52);
            this.fn1groupBox2.Controls.Add(this._rZ0Step1Gr2);
            this.fn1groupBox2.Controls.Add(this.label53);
            this.fn1groupBox2.Controls.Add(this.label74);
            this.fn1groupBox2.Controls.Add(this.label95);
            this.fn1groupBox2.Controls.Add(this.label109);
            this.fn1groupBox2.Controls.Add(this.label111);
            this.fn1groupBox2.Location = new System.Drawing.Point(6, 15);
            this.fn1groupBox2.Name = "fn1groupBox2";
            this.fn1groupBox2.Size = new System.Drawing.Size(215, 65);
            this.fn1groupBox2.TabIndex = 27;
            this.fn1groupBox2.TabStop = false;
            this.fn1groupBox2.Text = "Зона Ф-N1";
            this.fn1groupBox2.Visible = false;
            // 
            // _xZ1Step1Gr2
            // 
            this._xZ1Step1Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step1Gr2.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step1Gr2.Name = "_xZ1Step1Gr2";
            this._xZ1Step1Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step1Gr2.TabIndex = 23;
            this._xZ1Step1Gr2.Tag = "1500";
            this._xZ1Step1Gr2.Text = "0";
            this._xZ1Step1Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step1Gr2
            // 
            this._xZ0Step1Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step1Gr2.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step1Gr2.Name = "_xZ0Step1Gr2";
            this._xZ0Step1Gr2.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step1Gr2.TabIndex = 23;
            this._xZ0Step1Gr2.Tag = "1500";
            this._xZ0Step1Gr2.Text = "0";
            this._xZ0Step1Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step1Gr2
            // 
            this._rZ1Step1Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step1Gr2.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step1Gr2.Name = "_rZ1Step1Gr2";
            this._rZ1Step1Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step1Gr2.TabIndex = 23;
            this._rZ1Step1Gr2.Tag = "1500";
            this._rZ1Step1Gr2.Text = "0";
            this._rZ1Step1Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 41);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(15, 13);
            this.label52.TabIndex = 26;
            this.label52.Text = "+j";
            // 
            // _rZ0Step1Gr2
            // 
            this._rZ0Step1Gr2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step1Gr2.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step1Gr2.Name = "_rZ0Step1Gr2";
            this._rZ0Step1Gr2.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step1Gr2.TabIndex = 23;
            this._rZ0Step1Gr2.Tag = "1500";
            this._rZ0Step1Gr2.Text = "0";
            this._rZ0Step1Gr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(9, 41);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(26, 13);
            this.label53.TabIndex = 26;
            this.label53.Text = "Z1=";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(159, 41);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(53, 13);
            this.label74.TabIndex = 26;
            this.label74.Text = "Ом перв.";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(159, 21);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(53, 13);
            this.label95.TabIndex = 26;
            this.label95.Text = "Ом перв.";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(90, 21);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(15, 13);
            this.label109.TabIndex = 26;
            this.label109.Text = "+j";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(9, 21);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(26, 13);
            this.label111.TabIndex = 26;
            this.label111.Text = "Z0=";
            // 
            // fn2groupBox1
            // 
            this.fn2groupBox1.Controls.Add(this._xZ1Step2Gr1);
            this.fn2groupBox1.Controls.Add(this._xZ0Step2Gr1);
            this.fn2groupBox1.Controls.Add(this._l4);
            this.fn2groupBox1.Controls.Add(this._l3);
            this.fn2groupBox1.Controls.Add(this.label83);
            this.fn2groupBox1.Controls.Add(this._rZ1Step2Gr1);
            this.fn2groupBox1.Controls.Add(this.label84);
            this.fn2groupBox1.Controls.Add(this.label86);
            this.fn2groupBox1.Controls.Add(this.label85);
            this.fn2groupBox1.Controls.Add(this._rZ0Step2Gr1);
            this.fn2groupBox1.Location = new System.Drawing.Point(227, 15);
            this.fn2groupBox1.Name = "fn2groupBox1";
            this.fn2groupBox1.Size = new System.Drawing.Size(211, 65);
            this.fn2groupBox1.TabIndex = 0;
            this.fn2groupBox1.TabStop = false;
            this.fn2groupBox1.Text = "Зона Ф-N2";
            // 
            // _xZ1Step2Gr1
            // 
            this._xZ1Step2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step2Gr1.Location = new System.Drawing.Point(105, 39);
            this._xZ1Step2Gr1.Name = "_xZ1Step2Gr1";
            this._xZ1Step2Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step2Gr1.TabIndex = 23;
            this._xZ1Step2Gr1.Tag = "1500";
            this._xZ1Step2Gr1.Text = "0";
            this._xZ1Step2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step2Gr1
            // 
            this._xZ0Step2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step2Gr1.Location = new System.Drawing.Point(105, 19);
            this._xZ0Step2Gr1.Name = "_xZ0Step2Gr1";
            this._xZ0Step2Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step2Gr1.TabIndex = 23;
            this._xZ0Step2Gr1.Tag = "1500";
            this._xZ0Step2Gr1.Text = "0";
            this._xZ0Step2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _l4
            // 
            this._l4.AutoSize = true;
            this._l4.Location = new System.Drawing.Point(156, 41);
            this._l4.Name = "_l4";
            this._l4.Size = new System.Drawing.Size(52, 13);
            this._l4.TabIndex = 26;
            this._l4.Text = "Ом втор.";
            // 
            // _l3
            // 
            this._l3.AutoSize = true;
            this._l3.Location = new System.Drawing.Point(156, 21);
            this._l3.Name = "_l3";
            this._l3.Size = new System.Drawing.Size(52, 13);
            this._l3.TabIndex = 26;
            this._l3.Text = "Ом втор.";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 21);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(26, 13);
            this.label83.TabIndex = 26;
            this.label83.Text = "Z0=";
            // 
            // _rZ1Step2Gr1
            // 
            this._rZ1Step2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step2Gr1.Location = new System.Drawing.Point(36, 39);
            this._rZ1Step2Gr1.Name = "_rZ1Step2Gr1";
            this._rZ1Step2Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step2Gr1.TabIndex = 23;
            this._rZ1Step2Gr1.Tag = "1500";
            this._rZ1Step2Gr1.Text = "0";
            this._rZ1Step2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(87, 21);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(15, 13);
            this.label84.TabIndex = 26;
            this.label84.Text = "+j";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(87, 41);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(15, 13);
            this.label86.TabIndex = 26;
            this.label86.Text = "+j";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(6, 41);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(26, 13);
            this.label85.TabIndex = 26;
            this.label85.Text = "Z1=";
            // 
            // _rZ0Step2Gr1
            // 
            this._rZ0Step2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step2Gr1.Location = new System.Drawing.Point(36, 19);
            this._rZ0Step2Gr1.Name = "_rZ0Step2Gr1";
            this._rZ0Step2Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step2Gr1.TabIndex = 23;
            this._rZ0Step2Gr1.Tag = "1500";
            this._rZ0Step2Gr1.Text = "0";
            this._rZ0Step2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // fn1groupBox1
            // 
            this.fn1groupBox1.Controls.Add(this._xZ1Step1Gr1);
            this.fn1groupBox1.Controls.Add(this._xZ0Step1Gr1);
            this.fn1groupBox1.Controls.Add(this._rZ1Step1Gr1);
            this.fn1groupBox1.Controls.Add(this.label82);
            this.fn1groupBox1.Controls.Add(this._rZ0Step1Gr1);
            this.fn1groupBox1.Controls.Add(this.label81);
            this.fn1groupBox1.Controls.Add(this._l2);
            this.fn1groupBox1.Controls.Add(this._l1);
            this.fn1groupBox1.Controls.Add(this.label30);
            this.fn1groupBox1.Controls.Add(this.label28);
            this.fn1groupBox1.Location = new System.Drawing.Point(6, 15);
            this.fn1groupBox1.Name = "fn1groupBox1";
            this.fn1groupBox1.Size = new System.Drawing.Size(215, 65);
            this.fn1groupBox1.TabIndex = 0;
            this.fn1groupBox1.TabStop = false;
            this.fn1groupBox1.Text = "Зона Ф-N1";
            // 
            // _xZ1Step1Gr1
            // 
            this._xZ1Step1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ1Step1Gr1.Location = new System.Drawing.Point(108, 39);
            this._xZ1Step1Gr1.Name = "_xZ1Step1Gr1";
            this._xZ1Step1Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ1Step1Gr1.TabIndex = 23;
            this._xZ1Step1Gr1.Tag = "1500";
            this._xZ1Step1Gr1.Text = "0";
            this._xZ1Step1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xZ0Step1Gr1
            // 
            this._xZ0Step1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xZ0Step1Gr1.Location = new System.Drawing.Point(108, 19);
            this._xZ0Step1Gr1.Name = "_xZ0Step1Gr1";
            this._xZ0Step1Gr1.Size = new System.Drawing.Size(45, 20);
            this._xZ0Step1Gr1.TabIndex = 23;
            this._xZ0Step1Gr1.Tag = "1500";
            this._xZ0Step1Gr1.Text = "0";
            this._xZ0Step1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _rZ1Step1Gr1
            // 
            this._rZ1Step1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ1Step1Gr1.Location = new System.Drawing.Point(39, 39);
            this._rZ1Step1Gr1.Name = "_rZ1Step1Gr1";
            this._rZ1Step1Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ1Step1Gr1.TabIndex = 23;
            this._rZ1Step1Gr1.Tag = "1500";
            this._rZ1Step1Gr1.Text = "0";
            this._rZ1Step1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(90, 41);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(15, 13);
            this.label82.TabIndex = 26;
            this.label82.Text = "+j";
            // 
            // _rZ0Step1Gr1
            // 
            this._rZ0Step1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._rZ0Step1Gr1.Location = new System.Drawing.Point(39, 19);
            this._rZ0Step1Gr1.Name = "_rZ0Step1Gr1";
            this._rZ0Step1Gr1.Size = new System.Drawing.Size(45, 20);
            this._rZ0Step1Gr1.TabIndex = 23;
            this._rZ0Step1Gr1.Tag = "1500";
            this._rZ0Step1Gr1.Text = "0";
            this._rZ0Step1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(9, 41);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(26, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "Z1=";
            // 
            // _l2
            // 
            this._l2.AutoSize = true;
            this._l2.Location = new System.Drawing.Point(159, 41);
            this._l2.Name = "_l2";
            this._l2.Size = new System.Drawing.Size(52, 13);
            this._l2.TabIndex = 26;
            this._l2.Text = "Ом втор.";
            // 
            // _l1
            // 
            this._l1.AutoSize = true;
            this._l1.Location = new System.Drawing.Point(159, 21);
            this._l1.Name = "_l1";
            this._l1.Size = new System.Drawing.Size(52, 13);
            this._l1.TabIndex = 26;
            this._l1.Text = "Ом втор.";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(90, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 26;
            this.label30.Text = "+j";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 21);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(26, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "Z0=";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.resistanceDefTabPage1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(860, 515);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Z 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage1
            // 
            this.resistanceDefTabPage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resistanceDefTabPage1.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage1.Name = "resistanceDefTabPage1";
            this.resistanceDefTabPage1.Size = new System.Drawing.Size(854, 509);
            this.resistanceDefTabPage1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.resistanceDefTabPage2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(860, 515);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Z 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage2
            // 
            this.resistanceDefTabPage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resistanceDefTabPage2.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage2.Name = "resistanceDefTabPage2";
            this.resistanceDefTabPage2.Size = new System.Drawing.Size(854, 509);
            this.resistanceDefTabPage2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.resistanceDefTabPage3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(860, 515);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Z 3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage3
            // 
            this.resistanceDefTabPage3.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage3.Name = "resistanceDefTabPage3";
            this.resistanceDefTabPage3.Size = new System.Drawing.Size(736, 428);
            this.resistanceDefTabPage3.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.resistanceDefTabPage4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(860, 515);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Z 4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage4
            // 
            this.resistanceDefTabPage4.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage4.Name = "resistanceDefTabPage4";
            this.resistanceDefTabPage4.Size = new System.Drawing.Size(821, 477);
            this.resistanceDefTabPage4.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.resistanceDefTabPage5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(860, 515);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Z 5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage5
            // 
            this.resistanceDefTabPage5.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage5.Name = "resistanceDefTabPage5";
            this.resistanceDefTabPage5.Size = new System.Drawing.Size(661, 391);
            this.resistanceDefTabPage5.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.resistanceDefTabPage6);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(860, 515);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Z 6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabPage6
            // 
            this.resistanceDefTabPage6.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabPage6.Name = "resistanceDefTabPage6";
            this.resistanceDefTabPage6.Size = new System.Drawing.Size(688, 417);
            this.resistanceDefTabPage6.TabIndex = 1;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._isMash);
            this.tabPage11.Controls.Add(this.saveResistParamsBtn);
            this.tabPage11.Controls.Add(this.panel7);
            this.tabPage11.Controls.Add(this.button1);
            this.tabPage11.Controls.Add(this.DrawPanel);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(860, 515);
            this.tabPage11.TabIndex = 11;
            this.tabPage11.Text = "Диаграмма";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _isMash
            // 
            this._isMash.AutoSize = true;
            this._isMash.Location = new System.Drawing.Point(479, 331);
            this._isMash.Name = "_isMash";
            this._isMash.Size = new System.Drawing.Size(189, 17);
            this._isMash.TabIndex = 7;
            this._isMash.Text = "Масштабирование по 2-ум осям";
            this.toolTip1.SetToolTip(this._isMash, "При наличии круговой диаграммы масштабирование не произойдет");
            this._isMash.UseVisualStyleBackColor = true;
            this._isMash.CheckedChanged += new System.EventHandler(this._isMash_CheckedChanged);
            // 
            // saveResistParamsBtn
            // 
            this.saveResistParamsBtn.Location = new System.Drawing.Point(479, 351);
            this.saveResistParamsBtn.Name = "saveResistParamsBtn";
            this.saveResistParamsBtn.Size = new System.Drawing.Size(265, 23);
            this.saveResistParamsBtn.TabIndex = 6;
            this.saveResistParamsBtn.Text = "Сохранить характеристики";
            this.saveResistParamsBtn.UseVisualStyleBackColor = true;
            this.saveResistParamsBtn.Click += new System.EventHandler(this.saveResistParamsBtn_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.characteristicEnableControl6);
            this.panel7.Controls.Add(this.characteristicEnableControl5);
            this.panel7.Controls.Add(this.characteristicEnableControl4);
            this.panel7.Controls.Add(this.characteristicEnableControl3);
            this.panel7.Controls.Add(this.characteristicEnableControl2);
            this.panel7.Controls.Add(this.characteristicEnableControl1);
            this.panel7.Location = new System.Drawing.Point(479, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(308, 322);
            this.panel7.TabIndex = 5;
            // 
            // characteristicEnableControl6
            // 
            this.characteristicEnableControl6.CurrentCharacterictic = null;
            this.characteristicEnableControl6.Location = new System.Drawing.Point(3, 143);
            this.characteristicEnableControl6.Name = "characteristicEnableControl6";
            this.characteristicEnableControl6.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl6.TabIndex = 8;
            this.characteristicEnableControl6.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl6.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // characteristicEnableControl5
            // 
            this.characteristicEnableControl5.CurrentCharacterictic = null;
            this.characteristicEnableControl5.Location = new System.Drawing.Point(3, 115);
            this.characteristicEnableControl5.Name = "characteristicEnableControl5";
            this.characteristicEnableControl5.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl5.TabIndex = 7;
            this.characteristicEnableControl5.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl5.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // characteristicEnableControl4
            // 
            this.characteristicEnableControl4.CurrentCharacterictic = null;
            this.characteristicEnableControl4.Location = new System.Drawing.Point(3, 87);
            this.characteristicEnableControl4.Name = "characteristicEnableControl4";
            this.characteristicEnableControl4.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl4.TabIndex = 6;
            this.characteristicEnableControl4.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl4.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // characteristicEnableControl3
            // 
            this.characteristicEnableControl3.CurrentCharacterictic = null;
            this.characteristicEnableControl3.Location = new System.Drawing.Point(3, 59);
            this.characteristicEnableControl3.Name = "characteristicEnableControl3";
            this.characteristicEnableControl3.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl3.TabIndex = 5;
            this.characteristicEnableControl3.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl3.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // characteristicEnableControl2
            // 
            this.characteristicEnableControl2.CurrentCharacterictic = null;
            this.characteristicEnableControl2.Location = new System.Drawing.Point(3, 31);
            this.characteristicEnableControl2.Name = "characteristicEnableControl2";
            this.characteristicEnableControl2.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl2.TabIndex = 4;
            this.characteristicEnableControl2.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl2.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // characteristicEnableControl1
            // 
            this.characteristicEnableControl1.BackColor = System.Drawing.Color.Transparent;
            this.characteristicEnableControl1.CurrentCharacterictic = null;
            this.characteristicEnableControl1.Location = new System.Drawing.Point(3, 3);
            this.characteristicEnableControl1.Name = "characteristicEnableControl1";
            this.characteristicEnableControl1.Size = new System.Drawing.Size(179, 22);
            this.characteristicEnableControl1.TabIndex = 3;
            this.characteristicEnableControl1.Change += new System.Action(this.characteristicEnableControl1_Change);
            this.characteristicEnableControl1.SelectStage += new System.Action<int>(this.characteristicEnableControl1_SelectStage);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(479, 380);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Обновить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DrawPanel
            // 
            this.DrawPanel.Location = new System.Drawing.Point(3, 3);
            this.DrawPanel.Name = "DrawPanel";
            this.DrawPanel.Size = new System.Drawing.Size(470, 400);
            this.DrawPanel.TabIndex = 3;
            this.DrawPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawPanel_Paint);
            // 
            // _tablePage
            // 
            this._tablePage.Controls.Add(this._infoGrid);
            this._tablePage.Location = new System.Drawing.Point(4, 22);
            this._tablePage.Name = "_tablePage";
            this._tablePage.Padding = new System.Windows.Forms.Padding(3);
            this._tablePage.Size = new System.Drawing.Size(860, 515);
            this._tablePage.TabIndex = 10;
            this._tablePage.Text = "Текущая конфигурация";
            // 
            // _infoGrid
            // 
            this._infoGrid.AllowUserToAddRows = false;
            this._infoGrid.AllowUserToDeleteRows = false;
            this._infoGrid.AllowUserToResizeColumns = false;
            this._infoGrid.AllowUserToResizeRows = false;
            this._infoGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._infoGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._infoGrid.BackgroundColor = System.Drawing.Color.White;
            this._infoGrid.ColumnHeadersHeight = 30;
            this._infoGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._infoGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this._header1,
            this._header2,
            this.Column7,
            this.Column20,
            this.Column21,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column12,
            this.Column13,
            this.Column22,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column23,
            this.Column5,
            this.Column6,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column11});
            this._infoGrid.Location = new System.Drawing.Point(3, 6);
            this._infoGrid.MinimumSize = new System.Drawing.Size(854, 332);
            this._infoGrid.Name = "_infoGrid";
            this._infoGrid.RowHeadersVisible = false;
            this._infoGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._infoGrid.ShowCellErrors = false;
            this._infoGrid.ShowRowErrors = false;
            this._infoGrid.Size = new System.Drawing.Size(854, 332);
            this._infoGrid.TabIndex = 1;
            this._infoGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this._infoGrid_CellDoubleClick);
            // 
            // Column1
            // 
            this.Column1.Frozen = true;
            this.Column1.HeaderText = "Ступень";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 54;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Режим";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 48;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Тип";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 32;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Блокировка";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 74;
            // 
            // _header1
            // 
            this._header1.HeaderText = "R, Ом втор.";
            this._header1.Name = "_header1";
            this._header1.ReadOnly = true;
            this._header1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._header1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._header1.Width = 72;
            // 
            // _header2
            // 
            this._header2.HeaderText = "X, Ом втор.";
            this._header2.Name = "_header2";
            this._header2.ReadOnly = true;
            this._header2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._header2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._header2.Width = 71;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "f, град/r, Ом втор";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column7.Width = 101;
            // 
            // Column20
            // 
            this.Column20.HeaderText = "tcp, мс";
            this.Column20.Name = "Column20";
            this.Column20.ReadOnly = true;
            this.Column20.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column20.Width = 48;
            // 
            // Column21
            // 
            this.Column21.HeaderText = "Icp, Iн";
            this.Column21.Name = "Column21";
            this.Column21.ReadOnly = true;
            this.Column21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column21.Width = 43;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Вход ускорения";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column8.Width = 93;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "tу, мс";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column9.Width = 41;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "Направление";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 81;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Пуск по U";
            this.Column12.Name = "Column12";
            this.Column12.ReadOnly = true;
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column12.Width = 64;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "Uпуск, В";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column13.Width = 57;
            // 
            // Column22
            // 
            this.Column22.HeaderText = "Контур";
            this.Column22.Name = "Column22";
            this.Column22.ReadOnly = true;
            this.Column22.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column22.Width = 50;
            // 
            // Column14
            // 
            this.Column14.HeaderText = "Блок. от неиспр. ТН";
            this.Column14.Name = "Column14";
            this.Column14.ReadOnly = true;
            this.Column14.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column14.Width = 115;
            // 
            // Column15
            // 
            this.Column15.HeaderText = "Блок. от нагр.";
            this.Column15.Name = "Column15";
            this.Column15.ReadOnly = true;
            this.Column15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column15.Width = 84;
            // 
            // Column16
            // 
            this.Column16.HeaderText = "Блок. от качания";
            this.Column16.Name = "Column16";
            this.Column16.ReadOnly = true;
            this.Column16.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column16.Width = 99;
            // 
            // Column23
            // 
            this.Column23.HeaderText = "Ненапр. при уск.";
            this.Column23.Name = "Column23";
            this.Column23.ReadOnly = true;
            this.Column23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column23.Width = 99;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Пуск от ОПФ";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column5.Width = 82;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Сброс ступени";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column6.Width = 87;
            // 
            // Column17
            // 
            this.Column17.HeaderText = "Осциллограф";
            this.Column17.Name = "Column17";
            this.Column17.ReadOnly = true;
            this.Column17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column17.Width = 82;
            // 
            // Column18
            // 
            this.Column18.HeaderText = "УРОВ";
            this.Column18.Name = "Column18";
            this.Column18.ReadOnly = true;
            this.Column18.Width = 43;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "АПВ";
            this.Column19.Name = "Column19";
            this.Column19.ReadOnly = true;
            this.Column19.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column19.Width = 35;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "АВР";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            this.Column11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column11.Width = 34;
            // 
            // saveFileResistCharacteristic
            // 
            this.saveFileResistCharacteristic.Filter = "(*.pco) | *.pco";
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Подсказка";
            // 
            // settingsOpfGroupBox
            // 
            this.settingsOpfGroupBox.Controls.Add(this._settingsResetCB);
            this.settingsOpfGroupBox.Controls.Add(this._settingsUminTB);
            this.settingsOpfGroupBox.Controls.Add(this._settingsImaxTB);
            this.settingsOpfGroupBox.Controls.Add(this.label40);
            this.settingsOpfGroupBox.Controls.Add(this.label39);
            this.settingsOpfGroupBox.Location = new System.Drawing.Point(456, 140);
            this.settingsOpfGroupBox.Name = "settingsOpfGroupBox";
            this.settingsOpfGroupBox.Size = new System.Drawing.Size(398, 44);
            this.settingsOpfGroupBox.TabIndex = 10;
            this.settingsOpfGroupBox.TabStop = false;
            this.settingsOpfGroupBox.Text = "Настройки ОПФ";
            this.settingsOpfGroupBox.Visible = false;
            // 
            // _settingsResetCB
            // 
            this._settingsResetCB.AutoSize = true;
            this._settingsResetCB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this._settingsResetCB.Location = new System.Drawing.Point(205, 18);
            this._settingsResetCB.Name = "_settingsResetCB";
            this._settingsResetCB.Size = new System.Drawing.Size(155, 17);
            this._settingsResetCB.TabIndex = 4;
            this._settingsResetCB.Text = "Сброс 1-го КЗ при МФКЗ";
            this._settingsResetCB.UseVisualStyleBackColor = true;
            // 
            // _settingsUminTB
            // 
            this._settingsUminTB.Location = new System.Drawing.Point(62, 16);
            this._settingsUminTB.Name = "_settingsUminTB";
            this._settingsUminTB.Size = new System.Drawing.Size(30, 20);
            this._settingsUminTB.TabIndex = 3;
            this._settingsUminTB.Text = "0";
            this._settingsUminTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _settingsImaxTB
            // 
            this._settingsImaxTB.Location = new System.Drawing.Point(150, 16);
            this._settingsImaxTB.Name = "_settingsImaxTB";
            this._settingsImaxTB.Size = new System.Drawing.Size(45, 20);
            this._settingsImaxTB.TabIndex = 2;
            this._settingsImaxTB.Text = "0";
            this._settingsImaxTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 20);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(44, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "Umin, B";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(105, 19);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Imax, Iн";
            // 
            // ResistanceDefTabCtr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ResistTabControl);
            this.Name = "ResistanceDefTabCtr";
            this.Size = new System.Drawing.Size(868, 541);
            this.ResistTabControl.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox47.ResumeLayout(false);
            this.groupBox47.PerformLayout();
            this.swPanel2.ResumeLayout(false);
            this.swPanel2.PerformLayout();
            this.swPanel1.ResumeLayout(false);
            this.swPanel1.PerformLayout();
            this._loadedGroup.ResumeLayout(false);
            this.loadFaz2.ResumeLayout(false);
            this.loadFaz2.PerformLayout();
            this.loadLin2.ResumeLayout(false);
            this.loadLin2.PerformLayout();
            this.loadFaz1.ResumeLayout(false);
            this.loadFaz1.PerformLayout();
            this.loadLin1.ResumeLayout(false);
            this.loadLin1.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.fn2groupBox2.ResumeLayout(false);
            this.fn2groupBox2.PerformLayout();
            this.fn4groupBox1.ResumeLayout(false);
            this.fn4groupBox1.PerformLayout();
            this.fn4groupBox2.ResumeLayout(false);
            this.fn4groupBox2.PerformLayout();
            this.fn5groupBox2.ResumeLayout(false);
            this.fn5groupBox2.PerformLayout();
            this.fn5groupBox1.ResumeLayout(false);
            this.fn5groupBox1.PerformLayout();
            this.fn3groupBox2.ResumeLayout(false);
            this.fn3groupBox2.PerformLayout();
            this.fn3groupBox1.ResumeLayout(false);
            this.fn3groupBox1.PerformLayout();
            this.fn1groupBox2.ResumeLayout(false);
            this.fn1groupBox2.PerformLayout();
            this.fn2groupBox1.ResumeLayout(false);
            this.fn2groupBox1.PerformLayout();
            this.fn1groupBox1.ResumeLayout(false);
            this.fn1groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.panel7.ResumeLayout(false);
            this._tablePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._infoGrid)).EndInit();
            this.settingsOpfGroupBox.ResumeLayout(false);
            this.settingsOpfGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ResistTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private ResistanceDefTabPage resistanceDefTabPage1;
        private ResistanceDefTabPage resistanceDefTabPage2;
        private ResistanceDefTabPage resistanceDefTabPage4;
        private ResistanceDefTabPage resistanceDefTabPage5;
        private ResistanceDefTabPage resistanceDefTabPage6;
        private System.Windows.Forms.TabPage _tablePage;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Panel panel7;
        private CharacteristicEnableControl characteristicEnableControl6;
        private CharacteristicEnableControl characteristicEnableControl5;
        private CharacteristicEnableControl characteristicEnableControl4;
        private CharacteristicEnableControl characteristicEnableControl3;
        private CharacteristicEnableControl characteristicEnableControl2;
        private CharacteristicEnableControl characteristicEnableControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel DrawPanel;
        private System.Windows.Forms.Button saveResistParamsBtn;
        private System.Windows.Forms.SaveFileDialog saveFileResistCharacteristic;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.MaskedTextBox _corner2ForRGr1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.MaskedTextBox _corner1ForRGr1;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.Panel swPanel2;
        private System.Windows.Forms.MaskedTextBox _swingXGr2;
        private System.Windows.Forms.MaskedTextBox _swingdzGr2;
        private System.Windows.Forms.MaskedTextBox _swingRGr2;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Panel swPanel1;
        private System.Windows.Forms.MaskedTextBox _swingXGr1;
        private System.Windows.Forms.MaskedTextBox _swingdzGr1;
        private System.Windows.Forms.MaskedTextBox _swingRGr1;
        private System.Windows.Forms.Label _l11;
        private System.Windows.Forms.Label _l10;
        private System.Windows.Forms.Label _l9;
        private System.Windows.Forms.CheckBox _swingYesNoTrGr1;
        private System.Windows.Forms.MaskedTextBox _swingTrGr1;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _swingIGr1;
        private System.Windows.Forms.MaskedTextBox _swingI0Gr1;
        private System.Windows.Forms.MaskedTextBox _swingTGr1;
        private System.Windows.Forms.MaskedTextBox _swingfGr1;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ComboBox _swingTypeGr1;
        private System.Windows.Forms.GroupBox _loadedGroup;
        private System.Windows.Forms.GroupBox loadFaz2;
        private System.Windows.Forms.MaskedTextBox _r2FazGr2;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.MaskedTextBox _r1FazGr2;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.MaskedTextBox maskedTextBox4;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.GroupBox loadLin2;
        private System.Windows.Forms.MaskedTextBox _r2LinGr2;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.MaskedTextBox _r1LinGr2;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.MaskedTextBox maskedTextBox3;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.GroupBox loadFaz1;
        private System.Windows.Forms.MaskedTextBox _r2FazGr1;
        private System.Windows.Forms.Label _l8;
        private System.Windows.Forms.MaskedTextBox _r1FazGr1;
        private System.Windows.Forms.Label _l7;
        private System.Windows.Forms.MaskedTextBox _cornerFazGr1;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.GroupBox loadLin1;
        private System.Windows.Forms.MaskedTextBox _r2LinGr1;
        private System.Windows.Forms.Label _l6;
        private System.Windows.Forms.MaskedTextBox _r1LinGr1;
        private System.Windows.Forms.Label _l5;
        private System.Windows.Forms.MaskedTextBox _cornerLinGr1;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox fn2groupBox2;
        private System.Windows.Forms.MaskedTextBox _xZ1Step2Gr2;
        private System.Windows.Forms.MaskedTextBox _xZ0Step2Gr2;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.MaskedTextBox _rZ1Step2Gr2;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.MaskedTextBox _rZ0Step2Gr2;
        private System.Windows.Forms.GroupBox fn1groupBox2;
        private System.Windows.Forms.MaskedTextBox _xZ1Step1Gr2;
        private System.Windows.Forms.MaskedTextBox _xZ0Step1Gr2;
        private System.Windows.Forms.MaskedTextBox _rZ1Step1Gr2;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.MaskedTextBox _rZ0Step1Gr2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.GroupBox fn2groupBox1;
        private System.Windows.Forms.MaskedTextBox _xZ1Step2Gr1;
        private System.Windows.Forms.MaskedTextBox _xZ0Step2Gr1;
        private System.Windows.Forms.Label _l4;
        private System.Windows.Forms.Label _l3;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.MaskedTextBox _rZ1Step2Gr1;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.MaskedTextBox _rZ0Step2Gr1;
        private System.Windows.Forms.GroupBox fn1groupBox1;
        private System.Windows.Forms.MaskedTextBox _xZ1Step1Gr1;
        private System.Windows.Forms.MaskedTextBox _xZ0Step1Gr1;
        private System.Windows.Forms.MaskedTextBox _rZ1Step1Gr1;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.MaskedTextBox _rZ0Step1Gr1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label _l2;
        private System.Windows.Forms.Label _l1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox fn4groupBox1;
        private System.Windows.Forms.MaskedTextBox _xZ1Step4Gr1;
        private System.Windows.Forms.MaskedTextBox _xZ0Step4Gr1;
        private System.Windows.Forms.MaskedTextBox _rZ1Step4Gr1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _rZ0Step4Gr1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox fn4groupBox2;
        private System.Windows.Forms.MaskedTextBox _xZ1Step4Gr2;
        private System.Windows.Forms.MaskedTextBox _xZ0Step4Gr2;
        private System.Windows.Forms.MaskedTextBox _rZ1Step4Gr2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox _rZ0Step4Gr2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox fn3groupBox2;
        private System.Windows.Forms.MaskedTextBox _xZ1Step3Gr2;
        private System.Windows.Forms.MaskedTextBox _xZ0Step3Gr2;
        private System.Windows.Forms.MaskedTextBox _rZ1Step3Gr2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _rZ0Step3Gr2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox fn3groupBox1;
        private System.Windows.Forms.MaskedTextBox _xZ1Step3Gr1;
        private System.Windows.Forms.MaskedTextBox _xZ0Step3Gr1;
        private System.Windows.Forms.MaskedTextBox _rZ1Step3Gr1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox _rZ0Step3Gr1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox fn5groupBox2;
        private System.Windows.Forms.MaskedTextBox _xZ1Step5Gr2;
        private System.Windows.Forms.MaskedTextBox _xZ0Step5Gr2;
        private System.Windows.Forms.MaskedTextBox _rZ1Step5Gr2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _rZ0Step5Gr2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox fn5groupBox1;
        private System.Windows.Forms.MaskedTextBox _xZ1Step5Gr1;
        private System.Windows.Forms.MaskedTextBox _xZ0Step5Gr1;
        private System.Windows.Forms.MaskedTextBox _rZ1Step5Gr1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.MaskedTextBox _rZ0Step5Gr1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.MaskedTextBox _swingfGr2;
        private ResistanceDefTabPage resistanceDefTabPage3;
        private System.Windows.Forms.DataGridView _infoGrid;
        private CheckBox _isMash;
        private ToolTip toolTip1;
        private Button calcKoefBtn;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private DataGridViewTextBoxColumn Column4;
        private DataGridViewTextBoxColumn _header1;
        private DataGridViewTextBoxColumn _header2;
        private DataGridViewTextBoxColumn Column7;
        private DataGridViewTextBoxColumn Column20;
        private DataGridViewTextBoxColumn Column21;
        private DataGridViewTextBoxColumn Column8;
        private DataGridViewTextBoxColumn Column9;
        private DataGridViewTextBoxColumn Column10;
        private DataGridViewCheckBoxColumn Column12;
        private DataGridViewTextBoxColumn Column13;
        private DataGridViewTextBoxColumn Column22;
        private DataGridViewTextBoxColumn Column14;
        private DataGridViewCheckBoxColumn Column15;
        private DataGridViewCheckBoxColumn Column16;
        private DataGridViewCheckBoxColumn Column23;
        private DataGridViewCheckBoxColumn Column5;
        private DataGridViewCheckBoxColumn Column6;
        private DataGridViewTextBoxColumn Column17;
        private DataGridViewCheckBoxColumn Column18;
        private DataGridViewTextBoxColumn Column19;
        private DataGridViewTextBoxColumn Column11;
        private GroupBox settingsOpfGroupBox;
        private CheckBox _settingsResetCB;
        private MaskedTextBox _settingsUminTB;
        private MaskedTextBox _settingsImaxTB;
        private Label label40;
        private Label label39;
    }
}
