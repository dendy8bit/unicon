﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MBServer;
using BEMN.Mr762.Version300.Configuration.Structures.MeasuringTransformer;

namespace BEMN.Mr762.Version300.Configuration
{
    public class CurrentOptionsLoaderV300
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 6;

        public MemoryEntity<MeasureTransStructV3>[] Connections { get; }
        public MemoryEntity<MeasureTransN4Struct>[] ConnectionsN4 { get; }
        private int _numberOfGroup;
        #endregion [Private fields]

        #region [Events]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private Mr762Device _device;

        #endregion [Events]


        #region [Ctor's]

        public CurrentOptionsLoaderV300(Mr762Device device, int slotLen)
        {
            this._device = device;
            this._numberOfGroup = 0;
            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.Info.Version);

            if (StringsConfig.CurrentVersion >= 3.07)
            {
                this.ConnectionsN4 = new MemoryEntity<MeasureTransN4Struct>[COUNT_GROUPS];
                for (int i = 0; i < this.ConnectionsN4.Length; i++)
                {
                    this.ConnectionsN4[i] =
                        new MemoryEntity<MeasureTransN4Struct>(string.Format("Параметры измерительного трансформатора гр{0} V3", i + 1),
                            this._device, device.GetStartAddrMeasTrans(i), slotLen);
                    this.ConnectionsN4[i].AllReadOk += this._connections_AllReadOk;
                    this.ConnectionsN4[i].AllReadFail += this._connections_AllReadFail;
                }
            }
            else
            {
                this.Connections = new MemoryEntity<MeasureTransStructV3>[COUNT_GROUPS];
                for (int i = 0; i < this.Connections.Length; i++)
                {
                    this.Connections[i] =
                        new MemoryEntity<MeasureTransStructV3>(string.Format("Параметры измерительного трансформатора гр{0} V3", i + 1),
                            this._device, device.GetStartAddrMeasTrans(i), slotLen);
                    this.Connections[i].AllReadOk += this._connections_AllReadOk;
                    this.Connections[i].AllReadFail += this._connections_AllReadFail;
                }
            }
            
        }
        

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            switch (this._numberOfGroup)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    if (StringsConfig.CurrentVersion >= 3.07)
                    {
                        this.ConnectionsN4[++this._numberOfGroup].LoadStruct();
                    }
                    else
                    {
                        this.Connections[++this._numberOfGroup].LoadStruct();
                    }
                    break;
                case 5:
                    if (this.LoadOk != null)
                    {
                        this.LoadOk.Invoke();
                    }
                    break;
            }
        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this.Connections?[0].LoadStruct();
            this.ConnectionsN4?[0].LoadStruct();
        }
        
        #endregion [Methods]
    }
}
