﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures.UROV
{
    public class UrovStruct : StructBase
    {
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _t1;
        [Layout(2)] private ushort _t2;
        [Layout(3)] private ushort _i;
        [Layout(4)] private ushort _pusk;
        [Layout(5)] private ushort _block;

        [BindingProperty(0)]
        [XmlElement(ElementName = "По_току")]
        public bool OnCurrent
        {
            get { return Common.GetBit(this._config, 0); }
            set { this._config = Common.SetBit(this._config, 0, value); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "По_БК")]
        public bool OnBk
        {
            get { return Common.GetBit(this._config, 1); }
            set { this._config = Common.SetBit(this._config, 1, value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "На_себя")]
        public bool Self
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
        /// <summary>
        /// tуров1
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "tуров1")]
        public int TimeUrov1
        {
            get { return ValuesConverterCommon.GetWaitTime(this._t1); }
            set { this._t1 = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// tуров1
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "tуров2")]
        public int TimeUrov2
        {
            get { return ValuesConverterCommon.GetWaitTime(this._t2); }
            set { this._t2 = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// ток УРОВ
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Ток_УРОВ")]
        public double Urov
        {
            get { return ValuesConverterCommon.GetIn(this._i); }
            set { this._i = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Пуск")]
        public string Pusk
        {
            get { return Validator.Get(this._pusk, StringsConfig.SwitchSignals); }
            set { this._pusk = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Блокировка")]
        public string Blocking
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
