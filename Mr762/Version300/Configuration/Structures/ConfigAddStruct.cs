﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures
{
    public class ConfigAddStruct : StructBase
    {
        [Layout(0, Count = 128, Ignore = true)] private ushort[] _res;
        [Layout(1)] private ushort _inp;
        [Layout(2)] private ushort _resInd;

        [BindingProperty(0)]
        public string InputAdd
        {
            get { return Validator.Get(this._inp, StringsConfig.InpOporSignals); }
            set { this._inp = Validator.Set(value, StringsConfig.InpOporSignals); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал системы
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_системы")]
        public bool ResetSystem
        {
            get { return Common.GetBit(this._resInd, 0); }
            set { this._resInd = Common.SetBit(this._resInd, 0, value); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал аварий
        /// </summary>
        [BindingProperty(2)]
        [XmlAttribute(AttributeName = "Сброс_по_входу_в_журнал_аварий")]
        public bool ResetAlarm
        {
            get { return Common.GetBit(this._resInd, 1); }
            set { this._resInd = Common.SetBit(this._resInd, 1, value); }
        }

        /// <summary>
        /// Сброс индикаторов по входу в журнал аварий
        /// </summary>
        [BindingProperty(3)]
        [XmlAttribute(AttributeName = "Работа_частоты")]
        public bool FixErrorF
        {
            get { return !Common.GetBit(this._resInd, 2); }
            set { this._resInd = Common.SetBit(this._resInd, 2, !value); }
        }
    }
}