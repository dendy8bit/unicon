﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr762.Version300.Configuration.Structures.Avr
{
    /// <summary>
    /// Конфигурация АВР
    /// </summary>
    public class AvrStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация
        [Layout(1)] private ushort _block; //вход блокировки АВР
        [Layout(2)] private ushort _clear; //вход сброс блокировки АВР
        [Layout(3)] private ushort _start; //вход сигнала запуск АВР
        [Layout(4)] private ushort _on; //вход АВР срабатывания
        [Layout(5)] private ushort _timeOn; //время АВР срабатывания
        [Layout(6)] private ushort _off; //вход АВР возврат
        [Layout(7)] private ushort _timeOff; //время АВР возврат
        [Layout(8)] private ushort _timeOtkl; //задержка отключения резерва
        [Layout(9)] private ushort _res1; 
        [Layout(10)] private ushort _res2;
        [Layout(11)] private ushort _res3;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// от сигнала
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "от_сигнала")]
        public string BySignalXml
        {
            get { return Validator.Get(this._config, StringsConfig.BeNo, 0); }
            set { this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 0); }
        }

        /// <summary>
        /// по отключению
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "по_отключению")]
        public string ByOffXml
        {
            get { return Validator.Get(this._config, StringsConfig.BeNo, 2); }
            set { this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 2); }
        }

        /// <summary>
        /// по самоотключению
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "по_самоотключению")]
        public string BySelfOffXml
        {
            get { return Validator.Get(this._config, StringsConfig.BeNo, 1); }
            set { this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 1); }
        }

        /// <summary>
        /// по защите
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "по_защите")]
        public string ByDefenseXml
        {
            get { return Validator.Get(this._config, StringsConfig.BeNo, 3); }
            set { this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 3); }
        }
        /// <summary>
        /// Пуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Пуск")]
        public string StartXml
        {
            get { return Validator.Get(this._start, StringsConfig.SwitchSignals); }
            set { this._start = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// вход блокировки АВР
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "вход_блокировки_АВР")]
        public string BlockingXml
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// вход сброс блокировки АВР
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "вход_сброс_блокировки_АВР")]
        public string ClearXml
        {
            get { return Validator.Get(this._clear, StringsConfig.SwitchSignals); }
            set { this._clear = Validator.Set(value, StringsConfig.SwitchSignals); }
        }


        /// <summary>
        /// вход АВР срабатывания
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "вход_АВР_срабатывания")]
        public string OnXml
        {
            get { return Validator.Get(this._on, StringsConfig.SwitchSignals); }
            set { this._on = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// вход АВР возврат
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "вход_АВР_возврат")]
        public string BackXml
        {
            get { return Validator.Get(this._off, StringsConfig.SwitchSignals); }
            set { this._off = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// время АВР срабатывания
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "время_АВР_срабатывания")]
        public int TimeOn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOn); }
            set { this._timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// время АВР возврат
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "время_АВР_возврат")]
        public int TimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOff); }
            set { this._timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// задержка отключения резерва
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "задержка_отключения_резерва")]
        public int TimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeOtkl); }
            set { this._timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Сброс
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Сброс")]
        public string ResetXml
        {
            get { return Validator.Get(this._config, StringsConfig.ForbiddenAllowed, 7); }
            set { this._config = Validator.Set(value, StringsConfig.ForbiddenAllowed, this._config, 7); }
        }

        #endregion [Properties]
    }
}
