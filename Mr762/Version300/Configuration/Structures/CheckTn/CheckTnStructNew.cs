﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures.CheckTn
{
    public class CheckTnStructNew: StructBase
    {
        [Layout(0)] private ushort _config;
        [Layout(1)] private ushort _u2;
        [Layout(2)] private ushort _i2;
        [Layout(3)] private ushort _u0;
        [Layout(4)] private ushort _i0;
        [Layout(5)] private ushort _uMin;
        [Layout(6)] private ushort _uMax;
        [Layout(7)] private ushort _iMin;
        [Layout(8)] private ushort _iMax;
        [Layout(9)] private ushort _iDelta;
        [Layout(10)] private ushort _polarityX; // вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(11)] private ushort _uDelta;
        [Layout(12)] private ushort _td;
        [Layout(13)] private ushort _ts;
        [Layout(14)] private ushort _reset;
        [Layout(15)] private ushort _polarityL; // вход внешней неисправности тн (трансформатора напряжения)

        /// <summary>
        /// Неиспр_ТН
        /// </summary>
        [BindingProperty(0)]
        public string LfaultXml
        {
            get { return Validator.Get(this._polarityL, StringsConfig.SwitchSignals); }
            set { this._polarityL = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Неиспр_ТНn
        /// </summary>
        [BindingProperty(1)]
        public string XfaultXml
        {
            get { return Validator.Get(this._polarityX, StringsConfig.SwitchSignals); }
            set { this._polarityX = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(2)]
        public bool U2I2
        {
            get { return Common.GetBit(this._config, 0); }
            set { this._config = Common.SetBit(this._config, 0, value); }
        }

        [BindingProperty(3)]
        public double U2
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u2); }
            set { this._u2 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(4)]
        public double I2
        {
            get { return ValuesConverterCommon.GetUstavka40(this._i2); }
            set { this._i2 = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(5)]
        public bool U0I0
        {
            get { return Common.GetBit(this._config, 1); }
            set { this._config = Common.SetBit(this._config, 1, value); }
        }

        [BindingProperty(6)]
        public double U0
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u0); }
            set { this._u0 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(7)]
        public double I0
        {
            get { return ValuesConverterCommon.GetUstavka40(this._i0); }
            set { this._i0 = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(8)]
        public double Umin
        {
            get { return ValuesConverterCommon.GetUstavka256(this._uMin); }
            set { this._uMin = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(9)]
        public double Umax
        {
            get { return ValuesConverterCommon.GetUstavka256(this._uMax); }
            set { this._uMax = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(10)]
        public double Imin
        {
            get { return ValuesConverterCommon.GetUstavka40(this._iMin); }
            set { this._iMin = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(11)]
        public double Imax
        {
            get { return ValuesConverterCommon.GetUstavka40(this._iMax); }
            set { this._iMax = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(12)]
        public double Idelta
        {
            get { return ValuesConverterCommon.GetUstavka100(this._iDelta); }
            set { this._iDelta = ValuesConverterCommon.SetUstavka100(value); }
        }

        [BindingProperty(13)]
        public double Udelta
        {
            get { return ValuesConverterCommon.GetUstavka100(this._uDelta); }
            set { this._uDelta = ValuesConverterCommon.SetUstavka100(value); }
        }

        [BindingProperty(14)]
        public int Td
        {
            get { return ValuesConverterCommon.GetWaitTime(this._td); }
            set { this._td = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(15)]
        public int Ts
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ts); }
            set { this._ts = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(16)]
        public string Reset
        {
            get { return Validator.Get(this._reset, StringsConfig.SwitchSignals); }
            set { this._reset = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(17)]
        public bool Phase3
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
    }
}
