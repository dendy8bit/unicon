﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures.Defenses.Dugovaya
{
    public class ArcProtectionStructure : StructBase
    {
        [Layout(0)] private ushort _config;   //уставка пуска по напряжению
        [Layout(1)] private ushort _isr;  //уставка срабатывания
        [Layout(2)] private ushort _cn1; // Угол in1
        [Layout(3)] private ushort _block; //резерв 

        [BindingProperty(0)]
        [XmlElement("Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModesShort, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModesShort, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement("Уставка")]
        public double Srab
        {
            get { return ValuesConverterCommon.GetIn(this._isr); }
            set { this._isr = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(2)]
        [XmlElement("Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(3)]
        [XmlElement("Осциллограф")]
        public bool Osc
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }
        
        [BindingProperty(4)]
        [XmlElement(ElementName = "In1")]
        public ushort In1
        {
            get { return this._cn1; }
            set { this._cn1 = value; }
        }
    }
}
