﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version300.Configuration.Structures.Defenses.Q
{
    public class AllDefenseQStruct : StructBase, IDgvRowsContainer<DefenseQStruct>
    {
        [Layout(0, Count = 2)]
        private DefenseQStruct[] _q; // Q

        [XmlArray(ElementName = "Все_защиты_Q")]
        public DefenseQStruct[] Rows
        {
            get { return this._q; }
            set { this._q = value; }
        }
    }
}
