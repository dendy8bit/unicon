﻿using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR771.Version102.Configuration.Istar
{
    public class AllDefenseStarStructV2 : StructBase, IDgvRowsContainer<DefenseStarStructV2>
    {
        public const int DEF_COUNT = 6;
        [Layout(0, Count = DEF_COUNT)] private DefenseStarStructV2[] _mtzmaini0; //мтз I*

        [XmlArray(ElementName = "Все")]
        public DefenseStarStructV2[] Rows
        {
            get { return _mtzmaini0; }
            set { _mtzmaini0 = value; }
        }
    }
}
