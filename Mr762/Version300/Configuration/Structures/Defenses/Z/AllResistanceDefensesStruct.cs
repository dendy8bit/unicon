﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version300.Configuration.Structures.Defenses.Z
{
    public class AllResistanceDefensesStruct : StructBase
    {
        public const int RESIST_DEF_COUNT = 6;
        [Layout(0, Count = RESIST_DEF_COUNT)] private ResistanceDefensesStruct[] _resistanceDefenses;
        [Layout(1)] private ResistanceDefensesStruct _reserve; //РЕЗЕРВ!!!

        [BindingProperty(0)]
        public ResistanceDefensesStruct this[int index]
        {
            get { return this._resistanceDefenses[index]; }
            set { this._resistanceDefenses[index] = value; }
        }
        [XmlElement(ElementName = "Все_дистанционные_защиты")]
        public ResistanceDefensesStruct[] ResistanceDefenses
        {
            get { return this._resistanceDefenses; }
            set { this._resistanceDefenses = value; }
        }
    }
}
