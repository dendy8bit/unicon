﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures.Defenses.I2I1
{
    /// <summary>
    /// I2/I1
    /// </summary>
    [XmlRoot(ElementName = "защиты_I2_I1")]
    public class DefenseI2I1Struct: StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1; //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _k; //коэфиц. зависимой хар-ки
        #endregion [Private fields]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return StringsConfig.SwitchSignals[this._block]; }
            set { this._block = (ushort)StringsConfig.SwitchSignals.IndexOf(value); }
        }

        /// <summary>
        /// Уставка I2/I1
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка_I2_I1")]
        public double UstavkaI2I1
        {
            get { return ValuesConverterCommon.GetUstavka100(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka100(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 0); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "АВР")]
        public string Avr
        {
            get { return Validator.Get(this._config1, StringsConfig.AutomaticMode, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.AutomaticMode, this._config1, 1); }
        }

        /// <summary>
        /// Вход ввода U1 нет U2 нет для синхронизма
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "дискрет_U1_есть_U2_нет")]
        public string Discret3
        {
            get { return Validator.Get(this._k, StringsConfig.SwitchSignals); }
            set { this._k = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
