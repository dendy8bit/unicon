﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version300.Configuration.Structures.Defenses.P
{
    public class AllDefenseReversPower : StructBase, IDgvRowsContainer<ReversePowerStruct>
    {
        public const int DEF_COUNT = 2;
        [Layout(0, Count = DEF_COUNT)]
        private ReversePowerStruct[] _reversePower; //Защ. P

        [XmlArray(ElementName = "Все_P")]
        public ReversePowerStruct[] Rows
        {
            get { return this._reversePower; }
            set { this._reversePower = value; }
        }
    }
}
