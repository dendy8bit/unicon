﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version300.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStruct : StructBase
    {
        public const int CFG_OSC_COUNT = 8;

        #region [Private fields]

        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private ChannelStruct _test; //вход запуска осциллографа
        [Layout(2)] private OscopeAllChannelsStructV300 _oscopeAllChannels;
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }

        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStructV300 OscopeAllChannels
        {
            get { return this._oscopeAllChannels; }
            set { this._oscopeAllChannels = value; }
        }

        /// <summary>
        /// вход запуска осциллографа
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_запуска_осциллографа")]
        public ChannelStruct StartOscChannel
        {
            get { return this._test; }
            set { this._test = value; }
        }
        
        #endregion [Properties]
    }
}
