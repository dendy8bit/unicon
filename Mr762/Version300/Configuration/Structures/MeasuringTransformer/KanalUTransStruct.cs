﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора U
    /// </summary>
    public class KanalUTransStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx; //конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1; //резерв
        [Layout(3)] private ushort _res1; //резерв
        [Layout(4)] private ushort _res2; //резерв
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _res3; // резерв

        #endregion [Private fields]

        #region [U (ТН)]

        /// <summary>
        /// тип_Uo
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_Uo")]
        public string UtypeXml
        {
            get { return Validator.Get(this._binding, StringsConfig.UoType); }
            set { this._binding = Validator.Set(value, StringsConfig.UoType); }
        }

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(1)]
        [XmlIgnore]
        public double Kthl
        {
            get { return ValuesConverterCommon.GetKth(this._ittl); }
            set { this._ittl = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthlValue
        {
            get
            {
                double ktn = Common.SetBit(this._ittl, 15, false);
                return Common.GetBit(this._ittl, 15)
                    ? ktn*1000/256
                    : ktn/256;
            }
        }
        /// <summary>
        /// KTHL Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHL")]
        public double KthlXml
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._ittl);
                return Common.GetBit(this._ittl, 15)
                    ? ktn * 1000 
                    : ktn;
            }
            set { }
        }
        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(2)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._ittl, StringsConfig.KthKoefs, 15); }
            set { this._ittl = Validator.Set(value, StringsConfig.KthKoefs, this._ittl, 15); }
        }
        #endregion [U (ТН)] 
    }
}
