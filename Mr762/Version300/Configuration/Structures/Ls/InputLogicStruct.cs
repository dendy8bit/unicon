﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr762.Version300.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурация входного логического сигнала
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        public const int DISCRETS_COUNT = 40;

        #region [Private fields]

        [Layout(0)] private ushort _a1;
        [Layout(1)] private ushort _a2;
        [Layout(2)] private ushort _a3;
        [Layout(3)] private ushort _a4;
        [Layout(4)] private ushort _a5;
        [Layout(5)] private ushort _a6;

        #endregion [Private fields]

        [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                {
                    this._a1,
                    this._a2,
                    this._a3,
                    this._a4,
                    this._a5
                };
            }
            set
            {
                this._a1 = value[0];
                this._a2 = value[1];
                this._a3 = value[2];
                this._a4 = value[3];
                this._a5 = value[4];
            }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                var sourse = this.Mass[ls/8];
                var pos = (ls*2)%16;
                return Validator.Get(sourse, StringsConfig.LsState, pos, pos + 1);
            }

            set
            {
                var sourse = this.Mass[ls/8];
                var pos = (ls*2)%16;
                sourse = Validator.Set(value, StringsConfig.LsState, sourse, pos, pos + 1);
                var mass = this.Mass;
                mass[ls/8] = sourse;
                this.Mass = mass;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
