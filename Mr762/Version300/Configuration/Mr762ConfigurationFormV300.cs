﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr762.Properties;
using BEMN.Mr762.Version300.Configuration.Structures;
using BEMN.Mr762.Version300.Configuration.Structures.Apv;
using BEMN.Mr762.Version300.Configuration.Structures.Avr;
using BEMN.Mr762.Version300.Configuration.Structures.CheckTn;
using BEMN.Mr762.Version300.Configuration.Structures.ConfigSystem;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.Dugovaya;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.External;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.F;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.I;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.I2I1;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.Istar;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.P;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.Q;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.U;
using BEMN.Mr762.Version300.Configuration.Structures.Defenses.Z;
using BEMN.Mr762.Version300.Configuration.Structures.Engine;
using BEMN.Mr762.Version300.Configuration.Structures.Goose;
using BEMN.Mr762.Version300.Configuration.Structures.InputSignals;
using BEMN.Mr762.Version300.Configuration.Structures.Ls;
using BEMN.Mr762.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr762.Version300.Configuration.Structures.Omp;
using BEMN.Mr762.Version300.Configuration.Structures.Oscope;
using BEMN.Mr762.Version300.Configuration.Structures.RelayInd;
using BEMN.Mr762.Version300.Configuration.Structures.Sihronizm;
using BEMN.Mr762.Version300.Configuration.Structures.Switch;
using BEMN.Mr762.Version300.Configuration.Structures.UROV;
using BEMN.Mr762.Version300.Configuration.Structures.Vls;

namespace BEMN.Mr762.Version300.Configuration
{
    public partial class Mr762ConfigurationFormV300 : Form, IFormView
    {
        #region [Constants]

        private const string DEVICE_NAME = "МР762";
        private const string READING_CONFIG = "Чтение конфигурации";
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR762_SET_POINTS";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string MR762_BASE_CONFIG_PATH_WHITH_AC = "\\MR762\\MR762_BaseConfig_v{0}_{1}.xml";
        private const string MR762_BASE_CONFIG_PATH = "\\MR762\\MR762_BaseConfig_v{0}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;
        #endregion [Constants]

        #region [Private fields]

        private readonly MemoryEntity<ConfigurationStructV300> _configuration;
        private readonly MemoryEntity<ConfigurationStruct303> _configuration303;
        private readonly MemoryEntity<ConfigurationStruct304> _configuration304;
        private readonly MemoryEntity<ConfigurationStruct307> _configuration307;
        /// <summary>
        /// Текущие уставки
        /// </summary>
        private ConfigurationStructV300 _currentSetpointsStruct;
        private ConfigurationStruct303 _currentSetpointsStruct303;
        private ConfigurationStruct304 _currentSetpointsStruct304;
        private ConfigurationStruct307 _currentSetpointsStruct307;

        private MaskedTextBox[] _primary;
        private MaskedTextBox[] _second;
        private readonly Mr762Device _device;
        private bool _return;
        private double _version;

        private StructUnion<MeasureTransStructV3> _measureTransUnion;
        private StructUnion<MeasureTransN4Struct> _measureTransN4Union;

        private StructUnion<GroupSetpointStruct> _groupSetpointValidator;
        private StructUnion<GroupSetpoint304> _groupSetpointValidator304;
        private StructUnion<GroupSetpoint307> _groupSetpointValidator307;

        private SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct> _allGroupSetpointsValidator;
        private SetpointsValidator<AllGroupSetpointStruct304, GroupSetpoint304> _allGroupSetpointsValidator304;
        private SetpointsValidator<AllGroupSetpointStruct307, GroupSetpoint307> _allGroupSetpointsValidator307;

        private StructUnion<ConfigurationStructV300> _configurationValidator;
        private StructUnion<ConfigurationStruct303> _configurationValidator303;
        private StructUnion<ConfigurationStruct304> _configurationValidator304;
        private StructUnion<ConfigurationStruct307> _configurationValidator307;

        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();

        private ComboboxSelector _groupSetpointSelector;
        #endregion [Private fields]

        #region [Ctor's]

        public Mr762ConfigurationFormV300()
        {
            this.InitializeComponent();
        }

        public Mr762ConfigurationFormV300(Mr762Device device)
        {
            this.InitializeComponent();
            this._device = device;

            StringsConfig.DeviceType = this._device.Info.Plant;
            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);

            if (StringsConfig.CurrentVersion >= 3.07 && StringsConfig.DeviceType == Mr762Device.T5N4D42R19)
            {
                this.label6.Visible = true;
                this._KTHX_BoxGr1.Visible = true;
                this.label11.Visible = true;
                this._KTHXkoef_comboGr1.Visible = true;

                this.label180.Visible = true;
                this._errorX_comboGr1.Visible = true;

                //corner in1
                this.label182.Visible = true;
                this.label181.Visible = true;
                this._in1CornerGr1.Visible = true;
                this._cornerGroupBox.Size = new Size(333, 155);
            }

            this.resistanceDefTabCtr1.Initialization();
            this.resistanceDefTabCtr1.IsPrimary = false;
            this._version = Common.VersionConverter(this._device.DeviceVersion);
            if (this._version >= 3.07)
            {
                this._configuration307 = device.Mr762DeviceV2.ConfigurationV307;
                this._configuration307.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration307.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                    this.ConfigurationWriteOk);
                this._configuration307.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration307.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration307.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration307.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration307.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration307.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._progressBar.Maximum = this._configuration307.Slots.Count;
                this._currentSetpointsStruct307 = new ConfigurationStruct307();
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._configuration304 = device.Mr762DeviceV2.ConfigurationV304;
                this._configuration304.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration304.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                    this.ConfigurationWriteOk);
                this._configuration304.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration304.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration304.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration304.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration304.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration304.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._progressBar.Maximum = this._configuration304.Slots.Count;
                this._currentSetpointsStruct304 = new ConfigurationStruct304();
            }
            else if (this._version == 3.03)
            {
                this._configuration303 = device.Mr762DeviceV2.ConfigurationV303;
                this._configuration303.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration303.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration303.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration303.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration303.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration303.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration303.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration303.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._progressBar.Maximum = this._configuration303.Slots.Count;
                this._currentSetpointsStruct303 = new ConfigurationStruct303();
            }
            else
            {
                this._configuration = device.Mr762DeviceV2.ConfigurationV3;
                this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
                this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
                this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
                this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationReadFail();
                });
                this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
                {
                    this._configuration.RemoveStructQueries();
                    this.ConfigurationWriteFail();
                });
                this._progressBar.Maximum = this._configuration.Slots.Count;
                this._currentSetpointsStruct = new ConfigurationStructV300();
            }
            this.Init();
        }
        
        private void Init()
        {
            this._copySetpoinsGroupComboBox.DataSource = StringsConfig.CopyGroupsNames;
            this._second = new[] {this._xud1s, this._xud2s, this._xud3s, this._xud4s, this._xud5s};
            this._primary = new[] {this._xud1p, this._xud2p, this._xud3p, this._xud4p, this._xud5p};
            
            bool visibility = Common.VersionConverter(this._device.DeviceVersion) >= 3.01;
            if (!visibility)
            {
                this.dugGroupBox.Visible = visibility;
                this._defI2I1dug.Text = "Защ. I2I1";
            }
            if (Common.VersionConverter(this._device.DeviceVersion) < 3.02)
            {
                this._setpointsTab.TabPages.Remove(this._defPGr1);
                this.passportDataGroup.Visible = false;
            }

            this._groupSetpointSelector = new ComboboxSelector(this._setpointsComboBox, StringsConfig.GroupsNames);
            this._groupSetpointSelector.OnRefreshInfoTable += OnRefreshInfoTable;

            #region Уставки

            #region [Защиты]

            //углы
            NewStructValidator<CornerStruct>  cornerValidator = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._iCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i0CornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._inCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i2CornerGr1, RulesContainer.UshortTo360)
                );

            // I
            NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>  iValidator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                (
                new[] {this._difensesI1DataGrid,this._difensesI2DataGrid, this._difensesI3DataGrid},
                new[] {4, 2, 1},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesI, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.TypeI, ColumnsType.COMBO, false, true, false),
                new ColumnInfoText(RulesContainer.Ustavka256, true, true, false), //4
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCombo(StringsConfig.BlockTn),
                new ColumnInfoCombo(StringsConfig.Direction, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Undir, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Logic),
                new ColumnInfoCombo(StringsConfig.Characteristic, ColumnsType.COMBO, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000, true, true, false),
                new ColumnInfoCombo(StringsConfig.RelaySignals, ColumnsType.COMBO, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule, true, true, false),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka100, true, true, false),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCombo(StringsConfig.AutomaticMode)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI1DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI2DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesI3DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23)
                        )
                }
            };
            //I*
            NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>  iStarValidator = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
                (
                this._difensesI0DataGridGr1,
                AllDefenseStarStruct.DEF_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesIStar, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir),
                new ColumnInfoCombo(StringsConfig.I),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCombo(StringsConfig.AutomaticMode)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI0DataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        )
                }
            };
            //I2I1
           NewStructValidator<DefenseI2I1Struct> i2I1Validator = new NewStructValidator<DefenseI2I1Struct>
               (
               this._toolTip,
               new ControlInfoCombo(this.I2I1ModeComboGr1, StringsConfig.DefenseModes),
               new ControlInfoCombo(this.I2I1BlockingComboGr1, StringsConfig.SwitchSignals),
               new ControlInfoText(this.I2I1TBGr1, RulesContainer.Ustavka100),
               new ControlInfoText(this.I2I1tcpTBGr1, RulesContainer.TimeRule),
               new ControlInfoCombo(this.I2I1OscComboGr1, StringsConfig.OscModes),
               new ControlInfoCheck(this.I2I1UrovCheckGr1),
               new ControlInfoCombo(this.I2I1Apv, StringsConfig.AutomaticMode),
               new ControlInfoCombo(this.I2I1Avr, StringsConfig.AutomaticMode),
               new ControlInfoCombo(this._discretIn3Cmb, StringsConfig.SwitchSignals)
               );
            //Дуговая защита (с версии 3.01)
            NewStructValidator<DugovDefense> dugDefValdator = new NewStructValidator<DugovDefense>(
                this._toolTip,
                new ControlInfoCombo(this.dugMode, StringsConfig.DefenseModesShort),
                new ControlInfoText(this.dugSrab, RulesContainer.Ustavka40),
                new ControlInfoCombo(this.dugBlock, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.dugOsc));

            NewStructValidator<ArcProtectionStructure> arcDefValdator = new NewStructValidator<ArcProtectionStructure>(
                this._toolTip,
                new ControlInfoCombo(this.dugMode, StringsConfig.DefenseModesShort),
                new ControlInfoText(this.dugSrab, RulesContainer.Ustavka40),
                new ControlInfoCombo(this.dugBlock, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.dugOsc),
                new ControlInfoText(this._in1CornerGr1, RulesContainer.UshortTo360)
                );

            //U
            NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> uValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
                (
                new[] {this._difensesUBDataGridGr1, this._difensesUMDataGridGr1},
                new[] {4, 4},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.UStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true), //2
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoCheck(), //7
                new ColumnInfoCheck(false, true), //8
                new ColumnInfoCombo(StringsConfig.BlockTn, ColumnsType.COMBO, false, true),
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesUBDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesUMDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
                        )
                }
            };

            Func<IValidatingRule> fBDefFunc = () =>
            {
                if (!this._difensesFBDataGridGr1.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFBDataGridGr1.Tag as DataGridViewCell;
                    if (cell == null ||
                        this._difensesFBDataGridGr1[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };

            //F>
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fBValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFBDataGridGr1, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FBStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fBDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60),  //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals),  //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCombo(StringsConfig.AutomaticMode), //12
                new ColumnInfoCombo(StringsConfig.AutomaticMode), //13
                new ColumnInfoCheck(), //14
                new ColumnInfoCheck()  //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFBDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            Func<IValidatingRule> fMDefFunc = () =>
            {
                if (!this._difensesFMDataGridGr1.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFMDataGridGr1.Tag as DataGridViewCell;
                    if (cell == null || this._difensesFMDataGridGr1[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };
            //F<
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fMValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFMDataGridGr1, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FMStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fMDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCombo(StringsConfig.AutomaticMode), //12
                new ColumnInfoCombo(StringsConfig.AutomaticMode), //13
                new ColumnInfoCheck(), //14
                new ColumnInfoCheck() //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFMDataGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> qValidator = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
                this._engineDefensesGridGr1,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.QStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka256), //2
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //3
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCombo(StringsConfig.AutomaticMode)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._engineDefensesGridGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7)
                        )
                }
            };

            NewStructValidator<DefenseTermBlockStruct> termBlockValidator = new NewStructValidator<DefenseTermBlockStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._engineQmodeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._engineQconstraintGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._engineQtimeGr1, RulesContainer.UshortRule),
                new ControlInfoText(this._numHot, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._numCold, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._waitNumBlock, new CustomUshortRule(ushort.MinValue, ushort.MaxValue)),
                new ControlInfoText(this._cosfP, new CustomDoubleRule(0, 0.99)),
                new ControlInfoText(this._kpdP, RulesContainer.UshortTo100)
                );

            NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> externalValidator = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                this._externalDefensesGr1,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),//5
                new ColumnInfoCheck(), //6
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //7
                new ColumnInfoCombo(StringsConfig.OscModes),//8
                new ColumnInfoCheck(),//9
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefensesGr1,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                }
            };

            NewDgwValidatior<AllDefenseReversPower, ReversePowerStruct> reversPowerValidator = new NewDgwValidatior
                <AllDefenseReversPower, ReversePowerStruct>
                (
                this._reversePowerGrid,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.StageDefType, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                new ColumnInfoText(RulesContainer.UshortFazaRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.BlockTn),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCombo(StringsConfig.OffOn),
                new ColumnInfoCombo(StringsConfig.OffOn),
                new ColumnInfoCombo(StringsConfig.AutomaticMode),
                new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._reversePowerGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                    2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
                                )
                        }
            };

            StructUnion<DefensesSetpointsStructNew> defensesUnionNew = new StructUnion<DefensesSetpointsStructNew>
            (
                cornerValidator,
                iValidator,
                iStarValidator,
                i2I1Validator,
                arcDefValdator,
                uValidator,
                fBValidator,
                fMValidator,
                qValidator,
                termBlockValidator,
                externalValidator,
                this.resistanceDefTabCtr1.ResistanceUnionNew,
                reversPowerValidator
            );

            StructUnion<DefensesSetpointsStruct> defensesUnion = new StructUnion<DefensesSetpointsStruct>
                (
                cornerValidator,
                iValidator,
                iStarValidator,
                i2I1Validator,
                dugDefValdator,
                uValidator,
                fBValidator,
                fMValidator,
                qValidator,
                termBlockValidator,
                externalValidator,
                this.resistanceDefTabCtr1.ResistanceUnion,
                reversPowerValidator
                );

            #endregion [защиты]

            // АВР
            NewStructValidator<AvrStruct> avrValidator = new NewStructValidator<AvrStruct>
                (
                 this._toolTip,
                 new ControlInfoCombo(this._avrBySignal, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrBySelfOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByDiff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrSIGNOn, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlocking, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlockClear, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrResolve, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBack, StringsConfig.SwitchSignals),
                 new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                 new ControlInfoCombo(this._avrClear, StringsConfig.ForbiddenAllowed)
                );

           
            // Контроль цепей ТН
            NewStructValidator<CheckTnStructNew> chechTnValidatorNew = new NewStructValidator<CheckTnStructNew>
            (this._toolTip,
                new ControlInfoCombo(this._errorL_comboGr1, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._errorX_comboGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this._i2u2CheckGr1),
                new ControlInfoText(this._u2ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i2ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._i0u0CheckGr1),
                new ControlInfoText(this._u0ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i0ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._uMinContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._uMaxContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._iMinContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iMaxContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._uDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._tdContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._tsContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._resetTnGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.checkBox1)
            );

            
            NewStructValidator<CheckTnStruct> chechTnValidator = new NewStructValidator<CheckTnStruct>
            (this._toolTip,
                new ControlInfoCombo(this._errorL_comboGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this._i2u2CheckGr1),
                new ControlInfoText(this._u2ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i2ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._i0u0CheckGr1),
                new ControlInfoText(this._u0ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i0ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._uMinContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._uMaxContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._iMinContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iMaxContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._uDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._tdContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._tsContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._resetTnGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.checkBox1)
            );

            //АПВ
            NewStructValidator<ApvStruct> apvValidator = new NewStructValidator<ApvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._apvModeGr1, StringsConfig.ApvModes),
                new ControlInfoCheck(this._blokFromUrov),
                new ControlInfoCombo(this._disableApv, StringsConfig.SwitchSignals),
                new ControlInfoText(this._timeDisable, RulesContainer.TimeRule),
                new ControlInfoCombo(this._typeDisable, StringsConfig.DisableType),
                new ControlInfoCombo(this._apvBlockGr1, StringsConfig.SwitchSignals),
                new ControlInfoText(this._apvTblockGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvTreadyGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat1Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat2Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat3Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat4Gr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._apvSwitchOffGr1, StringsConfig.BeNo)
                );

            //Тепловая модель
            NewStructValidator<TermConfigStruct> termConfValidator = new NewStructValidator<TermConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineIdv, RulesContainer.Ustavka40),
                new ControlInfoText(this._iStartBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._tStartBox, new CustomIntRule(0, 3276700)),
                new ControlInfoText(this._qBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQresetGr1, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._engineNreset, StringsConfig.SwitchSignals),
                new ControlInfoText(this._tCount, RulesContainer.UshortTo65534),
                new ControlInfoText(this._ppd, new CustomDoubleRule(0, 128)),
                new ControlInfoCombo(this._ppdCombo, StringsConfig.Pkoef)
                );

            // Измерительный трансформатор
            NewStructValidator<KanalITransStruct> iTransValidator = new NewStructValidator<KanalITransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._TT_typeComboGr1, StringsConfig.TtType),
                new ControlInfoText(this._Im_BoxGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._ITTL, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX1, RulesContainer.UshortRule),
                new ControlInfoCombo(this._inpIn, StringsConfig.InpIn)
                );

            NewStructValidator<KanalUN4TransStruct> uTransValidatorNew = new NewStructValidator<KanalUN4TransStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._Uo_typeComboGr1, StringsConfig.UoType),
                new ControlInfoText(this._KTHL_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHLkoef_comboGr1, StringsConfig.KthKoefs),
                new ControlInfoText(this._KTHX_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHXkoef_comboGr1, StringsConfig.KthKoefs)
            );
            this._measureTransN4Union = new StructUnion<MeasureTransN4Struct>(iTransValidator, uTransValidatorNew);
            
            NewStructValidator<KanalUTransStruct> uTransValidator = new NewStructValidator<KanalUTransStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._Uo_typeComboGr1, StringsConfig.UoType),
                new ControlInfoText(this._KTHL_BoxGr1, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHLkoef_comboGr1, StringsConfig.KthKoefs)
            );
            this._measureTransUnion = new StructUnion<MeasureTransStructV3>(iTransValidator, uTransValidator);


            // ЛС
            dataGridsViewLsAND.Add(this._lsAndDgv1Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv2Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv3Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv4Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv5Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv6Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv7Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv8Gr1);

            dataGridsViewLsOR.Add(this._lsOrDgv1Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv2Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv3Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv4Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv5Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv6Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv7Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv8Gr1);

            DataGridView[] lsBoxes =
            {
                this._lsAndDgv1Gr1, this._lsAndDgv2Gr1, this._lsAndDgv3Gr1, this._lsAndDgv4Gr1, this._lsAndDgv5Gr1, this._lsAndDgv6Gr1, this._lsAndDgv7Gr1,
                this._lsAndDgv8Gr1,
                this._lsOrDgv1Gr1, this._lsOrDgv2Gr1, this._lsOrDgv3Gr1, this._lsOrDgv4Gr1, this._lsOrDgv5Gr1, this._lsOrDgv6Gr1, this._lsOrDgv7Gr1,
                this._lsOrDgv8Gr1
            };
            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }
            NewDgwValidatior<InputLogicStruct>[] inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[AllInputLogicStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    lsBoxes[i],
                    InputLogicStruct.DISCRETS_COUNT,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            StructUnion<AllInputLogicStruct> inputLogicUnion = new StructUnion<AllInputLogicStruct>(inputLogicValidator);

            // ВЛС
            allVlsCheckedListBoxs.Add(this.VLSclb1Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb2Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb3Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb4Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb5Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb6Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb7Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb8Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb9Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb10Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb11Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb12Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb13Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb14Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb15Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb16Gr1);

            CheckedListBox[] vlsBoxes = new[]
                {
                    this.VLSclb1Gr1, this.VLSclb2Gr1, this.VLSclb3Gr1, this.VLSclb4Gr1, this.VLSclb5Gr1, this.VLSclb6Gr1, this.VLSclb7Gr1, this.VLSclb8Gr1,
                    this.VLSclb9Gr1, this.VLSclb10Gr1, this.VLSclb11Gr1, this.VLSclb12Gr1, this.VLSclb13Gr1, this.VLSclb14Gr1, this.VLSclb15Gr1,
                    this.VLSclb16Gr1
                };
            NewCheckedListBoxValidator<OutputLogicStruct>[] vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(vlsBoxes[i], StringsConfig.VlsSignals);
            }
            StructUnion<AllOutputLogicSignalStruct> vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(vlsValidator);
            
            // ОМП
            NewStructValidator<ConfigurationOpmStruct> ompValidator = new NewStructValidator<ConfigurationOpmStruct>(
                this._toolTip,
                new ControlInfoCombo(this._OMPmode_comboGr1, StringsConfig.OmpModes),
                new ControlInfoText(this._xud1s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud2s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud3s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud4s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._xud5s, new DoubleToComaRule(0, 2, 4)),
                new ControlInfoText(this._l1, RulesContainer.Ustavka256),
                new ControlInfoText(this._l2, RulesContainer.Ustavka256),
                new ControlInfoText(this._l3, RulesContainer.Ustavka256),
                new ControlInfoText(this._l4, RulesContainer.Ustavka256)
                );

            // Синхронизм
            NewStructValidator<SinhronizmAddStruct> manualSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrManualUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrManualdFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrManualdFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrManualdFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrManual),
                new ControlInfoCheck(this._catchSinchrManual),
                new ControlInfoCheck(this._blockTNmanual)
                );

            NewStructValidator<SinhronizmAddStruct> autoSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrAutoUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrAutodFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrAutodFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrAutodFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrAuto),
                new ControlInfoCheck(this._catchSinchrAuto),
                new ControlInfoCheck(this._blockTNauto)
                );

            NewStructValidator<SinhronizmStruct> sinhronizmValidatorNew = new NewStructValidator<SinhronizmStruct>
            (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOtsGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwaitGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTonGr1, new CustomUshortRule(0, 2000)),
                new ControlInfoCombo(this._sinhrU1Gr1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2Gr1, StringsConfig.Usinhr),
                new ControlInfoValidator(manualSinhronizmValidator),
                new ControlInfoValidator(autoSinhronizmValidator),
                new ControlInfoCombo(this._blockSinhCmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn1Cmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn2Cmb, StringsConfig.SwitchSignals),
                new ControlInfoText(this._sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrF, new CustomUshortRule(0, 360))
            );

            NewStructValidator<SinhronizmStruct> sinhronizmValidator = new NewStructValidator<SinhronizmStruct>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOtsGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwaitGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTonGr1, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1Gr1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2Gr1, StringsConfig.Usinhr),
                new ControlInfoValidator(manualSinhronizmValidator),
                new ControlInfoValidator(autoSinhronizmValidator),
                new ControlInfoCombo(this._blockSinhCmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn1Cmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn2Cmb, StringsConfig.SwitchSignals),
                new ControlInfoText(this._sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrF, new CustomUshortRule(0, 360))
                );

            if (StringsConfig.CurrentVersion >= 3.07)
            {
                this._groupSetpointValidator307 = new StructUnion<GroupSetpoint307>
                (
                    defensesUnionNew,
                    avrValidator,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidatorNew,
                    chechTnValidatorNew,
                    this.resistanceDefTabCtr1.SwingValidator,
                    apvValidator,
                    termConfValidator,
                    this._measureTransN4Union,
                    inputLogicUnion,
                    vlsUnion,
                    sinhronizmValidatorNew,
                    ompValidator
                );

                this._allGroupSetpointsValidator307 = new SetpointsValidator<AllGroupSetpointStruct307, GroupSetpoint307>
                    (this._groupSetpointSelector, this._groupSetpointValidator307);
                
            }
            else if (StringsConfig.CurrentVersion >= 3.04 && StringsConfig.CurrentVersion < 3.07)
            {
                this._groupSetpointValidator304 = new StructUnion<GroupSetpoint304>
                    (
                    defensesUnion,
                    avrValidator,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    chechTnValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    apvValidator,
                    termConfValidator,
                    this._measureTransUnion,
                    inputLogicUnion,
                    vlsUnion,
                    sinhronizmValidator,
                    ompValidator
                    );

                this._allGroupSetpointsValidator304 = new SetpointsValidator<AllGroupSetpointStruct304, GroupSetpoint304>
                    (this._groupSetpointSelector, this._groupSetpointValidator304);
            }
            else
            {
                this._groupSetpointValidator = new StructUnion<GroupSetpointStruct>
                    (
                    defensesUnion,
                    avrValidator,
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    chechTnValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    apvValidator,
                    termConfValidator,
                    this._measureTransUnion,
                    inputLogicUnion,
                    vlsUnion,
                    sinhronizmValidator,
                    ompValidator
                    );
                this._allGroupSetpointsValidator = new SetpointsValidator<AllGroupSetpointStruct, GroupSetpointStruct>
                (this._groupSetpointSelector, this._groupSetpointValidator);
            }

            #endregion Уставки

            // Выключатель
            NewStructValidator<SwitchStruct> switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._controlSolenoidCombo, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._comandOtkl,StringsConfig.ImpDlit),
                   new ControlInfoCombo(this._blockSDTU, StringsConfig.SwitchSignals)
                   );  
            // Входные сигналы
            NewStructValidator<InputSignalStruct> inputSignalsValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst3ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst4ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst5ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst6ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );

            #region [Осц]
            NewStructValidator<OscopeConfigStruct> oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(this._oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscCount)
                     );

            Func<string, List<string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return new List<string>();
                int index = StringsConfig.OscBases.IndexOf(str);
                return index != -1 ? StringsConfig.OscChannelSignals[index] : new List<string>();
            };

            DgvValidatorWithDepend<OscopeAllChannelsStructV300, ChannelWithBase> channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStructV300, ChannelWithBase>
                (
                this._oscChannelsGrid,
                OscopeAllChannelsStructV300.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.OscChannelNames, ColumnsType.NAME),
                new ColumnInfoComboControl(StringsConfig.OscBases, 2),
                new ColumnInfoComboDependent(func, 1)
                );
            this._oscChannelsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            NewStructValidator<ChannelStruct> startOscChannelValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            StructUnion<OscopeStruct> oscopeUnion = new StructUnion<OscopeStruct>(oscopeConfigValidator, channelsValidator, startOscChannelValidator);

            #endregion [Осц]

            #region [Реле и Индикаторы]

            NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                this._outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.Mode)
                );

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoCheck(this._fault6CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            StructUnion<AutomaticsParametersStruct> automaticsParametersUnion = 
                new StructUnion<AutomaticsParametersStruct>(releyValidator, indicatorValidator, faultValidator);

            #endregion [Реле и Индикаторы]

            NewStructValidator<ConfigAddStruct> configAddValidator = new NewStructValidator<ConfigAddStruct>
                (this._toolTip, 
                new ControlInfoCombo(this._inpAddCombo, StringsConfig.InpOporSignals),
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox),
                new ControlInfoCheck(this._fixErrorFCheckBox)
                );

            if (this._version >= 3.07)
            {
                StructUnion<ConfigResistDiagramStructNew> resistConfigUnionNew = new StructUnion<ConfigResistDiagramStructNew>
                (
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidatorNew,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this.resistanceDefTabCtr1.ResistanceUnionNew,
                    this._measureTransN4Union
                );
                this.resistanceDefTabCtr1.ResistConfigUnionNew = resistConfigUnionNew;
            }
            else
            {
                StructUnion<ConfigResistDiagramStruct> resistConfigUnion = new StructUnion<ConfigResistDiagramStruct>
                (
                    this.resistanceDefTabCtr1.ResistValidator,
                    this.resistanceDefTabCtr1.LoadValidator,
                    this.resistanceDefTabCtr1.SwingValidator,
                    this.resistanceDefTabCtr1.ResistanceUnion,
                    this._measureTransUnion
                );
                this.resistanceDefTabCtr1.ResistConfigUnion = resistConfigUnion;
            }

            NewStructValidator<UrovStruct> urovValidator = new NewStructValidator<UrovStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._urovIcheck),
                new ControlInfoCheck(this._urovBkCheck),
                new ControlInfoCheck(this._urovSelf),
                new ControlInfoText(this._tUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._tUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._Iurov, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._urovPusk, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._urovBlock, StringsConfig.SwitchSignals)
                );

            if (this._version >= 3.04)
            {
                ComboBox[] bgs =
                {
                    this.goin1,this.goin2,this.goin3,this.goin4,this.goin5,this.goin6,this.goin7,this.goin8,this.goin9,this.goin10,
                    this.goin11,this.goin12,this.goin13,this.goin14,this.goin15,this.goin16,this.goin17,this.goin18,this.goin19,this.goin20,
                    this.goin21,this.goin22,this.goin23,this.goin24,this.goin25,this.goin26,this.goin27,this.goin28,this.goin29,this.goin30,
                    this.goin31,this.goin32,this.goin33,this.goin34,this.goin35,this.goin36,this.goin37,this.goin38,this.goin39,this.goin40,
                    this.goin41,this.goin42,this.goin43,this.goin44,this.goin45,this.goin46,this.goin47,this.goin48,this.goin49,this.goin50,
                    this.goin51,this.goin52,this.goin53,this.goin54,this.goin55,this.goin56,this.goin57,this.goin58,this.goin59,this.goin60,
                    this.goin61,this.goin62,this.goin63,this.goin64
                };

                NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                    this._toolTip,
                    new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

                NewStructValidator<Goose> _gooseValidator = new NewStructValidator<Goose>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this.operationBGS, StringsConfig.GooseConfig),
                    new ControlInfoMultiCombo(bgs, StringsConfig.GooseSignal)
                    );

                SetpointsValidator<GooseConfig, Goose> allGooseSetpointValidator = new SetpointsValidator<GooseConfig, Goose>
                    (new ComboboxSelector(this.currentBGS, StringsConfig.GooseNames), _gooseValidator);

                if (this._version < 3.07)
                {
                    this._configurationValidator304 = new StructUnion<ConfigurationStruct304>
                    (
                        this._allGroupSetpointsValidator304,
                        switchValidator,
                        inputSignalsValidator,
                        oscopeUnion,
                        automaticsParametersUnion,
                        ethernetValidator,
                        configAddValidator,
                        urovValidator,
                        allGooseSetpointValidator
                    );
                }
                else
                {
                    this._configurationValidator307 = new StructUnion<ConfigurationStruct307>
                    (
                        this._allGroupSetpointsValidator307,
                        switchValidator,
                        inputSignalsValidator,
                        oscopeUnion,
                        automaticsParametersUnion,
                        ethernetValidator,
                        configAddValidator,
                        urovValidator,
                        allGooseSetpointValidator
                    );
                }
            }
            else if(this._version == 3.03)
            {
                this._configurationTabControl.TabPages.Remove(this._gooseTabPage);

                NewStructValidator<ConfigIPAddress> ethernetValidator = new NewStructValidator<ConfigIPAddress>(
                    this._toolTip,
                    new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                    new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

                this._configurationValidator303 = new StructUnion<ConfigurationStruct303>
                (
                this._allGroupSetpointsValidator,
                switchValidator,
                inputSignalsValidator,
                oscopeUnion,
                automaticsParametersUnion,
                ethernetValidator,
                configAddValidator,
                urovValidator
                );
            }
            else
            {
                this._configurationTabControl.TabPages.Remove(this._ethernetPage);
                this._configurationTabControl.TabPages.Remove(this._gooseTabPage);
                this._configurationValidator = new StructUnion<ConfigurationStructV300>
                (
                this._allGroupSetpointsValidator,
                switchValidator,
                inputSignalsValidator,
                oscopeUnion,
                automaticsParametersUnion,
                configAddValidator,
                urovValidator
                );
            }

            Func<IValidatingRule> currentFuncOmpPrimary = () =>
            {
                double koef = this.GetKoeff();
                return new DoubleToComaRule(0, 2 * koef, 4);
            };

            //нужен только для инициализации валидаторов текстбоксов
            NewStructValidator<OneWordStruct> _primaryResistValidator = new NewStructValidator<OneWordStruct>
                (
                this._toolTip,
                new ControlInfoTextDependent(this._xud1p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud2p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud3p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud4p, currentFuncOmpPrimary),
                new ControlInfoTextDependent(this._xud5p, currentFuncOmpPrimary)
                );
        }

        #endregion [Ctor's]
        
        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;
            if (this._version >= 3.07)
            {
                this._currentSetpointsStruct307 = this._configuration307.Value;
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._currentSetpointsStruct304 = this._configuration304.Value;
            }
            else if (this._version == 3.03)
            {
                this._currentSetpointsStruct303 = this._configuration303.Value;
            }
            else
            {
                this._currentSetpointsStruct = this._configuration.Value;
            }
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0x0D00, true, "ConfirmConfig"+this._device.DeviceNumber, this._device);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]
        
        #region [Help members]
        
        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            if (this._version >= 3.07)
            {
                this._configurationValidator307.Set(this._currentSetpointsStruct307);
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._configurationValidator304.Set(this._currentSetpointsStruct304);
            }
            else if (this._version == 3.03)
            {
                this._configurationValidator303.Set(this._currentSetpointsStruct303);
            }
            else
            {
                this._configurationValidator.Set(this._currentSetpointsStruct);
            }
            if (this._resistCheck.Checked) this.GetPrimaryResistValue();
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._clearSetpointBtn.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            if (this._version >= 3.07)
            {
                if (this._configurationValidator307.Check(out message, true))
                {
                    if (this._resistCheck.Checked) this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._currentSetpointsStruct307 = this._configurationValidator307.Get();
                    this._configuration307.Value = this._currentSetpointsStruct307;
                    return true;
                }
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                if (this._configurationValidator304.Check(out message, true))
                {
                    if (this._resistCheck.Checked) this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._currentSetpointsStruct304 = this._configurationValidator304.Get();
                    this._configuration304.Value = this._currentSetpointsStruct304;
                    return true;
                }
            }
            else if (this._version == 3.03)
            {
                if (this._configurationValidator303.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._currentSetpointsStruct303 = this._configurationValidator303.Get();
                    this._configuration303.Value = this._currentSetpointsStruct303;
                    return true;
                }
            }
            else
            {
                if (this._configurationValidator.Check(out message, true))
                {
                    if (this._resistCheck.Checked)
                        this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                    this._currentSetpointsStruct = this._configurationValidator.Get();
                    this._configuration.Value = this._currentSetpointsStruct;
                    return true;
                }
            }
            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._statusLabel.Text = READING_CONFIG;
            this._progressBar.Value = 0;
            if (this._version >= 3.07)
            {
                this._configuration307.LoadStruct();
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._configuration304.LoadStruct();
            }
            else if (this._version == 3.03)
            {
                this._configuration303.LoadStruct();
            }
            else
            {
                this._configuration.LoadStruct();
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private double GetKoeff()
        {
            double ittf, ktnf, tokIn;
            if (this._version >= 3.07)
            {
                MeasureTransN4Struct str = this._measureTransN4Union.Get();
                ittf = str.ChannelI.Ittl;
                ktnf = str.ChannelU.KthlValue;
                tokIn = str.ChannelI.InpInValue;
                return ktnf / ittf * tokIn;
            }
            else
            {
                MeasureTransStructV3 str = this._measureTransUnion.Get();
                ittf = str.ChannelI.Ittl;
                ktnf = str.ChannelU.KthlValue;
                tokIn = str.ChannelI.InpInValue;
                return ktnf / ittf * tokIn;
            }
        }

        private void GetSecondResistValue()
        {
            this.IsProcess = true;
            double koef = 1/this.GetKoeff();
            for (int i = 0; i < this._primary.Length; i++)
            {
                this._second[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._primary[i].Text)*koef);
            }
            this.resistanceDefTabCtr1.GetSecondValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        private void GetPrimaryResistValue()
        {
            this.IsProcess = true;
            double koef = this.GetKoeff();
            for (int i = 0; i < this._second.Length; i++)
            {
                this._primary[i].Text = string.Format("{0:F4}", Convert.ToDouble(this._second[i].Text)*koef);
            }
            this.resistanceDefTabCtr1.GetPrimaryValue(koef);
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi1Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        #endregion [Help members]

        #region [Event handlers]

        private void OnRefreshInfoTable()
        {
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartRead();
            }
            this.ResetSetpoints(false);
        }

        private void ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this._configuration != null) this._configuration.RemoveStructQueries();
        }

        private void ConfigurationForm_Activated(object sender, EventArgs e)
        {
            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.WriteConfig();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}

            if (_version >= 3.03)
            {
                DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 762 №{0}? " +
                                                                    "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                        this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                    "Запись", MessageBoxButtons.YesNo);
                if (result != DialogResult.Yes) return;
            }
            else
            {
                DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 762 №{0}? ",
                        this._device.DeviceNumber),
                    "Запись", MessageBoxButtons.YesNo);
                if (result != DialogResult.Yes) return;
            }
            this._progressBar.Value = 0;
            if (!this.WriteConfiguration()) return;
            this._statusLabel.Text = WRITING_CONFIG;
            if (this._version >= 3.07)
            {
                this._configuration307.SaveStruct();
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._configuration304.SaveStruct();
            }
            else if (this._version == 3.03)
            {
                this._configuration303.SaveStruct();
            }
            else
            {
                this._configuration.SaveStruct();
            }
        }

        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;
            this._oscSizeTextBox.Text = (54528*2/(comboBox.SelectedIndex + 2)).ToString();
        }

        private void _clearSetpointBtn_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (this._version >= 3.07)
            {
                this._configurationValidator307.Reset("MR762");
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                this._configurationValidator304.Reset("MR762");
            }
            else if (this._version == 3.03)
            {
                this._configurationValidator303.Reset("MR762");
            }
            else
            {
                 this._configurationValidator.Reset("MR762");
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private void _resistCheck_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (this._return)
                {
                    this._return = false;
                    return;
                }
                if (this._resistCheck.Checked)
                {
                    DialogResult res =
                        MessageBox.Show("Сопротивления будут пересчитаны в первичные величины. Обратите внимание на корректность введенных значений IТТф и KТНф",
                            "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    string mes;

                    if (this._version < 3.07)
                    {
                        bool check = this._measureTransUnion.Check(out mes, false);
                        check &= this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                        if (check)
                        {
                            if (res == DialogResult.OK)
                            {
                                this.omp1.Visible = false;
                                this.omp2.Visible = true;
                                this.resistanceDefTabCtr1.IsPrimary = true;
                                this.GetPrimaryResistValue();
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        this._return = true;
                        this._resistCheck.Checked = false;
                    }

                    else
                    {
                        bool check = this._measureTransN4Union.Check(out mes, false);
                        check &= this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidatorNew.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                        if (check)
                        {
                            if (res == DialogResult.OK)
                            {
                                this.omp1.Visible = false;
                                this.omp2.Visible = true;
                                this.resistanceDefTabCtr1.IsPrimary = true;
                                this.GetPrimaryResistValue();
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        this._return = true;
                        this._resistCheck.Checked = false;
                    }

                }
                else
                {
                    DialogResult res = MessageBox.Show("Сопротивления будут пересчитаны во вторичные величины к размерности Ом*Iн.вт. Обратите внимание на корректность введенных значений IТТф и KТНф",
                        "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    string mes;

                    if (this._version < 3.07)
                    {
                        bool check = this._measureTransUnion.Check(out mes, false);
                        check &= this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                        if (check)
                        {
                            if (res == DialogResult.OK)
                            {
                                this.omp1.Visible = true;
                                this.omp2.Visible = false;
                                this.resistanceDefTabCtr1.IsPrimary = false;
                                this.GetSecondResistValue();
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

                    else
                    {
                        bool check = this._measureTransN4Union.Check(out mes, false);
                        check &= this.resistanceDefTabCtr1.ResistValidator.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.LoadValidatorNew.Check(out mes, false)
                                 && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                        if (check)
                        {
                            if (res == DialogResult.OK)
                            {
                                this.omp1.Visible = true;
                                this.omp2.Visible = false;
                                this.resistanceDefTabCtr1.IsPrimary = false;
                                this.GetSecondResistValue();
                                return;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }

                    this._return = true;
                    this._resistCheck.Checked = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка перевода значений");
            }
            
        }

        private void Mr771ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
                if (this._version >= 3.07)
                {
                    this._configurationValidator307.Reset();
                }
                else if (this._version >= 3.04 && this._version < 3.07)
                {
                    this._configurationValidator304.Reset();
                }
                else if (this._version == 3.03)
                {
                    this._configurationValidator303.Reset();
                }
                else
                {
                    this._configurationValidator.Reset();
                }
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]{
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.clearSetpointsItem,
                this.resetSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem});
        }
        #endregion [Event handlers]

        #region [Save/Load File Members]

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();

                if (Common.VersionConverter(_device.DeviceVersion) >= 3.06)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR762_BASE_CONFIG_PATH_WHITH_AC, this._device.DeviceVersion, _device.DevicePlant));
                }
                else
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR762_BASE_CONFIG_PATH, this._device.DeviceVersion));
                }
                

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (this._version >= 3.07)
                {
                    this._currentSetpointsStruct307.InitStruct(values);
                }
                else if (this._version >= 3.04 && this._version < 3.07)
                {
                    this._currentSetpointsStruct304.InitStruct(values);
                }
                else if (this._version == 3.03)
                {
                    this._currentSetpointsStruct303.InitStruct(values);
                }
                else
                {
                    this._currentSetpointsStruct.InitStruct(values);
                }
                this.ShowConfiguration();
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                        "Внимание", MessageBoxButtons.YesNo) != DialogResult.Yes)
                        return;
                }
                if (this._version >= 3.07)
                {
                    this._configurationValidator307.Reset();
                }
                else if (this._version >= 3.04 && this._version < 3.07)
                {
                    this._configurationValidator304.Reset();
                }
                else if (this._version == 3.03)
                {
                    this._configurationValidator303.Reset();
                }
                else
                {
                    this._configurationValidator.Reset();
                }
            }
        }
        
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveInFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }           
        }

        private void SaveInFile()
        {
            if (this.WriteConfiguration())
            {
                this._statusLabel.Text = "Идет запись конфигурации в файл";
                this._saveConfigurationDlg.FileName =
                    string.Format("МР762_Уставки_версия_{0}.xml", this._device.DeviceVersion);
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                try
                {
                    this.Serialize(this._saveConfigurationDlg.FileName);
                    this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", this._saveConfigurationDlg.FileName);
                    this.IsProcess = false;
                }
                catch
                {
                    MessageBox.Show(FILE_SAVE_FAIL);
                    this.IsProcess = false;
                }
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement(DEVICE_NAME));
            ushort[] values;
            if (this._version >= 3.07)
            {
                values = this._currentSetpointsStruct307.GetValues();
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                values = this._currentSetpointsStruct304.GetValues();
            }
            else if (this._version == 3.03)
            {
                values = this._currentSetpointsStruct303.GetValues();
            }
            else
            {
                values = this._currentSetpointsStruct.GetValues();
            }
            XmlElement element = doc.CreateElement(XML_HEAD);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
            if (doc.DocumentElement == null)
            {
                throw new NullReferenceException();
            }
            doc.DocumentElement.AppendChild(element);
            doc.Save(binFileName);
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (this._version >= 3.07)
                {
                    this._currentSetpointsStruct307.InitStruct(values);
                }
                else if (this._version >= 3.04 && this._version < 3.07)
                {
                    this._currentSetpointsStruct304.InitStruct(values);
                }
                else if (this._version == 3.03)
                {
                    this._currentSetpointsStruct303.InitStruct(values);
                }
                else
                {
                    this._currentSetpointsStruct.InitStruct(values);
                }
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch( Exception)
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveToHtml();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идет запись конфигурации в HTML";
                    string fileName;
                    if (this._version >= 3.07)
                    {
                        this._currentSetpointsStruct307.DeviceVersion = this._version;
                        this._currentSetpointsStruct307.DeviceConfig = this._device.DevicePlant;
                        this._currentSetpointsStruct307.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct307.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct307.SizeOsc = this._oscSizeTextBox.Text;
                        ExportGroupForm exportGroup = new ExportGroupForm();
                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }

                        this._currentSetpointsStruct307.Group = (int)exportGroup.SelectedGroup;
                        if (exportGroup.SelectedGroup == ExportStruct.ALL)
                        {
                            fileName = string.Format("{0} Уставки версия {1} все группы", DEVICE_NAME, this._device.DeviceVersion);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3All, fileName, this._currentSetpointsStruct307);
                        }
                        else
                        {
                            fileName = string.Format("{0} Уставки версия {1} группа{2}", DEVICE_NAME, this._device.DeviceVersion, this._currentSetpointsStruct307.Group);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3Group, fileName, this._currentSetpointsStruct307);
                        }
                    }
                    else if (this._version >= 3.04 && this._version < 3.07)
                    {
                        this._currentSetpointsStruct304.DeviceVersion = this._version;
                        this._currentSetpointsStruct304.DeviceConfig = this._device.DevicePlant;
                        this._currentSetpointsStruct304.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct304.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct304.SizeOsc = this._oscSizeTextBox.Text;
                        ExportGroupForm exportGroup = new ExportGroupForm();
                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }

                        this._currentSetpointsStruct304.Group = (int)exportGroup.SelectedGroup;
                        if (exportGroup.SelectedGroup == ExportStruct.ALL)
                        {
                            fileName = string.Format("{0} Уставки версия {1} все группы", DEVICE_NAME, this._device.DeviceVersion);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3All, fileName, this._currentSetpointsStruct304);
                        }
                        else
                        {
                            fileName = string.Format("{0} Уставки версия {1} группа{2}", DEVICE_NAME, this._device.DeviceVersion, this._currentSetpointsStruct304.Group);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3Group, fileName, this._currentSetpointsStruct304);
                        }
                    }
                    else if (this._version == 3.03)
                    {
                        this._currentSetpointsStruct303.DeviceVersion = this._version;
                        this._currentSetpointsStruct303.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct303.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct303.SizeOsc = this._oscSizeTextBox.Text;
                        ExportGroupForm exportGroup = new ExportGroupForm();
                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }

                        this._currentSetpointsStruct303.Group = (int)exportGroup.SelectedGroup;
                        if (exportGroup.SelectedGroup == ExportStruct.ALL)
                        {
                            fileName = string.Format("{0} Уставки версия {1} все группы", DEVICE_NAME, this._device.DeviceVersion);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3All, fileName, this._currentSetpointsStruct303);
                        }
                        else
                        {
                            fileName = string.Format("{0} Уставки версия {1} группа{2}", DEVICE_NAME, this._device.DeviceVersion, this._currentSetpointsStruct303.Group);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3Group, fileName, this._currentSetpointsStruct303);
                        }
                    }
                    else
                    {
                        this._currentSetpointsStruct.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                        this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                        this._currentSetpointsStruct.Primary = this._resistCheck.Checked;
                        this._currentSetpointsStruct.SizeOsc = this._oscSizeTextBox.Text;
                        ExportGroupForm exportGroup = new ExportGroupForm();
                        if (exportGroup.ShowDialog() != DialogResult.OK)
                        {
                            this.IsProcess = false;
                            this._statusLabel.Text = string.Empty;
                            return;
                        }

                        this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                        if (exportGroup.SelectedGroup == ExportStruct.ALL)
                        {
                            fileName = string.Format("{0} Уставки версия {1} все группы", DEVICE_NAME, this._device.DeviceVersion);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3All, fileName, this._currentSetpointsStruct);
                        }
                        else
                        {
                            fileName = string.Format("{0} Уставки версия {1} группа{2}", DEVICE_NAME, this._device.DeviceVersion, this._currentSetpointsStruct.Group);
                            this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR76XV3Group, fileName, this._currentSetpointsStruct);
                        }
                    }
                }
                this.IsProcess = false;

            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }

        #endregion [Save/Load File Members]

        #region [Switch setpoints]

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;
            if (this._version >= 3.07)
            {
                if (this._groupSetpointValidator307.Check(out message, true))
                {
                    GroupSetpoint307[] allSetpoints =
                        this.CopySetpoints<AllGroupSetpointStruct307, GroupSetpoint307>(this._groupSetpointValidator307, this._allGroupSetpointsValidator307);
                    this._currentSetpointsStruct307.AllGroupSetpoints.Setpoints = allSetpoints;
                    this._allGroupSetpointsValidator307.Set(this._currentSetpointsStruct307.AllGroupSetpoints);
                    MessageBox.Show("Копирование завершено");
                    return;
                }
            }
            else if (this._version >= 3.04 && this._version < 3.07)
            {
                if (this._groupSetpointValidator304.Check(out message, true))
                {
                    GroupSetpoint304[] allSetpoints =
                        this.CopySetpoints<AllGroupSetpointStruct304, GroupSetpoint304>(this._groupSetpointValidator304, this._allGroupSetpointsValidator304);
                    this._currentSetpointsStruct304.AllGroupSetpoints.Setpoints = allSetpoints;
                    this._allGroupSetpointsValidator304.Set(this._currentSetpointsStruct304.AllGroupSetpoints);
                    MessageBox.Show("Копирование завершено");
                    return;
                }
            }
            else
            {
                if (this._groupSetpointValidator.Check(out message, true))
                {
                    GroupSetpointStruct[] allSetpoints = 
                        this.CopySetpoints<AllGroupSetpointStruct, GroupSetpointStruct>(this._groupSetpointValidator, this._allGroupSetpointsValidator);
                    if (this._version == 3.03)
                    {
                        this._currentSetpointsStruct303.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._allGroupSetpointsValidator.Set(this._currentSetpointsStruct303.AllGroupSetpoints);
                    }
                    else
                    {
                        this._currentSetpointsStruct.AllGroupSetpoints.Setpoints = allSetpoints;
                        this._allGroupSetpointsValidator.Set(this._currentSetpointsStruct.AllGroupSetpoints);
                    }
                    
                    MessageBox.Show("Копирование завершено");
                    return;
                }
            }
            MessageBox.Show("Обнаружены некорректные уставки");
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2) union.Get();
            T2[] allSetpoints = ((T1) setpointValidator.Get()).Setpoints;
            if ((string) this._copySetpoinsGroupComboBox.SelectedItem == StringsConfig.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        #endregion

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr762Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr762ConfigurationFormV300); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        #endregion [IFormView Members]

		#region [Events CreateTree]
        private void VLSclbGr1_16SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }
        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _lsAndDgvGr1_8CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void _lsOrDgvGr1_8CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }
        #endregion [Events CreateTree]
    }
}