﻿namespace BEMN.Mr762.Version300.Measuring
{
    partial class Mr762MeasuringFormV300
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TabPage _analogTabPage;
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this.CN = new System.Windows.Forms.TextBox();
            this.CA = new System.Windows.Forms.TextBox();
            this.BN = new System.Windows.Forms.TextBox();
            this.AN = new System.Windows.Forms.TextBox();
            this.BC = new System.Windows.Forms.TextBox();
            this.AB = new System.Windows.Forms.TextBox();
            this.label340 = new System.Windows.Forms.Label();
            this.label339 = new System.Windows.Forms.Label();
            this.label338 = new System.Windows.Forms.Label();
            this.label337 = new System.Windows.Forms.Label();
            this.label336 = new System.Windows.Forms.Label();
            this.label335 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._dU = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this._dFi = new System.Windows.Forms.TextBox();
            this._dF = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this._iZc5Box = new System.Windows.Forms.TextBox();
            this._iZa5Box = new System.Windows.Forms.TextBox();
            this._iZb5Box = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._iZc4Box = new System.Windows.Forms.TextBox();
            this._iZa4Box = new System.Windows.Forms.TextBox();
            this._iZb4Box = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this._iZc3Box = new System.Windows.Forms.TextBox();
            this._iZa3Box = new System.Windows.Forms.TextBox();
            this._iZb3Box = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this._iZc2Box = new System.Windows.Forms.TextBox();
            this._iZa2Box = new System.Windows.Forms.TextBox();
            this._iZb2Box = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this._iZa1Box = new System.Windows.Forms.TextBox();
            this._iZc1Box = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this._iZb1Box = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this._iZabBox = new System.Windows.Forms.TextBox();
            this._iZcaBox = new System.Windows.Forms.TextBox();
            this._iZbcBox = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label330 = new System.Windows.Forms.Label();
            this.label331 = new System.Windows.Forms.Label();
            this.label332 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this._zc5Box = new System.Windows.Forms.TextBox();
            this._za5Box = new System.Windows.Forms.TextBox();
            this._zb5Box = new System.Windows.Forms.TextBox();
            this.label327 = new System.Windows.Forms.Label();
            this.label328 = new System.Windows.Forms.Label();
            this.label329 = new System.Windows.Forms.Label();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this._zc4Box = new System.Windows.Forms.TextBox();
            this._za4Box = new System.Windows.Forms.TextBox();
            this._zb4Box = new System.Windows.Forms.TextBox();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.label326 = new System.Windows.Forms.Label();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this._zc3Box = new System.Windows.Forms.TextBox();
            this._za3Box = new System.Windows.Forms.TextBox();
            this._zb3Box = new System.Windows.Forms.TextBox();
            this.label321 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.label323 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._zc2Box = new System.Windows.Forms.TextBox();
            this._za2Box = new System.Windows.Forms.TextBox();
            this._zb2Box = new System.Windows.Forms.TextBox();
            this.label152 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._za1Box = new System.Windows.Forms.TextBox();
            this._zc1Box = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this._zb1Box = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._zabBox = new System.Windows.Forms.TextBox();
            this._zcaBox = new System.Windows.Forms.TextBox();
            this._zbcBox = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.rxcaLabel = new System.Windows.Forms.Label();
            this.rxbcLabel = new System.Windows.Forms.Label();
            this.rxabLabel = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._nWarm = new System.Windows.Forms.TextBox();
            this.label217 = new System.Windows.Forms.Label();
            this._nPusk = new System.Windows.Forms.TextBox();
            this.label216 = new System.Windows.Forms.Label();
            this._qpTextBox = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._cosfTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._qTextBox = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this._pTextBox = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._fTextBox = new System.Windows.Forms.TextBox();
            this._cU0 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._u30 = new System.Windows.Forms.TextBox();
            this.label333 = new System.Windows.Forms.Label();
            this.label343 = new System.Windows.Forms.Label();
            this.label342 = new System.Windows.Forms.Label();
            this.label341 = new System.Windows.Forms.Label();
            this._cUca = new System.Windows.Forms.TextBox();
            this._ucaTextBox = new System.Windows.Forms.TextBox();
            this._cUbc = new System.Windows.Forms.TextBox();
            this._ubcTextBox = new System.Windows.Forms.TextBox();
            this._cUab = new System.Windows.Forms.TextBox();
            this._uabTextBox = new System.Windows.Forms.TextBox();
            this._cU2 = new System.Windows.Forms.TextBox();
            this._u2TextBox = new System.Windows.Forms.TextBox();
            this._cU1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._u1TextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._cUc = new System.Windows.Forms.TextBox();
            this._ucTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._cUb = new System.Windows.Forms.TextBox();
            this._ubTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._cUa = new System.Windows.Forms.TextBox();
            this._uaTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._cI0 = new System.Windows.Forms.TextBox();
            this._i30 = new System.Windows.Forms.TextBox();
            this.label334 = new System.Windows.Forms.Label();
            this._cIc = new System.Windows.Forms.TextBox();
            this._icTextBox = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this._igTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._cI2 = new System.Windows.Forms.TextBox();
            this._i2TextBox = new System.Windows.Forms.TextBox();
            this._cIn1 = new System.Windows.Forms.TextBox();
            this._in1TextBox = new System.Windows.Forms.TextBox();
            this._cIn = new System.Windows.Forms.TextBox();
            this._inTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._cIb = new System.Windows.Forms.TextBox();
            this._ibTextBox = new System.Windows.Forms.TextBox();
            this._cI1 = new System.Windows.Forms.TextBox();
            this._i1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._cIa = new System.Windows.Forms.TextBox();
            this._iaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._discretTabPage1 = new System.Windows.Forms.TabPage();
            this.reversGroup = new System.Windows.Forms.GroupBox();
            this.label448 = new System.Windows.Forms.Label();
            this.label449 = new System.Windows.Forms.Label();
            this._P2Io = new BEMN.Forms.LedControl();
            this._P1Io = new BEMN.Forms.LedControl();
            this._P2 = new BEMN.Forms.LedControl();
            this.label455 = new System.Windows.Forms.Label();
            this._P1 = new BEMN.Forms.LedControl();
            this.label456 = new System.Windows.Forms.Label();
            this.groupBox53 = new System.Windows.Forms.GroupBox();
            this._damageA = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this.label367 = new System.Windows.Forms.Label();
            this._damageB = new BEMN.Forms.LedControl();
            this._damageC = new BEMN.Forms.LedControl();
            this.label370 = new System.Windows.Forms.Label();
            this.groupBox52 = new System.Windows.Forms.GroupBox();
            this._OnKsAndYppN = new BEMN.Forms.LedControl();
            this.label361 = new System.Windows.Forms.Label();
            this._UnoUno = new BEMN.Forms.LedControl();
            this.label362 = new System.Windows.Forms.Label();
            this._UyUno = new BEMN.Forms.LedControl();
            this.label363 = new System.Windows.Forms.Label();
            this._US = new BEMN.Forms.LedControl();
            this._U1noU2y = new BEMN.Forms.LedControl();
            this.label364 = new System.Windows.Forms.Label();
            this.label365 = new System.Windows.Forms.Label();
            this._YS = new System.Windows.Forms.Label();
            this._OS = new BEMN.Forms.LedControl();
            this.label368 = new System.Windows.Forms.Label();
            this._autoSinchr = new BEMN.Forms.LedControl();
            this.groupBox50 = new System.Windows.Forms.GroupBox();
            this._readyApv = new BEMN.Forms.LedControl();
            this.label360 = new System.Windows.Forms.Label();
            this._blockApv = new BEMN.Forms.LedControl();
            this.label356 = new System.Windows.Forms.Label();
            this.label222 = new System.Windows.Forms.Label();
            this._puskApv = new BEMN.Forms.LedControl();
            this._krat4 = new BEMN.Forms.LedControl();
            this.label359 = new System.Windows.Forms.Label();
            this.label353 = new System.Windows.Forms.Label();
            this._turnOnApv = new BEMN.Forms.LedControl();
            this._krat3 = new BEMN.Forms.LedControl();
            this.label358 = new System.Windows.Forms.Label();
            this.label354 = new System.Windows.Forms.Label();
            this._krat1 = new BEMN.Forms.LedControl();
            this._zapretApv = new BEMN.Forms.LedControl();
            this.label357 = new System.Windows.Forms.Label();
            this._krat2 = new BEMN.Forms.LedControl();
            this.label355 = new System.Windows.Forms.Label();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._faultObruvFaz = new BEMN.Forms.LedControl();
            this.label351 = new System.Windows.Forms.Label();
            this._faultTH3u0 = new BEMN.Forms.LedControl();
            this.label350 = new System.Windows.Forms.Label();
            this._faultTHU2 = new BEMN.Forms.LedControl();
            this.label349 = new System.Windows.Forms.Label();
            this.label348 = new System.Windows.Forms.Label();
            this._faultTNmgn = new BEMN.Forms.LedControl();
            this.label218 = new System.Windows.Forms.Label();
            this._faultUabc = new BEMN.Forms.LedControl();
            this.label221 = new System.Windows.Forms.Label();
            this._faultTN = new BEMN.Forms.LedControl();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label211 = new System.Windows.Forms.Label();
            this.label209 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this._r4IoLed = new BEMN.Forms.LedControl();
            this._r5Led = new BEMN.Forms.LedControl();
            this._r1Led = new BEMN.Forms.LedControl();
            this._r3IoLed = new BEMN.Forms.LedControl();
            this.label214 = new System.Windows.Forms.Label();
            this.label199 = new System.Windows.Forms.Label();
            this._r6IoLed = new BEMN.Forms.LedControl();
            this._r2IoLed = new BEMN.Forms.LedControl();
            this._r6Led = new BEMN.Forms.LedControl();
            this._r2Led = new BEMN.Forms.LedControl();
            this._r4Led = new BEMN.Forms.LedControl();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this._r3Led = new BEMN.Forms.LedControl();
            this._r5IoLed = new BEMN.Forms.LedControl();
            this._r1IoLed = new BEMN.Forms.LedControl();
            this.dugGroup = new System.Windows.Forms.GroupBox();
            this.dugPusk = new BEMN.Forms.LedControl();
            this.label446 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._kachanie = new BEMN.Forms.LedControl();
            this.label371 = new System.Windows.Forms.Label();
            this._inZone = new BEMN.Forms.LedControl();
            this.label373 = new System.Windows.Forms.Label();
            this._outZone = new BEMN.Forms.LedControl();
            this.label374 = new System.Windows.Forms.Label();
            this.groupBox54 = new System.Windows.Forms.GroupBox();
            this._faultHardware1 = new BEMN.Forms.LedControl();
            this.label372 = new System.Windows.Forms.Label();
            this._faultLogic1 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._faultSwitchOff1 = new BEMN.Forms.LedControl();
            this.label378 = new System.Windows.Forms.Label();
            this._faultMeasuringF1 = new BEMN.Forms.LedControl();
            this.label375 = new System.Windows.Forms.Label();
            this._faultMeasuringU1 = new BEMN.Forms.LedControl();
            this.label376 = new System.Windows.Forms.Label();
            this._faultSoftware1 = new BEMN.Forms.LedControl();
            this.label377 = new System.Windows.Forms.Label();
            this.groupBox65 = new System.Windows.Forms.GroupBox();
            this.dvPusk = new BEMN.Forms.LedControl();
            this.label389 = new System.Windows.Forms.Label();
            this.dvBlockN = new BEMN.Forms.LedControl();
            this.dvBlockQ = new BEMN.Forms.LedControl();
            this.label441 = new System.Windows.Forms.Label();
            this.label439 = new System.Windows.Forms.Label();
            this.dvWork = new BEMN.Forms.LedControl();
            this.label440 = new System.Windows.Forms.Label();
            this.groupBox63 = new System.Windows.Forms.GroupBox();
            this.avrOn = new BEMN.Forms.LedControl();
            this.label160 = new System.Windows.Forms.Label();
            this.avrBlock = new BEMN.Forms.LedControl();
            this.label212 = new System.Windows.Forms.Label();
            this.avrOff = new BEMN.Forms.LedControl();
            this.label213 = new System.Windows.Forms.Label();
            this.urovGroup = new System.Windows.Forms.GroupBox();
            this.urov1Led = new BEMN.Forms.LedControl();
            this.label318 = new System.Windows.Forms.Label();
            this.blockUrovLed = new BEMN.Forms.LedControl();
            this.label401 = new System.Windows.Forms.Label();
            this.urov2Led = new BEMN.Forms.LedControl();
            this.label415 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this._faultOff = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._acceleration = new BEMN.Forms.LedControl();
            this._auto4Led = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label413 = new System.Windows.Forms.Label();
            this._iS8 = new BEMN.Forms.LedControl();
            this._iS8Io = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._iS7 = new BEMN.Forms.LedControl();
            this._iS7Io = new BEMN.Forms.LedControl();
            this.label229 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this._iS1 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._iS2 = new BEMN.Forms.LedControl();
            this._iS1Io = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._iS3 = new BEMN.Forms.LedControl();
            this._iS2Io = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._iS4 = new BEMN.Forms.LedControl();
            this._iS3Io = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._iS5 = new BEMN.Forms.LedControl();
            this._iS4Io = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._iS6 = new BEMN.Forms.LedControl();
            this._iS5Io = new BEMN.Forms.LedControl();
            this._iS6Io = new BEMN.Forms.LedControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label270 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i5Io = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._i3Io = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._i1 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.splGroupBox = new System.Windows.Forms.GroupBox();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.label190 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._module34 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._module33 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._module18 = new BEMN.Forms.LedControl();
            this._module10 = new BEMN.Forms.LedControl();
            this.label161 = new System.Windows.Forms.Label();
            this._module32 = new BEMN.Forms.LedControl();
            this._module17 = new BEMN.Forms.LedControl();
            this.label138 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this._module31 = new BEMN.Forms.LedControl();
            this.label164 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this._module9 = new BEMN.Forms.LedControl();
            this._module30 = new BEMN.Forms.LedControl();
            this._module16 = new BEMN.Forms.LedControl();
            this.label140 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this._module29 = new BEMN.Forms.LedControl();
            this.label163 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this._module15 = new BEMN.Forms.LedControl();
            this._module28 = new BEMN.Forms.LedControl();
            this._module5 = new BEMN.Forms.LedControl();
            this.label142 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this._module27 = new BEMN.Forms.LedControl();
            this._module8 = new BEMN.Forms.LedControl();
            this.label232 = new System.Windows.Forms.Label();
            this._module14 = new BEMN.Forms.LedControl();
            this._module26 = new BEMN.Forms.LedControl();
            this.label176 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this._module25 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._module13 = new BEMN.Forms.LedControl();
            this._module24 = new BEMN.Forms.LedControl();
            this._module1 = new BEMN.Forms.LedControl();
            this.label129 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this._module23 = new BEMN.Forms.LedControl();
            this._module7 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._module12 = new BEMN.Forms.LedControl();
            this._module22 = new BEMN.Forms.LedControl();
            this.label175 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this._module21 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._module11 = new BEMN.Forms.LedControl();
            this._module20 = new BEMN.Forms.LedControl();
            this.label178 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this._module2 = new BEMN.Forms.LedControl();
            this._module19 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._module6 = new BEMN.Forms.LedControl();
            this.label168 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this._module3 = new BEMN.Forms.LedControl();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this._module4 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._d40 = new BEMN.Forms.LedControl();
            this._d24 = new BEMN.Forms.LedControl();
            this.label233 = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this._d39 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this._d38 = new BEMN.Forms.LedControl();
            this._d23 = new BEMN.Forms.LedControl();
            this.label235 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._d37 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this._d36 = new BEMN.Forms.LedControl();
            this._d7 = new BEMN.Forms.LedControl();
            this.label271 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this._d35 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this._d34 = new BEMN.Forms.LedControl();
            this._d1 = new BEMN.Forms.LedControl();
            this.label273 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this._k2 = new BEMN.Forms.LedControl();
            this.label369 = new System.Windows.Forms.Label();
            this._k1 = new BEMN.Forms.LedControl();
            this.label366 = new System.Windows.Forms.Label();
            this._d33 = new BEMN.Forms.LedControl();
            this.label274 = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this._d20 = new BEMN.Forms.LedControl();
            this._d32 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._d31 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this._d30 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this._d29 = new BEMN.Forms.LedControl();
            this._d5 = new BEMN.Forms.LedControl();
            this.label278 = new System.Windows.Forms.Label();
            this._d18 = new BEMN.Forms.LedControl();
            this._d28 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this.label279 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._d27 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this._d26 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._d25 = new BEMN.Forms.LedControl();
            this.label282 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d16 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this._d9 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this._d13 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this._d12 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._discretTabPage2 = new System.Windows.Forms.TabPage();
            this.reversGroup1 = new System.Windows.Forms.GroupBox();
            this.label450 = new System.Windows.Forms.Label();
            this.label451 = new System.Windows.Forms.Label();
            this._P2Io1 = new BEMN.Forms.LedControl();
            this._P1Io1 = new BEMN.Forms.LedControl();
            this._P21 = new BEMN.Forms.LedControl();
            this.label452 = new System.Windows.Forms.Label();
            this._P11 = new BEMN.Forms.LedControl();
            this.label453 = new System.Windows.Forms.Label();
            this.dugGroup1 = new System.Windows.Forms.GroupBox();
            this.dugPusk1 = new BEMN.Forms.LedControl();
            this.label447 = new System.Windows.Forms.Label();
            this.groupBox66 = new System.Windows.Forms.GroupBox();
            this.dvPusk1 = new BEMN.Forms.LedControl();
            this.label442 = new System.Windows.Forms.Label();
            this.dvBlockN1 = new BEMN.Forms.LedControl();
            this.dvBlockQ1 = new BEMN.Forms.LedControl();
            this.label443 = new System.Windows.Forms.Label();
            this.label444 = new System.Windows.Forms.Label();
            this.dvWork1 = new BEMN.Forms.LedControl();
            this.label445 = new System.Windows.Forms.Label();
            this.groupBox64 = new System.Windows.Forms.GroupBox();
            this._avrOn1 = new BEMN.Forms.LedControl();
            this.label385 = new System.Windows.Forms.Label();
            this._avrBlock1 = new BEMN.Forms.LedControl();
            this.label386 = new System.Windows.Forms.Label();
            this._avrOff1 = new BEMN.Forms.LedControl();
            this.label387 = new System.Windows.Forms.Label();
            this.urovGroup1 = new System.Windows.Forms.GroupBox();
            this.urov1Led1 = new BEMN.Forms.LedControl();
            this.label417 = new System.Windows.Forms.Label();
            this.blockUrovLed1 = new BEMN.Forms.LedControl();
            this.label418 = new System.Windows.Forms.Label();
            this.urov2Led1 = new BEMN.Forms.LedControl();
            this.label419 = new System.Windows.Forms.Label();
            this.groupBox61 = new System.Windows.Forms.GroupBox();
            this._OnKsAndYppn1 = new BEMN.Forms.LedControl();
            this.label423 = new System.Windows.Forms.Label();
            this._UnoUno1 = new BEMN.Forms.LedControl();
            this.label424 = new System.Windows.Forms.Label();
            this._UyUno1 = new BEMN.Forms.LedControl();
            this.label425 = new System.Windows.Forms.Label();
            this._US1 = new BEMN.Forms.LedControl();
            this._U1noU2y1 = new BEMN.Forms.LedControl();
            this.label426 = new System.Windows.Forms.Label();
            this.label427 = new System.Windows.Forms.Label();
            this.label428 = new System.Windows.Forms.Label();
            this._OS1 = new BEMN.Forms.LedControl();
            this.label429 = new System.Windows.Forms.Label();
            this._autoSinchr1 = new BEMN.Forms.LedControl();
            this.groupBox62 = new System.Windows.Forms.GroupBox();
            this._readyApv1 = new BEMN.Forms.LedControl();
            this.label430 = new System.Windows.Forms.Label();
            this._blockApv1 = new BEMN.Forms.LedControl();
            this.label431 = new System.Windows.Forms.Label();
            this.label432 = new System.Windows.Forms.Label();
            this._puskApv1 = new BEMN.Forms.LedControl();
            this._krat41 = new BEMN.Forms.LedControl();
            this.label433 = new System.Windows.Forms.Label();
            this.label434 = new System.Windows.Forms.Label();
            this._turnOnApv1 = new BEMN.Forms.LedControl();
            this._krat31 = new BEMN.Forms.LedControl();
            this.label435 = new System.Windows.Forms.Label();
            this.label436 = new System.Windows.Forms.Label();
            this._krat11 = new BEMN.Forms.LedControl();
            this._zapretApv1 = new BEMN.Forms.LedControl();
            this.label437 = new System.Windows.Forms.Label();
            this._krat21 = new BEMN.Forms.LedControl();
            this.label438 = new System.Windows.Forms.Label();
            this.groupBox58 = new System.Windows.Forms.GroupBox();
            this._damageA1 = new BEMN.Forms.LedControl();
            this.label409 = new System.Windows.Forms.Label();
            this.label410 = new System.Windows.Forms.Label();
            this._damageB1 = new BEMN.Forms.LedControl();
            this._damageC1 = new BEMN.Forms.LedControl();
            this.label411 = new System.Windows.Forms.Label();
            this.groupBox59 = new System.Windows.Forms.GroupBox();
            this._tnObivFaz = new BEMN.Forms.LedControl();
            this.l3 = new System.Windows.Forms.Label();
            this._faultTn3U0 = new BEMN.Forms.LedControl();
            this.l4 = new System.Windows.Forms.Label();
            this._faultTnU2 = new BEMN.Forms.LedControl();
            this.l5 = new System.Windows.Forms.Label();
            this.label416 = new System.Windows.Forms.Label();
            this._faultTNmgn1 = new BEMN.Forms.LedControl();
            this.l6 = new System.Windows.Forms.Label();
            this._externalTn = new BEMN.Forms.LedControl();
            this.l2 = new System.Windows.Forms.Label();
            this._faultTN1 = new BEMN.Forms.LedControl();
            this.groupBox60 = new System.Windows.Forms.GroupBox();
            this._kachanie1 = new BEMN.Forms.LedControl();
            this.label420 = new System.Windows.Forms.Label();
            this._inZone1 = new BEMN.Forms.LedControl();
            this.label421 = new System.Windows.Forms.Label();
            this._outZone1 = new BEMN.Forms.LedControl();
            this.label422 = new System.Windows.Forms.Label();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this.label379 = new System.Windows.Forms.Label();
            this.label380 = new System.Windows.Forms.Label();
            this.label381 = new System.Windows.Forms.Label();
            this.label382 = new System.Windows.Forms.Label();
            this._r4IoLed1 = new BEMN.Forms.LedControl();
            this._r5Led1 = new BEMN.Forms.LedControl();
            this._r1Led1 = new BEMN.Forms.LedControl();
            this._r3IoLed1 = new BEMN.Forms.LedControl();
            this.label383 = new System.Windows.Forms.Label();
            this.label384 = new System.Windows.Forms.Label();
            this._r6IoLed1 = new BEMN.Forms.LedControl();
            this._r2IoLed1 = new BEMN.Forms.LedControl();
            this._r6Led1 = new BEMN.Forms.LedControl();
            this._r2Led1 = new BEMN.Forms.LedControl();
            this._r4Led1 = new BEMN.Forms.LedControl();
            this.label388 = new System.Windows.Forms.Label();
            this.label390 = new System.Windows.Forms.Label();
            this._r3Led1 = new BEMN.Forms.LedControl();
            this._r5IoLed1 = new BEMN.Forms.LedControl();
            this._r1IoLed1 = new BEMN.Forms.LedControl();
            this.groupBox56 = new System.Windows.Forms.GroupBox();
            this.label414 = new System.Windows.Forms.Label();
            this._iS81 = new BEMN.Forms.LedControl();
            this._iS8Io1 = new BEMN.Forms.LedControl();
            this.label402 = new System.Windows.Forms.Label();
            this._iS71 = new BEMN.Forms.LedControl();
            this._iS7Io1 = new BEMN.Forms.LedControl();
            this.label391 = new System.Windows.Forms.Label();
            this.label392 = new System.Windows.Forms.Label();
            this._iS11 = new BEMN.Forms.LedControl();
            this.label393 = new System.Windows.Forms.Label();
            this._iS21 = new BEMN.Forms.LedControl();
            this._iS1Io1 = new BEMN.Forms.LedControl();
            this.label394 = new System.Windows.Forms.Label();
            this._iS31 = new BEMN.Forms.LedControl();
            this._iS2Io1 = new BEMN.Forms.LedControl();
            this.label395 = new System.Windows.Forms.Label();
            this.label396 = new System.Windows.Forms.Label();
            this._iS41 = new BEMN.Forms.LedControl();
            this._iS3Io1 = new BEMN.Forms.LedControl();
            this.label397 = new System.Windows.Forms.Label();
            this._iS51 = new BEMN.Forms.LedControl();
            this._iS4Io1 = new BEMN.Forms.LedControl();
            this.label398 = new System.Windows.Forms.Label();
            this._iS61 = new BEMN.Forms.LedControl();
            this._iS5Io1 = new BEMN.Forms.LedControl();
            this._iS6Io1 = new BEMN.Forms.LedControl();
            this.groupBox57 = new System.Windows.Forms.GroupBox();
            this.label399 = new System.Windows.Forms.Label();
            this.label400 = new System.Windows.Forms.Label();
            this._i6Io1 = new BEMN.Forms.LedControl();
            this._i5Io1 = new BEMN.Forms.LedControl();
            this._i61 = new BEMN.Forms.LedControl();
            this.label403 = new System.Windows.Forms.Label();
            this._i4Io1 = new BEMN.Forms.LedControl();
            this._i51 = new BEMN.Forms.LedControl();
            this.label404 = new System.Windows.Forms.Label();
            this._i3Io1 = new BEMN.Forms.LedControl();
            this._i41 = new BEMN.Forms.LedControl();
            this.label405 = new System.Windows.Forms.Label();
            this._i2Io1 = new BEMN.Forms.LedControl();
            this._i31 = new BEMN.Forms.LedControl();
            this.label406 = new System.Windows.Forms.Label();
            this._i1Io1 = new BEMN.Forms.LedControl();
            this._i21 = new BEMN.Forms.LedControl();
            this.label407 = new System.Windows.Forms.Label();
            this._i11 = new BEMN.Forms.LedControl();
            this.label408 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._fSpl1 = new BEMN.Forms.LedControl();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this._fSpl2 = new BEMN.Forms.LedControl();
            this.label317 = new System.Windows.Forms.Label();
            this._fSpl3 = new BEMN.Forms.LedControl();
            this._fSpl4 = new BEMN.Forms.LedControl();
            this.label320 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.label347 = new System.Windows.Forms.Label();
            this._faultDisable2 = new BEMN.Forms.LedControl();
            this.label346 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._faultDisable1 = new BEMN.Forms.LedControl();
            this._faultSwithON = new BEMN.Forms.LedControl();
            this._faultManage = new BEMN.Forms.LedControl();
            this._faultOtkaz = new BEMN.Forms.LedControl();
            this._faultBlockCon = new BEMN.Forms.LedControl();
            this._faultOut = new BEMN.Forms.LedControl();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.label228 = new System.Windows.Forms.Label();
            this._UabcLow10 = new BEMN.Forms.LedControl();
            this.label313 = new System.Windows.Forms.Label();
            this._freqLow40 = new BEMN.Forms.LedControl();
            this._freqHiger60 = new BEMN.Forms.LedControl();
            this.label312 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._q2Led = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._q1Led = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this._i8Io1 = new BEMN.Forms.LedControl();
            this._i81 = new BEMN.Forms.LedControl();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.label307 = new System.Windows.Forms.Label();
            this.label308 = new System.Windows.Forms.Label();
            this._i2i1IoLed = new BEMN.Forms.LedControl();
            this._i2i1Led = new BEMN.Forms.LedControl();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label301 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this._f4IoLessLed = new BEMN.Forms.LedControl();
            this._f3IoLessLed = new BEMN.Forms.LedControl();
            this._f2IoLessLed = new BEMN.Forms.LedControl();
            this._f4LessLed = new BEMN.Forms.LedControl();
            this.label303 = new System.Windows.Forms.Label();
            this._f1IoLessLed = new BEMN.Forms.LedControl();
            this._f3LessLed = new BEMN.Forms.LedControl();
            this.label304 = new System.Windows.Forms.Label();
            this._f2LessLed = new BEMN.Forms.LedControl();
            this.label305 = new System.Windows.Forms.Label();
            this._f1LessLed = new BEMN.Forms.LedControl();
            this.label306 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label295 = new System.Windows.Forms.Label();
            this.label296 = new System.Windows.Forms.Label();
            this._f4IoMoreLed = new BEMN.Forms.LedControl();
            this._f3IoMoreLed = new BEMN.Forms.LedControl();
            this._f2IoMoreLed = new BEMN.Forms.LedControl();
            this._f4MoreLed = new BEMN.Forms.LedControl();
            this.label297 = new System.Windows.Forms.Label();
            this._f1IoMoreLed = new BEMN.Forms.LedControl();
            this._f3MoreLed = new BEMN.Forms.LedControl();
            this.label298 = new System.Windows.Forms.Label();
            this._f2MoreLed = new BEMN.Forms.LedControl();
            this.label299 = new System.Windows.Forms.Label();
            this._f1MoreLed = new BEMN.Forms.LedControl();
            this.label300 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label289 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this._u4IoLessLed = new BEMN.Forms.LedControl();
            this._u3IoLessLed = new BEMN.Forms.LedControl();
            this._u2IoLessLed = new BEMN.Forms.LedControl();
            this._u4LessLed = new BEMN.Forms.LedControl();
            this.label291 = new System.Windows.Forms.Label();
            this._u1IoLessLed = new BEMN.Forms.LedControl();
            this._u3LessLed = new BEMN.Forms.LedControl();
            this.label292 = new System.Windows.Forms.Label();
            this._u2LessLed = new BEMN.Forms.LedControl();
            this.label293 = new System.Windows.Forms.Label();
            this._u1LessLed = new BEMN.Forms.LedControl();
            this.label294 = new System.Windows.Forms.Label();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this._u4IoMoreLed = new BEMN.Forms.LedControl();
            this._u3IoMoreLed = new BEMN.Forms.LedControl();
            this._u2IoMoreLed = new BEMN.Forms.LedControl();
            this._u4MoreLed = new BEMN.Forms.LedControl();
            this.label285 = new System.Windows.Forms.Label();
            this._u1IoMoreLed = new BEMN.Forms.LedControl();
            this._u3MoreLed = new BEMN.Forms.LedControl();
            this.label286 = new System.Windows.Forms.Label();
            this._u2MoreLed = new BEMN.Forms.LedControl();
            this.label287 = new System.Windows.Forms.Label();
            this._u1MoreLed = new BEMN.Forms.LedControl();
            this.label288 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label344 = new System.Windows.Forms.Label();
            this._faultSwitchOff = new BEMN.Forms.LedControl();
            this._faultMeasuringf = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.label201 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this._faultPass = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this._faultMeasuringU = new BEMN.Forms.LedControl();
            this.label204 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._currenGroupLabel = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._groupCombo = new System.Windows.Forms.ComboBox();
            this._switchGroupButton = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._switchOn = new BEMN.Forms.LedControl();
            this._switchOff = new BEMN.Forms.LedControl();
            this._startOscBtn = new System.Windows.Forms.Button();
            this._resetTermStateButton = new System.Windows.Forms.Button();
            this._faultTnInd = new BEMN.Forms.LedControl();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetFaultTnBtn = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this.label223 = new System.Windows.Forms.Label();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.BGS_Group = new System.Windows.Forms.GroupBox();
            this._bgs16 = new BEMN.Forms.LedControl();
            this.label352 = new System.Windows.Forms.Label();
            this._bgs15 = new BEMN.Forms.LedControl();
            this.label412 = new System.Windows.Forms.Label();
            this._bgs14 = new BEMN.Forms.LedControl();
            this.label454 = new System.Windows.Forms.Label();
            this._bgs13 = new BEMN.Forms.LedControl();
            this.label457 = new System.Windows.Forms.Label();
            this._bgs12 = new BEMN.Forms.LedControl();
            this.label458 = new System.Windows.Forms.Label();
            this._bgs11 = new BEMN.Forms.LedControl();
            this.label459 = new System.Windows.Forms.Label();
            this._bgs10 = new BEMN.Forms.LedControl();
            this.label460 = new System.Windows.Forms.Label();
            this._bgs9 = new BEMN.Forms.LedControl();
            this.label461 = new System.Windows.Forms.Label();
            this._bgs8 = new BEMN.Forms.LedControl();
            this.label462 = new System.Windows.Forms.Label();
            this._bgs7 = new BEMN.Forms.LedControl();
            this.label463 = new System.Windows.Forms.Label();
            this._bgs6 = new BEMN.Forms.LedControl();
            this.label464 = new System.Windows.Forms.Label();
            this._bgs5 = new BEMN.Forms.LedControl();
            this.label465 = new System.Windows.Forms.Label();
            this._bgs4 = new BEMN.Forms.LedControl();
            this.label466 = new System.Windows.Forms.Label();
            this._bgs3 = new BEMN.Forms.LedControl();
            this.label467 = new System.Windows.Forms.Label();
            this._bgs2 = new BEMN.Forms.LedControl();
            this.label468 = new System.Windows.Forms.Label();
            this._bgs1 = new BEMN.Forms.LedControl();
            this.label469 = new System.Windows.Forms.Label();
            this.label224 = new System.Windows.Forms.Label();
            this._ssl33 = new BEMN.Forms.LedControl();
            this.label470 = new System.Windows.Forms.Label();
            this._ssl34 = new BEMN.Forms.LedControl();
            this.label471 = new System.Windows.Forms.Label();
            this._ssl35 = new BEMN.Forms.LedControl();
            this.label472 = new System.Windows.Forms.Label();
            this._ssl36 = new BEMN.Forms.LedControl();
            this.label473 = new System.Windows.Forms.Label();
            this._ssl37 = new BEMN.Forms.LedControl();
            this.label474 = new System.Windows.Forms.Label();
            this._ssl38 = new BEMN.Forms.LedControl();
            this.label475 = new System.Windows.Forms.Label();
            this._ssl39 = new BEMN.Forms.LedControl();
            this.label476 = new System.Windows.Forms.Label();
            this._ssl40 = new BEMN.Forms.LedControl();
            this.label477 = new System.Windows.Forms.Label();
            this._ssl41 = new BEMN.Forms.LedControl();
            this.label478 = new System.Windows.Forms.Label();
            this._ssl42 = new BEMN.Forms.LedControl();
            this.label479 = new System.Windows.Forms.Label();
            this._ssl43 = new BEMN.Forms.LedControl();
            this.label480 = new System.Windows.Forms.Label();
            this._ssl44 = new BEMN.Forms.LedControl();
            this.label481 = new System.Windows.Forms.Label();
            this._ssl45 = new BEMN.Forms.LedControl();
            this.label482 = new System.Windows.Forms.Label();
            this._ssl46 = new BEMN.Forms.LedControl();
            this.label483 = new System.Windows.Forms.Label();
            this._ssl47 = new BEMN.Forms.LedControl();
            this.label484 = new System.Windows.Forms.Label();
            this._ssl48 = new BEMN.Forms.LedControl();
            _analogTabPage = new System.Windows.Forms.TabPage();
            _analogTabPage.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this.groupBox45.SuspendLayout();
            this.groupBox46.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox43.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._dataBaseTabControl.SuspendLayout();
            this._discretTabPage1.SuspendLayout();
            this.reversGroup.SuspendLayout();
            this.groupBox53.SuspendLayout();
            this.groupBox52.SuspendLayout();
            this.groupBox50.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.dugGroup.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox54.SuspendLayout();
            this.groupBox65.SuspendLayout();
            this.groupBox63.SuspendLayout();
            this.urovGroup.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.splGroupBox.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._discretTabPage2.SuspendLayout();
            this.reversGroup1.SuspendLayout();
            this.dugGroup1.SuspendLayout();
            this.groupBox66.SuspendLayout();
            this.groupBox64.SuspendLayout();
            this.urovGroup1.SuspendLayout();
            this.groupBox61.SuspendLayout();
            this.groupBox62.SuspendLayout();
            this.groupBox58.SuspendLayout();
            this.groupBox59.SuspendLayout();
            this.groupBox60.SuspendLayout();
            this.groupBox55.SuspendLayout();
            this.groupBox56.SuspendLayout();
            this.groupBox57.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.BGS_Group.SuspendLayout();
            this.SuspendLayout();
            // 
            // _analogTabPage
            // 
            _analogTabPage.Controls.Add(this._dateTimeControl);
            _analogTabPage.Controls.Add(this.groupBox47);
            _analogTabPage.Controls.Add(this.groupBox23);
            _analogTabPage.Controls.Add(this.groupBox13);
            _analogTabPage.Controls.Add(this.groupBox7);
            _analogTabPage.Controls.Add(this.groupBox3);
            _analogTabPage.Controls.Add(this.groupBox2);
            _analogTabPage.Controls.Add(this.groupBox5);
            _analogTabPage.Controls.Add(this.groupBox1);
            _analogTabPage.Location = new System.Drawing.Point(4, 22);
            _analogTabPage.Name = "_analogTabPage";
            _analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            _analogTabPage.Size = new System.Drawing.Size(933, 599);
            _analogTabPage.TabIndex = 0;
            _analogTabPage.Text = "Аналоговая БД";
            _analogTabPage.UseVisualStyleBackColor = true;
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(297, 333);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 49;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this.CN);
            this.groupBox47.Controls.Add(this.CA);
            this.groupBox47.Controls.Add(this.BN);
            this.groupBox47.Controls.Add(this.AN);
            this.groupBox47.Controls.Add(this.BC);
            this.groupBox47.Controls.Add(this.AB);
            this.groupBox47.Controls.Add(this.label340);
            this.groupBox47.Controls.Add(this.label339);
            this.groupBox47.Controls.Add(this.label338);
            this.groupBox47.Controls.Add(this.label337);
            this.groupBox47.Controls.Add(this.label336);
            this.groupBox47.Controls.Add(this.label335);
            this.groupBox47.Location = new System.Drawing.Point(818, 10);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(104, 193);
            this.groupBox47.TabIndex = 48;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Направления Z";
            // 
            // CN
            // 
            this.CN.BackColor = System.Drawing.SystemColors.Control;
            this.CN.Location = new System.Drawing.Point(29, 165);
            this.CN.Name = "CN";
            this.CN.Size = new System.Drawing.Size(51, 20);
            this.CN.TabIndex = 11;
            // 
            // CA
            // 
            this.CA.BackColor = System.Drawing.SystemColors.Control;
            this.CA.Location = new System.Drawing.Point(29, 79);
            this.CA.Name = "CA";
            this.CA.Size = new System.Drawing.Size(51, 20);
            this.CA.TabIndex = 10;
            // 
            // BN
            // 
            this.BN.BackColor = System.Drawing.SystemColors.Control;
            this.BN.Location = new System.Drawing.Point(29, 145);
            this.BN.Name = "BN";
            this.BN.Size = new System.Drawing.Size(51, 20);
            this.BN.TabIndex = 9;
            // 
            // AN
            // 
            this.AN.BackColor = System.Drawing.SystemColors.Control;
            this.AN.Location = new System.Drawing.Point(29, 125);
            this.AN.Name = "AN";
            this.AN.Size = new System.Drawing.Size(51, 20);
            this.AN.TabIndex = 8;
            // 
            // BC
            // 
            this.BC.BackColor = System.Drawing.SystemColors.Control;
            this.BC.Location = new System.Drawing.Point(29, 58);
            this.BC.Name = "BC";
            this.BC.Size = new System.Drawing.Size(51, 20);
            this.BC.TabIndex = 7;
            // 
            // AB
            // 
            this.AB.BackColor = System.Drawing.SystemColors.Control;
            this.AB.Location = new System.Drawing.Point(29, 38);
            this.AB.Name = "AB";
            this.AB.Size = new System.Drawing.Size(51, 20);
            this.AB.TabIndex = 6;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(1, 168);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(20, 13);
            this.label340.TabIndex = 5;
            this.label340.Text = "Zc";
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(1, 148);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(20, 13);
            this.label339.TabIndex = 4;
            this.label339.Text = "Zb";
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(1, 128);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(20, 13);
            this.label338.TabIndex = 3;
            this.label338.Text = "Za";
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(1, 81);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(26, 13);
            this.label337.TabIndex = 2;
            this.label337.Text = "Zca";
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(1, 61);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(26, 13);
            this.label336.TabIndex = 1;
            this.label336.Text = "Zbc";
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(1, 41);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(26, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "Zab";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._dU);
            this.groupBox23.Controls.Add(this.label17);
            this.groupBox23.Controls.Add(this._dFi);
            this.groupBox23.Controls.Add(this._dF);
            this.groupBox23.Controls.Add(this.label16);
            this.groupBox23.Controls.Add(this.label15);
            this.groupBox23.Location = new System.Drawing.Point(8, 336);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(283, 100);
            this.groupBox23.TabIndex = 47;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Контроль синхронизма";
            // 
            // _dU
            // 
            this._dU.Location = new System.Drawing.Point(171, 18);
            this._dU.Name = "_dU";
            this._dU.ReadOnly = true;
            this._dU.Size = new System.Drawing.Size(84, 20);
            this._dU.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Разность частот dF";
            // 
            // _dFi
            // 
            this._dFi.Location = new System.Drawing.Point(171, 44);
            this._dFi.Name = "_dFi";
            this._dFi.ReadOnly = true;
            this._dFi.Size = new System.Drawing.Size(84, 20);
            this._dFi.TabIndex = 11;
            // 
            // _dF
            // 
            this._dF.Location = new System.Drawing.Point(171, 70);
            this._dF.Name = "_dF";
            this._dF.ReadOnly = true;
            this._dF.Size = new System.Drawing.Size(84, 20);
            this._dF.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(161, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Разность фазовых сдвигов dfi";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Разность напряжений dU";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.groupBox35);
            this.groupBox13.Controls.Add(this.groupBox36);
            this.groupBox13.Controls.Add(this.groupBox37);
            this.groupBox13.Controls.Add(this.groupBox44);
            this.groupBox13.Controls.Add(this.groupBox45);
            this.groupBox13.Controls.Add(this.groupBox46);
            this.groupBox13.Location = new System.Drawing.Point(658, 10);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(154, 548);
            this.groupBox13.TabIndex = 46;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сопротивления, Ом перв.";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this._iZc5Box);
            this.groupBox35.Controls.Add(this._iZa5Box);
            this.groupBox35.Controls.Add(this._iZb5Box);
            this.groupBox35.Controls.Add(this.label99);
            this.groupBox35.Controls.Add(this.label103);
            this.groupBox35.Controls.Add(this.label108);
            this.groupBox35.Location = new System.Drawing.Point(6, 454);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(142, 87);
            this.groupBox35.TabIndex = 8;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Фазные N5";
            // 
            // _iZc5Box
            // 
            this._iZc5Box.Location = new System.Drawing.Point(47, 59);
            this._iZc5Box.Name = "_iZc5Box";
            this._iZc5Box.ReadOnly = true;
            this._iZc5Box.Size = new System.Drawing.Size(87, 20);
            this._iZc5Box.TabIndex = 1;
            // 
            // _iZa5Box
            // 
            this._iZa5Box.Location = new System.Drawing.Point(47, 19);
            this._iZa5Box.Name = "_iZa5Box";
            this._iZa5Box.ReadOnly = true;
            this._iZa5Box.Size = new System.Drawing.Size(87, 20);
            this._iZa5Box.TabIndex = 1;
            // 
            // _iZb5Box
            // 
            this._iZb5Box.Location = new System.Drawing.Point(47, 39);
            this._iZb5Box.Name = "_iZb5Box";
            this._iZb5Box.ReadOnly = true;
            this._iZb5Box.Size = new System.Drawing.Size(87, 20);
            this._iZb5Box.TabIndex = 1;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 62);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(35, 13);
            this.label99.TabIndex = 5;
            this.label99.Text = "Zc5 =";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 42);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(35, 13);
            this.label103.TabIndex = 5;
            this.label103.Text = "Zb5 =";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 22);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(35, 13);
            this.label108.TabIndex = 5;
            this.label108.Text = "Za5 =";
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._iZc4Box);
            this.groupBox36.Controls.Add(this._iZa4Box);
            this.groupBox36.Controls.Add(this._iZb4Box);
            this.groupBox36.Controls.Add(this.label109);
            this.groupBox36.Controls.Add(this.label135);
            this.groupBox36.Controls.Add(this.label136);
            this.groupBox36.Location = new System.Drawing.Point(6, 367);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(142, 87);
            this.groupBox36.TabIndex = 8;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Фазные N4";
            // 
            // _iZc4Box
            // 
            this._iZc4Box.Location = new System.Drawing.Point(47, 59);
            this._iZc4Box.Name = "_iZc4Box";
            this._iZc4Box.ReadOnly = true;
            this._iZc4Box.Size = new System.Drawing.Size(87, 20);
            this._iZc4Box.TabIndex = 1;
            // 
            // _iZa4Box
            // 
            this._iZa4Box.Location = new System.Drawing.Point(47, 19);
            this._iZa4Box.Name = "_iZa4Box";
            this._iZa4Box.ReadOnly = true;
            this._iZa4Box.Size = new System.Drawing.Size(87, 20);
            this._iZa4Box.TabIndex = 1;
            // 
            // _iZb4Box
            // 
            this._iZb4Box.Location = new System.Drawing.Point(47, 39);
            this._iZb4Box.Name = "_iZb4Box";
            this._iZb4Box.ReadOnly = true;
            this._iZb4Box.Size = new System.Drawing.Size(87, 20);
            this._iZb4Box.TabIndex = 1;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 62);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(35, 13);
            this.label109.TabIndex = 5;
            this.label109.Text = "Zc4 =";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(6, 42);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(35, 13);
            this.label135.TabIndex = 5;
            this.label135.Text = "Zb4 =";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(6, 22);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(35, 13);
            this.label136.TabIndex = 5;
            this.label136.Text = "Za4 =";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this._iZc3Box);
            this.groupBox37.Controls.Add(this._iZa3Box);
            this.groupBox37.Controls.Add(this._iZb3Box);
            this.groupBox37.Controls.Add(this.label143);
            this.groupBox37.Controls.Add(this.label144);
            this.groupBox37.Controls.Add(this.label145);
            this.groupBox37.Location = new System.Drawing.Point(6, 280);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(142, 87);
            this.groupBox37.TabIndex = 8;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Фазные N3";
            // 
            // _iZc3Box
            // 
            this._iZc3Box.Location = new System.Drawing.Point(47, 59);
            this._iZc3Box.Name = "_iZc3Box";
            this._iZc3Box.ReadOnly = true;
            this._iZc3Box.Size = new System.Drawing.Size(87, 20);
            this._iZc3Box.TabIndex = 1;
            // 
            // _iZa3Box
            // 
            this._iZa3Box.Location = new System.Drawing.Point(47, 19);
            this._iZa3Box.Name = "_iZa3Box";
            this._iZa3Box.ReadOnly = true;
            this._iZa3Box.Size = new System.Drawing.Size(87, 20);
            this._iZa3Box.TabIndex = 1;
            // 
            // _iZb3Box
            // 
            this._iZb3Box.Location = new System.Drawing.Point(47, 39);
            this._iZb3Box.Name = "_iZb3Box";
            this._iZb3Box.ReadOnly = true;
            this._iZb3Box.Size = new System.Drawing.Size(87, 20);
            this._iZb3Box.TabIndex = 1;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(6, 62);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(35, 13);
            this.label143.TabIndex = 5;
            this.label143.Text = "Zc3 =";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(6, 42);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(35, 13);
            this.label144.TabIndex = 5;
            this.label144.Text = "Zb3 =";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(6, 22);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(35, 13);
            this.label145.TabIndex = 5;
            this.label145.Text = "Za3 =";
            // 
            // groupBox44
            // 
            this.groupBox44.Controls.Add(this._iZc2Box);
            this.groupBox44.Controls.Add(this._iZa2Box);
            this.groupBox44.Controls.Add(this._iZb2Box);
            this.groupBox44.Controls.Add(this.label146);
            this.groupBox44.Controls.Add(this.label148);
            this.groupBox44.Controls.Add(this.label150);
            this.groupBox44.Location = new System.Drawing.Point(6, 193);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(142, 87);
            this.groupBox44.TabIndex = 8;
            this.groupBox44.TabStop = false;
            this.groupBox44.Text = "Фазные N2";
            // 
            // _iZc2Box
            // 
            this._iZc2Box.Location = new System.Drawing.Point(47, 59);
            this._iZc2Box.Name = "_iZc2Box";
            this._iZc2Box.ReadOnly = true;
            this._iZc2Box.Size = new System.Drawing.Size(87, 20);
            this._iZc2Box.TabIndex = 1;
            // 
            // _iZa2Box
            // 
            this._iZa2Box.Location = new System.Drawing.Point(47, 19);
            this._iZa2Box.Name = "_iZa2Box";
            this._iZa2Box.ReadOnly = true;
            this._iZa2Box.Size = new System.Drawing.Size(87, 20);
            this._iZa2Box.TabIndex = 1;
            // 
            // _iZb2Box
            // 
            this._iZb2Box.Location = new System.Drawing.Point(47, 39);
            this._iZb2Box.Name = "_iZb2Box";
            this._iZb2Box.ReadOnly = true;
            this._iZb2Box.Size = new System.Drawing.Size(87, 20);
            this._iZb2Box.TabIndex = 1;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(6, 62);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(35, 13);
            this.label146.TabIndex = 5;
            this.label146.Text = "Zc2 =";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(6, 42);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(35, 13);
            this.label148.TabIndex = 5;
            this.label148.Text = "Zb2 =";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(6, 22);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(35, 13);
            this.label150.TabIndex = 5;
            this.label150.Text = "Za2 =";
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this._iZa1Box);
            this.groupBox45.Controls.Add(this._iZc1Box);
            this.groupBox45.Controls.Add(this.label151);
            this.groupBox45.Controls.Add(this._iZb1Box);
            this.groupBox45.Controls.Add(this.label153);
            this.groupBox45.Controls.Add(this.label155);
            this.groupBox45.Location = new System.Drawing.Point(6, 106);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(142, 87);
            this.groupBox45.TabIndex = 8;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Фазные N1";
            // 
            // _iZa1Box
            // 
            this._iZa1Box.Location = new System.Drawing.Point(47, 19);
            this._iZa1Box.Name = "_iZa1Box";
            this._iZa1Box.ReadOnly = true;
            this._iZa1Box.Size = new System.Drawing.Size(87, 20);
            this._iZa1Box.TabIndex = 1;
            // 
            // _iZc1Box
            // 
            this._iZc1Box.Location = new System.Drawing.Point(47, 59);
            this._iZc1Box.Name = "_iZc1Box";
            this._iZc1Box.ReadOnly = true;
            this._iZc1Box.Size = new System.Drawing.Size(87, 20);
            this._iZc1Box.TabIndex = 1;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(6, 22);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(35, 13);
            this.label151.TabIndex = 5;
            this.label151.Text = "Za1 =";
            // 
            // _iZb1Box
            // 
            this._iZb1Box.Location = new System.Drawing.Point(47, 39);
            this._iZb1Box.Name = "_iZb1Box";
            this._iZb1Box.ReadOnly = true;
            this._iZb1Box.Size = new System.Drawing.Size(87, 20);
            this._iZb1Box.TabIndex = 1;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(6, 42);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(35, 13);
            this.label153.TabIndex = 5;
            this.label153.Text = "Zb1 =";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(6, 62);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(35, 13);
            this.label155.TabIndex = 5;
            this.label155.Text = "Zc1 =";
            // 
            // groupBox46
            // 
            this.groupBox46.Controls.Add(this._iZabBox);
            this.groupBox46.Controls.Add(this._iZcaBox);
            this.groupBox46.Controls.Add(this._iZbcBox);
            this.groupBox46.Controls.Add(this.label156);
            this.groupBox46.Controls.Add(this.label157);
            this.groupBox46.Controls.Add(this.label158);
            this.groupBox46.Controls.Add(this.label330);
            this.groupBox46.Controls.Add(this.label331);
            this.groupBox46.Controls.Add(this.label332);
            this.groupBox46.Location = new System.Drawing.Point(6, 19);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(142, 87);
            this.groupBox46.TabIndex = 8;
            this.groupBox46.TabStop = false;
            this.groupBox46.Text = "Межфазные";
            // 
            // _iZabBox
            // 
            this._iZabBox.Location = new System.Drawing.Point(47, 19);
            this._iZabBox.Name = "_iZabBox";
            this._iZabBox.ReadOnly = true;
            this._iZabBox.Size = new System.Drawing.Size(87, 20);
            this._iZabBox.TabIndex = 1;
            // 
            // _iZcaBox
            // 
            this._iZcaBox.Location = new System.Drawing.Point(47, 59);
            this._iZcaBox.Name = "_iZcaBox";
            this._iZcaBox.ReadOnly = true;
            this._iZcaBox.Size = new System.Drawing.Size(87, 20);
            this._iZcaBox.TabIndex = 1;
            // 
            // _iZbcBox
            // 
            this._iZbcBox.Location = new System.Drawing.Point(47, 39);
            this._iZbcBox.Name = "_iZbcBox";
            this._iZbcBox.ReadOnly = true;
            this._iZbcBox.Size = new System.Drawing.Size(87, 20);
            this._iZbcBox.TabIndex = 1;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(6, 22);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(35, 13);
            this.label156.TabIndex = 5;
            this.label156.Text = "Zab =";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(6, 42);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(35, 13);
            this.label157.TabIndex = 5;
            this.label157.Text = "Zbc =";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(6, 62);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(35, 13);
            this.label158.TabIndex = 5;
            this.label158.Text = "Zca =";
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(110, 74);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(0, 13);
            this.label330.TabIndex = 2;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(110, 48);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(0, 13);
            this.label331.TabIndex = 2;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(110, 22);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(0, 13);
            this.label332.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox43);
            this.groupBox7.Controls.Add(this.groupBox42);
            this.groupBox7.Controls.Add(this.groupBox41);
            this.groupBox7.Controls.Add(this.groupBox25);
            this.groupBox7.Controls.Add(this.groupBox24);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Location = new System.Drawing.Point(504, 10);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(154, 548);
            this.groupBox7.TabIndex = 46;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Сопротивления, Ом вт.";
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this._zc5Box);
            this.groupBox43.Controls.Add(this._za5Box);
            this.groupBox43.Controls.Add(this._zb5Box);
            this.groupBox43.Controls.Add(this.label327);
            this.groupBox43.Controls.Add(this.label328);
            this.groupBox43.Controls.Add(this.label329);
            this.groupBox43.Location = new System.Drawing.Point(6, 454);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Size = new System.Drawing.Size(142, 87);
            this.groupBox43.TabIndex = 8;
            this.groupBox43.TabStop = false;
            this.groupBox43.Text = "Фазные N5";
            // 
            // _zc5Box
            // 
            this._zc5Box.Location = new System.Drawing.Point(47, 59);
            this._zc5Box.Name = "_zc5Box";
            this._zc5Box.ReadOnly = true;
            this._zc5Box.Size = new System.Drawing.Size(87, 20);
            this._zc5Box.TabIndex = 1;
            // 
            // _za5Box
            // 
            this._za5Box.Location = new System.Drawing.Point(47, 19);
            this._za5Box.Name = "_za5Box";
            this._za5Box.ReadOnly = true;
            this._za5Box.Size = new System.Drawing.Size(87, 20);
            this._za5Box.TabIndex = 1;
            // 
            // _zb5Box
            // 
            this._zb5Box.Location = new System.Drawing.Point(47, 39);
            this._zb5Box.Name = "_zb5Box";
            this._zb5Box.ReadOnly = true;
            this._zb5Box.Size = new System.Drawing.Size(87, 20);
            this._zb5Box.TabIndex = 1;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(6, 62);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(35, 13);
            this.label327.TabIndex = 5;
            this.label327.Text = "Zc5 =";
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(6, 42);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(35, 13);
            this.label328.TabIndex = 5;
            this.label328.Text = "Zb5 =";
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 22);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(35, 13);
            this.label329.TabIndex = 5;
            this.label329.Text = "Za5 =";
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this._zc4Box);
            this.groupBox42.Controls.Add(this._za4Box);
            this.groupBox42.Controls.Add(this._zb4Box);
            this.groupBox42.Controls.Add(this.label324);
            this.groupBox42.Controls.Add(this.label325);
            this.groupBox42.Controls.Add(this.label326);
            this.groupBox42.Location = new System.Drawing.Point(6, 367);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(142, 87);
            this.groupBox42.TabIndex = 8;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Фазные N4";
            // 
            // _zc4Box
            // 
            this._zc4Box.Location = new System.Drawing.Point(47, 59);
            this._zc4Box.Name = "_zc4Box";
            this._zc4Box.ReadOnly = true;
            this._zc4Box.Size = new System.Drawing.Size(87, 20);
            this._zc4Box.TabIndex = 1;
            // 
            // _za4Box
            // 
            this._za4Box.Location = new System.Drawing.Point(47, 19);
            this._za4Box.Name = "_za4Box";
            this._za4Box.ReadOnly = true;
            this._za4Box.Size = new System.Drawing.Size(87, 20);
            this._za4Box.TabIndex = 1;
            // 
            // _zb4Box
            // 
            this._zb4Box.Location = new System.Drawing.Point(47, 39);
            this._zb4Box.Name = "_zb4Box";
            this._zb4Box.ReadOnly = true;
            this._zb4Box.Size = new System.Drawing.Size(87, 20);
            this._zb4Box.TabIndex = 1;
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(6, 62);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(35, 13);
            this.label324.TabIndex = 5;
            this.label324.Text = "Zc4 =";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(6, 42);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(35, 13);
            this.label325.TabIndex = 5;
            this.label325.Text = "Zb4 =";
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(6, 22);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(35, 13);
            this.label326.TabIndex = 5;
            this.label326.Text = "Za4 =";
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this._zc3Box);
            this.groupBox41.Controls.Add(this._za3Box);
            this.groupBox41.Controls.Add(this._zb3Box);
            this.groupBox41.Controls.Add(this.label321);
            this.groupBox41.Controls.Add(this.label322);
            this.groupBox41.Controls.Add(this.label323);
            this.groupBox41.Location = new System.Drawing.Point(6, 280);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(142, 87);
            this.groupBox41.TabIndex = 8;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "Фазные N3";
            // 
            // _zc3Box
            // 
            this._zc3Box.Location = new System.Drawing.Point(47, 59);
            this._zc3Box.Name = "_zc3Box";
            this._zc3Box.ReadOnly = true;
            this._zc3Box.Size = new System.Drawing.Size(87, 20);
            this._zc3Box.TabIndex = 1;
            // 
            // _za3Box
            // 
            this._za3Box.Location = new System.Drawing.Point(47, 19);
            this._za3Box.Name = "_za3Box";
            this._za3Box.ReadOnly = true;
            this._za3Box.Size = new System.Drawing.Size(87, 20);
            this._za3Box.TabIndex = 1;
            // 
            // _zb3Box
            // 
            this._zb3Box.Location = new System.Drawing.Point(47, 39);
            this._zb3Box.Name = "_zb3Box";
            this._zb3Box.ReadOnly = true;
            this._zb3Box.Size = new System.Drawing.Size(87, 20);
            this._zb3Box.TabIndex = 1;
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(6, 62);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(35, 13);
            this.label321.TabIndex = 5;
            this.label321.Text = "Zc3 =";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(6, 42);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(35, 13);
            this.label322.TabIndex = 5;
            this.label322.Text = "Zb3 =";
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(6, 22);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(35, 13);
            this.label323.TabIndex = 5;
            this.label323.Text = "Za3 =";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._zc2Box);
            this.groupBox25.Controls.Add(this._za2Box);
            this.groupBox25.Controls.Add(this._zb2Box);
            this.groupBox25.Controls.Add(this.label152);
            this.groupBox25.Controls.Add(this.label154);
            this.groupBox25.Controls.Add(this.label159);
            this.groupBox25.Location = new System.Drawing.Point(6, 193);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(142, 87);
            this.groupBox25.TabIndex = 8;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Фазные N2";
            // 
            // _zc2Box
            // 
            this._zc2Box.Location = new System.Drawing.Point(47, 59);
            this._zc2Box.Name = "_zc2Box";
            this._zc2Box.ReadOnly = true;
            this._zc2Box.Size = new System.Drawing.Size(87, 20);
            this._zc2Box.TabIndex = 1;
            // 
            // _za2Box
            // 
            this._za2Box.Location = new System.Drawing.Point(47, 19);
            this._za2Box.Name = "_za2Box";
            this._za2Box.ReadOnly = true;
            this._za2Box.Size = new System.Drawing.Size(87, 20);
            this._za2Box.TabIndex = 1;
            // 
            // _zb2Box
            // 
            this._zb2Box.Location = new System.Drawing.Point(47, 39);
            this._zb2Box.Name = "_zb2Box";
            this._zb2Box.ReadOnly = true;
            this._zb2Box.Size = new System.Drawing.Size(87, 20);
            this._zb2Box.TabIndex = 1;
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(6, 62);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(35, 13);
            this.label152.TabIndex = 5;
            this.label152.Text = "Zc2 =";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(6, 42);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(35, 13);
            this.label154.TabIndex = 5;
            this.label154.Text = "Zb2 =";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(6, 22);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(35, 13);
            this.label159.TabIndex = 5;
            this.label159.Text = "Za2 =";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._za1Box);
            this.groupBox24.Controls.Add(this._zc1Box);
            this.groupBox24.Controls.Add(this.label137);
            this.groupBox24.Controls.Add(this._zb1Box);
            this.groupBox24.Controls.Add(this.label147);
            this.groupBox24.Controls.Add(this.label149);
            this.groupBox24.Location = new System.Drawing.Point(6, 106);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(142, 87);
            this.groupBox24.TabIndex = 8;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Фазные N1";
            // 
            // _za1Box
            // 
            this._za1Box.Location = new System.Drawing.Point(47, 19);
            this._za1Box.Name = "_za1Box";
            this._za1Box.ReadOnly = true;
            this._za1Box.Size = new System.Drawing.Size(87, 20);
            this._za1Box.TabIndex = 1;
            // 
            // _zc1Box
            // 
            this._zc1Box.Location = new System.Drawing.Point(47, 59);
            this._zc1Box.Name = "_zc1Box";
            this._zc1Box.ReadOnly = true;
            this._zc1Box.Size = new System.Drawing.Size(87, 20);
            this._zc1Box.TabIndex = 1;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(6, 22);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(35, 13);
            this.label137.TabIndex = 5;
            this.label137.Text = "Za1 =";
            // 
            // _zb1Box
            // 
            this._zb1Box.Location = new System.Drawing.Point(47, 39);
            this._zb1Box.Name = "_zb1Box";
            this._zb1Box.ReadOnly = true;
            this._zb1Box.Size = new System.Drawing.Size(87, 20);
            this._zb1Box.TabIndex = 1;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(6, 42);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(35, 13);
            this.label147.TabIndex = 5;
            this.label147.Text = "Zb1 =";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(6, 62);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(35, 13);
            this.label149.TabIndex = 5;
            this.label149.Text = "Zc1 =";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._zabBox);
            this.groupBox8.Controls.Add(this._zcaBox);
            this.groupBox8.Controls.Add(this._zbcBox);
            this.groupBox8.Controls.Add(this.label106);
            this.groupBox8.Controls.Add(this.label107);
            this.groupBox8.Controls.Add(this.label110);
            this.groupBox8.Controls.Add(this.rxcaLabel);
            this.groupBox8.Controls.Add(this.rxbcLabel);
            this.groupBox8.Controls.Add(this.rxabLabel);
            this.groupBox8.Location = new System.Drawing.Point(6, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(142, 87);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Межфазные";
            // 
            // _zabBox
            // 
            this._zabBox.Location = new System.Drawing.Point(47, 19);
            this._zabBox.Name = "_zabBox";
            this._zabBox.ReadOnly = true;
            this._zabBox.Size = new System.Drawing.Size(87, 20);
            this._zabBox.TabIndex = 1;
            // 
            // _zcaBox
            // 
            this._zcaBox.Location = new System.Drawing.Point(47, 59);
            this._zcaBox.Name = "_zcaBox";
            this._zcaBox.ReadOnly = true;
            this._zcaBox.Size = new System.Drawing.Size(87, 20);
            this._zcaBox.TabIndex = 1;
            // 
            // _zbcBox
            // 
            this._zbcBox.Location = new System.Drawing.Point(47, 39);
            this._zbcBox.Name = "_zbcBox";
            this._zbcBox.ReadOnly = true;
            this._zbcBox.Size = new System.Drawing.Size(87, 20);
            this._zbcBox.TabIndex = 1;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 22);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(35, 13);
            this.label106.TabIndex = 5;
            this.label106.Text = "Zab =";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 42);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(35, 13);
            this.label107.TabIndex = 5;
            this.label107.Text = "Zbc =";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 62);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(35, 13);
            this.label110.TabIndex = 5;
            this.label110.Text = "Zca =";
            // 
            // rxcaLabel
            // 
            this.rxcaLabel.AutoSize = true;
            this.rxcaLabel.Location = new System.Drawing.Point(110, 74);
            this.rxcaLabel.Name = "rxcaLabel";
            this.rxcaLabel.Size = new System.Drawing.Size(0, 13);
            this.rxcaLabel.TabIndex = 2;
            // 
            // rxbcLabel
            // 
            this.rxbcLabel.AutoSize = true;
            this.rxbcLabel.Location = new System.Drawing.Point(110, 48);
            this.rxbcLabel.Name = "rxbcLabel";
            this.rxbcLabel.Size = new System.Drawing.Size(0, 13);
            this.rxbcLabel.TabIndex = 2;
            // 
            // rxabLabel
            // 
            this.rxabLabel.AutoSize = true;
            this.rxabLabel.Location = new System.Drawing.Point(110, 22);
            this.rxabLabel.Name = "rxabLabel";
            this.rxabLabel.Size = new System.Drawing.Size(0, 13);
            this.rxabLabel.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._nWarm);
            this.groupBox3.Controls.Add(this.label217);
            this.groupBox3.Controls.Add(this._nPusk);
            this.groupBox3.Controls.Add(this.label216);
            this.groupBox3.Controls.Add(this._qpTextBox);
            this.groupBox3.Controls.Add(this.label105);
            this.groupBox3.Location = new System.Drawing.Point(369, 141);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(129, 101);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Состояние двигателя";
            // 
            // _nWarm
            // 
            this._nWarm.Location = new System.Drawing.Point(50, 72);
            this._nWarm.Name = "_nWarm";
            this._nWarm.ReadOnly = true;
            this._nWarm.Size = new System.Drawing.Size(61, 20);
            this._nWarm.TabIndex = 1;
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(6, 75);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(35, 13);
            this.label217.TabIndex = 0;
            this.label217.Text = "Nгор.";
            // 
            // _nPusk
            // 
            this._nPusk.Location = new System.Drawing.Point(50, 51);
            this._nPusk.Name = "_nPusk";
            this._nPusk.ReadOnly = true;
            this._nPusk.Size = new System.Drawing.Size(61, 20);
            this._nPusk.TabIndex = 1;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(6, 54);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(38, 13);
            this.label216.TabIndex = 0;
            this.label216.Text = "Nпуск";
            // 
            // _qpTextBox
            // 
            this._qpTextBox.Location = new System.Drawing.Point(50, 30);
            this._qpTextBox.Name = "_qpTextBox";
            this._qpTextBox.ReadOnly = true;
            this._qpTextBox.Size = new System.Drawing.Size(61, 20);
            this._qpTextBox.TabIndex = 1;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 37);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(15, 13);
            this.label105.TabIndex = 0;
            this.label105.Text = "Q";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._cosfTextBox);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._qTextBox);
            this.groupBox2.Controls.Add(this.label95);
            this.groupBox2.Controls.Add(this._pTextBox);
            this.groupBox2.Controls.Add(this.label97);
            this.groupBox2.Location = new System.Drawing.Point(369, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(129, 125);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Мощности";
            // 
            // _cosfTextBox
            // 
            this._cosfTextBox.Location = new System.Drawing.Point(46, 71);
            this._cosfTextBox.Name = "_cosfTextBox";
            this._cosfTextBox.ReadOnly = true;
            this._cosfTextBox.Size = new System.Drawing.Size(61, 20);
            this._cosfTextBox.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "cos f";
            // 
            // _qTextBox
            // 
            this._qTextBox.Location = new System.Drawing.Point(46, 45);
            this._qTextBox.Name = "_qTextBox";
            this._qTextBox.ReadOnly = true;
            this._qTextBox.Size = new System.Drawing.Size(61, 20);
            this._qTextBox.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(10, 48);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(15, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Q";
            // 
            // _pTextBox
            // 
            this._pTextBox.Location = new System.Drawing.Point(46, 19);
            this._pTextBox.Name = "_pTextBox";
            this._pTextBox.ReadOnly = true;
            this._pTextBox.Size = new System.Drawing.Size(61, 20);
            this._pTextBox.TabIndex = 1;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(10, 22);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(14, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "P";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._fTextBox);
            this.groupBox5.Controls.Add(this._cU0);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this._u30);
            this.groupBox5.Controls.Add(this.label333);
            this.groupBox5.Controls.Add(this.label343);
            this.groupBox5.Controls.Add(this.label342);
            this.groupBox5.Controls.Add(this.label341);
            this.groupBox5.Controls.Add(this._cUca);
            this.groupBox5.Controls.Add(this._ucaTextBox);
            this.groupBox5.Controls.Add(this._cUbc);
            this.groupBox5.Controls.Add(this._ubcTextBox);
            this.groupBox5.Controls.Add(this._cUab);
            this.groupBox5.Controls.Add(this._uabTextBox);
            this.groupBox5.Controls.Add(this._cU2);
            this.groupBox5.Controls.Add(this._u2TextBox);
            this.groupBox5.Controls.Add(this._cU1);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this._u1TextBox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this._cUc);
            this.groupBox5.Controls.Add(this._ucTextBox);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this._cUb);
            this.groupBox5.Controls.Add(this._ubTextBox);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this._cUa);
            this.groupBox5.Controls.Add(this._uaTextBox);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(8, 169);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(355, 160);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Напряжения и частота";
            // 
            // _fTextBox
            // 
            this._fTextBox.Location = new System.Drawing.Point(193, 129);
            this._fTextBox.Name = "_fTextBox";
            this._fTextBox.ReadOnly = true;
            this._fTextBox.Size = new System.Drawing.Size(50, 20);
            this._fTextBox.TabIndex = 31;
            // 
            // _cU0
            // 
            this._cU0.Location = new System.Drawing.Point(249, 103);
            this._cU0.Name = "_cU0";
            this._cU0.ReadOnly = true;
            this._cU0.Size = new System.Drawing.Size(50, 20);
            this._cU0.TabIndex = 28;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(174, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "F";
            // 
            // _u30
            // 
            this._u30.Location = new System.Drawing.Point(193, 103);
            this._u30.Name = "_u30";
            this._u30.ReadOnly = true;
            this._u30.Size = new System.Drawing.Size(50, 20);
            this._u30.TabIndex = 29;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(160, 106);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(27, 13);
            this.label333.TabIndex = 27;
            this.label333.Text = "3U0";
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(160, 80);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(27, 13);
            this.label343.TabIndex = 26;
            this.label343.Text = "Uca";
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(160, 54);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(27, 13);
            this.label342.TabIndex = 25;
            this.label342.Text = "Ubc";
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(160, 28);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(27, 13);
            this.label341.TabIndex = 24;
            this.label341.Text = "Uab";
            // 
            // _cUca
            // 
            this._cUca.Location = new System.Drawing.Point(249, 77);
            this._cUca.Name = "_cUca";
            this._cUca.ReadOnly = true;
            this._cUca.Size = new System.Drawing.Size(50, 20);
            this._cUca.TabIndex = 23;
            // 
            // _ucaTextBox
            // 
            this._ucaTextBox.Location = new System.Drawing.Point(193, 77);
            this._ucaTextBox.Name = "_ucaTextBox";
            this._ucaTextBox.ReadOnly = true;
            this._ucaTextBox.Size = new System.Drawing.Size(50, 20);
            this._ucaTextBox.TabIndex = 23;
            // 
            // _cUbc
            // 
            this._cUbc.Location = new System.Drawing.Point(249, 51);
            this._cUbc.Name = "_cUbc";
            this._cUbc.ReadOnly = true;
            this._cUbc.Size = new System.Drawing.Size(50, 20);
            this._cUbc.TabIndex = 21;
            // 
            // _ubcTextBox
            // 
            this._ubcTextBox.Location = new System.Drawing.Point(193, 51);
            this._ubcTextBox.Name = "_ubcTextBox";
            this._ubcTextBox.ReadOnly = true;
            this._ubcTextBox.Size = new System.Drawing.Size(50, 20);
            this._ubcTextBox.TabIndex = 21;
            // 
            // _cUab
            // 
            this._cUab.Location = new System.Drawing.Point(249, 25);
            this._cUab.Name = "_cUab";
            this._cUab.ReadOnly = true;
            this._cUab.Size = new System.Drawing.Size(50, 20);
            this._cUab.TabIndex = 19;
            // 
            // _uabTextBox
            // 
            this._uabTextBox.Location = new System.Drawing.Point(193, 25);
            this._uabTextBox.Name = "_uabTextBox";
            this._uabTextBox.ReadOnly = true;
            this._uabTextBox.Size = new System.Drawing.Size(50, 20);
            this._uabTextBox.TabIndex = 19;
            // 
            // _cU2
            // 
            this._cU2.Location = new System.Drawing.Point(95, 129);
            this._cU2.Name = "_cU2";
            this._cU2.ReadOnly = true;
            this._cU2.Size = new System.Drawing.Size(50, 20);
            this._cU2.TabIndex = 11;
            // 
            // _u2TextBox
            // 
            this._u2TextBox.Location = new System.Drawing.Point(39, 129);
            this._u2TextBox.Name = "_u2TextBox";
            this._u2TextBox.ReadOnly = true;
            this._u2TextBox.Size = new System.Drawing.Size(50, 20);
            this._u2TextBox.TabIndex = 11;
            // 
            // _cU1
            // 
            this._cU1.Location = new System.Drawing.Point(95, 103);
            this._cU1.Name = "_cU1";
            this._cU1.ReadOnly = true;
            this._cU1.Size = new System.Drawing.Size(50, 20);
            this._cU1.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 132);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "U2";
            // 
            // _u1TextBox
            // 
            this._u1TextBox.Location = new System.Drawing.Point(39, 103);
            this._u1TextBox.Name = "_u1TextBox";
            this._u1TextBox.ReadOnly = true;
            this._u1TextBox.Size = new System.Drawing.Size(50, 20);
            this._u1TextBox.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "U1";
            // 
            // _cUc
            // 
            this._cUc.Location = new System.Drawing.Point(95, 77);
            this._cUc.Name = "_cUc";
            this._cUc.ReadOnly = true;
            this._cUc.Size = new System.Drawing.Size(50, 20);
            this._cUc.TabIndex = 7;
            // 
            // _ucTextBox
            // 
            this._ucTextBox.Location = new System.Drawing.Point(39, 77);
            this._ucTextBox.Name = "_ucTextBox";
            this._ucTextBox.ReadOnly = true;
            this._ucTextBox.Size = new System.Drawing.Size(50, 20);
            this._ucTextBox.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Uc";
            // 
            // _cUb
            // 
            this._cUb.Location = new System.Drawing.Point(95, 51);
            this._cUb.Name = "_cUb";
            this._cUb.ReadOnly = true;
            this._cUb.Size = new System.Drawing.Size(50, 20);
            this._cUb.TabIndex = 5;
            // 
            // _ubTextBox
            // 
            this._ubTextBox.Location = new System.Drawing.Point(39, 51);
            this._ubTextBox.Name = "_ubTextBox";
            this._ubTextBox.ReadOnly = true;
            this._ubTextBox.Size = new System.Drawing.Size(50, 20);
            this._ubTextBox.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ub";
            // 
            // _cUa
            // 
            this._cUa.Location = new System.Drawing.Point(95, 25);
            this._cUa.Name = "_cUa";
            this._cUa.ReadOnly = true;
            this._cUa.Size = new System.Drawing.Size(50, 20);
            this._cUa.TabIndex = 3;
            // 
            // _uaTextBox
            // 
            this._uaTextBox.Location = new System.Drawing.Point(39, 25);
            this._uaTextBox.Name = "_uaTextBox";
            this._uaTextBox.ReadOnly = true;
            this._uaTextBox.Size = new System.Drawing.Size(50, 20);
            this._uaTextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Ua";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._cI0);
            this.groupBox1.Controls.Add(this._i30);
            this.groupBox1.Controls.Add(this.label334);
            this.groupBox1.Controls.Add(this._cIc);
            this.groupBox1.Controls.Add(this._icTextBox);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this._igTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._cI2);
            this.groupBox1.Controls.Add(this._i2TextBox);
            this.groupBox1.Controls.Add(this._cIn1);
            this.groupBox1.Controls.Add(this._in1TextBox);
            this.groupBox1.Controls.Add(this._cIn);
            this.groupBox1.Controls.Add(this._inTextBox);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._cIb);
            this.groupBox1.Controls.Add(this._ibTextBox);
            this.groupBox1.Controls.Add(this._cI1);
            this.groupBox1.Controls.Add(this._i1TextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._cIa);
            this.groupBox1.Controls.Add(this._iaTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 153);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Токи";
            // 
            // _cI0
            // 
            this._cI0.Location = new System.Drawing.Point(249, 71);
            this._cI0.Name = "_cI0";
            this._cI0.ReadOnly = true;
            this._cI0.Size = new System.Drawing.Size(50, 20);
            this._cI0.TabIndex = 6;
            // 
            // _i30
            // 
            this._i30.Location = new System.Drawing.Point(193, 71);
            this._i30.Name = "_i30";
            this._i30.ReadOnly = true;
            this._i30.Size = new System.Drawing.Size(50, 20);
            this._i30.TabIndex = 6;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(166, 74);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(22, 13);
            this.label334.TabIndex = 4;
            this.label334.Text = "3I0";
            // 
            // _cIc
            // 
            this._cIc.Location = new System.Drawing.Point(95, 71);
            this._cIc.Name = "_cIc";
            this._cIc.ReadOnly = true;
            this._cIc.Size = new System.Drawing.Size(50, 20);
            this._cIc.TabIndex = 7;
            // 
            // _icTextBox
            // 
            this._icTextBox.Location = new System.Drawing.Point(39, 71);
            this._icTextBox.Name = "_icTextBox";
            this._icTextBox.ReadOnly = true;
            this._icTextBox.Size = new System.Drawing.Size(50, 20);
            this._icTextBox.TabIndex = 7;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(12, 74);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(16, 13);
            this.label80.TabIndex = 5;
            this.label80.Text = "Ic";
            // 
            // _igTextBox
            // 
            this._igTextBox.Location = new System.Drawing.Point(193, 97);
            this._igTextBox.Name = "_igTextBox";
            this._igTextBox.ReadOnly = true;
            this._igTextBox.Size = new System.Drawing.Size(50, 20);
            this._igTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(171, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Iг";
            // 
            // _cI2
            // 
            this._cI2.Location = new System.Drawing.Point(249, 45);
            this._cI2.Name = "_cI2";
            this._cI2.ReadOnly = true;
            this._cI2.Size = new System.Drawing.Size(50, 20);
            this._cI2.TabIndex = 3;
            // 
            // _i2TextBox
            // 
            this._i2TextBox.Location = new System.Drawing.Point(193, 45);
            this._i2TextBox.Name = "_i2TextBox";
            this._i2TextBox.ReadOnly = true;
            this._i2TextBox.Size = new System.Drawing.Size(50, 20);
            this._i2TextBox.TabIndex = 3;
            // 
            // _cIn1
            // 
            this._cIn1.Location = new System.Drawing.Point(95, 123);
            this._cIn1.Name = "_cIn1";
            this._cIn1.ReadOnly = true;
            this._cIn1.Size = new System.Drawing.Size(50, 20);
            this._cIn1.TabIndex = 1;
            // 
            // _in1TextBox
            // 
            this._in1TextBox.Location = new System.Drawing.Point(39, 123);
            this._in1TextBox.Name = "_in1TextBox";
            this._in1TextBox.ReadOnly = true;
            this._in1TextBox.Size = new System.Drawing.Size(50, 20);
            this._in1TextBox.TabIndex = 1;
            // 
            // _cIn
            // 
            this._cIn.Location = new System.Drawing.Point(95, 97);
            this._cIn.Name = "_cIn";
            this._cIn.ReadOnly = true;
            this._cIn.Size = new System.Drawing.Size(50, 20);
            this._cIn.TabIndex = 1;
            // 
            // _inTextBox
            // 
            this._inTextBox.Location = new System.Drawing.Point(39, 97);
            this._inTextBox.Name = "_inTextBox";
            this._inTextBox.ReadOnly = true;
            this._inTextBox.Size = new System.Drawing.Size(50, 20);
            this._inTextBox.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "In1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(171, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "I2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "In";
            // 
            // _cIb
            // 
            this._cIb.Location = new System.Drawing.Point(95, 45);
            this._cIb.Name = "_cIb";
            this._cIb.ReadOnly = true;
            this._cIb.Size = new System.Drawing.Size(50, 20);
            this._cIb.TabIndex = 3;
            // 
            // _ibTextBox
            // 
            this._ibTextBox.Location = new System.Drawing.Point(39, 45);
            this._ibTextBox.Name = "_ibTextBox";
            this._ibTextBox.ReadOnly = true;
            this._ibTextBox.Size = new System.Drawing.Size(50, 20);
            this._ibTextBox.TabIndex = 3;
            // 
            // _cI1
            // 
            this._cI1.Location = new System.Drawing.Point(249, 19);
            this._cI1.Name = "_cI1";
            this._cI1.ReadOnly = true;
            this._cI1.Size = new System.Drawing.Size(50, 20);
            this._cI1.TabIndex = 1;
            // 
            // _i1TextBox
            // 
            this._i1TextBox.Location = new System.Drawing.Point(193, 19);
            this._i1TextBox.Name = "_i1TextBox";
            this._i1TextBox.ReadOnly = true;
            this._i1TextBox.Size = new System.Drawing.Size(50, 20);
            this._i1TextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ib";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(171, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "I1";
            // 
            // _cIa
            // 
            this._cIa.Location = new System.Drawing.Point(95, 19);
            this._cIa.Name = "_cIa";
            this._cIa.ReadOnly = true;
            this._cIa.Size = new System.Drawing.Size(50, 20);
            this._cIa.TabIndex = 1;
            // 
            // _iaTextBox
            // 
            this._iaTextBox.Location = new System.Drawing.Point(39, 19);
            this._iaTextBox.Name = "_iaTextBox";
            this._iaTextBox.ReadOnly = true;
            this._iaTextBox.Size = new System.Drawing.Size(50, 20);
            this._iaTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(_analogTabPage);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage1);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage2);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(941, 625);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // _discretTabPage1
            // 
            this._discretTabPage1.Controls.Add(this.BGS_Group);
            this._discretTabPage1.Controls.Add(this.reversGroup);
            this._discretTabPage1.Controls.Add(this.groupBox53);
            this._discretTabPage1.Controls.Add(this.groupBox52);
            this._discretTabPage1.Controls.Add(this.groupBox50);
            this._discretTabPage1.Controls.Add(this.groupBox51);
            this._discretTabPage1.Controls.Add(this.groupBox22);
            this._discretTabPage1.Controls.Add(this.dugGroup);
            this._discretTabPage1.Controls.Add(this.groupBox20);
            this._discretTabPage1.Controls.Add(this.groupBox54);
            this._discretTabPage1.Controls.Add(this.groupBox65);
            this._discretTabPage1.Controls.Add(this.groupBox63);
            this._discretTabPage1.Controls.Add(this.urovGroup);
            this._discretTabPage1.Controls.Add(this.groupBox14);
            this._discretTabPage1.Controls.Add(this.groupBox26);
            this._discretTabPage1.Controls.Add(this.groupBox11);
            this._discretTabPage1.Controls.Add(this.splGroupBox);
            this._discretTabPage1.Controls.Add(this.groupBox18);
            this._discretTabPage1.Controls.Add(this.groupBox17);
            this._discretTabPage1.Controls.Add(this.groupBox10);
            this._discretTabPage1.Controls.Add(this.groupBox9);
            this._discretTabPage1.Controls.Add(this.groupBox6);
            this._discretTabPage1.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage1.Name = "_discretTabPage1";
            this._discretTabPage1.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage1.Size = new System.Drawing.Size(933, 599);
            this._discretTabPage1.TabIndex = 1;
            this._discretTabPage1.Text = "Основные сигналы";
            this._discretTabPage1.UseVisualStyleBackColor = true;
            // 
            // reversGroup
            // 
            this.reversGroup.Controls.Add(this.label448);
            this.reversGroup.Controls.Add(this.label449);
            this.reversGroup.Controls.Add(this._P2Io);
            this.reversGroup.Controls.Add(this._P1Io);
            this.reversGroup.Controls.Add(this._P2);
            this.reversGroup.Controls.Add(this.label455);
            this.reversGroup.Controls.Add(this._P1);
            this.reversGroup.Controls.Add(this.label456);
            this.reversGroup.Location = new System.Drawing.Point(679, 522);
            this.reversGroup.Name = "reversGroup";
            this.reversGroup.Size = new System.Drawing.Size(84, 65);
            this.reversGroup.TabIndex = 66;
            this.reversGroup.TabStop = false;
            this.reversGroup.Text = "Защиты P";
            // 
            // label448
            // 
            this.label448.AutoSize = true;
            this.label448.ForeColor = System.Drawing.Color.Blue;
            this.label448.Location = new System.Drawing.Point(25, 15);
            this.label448.Name = "label448";
            this.label448.Size = new System.Drawing.Size(32, 13);
            this.label448.TabIndex = 65;
            this.label448.Text = "Сраб";
            // 
            // label449
            // 
            this.label449.AutoSize = true;
            this.label449.ForeColor = System.Drawing.Color.Red;
            this.label449.Location = new System.Drawing.Point(6, 15);
            this.label449.Name = "label449";
            this.label449.Size = new System.Drawing.Size(23, 13);
            this.label449.TabIndex = 64;
            this.label449.Text = "ИО";
            // 
            // _P2Io
            // 
            this._P2Io.Location = new System.Drawing.Point(9, 47);
            this._P2Io.Name = "_P2Io";
            this._P2Io.Size = new System.Drawing.Size(13, 13);
            this._P2Io.State = BEMN.Forms.LedState.Off;
            this._P2Io.TabIndex = 3;
            // 
            // _P1Io
            // 
            this._P1Io.Location = new System.Drawing.Point(9, 32);
            this._P1Io.Name = "_P1Io";
            this._P1Io.Size = new System.Drawing.Size(13, 13);
            this._P1Io.State = BEMN.Forms.LedState.Off;
            this._P1Io.TabIndex = 1;
            // 
            // _P2
            // 
            this._P2.Location = new System.Drawing.Point(28, 47);
            this._P2.Name = "_P2";
            this._P2.Size = new System.Drawing.Size(13, 13);
            this._P2.State = BEMN.Forms.LedState.Off;
            this._P2.TabIndex = 3;
            // 
            // label455
            // 
            this.label455.AutoSize = true;
            this.label455.Location = new System.Drawing.Point(47, 47);
            this.label455.Name = "label455";
            this.label455.Size = new System.Drawing.Size(23, 13);
            this.label455.TabIndex = 2;
            this.label455.Text = "P 2";
            // 
            // _P1
            // 
            this._P1.Location = new System.Drawing.Point(28, 32);
            this._P1.Name = "_P1";
            this._P1.Size = new System.Drawing.Size(13, 13);
            this._P1.State = BEMN.Forms.LedState.Off;
            this._P1.TabIndex = 1;
            // 
            // label456
            // 
            this.label456.AutoSize = true;
            this.label456.Location = new System.Drawing.Point(47, 32);
            this.label456.Name = "label456";
            this.label456.Size = new System.Drawing.Size(23, 13);
            this.label456.TabIndex = 0;
            this.label456.Text = "P 1";
            // 
            // groupBox53
            // 
            this.groupBox53.Controls.Add(this._damageA);
            this.groupBox53.Controls.Add(this.label219);
            this.groupBox53.Controls.Add(this.label367);
            this.groupBox53.Controls.Add(this._damageB);
            this.groupBox53.Controls.Add(this._damageC);
            this.groupBox53.Controls.Add(this.label370);
            this.groupBox53.Location = new System.Drawing.Point(769, 158);
            this.groupBox53.Name = "groupBox53";
            this.groupBox53.Size = new System.Drawing.Size(152, 79);
            this.groupBox53.TabIndex = 41;
            this.groupBox53.TabStop = false;
            this.groupBox53.Text = "Определение повр. фазы";
            // 
            // _damageA
            // 
            this._damageA.Location = new System.Drawing.Point(6, 19);
            this._damageA.Name = "_damageA";
            this._damageA.Size = new System.Drawing.Size(13, 13);
            this._damageA.State = BEMN.Forms.LedState.Off;
            this._damageA.TabIndex = 13;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(25, 20);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(90, 13);
            this.label219.TabIndex = 30;
            this.label219.Text = "Повр. по фазе А";
            // 
            // label367
            // 
            this.label367.AutoSize = true;
            this.label367.Location = new System.Drawing.Point(25, 38);
            this.label367.Name = "label367";
            this.label367.Size = new System.Drawing.Size(90, 13);
            this.label367.TabIndex = 32;
            this.label367.Text = "Повр. по фазе В";
            // 
            // _damageB
            // 
            this._damageB.Location = new System.Drawing.Point(6, 38);
            this._damageB.Name = "_damageB";
            this._damageB.Size = new System.Drawing.Size(13, 13);
            this._damageB.State = BEMN.Forms.LedState.Off;
            this._damageB.TabIndex = 33;
            // 
            // _damageC
            // 
            this._damageC.Location = new System.Drawing.Point(6, 57);
            this._damageC.Name = "_damageC";
            this._damageC.Size = new System.Drawing.Size(13, 13);
            this._damageC.State = BEMN.Forms.LedState.Off;
            this._damageC.TabIndex = 35;
            // 
            // label370
            // 
            this.label370.AutoSize = true;
            this.label370.Location = new System.Drawing.Point(25, 57);
            this.label370.Name = "label370";
            this.label370.Size = new System.Drawing.Size(90, 13);
            this.label370.TabIndex = 34;
            this.label370.Text = "Повр. по фазе С";
            // 
            // groupBox52
            // 
            this.groupBox52.Controls.Add(this._OnKsAndYppN);
            this.groupBox52.Controls.Add(this.label361);
            this.groupBox52.Controls.Add(this._UnoUno);
            this.groupBox52.Controls.Add(this.label362);
            this.groupBox52.Controls.Add(this._UyUno);
            this.groupBox52.Controls.Add(this.label363);
            this.groupBox52.Controls.Add(this._US);
            this.groupBox52.Controls.Add(this._U1noU2y);
            this.groupBox52.Controls.Add(this.label364);
            this.groupBox52.Controls.Add(this.label365);
            this.groupBox52.Controls.Add(this._YS);
            this.groupBox52.Controls.Add(this._OS);
            this.groupBox52.Controls.Add(this.label368);
            this.groupBox52.Controls.Add(this._autoSinchr);
            this.groupBox52.Location = new System.Drawing.Point(430, 370);
            this.groupBox52.Name = "groupBox52";
            this.groupBox52.Size = new System.Drawing.Size(135, 191);
            this.groupBox52.TabIndex = 40;
            this.groupBox52.TabStop = false;
            this.groupBox52.Text = "Сигналы КС и УППН";
            // 
            // _OnKsAndYppN
            // 
            this._OnKsAndYppN.Location = new System.Drawing.Point(6, 132);
            this._OnKsAndYppN.Name = "_OnKsAndYppN";
            this._OnKsAndYppN.Size = new System.Drawing.Size(13, 13);
            this._OnKsAndYppN.State = BEMN.Forms.LedState.Off;
            this._OnKsAndYppN.TabIndex = 39;
            // 
            // label361
            // 
            this.label361.AutoSize = true;
            this.label361.Location = new System.Drawing.Point(25, 132);
            this.label361.Name = "label361";
            this.label361.Size = new System.Drawing.Size(105, 13);
            this.label361.TabIndex = 38;
            this.label361.Text = "Вкл. по КС и УППН";
            // 
            // _UnoUno
            // 
            this._UnoUno.Location = new System.Drawing.Point(6, 75);
            this._UnoUno.Name = "_UnoUno";
            this._UnoUno.Size = new System.Drawing.Size(13, 13);
            this._UnoUno.State = BEMN.Forms.LedState.Off;
            this._UnoUno.TabIndex = 37;
            // 
            // label362
            // 
            this.label362.AutoSize = true;
            this.label362.Location = new System.Drawing.Point(25, 75);
            this.label362.Name = "label362";
            this.label362.Size = new System.Drawing.Size(75, 13);
            this.label362.TabIndex = 36;
            this.label362.Text = "U1нет, U2нет";
            // 
            // _UyUno
            // 
            this._UyUno.Location = new System.Drawing.Point(6, 56);
            this._UyUno.Name = "_UyUno";
            this._UyUno.Size = new System.Drawing.Size(13, 13);
            this._UyUno.State = BEMN.Forms.LedState.Off;
            this._UyUno.TabIndex = 35;
            // 
            // label363
            // 
            this.label363.AutoSize = true;
            this.label363.Location = new System.Drawing.Point(25, 56);
            this.label363.Name = "label363";
            this.label363.Size = new System.Drawing.Size(81, 13);
            this.label363.TabIndex = 34;
            this.label363.Text = "U1есть, U2нет";
            // 
            // _US
            // 
            this._US.Location = new System.Drawing.Point(6, 113);
            this._US.Name = "_US";
            this._US.Size = new System.Drawing.Size(13, 13);
            this._US.State = BEMN.Forms.LedState.Off;
            this._US.TabIndex = 29;
            // 
            // _U1noU2y
            // 
            this._U1noU2y.Location = new System.Drawing.Point(6, 38);
            this._U1noU2y.Name = "_U1noU2y";
            this._U1noU2y.Size = new System.Drawing.Size(13, 13);
            this._U1noU2y.State = BEMN.Forms.LedState.Off;
            this._U1noU2y.TabIndex = 33;
            // 
            // label364
            // 
            this.label364.AutoSize = true;
            this.label364.Location = new System.Drawing.Point(25, 38);
            this.label364.Name = "label364";
            this.label364.Size = new System.Drawing.Size(81, 13);
            this.label364.TabIndex = 32;
            this.label364.Text = "U1нет, U2есть";
            // 
            // label365
            // 
            this.label365.AutoSize = true;
            this.label365.Location = new System.Drawing.Point(25, 19);
            this.label365.Name = "label365";
            this.label365.Size = new System.Drawing.Size(90, 13);
            this.label365.TabIndex = 30;
            this.label365.Text = "Автомат. режим";
            // 
            // _YS
            // 
            this._YS.AutoSize = true;
            this._YS.Location = new System.Drawing.Point(25, 113);
            this._YS.Name = "_YS";
            this._YS.Size = new System.Drawing.Size(69, 13);
            this._YS.TabIndex = 28;
            this._YS.Text = "Условия УС";
            // 
            // _OS
            // 
            this._OS.Location = new System.Drawing.Point(6, 94);
            this._OS.Name = "_OS";
            this._OS.Size = new System.Drawing.Size(13, 13);
            this._OS.State = BEMN.Forms.LedState.Off;
            this._OS.TabIndex = 11;
            // 
            // label368
            // 
            this.label368.AutoSize = true;
            this.label368.Location = new System.Drawing.Point(25, 94);
            this.label368.Name = "label368";
            this.label368.Size = new System.Drawing.Size(69, 13);
            this.label368.TabIndex = 10;
            this.label368.Text = "Условия ОС";
            // 
            // _autoSinchr
            // 
            this._autoSinchr.Location = new System.Drawing.Point(6, 19);
            this._autoSinchr.Name = "_autoSinchr";
            this._autoSinchr.Size = new System.Drawing.Size(13, 13);
            this._autoSinchr.State = BEMN.Forms.LedState.Off;
            this._autoSinchr.TabIndex = 13;
            // 
            // groupBox50
            // 
            this.groupBox50.Controls.Add(this._readyApv);
            this.groupBox50.Controls.Add(this.label360);
            this.groupBox50.Controls.Add(this._blockApv);
            this.groupBox50.Controls.Add(this.label356);
            this.groupBox50.Controls.Add(this.label222);
            this.groupBox50.Controls.Add(this._puskApv);
            this.groupBox50.Controls.Add(this._krat4);
            this.groupBox50.Controls.Add(this.label359);
            this.groupBox50.Controls.Add(this.label353);
            this.groupBox50.Controls.Add(this._turnOnApv);
            this.groupBox50.Controls.Add(this._krat3);
            this.groupBox50.Controls.Add(this.label358);
            this.groupBox50.Controls.Add(this.label354);
            this.groupBox50.Controls.Add(this._krat1);
            this.groupBox50.Controls.Add(this._zapretApv);
            this.groupBox50.Controls.Add(this.label357);
            this.groupBox50.Controls.Add(this._krat2);
            this.groupBox50.Controls.Add(this.label355);
            this.groupBox50.Location = new System.Drawing.Point(299, 370);
            this.groupBox50.Name = "groupBox50";
            this.groupBox50.Size = new System.Drawing.Size(125, 191);
            this.groupBox50.TabIndex = 32;
            this.groupBox50.TabStop = false;
            this.groupBox50.Text = "Сигналы АПВ";
            // 
            // _readyApv
            // 
            this._readyApv.Location = new System.Drawing.Point(8, 170);
            this._readyApv.Name = "_readyApv";
            this._readyApv.Size = new System.Drawing.Size(13, 13);
            this._readyApv.State = BEMN.Forms.LedState.Off;
            this._readyApv.TabIndex = 57;
            // 
            // label360
            // 
            this.label360.AutoSize = true;
            this.label360.Location = new System.Drawing.Point(27, 170);
            this.label360.Name = "label360";
            this.label360.Size = new System.Drawing.Size(65, 13);
            this.label360.TabIndex = 56;
            this.label360.Text = "Готовность";
            // 
            // _blockApv
            // 
            this._blockApv.Location = new System.Drawing.Point(8, 151);
            this._blockApv.Name = "_blockApv";
            this._blockApv.Size = new System.Drawing.Size(13, 13);
            this._blockApv.State = BEMN.Forms.LedState.Off;
            this._blockApv.TabIndex = 55;
            // 
            // label356
            // 
            this.label356.AutoSize = true;
            this.label356.Location = new System.Drawing.Point(27, 18);
            this.label356.Name = "label356";
            this.label356.Size = new System.Drawing.Size(32, 13);
            this.label356.TabIndex = 47;
            this.label356.Text = "Пуск";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(27, 151);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(68, 13);
            this.label222.TabIndex = 54;
            this.label222.Text = "Блокировка";
            // 
            // _puskApv
            // 
            this._puskApv.Location = new System.Drawing.Point(8, 18);
            this._puskApv.Name = "_puskApv";
            this._puskApv.Size = new System.Drawing.Size(13, 13);
            this._puskApv.State = BEMN.Forms.LedState.Off;
            this._puskApv.TabIndex = 42;
            // 
            // _krat4
            // 
            this._krat4.Location = new System.Drawing.Point(8, 94);
            this._krat4.Name = "_krat4";
            this._krat4.Size = new System.Drawing.Size(13, 13);
            this._krat4.State = BEMN.Forms.LedState.Off;
            this._krat4.TabIndex = 53;
            // 
            // label359
            // 
            this.label359.AutoSize = true;
            this.label359.Location = new System.Drawing.Point(27, 113);
            this.label359.Name = "label359";
            this.label359.Size = new System.Drawing.Size(96, 13);
            this.label359.TabIndex = 40;
            this.label359.Text = "Включить по АПВ";
            // 
            // label353
            // 
            this.label353.AutoSize = true;
            this.label353.Location = new System.Drawing.Point(27, 94);
            this.label353.Name = "label353";
            this.label353.Size = new System.Drawing.Size(39, 13);
            this.label353.TabIndex = 52;
            this.label353.Text = "4 крат";
            // 
            // _turnOnApv
            // 
            this._turnOnApv.Location = new System.Drawing.Point(8, 113);
            this._turnOnApv.Name = "_turnOnApv";
            this._turnOnApv.Size = new System.Drawing.Size(13, 13);
            this._turnOnApv.State = BEMN.Forms.LedState.Off;
            this._turnOnApv.TabIndex = 41;
            // 
            // _krat3
            // 
            this._krat3.Location = new System.Drawing.Point(8, 75);
            this._krat3.Name = "_krat3";
            this._krat3.Size = new System.Drawing.Size(13, 13);
            this._krat3.State = BEMN.Forms.LedState.Off;
            this._krat3.TabIndex = 51;
            // 
            // label358
            // 
            this.label358.AutoSize = true;
            this.label358.Location = new System.Drawing.Point(27, 37);
            this.label358.Name = "label358";
            this.label358.Size = new System.Drawing.Size(39, 13);
            this.label358.TabIndex = 44;
            this.label358.Text = "1 крат";
            // 
            // label354
            // 
            this.label354.AutoSize = true;
            this.label354.Location = new System.Drawing.Point(27, 75);
            this.label354.Name = "label354";
            this.label354.Size = new System.Drawing.Size(39, 13);
            this.label354.TabIndex = 50;
            this.label354.Text = "3 крат";
            // 
            // _krat1
            // 
            this._krat1.Location = new System.Drawing.Point(8, 37);
            this._krat1.Name = "_krat1";
            this._krat1.Size = new System.Drawing.Size(13, 13);
            this._krat1.State = BEMN.Forms.LedState.Off;
            this._krat1.TabIndex = 46;
            // 
            // _zapretApv
            // 
            this._zapretApv.Location = new System.Drawing.Point(8, 132);
            this._zapretApv.Name = "_zapretApv";
            this._zapretApv.Size = new System.Drawing.Size(13, 13);
            this._zapretApv.State = BEMN.Forms.LedState.Off;
            this._zapretApv.TabIndex = 45;
            // 
            // label357
            // 
            this.label357.AutoSize = true;
            this.label357.Location = new System.Drawing.Point(27, 132);
            this.label357.Name = "label357";
            this.label357.Size = new System.Drawing.Size(43, 13);
            this.label357.TabIndex = 43;
            this.label357.Text = "Запрет";
            // 
            // _krat2
            // 
            this._krat2.Location = new System.Drawing.Point(8, 56);
            this._krat2.Name = "_krat2";
            this._krat2.Size = new System.Drawing.Size(13, 13);
            this._krat2.State = BEMN.Forms.LedState.Off;
            this._krat2.TabIndex = 49;
            // 
            // label355
            // 
            this.label355.AutoSize = true;
            this.label355.Location = new System.Drawing.Point(27, 56);
            this.label355.Name = "label355";
            this.label355.Size = new System.Drawing.Size(39, 13);
            this.label355.TabIndex = 48;
            this.label355.Text = "2 крат";
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._faultObruvFaz);
            this.groupBox51.Controls.Add(this.label351);
            this.groupBox51.Controls.Add(this._faultTH3u0);
            this.groupBox51.Controls.Add(this.label350);
            this.groupBox51.Controls.Add(this._faultTHU2);
            this.groupBox51.Controls.Add(this.label349);
            this.groupBox51.Controls.Add(this.label348);
            this.groupBox51.Controls.Add(this._faultTNmgn);
            this.groupBox51.Controls.Add(this.label218);
            this.groupBox51.Controls.Add(this._faultUabc);
            this.groupBox51.Controls.Add(this.label221);
            this.groupBox51.Controls.Add(this._faultTN);
            this.groupBox51.Location = new System.Drawing.Point(769, 6);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(152, 146);
            this.groupBox51.TabIndex = 31;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Неисправности измерения U";
            // 
            // _faultObruvFaz
            // 
            this._faultObruvFaz.Location = new System.Drawing.Point(6, 107);
            this._faultObruvFaz.Name = "_faultObruvFaz";
            this._faultObruvFaz.Size = new System.Drawing.Size(13, 13);
            this._faultObruvFaz.State = BEMN.Forms.LedState.Off;
            this._faultObruvFaz.TabIndex = 37;
            // 
            // label351
            // 
            this.label351.AutoSize = true;
            this.label351.Location = new System.Drawing.Point(25, 107);
            this.label351.Name = "label351";
            this.label351.Size = new System.Drawing.Size(105, 13);
            this.label351.TabIndex = 36;
            this.label351.Text = "ТН обрыв трёх фаз";
            // 
            // _faultTH3u0
            // 
            this._faultTH3u0.Location = new System.Drawing.Point(6, 88);
            this._faultTH3u0.Name = "_faultTH3u0";
            this._faultTH3u0.Size = new System.Drawing.Size(13, 13);
            this._faultTH3u0.State = BEMN.Forms.LedState.Off;
            this._faultTH3u0.TabIndex = 35;
            // 
            // label350
            // 
            this.label350.AutoSize = true;
            this.label350.Location = new System.Drawing.Point(25, 88);
            this.label350.Name = "label350";
            this.label350.Size = new System.Drawing.Size(60, 13);
            this.label350.TabIndex = 34;
            this.label350.Text = "ТН по 3U0";
            // 
            // _faultTHU2
            // 
            this._faultTHU2.Location = new System.Drawing.Point(6, 69);
            this._faultTHU2.Name = "_faultTHU2";
            this._faultTHU2.Size = new System.Drawing.Size(13, 13);
            this._faultTHU2.State = BEMN.Forms.LedState.Off;
            this._faultTHU2.TabIndex = 33;
            // 
            // label349
            // 
            this.label349.AutoSize = true;
            this.label349.Location = new System.Drawing.Point(25, 69);
            this.label349.Name = "label349";
            this.label349.Size = new System.Drawing.Size(54, 13);
            this.label349.TabIndex = 32;
            this.label349.Text = "ТН по U2";
            // 
            // label348
            // 
            this.label348.AutoSize = true;
            this.label348.Location = new System.Drawing.Point(25, 31);
            this.label348.Name = "label348";
            this.label348.Size = new System.Drawing.Size(119, 13);
            this.label348.TabIndex = 30;
            this.label348.Text = "ТН с задержкой и с/п";
            // 
            // _faultTNmgn
            // 
            this._faultTNmgn.Location = new System.Drawing.Point(6, 50);
            this._faultTNmgn.Name = "_faultTNmgn";
            this._faultTNmgn.Size = new System.Drawing.Size(13, 13);
            this._faultTNmgn.State = BEMN.Forms.LedState.Off;
            this._faultTNmgn.TabIndex = 29;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(25, 50);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(47, 13);
            this.label218.TabIndex = 28;
            this.label218.Text = "ТН мгн.";
            // 
            // _faultUabc
            // 
            this._faultUabc.Location = new System.Drawing.Point(6, 126);
            this._faultUabc.Name = "_faultUabc";
            this._faultUabc.Size = new System.Drawing.Size(13, 13);
            this._faultUabc.State = BEMN.Forms.LedState.Off;
            this._faultUabc.TabIndex = 11;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(25, 126);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(70, 13);
            this.label221.TabIndex = 10;
            this.label221.Text = "Внешняя ТН";
            // 
            // _faultTN
            // 
            this._faultTN.Location = new System.Drawing.Point(6, 31);
            this._faultTN.Name = "_faultTN";
            this._faultTN.Size = new System.Drawing.Size(13, 13);
            this._faultTN.State = BEMN.Forms.LedState.Off;
            this._faultTN.TabIndex = 13;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label211);
            this.groupBox22.Controls.Add(this.label209);
            this.groupBox22.Controls.Add(this.label215);
            this.groupBox22.Controls.Add(this.label191);
            this.groupBox22.Controls.Add(this._r4IoLed);
            this.groupBox22.Controls.Add(this._r5Led);
            this.groupBox22.Controls.Add(this._r1Led);
            this.groupBox22.Controls.Add(this._r3IoLed);
            this.groupBox22.Controls.Add(this.label214);
            this.groupBox22.Controls.Add(this.label199);
            this.groupBox22.Controls.Add(this._r6IoLed);
            this.groupBox22.Controls.Add(this._r2IoLed);
            this.groupBox22.Controls.Add(this._r6Led);
            this.groupBox22.Controls.Add(this._r2Led);
            this.groupBox22.Controls.Add(this._r4Led);
            this.groupBox22.Controls.Add(this.label207);
            this.groupBox22.Controls.Add(this.label208);
            this.groupBox22.Controls.Add(this._r3Led);
            this.groupBox22.Controls.Add(this._r5IoLed);
            this.groupBox22.Controls.Add(this._r1IoLed);
            this.groupBox22.Location = new System.Drawing.Point(679, 6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(84, 146);
            this.groupBox22.TabIndex = 26;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Защиты Z";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.ForeColor = System.Drawing.Color.Blue;
            this.label211.Location = new System.Drawing.Point(25, 12);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(32, 13);
            this.label211.TabIndex = 83;
            this.label211.Text = "Сраб";
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.ForeColor = System.Drawing.Color.Red;
            this.label209.Location = new System.Drawing.Point(6, 12);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(23, 13);
            this.label209.TabIndex = 82;
            this.label209.Text = "ИО";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(47, 108);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(23, 13);
            this.label215.TabIndex = 70;
            this.label215.Text = "Z 5";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(47, 32);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(23, 13);
            this.label191.TabIndex = 70;
            this.label191.Text = "Z 1";
            // 
            // _r4IoLed
            // 
            this._r4IoLed.Location = new System.Drawing.Point(9, 89);
            this._r4IoLed.Name = "_r4IoLed";
            this._r4IoLed.Size = new System.Drawing.Size(13, 13);
            this._r4IoLed.State = BEMN.Forms.LedState.Off;
            this._r4IoLed.TabIndex = 80;
            // 
            // _r5Led
            // 
            this._r5Led.Location = new System.Drawing.Point(28, 108);
            this._r5Led.Name = "_r5Led";
            this._r5Led.Size = new System.Drawing.Size(13, 13);
            this._r5Led.State = BEMN.Forms.LedState.Off;
            this._r5Led.TabIndex = 72;
            // 
            // _r1Led
            // 
            this._r1Led.Location = new System.Drawing.Point(28, 32);
            this._r1Led.Name = "_r1Led";
            this._r1Led.Size = new System.Drawing.Size(13, 13);
            this._r1Led.State = BEMN.Forms.LedState.Off;
            this._r1Led.TabIndex = 72;
            // 
            // _r3IoLed
            // 
            this._r3IoLed.Location = new System.Drawing.Point(9, 70);
            this._r3IoLed.Name = "_r3IoLed";
            this._r3IoLed.Size = new System.Drawing.Size(13, 13);
            this._r3IoLed.State = BEMN.Forms.LedState.Off;
            this._r3IoLed.TabIndex = 77;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(47, 127);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(23, 13);
            this.label214.TabIndex = 73;
            this.label214.Text = "Z 6";
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(47, 51);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(23, 13);
            this.label199.TabIndex = 73;
            this.label199.Text = "Z 2";
            // 
            // _r6IoLed
            // 
            this._r6IoLed.Location = new System.Drawing.Point(9, 127);
            this._r6IoLed.Name = "_r6IoLed";
            this._r6IoLed.Size = new System.Drawing.Size(13, 13);
            this._r6IoLed.State = BEMN.Forms.LedState.Off;
            this._r6IoLed.TabIndex = 74;
            // 
            // _r2IoLed
            // 
            this._r2IoLed.Location = new System.Drawing.Point(9, 51);
            this._r2IoLed.Name = "_r2IoLed";
            this._r2IoLed.Size = new System.Drawing.Size(13, 13);
            this._r2IoLed.State = BEMN.Forms.LedState.Off;
            this._r2IoLed.TabIndex = 74;
            // 
            // _r6Led
            // 
            this._r6Led.Location = new System.Drawing.Point(28, 127);
            this._r6Led.Name = "_r6Led";
            this._r6Led.Size = new System.Drawing.Size(13, 13);
            this._r6Led.State = BEMN.Forms.LedState.Off;
            this._r6Led.TabIndex = 75;
            // 
            // _r2Led
            // 
            this._r2Led.Location = new System.Drawing.Point(28, 51);
            this._r2Led.Name = "_r2Led";
            this._r2Led.Size = new System.Drawing.Size(13, 13);
            this._r2Led.State = BEMN.Forms.LedState.Off;
            this._r2Led.TabIndex = 75;
            // 
            // _r4Led
            // 
            this._r4Led.Location = new System.Drawing.Point(28, 89);
            this._r4Led.Name = "_r4Led";
            this._r4Led.Size = new System.Drawing.Size(13, 13);
            this._r4Led.State = BEMN.Forms.LedState.Off;
            this._r4Led.TabIndex = 81;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(47, 70);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(23, 13);
            this.label207.TabIndex = 76;
            this.label207.Text = "Z 3";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(48, 89);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(23, 13);
            this.label208.TabIndex = 79;
            this.label208.Text = "Z 4";
            // 
            // _r3Led
            // 
            this._r3Led.Location = new System.Drawing.Point(28, 70);
            this._r3Led.Name = "_r3Led";
            this._r3Led.Size = new System.Drawing.Size(13, 13);
            this._r3Led.State = BEMN.Forms.LedState.Off;
            this._r3Led.TabIndex = 78;
            // 
            // _r5IoLed
            // 
            this._r5IoLed.Location = new System.Drawing.Point(9, 108);
            this._r5IoLed.Name = "_r5IoLed";
            this._r5IoLed.Size = new System.Drawing.Size(13, 13);
            this._r5IoLed.State = BEMN.Forms.LedState.Off;
            this._r5IoLed.TabIndex = 71;
            // 
            // _r1IoLed
            // 
            this._r1IoLed.Location = new System.Drawing.Point(9, 32);
            this._r1IoLed.Name = "_r1IoLed";
            this._r1IoLed.Size = new System.Drawing.Size(13, 13);
            this._r1IoLed.State = BEMN.Forms.LedState.Off;
            this._r1IoLed.TabIndex = 71;
            // 
            // dugGroup
            // 
            this.dugGroup.Controls.Add(this.dugPusk);
            this.dugGroup.Controls.Add(this.label446);
            this.dugGroup.Location = new System.Drawing.Point(769, 324);
            this.dugGroup.Name = "dugGroup";
            this.dugGroup.Size = new System.Drawing.Size(152, 40);
            this.dugGroup.TabIndex = 24;
            this.dugGroup.TabStop = false;
            // 
            // dugPusk
            // 
            this.dugPusk.Location = new System.Drawing.Point(6, 15);
            this.dugPusk.Name = "dugPusk";
            this.dugPusk.Size = new System.Drawing.Size(13, 13);
            this.dugPusk.State = BEMN.Forms.LedState.Off;
            this.dugPusk.TabIndex = 37;
            // 
            // label446
            // 
            this.label446.AutoSize = true;
            this.label446.Location = new System.Drawing.Point(25, 15);
            this.label446.Name = "label446";
            this.label446.Size = new System.Drawing.Size(118, 13);
            this.label446.TabIndex = 36;
            this.label446.Text = "Пуск дуговой защиты";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._kachanie);
            this.groupBox20.Controls.Add(this.label371);
            this.groupBox20.Controls.Add(this._inZone);
            this.groupBox20.Controls.Add(this.label373);
            this.groupBox20.Controls.Add(this._outZone);
            this.groupBox20.Controls.Add(this.label374);
            this.groupBox20.Location = new System.Drawing.Point(769, 243);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(152, 80);
            this.groupBox20.TabIndex = 24;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Сигналы качаний";
            // 
            // _kachanie
            // 
            this._kachanie.Location = new System.Drawing.Point(6, 19);
            this._kachanie.Name = "_kachanie";
            this._kachanie.Size = new System.Drawing.Size(13, 13);
            this._kachanie.State = BEMN.Forms.LedState.Off;
            this._kachanie.TabIndex = 37;
            // 
            // label371
            // 
            this.label371.AutoSize = true;
            this.label371.Location = new System.Drawing.Point(25, 19);
            this.label371.Name = "label371";
            this.label371.Size = new System.Drawing.Size(49, 13);
            this.label371.TabIndex = 36;
            this.label371.Text = "Качание";
            // 
            // _inZone
            // 
            this._inZone.Location = new System.Drawing.Point(6, 57);
            this._inZone.Name = "_inZone";
            this._inZone.Size = new System.Drawing.Size(13, 13);
            this._inZone.State = BEMN.Forms.LedState.Off;
            this._inZone.TabIndex = 31;
            // 
            // label373
            // 
            this.label373.AutoSize = true;
            this.label373.Location = new System.Drawing.Point(25, 57);
            this.label373.Name = "label373";
            this.label373.Size = new System.Drawing.Size(93, 13);
            this.label373.TabIndex = 30;
            this.label373.Text = "Внутренняя зона";
            // 
            // _outZone
            // 
            this._outZone.Location = new System.Drawing.Point(6, 38);
            this._outZone.Name = "_outZone";
            this._outZone.Size = new System.Drawing.Size(13, 13);
            this._outZone.State = BEMN.Forms.LedState.Off;
            this._outZone.TabIndex = 27;
            // 
            // label374
            // 
            this.label374.AutoSize = true;
            this.label374.Location = new System.Drawing.Point(25, 38);
            this.label374.Name = "label374";
            this.label374.Size = new System.Drawing.Size(79, 13);
            this.label374.TabIndex = 26;
            this.label374.Text = "Внешняя зона";
            // 
            // groupBox54
            // 
            this.groupBox54.Controls.Add(this._faultHardware1);
            this.groupBox54.Controls.Add(this.label372);
            this.groupBox54.Controls.Add(this._faultLogic1);
            this.groupBox54.Controls.Add(this.label12);
            this.groupBox54.Controls.Add(this._faultSwitchOff1);
            this.groupBox54.Controls.Add(this.label378);
            this.groupBox54.Controls.Add(this._faultMeasuringF1);
            this.groupBox54.Controls.Add(this.label375);
            this.groupBox54.Controls.Add(this._faultMeasuringU1);
            this.groupBox54.Controls.Add(this.label376);
            this.groupBox54.Controls.Add(this._faultSoftware1);
            this.groupBox54.Controls.Add(this.label377);
            this.groupBox54.Location = new System.Drawing.Point(530, 182);
            this.groupBox54.Name = "groupBox54";
            this.groupBox54.Size = new System.Drawing.Size(112, 134);
            this.groupBox54.TabIndex = 24;
            this.groupBox54.TabStop = false;
            this.groupBox54.Text = "Неисправности";
            // 
            // _faultHardware1
            // 
            this._faultHardware1.Location = new System.Drawing.Point(6, 19);
            this._faultHardware1.Name = "_faultHardware1";
            this._faultHardware1.Size = new System.Drawing.Size(13, 13);
            this._faultHardware1.State = BEMN.Forms.LedState.Off;
            this._faultHardware1.TabIndex = 37;
            // 
            // label372
            // 
            this.label372.AutoSize = true;
            this.label372.Location = new System.Drawing.Point(25, 19);
            this.label372.Name = "label372";
            this.label372.Size = new System.Drawing.Size(67, 13);
            this.label372.TabIndex = 36;
            this.label372.Text = "Аппаратная";
            // 
            // _faultLogic1
            // 
            this._faultLogic1.Location = new System.Drawing.Point(6, 114);
            this._faultLogic1.Name = "_faultLogic1";
            this._faultLogic1.Size = new System.Drawing.Size(13, 13);
            this._faultLogic1.State = BEMN.Forms.LedState.Off;
            this._faultLogic1.TabIndex = 35;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Логики";
            // 
            // _faultSwitchOff1
            // 
            this._faultSwitchOff1.Location = new System.Drawing.Point(6, 95);
            this._faultSwitchOff1.Name = "_faultSwitchOff1";
            this._faultSwitchOff1.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff1.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff1.TabIndex = 35;
            // 
            // label378
            // 
            this.label378.AutoSize = true;
            this.label378.Location = new System.Drawing.Point(25, 95);
            this.label378.Name = "label378";
            this.label378.Size = new System.Drawing.Size(76, 13);
            this.label378.TabIndex = 34;
            this.label378.Text = "Выключателя";
            // 
            // _faultMeasuringF1
            // 
            this._faultMeasuringF1.Location = new System.Drawing.Point(6, 76);
            this._faultMeasuringF1.Name = "_faultMeasuringF1";
            this._faultMeasuringF1.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringF1.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringF1.TabIndex = 35;
            // 
            // label375
            // 
            this.label375.AutoSize = true;
            this.label375.Location = new System.Drawing.Point(25, 76);
            this.label375.Name = "label375";
            this.label375.Size = new System.Drawing.Size(74, 13);
            this.label375.TabIndex = 34;
            this.label375.Text = "Измерения F";
            // 
            // _faultMeasuringU1
            // 
            this._faultMeasuringU1.Location = new System.Drawing.Point(6, 57);
            this._faultMeasuringU1.Name = "_faultMeasuringU1";
            this._faultMeasuringU1.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringU1.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringU1.TabIndex = 31;
            // 
            // label376
            // 
            this.label376.AutoSize = true;
            this.label376.Location = new System.Drawing.Point(25, 57);
            this.label376.Name = "label376";
            this.label376.Size = new System.Drawing.Size(76, 13);
            this.label376.TabIndex = 30;
            this.label376.Text = "Измерения U";
            // 
            // _faultSoftware1
            // 
            this._faultSoftware1.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware1.Name = "_faultSoftware1";
            this._faultSoftware1.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware1.State = BEMN.Forms.LedState.Off;
            this._faultSoftware1.TabIndex = 27;
            // 
            // label377
            // 
            this.label377.AutoSize = true;
            this.label377.Location = new System.Drawing.Point(25, 38);
            this.label377.Name = "label377";
            this.label377.Size = new System.Drawing.Size(78, 13);
            this.label377.TabIndex = 26;
            this.label377.Text = "Программная";
            // 
            // groupBox65
            // 
            this.groupBox65.Controls.Add(this.dvPusk);
            this.groupBox65.Controls.Add(this.label389);
            this.groupBox65.Controls.Add(this.dvBlockN);
            this.groupBox65.Controls.Add(this.dvBlockQ);
            this.groupBox65.Controls.Add(this.label441);
            this.groupBox65.Controls.Add(this.label439);
            this.groupBox65.Controls.Add(this.dvWork);
            this.groupBox65.Controls.Add(this.label440);
            this.groupBox65.Location = new System.Drawing.Point(571, 457);
            this.groupBox65.Name = "groupBox65";
            this.groupBox65.Size = new System.Drawing.Size(102, 95);
            this.groupBox65.TabIndex = 24;
            this.groupBox65.TabStop = false;
            this.groupBox65.Text = "Двигатель";
            // 
            // dvPusk
            // 
            this.dvPusk.Location = new System.Drawing.Point(6, 19);
            this.dvPusk.Name = "dvPusk";
            this.dvPusk.Size = new System.Drawing.Size(13, 13);
            this.dvPusk.State = BEMN.Forms.LedState.Off;
            this.dvPusk.TabIndex = 37;
            // 
            // label389
            // 
            this.label389.AutoSize = true;
            this.label389.Location = new System.Drawing.Point(25, 19);
            this.label389.Name = "label389";
            this.label389.Size = new System.Drawing.Size(32, 13);
            this.label389.TabIndex = 36;
            this.label389.Text = "Пуск";
            // 
            // dvBlockN
            // 
            this.dvBlockN.Location = new System.Drawing.Point(6, 76);
            this.dvBlockN.Name = "dvBlockN";
            this.dvBlockN.Size = new System.Drawing.Size(13, 13);
            this.dvBlockN.State = BEMN.Forms.LedState.Off;
            this.dvBlockN.TabIndex = 35;
            // 
            // dvBlockQ
            // 
            this.dvBlockQ.Location = new System.Drawing.Point(6, 57);
            this.dvBlockQ.Name = "dvBlockQ";
            this.dvBlockQ.Size = new System.Drawing.Size(13, 13);
            this.dvBlockQ.State = BEMN.Forms.LedState.Off;
            this.dvBlockQ.TabIndex = 35;
            // 
            // label441
            // 
            this.label441.AutoSize = true;
            this.label441.Location = new System.Drawing.Point(25, 76);
            this.label441.Name = "label441";
            this.label441.Size = new System.Drawing.Size(61, 13);
            this.label441.TabIndex = 34;
            this.label441.Text = "Блок. по N";
            // 
            // label439
            // 
            this.label439.AutoSize = true;
            this.label439.Location = new System.Drawing.Point(25, 57);
            this.label439.Name = "label439";
            this.label439.Size = new System.Drawing.Size(61, 13);
            this.label439.TabIndex = 34;
            this.label439.Text = "Блок. по Q";
            // 
            // dvWork
            // 
            this.dvWork.Location = new System.Drawing.Point(6, 38);
            this.dvWork.Name = "dvWork";
            this.dvWork.Size = new System.Drawing.Size(13, 13);
            this.dvWork.State = BEMN.Forms.LedState.Off;
            this.dvWork.TabIndex = 31;
            // 
            // label440
            // 
            this.label440.AutoSize = true;
            this.label440.Location = new System.Drawing.Point(25, 38);
            this.label440.Name = "label440";
            this.label440.Size = new System.Drawing.Size(43, 13);
            this.label440.TabIndex = 30;
            this.label440.Text = "Работа";
            // 
            // groupBox63
            // 
            this.groupBox63.Controls.Add(this.avrOn);
            this.groupBox63.Controls.Add(this.label160);
            this.groupBox63.Controls.Add(this.avrBlock);
            this.groupBox63.Controls.Add(this.label212);
            this.groupBox63.Controls.Add(this.avrOff);
            this.groupBox63.Controls.Add(this.label213);
            this.groupBox63.Location = new System.Drawing.Point(571, 370);
            this.groupBox63.Name = "groupBox63";
            this.groupBox63.Size = new System.Drawing.Size(104, 81);
            this.groupBox63.TabIndex = 24;
            this.groupBox63.TabStop = false;
            this.groupBox63.Text = "АВР";
            // 
            // avrOn
            // 
            this.avrOn.Location = new System.Drawing.Point(6, 19);
            this.avrOn.Name = "avrOn";
            this.avrOn.Size = new System.Drawing.Size(13, 13);
            this.avrOn.State = BEMN.Forms.LedState.Off;
            this.avrOn.TabIndex = 37;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(25, 19);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(52, 13);
            this.label160.TabIndex = 36;
            this.label160.Text = "АВР вкл.";
            // 
            // avrBlock
            // 
            this.avrBlock.Location = new System.Drawing.Point(6, 57);
            this.avrBlock.Name = "avrBlock";
            this.avrBlock.Size = new System.Drawing.Size(13, 13);
            this.avrBlock.State = BEMN.Forms.LedState.Off;
            this.avrBlock.TabIndex = 35;
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(25, 57);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(58, 13);
            this.label212.TabIndex = 34;
            this.label212.Text = "АВР блок.";
            // 
            // avrOff
            // 
            this.avrOff.Location = new System.Drawing.Point(6, 38);
            this.avrOff.Name = "avrOff";
            this.avrOff.Size = new System.Drawing.Size(13, 13);
            this.avrOff.State = BEMN.Forms.LedState.Off;
            this.avrOff.TabIndex = 31;
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(25, 38);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(57, 13);
            this.label213.TabIndex = 30;
            this.label213.Text = "АВР откл.";
            // 
            // urovGroup
            // 
            this.urovGroup.Controls.Add(this.urov1Led);
            this.urovGroup.Controls.Add(this.label318);
            this.urovGroup.Controls.Add(this.blockUrovLed);
            this.urovGroup.Controls.Add(this.label401);
            this.urovGroup.Controls.Add(this.urov2Led);
            this.urovGroup.Controls.Add(this.label415);
            this.urovGroup.Location = new System.Drawing.Point(389, 269);
            this.urovGroup.Name = "urovGroup";
            this.urovGroup.Size = new System.Drawing.Size(135, 95);
            this.urovGroup.TabIndex = 24;
            this.urovGroup.TabStop = false;
            this.urovGroup.Text = "УРОВ";
            // 
            // urov1Led
            // 
            this.urov1Led.Location = new System.Drawing.Point(6, 19);
            this.urov1Led.Name = "urov1Led";
            this.urov1Led.Size = new System.Drawing.Size(13, 13);
            this.urov1Led.State = BEMN.Forms.LedState.Off;
            this.urov1Led.TabIndex = 37;
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(25, 19);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(43, 13);
            this.label318.TabIndex = 36;
            this.label318.Text = "УРОВ1";
            // 
            // blockUrovLed
            // 
            this.blockUrovLed.Location = new System.Drawing.Point(6, 57);
            this.blockUrovLed.Name = "blockUrovLed";
            this.blockUrovLed.Size = new System.Drawing.Size(13, 13);
            this.blockUrovLed.State = BEMN.Forms.LedState.Off;
            this.blockUrovLed.TabIndex = 35;
            // 
            // label401
            // 
            this.label401.AutoSize = true;
            this.label401.Location = new System.Drawing.Point(25, 57);
            this.label401.Name = "label401";
            this.label401.Size = new System.Drawing.Size(68, 13);
            this.label401.TabIndex = 34;
            this.label401.Text = "Блок. УРОВ";
            // 
            // urov2Led
            // 
            this.urov2Led.Location = new System.Drawing.Point(6, 38);
            this.urov2Led.Name = "urov2Led";
            this.urov2Led.Size = new System.Drawing.Size(13, 13);
            this.urov2Led.State = BEMN.Forms.LedState.Off;
            this.urov2Led.TabIndex = 31;
            // 
            // label415
            // 
            this.label415.AutoSize = true;
            this.label415.Location = new System.Drawing.Point(25, 38);
            this.label415.Name = "label415";
            this.label415.Size = new System.Drawing.Size(43, 13);
            this.label415.TabIndex = 30;
            this.label415.Text = "УРОВ2";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._fault);
            this.groupBox14.Controls.Add(this.label210);
            this.groupBox14.Controls.Add(this._faultOff);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Controls.Add(this._acceleration);
            this.groupBox14.Controls.Add(this._auto4Led);
            this.groupBox14.Location = new System.Drawing.Point(388, 182);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(135, 81);
            this.groupBox14.TabIndex = 24;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Общие сигналы";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 19);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 37;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 19);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 36;
            this.label210.Text = "Неисправность";
            // 
            // _faultOff
            // 
            this._faultOff.Location = new System.Drawing.Point(6, 57);
            this._faultOff.Name = "_faultOff";
            this._faultOff.Size = new System.Drawing.Size(13, 13);
            this._faultOff.State = BEMN.Forms.LedState.Off;
            this._faultOff.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Авар. отключение";
            // 
            // _acceleration
            // 
            this._acceleration.Location = new System.Drawing.Point(6, 38);
            this._acceleration.Name = "_acceleration";
            this._acceleration.Size = new System.Drawing.Size(13, 13);
            this._acceleration.State = BEMN.Forms.LedState.Off;
            this._acceleration.TabIndex = 31;
            // 
            // _auto4Led
            // 
            this._auto4Led.AutoSize = true;
            this._auto4Led.Location = new System.Drawing.Point(25, 38);
            this._auto4Led.Name = "_auto4Led";
            this._auto4Led.Size = new System.Drawing.Size(81, 13);
            this._auto4Led.TabIndex = 30;
            this._auto4Led.Text = "Ускор. по вкл.";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label413);
            this.groupBox26.Controls.Add(this._iS8);
            this.groupBox26.Controls.Add(this._iS8Io);
            this.groupBox26.Controls.Add(this.label88);
            this.groupBox26.Controls.Add(this._iS7);
            this.groupBox26.Controls.Add(this._iS7Io);
            this.groupBox26.Controls.Add(this.label229);
            this.groupBox26.Controls.Add(this.label86);
            this.groupBox26.Controls.Add(this._iS1);
            this.groupBox26.Controls.Add(this.label85);
            this.groupBox26.Controls.Add(this._iS2);
            this.groupBox26.Controls.Add(this._iS1Io);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this._iS3);
            this.groupBox26.Controls.Add(this._iS2Io);
            this.groupBox26.Controls.Add(this.label230);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this._iS4);
            this.groupBox26.Controls.Add(this._iS3Io);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this._iS5);
            this.groupBox26.Controls.Add(this._iS4Io);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._iS6);
            this.groupBox26.Controls.Add(this._iS5Io);
            this.groupBox26.Controls.Add(this._iS6Io);
            this.groupBox26.Location = new System.Drawing.Point(679, 341);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(84, 175);
            this.groupBox26.TabIndex = 17;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Защиты I*";
            // 
            // label413
            // 
            this.label413.AutoSize = true;
            this.label413.Location = new System.Drawing.Point(47, 153);
            this.label413.Name = "label413";
            this.label413.Size = new System.Drawing.Size(26, 13);
            this.label413.TabIndex = 71;
            this.label413.Text = "I*>8";
            // 
            // _iS8
            // 
            this._iS8.Location = new System.Drawing.Point(28, 153);
            this._iS8.Name = "_iS8";
            this._iS8.Size = new System.Drawing.Size(13, 13);
            this._iS8.State = BEMN.Forms.LedState.Off;
            this._iS8.TabIndex = 72;
            // 
            // _iS8Io
            // 
            this._iS8Io.Location = new System.Drawing.Point(9, 153);
            this._iS8Io.Name = "_iS8Io";
            this._iS8Io.Size = new System.Drawing.Size(13, 13);
            this._iS8Io.State = BEMN.Forms.LedState.Off;
            this._iS8Io.TabIndex = 73;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 137);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(26, 13);
            this.label88.TabIndex = 68;
            this.label88.Text = "I*>7";
            // 
            // _iS7
            // 
            this._iS7.Location = new System.Drawing.Point(28, 137);
            this._iS7.Name = "_iS7";
            this._iS7.Size = new System.Drawing.Size(13, 13);
            this._iS7.State = BEMN.Forms.LedState.Off;
            this._iS7.TabIndex = 69;
            // 
            // _iS7Io
            // 
            this._iS7Io.Location = new System.Drawing.Point(9, 137);
            this._iS7Io.Name = "_iS7Io";
            this._iS7Io.Size = new System.Drawing.Size(13, 13);
            this._iS7Io.State = BEMN.Forms.LedState.Off;
            this._iS7Io.TabIndex = 70;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.ForeColor = System.Drawing.Color.Blue;
            this.label229.Location = new System.Drawing.Point(25, 16);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(32, 13);
            this.label229.TabIndex = 67;
            this.label229.Text = "Сраб";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(47, 35);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(26, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "I*>1";
            // 
            // _iS1
            // 
            this._iS1.Location = new System.Drawing.Point(28, 35);
            this._iS1.Name = "_iS1";
            this._iS1.Size = new System.Drawing.Size(13, 13);
            this._iS1.State = BEMN.Forms.LedState.Off;
            this._iS1.TabIndex = 17;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(47, 52);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(26, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "I*>2";
            // 
            // _iS2
            // 
            this._iS2.Location = new System.Drawing.Point(28, 52);
            this._iS2.Name = "_iS2";
            this._iS2.Size = new System.Drawing.Size(13, 13);
            this._iS2.State = BEMN.Forms.LedState.Off;
            this._iS2.TabIndex = 19;
            // 
            // _iS1Io
            // 
            this._iS1Io.Location = new System.Drawing.Point(9, 35);
            this._iS1Io.Name = "_iS1Io";
            this._iS1Io.Size = new System.Drawing.Size(13, 13);
            this._iS1Io.State = BEMN.Forms.LedState.Off;
            this._iS1Io.TabIndex = 17;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(47, 69);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(26, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "I*>3";
            // 
            // _iS3
            // 
            this._iS3.Location = new System.Drawing.Point(28, 69);
            this._iS3.Name = "_iS3";
            this._iS3.Size = new System.Drawing.Size(13, 13);
            this._iS3.State = BEMN.Forms.LedState.Off;
            this._iS3.TabIndex = 21;
            // 
            // _iS2Io
            // 
            this._iS2Io.Location = new System.Drawing.Point(9, 52);
            this._iS2Io.Name = "_iS2Io";
            this._iS2Io.Size = new System.Drawing.Size(13, 13);
            this._iS2Io.State = BEMN.Forms.LedState.Off;
            this._iS2Io.TabIndex = 19;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.ForeColor = System.Drawing.Color.Red;
            this.label230.Location = new System.Drawing.Point(6, 16);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(23, 13);
            this.label230.TabIndex = 66;
            this.label230.Text = "ИО";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(47, 86);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(26, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I*>4";
            // 
            // _iS4
            // 
            this._iS4.Location = new System.Drawing.Point(28, 86);
            this._iS4.Name = "_iS4";
            this._iS4.Size = new System.Drawing.Size(13, 13);
            this._iS4.State = BEMN.Forms.LedState.Off;
            this._iS4.TabIndex = 23;
            // 
            // _iS3Io
            // 
            this._iS3Io.Location = new System.Drawing.Point(9, 69);
            this._iS3Io.Name = "_iS3Io";
            this._iS3Io.Size = new System.Drawing.Size(13, 13);
            this._iS3Io.State = BEMN.Forms.LedState.Off;
            this._iS3Io.TabIndex = 21;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(47, 103);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(26, 13);
            this.label82.TabIndex = 24;
            this.label82.Text = "I*>5";
            // 
            // _iS5
            // 
            this._iS5.Location = new System.Drawing.Point(28, 103);
            this._iS5.Name = "_iS5";
            this._iS5.Size = new System.Drawing.Size(13, 13);
            this._iS5.State = BEMN.Forms.LedState.Off;
            this._iS5.TabIndex = 25;
            // 
            // _iS4Io
            // 
            this._iS4Io.Location = new System.Drawing.Point(9, 86);
            this._iS4Io.Name = "_iS4Io";
            this._iS4Io.Size = new System.Drawing.Size(13, 13);
            this._iS4Io.State = BEMN.Forms.LedState.Off;
            this._iS4Io.TabIndex = 23;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(47, 120);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(26, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "I*>6";
            // 
            // _iS6
            // 
            this._iS6.Location = new System.Drawing.Point(28, 120);
            this._iS6.Name = "_iS6";
            this._iS6.Size = new System.Drawing.Size(13, 13);
            this._iS6.State = BEMN.Forms.LedState.Off;
            this._iS6.TabIndex = 27;
            // 
            // _iS5Io
            // 
            this._iS5Io.Location = new System.Drawing.Point(9, 103);
            this._iS5Io.Name = "_iS5Io";
            this._iS5Io.Size = new System.Drawing.Size(13, 13);
            this._iS5Io.State = BEMN.Forms.LedState.Off;
            this._iS5Io.TabIndex = 25;
            // 
            // _iS6Io
            // 
            this._iS6Io.Location = new System.Drawing.Point(9, 120);
            this._iS6Io.Name = "_iS6Io";
            this._iS6Io.Size = new System.Drawing.Size(13, 13);
            this._iS6Io.State = BEMN.Forms.LedState.Off;
            this._iS6Io.TabIndex = 27;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(679, 158);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(84, 177);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I";
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 149);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 149);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 149);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I<";
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 130);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 111);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 130);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 130);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(22, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I>6";
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 92);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 111);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 111);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I>5";
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 73);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 92);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 92);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(22, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>4";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 54);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 73);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 73);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(22, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>3";
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 35);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 54);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 54);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>2";
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 35);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>1";
            // 
            // splGroupBox
            // 
            this.splGroupBox.Controls.Add(this._ssl48);
            this.splGroupBox.Controls.Add(this._ssl40);
            this.splGroupBox.Controls.Add(this._ssl32);
            this.splGroupBox.Controls.Add(this.label484);
            this.splGroupBox.Controls.Add(this.label476);
            this.splGroupBox.Controls.Add(this.label237);
            this.splGroupBox.Controls.Add(this._ssl47);
            this.splGroupBox.Controls.Add(this._ssl39);
            this.splGroupBox.Controls.Add(this._ssl31);
            this.splGroupBox.Controls.Add(this.label483);
            this.splGroupBox.Controls.Add(this.label475);
            this.splGroupBox.Controls.Add(this.label238);
            this.splGroupBox.Controls.Add(this._ssl46);
            this.splGroupBox.Controls.Add(this._ssl38);
            this.splGroupBox.Controls.Add(this._ssl30);
            this.splGroupBox.Controls.Add(this.label482);
            this.splGroupBox.Controls.Add(this.label474);
            this.splGroupBox.Controls.Add(this.label239);
            this.splGroupBox.Controls.Add(this._ssl45);
            this.splGroupBox.Controls.Add(this._ssl37);
            this.splGroupBox.Controls.Add(this._ssl29);
            this.splGroupBox.Controls.Add(this.label481);
            this.splGroupBox.Controls.Add(this.label473);
            this.splGroupBox.Controls.Add(this.label240);
            this.splGroupBox.Controls.Add(this._ssl44);
            this.splGroupBox.Controls.Add(this._ssl36);
            this.splGroupBox.Controls.Add(this._ssl28);
            this.splGroupBox.Controls.Add(this.label480);
            this.splGroupBox.Controls.Add(this.label472);
            this.splGroupBox.Controls.Add(this.label241);
            this.splGroupBox.Controls.Add(this._ssl43);
            this.splGroupBox.Controls.Add(this._ssl35);
            this.splGroupBox.Controls.Add(this._ssl27);
            this.splGroupBox.Controls.Add(this.label479);
            this.splGroupBox.Controls.Add(this.label471);
            this.splGroupBox.Controls.Add(this.label242);
            this.splGroupBox.Controls.Add(this._ssl42);
            this.splGroupBox.Controls.Add(this._ssl34);
            this.splGroupBox.Controls.Add(this._ssl26);
            this.splGroupBox.Controls.Add(this.label478);
            this.splGroupBox.Controls.Add(this.label470);
            this.splGroupBox.Controls.Add(this.label243);
            this.splGroupBox.Controls.Add(this._ssl41);
            this.splGroupBox.Controls.Add(this.label477);
            this.splGroupBox.Controls.Add(this._ssl33);
            this.splGroupBox.Controls.Add(this.label224);
            this.splGroupBox.Controls.Add(this._ssl25);
            this.splGroupBox.Controls.Add(this.label244);
            this.splGroupBox.Controls.Add(this._ssl24);
            this.splGroupBox.Controls.Add(this.label245);
            this.splGroupBox.Controls.Add(this._ssl23);
            this.splGroupBox.Controls.Add(this.label246);
            this.splGroupBox.Controls.Add(this._ssl22);
            this.splGroupBox.Controls.Add(this.label247);
            this.splGroupBox.Controls.Add(this._ssl21);
            this.splGroupBox.Controls.Add(this.label248);
            this.splGroupBox.Controls.Add(this._ssl20);
            this.splGroupBox.Controls.Add(this.label249);
            this.splGroupBox.Controls.Add(this._ssl19);
            this.splGroupBox.Controls.Add(this.label250);
            this.splGroupBox.Controls.Add(this._ssl18);
            this.splGroupBox.Controls.Add(this.label251);
            this.splGroupBox.Controls.Add(this._ssl17);
            this.splGroupBox.Controls.Add(this.label252);
            this.splGroupBox.Controls.Add(this._ssl16);
            this.splGroupBox.Controls.Add(this.label253);
            this.splGroupBox.Controls.Add(this._ssl15);
            this.splGroupBox.Controls.Add(this.label254);
            this.splGroupBox.Controls.Add(this._ssl14);
            this.splGroupBox.Controls.Add(this.label255);
            this.splGroupBox.Controls.Add(this._ssl13);
            this.splGroupBox.Controls.Add(this.label256);
            this.splGroupBox.Controls.Add(this._ssl12);
            this.splGroupBox.Controls.Add(this.label257);
            this.splGroupBox.Controls.Add(this._ssl11);
            this.splGroupBox.Controls.Add(this.label258);
            this.splGroupBox.Controls.Add(this._ssl10);
            this.splGroupBox.Controls.Add(this.label259);
            this.splGroupBox.Controls.Add(this._ssl9);
            this.splGroupBox.Controls.Add(this.label260);
            this.splGroupBox.Controls.Add(this._ssl8);
            this.splGroupBox.Controls.Add(this.label261);
            this.splGroupBox.Controls.Add(this._ssl7);
            this.splGroupBox.Controls.Add(this.label262);
            this.splGroupBox.Controls.Add(this._ssl6);
            this.splGroupBox.Controls.Add(this.label263);
            this.splGroupBox.Controls.Add(this._ssl5);
            this.splGroupBox.Controls.Add(this.label264);
            this.splGroupBox.Controls.Add(this._ssl4);
            this.splGroupBox.Controls.Add(this.label265);
            this.splGroupBox.Controls.Add(this._ssl3);
            this.splGroupBox.Controls.Add(this.label266);
            this.splGroupBox.Controls.Add(this._ssl2);
            this.splGroupBox.Controls.Add(this.label267);
            this.splGroupBox.Controls.Add(this._ssl1);
            this.splGroupBox.Controls.Add(this.label268);
            this.splGroupBox.Location = new System.Drawing.Point(6, 182);
            this.splGroupBox.Name = "splGroupBox";
            this.splGroupBox.Size = new System.Drawing.Size(378, 182);
            this.splGroupBox.TabIndex = 14;
            this.splGroupBox.TabStop = false;
            this.splGroupBox.Text = "Сигналы СП-логики";
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(189, 152);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(205, 152);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(189, 133);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(205, 133);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(189, 114);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(205, 114);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(189, 95);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(205, 95);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(189, 76);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(205, 76);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(189, 57);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(205, 57);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(189, 38);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(205, 38);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(189, 19);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(205, 19);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(126, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(142, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(126, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(142, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(126, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(142, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(126, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(142, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(126, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(142, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(126, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(142, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(126, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(142, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(126, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(142, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(63, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(79, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(63, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(79, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(63, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(79, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(63, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(79, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(63, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(79, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(63, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(79, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(63, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(79, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(63, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(79, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(22, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(22, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(22, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(22, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(23, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(22, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(22, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(22, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.diod12);
            this.groupBox18.Controls.Add(this.diod11);
            this.groupBox18.Controls.Add(this.diod10);
            this.groupBox18.Controls.Add(this.diod9);
            this.groupBox18.Controls.Add(this.diod8);
            this.groupBox18.Controls.Add(this.diod7);
            this.groupBox18.Controls.Add(this.diod6);
            this.groupBox18.Controls.Add(this.diod5);
            this.groupBox18.Controls.Add(this.diod4);
            this.groupBox18.Controls.Add(this.diod3);
            this.groupBox18.Controls.Add(this.diod2);
            this.groupBox18.Controls.Add(this.diod1);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(85, 170);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(44, 115);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 30;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(44, 95);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 31;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(44, 75);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 32;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(44, 55);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 33;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(44, 35);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 34;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(44, 15);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 35;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(6, 115);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 36;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(6, 95);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 37;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(6, 75);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 38;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(6, 55);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 39;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(6, 35);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 40;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(6, 15);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 41;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(63, 96);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 116);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(63, 76);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(63, 56);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(63, 36);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(63, 16);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(63, 116);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 96);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 56);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 36);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 16);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._module34);
            this.groupBox17.Controls.Add(this.label18);
            this.groupBox17.Controls.Add(this._module33);
            this.groupBox17.Controls.Add(this.label20);
            this.groupBox17.Controls.Add(this._module18);
            this.groupBox17.Controls.Add(this._module10);
            this.groupBox17.Controls.Add(this.label161);
            this.groupBox17.Controls.Add(this._module32);
            this.groupBox17.Controls.Add(this._module17);
            this.groupBox17.Controls.Add(this.label138);
            this.groupBox17.Controls.Add(this.label162);
            this.groupBox17.Controls.Add(this._module31);
            this.groupBox17.Controls.Add(this.label164);
            this.groupBox17.Controls.Add(this.label139);
            this.groupBox17.Controls.Add(this._module9);
            this.groupBox17.Controls.Add(this._module30);
            this.groupBox17.Controls.Add(this._module16);
            this.groupBox17.Controls.Add(this.label140);
            this.groupBox17.Controls.Add(this.label165);
            this.groupBox17.Controls.Add(this._module29);
            this.groupBox17.Controls.Add(this.label163);
            this.groupBox17.Controls.Add(this.label141);
            this.groupBox17.Controls.Add(this._module15);
            this.groupBox17.Controls.Add(this._module28);
            this.groupBox17.Controls.Add(this._module5);
            this.groupBox17.Controls.Add(this.label142);
            this.groupBox17.Controls.Add(this.label169);
            this.groupBox17.Controls.Add(this._module27);
            this.groupBox17.Controls.Add(this._module8);
            this.groupBox17.Controls.Add(this.label232);
            this.groupBox17.Controls.Add(this._module14);
            this.groupBox17.Controls.Add(this._module26);
            this.groupBox17.Controls.Add(this.label176);
            this.groupBox17.Controls.Add(this.label127);
            this.groupBox17.Controls.Add(this.label170);
            this.groupBox17.Controls.Add(this._module25);
            this.groupBox17.Controls.Add(this.label128);
            this.groupBox17.Controls.Add(this.label166);
            this.groupBox17.Controls.Add(this._module13);
            this.groupBox17.Controls.Add(this._module24);
            this.groupBox17.Controls.Add(this._module1);
            this.groupBox17.Controls.Add(this.label129);
            this.groupBox17.Controls.Add(this.label171);
            this.groupBox17.Controls.Add(this._module23);
            this.groupBox17.Controls.Add(this._module7);
            this.groupBox17.Controls.Add(this.label130);
            this.groupBox17.Controls.Add(this._module12);
            this.groupBox17.Controls.Add(this._module22);
            this.groupBox17.Controls.Add(this.label175);
            this.groupBox17.Controls.Add(this.label131);
            this.groupBox17.Controls.Add(this.label177);
            this.groupBox17.Controls.Add(this._module21);
            this.groupBox17.Controls.Add(this.label167);
            this.groupBox17.Controls.Add(this.label132);
            this.groupBox17.Controls.Add(this._module11);
            this.groupBox17.Controls.Add(this._module20);
            this.groupBox17.Controls.Add(this.label178);
            this.groupBox17.Controls.Add(this.label133);
            this.groupBox17.Controls.Add(this._module2);
            this.groupBox17.Controls.Add(this._module19);
            this.groupBox17.Controls.Add(this.label134);
            this.groupBox17.Controls.Add(this._module6);
            this.groupBox17.Controls.Add(this.label168);
            this.groupBox17.Controls.Add(this.label174);
            this.groupBox17.Controls.Add(this._module3);
            this.groupBox17.Controls.Add(this.label173);
            this.groupBox17.Controls.Add(this.label172);
            this.groupBox17.Controls.Add(this._module4);
            this.groupBox17.Location = new System.Drawing.Point(97, 6);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(242, 169);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Реле";
            // 
            // _module34
            // 
            this._module34.Location = new System.Drawing.Point(200, 151);
            this._module34.Name = "_module34";
            this._module34.Size = new System.Drawing.Size(13, 13);
            this._module34.State = BEMN.Forms.LedState.Off;
            this._module34.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(219, 151);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "34";
            // 
            // _module33
            // 
            this._module33.Location = new System.Drawing.Point(200, 132);
            this._module33.Name = "_module33";
            this._module33.Size = new System.Drawing.Size(13, 13);
            this._module33.State = BEMN.Forms.LedState.Off;
            this._module33.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(219, 132);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "33";
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(112, 151);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 15;
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(71, 95);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 25;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(131, 151);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(19, 13);
            this.label161.TabIndex = 14;
            this.label161.Text = "18";
            // 
            // _module32
            // 
            this._module32.Location = new System.Drawing.Point(200, 113);
            this._module32.Name = "_module32";
            this._module32.Size = new System.Drawing.Size(13, 13);
            this._module32.State = BEMN.Forms.LedState.Off;
            this._module32.TabIndex = 27;
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(112, 132);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 13;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(219, 113);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(19, 13);
            this.label138.TabIndex = 26;
            this.label138.Text = "32";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(131, 132);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 13);
            this.label162.TabIndex = 12;
            this.label162.Text = "17";
            // 
            // _module31
            // 
            this._module31.Location = new System.Drawing.Point(200, 94);
            this._module31.Name = "_module31";
            this._module31.Size = new System.Drawing.Size(13, 13);
            this._module31.State = BEMN.Forms.LedState.Off;
            this._module31.TabIndex = 25;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(90, 95);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(19, 13);
            this.label164.TabIndex = 24;
            this.label164.Text = "10";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(219, 94);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 24;
            this.label139.Text = "31";
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(71, 76);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 23;
            // 
            // _module30
            // 
            this._module30.Location = new System.Drawing.Point(200, 75);
            this._module30.Name = "_module30";
            this._module30.Size = new System.Drawing.Size(13, 13);
            this._module30.State = BEMN.Forms.LedState.Off;
            this._module30.TabIndex = 23;
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(112, 113);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 11;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(219, 75);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(19, 13);
            this.label140.TabIndex = 22;
            this.label140.Text = "30";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(90, 76);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 13);
            this.label165.TabIndex = 22;
            this.label165.Text = "9";
            // 
            // _module29
            // 
            this._module29.Location = new System.Drawing.Point(200, 56);
            this._module29.Name = "_module29";
            this._module29.Size = new System.Drawing.Size(13, 13);
            this._module29.State = BEMN.Forms.LedState.Off;
            this._module29.TabIndex = 21;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(131, 113);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(19, 13);
            this.label163.TabIndex = 10;
            this.label163.Text = "16";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(219, 56);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(19, 13);
            this.label141.TabIndex = 20;
            this.label141.Text = "29";
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(112, 94);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 9;
            // 
            // _module28
            // 
            this._module28.Location = new System.Drawing.Point(200, 37);
            this._module28.Name = "_module28";
            this._module28.Size = new System.Drawing.Size(13, 13);
            this._module28.State = BEMN.Forms.LedState.Off;
            this._module28.TabIndex = 19;
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(6, 95);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 9;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(219, 37);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 18;
            this.label142.Text = "28";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(131, 94);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(19, 13);
            this.label169.TabIndex = 8;
            this.label169.Text = "15";
            // 
            // _module27
            // 
            this._module27.Location = new System.Drawing.Point(200, 18);
            this._module27.Name = "_module27";
            this._module27.Size = new System.Drawing.Size(13, 13);
            this._module27.State = BEMN.Forms.LedState.Off;
            this._module27.TabIndex = 17;
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(71, 57);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 21;
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(219, 18);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(19, 13);
            this.label232.TabIndex = 16;
            this.label232.Text = "27";
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(112, 75);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 7;
            // 
            // _module26
            // 
            this._module26.Location = new System.Drawing.Point(157, 151);
            this._module26.Name = "_module26";
            this._module26.Size = new System.Drawing.Size(13, 13);
            this._module26.State = BEMN.Forms.LedState.Off;
            this._module26.TabIndex = 15;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(25, 19);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(38, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "1(Вкл)";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(176, 151);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(19, 13);
            this.label127.TabIndex = 14;
            this.label127.Text = "26";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(131, 75);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(19, 13);
            this.label170.TabIndex = 6;
            this.label170.Text = "14";
            // 
            // _module25
            // 
            this._module25.Location = new System.Drawing.Point(157, 132);
            this._module25.Name = "_module25";
            this._module25.Size = new System.Drawing.Size(13, 13);
            this._module25.State = BEMN.Forms.LedState.Off;
            this._module25.TabIndex = 13;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(176, 132);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(19, 13);
            this.label128.TabIndex = 12;
            this.label128.Text = "25";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(90, 57);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(13, 13);
            this.label166.TabIndex = 20;
            this.label166.Text = "8";
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(112, 57);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 5;
            // 
            // _module24
            // 
            this._module24.Location = new System.Drawing.Point(157, 113);
            this._module24.Name = "_module24";
            this._module24.Size = new System.Drawing.Size(13, 13);
            this._module24.State = BEMN.Forms.LedState.Off;
            this._module24.TabIndex = 11;
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(6, 19);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 1;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(176, 113);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(19, 13);
            this.label129.TabIndex = 10;
            this.label129.Text = "24";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(131, 57);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(19, 13);
            this.label171.TabIndex = 4;
            this.label171.Text = "13";
            // 
            // _module23
            // 
            this._module23.Location = new System.Drawing.Point(157, 94);
            this._module23.Name = "_module23";
            this._module23.Size = new System.Drawing.Size(13, 13);
            this._module23.State = BEMN.Forms.LedState.Off;
            this._module23.TabIndex = 9;
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(71, 38);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 19;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(176, 94);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(19, 13);
            this.label130.TabIndex = 8;
            this.label130.Text = "23";
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(112, 38);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 3;
            // 
            // _module22
            // 
            this._module22.Location = new System.Drawing.Point(157, 75);
            this._module22.Name = "_module22";
            this._module22.Size = new System.Drawing.Size(13, 13);
            this._module22.State = BEMN.Forms.LedState.Off;
            this._module22.TabIndex = 7;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(25, 38);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(44, 13);
            this.label175.TabIndex = 2;
            this.label175.Text = "2(Откл)";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(176, 75);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(19, 13);
            this.label131.TabIndex = 6;
            this.label131.Text = "22";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(131, 38);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 2;
            this.label177.Text = "12";
            // 
            // _module21
            // 
            this._module21.Location = new System.Drawing.Point(157, 56);
            this._module21.Name = "_module21";
            this._module21.Size = new System.Drawing.Size(13, 13);
            this._module21.State = BEMN.Forms.LedState.Off;
            this._module21.TabIndex = 5;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(90, 38);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(13, 13);
            this.label167.TabIndex = 18;
            this.label167.Text = "7";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(176, 56);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(19, 13);
            this.label132.TabIndex = 4;
            this.label132.Text = "21";
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(112, 19);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 1;
            // 
            // _module20
            // 
            this._module20.Location = new System.Drawing.Point(157, 37);
            this._module20.Name = "_module20";
            this._module20.Size = new System.Drawing.Size(13, 13);
            this._module20.State = BEMN.Forms.LedState.Off;
            this._module20.TabIndex = 3;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(131, 19);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "11";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(176, 37);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(19, 13);
            this.label133.TabIndex = 2;
            this.label133.Text = "20";
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(6, 38);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 3;
            // 
            // _module19
            // 
            this._module19.Location = new System.Drawing.Point(157, 18);
            this._module19.Name = "_module19";
            this._module19.Size = new System.Drawing.Size(13, 13);
            this._module19.State = BEMN.Forms.LedState.Off;
            this._module19.TabIndex = 1;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(176, 18);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(19, 13);
            this.label134.TabIndex = 0;
            this.label134.Text = "19";
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(71, 19);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 17;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(90, 19);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(13, 13);
            this.label168.TabIndex = 16;
            this.label168.Text = "6";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(25, 57);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(13, 13);
            this.label174.TabIndex = 4;
            this.label174.Text = "3";
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(6, 57);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 5;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(25, 76);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 6;
            this.label173.Text = "4";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(25, 95);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(13, 13);
            this.label172.TabIndex = 8;
            this.label172.Text = "5";
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(6, 76);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 7;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(153, 370);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(6, 370);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 166);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 166);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 147);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 147);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 128);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 109);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 109);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 90);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 71);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 52);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 52);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 33);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._d40);
            this.groupBox6.Controls.Add(this._d24);
            this.groupBox6.Controls.Add(this.label233);
            this.groupBox6.Controls.Add(this._d8);
            this.groupBox6.Controls.Add(this._d39);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.label234);
            this.groupBox6.Controls.Add(this._d38);
            this.groupBox6.Controls.Add(this._d23);
            this.groupBox6.Controls.Add(this.label235);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this._d37);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label236);
            this.groupBox6.Controls.Add(this._d22);
            this.groupBox6.Controls.Add(this._d36);
            this.groupBox6.Controls.Add(this._d7);
            this.groupBox6.Controls.Add(this.label271);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this._d35);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label272);
            this.groupBox6.Controls.Add(this._d21);
            this.groupBox6.Controls.Add(this._d34);
            this.groupBox6.Controls.Add(this._d1);
            this.groupBox6.Controls.Add(this.label273);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this._k2);
            this.groupBox6.Controls.Add(this.label369);
            this.groupBox6.Controls.Add(this._k1);
            this.groupBox6.Controls.Add(this.label366);
            this.groupBox6.Controls.Add(this._d33);
            this.groupBox6.Controls.Add(this.label274);
            this.groupBox6.Controls.Add(this._d6);
            this.groupBox6.Controls.Add(this._d20);
            this.groupBox6.Controls.Add(this._d32);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label275);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this._d31);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label276);
            this.groupBox6.Controls.Add(this._d19);
            this.groupBox6.Controls.Add(this._d30);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label277);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this._d29);
            this.groupBox6.Controls.Add(this._d5);
            this.groupBox6.Controls.Add(this.label278);
            this.groupBox6.Controls.Add(this._d18);
            this.groupBox6.Controls.Add(this._d28);
            this.groupBox6.Controls.Add(this._d2);
            this.groupBox6.Controls.Add(this.label279);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this._d27);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label280);
            this.groupBox6.Controls.Add(this._d17);
            this.groupBox6.Controls.Add(this._d26);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label281);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this._d25);
            this.groupBox6.Controls.Add(this.label282);
            this.groupBox6.Controls.Add(this._d4);
            this.groupBox6.Controls.Add(this._d16);
            this.groupBox6.Controls.Add(this._d3);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this._d15);
            this.groupBox6.Controls.Add(this._d9);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this._d14);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this._d10);
            this.groupBox6.Controls.Add(this._d13);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this._d11);
            this.groupBox6.Controls.Add(this._d12);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Location = new System.Drawing.Point(345, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(316, 170);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискретные входы";
            // 
            // _d40
            // 
            this._d40.Location = new System.Drawing.Point(212, 152);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(13, 13);
            this._d40.State = BEMN.Forms.LedState.Off;
            this._d40.TabIndex = 31;
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(106, 152);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(231, 152);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(28, 13);
            this.label233.TabIndex = 30;
            this.label233.Text = "Д40";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // _d39
            // 
            this._d39.Location = new System.Drawing.Point(212, 133);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(13, 13);
            this._d39.State = BEMN.Forms.LedState.Off;
            this._d39.TabIndex = 29;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(125, 152);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Д24";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(231, 133);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(28, 13);
            this.label234.TabIndex = 28;
            this.label234.Text = "Д39";
            // 
            // _d38
            // 
            this._d38.Location = new System.Drawing.Point(212, 114);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(13, 13);
            this._d38.State = BEMN.Forms.LedState.Off;
            this._d38.TabIndex = 27;
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(106, 133);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(231, 114);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(28, 13);
            this.label235.TabIndex = 26;
            this.label235.Text = "Д38";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(25, 152);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 14;
            this.label30.Text = "Д8";
            // 
            // _d37
            // 
            this._d37.Location = new System.Drawing.Point(212, 95);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(13, 13);
            this._d37.State = BEMN.Forms.LedState.Off;
            this._d37.TabIndex = 25;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(125, 133);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(28, 13);
            this.label40.TabIndex = 28;
            this.label40.Text = "Д23";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(231, 95);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(28, 13);
            this.label236.TabIndex = 24;
            this.label236.Text = "Д37";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(106, 114);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // _d36
            // 
            this._d36.Location = new System.Drawing.Point(212, 76);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(13, 13);
            this._d36.State = BEMN.Forms.LedState.Off;
            this._d36.TabIndex = 23;
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(231, 76);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(28, 13);
            this.label271.TabIndex = 22;
            this.label271.Text = "Д36";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(125, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 26;
            this.label41.Text = "Д22";
            // 
            // _d35
            // 
            this._d35.Location = new System.Drawing.Point(212, 57);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(13, 13);
            this._d35.State = BEMN.Forms.LedState.Off;
            this._d35.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 133);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Д7";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(231, 57);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(28, 13);
            this.label272.TabIndex = 20;
            this.label272.Text = "Д35";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(106, 95);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // _d34
            // 
            this._d34.Location = new System.Drawing.Point(212, 38);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(13, 13);
            this._d34.State = BEMN.Forms.LedState.Off;
            this._d34.TabIndex = 19;
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Location = new System.Drawing.Point(231, 38);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(28, 13);
            this.label273.TabIndex = 18;
            this.label273.Text = "Д34";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(125, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Д21";
            // 
            // _k2
            // 
            this._k2.Location = new System.Drawing.Point(265, 38);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(13, 13);
            this._k2.State = BEMN.Forms.LedState.Off;
            this._k2.TabIndex = 17;
            // 
            // label369
            // 
            this.label369.AutoSize = true;
            this.label369.Location = new System.Drawing.Point(284, 38);
            this.label369.Name = "label369";
            this.label369.Size = new System.Drawing.Size(20, 13);
            this.label369.TabIndex = 16;
            this.label369.Text = "K2";
            // 
            // _k1
            // 
            this._k1.Location = new System.Drawing.Point(265, 19);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(13, 13);
            this._k1.State = BEMN.Forms.LedState.Off;
            this._k1.TabIndex = 17;
            // 
            // label366
            // 
            this.label366.AutoSize = true;
            this.label366.Location = new System.Drawing.Point(284, 19);
            this.label366.Name = "label366";
            this.label366.Size = new System.Drawing.Size(20, 13);
            this.label366.TabIndex = 16;
            this.label366.Text = "K1";
            // 
            // _d33
            // 
            this._d33.Location = new System.Drawing.Point(212, 19);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(13, 13);
            this._d33.State = BEMN.Forms.LedState.Off;
            this._d33.TabIndex = 17;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Location = new System.Drawing.Point(231, 19);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(28, 13);
            this.label274.TabIndex = 16;
            this.label274.Text = "Д33";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(106, 76);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // _d32
            // 
            this._d32.Location = new System.Drawing.Point(159, 152);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(13, 13);
            this._d32.State = BEMN.Forms.LedState.Off;
            this._d32.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Д1";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(178, 152);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(28, 13);
            this.label275.TabIndex = 14;
            this.label275.Text = "Д32";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(125, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Д20";
            // 
            // _d31
            // 
            this._d31.Location = new System.Drawing.Point(159, 133);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(13, 13);
            this._d31.State = BEMN.Forms.LedState.Off;
            this._d31.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(25, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Д6";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(178, 133);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(28, 13);
            this.label276.TabIndex = 12;
            this.label276.Text = "Д31";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(106, 57);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // _d30
            // 
            this._d30.Location = new System.Drawing.Point(159, 114);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(13, 13);
            this._d30.State = BEMN.Forms.LedState.Off;
            this._d30.TabIndex = 11;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Д2";
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(178, 114);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(28, 13);
            this.label277.TabIndex = 10;
            this.label277.Text = "Д30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(125, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "Д19";
            // 
            // _d29
            // 
            this._d29.Location = new System.Drawing.Point(159, 95);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(13, 13);
            this._d29.State = BEMN.Forms.LedState.Off;
            this._d29.TabIndex = 9;
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Location = new System.Drawing.Point(178, 95);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(28, 13);
            this.label278.TabIndex = 8;
            this.label278.Text = "Д29";
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(106, 38);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // _d28
            // 
            this._d28.Location = new System.Drawing.Point(159, 76);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(13, 13);
            this._d28.State = BEMN.Forms.LedState.Off;
            this._d28.TabIndex = 7;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Location = new System.Drawing.Point(178, 76);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(28, 13);
            this.label279.TabIndex = 6;
            this.label279.Text = "Д28";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(125, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "Д18";
            // 
            // _d27
            // 
            this._d27.Location = new System.Drawing.Point(159, 57);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(13, 13);
            this._d27.State = BEMN.Forms.LedState.Off;
            this._d27.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Д5";
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Location = new System.Drawing.Point(178, 57);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(28, 13);
            this.label280.TabIndex = 4;
            this.label280.Text = "Д27";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(106, 19);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // _d26
            // 
            this._d26.Location = new System.Drawing.Point(159, 38);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(13, 13);
            this._d26.State = BEMN.Forms.LedState.Off;
            this._d26.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(125, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Д17";
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(178, 38);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(28, 13);
            this.label281.TabIndex = 2;
            this.label281.Text = "Д26";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(25, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Д3";
            // 
            // _d25
            // 
            this._d25.Location = new System.Drawing.Point(159, 19);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(13, 13);
            this._d25.State = BEMN.Forms.LedState.Off;
            this._d25.TabIndex = 1;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(178, 19);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(28, 13);
            this.label282.TabIndex = 0;
            this.label282.Text = "Д25";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(53, 152);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(72, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Д16";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 76);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Д4";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(53, 133);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(53, 19);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(72, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Д15";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(72, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Д9";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(53, 114);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(72, 38);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Д10";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(72, 114);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "Д14";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(53, 38);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(53, 95);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(72, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Д11";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(72, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Д13";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(53, 57);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(53, 76);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(72, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Д12";
            // 
            // _discretTabPage2
            // 
            this._discretTabPage2.Controls.Add(this.reversGroup1);
            this._discretTabPage2.Controls.Add(this.dugGroup1);
            this._discretTabPage2.Controls.Add(this.groupBox66);
            this._discretTabPage2.Controls.Add(this.groupBox64);
            this._discretTabPage2.Controls.Add(this.urovGroup1);
            this._discretTabPage2.Controls.Add(this.groupBox61);
            this._discretTabPage2.Controls.Add(this.groupBox62);
            this._discretTabPage2.Controls.Add(this.groupBox58);
            this._discretTabPage2.Controls.Add(this.groupBox59);
            this._discretTabPage2.Controls.Add(this.groupBox60);
            this._discretTabPage2.Controls.Add(this.groupBox55);
            this._discretTabPage2.Controls.Add(this.groupBox56);
            this._discretTabPage2.Controls.Add(this.groupBox57);
            this._discretTabPage2.Controls.Add(this.groupBox12);
            this._discretTabPage2.Controls.Add(this.groupBox38);
            this._discretTabPage2.Controls.Add(this.groupBox49);
            this._discretTabPage2.Controls.Add(this.groupBox48);
            this._discretTabPage2.Controls.Add(this.groupBox4);
            this._discretTabPage2.Controls.Add(this.groupBox34);
            this._discretTabPage2.Controls.Add(this.groupBox33);
            this._discretTabPage2.Controls.Add(this.groupBox32);
            this._discretTabPage2.Controls.Add(this.groupBox31);
            this._discretTabPage2.Controls.Add(this.groupBox30);
            this._discretTabPage2.Controls.Add(this.groupBox29);
            this._discretTabPage2.Controls.Add(this.groupBox19);
            this._discretTabPage2.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage2.Name = "_discretTabPage2";
            this._discretTabPage2.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage2.Size = new System.Drawing.Size(933, 599);
            this._discretTabPage2.TabIndex = 3;
            this._discretTabPage2.Text = "Неисправности и защиты";
            this._discretTabPage2.UseVisualStyleBackColor = true;
            // 
            // reversGroup1
            // 
            this.reversGroup1.Controls.Add(this.label450);
            this.reversGroup1.Controls.Add(this.label451);
            this.reversGroup1.Controls.Add(this._P2Io1);
            this.reversGroup1.Controls.Add(this._P1Io1);
            this.reversGroup1.Controls.Add(this._P21);
            this.reversGroup1.Controls.Add(this.label452);
            this.reversGroup1.Controls.Add(this._P11);
            this.reversGroup1.Controls.Add(this.label453);
            this.reversGroup1.Location = new System.Drawing.Point(197, 522);
            this.reversGroup1.Name = "reversGroup1";
            this.reversGroup1.Size = new System.Drawing.Size(77, 71);
            this.reversGroup1.TabIndex = 67;
            this.reversGroup1.TabStop = false;
            this.reversGroup1.Text = "Защиты P";
            // 
            // label450
            // 
            this.label450.AutoSize = true;
            this.label450.ForeColor = System.Drawing.Color.Blue;
            this.label450.Location = new System.Drawing.Point(25, 16);
            this.label450.Name = "label450";
            this.label450.Size = new System.Drawing.Size(32, 13);
            this.label450.TabIndex = 65;
            this.label450.Text = "Сраб";
            // 
            // label451
            // 
            this.label451.AutoSize = true;
            this.label451.ForeColor = System.Drawing.Color.Red;
            this.label451.Location = new System.Drawing.Point(6, 16);
            this.label451.Name = "label451";
            this.label451.Size = new System.Drawing.Size(23, 13);
            this.label451.TabIndex = 64;
            this.label451.Text = "ИО";
            // 
            // _P2Io1
            // 
            this._P2Io1.Location = new System.Drawing.Point(9, 54);
            this._P2Io1.Name = "_P2Io1";
            this._P2Io1.Size = new System.Drawing.Size(13, 13);
            this._P2Io1.State = BEMN.Forms.LedState.Off;
            this._P2Io1.TabIndex = 3;
            // 
            // _P1Io1
            // 
            this._P1Io1.Location = new System.Drawing.Point(9, 35);
            this._P1Io1.Name = "_P1Io1";
            this._P1Io1.Size = new System.Drawing.Size(13, 13);
            this._P1Io1.State = BEMN.Forms.LedState.Off;
            this._P1Io1.TabIndex = 1;
            // 
            // _P21
            // 
            this._P21.Location = new System.Drawing.Point(28, 54);
            this._P21.Name = "_P21";
            this._P21.Size = new System.Drawing.Size(13, 13);
            this._P21.State = BEMN.Forms.LedState.Off;
            this._P21.TabIndex = 3;
            // 
            // label452
            // 
            this.label452.AutoSize = true;
            this.label452.Location = new System.Drawing.Point(47, 54);
            this.label452.Name = "label452";
            this.label452.Size = new System.Drawing.Size(23, 13);
            this.label452.TabIndex = 2;
            this.label452.Text = "P 2";
            // 
            // _P11
            // 
            this._P11.Location = new System.Drawing.Point(28, 35);
            this._P11.Name = "_P11";
            this._P11.Size = new System.Drawing.Size(13, 13);
            this._P11.State = BEMN.Forms.LedState.Off;
            this._P11.TabIndex = 1;
            // 
            // label453
            // 
            this.label453.AutoSize = true;
            this.label453.Location = new System.Drawing.Point(47, 35);
            this.label453.Name = "label453";
            this.label453.Size = new System.Drawing.Size(23, 13);
            this.label453.TabIndex = 0;
            this.label453.Text = "P 1";
            // 
            // dugGroup1
            // 
            this.dugGroup1.Controls.Add(this.dugPusk1);
            this.dugGroup1.Controls.Add(this.label447);
            this.dugGroup1.Location = new System.Drawing.Point(8, 501);
            this.dugGroup1.Name = "dugGroup1";
            this.dugGroup1.Size = new System.Drawing.Size(84, 45);
            this.dugGroup1.TabIndex = 50;
            this.dugGroup1.TabStop = false;
            // 
            // dugPusk1
            // 
            this.dugPusk1.Location = new System.Drawing.Point(6, 19);
            this.dugPusk1.Name = "dugPusk1";
            this.dugPusk1.Size = new System.Drawing.Size(13, 13);
            this.dugPusk1.State = BEMN.Forms.LedState.Off;
            this.dugPusk1.TabIndex = 37;
            // 
            // label447
            // 
            this.label447.AutoSize = true;
            this.label447.Location = new System.Drawing.Point(25, 13);
            this.label447.Name = "label447";
            this.label447.Size = new System.Drawing.Size(54, 26);
            this.label447.TabIndex = 36;
            this.label447.Text = "Пуск дуг.\r\nзащиты";
            // 
            // groupBox66
            // 
            this.groupBox66.Controls.Add(this.dvPusk1);
            this.groupBox66.Controls.Add(this.label442);
            this.groupBox66.Controls.Add(this.dvBlockN1);
            this.groupBox66.Controls.Add(this.dvBlockQ1);
            this.groupBox66.Controls.Add(this.label443);
            this.groupBox66.Controls.Add(this.label444);
            this.groupBox66.Controls.Add(this.dvWork1);
            this.groupBox66.Controls.Add(this.label445);
            this.groupBox66.Location = new System.Drawing.Point(196, 422);
            this.groupBox66.Name = "groupBox66";
            this.groupBox66.Size = new System.Drawing.Size(104, 95);
            this.groupBox66.TabIndex = 49;
            this.groupBox66.TabStop = false;
            this.groupBox66.Text = "Двигатель";
            // 
            // dvPusk1
            // 
            this.dvPusk1.Location = new System.Drawing.Point(6, 19);
            this.dvPusk1.Name = "dvPusk1";
            this.dvPusk1.Size = new System.Drawing.Size(13, 13);
            this.dvPusk1.State = BEMN.Forms.LedState.Off;
            this.dvPusk1.TabIndex = 37;
            // 
            // label442
            // 
            this.label442.AutoSize = true;
            this.label442.Location = new System.Drawing.Point(25, 19);
            this.label442.Name = "label442";
            this.label442.Size = new System.Drawing.Size(32, 13);
            this.label442.TabIndex = 36;
            this.label442.Text = "Пуск";
            // 
            // dvBlockN1
            // 
            this.dvBlockN1.Location = new System.Drawing.Point(6, 76);
            this.dvBlockN1.Name = "dvBlockN1";
            this.dvBlockN1.Size = new System.Drawing.Size(13, 13);
            this.dvBlockN1.State = BEMN.Forms.LedState.Off;
            this.dvBlockN1.TabIndex = 35;
            // 
            // dvBlockQ1
            // 
            this.dvBlockQ1.Location = new System.Drawing.Point(6, 57);
            this.dvBlockQ1.Name = "dvBlockQ1";
            this.dvBlockQ1.Size = new System.Drawing.Size(13, 13);
            this.dvBlockQ1.State = BEMN.Forms.LedState.Off;
            this.dvBlockQ1.TabIndex = 35;
            // 
            // label443
            // 
            this.label443.AutoSize = true;
            this.label443.Location = new System.Drawing.Point(25, 76);
            this.label443.Name = "label443";
            this.label443.Size = new System.Drawing.Size(61, 13);
            this.label443.TabIndex = 34;
            this.label443.Text = "Блок. по N";
            // 
            // label444
            // 
            this.label444.AutoSize = true;
            this.label444.Location = new System.Drawing.Point(25, 57);
            this.label444.Name = "label444";
            this.label444.Size = new System.Drawing.Size(61, 13);
            this.label444.TabIndex = 34;
            this.label444.Text = "Блок. по Q";
            // 
            // dvWork1
            // 
            this.dvWork1.Location = new System.Drawing.Point(6, 38);
            this.dvWork1.Name = "dvWork1";
            this.dvWork1.Size = new System.Drawing.Size(13, 13);
            this.dvWork1.State = BEMN.Forms.LedState.Off;
            this.dvWork1.TabIndex = 31;
            // 
            // label445
            // 
            this.label445.AutoSize = true;
            this.label445.Location = new System.Drawing.Point(25, 38);
            this.label445.Name = "label445";
            this.label445.Size = new System.Drawing.Size(43, 13);
            this.label445.TabIndex = 30;
            this.label445.Text = "Работа";
            // 
            // groupBox64
            // 
            this.groupBox64.Controls.Add(this._avrOn1);
            this.groupBox64.Controls.Add(this.label385);
            this.groupBox64.Controls.Add(this._avrBlock1);
            this.groupBox64.Controls.Add(this.label386);
            this.groupBox64.Controls.Add(this._avrOff1);
            this.groupBox64.Controls.Add(this.label387);
            this.groupBox64.Location = new System.Drawing.Point(374, 453);
            this.groupBox64.Name = "groupBox64";
            this.groupBox64.Size = new System.Drawing.Size(160, 81);
            this.groupBox64.TabIndex = 48;
            this.groupBox64.TabStop = false;
            this.groupBox64.Text = "АВР";
            // 
            // _avrOn1
            // 
            this._avrOn1.Location = new System.Drawing.Point(6, 19);
            this._avrOn1.Name = "_avrOn1";
            this._avrOn1.Size = new System.Drawing.Size(13, 13);
            this._avrOn1.State = BEMN.Forms.LedState.Off;
            this._avrOn1.TabIndex = 37;
            // 
            // label385
            // 
            this.label385.AutoSize = true;
            this.label385.Location = new System.Drawing.Point(25, 19);
            this.label385.Name = "label385";
            this.label385.Size = new System.Drawing.Size(52, 13);
            this.label385.TabIndex = 36;
            this.label385.Text = "АВР вкл.";
            // 
            // _avrBlock1
            // 
            this._avrBlock1.Location = new System.Drawing.Point(6, 57);
            this._avrBlock1.Name = "_avrBlock1";
            this._avrBlock1.Size = new System.Drawing.Size(13, 13);
            this._avrBlock1.State = BEMN.Forms.LedState.Off;
            this._avrBlock1.TabIndex = 35;
            // 
            // label386
            // 
            this.label386.AutoSize = true;
            this.label386.Location = new System.Drawing.Point(25, 57);
            this.label386.Name = "label386";
            this.label386.Size = new System.Drawing.Size(58, 13);
            this.label386.TabIndex = 34;
            this.label386.Text = "АВР блок.";
            // 
            // _avrOff1
            // 
            this._avrOff1.Location = new System.Drawing.Point(6, 38);
            this._avrOff1.Name = "_avrOff1";
            this._avrOff1.Size = new System.Drawing.Size(13, 13);
            this._avrOff1.State = BEMN.Forms.LedState.Off;
            this._avrOff1.TabIndex = 31;
            // 
            // label387
            // 
            this.label387.AutoSize = true;
            this.label387.Location = new System.Drawing.Point(25, 38);
            this.label387.Name = "label387";
            this.label387.Size = new System.Drawing.Size(57, 13);
            this.label387.TabIndex = 30;
            this.label387.Text = "АВР откл.";
            // 
            // urovGroup1
            // 
            this.urovGroup1.Controls.Add(this.urov1Led1);
            this.urovGroup1.Controls.Add(this.label417);
            this.urovGroup1.Controls.Add(this.blockUrovLed1);
            this.urovGroup1.Controls.Add(this.label418);
            this.urovGroup1.Controls.Add(this.urov2Led1);
            this.urovGroup1.Controls.Add(this.label419);
            this.urovGroup1.Location = new System.Drawing.Point(374, 367);
            this.urovGroup1.Name = "urovGroup1";
            this.urovGroup1.Size = new System.Drawing.Size(160, 80);
            this.urovGroup1.TabIndex = 47;
            this.urovGroup1.TabStop = false;
            this.urovGroup1.Text = "УРОВ";
            // 
            // urov1Led1
            // 
            this.urov1Led1.Location = new System.Drawing.Point(6, 19);
            this.urov1Led1.Name = "urov1Led1";
            this.urov1Led1.Size = new System.Drawing.Size(13, 13);
            this.urov1Led1.State = BEMN.Forms.LedState.Off;
            this.urov1Led1.TabIndex = 37;
            // 
            // label417
            // 
            this.label417.AutoSize = true;
            this.label417.Location = new System.Drawing.Point(25, 19);
            this.label417.Name = "label417";
            this.label417.Size = new System.Drawing.Size(43, 13);
            this.label417.TabIndex = 36;
            this.label417.Text = "УРОВ1";
            // 
            // blockUrovLed1
            // 
            this.blockUrovLed1.Location = new System.Drawing.Point(6, 57);
            this.blockUrovLed1.Name = "blockUrovLed1";
            this.blockUrovLed1.Size = new System.Drawing.Size(13, 13);
            this.blockUrovLed1.State = BEMN.Forms.LedState.Off;
            this.blockUrovLed1.TabIndex = 35;
            // 
            // label418
            // 
            this.label418.AutoSize = true;
            this.label418.Location = new System.Drawing.Point(25, 57);
            this.label418.Name = "label418";
            this.label418.Size = new System.Drawing.Size(68, 13);
            this.label418.TabIndex = 34;
            this.label418.Text = "Блок. УРОВ";
            // 
            // urov2Led1
            // 
            this.urov2Led1.Location = new System.Drawing.Point(6, 38);
            this.urov2Led1.Name = "urov2Led1";
            this.urov2Led1.Size = new System.Drawing.Size(13, 13);
            this.urov2Led1.State = BEMN.Forms.LedState.Off;
            this.urov2Led1.TabIndex = 31;
            // 
            // label419
            // 
            this.label419.AutoSize = true;
            this.label419.Location = new System.Drawing.Point(25, 38);
            this.label419.Name = "label419";
            this.label419.Size = new System.Drawing.Size(43, 13);
            this.label419.TabIndex = 30;
            this.label419.Text = "УРОВ2";
            // 
            // groupBox61
            // 
            this.groupBox61.Controls.Add(this._OnKsAndYppn1);
            this.groupBox61.Controls.Add(this.label423);
            this.groupBox61.Controls.Add(this._UnoUno1);
            this.groupBox61.Controls.Add(this.label424);
            this.groupBox61.Controls.Add(this._UyUno1);
            this.groupBox61.Controls.Add(this.label425);
            this.groupBox61.Controls.Add(this._US1);
            this.groupBox61.Controls.Add(this._U1noU2y1);
            this.groupBox61.Controls.Add(this.label426);
            this.groupBox61.Controls.Add(this.label427);
            this.groupBox61.Controls.Add(this.label428);
            this.groupBox61.Controls.Add(this._OS1);
            this.groupBox61.Controls.Add(this.label429);
            this.groupBox61.Controls.Add(this._autoSinchr1);
            this.groupBox61.Location = new System.Drawing.Point(374, 206);
            this.groupBox61.Name = "groupBox61";
            this.groupBox61.Size = new System.Drawing.Size(160, 155);
            this.groupBox61.TabIndex = 46;
            this.groupBox61.TabStop = false;
            this.groupBox61.Text = "Сигналы КС и УППН";
            // 
            // _OnKsAndYppn1
            // 
            this._OnKsAndYppn1.Location = new System.Drawing.Point(6, 132);
            this._OnKsAndYppn1.Name = "_OnKsAndYppn1";
            this._OnKsAndYppn1.Size = new System.Drawing.Size(13, 13);
            this._OnKsAndYppn1.State = BEMN.Forms.LedState.Off;
            this._OnKsAndYppn1.TabIndex = 39;
            // 
            // label423
            // 
            this.label423.AutoSize = true;
            this.label423.Location = new System.Drawing.Point(25, 132);
            this.label423.Name = "label423";
            this.label423.Size = new System.Drawing.Size(132, 13);
            this.label423.TabIndex = 38;
            this.label423.Text = "Включить по КС и УППН";
            // 
            // _UnoUno1
            // 
            this._UnoUno1.Location = new System.Drawing.Point(6, 75);
            this._UnoUno1.Name = "_UnoUno1";
            this._UnoUno1.Size = new System.Drawing.Size(13, 13);
            this._UnoUno1.State = BEMN.Forms.LedState.Off;
            this._UnoUno1.TabIndex = 37;
            // 
            // label424
            // 
            this.label424.AutoSize = true;
            this.label424.Location = new System.Drawing.Point(25, 75);
            this.label424.Name = "label424";
            this.label424.Size = new System.Drawing.Size(75, 13);
            this.label424.TabIndex = 36;
            this.label424.Text = "U1нет, U2нет";
            // 
            // _UyUno1
            // 
            this._UyUno1.Location = new System.Drawing.Point(6, 56);
            this._UyUno1.Name = "_UyUno1";
            this._UyUno1.Size = new System.Drawing.Size(13, 13);
            this._UyUno1.State = BEMN.Forms.LedState.Off;
            this._UyUno1.TabIndex = 35;
            // 
            // label425
            // 
            this.label425.AutoSize = true;
            this.label425.Location = new System.Drawing.Point(25, 56);
            this.label425.Name = "label425";
            this.label425.Size = new System.Drawing.Size(81, 13);
            this.label425.TabIndex = 34;
            this.label425.Text = "U1есть, U2нет";
            // 
            // _US1
            // 
            this._US1.Location = new System.Drawing.Point(6, 113);
            this._US1.Name = "_US1";
            this._US1.Size = new System.Drawing.Size(13, 13);
            this._US1.State = BEMN.Forms.LedState.Off;
            this._US1.TabIndex = 29;
            // 
            // _U1noU2y1
            // 
            this._U1noU2y1.Location = new System.Drawing.Point(6, 38);
            this._U1noU2y1.Name = "_U1noU2y1";
            this._U1noU2y1.Size = new System.Drawing.Size(13, 13);
            this._U1noU2y1.State = BEMN.Forms.LedState.Off;
            this._U1noU2y1.TabIndex = 33;
            // 
            // label426
            // 
            this.label426.AutoSize = true;
            this.label426.Location = new System.Drawing.Point(25, 38);
            this.label426.Name = "label426";
            this.label426.Size = new System.Drawing.Size(81, 13);
            this.label426.TabIndex = 32;
            this.label426.Text = "U1нет, U2есть";
            // 
            // label427
            // 
            this.label427.AutoSize = true;
            this.label427.Location = new System.Drawing.Point(25, 19);
            this.label427.Name = "label427";
            this.label427.Size = new System.Drawing.Size(128, 13);
            this.label427.TabIndex = 30;
            this.label427.Text = "Автоматический режим";
            // 
            // label428
            // 
            this.label428.AutoSize = true;
            this.label428.Location = new System.Drawing.Point(25, 113);
            this.label428.Name = "label428";
            this.label428.Size = new System.Drawing.Size(69, 13);
            this.label428.TabIndex = 28;
            this.label428.Text = "Условия УС";
            // 
            // _OS1
            // 
            this._OS1.Location = new System.Drawing.Point(6, 94);
            this._OS1.Name = "_OS1";
            this._OS1.Size = new System.Drawing.Size(13, 13);
            this._OS1.State = BEMN.Forms.LedState.Off;
            this._OS1.TabIndex = 11;
            // 
            // label429
            // 
            this.label429.AutoSize = true;
            this.label429.Location = new System.Drawing.Point(25, 94);
            this.label429.Name = "label429";
            this.label429.Size = new System.Drawing.Size(69, 13);
            this.label429.TabIndex = 10;
            this.label429.Text = "Условия ОС";
            // 
            // _autoSinchr1
            // 
            this._autoSinchr1.Location = new System.Drawing.Point(6, 19);
            this._autoSinchr1.Name = "_autoSinchr1";
            this._autoSinchr1.Size = new System.Drawing.Size(13, 13);
            this._autoSinchr1.State = BEMN.Forms.LedState.Off;
            this._autoSinchr1.TabIndex = 13;
            // 
            // groupBox62
            // 
            this.groupBox62.Controls.Add(this._readyApv1);
            this.groupBox62.Controls.Add(this.label430);
            this.groupBox62.Controls.Add(this._blockApv1);
            this.groupBox62.Controls.Add(this.label431);
            this.groupBox62.Controls.Add(this.label432);
            this.groupBox62.Controls.Add(this._puskApv1);
            this.groupBox62.Controls.Add(this._krat41);
            this.groupBox62.Controls.Add(this.label433);
            this.groupBox62.Controls.Add(this.label434);
            this.groupBox62.Controls.Add(this._turnOnApv1);
            this.groupBox62.Controls.Add(this._krat31);
            this.groupBox62.Controls.Add(this.label435);
            this.groupBox62.Controls.Add(this.label436);
            this.groupBox62.Controls.Add(this._krat11);
            this.groupBox62.Controls.Add(this._zapretApv1);
            this.groupBox62.Controls.Add(this.label437);
            this.groupBox62.Controls.Add(this._krat21);
            this.groupBox62.Controls.Add(this.label438);
            this.groupBox62.Location = new System.Drawing.Point(374, 6);
            this.groupBox62.Name = "groupBox62";
            this.groupBox62.Size = new System.Drawing.Size(160, 194);
            this.groupBox62.TabIndex = 45;
            this.groupBox62.TabStop = false;
            this.groupBox62.Text = "Сигналы АПВ";
            // 
            // _readyApv1
            // 
            this._readyApv1.Location = new System.Drawing.Point(8, 170);
            this._readyApv1.Name = "_readyApv1";
            this._readyApv1.Size = new System.Drawing.Size(13, 13);
            this._readyApv1.State = BEMN.Forms.LedState.Off;
            this._readyApv1.TabIndex = 57;
            // 
            // label430
            // 
            this.label430.AutoSize = true;
            this.label430.Location = new System.Drawing.Point(27, 170);
            this.label430.Name = "label430";
            this.label430.Size = new System.Drawing.Size(65, 13);
            this.label430.TabIndex = 56;
            this.label430.Text = "Готовность";
            // 
            // _blockApv1
            // 
            this._blockApv1.Location = new System.Drawing.Point(8, 151);
            this._blockApv1.Name = "_blockApv1";
            this._blockApv1.Size = new System.Drawing.Size(13, 13);
            this._blockApv1.State = BEMN.Forms.LedState.Off;
            this._blockApv1.TabIndex = 55;
            // 
            // label431
            // 
            this.label431.AutoSize = true;
            this.label431.Location = new System.Drawing.Point(27, 18);
            this.label431.Name = "label431";
            this.label431.Size = new System.Drawing.Size(32, 13);
            this.label431.TabIndex = 47;
            this.label431.Text = "Пуск";
            // 
            // label432
            // 
            this.label432.AutoSize = true;
            this.label432.Location = new System.Drawing.Point(27, 151);
            this.label432.Name = "label432";
            this.label432.Size = new System.Drawing.Size(68, 13);
            this.label432.TabIndex = 54;
            this.label432.Text = "Блокировка";
            // 
            // _puskApv1
            // 
            this._puskApv1.Location = new System.Drawing.Point(8, 18);
            this._puskApv1.Name = "_puskApv1";
            this._puskApv1.Size = new System.Drawing.Size(13, 13);
            this._puskApv1.State = BEMN.Forms.LedState.Off;
            this._puskApv1.TabIndex = 42;
            // 
            // _krat41
            // 
            this._krat41.Location = new System.Drawing.Point(8, 94);
            this._krat41.Name = "_krat41";
            this._krat41.Size = new System.Drawing.Size(13, 13);
            this._krat41.State = BEMN.Forms.LedState.Off;
            this._krat41.TabIndex = 53;
            // 
            // label433
            // 
            this.label433.AutoSize = true;
            this.label433.Location = new System.Drawing.Point(27, 113);
            this.label433.Name = "label433";
            this.label433.Size = new System.Drawing.Size(96, 13);
            this.label433.TabIndex = 40;
            this.label433.Text = "Включить по АПВ";
            // 
            // label434
            // 
            this.label434.AutoSize = true;
            this.label434.Location = new System.Drawing.Point(27, 94);
            this.label434.Name = "label434";
            this.label434.Size = new System.Drawing.Size(39, 13);
            this.label434.TabIndex = 52;
            this.label434.Text = "4 крат";
            // 
            // _turnOnApv1
            // 
            this._turnOnApv1.Location = new System.Drawing.Point(8, 113);
            this._turnOnApv1.Name = "_turnOnApv1";
            this._turnOnApv1.Size = new System.Drawing.Size(13, 13);
            this._turnOnApv1.State = BEMN.Forms.LedState.Off;
            this._turnOnApv1.TabIndex = 41;
            // 
            // _krat31
            // 
            this._krat31.Location = new System.Drawing.Point(8, 75);
            this._krat31.Name = "_krat31";
            this._krat31.Size = new System.Drawing.Size(13, 13);
            this._krat31.State = BEMN.Forms.LedState.Off;
            this._krat31.TabIndex = 51;
            // 
            // label435
            // 
            this.label435.AutoSize = true;
            this.label435.Location = new System.Drawing.Point(27, 37);
            this.label435.Name = "label435";
            this.label435.Size = new System.Drawing.Size(39, 13);
            this.label435.TabIndex = 44;
            this.label435.Text = "1 крат";
            // 
            // label436
            // 
            this.label436.AutoSize = true;
            this.label436.Location = new System.Drawing.Point(27, 75);
            this.label436.Name = "label436";
            this.label436.Size = new System.Drawing.Size(39, 13);
            this.label436.TabIndex = 50;
            this.label436.Text = "3 крат";
            // 
            // _krat11
            // 
            this._krat11.Location = new System.Drawing.Point(8, 37);
            this._krat11.Name = "_krat11";
            this._krat11.Size = new System.Drawing.Size(13, 13);
            this._krat11.State = BEMN.Forms.LedState.Off;
            this._krat11.TabIndex = 46;
            // 
            // _zapretApv1
            // 
            this._zapretApv1.Location = new System.Drawing.Point(8, 132);
            this._zapretApv1.Name = "_zapretApv1";
            this._zapretApv1.Size = new System.Drawing.Size(13, 13);
            this._zapretApv1.State = BEMN.Forms.LedState.Off;
            this._zapretApv1.TabIndex = 45;
            // 
            // label437
            // 
            this.label437.AutoSize = true;
            this.label437.Location = new System.Drawing.Point(27, 132);
            this.label437.Name = "label437";
            this.label437.Size = new System.Drawing.Size(43, 13);
            this.label437.TabIndex = 43;
            this.label437.Text = "Запрет";
            // 
            // _krat21
            // 
            this._krat21.Location = new System.Drawing.Point(8, 56);
            this._krat21.Name = "_krat21";
            this._krat21.Size = new System.Drawing.Size(13, 13);
            this._krat21.State = BEMN.Forms.LedState.Off;
            this._krat21.TabIndex = 49;
            // 
            // label438
            // 
            this.label438.AutoSize = true;
            this.label438.Location = new System.Drawing.Point(27, 56);
            this.label438.Name = "label438";
            this.label438.Size = new System.Drawing.Size(39, 13);
            this.label438.TabIndex = 48;
            this.label438.Text = "2 крат";
            // 
            // groupBox58
            // 
            this.groupBox58.Controls.Add(this._damageA1);
            this.groupBox58.Controls.Add(this.label409);
            this.groupBox58.Controls.Add(this.label410);
            this.groupBox58.Controls.Add(this._damageB1);
            this.groupBox58.Controls.Add(this._damageC1);
            this.groupBox58.Controls.Add(this.label411);
            this.groupBox58.Location = new System.Drawing.Point(196, 251);
            this.groupBox58.Name = "groupBox58";
            this.groupBox58.Size = new System.Drawing.Size(152, 79);
            this.groupBox58.TabIndex = 44;
            this.groupBox58.TabStop = false;
            this.groupBox58.Text = "Определение повр. фазы";
            // 
            // _damageA1
            // 
            this._damageA1.Location = new System.Drawing.Point(6, 19);
            this._damageA1.Name = "_damageA1";
            this._damageA1.Size = new System.Drawing.Size(13, 13);
            this._damageA1.State = BEMN.Forms.LedState.Off;
            this._damageA1.TabIndex = 13;
            // 
            // label409
            // 
            this.label409.AutoSize = true;
            this.label409.Location = new System.Drawing.Point(25, 20);
            this.label409.Name = "label409";
            this.label409.Size = new System.Drawing.Size(90, 13);
            this.label409.TabIndex = 30;
            this.label409.Text = "Повр. по фазе А";
            // 
            // label410
            // 
            this.label410.AutoSize = true;
            this.label410.Location = new System.Drawing.Point(25, 38);
            this.label410.Name = "label410";
            this.label410.Size = new System.Drawing.Size(90, 13);
            this.label410.TabIndex = 32;
            this.label410.Text = "Повр. по фазе В";
            // 
            // _damageB1
            // 
            this._damageB1.Location = new System.Drawing.Point(6, 38);
            this._damageB1.Name = "_damageB1";
            this._damageB1.Size = new System.Drawing.Size(13, 13);
            this._damageB1.State = BEMN.Forms.LedState.Off;
            this._damageB1.TabIndex = 33;
            // 
            // _damageC1
            // 
            this._damageC1.Location = new System.Drawing.Point(6, 57);
            this._damageC1.Name = "_damageC1";
            this._damageC1.Size = new System.Drawing.Size(13, 13);
            this._damageC1.State = BEMN.Forms.LedState.Off;
            this._damageC1.TabIndex = 35;
            // 
            // label411
            // 
            this.label411.AutoSize = true;
            this.label411.Location = new System.Drawing.Point(25, 57);
            this.label411.Name = "label411";
            this.label411.Size = new System.Drawing.Size(90, 13);
            this.label411.TabIndex = 34;
            this.label411.Text = "Повр. по фазе С";
            // 
            // groupBox59
            // 
            this.groupBox59.Controls.Add(this._tnObivFaz);
            this.groupBox59.Controls.Add(this.l3);
            this.groupBox59.Controls.Add(this._faultTn3U0);
            this.groupBox59.Controls.Add(this.l4);
            this.groupBox59.Controls.Add(this._faultTnU2);
            this.groupBox59.Controls.Add(this.l5);
            this.groupBox59.Controls.Add(this.label416);
            this.groupBox59.Controls.Add(this._faultTNmgn1);
            this.groupBox59.Controls.Add(this.l6);
            this.groupBox59.Controls.Add(this._externalTn);
            this.groupBox59.Controls.Add(this.l2);
            this.groupBox59.Controls.Add(this._faultTN1);
            this.groupBox59.Location = new System.Drawing.Point(540, 206);
            this.groupBox59.Name = "groupBox59";
            this.groupBox59.Size = new System.Drawing.Size(147, 155);
            this.groupBox59.TabIndex = 43;
            this.groupBox59.TabStop = false;
            this.groupBox59.Text = "Неисправности измерения U";
            // 
            // _tnObivFaz
            // 
            this._tnObivFaz.Location = new System.Drawing.Point(6, 107);
            this._tnObivFaz.Name = "_tnObivFaz";
            this._tnObivFaz.Size = new System.Drawing.Size(13, 13);
            this._tnObivFaz.State = BEMN.Forms.LedState.Off;
            this._tnObivFaz.TabIndex = 37;
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.Location = new System.Drawing.Point(25, 107);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(105, 13);
            this.l3.TabIndex = 36;
            this.l3.Text = "ТН обрыв трёх фаз";
            // 
            // _faultTn3U0
            // 
            this._faultTn3U0.Location = new System.Drawing.Point(6, 88);
            this._faultTn3U0.Name = "_faultTn3U0";
            this._faultTn3U0.Size = new System.Drawing.Size(13, 13);
            this._faultTn3U0.State = BEMN.Forms.LedState.Off;
            this._faultTn3U0.TabIndex = 35;
            // 
            // l4
            // 
            this.l4.AutoSize = true;
            this.l4.Location = new System.Drawing.Point(25, 88);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(60, 13);
            this.l4.TabIndex = 34;
            this.l4.Text = "ТН по 3U0";
            // 
            // _faultTnU2
            // 
            this._faultTnU2.Location = new System.Drawing.Point(6, 69);
            this._faultTnU2.Name = "_faultTnU2";
            this._faultTnU2.Size = new System.Drawing.Size(13, 13);
            this._faultTnU2.State = BEMN.Forms.LedState.Off;
            this._faultTnU2.TabIndex = 33;
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.Location = new System.Drawing.Point(25, 69);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(54, 13);
            this.l5.TabIndex = 32;
            this.l5.Text = "ТН по U2";
            // 
            // label416
            // 
            this.label416.AutoSize = true;
            this.label416.Location = new System.Drawing.Point(25, 31);
            this.label416.Name = "label416";
            this.label416.Size = new System.Drawing.Size(119, 13);
            this.label416.TabIndex = 30;
            this.label416.Text = "ТН с задержкой и с/п";
            // 
            // _faultTNmgn1
            // 
            this._faultTNmgn1.Location = new System.Drawing.Point(6, 50);
            this._faultTNmgn1.Name = "_faultTNmgn1";
            this._faultTNmgn1.Size = new System.Drawing.Size(13, 13);
            this._faultTNmgn1.State = BEMN.Forms.LedState.Off;
            this._faultTNmgn1.TabIndex = 29;
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.Location = new System.Drawing.Point(25, 50);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(47, 13);
            this.l6.TabIndex = 28;
            this.l6.Text = "ТН мгн.";
            // 
            // _externalTn
            // 
            this._externalTn.Location = new System.Drawing.Point(6, 126);
            this._externalTn.Name = "_externalTn";
            this._externalTn.Size = new System.Drawing.Size(13, 13);
            this._externalTn.State = BEMN.Forms.LedState.Off;
            this._externalTn.TabIndex = 11;
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Location = new System.Drawing.Point(25, 126);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(70, 13);
            this.l2.TabIndex = 10;
            this.l2.Text = "Внешняя ТН";
            // 
            // _faultTN1
            // 
            this._faultTN1.Location = new System.Drawing.Point(6, 31);
            this._faultTN1.Name = "_faultTN1";
            this._faultTN1.Size = new System.Drawing.Size(13, 13);
            this._faultTN1.State = BEMN.Forms.LedState.Off;
            this._faultTN1.TabIndex = 13;
            // 
            // groupBox60
            // 
            this.groupBox60.Controls.Add(this._kachanie1);
            this.groupBox60.Controls.Add(this.label420);
            this.groupBox60.Controls.Add(this._inZone1);
            this.groupBox60.Controls.Add(this.label421);
            this.groupBox60.Controls.Add(this._outZone1);
            this.groupBox60.Controls.Add(this.label422);
            this.groupBox60.Location = new System.Drawing.Point(196, 336);
            this.groupBox60.Name = "groupBox60";
            this.groupBox60.Size = new System.Drawing.Size(152, 80);
            this.groupBox60.TabIndex = 42;
            this.groupBox60.TabStop = false;
            this.groupBox60.Text = "Сигналы качаний";
            // 
            // _kachanie1
            // 
            this._kachanie1.Location = new System.Drawing.Point(6, 19);
            this._kachanie1.Name = "_kachanie1";
            this._kachanie1.Size = new System.Drawing.Size(13, 13);
            this._kachanie1.State = BEMN.Forms.LedState.Off;
            this._kachanie1.TabIndex = 37;
            // 
            // label420
            // 
            this.label420.AutoSize = true;
            this.label420.Location = new System.Drawing.Point(25, 19);
            this.label420.Name = "label420";
            this.label420.Size = new System.Drawing.Size(49, 13);
            this.label420.TabIndex = 36;
            this.label420.Text = "Качание";
            // 
            // _inZone1
            // 
            this._inZone1.Location = new System.Drawing.Point(6, 57);
            this._inZone1.Name = "_inZone1";
            this._inZone1.Size = new System.Drawing.Size(13, 13);
            this._inZone1.State = BEMN.Forms.LedState.Off;
            this._inZone1.TabIndex = 31;
            // 
            // label421
            // 
            this.label421.AutoSize = true;
            this.label421.Location = new System.Drawing.Point(25, 57);
            this.label421.Name = "label421";
            this.label421.Size = new System.Drawing.Size(93, 13);
            this.label421.TabIndex = 30;
            this.label421.Text = "Внутренняя зона";
            // 
            // _outZone1
            // 
            this._outZone1.Location = new System.Drawing.Point(6, 38);
            this._outZone1.Name = "_outZone1";
            this._outZone1.Size = new System.Drawing.Size(13, 13);
            this._outZone1.State = BEMN.Forms.LedState.Off;
            this._outZone1.TabIndex = 27;
            // 
            // label422
            // 
            this.label422.AutoSize = true;
            this.label422.Location = new System.Drawing.Point(25, 38);
            this.label422.Name = "label422";
            this.label422.Size = new System.Drawing.Size(79, 13);
            this.label422.TabIndex = 26;
            this.label422.Text = "Внешняя зона";
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this.label379);
            this.groupBox55.Controls.Add(this.label380);
            this.groupBox55.Controls.Add(this.label381);
            this.groupBox55.Controls.Add(this.label382);
            this.groupBox55.Controls.Add(this._r4IoLed1);
            this.groupBox55.Controls.Add(this._r5Led1);
            this.groupBox55.Controls.Add(this._r1Led1);
            this.groupBox55.Controls.Add(this._r3IoLed1);
            this.groupBox55.Controls.Add(this.label383);
            this.groupBox55.Controls.Add(this.label384);
            this.groupBox55.Controls.Add(this._r6IoLed1);
            this.groupBox55.Controls.Add(this._r2IoLed1);
            this.groupBox55.Controls.Add(this._r6Led1);
            this.groupBox55.Controls.Add(this._r2Led1);
            this.groupBox55.Controls.Add(this._r4Led1);
            this.groupBox55.Controls.Add(this.label388);
            this.groupBox55.Controls.Add(this.label390);
            this.groupBox55.Controls.Add(this._r3Led1);
            this.groupBox55.Controls.Add(this._r5IoLed1);
            this.groupBox55.Controls.Add(this._r1IoLed1);
            this.groupBox55.Location = new System.Drawing.Point(8, 6);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(84, 146);
            this.groupBox55.TabIndex = 40;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Защиты Z";
            // 
            // label379
            // 
            this.label379.AutoSize = true;
            this.label379.ForeColor = System.Drawing.Color.Blue;
            this.label379.Location = new System.Drawing.Point(25, 12);
            this.label379.Name = "label379";
            this.label379.Size = new System.Drawing.Size(32, 13);
            this.label379.TabIndex = 83;
            this.label379.Text = "Сраб";
            // 
            // label380
            // 
            this.label380.AutoSize = true;
            this.label380.ForeColor = System.Drawing.Color.Red;
            this.label380.Location = new System.Drawing.Point(6, 12);
            this.label380.Name = "label380";
            this.label380.Size = new System.Drawing.Size(23, 13);
            this.label380.TabIndex = 82;
            this.label380.Text = "ИО";
            // 
            // label381
            // 
            this.label381.AutoSize = true;
            this.label381.Location = new System.Drawing.Point(47, 108);
            this.label381.Name = "label381";
            this.label381.Size = new System.Drawing.Size(23, 13);
            this.label381.TabIndex = 70;
            this.label381.Text = "Z 5";
            // 
            // label382
            // 
            this.label382.AutoSize = true;
            this.label382.Location = new System.Drawing.Point(47, 32);
            this.label382.Name = "label382";
            this.label382.Size = new System.Drawing.Size(23, 13);
            this.label382.TabIndex = 70;
            this.label382.Text = "Z 1";
            // 
            // _r4IoLed1
            // 
            this._r4IoLed1.Location = new System.Drawing.Point(9, 89);
            this._r4IoLed1.Name = "_r4IoLed1";
            this._r4IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r4IoLed1.State = BEMN.Forms.LedState.Off;
            this._r4IoLed1.TabIndex = 80;
            // 
            // _r5Led1
            // 
            this._r5Led1.Location = new System.Drawing.Point(28, 108);
            this._r5Led1.Name = "_r5Led1";
            this._r5Led1.Size = new System.Drawing.Size(13, 13);
            this._r5Led1.State = BEMN.Forms.LedState.Off;
            this._r5Led1.TabIndex = 72;
            // 
            // _r1Led1
            // 
            this._r1Led1.Location = new System.Drawing.Point(28, 32);
            this._r1Led1.Name = "_r1Led1";
            this._r1Led1.Size = new System.Drawing.Size(13, 13);
            this._r1Led1.State = BEMN.Forms.LedState.Off;
            this._r1Led1.TabIndex = 72;
            // 
            // _r3IoLed1
            // 
            this._r3IoLed1.Location = new System.Drawing.Point(9, 70);
            this._r3IoLed1.Name = "_r3IoLed1";
            this._r3IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r3IoLed1.State = BEMN.Forms.LedState.Off;
            this._r3IoLed1.TabIndex = 77;
            // 
            // label383
            // 
            this.label383.AutoSize = true;
            this.label383.Location = new System.Drawing.Point(47, 127);
            this.label383.Name = "label383";
            this.label383.Size = new System.Drawing.Size(23, 13);
            this.label383.TabIndex = 73;
            this.label383.Text = "Z 6";
            // 
            // label384
            // 
            this.label384.AutoSize = true;
            this.label384.Location = new System.Drawing.Point(47, 51);
            this.label384.Name = "label384";
            this.label384.Size = new System.Drawing.Size(23, 13);
            this.label384.TabIndex = 73;
            this.label384.Text = "Z 2";
            // 
            // _r6IoLed1
            // 
            this._r6IoLed1.Location = new System.Drawing.Point(9, 127);
            this._r6IoLed1.Name = "_r6IoLed1";
            this._r6IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r6IoLed1.State = BEMN.Forms.LedState.Off;
            this._r6IoLed1.TabIndex = 74;
            // 
            // _r2IoLed1
            // 
            this._r2IoLed1.Location = new System.Drawing.Point(9, 51);
            this._r2IoLed1.Name = "_r2IoLed1";
            this._r2IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r2IoLed1.State = BEMN.Forms.LedState.Off;
            this._r2IoLed1.TabIndex = 74;
            // 
            // _r6Led1
            // 
            this._r6Led1.Location = new System.Drawing.Point(28, 127);
            this._r6Led1.Name = "_r6Led1";
            this._r6Led1.Size = new System.Drawing.Size(13, 13);
            this._r6Led1.State = BEMN.Forms.LedState.Off;
            this._r6Led1.TabIndex = 75;
            // 
            // _r2Led1
            // 
            this._r2Led1.Location = new System.Drawing.Point(28, 51);
            this._r2Led1.Name = "_r2Led1";
            this._r2Led1.Size = new System.Drawing.Size(13, 13);
            this._r2Led1.State = BEMN.Forms.LedState.Off;
            this._r2Led1.TabIndex = 75;
            // 
            // _r4Led1
            // 
            this._r4Led1.Location = new System.Drawing.Point(28, 89);
            this._r4Led1.Name = "_r4Led1";
            this._r4Led1.Size = new System.Drawing.Size(13, 13);
            this._r4Led1.State = BEMN.Forms.LedState.Off;
            this._r4Led1.TabIndex = 81;
            // 
            // label388
            // 
            this.label388.AutoSize = true;
            this.label388.Location = new System.Drawing.Point(47, 70);
            this.label388.Name = "label388";
            this.label388.Size = new System.Drawing.Size(23, 13);
            this.label388.TabIndex = 76;
            this.label388.Text = "Z 3";
            // 
            // label390
            // 
            this.label390.AutoSize = true;
            this.label390.Location = new System.Drawing.Point(48, 89);
            this.label390.Name = "label390";
            this.label390.Size = new System.Drawing.Size(23, 13);
            this.label390.TabIndex = 79;
            this.label390.Text = "Z 4";
            // 
            // _r3Led1
            // 
            this._r3Led1.Location = new System.Drawing.Point(28, 70);
            this._r3Led1.Name = "_r3Led1";
            this._r3Led1.Size = new System.Drawing.Size(13, 13);
            this._r3Led1.State = BEMN.Forms.LedState.Off;
            this._r3Led1.TabIndex = 78;
            // 
            // _r5IoLed1
            // 
            this._r5IoLed1.Location = new System.Drawing.Point(9, 108);
            this._r5IoLed1.Name = "_r5IoLed1";
            this._r5IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r5IoLed1.State = BEMN.Forms.LedState.Off;
            this._r5IoLed1.TabIndex = 71;
            // 
            // _r1IoLed1
            // 
            this._r1IoLed1.Location = new System.Drawing.Point(9, 32);
            this._r1IoLed1.Name = "_r1IoLed1";
            this._r1IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r1IoLed1.State = BEMN.Forms.LedState.Off;
            this._r1IoLed1.TabIndex = 71;
            // 
            // groupBox56
            // 
            this.groupBox56.Controls.Add(this.label414);
            this.groupBox56.Controls.Add(this._iS81);
            this.groupBox56.Controls.Add(this._iS8Io1);
            this.groupBox56.Controls.Add(this.label402);
            this.groupBox56.Controls.Add(this._iS71);
            this.groupBox56.Controls.Add(this._iS7Io1);
            this.groupBox56.Controls.Add(this.label391);
            this.groupBox56.Controls.Add(this.label392);
            this.groupBox56.Controls.Add(this._iS11);
            this.groupBox56.Controls.Add(this.label393);
            this.groupBox56.Controls.Add(this._iS21);
            this.groupBox56.Controls.Add(this._iS1Io1);
            this.groupBox56.Controls.Add(this.label394);
            this.groupBox56.Controls.Add(this._iS31);
            this.groupBox56.Controls.Add(this._iS2Io1);
            this.groupBox56.Controls.Add(this.label395);
            this.groupBox56.Controls.Add(this.label396);
            this.groupBox56.Controls.Add(this._iS41);
            this.groupBox56.Controls.Add(this._iS3Io1);
            this.groupBox56.Controls.Add(this.label397);
            this.groupBox56.Controls.Add(this._iS51);
            this.groupBox56.Controls.Add(this._iS4Io1);
            this.groupBox56.Controls.Add(this.label398);
            this.groupBox56.Controls.Add(this._iS61);
            this.groupBox56.Controls.Add(this._iS5Io1);
            this.groupBox56.Controls.Add(this._iS6Io1);
            this.groupBox56.Location = new System.Drawing.Point(8, 315);
            this.groupBox56.Name = "groupBox56";
            this.groupBox56.Size = new System.Drawing.Size(84, 186);
            this.groupBox56.TabIndex = 39;
            this.groupBox56.TabStop = false;
            this.groupBox56.Text = "Защиты I*";
            // 
            // label414
            // 
            this.label414.AutoSize = true;
            this.label414.Location = new System.Drawing.Point(47, 167);
            this.label414.Name = "label414";
            this.label414.Size = new System.Drawing.Size(26, 13);
            this.label414.TabIndex = 71;
            this.label414.Text = "I*>8";
            // 
            // _iS81
            // 
            this._iS81.Location = new System.Drawing.Point(28, 167);
            this._iS81.Name = "_iS81";
            this._iS81.Size = new System.Drawing.Size(13, 13);
            this._iS81.State = BEMN.Forms.LedState.Off;
            this._iS81.TabIndex = 72;
            // 
            // _iS8Io1
            // 
            this._iS8Io1.Location = new System.Drawing.Point(9, 167);
            this._iS8Io1.Name = "_iS8Io1";
            this._iS8Io1.Size = new System.Drawing.Size(13, 13);
            this._iS8Io1.State = BEMN.Forms.LedState.Off;
            this._iS8Io1.TabIndex = 73;
            // 
            // label402
            // 
            this.label402.AutoSize = true;
            this.label402.Location = new System.Drawing.Point(47, 149);
            this.label402.Name = "label402";
            this.label402.Size = new System.Drawing.Size(26, 13);
            this.label402.TabIndex = 68;
            this.label402.Text = "I*>7";
            // 
            // _iS71
            // 
            this._iS71.Location = new System.Drawing.Point(28, 149);
            this._iS71.Name = "_iS71";
            this._iS71.Size = new System.Drawing.Size(13, 13);
            this._iS71.State = BEMN.Forms.LedState.Off;
            this._iS71.TabIndex = 69;
            // 
            // _iS7Io1
            // 
            this._iS7Io1.Location = new System.Drawing.Point(9, 149);
            this._iS7Io1.Name = "_iS7Io1";
            this._iS7Io1.Size = new System.Drawing.Size(13, 13);
            this._iS7Io1.State = BEMN.Forms.LedState.Off;
            this._iS7Io1.TabIndex = 70;
            // 
            // label391
            // 
            this.label391.AutoSize = true;
            this.label391.ForeColor = System.Drawing.Color.Blue;
            this.label391.Location = new System.Drawing.Point(25, 16);
            this.label391.Name = "label391";
            this.label391.Size = new System.Drawing.Size(32, 13);
            this.label391.TabIndex = 67;
            this.label391.Text = "Сраб";
            // 
            // label392
            // 
            this.label392.AutoSize = true;
            this.label392.Location = new System.Drawing.Point(47, 35);
            this.label392.Name = "label392";
            this.label392.Size = new System.Drawing.Size(26, 13);
            this.label392.TabIndex = 16;
            this.label392.Text = "I*>1";
            // 
            // _iS11
            // 
            this._iS11.Location = new System.Drawing.Point(28, 35);
            this._iS11.Name = "_iS11";
            this._iS11.Size = new System.Drawing.Size(13, 13);
            this._iS11.State = BEMN.Forms.LedState.Off;
            this._iS11.TabIndex = 17;
            // 
            // label393
            // 
            this.label393.AutoSize = true;
            this.label393.Location = new System.Drawing.Point(47, 54);
            this.label393.Name = "label393";
            this.label393.Size = new System.Drawing.Size(26, 13);
            this.label393.TabIndex = 18;
            this.label393.Text = "I*>2";
            // 
            // _iS21
            // 
            this._iS21.Location = new System.Drawing.Point(28, 54);
            this._iS21.Name = "_iS21";
            this._iS21.Size = new System.Drawing.Size(13, 13);
            this._iS21.State = BEMN.Forms.LedState.Off;
            this._iS21.TabIndex = 19;
            // 
            // _iS1Io1
            // 
            this._iS1Io1.Location = new System.Drawing.Point(9, 35);
            this._iS1Io1.Name = "_iS1Io1";
            this._iS1Io1.Size = new System.Drawing.Size(13, 13);
            this._iS1Io1.State = BEMN.Forms.LedState.Off;
            this._iS1Io1.TabIndex = 17;
            // 
            // label394
            // 
            this.label394.AutoSize = true;
            this.label394.Location = new System.Drawing.Point(47, 73);
            this.label394.Name = "label394";
            this.label394.Size = new System.Drawing.Size(26, 13);
            this.label394.TabIndex = 20;
            this.label394.Text = "I*>3";
            // 
            // _iS31
            // 
            this._iS31.Location = new System.Drawing.Point(28, 73);
            this._iS31.Name = "_iS31";
            this._iS31.Size = new System.Drawing.Size(13, 13);
            this._iS31.State = BEMN.Forms.LedState.Off;
            this._iS31.TabIndex = 21;
            // 
            // _iS2Io1
            // 
            this._iS2Io1.Location = new System.Drawing.Point(9, 54);
            this._iS2Io1.Name = "_iS2Io1";
            this._iS2Io1.Size = new System.Drawing.Size(13, 13);
            this._iS2Io1.State = BEMN.Forms.LedState.Off;
            this._iS2Io1.TabIndex = 19;
            // 
            // label395
            // 
            this.label395.AutoSize = true;
            this.label395.ForeColor = System.Drawing.Color.Red;
            this.label395.Location = new System.Drawing.Point(6, 16);
            this.label395.Name = "label395";
            this.label395.Size = new System.Drawing.Size(23, 13);
            this.label395.TabIndex = 66;
            this.label395.Text = "ИО";
            // 
            // label396
            // 
            this.label396.AutoSize = true;
            this.label396.Location = new System.Drawing.Point(47, 92);
            this.label396.Name = "label396";
            this.label396.Size = new System.Drawing.Size(26, 13);
            this.label396.TabIndex = 22;
            this.label396.Text = "I*>4";
            // 
            // _iS41
            // 
            this._iS41.Location = new System.Drawing.Point(28, 92);
            this._iS41.Name = "_iS41";
            this._iS41.Size = new System.Drawing.Size(13, 13);
            this._iS41.State = BEMN.Forms.LedState.Off;
            this._iS41.TabIndex = 23;
            // 
            // _iS3Io1
            // 
            this._iS3Io1.Location = new System.Drawing.Point(9, 73);
            this._iS3Io1.Name = "_iS3Io1";
            this._iS3Io1.Size = new System.Drawing.Size(13, 13);
            this._iS3Io1.State = BEMN.Forms.LedState.Off;
            this._iS3Io1.TabIndex = 21;
            // 
            // label397
            // 
            this.label397.AutoSize = true;
            this.label397.Location = new System.Drawing.Point(47, 111);
            this.label397.Name = "label397";
            this.label397.Size = new System.Drawing.Size(26, 13);
            this.label397.TabIndex = 24;
            this.label397.Text = "I*>5";
            // 
            // _iS51
            // 
            this._iS51.Location = new System.Drawing.Point(28, 111);
            this._iS51.Name = "_iS51";
            this._iS51.Size = new System.Drawing.Size(13, 13);
            this._iS51.State = BEMN.Forms.LedState.Off;
            this._iS51.TabIndex = 25;
            // 
            // _iS4Io1
            // 
            this._iS4Io1.Location = new System.Drawing.Point(9, 92);
            this._iS4Io1.Name = "_iS4Io1";
            this._iS4Io1.Size = new System.Drawing.Size(13, 13);
            this._iS4Io1.State = BEMN.Forms.LedState.Off;
            this._iS4Io1.TabIndex = 23;
            // 
            // label398
            // 
            this.label398.AutoSize = true;
            this.label398.Location = new System.Drawing.Point(47, 130);
            this.label398.Name = "label398";
            this.label398.Size = new System.Drawing.Size(26, 13);
            this.label398.TabIndex = 26;
            this.label398.Text = "I*>6";
            // 
            // _iS61
            // 
            this._iS61.Location = new System.Drawing.Point(28, 130);
            this._iS61.Name = "_iS61";
            this._iS61.Size = new System.Drawing.Size(13, 13);
            this._iS61.State = BEMN.Forms.LedState.Off;
            this._iS61.TabIndex = 27;
            // 
            // _iS5Io1
            // 
            this._iS5Io1.Location = new System.Drawing.Point(9, 111);
            this._iS5Io1.Name = "_iS5Io1";
            this._iS5Io1.Size = new System.Drawing.Size(13, 13);
            this._iS5Io1.State = BEMN.Forms.LedState.Off;
            this._iS5Io1.TabIndex = 25;
            // 
            // _iS6Io1
            // 
            this._iS6Io1.Location = new System.Drawing.Point(9, 130);
            this._iS6Io1.Name = "_iS6Io1";
            this._iS6Io1.Size = new System.Drawing.Size(13, 13);
            this._iS6Io1.State = BEMN.Forms.LedState.Off;
            this._iS6Io1.TabIndex = 27;
            // 
            // groupBox57
            // 
            this.groupBox57.Controls.Add(this.label399);
            this.groupBox57.Controls.Add(this.label400);
            this.groupBox57.Controls.Add(this._i6Io1);
            this.groupBox57.Controls.Add(this._i5Io1);
            this.groupBox57.Controls.Add(this._i61);
            this.groupBox57.Controls.Add(this.label403);
            this.groupBox57.Controls.Add(this._i4Io1);
            this.groupBox57.Controls.Add(this._i51);
            this.groupBox57.Controls.Add(this.label404);
            this.groupBox57.Controls.Add(this._i3Io1);
            this.groupBox57.Controls.Add(this._i41);
            this.groupBox57.Controls.Add(this.label405);
            this.groupBox57.Controls.Add(this._i2Io1);
            this.groupBox57.Controls.Add(this._i31);
            this.groupBox57.Controls.Add(this.label406);
            this.groupBox57.Controls.Add(this._i1Io1);
            this.groupBox57.Controls.Add(this._i21);
            this.groupBox57.Controls.Add(this.label407);
            this.groupBox57.Controls.Add(this._i11);
            this.groupBox57.Controls.Add(this.label408);
            this.groupBox57.Location = new System.Drawing.Point(8, 158);
            this.groupBox57.Name = "groupBox57";
            this.groupBox57.Size = new System.Drawing.Size(84, 151);
            this.groupBox57.TabIndex = 38;
            this.groupBox57.TabStop = false;
            this.groupBox57.Text = "Защиты I";
            // 
            // label399
            // 
            this.label399.AutoSize = true;
            this.label399.ForeColor = System.Drawing.Color.Blue;
            this.label399.Location = new System.Drawing.Point(25, 16);
            this.label399.Name = "label399";
            this.label399.Size = new System.Drawing.Size(32, 13);
            this.label399.TabIndex = 65;
            this.label399.Text = "Сраб";
            // 
            // label400
            // 
            this.label400.AutoSize = true;
            this.label400.ForeColor = System.Drawing.Color.Red;
            this.label400.Location = new System.Drawing.Point(6, 16);
            this.label400.Name = "label400";
            this.label400.Size = new System.Drawing.Size(23, 13);
            this.label400.TabIndex = 64;
            this.label400.Text = "ИО";
            // 
            // _i6Io1
            // 
            this._i6Io1.Location = new System.Drawing.Point(9, 130);
            this._i6Io1.Name = "_i6Io1";
            this._i6Io1.Size = new System.Drawing.Size(13, 13);
            this._i6Io1.State = BEMN.Forms.LedState.Off;
            this._i6Io1.TabIndex = 11;
            // 
            // _i5Io1
            // 
            this._i5Io1.Location = new System.Drawing.Point(9, 111);
            this._i5Io1.Name = "_i5Io1";
            this._i5Io1.Size = new System.Drawing.Size(13, 13);
            this._i5Io1.State = BEMN.Forms.LedState.Off;
            this._i5Io1.TabIndex = 9;
            // 
            // _i61
            // 
            this._i61.Location = new System.Drawing.Point(28, 130);
            this._i61.Name = "_i61";
            this._i61.Size = new System.Drawing.Size(13, 13);
            this._i61.State = BEMN.Forms.LedState.Off;
            this._i61.TabIndex = 11;
            // 
            // label403
            // 
            this.label403.AutoSize = true;
            this.label403.Location = new System.Drawing.Point(47, 130);
            this.label403.Name = "label403";
            this.label403.Size = new System.Drawing.Size(22, 13);
            this.label403.TabIndex = 10;
            this.label403.Text = "I>6";
            // 
            // _i4Io1
            // 
            this._i4Io1.Location = new System.Drawing.Point(9, 92);
            this._i4Io1.Name = "_i4Io1";
            this._i4Io1.Size = new System.Drawing.Size(13, 13);
            this._i4Io1.State = BEMN.Forms.LedState.Off;
            this._i4Io1.TabIndex = 7;
            // 
            // _i51
            // 
            this._i51.Location = new System.Drawing.Point(28, 111);
            this._i51.Name = "_i51";
            this._i51.Size = new System.Drawing.Size(13, 13);
            this._i51.State = BEMN.Forms.LedState.Off;
            this._i51.TabIndex = 9;
            // 
            // label404
            // 
            this.label404.AutoSize = true;
            this.label404.Location = new System.Drawing.Point(47, 111);
            this.label404.Name = "label404";
            this.label404.Size = new System.Drawing.Size(22, 13);
            this.label404.TabIndex = 8;
            this.label404.Text = "I>5";
            // 
            // _i3Io1
            // 
            this._i3Io1.Location = new System.Drawing.Point(9, 73);
            this._i3Io1.Name = "_i3Io1";
            this._i3Io1.Size = new System.Drawing.Size(13, 13);
            this._i3Io1.State = BEMN.Forms.LedState.Off;
            this._i3Io1.TabIndex = 5;
            // 
            // _i41
            // 
            this._i41.Location = new System.Drawing.Point(28, 92);
            this._i41.Name = "_i41";
            this._i41.Size = new System.Drawing.Size(13, 13);
            this._i41.State = BEMN.Forms.LedState.Off;
            this._i41.TabIndex = 7;
            // 
            // label405
            // 
            this.label405.AutoSize = true;
            this.label405.Location = new System.Drawing.Point(48, 92);
            this.label405.Name = "label405";
            this.label405.Size = new System.Drawing.Size(22, 13);
            this.label405.TabIndex = 6;
            this.label405.Text = "I>4";
            // 
            // _i2Io1
            // 
            this._i2Io1.Location = new System.Drawing.Point(9, 54);
            this._i2Io1.Name = "_i2Io1";
            this._i2Io1.Size = new System.Drawing.Size(13, 13);
            this._i2Io1.State = BEMN.Forms.LedState.Off;
            this._i2Io1.TabIndex = 3;
            // 
            // _i31
            // 
            this._i31.Location = new System.Drawing.Point(28, 73);
            this._i31.Name = "_i31";
            this._i31.Size = new System.Drawing.Size(13, 13);
            this._i31.State = BEMN.Forms.LedState.Off;
            this._i31.TabIndex = 5;
            // 
            // label406
            // 
            this.label406.AutoSize = true;
            this.label406.Location = new System.Drawing.Point(47, 73);
            this.label406.Name = "label406";
            this.label406.Size = new System.Drawing.Size(22, 13);
            this.label406.TabIndex = 4;
            this.label406.Text = "I>3";
            // 
            // _i1Io1
            // 
            this._i1Io1.Location = new System.Drawing.Point(9, 35);
            this._i1Io1.Name = "_i1Io1";
            this._i1Io1.Size = new System.Drawing.Size(13, 13);
            this._i1Io1.State = BEMN.Forms.LedState.Off;
            this._i1Io1.TabIndex = 1;
            // 
            // _i21
            // 
            this._i21.Location = new System.Drawing.Point(28, 54);
            this._i21.Name = "_i21";
            this._i21.Size = new System.Drawing.Size(13, 13);
            this._i21.State = BEMN.Forms.LedState.Off;
            this._i21.TabIndex = 3;
            // 
            // label407
            // 
            this.label407.AutoSize = true;
            this.label407.Location = new System.Drawing.Point(47, 54);
            this.label407.Name = "label407";
            this.label407.Size = new System.Drawing.Size(22, 13);
            this.label407.TabIndex = 2;
            this.label407.Text = "I>2";
            // 
            // _i11
            // 
            this._i11.Location = new System.Drawing.Point(28, 35);
            this._i11.Name = "_i11";
            this._i11.Size = new System.Drawing.Size(13, 13);
            this._i11.State = BEMN.Forms.LedState.Off;
            this._i11.TabIndex = 1;
            // 
            // label408
            // 
            this.label408.AutoSize = true;
            this.label408.Location = new System.Drawing.Point(47, 35);
            this.label408.Name = "label408";
            this.label408.Size = new System.Drawing.Size(22, 13);
            this.label408.TabIndex = 0;
            this.label408.Text = "I>1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(196, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(172, 175);
            this.groupBox12.TabIndex = 37;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(84, 152);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(103, 152);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(56, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВНЕШ. 16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(84, 133);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(103, 133);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(56, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВНЕШ. 15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(84, 114);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(103, 114);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(56, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВНЕШ. 14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(84, 95);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(103, 95);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(56, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВНЕШ. 13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(84, 76);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(103, 76);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(56, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВНЕШ. 12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(84, 57);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(103, 57);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(56, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВНЕШ. 11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(84, 38);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(103, 38);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(56, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВНЕШ. 10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(84, 19);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(103, 19);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВНЕШ. 9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(50, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВНЕШ. 8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(50, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВНЕШ. 7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(50, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВНЕШ. 6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(50, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВНЕШ. 5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(50, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВНЕШ. 4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВНЕШ. 3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(50, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВНЕШ. 2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(50, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВНЕШ. 1";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.groupBox39);
            this.groupBox38.Controls.Add(this._fSpl4);
            this.groupBox38.Controls.Add(this.label320);
            this.groupBox38.Controls.Add(this.label319);
            this.groupBox38.Location = new System.Drawing.Point(772, 6);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(122, 127);
            this.groupBox38.TabIndex = 36;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Ошибки СПЛ";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._fSpl1);
            this.groupBox39.Controls.Add(this.label315);
            this.groupBox39.Controls.Add(this.label316);
            this.groupBox39.Controls.Add(this._fSpl2);
            this.groupBox39.Controls.Add(this.label317);
            this.groupBox39.Controls.Add(this._fSpl3);
            this.groupBox39.Location = new System.Drawing.Point(6, 44);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(110, 77);
            this.groupBox39.TabIndex = 32;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Ошибки CRC";
            // 
            // _fSpl1
            // 
            this._fSpl1.Location = new System.Drawing.Point(6, 19);
            this._fSpl1.Name = "_fSpl1";
            this._fSpl1.Size = new System.Drawing.Size(13, 13);
            this._fSpl1.State = BEMN.Forms.LedState.Off;
            this._fSpl1.TabIndex = 25;
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(25, 19);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(54, 13);
            this.label315.TabIndex = 24;
            this.label315.Text = "Констант";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(25, 38);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(70, 13);
            this.label316.TabIndex = 26;
            this.label316.Text = "Разрешения";
            // 
            // _fSpl2
            // 
            this._fSpl2.Location = new System.Drawing.Point(6, 38);
            this._fSpl2.Name = "_fSpl2";
            this._fSpl2.Size = new System.Drawing.Size(13, 13);
            this._fSpl2.State = BEMN.Forms.LedState.Off;
            this._fSpl2.TabIndex = 27;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(25, 57);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(68, 13);
            this.label317.TabIndex = 28;
            this.label317.Text = "Программы";
            // 
            // _fSpl3
            // 
            this._fSpl3.Location = new System.Drawing.Point(6, 57);
            this._fSpl3.Name = "_fSpl3";
            this._fSpl3.Size = new System.Drawing.Size(13, 13);
            this._fSpl3.State = BEMN.Forms.LedState.Off;
            this._fSpl3.TabIndex = 29;
            // 
            // _fSpl4
            // 
            this._fSpl4.Location = new System.Drawing.Point(4, 19);
            this._fSpl4.Name = "_fSpl4";
            this._fSpl4.Size = new System.Drawing.Size(13, 13);
            this._fSpl4.State = BEMN.Forms.LedState.Off;
            this._fSpl4.TabIndex = 31;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(17, 28);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(66, 13);
            this.label320.TabIndex = 30;
            this.label320.Text = "программы";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(16, 14);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(105, 13);
            this.label319.TabIndex = 30;
            this.label319.Text = "В ходе выполнения";
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.label347);
            this.groupBox49.Controls.Add(this._faultDisable2);
            this.groupBox49.Controls.Add(this.label346);
            this.groupBox49.Controls.Add(this.label345);
            this.groupBox49.Controls.Add(this.label311);
            this.groupBox49.Controls.Add(this.label220);
            this.groupBox49.Controls.Add(this.label98);
            this.groupBox49.Controls.Add(this.label22);
            this.groupBox49.Controls.Add(this._faultDisable1);
            this.groupBox49.Controls.Add(this._faultSwithON);
            this.groupBox49.Controls.Add(this._faultManage);
            this.groupBox49.Controls.Add(this._faultOtkaz);
            this.groupBox49.Controls.Add(this._faultBlockCon);
            this.groupBox49.Controls.Add(this._faultOut);
            this.groupBox49.Location = new System.Drawing.Point(692, 205);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(179, 157);
            this.groupBox49.TabIndex = 35;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "Неисправности выключателя";
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Location = new System.Drawing.Point(25, 138);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(105, 13);
            this.label347.TabIndex = 13;
            this.label347.Text = "Цепи отключения 2";
            // 
            // _faultDisable2
            // 
            this._faultDisable2.Location = new System.Drawing.Point(6, 137);
            this._faultDisable2.Name = "_faultDisable2";
            this._faultDisable2.Size = new System.Drawing.Size(13, 13);
            this._faultDisable2.State = BEMN.Forms.LedState.Off;
            this._faultDisable2.TabIndex = 12;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Location = new System.Drawing.Point(25, 118);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(105, 13);
            this.label346.TabIndex = 11;
            this.label346.Text = "Цепи отключения 1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(25, 99);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(91, 13);
            this.label345.TabIndex = 10;
            this.label345.Text = "Цепи включения";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(25, 60);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(69, 13);
            this.label311.TabIndex = 9;
            this.label311.Text = "Управления";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(25, 79);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(109, 13);
            this.label220.TabIndex = 8;
            this.label220.Text = "Отказ выключателя";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(25, 41);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(105, 13);
            this.label98.TabIndex = 7;
            this.label98.Text = "По блок-контактам";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Внешняя ";
            // 
            // _faultDisable1
            // 
            this._faultDisable1.Location = new System.Drawing.Point(6, 118);
            this._faultDisable1.Name = "_faultDisable1";
            this._faultDisable1.Size = new System.Drawing.Size(13, 13);
            this._faultDisable1.State = BEMN.Forms.LedState.Off;
            this._faultDisable1.TabIndex = 5;
            // 
            // _faultSwithON
            // 
            this._faultSwithON.Location = new System.Drawing.Point(6, 99);
            this._faultSwithON.Name = "_faultSwithON";
            this._faultSwithON.Size = new System.Drawing.Size(13, 13);
            this._faultSwithON.State = BEMN.Forms.LedState.Off;
            this._faultSwithON.TabIndex = 4;
            // 
            // _faultManage
            // 
            this._faultManage.Location = new System.Drawing.Point(6, 60);
            this._faultManage.Name = "_faultManage";
            this._faultManage.Size = new System.Drawing.Size(13, 13);
            this._faultManage.State = BEMN.Forms.LedState.Off;
            this._faultManage.TabIndex = 3;
            // 
            // _faultOtkaz
            // 
            this._faultOtkaz.Location = new System.Drawing.Point(6, 79);
            this._faultOtkaz.Name = "_faultOtkaz";
            this._faultOtkaz.Size = new System.Drawing.Size(13, 13);
            this._faultOtkaz.State = BEMN.Forms.LedState.Off;
            this._faultOtkaz.TabIndex = 2;
            // 
            // _faultBlockCon
            // 
            this._faultBlockCon.Location = new System.Drawing.Point(6, 41);
            this._faultBlockCon.Name = "_faultBlockCon";
            this._faultBlockCon.Size = new System.Drawing.Size(13, 13);
            this._faultBlockCon.State = BEMN.Forms.LedState.Off;
            this._faultBlockCon.TabIndex = 1;
            // 
            // _faultOut
            // 
            this._faultOut.Location = new System.Drawing.Point(6, 22);
            this._faultOut.Name = "_faultOut";
            this._faultOut.Size = new System.Drawing.Size(13, 13);
            this._faultOut.State = BEMN.Forms.LedState.Off;
            this._faultOut.TabIndex = 0;
            // 
            // groupBox48
            // 
            this.groupBox48.Controls.Add(this.label228);
            this.groupBox48.Controls.Add(this._UabcLow10);
            this.groupBox48.Controls.Add(this.label313);
            this.groupBox48.Controls.Add(this._freqLow40);
            this.groupBox48.Controls.Add(this._freqHiger60);
            this.groupBox48.Controls.Add(this.label312);
            this.groupBox48.Location = new System.Drawing.Point(540, 367);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(147, 94);
            this.groupBox48.TabIndex = 34;
            this.groupBox48.TabStop = false;
            this.groupBox48.Text = "Неисправности измерения F";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(25, 33);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(64, 13);
            this.label228.TabIndex = 26;
            this.label228.Text = "Uabc < 10B";
            // 
            // _UabcLow10
            // 
            this._UabcLow10.Location = new System.Drawing.Point(6, 33);
            this._UabcLow10.Name = "_UabcLow10";
            this._UabcLow10.Size = new System.Drawing.Size(13, 13);
            this._UabcLow10.State = BEMN.Forms.LedState.Off;
            this._UabcLow10.TabIndex = 27;
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(25, 52);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(85, 13);
            this.label313.TabIndex = 6;
            this.label313.Text = "Частота > 60Гц";
            // 
            // _freqLow40
            // 
            this._freqLow40.Location = new System.Drawing.Point(6, 71);
            this._freqLow40.Name = "_freqLow40";
            this._freqLow40.Size = new System.Drawing.Size(13, 13);
            this._freqLow40.State = BEMN.Forms.LedState.Off;
            this._freqLow40.TabIndex = 13;
            // 
            // _freqHiger60
            // 
            this._freqHiger60.Location = new System.Drawing.Point(6, 52);
            this._freqHiger60.Name = "_freqHiger60";
            this._freqHiger60.Size = new System.Drawing.Size(13, 13);
            this._freqHiger60.State = BEMN.Forms.LedState.Off;
            this._freqHiger60.TabIndex = 7;
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(25, 71);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(85, 13);
            this.label312.TabIndex = 12;
            this.label312.Text = "Частота < 40Гц";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._q2Led);
            this.groupBox4.Controls.Add(this.label102);
            this.groupBox4.Controls.Add(this._q1Led);
            this.groupBox4.Controls.Add(this.label104);
            this.groupBox4.Location = new System.Drawing.Point(196, 187);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(152, 58);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Защиты Q";
            // 
            // _q2Led
            // 
            this._q2Led.Location = new System.Drawing.Point(6, 38);
            this._q2Led.Name = "_q2Led";
            this._q2Led.Size = new System.Drawing.Size(13, 13);
            this._q2Led.State = BEMN.Forms.LedState.Off;
            this._q2Led.TabIndex = 75;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(25, 38);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(27, 13);
            this.label102.TabIndex = 73;
            this.label102.Text = "Q>>";
            // 
            // _q1Led
            // 
            this._q1Led.Location = new System.Drawing.Point(6, 19);
            this._q1Led.Name = "_q1Led";
            this._q1Led.Size = new System.Drawing.Size(13, 13);
            this._q1Led.State = BEMN.Forms.LedState.Off;
            this._q1Led.TabIndex = 72;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(25, 19);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(21, 13);
            this.label104.TabIndex = 70;
            this.label104.Text = "Q>";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.label309);
            this.groupBox34.Controls.Add(this.label310);
            this.groupBox34.Controls.Add(this._i8Io1);
            this.groupBox34.Controls.Add(this._i81);
            this.groupBox34.Location = new System.Drawing.Point(98, 470);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(92, 49);
            this.groupBox34.TabIndex = 31;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "I<";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.ForeColor = System.Drawing.Color.Blue;
            this.label309.Location = new System.Drawing.Point(25, 12);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(32, 13);
            this.label309.TabIndex = 83;
            this.label309.Text = "Сраб";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.ForeColor = System.Drawing.Color.Red;
            this.label310.Location = new System.Drawing.Point(6, 12);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(23, 13);
            this.label310.TabIndex = 82;
            this.label310.Text = "ИО";
            // 
            // _i8Io1
            // 
            this._i8Io1.Location = new System.Drawing.Point(9, 30);
            this._i8Io1.Name = "_i8Io1";
            this._i8Io1.Size = new System.Drawing.Size(13, 13);
            this._i8Io1.State = BEMN.Forms.LedState.Off;
            this._i8Io1.TabIndex = 15;
            // 
            // _i81
            // 
            this._i81.Location = new System.Drawing.Point(28, 30);
            this._i81.Name = "_i81";
            this._i81.Size = new System.Drawing.Size(13, 13);
            this._i81.State = BEMN.Forms.LedState.Off;
            this._i81.TabIndex = 15;
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this.label307);
            this.groupBox33.Controls.Add(this.label308);
            this.groupBox33.Controls.Add(this._i2i1IoLed);
            this.groupBox33.Controls.Add(this._i2i1Led);
            this.groupBox33.Location = new System.Drawing.Point(98, 522);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(93, 50);
            this.groupBox33.TabIndex = 30;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Защита I2/I1";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.ForeColor = System.Drawing.Color.Blue;
            this.label307.Location = new System.Drawing.Point(25, 12);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(32, 13);
            this.label307.TabIndex = 83;
            this.label307.Text = "Сраб";
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.ForeColor = System.Drawing.Color.Red;
            this.label308.Location = new System.Drawing.Point(6, 12);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(23, 13);
            this.label308.TabIndex = 82;
            this.label308.Text = "ИО";
            // 
            // _i2i1IoLed
            // 
            this._i2i1IoLed.Location = new System.Drawing.Point(9, 31);
            this._i2i1IoLed.Name = "_i2i1IoLed";
            this._i2i1IoLed.Size = new System.Drawing.Size(13, 13);
            this._i2i1IoLed.State = BEMN.Forms.LedState.Off;
            this._i2i1IoLed.TabIndex = 71;
            // 
            // _i2i1Led
            // 
            this._i2i1Led.Location = new System.Drawing.Point(28, 31);
            this._i2i1Led.Name = "_i2i1Led";
            this._i2i1Led.Size = new System.Drawing.Size(13, 13);
            this._i2i1Led.State = BEMN.Forms.LedState.Off;
            this._i2i1Led.TabIndex = 72;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label301);
            this.groupBox32.Controls.Add(this.label302);
            this.groupBox32.Controls.Add(this._f4IoLessLed);
            this.groupBox32.Controls.Add(this._f3IoLessLed);
            this.groupBox32.Controls.Add(this._f2IoLessLed);
            this.groupBox32.Controls.Add(this._f4LessLed);
            this.groupBox32.Controls.Add(this.label303);
            this.groupBox32.Controls.Add(this._f1IoLessLed);
            this.groupBox32.Controls.Add(this._f3LessLed);
            this.groupBox32.Controls.Add(this.label304);
            this.groupBox32.Controls.Add(this._f2LessLed);
            this.groupBox32.Controls.Add(this.label305);
            this.groupBox32.Controls.Add(this._f1LessLed);
            this.groupBox32.Controls.Add(this.label306);
            this.groupBox32.Location = new System.Drawing.Point(98, 356);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(92, 108);
            this.groupBox32.TabIndex = 29;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Защиты F<";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.ForeColor = System.Drawing.Color.Blue;
            this.label301.Location = new System.Drawing.Point(25, 12);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(32, 13);
            this.label301.TabIndex = 83;
            this.label301.Text = "Сраб";
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.ForeColor = System.Drawing.Color.Red;
            this.label302.Location = new System.Drawing.Point(6, 12);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(23, 13);
            this.label302.TabIndex = 82;
            this.label302.Text = "ИО";
            // 
            // _f4IoLessLed
            // 
            this._f4IoLessLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoLessLed.Name = "_f4IoLessLed";
            this._f4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f4IoLessLed.TabIndex = 80;
            // 
            // _f3IoLessLed
            // 
            this._f3IoLessLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoLessLed.Name = "_f3IoLessLed";
            this._f3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f3IoLessLed.TabIndex = 77;
            // 
            // _f2IoLessLed
            // 
            this._f2IoLessLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoLessLed.Name = "_f2IoLessLed";
            this._f2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f2IoLessLed.TabIndex = 74;
            // 
            // _f4LessLed
            // 
            this._f4LessLed.Location = new System.Drawing.Point(28, 88);
            this._f4LessLed.Name = "_f4LessLed";
            this._f4LessLed.Size = new System.Drawing.Size(13, 13);
            this._f4LessLed.State = BEMN.Forms.LedState.Off;
            this._f4LessLed.TabIndex = 81;
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(48, 88);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(25, 13);
            this.label303.TabIndex = 79;
            this.label303.Text = "F<4";
            // 
            // _f1IoLessLed
            // 
            this._f1IoLessLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoLessLed.Name = "_f1IoLessLed";
            this._f1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f1IoLessLed.TabIndex = 71;
            // 
            // _f3LessLed
            // 
            this._f3LessLed.Location = new System.Drawing.Point(28, 69);
            this._f3LessLed.Name = "_f3LessLed";
            this._f3LessLed.Size = new System.Drawing.Size(13, 13);
            this._f3LessLed.State = BEMN.Forms.LedState.Off;
            this._f3LessLed.TabIndex = 78;
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(47, 69);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(25, 13);
            this.label304.TabIndex = 76;
            this.label304.Text = "F<3";
            // 
            // _f2LessLed
            // 
            this._f2LessLed.Location = new System.Drawing.Point(28, 50);
            this._f2LessLed.Name = "_f2LessLed";
            this._f2LessLed.Size = new System.Drawing.Size(13, 13);
            this._f2LessLed.State = BEMN.Forms.LedState.Off;
            this._f2LessLed.TabIndex = 75;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(47, 50);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(25, 13);
            this.label305.TabIndex = 73;
            this.label305.Text = "F<2";
            // 
            // _f1LessLed
            // 
            this._f1LessLed.Location = new System.Drawing.Point(28, 31);
            this._f1LessLed.Name = "_f1LessLed";
            this._f1LessLed.Size = new System.Drawing.Size(13, 13);
            this._f1LessLed.State = BEMN.Forms.LedState.Off;
            this._f1LessLed.TabIndex = 72;
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(47, 31);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(25, 13);
            this.label306.TabIndex = 70;
            this.label306.Text = "F<1";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label295);
            this.groupBox31.Controls.Add(this.label296);
            this.groupBox31.Controls.Add(this._f4IoMoreLed);
            this.groupBox31.Controls.Add(this._f3IoMoreLed);
            this.groupBox31.Controls.Add(this._f2IoMoreLed);
            this.groupBox31.Controls.Add(this._f4MoreLed);
            this.groupBox31.Controls.Add(this.label297);
            this.groupBox31.Controls.Add(this._f1IoMoreLed);
            this.groupBox31.Controls.Add(this._f3MoreLed);
            this.groupBox31.Controls.Add(this.label298);
            this.groupBox31.Controls.Add(this._f2MoreLed);
            this.groupBox31.Controls.Add(this.label299);
            this.groupBox31.Controls.Add(this._f1MoreLed);
            this.groupBox31.Controls.Add(this.label300);
            this.groupBox31.Location = new System.Drawing.Point(98, 242);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(92, 108);
            this.groupBox31.TabIndex = 28;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Защиты F>";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.ForeColor = System.Drawing.Color.Blue;
            this.label295.Location = new System.Drawing.Point(25, 12);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(32, 13);
            this.label295.TabIndex = 83;
            this.label295.Text = "Сраб";
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.ForeColor = System.Drawing.Color.Red;
            this.label296.Location = new System.Drawing.Point(6, 12);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(23, 13);
            this.label296.TabIndex = 82;
            this.label296.Text = "ИО";
            // 
            // _f4IoMoreLed
            // 
            this._f4IoMoreLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoMoreLed.Name = "_f4IoMoreLed";
            this._f4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f4IoMoreLed.TabIndex = 80;
            // 
            // _f3IoMoreLed
            // 
            this._f3IoMoreLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoMoreLed.Name = "_f3IoMoreLed";
            this._f3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f3IoMoreLed.TabIndex = 77;
            // 
            // _f2IoMoreLed
            // 
            this._f2IoMoreLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoMoreLed.Name = "_f2IoMoreLed";
            this._f2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f2IoMoreLed.TabIndex = 74;
            // 
            // _f4MoreLed
            // 
            this._f4MoreLed.Location = new System.Drawing.Point(28, 88);
            this._f4MoreLed.Name = "_f4MoreLed";
            this._f4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4MoreLed.State = BEMN.Forms.LedState.Off;
            this._f4MoreLed.TabIndex = 81;
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(48, 88);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(25, 13);
            this.label297.TabIndex = 79;
            this.label297.Text = "F>4";
            // 
            // _f1IoMoreLed
            // 
            this._f1IoMoreLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoMoreLed.Name = "_f1IoMoreLed";
            this._f1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f1IoMoreLed.TabIndex = 71;
            // 
            // _f3MoreLed
            // 
            this._f3MoreLed.Location = new System.Drawing.Point(28, 69);
            this._f3MoreLed.Name = "_f3MoreLed";
            this._f3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3MoreLed.State = BEMN.Forms.LedState.Off;
            this._f3MoreLed.TabIndex = 78;
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(47, 69);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(25, 13);
            this.label298.TabIndex = 76;
            this.label298.Text = "F>3";
            // 
            // _f2MoreLed
            // 
            this._f2MoreLed.Location = new System.Drawing.Point(28, 50);
            this._f2MoreLed.Name = "_f2MoreLed";
            this._f2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2MoreLed.State = BEMN.Forms.LedState.Off;
            this._f2MoreLed.TabIndex = 75;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(47, 50);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(25, 13);
            this.label299.TabIndex = 73;
            this.label299.Text = "F>2";
            // 
            // _f1MoreLed
            // 
            this._f1MoreLed.Location = new System.Drawing.Point(28, 31);
            this._f1MoreLed.Name = "_f1MoreLed";
            this._f1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1MoreLed.State = BEMN.Forms.LedState.Off;
            this._f1MoreLed.TabIndex = 72;
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(47, 31);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(25, 13);
            this.label300.TabIndex = 70;
            this.label300.Text = "F>1";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label289);
            this.groupBox30.Controls.Add(this.label290);
            this.groupBox30.Controls.Add(this._u4IoLessLed);
            this.groupBox30.Controls.Add(this._u3IoLessLed);
            this.groupBox30.Controls.Add(this._u2IoLessLed);
            this.groupBox30.Controls.Add(this._u4LessLed);
            this.groupBox30.Controls.Add(this.label291);
            this.groupBox30.Controls.Add(this._u1IoLessLed);
            this.groupBox30.Controls.Add(this._u3LessLed);
            this.groupBox30.Controls.Add(this.label292);
            this.groupBox30.Controls.Add(this._u2LessLed);
            this.groupBox30.Controls.Add(this.label293);
            this.groupBox30.Controls.Add(this._u1LessLed);
            this.groupBox30.Controls.Add(this.label294);
            this.groupBox30.Location = new System.Drawing.Point(98, 124);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(92, 112);
            this.groupBox30.TabIndex = 27;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Защиты U<";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.ForeColor = System.Drawing.Color.Blue;
            this.label289.Location = new System.Drawing.Point(25, 12);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(32, 13);
            this.label289.TabIndex = 83;
            this.label289.Text = "Сраб";
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.ForeColor = System.Drawing.Color.Red;
            this.label290.Location = new System.Drawing.Point(6, 12);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(23, 13);
            this.label290.TabIndex = 82;
            this.label290.Text = "ИО";
            // 
            // _u4IoLessLed
            // 
            this._u4IoLessLed.Location = new System.Drawing.Point(9, 88);
            this._u4IoLessLed.Name = "_u4IoLessLed";
            this._u4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u4IoLessLed.TabIndex = 80;
            // 
            // _u3IoLessLed
            // 
            this._u3IoLessLed.Location = new System.Drawing.Point(9, 69);
            this._u3IoLessLed.Name = "_u3IoLessLed";
            this._u3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u3IoLessLed.TabIndex = 77;
            // 
            // _u2IoLessLed
            // 
            this._u2IoLessLed.Location = new System.Drawing.Point(9, 50);
            this._u2IoLessLed.Name = "_u2IoLessLed";
            this._u2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u2IoLessLed.TabIndex = 74;
            // 
            // _u4LessLed
            // 
            this._u4LessLed.Location = new System.Drawing.Point(28, 88);
            this._u4LessLed.Name = "_u4LessLed";
            this._u4LessLed.Size = new System.Drawing.Size(13, 13);
            this._u4LessLed.State = BEMN.Forms.LedState.Off;
            this._u4LessLed.TabIndex = 81;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(48, 88);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(27, 13);
            this.label291.TabIndex = 79;
            this.label291.Text = "U<4";
            // 
            // _u1IoLessLed
            // 
            this._u1IoLessLed.Location = new System.Drawing.Point(9, 31);
            this._u1IoLessLed.Name = "_u1IoLessLed";
            this._u1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u1IoLessLed.TabIndex = 71;
            // 
            // _u3LessLed
            // 
            this._u3LessLed.Location = new System.Drawing.Point(28, 69);
            this._u3LessLed.Name = "_u3LessLed";
            this._u3LessLed.Size = new System.Drawing.Size(13, 13);
            this._u3LessLed.State = BEMN.Forms.LedState.Off;
            this._u3LessLed.TabIndex = 78;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(47, 69);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(27, 13);
            this.label292.TabIndex = 76;
            this.label292.Text = "U<3";
            // 
            // _u2LessLed
            // 
            this._u2LessLed.Location = new System.Drawing.Point(28, 50);
            this._u2LessLed.Name = "_u2LessLed";
            this._u2LessLed.Size = new System.Drawing.Size(13, 13);
            this._u2LessLed.State = BEMN.Forms.LedState.Off;
            this._u2LessLed.TabIndex = 75;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(47, 50);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(27, 13);
            this.label293.TabIndex = 73;
            this.label293.Text = "U<2";
            // 
            // _u1LessLed
            // 
            this._u1LessLed.Location = new System.Drawing.Point(28, 31);
            this._u1LessLed.Name = "_u1LessLed";
            this._u1LessLed.Size = new System.Drawing.Size(13, 13);
            this._u1LessLed.State = BEMN.Forms.LedState.Off;
            this._u1LessLed.TabIndex = 72;
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(47, 31);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(27, 13);
            this.label294.TabIndex = 70;
            this.label294.Text = "U<1";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label283);
            this.groupBox29.Controls.Add(this.label284);
            this.groupBox29.Controls.Add(this._u4IoMoreLed);
            this.groupBox29.Controls.Add(this._u3IoMoreLed);
            this.groupBox29.Controls.Add(this._u2IoMoreLed);
            this.groupBox29.Controls.Add(this._u4MoreLed);
            this.groupBox29.Controls.Add(this.label285);
            this.groupBox29.Controls.Add(this._u1IoMoreLed);
            this.groupBox29.Controls.Add(this._u3MoreLed);
            this.groupBox29.Controls.Add(this.label286);
            this.groupBox29.Controls.Add(this._u2MoreLed);
            this.groupBox29.Controls.Add(this.label287);
            this.groupBox29.Controls.Add(this._u1MoreLed);
            this.groupBox29.Controls.Add(this.label288);
            this.groupBox29.Location = new System.Drawing.Point(98, 6);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(92, 112);
            this.groupBox29.TabIndex = 26;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Защиты U>";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.ForeColor = System.Drawing.Color.Blue;
            this.label283.Location = new System.Drawing.Point(25, 12);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(32, 13);
            this.label283.TabIndex = 83;
            this.label283.Text = "Сраб";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.ForeColor = System.Drawing.Color.Red;
            this.label284.Location = new System.Drawing.Point(6, 12);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(23, 13);
            this.label284.TabIndex = 82;
            this.label284.Text = "ИО";
            // 
            // _u4IoMoreLed
            // 
            this._u4IoMoreLed.Location = new System.Drawing.Point(9, 88);
            this._u4IoMoreLed.Name = "_u4IoMoreLed";
            this._u4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u4IoMoreLed.TabIndex = 80;
            // 
            // _u3IoMoreLed
            // 
            this._u3IoMoreLed.Location = new System.Drawing.Point(9, 69);
            this._u3IoMoreLed.Name = "_u3IoMoreLed";
            this._u3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u3IoMoreLed.TabIndex = 77;
            // 
            // _u2IoMoreLed
            // 
            this._u2IoMoreLed.Location = new System.Drawing.Point(9, 50);
            this._u2IoMoreLed.Name = "_u2IoMoreLed";
            this._u2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u2IoMoreLed.TabIndex = 74;
            // 
            // _u4MoreLed
            // 
            this._u4MoreLed.Location = new System.Drawing.Point(28, 88);
            this._u4MoreLed.Name = "_u4MoreLed";
            this._u4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4MoreLed.State = BEMN.Forms.LedState.Off;
            this._u4MoreLed.TabIndex = 81;
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(48, 88);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(27, 13);
            this.label285.TabIndex = 79;
            this.label285.Text = "U>4";
            // 
            // _u1IoMoreLed
            // 
            this._u1IoMoreLed.Location = new System.Drawing.Point(9, 31);
            this._u1IoMoreLed.Name = "_u1IoMoreLed";
            this._u1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u1IoMoreLed.TabIndex = 71;
            // 
            // _u3MoreLed
            // 
            this._u3MoreLed.Location = new System.Drawing.Point(28, 69);
            this._u3MoreLed.Name = "_u3MoreLed";
            this._u3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3MoreLed.State = BEMN.Forms.LedState.Off;
            this._u3MoreLed.TabIndex = 78;
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(47, 69);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(27, 13);
            this.label286.TabIndex = 76;
            this.label286.Text = "U>3";
            // 
            // _u2MoreLed
            // 
            this._u2MoreLed.Location = new System.Drawing.Point(28, 50);
            this._u2MoreLed.Name = "_u2MoreLed";
            this._u2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2MoreLed.State = BEMN.Forms.LedState.Off;
            this._u2MoreLed.TabIndex = 75;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(47, 50);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(27, 13);
            this.label287.TabIndex = 73;
            this.label287.Text = "U>2";
            // 
            // _u1MoreLed
            // 
            this._u1MoreLed.Location = new System.Drawing.Point(28, 31);
            this._u1MoreLed.Name = "_u1MoreLed";
            this._u1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1MoreLed.State = BEMN.Forms.LedState.Off;
            this._u1MoreLed.TabIndex = 72;
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(47, 31);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(27, 13);
            this.label288.TabIndex = 70;
            this.label288.Text = "U>1";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Transparent;
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Controls.Add(this._faultSwitchOff);
            this.groupBox19.Controls.Add(this._faultMeasuringf);
            this.groupBox19.Controls.Add(this.label96);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.label79);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultPass);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this._faultMeasuringU);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Location = new System.Drawing.Point(540, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(226, 194);
            this.groupBox19.TabIndex = 13;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(25, 95);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(76, 13);
            this.label344.TabIndex = 35;
            this.label344.Text = "Выключателя";
            // 
            // _faultSwitchOff
            // 
            this._faultSwitchOff.Location = new System.Drawing.Point(6, 95);
            this._faultSwitchOff.Name = "_faultSwitchOff";
            this._faultSwitchOff.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff.TabIndex = 34;
            // 
            // _faultMeasuringf
            // 
            this._faultMeasuringf.Location = new System.Drawing.Point(6, 76);
            this._faultMeasuringf.Name = "_faultMeasuringf";
            this._faultMeasuringf.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringf.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringf.TabIndex = 31;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(25, 76);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(74, 13);
            this.label96.TabIndex = 30;
            this.label96.Text = "Измерения F";
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(117, 57);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(136, 57);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "ЖА";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(117, 19);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(136, 19);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(84, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осциллограмы";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(117, 152);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(117, 38);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(136, 38);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "ЖС";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(136, 152);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 152);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(25, 152);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(117, 133);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 114);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 9;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(25, 114);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(44, 13);
            this.label79.TabIndex = 8;
            this.label79.Text = "Логики";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 133);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(25, 133);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(136, 133);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultPass
            // 
            this._faultPass.Location = new System.Drawing.Point(6, 171);
            this._faultPass.Name = "_faultPass";
            this._faultPass.Size = new System.Drawing.Size(13, 13);
            this._faultPass.State = BEMN.Forms.LedState.Off;
            this._faultPass.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(25, 171);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(45, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Пароля";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(117, 114);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // _faultMeasuringU
            // 
            this._faultMeasuringU.Location = new System.Drawing.Point(6, 57);
            this._faultMeasuringU.Name = "_faultMeasuringU";
            this._faultMeasuringU.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringU.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringU.TabIndex = 5;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(25, 57);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(76, 13);
            this.label204.TabIndex = 4;
            this.label204.Text = "Измерения U";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(136, 114);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(117, 95);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(136, 95);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(117, 76);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(136, 76);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(933, 599);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.groupBox16);
            this.groupBox28.Controls.Add(this.groupBox15);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(328, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(223, 101);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._currenGroupLabel);
            this.groupBox16.Location = new System.Drawing.Point(114, 14);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(103, 81);
            this.groupBox16.TabIndex = 32;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Текущая группа";
            // 
            // _currenGroupLabel
            // 
            this._currenGroupLabel.AutoSize = true;
            this._currenGroupLabel.Location = new System.Drawing.Point(17, 33);
            this._currenGroupLabel.Name = "_currenGroupLabel";
            this._currenGroupLabel.Size = new System.Drawing.Size(62, 13);
            this._currenGroupLabel.TabIndex = 0;
            this._currenGroupLabel.Text = "Группа №1";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._groupCombo);
            this.groupBox15.Controls.Add(this._switchGroupButton);
            this.groupBox15.Location = new System.Drawing.Point(6, 14);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(102, 81);
            this.groupBox15.TabIndex = 32;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Выбрать группу";
            // 
            // _groupCombo
            // 
            this._groupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._groupCombo.FormattingEnabled = true;
            this._groupCombo.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2",
            "Группа 3",
            "Группа 4",
            "Группа 5",
            "Группа 6"});
            this._groupCombo.Location = new System.Drawing.Point(6, 25);
            this._groupCombo.Name = "_groupCombo";
            this._groupCombo.Size = new System.Drawing.Size(86, 21);
            this._groupCombo.TabIndex = 31;
            // 
            // _switchGroupButton
            // 
            this._switchGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._switchGroupButton.Location = new System.Drawing.Point(6, 52);
            this._switchGroupButton.Name = "_switchGroupButton";
            this._switchGroupButton.Size = new System.Drawing.Size(86, 23);
            this._switchGroupButton.TabIndex = 30;
            this._switchGroupButton.Text = "Переключить";
            this._switchGroupButton.UseVisualStyleBackColor = true;
            this._switchGroupButton.Click += new System.EventHandler(this._switchGroupButton_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._switchOn);
            this.groupBox27.Controls.Add(this._switchOff);
            this.groupBox27.Controls.Add(this._startOscBtn);
            this.groupBox27.Controls.Add(this._resetTermStateButton);
            this.groupBox27.Controls.Add(this._faultTnInd);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetFaultTnBtn);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this.label223);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 329);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 176);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 54;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.StopLogicBtnClick);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.StartLogicBtnClick);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 37);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _switchOn
            // 
            this._switchOn.Location = new System.Drawing.Point(6, 42);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(13, 13);
            this._switchOn.State = BEMN.Forms.LedState.Off;
            this._switchOn.TabIndex = 49;
            // 
            // _switchOff
            // 
            this._switchOff.Location = new System.Drawing.Point(6, 19);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(13, 13);
            this._switchOff.State = BEMN.Forms.LedState.Off;
            this._switchOff.TabIndex = 48;
            // 
            // _startOscBtn
            // 
            this._startOscBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._startOscBtn.Location = new System.Drawing.Point(6, 300);
            this._startOscBtn.Name = "_startOscBtn";
            this._startOscBtn.Size = new System.Drawing.Size(304, 23);
            this._startOscBtn.TabIndex = 14;
            this._startOscBtn.Text = "Пуск осциллографа";
            this._startOscBtn.UseVisualStyleBackColor = true;
            this._startOscBtn.Click += new System.EventHandler(this._startOscBtnClick);
            // 
            // _resetTermStateButton
            // 
            this._resetTermStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetTermStateButton.Location = new System.Drawing.Point(6, 271);
            this._resetTermStateButton.Name = "_resetTermStateButton";
            this._resetTermStateButton.Size = new System.Drawing.Size(304, 23);
            this._resetTermStateButton.TabIndex = 14;
            this._resetTermStateButton.Text = "Сбросить состояние тепловой модели";
            this._resetTermStateButton.UseVisualStyleBackColor = true;
            this._resetTermStateButton.Click += new System.EventHandler(this._resetTermStateButton_Click);
            // 
            // _faultTnInd
            // 
            this._faultTnInd.Location = new System.Drawing.Point(6, 157);
            this._faultTnInd.Name = "_faultTnInd";
            this._faultTnInd.Size = new System.Drawing.Size(13, 13);
            this._faultTnInd.State = BEMN.Forms.LedState.Off;
            this._faultTnInd.TabIndex = 13;
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 134);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 111);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 88);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 242);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс блинкеров";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetFaultTnBtn
            // 
            this._resetFaultTnBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetFaultTnBtn.Location = new System.Drawing.Point(235, 152);
            this._resetFaultTnBtn.Name = "_resetFaultTnBtn";
            this._resetFaultTnBtn.Size = new System.Drawing.Size(75, 23);
            this._resetFaultTnBtn.TabIndex = 8;
            this._resetFaultTnBtn.Text = "Сбросить";
            this._resetFaultTnBtn.UseVisualStyleBackColor = true;
            this._resetFaultTnBtn.Click += new System.EventHandler(this._resetFaultTnBtnClick);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 129);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 106);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label223.Location = new System.Drawing.Point(30, 157);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(104, 13);
            this.label223.TabIndex = 3;
            this.label223.Text = "Неисправность ТН";
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 134);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 111);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая запись журнала осциллографа";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 88);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 65);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // BGS_Group
            // 
            this.BGS_Group.Controls.Add(this._bgs16);
            this.BGS_Group.Controls.Add(this.label352);
            this.BGS_Group.Controls.Add(this._bgs15);
            this.BGS_Group.Controls.Add(this.label412);
            this.BGS_Group.Controls.Add(this._bgs14);
            this.BGS_Group.Controls.Add(this.label454);
            this.BGS_Group.Controls.Add(this._bgs13);
            this.BGS_Group.Controls.Add(this.label457);
            this.BGS_Group.Controls.Add(this._bgs12);
            this.BGS_Group.Controls.Add(this.label458);
            this.BGS_Group.Controls.Add(this._bgs11);
            this.BGS_Group.Controls.Add(this.label459);
            this.BGS_Group.Controls.Add(this._bgs10);
            this.BGS_Group.Controls.Add(this.label460);
            this.BGS_Group.Controls.Add(this._bgs9);
            this.BGS_Group.Controls.Add(this.label461);
            this.BGS_Group.Controls.Add(this._bgs8);
            this.BGS_Group.Controls.Add(this.label462);
            this.BGS_Group.Controls.Add(this._bgs7);
            this.BGS_Group.Controls.Add(this.label463);
            this.BGS_Group.Controls.Add(this._bgs6);
            this.BGS_Group.Controls.Add(this.label464);
            this.BGS_Group.Controls.Add(this._bgs5);
            this.BGS_Group.Controls.Add(this.label465);
            this.BGS_Group.Controls.Add(this._bgs4);
            this.BGS_Group.Controls.Add(this.label466);
            this.BGS_Group.Controls.Add(this._bgs3);
            this.BGS_Group.Controls.Add(this.label467);
            this.BGS_Group.Controls.Add(this._bgs2);
            this.BGS_Group.Controls.Add(this.label468);
            this.BGS_Group.Controls.Add(this._bgs1);
            this.BGS_Group.Controls.Add(this.label469);
            this.BGS_Group.Location = new System.Drawing.Point(769, 370);
            this.BGS_Group.Name = "BGS_Group";
            this.BGS_Group.Size = new System.Drawing.Size(152, 183);
            this.BGS_Group.TabIndex = 68;
            this.BGS_Group.TabStop = false;
            this.BGS_Group.Text = "Сигналы БГС";
            // 
            // _bgs16
            // 
            this._bgs16.Location = new System.Drawing.Point(71, 152);
            this._bgs16.Name = "_bgs16";
            this._bgs16.Size = new System.Drawing.Size(13, 13);
            this._bgs16.State = BEMN.Forms.LedState.Off;
            this._bgs16.TabIndex = 31;
            // 
            // label352
            // 
            this.label352.AutoSize = true;
            this.label352.Location = new System.Drawing.Point(90, 152);
            this.label352.Name = "label352";
            this.label352.Size = new System.Drawing.Size(39, 13);
            this.label352.TabIndex = 30;
            this.label352.Text = "БГС16";
            // 
            // _bgs15
            // 
            this._bgs15.Location = new System.Drawing.Point(71, 133);
            this._bgs15.Name = "_bgs15";
            this._bgs15.Size = new System.Drawing.Size(13, 13);
            this._bgs15.State = BEMN.Forms.LedState.Off;
            this._bgs15.TabIndex = 29;
            // 
            // label412
            // 
            this.label412.AutoSize = true;
            this.label412.Location = new System.Drawing.Point(90, 133);
            this.label412.Name = "label412";
            this.label412.Size = new System.Drawing.Size(39, 13);
            this.label412.TabIndex = 28;
            this.label412.Text = "БГС15";
            // 
            // _bgs14
            // 
            this._bgs14.Location = new System.Drawing.Point(71, 114);
            this._bgs14.Name = "_bgs14";
            this._bgs14.Size = new System.Drawing.Size(13, 13);
            this._bgs14.State = BEMN.Forms.LedState.Off;
            this._bgs14.TabIndex = 27;
            // 
            // label454
            // 
            this.label454.AutoSize = true;
            this.label454.Location = new System.Drawing.Point(90, 114);
            this.label454.Name = "label454";
            this.label454.Size = new System.Drawing.Size(39, 13);
            this.label454.TabIndex = 26;
            this.label454.Text = "БГС14";
            // 
            // _bgs13
            // 
            this._bgs13.Location = new System.Drawing.Point(71, 95);
            this._bgs13.Name = "_bgs13";
            this._bgs13.Size = new System.Drawing.Size(13, 13);
            this._bgs13.State = BEMN.Forms.LedState.Off;
            this._bgs13.TabIndex = 25;
            // 
            // label457
            // 
            this.label457.AutoSize = true;
            this.label457.Location = new System.Drawing.Point(90, 95);
            this.label457.Name = "label457";
            this.label457.Size = new System.Drawing.Size(39, 13);
            this.label457.TabIndex = 24;
            this.label457.Text = "БГС13";
            // 
            // _bgs12
            // 
            this._bgs12.Location = new System.Drawing.Point(71, 76);
            this._bgs12.Name = "_bgs12";
            this._bgs12.Size = new System.Drawing.Size(13, 13);
            this._bgs12.State = BEMN.Forms.LedState.Off;
            this._bgs12.TabIndex = 23;
            // 
            // label458
            // 
            this.label458.AutoSize = true;
            this.label458.Location = new System.Drawing.Point(90, 76);
            this.label458.Name = "label458";
            this.label458.Size = new System.Drawing.Size(39, 13);
            this.label458.TabIndex = 22;
            this.label458.Text = "БГС12";
            // 
            // _bgs11
            // 
            this._bgs11.Location = new System.Drawing.Point(71, 57);
            this._bgs11.Name = "_bgs11";
            this._bgs11.Size = new System.Drawing.Size(13, 13);
            this._bgs11.State = BEMN.Forms.LedState.Off;
            this._bgs11.TabIndex = 21;
            // 
            // label459
            // 
            this.label459.AutoSize = true;
            this.label459.Location = new System.Drawing.Point(90, 57);
            this.label459.Name = "label459";
            this.label459.Size = new System.Drawing.Size(39, 13);
            this.label459.TabIndex = 20;
            this.label459.Text = "БГС11";
            // 
            // _bgs10
            // 
            this._bgs10.Location = new System.Drawing.Point(71, 38);
            this._bgs10.Name = "_bgs10";
            this._bgs10.Size = new System.Drawing.Size(13, 13);
            this._bgs10.State = BEMN.Forms.LedState.Off;
            this._bgs10.TabIndex = 19;
            // 
            // label460
            // 
            this.label460.AutoSize = true;
            this.label460.Location = new System.Drawing.Point(90, 38);
            this.label460.Name = "label460";
            this.label460.Size = new System.Drawing.Size(39, 13);
            this.label460.TabIndex = 18;
            this.label460.Text = "БГС10";
            // 
            // _bgs9
            // 
            this._bgs9.Location = new System.Drawing.Point(71, 19);
            this._bgs9.Name = "_bgs9";
            this._bgs9.Size = new System.Drawing.Size(13, 13);
            this._bgs9.State = BEMN.Forms.LedState.Off;
            this._bgs9.TabIndex = 17;
            // 
            // label461
            // 
            this.label461.AutoSize = true;
            this.label461.Location = new System.Drawing.Point(90, 19);
            this.label461.Name = "label461";
            this.label461.Size = new System.Drawing.Size(33, 13);
            this.label461.TabIndex = 16;
            this.label461.Text = "БГС9";
            // 
            // _bgs8
            // 
            this._bgs8.Location = new System.Drawing.Point(6, 152);
            this._bgs8.Name = "_bgs8";
            this._bgs8.Size = new System.Drawing.Size(13, 13);
            this._bgs8.State = BEMN.Forms.LedState.Off;
            this._bgs8.TabIndex = 15;
            // 
            // label462
            // 
            this.label462.AutoSize = true;
            this.label462.Location = new System.Drawing.Point(25, 152);
            this.label462.Name = "label462";
            this.label462.Size = new System.Drawing.Size(33, 13);
            this.label462.TabIndex = 14;
            this.label462.Text = "БГС8";
            // 
            // _bgs7
            // 
            this._bgs7.Location = new System.Drawing.Point(6, 133);
            this._bgs7.Name = "_bgs7";
            this._bgs7.Size = new System.Drawing.Size(13, 13);
            this._bgs7.State = BEMN.Forms.LedState.Off;
            this._bgs7.TabIndex = 13;
            // 
            // label463
            // 
            this.label463.AutoSize = true;
            this.label463.Location = new System.Drawing.Point(25, 133);
            this.label463.Name = "label463";
            this.label463.Size = new System.Drawing.Size(33, 13);
            this.label463.TabIndex = 12;
            this.label463.Text = "БГС7";
            // 
            // _bgs6
            // 
            this._bgs6.Location = new System.Drawing.Point(6, 114);
            this._bgs6.Name = "_bgs6";
            this._bgs6.Size = new System.Drawing.Size(13, 13);
            this._bgs6.State = BEMN.Forms.LedState.Off;
            this._bgs6.TabIndex = 11;
            // 
            // label464
            // 
            this.label464.AutoSize = true;
            this.label464.Location = new System.Drawing.Point(25, 114);
            this.label464.Name = "label464";
            this.label464.Size = new System.Drawing.Size(33, 13);
            this.label464.TabIndex = 10;
            this.label464.Text = "БГС6";
            // 
            // _bgs5
            // 
            this._bgs5.Location = new System.Drawing.Point(6, 95);
            this._bgs5.Name = "_bgs5";
            this._bgs5.Size = new System.Drawing.Size(13, 13);
            this._bgs5.State = BEMN.Forms.LedState.Off;
            this._bgs5.TabIndex = 9;
            // 
            // label465
            // 
            this.label465.AutoSize = true;
            this.label465.Location = new System.Drawing.Point(25, 95);
            this.label465.Name = "label465";
            this.label465.Size = new System.Drawing.Size(33, 13);
            this.label465.TabIndex = 8;
            this.label465.Text = "БГС5";
            // 
            // _bgs4
            // 
            this._bgs4.Location = new System.Drawing.Point(6, 76);
            this._bgs4.Name = "_bgs4";
            this._bgs4.Size = new System.Drawing.Size(13, 13);
            this._bgs4.State = BEMN.Forms.LedState.Off;
            this._bgs4.TabIndex = 7;
            // 
            // label466
            // 
            this.label466.AutoSize = true;
            this.label466.Location = new System.Drawing.Point(25, 76);
            this.label466.Name = "label466";
            this.label466.Size = new System.Drawing.Size(33, 13);
            this.label466.TabIndex = 6;
            this.label466.Text = "БГС4";
            // 
            // _bgs3
            // 
            this._bgs3.Location = new System.Drawing.Point(6, 57);
            this._bgs3.Name = "_bgs3";
            this._bgs3.Size = new System.Drawing.Size(13, 13);
            this._bgs3.State = BEMN.Forms.LedState.Off;
            this._bgs3.TabIndex = 5;
            // 
            // label467
            // 
            this.label467.AutoSize = true;
            this.label467.Location = new System.Drawing.Point(25, 57);
            this.label467.Name = "label467";
            this.label467.Size = new System.Drawing.Size(33, 13);
            this.label467.TabIndex = 4;
            this.label467.Text = "БГС3";
            // 
            // _bgs2
            // 
            this._bgs2.Location = new System.Drawing.Point(6, 38);
            this._bgs2.Name = "_bgs2";
            this._bgs2.Size = new System.Drawing.Size(13, 13);
            this._bgs2.State = BEMN.Forms.LedState.Off;
            this._bgs2.TabIndex = 3;
            // 
            // label468
            // 
            this.label468.AutoSize = true;
            this.label468.Location = new System.Drawing.Point(25, 38);
            this.label468.Name = "label468";
            this.label468.Size = new System.Drawing.Size(33, 13);
            this.label468.TabIndex = 2;
            this.label468.Text = "БГС2";
            // 
            // _bgs1
            // 
            this._bgs1.Location = new System.Drawing.Point(6, 19);
            this._bgs1.Name = "_bgs1";
            this._bgs1.Size = new System.Drawing.Size(13, 13);
            this._bgs1.State = BEMN.Forms.LedState.Off;
            this._bgs1.TabIndex = 1;
            // 
            // label469
            // 
            this.label469.AutoSize = true;
            this.label469.Location = new System.Drawing.Point(25, 19);
            this.label469.Name = "label469";
            this.label469.Size = new System.Drawing.Size(33, 13);
            this.label469.TabIndex = 0;
            this.label469.Text = "БГС1";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(269, 19);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(41, 13);
            this.label224.TabIndex = 48;
            this.label224.Text = "ССЛ33";
            // 
            // _ssl33
            // 
            this._ssl33.Location = new System.Drawing.Point(253, 19);
            this._ssl33.Name = "_ssl33";
            this._ssl33.Size = new System.Drawing.Size(13, 13);
            this._ssl33.State = BEMN.Forms.LedState.Off;
            this._ssl33.TabIndex = 49;
            // 
            // label470
            // 
            this.label470.AutoSize = true;
            this.label470.Location = new System.Drawing.Point(269, 38);
            this.label470.Name = "label470";
            this.label470.Size = new System.Drawing.Size(41, 13);
            this.label470.TabIndex = 50;
            this.label470.Text = "ССЛ34";
            // 
            // _ssl34
            // 
            this._ssl34.Location = new System.Drawing.Point(253, 38);
            this._ssl34.Name = "_ssl34";
            this._ssl34.Size = new System.Drawing.Size(13, 13);
            this._ssl34.State = BEMN.Forms.LedState.Off;
            this._ssl34.TabIndex = 51;
            // 
            // label471
            // 
            this.label471.AutoSize = true;
            this.label471.Location = new System.Drawing.Point(269, 57);
            this.label471.Name = "label471";
            this.label471.Size = new System.Drawing.Size(41, 13);
            this.label471.TabIndex = 52;
            this.label471.Text = "ССЛ35";
            // 
            // _ssl35
            // 
            this._ssl35.Location = new System.Drawing.Point(253, 57);
            this._ssl35.Name = "_ssl35";
            this._ssl35.Size = new System.Drawing.Size(13, 13);
            this._ssl35.State = BEMN.Forms.LedState.Off;
            this._ssl35.TabIndex = 53;
            // 
            // label472
            // 
            this.label472.AutoSize = true;
            this.label472.Location = new System.Drawing.Point(269, 76);
            this.label472.Name = "label472";
            this.label472.Size = new System.Drawing.Size(41, 13);
            this.label472.TabIndex = 54;
            this.label472.Text = "ССЛ36";
            // 
            // _ssl36
            // 
            this._ssl36.Location = new System.Drawing.Point(253, 76);
            this._ssl36.Name = "_ssl36";
            this._ssl36.Size = new System.Drawing.Size(13, 13);
            this._ssl36.State = BEMN.Forms.LedState.Off;
            this._ssl36.TabIndex = 55;
            // 
            // label473
            // 
            this.label473.AutoSize = true;
            this.label473.Location = new System.Drawing.Point(269, 95);
            this.label473.Name = "label473";
            this.label473.Size = new System.Drawing.Size(41, 13);
            this.label473.TabIndex = 56;
            this.label473.Text = "ССЛ37";
            // 
            // _ssl37
            // 
            this._ssl37.Location = new System.Drawing.Point(253, 95);
            this._ssl37.Name = "_ssl37";
            this._ssl37.Size = new System.Drawing.Size(13, 13);
            this._ssl37.State = BEMN.Forms.LedState.Off;
            this._ssl37.TabIndex = 57;
            // 
            // label474
            // 
            this.label474.AutoSize = true;
            this.label474.Location = new System.Drawing.Point(269, 114);
            this.label474.Name = "label474";
            this.label474.Size = new System.Drawing.Size(41, 13);
            this.label474.TabIndex = 58;
            this.label474.Text = "ССЛ38";
            // 
            // _ssl38
            // 
            this._ssl38.Location = new System.Drawing.Point(253, 114);
            this._ssl38.Name = "_ssl38";
            this._ssl38.Size = new System.Drawing.Size(13, 13);
            this._ssl38.State = BEMN.Forms.LedState.Off;
            this._ssl38.TabIndex = 59;
            // 
            // label475
            // 
            this.label475.AutoSize = true;
            this.label475.Location = new System.Drawing.Point(269, 133);
            this.label475.Name = "label475";
            this.label475.Size = new System.Drawing.Size(41, 13);
            this.label475.TabIndex = 60;
            this.label475.Text = "ССЛ39";
            // 
            // _ssl39
            // 
            this._ssl39.Location = new System.Drawing.Point(253, 133);
            this._ssl39.Name = "_ssl39";
            this._ssl39.Size = new System.Drawing.Size(13, 13);
            this._ssl39.State = BEMN.Forms.LedState.Off;
            this._ssl39.TabIndex = 61;
            // 
            // label476
            // 
            this.label476.AutoSize = true;
            this.label476.Location = new System.Drawing.Point(269, 152);
            this.label476.Name = "label476";
            this.label476.Size = new System.Drawing.Size(41, 13);
            this.label476.TabIndex = 62;
            this.label476.Text = "ССЛ40";
            // 
            // _ssl40
            // 
            this._ssl40.Location = new System.Drawing.Point(253, 152);
            this._ssl40.Name = "_ssl40";
            this._ssl40.Size = new System.Drawing.Size(13, 13);
            this._ssl40.State = BEMN.Forms.LedState.Off;
            this._ssl40.TabIndex = 63;
            // 
            // label477
            // 
            this.label477.AutoSize = true;
            this.label477.Location = new System.Drawing.Point(332, 19);
            this.label477.Name = "label477";
            this.label477.Size = new System.Drawing.Size(41, 13);
            this.label477.TabIndex = 48;
            this.label477.Text = "ССЛ41";
            // 
            // _ssl41
            // 
            this._ssl41.Location = new System.Drawing.Point(316, 19);
            this._ssl41.Name = "_ssl41";
            this._ssl41.Size = new System.Drawing.Size(13, 13);
            this._ssl41.State = BEMN.Forms.LedState.Off;
            this._ssl41.TabIndex = 49;
            // 
            // label478
            // 
            this.label478.AutoSize = true;
            this.label478.Location = new System.Drawing.Point(332, 38);
            this.label478.Name = "label478";
            this.label478.Size = new System.Drawing.Size(41, 13);
            this.label478.TabIndex = 50;
            this.label478.Text = "ССЛ42";
            // 
            // _ssl42
            // 
            this._ssl42.Location = new System.Drawing.Point(316, 38);
            this._ssl42.Name = "_ssl42";
            this._ssl42.Size = new System.Drawing.Size(13, 13);
            this._ssl42.State = BEMN.Forms.LedState.Off;
            this._ssl42.TabIndex = 51;
            // 
            // label479
            // 
            this.label479.AutoSize = true;
            this.label479.Location = new System.Drawing.Point(332, 57);
            this.label479.Name = "label479";
            this.label479.Size = new System.Drawing.Size(41, 13);
            this.label479.TabIndex = 52;
            this.label479.Text = "ССЛ43";
            // 
            // _ssl43
            // 
            this._ssl43.Location = new System.Drawing.Point(316, 57);
            this._ssl43.Name = "_ssl43";
            this._ssl43.Size = new System.Drawing.Size(13, 13);
            this._ssl43.State = BEMN.Forms.LedState.Off;
            this._ssl43.TabIndex = 53;
            // 
            // label480
            // 
            this.label480.AutoSize = true;
            this.label480.Location = new System.Drawing.Point(332, 76);
            this.label480.Name = "label480";
            this.label480.Size = new System.Drawing.Size(41, 13);
            this.label480.TabIndex = 54;
            this.label480.Text = "ССЛ44";
            // 
            // _ssl44
            // 
            this._ssl44.Location = new System.Drawing.Point(316, 76);
            this._ssl44.Name = "_ssl44";
            this._ssl44.Size = new System.Drawing.Size(13, 13);
            this._ssl44.State = BEMN.Forms.LedState.Off;
            this._ssl44.TabIndex = 55;
            // 
            // label481
            // 
            this.label481.AutoSize = true;
            this.label481.Location = new System.Drawing.Point(332, 95);
            this.label481.Name = "label481";
            this.label481.Size = new System.Drawing.Size(41, 13);
            this.label481.TabIndex = 56;
            this.label481.Text = "ССЛ45";
            // 
            // _ssl45
            // 
            this._ssl45.Location = new System.Drawing.Point(316, 95);
            this._ssl45.Name = "_ssl45";
            this._ssl45.Size = new System.Drawing.Size(13, 13);
            this._ssl45.State = BEMN.Forms.LedState.Off;
            this._ssl45.TabIndex = 57;
            // 
            // label482
            // 
            this.label482.AutoSize = true;
            this.label482.Location = new System.Drawing.Point(332, 114);
            this.label482.Name = "label482";
            this.label482.Size = new System.Drawing.Size(41, 13);
            this.label482.TabIndex = 58;
            this.label482.Text = "ССЛ46";
            // 
            // _ssl46
            // 
            this._ssl46.Location = new System.Drawing.Point(316, 114);
            this._ssl46.Name = "_ssl46";
            this._ssl46.Size = new System.Drawing.Size(13, 13);
            this._ssl46.State = BEMN.Forms.LedState.Off;
            this._ssl46.TabIndex = 59;
            // 
            // label483
            // 
            this.label483.AutoSize = true;
            this.label483.Location = new System.Drawing.Point(332, 133);
            this.label483.Name = "label483";
            this.label483.Size = new System.Drawing.Size(41, 13);
            this.label483.TabIndex = 60;
            this.label483.Text = "ССЛ47";
            // 
            // _ssl47
            // 
            this._ssl47.Location = new System.Drawing.Point(316, 133);
            this._ssl47.Name = "_ssl47";
            this._ssl47.Size = new System.Drawing.Size(13, 13);
            this._ssl47.State = BEMN.Forms.LedState.Off;
            this._ssl47.TabIndex = 61;
            // 
            // label484
            // 
            this.label484.AutoSize = true;
            this.label484.Location = new System.Drawing.Point(332, 152);
            this.label484.Name = "label484";
            this.label484.Size = new System.Drawing.Size(41, 13);
            this.label484.TabIndex = 62;
            this.label484.Text = "ССЛ48";
            // 
            // _ssl48
            // 
            this._ssl48.Location = new System.Drawing.Point(316, 152);
            this._ssl48.Name = "_ssl48";
            this._ssl48.Size = new System.Drawing.Size(13, 13);
            this._ssl48.State = BEMN.Forms.LedState.Off;
            this._ssl48.TabIndex = 63;
            // 
            // Mr762MeasuringFormV300
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 625);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr762MeasuringFormV300";
            this.Text = "MeasuringForm";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            _analogTabPage.ResumeLayout(false);
            this.groupBox47.ResumeLayout(false);
            this.groupBox47.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this.groupBox45.ResumeLayout(false);
            this.groupBox45.PerformLayout();
            this.groupBox46.ResumeLayout(false);
            this.groupBox46.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox43.ResumeLayout(false);
            this.groupBox43.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._dataBaseTabControl.ResumeLayout(false);
            this._discretTabPage1.ResumeLayout(false);
            this.reversGroup.ResumeLayout(false);
            this.reversGroup.PerformLayout();
            this.groupBox53.ResumeLayout(false);
            this.groupBox53.PerformLayout();
            this.groupBox52.ResumeLayout(false);
            this.groupBox52.PerformLayout();
            this.groupBox50.ResumeLayout(false);
            this.groupBox50.PerformLayout();
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.dugGroup.ResumeLayout(false);
            this.dugGroup.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox54.ResumeLayout(false);
            this.groupBox54.PerformLayout();
            this.groupBox65.ResumeLayout(false);
            this.groupBox65.PerformLayout();
            this.groupBox63.ResumeLayout(false);
            this.groupBox63.PerformLayout();
            this.urovGroup.ResumeLayout(false);
            this.urovGroup.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.splGroupBox.ResumeLayout(false);
            this.splGroupBox.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._discretTabPage2.ResumeLayout(false);
            this.reversGroup1.ResumeLayout(false);
            this.reversGroup1.PerformLayout();
            this.dugGroup1.ResumeLayout(false);
            this.dugGroup1.PerformLayout();
            this.groupBox66.ResumeLayout(false);
            this.groupBox66.PerformLayout();
            this.groupBox64.ResumeLayout(false);
            this.groupBox64.PerformLayout();
            this.urovGroup1.ResumeLayout(false);
            this.urovGroup1.PerformLayout();
            this.groupBox61.ResumeLayout(false);
            this.groupBox61.PerformLayout();
            this.groupBox62.ResumeLayout(false);
            this.groupBox62.PerformLayout();
            this.groupBox58.ResumeLayout(false);
            this.groupBox58.PerformLayout();
            this.groupBox59.ResumeLayout(false);
            this.groupBox59.PerformLayout();
            this.groupBox60.ResumeLayout(false);
            this.groupBox60.PerformLayout();
            this.groupBox55.ResumeLayout(false);
            this.groupBox55.PerformLayout();
            this.groupBox56.ResumeLayout(false);
            this.groupBox56.PerformLayout();
            this.groupBox57.ResumeLayout(false);
            this.groupBox57.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox49.ResumeLayout(false);
            this.groupBox49.PerformLayout();
            this.groupBox48.ResumeLayout(false);
            this.groupBox48.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.BGS_Group.ResumeLayout(false);
            this.BGS_Group.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox _ucaTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _ubcTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _uabTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _u2TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _u1TextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _ucTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _ubTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _uaTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage _discretTabPage1;
        private System.Windows.Forms.GroupBox groupBox14;
        private BEMN.Forms.LedControl _acceleration;
        private System.Windows.Forms.Label _auto4Led;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _iS1;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _iS2;
        private BEMN.Forms.LedControl _iS1Io;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _iS3;
        private BEMN.Forms.LedControl _iS2Io;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _iS4;
        private BEMN.Forms.LedControl _iS3Io;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _iS5;
        private BEMN.Forms.LedControl _iS4Io;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _iS6;
        private BEMN.Forms.LedControl _iS5Io;
        private BEMN.Forms.LedControl _iS6Io;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label269;
        private BEMN.Forms.LedControl _i8Io;
        private BEMN.Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _i6Io;
        private BEMN.Forms.LedControl _i5Io;
        private BEMN.Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _i4Io;
        private BEMN.Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _i3Io;
        private BEMN.Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _i2Io;
        private BEMN.Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _i1Io;
        private BEMN.Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox splGroupBox;
        private BEMN.Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private BEMN.Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private BEMN.Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private BEMN.Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private BEMN.Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private BEMN.Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private BEMN.Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private BEMN.Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private BEMN.Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.GroupBox groupBox17;
        private BEMN.Forms.LedControl _module32;
        private System.Windows.Forms.Label label138;
        private BEMN.Forms.LedControl _module31;
        private System.Windows.Forms.Label label139;
        private BEMN.Forms.LedControl _module30;
        private System.Windows.Forms.Label label140;
        private BEMN.Forms.LedControl _module29;
        private System.Windows.Forms.Label label141;
        private BEMN.Forms.LedControl _module28;
        private System.Windows.Forms.Label label142;
        private BEMN.Forms.LedControl _module27;
        private System.Windows.Forms.Label label232;
        private BEMN.Forms.LedControl _module26;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _module25;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _module24;
        private System.Windows.Forms.Label label129;
        private BEMN.Forms.LedControl _module23;
        private System.Windows.Forms.Label label130;
        private BEMN.Forms.LedControl _module22;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _module21;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _module20;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _module19;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _module10;
        private System.Windows.Forms.Label label164;
        private BEMN.Forms.LedControl _module9;
        private System.Windows.Forms.Label label165;
        private BEMN.Forms.LedControl _module8;
        private System.Windows.Forms.Label label166;
        private BEMN.Forms.LedControl _module7;
        private System.Windows.Forms.Label label167;
        private BEMN.Forms.LedControl _module6;
        private System.Windows.Forms.Label label168;
        private BEMN.Forms.LedControl _module5;
        private System.Windows.Forms.Label label172;
        private BEMN.Forms.LedControl _module4;
        private System.Windows.Forms.Label label173;
        private BEMN.Forms.LedControl _module3;
        private System.Windows.Forms.Label label174;
        private BEMN.Forms.LedControl _module2;
        private System.Windows.Forms.Label label175;
        private BEMN.Forms.LedControl _module1;
        private System.Windows.Forms.Label label176;
        private BEMN.Forms.LedControl _module18;
        private System.Windows.Forms.Label label161;
        private BEMN.Forms.LedControl _module17;
        private System.Windows.Forms.Label label162;
        private BEMN.Forms.LedControl _module16;
        private System.Windows.Forms.Label label163;
        private BEMN.Forms.LedControl _module15;
        private System.Windows.Forms.Label label169;
        private BEMN.Forms.LedControl _module14;
        private System.Windows.Forms.Label label170;
        private BEMN.Forms.LedControl _module13;
        private System.Windows.Forms.Label label171;
        private BEMN.Forms.LedControl _module12;
        private System.Windows.Forms.Label label177;
        private BEMN.Forms.LedControl _module11;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private BEMN.Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private BEMN.Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private BEMN.Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private BEMN.Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private BEMN.Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private BEMN.Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private BEMN.Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private BEMN.Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private BEMN.Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private BEMN.Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private BEMN.Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private BEMN.Forms.LedControl _d40;
        private System.Windows.Forms.Label label233;
        private BEMN.Forms.LedControl _d39;
        private System.Windows.Forms.Label label234;
        private BEMN.Forms.LedControl _d38;
        private System.Windows.Forms.Label label235;
        private BEMN.Forms.LedControl _d37;
        private System.Windows.Forms.Label label236;
        private BEMN.Forms.LedControl _d36;
        private System.Windows.Forms.Label label271;
        private BEMN.Forms.LedControl _d35;
        private System.Windows.Forms.Label label272;
        private BEMN.Forms.LedControl _d34;
        private System.Windows.Forms.Label label273;
        private BEMN.Forms.LedControl _d33;
        private System.Windows.Forms.Label label274;
        private BEMN.Forms.LedControl _d32;
        private System.Windows.Forms.Label label275;
        private BEMN.Forms.LedControl _d31;
        private System.Windows.Forms.Label label276;
        private BEMN.Forms.LedControl _d30;
        private System.Windows.Forms.Label label277;
        private BEMN.Forms.LedControl _d29;
        private System.Windows.Forms.Label label278;
        private BEMN.Forms.LedControl _d28;
        private System.Windows.Forms.Label label279;
        private BEMN.Forms.LedControl _d27;
        private System.Windows.Forms.Label label280;
        private BEMN.Forms.LedControl _d26;
        private System.Windows.Forms.Label label281;
        private BEMN.Forms.LedControl _d25;
        private System.Windows.Forms.Label label282;
        private BEMN.Forms.LedControl _d24;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _d23;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _d22;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _d21;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _d20;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _d19;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _d18;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _d17;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _d16;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _d15;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _d14;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _d13;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _d12;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _d11;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _d10;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _d9;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _d8;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _d7;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _d6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _d5;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _d4;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _d3;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _d2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _d1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button _switchGroupButton;
        private System.Windows.Forms.GroupBox groupBox27;
        private BEMN.Forms.LedControl _availabilityFaultSystemJournal;
        private BEMN.Forms.LedControl _newRecordOscJournal;
        private BEMN.Forms.LedControl _newRecordAlarmJournal;
        private BEMN.Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox _qpTextBox;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _cosfTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _qTextBox;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox _pTextBox;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _module34;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _module33;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button _resetTermStateButton;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _switchOn;
        private BEMN.Forms.LedControl _switchOff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _icTextBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox _igTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _i2TextBox;
        private System.Windows.Forms.TextBox _inTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _ibTextBox;
        private System.Windows.Forms.TextBox _i1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _iaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox _za2Box;
        private System.Windows.Forms.TextBox _za1Box;
        private System.Windows.Forms.TextBox _zc2Box;
        private System.Windows.Forms.TextBox _zc1Box;
        private System.Windows.Forms.TextBox _zb2Box;
        private System.Windows.Forms.TextBox _zb1Box;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox _zabBox;
        private System.Windows.Forms.TextBox _zcaBox;
        private System.Windows.Forms.TextBox _zbcBox;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.ComboBox _groupCombo;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label _currenGroupLabel;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.Label label191;
        private BEMN.Forms.LedControl _r4IoLed;
        private BEMN.Forms.LedControl _r5Led;
        private BEMN.Forms.LedControl _r1Led;
        private BEMN.Forms.LedControl _r3IoLed;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label199;
        private BEMN.Forms.LedControl _r6IoLed;
        private BEMN.Forms.LedControl _r2IoLed;
        private BEMN.Forms.LedControl _r6Led;
        private BEMN.Forms.LedControl _r2Led;
        private BEMN.Forms.LedControl _r4Led;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private BEMN.Forms.LedControl _r3Led;
        private BEMN.Forms.LedControl _r5IoLed;
        private BEMN.Forms.LedControl _r1IoLed;
        private BEMN.Forms.LedControl _faultTNmgn;
        private System.Windows.Forms.Label label218;
        private BEMN.Forms.LedControl _faultTN;
        private BEMN.Forms.LedControl _faultUabc;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label rxcaLabel;
        private System.Windows.Forms.Label rxbcLabel;
        private System.Windows.Forms.Label rxabLabel;
        private BEMN.Forms.LedControl _faultTnInd;
        private System.Windows.Forms.Button _resetFaultTnBtn;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.Button _startOscBtn;
        private System.Windows.Forms.GroupBox groupBox40;
        private BEMN.Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.TextBox _dU;
        private System.Windows.Forms.TextBox _dFi;
        private System.Windows.Forms.TextBox _dF;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.TextBox _iZc5Box;
        private System.Windows.Forms.TextBox _iZa5Box;
        private System.Windows.Forms.TextBox _iZb5Box;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.TextBox _iZc4Box;
        private System.Windows.Forms.TextBox _iZa4Box;
        private System.Windows.Forms.TextBox _iZb4Box;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.TextBox _iZc3Box;
        private System.Windows.Forms.TextBox _iZa3Box;
        private System.Windows.Forms.TextBox _iZb3Box;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.GroupBox groupBox44;
        private System.Windows.Forms.TextBox _iZc2Box;
        private System.Windows.Forms.TextBox _iZa2Box;
        private System.Windows.Forms.TextBox _iZb2Box;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TextBox _iZa1Box;
        private System.Windows.Forms.TextBox _iZc1Box;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox _iZb1Box;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.TextBox _iZabBox;
        private System.Windows.Forms.TextBox _iZcaBox;
        private System.Windows.Forms.TextBox _iZbcBox;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.GroupBox groupBox43;
        private System.Windows.Forms.TextBox _zc5Box;
        private System.Windows.Forms.TextBox _za5Box;
        private System.Windows.Forms.TextBox _zb5Box;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.TextBox _zc4Box;
        private System.Windows.Forms.TextBox _za4Box;
        private System.Windows.Forms.TextBox _zb4Box;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.TextBox _zc3Box;
        private System.Windows.Forms.TextBox _za3Box;
        private System.Windows.Forms.TextBox _zb3Box;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.Label label323;
        private BEMN.Forms.Diod diod12;
        private BEMN.Forms.Diod diod11;
        private BEMN.Forms.Diod diod10;
        private BEMN.Forms.Diod diod9;
        private BEMN.Forms.Diod diod8;
        private BEMN.Forms.Diod diod7;
        private BEMN.Forms.Diod diod6;
        private BEMN.Forms.Diod diod5;
        private BEMN.Forms.Diod diod4;
        private BEMN.Forms.Diod diod3;
        private BEMN.Forms.Diod diod2;
        private BEMN.Forms.Diod diod1;
        private System.Windows.Forms.TextBox _i30;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.TextBox CN;
        private System.Windows.Forms.TextBox CA;
        private System.Windows.Forms.TextBox BN;
        private System.Windows.Forms.TextBox AN;
        private System.Windows.Forms.TextBox BC;
        private System.Windows.Forms.TextBox AB;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.GroupBox groupBox51;
        private BEMN.Forms.LedControl _faultTHU2;
        private System.Windows.Forms.Label label349;
        private BEMN.Forms.LedControl _faultTH3u0;
        private System.Windows.Forms.Label label350;
        private BEMN.Forms.LedControl _faultObruvFaz;
        private System.Windows.Forms.Label label351;
        private System.Windows.Forms.Label label348;
        private System.Windows.Forms.GroupBox groupBox50;
        private BEMN.Forms.LedControl _readyApv;
        private System.Windows.Forms.Label label360;
        private BEMN.Forms.LedControl _blockApv;
        private System.Windows.Forms.Label label356;
        private System.Windows.Forms.Label label222;
        private BEMN.Forms.LedControl _puskApv;
        private BEMN.Forms.LedControl _krat4;
        private System.Windows.Forms.Label label359;
        private System.Windows.Forms.Label label353;
        private BEMN.Forms.LedControl _turnOnApv;
        private BEMN.Forms.LedControl _krat3;
        private System.Windows.Forms.Label label358;
        private System.Windows.Forms.Label label354;
        private BEMN.Forms.LedControl _krat1;
        private BEMN.Forms.LedControl _zapretApv;
        private System.Windows.Forms.Label label357;
        private BEMN.Forms.LedControl _krat2;
        private System.Windows.Forms.Label label355;
        private System.Windows.Forms.GroupBox groupBox52;
        private BEMN.Forms.LedControl _OnKsAndYppN;
        private System.Windows.Forms.Label label361;
        private BEMN.Forms.LedControl _UnoUno;
        private System.Windows.Forms.Label label362;
        private BEMN.Forms.LedControl _UyUno;
        private System.Windows.Forms.Label label363;
        private BEMN.Forms.LedControl _US;
        private BEMN.Forms.LedControl _U1noU2y;
        private System.Windows.Forms.Label label364;
        private System.Windows.Forms.Label label365;
        private System.Windows.Forms.Label _YS;
        private BEMN.Forms.LedControl _OS;
        private System.Windows.Forms.Label label368;
        private BEMN.Forms.LedControl _autoSinchr;
        private System.Windows.Forms.TabPage _discretTabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private BEMN.Forms.LedControl _q2Led;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _q1Led;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.Label label308;
        private BEMN.Forms.LedControl _i2i1IoLed;
        private BEMN.Forms.LedControl _i2i1Led;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Label label302;
        private BEMN.Forms.LedControl _f4IoLessLed;
        private BEMN.Forms.LedControl _f3IoLessLed;
        private BEMN.Forms.LedControl _f2IoLessLed;
        private BEMN.Forms.LedControl _f4LessLed;
        private System.Windows.Forms.Label label303;
        private BEMN.Forms.LedControl _f1IoLessLed;
        private BEMN.Forms.LedControl _f3LessLed;
        private System.Windows.Forms.Label label304;
        private BEMN.Forms.LedControl _f2LessLed;
        private System.Windows.Forms.Label label305;
        private BEMN.Forms.LedControl _f1LessLed;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.Label label296;
        private BEMN.Forms.LedControl _f4IoMoreLed;
        private BEMN.Forms.LedControl _f3IoMoreLed;
        private BEMN.Forms.LedControl _f2IoMoreLed;
        private BEMN.Forms.LedControl _f4MoreLed;
        private System.Windows.Forms.Label label297;
        private BEMN.Forms.LedControl _f1IoMoreLed;
        private BEMN.Forms.LedControl _f3MoreLed;
        private System.Windows.Forms.Label label298;
        private BEMN.Forms.LedControl _f2MoreLed;
        private System.Windows.Forms.Label label299;
        private BEMN.Forms.LedControl _f1MoreLed;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.Label label290;
        private BEMN.Forms.LedControl _u4IoLessLed;
        private BEMN.Forms.LedControl _u3IoLessLed;
        private BEMN.Forms.LedControl _u2IoLessLed;
        private BEMN.Forms.LedControl _u4LessLed;
        private System.Windows.Forms.Label label291;
        private BEMN.Forms.LedControl _u1IoLessLed;
        private BEMN.Forms.LedControl _u3LessLed;
        private System.Windows.Forms.Label label292;
        private BEMN.Forms.LedControl _u2LessLed;
        private System.Windows.Forms.Label label293;
        private BEMN.Forms.LedControl _u1LessLed;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label284;
        private BEMN.Forms.LedControl _u4IoMoreLed;
        private BEMN.Forms.LedControl _u3IoMoreLed;
        private BEMN.Forms.LedControl _u2IoMoreLed;
        private BEMN.Forms.LedControl _u4MoreLed;
        private System.Windows.Forms.Label label285;
        private BEMN.Forms.LedControl _u1IoMoreLed;
        private BEMN.Forms.LedControl _u3MoreLed;
        private System.Windows.Forms.Label label286;
        private BEMN.Forms.LedControl _u2MoreLed;
        private System.Windows.Forms.Label label287;
        private BEMN.Forms.LedControl _u1MoreLed;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label344;
        private BEMN.Forms.LedControl _faultSwitchOff;
        private BEMN.Forms.LedControl _faultMeasuringf;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _faultModule5;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _faultModule4;
        private System.Windows.Forms.Label label195;
        private BEMN.Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private BEMN.Forms.LedControl _faultModule2;
        private System.Windows.Forms.Label label197;
        private BEMN.Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private BEMN.Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private BEMN.Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private BEMN.Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private BEMN.Forms.LedControl _faultPass;
        private System.Windows.Forms.Label label203;
        private BEMN.Forms.LedControl _faultMeasuringU;
        private System.Windows.Forms.Label label204;
        private BEMN.Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private BEMN.Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.GroupBox groupBox53;
        private BEMN.Forms.LedControl _k2;
        private System.Windows.Forms.Label label369;
        private BEMN.Forms.LedControl _k1;
        private System.Windows.Forms.Label label366;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.GroupBox groupBox39;
        private BEMN.Forms.LedControl _fSpl1;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private BEMN.Forms.LedControl _fSpl2;
        private System.Windows.Forms.Label label317;
        private BEMN.Forms.LedControl _fSpl3;
        private BEMN.Forms.LedControl _fSpl4;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.Label label347;
        private BEMN.Forms.LedControl _faultDisable2;
        private System.Windows.Forms.Label label346;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _faultDisable1;
        private BEMN.Forms.LedControl _faultSwithON;
        private BEMN.Forms.LedControl _faultManage;
        private BEMN.Forms.LedControl _faultOtkaz;
        private BEMN.Forms.LedControl _faultBlockCon;
        private BEMN.Forms.LedControl _faultOut;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.Label label228;
        private BEMN.Forms.LedControl _UabcLow10;
        private System.Windows.Forms.Label label313;
        private BEMN.Forms.LedControl _freqLow40;
        private BEMN.Forms.LedControl _freqHiger60;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private BEMN.Forms.LedControl _damageA;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.Label label367;
        private BEMN.Forms.LedControl _damageB;
        private BEMN.Forms.LedControl _damageC;
        private System.Windows.Forms.Label label370;
        private System.Windows.Forms.GroupBox groupBox20;
        private BEMN.Forms.LedControl _kachanie;
        private System.Windows.Forms.Label label371;
        private BEMN.Forms.LedControl _inZone;
        private System.Windows.Forms.Label label373;
        private BEMN.Forms.LedControl _outZone;
        private System.Windows.Forms.Label label374;
        private System.Windows.Forms.GroupBox groupBox54;
        private BEMN.Forms.LedControl _faultHardware1;
        private System.Windows.Forms.Label label372;
        private BEMN.Forms.LedControl _faultMeasuringF1;
        private System.Windows.Forms.Label label375;
        private BEMN.Forms.LedControl _faultMeasuringU1;
        private System.Windows.Forms.Label label376;
        private BEMN.Forms.LedControl _faultSoftware1;
        private System.Windows.Forms.Label label377;
        private BEMN.Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private BEMN.Forms.LedControl _faultOff;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _faultSwitchOff1;
        private System.Windows.Forms.Label label378;
        private System.Windows.Forms.GroupBox groupBox61;
        private BEMN.Forms.LedControl _OnKsAndYppn1;
        private System.Windows.Forms.Label label423;
        private BEMN.Forms.LedControl _UnoUno1;
        private System.Windows.Forms.Label label424;
        private BEMN.Forms.LedControl _UyUno1;
        private System.Windows.Forms.Label label425;
        private BEMN.Forms.LedControl _US1;
        private BEMN.Forms.LedControl _U1noU2y1;
        private System.Windows.Forms.Label label426;
        private System.Windows.Forms.Label label427;
        private System.Windows.Forms.Label label428;
        private BEMN.Forms.LedControl _OS1;
        private System.Windows.Forms.Label label429;
        private BEMN.Forms.LedControl _autoSinchr1;
        private System.Windows.Forms.GroupBox groupBox62;
        private BEMN.Forms.LedControl _readyApv1;
        private System.Windows.Forms.Label label430;
        private BEMN.Forms.LedControl _blockApv1;
        private System.Windows.Forms.Label label431;
        private System.Windows.Forms.Label label432;
        private BEMN.Forms.LedControl _puskApv1;
        private BEMN.Forms.LedControl _krat41;
        private System.Windows.Forms.Label label433;
        private System.Windows.Forms.Label label434;
        private BEMN.Forms.LedControl _turnOnApv1;
        private BEMN.Forms.LedControl _krat31;
        private System.Windows.Forms.Label label435;
        private System.Windows.Forms.Label label436;
        private BEMN.Forms.LedControl _krat11;
        private BEMN.Forms.LedControl _zapretApv1;
        private System.Windows.Forms.Label label437;
        private BEMN.Forms.LedControl _krat21;
        private System.Windows.Forms.Label label438;
        private System.Windows.Forms.GroupBox groupBox58;
        private BEMN.Forms.LedControl _damageA1;
        private System.Windows.Forms.Label label409;
        private System.Windows.Forms.Label label410;
        private BEMN.Forms.LedControl _damageB1;
        private BEMN.Forms.LedControl _damageC1;
        private System.Windows.Forms.Label label411;
        private System.Windows.Forms.GroupBox groupBox59;
        private BEMN.Forms.LedControl _tnObivFaz;
        private System.Windows.Forms.Label l3;
        private BEMN.Forms.LedControl _faultTn3U0;
        private System.Windows.Forms.Label l4;
        private BEMN.Forms.LedControl _faultTnU2;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label label416;
        private BEMN.Forms.LedControl _faultTNmgn1;
        private System.Windows.Forms.Label l6;
        private BEMN.Forms.LedControl _externalTn;
        private System.Windows.Forms.Label l2;
        private BEMN.Forms.LedControl _faultTN1;
        private System.Windows.Forms.GroupBox groupBox60;
        private BEMN.Forms.LedControl _kachanie1;
        private System.Windows.Forms.Label label420;
        private BEMN.Forms.LedControl _inZone1;
        private System.Windows.Forms.Label label421;
        private BEMN.Forms.LedControl _outZone1;
        private System.Windows.Forms.Label label422;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.Label label379;
        private System.Windows.Forms.Label label380;
        private System.Windows.Forms.Label label381;
        private System.Windows.Forms.Label label382;
        private BEMN.Forms.LedControl _r4IoLed1;
        private BEMN.Forms.LedControl _r5Led1;
        private BEMN.Forms.LedControl _r1Led1;
        private BEMN.Forms.LedControl _r3IoLed1;
        private System.Windows.Forms.Label label383;
        private System.Windows.Forms.Label label384;
        private BEMN.Forms.LedControl _r6IoLed1;
        private BEMN.Forms.LedControl _r2IoLed1;
        private BEMN.Forms.LedControl _r6Led1;
        private BEMN.Forms.LedControl _r2Led1;
        private BEMN.Forms.LedControl _r4Led1;
        private System.Windows.Forms.Label label388;
        private System.Windows.Forms.Label label390;
        private BEMN.Forms.LedControl _r3Led1;
        private BEMN.Forms.LedControl _r5IoLed1;
        private BEMN.Forms.LedControl _r1IoLed1;
        private System.Windows.Forms.GroupBox groupBox56;
        private System.Windows.Forms.Label label391;
        private System.Windows.Forms.Label label392;
        private BEMN.Forms.LedControl _iS11;
        private System.Windows.Forms.Label label393;
        private BEMN.Forms.LedControl _iS21;
        private BEMN.Forms.LedControl _iS1Io1;
        private System.Windows.Forms.Label label394;
        private BEMN.Forms.LedControl _iS31;
        private BEMN.Forms.LedControl _iS2Io1;
        private System.Windows.Forms.Label label395;
        private System.Windows.Forms.Label label396;
        private BEMN.Forms.LedControl _iS41;
        private BEMN.Forms.LedControl _iS3Io1;
        private System.Windows.Forms.Label label397;
        private BEMN.Forms.LedControl _iS51;
        private BEMN.Forms.LedControl _iS4Io1;
        private System.Windows.Forms.Label label398;
        private BEMN.Forms.LedControl _iS61;
        private BEMN.Forms.LedControl _iS5Io1;
        private BEMN.Forms.LedControl _iS6Io1;
        private System.Windows.Forms.GroupBox groupBox57;
        private System.Windows.Forms.Label label399;
        private System.Windows.Forms.Label label400;
        private BEMN.Forms.LedControl _i8Io1;
        private BEMN.Forms.LedControl _i81;
        private BEMN.Forms.LedControl _i6Io1;
        private BEMN.Forms.LedControl _i5Io1;
        private BEMN.Forms.LedControl _i61;
        private System.Windows.Forms.Label label403;
        private BEMN.Forms.LedControl _i4Io1;
        private BEMN.Forms.LedControl _i51;
        private System.Windows.Forms.Label label404;
        private BEMN.Forms.LedControl _i3Io1;
        private BEMN.Forms.LedControl _i41;
        private System.Windows.Forms.Label label405;
        private BEMN.Forms.LedControl _i2Io1;
        private BEMN.Forms.LedControl _i31;
        private System.Windows.Forms.Label label406;
        private BEMN.Forms.LedControl _i1Io1;
        private BEMN.Forms.LedControl _i21;
        private System.Windows.Forms.Label label407;
        private BEMN.Forms.LedControl _i11;
        private System.Windows.Forms.Label label408;
        private System.Windows.Forms.TextBox _cI0;
        private System.Windows.Forms.TextBox _cIc;
        private System.Windows.Forms.TextBox _cI2;
        private System.Windows.Forms.TextBox _cIn;
        private System.Windows.Forms.TextBox _cIb;
        private System.Windows.Forms.TextBox _cI1;
        private System.Windows.Forms.TextBox _cIa;
        private System.Windows.Forms.TextBox _cUca;
        private System.Windows.Forms.TextBox _cUbc;
        private System.Windows.Forms.TextBox _cUab;
        private System.Windows.Forms.TextBox _cU2;
        private System.Windows.Forms.TextBox _cU1;
        private System.Windows.Forms.TextBox _cUc;
        private System.Windows.Forms.TextBox _cUb;
        private System.Windows.Forms.TextBox _cUa;
        private BEMN.Forms.LedControl _faultLogic1;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _iS7;
        private BEMN.Forms.LedControl _iS7Io;
        private System.Windows.Forms.Label label413;
        private BEMN.Forms.LedControl _iS8;
        private BEMN.Forms.LedControl _iS8Io;
        private System.Windows.Forms.Label label414;
        private BEMN.Forms.LedControl _iS81;
        private BEMN.Forms.LedControl _iS8Io1;
        private System.Windows.Forms.Label label402;
        private BEMN.Forms.LedControl _iS71;
        private BEMN.Forms.LedControl _iS7Io1;
        private System.Windows.Forms.GroupBox urovGroup;
        private BEMN.Forms.LedControl urov1Led;
        private System.Windows.Forms.Label label318;
        private BEMN.Forms.LedControl blockUrovLed;
        private System.Windows.Forms.Label label401;
        private BEMN.Forms.LedControl urov2Led;
        private System.Windows.Forms.Label label415;
        private System.Windows.Forms.GroupBox urovGroup1;
        private BEMN.Forms.LedControl urov1Led1;
        private System.Windows.Forms.Label label417;
        private BEMN.Forms.LedControl blockUrovLed1;
        private System.Windows.Forms.Label label418;
        private BEMN.Forms.LedControl urov2Led1;
        private System.Windows.Forms.Label label419;
        private System.Windows.Forms.GroupBox groupBox63;
        private BEMN.Forms.LedControl avrOn;
        private System.Windows.Forms.Label label160;
        private BEMN.Forms.LedControl avrBlock;
        private System.Windows.Forms.Label label212;
        private BEMN.Forms.LedControl avrOff;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.TextBox _nWarm;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.TextBox _nPusk;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.GroupBox groupBox65;
        private BEMN.Forms.LedControl dvBlockN;
        private BEMN.Forms.LedControl dvBlockQ;
        private System.Windows.Forms.Label label441;
        private System.Windows.Forms.Label label439;
        private System.Windows.Forms.GroupBox groupBox64;
        private BEMN.Forms.LedControl _avrOn1;
        private System.Windows.Forms.Label label385;
        private BEMN.Forms.LedControl _avrBlock1;
        private System.Windows.Forms.Label label386;
        private BEMN.Forms.LedControl _avrOff1;
        private System.Windows.Forms.Label label387;
        private BEMN.Forms.LedControl dvPusk;
        private System.Windows.Forms.Label label389;
        private BEMN.Forms.LedControl dvWork;
        private System.Windows.Forms.Label label440;
        private System.Windows.Forms.GroupBox groupBox66;
        private BEMN.Forms.LedControl dvPusk1;
        private System.Windows.Forms.Label label442;
        private BEMN.Forms.LedControl dvBlockN1;
        private BEMN.Forms.LedControl dvBlockQ1;
        private System.Windows.Forms.Label label443;
        private System.Windows.Forms.Label label444;
        private BEMN.Forms.LedControl dvWork1;
        private System.Windows.Forms.Label label445;
        private System.Windows.Forms.GroupBox dugGroup;
        private BEMN.Forms.LedControl dugPusk;
        private System.Windows.Forms.Label label446;
        private System.Windows.Forms.GroupBox dugGroup1;
        private BEMN.Forms.LedControl dugPusk1;
        private System.Windows.Forms.Label label447;
        private System.Windows.Forms.GroupBox reversGroup;
        private System.Windows.Forms.Label label448;
        private System.Windows.Forms.Label label449;
        private BEMN.Forms.LedControl _P2Io;
        private BEMN.Forms.LedControl _P1Io;
        private BEMN.Forms.LedControl _P2;
        private System.Windows.Forms.Label label455;
        private BEMN.Forms.LedControl _P1;
        private System.Windows.Forms.Label label456;
        private System.Windows.Forms.GroupBox reversGroup1;
        private System.Windows.Forms.Label label450;
        private System.Windows.Forms.Label label451;
        private BEMN.Forms.LedControl _P2Io1;
        private BEMN.Forms.LedControl _P1Io1;
        private BEMN.Forms.LedControl _P21;
        private System.Windows.Forms.Label label452;
        private BEMN.Forms.LedControl _P11;
        private System.Windows.Forms.Label label453;
        private System.Windows.Forms.TextBox _fTextBox;
        private System.Windows.Forms.TextBox _cU0;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _u30;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.TextBox _cIn1;
        private System.Windows.Forms.TextBox _in1TextBox;
        private System.Windows.Forms.Label label14;
        private Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox BGS_Group;
        private Forms.LedControl _bgs16;
        private System.Windows.Forms.Label label352;
        private Forms.LedControl _bgs15;
        private System.Windows.Forms.Label label412;
        private Forms.LedControl _bgs14;
        private System.Windows.Forms.Label label454;
        private Forms.LedControl _bgs13;
        private System.Windows.Forms.Label label457;
        private Forms.LedControl _bgs12;
        private System.Windows.Forms.Label label458;
        private Forms.LedControl _bgs11;
        private System.Windows.Forms.Label label459;
        private Forms.LedControl _bgs10;
        private System.Windows.Forms.Label label460;
        private Forms.LedControl _bgs9;
        private System.Windows.Forms.Label label461;
        private Forms.LedControl _bgs8;
        private System.Windows.Forms.Label label462;
        private Forms.LedControl _bgs7;
        private System.Windows.Forms.Label label463;
        private Forms.LedControl _bgs6;
        private System.Windows.Forms.Label label464;
        private Forms.LedControl _bgs5;
        private System.Windows.Forms.Label label465;
        private Forms.LedControl _bgs4;
        private System.Windows.Forms.Label label466;
        private Forms.LedControl _bgs3;
        private System.Windows.Forms.Label label467;
        private Forms.LedControl _bgs2;
        private System.Windows.Forms.Label label468;
        private Forms.LedControl _bgs1;
        private System.Windows.Forms.Label label469;
        private Forms.LedControl _ssl48;
        private Forms.LedControl _ssl40;
        private System.Windows.Forms.Label label484;
        private System.Windows.Forms.Label label476;
        private Forms.LedControl _ssl47;
        private Forms.LedControl _ssl39;
        private System.Windows.Forms.Label label483;
        private System.Windows.Forms.Label label475;
        private Forms.LedControl _ssl46;
        private Forms.LedControl _ssl38;
        private System.Windows.Forms.Label label482;
        private System.Windows.Forms.Label label474;
        private Forms.LedControl _ssl45;
        private Forms.LedControl _ssl37;
        private System.Windows.Forms.Label label481;
        private System.Windows.Forms.Label label473;
        private Forms.LedControl _ssl44;
        private Forms.LedControl _ssl36;
        private System.Windows.Forms.Label label480;
        private System.Windows.Forms.Label label472;
        private Forms.LedControl _ssl43;
        private Forms.LedControl _ssl35;
        private System.Windows.Forms.Label label479;
        private System.Windows.Forms.Label label471;
        private Forms.LedControl _ssl42;
        private Forms.LedControl _ssl34;
        private System.Windows.Forms.Label label478;
        private System.Windows.Forms.Label label470;
        private Forms.LedControl _ssl41;
        private System.Windows.Forms.Label label477;
        private Forms.LedControl _ssl33;
        private System.Windows.Forms.Label label224;
    }
}