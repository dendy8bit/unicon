﻿using System.Collections.Generic;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Measuring.Structures
{
    public class DiscretDataBaseStruct304 : DiscretDataBaseStructV300
    {
        #region [Constants]
        private const int BASE_SIZE = 24;
        private const int ALARM_SIZE = 16;
        private const int PARAM_SIZE = 16;
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private ushort[] _base;      //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm;    //БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private ushort[] _param;    //БД параметров
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public override bool[] DiscretInputs
        {
            get
            {
                List<bool> retDiscr = new List<bool>();
                retDiscr.AddRange(Common.GetBitsArray(this._base, 0, 39));
                retDiscr.AddRange(Common.GetBitsArray(this._base, 256, 257));
                return retDiscr.ToArray();
            }
        }
        /// <summary>
        /// Входные ЛС
        /// </summary>
        public override bool[] InputsLogicSignals => Common.GetBitsArray(this._base, 40, 55);

        public bool[] Bgs => Common.GetBitsArray(this._base, 56, 71);

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public override bool[] OutputLogicSignals => Common.GetBitsArray(this._base, 72, 87);

        /// <summary>
        /// Свободная логика
        /// </summary>
        public override bool[] FreeLogic => Common.GetBitsArray(this._base, 88, 135);

        /// <summary>
        /// Защиты Z
        /// </summary>
        public override bool[] ResistDef => Common.GetBitsArray(this._base, 136, 147);
        /// <summary>
        /// Защиты  P
        /// </summary>
        public override bool[] ReversPower => Common.GetBitsArray(this._base, 150, 155);

        /// <summary>
        /// Защиты I, I*, I2/I1, Ig
        /// </summary>
        public override bool[] MaximumCurrent => Common.GetBitsArray(this._base, 156, 187);

        /// <summary>
        /// Защиты U, F, Q
        /// </summary>
        public override bool[] VoltageFeaq => Common.GetBitsArray(this._base, 188, 221);
        /// <summary>
        /// Двигатель
        /// </summary>
        public override bool[] Motor
        {
            get
            {
                List<bool> ret = new List<bool>();
                ret.AddRange(Common.GetBitsArray(this._base, 222, 224));
                ret.Add(!ret[2]);
                return ret.ToArray();
            }
        }
        /// <summary>
        /// Состояния и АПВ
        /// </summary>
        public override bool[] StateAndApv => Common.GetBitsArray(this._base, 225, 240);

        /// <summary>
        /// Контроль синхронизма
        /// </summary>
        public override bool[] ContrSinchr => Common.GetBitsArray(this._base, 241, 247);

        /// <summary>
        /// Повр. фаз и качание
        /// </summary>
        public override bool[] PhaseAndSw => Common.GetBitsArray(this._base, 248, 253);

        /// <summary>
        /// УРОВ
        /// </summary>
        public override bool[] Urov => Common.GetBitsArray(this._base, 258, 260);
        /// <summary>
        /// АВР и дуговая защита
        /// </summary>
        public override bool[] AvrAndDug => Common.GetBitsArray(this._base, 261, 264);

        /// <summary>
        /// ВЧС
        /// </summary>
        public bool[] Hfl => Common.GetBitsArray(this._base, 265, 274);

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public override bool[] ExternalDefenses => Common.GetBitsArray(this._base, 288, 303);

        /// <summary>
        /// Реле
        /// </summary>
        public override bool[] Relays => Common.GetBitsArray(this._base, 304, 337);

        /// <summary>
        /// Индикаторы
        /// </summary>
        public override List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 338, 361); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2)    // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] });// (bool зеленый, bool красный)
                }
                return retList;
            }
        }
        /// <summary>
        /// Контроль
        /// </summary>
        public override bool[] ControlSignals => Common.GetBitsArray(this._base, 364, 378);

        /// <summary>
        /// Неисправности общие
        /// </summary>
        public override bool[] Faults1 => Common.GetBitsArray(this._alarm, 0, 17);

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        public override bool[] Faults2 => Common.GetBitsArray(this._alarm, 18, 24);

        /// <summary>
        /// Нисправности ТН и частоты
        /// </summary>
        public override bool[] Faults3
        {
            get
            {
                List<bool> retList = new List<bool>(Common.GetBitsArray(this._base, 222, 223));
                retList.AddRange(Common.GetBitsArray(this._alarm, 26, 37));
                return retList.ToArray();
            }
        }



        /// <summary>
        /// Направления токов
        /// </summary>
        public override string[] CurrentsSymbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(this._param, 0, 53);
                string[] result = new string[27];
                for (int i = 0; i < 54; i += 2)
                {
                    result[i / 2] = this.GetCurrentSymbol(i, booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(int i, bool symbol, bool error)
        {
            string res = "";
            if (i >= 12) res = "нет";
            return error ? res : (symbol ? "-" : "+");
        }
        List<bool> _logicList = new List<bool>();
        /// <summary>
        /// Неисправность логики
        /// </summary>
        public override bool[] FaultLogicErr
        {
            get
            {
                this._logicList.Clear();
                this._logicList.AddRange(Common.GetBitsArray(this._alarm, 39, 43));
                this._logicList.RemoveAt(3);
                return this._logicList.ToArray();
            }
        }
        #endregion [Properties]
    }
}
