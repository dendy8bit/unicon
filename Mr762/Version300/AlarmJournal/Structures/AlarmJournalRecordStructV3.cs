﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.AlarmJournal.Structures
{
    public class AlarmJournalRecordStructV3 : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d3}";
        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _numOfDefAndNumOfTrigParam;
        [Layout(9)] private ushort _groupOfSetpointsAndTypeDmg;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _valueOfTriggeredParametr1;
        [Layout(12)] private ushort _rab;
        [Layout(13)] private ushort _xab;
        [Layout(14)] private ushort _rbc;
        [Layout(15)] private ushort _xbc;
        [Layout(16)] private ushort _rca;
        [Layout(17)] private ushort _xca;
        [Layout(18)] private ushort _ra1;
        [Layout(19)] private ushort _xa1;
        [Layout(20)] private ushort _rb1;
        [Layout(21)] private ushort _xb1;
        [Layout(22)] private ushort _rc1;
        [Layout(23)] private ushort _xc1;
        [Layout(24)] private ushort _in1;
        [Layout(25)] private ushort _xa2;
        [Layout(26)] private ushort _rb2;
        [Layout(27)] private ushort _xb2;
        [Layout(28)] private ushort _rc2;
        [Layout(29)] private ushort _xc2;
        [Layout(30)] private ushort _ia;
        [Layout(31)] private ushort _ib;
        [Layout(32)] private ushort _ic;
        [Layout(33)] private ushort _3i0;
        [Layout(34)] private ushort _i2;
        [Layout(35)] private ushort _ig;
        [Layout(36)] private ushort _i1;
        [Layout(37)] private ushort _in;
        [Layout(38)] private ushort _ua;
        [Layout(39)] private ushort _ub;
        [Layout(40)] private ushort _uc;
        [Layout(41)] private ushort _uab;
        [Layout(42)] private ushort _ubc;
        [Layout(43)] private ushort _uca;
        [Layout(44)] private ushort _3u0;
        [Layout(45)] private ushort _u2;
        [Layout(46)] private ushort _u1;
        [Layout(47)] private ushort _un;
        [Layout(48)] private ushort _un1;
        [Layout(49)] private ushort _f;
        [Layout(50)] private ushort _d1;
        [Layout(51)] private ushort _d2;
        [Layout(52)] private ushort _d3;
        [Layout(53)] private ushort _omp;
        [Layout(54)] private ushort _q;
        [Layout(55)] private ushort _spl;

        #endregion [Private fields]


        #region [Properties]
        public ushort Year
        {
            get { return this._year; }
        }

        public ushort Month
        {
            get { return this._month; }
        }

        public ushort Date
        {
            get { return this._date; }
        }

        public ushort Hour
        {
            get { return this._hour; }
        }

        public ushort Minute
        {
            get { return this._minute; }
        }

        public ushort Second
        {
            get { return this._second; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
        }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond
                    + this._message + this._numOfDefAndNumOfTrigParam + this._groupOfSetpointsAndTypeDmg + this._valueOfTriggeredParametr;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );
            }
        }

        public ushort Message
        {
            get { return this._message; }
        }

        public int TriggeredDefense
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public int NumberOfTriggeredParametr
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 8, 9, 10, 11, 12, 13, 14, 15)>>8; }
        }

        public ushort ValueOfTriggeredParametr
        {
            get { return this._valueOfTriggeredParametr; }
        }

        public ushort ValueOfTriggeredParametr1
        {
            get { return this._valueOfTriggeredParametr1; }
        }

        public int GroupOfSetpoints
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 0, 1, 2); }
        }
        
        public int TypeOfDmg
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 8,9,10,11)>>8; }
        }

        public ushort Rab
        {
            get { return this._rab; }
        }

        public ushort Xab
        {
            get { return this._xab; }
        }

        public ushort Rbc
        {
            get { return this._rbc; }
        }

        public ushort Xbc
        {
            get { return this._xbc; }
        }

        public ushort Rca
        {
            get { return this._rca; }
        }

        public ushort Xca
        {
            get { return this._xca; }
        }

        public ushort Ra1
        {
            get { return this._ra1; }
        }

        public ushort Xa1
        {
            get { return this._xa1; }
        }

        public ushort Rb1
        {
            get { return this._rb1; }
        }

        public ushort Xb1
        {
            get { return this._xb1; }
        }

        public ushort Rc1
        {
            get { return this._rc1; }
        }

        public ushort Xc1
        {
            get { return this._xc1; }
        }

        public ushort Ia
        {
            get { return this._ia; }
        }

        public ushort Ib
        {
            get { return this._ib; }
        }

        public ushort Ic
        {
            get { return this._ic; }
        }

        public ushort I0
        {
            get { return this._3i0; }
        }

        public ushort I2
        {
            get { return this._i2; }
        }

        public ushort Ig
        {
            get { return this._ig; }
        }

        public ushort I1
        {
            get { return this._i1; }
        }

        public ushort In
        {
            get { return this._in; }
        }

        public ushort In1
        {
            get { return this._in1; }
        }

        public ushort Ua
        {
            get { return this._ua; }
        }

        public ushort Ub
        {
            get { return this._ub; }
        }

        public ushort Uc
        {
            get { return this._uc; }
        }

        public ushort Uab
        {
            get { return this._uab; }
        }

        public ushort Ubc
        {
            get { return this._ubc; }
        }

        public ushort Uca
        {
            get { return this._uca; }
        }

        public ushort U0
        {
            get { return this._3u0; }
        }

        public ushort Un
        {
            get { return this._un; }
        }

        public ushort U2
        {
            get { return this._u2; }
        }

        public ushort U1
        {
            get { return this._u1; }
        }

        public ushort F
        {
            get { return this._f; }
        }

        public ushort D1
        {
            get { return this._d1; }
        }

        public ushort D2
        {
            get { return this._d2; }
        }

        public ushort D3
        {
            get { return this._d3; }
        }
        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1), true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D2), true); }
        }

        public string D25To32
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D2), true); }
        }

        public string D33To40
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D3), true); }
        }

        public ushort Q
        {
            get { return this._q; }
        }

        #endregion [Properties]

    }
}
