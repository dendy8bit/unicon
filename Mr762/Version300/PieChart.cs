﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BEMN.Mr762.Version300
{
    [Serializable]
    public class PieChartOptions
    {
        private ChannelPieChartOptions[] _pieIChartOptions;
        private ChannelPieChartOptions[] _pieUChartOptions;
        private ChannelPieChartOptions[] _allChartOptions;
        private PieChartVisiblyOptions _visiblyOptions = new PieChartVisiblyOptions();
        private List<ICharacteristic> _characteristics = new List<ICharacteristic>();
        
        public PieChartOptions() {}

        public ChannelPieChartOptions IaChannel { get; set; }
        public ChannelPieChartOptions IbChannel { get; set; }
        public ChannelPieChartOptions IcChannel { get; set; }

        public ChannelPieChartOptions UaChannel { get; set; }
        public ChannelPieChartOptions UbChannel { get; set; }
        public ChannelPieChartOptions UcChannel { get; set; }


        public PieChartDirectionCharacteristicOption Corners { get; set; }
        public PieChartChargeCharacteristicOption Linear { get; set; }
        public PieChartChargeCharacteristicOption Phase { get; set; }
        public ICharacteristic Kachanie { get; set; }
        public ICharacteristic Kachanie2 { get; set; }

        public ZnOptions Z1 { get; set; }
        public ZnOptions Z2 { get; set; }
        public ZnOptions Z3 { get; set; }
        public ZnOptions Z4 { get; set; }
        public ZnOptions Z5 { get; set; }

        public void Save(string fileName)
        {
            var characteristics = new XElement("Characteristics");
            foreach (var ch in this._characteristics)
            {
                characteristics.Add(ch.ToXml());
            }
          
            characteristics.Add(this.Corners?.ToXml());
            characteristics.Add(this.Linear?.ToXml());
            characteristics.Add(this.Phase?.ToXml());
            var mainElement = new XElement("PieOptions",
                new XElement("Channels",
                    new XAttribute("Ia", (object) this.IaChannel ?? string.Empty),
                    new XAttribute("Ib", (object) this.IbChannel ?? string.Empty),
                    new XAttribute("Ic", (object) this.IcChannel ?? string.Empty),
                    new XAttribute("Ua", (object) this.UaChannel ?? string.Empty),
                    new XAttribute("Ub", (object) this.UbChannel ?? string.Empty),
                    new XAttribute("Uc", (object) this.UcChannel ?? string.Empty)
                    ), this.VisiblyOptions.ToXml(this.Z3 == null));
            if (this.Z1 != null)
            {
                mainElement.Add(this.Z1.ToXml("Z1"));
            }
            if (this.Z2 != null)
            {
                mainElement.Add(this.Z2.ToXml("Z2"));
            }
            if (this.Z3 != null)
            {
                mainElement.Add(this.Z3.ToXml("Z3"));
            }
            if (this.Z4 != null)
            {
                mainElement.Add(this.Z4.ToXml("Z4"));
            }
            if (this.Z5 != null)
            {
                mainElement.Add(this.Z5.ToXml("Z5"));
            }
            mainElement.Add(characteristics);
            XDocument doc = new XDocument(mainElement);
            doc.Save(fileName);
        }


        public event Action ChannelChanged;
        public List<AnalogChannel> AddedChannel { get; set; }
        public int Lenght { get; private set; }



        public PieChartOptions(AnalogChannel[] channels)
        {
            this.Lenght = channels[0].Length;
            this._allChartOptions = channels.Select(o => new ChannelPieChartOptions(o)).ToArray();
            this._pieIChartOptions = this._allChartOptions.Where(o => o.Channel.Measure == "A").ToArray();
            this.StartTime = 20;
            this.EndTime = this.Lenght - 1;

            for (int i = 0; i < this.PieIChartOptions.Length; i++)
            {
                var option = this.PieIChartOptions[i];

                switch (i)
                {
                    case 0:
                    {
                        option.A = true;
                        break;
                    }
                    case 1:
                    {
                        option.B = true;
                        break;
                    }
                    case 2:
                    {
                        option.C = true;
                        break;
                    }
                    case 3:
                    {
                        option.N = true;
                        break;
                    }
                }
                option.AisTrue += o => this.OnOptionOnAisTrue(o, this.PieIChartOptions);
                option.BisTrue += o => this.OnOptionOnBisTrue(o, this.PieIChartOptions);
                option.CisTrue += o => this.OnOptionOnCisTrue(o, this.PieIChartOptions);
                option.NisTrue += o => this.OnOptionOnNisTrue(o, this.PieIChartOptions);
                option.PropertyChanged += this.option_PropertyChanged;
            }
            this._pieUChartOptions = this._allChartOptions.Where(o => o.Channel.Measure == "V").ToArray();
            for (int i = 0; i < this.PieUChartOptions.Length; i++)
            {
                var option = this.PieUChartOptions[i];
                switch (i)
                {
                    case 0:
                    {
                        option.A = true;
                        break;
                    }
                    case 1:
                    {
                        option.B = true;
                        break;
                    }
                    case 2:
                    {
                        option.C = true;
                        break;
                    }
                    case 3:
                    {
                        option.N = true;
                        break;
                    }
                }
                option.AisTrue += o => this.OnOptionOnAisTrue(o, this.PieUChartOptions);
                option.BisTrue += o => this.OnOptionOnBisTrue(o, this.PieUChartOptions);
                option.CisTrue += o => this.OnOptionOnCisTrue(o, this.PieUChartOptions);
                option.PropertyChanged += this.option_PropertyChanged;
            }
        }

        public ChannelPieChartOptions[] PieIChartOptions
        {
            get { return this._pieIChartOptions; }
        }

        public ChannelPieChartOptions[] PieUChartOptions
        {
            get { return this._pieUChartOptions; }
        }

        public ChannelPieChartOptions[] AllChartOptions
        {
            get { return this._allChartOptions; }
        }


        private void option_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.ChannelChanged != null)
            {
                this.ChannelChanged.Invoke();
            }
        }

        private void OnOptionOnAisTrue(ChannelPieChartOptions o, ChannelPieChartOptions[] collection)
        {
            foreach (var optionse in collection)
            {
                if (optionse != o)
                {
                    optionse.A = false;
                }
            }
        }

        private void OnOptionOnBisTrue(ChannelPieChartOptions o, ChannelPieChartOptions[] collection)
        {
            foreach (var optionse in collection)
            {
                if (optionse != o)
                {
                    optionse.B = false;
                }
            }
        }

        private void OnOptionOnCisTrue(ChannelPieChartOptions o, ChannelPieChartOptions[] collection)
        {
            foreach (var optionse in collection)
            {
                if (optionse != o)
                {
                    optionse.C = false;
                }
            }
        }

        private void OnOptionOnNisTrue(ChannelPieChartOptions o, ChannelPieChartOptions[] collection)
        {
            foreach (var optionse in collection)
            {
                if (optionse != o)
                {
                    optionse.N = false;
                }
            }
        }

        public List<ICharacteristic> Characteristics
        {
            get { return this._characteristics; }
            set { this._characteristics = value; }
        }

        public PieChartVisiblyOptions VisiblyOptions
        {
            get { return this._visiblyOptions; }
            set { this._visiblyOptions = value; }
        }

     

        public void ChannelAccept()
        {
            this.IaChannel = this._pieIChartOptions.FirstOrDefault(o => o.A);
            this.IbChannel = this._pieIChartOptions.FirstOrDefault(o => o.B);
            this.IcChannel = this._pieIChartOptions.FirstOrDefault(o => o.C);

            this.UaChannel = this._pieUChartOptions.FirstOrDefault(o => o.A);
            this.UbChannel = this._pieUChartOptions.FirstOrDefault(o => o.B);
            this.UcChannel = this._pieUChartOptions.FirstOrDefault(o => o.C);

        }



        public int StartTime { get; set; }
        public int EndTime { get; set; }
    }
    


    [Serializable]
    [XmlRoot(ElementName = "qwe")]
    public class PieChartVisiblyOptions
    {
        private VisibilityItem _zab = new VisibilityItem("Zab");
        private VisibilityItem _zbc = new VisibilityItem("Zbc");
        private VisibilityItem _zca = new VisibilityItem("Zca");
        private VisibilityItem _z1a = new VisibilityItem("Z1A");
        private VisibilityItem _z1b = new VisibilityItem("Z1B");
        private VisibilityItem _z1c = new VisibilityItem("Z1C");
        private VisibilityItem _z2a = new VisibilityItem("Z2A");
        private VisibilityItem _z2b = new VisibilityItem("Z2B");
        private VisibilityItem _z2c = new VisibilityItem("Z2C");
        private VisibilityItem _z3a = new VisibilityItem("Z3A");
        private VisibilityItem _z3b = new VisibilityItem("Z3B");
        private VisibilityItem _z3c = new VisibilityItem("Z3C");
        private VisibilityItem _z4a = new VisibilityItem("Z4A");
        private VisibilityItem _z4b = new VisibilityItem("Z4B");
        private VisibilityItem _z4c = new VisibilityItem("Z4C");
        private VisibilityItem _z5a = new VisibilityItem("Z5A");
        private VisibilityItem _z5b = new VisibilityItem("Z5B");
        private VisibilityItem _z5c = new VisibilityItem("Z5C");

        public XElement ToXml(bool old)
        {
            if (old)
            {
                return new XElement("Visibly",
                    this._zab.ToXml(),
                    this._zbc.ToXml(),
                    this._zca.ToXml(),
                    this._z1a.ToXml(),
                    this._z1b.ToXml(),
                    this._z1c.ToXml(),
                    this._z2a.ToXml(),
                    this._z2b.ToXml(),
                    this._z2c.ToXml()
                             
                    );
            }
            return new XElement("Visibly",
                this._zab.ToXml(),
                this._zbc.ToXml(),
                this._zca.ToXml(),
                this._z1a.ToXml(),
                this._z1b.ToXml(),
                this._z1c.ToXml(),
                this._z2a.ToXml(),
                this._z2b.ToXml(),
                this._z2c.ToXml(),
                this._z3a.ToXml(),
                this._z3b.ToXml(),
                this._z3c.ToXml(),
                this._z4a.ToXml(),
                this._z4b.ToXml(),
                this._z4c.ToXml(),
                this._z5a.ToXml(),
                this._z5b.ToXml(),
                this._z5c.ToXml()
                );
        }


   


        
    
    }

    [Serializable]
    [XmlRoot(ElementName = "защиты")]
    public class VisibilityItem : INotifyPropertyChanged
    {
        private string _channel;
        private bool _visible;
        private bool _r;
        private bool _x;

        public VisibilityItem(string channel)
        {
            this.Channel = channel;
            this.Visible = true;
            this.R = false;
            this.X = false;
        }
        public XElement ToXml()
        {
            return new XElement(this._channel,
                new XAttribute("Visible", this._visible),
                new XAttribute("R", this._r),
                new XAttribute("X", this._x));
        }

        public void FromXml(XElement element)
        {
            element = element.Element(this._channel);
            this._visible = Convert.ToBoolean(element.Attribute("Visible").Value);
            this._r = Convert.ToBoolean(element.Attribute("R").Value);
            this._x = Convert.ToBoolean(element.Attribute("X").Value);
        }

        public string Channel
        {
            get { return this._channel; }
            set
            {
                this.RaisePropertyChanged("Channel");
                this._channel = value;
            }
        }

        public bool Visible
        {
            get { return this._visible; }
            set
            {
                this.RaisePropertyChanged("Visible");
                this._visible = value;
            }
        }

        public bool R
        {
            get { return this._r; }
            set
            {
                this.RaisePropertyChanged("R");
                this._r = value;
            }
        }

        public bool X
        {
            get { return this._x; }
            set
            {
                this.RaisePropertyChanged("X");
                this._x = value;
            }
        }

        private void RaisePropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    [Serializable]
    public class ChannelPieChartOptions : INotifyPropertyChanged
    {
        private AnalogChannel _channel;
        private string _name;
        private bool _a;
        private bool _b;
        private bool _c;
        private bool _n;

        public override string ToString()
        {
            return this._name;
        }

        public AnalogChannel Channel
        {
            get { return this._channel; }
        }

        public ChannelPieChartOptions(string name)
        {
            this._name = name;
        }

        public ChannelPieChartOptions(AnalogChannel channel)
        {
            this._channel = channel;
            this._name = channel.Name;
        }

        //public Point[] Values { get { return Channel.FirstHarmonic; } }

        public string Name
        {
            get { return this._name; }
            set
            {
                this._name = value;

            }
        }

        public bool A
        {
            get { return this._a; }
            set
            {
                this._a = value;
                if (value)
                {
                    this.B = false;
                    this.C = false;
                    this.N = false;
                    this.OnAisTrue(this);
                }
                this.RaisePropertyChanged("A");
            }
        }

        public bool B
        {
            get { return this._b; }
            set
            {
                this._b = value;
                if (value)
                {
                    this.A = false;
                    this.C = false;
                    this.N = false;
                    this.OnBisTrue(this);
                }
                this.RaisePropertyChanged("B");
            }
        }

        public bool C
        {
            get { return this._c; }
            set
            {
                this._c = value;
                if (value)
                {
                    this.B = false;
                    this.A = false;
                    this.N = false;
                    this.OnCisTrue(this);
                }
                this.RaisePropertyChanged("C");
            }
        }

        public bool N
        {
            get { return this._n; }
            set
            {
                this._n = value;
                if (value)
                {
                    this.B = false;
                    this.C = false;
                    this.A = false;
                    this.OnNisTrue(this);
                }
                this.RaisePropertyChanged("N");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event Action<ChannelPieChartOptions> AisTrue;
        public event Action<ChannelPieChartOptions> BisTrue;
        public event Action<ChannelPieChartOptions> CisTrue;
        public event Action<ChannelPieChartOptions> NisTrue;

        protected virtual void OnBisTrue(ChannelPieChartOptions obj)
        {
            Action<ChannelPieChartOptions> handler = this.BisTrue;
            if (handler != null) handler(obj);
        }

        protected virtual void OnCisTrue(ChannelPieChartOptions obj)
        {
            Action<ChannelPieChartOptions> handler = this.CisTrue;
            if (handler != null) handler(obj);
        }

        protected virtual void OnNisTrue(ChannelPieChartOptions obj)
        {
            Action<ChannelPieChartOptions> handler = this.NisTrue;
            if (handler != null) handler(obj);
        }

        protected virtual void OnAisTrue(ChannelPieChartOptions obj)
        {
            Action<ChannelPieChartOptions> handler = this.AisTrue;
            if (handler != null) handler(obj);
        }

        private void RaisePropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    [Serializable]
    public class ZnOptions
    {
        public double R0 { get; set; }
        public double R1 { get; set; }
        public double X0 { get; set; }
        public double X1 { get; set; }

        public double Kr { get { return (this.R0 / this.R1 - 1) / 3.0; } }
        public double Kx { get { return (this.X0 / this.X1 - 1) / 3.0; } }

        public void FromXml(XElement element)
        {
            this.R0 = Convert.ToDouble(element.Attribute("R0").Value);
            this.R1 = Convert.ToDouble(element.Attribute("R1").Value);
            this.X0 = Convert.ToDouble(element.Attribute("X0").Value);
            this.X1 = Convert.ToDouble(element.Attribute("X1").Value);
        }
        public XElement ToXml(string name)
        {
            return new XElement(name,
                new XAttribute("R0", this.R0),
                new XAttribute("R1", this.R1),
                new XAttribute("X0", this.X0),
                new XAttribute("X1", this.X1)
                );
        }
    }

    public class AnalogChannel
    {
        private AnalogChartOptions _analogChartOptions;
        private AnalogChannelConfiguration _analogChannelConfiguration;
        private double[] _data;
        private double[] _rmsValues;

        private double _min;
        private double _max;

        //public Brush ChannelBrush { get; set; }
        public double[] ActivValues { get; private set; }
        private double AmplitudeActiv { get; set; }

        public double[] RmsValues
        {
            get { return this._rmsValues.Select(o => o*this._analogChartOptions.Factor).ToArray(); }
        }

        private double RmsAmplitude { get; set; }
        private double FirstHarmonicAmplitude { get; set; }

        public event Action NeedDraw;

        public enum ChannelType
        {
            UNKNOWN,
            I,
            U,
            R,
            X
        };

        public ChannelType ThisType { get; set; }

        //public static Brush[] DefaultBrushes = new Brush[]
        //{
        //    new SolidColorBrush(Color.FromRgb(231, 157, 48)), //  Brushes.Yellow,//5 
        //    Brushes.Green, //3
        //    Brushes.Red, //1
        //    Brushes.Blue, //2
        //    Brushes.Orange, //4              
        //    Brushes.Purple, //6
        //    Brushes.Brown, //7
        //    Brushes.DeepPink, //8
        //    Brushes.Indigo, //9
        //    Brushes.Coral //10
        //};

        //public static void SetBrushes(AnalogChannel[] channels)
        //{
        //    int iCount = 0;
        //    int uCount = 0;
        //    int rCount = 0;
        //    int xCount = 0;
        //    for (int i = 0; i < channels.Length; i++)
        //    {
        //        if (channels[i].ThisType == ChannelType.I)
        //        {
        //            channels[i].ChannelBrush = DefaultBrushes[iCount];
        //            if (iCount < DefaultBrushes.Length - 1)
        //            {
        //                iCount++;
        //            }

        //        }
        //        if (channels[i].ThisType == ChannelType.U)
        //        {
        //            channels[i].ChannelBrush = DefaultBrushes[uCount];

        //            if (uCount < DefaultBrushes.Length - 1)
        //            {
        //                uCount++;
        //            }
        //        }

        //        if (channels[i].ThisType == ChannelType.R)
        //        {
        //            channels[i].ChannelBrush = DefaultBrushes[rCount];

        //            if (rCount < DefaultBrushes.Length - 1)
        //            {
        //                rCount++;
        //            }
        //        }

        //        if (channels[i].ThisType == ChannelType.X)
        //        {
        //            channels[i].ChannelBrush = DefaultBrushes[xCount];

        //            if (xCount < DefaultBrushes.Length - 1)
        //            {
        //                xCount++;
        //            }
        //        }

        //        if (channels[i].ThisType == ChannelType.UNKNOWN)
        //        {
        //            channels[i].ChannelBrush = Brushes.Black;
        //        }
        //    }
        //}

        private void RaiseNeedDraw()
        {
            if (this.NeedDraw != null)
            {
                this.NeedDraw.Invoke();
            }
        }

        public string Min
        {
            get
            {
                return string.Format("Min = {0}",
                    AnalogChannel.Normalize(this._min*this._analogChartOptions.Factor, this.Measure));
            }
        }

        public string Max
        {
            get
            {
                return string.Format("Max = {0}",
                    AnalogChannel.Normalize(this._max*this._analogChartOptions.Factor, this.Measure));
            }
        }

        public double[] Values(GrafType type)
        {

            double[] res = new double[1];
            switch (type)
            {
                case GrafType.ACTIV:
                    res = this.ActivValues;
                    break;
                case GrafType.RMS:
                    res = this._rmsValues;
                    break;
                case GrafType.FIRST_HARMONIC:
                    res = this.FirstHarmonicDouble;
                    break;
            }
            if (this._analogChartOptions == null)
            {
                return res;
            }
            return res.Select(o => o*this._analogChartOptions.Factor).ToArray();

        }

        public double Amplitude(GrafType type)
        {

            double res = 0;
            switch (type)
            {
                case GrafType.ACTIV:
                    res = this.AmplitudeActiv;
                    break;
                case GrafType.RMS:
                    res = this.RmsAmplitude;
                    break;
                case GrafType.FIRST_HARMONIC:
                    res = this.FirstHarmonicAmplitude;
                    break;
            }
            if (res == 0)
            {
                return 1.0;
            }
            return res*this._analogChartOptions.Factor;

        }

        public enum GrafType
        {
            ACTIV,
            RMS,
            FIRST_HARMONIC
        }


        public string Measure { get; private set; }

        public string Name { get; private set; }

        public int Length
        {
            get { return this.ActivValues == null ? 0 : this.ActivValues.Length; }
        }

        internal AnalogChartOptions Options
        {
            get
            {

                if (this._analogChartOptions == null)
                {

                    this._analogChartOptions = new AnalogChartOptions
                    {
                        ChannelName = this.Name
                    };
                    this._analogChartOptions.FactorChange += this.RaiseNeedDraw;

                }
                return this._analogChartOptions;
            }
        }

        //public Point[] FirstHarmonic { get; private set; }
        public double[] FirstHarmonicDouble { get; private set; }

        public double[] Data
        {
            get { return this._data; }
        }

        public int Run
        {
            get { return this._run; }
        }

        public AnalogChannel(AnalogChannelConfiguration configuration, double[] data, int run)
        {
            this._run = run;
            this._analogChannelConfiguration = configuration;
            this._data = data;
            this.Measure = configuration.Measure;
            this.Name = configuration.Name;
            this.ActivValues = data.Select(o => configuration.A*o + configuration.B).ToArray();
            this.AmplitudeActiv = this.ActivValues.Max(o => Math.Abs(o));
            this._min = this.ActivValues.Min();
            this._max = this.ActivValues.Max();

            var t = 20;
            this._rmsValues = new double[this.ActivValues.Length];
            for (int i = 0; i < this._rmsValues.Length; i++)
            {
                double temp = 0;


                for (int j = 0; j < t; j++)
                {
                    int index = i - j;
                    if ((index >= 0) && (index < this._rmsValues.Length))
                    {
                        temp += Math.Pow(this.ActivValues[index], 2);
                    }

                }
                temp = Math.Sqrt(temp/(t));
                this._rmsValues[i] = temp;
            }
            this.RmsAmplitude = this._rmsValues.Max(o => Math.Abs(o));
            //this.PrepareFirstHarmonic();
            this.FirstHarmonicAmplitude = this.FirstHarmonicDouble.Max(o => Math.Abs(o));
            switch (configuration.Measure)
            {
                case "A":
                    this.ThisType = ChannelType.I;
                    break;
                case "V":
                    this.ThisType = ChannelType.U;
                    break;
                default:
                    this.ThisType = ChannelType.UNKNOWN;
                    break;
            }
        }


        public AnalogChannel(string name, string measure, double[] data)
        {
            // _data = data;
            this.Measure = measure;
            this.Name = name;
            this.ActivValues = data;
            this.AmplitudeActiv = this.ActivValues.Max(o => Math.Abs(o));
            this._min = this.ActivValues.Min();
            this._max = this.ActivValues.Max();
            var t = 20;
            this._rmsValues = new double[this.ActivValues.Length];
            for (int i = 0; i < this._rmsValues.Length; i++)
            {
                double temp = 0;
                for (int j = 0; j < t; j++)
                {
                    int index = i - j;
                    if ((index >= 0) && (index < this._rmsValues.Length))
                    {
                        temp += Math.Pow(this.ActivValues[index], 2);
                    }

                }
                temp = Math.Sqrt(temp/(t));
                this._rmsValues[i] = temp;
            }
            this.RmsAmplitude = this._rmsValues.Max(o => Math.Abs(o));
            //this.PrepareFirstHarmonic();
            //this.ChannelBrush = Brushes.DarkBlue;
            this.FirstHarmonicAmplitude = this.FirstHarmonicDouble.Max(o => Math.Abs(o));
            switch (name[0])
            {
                case 'R':
                {
                    this.ThisType = ChannelType.R;
                    break;
                }
                case 'X':
                {
                    this.ThisType = ChannelType.X;
                    break;
                }
                default:
                {
                    this.ThisType = ChannelType.UNKNOWN;
                    break;
                }
            }
        }

        //public double[] PrepareHarmonic(int k, int periodCount)
        //{
        //    var res = new double[ActivValues.Length];
        //    var factors = new double[20];
        //    int N = 20 * periodCount;
        //    double K = k;
        //    if (k == 0)
        //    {
        //        for (int j = N - 1; j < ActivValues.Length; j++)
        //        {
        //            double sum = 0;
        //            for (int i = 0; i < N; i++)
        //            {
        //                sum += ActivValues[j - N + 1 + i];
        //            }
        //            res[j] = sum / N;
        //        }
        //        return res;
        //    }

        //    for (int j = N - 1; j < ActivValues.Length; j++)
        //    {
        //        for (int i = 0; i < N; i++)
        //        {
        //            if (i == 0)
        //            {
        //                factors[i] = ActivValues[j - N + 1 + i];
        //                continue;
        //            }
        //            if (i == 1)
        //            {
        //                factors[i] = ActivValues[j - N + 1 + i] + 2 * Math.Cos(2 * Math.PI / N * K) * factors[i - 1];
        //                continue;
        //            }
        //            factors[i] = ActivValues[j - N + 1 + i] + 2 * Math.Cos(2 * Math.PI / N * K) * factors[i - 1] - factors[i - 2];
        //        }
        //        var x = Math.Cos(2 * Math.PI / N * K) * factors[N - 1] - factors[N - 2];
        //        var y = Math.Sin(2 * Math.PI / N * K) * factors[N - 1];
        //        res[j] = Math.Sqrt(x * x + y * y) / (10.0 * Math.Sqrt(2));

        //    }
        //    return res;
        //}

        //private void PrepareFirstHarmonic()
        //{
        //    FirstHarmonic = new Point[ActivValues.Length];
        //    var factors = new double[20];
        //    int N = 20;
        //    double K = 1;
        //    for (int j = N - 1; j < FirstHarmonic.Length; j++)
        //    {
        //        for (int i = 0; i < N; i++)
        //        {
        //            if (i == 0)
        //            {
        //                factors[i] = ActivValues[j - N + 1 + i];
        //                continue;
        //            }
        //            if (i == 1)
        //            {
        //                factors[i] = ActivValues[j - N + 1 + i] + 2 * Math.Cos(2 * Math.PI / N * K) * factors[i - 1];
        //                continue;
        //            }
        //            factors[i] = ActivValues[j - N + 1 + i] + 2 * Math.Cos(2 * Math.PI / N * K) * factors[i - 1] - factors[i - 2];
        //        }

        //        FirstHarmonic[j].X = Math.Cos(2 * Math.PI / N * K) * factors[N - 1] - factors[N - 2];
        //        FirstHarmonic[j].Y = Math.Sin(2 * Math.PI / N * K) * factors[N - 1];
        //    }
        //    FirstHarmonicDouble = FirstHarmonic.Select(o => Math.Sqrt(o.X * o.X + o.Y * o.Y) / (10.0 * Math.Sqrt(2))).ToArray();
        //}

        public double GetY(GrafType type)
        {
            return GetMiddleValue(this.Amplitude(type));
        }

        public static double GetMiddleValue(double max)
        {
            var tempMax = max;
            int exponent = 0;
            if (tempMax >= 1)
            {
                while (tempMax/10.0 > 1.0)
                {
                    tempMax /= 10;
                    exponent++;
                }
            }
            else
            {
                while (tempMax*10.0 < 10.0)
                {
                    tempMax *= 10;
                    exponent--;
                }
            }


            if (tempMax/1.5 >= 1)
            {
                tempMax /= 3;
                if (tempMax >= 1)
                {
                    return Math.Truncate(tempMax)*Math.Pow(10, exponent)*1.5;
                }
                else
                {
                    return Math.Truncate(tempMax*10)*Math.Pow(10, exponent - 1)*1.5;
                }
            }

            tempMax /= 2;

            if (tempMax >= 1)
            {
                return Math.Truncate(tempMax)*Math.Pow(10, exponent);
            }
            else
            {
                return Math.Truncate(tempMax*10)*Math.Pow(10, exponent - 1);
            }
        }

        public static string Normalize(double value, string measure)
        {
            string mod = string.Empty;
            double res = 0;
            if (Math.Abs(value) >= 1000)
            {
                if (Math.Abs(value) >= 1000000)
                {
                    res = value/1000000;
                    mod = "M";
                }
                else
                {
                    res = value/1000;
                    mod = "k";
                }
            }
            if ((Math.Abs(value) >= 0.1) && (Math.Abs(value) <= 1))
            {
                res = value;
            }

            if (Math.Abs(value) < 0.1)
            {
                if (Math.Abs(value) <= 0.001)
                {
                    res = value*1000000;
                    mod = "mk";
                }
                else
                {
                    res = value*1000;
                    mod = "m";
                }
            }

            if (res == 0)
            {
                res = value;
                mod = string.Empty;
            }
            string str = string.Empty;
            if (res == 0)
            {
                str = "0";
            }
            else
            {
                if (Math.Abs(res) >= 100)
                {
                    str = Math.Round(res, 0).ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    if (Math.Abs(res) >= 10)
                    {
                        str = Math.Round(res, 1).ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        str = Math.Round(res, 2).ToString(CultureInfo.InvariantCulture);
                    }
                }
            }
            return string.Format("{0} {1}{2}", str, mod, measure);
        }

        private int _run;

        public override string ToString()
        {
            return this._analogChannelConfiguration.ToString();
        }
    }

    internal class AnalogChartOptions : INotifyPropertyChanged
    {
        private string _name;
        private bool _visibility = true;
        private bool _enabledFrequency;
        private bool _enabledVector;
        private double _primaryFactor = 1.0;
        private double _secondaryFactor = 1.0;
        private bool _isPrimaryFactor = true;
        public event Action FactorChange;

        public double Factor
        {
            get { return this._isPrimaryFactor ? this._primaryFactor : this._secondaryFactor; }
        }

        public bool Vector
        {
            get { return this._enabledVector; }
            set
            {
                this._enabledVector = value;
                this.RaisePropertyChanged("Vector");
            }
        }

        public bool Frequency
        {
            get { return this._enabledFrequency; }
            set
            {
                this._enabledFrequency = value;
                this.RaisePropertyChanged("Frequency");
            }
        }
        public string ChannelName
        {
            get { return this._name; }
            set { this._name = value; }
        }

        public bool Visibility
        {
            get { return this._visibility; }
            set
            {
                this._visibility = value;
                this.RaisePropertyChanged("Visibility");
                this.RaisePropertyChanged("ChannelVisibility");
            }
        }
        
        public bool ChannelVisibility
        {
            get
            {
                return this._visibility & this._channelVisibility;
            }
            set
            {
                this._channelVisibility = value;
                this.RaisePropertyChanged("ChannelVisibility");
            }
        }
        
        public double PrimaryFactor
        {
            get { return this._primaryFactor; }
            set
            {
                this._primaryFactor = value;
                this.RaisePropertyChanged("PrimaryFactor");
            }
        }

        public double SecondaryFactor
        {
            get { return this._secondaryFactor; }
            set
            {
                this._secondaryFactor = value;
                this.RaisePropertyChanged("SecondaryFactor");
            }
        }

        public bool IsPrimaryFactor
        {
            get { return this._isPrimaryFactor; }
            set
            {
                if (this._isPrimaryFactor == value)
                {
                    return;
                }
                this._isPrimaryFactor = value;
                this.RaiseFactorChange();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private bool _channelVisibility;

        public void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }


        public void RaiseFactorChange()
        {
            if (this.FactorChange != null)
                this.FactorChange.Invoke();
        }

        public AnalogChartOptions Clone()
        {
            return (AnalogChartOptions)this.MemberwiseClone();
        }

        public static void ApplyFactors(AnalogChartOptions[] sourse, AnalogChartOptions[] desteny)
        {
            if (sourse.Length != desteny.Length)
            {
                throw new ArgumentException();
            }
            for (int i = 0; i < sourse.Length; i++)
            {
                desteny[i]._primaryFactor = sourse[i]._primaryFactor;
                desteny[i]._secondaryFactor = sourse[i]._secondaryFactor;
                desteny[i].RaiseFactorChange();
            }
        }
    }

    public class AnalogChannelConfiguration
    {
        private int _number;
        private string _name;
        private string _phase;
        private string _component;
        private string _measure;
        private double _a;
        private double _b;
        private double _skew;
        private int _min;
        private int _max;
        private int _samplingRate;

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", this._name, this._phase,
                this._component, this._measure, this._a.ToString(CultureInfo.InvariantCulture),
                this._b.ToString(CultureInfo.InvariantCulture), this._skew.ToString(CultureInfo.InvariantCulture),
                this._min, this._max);
        }

        public AnalogChannelConfiguration Clone()
        {
            return (AnalogChannelConfiguration) this.MemberwiseClone();
        }

        public AnalogChannelConfiguration(string line)
        {
            var parameters = line.Split(new[] {','});
            this._number = int.Parse(parameters[0]);
            this._name = parameters[1];
            this._phase = parameters[2];
            this._component = parameters[3];
            this._measure = parameters[4];
            this._a = this.DoubleParse(parameters[5]);
            this._b = this.DoubleParse(parameters[6]);
            this._skew = this.DoubleParse(parameters[7]);
            this._min = int.Parse(parameters[8]);
            this._max = int.Parse(parameters[9]);

        }

        public AnalogChannelConfiguration(string name, string measure, double a, double b, int min = -32768,
            int max = 32767)
        {
            this._name = name;
            this._measure = measure;
            this._a = a;
            this._b = b;
            this._min = min;
            this._max = max;
        }

        private double DoubleParse(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return 0;
            }
            return double.Parse(value, CultureInfo.InvariantCulture.NumberFormat);
        }

        public string Measure
        {
            get { return this._measure; }
        }


        public string Name
        {
            get { return this._name; }
        }

        public double A
        {
            get { return this._a; }
        }

        public double B
        {
            get { return this._b; }
        }

        internal bool IsEqual(AnalogChannelConfiguration analogChannelConfiguration)
        {
            return analogChannelConfiguration._number == this._number &
                   analogChannelConfiguration._name == this._name &
                   analogChannelConfiguration._phase == this._phase &
                   analogChannelConfiguration._component == this._component &
                   analogChannelConfiguration._measure == this._measure &
                   analogChannelConfiguration._a == this._a &
                   analogChannelConfiguration._b == this._b &
                   analogChannelConfiguration._skew == this._skew &
                   analogChannelConfiguration._min == this._min &
                   analogChannelConfiguration._max == this._max &
                   analogChannelConfiguration._samplingRate == this._samplingRate;
        }
    }

    public interface ICharacteristic
    {
        bool Enabled { get; set; }
        void Draw(Graphics context, Point mid, double factorX, double factorY, double radius, bool fill);
        PointF MaxPoint { get; }
        PointF MaxPointForCorner { get; }
        Block BlockFromLoad { get; set; }
        Direction Direction { get; set; }
        XElement ToXml();
        void FromXml(XElement element);
        string Name { get; }
        Color Color { get; }
        bool Enable { get; set; }
        bool IsValid { get; }
        void ToFirst(double koefM);
    }

    public enum Block
    {
        NONE,
        LINE,
        PHASE
    }

    public enum Direction
    {
        NONE,
        DIRECT,
        INVERSION
    }

    [Serializable]
    public class PieChartDirectionCharacteristicOption
    {
        //static Pen DarkVioletPen = new Pen(Brushes.DarkViolet, 1);
        //static Pen GoldPen = new Pen(Brushes.Gold, 1);
        public bool Enabled { get; set; }
        public double F1 { get; set; }
        public double F2 { get; set; }

        static PieChartDirectionCharacteristicOption()
        {
            //DarkVioletPen.Freeze();
            //GoldPen.Freeze();
        }

        public void FromXml(XElement element)
        {
            this.Enabled = Convert.ToBoolean(element.Attribute("Enabled").Value);
            this.F1 = Convert.ToDouble(element.Attribute("F1").Value);
            this.F2 = Convert.ToDouble(element.Attribute("F2").Value);
        }

        public XElement ToXml()
        {
            return new XElement(this.GetType().ToString(),
                new XAttribute("Enabled", true),
                new XAttribute("F1", this.F1),
                new XAttribute("F2", this.F2)
                );
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Направление \u03C61={0} \u03C62={1}", this.F1, this.F2);
        }

        public void SetClip(Graphics context, Point mid,double radius, double factorX, double factorY,double R,double X,  Direction direction)
        {
            if (direction == Direction.NONE)
            {
                return;
            }

            var redPen = new Pen(Color.Red);
            var tg1 = Math.Tan(this.F1*Math.PI/180);
            var p1 = new Point((int)Math.Round(mid.X + R * factorX), (int)Math.Round(tg1 * R * factorY + mid.Y));
            var p2 = new Point((int)Math.Round(mid.X - R * factorX), (int)(mid.Y - tg1 * R * factorY));
            var tg2 = Math.Tan((this.F2*Math.PI/180));
            var p3 = new Point((int)Math.Round(tg2 * X * factorX + mid.X), (int)Math.Round(mid.Y + X * factorY));
            var p4 = new Point((int)Math.Round(mid.X - tg2 * X * factorX), (int)Math.Round(mid.Y - X * factorY));
            GraphicsPath path = new GraphicsPath();
            if (direction == Direction.DIRECT)
            {

                var topRight = new Point((int)mid.X + (int)(radius), (int)(mid.Y - radius));
                var p5 =new Point(p4.X,topRight.Y);
                path.AddLine(mid, p1);
                path.AddLine(p1, new Point((int)Math.Round(mid.X + radius), mid.Y));
                path.AddLine(new Point((int)Math.Round(mid.X + radius), mid.Y), topRight);
                path.AddLine(topRight, p5);
                path.AddLine(p5, p4);
                path.AddLine(p4, mid);
                context.SetClip(path, CombineMode.Replace);
            }


            if (direction == Direction.INVERSION)
            {
                var botLeft = new Point((int)Math.Round(mid.X - radius), (int)Math.Round(mid.Y + radius));
                var p5 =new Point(p3.X,botLeft.Y);
                path.AddLine(mid, p2);
                path.AddLine(p2, new Point((int)(mid.X - radius), mid.Y));
                path.AddLine(new Point((int)(mid.X - radius), mid.Y), botLeft);
                path.AddLine(botLeft, p5);
                path.AddLine(p5, p3);
                path.AddLine(p3, mid);
                context.SetClip(path, CombineMode.Replace);
            }


        }

        //public void Draw(System.Windows.Media.DrawingContext context, Point mid, double factor, double radius)
        //{
        //    var tg1 = Math.Tan((this.F1) / 180.0 * Math.PI);

        //    var p1 = new Point(mid.X + radius, tg1 * radius + mid.Y);
        //    var p2 = new Point(mid.X - radius, mid.Y - tg1 * radius);


        //    var tg2 = Math.Tan((this.F2) / 180.0 * Math.PI);
        //    var p3 = new Point(tg2 * radius + mid.X, mid.Y + radius);
        //    var p4 = new Point(mid.X - tg2 * radius, mid.Y - radius);

        //    var midPoint = new Point(mid.X, mid.Y);
        //    context.DrawLine(DarkVioletPen, p1, midPoint);
        //    context.DrawLine(GoldPen, midPoint, p2);

        //    context.DrawLine(GoldPen, p3, midPoint);
        //    context.DrawLine(DarkVioletPen, midPoint, p4);

        //}

        //[XmlIgnore]
        //public System.Windows.Point MaxPoint
        //{
        //    get { return new Point(0, 0); }
        //}
    }

    [Serializable]
    public class PieChartChargeCharacteristicOption
    {
        //static Pen DeepPinkPen = new Pen(Brushes.DeepPink, 1);
        public bool Enabled { get; set; }
        public double R1 { get; set; }
        public double R2 { get; set; }
        public double F { get; set; }

        public void FromXml(XElement element)
        {
            this.Enabled = Convert.ToBoolean(element.Attribute("Enabled").Value);
            this.R1 = Convert.ToDouble(element.Attribute("R1").Value);
            this.R2 = Convert.ToDouble(element.Attribute("R2").Value);
            this.F = Convert.ToDouble(element.Attribute("F").Value);
        }

        public XElement ToXml()
        {
            return new XElement(this.GetType().ToString(),
                new XAttribute("Enabled", true),
                new XAttribute("R1", this.R1),
                new XAttribute("R2", this.R2),
                new XAttribute("F", this.F));
        }

        public void ToFirst(double koefM)
        {
            this.R1 *= koefM;
            this.R2 *= koefM;
        }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Нагрузка R1={0} R2={1} \u03C6={2}", this.R1, this.R2, this.F);
        }

        public void SetClip(Graphics context, Point mid, double factorX, double factorY, double radius)
        { 
            var tg = Math.Tan((this.F*Math.PI/180));
            var p1 = new Point((int) (mid.X + this.R1*factorX),
                (int) (mid.Y - this.R1*factorY*tg));
            var p2 = new Point((int) (mid.X + this.R1*factorX),
                (int) (mid.Y +this.R1*factorY*tg));

            var p3 = new Point((int)(mid.X + radius), (int)(mid.Y - (radius/factorX*factorY * this.R1 * factorY * tg/(this.R1*factorY))));
            var p4 = new Point((int)(mid.X + radius), (int)(mid.Y + (radius/factorX*factorY * this.R1 * factorY * tg / (this.R1 * factorY))));
           

            GraphicsPath p = new GraphicsPath();
            p.AddLines(new[] {p1, p3, p4, p2, p1});
            context.SetClip(p, CombineMode.Exclude);

            p3 = new Point((int)(mid.X - radius), (int)(mid.Y - (radius / factorX * factorY * this.R1 * factorY * tg / (this.R1 * factorY))));
            p4 = new Point((int)(mid.X - radius), (int)(mid.Y + (radius / factorX * factorY * this.R1 * factorY * tg / (this.R1 * factorY))));
            p1 = new Point((int)(mid.X - this.R2 * factorX), (int)(mid.Y - this.R1 * factorY * tg));
            p2 = new Point((int)(mid.X - this.R2 * factorX), (int)(mid.Y + this.R1 * factorY * tg));
            p = new GraphicsPath();
            p.AddLines(new[] {p1, p3, p4, p2, p1});
            context.SetClip(p, CombineMode.Exclude);
        }

    }

    [Serializable]
    public class PieChartPolyCharacteristicOption : ICharacteristic, INotifyPropertyChanged
    {
        private double _r;
        private bool _enable = true;
        public bool Enabled { get; set; }
        public Block BlockFromLoad { get; set; }
        public Direction Direction { get; set; }

        public double R
        {
            get { return this._r; }
            set
            {
                this._r = value;
                if (this.PropertyChanged != null)
                {
                    this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs("R"));
                }
            }
        }

        public double X { get; set; }
        public double F { get; set; }

        public void ToFirst(double koefM)
        {
            this.R *= koefM;
            this.X *= koefM;
        }


        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "Полигональная R={0} X={1} \u03C6={2}", this.R, this.X, this.F);
        }

        public void FromXml(XElement element)
        {
            this.Enabled = Convert.ToBoolean(element.Attribute("Enabled").Value, CultureInfo.InvariantCulture);
            this.R = Convert.ToDouble(element.Attribute("R").Value, CultureInfo.InvariantCulture);
            this.X = Convert.ToDouble(element.Attribute("X").Value, CultureInfo.InvariantCulture);
            this.F = Convert.ToDouble(element.Attribute("F").Value, CultureInfo.InvariantCulture);
        }

        public XElement ToXml()
        {
            return new XElement(this.GetType().ToString(),
                new XAttribute("Enabled", true),
                new XAttribute("R", this.R),
                new XAttribute("X", this.X),
                new XAttribute("F", this.F)
                );
        }

        public void Draw(Graphics context, Point mid, double factorX, double factorY, double radius, bool fill)
        {

            var tg = Math.Tan((90 - this.F)*Math.PI/180);
            var p1 = new Point((int) (mid.X - this.R*factorX),
                (int) (mid.Y - this.X*factorY));
            var p2 = new Point((int) (((mid.X + this.R*factorX)) + this.X*factorX*tg), (int) (mid.Y - this.X*factorY));
            var p3 = new Point((int) (mid.X + this.R*factorX), mid.Y);
            var p4 = new Point((int) (mid.X + this.R*factorX),
                (int) (mid.Y + this.X*factorY));
            var p5 = new Point((int) ((mid.X - this.R*factorX) - (this.X*factorX*tg)),
                (int) Math.Round(mid.Y + this.X*factorY));
            var p6 = new Point((int) (mid.X - this.R*factorX), mid.Y);

            GraphicsPath gp = new GraphicsPath();


            gp.AddLine(p1, p2);
            gp.AddLine(p2, p3);
            gp.AddLine(p3, p4);
            gp.AddLine(p4, p5);
            gp.AddLine(p5, p6);
            gp.AddLine(p6, p1);
            if (fill)
            {

                context.FillPath(new SolidBrush(this.Color), gp);
            }
            else
            {
                context.DrawPath(new Pen(this.Color), gp);
            }
            //   context.DrawLine(RandomPen, p1, p2);
            //context.DrawLine(RandomPen, p2, p3);
            //context.DrawLine(RandomPen, p3, p4);
            //context.DrawLine(RandomPen, p4, p5);
            //context.DrawLine(RandomPen, p5, p6);
            //context.DrawLine(RandomPen, p6, p1);

        }

        static Random rand = new Random();

        [XmlIgnore]
        public PointF MaxPoint
        {
            get
            {
                if (!this.IsValid)
                {
                    return new Point(0, 0);
                }
                return new PointF((float) (this.R + this.X*Math.Tan((90 - this.F)/180.0*Math.PI)), (float) this.X);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        public string Name { get; set; }

        public Color Color { get; set; }

        public bool Enable
        {
            get
            {
                if (!this.IsValid)
                {
                    return false;
                }
                return this._enable;
            }
            set { this._enable = value; }
        }


        public bool IsValid
        {
            get { return !(Math.Abs(this.F) <= 0.01); }
        }


        public PointF MaxPointForCorner
        {
            get
            {
                if (!this.IsValid)
                {
                    return new PointF(0, 0);
                }
                return new PointF((float)Math.Round(this.R), (float)(this.X));
            }
        }
    }

    [Serializable]
    public class PieChartRoundCharacteristicOption : ICharacteristic
    {
        static Random rand = new Random();
        private bool _enable = true;
        // static Pen ChartreusePen = new Pen(Brushes.Chartreuse, 1);
        public bool Enabled { get; set; }
        public double R { get; set; }
        public double X { get; set; }
        public double Radius { get; set; }
        public Block BlockFromLoad { get; set; }
        public Direction Direction { get; set; }


        public void ToFirst(double koefM)
        {
            this.R *= koefM;
            this.X *= koefM;
            this.Radius *= koefM;
        }

        public override string ToString()
        {

            return string.Format(CultureInfo.InvariantCulture, "Круговая R={0} X={1} r={2}", this.R, this.X, this.Radius);
        }

        public void FromXml(XElement element)
        {
            this.Enabled = Convert.ToBoolean(element.Attribute("Enabled").Value);
            this.R = Convert.ToDouble(element.Attribute("R").Value);
            this.X = Convert.ToDouble(element.Attribute("X").Value);
            this.Radius = Convert.ToDouble(element.Attribute("Radius").Value);
        }

        public XElement ToXml()
        {
            return new XElement(this.GetType().ToString(),
                new XAttribute("Enabled", true),
                new XAttribute("R", this.R),
                new XAttribute("X", this.X),
                new XAttribute("Radius", this.Radius)
                );
        }

        public void Draw(Graphics context, Point mid, double factorX, double factorY, double radius, bool fill)
        {


            var RandomPen = new Pen(this.Color);
            if (fill)
            {
                context.FillEllipse(
                    new SolidBrush(RandomPen.Color),
                    (int) (mid.X + this.R*factorX - (this.Radius*factorX)),
                    (int) (mid.Y - this.X*factorY - (this.Radius*factorX)),
                    (int) (this.Radius*2*factorX),
                    (int) (this.Radius*2*factorX));
            }
            else
            {

                context.DrawEllipse(RandomPen,
                    (int) (mid.X + this.R*factorX - (this.Radius*factorX)),
                    (int) (mid.Y - this.X*factorX - (this.Radius*factorX)),
                    (int) (this.Radius*2*factorX),
                    (int) (this.Radius*2*factorX));
            }

        }

        [XmlIgnore]
        public PointF MaxPoint
        {
            get { return new PointF((float) (Math.Abs(this.R) + this.Radius), (float) (Math.Abs(this.X) + this.Radius)); }
        }


        public string Name { get; set; }

        public Color Color { get; set; }


        public bool Enable
        {
            get { return this._enable; }
            set { this._enable = value; }
        }


        public bool IsValid
        {
            get { return true; }
        }

        [XmlIgnore]
        public PointF MaxPointForCorner
        {
            get { return new PointF((float)(Math.Abs(this.R) + this.Radius), (float)(Math.Abs(this.X) + this.Radius)); }
        }
    }
}