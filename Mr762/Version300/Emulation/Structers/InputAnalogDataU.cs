﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.Mr762.Version300.Emulation.Structers
{
    public class InputAnalogDataU: StructBase
    {
        #region [Private Fields]

        [Layout(0)]
        private ushort _a; // амплитуда
        [Layout(1)]
        private ushort _fi; // фаза (начальный сдвиг)
        [Layout(2)]
        private ushort _f; // частота
        [Layout(3)]
        private ushort _res; //резерв

        #endregion

        #region [Properties]
        /// <summary>
        /// Амлитуда
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Амлитуда")]
        public double Amplituda
        {
            get { return ValuesConverterCommon.GetU(this._a); }
            set { this._a = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Фаза
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Фаза")]
        public double Faza
        {
            get { return this._fi; }
            set { if (value < 360) this._fi = (ushort)value; }
        }

        /// <summary>
        /// Частота
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Частота")]
        public double F
        {
            get { return ValuesConverterCommon.GetU(this._f); }
            set { this._f = ValuesConverterCommon.SetU(value); }
        }

        #endregion 
    }
}


