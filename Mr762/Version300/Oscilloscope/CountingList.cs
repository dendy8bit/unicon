using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.Mr762.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr762.Version300.Configuration.Structures.Oscope;

namespace BEMN.Mr762.Version300.Oscilloscope
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 16;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 40;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int CHANNELS_COUNT = 56;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 5;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 3;
        public const int VOLTAGES_N4_COUNT = 4;
        private string _deviceType;

        public static readonly string[] INames = new[] { "Ia", "Ib", "Ic", "In", "In1" };
        public static readonly string[] UNames = new[] { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 1-40
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ������ ������� 1-8
        /// </summary>
        private ushort[][] _channels;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI;

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]


        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }  
        #endregion [����������]
        

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;
        private OscJournalStructV3 _oscJournalStruct;
        

        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] Channels
        {
            get { return this._channels; }
            set { this._channels = value; }
        }

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }
        
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
     
        public ChannelWithBase[] ChannelsWithBase
        {
            get { return this._channelsWithBase; }
        }

        private ChannelWithBase[] _channelsWithBase;
        
        private string _hdrString;
      
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }
        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[] VoltageKoefs { get; set; }

        public OscJournalStructV3 OscJournalStruct
        {
            get { return this._oscJournalStruct; }
        }

        public string HdrString
        {
            get { return this._hdrString; }
        }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStructV3 oscJournalStruct, MeasureTransStructV3 measure, ChannelWithBase[] channelWithBases, string version)
        {
            this._deviceType = Mr762Device.T5N3D42R19;

            this._channelsWithBase = channelWithBases;
            this._oscJournalStruct = oscJournalStruct;

            this._alarm = this.OscJournalStruct.Len - this.OscJournalStruct.After;

            ushort groupIndex = oscJournalStruct.GroupIndex;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n != COUNTING_SIZE) continue;
                m++;
                n = 0;
            }
            //��������
            ushort[][] d8To24 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d25To40 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d1To8AndC1To8 = this.DiscretArrayInit(this._countingArray[11]);

            ushort[][] c9To24 = this.DiscretArrayInit(this._countingArray[12]);
            ushort[][] c25To40 = this.DiscretArrayInit(this._countingArray[13]);
            ushort[][] c41To56 = this.DiscretArrayInit(this._countingArray[14]);

            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To8AndC1To8.Take(8));
            dicrets.AddRange(d8To24);
            dicrets.AddRange(d25To40);
          
            this._discrets = dicrets.ToArray();
            //������
            List<ushort[]> channels = new List<ushort[]>(CHANNELS_COUNT);
            channels.AddRange(d1To8AndC1To8.Skip(8));
            channels.AddRange(c9To24);
            channels.AddRange(c25To40);
            channels.AddRange(c41To56);
            this._channels = channels.ToArray();
            //����
            this.SetCountinCurrents(measure);
            //����������
            this.SetCountingVoltage(measure);
            this._hdrString = string.Format("�� 762 {0} {1} ������� - {2}, ��. ������� �{3}, ������ ���������� - {4}", 
               oscJournalStruct.GetDate, oscJournalStruct.GetTime, oscJournalStruct.Stage, groupIndex + 1, version);
        }

        public CountingList(ushort[] pageValue, OscJournalStructV3 oscJournalStruct, MeasureTransN4Struct measure, ChannelWithBase[] channelWithBases, string version)
        {
            this._deviceType = Mr762Device.T5N4D42R19;

            this._channelsWithBase = channelWithBases;
            this._oscJournalStruct = oscJournalStruct;

            this._alarm = this.OscJournalStruct.Len - this.OscJournalStruct.After;

            ushort groupIndex = oscJournalStruct.GroupIndex;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length / COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n != COUNTING_SIZE) continue;
                m++;
                n = 0;
            }
            //��������
            ushort[][] d8To24 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d25To40 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] d1To8AndC1To8 = this.DiscretArrayInit(this._countingArray[11]);

            ushort[][] c9To24 = this.DiscretArrayInit(this._countingArray[12]);
            ushort[][] c25To40 = this.DiscretArrayInit(this._countingArray[13]);
            ushort[][] c41To56 = this.DiscretArrayInit(this._countingArray[14]);

            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To8AndC1To8.Take(8));
            dicrets.AddRange(d8To24);
            dicrets.AddRange(d25To40);

            this._discrets = dicrets.ToArray();
            //������
            List<ushort[]> channels = new List<ushort[]>(CHANNELS_COUNT);
            channels.AddRange(d1To8AndC1To8.Skip(8));
            channels.AddRange(c9To24);
            channels.AddRange(c25To40);
            channels.AddRange(c41To56);
            this._channels = channels.ToArray();
            //����
            this.SetCountinCurrents(measure);
            //����������
            this.SetCountingVoltage(measure);
            this._hdrString = string.Format("�� 762 {0} {1} ������� - {2}, ��. ������� �{3}, ������ ���������� - {4}",
               oscJournalStruct.GetDate, oscJournalStruct.GetTime, oscJournalStruct.Stage, groupIndex + 1, version);
        }

        private void SetCountinCurrents(MeasureTransStructV3 measure)
        {
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)a).ToArray();
                double koef = 0;
                if (i == CURRENTS_COUNT - 1)
                {
                    koef = measure.ChannelI.Ittx1;
                }
                else if (i == CURRENTS_COUNT - 2)
                {
                    koef = measure.ChannelI.Ittx;
                }
                else
                {
                     koef = measure.ChannelI.Ittl;
                }

                currentsKoefs[i] = koef * 40 * 2 * Math.Sqrt(2) / 65536.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;
        }

        private void SetCountinCurrents(MeasureTransN4Struct measure)
        {
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)a).ToArray();
                double koef = 0;
                if (i == CURRENTS_COUNT - 1)
                {
                    koef = measure.ChannelI.Ittx1;
                }
                else if (i == CURRENTS_COUNT - 2)
                {
                    koef = measure.ChannelI.Ittx;
                }
                else
                {
                    koef = measure.ChannelI.Ittl;
                }

                currentsKoefs[i] = koef * 40 * 2 * Math.Sqrt(2) / 65536.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;
        }

        private void SetCountingVoltage(MeasureTransN4Struct measure)
        {
            this._voltages = new double[VOLTAGES_N4_COUNT][];
            this._baseVoltages = new short[VOLTAGES_N4_COUNT][];
            double[] voltageKoefs = new double[VOLTAGES_N4_COUNT];

            for (int i = 0; i < VOLTAGES_N4_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + CURRENTS_COUNT].Select(a => (short)a).ToArray();
                double koef; //= i == VOLTAGES_N5_COUNT - 1 ?  : measure.ChannelU.KthlValue;
                if (i < VOLTAGES_N4_COUNT - 2)
                {
                    koef = measure.ChannelU.KthlValue;
                }
                else if (i == VOLTAGES_N4_COUNT - 2)
                {
                    koef = measure.ChannelU.KthxValue;
                }
                else
                {
                    koef = measure.ChannelU.Kthx1Value;
                }

                voltageKoefs[i] = koef * 2 * Math.Sqrt(2) / 256.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i] * (double)a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;
        }

        private void SetCountingVoltage(MeasureTransStructV3 measure)
        {
            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            double[] voltageKoefs = new double[VOLTAGES_COUNT];

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i + CURRENTS_COUNT].Select(a => (short)a).ToArray();
                voltageKoefs[i] = measure.ChannelU.KthlValue * 2 * Math.Sqrt(2) / 256.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i] * (double)a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;
        }

        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._channels = new ushort[CHANNELS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];

            this._count = count;
        }
        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }
            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this._hdrString);
                for (int i = 0; i < this._channelsWithBase.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}, {2}", i + 1, this._channelsWithBase[i].Channel, this._channelsWithBase[i].Base);
                }
                hdrFile.WriteLine(1251);
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP762,1");
                cgfFile.WriteLine(this._deviceType == Mr762Device.T5N4D42R19 ? "105,9A,96D" : "104,8A,96D");
                int index = 1;
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, INames[i],
                        this.CurrentsKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Voltages.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index, UNames[i], this.VoltageKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this._discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                for (int i = 0; i < this._channels.Length; i++)
                {
                    try
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, this._channelsWithBase[i].ChannelStr);
                        index++;
                    }
                    catch (Exception)
                    {
                        cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, "Error");
                        index++;
                    }
                }

                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this._alarm));
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);

                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (short[] voltage in this._baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in this._discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    foreach (ushort[] chanel in this._channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }
        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string hdrString = hdrStrings[0];

            ChannelWithBase[] channelWithBase = new ChannelWithBase[CHANNELS_COUNT];
            for (int i = 0; i < CHANNELS_COUNT; i++)
            {
                string[] channelAndBase =
                    hdrStrings[1 + i].Split(new[] { '=', ' ' }, StringSplitOptions.RemoveEmptyEntries)[1]
                        .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (channelAndBase.Length > 1)
                {
                    channelWithBase[i] = new ChannelWithBase
                    {
                        Channel = Convert.ToUInt16(channelAndBase[0]),
                        Base = Convert.ToByte(channelAndBase[1])
                    };
                }
                else
                {
                    channelWithBase[i] = new ChannelWithBase
                    {
                        Channel = Convert.ToUInt16(channelAndBase[0]),
                        Base = 0
                    };
                }
            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            //������ ���������� ���������� �������
            int analogCount =
                int.Parse(cfgStrings[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[1].Replace("A", string.Empty));

            double[] factors = new double[analogCount];
            Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,\,\,[AV]\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2 + analogCount; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2] = double.Parse(res, format);
            }
            int counts = int.Parse(cfgStrings[2 + analogCount + DISCRETS_COUNT + CHANNELS_COUNT + 2].Replace("1000,", string.Empty));
            int alarm = 0;
            try
            {
                string startTime = cfgStrings[2 + analogCount + DISCRETS_COUNT + CHANNELS_COUNT + 3].Split(',')[1];
                string runTime = cfgStrings[2 + analogCount + DISCRETS_COUNT + CHANNELS_COUNT + 4].Split(',')[1];

                TimeSpan a = DateTime.Parse(runTime) - DateTime.Parse(startTime);
                alarm = (int)a.TotalMilliseconds;
            }
            catch (Exception)
            {
                // ignored
            }
            CountingList result = new CountingList(counts) { _alarm = alarm };
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = analogCount - CURRENTS_COUNT == VOLTAGES_N4_COUNT
                ? new double[VOLTAGES_N4_COUNT][]
                : new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            ushort[][] channels = new ushort[CHANNELS_COUNT][];

            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                string[] means = datStrings[i].Split(',');
                for (int j = 0; j < currents.Length; j++)
                {
                    currents[j][i] = Convert.ToDouble(means[j + 2], CultureInfo.InvariantCulture) * factors[j];
                }

                for (int j = 0; j < voltages.Length; j++)
                {
                    voltages[j][i] = Convert.ToDouble(means[j + 2 + currents.Length], CultureInfo.InvariantCulture) * factors[j + currents.Length];
                }

                for (int j = 0; j < discrets.Length; j++)
                {
                    discrets[j][i] = Convert.ToUInt16(means[j + 2 + currents.Length + voltages.Length]);
                }

                for (int j = 0; j < channels.Length; j++)
                {
                    channels[j][i] = Convert.ToUInt16(means[j + 2 + currents.Length + voltages.Length + discrets.Length]);
                }
            }
            result._minI = double.MaxValue;
            result._maxI = double.MinValue;
            for (int i = 0; i < currents.Length; i++)
            {
                result._maxI = Math.Max(result._maxI, currents[i].Max());
                result._minI = Math.Min(result._minI, currents[i].Min());
            }
            result._minU = double.MaxValue;
            result._maxU = double.MinValue;
            for (int i = 0; i < voltages.Length; i++)
            {
                result._maxU = Math.Max(result._maxU, voltages[i].Max());
                result._minU = Math.Min(result._minU, voltages[i].Min());
            }

            result._currents = currents;
            result._channels = channels;
            result._discrets = discrets;
            result._voltages = voltages;
            result._channelsWithBase = channelWithBase;
            result._hdrString = hdrString;
            result.IsLoad = true;
            result.FilePath = filePath;
            return result;
        }
    }
}
