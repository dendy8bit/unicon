﻿using System;
using System.Collections;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr762.Version300.Diagnostic
{
    public class ConfigDiagnosticStruct : StructBase
    {
        [Layout(0)] private ushort _status;     //остановлена/запущена статистика
        [Layout(1)] private ushort _maska;      //фильтр неисправностей
        [Layout(2)] private ushort _maskaMod;   //фильтр модулей
        [Layout(3)] private ushort _len;        //число сохраняемых кадров

        public bool Status
        {
            get { return Common.GetBit(this._status, 0); }
            set { this._status = Common.SetBit(this._status, 0, value); }
        }

        public BitArray FaultsConfig
        {
            get
            {
                return new BitArray(Common.TOBYTE(this._maska, false));
            }
            set
            {
                int val = 0;
                for (int i = 0; i < value.Count; i++)
                {
                    if (i != 7)
                    {
                        val += value[i] ? (int) Math.Pow(2, i) : 0;
                    }
                    else
                    {
                        val += value[i] ? (int)Math.Pow(2, 15) : 0;
                    }
                    this._maska = (ushort) val;
                }
            }
        }

        public BitArray ModulesConfig
        {
            get { return new BitArray(Common.TOBYTE(this._maskaMod, false)); }
            set
            {
                int val = 0;
                for (int i = 0; i < value.Count; i++)
                {
                    val += value[i] ? (int)Math.Pow(2, i) : 0;
                    this._maskaMod = (ushort)val;
                }
            }
        }

        public ushort CountFrames
        {
            get { return this._len; }
            set { this._len = value; }
        }
    }
}
