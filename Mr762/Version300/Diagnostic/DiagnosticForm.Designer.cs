﻿namespace BEMN.Mr762.Version300.Diagnostic
{
    partial class DiagnosticForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.err8 = new System.Windows.Forms.CheckBox();
            this.err7 = new System.Windows.Forms.CheckBox();
            this.err6 = new System.Windows.Forms.CheckBox();
            this.err5 = new System.Windows.Forms.CheckBox();
            this.err4 = new System.Windows.Forms.CheckBox();
            this.err3 = new System.Windows.Forms.CheckBox();
            this.err2 = new System.Windows.Forms.CheckBox();
            this.err1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.module5 = new System.Windows.Forms.CheckBox();
            this.module4 = new System.Windows.Forms.CheckBox();
            this.module3 = new System.Windows.Forms.CheckBox();
            this.module2 = new System.Windows.Forms.CheckBox();
            this.module1 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LoadLogListBtn = new System.Windows.Forms.Button();
            this.LogListTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.SaveConfigDiagnosticBtn = new System.Windows.Forms.Button();
            this.LoadConfigDiagnosticBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.workLed = new BEMN.Forms.LedControl();
            this.StopDiagnosticBtn = new System.Windows.Forms.Button();
            this.StartDiagnosticBtn = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Len = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._counter = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.err8);
            this.groupBox1.Controls.Add(this.err7);
            this.groupBox1.Controls.Add(this.err6);
            this.groupBox1.Controls.Add(this.err5);
            this.groupBox1.Controls.Add(this.err4);
            this.groupBox1.Controls.Add(this.err3);
            this.groupBox1.Controls.Add(this.err2);
            this.groupBox1.Controls.Add(this.err1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 168);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Типы неисправностей модулей";
            // 
            // err8
            // 
            this.err8.AutoSize = true;
            this.err8.Location = new System.Drawing.Point(6, 145);
            this.err8.Name = "err8";
            this.err8.Size = new System.Drawing.Size(100, 17);
            this.err8.TabIndex = 0;
            this.err8.Text = "Норма модуля";
            this.err8.UseVisualStyleBackColor = true;
            // 
            // err7
            // 
            this.err7.AutoSize = true;
            this.err7.Location = new System.Drawing.Point(6, 127);
            this.err7.Name = "err7";
            this.err7.Size = new System.Drawing.Size(192, 17);
            this.err7.TabIndex = 0;
            this.err7.Text = "Ошибка CRC, принятое модулем";
            this.err7.UseVisualStyleBackColor = true;
            // 
            // err6
            // 
            this.err6.AutoSize = true;
            this.err6.Location = new System.Drawing.Point(6, 109);
            this.err6.Name = "err6";
            this.err6.Size = new System.Drawing.Size(91, 17);
            this.err6.TabIndex = 0;
            this.err6.Text = "Ошибка CRC";
            this.err6.UseVisualStyleBackColor = true;
            // 
            // err5
            // 
            this.err5.AutoSize = true;
            this.err5.Location = new System.Drawing.Point(6, 91);
            this.err5.Name = "err5";
            this.err5.Size = new System.Drawing.Size(196, 17);
            this.err5.TabIndex = 0;
            this.err5.Text = "Ошибка для аналоговых модулей";
            this.err5.UseVisualStyleBackColor = true;
            // 
            // err4
            // 
            this.err4.AutoSize = true;
            this.err4.Location = new System.Drawing.Point(6, 73);
            this.err4.Name = "err4";
            this.err4.Size = new System.Drawing.Size(151, 17);
            this.err4.TabIndex = 0;
            this.err4.Text = "Ошибка запроса модуля";
            this.err4.UseVisualStyleBackColor = true;
            // 
            // err3
            // 
            this.err3.AutoSize = true;
            this.err3.Location = new System.Drawing.Point(6, 55);
            this.err3.Name = "err3";
            this.err3.Size = new System.Drawing.Size(145, 17);
            this.err3.TabIndex = 0;
            this.err3.Text = "Неисправность модуля";
            this.err3.UseVisualStyleBackColor = true;
            // 
            // err2
            // 
            this.err2.AutoSize = true;
            this.err2.Location = new System.Drawing.Point(6, 37);
            this.err2.Name = "err2";
            this.err2.Size = new System.Drawing.Size(132, 17);
            this.err2.TabIndex = 0;
            this.err2.Text = "Ошибка типа модуля";
            this.err2.UseVisualStyleBackColor = true;
            // 
            // err1
            // 
            this.err1.AutoSize = true;
            this.err1.Location = new System.Drawing.Point(6, 19);
            this.err1.Name = "err1";
            this.err1.Size = new System.Drawing.Size(85, 17);
            this.err1.TabIndex = 0;
            this.err1.Text = "Нет модуля";
            this.err1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.module5);
            this.groupBox2.Controls.Add(this.module4);
            this.groupBox2.Controls.Add(this.module3);
            this.groupBox2.Controls.Add(this.module2);
            this.groupBox2.Controls.Add(this.module1);
            this.groupBox2.Location = new System.Drawing.Point(12, 186);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(113, 114);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Номера модулей";
            // 
            // module5
            // 
            this.module5.AutoSize = true;
            this.module5.Location = new System.Drawing.Point(6, 91);
            this.module5.Name = "module5";
            this.module5.Size = new System.Drawing.Size(73, 17);
            this.module5.TabIndex = 0;
            this.module5.Text = "Модуль 5";
            this.module5.UseVisualStyleBackColor = true;
            // 
            // module4
            // 
            this.module4.AutoSize = true;
            this.module4.Location = new System.Drawing.Point(6, 73);
            this.module4.Name = "module4";
            this.module4.Size = new System.Drawing.Size(73, 17);
            this.module4.TabIndex = 0;
            this.module4.Text = "Модуль 4";
            this.module4.UseVisualStyleBackColor = true;
            // 
            // module3
            // 
            this.module3.AutoSize = true;
            this.module3.Location = new System.Drawing.Point(6, 55);
            this.module3.Name = "module3";
            this.module3.Size = new System.Drawing.Size(73, 17);
            this.module3.TabIndex = 0;
            this.module3.Text = "Модуль 3";
            this.module3.UseVisualStyleBackColor = true;
            // 
            // module2
            // 
            this.module2.AutoSize = true;
            this.module2.Location = new System.Drawing.Point(6, 37);
            this.module2.Name = "module2";
            this.module2.Size = new System.Drawing.Size(73, 17);
            this.module2.TabIndex = 0;
            this.module2.Text = "Модуль 2";
            this.module2.UseVisualStyleBackColor = true;
            // 
            // module1
            // 
            this.module1.AutoSize = true;
            this.module1.Location = new System.Drawing.Point(6, 19);
            this.module1.Name = "module1";
            this.module1.Size = new System.Drawing.Size(73, 17);
            this.module1.TabIndex = 0;
            this.module1.Text = "Модуль 1";
            this.module1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.LoadLogListBtn);
            this.groupBox3.Controls.Add(this.LogListTextBox);
            this.groupBox3.Location = new System.Drawing.Point(228, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(444, 452);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Диагностика модулей";
            // 
            // LoadLogListBtn
            // 
            this.LoadLogListBtn.Location = new System.Drawing.Point(121, 423);
            this.LoadLogListBtn.Name = "LoadLogListBtn";
            this.LoadLogListBtn.Size = new System.Drawing.Size(205, 23);
            this.LoadLogListBtn.TabIndex = 1;
            this.LoadLogListBtn.Text = "Загрузить файл диагностики";
            this.LoadLogListBtn.UseVisualStyleBackColor = true;
            // 
            // LogListTextBox
            // 
            this.LogListTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogListTextBox.Location = new System.Drawing.Point(3, 16);
            this.LogListTextBox.Name = "LogListTextBox";
            this.LogListTextBox.ReadOnly = true;
            this.LogListTextBox.Size = new System.Drawing.Size(438, 401);
            this.LogListTextBox.TabIndex = 0;
            this.LogListTextBox.Text = "";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.SaveConfigDiagnosticBtn);
            this.groupBox5.Controls.Add(this.LoadConfigDiagnosticBtn);
            this.groupBox5.Location = new System.Drawing.Point(12, 306);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(210, 67);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            // 
            // SaveConfigDiagnosticBtn
            // 
            this.SaveConfigDiagnosticBtn.Location = new System.Drawing.Point(6, 36);
            this.SaveConfigDiagnosticBtn.Name = "SaveConfigDiagnosticBtn";
            this.SaveConfigDiagnosticBtn.Size = new System.Drawing.Size(198, 23);
            this.SaveConfigDiagnosticBtn.TabIndex = 0;
            this.SaveConfigDiagnosticBtn.Text = "Сохранить конфигурацию";
            this.SaveConfigDiagnosticBtn.UseVisualStyleBackColor = true;
            this.SaveConfigDiagnosticBtn.Click += new System.EventHandler(this.SaveConfigDiagnosticBtn_Click);
            // 
            // LoadConfigDiagnosticBtn
            // 
            this.LoadConfigDiagnosticBtn.Location = new System.Drawing.Point(6, 14);
            this.LoadConfigDiagnosticBtn.Name = "LoadConfigDiagnosticBtn";
            this.LoadConfigDiagnosticBtn.Size = new System.Drawing.Size(198, 23);
            this.LoadConfigDiagnosticBtn.TabIndex = 0;
            this.LoadConfigDiagnosticBtn.Text = "Загрузить конфигурацию";
            this.LoadConfigDiagnosticBtn.UseVisualStyleBackColor = true;
            this.LoadConfigDiagnosticBtn.Click += new System.EventHandler(this.LoadConfigDiagnosticBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._counter);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.workLed);
            this.groupBox4.Controls.Add(this.StopDiagnosticBtn);
            this.groupBox4.Controls.Add(this.StartDiagnosticBtn);
            this.groupBox4.Location = new System.Drawing.Point(12, 379);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(210, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // workLed
            // 
            this.workLed.Location = new System.Drawing.Point(191, 19);
            this.workLed.Name = "workLed";
            this.workLed.Size = new System.Drawing.Size(13, 13);
            this.workLed.State = BEMN.Forms.LedState.Off;
            this.workLed.TabIndex = 1;
            // 
            // StopDiagnosticBtn
            // 
            this.StopDiagnosticBtn.Enabled = false;
            this.StopDiagnosticBtn.Location = new System.Drawing.Point(6, 38);
            this.StopDiagnosticBtn.Name = "StopDiagnosticBtn";
            this.StopDiagnosticBtn.Size = new System.Drawing.Size(179, 23);
            this.StopDiagnosticBtn.TabIndex = 0;
            this.StopDiagnosticBtn.Tag = "False";
            this.StopDiagnosticBtn.Text = "Остановить диагностику";
            this.StopDiagnosticBtn.UseVisualStyleBackColor = true;
            this.StopDiagnosticBtn.Click += new System.EventHandler(this.StartStopDiagnosticBtn_Click);
            // 
            // StartDiagnosticBtn
            // 
            this.StartDiagnosticBtn.Enabled = false;
            this.StartDiagnosticBtn.Location = new System.Drawing.Point(6, 13);
            this.StartDiagnosticBtn.Name = "StartDiagnosticBtn";
            this.StartDiagnosticBtn.Size = new System.Drawing.Size(179, 23);
            this.StartDiagnosticBtn.TabIndex = 0;
            this.StartDiagnosticBtn.Tag = "True";
            this.StartDiagnosticBtn.Text = "Запустить диагностику";
            this.StartDiagnosticBtn.UseVisualStyleBackColor = true;
            this.StartDiagnosticBtn.Click += new System.EventHandler(this.StartStopDiagnosticBtn_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Len);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(131, 186);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(91, 114);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            // 
            // Len
            // 
            this.Len.Location = new System.Drawing.Point(6, 54);
            this.Len.Name = "Len";
            this.Len.Size = new System.Drawing.Size(79, 20);
            this.Len.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 39);
            this.label1.TabIndex = 5;
            this.label1.Text = "Количетво\r\nсохраняемых\r\nкадров";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Всего обменов:";
            // 
            // _counter
            // 
            this._counter.AutoSize = true;
            this._counter.Location = new System.Drawing.Point(99, 64);
            this._counter.Name = "_counter";
            this._counter.Size = new System.Drawing.Size(0, 13);
            this._counter.TabIndex = 2;
            // 
            // DiagnosticForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 476);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DiagnosticForm";
            this.Text = "DiagnosticForm";
            this.Load += new System.EventHandler(this.DiagnosticForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox err8;
        private System.Windows.Forms.CheckBox err7;
        private System.Windows.Forms.CheckBox err6;
        private System.Windows.Forms.CheckBox err5;
        private System.Windows.Forms.CheckBox err4;
        private System.Windows.Forms.CheckBox err3;
        private System.Windows.Forms.CheckBox err2;
        private System.Windows.Forms.CheckBox err1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox module5;
        private System.Windows.Forms.CheckBox module4;
        private System.Windows.Forms.CheckBox module3;
        private System.Windows.Forms.CheckBox module2;
        private System.Windows.Forms.CheckBox module1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button LoadLogListBtn;
        private System.Windows.Forms.RichTextBox LogListTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button SaveConfigDiagnosticBtn;
        private System.Windows.Forms.Button LoadConfigDiagnosticBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button StopDiagnosticBtn;
        private System.Windows.Forms.Button StartDiagnosticBtn;
        private BEMN.Forms.LedControl workLed;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.MaskedTextBox Len;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label _counter;
        private System.Windows.Forms.Label label2;
    }
}