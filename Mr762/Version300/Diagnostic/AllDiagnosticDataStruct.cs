﻿using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version300.Diagnostic
{
    public class AllDiagnosticDataStruct : StructBase
    {
        [Layout(0, Count = 30)] private DiagnosticDataStruct[] _allDiagnostic;
        [Layout(1, Count = 4)] private ushort[] _res;

        public DiagnosticDataStruct[] AllDiagnostic
        {
            get { return this._allDiagnostic; }
        }

        public void SortByError()
        {
            this._allDiagnostic = this._allDiagnostic.OrderBy(d => d.Error).ToArray();
        }
    }
}
