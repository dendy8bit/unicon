using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version204.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����������
    /// </summary>
    public class AllIndicatorsStructV204 : StructBase, IDgvRowsContainer<IndicatorsStructV204>
    {
        public const int INDICATORS_COUNT = 12;

        /// <summary>
        /// ����������
        /// </summary>
        [Layout(0, Count = INDICATORS_COUNT)]
        private IndicatorsStructV204[] _indicators;

        /// <summary>
        /// ����������
        /// </summary>
        [XmlArray(ElementName = "���_����������")]
        public IndicatorsStructV204[] Rows
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }
    }
}