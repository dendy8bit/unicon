﻿using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;
using BEMN.Mr762.Version2.Configuration;

namespace BEMN.Mr762.Version204.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsStructV204 : StructBase
    {
        #region [Private fields]

        [Layout(0)]
        private ushort _signal;
        [Layout(1)]
        private ushort _type;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Тип
        /// </summary>
         [BindingProperty(0)]
         [XmlAttribute(AttributeName = "Тип")]
         public string Type
        {
            get
            {
                var index = Common.GetBits(this._type, 0);
                return StringsConfig.ReleyType[index];
            }
            set
            {
                var index = (ushort) StringsConfig.ReleyType.IndexOf(value);
                this._type = Common.SetBits(this._type, index, 0);
            }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string Signal
        {
            get { return Validator.Get(this._signal,StringsConfig.RelaySignalsV204); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignalsV204); }
        }

        /// <summary>
        /// Цвет
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(Type = typeof(XmlColor), ElementName =  "Цвет_индикатора")]
        public Color Color
        {
            get { return Common.GetBit(this._type, 8) ? Color.Green : Color.Red; }
            set
            {
                var bit = value == Color.Green;
                this._type = Common.SetBit(this._type, 8, bit);
            }
        }

        #endregion [Properties]
    }
}
