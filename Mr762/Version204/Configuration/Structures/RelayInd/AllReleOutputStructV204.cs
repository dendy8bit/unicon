using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version204.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStructV204 : StructBase, IDgvRowsContainer<ReleOutputStructV204>
    {
        public const int RELAY_COUNT = 32;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT)]
        private ReleOutputStructV204[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]
    
       public ReleOutputStructV204[] Rows
        {
            get
            {
                return this._relays;
            }
            set
            {
                this._relays = value;
            }
        }
    }
}