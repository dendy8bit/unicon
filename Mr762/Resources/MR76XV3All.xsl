<?xml version="1.0" encoding="windows-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
 <script type="text/javascript">
   function translateBoolean(value, elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "��";
   else if(value.toString().toLowerCase() == "false")
   result = "���";
   else
   result = "������������ ��������"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeText(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "�� ����."
   else if(value.toString().toLowerCase() == "false")
   result = "�� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR1(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX1(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextRp(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR1line(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R1, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R1, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR2line(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R2, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R2, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextRS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextXS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextdzS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "dz, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "dz, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextFS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "f, ����/�, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "f, ����/�, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   
   function changeTextX1Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��1, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��1, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX2Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��2, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��2, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX3Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��3, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��3, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX4Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��4, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��4, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX5Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��5, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��5, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
 </script>

</head>
  <body>
    <xsl:variable name="vers" select="��762/������"></xsl:variable>
    <xsl:variable name="plant" select="��762/������������_����������"></xsl:variable>
    <xsl:variable name="isPrimary" select="��762/���������_�������"/>
    <h3>���������� <xsl:value-of select="��762/���_����������"/>. ����� ���������� <xsl:value-of select="��762/�����_����������"/>. ������ �� <xsl:value-of select="$vers"/>. ������������ ���������� <xsl:value-of select="$plant"/></h3>
	  <p></p>
	
	  <div name="�������">
      <h2>�������</h2>
      
      <table border="1" cellspacing="0">
        <tr>
          <th bgcolor="c1ced5">���� �������� �������</th>
			    <th><xsl:value-of select="��762/����_��������_������/InputAdd"/></th>
		    </tr>
	    </table>
	    <p></p>
	    <table border="1" cellspacing="0">
		    <tr>
			    <th bgcolor="c1ced5">���� ������������� � ��������� ���������</th>
			    <xsl:element name="th">
			    <xsl:attribute name="id">InputParam_<xsl:value-of select="position()"/></xsl:attribute>            
			    <script>translateBoolean(<xsl:value-of select="$isPrimary"/>,"InputParam_<xsl:value-of select="position()"/>");</script>
			    </xsl:element>
		    </tr>
	    </table>
      
      <div name="$group">
        <xsl:for-each select="��762/������������_����_�����_�������/������">
            <h2>������ ������� <xsl:value-of select="position()"/></h2>
            <h4>��������� ���������</h4>
            <p></p>
            <h4>����</h4>
            <table border="1" cellspacing="0">
              <tr bgcolor="#c1ced5">
                 <th>��� ��</th>
                 <th>I�, I�</th>
		             <th>ITT�, A</th>
                 <th>ITTn, A</th>
                 <th>ITTn1, A</th>
                 <th>������� ����</th>
              </tr>
              <tr align="center">
                <td><xsl:value-of select="MeasureTrans/�����_I/���_��"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_I/I�"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_I/������������_��"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_I/������������_����"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_I/������������_����1"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_I/�������_����"/></td>
              </tr>
            </table>
            <p></p>
            
            <h4>���</h4>
            <table border="1" cellspacing="0">
              <tr bgcolor="#c1ced5">
                <th>�����</th>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeX1Y<xsl:value-of select="position()"/></xsl:attribute>  
                  <script>changeTextX1Y(<xsl:value-of select="$isPrimary"/>,"_ChangeX1Y<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeX2Y<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextX2Y(<xsl:value-of select="$isPrimary"/>,"_ChangeX2Y<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeX3Y<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextX3Y(<xsl:value-of select="$isPrimary"/>,"_ChangeX3Y<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeX4Y<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextX4Y(<xsl:value-of select="$isPrimary"/>,"_ChangeX4Y<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeX5Y<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextX5Y(<xsl:value-of select="$isPrimary"/>,"_ChangeX5Y<xsl:value-of select="position()"/>");
                  </script>
                </xsl:element>
                <th>L1, ��</th>
                <th>L2, ��</th>
                <th>L3, ��</th>
                <th>L4, ��</th>
              </tr>
              <tr align="center">
                <td><xsl:value-of select="Omp/�����"/></td>
                <td><xsl:value-of select="Omp/X��1"/></td>
                <td><xsl:value-of select="Omp/X��2"/></td>
                <td><xsl:value-of select="Omp/X��3"/></td>
                <td><xsl:value-of select="Omp/X��4"/></td>
                <td><xsl:value-of select="Omp/X��5"/></td>
                <td><xsl:value-of select="Omp/L1"/></td>
                <td><xsl:value-of select="Omp/L2"/></td>
                <td><xsl:value-of select="Omp/L3"/></td>
                <td><xsl:value-of select="Omp/L4"/></td>
              </tr>
            </table>
        
            <p></p>
            <h4>����������</h4>
            <table border="1" cellspacing="0">
              <tr bgcolor="#c1ced5">
                <th>Uo*</th>
                <th>KTH�</th>
                <th>KTHn</th>
              </tr>
              <tr align="center">
                <td><xsl:value-of select="MeasureTrans/�����_U/���_Uo"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_U/KTHL"/></td>
                <td><xsl:value-of select="MeasureTrans/�����_U/KTHX"/></td>
             </tr>
             </table>
            <p>*-���������� ����� � ����������� ��� ����� I* � ������ 3I0 ��� ln, ������ Ir</p>
        
            <h4>��������� ���������</h4>
            <table border="1" cellspacing="0">
              <tr bgcolor="#c1ced5">
                <th>T �������, �</th>
                <th>T ����������, �</th>
                <th>I��, I�</th>
                <th>I����, I�</th>
                <th>T����, ��</th>
                <th>T����, �</th>
                <th>Q���., %</th>
                <th>���� Q �����</th>
                <th>���� N �����</th>
              </tr>

              <tr align="center">
                <td><xsl:value-of select="TermConfig/T���"/></td>
                <td><xsl:value-of select="TermConfig/T���"/></td>
                <td><xsl:value-of select="TermConfig/I��"/></td>
                <td><xsl:value-of select="TermConfig/I����"/></td>
                <td><xsl:value-of select="TermConfig/T����"/></td>
                <td><xsl:value-of select="TermConfig/T����"/></td>
                <td><xsl:value-of select="TermConfig/Q"/></td>
                <td><xsl:value-of select="TermConfig/Q_�����"/></td>
                <td><xsl:value-of select="TermConfig/N_�����"/></td>
              </tr>
            </table>
            <p></p>
        
            <h4>�������� ����� ��</h4>
            <table border="1" cellspacing="0">
	          <td>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th>������������� ��</th>
			          <th>������������� ��n</th>
			          <th>Umax, �</th>
			          <th>Umin, �</th>
			          <th>Imax, I�</th>
			          <th>Imin, I�</th>
			          <th>Td, ��</th>
			          <th>Ts, ��</th>
			          <th>�����</th>
			          <th>���. 3-� ���</th>
			          <th>dI, %</th>
			          <th>dU, %</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="CheckTn/LfaultXml"/></td>
			          <td><xsl:value-of select="CheckTn/XfaultXml"/></td>
			          <td><xsl:value-of select="CheckTn/Umax"/></td>
			          <td><xsl:value-of select="CheckTn/Umin"/></td>
			          <td><xsl:value-of select="CheckTn/Imax"/></td>
			          <td><xsl:value-of select="CheckTn/Imin"/></td>
			          <td><xsl:value-of select="CheckTn/Td"/></td>
			          <td><xsl:value-of select="CheckTn/Ts"/></td>
			          <td><xsl:value-of select="CheckTn/Reset"/></td>
			          <xsl:element name="td">
			            <xsl:attribute name="id">_Phase3<xsl:value-of select="position()"/></xsl:attribute>            
			            <script>translateBoolean(<xsl:value-of select="CheckTn/Phase3"/>,"_Phase3<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="CheckTn/Idelta"/></td>
			          <td><xsl:value-of select="CheckTn/Udelta"/></td>
		          </tr>
              
	          </table>
              <table border="1" cellpadding="4" cellspacing="0">
                <tr>
                  <xsl:element name="td"><xsl:attribute name="id">_U2I2<xsl:value-of select="position()"/></xsl:attribute>
                    <script>translateBoolean(<xsl:value-of select="CheckTn/U2I2"/>,"_U2I2<xsl:value-of select="position()"/>");</script>
                  </xsl:element>
                  <th bgcolor="c1ced5">I2, I�</th>
                  <td><xsl:value-of select="CheckTn/I2"/></td>
                  <th bgcolor="c1ced5">U2, �</th>
                  <td><xsl:value-of select="CheckTn/U2"/></td>
		            </tr>
		            <tr>
			            <xsl:element name="td">
                    <xsl:attribute name="id">_U0I0<xsl:value-of select="position()"/></xsl:attribute>
                    <script>translateBoolean(<xsl:value-of select="CheckTn/U0I0"/>,"_U0I0<xsl:value-of select="position()"/>");</script>
			            </xsl:element>
                  <th bgcolor="c1ced5">3I0, I�</th>
                  <td><xsl:value-of select="CheckTn/I0"/></td>
                  <th bgcolor="c1ced5">3U0, �</th>
                  <td><xsl:value-of select="CheckTn/U0"/></td>
		            </tr>
              </table>
            </td>
            </table>
          <p></p>
          <h4>���������� ������</h4>
          <table border="1" cellspacing="0">
            <tr bgcolor="#c1ced5" align="center">
              <th>cosf</th>
              <th>���, %</th>
              <th>P</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="DefensesSetpoints/��������/Cosf"/>
              </td>
              <td>
                <xsl:value-of select="DefensesSetpoints/��������/���"/>
              </td>
              <td>
                <xsl:value-of select="TermConfig/P"/>
                <xsl:value-of select="TermConfig/�����������"/>
              </td>
            </tr>
          </table>
            <p></p>
        
            <h3>�������.���.</h3>
            <p></p>
            <h4>����� ���������</h4>
            <p></p>
	
	          <h4>�����. ����������� ��</h4>
	          <table border="1" cellspacing="0">
	          <th>
	          <table>
		          <tr>������ �-N1</tr>
			          <tr>
				          <th bgcolor="c1ced5">Z0=</th>
				          <th><xsl:value-of select="ResistanceParam/R0Step1"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X0Step1"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
			          <tr>
				          <th bgcolor="c1ced5">Z1=</th>
				          <th><xsl:value-of select="ResistanceParam/R1Step1"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X1Step1"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change1<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change1<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
	          </table>
	          <table>
		          <tr>������ �-N2</tr>
			          <tr>
				          <th bgcolor="c1ced5">Z0=</th>
				          <th><xsl:value-of select="ResistanceParam/R0Step2"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X0Step2"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change2<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change2<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
			          <tr>
				          <th bgcolor="c1ced5">Z1=</th>
				          <th><xsl:value-of select="ResistanceParam/R1Step2"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X1Step2"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change3<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change3<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
	          </table>
	          <table>
              <tr>������ �-N3</tr>
              <tr>
                <th bgcolor="c1ced5">Z0=</th>
                <th><xsl:value-of select="ResistanceParam/R0Step3"/></th>
                <th>+j</th>
                <th><xsl:value-of select="ResistanceParam/X0Step3"/></th>
                <xsl:element name="th">
                  <xsl:attribute name="id">_Change4<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change4<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
              <tr>
				        <th bgcolor="c1ced5">Z1=</th>
				        <th><xsl:value-of select="ResistanceParam/R1Step3"/></th>
				        <th>+j</th>
				        <th><xsl:value-of select="ResistanceParam/X1Step3"/></th>
				        <xsl:element name="th">
					        <xsl:attribute name="id">_Change5<xsl:value-of select="position()"/></xsl:attribute>            
					        <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change5<xsl:value-of select="position()"/>");</script>
				        </xsl:element>
			        </tr>
            </table>
	          <table>
		          <tr>������ �-N4</tr>
			          <tr>
				          <th bgcolor="c1ced5">Z0=</th>
				          <th><xsl:value-of select="ResistanceParam/R0Step4"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X0Step4"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change6<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change6<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
			          <tr>
				          <th bgcolor="c1ced5">Z1=</th>
				          <th><xsl:value-of select="ResistanceParam/R1Step4"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X1Step4"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change7<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change7<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
	          </table>
	          <table>
		          <tr>������ �-N5</tr>
			          <tr>
				          <th bgcolor="c1ced5">Z0=</th>
				          <th><xsl:value-of select="ResistanceParam/R0Step5"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X0Step5"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change8<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change8<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
			          <tr>
				          <th bgcolor="c1ced5">Z1=</th>
				          <th><xsl:value-of select="ResistanceParam/R1Step5"/></th>
				          <th>+j</th>
				          <th><xsl:value-of select="ResistanceParam/X1Step5"/></th>
				          <xsl:element name="th">
					          <xsl:attribute name="id">_Change9<xsl:value-of select="position()"/></xsl:attribute>            
					          <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change9<xsl:value-of select="position()"/>");</script>
				          </xsl:element>
			          </tr>
	          </table>
	          </th>
	          </table>
            <p></p>
	
	          <h4>���� ��������</h4>
    
	          <table border="1" cellspacing="0">
		          <tr>��� �������� �-�</tr>
		          <tr bgcolor="c1ced5" align="center">
			          <th>y1, ����</th>
			          <xsl:element name="th">
			          <xsl:attribute name="id">_ChangeLine<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>changeTextR1line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <xsl:element name="th">
			          <xsl:attribute name="id">_ChangeLine1<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>changeTextR2line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine1<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="AcCountLoad/CornerLine"/></td>
			          <td><xsl:value-of select="AcCountLoad/R1Line"/></td>
			          <td><xsl:value-of select="AcCountLoad/R2Line"/></td>
		          </tr>
	          </table>
          <table border="1" cellspacing="0">
            <xsl:if test="$vers &gt; 3.06">
              <p></p>
              <h4>��������� ���</h4>
              <tr bgcolor="c1ced5" align="center">
                <th>Umin, B</th>
                <th>Imax, I�</th>
                <th>����� 1-�� �� ��� ����</th>
              </tr>
              <tr align="center">
                <td>
                  <xsl:value-of select="AcCountLoad/Umin"/>
                </td>
                <td>
                  <xsl:value-of select="AcCountLoad/Imax"/>
                </td>
                <td>
                  <xsl:for-each select="AcCountLoad/Config">
                    <xsl:if test="current() ='false'">���</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>

            <xsl:if test="$vers &lt; 3.07">
              <tr>������</tr>
              <tr bgcolor="c1ced5" align="center">
                <th>y1, ����</th>
                <xsl:element name="th">
                  <xsl:attribute name="id">
                    _ChangeLine3<xsl:value-of select="position()"/>
                  </xsl:attribute>
                  <script>
                    changeTextR1line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine3<xsl:value-of select="position()"/>");
                  </script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">
                    _ChangeLine4<xsl:value-of select="position()"/>
                  </xsl:attribute>
                  <script>
                    changeTextR2line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine4<xsl:value-of select="position()"/>");
                  </script>
                </xsl:element>
              </tr>
              <tr align="center">
                <td>
                  <xsl:value-of select="AcCountLoad/CornerFaza"/>
                </td>
                <td>
                  <xsl:value-of select="AcCountLoad/R1Faza"/>
                </td>
                <td>
                  <xsl:value-of select="AcCountLoad/R2Faza"/>
                </td>
              </tr>
            </xsl:if>

          </table>
	
	          <h4>�������</h4>
	
	          <table border="1" cellspacing="0" cellpadding="4">
		          <tr bgcolor="c1ced5" align="center">
			          <th>���</th>
			          <xsl:element name="th">
			          <xsl:attribute name="id">_ChangeRS<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>changeTextRS(<xsl:value-of select="$isPrimary"/>,"_ChangeRS<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <xsl:element name="th">
			          <xsl:attribute name="id">_ChangeXS<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>changeTextXS(<xsl:value-of select="$isPrimary"/>,"_ChangeXS<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <xsl:element name="th">
			          <xsl:attribute name="id">_ChangedzS<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>changeTextdzS(<xsl:value-of select="$isPrimary"/>,"_ChangedzS<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <th>f/r</th>
			          <th>Tdz, ��</th>
			          <th>3I0�, I�</th>
			          <th>I�, I�</th>
			          <th>����� ���������� �� �������</th>
			          <th>T�, ��</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="Swing/Type"/></td>
			          <td><xsl:value-of select="Swing/R1"/></td>
			          <td><xsl:value-of select="Swing/X1"/></td>
			          <td><xsl:value-of select="Swing/Dr"/></td>
			          <td><xsl:value-of select="Swing/C1"/></td>
			          <td><xsl:value-of select="Swing/Tsrab"/></td>
			          <td><xsl:value-of select="Swing/I0"/></td>
			          <td><xsl:value-of select="Swing/Imin"/></td>
			          <xsl:element name="td">
			          <xsl:attribute name="id">_Reset<xsl:value-of select="position()"/></xsl:attribute>            
			          <script>translateBoolean(<xsl:value-of select="Swing/Reset"/>,"_Reset<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Swing/ResetTime"/></td>
		          </tr>
	          </table>
	          <p></p>
	
	          <h4>���� ������������ �������������� ��� �������� Z</h4>
	
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr>
			          <th bgcolor="c1ced5">y1, ����.</th>
			          <th><xsl:value-of select="ResistanceParam/C1"/></th>
		          </tr>
		          <tr>
			          <th bgcolor="c1ced5">y1, ����.</th>
			          <th><xsl:value-of select="ResistanceParam/C2"/></th>
		          </tr>
	          </table>
        
            <h4>�������</h4>
            <table border="1" cellpadding="4" cellspacing="0">
              <tr bgcolor="c1ced5" align="center">
			          <th>������� �</th>
                <th>�����</th>
			          <th>���</th>
			          <th>����������</th>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeRSS<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextRS(<xsl:value-of select="$isPrimary"/>,"_ChangeRSS<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangeXSS<xsl:value-of select="position()"/></xsl:attribute>
                <script>changeTextXS(<xsl:value-of select="$isPrimary"/>,"_ChangeXSS<xsl:value-of select="position()"/>");</script>
                </xsl:element>
                <xsl:element name="th">
                  <xsl:attribute name="id">_ChangefS<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeTextFS(<xsl:value-of select="$isPrimary"/>,"_ChangefS<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <th>tcp, ��</th>
			          <th>Icp, In</th>
			          <th>���� ���������</th>
			          <th>ty, ��</th>
			          <th>�����������</th>
			          <th>���� �� U</th>
			          <th>U����, �</th>
			          <th>������</th>
			          <th>����. ������. ��</th>
			          <th>����. �� ��������</th>
			          <th>����. �� �������</th>
			          <th>��������. ��� �����.</th>
			          <th>���� �� ���</th>
                <xsl:if test="$vers &lt; 3.08">
                  <th>����� 1��� ��� ����*</th>
                </xsl:if>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/��_�������������/���_�������������_������">
		          <tr align="center">
			          <td><xsl:value-of select="position()"/></td>
			          <td><xsl:value-of select="Mode"/></td>
			          <td><xsl:value-of select="Type"/></td>
			          <td><xsl:value-of select="BlocInp"/></td>
			          <td><xsl:value-of select="UstR"/></td>
			          <td><xsl:value-of select="UstX"/></td>
			          <td><xsl:value-of select="Ustr"/></td>
			          <td><xsl:value-of select="Tsr"/></td>
			          <td><xsl:value-of select="Isr"/></td>
			          <td><xsl:value-of select="Acceleration"/></td>
			          <td><xsl:value-of select="Tu"/></td>
			          <td><xsl:value-of select="Direction"/></td>
                <td><xsl:for-each select="StartOnU"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="Ustart"/></td>
			          <td><xsl:value-of select="Conture"/></td>
			          <td><xsl:value-of select="BlockFromTn"/></td>
                <td><xsl:for-each select="BlockFromLoad"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="BlockFromSwing"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="SteeredModeAcceler"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="DamageFaza"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
		            <xsl:if test="$vers &lt; 3.08">
		              <td>
		                <xsl:for-each select="ResetStep">
		                  <xsl:if test="current() ='false'">���	</xsl:if>
		                  <xsl:if test="current() ='true'">��</xsl:if>
		                </xsl:for-each>
		              </td>
		            </xsl:if>
			          <td><xsl:value-of select="Oscilloscope"/></td>
                <td><xsl:for-each select="Urov"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="Apv"/></td>
                <td><xsl:value-of select="Avr"/></td>
		          </tr>
		          </xsl:for-each>
	          </table>
	
	          <h4>���� �����</h4>
	          <table border="1" cellspacing="0" cellpadding="4">
		          <tr align ="center">
			          <th bgcolor="c1ced5">I</th>
			          <th bgcolor="c1ced5">3I0</th>
                <th bgcolor="c1ced5">In</th>
                <th bgcolor="c1ced5">I2</th>
                <xsl:if test="$plant = 'T5N4D42R35'">
                  <th bgcolor="c1ced5">In1</th>
                </xsl:if>
              </tr>
              <tr align ="center">
                <td><xsl:value-of select="DefensesSetpoints/����/I"/></td>
                <td><xsl:value-of select="DefensesSetpoints/����/I0"/></td>
                <td><xsl:value-of select="DefensesSetpoints/����/In"/></td>
			          <td><xsl:value-of select="DefensesSetpoints/����/I2"/></td>
                <xsl:if test="$plant = 'T5N4D42R35'">
                  <td><xsl:value-of select="DefensesSetpoints/�������/In1"/></td>
                </xsl:if>
		          </tr>
	          </table>
	          <p></p>
	
	          <h4>���. I</h4>
	          <table border="1" cellspacing="0" cellpadding="4">
	          <tr>������ I>1-4 ������������� ����</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
			          <th>I��, I� ��</th>
			          <th>U����[�]</th>
			          <th>���� �� U</th>
			          <th>���. �� ������. ��</th>
			          <th>�����������</th>
			          <th>������. ����.</th>
			          <th>������</th>
			          <th>������-��</th>
			          <th>t��., ��/����</th>
			          <th>k �����. ���-��</th>
			          <th>���� ���������</th>
			          <th>Ty[��]</th>
			          <th>����������</th>
			          <th>I2�/I1�[%]</th>
			          <th>����. �� I2�/I1�</th>
			          <th>������. ����.</th>
			          <th>������. ��� �����.</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
			          <tr align="center">
			          <xsl:if test="position() &lt; 5">
				          <td>������� I><xsl:value-of select="position()"/></td>
				          <td><xsl:value-of select="�����"/></td>
				          <td><xsl:value-of select="�������"/></td>
				          <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="����_��_������_��"/></td>
				          <td><xsl:value-of select="�����������"/></td>
				          <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
				          <td><xsl:value-of select="������"/></td>
				          <td><xsl:value-of select="��������������"/></td>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="K"/></td>
				          <td><xsl:value-of select="Uskor"/></td>
				          <td><xsl:value-of select="ty"/></td>
				          <td><xsl:value-of select="����������"/></td>
				          <td><xsl:value-of select="�������_2�_1�"/></td>
                  <td><xsl:for-each select="����_2�_1�"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������_����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
			          </xsl:if>
			          </tr>
		          </xsl:for-each>
	          </table>
            <table border="1" cellspacing="0" cellpadding="4">
              
	          <tr>������ I>5-6 ������������� ����</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
			          <th>I��, I� ��</th>
                <th>�������</th>
			          <th>U����[�]</th>
			          <th>���� �� U</th>
			          <th>���. �� ������. ��</th>
			          <th>�����������</th>
			          <th>������. ����.</th>
			          <th>������</th>
			          <th>������-��</th>
			          <th>t��., ��/����</th>
			          <th>k �����. ���-��</th>
			          <th>���� ���������</th>
			          <th>Ty[��]</th>
			          <th>����������</th>
			          <th>I2�/I1�[%]</th>
			          <th>����. �� I2�/I1�</th>
			          <th>������. ����.</th>
			          <th>������. ��� �����.</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
			          <tr align="center">
			          <xsl:if test="position() &gt; 4 and position() &lt; 7">
				          <td>������� I><xsl:value-of select="position()"/></td>
				          <td><xsl:value-of select="�����"/></td>
                  <td><xsl:value-of select="�������"/></td>
                  <td><xsl:value-of select="�������"/></td>
				          <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="����_��_������_��"/></td>
				          <td><xsl:value-of select="�����������"/></td>
				          <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
				          <td><xsl:value-of select="������"/></td>
				          <td><xsl:value-of select="��������������"/></td>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="K"/></td>
				          <td><xsl:value-of select="Uskor"/></td>
				          <td><xsl:value-of select="ty"/></td>
				          <td><xsl:value-of select="����������"/></td>
				          <td><xsl:value-of select="�������_2�_1�"/></td>
                  <td><xsl:for-each select="����_2�_1�"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������_����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
                </xsl:if>
			          </tr>
              </xsl:for-each>
	          </table>
	          <table border="1" cellpadding="4" cellspacing="0">
	          <tr>������ I&#60; ������������ ����</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
			          <th>I��, I� ��</th>
			          <th>������</th>
			          <th>t[��]</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
		          <tr align="center">
			          <xsl:if test="position()=7">
			          <td>������� I&#60;</td>
			          <td><xsl:value-of select="�����"/></td>
			          <td><xsl:value-of select="�������"/></td>
			          <td><xsl:value-of select="������"/></td>
			          <td><xsl:value-of select="t��"/></td>
			          <td><xsl:value-of select="����������"/></td>
			          <td><xsl:value-of select="���"/></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:value-of select="���"/></td>
			          </xsl:if>
		          </tr>
		          </xsl:for-each>
	          </table>
	          <p></p>
	
	          <h4>������ I*</h4>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5" align="center">
			          <th>�������</th>
			          <th>���������</th>
			          <th>I, I� ��</th>
			          <th>U���� [�]</th>
			          <th>���� �� U</th>
			          <th>�����������</th>
			          <th>������. ������.</th>
			          <th>I*</th>
			          <th>��������������</th>
			          <th>t��., ��/����</th>
			          <th>k �����. ���-��</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>���� ���������</th>
			          <th>Ty [��]</th>
			          <th>������. ��� �����.</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/I_x002A_/���_I_x002A_/DefenseStarStruct">
			          <tr align="center">
				          <td>������� I*><xsl:value-of select="position()"/></td>
				          <td><xsl:value-of select="�����"/></td>
				          <td><xsl:value-of select="�������"/></td>
				          <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="�����������"/></td>
				          <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
				          <td><xsl:value-of select="I_x002A_"/></td>
				          <td><xsl:value-of select="��������������"/></td>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="K"/></td>
				          <td><xsl:value-of select="����������"/></td>
				          <td><xsl:value-of select="���"/></td>
				          <td><xsl:value-of select="Uskor"/></td>
				          <td><xsl:value-of select="ty"/></td>
                  <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
			          </tr>
		          </xsl:for-each>
            </table>
	          <p></p>
	
	          <h4>I2I1</h4>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr align="center">
			          <th bgcolor="c1ced5">�����</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/�����"/></td>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">����������</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/����������"/></td>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">I2/I1, %</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/�������_I2_I1"/></td>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">tcp, ��</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/t��"/></td>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">���.</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">����</th>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_����_I2I0<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="DefensesSetpoints/I2I1/����"/>,"_����_I2I0<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
		          </tr>
		          <tr align="center">
			          <th bgcolor="c1ced5">���</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
		          </tr>
              <tr align="center">
			          <th bgcolor="c1ced5">���</th>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
		          </tr>
	          </table>
	          <p></p>

            <xsl:if test="$vers &gt; 3.00">
              <h4>������� ������</h4>
	            <table border="1" cellpadding="4" cellspacing="0">
		            <tr align="center">
			            <th bgcolor="c1ced5">�����</th>
			            <td><xsl:value-of select="DefensesSetpoints/�������/�����"/></td>
		            </tr>
                <tr align="center">
			            <th bgcolor="c1ced5">Icp, I�</th>
			            <td><xsl:value-of select="DefensesSetpoints/�������/�������"/></td>
		            </tr>
		            <tr align="center">
			            <th bgcolor="c1ced5">����������</th>
			            <td><xsl:value-of select="DefensesSetpoints/�������/����������"/></td>
		            </tr>
		            <tr align="center">
			            <th bgcolor="c1ced5">�����������</th>
			            <xsl:element name="td">
				            <xsl:attribute name="id">Osc_Dug_<xsl:value-of select="position()"/></xsl:attribute>            
				            <script>translateBoolean(<xsl:value-of select="DefensesSetpoints/�������/�����������"/>,"Osc_Dug_<xsl:value-of select="position()"/>");</script>
			            </xsl:element>
		            </tr>
	            </table>
              <p></p>
            </xsl:if>
            
	          <h4>���. U</h4>
	
	          <table border="1" cellpadding="4" cellspacing="0">
	          <tr>������ U></tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
			          <th>���</th>
			          <th>U��[�]</th>
			          <th>t��[��]</th>
			          <th>t��[��]</th>
			          <th>U���[�]</th>
			          <th>�������</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
			          <th>��� �����.</th>
			          <th>�����</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/U/���_������_U/DefenceUStruct">
			          <xsl:if test="position()&lt;5">
			          <tr align="center">
				          <td>������� U><xsl:value-of select="position()"/></td>
				          <td><xsl:value-of select="�����"/></td>
				          <td><xsl:value-of select="���_Umax"/></td>
				          <td><xsl:value-of select="�������"/></td>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="U��"/></td>
                  <td><xsl:for-each select="U��_����_x002F_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="����������"/></td>
				          <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td> 
			          </tr>
			          </xsl:if>
		          </xsl:for-each>
	          </table>
	
	          <table border="1" cellpadding="4" cellspacing="0">
	          <tr>������ U&#60;</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
			          <th>���</th>
			          <th>U��[�]</th>
			          <th>t��[��]</th>
			          <th>t��[��]</th>
			          <th>U���[�]</th>
			          <th>�������</th>
			          <th>����-�� U&#60;5�</th>
			          <th>����. �� ������. ��</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
			          <th>��� �����.</th>
			          <th>�����</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/U/���_������_U/DefenceUStruct">
                <xsl:if test="position() &gt; 4">
                  <tr align="center">
                    <td>������� U&#60;<xsl:value-of select="position()-4"/></td>
                    <td><xsl:value-of select="�����"/></td>
				            <td><xsl:value-of select="���_Umin"/></td>
				            <td><xsl:value-of select="�������"/></td>
				            <td><xsl:value-of select="t��"/></td>
				            <td><xsl:value-of select="t��"/></td>
				            <td><xsl:value-of select="U��"/></td>
                    <td><xsl:for-each select="U��_����_x002F_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				            <td><xsl:for-each select="����������_U_5V"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				            <td><xsl:value-of select="����_��_������_��"/></td>
				            <td><xsl:value-of select="����������"/></td>
				            <td><xsl:value-of select="���"/></td>
                    <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				            <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="���"/></td>
                    <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                    <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			            </tr>
                </xsl:if>
              </xsl:for-each>
            </table>
            <p></p>
	
	          <h4>���. F</h4>
            <table border="1" cellpadding="4" cellspacing="0">
	          <tr>������ F&#62;</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>�����</th>
                <th>���</th>
                <th>F��[��] | dF/dt[��/�]</th>
                <th>U1[�]</th>
			          <th>tcp[��]</th>
			          <th>t��[��]</th>
			          <th>F��[��]</th>
			          <th>�������</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
			          <th>��� �����.</th>
			          <th>�����</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/Fb/���_������_F/DefenseFStruct">
                <tr align="center">
                  <td>������� F&#62;<xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="�����"/></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="�������"/></td>
                  <td><xsl:value-of select="U1"/></td>
                  <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="ty"/></td>
				          <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="����������"/></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                </tr>
              </xsl:for-each>
	          </table>
    
	          <table border="1" cellpadding="4" cellspacing="0">
	          <tr>������ F&#60;</tr>
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>���������</th>
                <th>���</th>
                <th>F��[��] | dF/dt[��/�]</th>
                <th>U1[�]</th>
			          <th>tcp[��]</th>
			          <th>t��[��]</th>
			          <th>F��[��]</th>
			          <th>�������</th>
			          <th>����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
			          <th>��� �����.</th>
			          <th>�����</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/Fm/���_������_F/DefenseFStruct">
			          <tr align="center">
                  <td>������� F&#60;<xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="�����"/></td>
                  <xsl:if test="$vers &lt; 1.05">
                    <td><xsl:value-of select="�������"/></td>
                  </xsl:if>
                  <xsl:if test="$vers &gt; 1.04">
                    <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="�������"/></td>
                    <td><xsl:value-of select="U1"/></td>
                  </xsl:if>
				          <td><xsl:value-of select="t��"/></td>
				          <td><xsl:value-of select="ty"/></td>
				          <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
				          <td><xsl:value-of select="����������"/></td>
				          <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			          </tr>
		          </xsl:for-each>
	          </table>

          <p></p>

          <h4>������ P</h4>
          <table border="1" cellpadding="4" cellspacing="0">
            <tr bgcolor="c1ced5">
              <th>�������</th>
              <th>�����</th>
              <th>S��[S�]</th>
              <th>���[']</th>
              <th>t��[��]</th>
              <th>t��[��]</th>
              <th>S��[S�]</th>
              <th>S��[����, ���]</th>
              <th>I��[�]</th>
              <th>����������</th>
              <th>���</th>
              <th>���.</th>
              <th>��� �����.</th>
              <th>����</th>
              <th>���</th>
              <th>����� �������</th>
            </tr>
            <xsl:for-each select="DefensesSetpoints/P/���_P/ReversePowerStruct">
              <tr align="center">
                <td>
                  ������� P <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="�������_������������"/>
                </td>
                <td>
                  <xsl:value-of select="����_������������"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="�������_��������"/>
                </td>
                <td>
                  <xsl:for-each select="�������_��������_����_���">
                    <xsl:if test="current() ='false'">���	</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
                <td>
                  <xsl:value-of select="���_������������"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:value-of select="���_�������"/>
                </td>
                <td>
                  <xsl:value-of select="����"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td>
                  <xsl:for-each select="�����_�������">
                    <xsl:if test="current() ='false'">���	</xsl:if>
                    <xsl:if test="current() ='true'">��</xsl:if>
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>

          <p></p>
	
	          <h4>������ ���������</h4>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr>���������� �� ��������� ��������� Q</tr>
			          <tr bgcolor="c1ced5">
				          <th>�����</th>
				          <th>������� Q���, %</th>
				          <th>����� t���., �</th>
			          </tr>
				          <tr align="center">
					          <td><xsl:value-of select="DefensesSetpoints/��������/�����"/></td>
					          <td><xsl:value-of select="DefensesSetpoints/��������/�������_������������"/></td>
					          <td><xsl:value-of select="DefensesSetpoints/��������/�����_������������"/></td>
				          </tr>
	          </table>
	          <p></p>
            <table border="1" cellpadding="4" cellspacing="0">
		          <tr>���������� �� ����� ������</tr>
			          <tr bgcolor="c1ced5">
				          <th>����� ����� ������ N����</th>
				          <th>����� ������� ������ N���.</th>
				          <th>����� t���., �</th>
			          </tr>
				          <tr align="center">
					          <td><xsl:value-of select="DefensesSetpoints/��������/�����_��������_������"/></td>
					          <td><xsl:value-of select="DefensesSetpoints/��������/�����_�������_������"/></td>
					          <td><xsl:value-of select="DefensesSetpoints/��������/�����_����������"/></td>
				          </tr>
	          </table>
	          <p></p>
            
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th></th>
			          <th>�����</th>
			          <th>������� Q, %</th>
			          <th>����������</th>
			          <th>���.</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
		          </tr>
              <xsl:for-each select="DefensesSetpoints/Q/���_������_Q/DefenseQStruct">
                <xsl:if test="position()=1">
                  <tr align="center">
			              <td>Q&#62;</td>
			              <td><xsl:value-of select="�����"/></td>
			              <td><xsl:value-of select="Ustavka"/></td>
			              <td><xsl:value-of select="����������"/></td>
			              <td><xsl:value-of select="���"/></td>
                    <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                    <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="���"/></td>
                  </tr>
                </xsl:if>
		            <xsl:if test="position()=2">
		              <tr align="center">
			              <td>Q&#62;&#62;</td>
			              <td><xsl:value-of select="�����"/></td>
			              <td><xsl:value-of select="Ustavka"/></td>
			              <td><xsl:value-of select="����������"/></td>
			              <td><xsl:value-of select="���"/></td>
                    <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			              <td><xsl:value-of select="���"/></td>
                    <td><xsl:value-of select="���"/></td>
		              </tr>
		            </xsl:if>
		          </xsl:for-each>
	          </table>
	          <p></p>
	
	          <h4>�������</h4>
	
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th>�������</th>
			          <th>���������</th>
			          <th>������ ������������</th>
			          <th>t��[��]</th>
			          <th>t��[��]</th>
			          <th>������ �������</th>
			          <th>�������</th>
			          <th>������ ����������</th>
			          <th>�����������</th>
			          <th>����</th>
			          <th>���</th>
                <th>���</th>
			          <th>��� �����.</th>
			          <th>�����</th>
		          </tr>
		          <xsl:for-each select="DefensesSetpoints/�������/���_�������/DefenseExternalStruct">
		          <xsl:if test="position() &lt; 17">
		          <tr align="center">
			          <td>������� <xsl:value-of select="position()"/></td>
			          <td><xsl:value-of select="�����"/></td>
			          <td><xsl:value-of select="����_���_�������_�����"/></td>
			          <td><xsl:value-of select="t��"/></td>
			          <td><xsl:value-of select="ty"/></td>
			          <td><xsl:value-of select="�������_���_�������_�����"/></td>
                <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
			          <td><xsl:value-of select="����������"/></td>
			          <td><xsl:value-of select="���"/></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
		          </tr>
		          </xsl:if>
		          </xsl:for-each>
	          </table>
	          <p></p>
	
	          <h4>���������� �������</h4>
	          <table border="1" cellspacing="0">
	          <td>
	          <b>���������� ������� �</b>
	          <table border="1" cellspacing="0">

                <tr bgcolor="c1ced5">
                   <th>����� ��</th>
                   <th>������������</th>
	            </tr>
 
             <xsl:for-each select="InputLogicSignal/��">  
               <xsl:if test="position() &lt; 9 ">
   	            <tr>	
                   <td><xsl:value-of  select="position()"/></td>
	
			           <td>
			           <xsl:for-each select="�������">
				          <xsl:if test="current() !='���'">
					          <xsl:if test="current() ='��'">
					          �<xsl:value-of select="position()"/>
					          </xsl:if>
					          <xsl:if test="current() ='������'">
					          ^�<xsl:value-of select="position()"/>
					          </xsl:if>
				          </xsl:if>
			           </xsl:for-each>
			           </td>
	            </tr>
	          </xsl:if>	
              </xsl:for-each>
              </table>
	
		          <b>���������� ������� ���</b>
	          <table border="1" cellspacing="0">

                <tr bgcolor="c1ced5">
                   <th>����� ��</th>
                   <th>������������</th>
	            </tr>
 
             <xsl:for-each select="InputLogicSignal/��">  
               <xsl:if test="position() &gt; 8 ">
		          <xsl:if test="position() &lt; 17">
   	            <tr>	
                   <td><xsl:value-of  select="position()"/></td>
	
			           <td>
			           <xsl:for-each select="�������">
				          <xsl:if test="current() !='���'">
					          <xsl:if test="current() ='��'">
					          �<xsl:value-of select="position()"/>
					          </xsl:if>
					          <xsl:if test="current() ='������'">
					          ^�<xsl:value-of select="position()"/>
					          </xsl:if>
				          </xsl:if>
			           </xsl:for-each>
			           </td>
	            </tr>
	            </xsl:if>
	          </xsl:if>	
              </xsl:for-each>
              </table>
	          </td>
	          </table>
	
	           <b>���</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="c1ced5">
                   <th>�����</th>
                   <th>������������</th>
                </tr>
     
		           <xsl:for-each select="OutputLogicSignal/���">
			          <tr>
				          <td><xsl:value-of select="position()"/></td>
				          <td>
					          <xsl:for-each select="�������">
						          <xsl:value-of select="current()"/>|
					          </xsl:for-each>
				          </td>
			          </tr>
		           </xsl:for-each>
	           </table>  
	          <p></p>
	
	          <h4>���</h4>
	
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th>�����</th>
			          <th>���������� �� ����</th>
			          <th>������ ���</th>
			          <th>t �������, ��</th>
			          <th>��� �������</th>
			          <th>����������</th>
			          <th>t ����., ��</th>
			          <th>t �����., ��</th>
			          <th>1 ����, ��</th>
			          <th>2 ����, ��</th>
			          <th>3 ����, ��</th>
			          <th>4 ����, ��</th>
			          <th>����������.</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="Apv/�����"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_����������_��_����_���<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Apv/����������_��_����"/>,"_����������_��_����_���<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Apv/����_�������_���"/></td>
			          <td><xsl:value-of select="Apv/�����_�������_���"/></td>
			          <td><xsl:value-of select="Apv/���_�������"/></td>
			          <td><xsl:value-of select="Apv/����_����������_���"/></td>
			          <td><xsl:value-of select="Apv/�����_����������_���"/></td>
			          <td><xsl:value-of select="Apv/�����_����������_���"/></td>
			          <td><xsl:value-of select="Apv/����1"/></td>
			          <td><xsl:value-of select="Apv/����2"/></td>
			          <td><xsl:value-of select="Apv/����3"/></td>
			          <td><xsl:value-of select="Apv/����4"/></td>
			          <td><xsl:value-of select="Apv/������_���_��_�����������������_����������_�����������"/></td>
		          </tr>
	          </table>
	          <p></p>
            
            <h4>���</h4>
            <table border="1" cellpadding="4" cellspacing="0">
              <tr align="center">
                <th bgcolor="c1ced5">�� ������� (�� �������)</th>
                <td><xsl:value-of select="Avr/��_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ����������</th>
                <td><xsl:value-of select="Avr/��_����������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ��������������</th>
                <td><xsl:value-of select="Avr/��_��������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ������</th>
                <td><xsl:value-of select="Avr/��_������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">������ �����</th>
                <td><xsl:value-of select="Avr/����"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">����������</th>
                <td><xsl:value-of select="Avr/����_����������_���"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�����</th>
                <td><xsl:value-of select="Avr/����_�����_����������_���"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">��� ���������</th>
                <td><xsl:value-of select="Avr/����_���_������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t��, ��</th>
                <td><xsl:value-of select="Avr/�����_���_������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�������</th>
                <td><xsl:value-of select="Avr/����_���_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t���, ��</th>
                <td><xsl:value-of select="Avr/�����_���_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t����, ��</th>
                <td><xsl:value-of select="Avr/��������_����������_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�����</th>
                <td><xsl:value-of select="Avr/�����"/></td>
              </tr>
            </table>
            <p></p>
            
	
	          <h4>�������� ����������� � ������� �������� ��� ���������� (�� � ����)</h4>
	
	          <table border="1" cellpadding="4" cellspacing="0">
	          <tr>����� �������</tr>
		          <tr bgcolor="c1ced5">
			          <th>U1, �</th>
			          <th>U2, �</th>
			          <th>Umin. ���*, �</th>
			          <th>Umin. ���*, �</th>
			          <th>Umax. ���*, �</th>
			          <th>t��, ��</th>
			          <th>t�����, ��</th>
			          <th>t ���, ��</th>
			          <th>����, %</th>
			          <th>f(U1;U2), ����</th>
			          <th>���� ���������� ��</th>
			          <th>���� ����� U1 ���, U2 ����</th>
			          <th>���� ����� U1 ����, U2 ���</th>
			          <th>���� ����� U1 ���, U2 ���</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="Sinhronizm/U1"/></td>
			          <td><xsl:value-of select="Sinhronizm/U2"/></td>
			          <td><xsl:value-of select="Sinhronizm/Umin"/></td>
			          <td><xsl:value-of select="Sinhronizm/Umin_���"/></td>
			          <td><xsl:value-of select="Sinhronizm/Umax_���"/></td>
			          <td><xsl:value-of select="Sinhronizm/t��"/></td>
			          <td><xsl:value-of select="Sinhronizm/t�����"/></td>
			          <td><xsl:value-of select="Sinhronizm/t���"/></td>
			          <td><xsl:value-of select="Sinhronizm/����"/></td>
			          <td><xsl:value-of select="Sinhronizm/f"/></td>
			          <td><xsl:value-of select="Sinhronizm/����������"/></td>
			          <td><xsl:value-of select="Sinhronizm/�������_U1_���_U2_����"/></td>
			          <td><xsl:value-of select="Sinhronizm/�������_U1_����_U2_���"/></td>
			          <td><xsl:value-of select="DefensesSetpoints/I2I1/�������_U1_����_U2_���"/></td>
		          </tr>
	          </table>
	          <b>* - ������ ������� ������������ ��� ��������� � ������������ U1 � U2*����/100</b>
	          <p></p>
	
	          <h4>������� ������� ���������</h4>
	
	          <table border="1" cellspacing="0">
	          <td>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th>�����</th>
			          <th>���������� �� ������������� ��</th>
			          <th>dUmax., �</th>
			          <th>�������� ����������� (�����. �����)</th>
			          <th>dF, ��</th>
			          <th>dFi, ����</th>
			          <th>����������� ����������� (�������. �����)</th>
			          <th>dF, ��</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/�����"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_����.��_������.��_��<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_�������_���������/����.��_������.��"/>,"_����.��_������.��_��<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/dUmax"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_��������_�����������_��<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_�������_���������/��������_�����������"/>,"_��������_�����������_��<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/����������_���������"/></td>
			          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/dFi"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_�����������_�����������_��<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_�������_���������/�����������_�����������"/>,"_�����������_�����������_��<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/������������_���������"/></td>
		          </tr>
	          </table>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr>���������� ���������</tr>
			          <tr bgcolor="c1ced5">
				          <th>U1 ���, U2 ����</th>
				          <th>U1 ����, U2 ���</th>
				          <th>U1 ���, U2 ���</th>
			          </tr>
			          <tr align="center">
				          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2����"/></td>
				          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1����U2���"/></td>
				          <td><xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2���"/></td>
			          </tr>
	          </table>
	          </td>
	          </table>
	          <h4>������� ��������������� ���������</h4>
	
	          <table border="1" cellspacing="0">
	          <td>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr bgcolor="c1ced5">
			          <th>�����</th>
			          <th>���������� �� ������������� ��</th>
			          <th>dUmax., �</th>
			          <th>�������� ����������� (�����. �����)</th>
			          <th>dF, ��</th>
			          <th>dFi, ����</th>
			          <th>����������� ����������� (�������. �����)</th>
			          <th>dF, ��</th>
		          </tr>
		          <tr align="center">
			          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/�����"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_����.��_������.��_��_���<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_���������������_���������/����.��_������.��"/>,"_����.��_������.��_��_���<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/dUmax"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_��������_�����������_��_���<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_���������������_���������/��������_�����������"/>,"_��������_�����������_��_���<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/����������_���������"/></td>
			          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/dFi"/></td>
			          <xsl:element name="td">
				          <xsl:attribute name="id">_�����������_�����������_��_���<xsl:value-of select="position()"/></xsl:attribute>            
				          <script>translateBoolean(<xsl:value-of select="Sinhronizm/������_���_���������������_���������/�����������_�����������"/>,"_�����������_�����������_��_���<xsl:value-of select="position()"/>");</script>
			          </xsl:element>
			          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/������������_���������"/></td>
		          </tr>
	          </table>
	          <table border="1" cellpadding="4" cellspacing="0">
		          <tr>���������� ���������</tr>
			          <tr bgcolor="c1ced5">
				          <th>U1 ���, U2 ����</th>
				          <th>U1 ����, U2 ���</th>
				          <th>U1 ���, U2 ���</th>
			          </tr>
			          <tr align="center">
				          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2����"/></td>
				          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1����U2���"/></td>
				          <td><xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2���"/></td>
			          </tr>
	          </table>
	          </td>
	          </table>
        </xsl:for-each>
      </div>
      
      <p></p>
    </div>
    <h2>������� �������</h2>
    <table border="1" cellpadding="4" cellspacing="0">
      <tr>������ �������</tr>	
      <tr>
        <th bgcolor="FF8C00">������ 1</th>
			  <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_1"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF8C00">������ 2</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_2"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF8C00">������ 3</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_3"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF8C00">������ 4</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_4"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF8C00">������ 5</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_5"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF8C00">������ 6</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_���������_������_�������_x002B_6"/></td>
		    </tr>
	    </table>
	
	    <p></p>

	    <table border="1" cellpadding="4" cellspacing="0">
		    <tr>
			    <th bgcolor="FF8C00">����� ���������</th>
			    <td><xsl:value-of select="��762/������������_�������_��������/����_�����_���������"/></td>
		    </tr>
	    </table>
	    <p></p>
	
	    <h2>�������� �������</h2>
	
	    <table border="1" cellpadding="4" cellspacing="0">
	    <tr>�������� ����</tr>
		    <tr bgcolor="90EE90">
			    <th>�</th>
			    <th>���</th>
			    <th>������</th>
			    <th>T�����., ��</th>
		    </tr>
		    <xsl:for-each select="��762/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
		    <tr align="center">
			    <td><xsl:value-of select="position() + 2"/></td>
			    <td><xsl:value-of select="@���"/></td>
			    <td><xsl:value-of select="@������"/></td>
			    <td><xsl:value-of select="@�����"/></td>
		    </tr>
		    </xsl:for-each>
	    </table>
	    <p></p>
	
	    <table border="1" cellpadding="4" cellspacing="0">
	    <tr>����������</tr>
		    <tr bgcolor="90EE90">
			    <th>�</th>
			    <th>���</th>
			    <th>������ "�������"</th>
			    <th>������ "�������"</th>
			    <th>����� ������</th>
		    </tr>
		    <xsl:for-each select="��762/������������_����_x002C_�����������_x002C_��������������/����������/���_����������/����_���������">
		    <tr align="center">
			    <td><xsl:value-of select="position()"/></td>
			    <td><xsl:value-of select="@���"/></td>
			    <td><xsl:value-of select="@������_�������"/></td>
			    <td><xsl:value-of select="@������_�������"/></td>
			    <td><xsl:value-of select="@�����_������"/></td>
		    </tr>
		    </xsl:for-each>
	    </table>
	    <p></p>
	
	    <table border="1" cellspacing="0" cellpadding="4">
		    <tr>����� �����������</tr>
			    <xsl:for-each select="��762/����_��������_������">
			    <tr align="left">
				    <th bgcolor="90EE90">1. �� ����� � ������ �������</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">ResetBySys_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�����_��_�����_�_������_�������"/>,"ResetBySys_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">2. �� ����� � ������ ������</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">ResetByAlrm_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�����_��_�����_�_������_������"/>,"ResetByAlrm_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    </xsl:for-each>
	    </table>
	    <p></p>
	
      <table border="1" cellpadding="4" cellspacing="0">
        <tr>������ �������</tr>
        <th bgcolor="90EE90">����. ������ ������� F</th>
        <td>
          <xsl:for-each select="��762/����_��������_������/@������_�������">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </table>
      <p></p>

	    <table border="1" cellpadding="4" cellspacing="0">
		    <tr>���� �������������</tr>
			    <xsl:for-each select="��762/������������_����_x002C_�����������_x002C_��������������/����_�������������">
			    <tr align="left">
				    <th bgcolor="90EE90">1. ���������� �������������</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">ReleFault_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�������������_1"/>,"ReleFault_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">2. ����������� �������������</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">ProgFault_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�������������_2"/>,"ProgFault_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">3. ������������� ��������� U</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">FaultU_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�������������_3"/>,"FaultU_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">4. ������������� ��������� F</th>
				    <xsl:element name="td">
				    <xsl:attribute name="id">FaultF_<xsl:value-of select="position()"/></xsl:attribute>            
				    <script>translateBoolean(<xsl:value-of select="@�������������_4"/>,"FaultF_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">5. ������������� �����������</th>
				    <xsl:element name="td">
				      <xsl:attribute name="id">FaultSwitch_<xsl:value-of select="position()"/></xsl:attribute>            
				      <script>translateBoolean(<xsl:value-of select="@�������������_5"/>,"FaultSwitch_<xsl:value-of select="position()"/>");</script>
				    </xsl:element>
			    </tr>
          <tr align="left">
            <th bgcolor="90EE90">6. ������������� ������</th>
            <xsl:element name="td">
              <xsl:attribute name="id">FaultLogic_<xsl:value-of select="position()"/></xsl:attribute>
              <script>translateBoolean(<xsl:value-of select="@�������������_6"/>,"FaultLogic_<xsl:value-of select="position()"/>");</script>
            </xsl:element>
          </tr>
			    <tr align="left">
				    <th bgcolor="90EE90">T�����., ��</th>
				    <td><xsl:value-of select="@�������_����_�������������"/></td>
			    </tr>
			    </xsl:for-each>
	    </table>
	    <p></p>
	
	    <h2>�����������</h2>
	    <table border="1" cellpadding="4" cellspacing="0">
		    <tr>
			    <th bgcolor="FF69B4">���������� ������������</th>
			    <td><xsl:value-of select="��762/������������_�����������/������������_���/����������_�����������"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF69B4">������, ��</th>
			    <td><xsl:value-of select="��762/������_������������"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF69B4">����. ����������, %</th>
			    <td><xsl:value-of select="��762/������������_�����������/������������_���/����������"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF69B4">������. ��</th>
			    <td><xsl:value-of select="��762/������������_�����������/������������_���/��������"/></td>
		    </tr>
		    <tr>
			    <th bgcolor="FF69B4">���� ����� ���.</th>
			    <td><xsl:value-of select="��762/������������_�����������/����_�������_������������/@�����"/></td>
		    </tr>
	    </table>
	    <p></p>

      <table border="1" cellpadding="4" cellspacing="0">
        <tr>��������������� ���������� ������</tr>
        <tr bgcolor="FF69B4">
          <th>�����</th>
          <th>����</th>
          <th>�����</th>
        </tr>
        <xsl:for-each select="��762/������������_�����������/������������_�������/���_������/ChannelWithBase">
          <tr align="center">
            <td><xsl:value-of select="position()"/></td>
            <td><xsl:value-of select="@����"/></td>
            <td><xsl:value-of select="@�����"/></td>
          </tr>
        </xsl:for-each>
      </table>
  
	    <p></p>
	
	    <h2>�����������</h2>
	    <table border="1" cellpadding="4" cellspacing="0">
		    <tr>�����������</tr>
			    <tr bgcolor="FFC0CB">
				    <th>��������� "���������"</th>
				    <th>��������� "��������"</th>
				    <th>���� �������������</th>
				    <th>���� ����������</th>
            <th>������� ������� ���., ��</th>
				    <th>������� ����������</th>
				    <th>t�����, ��</th>
				    <th>�������� ����� ����������</th>
				    <th>�������� ������� ��������� ����������</th>
			    </tr>
			    <tr align="center">
				    <td><xsl:value-of select="��762/������������_�����������/���������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/��������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����������"/></td>
            <td><xsl:value-of select="��762/������������_�����������/�������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/�������_����������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/���������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/��������_�����_���������_����������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/��������_�����_�������_���������_����������"/></td>
			    </tr>
	    </table>

    <p></p>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr>����</tr>
        <tr bgcolor="FFC0CB">
          <th>�� ����</th>
          <th>�� ��</th>
          <th>�� ����</th>
          <th>t����1, ��</th>
          <th>t����2, ��</th>
          <th>I����, I�</th>
          <th>���� �����</th>
          <th>���� ����������</th>
        </tr>
        <tr align="center">
          <xsl:element name="td">
            <xsl:attribute name="id">����_��_����</xsl:attribute>
            <script>
              translateBoolean(<xsl:value-of select="��762/����/��_����"/>,"����_��_����");
            </script>
          </xsl:element>
          <xsl:element name="td">
            <xsl:attribute name="id">����_��_��</xsl:attribute>
            <script>
              translateBoolean(<xsl:value-of select="��762/����/��_��"/>,"����_��_��");
            </script>
          </xsl:element>
          <xsl:element name="td">
            <xsl:attribute name="id">����_��_����</xsl:attribute>
            <script>
              translateBoolean(<xsl:value-of select="��762/����/��_����"/>,"����_��_����");
            </script>
          </xsl:element>
          <td><xsl:value-of select="��762/����/t����1"/></td>
          <td><xsl:value-of select="��762/����/t����2"/></td>
          <td><xsl:value-of select="��762/����/���_����"/></td>
          <td><xsl:value-of select="��762/����/����"/></td>
          <td><xsl:value-of select="��762/����/����������"/></td>
        </tr>
      </table>
  
	    <p></p>
	
	    <table border="1" cellpadding="4" cellspacing="0">
		    <tr>����������</tr>
			    <tr bgcolor="FFC0CB">
				    <th>���� ��������</th>
				    <th>���� ���������</th>
				    <th>������� ��������</th>
				    <th>������� ���������</th>
				    <th>�� ������ ������</th>
				    <th>�� �����</th>
				    <th>�������</th>
				    <th>�� ����</th>
            <th>���������� ����</th>
			    </tr>
			    <tr align="center">
				    <td><xsl:value-of select="��762/������������_�����������/����_���"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����_����"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����_����_��������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����_����_���������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/�������"/></td>
				    <td><xsl:value-of select="��762/������������_�����������/����"/></td>
            <td><xsl:value-of select="��762/������������_�����������/����������_����"/></td>
			    </tr>
	    </table>
	
    <xsl:if test="$vers &gt; 3.02">
      <p></p>
      <h2>������������ ���</h2>
      <table border="1" align="center">
        <tr bgcolor="FFF000">
          <th>�����</th>
          <th>��������</th>
          <xsl:for-each select="��762/����/Setpoints/Goose">
            <xsl:if test="position() = 1">
              <xsl:for-each select="������_���">
                <th>GoIn <xsl:value-of select="position()"/></th>
              </xsl:for-each>
            </xsl:if>
          </xsl:for-each>
        </tr>
        <xsl:for-each select="��762/����/Setpoints/Goose">
          <tr>
            <td><xsl:value-of select="position()"/></td>
            <td><xsl:value-of select="������������_���"/></td>
            <xsl:for-each select="������_���">
              <th><xsl:value-of select="current()"/></th>
            </xsl:for-each>
          </tr> 
        </xsl:for-each>
      </table>
    </xsl:if>

    <xsl:if test="$vers &gt; 3.02">
    <p></p>
    <b>������������ Ethernet</b>
    <table border="1" cellspacing="0">
      <tr bgcolor="B0C4DE">
        <th>IP-�����</th>
      </tr>
      <xsl:for-each select="��762/IP">
        <tr align="center">
          <td>
            <xsl:value-of select="IpHi2"/>.
            <xsl:value-of select="IpHi1"/>.
            <xsl:value-of select="IpLo2"/>.
            <xsl:value-of select="IpLo1"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>
    </xsl:if>

  </body>
</html>
</xsl:template>
</xsl:stylesheet>
