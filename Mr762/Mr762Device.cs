﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;
using System.Xml.Serialization;
using System.ComponentModel;
using BEMN.MBServer.Queries;
using BEMN.Mr762.BSBGL;
using BEMN.Mr762.Version1.AlarmJournal;
using BEMN.Mr762.Version1.Configuration;
using BEMN.Mr762.Version1.Configuration.Structures;
using BEMN.Mr762.Version1.Measuring;
using BEMN.Mr762.Version1.OldClasses;
using BEMN.Mr762.Version1.Osc;
using BEMN.Mr762.Version1.Osc.Loaders;
using BEMN.Mr762.Version1.Osc.Structures;
using BEMN.Mr762.Version1.SystemJournal;
using BEMN.Mr762.Version1.SystemJournal.Structures;
using BEMN.Mr762.Version2.AlarmJournal;
using BEMN.Mr762.Version2.Configuration;
using BEMN.Mr762.Version2.Measuring;
using BEMN.Mr762.Version2.Osc;
using BEMN.Mr762.Version2.SystemJournal;
using BEMN.Mr762.Version201.Configuration;
using BEMN.Mr762.Version201.Measuring;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.Mr762.Version1.AlarmJournal.Structures;
using BEMN.Mr762.Version1.Measuring.Structures;
using BEMN.Mr762.Version204.Configuration;
using BEMN.Mr762.Version300.AlarmJournal;
using BEMN.Mr762.Version300.Configuration;
using BEMN.Mr762.Version300.Configuration.Structures;
using BEMN.Mr762.Version300.Emulation;
using BEMN.Mr762.Version300.Emulation.Structers;
using BEMN.Mr762.Version300.Measuring;
using BEMN.Mr762.Version300.Oscilloscope;
using BEMN.Mr762.Version300.SystemJournal;

namespace BEMN.Mr762
{
    public class Mr762Device: Device, IDeviceView, IDeviceVersion
    {
        #region Константы
        public const int RELE_COUNT = 32;
        public const int INDICATOR_COUNT = 12;

        public const int BAUDE_RATE_MAX = 921600;
        private ushort _groupSetPointSize;
        private bool _isCheckVersion;
        #endregion

        #region Переменные

        private Mr762DeviceV2 _mr762DeviceV2;
        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;


        private Dictionary<string, StObj> _memoryMap;
        private Dictionary<int, MLKRequest> _transactions = new Dictionary<int, MLKRequest>();

        public Dictionary<string, StObj> MemoryMap
        {
            get
            {
                return _memoryMap;
            }
            set
            {
                _memoryMap = value;
            }
        }
        public Dictionary<int, MLKRequest> Transactions
        {
            get { return _transactions; }
            set { _transactions = value; }
        }

        private bool _devicesInitialised;
        #endregion

        #region Структуры
        private SWITCH _switch;
        private APV _apv;
        private AVR _avr;
        private LPB _lpb;
        private AUTOBLOWER _autoblower;
        private TERMAL _termall;
        private INPUTSIGNAL _inputsignal;
        private OSCOPE _osc = new OSCOPE(true);
        private MEASURETRANS _measuretrans;
        private INPSYGNAL _inpsignal =  new INPSYGNAL(64);
        private ELSSYGNAL _elssignal = new ELSSYGNAL(64);
        private CURRENTPROTMAIN _protmain = new CURRENTPROTMAIN(64);
        private CURRENTPROTRESERVE _protreserve =  new CURRENTPROTRESERVE(64);
        private PARAMAUTOMAT _paramautomat =  new PARAMAUTOMAT(64);
        private CONFIGSYSTEM _configsystem;
        private CONFOMP _confOMP;
        private MTZMAIN _mtzMain;
        private MTZMAIN _mtzMainI0;
        private MTZMAIN _mtzMainI2I1;
        private MTZMAIN _mtzMainIr;
        private MTZUEXT _mtzExt;
        private MTZUEXT _mtzFMax;
        private MTZUEXT _mtzFMin;
        private MTZUEXT _mtzUMax;
        private MTZUEXT _mtzUMin;

        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;

        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return _refreshAlarmJournal; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return _analogDataBase; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return _discretDataBase; }
        }

        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return _alarmJournal; }
        }
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return _measureTrans; }
        }

        private MemoryEntity<MeasureTransStruct> _measureTransOsc;
        public MemoryEntity<MeasureTransStruct> MeasureTransOsc
        {
            get { return _measureTransOsc; }
        }

        private MemoryEntity<SystemJournalStruct> _systemJournal;
        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        private MemoryEntity<DateTimeStruct> _dateTime; 
        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return _dateTime; }
        }
        private MemoryEntity<JournalRefreshStruct> _refreshSystemJournal;
        public MemoryEntity<JournalRefreshStruct> RefreshSystemJournal
        {
            get { return _refreshSystemJournal; }
        }
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        private CurrentOptionsLoaderV1 _currentOptionsLoaderV1;
        /// <summary>
        /// Загрузчик уставок токов
        /// </summary>
        public CurrentOptionsLoaderV1 CurrentOptionsLoaderV1
        {
            get { return _currentOptionsLoaderV1; }
        }

        public MemoryEntity<OscJournalStructV1> OscJournal
        {
            get { return _oscJournal; }
        }

        public MTZMAIN sMtzMain
        {
            get { return _mtzMain; }
            set { _mtzMain = value; }
        }

        public MTZMAIN sMtzMainI0
        {
            get { return _mtzMainI0; }
            set { _mtzMainI0 = value; }
        }

        public MTZMAIN sMtzMainI2I1
        {
            get { return _mtzMainI2I1; }
            set { _mtzMainI2I1 = value; }
        }

        public MTZMAIN sMtzMainIr
        {
            get { return _mtzMainIr; }
            set { _mtzMainIr = value; }
        }

        public MTZUEXT sMtzExt
        {
            get { return _mtzExt; }
            set { _mtzExt = value; }
        }

        public MTZUEXT sMtzFMax
        {
            get { return _mtzFMax; }
            set { _mtzFMax = value; }
        }

        public MTZUEXT sMtzFMin
        {
            get { return _mtzFMin; }
            set { _mtzFMin = value; }
        }

        public MTZUEXT sMtzUMax
        {
            get { return _mtzUMax; }
            set { _mtzUMax = value; }
        }

        public MTZUEXT sMtzUMin
        {
            get { return _mtzUMin; }
            set { _mtzUMin = value; }
        }

        public SWITCH sSwitch 
        {
            get { return _switch; }
            set { _switch = value; }
        }

        public APV sApv
        {
            get { return _apv; }
            set { _apv = value; }
        }

        public AVR sAvr
        {
            get { return _avr; }
            set { _avr = value; }
        }

        public LPB sLpb
        {
            get { return _lpb; }
            set { _lpb = value; }
        }

        public AUTOBLOWER sAutoblower
        {
            get { return _autoblower; }
            set { _autoblower = value; }
        }

        public TERMAL sTermall
        {
            get { return _termall; }
            set { _termall = value; }
        }

        public INPUTSIGNAL sInputsignal
        {
            get { return _inputsignal; }
            set { _inputsignal = value; }
        }

        public OSCOPE sOsc
        {
            get { return _osc; }
            set { _osc = value; }
        }

        public MEASURETRANS sMeasuretrans
        {
            get { return _measuretrans; }
            set { _measuretrans = value; }
        }

        public INPSYGNAL sInpsignal
        {
            get { return _inpsignal; }
            set { _inpsignal = value; }
        }

        public ELSSYGNAL sElssignal
        {
            get { return _elssignal; }
            set { _elssignal = value; }
        }

        public CURRENTPROTMAIN sProtmain
        {
            get { return _protmain; }
            set { _protmain = value; }
        }

        public CURRENTPROTRESERVE sProtreserve
        {
            get { return _protreserve; }
            set { _protreserve = value; }
        }

        public PARAMAUTOMAT sParamautomat
        {
            get { return _paramautomat; }
            set { _paramautomat = value; }
        }

        public CONFIGSYSTEM sConfigsystem
        {
            get { return _configsystem; }
            set { _configsystem = value; }
        }

        public CONFOMP sConfOMP
        {
            get { return _confOMP; }
            set { _confOMP = value; }
        }
        #endregion

        #region Конструкторы и инициализация

        public Mr762Device()
        {
            HaveVersion = true;
        }

        public Mr762Device(Modbus mb)
        {
            MB = mb;
            HaveVersion = true;
            InitAddrVers1();
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }


        #endregion

        #region Поля
        private slot _program = new slot(0x4300, 0x4700);                     //Бинарный код программы
        private slot _programstart = new slot(0x0E00, 0x0E01);                //Управление программы
        private slot _programStorage = new slot(0xC000, 0xE000);              //Архив схемы
        private slot _programSignals = new slot(0x4100, 0x4300);              //Рабочая база данных
        private slot _programPage = new slot(0x4000, 0x4001);

        #region Новый осциллограф
        private static ushort _startOscJournalAdress = 0x0800;
        private static ushort _startOscJournal = _startOscJournalAdress;
        private static ushort _sizeOscJournal = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OscJournal)) / 2 - 2);
        private static ushort _endOscJournal = (ushort)(_startOscJournal + _sizeOscJournal);

        private static ushort _startOscAdress = 0x0900;
        private static ushort _startOscilloscope = _startOscAdress;
        private static ushort _sizeOscilloscope = (ushort)0x7c;//Слова
        private static ushort _endOscilloscope = (ushort)(_startOscilloscope + _sizeOscilloscope);

        //Журнал осциллограммы
        private slot _oscilloscopeJournalReadSlot = new slot(_startOscJournal, _endOscJournal);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopeJournalWriteSlot = new slot(_startOscJournal, (ushort)(_startOscJournal + 1));

        //Журнал осциллограммы
        private List<slot> CurrentOscilloscope = new List<slot>();
        private slot _oscilloscopeRead = new slot(_startOscilloscope, _endOscilloscope);

        //Номер записи журнала осциллогрфа
        private slot _oscilloscopePageWrite = new slot(_startOscilloscope, (ushort)(_startOscilloscope + 1));

        private slot _oscLength = new slot(0x05A0, 0x05A2);  
        #endregion
        #endregion
        
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(Mr762Device);}
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mrBig; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР762"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        #region TT
        public string TTtype
        {
            get
            {
                return Strings.TT_Type[_measuretrans.L1.Binding];
            }
            set
            {
                _measuretrans.L1.Binding = (ushort)Strings.TT_Type.IndexOf(value);
            }
        }

        public double Imax
        {
            get
            {
                return Measuring.GetConstraintOnly(_measuretrans.L1.Imax, ConstraintKoefficient.K_4000);
            }
            set
            {
                _measuretrans.L1.Imax = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public ushort ITTL
        {
            get
            {
                return _measuretrans.L1.Ittl;
            }
            set
            {
                _measuretrans.L1.Ittl = value;
            }
        }

        public ushort ITTX
        {
            get
            {
                return _measuretrans.L1.Ittx;
            }
            set
            {
                _measuretrans.L1.Ittx = value;
            }
        }

        public ushort ITTX1
        {
            get
            {
                return _measuretrans.L1.Ittx1;
            }
            set
            {
                _measuretrans.L1.Ittx1 = value;
            }
        }
        #endregion

        #region TT
      

        public double KTHL
        {
            get
            {
                ushort res = Common.GetBits(_measuretrans.U1.Ittl, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Measuring.GetU(res, ConstraintKoefficient.K_1000);
            }
            set
            {
                _measuretrans.U1.Ittl = Common.SetBits(_measuretrans.U1.Ittl, (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000), 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        public double KTHX
        {
            get
            {
                ushort res = Common.GetBits(_measuretrans.U1.Ittx, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
                return Measuring.GetU(res, ConstraintKoefficient.K_1000);
            }
            set
            {
                _measuretrans.U1.Ittx = Common.SetBits(_measuretrans.U1.Ittx, (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000), 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
        }

        public string KTHLKoeff
        {
            get
            {
                return Strings.Measure_Koef[Common.GetBits(_measuretrans.U1.Ittl, 15) >> 15];
            }
            set
            {
                _measuretrans.U1.Ittl = Common.SetBits(_measuretrans.U1.Ittl, (ushort)Strings.Measure_Koef.IndexOf(value), 15);
            }
        }

        public string KTHXKoeff
        {
            get
            {
                return Strings.Measure_Koef[Common.GetBits(_measuretrans.U1.Ittx, 15) >> 15];
            }
            set
            {
                _measuretrans.U1.Ittx = Common.SetBits(_measuretrans.U1.Ittx, (ushort)Strings.Measure_Koef.IndexOf(value), 15);
            }
        }

        public string PolarityL
        {
            get
            {
                return Strings.SygnalInputSignals[_measuretrans.U1.PolarityL];
            }
            set
            {
                _measuretrans.U1.PolarityL = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string PolarityX
        {
            get
            {
                return Strings.SygnalInputSignals[_measuretrans.U1.PolarityX];
            }
            set
            {
                _measuretrans.U1.PolarityX = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }
        #endregion

        #region ОМП
        public string OMPtype
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_confOMP.config, 0)];
            }
            set
            {
                _confOMP.config = Common.SetBits(_confOMP.config, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public double Hud
        {
            get
            {
                return (double)_confOMP.Xyg / (double)1000;
            }
            set
            {
                _confOMP.Xyg = (ushort)(value * 1000);
            }
        }
        #endregion

        #region Реле
        public ushort[] OutputReleValues
        {
            get
            {
                List<ushort> rele = new List<ushort>();
                for (int i = 0; i < _paramautomat.releoutrom.Length; i++)
                {
                    rele.AddRange(_paramautomat.releoutrom[i].GetValues());
                }
                return rele.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < _paramautomat.releoutrom.Length; i++)
                {
                    StructInfo sInfo = _paramautomat.releoutrom[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    _paramautomat.releoutrom[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }
        #endregion

        #region Индикаторы 
        public ushort[] OutputIndicatorValues
        {
            get
            {
                List<ushort> rele = new List<ushort>();
                for (int i = 0; i < _paramautomat.indicatorrom.Length; i++)
                {
                    rele.AddRange(_paramautomat.indicatorrom[i].GetValues());
                }
                return rele.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < _paramautomat.indicatorrom.Length; i++)
                {
                    StructInfo sInfo = _paramautomat.indicatorrom[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    _paramautomat.indicatorrom[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }
        #endregion

        #region Параметры неисправности
        public string OutputN1
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(_paramautomat.disrepair, 0);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                _paramautomat.disrepair = Common.SetBit(_paramautomat.disrepair, 0, rez);
            }
        }

        public string OutputN2
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(_paramautomat.disrepair, 1);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                _paramautomat.disrepair = Common.SetBit(_paramautomat.disrepair, 1, rez);
            }
        }

        public string OutputN3
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(_paramautomat.disrepair, 2);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                _paramautomat.disrepair = Common.SetBit(_paramautomat.disrepair, 2, rez);
            }
        }

        public string OutputN4
        {
            get
            {
                string rez = "Нет";
                bool bit = Common.GetBit(_paramautomat.disrepair, 3);
                if (bit)
                {
                    rez = "Есть";
                }

                return rez;
            }
            set
            {
                bool rez = false;
                if (value == "Есть")
                {
                    rez = true;
                }

                _paramautomat.disrepair = Common.SetBit(_paramautomat.disrepair, 3, rez);
            }
        }

        public int OutputImp
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_paramautomat.disrepairImp) ;
            }
            set
            {
                _paramautomat.disrepairImp = ValuesConverterCommon.SetWaitTime(value);
            }
        }
        #endregion

        #region Выключатель
        public string SwitchOff
        {
            get { return Strings.SygnalInputSignals[_switch.off]; }
            set { _switch.off = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchOn
        {
            get { return Strings.SygnalInputSignals[_switch.on]; }
            set { _switch.on = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchError
        {
            get { return Strings.SygnalInputSignals[_switch.err]; }
            set { _switch.err = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchBlock
        {
            get { return Strings.SygnalInputSignals[_switch.block]; }
            set { _switch.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int SwitchTUrov
        {
            get { return ValuesConverterCommon.GetWaitTime(_switch.timeUrov); }
            set { _switch.timeUrov = ValuesConverterCommon.SetWaitTime(value); }
        }

        public double SwitchIUrov
        {
            get
            {
                return ValuesConverterCommon.GetIn(_switch.urov);/* Measuring.GetConstraintOnly(_switch.urov, ConstraintKoefficient.K_4001);*/
            }
            set
            {
                _switch.urov = ValuesConverterCommon.SetIn(value);
                    /* Measuring.SetConstraint(value, ConstraintKoefficient.K_4001);*/
            }
        }

        public int SwitchImpuls
        {
            get { return ValuesConverterCommon.GetWaitTime(_switch.timeImp) ; }
            set { _switch.timeImp = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int SwitchTUskor
        {
            get { return ValuesConverterCommon.GetWaitTime(_switch.timeAcc); }
            set { _switch.timeAcc = ValuesConverterCommon.SetWaitTime(value); ; }
        }

        public string SwitchKontCep
        {
            get { return Validator.Get(_switch.ccde, Strings.ModesLight, 0); }
            set { _switch.ccde = Validator.Set(value, Strings.ModesLight); }
        }
        #endregion

        #region АПВ
        public string APVModes
        {
            get { return Validator.Get(_apv.config, Strings.APVMode, 0, 1, 2); }
            set { _apv.config = Validator.Set(value, Strings.APVMode, this._apv.config, 0, 1, 2); }
        }

        public string APVBlocking
        {
            get { return Strings.SygnalInputSignals[_apv.block]; }
            set { _apv.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int APVTBlock
        {
            get { return ValuesConverterCommon.GetWaitTime(_apv.timeBlock) ; }
            set { _apv.timeBlock = ValuesConverterCommon.SetWaitTime(value); ; }
        }

        public int APVTReady
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_apv.ctrl); }
            set { _apv.ctrl = ValuesConverterCommon.SetWaitTime(value); ; }
        }

        public int APV1Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(_apv.step1); }
            set { _apv.step1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int APV2Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(_apv.step2 );  }
            set
            {
                _apv.step2 = ValuesConverterCommon.SetWaitTime(value); 
            }
        }

        public int APV3Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(_apv.step3); }
            set
            {
                _apv.step3 = ValuesConverterCommon.SetWaitTime(value); 
            }

        }

        public int APV4Krat
        {
            get { return ValuesConverterCommon.GetWaitTime(_apv.step4 );  }
            set
            {
                _apv.step4 = ValuesConverterCommon.SetWaitTime(value); 
            }

        }

        public string APVOff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(_apv.config, 8) >> 8)];
            }
            set
            {
                _apv.config = Common.SetBits(_apv.config, (ushort)Strings.YesNo.IndexOf(value), 8);
            }
        }
        #endregion

        #region АВР
        public string AVRBySignal
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(_avr.config, 0) >> 0)];
            }
            set
            {
                _avr.config = Common.SetBits(_avr.config, (ushort)Strings.YesNo.IndexOf(value), 0);
            }
        }

        public string AVRByOff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(_avr.config, 2) >> 2)];
            }
            set
            {
                _avr.config = Common.SetBits(_avr.config, (ushort)Strings.YesNo.IndexOf(value), 2);
            }
        }

        public string AVRBySelfOff
        {
            get { return Strings.YesNo[(Common.GetBits(_avr.config, 1) >> 1)]; }
            set { _avr.config = Common.SetBits(_avr.config, (ushort)Strings.YesNo.IndexOf(value), 1); }
        }

        public string AVRByDiff
        {
            get
            {
                return Strings.YesNo[(Common.GetBits(_avr.config, 3) >> 3)];
            }
            set
            {
                _avr.config = Common.SetBits(_avr.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string AVRSIGNOn
        {
            get { return Strings.SygnalInputSignals[_avr.on]; }
            set { _avr.on = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRBlocking
        {
            get { return Strings.SygnalInputSignals[_avr.block]; }
            set { _avr.block = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRBlockClear
        {
            get { return Strings.SygnalInputSignals[_avr.clear]; }
            set { _avr.clear = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string AVRResolve
        {
            get { return Strings.SygnalInputSignals[_avr.start]; }
            set { _avr.start = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int AVRTimeOn
        {
            get { return ValuesConverterCommon.GetWaitTime(_avr.timeOn) ; }
            set { _avr.timeOn = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRBack
        {
            get { return Strings.SygnalInputSignals[_avr.off]; }
            set { _avr.off = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public int AVRTimeBack
        {
            get { return ValuesConverterCommon.GetWaitTime(_avr.timeOff); }
            set { _avr.timeOff = ValuesConverterCommon.SetWaitTime(value); }
        }

        public int AVRTimeOtkl
        {
            get { return ValuesConverterCommon.GetWaitTime(_avr.timeOtkl); }
            set { _avr.timeOtkl = ValuesConverterCommon.SetWaitTime(value); }
        }

        public string AVRClear
        {
            get
            {
                int index = 1;
                if (Common.GetBit(_avr.config, 7))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                _avr.config = Common.SetBit(_avr.config, 7, res);
            }
        }

        #endregion

        #region ЛЗШ
        public string LZHMode
        {
            get { return Validator.Get(this._lpb.config, Strings.LZHModes, 0, 1); }
            set { _lpb.config = Validator.Set(value, Strings.LZHModes); }
        }

        public double LZHUstavka
        {
            get { return ValuesConverterCommon.GetIn(_lpb.val); }
            set { _lpb.val = ValuesConverterCommon.SetIn(value); }
        }
        #endregion

        #region Управление
        public string SwitchKeyOn
        {
            get { return Strings.SygnalInputSignals[_switch.keyOn]; }
            set { _switch.keyOn = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchKeyOff
        {
            get { return Strings.SygnalInputSignals[_switch.keyOff]; }
            set { _switch.keyOff = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchVneshOn
        {
            get { return Strings.SygnalInputSignals[_switch.extOn]; }
            set { _switch.extOn = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchVneshOff
        {
            get { return Strings.SygnalInputSignals[_switch.extOff]; }
            set { _switch.extOff = (ushort)Strings.SygnalInputSignals.IndexOf(value); }
        }

        public string SwitchButtons
        {
            get
            {
                int index = 1;
                if (Common.GetBit(_switch.config, 0))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                _switch.config = Common.SetBit(_switch.config, 0, res);
            }
        }

        public string SwitchKey
        {
            get
            {
                int index = 1;
                if (Common.GetBit(_switch.config, 1))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                _switch.config = Common.SetBit(_switch.config, 1, res);
            }
        }

        public string SwitchVnesh
        {
            get
            {
                int index = 1;
                if (Common.GetBit(_switch.config, 2))
                {
                    index = 0;
                }
                return Strings.Forbidden2[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden2.IndexOf(value) == 0)
                {
                    res = true;
                }
                _switch.config = Common.SetBit(_switch.config, 2, res);
            }
        }

        public string SwitchSDTU
        {
            get
            {
                int index = 1;
                if (Common.GetBit(_switch.config, 3))
                {
                    index = 0;
                }
                return Strings.Forbidden[index];
            }
            set
            {
                bool res = false;
                if (Strings.Forbidden.IndexOf(value) == 0)
                {
                    res = true;
                }
                _switch.config = Common.SetBit(_switch.config, 3, res);
            }
        }
        #endregion

        #region ВЛС

        public ushort[] VLS
        {
            get
            {
                ushort[] res = new ushort[_elssignal.i.Length * _elssignal.i[0].a.Length];
                int index = 0;
                for (int i = 0; i < _elssignal.i.Length; i++)
                {
                    Array.ConstrainedCopy(_elssignal.i[i].a, 0, res, index, _elssignal.i[i].a.Length);
                    index += _elssignal.i[i].a.Length;
                }

                return res;
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < _elssignal.i.Length; i++)
                {
                    Array.ConstrainedCopy(res, index, _elssignal.i[i].a, 0, _elssignal.i[i].a.Length);
                    index += _elssignal.i[i].a.Length;
                }
            }
        }

        #endregion

        #region Входные сигналы
        public ushort[] InpSygnals
        {
            get
            {
                List<ushort> res = new List<ushort>();
                for (int i = 0; i < _inpsignal.i.Length; i++)
                {
                    res.AddRange(_inpsignal.i[i].GetValues());
                }

                return res.ToArray();
            }
            set
            {
                ushort[] res = value;
                int index = 0;
                for (int i = 0; i < _inpsignal.i.Length; i++)
                {
                    StructInfo sInfo = _inpsignal.i[i].GetStructInfo(64);
                    ushort[] tmp = new ushort[sInfo.FullSize];
                    Array.ConstrainedCopy(res, index, tmp, 0, tmp.Length);
                    _inpsignal.i[i].InitStruct(Common.TOBYTES(tmp, false));
                    index += tmp.Length;
                }
            }
        }

        public string GrUst
        {
            get
            {
                return Strings.SygnalInputSignals[_inputsignal.groopUst];
            }
            set
            {
                _inputsignal.groopUst = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string SbInd
        {
            get
            {
                return Strings.SygnalInputSignals[_inputsignal.clrInd];
            }
            set
            {
                _inputsignal.clrInd = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }
        #endregion
        
        #region Защиты I
        public string I_MODE 
        {
            get { return Validator.Get(_mtzMain.config, Strings.ModesLightMode, 0, 1); }
            set { _mtzMain.config = Validator.Set(value, Strings.ModesLightMode, _mtzMain.config, 0, 1); }
        }

        public double I_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzMain.ust, ConstraintKoefficient.K_4000);
            }
            set 
            {
                _mtzMain.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double I_UPUSK
        {
            get
            {
                return Measuring.GetU(_mtzMain.u, ConstraintKoefficient.K_1000);
            }
            set 
            {
                _mtzMain.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string I_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMain.config, 3) >> 3];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 3); 
            }
        }

        public string I_DIRECTION
        {
            get
            {
                return Strings.BusDirection[Common.GetBits(_mtzMain.config, 6, 7) >> 6];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.BusDirection.IndexOf(value), 6, 7); 
            }
        }

        public string I_UNDIRECTION
        {
            get
            {
                return Strings.UnDirection[Common.GetBits(_mtzMain.config, 8) >> 8];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.UnDirection.IndexOf(value), 8); 
            }
        }

        public string I_LOGIC
        {
            get
            {
                return Strings.TokParameter[Common.GetBits(_mtzMain.config, 12) >> 12];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.TokParameter.IndexOf(value), 12); 
            }
        }

        public string I_CHAR
        {
            get
            {
                return Strings.Characteristic[Common.GetBits(_mtzMain.config, 4) >> 4];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.Characteristic.IndexOf(value), 4); 
            }
        }

        public int I_T
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMain.time);
            }
            set 
            {
                _mtzMain.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public ushort I_K
        {
            get
            {
                return _mtzMain.k;
            }
            set 
            {
                _mtzMain.k = value;
            }
        }

        public int I_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMain.tu);
            }
            set 
            {
                _mtzMain.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMain.config, 5) >> 5];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 5); 
            }
        }

        public string I_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzMain.block];
            }
            set 
            {
                _mtzMain.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double I_2r1r
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzMain.I21, ConstraintKoefficient.K_10000);
            }
            set 
            {
                _mtzMain.I21 = Measuring.SetConstraint(value, ConstraintKoefficient.K_10000);
            }
        }

        public string I_2r1r_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMain.config, 9) >> 9];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 9); 
            }
        }

        public string I_BLOCK_DIRECT
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMain.config, 10) >> 10];
            }
            set 
            {
                _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort)Strings.YesNo.IndexOf(value), 10); 
            }
        }

        public string I_OSC
        {
            get { return Validator.Get(_mtzMain.config1, Strings.ModesLightOsc, 4,5); }
            set { _mtzMain.config1 = Validator.Set(value, Strings.ModesLightOsc, _mtzMain.config1, 4, 5); }
        }

        public string I_UROV
        {
            get { return Strings.ModesLight[Common.GetBits(_mtzMain.config, 2) >> 2]; }
            set { _mtzMain.config = Common.SetBits(_mtzMain.config, (ushort) Strings.ModesLight.IndexOf(value), 2); }
        }

        public string I_APV
        {
            get { return Strings.ModesLight[Common.GetBits(_mtzMain.config1, 0) >> 0]; }
            set { _mtzMain.config1 = Common.SetBits(_mtzMain.config1, (ushort) Strings.ModesLight.IndexOf(value), 0); }
        }

        public string I_AVR
        {
            get { return Strings.ModesLight[Common.GetBits(_mtzMain.config1, 1) >> 1]; }
            set { _mtzMain.config1 = Common.SetBits(_mtzMain.config1, (ushort) Strings.ModesLight.IndexOf(value), 1); }
        }

        #endregion

        #region Защиты I*
        public string I0_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzMainI0.config, 0, 1) >> 0];
            }
            set
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double I0_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzMainI0.ust, ConstraintKoefficient.K_4000);
            }
            set 
            {
                _mtzMainI0.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public double I0_UPUSK
        {
            get
            {
                return Measuring.GetU(_mtzMainI0.u, ConstraintKoefficient.K_1000);
            }
            set 
            {
                _mtzMainI0.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string I0_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMainI0.config, 3) >> 3];
            }
            set
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string I0_DIRECTION
        {
            get
            {
                return Strings.BusDirection[Common.GetBits(_mtzMainI0.config, 6, 7) >> 6];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.BusDirection.IndexOf(value), 6, 7);
            }
        }

        public string I0_UNDIRECTION
        {
            get
            {
                return Strings.UnDirection[Common.GetBits(_mtzMainI0.config, 8) >> 8];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.UnDirection.IndexOf(value), 8);
            }
        }

        public string I0_I0
        {
            get
            {
                return Strings.I0Modes[Common.GetBits(_mtzMainI0.config, 12, 13) >> 12];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.I0Modes.IndexOf(value), 12, 13);
            }
        }

        public string I0_CHAR
        {
            get
            {
                return Strings.Characteristic[Common.GetBits(_mtzMainI0.config, 4) >> 4];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.Characteristic.IndexOf(value), 4);
            }
        }

        public int I0_T
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMainI0.time);
            }
            set 
            {
                _mtzMainI0.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public ushort I0_K
        {
            get
            {
                return _mtzMainI0.k;
            }
            set 
            {
                _mtzMainI0.k = value;
            }
        }

        public string I0_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzMainI0.block];
            }
            set 
            {
                _mtzMainI0.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string I0_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzMainI0.config1, 4, 5) >> 4];
            }
            set 
            {
                _mtzMainI0.config1 = Common.SetBits(_mtzMainI0.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public int I0_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMainI0.tu) ;
            }
            set 
            {
                _mtzMainI0.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I0_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMainI0.config, 5) >> 5];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.YesNo.IndexOf(value), 5);
            }
        }

        public string I0_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI0.config, 2) >> 2];
            }
            set 
            {
                _mtzMainI0.config = Common.SetBits(_mtzMainI0.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string I0_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI0.config1, 0) >> 0];
            }
            set 
            {
                _mtzMainI0.config1 = Common.SetBits(_mtzMainI0.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string I0_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI0.config1, 1) >> 1];
            }
            set
            {
                _mtzMainI0.config1 = Common.SetBits(_mtzMainI0.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты I2I1
        public string I2I1_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzMainI2I1.config, 0, 1) >> 0];
            }
            set
            {
                _mtzMainI2I1.config = Common.SetBits(_mtzMainI2I1.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string I2I1_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzMainI2I1.block];
            }
            set
            {
                _mtzMainI2I1.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double I2I1_I2I1
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzMainI2I1.ust, ConstraintKoefficient.K_10000);
            }
            set
            {
                _mtzMainI2I1.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_10000);
            }
        }

        public int I2I1_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMainI2I1.time) ;
            }
            set
            {
                _mtzMainI2I1.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string I2I1_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzMainI2I1.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzMainI2I1.config1 = Common.SetBits(_mtzMainI2I1.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string I2I1_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI2I1.config, 2) >> 2];
            }
            set
            {
                _mtzMainI2I1.config = Common.SetBits(_mtzMainI2I1.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string I2I1_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI2I1.config1, 0) >> 0];
            }
            set
            {
                _mtzMainI2I1.config1 = Common.SetBits(_mtzMainI2I1.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string I2I1_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainI2I1.config1, 1) >> 1];
            }
            set
            {
                _mtzMainI2I1.config1 = Common.SetBits(_mtzMainI2I1.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты Ir
        public string Ir_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzMainIr.config, 0, 1) >> 0];
            }
            set
            {
                _mtzMainIr.config = Common.SetBits(_mtzMainIr.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string Ir_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzMainIr.block];
            }
            set
            {
                _mtzMainIr.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public double Ir_UPUSK
        {
            get
            {
                return Measuring.GetU(_mtzMainIr.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                _mtzMainIr.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string Ir_UPUSK_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMainIr.config, 3) >> 3];
            }
            set
            {
                _mtzMainIr.config = Common.SetBits(_mtzMainIr.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public double Ir_ICP
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzMainIr.ust, ConstraintKoefficient.K_4000);
            }
            set
            {
                _mtzMainIr.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_4000);
            }
        }

        public int Ir_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMainIr.time);
            }
            set
            {
                _mtzMainIr.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }


        public int Ir_TY
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzMainIr.tu) ;
            }
            set
            {
                _mtzMainIr.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string Ir_TY_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzMainIr.config, 5) >> 5];
            }
            set
            {
                _mtzMainIr.config = Common.SetBits(_mtzMainIr.config, (ushort)Strings.YesNo.IndexOf(value), 5);
            }
        }

        public string Ir_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzMainIr.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzMainIr.config1 = Common.SetBits(_mtzMainIr.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string Ir_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainIr.config, 2) >> 2];
            }
            set
            {
                _mtzMainIr.config = Common.SetBits(_mtzMainIr.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string Ir_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainIr.config1, 0) >> 0];
            }
            set
            {
                _mtzMainIr.config1 = Common.SetBits(_mtzMainIr.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string Ir_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzMainIr.config1, 1) >> 1];
            }
            set
            {
                _mtzMainIr.config1 = Common.SetBits(_mtzMainIr.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }
        #endregion

        #region Защиты F>
        public string FB_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzFMax.config, 0, 1) >> 0];
            }
            set
            {
                _mtzFMax.config = Common.SetBits(_mtzFMax.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double FB_FCP
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzFMax.ust, ConstraintKoefficient.K_25600);
            }
            set
            {
                _mtzFMax.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FB_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzFMax.time) ;
            }
            set
            {
                _mtzFMax.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double FB_FVZ
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzFMax.u, ConstraintKoefficient.K_25600);
            }
            set
            {
                _mtzFMax.u = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FB_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzFMax.tu );
            }
            set
            {
                _mtzFMax.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }
        /// <summary>
        /// //////////
        /// </summary>
        public string FB_FVZ_YN
        {
            get
            {

                return Strings.YesNo[Common.GetBits(_mtzFMax.config, 3) >> 3];
            }
            set
            {
                _mtzFMax.config = Common.SetBits(_mtzFMax.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string FB_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzFMax.block];
            }
            set
            {
                _mtzFMax.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string FB_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzFMax.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzFMax.config1 = Common.SetBits(_mtzFMax.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string FB_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMax.config1, 2) >> 2];
            }
            set
            {
                _mtzFMax.config1 = Common.SetBits(_mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FB_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMax.config, 2) >> 2];
            }
            set
            {
                _mtzFMax.config = Common.SetBits(_mtzFMax.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FB_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMax.config1, 0) >> 0];
            }
            set
            {
                _mtzFMax.config1 = Common.SetBits(_mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string FB_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMax.config1, 1) >> 1];
            }
            set
            {
                _mtzFMax.config1 = Common.SetBits(_mtzFMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string FB_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzFMax.config, 15) >> 15];
            }
            set
            {
                _mtzFMax.config = Common.SetBits(_mtzFMax.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты F<
        public string FM_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzFMin.config, 0, 1) >> 0];
            }
            set
            {
                _mtzFMin.config = Common.SetBits(_mtzFMin.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public double FM_FCP
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzFMin.ust, ConstraintKoefficient.K_25600);
            }
            set
            {
                _mtzFMin.ust = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public int FM_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzFMin.time);
            }
            set
            {
                _mtzFMin.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }


        public int FM_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzFMin.tu) ;
            }
            set
            {
                _mtzFMin.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double FM_FVZ
        {
            get
            {
                return Measuring.GetConstraintOnly(_mtzFMin.u, ConstraintKoefficient.K_25600);
            }
            set
            {
                _mtzFMin.u = Measuring.SetConstraint(value, ConstraintKoefficient.K_25600);
            }
        }

        public string FM_FVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzFMin.config, 3) >> 3];
            }
            set
            {
                _mtzFMin.config = Common.SetBits(_mtzFMin.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string FM_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzFMin.block];
            }
            set
            {
                _mtzFMin.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string FM_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzFMin.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzFMin.config1 = Common.SetBits(_mtzFMin.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string FM_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMin.config1, 2) >> 2];
            }
            set
            {
                _mtzFMin.config1 = Common.SetBits(_mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FM_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMin.config, 2) >> 2];
            }
            set
            {
                _mtzFMin.config = Common.SetBits(_mtzFMin.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string FM_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMin.config1, 0) >> 0];
            }
            set
            {
                _mtzFMin.config1 = Common.SetBits(_mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string FM_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzFMin.config1, 1) >> 1];
            }
            set
            {
                _mtzFMin.config1 = Common.SetBits(_mtzFMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string FM_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzFMin.config, 15) >> 15];
            }
            set
            {
                _mtzFMin.config = Common.SetBits(_mtzFMin.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты U>
        public string UB_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzUMax.config, 0, 1) >> 0];
            }
            set
            {
                _mtzUMax.config = Common.SetBits(_mtzUMax.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string UB_TYPE
        {
            get
            {
                return Strings.UType[Common.GetBits(_mtzUMax.config, 5, 6, 7) >> 5];
            }
            set
            {
                _mtzUMax.config = Common.SetBits(_mtzUMax.config, (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }

        public double UB_UCP
        {
            get
            {
                return Measuring.GetU(_mtzUMax.ust, ConstraintKoefficient.K_1000);
            }
            set
            {
                _mtzUMax.ust = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public int UB_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzUMax.time);
            }
            set
            {
                _mtzUMax.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int UB_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzUMax.tu) ;
            }
            set
            {
                _mtzUMax.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double UB_UVZ
        {
            get
            {
                return Measuring.GetU(_mtzUMax.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                _mtzUMax.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string UB_UVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzUMax.config, 3) >> 3];
            }
            set
            {
                _mtzUMax.config = Common.SetBits(_mtzUMax.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string UB_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzUMax.block];
            }
            set
            {
                _mtzUMax.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string UB_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzUMax.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzUMax.config1 = Common.SetBits(_mtzUMax.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string UB_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMax.config1, 2) >> 2];
            }
            set
            {
                _mtzUMax.config1 = Common.SetBits(_mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UB_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMax.config, 2) >> 2];
            }
            set
            {
                _mtzUMax.config = Common.SetBits(_mtzUMax.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UB_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMax.config1, 0) >> 0];
            }
            set
            {
                _mtzUMax.config1 = Common.SetBits(_mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string UB_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMax.config1, 1) >> 1];
            }
            set
            {
                _mtzUMax.config1 = Common.SetBits(_mtzUMax.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string UB_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzUMax.config, 15) >> 15];
            }
            set
            {
                _mtzUMax.config = Common.SetBits(_mtzUMax.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Защиты U<
        public string UM_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzUMin.config, 0, 1) >> 0];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string UM_TYPE
        {
            get
            {
                return Strings.UType[Common.GetBits(_mtzUMin.config, 5, 6, 7) >> 5];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.UType.IndexOf(value), 5, 6, 7);
            }
        }

        public double UM_UCP
        {
            get
            {
                return Measuring.GetU(_mtzUMin.ust, ConstraintKoefficient.K_1000);
            }
            set
            {
                _mtzUMin.ust = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public int UM_TCP
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzUMin.time) ;
            }
            set
            {
                _mtzUMin.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int UM_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzUMin.tu);
            }
            set
            {
                _mtzUMin.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public double UM_UVZ
        {
            get
            {
                return Measuring.GetU(_mtzUMin.u, ConstraintKoefficient.K_1000);
            }
            set
            {
                _mtzUMin.u = (ushort)Measuring.SetU(value, ConstraintKoefficient.K_1000);
            }
        }

        public string UM_UVZ_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzUMin.config, 3) >> 3];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string UM_BLOCK_M5
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzUMin.config, 4) >> 4];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 4);
            }
        }

        public string UM_BLOCK
        {
            get
            {
                return Strings.SygnalInputSignals[_mtzUMin.block];
            }
            set
            {
                _mtzUMin.block = (ushort)Strings.SygnalInputSignals.IndexOf(value);
            }
        }

        public string UM_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzUMin.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzUMin.config1 = Common.SetBits(_mtzUMin.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string UM_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMin.config1, 2) >> 2];
            }
            set
            {
                _mtzUMin.config1 = Common.SetBits(_mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UM_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMin.config, 2) >> 2];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string UM_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMin.config1, 0) >> 0];
            }
            set
            {
                _mtzUMin.config1 = Common.SetBits(_mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string UM_AVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzUMin.config1, 1) >> 1];
            }
            set
            {
                _mtzUMin.config1 = Common.SetBits(_mtzUMin.config1, (ushort)Strings.ModesLight.IndexOf(value), 1);
            }
        }


        public string UM_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzUMin.config, 15) >> 15];
            }
            set
            {
                _mtzUMin.config = Common.SetBits(_mtzUMin.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion

        #region Внешние защиты
        public string EXTERNAL_MODE
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzExt.config, 0, 1) >> 0];
            }
            set
            {
                _mtzExt.config = Common.SetBits(_mtzExt.config, (ushort)Strings.ModesLightMode.IndexOf(value), 0, 1);
            }
        }

        public string EXTERNAL_SRAB
        {
            get
            {
                return Strings.SygnalSrabExternal[_mtzExt.ust];
            }
            set
            {
                _mtzExt.ust = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }


        public int EXTERNAL_TSR
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzExt.time) ;
            }
            set
            {
                _mtzExt.time = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public int EXTERNAL_TVZ
        {
            get
            {
                return ValuesConverterCommon.GetWaitTime(_mtzExt.tu) ;
            }
            set
            {
                _mtzExt.tu = ValuesConverterCommon.SetWaitTime(value);
            }
        }

        public string EXTERNAL_VOZVR
        {
            get
            {
                return Strings.SygnalSrabExternal[_mtzExt.u];
            }
            set
            {
                _mtzExt.u = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }


        public string EXTERNAL_VOZVR_YN
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzExt.config, 3) >> 3];
            }
            set
            {
                _mtzExt.config = Common.SetBits(_mtzExt.config, (ushort)Strings.YesNo.IndexOf(value), 3);
            }
        }

        public string EXTERNAL_BLOCK
        {
            get
            {
                return Strings.SygnalSrabExternal[_mtzExt.block];
            }
            set
            {
                _mtzExt.block = (ushort)Strings.SygnalSrabExternal.IndexOf(value);
            }
        }

        public string EXTERNAL_OSC
        {
            get
            {
                return Strings.ModesLightOsc[Common.GetBits(_mtzExt.config1, 4, 5) >> 4];
            }
            set
            {
                _mtzExt.config1 = Common.SetBits(_mtzExt.config1, (ushort)Strings.ModesLightOsc.IndexOf(value), 4, 5);
            }
        }

        public string EXTERNAL_APV_VOZVR
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzExt.config1, 2) >> 2];
            }
            set
            {
                _mtzExt.config1 = Common.SetBits(_mtzExt.config1, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string EXTERNAL_UROV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzExt.config, 2) >> 2];
            }
            set
            {
                _mtzExt.config = Common.SetBits(_mtzExt.config, (ushort)Strings.ModesLight.IndexOf(value), 2);
            }
        }

        public string EXTERNAL_APV
        {
            get
            {
                return Strings.ModesLight[Common.GetBits(_mtzExt.config1, 0) >> 0];
            }
            set
            {
                _mtzExt.config1 = Common.SetBits(_mtzExt.config1, (ushort)Strings.ModesLight.IndexOf(value), 0);
            }
        }

        public string EXTERNAL_AVR
        {
            get
            {
                return Strings.ModesLightMode[Common.GetBits(_mtzExt.config1, 1) >> 1];
            }
            set
            {
                _mtzExt.config1 = Common.SetBits(_mtzExt.config1, (ushort)Strings.ModesLightMode.IndexOf(value), 1);
            }
        }

        public string EXTERNAL_DROP
        {
            get
            {
                return Strings.YesNo[Common.GetBits(_mtzExt.config, 15) >> 15];
            }
            set
            {
                _mtzExt.config = Common.SetBits(_mtzExt.config, (ushort)Strings.YesNo.IndexOf(value), 15);
            }
        }
        #endregion 

        #region Конфигурация Осциллографа
        public string OSC_LENGTH
        {
            get
            {
                return Strings.OscLength[_osc.size - 1];
            }
            set
            {
                _osc.size = (ushort)(Strings.OscLength.IndexOf(value) + 1);
            }
        }

        public ushort OSC_W_LENGTH
        {
            get
            {
                return (ushort)(_osc.percent);
            }
            set
            {
                _osc.percent = (ushort)(value);
            }
        }

        public string OSC_FIX
        {
            get
            {
                return Strings.OscFix[_osc.config];
            }
            set
            {
                _osc.config = (ushort)Strings.OscFix.IndexOf(value);
            }
        }
        #endregion

        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int ALARM_JOURNAL_START_ADRESS = 0x700;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;

        private MemoryEntity<OscJournalStructV1> _oscJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;
        private MemoryEntity<OscopeStruct> _oscopeStruct;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private OscOptionsLoader _oscOptionsLoader;
        private MemoryEntity<OneWordStruct> _startSpl;
        private MemoryEntity<OneWordStruct> _stopSpl;
        private MemoryEntity<OneWordStruct> _stateSpl;
        private MemoryEntity<OneWordStruct> _isSplOn;
        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return _programPageStruct; }
        }


        public MemoryEntity<OneWordStruct> StopSpl
        {
            get { return _stopSpl; }
        }

        public MemoryEntity<OneWordStruct> StartSpl
        {
            get { return _startSpl; }
        }

        public MemoryEntity<OneWordStruct> StateSpl
        {
            get { return _stateSpl; }
        }
        public MemoryEntity<OneWordStruct> IsSplOn
        {
            get { return _isSplOn; }
        }

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return _programStorageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return _programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return _programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return _sourceProgramStruct; }
        }
        #endregion
        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }

        public OscOptionsLoader OscopeOptionsLoader
        {
            get { return this._oscOptionsLoader; }
        }


        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return _setOscStartPage; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulation
        {
            get { return this._writeSructEmulation; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull
        {
            get { return this._writeSructEmulationNull; }
        }
        public MemoryEntity<ReadStructEmul> ReadStructEmulation
        {
            get { return this._readSructEmulation; }
        }

        private MemoryEntity<MeasureTransStruct> _connectionsAlarmJournal;
        private void InitAddrVers1()
        {
            _memoryMap = new Dictionary<string, StObj>();

            InitStruct(new SWITCH(), "Выключатель", 0x1000);
            InitStruct(new APV(), "АПВ", "Выключатель");
            InitStruct(new AVR(), "АВР", "АПВ");
            InitStruct(new LPB(), "ЛПБ", "АВР");
            InitStruct(new AUTOBLOWER(), "Обдув", "ЛПБ");
            InitStruct(new TERMAL(), "Тепловая_Модель", "Обдув");
            InitStruct(new INPUTSIGNAL(), "Входные_Сигналы", "Тепловая_Модель");
            InitStruct(new OSCOPE(), "Осциллограмма", "Входные_Сигналы");
            InitStruct(new MEASURETRANS(), "Измерительный_Трансформатор", "Осциллограмма");
            InitStruct(new INPSYGNAL(64), "Входные_ЛС", "Измерительный_Трансформатор");
            InitStruct(new ELSSYGNAL(64), "Выходные_ЛС", "Входные_ЛС");
            InitStruct(new CURRENTPROTMAIN(), "Защиты_Основные", "Выходные_ЛС");
            InitStruct(new CURRENTPROTRESERVE(), "Защиты_Резервные", "Защиты_Основные");
            InitStruct(new PARAMAUTOMAT(64), "Автоматика", "Защиты_Резервные");
            InitStruct(new CONFIGSYSTEM(), "Конфигурация_Системы","Автоматика");
            InitStruct(new CONFOMP(), "ОМП", "Конфигурация_Системы");
            InitStruct(new SihronizmStruct(), "Синхронизм", "ОМП");

            this._refreshSystemJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала системы", this, 0x600);
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("ЖС", this, 0x600);
            this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", this, 0x700);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, ANALOG_DATABASE_START_ADRESS);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS);
            this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", this, ALARM_JOURNAL_START_ADRESS);
            this._connectionsAlarmJournal = new MemoryEntity<MeasureTransStruct>("Токи присоединений для ЖА", this, 0x105A);
            this._currentOptionsLoaderV1 = new CurrentOptionsLoaderV1(this._connectionsAlarmJournal);
            this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений measuring", this, 0x105A);
            this._measureTransOsc = new MemoryEntity<MeasureTransStruct>("Параметры измерений osc", this, 0x105A);

            this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x5A0);
            this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", this,OSC_JOURNAL_START_ADRESS);
            this._oscJournal = new MemoryEntity<OscJournalStructV1>("Журнал осциллографа", this,OSC_JOURNAL_START_ADRESS);
            this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
            this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);

            this._oscopeStruct = new MemoryEntity<OscopeStruct>("Уставки осциллографа", this, 0x103a);

            this._oscOptionsLoader = new OscOptionsLoader(this._oscopeStruct);

            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
            this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);

            this._stopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this._startSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);
            this._stateSpl = new MemoryEntity<OneWordStruct>("Состояние ошибок логики", this, 0x0D13);
            this._isSplOn = new MemoryEntity<OneWordStruct>("Состояние логики", this, 0x0D10);

            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись  нулевых аналоговых сигналов ", this, 0x5800);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение аналоговых сигналов", this, 0x582E);
        }

        private void InitStruct(object _struct, string _currentNameSTR, ushort _startAddr)
        {
            StObj strObj = new StObj(_currentNameSTR, mb);
            strObj.StructObj = _struct;
            StructInfo sInf = (strObj.StructObj as IStruct).GetStructInfo(64);
            strObj.StartAddr = _startAddr;
            strObj.EndAddr = (ushort)(strObj.StartAddr + sInf.FullSize);
            if (strObj.EndAddr > strObj.StartAddr)
            {
                strObj.Values = new ushort[strObj.EndAddr - strObj.StartAddr];
            }
            strObj.Slots = new List<slot>();
            object slot = (strObj.StructObj as IStruct).GetSlots(strObj.StartAddr, sInf.SlotsArray, 64);
            if (slot != null)
            {
                if (slot is Array)
                {
                    slot[] slotArray = slot as slot[];
                    foreach (slot s in slotArray)
                    {
                        strObj.Slots.Add(s);
                    }
                }
                else
                {
                    strObj.Slots.Add(slot as slot);
                }
            }
            else
            {
                strObj.Slots = null;
            }
            _memoryMap.Add(_currentNameSTR, strObj);
        }

        private void InitStruct(object structObj, string currentNameSTR, string lastNameSTR)
        {
            this.InitStruct(structObj, currentNameSTR, _memoryMap[lastNameSTR].EndAddr);
        }

        private void InitDeviceNumbers()
        {
            if (!_devicesInitialised)
            {
                if (_memoryMap == null)
                {
                    return;
                }
                foreach (var item in _memoryMap.Values)
                {
                    item.DeviceNum = DeviceNumber;
                }
                _devicesInitialised = true;
            }
        }

        public void ConfirmConstraint()
        {
            SetBit(DeviceNumber, 0x0D00, true, "МР762 запрос подтверждения" + this.DeviceNumber, this);
        }

        public event Action ConfigWriteOk;
        public event Action ConfigWriteFail;

        private new void mb_CompleteExchange(object sender, Query query)
        {
            if (query.name == "version" + this.DeviceNumber)
            {
                this.SetStartConnectionState(!query.error);
                DeviceInfo info = DeviceInfo.GetVersion(Common.SwapArrayItems(query.readBuffer));
                this.Info = info;
                if (Common.VersionConverter(info.Version) >= 3.03 && !_isCheckVersion)
                {
                    _isCheckVersion = true;
                    this._infoSlot = new slot(0x500, 0x500 + 0x20);
                    this.MB.RemoveQuery("version" + this.DeviceNumber);
                    base.LoadVersion(this);
                    return;
                }
                this.LoadVersionOk();
                _isCheckVersion = false;
                return;
            }
            this.InitDeviceNumbers();
            if ("МР762 запрос подтверждения" + this.DeviceNumber == query.name)
            {
                if (query.error)
                {
                    if (this.ConfigWriteFail != null)
                    {
                        this.ConfigWriteFail.Invoke();
                    }
                }
                else
                {
                    if (this.ConfigWriteOk != null)
                    {
                        this.ConfigWriteOk.Invoke();
                    }
                }
            }
            if (query.name == "ConfirmConfig" + this.DeviceNumber)
            {
                if (query.error)
                {
                    MessageBox.Show("Не удалось записать бит подтверждения изменения конфигурации. Конфигурация в устройстве не изменена",
                        "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("Конфигурация записана успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            base.mb_CompleteExchange(sender, query);
        }

        public const string T5N3D42R19 = "T5N3D42R35";
        public const string T5N4D42R19 = "T5N4D42R35";
        
        private static readonly string[] _deviceApparatConfig = { T5N3D42R19, T5N4D42R19 };

        private static readonly string[] _deviceOutString = { T5N3D42R19, T5N4D42R19 };

        private const ushort START_ADDR_MEAS_TRANS = 0x1278;

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return START_ADDR_MEAS_TRANS;
            return (ushort)(START_ADDR_MEAS_TRANS + this._groupSetPointSize * group);
        }

        public Type[] Forms
        {
            get
            {
                if (this.DeviceVersion == "1.00 - 1.01")
                {
                    this.DeviceVersion = "1.01";
                }
                if (this.DeviceVersion == "1.02 - 1.05")
                {
                    this.DeviceVersion = "1.02";
                }

                this._groupSetPointSize = new GroupSetpoint307()
                    .GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 64).FullSize;

                double ver = Common.VersionConverter(this.DeviceVersion);
                _mr762DeviceV2 = new Mr762DeviceV2(this);
                Version300.Configuration.StringsConfig.CurrentVersion = ver;

                if (ver >= 3.02)
                {
                    //if (ver >= 3.07)
                    //{
                    //    if ((!this.DeviceDlgInfo.IsConnectionMode || !this.IsConnect) && !Framework.Framework.IsProjectOpening)
                    //    {
                    //        ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                    //        choice.ShowDialog();
                    //        this.DevicePlant = choice.DeviceType;
                    //    }
                    //}
                    
                    if (Info.Plant == T5N4D42R19)
                    {
                        return new[]
                        {
                            typeof (BSBGLEFV3),
                            typeof (Mr762ConfigurationFormV300),
                            typeof (Mr762MeasuringFormV307),
                            typeof (Mr762OscilloscopeFormV3),
                            typeof (Mr762SystemJournalFormV3),
                            typeof (Mr762AlarmJournalFormV3),
                            typeof (EmulationForm)
                            //typeof (DiagnosticForm)
                        };
                    }
                    return new[]
                    {
                        typeof (BSBGLEFV3),
                        typeof (Mr762ConfigurationFormV300),
                        typeof (Mr762MeasuringFormV300),
                        typeof (Mr762OscilloscopeFormV3),
                        typeof (Mr762SystemJournalFormV3),
                        typeof (Mr762AlarmJournalFormV3),
                        typeof (EmulationForm)
                        //typeof (DiagnosticForm)
                    };
                    
                }
                if (ver >= 3.00 && ver < 3.02)
                {
                    return new[]
                    {
                        typeof (BSBGLEFV3),
                        typeof (Mr762ConfigurationFormV300),
                        typeof (Mr762MeasuringFormV300),
                        typeof (Mr762OscilloscopeFormV3),
                        typeof (Mr762SystemJournalFormV3),
                        typeof (Mr762AlarmJournalFormV3)
                    };
                }
                if (ver >= 2.04)
                {
                    return new[]
                    {
                        typeof (Mr762AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr762ConfigurationFormV204),
                        typeof (Mr762MeasuringFormV201),
                        typeof (Mr762OscilloscopeFormV2),
                        typeof (Mr762SystemJournalFormV2)
                    };
                }

                if (ver >= 2.01 && ver < 2.04)
                {
                    return new[]
                    {
                        typeof (Mr762AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr762ConfigurationFormV201),
                        typeof (Mr762MeasuringFormV201),
                        typeof (Mr762OscilloscopeFormV2),
                        typeof (Mr762SystemJournalFormV2)
                    };
                }

                if (Math.Abs(ver - 2.00) < 0.001)
                {
                    return new[]
                    {
                        typeof (Mr762AlarmJournalFormV2),
                        typeof (BSBGLEF),
                        typeof (Mr762ConfigurationFormV2),
                        typeof (Mr762MeasuringFormV2),
                        typeof (Mr762OscilloscopeFormV2),
                        typeof (Mr762SystemJournalFormV2)
                    };
                }
                return new[]
                {
                    typeof (Mr762AlarmJournalFormV1),
                    typeof (BSBGLEF),
                    typeof (Mr762ConfigurationForm),
                    typeof (Mr762MeasuringForm),
                    typeof (Mr762OscilloscopeFormV1),
                    typeof (Mr762SystemJournalForm),
                };
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                    {
                        "1.00 - 1.01",
                        "1.02 - 1.05",
                        "2.00",
                        "2.01",
                        "2.02",
                        "2.03",
                        "2.04",
                        "2.05",
                        "2.06",
                        "3.00",
                        "3.01",
                        "3.02",
                        "3.03",
                        "3.04",
                        "3.05",
                        "3.06",
                        "3.07",
                        "3.08"
                    };
            }
        }

        public Mr762DeviceV2 Mr762DeviceV2
        {
            get { return _mr762DeviceV2; }
        }
    }
}
