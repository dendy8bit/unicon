﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version2.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора U
    /// </summary>
    public class KanalUTransStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)]
        private ushort _ittx;//конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)]
        private ushort _ittx1; //конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
        [Layout(3)]
        private ushort _polarityL; //резерв, вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)]
        private ushort _polarityX;//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)]
        private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)]
        private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)]
        private ushort _rez; //резерв 
        #endregion [Private fields]

        #region [U (ТН)]

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(0)]
        [XmlIgnore]
        public double Kthl
        {
            get { return ValuesConverterCommon.GetKth(this._ittl); }
            set { this._ittl = ValuesConverterCommon.SetKth(value); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlElement(ElementName = "KTHL")]
        public double KthlValue
        {
            get
            {
                var value = ValuesConverterCommon.GetKth(this._ittl);
                return Common.GetBit(this._ittl, 15)
                           ? value * 1000
                           : value;
            }
            set
            {
                
            }
        }






        /// <summary>
        /// Неисп_L
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Неисп_L")]
        public string LfaultXml
        {
            get { return Validator.Get(this._polarityL,StringsConfig.SwitchSignals); }
            set { this._polarityL = Validator.Set(value,StringsConfig.SwitchSignals) ; }
        }









        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(2)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._ittl, StringsConfig.KthKoefs, 15); }
            set { this._ittl = Validator.Set(value, StringsConfig.KthKoefs, this._ittl, 15); }
        }




        #endregion [U (ТН)] 
    }
}
