﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr762.Version2.Configuration.Structures.Opm
{
    /// <summary>
    /// конфигурациия ОМП
    /// </summary> 
    public class ConfigurationOpmStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _config;			//конфигурация ОМП (выведено, введено)
        [Layout(1)]
        private ushort _xyd;			//удельное индуктивное сопративление линии (число 0-1000) 
        #endregion [Private fields]


        #region [Properties]
       
        /// <summary>
        /// Режим
        /// </summary>
         [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn); }
            set { this._config =Validator.Set(value,StringsConfig.OffOn) ; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Xyd")]
        public double Xyd
        {
            get { return this._xyd / 1000.0; }
            set { this._xyd = (ushort)(value * 1000); }
        }

        #endregion [Properties]


  
     
    }
}
