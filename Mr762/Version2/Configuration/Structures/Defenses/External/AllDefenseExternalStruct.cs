﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version2.Configuration.Structures.Defenses.External
{
    public class AllDefenseExternalStruct : StructBase, IDgvRowsContainer<DefenseExternalStruct>
    {
        [XmlArray(ElementName = "External")]
        [Layout(0, Count =16)]
        private DefenseExternalStruct[] _u; //мтз U>

        [XmlArray(ElementName = "Все")]
        public DefenseExternalStruct[] Rows
        {
            get { return _u; }
            set { _u = value; }
        }
    }
}
