﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr762.Version2.Configuration.Structures.Defenses.U
{
    /// <summary>
    /// конфигурациия основной ступени Umax, Umin
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "конфигурациия_основной_ступени_U")]
    public class DefenceUStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)]
        private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)]
        private ushort _config1; //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)]
        private ushort _block; //вход блокировки
        [Layout(3)]
        private ushort _ust; //уставка срабатывания_
        [Layout(4)]
        private ushort _time; //время срабатывания_
        [Layout(5)]
        private ushort _u; //уставка возврата
        [Layout(6)]
        private ushort _tu; //время возврата
        [Layout(7)]
        private ushort _rez; //резерв 

        #endregion [Private fields]
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ТИп_Umax")]
        public string UmaxModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.UmaxDefenseMode, 5, 6, 7); }
            set { this._config = Validator.Set(value, StringsConfig.UmaxDefenseMode, this._config, 5, 6, 7); }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "ТИп_Umin")]
        public string UminModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.UminDefenseMode, 5, 6, 7); }
            set { this._config = Validator.Set(value, StringsConfig.UminDefenseMode, this._config, 5, 6, 7); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Uпуск")]
        public bool UStartBoolXml
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Блокировка_U_5V")]
        public bool BlockUmin5VXml
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes,this._config1, 4, 5); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Уров")]
        public bool UrovXml
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "АПВ")]
        public bool ApvXml
        {
            get { return Common.GetBit(this._config1, 0); }
            set { this._config1 = Common.SetBit(this._config1, 0, value); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "АВР")]
        public bool AvrXml
        {
            get { return Common.GetBit(this._config1, 1); }
            set { this._config1 = Common.SetBit(this._config1, 1, value); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Апв_возврат")]
        public bool ApvBackXml
        {
            get { return Common.GetBit(this._config1, 2); }
            set { this._config1 = Common.SetBit(this._config1, 2, value); }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool ResetXml
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
    }
}
