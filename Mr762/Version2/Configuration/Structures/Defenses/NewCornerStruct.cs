﻿using System.Xml.Serialization;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version2.Configuration.Structures.Defenses
{
    public class NewCornerStruct : StructBase
    {
        public NewCornerStruct() { }
        public NewCornerStruct(ushort c, ushort cn, ushort c0, ushort c2, ushort cn1)
        {
            C = c;
            Cn = cn;
            C0 = c0;
            C2 = c2;
            Cn1 = cn1;
        }

        /// <summary>
        /// I
        /// </summary>
        [BindingProperty(0), XmlElement(ElementName = "I")]
        public ushort C { get; set; }

        /// <summary>
        /// угол для расчета по I0
        /// </summary>
        [BindingProperty(1), XmlElement(ElementName = "I0")]
        public ushort C0 { get; set; }

        /// <summary>
        /// угол для расчета по In
        /// </summary>
        [BindingProperty(2), XmlElement(ElementName = "In")]
        public ushort Cn { get; set; }

        /// <summary>
        /// угол для расчета по I2
        /// </summary>
        [BindingProperty(3), XmlElement(ElementName = "I2")]
        public ushort C2 { get; set; }

        /// <summary>
        /// угол для расчета по I2
        /// </summary>
        [BindingProperty(4), XmlElement(ElementName = "In1")]
        public ushort Cn1 { get; set; }
    }
}
