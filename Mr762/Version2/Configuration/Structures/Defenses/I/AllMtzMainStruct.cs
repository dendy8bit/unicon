﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version2.Configuration.Structures.Defenses.I
{
    public class AllMtzMainStruct : StructBase, IDgvRowsContainer<MtzMainStruct>
    {
        [Layout(1, Count = 8)] private MtzMainStruct[] _mtzmain;

        [XmlArray(ElementName = "Все")]
        public MtzMainStruct[] Rows
        {
            get { return _mtzmain; }
            set { _mtzmain = value; }
        }
    }
}
