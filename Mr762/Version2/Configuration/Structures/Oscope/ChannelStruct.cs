﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr762.Version2.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
   public class ChannelStruct : StructBase
   {
       [Layout(0)] private ushort _channel;

       [BindingProperty(0)]
       [XmlAttribute(AttributeName = "Канал")]
       public string Channel
       {
           get { return Validator.Get(this._channel, StringsConfig.RelaySignals); }
           set { this._channel = Validator.Set(value, StringsConfig.RelaySignals); }
       }
   }
}
