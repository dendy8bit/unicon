﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version2.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStruct : StructBase 
    {
        #region [Private fields]

        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private OscopeAllChannelsStruct _oscopeAllChannels;
        
        #endregion [Private fields]
        
        #region [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return _oscopeConfig; }
            set { _oscopeConfig = value; }
        }
        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStruct OscopeAllChannels
        {
            get { return _oscopeAllChannels; }
            set { _oscopeAllChannels = value; }
        }
        #endregion [Properties]
    }
}
