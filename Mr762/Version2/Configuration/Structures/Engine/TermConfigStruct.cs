﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr762.Version2.Configuration.Structures.Engine
{
    /// <summary>
    /// Конфигурация тепловой модели
    /// </summary>
    public class TermConfigStruct : StructBase 
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //привязка к стороне 0,1,2 РЕЗЕРВ!!!
        [Layout(1)] private ushort _timeHot; //постоянная времени нагрева
        [Layout(2)] private ushort _timeCold; //постоянная времени охлаждения
        [Layout(3)] private ushort _termN; //номинальный ток
        [Layout(4)] private ushort _iVal; //уставка срабатывания пуска двигателя
        [Layout(5)] private ushort _iWait; //время пуска двигателя
        [Layout(6)] private ushort _hotpusk; //уставка для горячего пуска
        [Layout(7)] private ushort _reset; //вход сброса тепловой модели (теплового состояния)
        [Layout(8)] private ushort _numreset; //вход сброса тепловой модели (числа пусков)
        [Layout(9)] private ushort _tdlit; //время за которое считается число пусков 
       #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// постоянная времени нагрева
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Tгор")]
        public ushort TimeHot
        {
            get { return _timeHot; }
            set { _timeHot = value; }
        }
        /// <summary>
        /// постоянная времени охлаждения
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Tхол")]
        public ushort TimeCold
        {
            get { return _timeCold; }
            set { _timeCold = value; }
        }

        /// <summary>
        /// Iдв
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Iдв")]
        public double Ingine
        {
            get { return ValuesConverterCommon.GetIn(this._termN); }
            set { this._termN = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// Iпуск
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Iпуск")]
        public double Start
        {
            get { return ValuesConverterCommon.GetIn(this._iVal); }
            set { this._iVal = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// Тпуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Тпуск")]
        public int TimeStart
        {
            get { return ValuesConverterCommon.GetWaitTime(this._iWait); }
            set { this._iWait = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Tдлит
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Tдлит")]
        public ushort Tdlit
        {
            get { return _tdlit; }
            set { _tdlit = value; }
        }

        /// <summary>
        /// Qгор
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Qгор")]
        public double Qhot
        {
            get { return ValuesConverterCommon.GetUstavka256(this._hotpusk); }
            set { this._hotpusk = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Q сброс
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Q_сброс")]
        public string QresetXml
        {
            get { return Validator.Get(this._reset,StringsConfig.ExternalDafenseSrab) ; }
            set { this._reset = Validator.Set(value,StringsConfig.ExternalDafenseSrab) ; }
        }
        
        /// <summary>
        /// N сброс
        /// </summary>
        [XmlElement(ElementName = "N_сброс")]
        [BindingProperty(8)]
        public string NresetXml
        {
            get { return Validator.Get(this._numreset,StringsConfig.ExternalDafenseSrab); }
            set { this._numreset = Validator.Set(value,StringsConfig.ExternalDafenseSrab); }
        }

        #endregion [Properties]

    }
}
