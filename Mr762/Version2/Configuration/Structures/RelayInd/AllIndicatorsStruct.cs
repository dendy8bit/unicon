using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr762.Version2.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����������
    /// </summary>
    public class AllIndicatorsStruct : StructBase, IDgvRowsContainer<IndicatorsStruct>
    {
        public const int INDICATORS_COUNT = 12;

        /// <summary>
        /// ����������
        /// </summary>
        [Layout(0, Count = INDICATORS_COUNT)]
        private IndicatorsStruct[] _indicators;

        /// <summary>
        /// ����������
        /// </summary>
        [XmlArray(ElementName = "���_����������")]
      public IndicatorsStruct[] Rows
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }
    }
}