﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version2.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры автоматики(Реле и индикаторы)
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "Реле_и_Индикаторы")]
    public class AutomaticsParametersStruct : StructBase
    {
        #region [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [Layout(0)] private AllReleOutputStruct _relays;

        /// <summary>
        /// индикаторы
        /// </summary>
        [Layout(1)] private AllIndicatorsStruct _indicators;

        /// <summary>
        /// реле неисправность
        /// </summary>
        [Layout(2)] private FaultStruct _fault;

       

        #endregion [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Реле")]
        public AllReleOutputStruct Relays
        {
            get { return _relays; }
            set { _relays = value; }
        }

        /// <summary>
        /// индикаторы
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStruct Indicators
        {
            get { return _indicators; }
            set { _indicators = value; }
        }

        /// <summary>
        /// реле неисправность
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Реле_неисправности")]
        public FaultStruct Fault
        {
            get { return _fault; }
            set { _fault = value; }
        }
    }
}
