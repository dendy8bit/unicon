﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.Export;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr762.Properties;
using BEMN.Mr762.Version2.Configuration.Structures;
using BEMN.Mr762.Version2.Configuration.Structures.Apv;
using BEMN.Mr762.Version2.Configuration.Structures.Avr;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Block;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.External;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.F;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.I;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.I2I1;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Ig;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Istar;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Q;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.U;
using BEMN.Mr762.Version2.Configuration.Structures.Engine;
using BEMN.Mr762.Version2.Configuration.Structures.InputSignals;
using BEMN.Mr762.Version2.Configuration.Structures.Ls;
using BEMN.Mr762.Version2.Configuration.Structures.Lzsh;
using BEMN.Mr762.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr762.Version2.Configuration.Structures.Opm;
using BEMN.Mr762.Version2.Configuration.Structures.Oscope;
using BEMN.Mr762.Version2.Configuration.Structures.RelayInd;
using BEMN.Mr762.Version2.Configuration.Structures.Sihronizm;
using BEMN.Mr762.Version2.Configuration.Structures.Switch;
using BEMN.Mr762.Version2.Configuration.Structures.Vls;

namespace BEMN.Mr762.Version2.Configuration
{
    public partial class Mr762ConfigurationFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string WRITE_OK = "Конфигурация записана";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR762_SET_POINTS";
        private const string ERROR_SETPOINTS_VALUE = "В конфигурации заданы некорректные значения. Проверьте конфигурацию.";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string WRITING = "Идёт запись конфигурации";
        private const string READING = "Идёт чтение конфигурации";
        #endregion [Constants]


        #region [Private fields]
        private RadioButtonSelector _groupSelector;
        private readonly MemoryEntity<ConfigurationStruct> _configuration;
        /// <summary>
        /// Текущие уставки
        /// </summary>
        private ConfigurationStruct _currentSetpointsStruct;
        /// <summary>
        /// Массив контролов для ВЛС
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        /// <summary>
        /// Массив контролов для ЛС
        /// </summary>
        private DataGridView[] _lsBoxes;
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();
        private readonly Mr762.Mr762Device _device; 

        #region [Валидаторы]
        #region [Реле и индикаторы]
        private NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> _releyValidator;
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<FaultStruct> _faultValidator;
        private StructUnion<AutomaticsParametersStruct> _automaticsParametersUnion;
        #endregion [Реле и индикаторы]
        /// <summary>
        /// Выключатель
        /// </summary>
        private NewStructValidator<SwitchStruct> _switchValidator;
        /// <summary>
        /// АПВ
        /// </summary>
        private NewStructValidator<ApvStruct> _apvValidator;
        /// <summary>
        /// АВР
        /// </summary>
        private NewStructValidator<AvrStruct> _avrValidator;
        /// <summary>
        /// ЛЗШ
        /// </summary>
        private NewStructValidator<LpbStruct> _lpbValidator;
        /// <summary>
        /// Двигатель
        /// </summary>
        private NewStructValidator<TermConfigStruct> _termValidator;
        /// <summary>
        /// Входные сигналы
        /// </summary>
        private NewStructValidator<InputSignalStruct> _inputSignalValidator;


        #region [Осц]
        private NewStructValidator<OscopeConfigStruct> _oscopeConfigValidator;
        private NewDgwValidatior<OscopeAllChannelsStruct, ChannelStruct> _channelsValidator;
        private NewStructValidator<ChannelStruct> _inpOscValidator;
        private StructUnion<OscopeStruct> _oscopeUnion;
        #endregion [Осц]


        #region [Конфигурациия_измерительных_трансформаторов]

        private NewStructValidator<KanalITransStruct> _iTransValidator;
        private NewStructValidator<KanalUTransStruct> _uTransValidator;
        private StructUnion<MeasureTransStruct> _measureTransUnion;

        private NewStructValidator<ConfigurationOpmStruct> _opmValidator;
        #endregion [Конфигурациия_измерительных_трансформаторов]


        #region [Входные сигналы]
        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;
        #endregion [Входные сигналы]


        #region [Синхронизм]
        private NewStructValidator<SinhronizmAddStruct> _manualSihronizmValidator;
        private NewStructValidator<SinhronizmAddStruct> _autoSihronizmValidator;

        private NewStructValidator<SinhronizmStruct> _sihronizmValidator;
        #endregion [Синхронизм]


        #region [ВЛС]
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;
        #endregion [ВЛС]


        #region [Защиты]
        /// <summary>
        /// Углы МЧ
        /// </summary>
        private NewStructValidator<CornerStruct> _cornerValidator;
        /// <summary>
        /// I*
        /// </summary>
        private NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct> _iStarValidator;
        /// <summary>
        /// I2/I1
        /// </summary>
        private NewStructValidator<DefenseI2I1Struct> _i2I1Validator;
        /// <summary>
        /// Ig
        /// </summary>
        private NewStructValidator<DefenseIgStruct> _igValidator;
        #region [I]
        /// <summary>
        /// I1-I5
        /// </summary>
        private NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> _i1To5Validator;

        #endregion [I]

        /// <summary>
        /// U
        /// </summary>
        private NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> _uValidator;
        /// <summary>
        /// F
        /// </summary>
        private NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> _fValidator;
        /// <summary>
        /// External
        /// </summary>
        private NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> _externalValidator;
        /// <summary>
        /// Q
        /// </summary>
        private NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> _qValidator;

        private NewStructValidator<DefenseTermBlockStruct> _termBlockValidator;
        private NewStructValidator<DefenseNBlockStruct> _nBlockValidator;

        private StructUnion<DefensesSetpointsStruct> _defensesUnion;
        #endregion [Защиты]

        private SetpointsValidator<AllDefensesSetpointsStruct, DefensesSetpointsStruct> _defensesValidator;
        private StructUnion<ConfigurationStruct> _configurationValidator; 
        #endregion [Валидаторы]

        #endregion [Private fields]


        #region [Ctor's]
        public Mr762ConfigurationFormV2()
        {
            InitializeComponent();
        }

        public Mr762ConfigurationFormV2(Mr762.Mr762Device device)
        {
            InitializeComponent();
            this._device = device;
            
            this._configuration = device.Mr762DeviceV2.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ConfigurationWriteOk);
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                _configuration.RemoveStructQueries();
                ConfigurationReadFail();
            });
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                _configuration.RemoveStructQueries();
                ConfigurationWriteFail();
            });

            this._progressBar.Maximum = this._configuration.Slots.Count;
            this.Init();
        }

        

        /// <summary>
        /// Инициализация
        /// </summary>
        private void Init()
        {
            this._currentSetpointsStruct = new ConfigurationStruct();
            this._lsBoxes = new[]
                {
                    this._inputSignals1,
                    this._inputSignals2,
                    this._inputSignals3,
                    this._inputSignals4,
                    this._inputSignals5,
                    this._inputSignals6,
                    this._inputSignals7,
                    this._inputSignals8,
                    this._inputSignals9,
                    this._inputSignals10,
                    this._inputSignals11,
                    this._inputSignals12,
                    this._inputSignals13,
                    this._inputSignals14,
                    this._inputSignals15,
                    this._inputSignals16
                };


            this._vlsBoxes = new[]
                {
                    VLScheckedListBox1,
                    VLScheckedListBox2,
                    VLScheckedListBox3,
                    VLScheckedListBox4,
                    VLScheckedListBox5,
                    VLScheckedListBox6,
                    VLScheckedListBox7,
                    VLScheckedListBox8,
                    VLScheckedListBox9,
                    VLScheckedListBox10,
                    VLScheckedListBox11,
                    VLScheckedListBox12,
                    VLScheckedListBox13,
                    VLScheckedListBox14,
                    VLScheckedListBox15,
                    VLScheckedListBox16
                };

            #region [Выключатель]
            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._switchTUrov, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchIUrov, RulesContainer.Ustavka40),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed)
                   );  
            #endregion [Выключатель]


            #region [Апв]

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                 new ControlInfoCombo(this._apvModes,StringsConfig.ApvModes),
                 new ControlInfoCombo(this._apvBlocking,StringsConfig.SwitchSignals),
                 new ControlInfoText(this._apvTBlock, RulesContainer.TimeRule),
                 new ControlInfoText(this._apvTReady, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv1Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv2Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv3Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv4Krat, RulesContainer.TimeRule),
                 new ControlInfoCombo(this._apvOff, StringsConfig.BeNo)
                ); 
            #endregion [Апв]


            #region [Авр]

            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                 new ControlInfoCombo(this._avrBySignal, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrBySelfOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByDiff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrSIGNOn, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlocking, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlockClear, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrResolve, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBack, StringsConfig.SwitchSignals),
                 new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                 new ControlInfoCombo(this._avrClear, StringsConfig.ForbiddenAllowed)
                );
            #endregion [Авр]


            #region [ЛЗШ]
            this._lpbValidator = new NewStructValidator<LpbStruct>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this._lzhModes, StringsConfig.LzhModes),
                    new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40)
                    ); 
            #endregion [ЛЗШ]

            
            #region [Двигатель]

            this._termValidator = new NewStructValidator<TermConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineInBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._engineIpConstraintBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._enginePuskTimeBox, RulesContainer.TimeRule),
                new ControlInfoText(this._engineBlockDurationBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineHeatPuskConstraintBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQresetCombo, StringsConfig.ExternalDafenseSrab),
                new ControlInfoCombo(this._engineNpuskCombo, StringsConfig.ExternalDafenseSrab)
                );
      #endregion [Двигатель]


            #region [Конфигурация входных сигналов]

            this._inputSignalValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );
            #endregion [Конфигурация входных сигналов]


            #region [Реле и Индикаторы]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                _outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                _outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoColor()
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            _automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>(_releyValidator, _indicatorValidator, _faultValidator);

            #endregion [Реле и Индикаторы]


            #region [Осц]
            this._oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(_oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscSize)
                     );

            this._channelsValidator = new NewDgwValidatior<OscopeAllChannelsStruct, ChannelStruct>
                (
                this._oscChannels,
                OscopeAllChannelsStruct.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ChannelNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.RelaySignals)
                );

            this._inpOscValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            this._oscopeUnion = new StructUnion<OscopeStruct>(_oscopeConfigValidator, _channelsValidator, _inpOscValidator);

            #endregion [Осц]


            #region [Конфигурациия_измерительных_трансформаторов]

            this._iTransValidator = new NewStructValidator<KanalITransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._TT_typeCombo, StringsConfig.TtType),
                new ControlInfoText(this._Im_Box, RulesContainer.Ustavka40),
                new ControlInfoText(this._ITTL_Box, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX_Box, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX1_Box, RulesContainer.UshortRule)
                );

            this._uTransValidator = new NewStructValidator<KanalUTransStruct>
                (
                this._toolTip,
                new ControlInfoText(this._KTHL_Box, RulesContainer.Ustavka128),       
                new ControlInfoCombo(this._errorL_combo, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._KTHLkoef_combo, StringsConfig.KthKoefs)
                );

            this._measureTransUnion = new StructUnion<MeasureTransStruct>(this._iTransValidator, this._uTransValidator);

            _opmValidator = new NewStructValidator<ConfigurationOpmStruct>
            (
            this._toolTip,
            new ControlInfoCombo(this._OMPmode_combo, StringsConfig.OffOn),
            new ControlInfoText(this._Xline_Box, RulesContainer.DoubleTo1)
            );

            #endregion [Конфигурациия_измерительных_трансформаторов]


            #region [ЛС]
            dataGridsViewLsAND.Add(this._inputSignals1);
            dataGridsViewLsAND.Add(this._inputSignals2);
            dataGridsViewLsAND.Add(this._inputSignals3);
            dataGridsViewLsAND.Add(this._inputSignals4);
            dataGridsViewLsAND.Add(this._inputSignals5);
            dataGridsViewLsAND.Add(this._inputSignals6);
            dataGridsViewLsAND.Add(this._inputSignals7);
            dataGridsViewLsAND.Add(this._inputSignals8);

            dataGridsViewLsOR.Add(this._inputSignals9);
            dataGridsViewLsOR.Add(this._inputSignals10);
            dataGridsViewLsOR.Add(this._inputSignals11);
            dataGridsViewLsOR.Add(this._inputSignals12);
            dataGridsViewLsOR.Add(this._inputSignals13);
            dataGridsViewLsOR.Add(this._inputSignals14);
            dataGridsViewLsOR.Add(this._inputSignals15);
            dataGridsViewLsOR.Add(this._inputSignals16);

            _inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
              (
              this._lsBoxes[i],
             InputLogicStruct.DISCRETS_COUNT,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            _inputLogicUnion = new StructUnion<InputLogicSignalStruct>(_inputLogicValidator);

            #endregion [ЛС]
            

            #region [Синхронизм]

            _manualSihronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualMode, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrManualUmax, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYes, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrManualYesNo, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrManualNoNo, StringsConfig.YesNo),
                new ControlInfoText(this._sinhrManualdF, RulesContainer.DoubleTo01),
                new ControlInfoText(this._sinhrManualdFi, RulesContainer.UshortTo15),
                new ControlInfoText(this._sinhrManualdFno, RulesContainer.DoubleTo04)
                );

            _autoSihronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoMode, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrAutoUmax, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYes, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrAutoYesNo, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrAutoNoNo, StringsConfig.YesNo),
                new ControlInfoText(this._sinhrAutodF, RulesContainer.DoubleTo01),
                new ControlInfoText(this._sinhrAutodFi, RulesContainer.UshortTo15),
                new ControlInfoText(this._sinhrAutodFno, RulesContainer.DoubleTo04)
                );


            _sihronizmValidator = new NewStructValidator<SinhronizmStruct>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOts, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNal, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNal, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwait, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhr, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTon, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2, StringsConfig.Usinhr),
                new ControlInfoValidator(this._manualSihronizmValidator),
                new ControlInfoValidator(this._autoSihronizmValidator)
                );
            #endregion [Синхронизм]


            #region [ВЛС]
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);

            _vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i], StringsConfig.VlsSignals);
            }
            _vlsUnion = new StructUnion<OutputLogicSignalStruct>(_vlsValidator); 
            #endregion [ВЛС]


            #region [защиты]
            //углы
            _cornerValidator = new NewStructValidator<CornerStruct>
                      (
                      this._toolTip,
                      new ControlInfoText(this._iCorner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._i0Corner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._inCorner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._i2Corner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._in1Corner, RulesContainer.UshortTo360)
                      );

            //I*
            _iStarValidator = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
                (
                this._difensesI0DataGrid,
                AllDefenseStarStruct.DEF_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesIStar, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir),
                new ColumnInfoCombo(StringsConfig.I),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesI0DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 
                                    2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12, 13, 14, 15, 16, 17)
                                )
                        }
                };
            
            //I2I1
            _i2I1Validator = new NewStructValidator<DefenseI2I1Struct>
               (
               this._toolTip,
               new ControlInfoCombo(this.I2I1ModeCombo, StringsConfig.DefenseModes),
               new ControlInfoCombo(this.I2I1BlockingCombo, StringsConfig.SwitchSignals),
               new ControlInfoText(this.I2I1TB, RulesContainer.Ustavka100),
                new ControlInfoText(this.I2I1tcp, new CustomIntRule(20,3276700)),
               new ControlInfoCombo(this.I2I1OSCCombo, StringsConfig.OscModes),
               new ControlInfoCheck(this.I2I1UROVCheck),
               new ControlInfoCheck(this.I2I1APVCheck),
               new ControlInfoCheck(this.I2I1AVRCheck)
               );
            //Ig
            _igValidator = new NewStructValidator<DefenseIgStruct>
             (
             this._toolTip,
             new ControlInfoCombo(this.IrModesCombo, StringsConfig.DefenseModes),
             new ControlInfoCombo(this.IrBlockingCombo, StringsConfig.SwitchSignals),
             new ControlInfoText(this.IrUpuskTB, RulesContainer.Ustavka256),
             new ControlInfoText(this.IrIcpTB, RulesContainer.Ustavka40),
             new ControlInfoText(this.IrtcpTB, RulesContainer.TimeRule),
             new ControlInfoText(this.IrtyTB, RulesContainer.TimeRule),
             new ControlInfoCheck(this.IrUpuskCheck),
             new ControlInfoCheck(this.IrtyCheck),
             new ControlInfoCombo(this.IrOSCCombo, StringsConfig.OscModes),
             new ControlInfoCheck(this.IrUROVCheck),
             new ControlInfoCheck(this.IrAPVCheck),
             new ControlInfoCheck(this.IrAVRCheck)
             );




            #region [I]

            _i1To5Validator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                (
                new[] {this._difensesIDataGrid, this._difensesI67DataGrid, this._difensesI8DataGrid},
                new[] {5, 2, 1},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesI, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.I67Modes, ColumnsType.COMBO, false, true, false),
                new ColumnInfoText(RulesContainer.Ustavka256, true, true, false), //4
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCombo(StringsConfig.Direction, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Undir, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Logic),
                new ColumnInfoCombo(StringsConfig.Characteristic, ColumnsType.COMBO, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule, true, true, false),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka100),
                new ColumnInfoCombo(StringsConfig.BeNo),
                new ColumnInfoCombo(StringsConfig.BeNo),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesIDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesI67DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesI8DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                )
                        }
                };  
            #endregion [I]

            //U
            _uValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
                (
                new[] {_difensesUBDataGrid, _difensesUMDataGrid},
                new[] {4, 4},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.UStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true), //2
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoCheck(), //7
                new ColumnInfoCheck(false, true), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesUBDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesUMDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
                                )
                        }
                };

            //F
            _fValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                new[] {_difensesFBDataGrid, _difensesFMDataGrid},
                new[] {4, 4},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.FStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40To60), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka40To60), //6
                new ColumnInfoCheck(), //7
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesFBDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesFMDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                )
                        }
                };

            _externalValidator = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                _externalDifensesDataGrid,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoCheck(), //7
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._externalDifensesDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                )
                        }
                };

            _qValidator = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
                _engineDefensesGrid,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.QStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._engineDefensesGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7)
                                )
                        }
                };

            _termBlockValidator = new NewStructValidator<DefenseTermBlockStruct>
              (
              this._toolTip,
              new ControlInfoCombo(this._engineQmodeCombo, StringsConfig.OffOn),
              new ControlInfoText(this._engineQconstraintBox, RulesContainer.Ustavka256),
              new ControlInfoText(this._engineQtimeBox, RulesContainer.UshortRule)
              );

            _nBlockValidator = new NewStructValidator<DefenseNBlockStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineBlockHeatBox, RulesContainer.UshortTo10),
                new ControlInfoText(this._engineBlockPuskBox, RulesContainer.UshortTo10),
                new ControlInfoText(this._engineBlockTimeBox, RulesContainer.UshortRule)
                );

            _defensesUnion = new StructUnion<DefensesSetpointsStruct>
                (
                _cornerValidator,
                _i1To5Validator,
                _iStarValidator,
                _i2I1Validator,
                _igValidator,
                _uValidator,
                _fValidator,
                _qValidator,
                _termBlockValidator,
                _nBlockValidator,
                _externalValidator
                );

            #endregion [защиты]

            _groupSelector = new RadioButtonSelector(_mainRadioButton, _reserveRadioButton, _groupChangeButton);

            _defensesValidator = new SetpointsValidator<AllDefensesSetpointsStruct, DefensesSetpointsStruct>
                (
                _groupSelector,
                _defensesUnion
                );
            
            _configurationValidator = new StructUnion<ConfigurationStruct>
                (
                _automaticsParametersUnion,
                _switchValidator,
                _apvValidator,
                _avrValidator,
                _lpbValidator,
                _termValidator,
                _inputSignalValidator,
                _oscopeUnion,
                _measureTransUnion,
                _inputLogicUnion,
                _opmValidator,
                _sihronizmValidator,
                _vlsUnion,
                _defensesValidator
                );
        }

        
        #endregion [Ctor's]


        #region [MemoryEntity Events Handlers]
        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            IsProcess = false;
            this._currentSetpointsStruct = this._configuration.Value;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;
            MessageBox.Show(READ_OK);
            ShowConfiguration();
        }
        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            IsProcess = false;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }
        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            IsProcess = false;
            this._statusLabel.Text = WRITE_OK;
            this._progressBar.Value = this._progressBar.Maximum;
            MessageBox.Show(WRITE_OK);
        }
        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            IsProcess = false;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        } 
        #endregion [MemoryEntity Events Handlers]


        #region [Help members]
       
    

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            _configurationValidator.Set(_currentSetpointsStruct);                   
        }

        private bool IsProcess
        {
            set
            {
                _readConfigBut.Enabled = !value;
                _writeConfigBut.Enabled = !value;
                _resetSetpointsButton.Enabled = !value;
                _loadConfigBut.Enabled = !value;
                _saveConfigBut.Enabled = !value;
                _saveToXmlButton.Enabled = !value;
            }
        }


        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;
            if (this._configurationValidator.Check(out message, true))
            {
                this._currentSetpointsStruct = this._configurationValidator.Get();
                return true;
            }
            else
            {
                MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
                return false;
            }
        }

       
       
        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if(!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            IsProcess = true;
            this._statusLabel.Text = READING;
            this._progressBar.Value = 0;
            this._configuration.LoadStruct();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        } 
        
        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}
            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 762 №{0}?", this._device.DeviceNumber), 
                "Запись", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                bool res;
                try
                {
                    res = this.WriteConfiguration();
                }
                catch (Exception ex)
                {
                    if (Validator.DEBUG)
                    {
                        var a = ex.Source;
                        MessageBox.Show(ERROR_SETPOINTS_VALUE);
                    }
                    return;
                }
                if (res)
                {
                    this._statusLabel.Text = WRITING;
                    this.IsProcess = true;
                    this._progressBar.Value = 0;
                    this._configuration.Value = this._currentSetpointsStruct;
                    this._configuration.SaveStruct();
                    this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);
                }
            }
        }
        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr762.Mr762Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr762ConfigurationFormV2); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }
        #endregion [IFormView Members]

        
        #region [Event handlers]
        private void Mr762ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig)
            {
                this.StartRead();
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.WriteConfig();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }         
        }

        
        private void Mr762ConfigurationForm_Shown(object sender, EventArgs e)
        {
            this._configurationValidator.Reset();
        }

        private void Mr762ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration.RemoveStructQueries();
        }

        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = (72874 * 2 / (index + 2)).ToString();
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this._configurationValidator.Reset();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }


        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveToHtml();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void SaveToHtml()
        {
            try
            {
                if (WriteConfiguration())
                {
                    this._currentSetpointsStruct.DeviceVersion = Common.VersionConverter(this._device.DeviceVersion);
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    _statusLabel.Text = HtmlExport.Export(Resources.MR76XMain,
                        Resources.MR76XRes, _currentSetpointsStruct,
                        _currentSetpointsStruct.DeviceType, this._device.DeviceVersion);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
            }
        }
        #endregion [Event handlers]


        #region [Save/Load File Members]
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveInFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void SaveInFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР762_Уставки_версия {0:F1}.xml", this._device.DeviceVersion);
            if (DialogResult.OK == _saveConfigurationDlg.ShowDialog())
            {
                this.WriteConfiguration();
                this.Serialize(_saveConfigurationDlg.FileName);
            } 
        }
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(_openConfigurationDlg.FileName);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            }
        }

        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR762"));
                ushort[] values = this._currentSetpointsStruct.GetValues();
                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                {
                    a = doc.FirstChild.SelectSingleNode(XML_HEAD.Replace("62", "31"));   // заглушка для приемственности старых файлов конфигурации
                    if (a == null) throw new Exception(FILE_LOAD_FAIL);                  // у них в заголовке встречается MR731_SET_POINTS
                }

                byte[] values = Convert.FromBase64String(a.InnerText);
                if (values.Length > this._currentSetpointsStruct.GetSize() - this._currentSetpointsStruct.SystemCfgSize)
                {
                    List<byte> list = new List<byte>();
                    list.AddRange(values.Take(this._currentSetpointsStruct.GetSize()
                        - this._currentSetpointsStruct.SystemCfgSize - this._currentSetpointsStruct.UnderSysSize));
                    list.AddRange(values.Skip(this._currentSetpointsStruct.GetSize() - this._currentSetpointsStruct.UnderSysSize));
                    values = list.ToArray();
                }
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        } 
        #endregion [Save/Load File Members]

        private void Mr762ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configurationValidator.Reset();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        #region [Events CreateTree]
        private void VLScheckedListBox1_16SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _inputSignals1_8CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void _inputSignals9_16CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]{
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});            
        }
        #endregion [Events CreateTree]
    }
}
