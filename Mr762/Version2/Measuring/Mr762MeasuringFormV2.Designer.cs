﻿namespace BEMN.Mr762.Version2.Measuring
{
    partial class Mr762MeasuringFormV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._analogTabPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._nHotTextBox = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this._nStartTextBox = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this._qpTextBox = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._cosfTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._qTextBox = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this._pTextBox = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._ucaTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this._ubcTextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._uabTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._fTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._u0TextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._u2TextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this._u1TextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._ucTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._ubTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._uaTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._in1TextBox = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this._i0TextBox = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this._icTextBox = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this._igTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._i2TextBox = new System.Windows.Forms.TextBox();
            this._inTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._ibTextBox = new System.Windows.Forms.TextBox();
            this._i1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._iaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._discretTabPage = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._q2Led = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._q1Led = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._acceleration = new BEMN.Forms.LedControl();
            this._auto4Led = new System.Windows.Forms.Label();
            this._lzhOn = new BEMN.Forms.LedControl();
            this._auto3Led = new System.Windows.Forms.Label();
            this._urovOn = new BEMN.Forms.LedControl();
            this._auto2Led = new System.Windows.Forms.Label();
            this._avrBlock = new BEMN.Forms.LedControl();
            this._auto1Led = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this._igIoLed = new BEMN.Forms.LedControl();
            this._igLed = new BEMN.Forms.LedControl();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.label307 = new System.Windows.Forms.Label();
            this.label308 = new System.Windows.Forms.Label();
            this._i2i1IoLed = new BEMN.Forms.LedControl();
            this._i2i1Led = new BEMN.Forms.LedControl();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label301 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this._f4IoLessLed = new BEMN.Forms.LedControl();
            this._f3IoLessLed = new BEMN.Forms.LedControl();
            this._f2IoLessLed = new BEMN.Forms.LedControl();
            this._f4LessLed = new BEMN.Forms.LedControl();
            this.label303 = new System.Windows.Forms.Label();
            this._f1IoLessLed = new BEMN.Forms.LedControl();
            this._f3LessLed = new BEMN.Forms.LedControl();
            this.label304 = new System.Windows.Forms.Label();
            this._f2LessLed = new BEMN.Forms.LedControl();
            this.label305 = new System.Windows.Forms.Label();
            this._f1LessLed = new BEMN.Forms.LedControl();
            this.label306 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label295 = new System.Windows.Forms.Label();
            this.label296 = new System.Windows.Forms.Label();
            this._f4IoMoreLed = new BEMN.Forms.LedControl();
            this._f3IoMoreLed = new BEMN.Forms.LedControl();
            this._f2IoMoreLed = new BEMN.Forms.LedControl();
            this._f4MoreLed = new BEMN.Forms.LedControl();
            this.label297 = new System.Windows.Forms.Label();
            this._f1IoMoreLed = new BEMN.Forms.LedControl();
            this._f3MoreLed = new BEMN.Forms.LedControl();
            this.label298 = new System.Windows.Forms.Label();
            this._f2MoreLed = new BEMN.Forms.LedControl();
            this.label299 = new System.Windows.Forms.Label();
            this._f1MoreLed = new BEMN.Forms.LedControl();
            this.label300 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label289 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this._u4IoLessLed = new BEMN.Forms.LedControl();
            this._u3IoLessLed = new BEMN.Forms.LedControl();
            this._u2IoLessLed = new BEMN.Forms.LedControl();
            this._u4LessLed = new BEMN.Forms.LedControl();
            this.label291 = new System.Windows.Forms.Label();
            this._u1IoLessLed = new BEMN.Forms.LedControl();
            this._u3LessLed = new BEMN.Forms.LedControl();
            this.label292 = new System.Windows.Forms.Label();
            this._u2LessLed = new BEMN.Forms.LedControl();
            this.label293 = new System.Windows.Forms.Label();
            this._u1LessLed = new BEMN.Forms.LedControl();
            this.label294 = new System.Windows.Forms.Label();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this._u4IoMoreLed = new BEMN.Forms.LedControl();
            this._u3IoMoreLed = new BEMN.Forms.LedControl();
            this._u2IoMoreLed = new BEMN.Forms.LedControl();
            this._u4MoreLed = new BEMN.Forms.LedControl();
            this.label285 = new System.Windows.Forms.Label();
            this._u1IoMoreLed = new BEMN.Forms.LedControl();
            this._u3MoreLed = new BEMN.Forms.LedControl();
            this.label286 = new System.Windows.Forms.Label();
            this._u2MoreLed = new BEMN.Forms.LedControl();
            this.label287 = new System.Windows.Forms.Label();
            this._u1MoreLed = new BEMN.Forms.LedControl();
            this.label288 = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label229 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this._iS1 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._iS2 = new BEMN.Forms.LedControl();
            this._iS1Io = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._iS3 = new BEMN.Forms.LedControl();
            this._iS2Io = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._iS4 = new BEMN.Forms.LedControl();
            this._iS3Io = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._iS5 = new BEMN.Forms.LedControl();
            this._iS4Io = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._iS6 = new BEMN.Forms.LedControl();
            this._iS5Io = new BEMN.Forms.LedControl();
            this._iS6Io = new BEMN.Forms.LedControl();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._faultOff = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._ground = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._reservedGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label208 = new System.Windows.Forms.Label();
            this._mainGroupOfSetpoints = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this._fault = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label270 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i7Io = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i7 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._i5Io = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._i3Io = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._i1 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl32 = new BEMN.Forms.LedControl();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl31 = new BEMN.Forms.LedControl();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl30 = new BEMN.Forms.LedControl();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl29 = new BEMN.Forms.LedControl();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl28 = new BEMN.Forms.LedControl();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl27 = new BEMN.Forms.LedControl();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl26 = new BEMN.Forms.LedControl();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._faultManageNet = new BEMN.Forms.LedControl();
            this.label98 = new System.Windows.Forms.Label();
            this._faultSwitchOff = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this.label194 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this.label195 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this.label196 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.label201 = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this._faultPass = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultMeasuring = new BEMN.Forms.LedControl();
            this.label204 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indicator11 = new BEMN.Forms.LedControl();
            this.label190 = new System.Windows.Forms.Label();
            this._indicator6 = new BEMN.Forms.LedControl();
            this.label189 = new System.Windows.Forms.Label();
            this._indicator10 = new BEMN.Forms.LedControl();
            this.label179 = new System.Windows.Forms.Label();
            this._indicator9 = new BEMN.Forms.LedControl();
            this.label180 = new System.Windows.Forms.Label();
            this._indicator8 = new BEMN.Forms.LedControl();
            this.label181 = new System.Windows.Forms.Label();
            this._indicator7 = new BEMN.Forms.LedControl();
            this.label182 = new System.Windows.Forms.Label();
            this._indicator12 = new BEMN.Forms.LedControl();
            this.label183 = new System.Windows.Forms.Label();
            this._indicator5 = new BEMN.Forms.LedControl();
            this.label184 = new System.Windows.Forms.Label();
            this._indicator4 = new BEMN.Forms.LedControl();
            this.label185 = new System.Windows.Forms.Label();
            this._indicator3 = new BEMN.Forms.LedControl();
            this.label186 = new System.Windows.Forms.Label();
            this._indicator2 = new BEMN.Forms.LedControl();
            this.label187 = new System.Windows.Forms.Label();
            this._indicator1 = new BEMN.Forms.LedControl();
            this.label188 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._module34 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._module33 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._module18 = new BEMN.Forms.LedControl();
            this._module10 = new BEMN.Forms.LedControl();
            this.label161 = new System.Windows.Forms.Label();
            this._module32 = new BEMN.Forms.LedControl();
            this._module17 = new BEMN.Forms.LedControl();
            this.label138 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this._module31 = new BEMN.Forms.LedControl();
            this.label164 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this._module9 = new BEMN.Forms.LedControl();
            this._module30 = new BEMN.Forms.LedControl();
            this._module16 = new BEMN.Forms.LedControl();
            this.label140 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this._module29 = new BEMN.Forms.LedControl();
            this.label163 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this._module15 = new BEMN.Forms.LedControl();
            this._module28 = new BEMN.Forms.LedControl();
            this._module5 = new BEMN.Forms.LedControl();
            this.label142 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this._module27 = new BEMN.Forms.LedControl();
            this._module8 = new BEMN.Forms.LedControl();
            this.label232 = new System.Windows.Forms.Label();
            this._module14 = new BEMN.Forms.LedControl();
            this._module26 = new BEMN.Forms.LedControl();
            this.label176 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this._module25 = new BEMN.Forms.LedControl();
            this.label128 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._module13 = new BEMN.Forms.LedControl();
            this._module24 = new BEMN.Forms.LedControl();
            this._module1 = new BEMN.Forms.LedControl();
            this.label129 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this._module23 = new BEMN.Forms.LedControl();
            this._module7 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this._module12 = new BEMN.Forms.LedControl();
            this._module22 = new BEMN.Forms.LedControl();
            this.label175 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this._module21 = new BEMN.Forms.LedControl();
            this.label167 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._module11 = new BEMN.Forms.LedControl();
            this._module20 = new BEMN.Forms.LedControl();
            this.label178 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this._module2 = new BEMN.Forms.LedControl();
            this._module19 = new BEMN.Forms.LedControl();
            this.label134 = new System.Windows.Forms.Label();
            this._module6 = new BEMN.Forms.LedControl();
            this.label168 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this._module3 = new BEMN.Forms.LedControl();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this._module4 = new BEMN.Forms.LedControl();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._d40 = new BEMN.Forms.LedControl();
            this._d24 = new BEMN.Forms.LedControl();
            this.label233 = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this._d39 = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this.label234 = new System.Windows.Forms.Label();
            this._d38 = new BEMN.Forms.LedControl();
            this._d23 = new BEMN.Forms.LedControl();
            this.label235 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._d37 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this.label236 = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this._d36 = new BEMN.Forms.LedControl();
            this._d7 = new BEMN.Forms.LedControl();
            this.label271 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this._d35 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this.label272 = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this._d34 = new BEMN.Forms.LedControl();
            this._d1 = new BEMN.Forms.LedControl();
            this.label273 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this._d33 = new BEMN.Forms.LedControl();
            this.label274 = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this._d20 = new BEMN.Forms.LedControl();
            this._d32 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this.label275 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._d31 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this.label276 = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this._d30 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this.label277 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this._d29 = new BEMN.Forms.LedControl();
            this._d5 = new BEMN.Forms.LedControl();
            this.label278 = new System.Windows.Forms.Label();
            this._d18 = new BEMN.Forms.LedControl();
            this._d28 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this.label279 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._d27 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.label280 = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this._d26 = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.label281 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._d25 = new BEMN.Forms.LedControl();
            this.label282 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d16 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this._d9 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this._d13 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this._d12 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._reserveGroupButton = new System.Windows.Forms.Button();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.label224 = new System.Windows.Forms.Label();
            this.label228 = new System.Windows.Forms.Label();
            this._reservedGroupOfSetpointsFromInterface = new BEMN.Forms.LedControl();
            this._mainGroupOfSetpointsFromInterface = new BEMN.Forms.LedControl();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._manageLed2 = new BEMN.Forms.LedControl();
            this._manageLed1 = new BEMN.Forms.LedControl();
            this.startOscopeBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this._resetTermStateButton = new System.Windows.Forms.Button();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label109 = new System.Windows.Forms.Label();
            this._logicState = new BEMN.Forms.LedControl();
            this._dataBaseTabControl.SuspendLayout();
            this._analogTabPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._discretTabPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._analogTabPage);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(936, 612);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // _analogTabPage
            // 
            this._analogTabPage.Controls.Add(this.groupBox3);
            this._analogTabPage.Controls.Add(this.groupBox2);
            this._analogTabPage.Controls.Add(this._dateTimeControl);
            this._analogTabPage.Controls.Add(this.groupBox5);
            this._analogTabPage.Controls.Add(this.groupBox1);
            this._analogTabPage.Location = new System.Drawing.Point(4, 22);
            this._analogTabPage.Name = "_analogTabPage";
            this._analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogTabPage.Size = new System.Drawing.Size(928, 586);
            this._analogTabPage.TabIndex = 0;
            this._analogTabPage.Text = "Аналоговая БД";
            this._analogTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._nHotTextBox);
            this.groupBox3.Controls.Add(this.label99);
            this.groupBox3.Controls.Add(this._nStartTextBox);
            this.groupBox3.Controls.Add(this.label103);
            this.groupBox3.Controls.Add(this._qpTextBox);
            this.groupBox3.Controls.Add(this.label105);
            this.groupBox3.Location = new System.Drawing.Point(155, 238);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(139, 101);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сост. двигателя";
            // 
            // _nHotTextBox
            // 
            this._nHotTextBox.Location = new System.Drawing.Point(54, 71);
            this._nHotTextBox.Name = "_nHotTextBox";
            this._nHotTextBox.ReadOnly = true;
            this._nHotTextBox.Size = new System.Drawing.Size(61, 20);
            this._nHotTextBox.TabIndex = 7;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(10, 74);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(32, 13);
            this.label99.TabIndex = 5;
            this.label99.Text = "Nгор";
            // 
            // _nStartTextBox
            // 
            this._nStartTextBox.Location = new System.Drawing.Point(54, 45);
            this._nStartTextBox.Name = "_nStartTextBox";
            this._nStartTextBox.ReadOnly = true;
            this._nStartTextBox.Size = new System.Drawing.Size(61, 20);
            this._nStartTextBox.TabIndex = 3;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(10, 48);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(38, 13);
            this.label103.TabIndex = 2;
            this.label103.Text = "Nпуск";
            // 
            // _qpTextBox
            // 
            this._qpTextBox.Location = new System.Drawing.Point(54, 19);
            this._qpTextBox.Name = "_qpTextBox";
            this._qpTextBox.ReadOnly = true;
            this._qpTextBox.Size = new System.Drawing.Size(61, 20);
            this._qpTextBox.TabIndex = 1;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(10, 22);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(15, 13);
            this.label105.TabIndex = 0;
            this.label105.Text = "Q";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._cosfTextBox);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._qTextBox);
            this.groupBox2.Controls.Add(this.label95);
            this.groupBox2.Controls.Add(this._pTextBox);
            this.groupBox2.Controls.Add(this.label97);
            this.groupBox2.Location = new System.Drawing.Point(6, 238);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(129, 101);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Мощности";
            // 
            // _cosfTextBox
            // 
            this._cosfTextBox.Location = new System.Drawing.Point(46, 71);
            this._cosfTextBox.Name = "_cosfTextBox";
            this._cosfTextBox.ReadOnly = true;
            this._cosfTextBox.Size = new System.Drawing.Size(61, 20);
            this._cosfTextBox.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "cos f";
            // 
            // _qTextBox
            // 
            this._qTextBox.Location = new System.Drawing.Point(46, 45);
            this._qTextBox.Name = "_qTextBox";
            this._qTextBox.ReadOnly = true;
            this._qTextBox.Size = new System.Drawing.Size(61, 20);
            this._qTextBox.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(10, 48);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(15, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Q";
            // 
            // _pTextBox
            // 
            this._pTextBox.Location = new System.Drawing.Point(46, 19);
            this._pTextBox.Name = "_pTextBox";
            this._pTextBox.ReadOnly = true;
            this._pTextBox.Size = new System.Drawing.Size(61, 20);
            this._pTextBox.TabIndex = 1;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(10, 22);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(14, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "P";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.Location = new System.Drawing.Point(6, 345);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 43;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._ucaTextBox);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this._ubcTextBox);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this._uabTextBox);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this._fTextBox);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this._u0TextBox);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this._u2TextBox);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this._u1TextBox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this._ucTextBox);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this._ubTextBox);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this._uaTextBox);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Location = new System.Drawing.Point(6, 117);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(449, 115);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Напряжения и частота";
            // 
            // _ucaTextBox
            // 
            this._ucaTextBox.Location = new System.Drawing.Point(149, 81);
            this._ucaTextBox.Name = "_ucaTextBox";
            this._ucaTextBox.ReadOnly = true;
            this._ucaTextBox.Size = new System.Drawing.Size(61, 20);
            this._ucaTextBox.TabIndex = 23;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(116, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(27, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Uca";
            // 
            // _ubcTextBox
            // 
            this._ubcTextBox.Location = new System.Drawing.Point(149, 55);
            this._ubcTextBox.Name = "_ubcTextBox";
            this._ubcTextBox.ReadOnly = true;
            this._ubcTextBox.Size = new System.Drawing.Size(61, 20);
            this._ubcTextBox.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(116, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(27, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Ubc";
            // 
            // _uabTextBox
            // 
            this._uabTextBox.Location = new System.Drawing.Point(149, 29);
            this._uabTextBox.Name = "_uabTextBox";
            this._uabTextBox.ReadOnly = true;
            this._uabTextBox.Size = new System.Drawing.Size(61, 20);
            this._uabTextBox.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(116, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(27, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Uab";
            // 
            // _fTextBox
            // 
            this._fTextBox.Location = new System.Drawing.Point(364, 29);
            this._fTextBox.Name = "_fTextBox";
            this._fTextBox.ReadOnly = true;
            this._fTextBox.Size = new System.Drawing.Size(61, 20);
            this._fTextBox.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(337, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "F";
            // 
            // _u0TextBox
            // 
            this._u0TextBox.Location = new System.Drawing.Point(258, 81);
            this._u0TextBox.Name = "_u0TextBox";
            this._u0TextBox.ReadOnly = true;
            this._u0TextBox.Size = new System.Drawing.Size(61, 20);
            this._u0TextBox.TabIndex = 13;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(231, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "U0";
            // 
            // _u2TextBox
            // 
            this._u2TextBox.Location = new System.Drawing.Point(258, 55);
            this._u2TextBox.Name = "_u2TextBox";
            this._u2TextBox.ReadOnly = true;
            this._u2TextBox.Size = new System.Drawing.Size(61, 20);
            this._u2TextBox.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(231, 58);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "U2";
            // 
            // _u1TextBox
            // 
            this._u1TextBox.Location = new System.Drawing.Point(258, 29);
            this._u1TextBox.Name = "_u1TextBox";
            this._u1TextBox.ReadOnly = true;
            this._u1TextBox.Size = new System.Drawing.Size(61, 20);
            this._u1TextBox.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(231, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "U1";
            // 
            // _ucTextBox
            // 
            this._ucTextBox.Location = new System.Drawing.Point(38, 81);
            this._ucTextBox.Name = "_ucTextBox";
            this._ucTextBox.ReadOnly = true;
            this._ucTextBox.Size = new System.Drawing.Size(61, 20);
            this._ucTextBox.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Uc";
            // 
            // _ubTextBox
            // 
            this._ubTextBox.Location = new System.Drawing.Point(38, 55);
            this._ubTextBox.Name = "_ubTextBox";
            this._ubTextBox.ReadOnly = true;
            this._ubTextBox.Size = new System.Drawing.Size(61, 20);
            this._ubTextBox.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ub";
            // 
            // _uaTextBox
            // 
            this._uaTextBox.Location = new System.Drawing.Point(38, 29);
            this._uaTextBox.Name = "_uaTextBox";
            this._uaTextBox.ReadOnly = true;
            this._uaTextBox.Size = new System.Drawing.Size(61, 20);
            this._uaTextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Ua";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._in1TextBox);
            this.groupBox1.Controls.Add(this.label106);
            this.groupBox1.Controls.Add(this._i0TextBox);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this._icTextBox);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this._igTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._i2TextBox);
            this.groupBox1.Controls.Add(this._inTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._ibTextBox);
            this.groupBox1.Controls.Add(this._i1TextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._iaTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Токи";
            // 
            // _in1TextBox
            // 
            this._in1TextBox.Location = new System.Drawing.Point(258, 45);
            this._in1TextBox.Name = "_in1TextBox";
            this._in1TextBox.ReadOnly = true;
            this._in1TextBox.Size = new System.Drawing.Size(61, 20);
            this._in1TextBox.TabIndex = 9;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(230, 48);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(22, 13);
            this.label106.TabIndex = 8;
            this.label106.Text = "In1";
            // 
            // _i0TextBox
            // 
            this._i0TextBox.Location = new System.Drawing.Point(149, 71);
            this._i0TextBox.Name = "_i0TextBox";
            this._i0TextBox.ReadOnly = true;
            this._i0TextBox.Size = new System.Drawing.Size(61, 20);
            this._i0TextBox.TabIndex = 6;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(127, 74);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(16, 13);
            this.label79.TabIndex = 4;
            this.label79.Text = "I0";
            // 
            // _icTextBox
            // 
            this._icTextBox.Location = new System.Drawing.Point(38, 71);
            this._icTextBox.Name = "_icTextBox";
            this._icTextBox.ReadOnly = true;
            this._icTextBox.Size = new System.Drawing.Size(61, 20);
            this._icTextBox.TabIndex = 7;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(16, 74);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(16, 13);
            this.label80.TabIndex = 5;
            this.label80.Text = "Ic";
            // 
            // _igTextBox
            // 
            this._igTextBox.Location = new System.Drawing.Point(258, 71);
            this._igTextBox.Name = "_igTextBox";
            this._igTextBox.ReadOnly = true;
            this._igTextBox.Size = new System.Drawing.Size(61, 20);
            this._igTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Iг";
            // 
            // _i2TextBox
            // 
            this._i2TextBox.Location = new System.Drawing.Point(149, 45);
            this._i2TextBox.Name = "_i2TextBox";
            this._i2TextBox.ReadOnly = true;
            this._i2TextBox.Size = new System.Drawing.Size(61, 20);
            this._i2TextBox.TabIndex = 3;
            // 
            // _inTextBox
            // 
            this._inTextBox.Location = new System.Drawing.Point(258, 19);
            this._inTextBox.Name = "_inTextBox";
            this._inTextBox.ReadOnly = true;
            this._inTextBox.Size = new System.Drawing.Size(61, 20);
            this._inTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "I2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(231, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "In";
            // 
            // _ibTextBox
            // 
            this._ibTextBox.Location = new System.Drawing.Point(38, 45);
            this._ibTextBox.Name = "_ibTextBox";
            this._ibTextBox.ReadOnly = true;
            this._ibTextBox.Size = new System.Drawing.Size(61, 20);
            this._ibTextBox.TabIndex = 3;
            // 
            // _i1TextBox
            // 
            this._i1TextBox.Location = new System.Drawing.Point(149, 19);
            this._i1TextBox.Name = "_i1TextBox";
            this._i1TextBox.ReadOnly = true;
            this._i1TextBox.Size = new System.Drawing.Size(61, 20);
            this._i1TextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ib";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "I1";
            // 
            // _iaTextBox
            // 
            this._iaTextBox.Location = new System.Drawing.Point(38, 19);
            this._iaTextBox.Name = "_iaTextBox";
            this._iaTextBox.ReadOnly = true;
            this._iaTextBox.Size = new System.Drawing.Size(61, 20);
            this._iaTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // _discretTabPage
            // 
            this._discretTabPage.Controls.Add(this.groupBox4);
            this._discretTabPage.Controls.Add(this.groupBox14);
            this._discretTabPage.Controls.Add(this.groupBox34);
            this._discretTabPage.Controls.Add(this.groupBox33);
            this._discretTabPage.Controls.Add(this.groupBox32);
            this._discretTabPage.Controls.Add(this.groupBox31);
            this._discretTabPage.Controls.Add(this.groupBox30);
            this._discretTabPage.Controls.Add(this.groupBox29);
            this._discretTabPage.Controls.Add(this.groupBox26);
            this._discretTabPage.Controls.Add(this.groupBox20);
            this._discretTabPage.Controls.Add(this.groupBox11);
            this._discretTabPage.Controls.Add(this.groupBox21);
            this._discretTabPage.Controls.Add(this.groupBox12);
            this._discretTabPage.Controls.Add(this.groupBox19);
            this._discretTabPage.Controls.Add(this.groupBox18);
            this._discretTabPage.Controls.Add(this.groupBox17);
            this._discretTabPage.Controls.Add(this.groupBox10);
            this._discretTabPage.Controls.Add(this.groupBox9);
            this._discretTabPage.Controls.Add(this.groupBox6);
            this._discretTabPage.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage.Name = "_discretTabPage";
            this._discretTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage.Size = new System.Drawing.Size(928, 586);
            this._discretTabPage.TabIndex = 1;
            this._discretTabPage.Text = "Дискретная БД";
            this._discretTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._q2Led);
            this.groupBox4.Controls.Add(this.label102);
            this.groupBox4.Controls.Add(this._q1Led);
            this.groupBox4.Controls.Add(this.label104);
            this.groupBox4.Location = new System.Drawing.Point(829, 131);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(88, 63);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Защиты Q";
            // 
            // _q2Led
            // 
            this._q2Led.Location = new System.Drawing.Point(6, 38);
            this._q2Led.Name = "_q2Led";
            this._q2Led.Size = new System.Drawing.Size(13, 13);
            this._q2Led.State = BEMN.Forms.LedState.Off;
            this._q2Led.TabIndex = 75;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(25, 38);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(27, 13);
            this.label102.TabIndex = 73;
            this.label102.Text = "Q>>";
            // 
            // _q1Led
            // 
            this._q1Led.Location = new System.Drawing.Point(6, 19);
            this._q1Led.Name = "_q1Led";
            this._q1Led.Size = new System.Drawing.Size(13, 13);
            this._q1Led.State = BEMN.Forms.LedState.Off;
            this._q1Led.TabIndex = 72;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(25, 19);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(21, 13);
            this.label104.TabIndex = 70;
            this.label104.Text = "Q>";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._acceleration);
            this.groupBox14.Controls.Add(this._auto4Led);
            this.groupBox14.Controls.Add(this._lzhOn);
            this.groupBox14.Controls.Add(this._auto3Led);
            this.groupBox14.Controls.Add(this._urovOn);
            this.groupBox14.Controls.Add(this._auto2Led);
            this.groupBox14.Controls.Add(this._avrBlock);
            this.groupBox14.Controls.Add(this._auto1Led);
            this.groupBox14.Location = new System.Drawing.Point(204, 196);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(129, 96);
            this.groupBox14.TabIndex = 24;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Автоматика";
            // 
            // _acceleration
            // 
            this._acceleration.Location = new System.Drawing.Point(6, 76);
            this._acceleration.Name = "_acceleration";
            this._acceleration.Size = new System.Drawing.Size(13, 13);
            this._acceleration.State = BEMN.Forms.LedState.Off;
            this._acceleration.TabIndex = 31;
            // 
            // _auto4Led
            // 
            this._auto4Led.AutoSize = true;
            this._auto4Led.Location = new System.Drawing.Point(25, 76);
            this._auto4Led.Name = "_auto4Led";
            this._auto4Led.Size = new System.Drawing.Size(98, 13);
            this._auto4Led.TabIndex = 30;
            this._auto4Led.Text = "Режим ускорения";
            // 
            // _lzhOn
            // 
            this._lzhOn.Location = new System.Drawing.Point(6, 57);
            this._lzhOn.Name = "_lzhOn";
            this._lzhOn.Size = new System.Drawing.Size(13, 13);
            this._lzhOn.State = BEMN.Forms.LedState.Off;
            this._lzhOn.TabIndex = 29;
            // 
            // _auto3Led
            // 
            this._auto3Led.AutoSize = true;
            this._auto3Led.Location = new System.Drawing.Point(25, 57);
            this._auto3Led.Name = "_auto3Led";
            this._auto3Led.Size = new System.Drawing.Size(70, 13);
            this._auto3Led.TabIndex = 28;
            this._auto3Led.Text = "Работа ЛЗШ";
            // 
            // _urovOn
            // 
            this._urovOn.Location = new System.Drawing.Point(6, 38);
            this._urovOn.Name = "_urovOn";
            this._urovOn.Size = new System.Drawing.Size(13, 13);
            this._urovOn.State = BEMN.Forms.LedState.Off;
            this._urovOn.TabIndex = 27;
            // 
            // _auto2Led
            // 
            this._auto2Led.AutoSize = true;
            this._auto2Led.Location = new System.Drawing.Point(25, 38);
            this._auto2Led.Name = "_auto2Led";
            this._auto2Led.Size = new System.Drawing.Size(76, 13);
            this._auto2Led.TabIndex = 26;
            this._auto2Led.Text = "Работа УРОВ";
            // 
            // _avrBlock
            // 
            this._avrBlock.Location = new System.Drawing.Point(6, 19);
            this._avrBlock.Name = "_avrBlock";
            this._avrBlock.Size = new System.Drawing.Size(13, 13);
            this._avrBlock.State = BEMN.Forms.LedState.Off;
            this._avrBlock.TabIndex = 25;
            // 
            // _auto1Led
            // 
            this._auto1Led.AutoSize = true;
            this._auto1Led.Location = new System.Drawing.Point(25, 19);
            this._auto1Led.Name = "_auto1Led";
            this._auto1Led.Size = new System.Drawing.Size(91, 13);
            this._auto1Led.TabIndex = 24;
            this._auto1Led.Text = "АВР блокирован";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this.label309);
            this.groupBox34.Controls.Add(this.label310);
            this.groupBox34.Controls.Add(this._igIoLed);
            this.groupBox34.Controls.Add(this._igLed);
            this.groupBox34.Location = new System.Drawing.Point(828, 7);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(89, 54);
            this.groupBox34.TabIndex = 23;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Защита Iг";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.ForeColor = System.Drawing.Color.Blue;
            this.label309.Location = new System.Drawing.Point(25, 12);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(32, 13);
            this.label309.TabIndex = 83;
            this.label309.Text = "Сраб";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.ForeColor = System.Drawing.Color.Red;
            this.label310.Location = new System.Drawing.Point(6, 12);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(23, 13);
            this.label310.TabIndex = 82;
            this.label310.Text = "ИО";
            // 
            // _igIoLed
            // 
            this._igIoLed.Location = new System.Drawing.Point(9, 31);
            this._igIoLed.Name = "_igIoLed";
            this._igIoLed.Size = new System.Drawing.Size(13, 13);
            this._igIoLed.State = BEMN.Forms.LedState.Off;
            this._igIoLed.TabIndex = 71;
            // 
            // _igLed
            // 
            this._igLed.Location = new System.Drawing.Point(28, 31);
            this._igLed.Name = "_igLed";
            this._igLed.Size = new System.Drawing.Size(13, 13);
            this._igLed.State = BEMN.Forms.LedState.Off;
            this._igLed.TabIndex = 72;
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this.label307);
            this.groupBox33.Controls.Add(this.label308);
            this.groupBox33.Controls.Add(this._i2i1IoLed);
            this.groupBox33.Controls.Add(this._i2i1Led);
            this.groupBox33.Location = new System.Drawing.Point(829, 67);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(88, 54);
            this.groupBox33.TabIndex = 22;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Защита I2/I1";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.ForeColor = System.Drawing.Color.Blue;
            this.label307.Location = new System.Drawing.Point(25, 12);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(32, 13);
            this.label307.TabIndex = 83;
            this.label307.Text = "Сраб";
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.ForeColor = System.Drawing.Color.Red;
            this.label308.Location = new System.Drawing.Point(6, 12);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(23, 13);
            this.label308.TabIndex = 82;
            this.label308.Text = "ИО";
            // 
            // _i2i1IoLed
            // 
            this._i2i1IoLed.Location = new System.Drawing.Point(9, 31);
            this._i2i1IoLed.Name = "_i2i1IoLed";
            this._i2i1IoLed.Size = new System.Drawing.Size(13, 13);
            this._i2i1IoLed.State = BEMN.Forms.LedState.Off;
            this._i2i1IoLed.TabIndex = 71;
            // 
            // _i2i1Led
            // 
            this._i2i1Led.Location = new System.Drawing.Point(28, 31);
            this._i2i1Led.Name = "_i2i1Led";
            this._i2i1Led.Size = new System.Drawing.Size(13, 13);
            this._i2i1Led.State = BEMN.Forms.LedState.Off;
            this._i2i1Led.TabIndex = 72;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label301);
            this.groupBox32.Controls.Add(this.label302);
            this.groupBox32.Controls.Add(this._f4IoLessLed);
            this.groupBox32.Controls.Add(this._f3IoLessLed);
            this.groupBox32.Controls.Add(this._f2IoLessLed);
            this.groupBox32.Controls.Add(this._f4LessLed);
            this.groupBox32.Controls.Add(this.label303);
            this.groupBox32.Controls.Add(this._f1IoLessLed);
            this.groupBox32.Controls.Add(this._f3LessLed);
            this.groupBox32.Controls.Add(this.label304);
            this.groupBox32.Controls.Add(this._f2LessLed);
            this.groupBox32.Controls.Add(this.label305);
            this.groupBox32.Controls.Add(this._f1LessLed);
            this.groupBox32.Controls.Add(this.label306);
            this.groupBox32.Location = new System.Drawing.Point(836, 393);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(84, 112);
            this.groupBox32.TabIndex = 21;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Защиты F<";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.ForeColor = System.Drawing.Color.Blue;
            this.label301.Location = new System.Drawing.Point(25, 12);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(32, 13);
            this.label301.TabIndex = 83;
            this.label301.Text = "Сраб";
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.ForeColor = System.Drawing.Color.Red;
            this.label302.Location = new System.Drawing.Point(6, 12);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(23, 13);
            this.label302.TabIndex = 82;
            this.label302.Text = "ИО";
            // 
            // _f4IoLessLed
            // 
            this._f4IoLessLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoLessLed.Name = "_f4IoLessLed";
            this._f4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f4IoLessLed.TabIndex = 80;
            // 
            // _f3IoLessLed
            // 
            this._f3IoLessLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoLessLed.Name = "_f3IoLessLed";
            this._f3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f3IoLessLed.TabIndex = 77;
            // 
            // _f2IoLessLed
            // 
            this._f2IoLessLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoLessLed.Name = "_f2IoLessLed";
            this._f2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f2IoLessLed.TabIndex = 74;
            // 
            // _f4LessLed
            // 
            this._f4LessLed.Location = new System.Drawing.Point(28, 88);
            this._f4LessLed.Name = "_f4LessLed";
            this._f4LessLed.Size = new System.Drawing.Size(13, 13);
            this._f4LessLed.State = BEMN.Forms.LedState.Off;
            this._f4LessLed.TabIndex = 81;
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(48, 88);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(25, 13);
            this.label303.TabIndex = 79;
            this.label303.Text = "F<4";
            // 
            // _f1IoLessLed
            // 
            this._f1IoLessLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoLessLed.Name = "_f1IoLessLed";
            this._f1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f1IoLessLed.TabIndex = 71;
            // 
            // _f3LessLed
            // 
            this._f3LessLed.Location = new System.Drawing.Point(28, 69);
            this._f3LessLed.Name = "_f3LessLed";
            this._f3LessLed.Size = new System.Drawing.Size(13, 13);
            this._f3LessLed.State = BEMN.Forms.LedState.Off;
            this._f3LessLed.TabIndex = 78;
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(47, 69);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(25, 13);
            this.label304.TabIndex = 76;
            this.label304.Text = "F<3";
            // 
            // _f2LessLed
            // 
            this._f2LessLed.Location = new System.Drawing.Point(28, 50);
            this._f2LessLed.Name = "_f2LessLed";
            this._f2LessLed.Size = new System.Drawing.Size(13, 13);
            this._f2LessLed.State = BEMN.Forms.LedState.Off;
            this._f2LessLed.TabIndex = 75;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(47, 50);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(25, 13);
            this.label305.TabIndex = 73;
            this.label305.Text = "F<2";
            // 
            // _f1LessLed
            // 
            this._f1LessLed.Location = new System.Drawing.Point(28, 31);
            this._f1LessLed.Name = "_f1LessLed";
            this._f1LessLed.Size = new System.Drawing.Size(13, 13);
            this._f1LessLed.State = BEMN.Forms.LedState.Off;
            this._f1LessLed.TabIndex = 72;
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(47, 31);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(25, 13);
            this.label306.TabIndex = 70;
            this.label306.Text = "F<1";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label295);
            this.groupBox31.Controls.Add(this.label296);
            this.groupBox31.Controls.Add(this._f4IoMoreLed);
            this.groupBox31.Controls.Add(this._f3IoMoreLed);
            this.groupBox31.Controls.Add(this._f2IoMoreLed);
            this.groupBox31.Controls.Add(this._f4MoreLed);
            this.groupBox31.Controls.Add(this.label297);
            this.groupBox31.Controls.Add(this._f1IoMoreLed);
            this.groupBox31.Controls.Add(this._f3MoreLed);
            this.groupBox31.Controls.Add(this.label298);
            this.groupBox31.Controls.Add(this._f2MoreLed);
            this.groupBox31.Controls.Add(this.label299);
            this.groupBox31.Controls.Add(this._f1MoreLed);
            this.groupBox31.Controls.Add(this.label300);
            this.groupBox31.Location = new System.Drawing.Point(731, 393);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(84, 112);
            this.groupBox31.TabIndex = 20;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Защиты F>";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.ForeColor = System.Drawing.Color.Blue;
            this.label295.Location = new System.Drawing.Point(25, 12);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(32, 13);
            this.label295.TabIndex = 83;
            this.label295.Text = "Сраб";
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.ForeColor = System.Drawing.Color.Red;
            this.label296.Location = new System.Drawing.Point(6, 12);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(23, 13);
            this.label296.TabIndex = 82;
            this.label296.Text = "ИО";
            // 
            // _f4IoMoreLed
            // 
            this._f4IoMoreLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoMoreLed.Name = "_f4IoMoreLed";
            this._f4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f4IoMoreLed.TabIndex = 80;
            // 
            // _f3IoMoreLed
            // 
            this._f3IoMoreLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoMoreLed.Name = "_f3IoMoreLed";
            this._f3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f3IoMoreLed.TabIndex = 77;
            // 
            // _f2IoMoreLed
            // 
            this._f2IoMoreLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoMoreLed.Name = "_f2IoMoreLed";
            this._f2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f2IoMoreLed.TabIndex = 74;
            // 
            // _f4MoreLed
            // 
            this._f4MoreLed.Location = new System.Drawing.Point(28, 88);
            this._f4MoreLed.Name = "_f4MoreLed";
            this._f4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4MoreLed.State = BEMN.Forms.LedState.Off;
            this._f4MoreLed.TabIndex = 81;
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(48, 88);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(25, 13);
            this.label297.TabIndex = 79;
            this.label297.Text = "F>4";
            // 
            // _f1IoMoreLed
            // 
            this._f1IoMoreLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoMoreLed.Name = "_f1IoMoreLed";
            this._f1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f1IoMoreLed.TabIndex = 71;
            // 
            // _f3MoreLed
            // 
            this._f3MoreLed.Location = new System.Drawing.Point(28, 69);
            this._f3MoreLed.Name = "_f3MoreLed";
            this._f3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3MoreLed.State = BEMN.Forms.LedState.Off;
            this._f3MoreLed.TabIndex = 78;
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(47, 69);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(25, 13);
            this.label298.TabIndex = 76;
            this.label298.Text = "F>3";
            // 
            // _f2MoreLed
            // 
            this._f2MoreLed.Location = new System.Drawing.Point(28, 50);
            this._f2MoreLed.Name = "_f2MoreLed";
            this._f2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2MoreLed.State = BEMN.Forms.LedState.Off;
            this._f2MoreLed.TabIndex = 75;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(47, 50);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(25, 13);
            this.label299.TabIndex = 73;
            this.label299.Text = "F>2";
            // 
            // _f1MoreLed
            // 
            this._f1MoreLed.Location = new System.Drawing.Point(28, 31);
            this._f1MoreLed.Name = "_f1MoreLed";
            this._f1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1MoreLed.State = BEMN.Forms.LedState.Off;
            this._f1MoreLed.TabIndex = 72;
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(47, 31);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(25, 13);
            this.label300.TabIndex = 70;
            this.label300.Text = "F>1";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label289);
            this.groupBox30.Controls.Add(this.label290);
            this.groupBox30.Controls.Add(this._u4IoLessLed);
            this.groupBox30.Controls.Add(this._u3IoLessLed);
            this.groupBox30.Controls.Add(this._u2IoLessLed);
            this.groupBox30.Controls.Add(this._u4LessLed);
            this.groupBox30.Controls.Add(this.label291);
            this.groupBox30.Controls.Add(this._u1IoLessLed);
            this.groupBox30.Controls.Add(this._u3LessLed);
            this.groupBox30.Controls.Add(this.label292);
            this.groupBox30.Controls.Add(this._u2LessLed);
            this.groupBox30.Controls.Add(this.label293);
            this.groupBox30.Controls.Add(this._u1LessLed);
            this.groupBox30.Controls.Add(this.label294);
            this.groupBox30.Location = new System.Drawing.Point(641, 324);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(84, 112);
            this.groupBox30.TabIndex = 19;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Защиты U<";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.ForeColor = System.Drawing.Color.Blue;
            this.label289.Location = new System.Drawing.Point(25, 12);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(32, 13);
            this.label289.TabIndex = 83;
            this.label289.Text = "Сраб";
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.ForeColor = System.Drawing.Color.Red;
            this.label290.Location = new System.Drawing.Point(6, 12);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(23, 13);
            this.label290.TabIndex = 82;
            this.label290.Text = "ИО";
            // 
            // _u4IoLessLed
            // 
            this._u4IoLessLed.Location = new System.Drawing.Point(9, 88);
            this._u4IoLessLed.Name = "_u4IoLessLed";
            this._u4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u4IoLessLed.TabIndex = 80;
            // 
            // _u3IoLessLed
            // 
            this._u3IoLessLed.Location = new System.Drawing.Point(9, 69);
            this._u3IoLessLed.Name = "_u3IoLessLed";
            this._u3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u3IoLessLed.TabIndex = 77;
            // 
            // _u2IoLessLed
            // 
            this._u2IoLessLed.Location = new System.Drawing.Point(9, 50);
            this._u2IoLessLed.Name = "_u2IoLessLed";
            this._u2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u2IoLessLed.TabIndex = 74;
            // 
            // _u4LessLed
            // 
            this._u4LessLed.Location = new System.Drawing.Point(28, 88);
            this._u4LessLed.Name = "_u4LessLed";
            this._u4LessLed.Size = new System.Drawing.Size(13, 13);
            this._u4LessLed.State = BEMN.Forms.LedState.Off;
            this._u4LessLed.TabIndex = 81;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(48, 88);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(27, 13);
            this.label291.TabIndex = 79;
            this.label291.Text = "U<4";
            // 
            // _u1IoLessLed
            // 
            this._u1IoLessLed.Location = new System.Drawing.Point(9, 31);
            this._u1IoLessLed.Name = "_u1IoLessLed";
            this._u1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u1IoLessLed.TabIndex = 71;
            // 
            // _u3LessLed
            // 
            this._u3LessLed.Location = new System.Drawing.Point(28, 69);
            this._u3LessLed.Name = "_u3LessLed";
            this._u3LessLed.Size = new System.Drawing.Size(13, 13);
            this._u3LessLed.State = BEMN.Forms.LedState.Off;
            this._u3LessLed.TabIndex = 78;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(47, 69);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(27, 13);
            this.label292.TabIndex = 76;
            this.label292.Text = "U<3";
            // 
            // _u2LessLed
            // 
            this._u2LessLed.Location = new System.Drawing.Point(28, 50);
            this._u2LessLed.Name = "_u2LessLed";
            this._u2LessLed.Size = new System.Drawing.Size(13, 13);
            this._u2LessLed.State = BEMN.Forms.LedState.Off;
            this._u2LessLed.TabIndex = 75;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(47, 50);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(27, 13);
            this.label293.TabIndex = 73;
            this.label293.Text = "U<2";
            // 
            // _u1LessLed
            // 
            this._u1LessLed.Location = new System.Drawing.Point(28, 31);
            this._u1LessLed.Name = "_u1LessLed";
            this._u1LessLed.Size = new System.Drawing.Size(13, 13);
            this._u1LessLed.State = BEMN.Forms.LedState.Off;
            this._u1LessLed.TabIndex = 72;
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(47, 31);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(27, 13);
            this.label294.TabIndex = 70;
            this.label294.Text = "U<1";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label283);
            this.groupBox29.Controls.Add(this.label284);
            this.groupBox29.Controls.Add(this._u4IoMoreLed);
            this.groupBox29.Controls.Add(this._u3IoMoreLed);
            this.groupBox29.Controls.Add(this._u2IoMoreLed);
            this.groupBox29.Controls.Add(this._u4MoreLed);
            this.groupBox29.Controls.Add(this.label285);
            this.groupBox29.Controls.Add(this._u1IoMoreLed);
            this.groupBox29.Controls.Add(this._u3MoreLed);
            this.groupBox29.Controls.Add(this.label286);
            this.groupBox29.Controls.Add(this._u2MoreLed);
            this.groupBox29.Controls.Add(this.label287);
            this.groupBox29.Controls.Add(this._u1MoreLed);
            this.groupBox29.Controls.Add(this.label288);
            this.groupBox29.Location = new System.Drawing.Point(641, 206);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(84, 112);
            this.groupBox29.TabIndex = 18;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Защиты U>";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.ForeColor = System.Drawing.Color.Blue;
            this.label283.Location = new System.Drawing.Point(25, 12);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(32, 13);
            this.label283.TabIndex = 83;
            this.label283.Text = "Сраб";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.ForeColor = System.Drawing.Color.Red;
            this.label284.Location = new System.Drawing.Point(6, 12);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(23, 13);
            this.label284.TabIndex = 82;
            this.label284.Text = "ИО";
            // 
            // _u4IoMoreLed
            // 
            this._u4IoMoreLed.Location = new System.Drawing.Point(9, 88);
            this._u4IoMoreLed.Name = "_u4IoMoreLed";
            this._u4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u4IoMoreLed.TabIndex = 80;
            // 
            // _u3IoMoreLed
            // 
            this._u3IoMoreLed.Location = new System.Drawing.Point(9, 69);
            this._u3IoMoreLed.Name = "_u3IoMoreLed";
            this._u3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u3IoMoreLed.TabIndex = 77;
            // 
            // _u2IoMoreLed
            // 
            this._u2IoMoreLed.Location = new System.Drawing.Point(9, 50);
            this._u2IoMoreLed.Name = "_u2IoMoreLed";
            this._u2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u2IoMoreLed.TabIndex = 74;
            // 
            // _u4MoreLed
            // 
            this._u4MoreLed.Location = new System.Drawing.Point(28, 88);
            this._u4MoreLed.Name = "_u4MoreLed";
            this._u4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4MoreLed.State = BEMN.Forms.LedState.Off;
            this._u4MoreLed.TabIndex = 81;
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(48, 88);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(27, 13);
            this.label285.TabIndex = 79;
            this.label285.Text = "U>4";
            // 
            // _u1IoMoreLed
            // 
            this._u1IoMoreLed.Location = new System.Drawing.Point(9, 31);
            this._u1IoMoreLed.Name = "_u1IoMoreLed";
            this._u1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u1IoMoreLed.TabIndex = 71;
            // 
            // _u3MoreLed
            // 
            this._u3MoreLed.Location = new System.Drawing.Point(28, 69);
            this._u3MoreLed.Name = "_u3MoreLed";
            this._u3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3MoreLed.State = BEMN.Forms.LedState.Off;
            this._u3MoreLed.TabIndex = 78;
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(47, 69);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(27, 13);
            this.label286.TabIndex = 76;
            this.label286.Text = "U>3";
            // 
            // _u2MoreLed
            // 
            this._u2MoreLed.Location = new System.Drawing.Point(28, 50);
            this._u2MoreLed.Name = "_u2MoreLed";
            this._u2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2MoreLed.State = BEMN.Forms.LedState.Off;
            this._u2MoreLed.TabIndex = 75;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(47, 50);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(27, 13);
            this.label287.TabIndex = 73;
            this.label287.Text = "U>2";
            // 
            // _u1MoreLed
            // 
            this._u1MoreLed.Location = new System.Drawing.Point(28, 31);
            this._u1MoreLed.Name = "_u1MoreLed";
            this._u1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1MoreLed.State = BEMN.Forms.LedState.Off;
            this._u1MoreLed.TabIndex = 72;
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(47, 31);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(27, 13);
            this.label288.TabIndex = 70;
            this.label288.Text = "U>1";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label229);
            this.groupBox26.Controls.Add(this.label86);
            this.groupBox26.Controls.Add(this._iS1);
            this.groupBox26.Controls.Add(this.label85);
            this.groupBox26.Controls.Add(this._iS2);
            this.groupBox26.Controls.Add(this._iS1Io);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this._iS3);
            this.groupBox26.Controls.Add(this._iS2Io);
            this.groupBox26.Controls.Add(this.label230);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this._iS4);
            this.groupBox26.Controls.Add(this._iS3Io);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this._iS5);
            this.groupBox26.Controls.Add(this._iS4Io);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._iS6);
            this.groupBox26.Controls.Add(this._iS5Io);
            this.groupBox26.Controls.Add(this._iS6Io);
            this.groupBox26.Location = new System.Drawing.Point(734, 6);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(84, 151);
            this.groupBox26.TabIndex = 17;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Защиты I*";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.ForeColor = System.Drawing.Color.Blue;
            this.label229.Location = new System.Drawing.Point(25, 16);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(32, 13);
            this.label229.TabIndex = 67;
            this.label229.Text = "Сраб";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(47, 35);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(26, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "I*>1";
            // 
            // _iS1
            // 
            this._iS1.Location = new System.Drawing.Point(28, 35);
            this._iS1.Name = "_iS1";
            this._iS1.Size = new System.Drawing.Size(13, 13);
            this._iS1.State = BEMN.Forms.LedState.Off;
            this._iS1.TabIndex = 17;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(47, 54);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(26, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "I*>2";
            // 
            // _iS2
            // 
            this._iS2.Location = new System.Drawing.Point(28, 54);
            this._iS2.Name = "_iS2";
            this._iS2.Size = new System.Drawing.Size(13, 13);
            this._iS2.State = BEMN.Forms.LedState.Off;
            this._iS2.TabIndex = 19;
            // 
            // _iS1Io
            // 
            this._iS1Io.Location = new System.Drawing.Point(9, 35);
            this._iS1Io.Name = "_iS1Io";
            this._iS1Io.Size = new System.Drawing.Size(13, 13);
            this._iS1Io.State = BEMN.Forms.LedState.Off;
            this._iS1Io.TabIndex = 17;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(47, 73);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(26, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "I*>3";
            // 
            // _iS3
            // 
            this._iS3.Location = new System.Drawing.Point(28, 73);
            this._iS3.Name = "_iS3";
            this._iS3.Size = new System.Drawing.Size(13, 13);
            this._iS3.State = BEMN.Forms.LedState.Off;
            this._iS3.TabIndex = 21;
            // 
            // _iS2Io
            // 
            this._iS2Io.Location = new System.Drawing.Point(9, 54);
            this._iS2Io.Name = "_iS2Io";
            this._iS2Io.Size = new System.Drawing.Size(13, 13);
            this._iS2Io.State = BEMN.Forms.LedState.Off;
            this._iS2Io.TabIndex = 19;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.ForeColor = System.Drawing.Color.Red;
            this.label230.Location = new System.Drawing.Point(6, 16);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(23, 13);
            this.label230.TabIndex = 66;
            this.label230.Text = "ИО";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(47, 92);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(26, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I*>4";
            // 
            // _iS4
            // 
            this._iS4.Location = new System.Drawing.Point(28, 92);
            this._iS4.Name = "_iS4";
            this._iS4.Size = new System.Drawing.Size(13, 13);
            this._iS4.State = BEMN.Forms.LedState.Off;
            this._iS4.TabIndex = 23;
            // 
            // _iS3Io
            // 
            this._iS3Io.Location = new System.Drawing.Point(9, 73);
            this._iS3Io.Name = "_iS3Io";
            this._iS3Io.Size = new System.Drawing.Size(13, 13);
            this._iS3Io.State = BEMN.Forms.LedState.Off;
            this._iS3Io.TabIndex = 21;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(47, 111);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(26, 13);
            this.label82.TabIndex = 24;
            this.label82.Text = "I*>5";
            // 
            // _iS5
            // 
            this._iS5.Location = new System.Drawing.Point(28, 111);
            this._iS5.Name = "_iS5";
            this._iS5.Size = new System.Drawing.Size(13, 13);
            this._iS5.State = BEMN.Forms.LedState.Off;
            this._iS5.TabIndex = 25;
            // 
            // _iS4Io
            // 
            this._iS4Io.Location = new System.Drawing.Point(9, 92);
            this._iS4Io.Name = "_iS4Io";
            this._iS4Io.Size = new System.Drawing.Size(13, 13);
            this._iS4Io.State = BEMN.Forms.LedState.Off;
            this._iS4Io.TabIndex = 23;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(47, 130);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(26, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "I*>6";
            // 
            // _iS6
            // 
            this._iS6.Location = new System.Drawing.Point(28, 130);
            this._iS6.Name = "_iS6";
            this._iS6.Size = new System.Drawing.Size(13, 13);
            this._iS6.State = BEMN.Forms.LedState.Off;
            this._iS6.TabIndex = 27;
            // 
            // _iS5Io
            // 
            this._iS5Io.Location = new System.Drawing.Point(9, 111);
            this._iS5Io.Name = "_iS5Io";
            this._iS5Io.Size = new System.Drawing.Size(13, 13);
            this._iS5Io.State = BEMN.Forms.LedState.Off;
            this._iS5Io.TabIndex = 25;
            // 
            // _iS6Io
            // 
            this._iS6Io.Location = new System.Drawing.Point(9, 130);
            this._iS6Io.Name = "_iS6Io";
            this._iS6Io.Size = new System.Drawing.Size(13, 13);
            this._iS6Io.State = BEMN.Forms.LedState.Off;
            this._iS6Io.TabIndex = 27;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._faultOff);
            this.groupBox20.Controls.Add(this.label21);
            this.groupBox20.Controls.Add(this._ground);
            this.groupBox20.Controls.Add(this.label22);
            this.groupBox20.Controls.Add(this._reservedGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label208);
            this.groupBox20.Controls.Add(this._mainGroupOfSetpoints);
            this.groupBox20.Controls.Add(this.label209);
            this.groupBox20.Controls.Add(this._fault);
            this.groupBox20.Controls.Add(this.label210);
            this.groupBox20.Location = new System.Drawing.Point(78, 197);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(120, 117);
            this.groupBox20.TabIndex = 15;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Состояние";
            // 
            // _faultOff
            // 
            this._faultOff.Location = new System.Drawing.Point(6, 97);
            this._faultOff.Name = "_faultOff";
            this._faultOff.Size = new System.Drawing.Size(13, 13);
            this._faultOff.State = BEMN.Forms.LedState.Off;
            this._faultOff.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 97);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "Авар. откл";
            // 
            // _ground
            // 
            this._ground.Location = new System.Drawing.Point(6, 78);
            this._ground.Name = "_ground";
            this._ground.Size = new System.Drawing.Size(13, 13);
            this._ground.State = BEMN.Forms.LedState.Off;
            this._ground.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 78);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Земля";
            // 
            // _reservedGroupOfSetpoints
            // 
            this._reservedGroupOfSetpoints.Location = new System.Drawing.Point(6, 59);
            this._reservedGroupOfSetpoints.Name = "_reservedGroupOfSetpoints";
            this._reservedGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._reservedGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._reservedGroupOfSetpoints.TabIndex = 11;
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(25, 59);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(68, 13);
            this.label208.TabIndex = 10;
            this.label208.Text = "Рез. гр. уст.";
            // 
            // _mainGroupOfSetpoints
            // 
            this._mainGroupOfSetpoints.Location = new System.Drawing.Point(6, 40);
            this._mainGroupOfSetpoints.Name = "_mainGroupOfSetpoints";
            this._mainGroupOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._mainGroupOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._mainGroupOfSetpoints.TabIndex = 9;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(25, 40);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(69, 13);
            this.label209.TabIndex = 8;
            this.label209.Text = "Осн. гр. уст.";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 21);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 7;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 21);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 6;
            this.label210.Text = "Неисправность";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i7Io);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i7);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(640, 6);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(84, 193);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I";
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 168);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i7Io
            // 
            this._i7Io.Location = new System.Drawing.Point(9, 149);
            this._i7Io.Name = "_i7Io";
            this._i7Io.Size = new System.Drawing.Size(13, 13);
            this._i7Io.State = BEMN.Forms.LedState.Off;
            this._i7Io.TabIndex = 13;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 168);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I<";
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 130);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i7
            // 
            this._i7.Location = new System.Drawing.Point(28, 149);
            this._i7.Name = "_i7";
            this._i7.Size = new System.Drawing.Size(13, 13);
            this._i7.State = BEMN.Forms.LedState.Off;
            this._i7.TabIndex = 13;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 149);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 13);
            this.label88.TabIndex = 12;
            this.label88.Text = "I>7";
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 111);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 130);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 130);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(22, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I>6";
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 92);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 111);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 111);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I>5";
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 73);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 92);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 92);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(22, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>4";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 54);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 73);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 73);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(22, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>3";
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 35);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 54);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 54);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>2";
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 35);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>1";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl32);
            this.groupBox21.Controls.Add(this.label237);
            this.groupBox21.Controls.Add(this._ssl31);
            this.groupBox21.Controls.Add(this.label238);
            this.groupBox21.Controls.Add(this._ssl30);
            this.groupBox21.Controls.Add(this.label239);
            this.groupBox21.Controls.Add(this._ssl29);
            this.groupBox21.Controls.Add(this.label240);
            this.groupBox21.Controls.Add(this._ssl28);
            this.groupBox21.Controls.Add(this.label241);
            this.groupBox21.Controls.Add(this._ssl27);
            this.groupBox21.Controls.Add(this.label242);
            this.groupBox21.Controls.Add(this._ssl26);
            this.groupBox21.Controls.Add(this.label243);
            this.groupBox21.Controls.Add(this._ssl25);
            this.groupBox21.Controls.Add(this.label244);
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(344, 403);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(286, 175);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(219, 152);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(238, 152);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(219, 133);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(238, 133);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(219, 114);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(238, 114);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(219, 95);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(238, 95);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(219, 76);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(238, 76);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(219, 57);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(238, 57);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(219, 38);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(238, 38);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(219, 19);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(238, 19);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(142, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(161, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(142, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(161, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(142, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(161, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(142, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(161, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(142, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(162, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(142, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(161, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(142, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(161, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(142, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(161, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(71, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(90, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(71, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(90, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(71, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(90, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(71, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(90, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(71, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(90, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(71, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(90, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(71, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(90, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(71, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(90, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(735, 206);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(185, 177);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(96, 152);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(115, 152);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(56, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВНЕШ. 16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(96, 133);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(115, 133);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(56, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВНЕШ. 15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(96, 114);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(115, 114);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(56, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВНЕШ. 14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(96, 95);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(115, 95);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(56, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВНЕШ. 13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(96, 76);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(115, 76);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(56, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВНЕШ. 12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(96, 57);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(115, 57);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(56, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВНЕШ. 11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(96, 38);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(115, 38);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(56, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВНЕШ. 10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(96, 19);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(115, 19);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВНЕШ. 9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(50, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВНЕШ. 8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(50, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВНЕШ. 7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(50, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВНЕШ. 6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(50, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВНЕШ. 5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(50, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВНЕШ. 4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВНЕШ. 3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(50, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВНЕШ. 2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(50, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВНЕШ. 1";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Transparent;
            this.groupBox19.Controls.Add(this._faultManageNet);
            this.groupBox19.Controls.Add(this.label98);
            this.groupBox19.Controls.Add(this._faultSwitchOff);
            this.groupBox19.Controls.Add(this.label96);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this._faultPass);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultMeasuring);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Location = new System.Drawing.Point(9, 332);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(318, 142);
            this.groupBox19.TabIndex = 12;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // _faultManageNet
            // 
            this._faultManageNet.Location = new System.Drawing.Point(120, 19);
            this._faultManageNet.Name = "_faultManageNet";
            this._faultManageNet.Size = new System.Drawing.Size(13, 13);
            this._faultManageNet.State = BEMN.Forms.LedState.Off;
            this._faultManageNet.TabIndex = 33;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(139, 19);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(62, 13);
            this.label98.TabIndex = 32;
            this.label98.Text = "Цепей упр.";
            // 
            // _faultSwitchOff
            // 
            this._faultSwitchOff.Location = new System.Drawing.Point(6, 76);
            this._faultSwitchOff.Name = "_faultSwitchOff";
            this._faultSwitchOff.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff.TabIndex = 31;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(25, 76);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(76, 13);
            this.label96.TabIndex = 30;
            this.label96.Text = "Выключателя";
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(207, 95);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(226, 95);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "ЖА";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(207, 114);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(226, 114);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(48, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осцилл.";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(120, 114);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(139, 114);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(120, 95);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(139, 95);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(120, 76);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(139, 76);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(120, 57);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(139, 57);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(120, 38);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(139, 38);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(207, 76);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(226, 76);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "ЖС";
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(207, 38);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(226, 38);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(207, 19);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(226, 19);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // _faultPass
            // 
            this._faultPass.Location = new System.Drawing.Point(207, 57);
            this._faultPass.Name = "_faultPass";
            this._faultPass.Size = new System.Drawing.Size(13, 13);
            this._faultPass.State = BEMN.Forms.LedState.Off;
            this._faultPass.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(226, 57);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(45, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Пароль";
            // 
            // _faultMeasuring
            // 
            this._faultMeasuring.Location = new System.Drawing.Point(6, 57);
            this._faultMeasuring.Name = "_faultMeasuring";
            this._faultMeasuring.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuring.State = BEMN.Forms.LedState.Off;
            this._faultMeasuring.TabIndex = 5;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(25, 57);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(65, 13);
            this.label204.TabIndex = 4;
            this.label204.Text = "Измерений";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indicator11);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this._indicator6);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this._indicator10);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this._indicator9);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this._indicator8);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this._indicator7);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this._indicator12);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this._indicator5);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this._indicator4);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this._indicator3);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this._indicator2);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this._indicator1);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(65, 254);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // _indicator11
            // 
            this._indicator11.Location = new System.Drawing.Point(6, 209);
            this._indicator11.Name = "_indicator11";
            this._indicator11.Size = new System.Drawing.Size(13, 13);
            this._indicator11.State = BEMN.Forms.LedState.Off;
            this._indicator11.TabIndex = 29;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(25, 209);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // _indicator6
            // 
            this._indicator6.Location = new System.Drawing.Point(6, 114);
            this._indicator6.Name = "_indicator6";
            this._indicator6.Size = new System.Drawing.Size(13, 13);
            this._indicator6.State = BEMN.Forms.LedState.Off;
            this._indicator6.TabIndex = 27;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 114);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // _indicator10
            // 
            this._indicator10.Location = new System.Drawing.Point(6, 190);
            this._indicator10.Name = "_indicator10";
            this._indicator10.Size = new System.Drawing.Size(13, 13);
            this._indicator10.State = BEMN.Forms.LedState.Off;
            this._indicator10.TabIndex = 25;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(25, 190);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // _indicator9
            // 
            this._indicator9.Location = new System.Drawing.Point(6, 171);
            this._indicator9.Name = "_indicator9";
            this._indicator9.Size = new System.Drawing.Size(13, 13);
            this._indicator9.State = BEMN.Forms.LedState.Off;
            this._indicator9.TabIndex = 23;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(25, 171);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // _indicator8
            // 
            this._indicator8.Location = new System.Drawing.Point(6, 152);
            this._indicator8.Name = "_indicator8";
            this._indicator8.Size = new System.Drawing.Size(13, 13);
            this._indicator8.State = BEMN.Forms.LedState.Off;
            this._indicator8.TabIndex = 21;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(25, 152);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // _indicator7
            // 
            this._indicator7.Location = new System.Drawing.Point(6, 133);
            this._indicator7.Name = "_indicator7";
            this._indicator7.Size = new System.Drawing.Size(13, 13);
            this._indicator7.State = BEMN.Forms.LedState.Off;
            this._indicator7.TabIndex = 19;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(25, 133);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // _indicator12
            // 
            this._indicator12.Location = new System.Drawing.Point(6, 228);
            this._indicator12.Name = "_indicator12";
            this._indicator12.Size = new System.Drawing.Size(13, 13);
            this._indicator12.State = BEMN.Forms.LedState.Off;
            this._indicator12.TabIndex = 17;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(25, 228);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // _indicator5
            // 
            this._indicator5.Location = new System.Drawing.Point(6, 95);
            this._indicator5.Name = "_indicator5";
            this._indicator5.Size = new System.Drawing.Size(13, 13);
            this._indicator5.State = BEMN.Forms.LedState.Off;
            this._indicator5.TabIndex = 9;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 95);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // _indicator4
            // 
            this._indicator4.Location = new System.Drawing.Point(6, 76);
            this._indicator4.Name = "_indicator4";
            this._indicator4.Size = new System.Drawing.Size(13, 13);
            this._indicator4.State = BEMN.Forms.LedState.Off;
            this._indicator4.TabIndex = 7;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // _indicator3
            // 
            this._indicator3.Location = new System.Drawing.Point(6, 57);
            this._indicator3.Name = "_indicator3";
            this._indicator3.Size = new System.Drawing.Size(13, 13);
            this._indicator3.State = BEMN.Forms.LedState.Off;
            this._indicator3.TabIndex = 5;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 57);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // _indicator2
            // 
            this._indicator2.Location = new System.Drawing.Point(6, 38);
            this._indicator2.Name = "_indicator2";
            this._indicator2.Size = new System.Drawing.Size(13, 13);
            this._indicator2.State = BEMN.Forms.LedState.Off;
            this._indicator2.TabIndex = 3;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 38);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // _indicator1
            // 
            this._indicator1.Location = new System.Drawing.Point(6, 19);
            this._indicator1.Name = "_indicator1";
            this._indicator1.Size = new System.Drawing.Size(13, 13);
            this._indicator1.State = BEMN.Forms.LedState.Off;
            this._indicator1.TabIndex = 1;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 19);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._module34);
            this.groupBox17.Controls.Add(this.label18);
            this.groupBox17.Controls.Add(this._module33);
            this.groupBox17.Controls.Add(this.label20);
            this.groupBox17.Controls.Add(this._module18);
            this.groupBox17.Controls.Add(this._module10);
            this.groupBox17.Controls.Add(this.label161);
            this.groupBox17.Controls.Add(this._module32);
            this.groupBox17.Controls.Add(this._module17);
            this.groupBox17.Controls.Add(this.label138);
            this.groupBox17.Controls.Add(this.label162);
            this.groupBox17.Controls.Add(this._module31);
            this.groupBox17.Controls.Add(this.label164);
            this.groupBox17.Controls.Add(this.label139);
            this.groupBox17.Controls.Add(this._module9);
            this.groupBox17.Controls.Add(this._module30);
            this.groupBox17.Controls.Add(this._module16);
            this.groupBox17.Controls.Add(this.label140);
            this.groupBox17.Controls.Add(this.label165);
            this.groupBox17.Controls.Add(this._module29);
            this.groupBox17.Controls.Add(this.label163);
            this.groupBox17.Controls.Add(this.label141);
            this.groupBox17.Controls.Add(this._module15);
            this.groupBox17.Controls.Add(this._module28);
            this.groupBox17.Controls.Add(this._module5);
            this.groupBox17.Controls.Add(this.label142);
            this.groupBox17.Controls.Add(this.label169);
            this.groupBox17.Controls.Add(this._module27);
            this.groupBox17.Controls.Add(this._module8);
            this.groupBox17.Controls.Add(this.label232);
            this.groupBox17.Controls.Add(this._module14);
            this.groupBox17.Controls.Add(this._module26);
            this.groupBox17.Controls.Add(this.label176);
            this.groupBox17.Controls.Add(this.label127);
            this.groupBox17.Controls.Add(this.label170);
            this.groupBox17.Controls.Add(this._module25);
            this.groupBox17.Controls.Add(this.label128);
            this.groupBox17.Controls.Add(this.label166);
            this.groupBox17.Controls.Add(this._module13);
            this.groupBox17.Controls.Add(this._module24);
            this.groupBox17.Controls.Add(this._module1);
            this.groupBox17.Controls.Add(this.label129);
            this.groupBox17.Controls.Add(this.label171);
            this.groupBox17.Controls.Add(this._module23);
            this.groupBox17.Controls.Add(this._module7);
            this.groupBox17.Controls.Add(this.label130);
            this.groupBox17.Controls.Add(this._module12);
            this.groupBox17.Controls.Add(this._module22);
            this.groupBox17.Controls.Add(this.label175);
            this.groupBox17.Controls.Add(this.label131);
            this.groupBox17.Controls.Add(this.label177);
            this.groupBox17.Controls.Add(this._module21);
            this.groupBox17.Controls.Add(this.label167);
            this.groupBox17.Controls.Add(this.label132);
            this.groupBox17.Controls.Add(this._module11);
            this.groupBox17.Controls.Add(this._module20);
            this.groupBox17.Controls.Add(this.label178);
            this.groupBox17.Controls.Add(this.label133);
            this.groupBox17.Controls.Add(this._module2);
            this.groupBox17.Controls.Add(this._module19);
            this.groupBox17.Controls.Add(this.label134);
            this.groupBox17.Controls.Add(this._module6);
            this.groupBox17.Controls.Add(this.label168);
            this.groupBox17.Controls.Add(this.label174);
            this.groupBox17.Controls.Add(this._module3);
            this.groupBox17.Controls.Add(this.label173);
            this.groupBox17.Controls.Add(this.label172);
            this.groupBox17.Controls.Add(this._module4);
            this.groupBox17.Location = new System.Drawing.Point(77, 6);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(258, 179);
            this.groupBox17.TabIndex = 10;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Реле";
            // 
            // _module34
            // 
            this._module34.Location = new System.Drawing.Point(212, 38);
            this._module34.Name = "_module34";
            this._module34.Size = new System.Drawing.Size(13, 13);
            this._module34.State = BEMN.Forms.LedState.Off;
            this._module34.TabIndex = 31;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(231, 38);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "34";
            // 
            // _module33
            // 
            this._module33.Location = new System.Drawing.Point(212, 19);
            this._module33.Name = "_module33";
            this._module33.Size = new System.Drawing.Size(13, 13);
            this._module33.State = BEMN.Forms.LedState.Off;
            this._module33.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(231, 19);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(19, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "33";
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(124, 38);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 15;
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(80, 38);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 25;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(143, 38);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(19, 13);
            this.label161.TabIndex = 14;
            this.label161.Text = "18";
            // 
            // _module32
            // 
            this._module32.Location = new System.Drawing.Point(168, 152);
            this._module32.Name = "_module32";
            this._module32.Size = new System.Drawing.Size(13, 13);
            this._module32.State = BEMN.Forms.LedState.Off;
            this._module32.TabIndex = 27;
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(124, 19);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 13;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(187, 152);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(19, 13);
            this.label138.TabIndex = 26;
            this.label138.Text = "32";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(143, 19);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 13);
            this.label162.TabIndex = 12;
            this.label162.Text = "17";
            // 
            // _module31
            // 
            this._module31.Location = new System.Drawing.Point(168, 133);
            this._module31.Name = "_module31";
            this._module31.Size = new System.Drawing.Size(13, 13);
            this._module31.State = BEMN.Forms.LedState.Off;
            this._module31.TabIndex = 25;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(99, 38);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(19, 13);
            this.label164.TabIndex = 24;
            this.label164.Text = "10";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(187, 133);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(19, 13);
            this.label139.TabIndex = 24;
            this.label139.Text = "31";
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(80, 19);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 23;
            // 
            // _module30
            // 
            this._module30.Location = new System.Drawing.Point(168, 114);
            this._module30.Name = "_module30";
            this._module30.Size = new System.Drawing.Size(13, 13);
            this._module30.State = BEMN.Forms.LedState.Off;
            this._module30.TabIndex = 23;
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(80, 152);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 11;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(187, 114);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(19, 13);
            this.label140.TabIndex = 22;
            this.label140.Text = "30";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(99, 19);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 13);
            this.label165.TabIndex = 22;
            this.label165.Text = "9";
            // 
            // _module29
            // 
            this._module29.Location = new System.Drawing.Point(168, 95);
            this._module29.Name = "_module29";
            this._module29.Size = new System.Drawing.Size(13, 13);
            this._module29.State = BEMN.Forms.LedState.Off;
            this._module29.TabIndex = 21;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(99, 152);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(19, 13);
            this.label163.TabIndex = 10;
            this.label163.Text = "16";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(187, 95);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(19, 13);
            this.label141.TabIndex = 20;
            this.label141.Text = "29";
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(80, 133);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 9;
            // 
            // _module28
            // 
            this._module28.Location = new System.Drawing.Point(168, 76);
            this._module28.Name = "_module28";
            this._module28.Size = new System.Drawing.Size(13, 13);
            this._module28.State = BEMN.Forms.LedState.Off;
            this._module28.TabIndex = 19;
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(6, 95);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 9;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(187, 76);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(19, 13);
            this.label142.TabIndex = 18;
            this.label142.Text = "28";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(99, 133);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(19, 13);
            this.label169.TabIndex = 8;
            this.label169.Text = "15";
            // 
            // _module27
            // 
            this._module27.Location = new System.Drawing.Point(168, 57);
            this._module27.Name = "_module27";
            this._module27.Size = new System.Drawing.Size(13, 13);
            this._module27.State = BEMN.Forms.LedState.Off;
            this._module27.TabIndex = 17;
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(6, 152);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 21;
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(187, 57);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(19, 13);
            this.label232.TabIndex = 16;
            this.label232.Text = "27";
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(80, 114);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 7;
            // 
            // _module26
            // 
            this._module26.Location = new System.Drawing.Point(168, 38);
            this._module26.Name = "_module26";
            this._module26.Size = new System.Drawing.Size(13, 13);
            this._module26.State = BEMN.Forms.LedState.Off;
            this._module26.TabIndex = 15;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(25, 19);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(38, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "1(Вкл)";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(187, 38);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(19, 13);
            this.label127.TabIndex = 14;
            this.label127.Text = "26";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(99, 114);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(19, 13);
            this.label170.TabIndex = 6;
            this.label170.Text = "14";
            // 
            // _module25
            // 
            this._module25.Location = new System.Drawing.Point(168, 19);
            this._module25.Name = "_module25";
            this._module25.Size = new System.Drawing.Size(13, 13);
            this._module25.State = BEMN.Forms.LedState.Off;
            this._module25.TabIndex = 13;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(187, 19);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(19, 13);
            this.label128.TabIndex = 12;
            this.label128.Text = "25";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(25, 152);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(13, 13);
            this.label166.TabIndex = 20;
            this.label166.Text = "8";
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(80, 95);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 5;
            // 
            // _module24
            // 
            this._module24.Location = new System.Drawing.Point(124, 152);
            this._module24.Name = "_module24";
            this._module24.Size = new System.Drawing.Size(13, 13);
            this._module24.State = BEMN.Forms.LedState.Off;
            this._module24.TabIndex = 11;
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(6, 19);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 1;
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(143, 152);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(19, 13);
            this.label129.TabIndex = 10;
            this.label129.Text = "24";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(99, 95);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(19, 13);
            this.label171.TabIndex = 4;
            this.label171.Text = "13";
            // 
            // _module23
            // 
            this._module23.Location = new System.Drawing.Point(124, 133);
            this._module23.Name = "_module23";
            this._module23.Size = new System.Drawing.Size(13, 13);
            this._module23.State = BEMN.Forms.LedState.Off;
            this._module23.TabIndex = 9;
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(6, 133);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 19;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(143, 133);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(19, 13);
            this.label130.TabIndex = 8;
            this.label130.Text = "23";
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(80, 76);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 3;
            // 
            // _module22
            // 
            this._module22.Location = new System.Drawing.Point(124, 114);
            this._module22.Name = "_module22";
            this._module22.Size = new System.Drawing.Size(13, 13);
            this._module22.State = BEMN.Forms.LedState.Off;
            this._module22.TabIndex = 7;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(25, 38);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(44, 13);
            this.label175.TabIndex = 2;
            this.label175.Text = "2(Откл)";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(143, 114);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(19, 13);
            this.label131.TabIndex = 6;
            this.label131.Text = "22";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(99, 76);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 2;
            this.label177.Text = "12";
            // 
            // _module21
            // 
            this._module21.Location = new System.Drawing.Point(124, 95);
            this._module21.Name = "_module21";
            this._module21.Size = new System.Drawing.Size(13, 13);
            this._module21.State = BEMN.Forms.LedState.Off;
            this._module21.TabIndex = 5;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(25, 133);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(13, 13);
            this.label167.TabIndex = 18;
            this.label167.Text = "7";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(143, 95);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(19, 13);
            this.label132.TabIndex = 4;
            this.label132.Text = "21";
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(80, 57);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 1;
            // 
            // _module20
            // 
            this._module20.Location = new System.Drawing.Point(124, 76);
            this._module20.Name = "_module20";
            this._module20.Size = new System.Drawing.Size(13, 13);
            this._module20.State = BEMN.Forms.LedState.Off;
            this._module20.TabIndex = 3;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(99, 57);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "11";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(143, 76);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(19, 13);
            this.label133.TabIndex = 2;
            this.label133.Text = "20";
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(6, 38);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 3;
            // 
            // _module19
            // 
            this._module19.Location = new System.Drawing.Point(124, 57);
            this._module19.Name = "_module19";
            this._module19.Size = new System.Drawing.Size(13, 13);
            this._module19.State = BEMN.Forms.LedState.Off;
            this._module19.TabIndex = 1;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(143, 57);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(19, 13);
            this.label134.TabIndex = 0;
            this.label134.Text = "19";
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(6, 114);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 17;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(25, 114);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(13, 13);
            this.label168.TabIndex = 16;
            this.label168.Text = "6";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(25, 57);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(13, 13);
            this.label174.TabIndex = 4;
            this.label174.Text = "3";
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(6, 57);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 5;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(25, 76);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 6;
            this.label173.Text = "4";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(25, 95);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(13, 13);
            this.label172.TabIndex = 8;
            this.label172.Text = "5";
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(6, 76);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 7;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(489, 206);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 166);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 166);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 147);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 147);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 128);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 128);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 109);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 109);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 90);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 90);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 71);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 71);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 52);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 33);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 33);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(342, 206);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 166);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 166);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 147);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 147);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 128);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 128);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 109);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 109);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 90);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 71);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 52);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 52);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 33);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 33);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._d40);
            this.groupBox6.Controls.Add(this._d24);
            this.groupBox6.Controls.Add(this.label233);
            this.groupBox6.Controls.Add(this._d8);
            this.groupBox6.Controls.Add(this._d39);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.label234);
            this.groupBox6.Controls.Add(this._d38);
            this.groupBox6.Controls.Add(this._d23);
            this.groupBox6.Controls.Add(this.label235);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this._d37);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.label236);
            this.groupBox6.Controls.Add(this._d22);
            this.groupBox6.Controls.Add(this._d36);
            this.groupBox6.Controls.Add(this._d7);
            this.groupBox6.Controls.Add(this.label271);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Controls.Add(this._d35);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label272);
            this.groupBox6.Controls.Add(this._d21);
            this.groupBox6.Controls.Add(this._d34);
            this.groupBox6.Controls.Add(this._d1);
            this.groupBox6.Controls.Add(this.label273);
            this.groupBox6.Controls.Add(this.label42);
            this.groupBox6.Controls.Add(this._d33);
            this.groupBox6.Controls.Add(this.label274);
            this.groupBox6.Controls.Add(this._d6);
            this.groupBox6.Controls.Add(this._d20);
            this.groupBox6.Controls.Add(this._d32);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label275);
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this._d31);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label276);
            this.groupBox6.Controls.Add(this._d19);
            this.groupBox6.Controls.Add(this._d30);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.label277);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this._d29);
            this.groupBox6.Controls.Add(this._d5);
            this.groupBox6.Controls.Add(this.label278);
            this.groupBox6.Controls.Add(this._d18);
            this.groupBox6.Controls.Add(this._d28);
            this.groupBox6.Controls.Add(this._d2);
            this.groupBox6.Controls.Add(this.label279);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this._d27);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label280);
            this.groupBox6.Controls.Add(this._d17);
            this.groupBox6.Controls.Add(this._d26);
            this.groupBox6.Controls.Add(this.label46);
            this.groupBox6.Controls.Add(this.label281);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this._d25);
            this.groupBox6.Controls.Add(this.label282);
            this.groupBox6.Controls.Add(this._d4);
            this.groupBox6.Controls.Add(this._d16);
            this.groupBox6.Controls.Add(this._d3);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this._d15);
            this.groupBox6.Controls.Add(this._d9);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this._d14);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this._d10);
            this.groupBox6.Controls.Add(this._d13);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this._d11);
            this.groupBox6.Controls.Add(this._d12);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Location = new System.Drawing.Point(341, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(264, 179);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискретные входы";
            // 
            // _d40
            // 
            this._d40.Location = new System.Drawing.Point(212, 152);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(13, 13);
            this._d40.State = BEMN.Forms.LedState.Off;
            this._d40.TabIndex = 31;
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(106, 152);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(231, 152);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(28, 13);
            this.label233.TabIndex = 30;
            this.label233.Text = "Д40";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // _d39
            // 
            this._d39.Location = new System.Drawing.Point(212, 133);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(13, 13);
            this._d39.State = BEMN.Forms.LedState.Off;
            this._d39.TabIndex = 29;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(125, 152);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "Д24";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(231, 133);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(28, 13);
            this.label234.TabIndex = 28;
            this.label234.Text = "Д39";
            // 
            // _d38
            // 
            this._d38.Location = new System.Drawing.Point(212, 114);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(13, 13);
            this._d38.State = BEMN.Forms.LedState.Off;
            this._d38.TabIndex = 27;
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(106, 133);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(231, 114);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(28, 13);
            this.label235.TabIndex = 26;
            this.label235.Text = "Д38";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(25, 152);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 14;
            this.label30.Text = "Д8";
            // 
            // _d37
            // 
            this._d37.Location = new System.Drawing.Point(212, 95);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(13, 13);
            this._d37.State = BEMN.Forms.LedState.Off;
            this._d37.TabIndex = 25;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(125, 133);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(28, 13);
            this.label40.TabIndex = 28;
            this.label40.Text = "Д23";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(231, 95);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(28, 13);
            this.label236.TabIndex = 24;
            this.label236.Text = "Д37";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(106, 114);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // _d36
            // 
            this._d36.Location = new System.Drawing.Point(212, 76);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(13, 13);
            this._d36.State = BEMN.Forms.LedState.Off;
            this._d36.TabIndex = 23;
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // label271
            // 
            this.label271.AutoSize = true;
            this.label271.Location = new System.Drawing.Point(231, 76);
            this.label271.Name = "label271";
            this.label271.Size = new System.Drawing.Size(28, 13);
            this.label271.TabIndex = 22;
            this.label271.Text = "Д36";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(125, 114);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 26;
            this.label41.Text = "Д22";
            // 
            // _d35
            // 
            this._d35.Location = new System.Drawing.Point(212, 57);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(13, 13);
            this._d35.State = BEMN.Forms.LedState.Off;
            this._d35.TabIndex = 21;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 133);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Д7";
            // 
            // label272
            // 
            this.label272.AutoSize = true;
            this.label272.Location = new System.Drawing.Point(231, 57);
            this.label272.Name = "label272";
            this.label272.Size = new System.Drawing.Size(28, 13);
            this.label272.TabIndex = 20;
            this.label272.Text = "Д35";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(106, 95);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // _d34
            // 
            this._d34.Location = new System.Drawing.Point(212, 38);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(13, 13);
            this._d34.State = BEMN.Forms.LedState.Off;
            this._d34.TabIndex = 19;
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // label273
            // 
            this.label273.AutoSize = true;
            this.label273.Location = new System.Drawing.Point(231, 38);
            this.label273.Name = "label273";
            this.label273.Size = new System.Drawing.Size(28, 13);
            this.label273.TabIndex = 18;
            this.label273.Text = "Д34";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(125, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Д21";
            // 
            // _d33
            // 
            this._d33.Location = new System.Drawing.Point(212, 19);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(13, 13);
            this._d33.State = BEMN.Forms.LedState.Off;
            this._d33.TabIndex = 17;
            // 
            // label274
            // 
            this.label274.AutoSize = true;
            this.label274.Location = new System.Drawing.Point(231, 19);
            this.label274.Name = "label274";
            this.label274.Size = new System.Drawing.Size(28, 13);
            this.label274.TabIndex = 16;
            this.label274.Text = "Д33";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(106, 76);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // _d32
            // 
            this._d32.Location = new System.Drawing.Point(159, 152);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(13, 13);
            this._d32.State = BEMN.Forms.LedState.Off;
            this._d32.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Д1";
            // 
            // label275
            // 
            this.label275.AutoSize = true;
            this.label275.Location = new System.Drawing.Point(178, 152);
            this.label275.Name = "label275";
            this.label275.Size = new System.Drawing.Size(28, 13);
            this.label275.TabIndex = 14;
            this.label275.Text = "Д32";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(125, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Д20";
            // 
            // _d31
            // 
            this._d31.Location = new System.Drawing.Point(159, 133);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(13, 13);
            this._d31.State = BEMN.Forms.LedState.Off;
            this._d31.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(25, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Д6";
            // 
            // label276
            // 
            this.label276.AutoSize = true;
            this.label276.Location = new System.Drawing.Point(178, 133);
            this.label276.Name = "label276";
            this.label276.Size = new System.Drawing.Size(28, 13);
            this.label276.TabIndex = 12;
            this.label276.Text = "Д31";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(106, 57);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // _d30
            // 
            this._d30.Location = new System.Drawing.Point(159, 114);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(13, 13);
            this._d30.State = BEMN.Forms.LedState.Off;
            this._d30.TabIndex = 11;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Д2";
            // 
            // label277
            // 
            this.label277.AutoSize = true;
            this.label277.Location = new System.Drawing.Point(178, 114);
            this.label277.Name = "label277";
            this.label277.Size = new System.Drawing.Size(28, 13);
            this.label277.TabIndex = 10;
            this.label277.Text = "Д30";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(125, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 20;
            this.label44.Text = "Д19";
            // 
            // _d29
            // 
            this._d29.Location = new System.Drawing.Point(159, 95);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(13, 13);
            this._d29.State = BEMN.Forms.LedState.Off;
            this._d29.TabIndex = 9;
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // label278
            // 
            this.label278.AutoSize = true;
            this.label278.Location = new System.Drawing.Point(178, 95);
            this.label278.Name = "label278";
            this.label278.Size = new System.Drawing.Size(28, 13);
            this.label278.TabIndex = 8;
            this.label278.Text = "Д29";
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(106, 38);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // _d28
            // 
            this._d28.Location = new System.Drawing.Point(159, 76);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(13, 13);
            this._d28.State = BEMN.Forms.LedState.Off;
            this._d28.TabIndex = 7;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // label279
            // 
            this.label279.AutoSize = true;
            this.label279.Location = new System.Drawing.Point(178, 76);
            this.label279.Name = "label279";
            this.label279.Size = new System.Drawing.Size(28, 13);
            this.label279.TabIndex = 6;
            this.label279.Text = "Д28";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(125, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 18;
            this.label45.Text = "Д18";
            // 
            // _d27
            // 
            this._d27.Location = new System.Drawing.Point(159, 57);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(13, 13);
            this._d27.State = BEMN.Forms.LedState.Off;
            this._d27.TabIndex = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Д5";
            // 
            // label280
            // 
            this.label280.AutoSize = true;
            this.label280.Location = new System.Drawing.Point(178, 57);
            this.label280.Name = "label280";
            this.label280.Size = new System.Drawing.Size(28, 13);
            this.label280.TabIndex = 4;
            this.label280.Text = "Д27";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(106, 19);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // _d26
            // 
            this._d26.Location = new System.Drawing.Point(159, 38);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(13, 13);
            this._d26.State = BEMN.Forms.LedState.Off;
            this._d26.TabIndex = 3;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(125, 19);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 16;
            this.label46.Text = "Д17";
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(178, 38);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(28, 13);
            this.label281.TabIndex = 2;
            this.label281.Text = "Д26";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(25, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Д3";
            // 
            // _d25
            // 
            this._d25.Location = new System.Drawing.Point(159, 19);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(13, 13);
            this._d25.State = BEMN.Forms.LedState.Off;
            this._d25.TabIndex = 1;
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(178, 19);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(28, 13);
            this.label282.TabIndex = 0;
            this.label282.Text = "Д25";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(53, 152);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(72, 152);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Д16";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 76);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Д4";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(53, 133);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(53, 19);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(72, 133);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "Д15";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(72, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Д9";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(53, 114);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(72, 38);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 2;
            this.label37.Text = "Д10";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(72, 114);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(28, 13);
            this.label33.TabIndex = 10;
            this.label33.Text = "Д14";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(53, 38);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(53, 95);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(72, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "Д11";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(72, 95);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(28, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Д13";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(53, 57);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(53, 76);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(72, 76);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 13);
            this.label35.TabIndex = 6;
            this.label35.Text = "Д12";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(928, 586);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._reserveGroupButton);
            this.groupBox28.Controls.Add(this._mainGroupButton);
            this.groupBox28.Controls.Add(this.label224);
            this.groupBox28.Controls.Add(this.label228);
            this.groupBox28.Controls.Add(this._reservedGroupOfSetpointsFromInterface);
            this.groupBox28.Controls.Add(this._mainGroupOfSetpointsFromInterface);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(328, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(191, 83);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            // 
            // _reserveGroupButton
            // 
            this._reserveGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._reserveGroupButton.Location = new System.Drawing.Point(99, 56);
            this._reserveGroupButton.Name = "_reserveGroupButton";
            this._reserveGroupButton.Size = new System.Drawing.Size(86, 23);
            this._reserveGroupButton.TabIndex = 30;
            this._reserveGroupButton.Text = "Переключить";
            this._reserveGroupButton.UseVisualStyleBackColor = true;
            this._reserveGroupButton.Click += new System.EventHandler(this._reserveGroupButton_Click);
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(6, 56);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label224.Location = new System.Drawing.Point(110, 39);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(62, 13);
            this.label224.TabIndex = 28;
            this.label224.Text = "Резервная";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label228.Location = new System.Drawing.Point(21, 39);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(57, 13);
            this.label228.TabIndex = 27;
            this.label228.Text = "Основная";
            // 
            // _reservedGroupOfSetpointsFromInterface
            // 
            this._reservedGroupOfSetpointsFromInterface.Location = new System.Drawing.Point(136, 19);
            this._reservedGroupOfSetpointsFromInterface.Name = "_reservedGroupOfSetpointsFromInterface";
            this._reservedGroupOfSetpointsFromInterface.Size = new System.Drawing.Size(13, 13);
            this._reservedGroupOfSetpointsFromInterface.State = BEMN.Forms.LedState.Off;
            this._reservedGroupOfSetpointsFromInterface.TabIndex = 24;
            // 
            // _mainGroupOfSetpointsFromInterface
            // 
            this._mainGroupOfSetpointsFromInterface.Location = new System.Drawing.Point(41, 19);
            this._mainGroupOfSetpointsFromInterface.Name = "_mainGroupOfSetpointsFromInterface";
            this._mainGroupOfSetpointsFromInterface.Size = new System.Drawing.Size(14, 13);
            this._mainGroupOfSetpointsFromInterface.State = BEMN.Forms.LedState.Off;
            this._mainGroupOfSetpointsFromInterface.TabIndex = 23;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox7);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._manageLed2);
            this.groupBox27.Controls.Add(this._manageLed1);
            this.groupBox27.Controls.Add(this.startOscopeBtn);
            this.groupBox27.Controls.Add(this.button1);
            this.groupBox27.Controls.Add(this._resetTermStateButton);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 349);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 37);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _manageLed2
            // 
            this._manageLed2.Location = new System.Drawing.Point(6, 42);
            this._manageLed2.Name = "_manageLed2";
            this._manageLed2.Size = new System.Drawing.Size(13, 13);
            this._manageLed2.State = BEMN.Forms.LedState.Off;
            this._manageLed2.TabIndex = 49;
            // 
            // _manageLed1
            // 
            this._manageLed1.Location = new System.Drawing.Point(6, 19);
            this._manageLed1.Name = "_manageLed1";
            this._manageLed1.Size = new System.Drawing.Size(13, 13);
            this._manageLed1.State = BEMN.Forms.LedState.Off;
            this._manageLed1.TabIndex = 48;
            // 
            // startOscopeBtn
            // 
            this.startOscopeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startOscopeBtn.Location = new System.Drawing.Point(6, 316);
            this.startOscopeBtn.Name = "startOscopeBtn";
            this.startOscopeBtn.Size = new System.Drawing.Size(304, 23);
            this.startOscopeBtn.TabIndex = 15;
            this.startOscopeBtn.Text = "Пуск осциллографа";
            this.startOscopeBtn.UseVisualStyleBackColor = true;
            this.startOscopeBtn.Click += new System.EventHandler(this.StartOscopeBtnClick);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(6, 287);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(304, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Сбросить число пусков по тепловой";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // _resetTermStateButton
            // 
            this._resetTermStateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetTermStateButton.Location = new System.Drawing.Point(6, 258);
            this._resetTermStateButton.Name = "_resetTermStateButton";
            this._resetTermStateButton.Size = new System.Drawing.Size(304, 23);
            this._resetTermStateButton.TabIndex = 14;
            this._resetTermStateButton.Text = "Сбросить состояние тепловой";
            this._resetTermStateButton.UseVisualStyleBackColor = true;
            this._resetTermStateButton.Click += new System.EventHandler(this._resetTermStateButton_Click);
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 134);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 111);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 88);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 65);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 229);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс индикации";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 129);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 106);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 134);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 111);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая запись журнала осциллографа";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 88);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 65);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.stopLogic);
            this.groupBox7.Controls.Add(this.startLogic);
            this.groupBox7.Controls.Add(this.label109);
            this.groupBox7.Controls.Add(this._logicState);
            this.groupBox7.Location = new System.Drawing.Point(6, 158);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(304, 65);
            this.groupBox7.TabIndex = 58;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Свободно программируемая логика";
            // 
            // stopLogic
            // 
            this.stopLogic.Location = new System.Drawing.Point(223, 37);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 3;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.stopLogic_Click);
            // 
            // startLogic
            // 
            this.startLogic.Location = new System.Drawing.Point(223, 12);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 2;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.startLogic_Click);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(59, 29);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(137, 13);
            this.label109.TabIndex = 1;
            this.label109.Text = "Состояние задачи логики";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(40, 29);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 0;
            // 
            // Mr762MeasuringFormV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 613);
            this.Controls.Add(this._dataBaseTabControl);
            this.Name = "Mr762MeasuringFormV2";
            this.Text = "Mr731MeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._dataBaseTabControl.ResumeLayout(false);
            this._analogTabPage.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._discretTabPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private System.Windows.Forms.TabPage _analogTabPage;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox _ucaTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox _ubcTextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox _uabTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox _fTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox _u0TextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox _u2TextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _u1TextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _ucTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _ubTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox _uaTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage _discretTabPage;
        private System.Windows.Forms.GroupBox groupBox14;
        private BEMN.Forms.LedControl _acceleration;
        private System.Windows.Forms.Label _auto4Led;
        private BEMN.Forms.LedControl _lzhOn;
        private System.Windows.Forms.Label _auto3Led;
        private BEMN.Forms.LedControl _urovOn;
        private System.Windows.Forms.Label _auto2Led;
        private BEMN.Forms.LedControl _avrBlock;
        private System.Windows.Forms.Label _auto1Led;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private BEMN.Forms.LedControl _igIoLed;
        private BEMN.Forms.LedControl _igLed;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.Label label308;
        private BEMN.Forms.LedControl _i2i1IoLed;
        private BEMN.Forms.LedControl _i2i1Led;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Label label302;
        private BEMN.Forms.LedControl _f4IoLessLed;
        private BEMN.Forms.LedControl _f3IoLessLed;
        private BEMN.Forms.LedControl _f2IoLessLed;
        private BEMN.Forms.LedControl _f4LessLed;
        private System.Windows.Forms.Label label303;
        private BEMN.Forms.LedControl _f1IoLessLed;
        private BEMN.Forms.LedControl _f3LessLed;
        private System.Windows.Forms.Label label304;
        private BEMN.Forms.LedControl _f2LessLed;
        private System.Windows.Forms.Label label305;
        private BEMN.Forms.LedControl _f1LessLed;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.Label label296;
        private BEMN.Forms.LedControl _f4IoMoreLed;
        private BEMN.Forms.LedControl _f3IoMoreLed;
        private BEMN.Forms.LedControl _f2IoMoreLed;
        private BEMN.Forms.LedControl _f4MoreLed;
        private System.Windows.Forms.Label label297;
        private BEMN.Forms.LedControl _f1IoMoreLed;
        private BEMN.Forms.LedControl _f3MoreLed;
        private System.Windows.Forms.Label label298;
        private BEMN.Forms.LedControl _f2MoreLed;
        private System.Windows.Forms.Label label299;
        private BEMN.Forms.LedControl _f1MoreLed;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.Label label290;
        private BEMN.Forms.LedControl _u4IoLessLed;
        private BEMN.Forms.LedControl _u3IoLessLed;
        private BEMN.Forms.LedControl _u2IoLessLed;
        private BEMN.Forms.LedControl _u4LessLed;
        private System.Windows.Forms.Label label291;
        private BEMN.Forms.LedControl _u1IoLessLed;
        private BEMN.Forms.LedControl _u3LessLed;
        private System.Windows.Forms.Label label292;
        private BEMN.Forms.LedControl _u2LessLed;
        private System.Windows.Forms.Label label293;
        private BEMN.Forms.LedControl _u1LessLed;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label284;
        private BEMN.Forms.LedControl _u4IoMoreLed;
        private BEMN.Forms.LedControl _u3IoMoreLed;
        private BEMN.Forms.LedControl _u2IoMoreLed;
        private BEMN.Forms.LedControl _u4MoreLed;
        private System.Windows.Forms.Label label285;
        private BEMN.Forms.LedControl _u1IoMoreLed;
        private BEMN.Forms.LedControl _u3MoreLed;
        private System.Windows.Forms.Label label286;
        private BEMN.Forms.LedControl _u2MoreLed;
        private System.Windows.Forms.Label label287;
        private BEMN.Forms.LedControl _u1MoreLed;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _iS1;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _iS2;
        private BEMN.Forms.LedControl _iS1Io;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _iS3;
        private BEMN.Forms.LedControl _iS2Io;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _iS4;
        private BEMN.Forms.LedControl _iS3Io;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _iS5;
        private BEMN.Forms.LedControl _iS4Io;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _iS6;
        private BEMN.Forms.LedControl _iS5Io;
        private BEMN.Forms.LedControl _iS6Io;
        private System.Windows.Forms.GroupBox groupBox20;
        private BEMN.Forms.LedControl _reservedGroupOfSetpoints;
        private System.Windows.Forms.Label label208;
        private BEMN.Forms.LedControl _mainGroupOfSetpoints;
        private System.Windows.Forms.Label label209;
        private BEMN.Forms.LedControl _fault;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label269;
        private BEMN.Forms.LedControl _i8Io;
        private BEMN.Forms.LedControl _i7Io;
        private BEMN.Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _i6Io;
        private BEMN.Forms.LedControl _i7;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _i5Io;
        private BEMN.Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _i4Io;
        private BEMN.Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _i3Io;
        private BEMN.Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _i2Io;
        private BEMN.Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _i1Io;
        private BEMN.Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox groupBox21;
        private BEMN.Forms.LedControl _ssl32;
        private System.Windows.Forms.Label label237;
        private BEMN.Forms.LedControl _ssl31;
        private System.Windows.Forms.Label label238;
        private BEMN.Forms.LedControl _ssl30;
        private System.Windows.Forms.Label label239;
        private BEMN.Forms.LedControl _ssl29;
        private System.Windows.Forms.Label label240;
        private BEMN.Forms.LedControl _ssl28;
        private System.Windows.Forms.Label label241;
        private BEMN.Forms.LedControl _ssl27;
        private System.Windows.Forms.Label label242;
        private BEMN.Forms.LedControl _ssl26;
        private System.Windows.Forms.Label label243;
        private BEMN.Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private BEMN.Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private BEMN.Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private BEMN.Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private BEMN.Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private BEMN.Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private BEMN.Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private BEMN.Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private BEMN.Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private BEMN.Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private BEMN.Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private BEMN.Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private BEMN.Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private BEMN.Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private BEMN.Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private BEMN.Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private BEMN.Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private BEMN.Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private BEMN.Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private BEMN.Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private BEMN.Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private BEMN.Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private BEMN.Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private BEMN.Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private BEMN.Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox19;
        private BEMN.Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _faultModule5;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _faultModule4;
        private System.Windows.Forms.Label label195;
        private BEMN.Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private BEMN.Forms.LedControl _faultModule2;
        private System.Windows.Forms.Label label197;
        private BEMN.Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private BEMN.Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private BEMN.Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private BEMN.Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private BEMN.Forms.LedControl _faultPass;
        private System.Windows.Forms.Label label203;
        private BEMN.Forms.LedControl _faultMeasuring;
        private System.Windows.Forms.Label label204;
        private BEMN.Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private BEMN.Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.GroupBox groupBox18;
        private BEMN.Forms.LedControl _indicator11;
        private System.Windows.Forms.Label label190;
        private BEMN.Forms.LedControl _indicator6;
        private System.Windows.Forms.Label label189;
        private BEMN.Forms.LedControl _indicator10;
        private System.Windows.Forms.Label label179;
        private BEMN.Forms.LedControl _indicator9;
        private System.Windows.Forms.Label label180;
        private BEMN.Forms.LedControl _indicator8;
        private System.Windows.Forms.Label label181;
        private BEMN.Forms.LedControl _indicator7;
        private System.Windows.Forms.Label label182;
        private BEMN.Forms.LedControl _indicator12;
        private System.Windows.Forms.Label label183;
        private BEMN.Forms.LedControl _indicator5;
        private System.Windows.Forms.Label label184;
        private BEMN.Forms.LedControl _indicator4;
        private System.Windows.Forms.Label label185;
        private BEMN.Forms.LedControl _indicator3;
        private System.Windows.Forms.Label label186;
        private BEMN.Forms.LedControl _indicator2;
        private System.Windows.Forms.Label label187;
        private BEMN.Forms.LedControl _indicator1;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.GroupBox groupBox17;
        private BEMN.Forms.LedControl _module32;
        private System.Windows.Forms.Label label138;
        private BEMN.Forms.LedControl _module31;
        private System.Windows.Forms.Label label139;
        private BEMN.Forms.LedControl _module30;
        private System.Windows.Forms.Label label140;
        private BEMN.Forms.LedControl _module29;
        private System.Windows.Forms.Label label141;
        private BEMN.Forms.LedControl _module28;
        private System.Windows.Forms.Label label142;
        private BEMN.Forms.LedControl _module27;
        private System.Windows.Forms.Label label232;
        private BEMN.Forms.LedControl _module26;
        private System.Windows.Forms.Label label127;
        private BEMN.Forms.LedControl _module25;
        private System.Windows.Forms.Label label128;
        private BEMN.Forms.LedControl _module24;
        private System.Windows.Forms.Label label129;
        private BEMN.Forms.LedControl _module23;
        private System.Windows.Forms.Label label130;
        private BEMN.Forms.LedControl _module22;
        private System.Windows.Forms.Label label131;
        private BEMN.Forms.LedControl _module21;
        private System.Windows.Forms.Label label132;
        private BEMN.Forms.LedControl _module20;
        private System.Windows.Forms.Label label133;
        private BEMN.Forms.LedControl _module19;
        private System.Windows.Forms.Label label134;
        private BEMN.Forms.LedControl _module10;
        private System.Windows.Forms.Label label164;
        private BEMN.Forms.LedControl _module9;
        private System.Windows.Forms.Label label165;
        private BEMN.Forms.LedControl _module8;
        private System.Windows.Forms.Label label166;
        private BEMN.Forms.LedControl _module7;
        private System.Windows.Forms.Label label167;
        private BEMN.Forms.LedControl _module6;
        private System.Windows.Forms.Label label168;
        private BEMN.Forms.LedControl _module5;
        private System.Windows.Forms.Label label172;
        private BEMN.Forms.LedControl _module4;
        private System.Windows.Forms.Label label173;
        private BEMN.Forms.LedControl _module3;
        private System.Windows.Forms.Label label174;
        private BEMN.Forms.LedControl _module2;
        private System.Windows.Forms.Label label175;
        private BEMN.Forms.LedControl _module1;
        private System.Windows.Forms.Label label176;
        private BEMN.Forms.LedControl _module18;
        private System.Windows.Forms.Label label161;
        private BEMN.Forms.LedControl _module17;
        private System.Windows.Forms.Label label162;
        private BEMN.Forms.LedControl _module16;
        private System.Windows.Forms.Label label163;
        private BEMN.Forms.LedControl _module15;
        private System.Windows.Forms.Label label169;
        private BEMN.Forms.LedControl _module14;
        private System.Windows.Forms.Label label170;
        private BEMN.Forms.LedControl _module13;
        private System.Windows.Forms.Label label171;
        private BEMN.Forms.LedControl _module12;
        private System.Windows.Forms.Label label177;
        private BEMN.Forms.LedControl _module11;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private BEMN.Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private BEMN.Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private BEMN.Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private BEMN.Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private BEMN.Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private BEMN.Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private BEMN.Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private BEMN.Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private BEMN.Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private BEMN.Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private BEMN.Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private BEMN.Forms.LedControl _d40;
        private System.Windows.Forms.Label label233;
        private BEMN.Forms.LedControl _d39;
        private System.Windows.Forms.Label label234;
        private BEMN.Forms.LedControl _d38;
        private System.Windows.Forms.Label label235;
        private BEMN.Forms.LedControl _d37;
        private System.Windows.Forms.Label label236;
        private BEMN.Forms.LedControl _d36;
        private System.Windows.Forms.Label label271;
        private BEMN.Forms.LedControl _d35;
        private System.Windows.Forms.Label label272;
        private BEMN.Forms.LedControl _d34;
        private System.Windows.Forms.Label label273;
        private BEMN.Forms.LedControl _d33;
        private System.Windows.Forms.Label label274;
        private BEMN.Forms.LedControl _d32;
        private System.Windows.Forms.Label label275;
        private BEMN.Forms.LedControl _d31;
        private System.Windows.Forms.Label label276;
        private BEMN.Forms.LedControl _d30;
        private System.Windows.Forms.Label label277;
        private BEMN.Forms.LedControl _d29;
        private System.Windows.Forms.Label label278;
        private BEMN.Forms.LedControl _d28;
        private System.Windows.Forms.Label label279;
        private BEMN.Forms.LedControl _d27;
        private System.Windows.Forms.Label label280;
        private BEMN.Forms.LedControl _d26;
        private System.Windows.Forms.Label label281;
        private BEMN.Forms.LedControl _d25;
        private System.Windows.Forms.Label label282;
        private BEMN.Forms.LedControl _d24;
        private System.Windows.Forms.Label label39;
        private BEMN.Forms.LedControl _d23;
        private System.Windows.Forms.Label label40;
        private BEMN.Forms.LedControl _d22;
        private System.Windows.Forms.Label label41;
        private BEMN.Forms.LedControl _d21;
        private System.Windows.Forms.Label label42;
        private BEMN.Forms.LedControl _d20;
        private System.Windows.Forms.Label label43;
        private BEMN.Forms.LedControl _d19;
        private System.Windows.Forms.Label label44;
        private BEMN.Forms.LedControl _d18;
        private System.Windows.Forms.Label label45;
        private BEMN.Forms.LedControl _d17;
        private System.Windows.Forms.Label label46;
        private BEMN.Forms.LedControl _d16;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _d15;
        private System.Windows.Forms.Label label32;
        private BEMN.Forms.LedControl _d14;
        private System.Windows.Forms.Label label33;
        private BEMN.Forms.LedControl _d13;
        private System.Windows.Forms.Label label34;
        private BEMN.Forms.LedControl _d12;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _d11;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _d10;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _d9;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _d8;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _d7;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _d6;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _d5;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _d4;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _d3;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _d2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _d1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button _reserveGroupButton;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.Label label228;
        private BEMN.Forms.LedControl _reservedGroupOfSetpointsFromInterface;
        private BEMN.Forms.LedControl _mainGroupOfSetpointsFromInterface;
        private System.Windows.Forms.GroupBox groupBox27;
        private BEMN.Forms.LedControl _availabilityFaultSystemJournal;
        private BEMN.Forms.LedControl _newRecordOscJournal;
        private BEMN.Forms.LedControl _newRecordAlarmJournal;
        private BEMN.Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox _nHotTextBox;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox _nStartTextBox;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox _qpTextBox;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _cosfTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _qTextBox;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox _pTextBox;
        private System.Windows.Forms.Label label97;
        private BEMN.Forms.LedControl _module34;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _module33;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _faultOff;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _ground;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox4;
        private BEMN.Forms.LedControl _q2Led;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _q1Led;
        private System.Windows.Forms.Label label104;
        private BEMN.Forms.LedControl _faultManageNet;
        private System.Windows.Forms.Label label98;
        private BEMN.Forms.LedControl _faultSwitchOff;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Button _resetTermStateButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _manageLed2;
        private BEMN.Forms.LedControl _manageLed1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _i0TextBox;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox _icTextBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox _igTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _i2TextBox;
        private System.Windows.Forms.TextBox _inTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _ibTextBox;
        private System.Windows.Forms.TextBox _i1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _iaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _in1TextBox;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Button startOscopeBtn;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label109;
        private Forms.LedControl _logicState;
    }
}