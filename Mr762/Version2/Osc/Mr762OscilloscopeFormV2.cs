using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.Mr762.Properties;
using BEMN.Mr762.Version2.AlarmJournal;
using BEMN.Mr762.Version2.Configuration.Structures;
using BEMN.Mr762.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr762.Version2.Osc.HelpClasses;
using BEMN.Mr762.Version2.Osc.Loaders;
using BEMN.Mr762.Version2.Osc.ShowOsc;
using BEMN.Mr762.Version2.Osc.Structures;

namespace BEMN.Mr762.Version2.Osc
{
    public partial class Mr762OscilloscopeFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ����";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoaderV2 _pageLoaderV2;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoaderV2 _oscJournalLoaderV2;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoaderV2 _currentOptionsLoader;
        
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV2 _countingListV2;

        private Mr762Device _device;
        private OscJournalStructV2 _journalStructV2;
        private readonly DataTable _table;
        private OscOptionsLoaderV2 _oscopeOptionsLoaderV2;
        private readonly MemoryEntity<OscOptionsStruct> _oscilloscopeSettings;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr762OscilloscopeFormV2()
        {
            this.InitializeComponent();
        }

        public Mr762OscilloscopeFormV2(Mr762Device device)
        {
            this.InitializeComponent();
            this._device = device;
            //��������� �������
            this._pageLoaderV2 = new OscPageLoaderV2(device.Mr762DeviceV2.SetOscStartPage, device.Mr762DeviceV2.OscPage);
            this._pageLoaderV2.PageRead += HandlerHelper.CreateActionHandler(this,this.PerformStep );
            this._pageLoaderV2.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoaderV2.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
            //��������� �������
            this._oscJournalLoaderV2 = new OscJournalLoaderV2(device.Mr762DeviceV2.OscJournal, device.Mr762DeviceV2.OneWordStruct);
            this._oscJournalLoaderV2.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoaderV2.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            this._oscJournalLoaderV2.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            this._oscilloscopeSettings = device.Mr762DeviceV2.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscJournalLoaderV2.StartReadJournal);
            this._oscilloscopeSettings.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            //������������ �����������
            this._oscopeOptionsLoaderV2 = device.Mr762DeviceV2.OscopeOptionsLoaderV2;
            this._oscopeOptionsLoaderV2.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscJournalLoaderV2.StartReadJournal);
            this._oscopeOptionsLoaderV2.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //��������� ������� �����
            this._currentOptionsLoader = device.Mr762DeviceV2.CurrentOptionsLoader;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscopeOptionsLoaderV2.StartRead);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);


            this._table = this.GetJournalDataTable();
        }
        #endregion [Ctor's]

        
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr762Device); }
        }

        public Type ClassType
        {
            get { return typeof (Mr762OscilloscopeFormV2); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        private void PerformStep()
        {
            this._oscProgressBar.PerformStep();
        }

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
            this.EnableButtons = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            var number = this._oscJournalLoaderV2.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoaderV2.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingListV2 = new CountingListV2(this._pageLoaderV2.ResultArray, this._journalStructV2,
                    this._currentOptionsLoader.MeasureStruct, this._oscopeOptionsLoaderV2.OscOptions.OscopeAllChannels.ChannelsInWords, this._device.DeviceVersion);
            }
            catch (Exception)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this.EnableButtons = true;
            }

            this.EnableButtons = true;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this._oscProgressBar.Value = this._oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV2 CountingListV2
        {
            get { return this._countingListV2; }
            set
            {
                this._countingListV2 = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��762_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();

        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingListV2 == null)
            {
                this.CountingListV2 = new CountingListV2(new ushort[12000],new OscJournalStructV2(), new MeasureTransStruct(), new ushort[24], this._device.DeviceVersion);
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this.CountingListV2.IsLoad)
                {
                    fileName = this.CountingListV2.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��762 v{this._device.DeviceVersion} �������������");
                    this._countingListV2.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new Mr762OscilloscopeResultFormV2(this.CountingListV2,
                    this._oscilloscopeSettings.Value);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoaderV2.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscReadButton.Enabled = false;
            this._currentOptionsLoader.StartRead();
        }

        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoaderV2.RecordNumber == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }

            this.EnableButtons = true;
        }
        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStructV2 = this._oscJournalLoaderV2.OscRecords[selectedOsc];          
            this._pageLoaderV2.StartRead(this._journalStructV2, this._oscilloscopeSettings.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoaderV2.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this._saveOscilloscopeDlg.FileName = "������������� ��762";
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = this._saveOscilloscopeDlg.FileName.Replace(".hdr", $"[v{this._device.DeviceVersion}].hdr");
                this._countingListV2.Save(fileName);
                this._statusLabel.Text = "������������ ���������";
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingListV2 = CountingListV2.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoaderV2.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        
        public bool Multishow
        {
            get { return false; }
        }
    }
}