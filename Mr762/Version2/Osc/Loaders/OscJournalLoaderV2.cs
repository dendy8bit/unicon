﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Mr762.Version2.Configuration.Structures;
using BEMN.Mr762.Version2.Osc.Structures;

namespace BEMN.Mr762.Version2.Osc.Loaders
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoaderV2
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructV2> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Длинна осцилограммы
        /// </summary>
        private readonly MemoryEntity<OscOptionsStruct> _oscOptions;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructV2> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]
        
        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;

        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]
        
        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        /// <param name="oscOptions">Объект параметров журнала</param>
        public OscJournalLoaderV2(MemoryEntity<OscJournalStructV2> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStructV2>();
            //Длинна осцилограммы
             //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            
        } 
        #endregion [Ctor's]
        
        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructV2> OscRecords
        {
            get { return this._oscRecords; }
        }

        #endregion [Properties]
        
        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void StartReadOscJournal()
        {
            this._recordNumber = 0;
            this._oscJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructV2.RecordIndex = this.RecordNumber;

                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructV2>());
                this._recordNumber = this.RecordNumber + 1;
                this.SaveIdex();
                if (this.ReadRecordOk == null) return;
                this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        private void SaveIdex()
        {
            OneWordStruct ind = this._refreshOscJournal.Value;
            ind.Word = (ushort)this._recordNumber;
            this._refreshOscJournal.Value = ind;
            this._refreshOscJournal.SaveStruct6();
        }
        #endregion [Private MemoryEntity Events Handlers]
        
        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            this.SaveIdex();
        }
        internal void Reset()
        {
            this._oscRecords.Clear();
        }
        #endregion [Public members]

    }
}
