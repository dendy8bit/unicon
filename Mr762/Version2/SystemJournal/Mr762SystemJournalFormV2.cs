﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr762.Properties;
using BEMN.Mr762.Version2.SystemJournal.Structures;


namespace BEMN.Mr762.Version2.SystemJournal
{
    public partial class Mr762SystemJournalFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Записей в журнале - {0}";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READ_FAIL = "Невозможно прочитать журнал";
        private const string TABLE_NAME_SYS = "МР762_журнал_системы";
        private const string NUMBER_SYS = "Номер";
        private const string TIME_SYS = "Время";
        private const string MESSAGE_SYS = "Сообщение";
        private const string SYSTEM_JOURNAL = "Журнал системы";

        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<JournalRefreshStruct> _refreshSystemJournal;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private DataTable _dataTable;
        private int _recordNumber;
        private Mr762Device _device;
        #endregion [Private fields]

        public Mr762SystemJournalFormV2()
        {
            this.InitializeComponent();
        }

        public Mr762SystemJournalFormV2(Mr762Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._systemJournal = device.Mr762DeviceV2.SystemJournal;
            this._systemJournal.Value.Messages = Common.VersionConverter(device.DeviceVersion) >= 2.04
                ? StringsSj.MessageV204
                : StringsSj.Message;
            this._refreshSystemJournal = device.Mr762DeviceV2.RefreshSystemJournal;
            this._refreshSystemJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.StartReadJournal);
            this._refreshSystemJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecord);
        }

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr762Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr762SystemJournalFormV2); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return SYSTEM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Properties]
        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set
            {
                this._recordNumber = value;
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                this.statusStrip1.Update();
            }
        }
        #endregion [Properties]


        #region [Help members]
        private void FailReadJournal()
        {
            this.RecordNumber = 0;
            this._statusLabel.Text = READ_FAIL;
        }

        private void StartReadJournal()
        {
            this.RecordNumber = 0;
            this._systemJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                this._dataTable.Rows.Add(new object[]
                {
                    this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                });
            }
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable(TABLE_NAME_SYS);
            table.Columns.Add(NUMBER_SYS);
            table.Columns.Add(TIME_SYS);
            table.Columns.Add(MESSAGE_SYS);
            return table;
        }

        private void SaveJournalToFile()
        {
            if (DialogResult.OK == this._saveJournalDialog.ShowDialog())
            {
                this._dataTable.WriteXml(this._saveJournalDialog.FileName);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }

        private void LoadJournalFromFile()
        {

            if (DialogResult.OK == this._openJournalDialog.ShowDialog())
            {
                this._dataTable.Clear();
                this._dataTable.ReadXml(this._openJournalDialog.FileName);
            }
            this.RecordNumber = this._systemJournalGrid.Rows.Count;
        }
        #endregion [Help members]


        #region [Events Handlers]
        private void SystemJournalForm_Load(object sender, EventArgs e)
        {
            this._dataTable = this.GetJournalDataTable();
            this._systemJournalGrid.DataSource = this._dataTable;
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._refreshSystemJournal.SaveStruct();
        }

        private void _readJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dataTable.Clear();
            this._refreshSystemJournal.SaveStruct();
        }

        private void _saveJournalButton_Click(object sender, EventArgs e)
        {
            this.SaveJournalToFile();
        }

        private void _loadJournalButton_Click(object sender, EventArgs e)
        {
            this.LoadJournalFromFile();
        }
        #endregion [Events Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._dataTable, this._saveJournalHtmlDialog.FileName, Resources.MR731SJ);
               this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
