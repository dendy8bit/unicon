﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version1.Measuring.Structures
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]
        [Layout(0)] private KanalTransStruct _i; //канал I защиты
        [Layout(1)] private KanalTransStruct _u; //канал U защиты 
        #endregion [Public field]

        public KanalTransStruct ChannelI
        {
           get { return this._i; }
        }

        public KanalTransStruct ChannelU
        {
            get { return this._u; }
        }
    }
}
