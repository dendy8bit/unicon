﻿using System.Linq;

namespace BEMN.Mr762.Version1.Measuring
{
    public static class CurrentConverter
    {
        //private const double K = 40 / (double)65536;
        /// <summary>
        /// Коэффициенты привидения
        /// </summary>
        private static ushort[] _factor;
        private static ushort _maxFactor;

        public static ushort[] Factors
        {
            set
            {
                CurrentConverter._factor = value;
                CurrentConverter._maxFactor = CurrentConverter._factor.Max();
            }
            get { return CurrentConverter._factor; }
        }
        /// <summary>
        /// Рассчитывает ток
        /// </summary>
        /// <param name="current">Полученное значение</param>
        /// <param name="number">Номер(0 для общих)</param>
        /// <returns></returns>
        public static string GetAmperage(ushort current, ushort number)
        {

            return string.Format("{0} A", CurrentConverter.GetAmperageValue(current,number));
        }

        /// <summary>
        /// Рассчитывает ток
        /// </summary>
        /// <param name="current">Полученное значение</param>
        /// <param name="number">Номер(0 для общих)</param>
        /// <returns></returns>
        public static double GetAmperageValue(ushort current, ushort number)
        {
            ushort factor = number == 0 ? CurrentConverter._maxFactor : CurrentConverter._factor[number - 1];
            //var realCurrent = current*K*factor; //Math.Round(, 2);

            return CalculateCurrent((short)(current), factor);

        }
        #warning Надо сделать нормально
        private static double CalculateCurrent(int current, int factor)
        {
            var delitel = 100;
         var num =   ((factor * 0x02800 * current) >> 16);
            if (num < 0x3E780)		//число между    0 - 999.5				диапазон _
            {
                num = num * 100;
            }
            else
                if (num < 0xF404C00)	//число между    999.5 - 999500.000000	диапазон K
                {
                    num = num / 10;
                }
                else				//число больше	999500.000000			диапазон M
                {
                    num = num / 10000;
                }
            //выделим подиапазон (x.xx, xx.x, xxx.)
            //===========================================

            if (num >= 0x3E780)
            {
                num = num/10;
                delitel = delitel/10;

            }

            if (num >= 0x3E780)
            {
                num = num / 10;
                delitel = delitel / 10;
            }
           
            if ((num & 0x80) != 0)
                num += 0x100;

            num = num >> 8;
            return num /(double) delitel;

        }
    }
}
