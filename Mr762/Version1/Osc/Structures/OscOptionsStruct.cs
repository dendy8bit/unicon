﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version1.Osc.Structures
{
    /// <summary>
    /// Параматры осцилографа
    /// </summary>
    public class OscOptionsStruct : StructBase
    {
        #region [Private fields]
        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        [Layout(0)] private int _loadedFullOscSize;
        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        [Layout(1)] private ushort _fullOscSizeInPages;
        /// <summary>
        /// Размер одного отсчёта в словах
        /// </summary>
        [Layout(2)] private ushort _loadedOscPoint;
        /// <summary>
        /// Длинна одной осциллограммы в отсчётах
        /// </summary>
        [Layout(3)] private ushort _oscLenght;
        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        [Layout(4)] private ushort _pageSize; 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Полный размер осцилографа в страницах
        /// </summary>
        public ushort FullOscSizeInPages
        {
            get { return this._fullOscSizeInPages; }
        }

        /// <summary>
        /// Размер страницы в словах(const)
        /// </summary>
        public ushort PageSize
        {
            get
            {
                return 1024; /*_pageSize; */}
        }

        /// <summary>
        /// Общий размер осциллографа
        /// </summary>
        public int LoadedFullOscSizeInWords
        {
            get { return this._loadedFullOscSize; }
            set { this._loadedFullOscSize = value; }
        }

        #endregion [Properties]
    }
}
