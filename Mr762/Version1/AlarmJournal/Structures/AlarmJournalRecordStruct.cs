﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr762.Version1.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]
        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        #endregion [Constants]
        
        #region [Private fields]
        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;
        [Layout(12)] private ushort _ia;
        [Layout(13)] private ushort _ib;
        [Layout(14)] private ushort _ic;
        [Layout(15)] private ushort _i0;
        [Layout(16)] private ushort _i2;
        [Layout(17)] private ushort _ig;
        [Layout(18)] private ushort _i1;
        [Layout(19)] private ushort _in;
        [Layout(20)] private ushort _in1;
        [Layout(21)] private ushort _i2A;
        [Layout(22)] private ushort _i2B;
        [Layout(23)] private ushort _i2C;
        [Layout(24)] private ushort _ua;
        [Layout(25)] private ushort _ub;
        [Layout(26)] private ushort _uc;
        [Layout(27)] private ushort _uab;
        [Layout(28)] private ushort _ubc;
        [Layout(29)] private ushort _uca;
        [Layout(30)] private ushort _u0;
        [Layout(31)] private ushort _u2;
        [Layout(32)] private ushort _u1;
        [Layout(33)] private ushort _un;
        [Layout(34)] private ushort _un1;
        [Layout(35)] private ushort _f;
        [Layout(36)] private ushort _d1;
        [Layout(37)] private ushort _d2;
        [Layout(38)] private ushort _d3;
        [Layout(39)] private ushort _omp;
        [Layout(40)] private ushort _spl;
        #endregion [Private fields]


        #region [Properties]
        public string Message
        {
            get
            {
                return AjStringsV1.Message.Count > this._message
                    ? AjStringsV1.Message[_message]
                    : this._message.ToString();
            }
        }

        public string Stage
        {
            get
            {
                int ind = Common.GetBits(this._stage, 0, 1, 2, 3, 4, 5, 6, 7);
                return AjStringsV1.Stage.Count > ind ? AjStringsV1.Stage[ind] : ind.ToString();
            }
        }

        public string TriggeredParametr
        {
            get
            {
                return AjStringsV1.Parametr.Count > this._numberOfTriggeredParametr
                    ? AjStringsV1.Parametr[this._numberOfTriggeredParametr]
                    : this._numberOfTriggeredParametr.ToString();
            }
        }

        public ushort NumberOfTriggeredParametr
        {
            get { return this._numberOfTriggeredParametr; }
        }

        public ushort ValueOfTriggeredParametr
        {
            get { return this._valueOfTriggeredParametr; }
        }

        public ushort GroupOfSetpoints
        {
            get { return this._groupOfSetpoints; }
        }

        public ushort Ia
        {
            get { return _ia; }
        }

        public ushort Ib
        {
            get { return _ib; }
        }

        public ushort Ic
        {
            get { return _ic; }
        }

        public ushort I0
        {
            get { return _i0; }
        }

        public ushort I2
        {
            get { return _i2; }
        }

        public ushort Ig
        {
            get { return _ig; }
        }

        public ushort I1
        {
            get { return _i1; }
        }

        public ushort In
        {
            get { return _in; }
        }

        public ushort In1
        {
            get { return _in1; }
        }

        public ushort I2A
        {
            get { return _i2A; }
        }

        public ushort I2B
        {
            get { return _i2B; }
        }

        public ushort I2C
        {
            get { return _i2C; }
        }

        public ushort Ua
        {
            get { return _ua; }
        }

        public ushort Ub
        {
            get { return _ub; }
        }

        public ushort Uc
        {
            get { return _uc; }
        }

        public ushort Uab
        {
            get { return _uab; }
        }

        public ushort Ubc
        {
            get { return _ubc; }
        }

        public ushort Uca
        {
            get { return _uca; }
        }

        public ushort U0
        {
            get { return _u0; }
        }

        public ushort U2
        {
            get { return _u2; }
        }

        public ushort U1
        {
            get { return _u1; }
        }

        public ushort Un
        {
            get { return _un; }
        }

        public ushort Un1
        {
            get { return _un1; }
        }

        public ushort F
        {
            get { return _f; }
        }

        public string D1To8
        {
            get { return this.GetMask(Common.LOBYTE(this._d1)); }
        }

        public string D9To16
        {
            get { return this.GetMask(Common.HIBYTE(this._d1)); }
        }

        public string D17To24
        {
            get { return this.GetMask(Common.LOBYTE(this._d2)); }
        }

        public string D25To32
        {
            get { return this.GetMask(Common.HIBYTE(this._d2)); }
        }

        public string D33To40
        {
            get { return this.GetMask(Common.LOBYTE(this._d3)); }
        }

        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get { return this._message == 0; }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                   (
                       DATE_TIME_PATTERN,
                       this._date,
                       this._month,
                       this._year,
                       this._hour,
                       this._minute,
                       this._second,
                       this._millisecond
                   );
            }
        }
        #endregion [Properties]

        #region [Help members]
        /// <summary>
        /// Ивертирует двоичное представление числа
        /// </summary>
        /// <param name="value">Число</param>
        /// <returns>Инвертированое двоичное представление</returns>
        private string GetMask(byte value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        } 
        #endregion [Help members]
    }
}
