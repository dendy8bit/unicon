﻿using System;

namespace BEMN.Mr762.Version1.OldClasses
{
    public class Measuring
    {
        public static double GetU(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)((double)value / (double)0x100 * (double)koeff) / 1000;

            return Math.Round(temp, 2);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double)((double)value / (double)0x100 * (double)Koeff);
        }

        public static double SetU(double value, ConstraintKoefficient koeff)
        {
            return (double)((double)value * 1000 / (double)koeff) * (double)0x100;
        }

        public static double GetTH(ushort value)
        {
            double temp = (double)((double)value / (double)0x100);
            return Math.Round(temp, 2);
        }

        public static double SetTH(double value)
        {
            double f = (value * 0x100);
            return f;
        }

        public static double GetConstraintOnly(ushort value, ConstraintKoefficient koeff)
        {
            double temp = ((double)value * (int)koeff / 65535) / 100;
            return Math.Round(temp, 2);
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            ushort t = (ushort)value;
            return (ushort)(value * 65535 * 100 / (int)koeff);
        }
    }
}