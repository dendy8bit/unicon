﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.Mr762.Version1.OldClasses
{
    public class StObj : Device
    {
        #region Поля
        private string _queryName;
        private int _valuesIndex = 0;
        private List<string> _slotNames;
        private byte _deviceNumber;
        private ushort _startAddr;
        private ushort _endAddr;
        private int _slotIndex = 0;
        private object _structObj;
        private ushort[] _values;
        private List<Device.slot> _slots;
        private bool _allReadOk = true;
        private bool _allWriteOk = true;
        private RW status = RW.NONE;
        //private Hashtable _logHash = new Hashtable();
        #endregion

        #region Свойства
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += new CompleteExchangeHandler(this._mb_CompleteExchange);
                }
            }
        }

        public byte DeviceNum
        {
            set
            {
                this._deviceNumber = value;
            }
        }

        public ushort StartAddr
        {
            get
            {
                return this._startAddr;
            }
            set
            {
                this._startAddr = value;
            }
        }

        public ushort EndAddr
        {
            get
            {
                return this._endAddr;
            }
            set
            {
                this._endAddr = value;
            }
        }

        public int SlotIndex
        {
            get
            {
                return this._slotIndex;
            }
            set
            {
                this._slotIndex = value;
            }
        }

        public object StructObj
        {
            get { return this._structObj; }
            set { this._structObj = value; }
        }

        public ushort[] Values
        {
            get { return this._values; }
            set { this._values = value; }
        }

        public List<Device.slot> Slots
        {
            get { return this._slots; }
            set { this._slots = value; }
        }

        //public Hashtable LogHash
        //{
        //    get
        //    {
        //        return _logHash;
        //    }
        //}
        #endregion

        #region Конструкторы
        public StObj(string queryName, Modbus mb)
        {
            this._queryName = queryName;
            this.MB = mb;

            this.ReadOk += new Handler(this.StObj_ReadOk);
            this.ReadFail += new Handler(this.StObj_ReadFail);
            this.WriteOk += new Handler(this.StObj_WriteOk);
            this.WriteFail += new Handler(this.StObj_WriteFail);
        }
        #endregion

        #region Ивенты
        public event Handler ReadOk;
        public event Handler ReadFail;
        public event Handler WriteOk;
        public event Handler WriteFail;

        public delegate void ReadArray(object sender);
        public event ReadArray AllReadOk;
        public event ReadArray AllReadFail;
        public event ReadArray AllWriteOk;
        public event ReadArray AllWriteFail;
        #endregion

        #region Обработчики событий
        void StObj_WriteFail(object sender)
        {
            this._allWriteOk = false;
            this._slotIndex++;
            if (this._slotIndex >= this._slots.Count && this.AllWriteFail != null)
            {
                this._slotIndex = 0;
                this.AllWriteFail(this);
                this.status = RW.NONE;
            }
        }

        void StObj_WriteOk(object sender)
        {
            this._allWriteOk = true;
            this.SlotIndex++;
            if (this.SlotIndex >= this.Slots.Count)
            {
                this.SlotIndex = 0;
                if (this._allWriteOk)
                {
                    if (this.AllWriteOk != null)
                        this.AllWriteOk(this);
                    //status = RW.NONE;
                }
                else
                {
                    if (this.AllWriteFail != null)
                        this.AllWriteFail(this);
                    //status = RW.NONE;
                }
            }
           /* _allWriteOk = false;
            _slotIndex++;
            if (_slotIndex >= _slots.Count)
            {
                _slotIndex = 0;
                if (_allWriteOk)
                {
                    if (AllWriteOk != null)
                    {
                        AllWriteOk(this);
                    }
                    status = RW.NONE;
                }
                else
                {
                    if (AllWriteFail != null)
                    {
                        AllWriteFail(this);
                    }
                    status = RW.NONE;
                }
            }*/
        }

        void StObj_ReadFail(object sender)
        {
            this._allReadOk = false;
            this._slotIndex++;
            if (this._slotIndex >= this._slots.Count && this.AllReadFail != null)
            {
                this._slotIndex = 0;
                this.AllReadFail(this);
                this.status = RW.NONE;
            }
        }

        void StObj_ReadOk(object sender)
        {
            Array.ConstrainedCopy(this._slots[this._slotIndex].Value, 0, this._values, this._valuesIndex, this._slots[this._slotIndex].Value.Length);
            
            if (this._slotIndex < this._slots.Count)
            {
                this._valuesIndex += this._slots[this._slotIndex].Value.Length;
            }
            this._slotIndex++;
            if (this._slotIndex >= this._slots.Count)
            {
                this._slotIndex = 0;
                this._valuesIndex = 0;
                if (this._structObj != null)
                {
                    (this._structObj as IStructInit).InitStruct(Common.TOBYTES(this._values, false));
                }

                if (this._allReadOk)
                {
                    if (this.AllReadOk != null)
                    {
                        this.AllReadOk(this);
                    }
                    this.status = RW.NONE;
                }
                else
                {
                    if (this.AllReadFail != null)
                    {
                        this.AllReadFail(this);
                    }
                    this._allReadOk = true;
                    this.status = RW.NONE;
                }
            }
        }
        #endregion

        #region Функции чтения записи
        public void LoadStruckt(object deviceObj)
        {
            this.status = RW.Read;
            this._slotNames = new List<string>();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._slotNames.Add(this._queryName + "_" + i.ToString());
                LoadSlot(this._deviceNumber, this._slots[i], this._queryName + "_" + i.ToString(), deviceObj);

                //switch (_queryName)
                //{
                //    case "Выключатель": 
                //        {
                //            _logHash.Add(_queryName + "_" + i.ToString(), "МР761 №" + _deviceNumber + " - Выключатель: чтение");
                //            break;
                //        }
                //    default:
                //        break;
                //}
            }
        }

        public void LoadStrucktCicle(object deviceObj)
        {
            this.status = RW.Read;
            this._slotNames = new List<string>();
            for (int i = 0; i < this._slots.Count; i++)
            {
                this._slotNames.Add(this._queryName + "_" + i.ToString());
                LoadSlotCycle(this._deviceNumber, this._slots[i], this._queryName + "_" + i.ToString(), new TimeSpan(10), 10, deviceObj);
            }
        }

        public void RemoveStrucktQuerys()
        {
            if (this._slotNames != null)
            {
                for (int i = 0; i < this._slots.Count; i++)
                {
                    this.MB.RemoveQuery(this._slotNames[i]);
                }
            }
        }

        public void SaveStruckt()
        {
            this.status = RW.Write;
            this._values = (this._structObj as IStructInit).GetValues();

            int valIndex = 0;
            for (int i = 0; i < this._slots.Count; i++)
            {
                Array.ConstrainedCopy(this._values, valIndex, this._slots[i].Value, 0, this._slots[i].Value.Length);
                valIndex += this._slots[i].Value.Length;
            }

            for (int i = 0; i < this._slots.Count; i++)
            {
                SaveSlot(this._deviceNumber, this._slots[i], this._slotNames[i], null);
            }
        }
        #endregion

        #region Доп. функции
        public bool RefreshQuerysSlot(ushort startAddr)
        {
            bool res = false;
            if (startAddr + 8 < 65535)
            {
                this._slots = new List<slot>();
                this._startAddr = startAddr;
                this._endAddr = (ushort)(this._startAddr + 8);
                slot slot = new slot(this._startAddr, this._endAddr);
                this._slots.Add(slot);
                this.RemoveStrucktQuerys();
                this.LoadStrucktCicle(this);
                res = true;
            }
            return res;
        }
        #endregion

        private void _mb_CompleteExchange(object sender, Query query)
        {
            if (this._slotNames != null && this._slotNames.Contains(query.name))
            {
                if (this.status == RW.Read)
                {
                    slot result = this._slots[this._slotNames.IndexOf(query.name)];
                    Raise(query, this.ReadOk, this.ReadFail, ref result);
                }
                else if (this.status == RW.Write)
                {
                    if (0 != query.fail)
                    {
                        if (null != this.WriteFail)
                        {
                            this.WriteFail(this);
                        }
                    }
                    else
                    {
                        if (null != this.WriteOk)
                        {
                            this.WriteOk(this);
                        }
                    }
                }
                
            }
            base.mb_CompleteExchange(sender, query);
        }
    }
}