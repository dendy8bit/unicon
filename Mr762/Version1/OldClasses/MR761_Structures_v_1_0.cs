﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Mr762.Version1.OldClasses
{
    public struct CONFOMP : IStruct, IStructInit
    {
        public ushort config;					
        public ushort Xyg;						

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFOMP)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray, int slotLength)
        {
            object result = null;
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFOMP)) / 2));
            }
            else
            {
                int arrayLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFOMP)) / 2 / slotLength;
                int lastSlotLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFOMP)) / 2 % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Xyg = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.Xyg);
            return result.ToArray();
        }

        #endregion
    }

    /*1*/
    public struct SWITCH : IStruct, IStructInit
    {
        public ushort config;					//конфигурация: управления выключателем
        public ushort on;						//вход положение включено
        public ushort off;					//вход положение выключено
        public ushort err;					//вход неисправность выключателя
        public ushort block;					//вход блокировка включения
        public ushort timeUrov;				//время УРОВ
        public ushort urov;					//ток УРОВ
        public ushort timeImp;				//время импульса сигнала управления
        public ushort timeAcc;				//длительность включения (время ускорения)
        public ushort ccde;					//для контроля цепей включения отключения
        public ushort keyOn;					//вход ключ включить
        public ushort keyOff;					//вход ключ выключить
        public ushort extOn;					//вход внеш. включить
        public ushort extOff;					//вход внеш. выключить

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {         
            object result = null;
            
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH)) / 2));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH)) / 2 / slotLength;
                lastSlotLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH)) / 2 % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.@on = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.off = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.err = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeUrov = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.urov = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeImp = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeAcc = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ccde = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.keyOn = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.keyOff = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.extOn = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.extOff = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues() 
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.@on);
            result.Add(this.off);
            result.Add(this.err);
            result.Add(this.block);
            result.Add(this.timeUrov);
            result.Add(this.urov);
            result.Add(this.timeImp);
            result.Add(this.timeAcc);
            result.Add(this.ccde);
            result.Add(this.keyOn);
            result.Add(this.keyOff);
            result.Add(this.extOn);
            result.Add(this.extOff);
            return result.ToArray();
        }

        #endregion
    }

    /*2*/
    public struct APV : IStruct, IStructInit
    {
        public ushort config;						//конфигурация
        public ushort block;						//вход блокировки АПВ
        public ushort timeBlock;					//время блокировки АПВ
        public ushort ctrl;						    //время готовности АПВ
        public ushort step1;						//время 1 крата АПВ
        public ushort step2;						//время 2 крата АПВ
        public ushort step3;						//время 3 крата АПВ
        public ushort step4;						//время 4 крата АПВ

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(APV)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(APV)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeBlock = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ctrl = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.step1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.step2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.step3 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.step4 = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.block);
            result.Add(this.timeBlock);
            result.Add(this.ctrl);
            result.Add(this.step1);
            result.Add(this.step2);
            result.Add(this.step3);
            result.Add(this.step4);
            return result.ToArray();
        }
        #endregion
    }

    /*3*/
    public struct AVR : IStruct, IStructInit
    {
	    public ushort config;					//конфигурация
	    public ushort block;					//вход блокировки АВР
	    public ushort clear;					//вход сброс блокировки АВР
	    public ushort start;					//вход сигнала запуск АВР
	    public ushort on;						//вход АВР срабатывания
	    public ushort timeOn;					//время АВР срабатывания
	    public ushort off;					    //вход АВР возврат
	    public ushort timeOff;				    //время АВР возврат
	    public ushort timeOtkl;				    //задержка отключения резерва
	    public ushort rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AVR)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(AVR)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.clear = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.start = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.@on = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeOn = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.off = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeOff = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeOtkl = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;

        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.block);
            result.Add(this.clear);
            result.Add(this.start);
            result.Add(this.@on);
            result.Add(this.timeOn);
            result.Add(this.off);
            result.Add(this.timeOff);
            result.Add(this.timeOtkl);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    /*4*/
    public struct LPB : IStruct, IStructInit
    {
	    public ushort config;					//конфигурация ЛЗШ
	    public ushort val;					    //уставка ЛЗШ

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(LPB)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(LPB)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.val = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.val);
            return result.ToArray();
        }
        #endregion
    }

    /*5*/
    public struct AUTOBLOWER : IStruct, IStructInit
    {
        public ushort config;				//число ступеней обдува (нет, 1, 2, 3), контроль (по току, тепловому состоянию)
        public ushort timeret;			    //время возврата
        public BLOWER ST1;				    //ступень 1
        public BLOWER ST2;				    //ступень 2
        public BLOWER ST3;				    //ступень 3

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(AUTOBLOWER)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(AUTOBLOWER)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.ST1 = new BLOWER();
            this.ST2 = new BLOWER();
            this.ST3 = new BLOWER();

            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeret = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(BLOWER))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.ST1.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(BLOWER))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.ST2.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(BLOWER))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.ST3.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.timeret);
            result.AddRange(this.ST1.GetValues());
            result.AddRange(this.ST2.GetValues());
            result.AddRange(this.ST3.GetValues());
            return result.ToArray();
        }
        #endregion
    }

    /*6*/
    public struct TERMAL : IStruct, IStructInit
    {
        public ushort config;				//конфигурация (привязка S1, S2, S3)
        public ushort timeN1;				//постоянная времени нагрева 1
        public ushort timeN2;				//постоянная времени нагрева 2
        public ushort timeN3;				//постоянная времени нагрева 3
        public ushort timeN4;				//постоянная времени нагрева 4
        public ushort timeO1;				//постоянная времени охлаждения 1
        public ushort reset;				//вход сброс теплового состояния
        public ushort rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(TERMAL)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(TERMAL)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeN1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeN2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeN3 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeN4 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.timeO1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.reset = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.timeN1);
            result.Add(this.timeN2);
            result.Add(this.timeN3);
            result.Add(this.timeN4);
            result.Add(this.timeO1);
            result.Add(this.reset);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    /*7*/
    public struct INPUTSIGNAL : IStruct, IStructInit
    {
	    public ushort groopUst;			    //вход аварийная группа уставок
	    public ushort clrInd;				//вход сброс индикации

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPUTSIGNAL)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPUTSIGNAL)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.groopUst = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.clrInd = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.groopUst);
            result.Add(this.clrInd);
            return result.ToArray();
        }
        #endregion
    }

    /*8*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OSCOPE : IStruct, IStructInit
    {
	    public ushort config;				        //0 - фиксация по первой аварии 1 - фиксация по последней аварии
	    public ushort size;				            //размер осциллограмы
	    public ushort percent;			            //процент от размера осциллограммы
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
	    public ushort[] kanal;	                    //конфигурация канала осциллографирования
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 5)]
	    public ushort[] rez;

        public OSCOPE(bool a)
        {
            this.config = new ushort();
            this.size = new ushort();
            this.percent = new ushort();
            this.kanal = new ushort[24];
            this.rez = new ushort[5];
        }

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OSCOPE)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(OSCOPE)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.kanal = new ushort[24];
            this.rez = new ushort[5];

            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.size = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.percent = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            for (int i = 0; i < this.kanal.Length; i++)
            {
                this.kanal[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }
            for (int i = 0; i < this.rez.Length; i++)
            {
                this.rez[i] = 0;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.size);
            result.Add(this.percent);
            for (int i = 0; i < this.kanal.Length; i++)
            {
                result.Add(this.kanal[i]);
            }
            for (int i = 0; i < this.rez.Length; i++)
            {
                result.Add(this.rez[i]);
            }
            return result.ToArray();
        }
        #endregion
    }

    /*9*/
    public struct MEASURETRANS : IStruct, IStructInit
    {
        public KANALTRANS L1;					//канал L1 защиты
        public KANALTRANS U1;					//канал U защиты

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MEASURETRANS)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(MEASURETRANS)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.L1 = new KANALTRANS();
            this.U1 = new KANALTRANS();

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(KANALTRANS))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.L1.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(KANALTRANS))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.U1.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.L1.GetValues());
            result.AddRange(this.U1.GetValues());
            return result.ToArray();
        }
        #endregion
    }

    /*10*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct INPSYGNAL : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public INP[] i;

        public INPSYGNAL(int len)
        {
            this.i = null;
            this.InitStruct(new byte[this.GetStructInfo(len).FullSize * 2]);
        }
        #region IStruct Members


        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPSYGNAL)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPSYGNAL)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.i = new INP[16];
            int index = 0;
            for (int j = 0; j < this.i.Length; j++)
            {
                byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(INP))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.i[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.i.Length; j++)
            {
                result.AddRange(this.i[j].GetValues());
            }            
            return result.ToArray();
        }
        #endregion
    }

    /*11*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ELSSYGNAL : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public ELS[] i;

        public ELSSYGNAL(int len)
        {
            this.i = null;
            this.InitStruct(new byte[this.GetStructInfo(len).FullSize * 2]);
        }

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.i = new ELS[16];
            int index = 0;
            for (int j = 0; j < this.i.Length; j++)
            {
                byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELS))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.i[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.i.Length; j++)
            {
                result.AddRange(this.i[j].GetValues());
            }
            return result.ToArray();
        }
        #endregion
    }

    /*12*/

    public struct CURRENTPROTMAIN : IStruct, IStructInit
    {
        public CURRENTPROTLIST currentprotlist;

        public CURRENTPROTMAIN(int len)
        {
            this.currentprotlist = new CURRENTPROTLIST();
            this.InitStruct(new byte[this.GetStructInfo(len).FullSize*2]);
        }

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort) (System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTMAIN))/2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTMAIN))/2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort) (start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength/slotLength;
                lastSlotLength = structLength%slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLength));
                        startAddr += (ushort) slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLength));
                        startAddr += (ushort) slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr,
                                                                          (ushort) (startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.currentprotlist = new CURRENTPROTLIST();

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTLIST))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.currentprotlist.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.currentprotlist.GetValues());
            return result.ToArray();
        }

        #endregion
    }

    /*13*/

    public struct CURRENTPROTRESERVE : IStruct, IStructInit
    {
        public CURRENTPROTLIST currentprotlist;


        public CURRENTPROTRESERVE(int len)
        {
            this.currentprotlist = new CURRENTPROTLIST();
            this.InitStruct(new byte[this.GetStructInfo(len).FullSize*2]);
        }

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort) (System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTRESERVE))/2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTRESERVE))/2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort) (start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength/slotLength;
                lastSlotLength = structLength%slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLength));
                        startAddr += (ushort) slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort) (startAddr + slotLength));
                        startAddr += (ushort) slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr,
                                                                          (ushort) (startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.currentprotlist = new CURRENTPROTLIST();

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof (CURRENTPROTLIST))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.currentprotlist.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.currentprotlist.GetValues());
            return result.ToArray();
        }

        #endregion
    }

    /*14*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct PARAMAUTOMAT : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public RELEOUTROM[] releoutrom;		//реле
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
        public INDICATORROM[] indicatorrom;	//индикаторы
        public ushort disrepair;					//реле неисправность
        public ushort disrepairImp;				//импульс реле неисправность


        public PARAMAUTOMAT(int len)
        {
            this.releoutrom = null;
            this.indicatorrom = null;
            this.disrepair = 0;
            this.disrepairImp = 0;
            this.InitStruct(new byte[this.GetStructInfo(len).FullSize * 2]);
        }

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(PARAMAUTOMAT)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {

            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(PARAMAUTOMAT)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.releoutrom = new RELEOUTROM[32];
            this.indicatorrom = new INDICATORROM[12];

            int index = 0;
            byte[] oneStruct;
            for (int j = 0; j < this.releoutrom.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(RELEOUTROM))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.releoutrom[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.indicatorrom.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(INDICATORROM))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.indicatorrom[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            this.disrepair = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.disrepairImp = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.releoutrom.Length; j++)
            {
                result.AddRange(this.releoutrom[j].GetValues());
            }
            for (int j = 0; j < this.indicatorrom.Length; j++)
            {
                result.AddRange(this.indicatorrom[j].GetValues());
            }
            result.Add(this.disrepair);
            result.Add(this.disrepairImp);
            return result.ToArray();
        }
        #endregion
    }

    /*15*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CONFIGSYSTEM : IStruct, IStructInit
    {
        public CONFIGNET confnet;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 126)]
        public ushort[] rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGSYSTEM)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGSYSTEM)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.confnet = new CONFIGNET();
            this.rez = new ushort[128];

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGNET))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.confnet.InitStruct(oneStruct);
            for (int j = 0; j < this.rez.Length; j++)
            {
                this.rez[j] = 0;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.confnet.GetValues());
            for (int j = 0; j < this.rez.Length; j++)
            {
                result.Add(this.rez[j]);
            }
            return result.ToArray();
        }
        #endregion
    }

    public struct BLOWER : IStruct, IStructInit
    {
        public ushort ustI;				//уставка срабатывания ступени по току
        public ushort ustQ;				//уставка срабатывания ступени по тепловому состоянию
        public ushort time;				//время срабатывания ступени
        public ushort inp;				//вход состояние обдува

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(BLOWER)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(BLOWER)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.ustI = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ustQ = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.time = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.inp = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.ustI);
            result.Add(this.ustQ);
            result.Add(this.time);
            result.Add(this.inp);
            return result.ToArray();
        }
        #endregion
    }

    public struct CONFIGNET : IStruct, IStructInit
    {
	    public ushort adr;		                    //сетевой адрес устройства (1-247)
	    public ushort spd;		                    //скорость работы (1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200)
	    public ushort pause;		                    //пауза ответа (мс)
	    public ushort rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGNET)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGNET)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.adr = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.spd = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.pause = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.adr);
            result.Add(this.spd);
            result.Add(this.pause);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    public struct CORNER : IStruct, IStructInit
    {
	    public ushort c;			//угол для расчета по фазом
	    public ushort cn;			//угол для расчета по In
	    public ushort c0;			//угол для расчета по I0
	    public ushort c2;			//угол для расчета по I2

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNER)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNER)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.c = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.cn = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.c0 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.c2 = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.c);
            result.Add(this.cn);
            result.Add(this.c0);
            result.Add(this.c2);
            return result.ToArray();
        }
        #endregion
    }

    public struct CORNERSIDE : IStruct, IStructInit
    {
	    public CORNER S1;		//уголы для расчетов

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.S1 = new CORNER();

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNER))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.S1.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.S1.GetValues());
            return result.ToArray();
        }
        #endregion
    }

    public struct KANALTRANS : IStruct, IStructInit
    {
	    public ushort Ittl;				//ITTL конфигурация ТТ - номинальный первичный ток, KTHL конфигурация ТН, bit 15 - коэфициэнт 0-1, 1-1000
        public ushort Ittx;				//ITTX конфигурация ТТНП - номинальный первичный ток нулувой последовательности,KTHX конфигурация ТННП bit 15 - коэфициэнт 0-1, 1-1000
	    public ushort Ittx1;				//конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
		public ushort PolarityL;			//резерв, вход внешней неисправности тн (трансформатора напряжения)
		public ushort PolarityX;			//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
	    public ushort Binding;			//(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
	    public ushort Imax;				//max ток нагрузки,резерв
	    public ushort rez;				//резерв

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(KANALTRANS)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(KANALTRANS)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.Ittl = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Ittx = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Ittx1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.PolarityL = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.PolarityX = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Binding = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.Imax = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.Ittl);
            result.Add(this.Ittx);
            result.Add(this.Ittx1);
            result.Add(this.PolarityL);
            result.Add(this.PolarityX);
            result.Add(this.Binding);
            result.Add(this.Imax);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }


    public struct INP : IStruct, IStructInit
    {
	    public ushort a1;
	    public ushort a2;
	    public ushort a3;
	    public ushort a4;
	    public ushort a5;
	    public ushort a6;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INP)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(INP)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.a1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.a2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.a3 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.a4 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.a5 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.a6 = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.a1);
            result.Add(this.a2);
            result.Add(this.a3);
            result.Add(this.a4);
            result.Add(this.a5);
            result.Add(this.a6);
            return result.ToArray();
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ELS : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
	    public ushort[] a;

        #region [Properties]
        public BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this.a, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i / 16;
                    int y = i % 16;
                    this.a[x] = Common.SetBit(this.a[x], y, value[i]);
                }
            }
        }
        #endregion [Properties]


        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELS)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELS)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.a = new ushort[18];
            int index = 0;
            for (int i = 0; i < this.a.Length; i++)
            {
                this.a[i] = Common.TOWORD(array[index + 1], array[index]);
                index += sizeof(ushort);
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.a.Length; j++)
            {
                result.Add(this.a[j]);
            }
            return result.ToArray();
        }
        #endregion
    }



    public struct MTZMAIN : IStruct, IStructInit
    {
	    public ushort config;				//конфигурация выведено/введено (УРОВ - выведено/введено)...
	    public ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
	    public ushort block;					//вход блокировки
	    public ushort ust;					//уставка срабатывания_
	    public ushort time;					//время срабатывания_ тср
	    public ushort k;						//коэфиц. зависимой хар-ки
	    public ushort u;						//уставка пуска по напряжению Упуск
	    public ushort tu;					//время ускорения_
	    public ushort I21;			        // уставка в %
	    public ushort rez;					//резерв

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.config1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ust = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.time = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.k = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.u = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.tu = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.I21 = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.config1);
            result.Add(this.block);
            result.Add(this.ust);
            result.Add(this.time);
            result.Add(this.k);
            result.Add(this.u);
            result.Add(this.tu);
            result.Add(this.I21);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    public struct MTZUEXT : IStruct, IStructInit
    {
	    public ushort config;				//конфигурация выведено/введено (УРОВ - выведено/введено)...
	    public ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
	    public ushort block;					//вход блокировки
	    public ushort ust;					//уставка срабатывания_
	    public ushort time;					//время срабатывания_
	    public ushort u;						//уставка возврата
	    public ushort tu;					//время возврата
	    public ushort rez;					//резерв

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.config1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ust = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.time = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.u = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.tu = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.config1);
            result.Add(this.block);
            result.Add(this.ust);
            result.Add(this.time);
            result.Add(this.u);
            result.Add(this.tu);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    public struct MTZQ : IStruct, IStructInit
    {
	    public ushort config;				//конфигурация выведено/введено (УРОВ - выведено/введено)...
	    public ushort config1;				//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
	    public ushort block;					//вход блокировки
	    public ushort ust;					//уставка срабатывания_
	    public ushort time;					//время срабатывания_
	    public ushort rez;					//резерв

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.config = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.config1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.block = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.ust = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.time = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.config);
            result.Add(this.config1);
            result.Add(this.block);
            result.Add(this.ust);
            result.Add(this.time);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTLIST : IStruct, IStructInit
    {
	    public CORNERSIDE	    corner;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
	    public MTZMAIN[]		mtzmain;			//мтз основная
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
	    public MTZMAIN[]		mtzmaini0;		    //мтз I*
	    public MTZMAIN		    mtzi2i1;			//обрыв провода
	    public MTZMAIN		    mtzig;				//гармоника
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
	    public MTZUEXT[]		mtzumax;			//мтз U>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
	    public MTZUEXT[]		mtzumin;			//мтз U<
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
	    public MTZUEXT[]		mtzfmax;			//мтз F>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
	    public MTZUEXT[]		mtzfmin;			//мтз F<
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
	    public MTZQ[]		    mtzq;			//мтз Q>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
	    public MTZUEXT[]		mtzext;			//внешние

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.corner = new CORNERSIDE();
            this.mtzmain = new MTZMAIN[8];
            this.mtzmaini0 = new MTZMAIN[6];
            this.mtzi2i1 = new MTZMAIN();
            this.mtzig = new MTZMAIN();
            this.mtzumax = new MTZUEXT[4];
            this.mtzumin = new MTZUEXT[4];
            this.mtzfmax = new MTZUEXT[4];
            this.mtzfmin = new MTZUEXT[4];
            this.mtzq = new MTZQ[2];
            this.mtzext = new MTZUEXT[16];

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CORNERSIDE))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.corner.InitStruct(oneStruct);
            index += oneStruct.Length;

            for (int j = 0; j < this.mtzmain.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzmain[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzmaini0.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzmaini0[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.mtzi2i1.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZMAIN))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.mtzig.InitStruct(oneStruct);
            index += oneStruct.Length;

            for (int j = 0; j < this.mtzumax.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzumax[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzumin.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzumin[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzfmax.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzfmax[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzfmin.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzfmin[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzq.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZQ))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzq[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

            for (int j = 0; j < this.mtzext.Length; j++)
            {
                oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MTZUEXT))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.mtzext[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }

        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.corner.GetValues());
            for (int j = 0; j < this.mtzmain.Length; j++)
            {
                result.AddRange(this.mtzmain[j].GetValues());
            }
            for (int j = 0; j < this.mtzmaini0.Length; j++)
            {
                result.AddRange(this.mtzmaini0[j].GetValues());
            }
            result.AddRange(this.mtzi2i1.GetValues());
            result.AddRange(this.mtzig.GetValues());
            for (int j = 0; j < this.mtzumax.Length; j++)
            {
                result.AddRange(this.mtzumax[j].GetValues());
            }
            for (int j = 0; j < this.mtzumin.Length; j++)
            {
                result.AddRange(this.mtzumin[j].GetValues());
            }
            for (int j = 0; j < this.mtzfmax.Length; j++)
            {
                result.AddRange(this.mtzfmax[j].GetValues());
            }
            for (int j = 0; j < this.mtzfmin.Length; j++)
            {
                result.AddRange(this.mtzfmin[j].GetValues());
            }
            for (int j = 0; j < this.mtzq.Length; j++)
            {
                result.AddRange(this.mtzq[j].GetValues());
            }
            for (int j = 0; j < this.mtzext.Length; j++)
            {
                result.AddRange(this.mtzext[j].GetValues());
            }
            return result.ToArray();
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct CURRENTPROTALL : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
	    public CURRENTPROTLIST[] currentprotlist;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTALL)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTALL)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.currentprotlist = new CURRENTPROTLIST[2];

            int index = 0;
            for (int j = 0; j < this.currentprotlist.Length; j++)
            {
                byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTLIST))];
                Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
                this.currentprotlist[j].InitStruct(oneStruct);
                index += oneStruct.Length;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            for (int j = 0; j < this.currentprotlist.Length; j++)
            {
                result.AddRange(this.currentprotlist[j].GetValues());
            }
            return result.ToArray();
        }
        #endregion
    }

    public struct INDICATORROM : IStruct, IStructInit
    {
	    public ushort signal;
	    public ushort type;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(INDICATORROM)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(INDICATORROM)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.signal = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.type = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.signal);
            result.Add(this.type);
            return result.ToArray();
        }
        #endregion
    }

    public struct RELEOUTROM : IStruct, IStructInit
    {
	    public ushort signal;
	    public ushort type;
	    public ushort wait;
	    public ushort rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(RELEOUTROM)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(RELEOUTROM)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            int index = 0;
            this.signal = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.type = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            this.wait = Common.TOWORD(array[index + 1], array[index]);
            this.rez = 0;
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(this.signal);
            result.Add(this.type);
            result.Add(this.wait);
            result.Add(this.rez);
            return result.ToArray();
        }
        #endregion
    }


    public struct USTAVKI : IStruct, IStructInit
    {
	    public SWITCH			sw;					//конфигурациия выключателя
	    public APV				apv;				//конфигурациия АПВ
	    public AVR				avr;				//конфигурациия АВР
	    public LPB				lpb;				//конфигурациия АВР
	    public AUTOBLOWER		blower;				//конфигурациия всей автоматики обдува
	    public TERMAL			term;				//конфигурациия тепловой модели
	    public INPUTSIGNAL		impsg;				//конфигурациия входных сигналов
	    public OSCOPE			osc;				//конфигурациия осцилографа
	    public MEASURETRANS	mt;					    //структура измерительного трансформатора
	    public INPSYGNAL		inp;				//структура входных логических сигналов
	    public ELSSYGNAL		els;				//структура выходных логических сигналов
	    public CURRENTPROTALL	currentprotall;		//все защиты
	    public PARAMAUTOMAT	paramautomat;		    //параметры автоматики
	    public CONFIGSYSTEM	cnfsys;				    //конфигурациия системы
        public CONFOMP ompdata;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(USTAVKI)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(USTAVKI)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.sw = new SWITCH();
            this.apv = new APV();
            this.avr = new AVR();
            this.lpb = new LPB();
            this.blower = new AUTOBLOWER();
            this.term = new TERMAL();
            this.impsg = new INPUTSIGNAL();
            this.osc = new OSCOPE();
            this.mt = new MEASURETRANS();
            this.inp = new INPSYGNAL();
            this.els = new ELSSYGNAL();
            this.currentprotall = new CURRENTPROTALL();
            this.paramautomat = new PARAMAUTOMAT();
            this.cnfsys = new CONFIGSYSTEM();

            int index = 0;
            byte[] oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(SWITCH))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.sw.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(APV))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.apv.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(AVR))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.avr.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(LPB))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.lpb.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(AUTOBLOWER))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.blower.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(TERMAL))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.term.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPUTSIGNAL))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.impsg.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(OSCOPE))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.osc.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(MEASURETRANS))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.mt.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(INPSYGNAL))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.inp.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(ELSSYGNAL))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.els.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CURRENTPROTALL))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.currentprotall.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(PARAMAUTOMAT))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.paramautomat.InitStruct(oneStruct);
            index += oneStruct.Length;

            oneStruct = new byte[System.Runtime.InteropServices.Marshal.SizeOf(typeof(CONFIGSYSTEM))];
            Array.ConstrainedCopy(array, index, oneStruct, 0, oneStruct.Length);
            this.cnfsys.InitStruct(oneStruct);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(this.sw.GetValues());
            result.AddRange(this.apv.GetValues());
            result.AddRange(this.avr.GetValues());
            result.AddRange(this.lpb.GetValues());
            result.AddRange(this.blower.GetValues());
            result.AddRange(this.term.GetValues());
            result.AddRange(this.impsg.GetValues());
            result.AddRange(this.osc.GetValues());
            result.AddRange(this.mt.GetValues());
            result.AddRange(this.inp.GetValues());
            result.AddRange(this.els.GetValues());
            result.AddRange(this.currentprotall.GetValues());
            result.AddRange(this.paramautomat.GetValues());
            result.AddRange(this.cnfsys.GetValues());
            return result.ToArray();
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct OscJournal : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] datatime;
        public int ready;
        public int point;
        public int begin;
        public int len;
        public int after;
        public int alm;
        public int rez;

        #region IStruct Members

        public StructInfo GetStructInfo(int len)
        {
            StructInfo sInfo = new StructInfo();
            sInfo.FullSize = (ushort)(System.Runtime.InteropServices.Marshal.SizeOf(typeof(OscJournal)) / 2);
            sInfo.SlotsArray = sInfo.FullSize > len;
            return sInfo;
        }

        public object GetSlots(ushort start, bool slotArray,  int slotLength)
        {
            object result = null;
            
            int structLength = System.Runtime.InteropServices.Marshal.SizeOf(typeof(OscJournal)) / 2;
            
            if (!slotArray)
            {
                result = new Device.slot(start, (ushort)(start + structLength));
            }
            else
            {
                int lastSlotLength = 0;
                int arrayLength = structLength / slotLength;
                lastSlotLength = structLength % slotLength;
                if (lastSlotLength > 0)
                {
                    arrayLength++;
                }
                Device.slot[] slots = new Device.slot[arrayLength];
                ushort startAddr = start;
                if (lastSlotLength == 0)
                {
                    for (int i = 0; i < arrayLength; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                }
                else
                {
                    for (int i = 0; i < arrayLength - 1; i++)
                    {
                        slots[i] = new Device.slot(startAddr, (ushort)(startAddr + slotLength));
                        startAddr += (ushort)slotLength;
                    }
                    slots[arrayLength - 1] = new Device.slot(startAddr, (ushort)(startAddr + lastSlotLength));
                }
                result = slots;
            }
            return result;
        }

        #endregion

        #region IStructInit Members

        public void InitStruct(byte[] array)
        {
            this.datatime = new int[4];
            int index = 0;
            for (int j = 0; j < this.datatime.Length; j++)
            {
                this.datatime[j] = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
                index += sizeof(int);
            }

            this.ready = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
            index += sizeof(int);

            this.point = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
            index += sizeof(int);

            this.begin = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
            index += sizeof(int);

            this.len = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
            index += sizeof(int);

            this.after = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
            index += sizeof(int);

            this.alm = Common.UshortUshortToInt(Common.TOWORD(array[index + 1], array[index]), Common.TOWORD(array[index + 3], array[index + 2]));
        }

        public ushort[] GetValues()
        {
            return new ushort[1];
        }
        #endregion
    }

    public struct Window_Param		//	AnalogBDStruct ???
    {
	    public ushort _Ia;		// ток A
	    public ushort _Ib;		// ток B
	    public ushort _Ic;		// ток C
	    public ushort _I0;		// ток 0
	    public ushort _I1;		// ток 2
	    public ushort _I2;		// ток 2
	    public ushort _In;		// ток N
	    public ushort _In1;		// ток N1
	    public ushort _I2a;		// ток A
	    public ushort _I2b;		// ток B
	    public ushort _I2c;		// ток C
	    public ushort _Ua;		// напряжение A
	    public ushort _Ub;		// напряжение B
	    public ushort _Uc;		// напряжение C
	    public ushort _Un;		// напряжение N
	    public ushort _Un1;		// напряжение N1
	    public ushort _Uab;
	    public ushort _Ubc;
	    public ushort _Uca;
	    public ushort _U0;
	    public ushort _U1;
	    public ushort _U2;
	    public ushort _F;		// частота
	    public ushort _Fi;		// частота
	    public uint P;
	    public uint Q;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BDBIT_CTRL
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 18)]
	    public ushort[] _base;			    //бд общаяя
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
	    public ushort[] alarm;				//БД неисправностей
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
	    public ushort[] param;				//БД параметров
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
	    public ushort[] control;			    //БД управления
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
	    public uint[] protectoff;		    //БД защит давших комманду на откл.
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public uint[] returnoff;		    //БД защит с возвратом
    }

}
