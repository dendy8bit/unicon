﻿namespace BEMN.Mr762.Version1.OldClasses
{
    public enum RW
    {
        NONE = 0,
        Read = 1,
        Write = 2
    }
}