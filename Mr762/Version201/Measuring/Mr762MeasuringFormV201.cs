﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Mr762.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr762.Version201.Measuring.Structures;
using System.Linq;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr762.Properties;
using BEMN.Mr762.Version2.Measuring.Structures;

namespace BEMN.Mr762.Version201.Measuring
{
    public partial class Mr762MeasuringFormV201 : Form, IFormView
    {
        #region Const
        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить индикацию";
        private const string MAIN_GROUP_SETPOINTS = "Переключить на основную группу уставок";
        private const string RESERVE_GROUP_SETPOINTS = "Переключить на резервную группу уставок";
        private const string RESET_HOT_STATE = "Сбросить состояние тепловой";
        private const string RESET_COUNT_START = "Сбросить число пусков по тепловой";
        private const string MEASURE_TRANS_READ_FAIL = "Параметры измерений не были загружены";
        private const string MEASURE_TRANS_READ_OK = "Параметры измерений загружены";
        private const string BREAKER_ON = "Включить выключатель";
        private const string BREAKER_OFF = "Отключить выключатель";
        private const string START_OSCOPE = "Запуск осциллографа от СДТУ";
        private const string START_LOGIC = "Запустить СПЛ";
        private const string STOP_LOGIC = "ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства. Остановить СПЛ";
        #endregion

        #region [Private fields]
        private readonly MemoryEntity<AnalogDataBaseStructV201> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<MeasureTransStruct> _measureTrans;
        private MeasureTransStruct _measureTransStruct;
        private readonly AveragerTime<AnalogDataBaseStructV201> _averagerTime;
        private string[] _symbols;
        private Mr762Device _device;
        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Защиты I(I*, I2/I1, Ig)
        /// </summary>
        private LedControl[] _currents;

        /// <summary>
        /// Защиты U,F,Q
        /// </summary>
        private LedControl[] _voltage;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;

        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;


        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _state;

        /// <summary>
        /// Группа уставок
        /// </summary>
        private LedControl[] _groupOfSetpoints;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faults;

        /// <summary>
        /// Автоматика
        /// </summary>
        private LedControl[] _automatics;

        #endregion [Private fields]

        #region Constructor
        public Mr762MeasuringFormV201()
        {
            InitializeComponent();
        }

        public Mr762MeasuringFormV201(Mr762Device device)
        {
            InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;
            
            this._dateTime = device.Mr762DeviceV2.DateAndTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, DateTimeLoad);

            this._discretDataBase = device.Mr762DeviceV2.DiscretDataBase;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, DiscretBdReadFail);

            this._analogDataBase = device.Mr762DeviceV2.AnalogDataBaseV201;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, AnalogBdReadFail);

            this._measureTrans = device.Mr762DeviceV2.MeasureTrans;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, MeasureTransReadFail);

            device.Mr762DeviceV2.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                () => this._measureTransStruct = device.Mr762DeviceV2.Configuration.Value.Mt);

            this._averagerTime = new AveragerTime<AnalogDataBaseStructV201>(500);
            this._averagerTime.Tick +=AveragerTimeTick;

            this.Init();
        }
        private void Init()
        {
            this._automatics = new[]
                {
                    this._avrBlock,
                    this._lzhOn,
                    this._urovOn,
                    new LedControl(), 
                    this._acceleration
                };
            this._voltage = new[]
                {
                    this._u1IoMoreLed,this._u1MoreLed,this._u2IoMoreLed,this._u2MoreLed,
                    this._u3IoMoreLed,this._u3MoreLed,this._u4IoMoreLed,this._u4MoreLed,

                    this._u1IoLessLed,this._u1LessLed,this._u2IoLessLed,this._u2LessLed,
                    this._u3IoLessLed,this._u3LessLed,this._u4IoLessLed,this._u4LessLed,

                    this._f1IoMoreLed,this._f1MoreLed,this._f2IoMoreLed,this._f2MoreLed,
                    this._f3IoMoreLed,this._f3MoreLed,this._f4IoMoreLed,this._f4MoreLed,

                    this._f1IoLessLed,this._f1LessLed,this._f2IoLessLed,this._f2LessLed,
                    this._f3IoLessLed,this._f3LessLed,this._f4IoLessLed,this._f4LessLed,

                    this._q1Led,this._q2Led
                };

            this._freeLogic = new[]
                {
                    this._ssl1,this._ssl2,this._ssl3,this._ssl4,this._ssl5,this._ssl6,this._ssl7,this._ssl8,
                    this._ssl9,this._ssl10,this._ssl11,this._ssl12,this._ssl13,this._ssl14,this._ssl15,this._ssl16,
                    this._ssl17,this._ssl18,this._ssl19,this._ssl20,this._ssl21,this._ssl22,this._ssl23,this._ssl24,
                    this._ssl25,this._ssl26,this._ssl27,this._ssl28,this._ssl29,this._ssl30,this._ssl31,this._ssl32
                };
            this._currents = new[]
                {
                    this._i1Io,this._i1,this._i2Io,this._i2,this._i3Io,this._i3,this._i4Io,this._i4,this._i5Io,
                    this._i5,this._i6Io,this._i6,this._i7Io,this._i7,this._i8Io,this._i8,this._iS1Io,this._iS1,
                    this._iS2Io,this._iS2,this._iS3Io,this._iS3,this._iS4Io,this._iS4,this._iS5Io,this._iS5,
                    this._iS6Io,this._iS6,this._i2i1IoLed,this._i2i1Led,this._igIoLed,this._igLed
                };


            this._state = new[]
                {
                    this._fault,this._mainGroupOfSetpoints,this._reservedGroupOfSetpoints,
                    this._ground,this._faultOff,this._manageLed1,this._manageLed2
                };
            this._groupOfSetpoints = new[]
                {
                    new LedControl(), 
                    this._mainGroupOfSetpointsFromInterface,
                    this._reservedGroupOfSetpointsFromInterface
                };
            this._indicators = new[]
                {
                    this._indicator1,this._indicator2,this._indicator3,this._indicator4,
                    this._indicator5,this._indicator6,this._indicator7,this._indicator8,
                    this._indicator9,this._indicator10,this._indicator11,this._indicator12
                };
            this._relays = new[]
                {
                    this._module1,this._module2,this._module3,this._module4,this._module5,this._module6,this._module7,
                    this._module8,this._module9,this._module10,this._module11,this._module12,this._module13,this._module14,
                    this._module15,this._module16,this._module17,this._module18,this._module19,this._module20,this._module21,
                    this._module22,this._module23,this._module24,this._module25,this._module26,this._module27,this._module28,
                    this._module29,this._module30,this._module31,this._module32,this._module33,this._module34
                };
            this._externalDefenses = new[]
                {
                    this._vz1,this._vz2,this._vz3,this._vz4,this._vz5,this._vz6,this._vz7,this._vz8,
                    this._vz9,this._vz10,this._vz11,this._vz12,this._vz13,this._vz14,this._vz15,this._vz16
                };

            this._outputLogicSignals = new[]
                {
                    this._vls1,this._vls2,this._vls3,this._vls4,this._vls5,this._vls6,this._vls7,this._vls8,
                    this._vls9,this._vls10,this._vls11,this._vls12,this._vls13,this._vls14,this._vls15,this._vls16
                };
            this._inputsLogicSignals = new[]
                {
                    this._ls1,this._ls2,this._ls3,this._ls4,this._ls5,this._ls6,this._ls7,this._ls8,
                    this._ls9,this._ls10,this._ls11,this._ls12,this._ls13,this._ls14,this._ls15,this._ls16
                };
            this._controlSignals = new[]
                {
                    this._newRecordSystemJournal,
                    this._newRecordAlarmJournal,
                    this._newRecordOscJournal,
                    this._availabilityFaultSystemJournal
            
                };
            this._faults = new[]
                {
                    this._faultHardware,this._faultSoftware,this._faultMeasuring,this._faultSwitchOff,this._faultManageNet,
                    this._faultModule1,this._faultModule2,this._faultModule3,this._faultModule4,this._faultModule5,
                    this._faultSetpoints,this._faultGroupsOfSetpoints,this._faultPass,this._faultSystemJournal,
                    this._faultAlarmJournal,this._faultOsc
                };
            this._discretInputs = new[]
                {
                    this._d1,this._d2,this._d3,this._d4,this._d5,this._d6,this._d7,this._d8,this._d9,this._d10,
                    this._d11,this._d12,this._d13,this._d14,this._d15,this._d16,this._d17,this._d18,this._d19,this._d20,
                    this._d21,this._d22,this._d23,this._d24,this._d25,this._d26,this._d27,this._d28,this._d29,this._d30,
                    this._d31,this._d32,this._d33,this._d34,this._d35,this._d36,this._d37,this._d38,this._d39,this._d40
                };

        }

        #endregion Constructor
        
        #region MemoryEntity Members
        private void AnalogBdReadFail()
        {
            const string errorValue = "0";
            this._uaTextBox.Text = errorValue;
            this._ubTextBox.Text = errorValue;
            this._ucTextBox.Text = errorValue;
            this._uabTextBox.Text = errorValue;
            this._ubcTextBox.Text = errorValue;
            this._ucaTextBox.Text = errorValue;
            this._u1TextBox.Text = errorValue; 
            this._u2TextBox.Text = errorValue;
            this._u0TextBox.Text = errorValue;
            this._iaTextBox.Text = errorValue;
            this._ibTextBox.Text = errorValue;
            this._icTextBox.Text = errorValue;
            this._i1TextBox.Text = errorValue; 
            this._i2TextBox.Text = errorValue; 
            this._i0TextBox.Text = errorValue; 
            this._inTextBox.Text = errorValue;
            this._igTextBox.Text = errorValue; 
            this._fTextBox.Text = errorValue; 
            this._pTextBox.Text = errorValue; 
            this._qTextBox.Text = errorValue; 
            this._cosfTextBox.Text = errorValue; 
            this._qpTextBox.Text = errorValue;
            this._nStartTextBox.Text = errorValue; 
            this._nHotTextBox.Text = errorValue;
            this._dU.Text = this._dFi.Text = this._dF.Text = errorValue;
        }
        
        private void AveragerTimeTick()
        {
            try
            {
                string[] symbols;

                if (this._symbols != null || this._symbols.Length < 6)
                {
                    symbols = this._symbols;
                }
                else
                {
                    symbols = new string[7];
                    for (int i = 0; i < 7; i++)
                    {
                        symbols[i] = "!";
                    }
                }

                this._uaTextBox.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._measureTransStruct);
                this._ubTextBox.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._measureTransStruct);
                this._ucTextBox.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._measureTransStruct);
                this._uabTextBox.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._measureTransStruct);
                this._ubcTextBox.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._measureTransStruct);
                this._ucaTextBox.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._measureTransStruct);
                this._u1TextBox.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._measureTransStruct);
                this._u2TextBox.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._measureTransStruct);
                this._u0TextBox.Text = this._analogDataBase.Value.GetU0(this._averagerTime.ValueList, this._measureTransStruct);
                
                this._iaTextBox.Text = symbols[0] + this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._measureTransStruct);
                this._ibTextBox.Text = symbols[1] + this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._measureTransStruct);
                this._icTextBox.Text = symbols[2] + this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._measureTransStruct);
                this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._measureTransStruct);
                this._i2TextBox.Text = symbols[4] + this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._measureTransStruct);
                this._i0TextBox.Text = symbols[3] + this._analogDataBase.Value.GetI0(this._averagerTime.ValueList, this._measureTransStruct);
                
                this._inTextBox.Text = symbols[5] + this._analogDataBase.Value.GetIn(this._averagerTime.ValueList, this._measureTransStruct);
                this._in1TextBox.Text = symbols[6] + this._analogDataBase.Value.GetIn1(this._averagerTime.ValueList, this._measureTransStruct);
                this._igTextBox.Text = this._analogDataBase.Value.GetIg(this._averagerTime.ValueList, this._measureTransStruct);
                this._fTextBox.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                this._pTextBox.Text = this._analogDataBase.Value.GetP(this._averagerTime.ValueList, this._measureTransStruct);
                this._qTextBox.Text = this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, this._measureTransStruct);
                this._cosfTextBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);
                this._qpTextBox.Text = this._analogDataBase.Value.GetQt(this._averagerTime.ValueList);
                this._nStartTextBox.Text = this._analogDataBase.Value.GetStarts();
                this._nHotTextBox.Text = this._analogDataBase.Value.GetHotStarts();
                this._dU.Text = this._analogDataBase.Value.GetdU(this._averagerTime.ValueList);
                this._dFi.Text = this._analogDataBase.Value.GetdFi(this._averagerTime.ValueList);
                this._dF.Text = this._analogDataBase.Value.GetdF(this._averagerTime.ValueList);
            }
            catch (Exception)
            {
               // MessageBox.Show("Ошибка в _averager_Tick");
            }
        }

        private void MeasureTransReadFail()
        {
            MessageBox.Show(MEASURE_TRANS_READ_FAIL);
        }

        private void MeasureTransReadOk()
        {
            this._measureTransStruct = this._measureTrans.Value;
            MessageBox.Show(MEASURE_TRANS_READ_OK);
            this._analogDataBase.LoadStructCycle();
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);    
            LedManager.TurnOffLeds(this._currents);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._state);
            LedManager.TurnOffLeds(this._groupOfSetpoints);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._indicators);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faults);
            LedManager.TurnOffLeds(this._voltage);
            LedManager.TurnOffLeds(this._automatics);
            _logicState.State = LedState.Off;
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            this._symbols = this._discretDataBase.Value.CurrentsSymbols;
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, new BitArray(this._discretDataBase.Value.DiscretInputs));
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, new BitArray(this._discretDataBase.Value.InputsLogicSignals));
            //Выходные ЛС
            LedManager.SetLeds(this._outputLogicSignals, new BitArray(this._discretDataBase.Value.OutputLogicSignals));
            //Защиты U,F,Q
            LedManager.SetLeds(this._voltage, new BitArray(this._discretDataBase.Value.Voltage));
            //Защиты I(I*, I2/I1, Ig)
            LedManager.SetLeds(this._currents, new BitArray(this._discretDataBase.Value.MaximumCurrent));
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, new BitArray(this._discretDataBase.Value.ExternalDefenses));
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, new BitArray(this._discretDataBase.Value.FreeLogic));
            //Состояния
            LedManager.SetLeds(this._state, new BitArray(this._discretDataBase.Value.State));
            //Группа уставок, дублирует состояния
            LedManager.SetLeds(this._groupOfSetpoints, new BitArray(this._discretDataBase.Value.State));
            //Реле
            LedManager.SetLeds(this._relays, new BitArray(this._discretDataBase.Value.Relays));
            //Индикаторы
            LedManager.SetLeds(this._indicators, new BitArray(this._discretDataBase.Value.Indicators));
            //Автоматика
            LedManager.SetLeds(this._automatics, new BitArray(this._discretDataBase.Value.Automatics));
            //Контроль
            LedManager.SetLeds(this._controlSignals, new BitArray(this._discretDataBase.Value.ControlSignals));
            //Неисправности
            LedManager.SetLeds(this._faults, new BitArray(this._discretDataBase.Value.Faults));
            bool enableLogic = this._discretDataBase.Value.State.Last() && !this._discretDataBase.Value.FaultLogic;
            this._logicState.State = enableLogic ? LedState.NoSignaled : LedState.Signaled;
        }


        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        #endregion MemoryEntity Members

        #region HelpMembers
        private void dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartStopLoad();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
                this._measureTrans.LoadStruct();
            }
            else
            {
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D03, RESET_OSC);

        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D05, RESET_INDICATION);
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D06, MAIN_GROUP_SETPOINTS);
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D07, RESERVE_GROUP_SETPOINTS);
        }

        private void _resetTermStateButton_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D0E, RESET_HOT_STATE);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D0F, RESET_COUNT_START);
        }

        private void StartOscopeBtnClick(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D10, START_OSCOPE);
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D09, BREAKER_ON);
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D08, BREAKER_OFF);
        }
        
        private void startLogic_Click(object sender, EventArgs e)
        {
            this._discretDataBase.SetBitByAdress(0x0D0D, START_LOGIC);
        }

        private void stopLogic_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D0C, STOP_LOGIC);
        }
        #endregion HelpMembers

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr762.Mr762Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr762MeasuringFormV201); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        
    }
}
