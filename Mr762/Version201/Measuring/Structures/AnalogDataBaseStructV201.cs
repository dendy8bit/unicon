﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Mr762.Version2.Configuration.Structures.MeasuringTransformer;

namespace BEMN.Mr762.Version201.Measuring.Structures
{
    /// <summary>
    /// МР 762 Аналоговая база данных V2.01
    /// </summary>
    public class AnalogDataBaseStructV201 : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _ia; // ток A
        [Layout(1)] private ushort _ib; // ток B
        [Layout(2)] private ushort _ic; // ток C
        [Layout(3)] private ushort _i0; // ток 0
        [Layout(4)] private ushort _i1; // ток 2
        [Layout(5)] private ushort _i2; // ток 2
        [Layout(6)] private ushort _in; // ток N
        [Layout(7)] private ushort _in1; // ток N1
        [Layout(8)] private ushort _ig; // ток Iг
        //ток вторая гармоника
        [Layout(9)] private ushort _i2A; // ток A
        [Layout(10)] private ushort _i2B; // ток B
        [Layout(11)] private ushort _i2C; // ток C
        //канал U
        [Layout(12)] private ushort _ua; // напряжение A
        [Layout(13)] private ushort _ub; // напряжение B
        [Layout(14)] private ushort _uc; // напряжение C
        [Layout(15)] private ushort _un; // напряжение N
        [Layout(16)] private ushort _un1; // напряжение N1
        //напряжения расчетные
        [Layout(17)] private ushort _uab;
        [Layout(18)] private ushort _ubc;
        [Layout(19)] private ushort _uca;
        [Layout(20)] private ushort _u0;
        [Layout(21)] private ushort _u1;
        [Layout(22)] private ushort _u2;
        //канал F
        [Layout(23)] private ushort _f; // частота
        [Layout(24)] private int _p; //дб выровнено на 2 слова	
        [Layout(25)] private int _q; //дб выровнено на 2 слова
        [Layout(26)] private ushort _fi; // частота
        [Layout(27)] private ushort _omp;
        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(28)] private ushort _sfl1;		//
        [Layout(29)] private ushort _sfl2;		//
        [Layout(30)] private ushort _sfl3;		//
        [Layout(31)] private ushort _sfl4;		//
        [Layout(32)] private ushort _sfl5;		//
        [Layout(33)] private ushort _sfl6;		//
        [Layout(34)] private ushort _sfl7;		//
        [Layout(35)] private ushort _sfl8;		//
        //тепловая модель
        [Layout(36)] private ushort _qt;		//состояние тепловой модели
        [Layout(37)] private ushort _qtnum;	//число пусков
        [Layout(38)] private ushort _qtnumhot;	//число горячих пусков 
        [Layout(39)] private short _dU;
        [Layout(40)] private ushort _dFi;
        [Layout(41)] private short _dF;
        #endregion [Private fields]
        
        #region [Public members]
        public string GetStarts()
        {
            return this._qtnum.ToString();
        }

        public string GetHotStarts()
        {
            return this._qtnumhot.ToString();
        }

        public string GetQt(List<AnalogDataBaseStructV201> list)
        {
            ushort value = this.GetMean(list, o => o._qt);
            return ValuesConverterCommon.Analog.GetQt(value);
        }

        private ushort GetMean(List<AnalogDataBaseStructV201> list,Func<AnalogDataBaseStructV201, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort) (sum/(double) count);
        }

        private int GetMean(List<AnalogDataBaseStructV201> list, Func<AnalogDataBaseStructV201, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Sum(func);
            return (int) (sum / (double)count);
        }

        private short GetMean(List<AnalogDataBaseStructV201> list, Func<AnalogDataBaseStructV201, short> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (short)(sum / (double)count);
        }

        public string GetCosF(List<AnalogDataBaseStructV201> list)
        {
            ushort value = this.GetMean(list, o => o._fi);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStructV201> list,MeasureTransStruct measure)
        {
            int value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQ(value, measure.I1.Ittl * measure.U1.KthlValue);
        }

        public string GetP(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            int value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.I1.Ittl * measure.U1.KthlValue);
        }

        public string GetF(List<AnalogDataBaseStructV201> list)
        {
            ushort value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF(value);
        }

        public string GetIa(List<AnalogDataBaseStructV201> list,MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ia);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIb(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ib);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIc(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ic);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI1(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI2(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetI0(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._i0);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl * 40);
        }
        public string GetIn(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._in);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittx * 40);
        }

        public string GetIn1(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._in1);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittx1 * 40);
        }

        public string GetIg(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ig);
            return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittx * 40);
        }

        public string GetUa(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUb(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUc(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUab(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUbc(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetUca(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU1(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU2(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetU0(List<AnalogDataBaseStructV201> list, MeasureTransStruct measure)
        {
            ushort value = this.GetMean(list, o => o._u0);
            return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
        }

        public string GetdU(List<AnalogDataBaseStructV201> list)
        {
            short value = this.GetMean(list, o => o._dU);
            return ValuesConverterCommon.Analog.GetUdouble(value);
        }

        public string GetdFi(List<AnalogDataBaseStructV201> list)
        {
            ushort value = this.GetMean(list, o => o._dFi);
            return string.Format("{0} град.", Math.Round((double)value * 360 / ushort.MaxValue, 2));
        }

        public string GetdF(List<AnalogDataBaseStructV201> list)
        {
            short value = this.GetMean(list, o => o._dF);
            return ValuesConverterCommon.Analog.GetF(value);
        }
        #endregion [Public members]
    }
}
