﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Mr762.Version2.Configuration;
using BEMN.Mr762.Version2.Configuration.Structures.Oscope;

namespace BEMN.Mr762.Version201.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStructV201 : StructBase, IDgvRowsContainer<ChannelStruct>
    {
        #region [Constants]

        public const int KANAL_COUNT = 24;

        #endregion [Constants]

        [Layout(0, Count = KANAL_COUNT)]
        private ChannelStruct[] _kanal; //конфигурация канала осциллографирования

        #region Properties
        [XmlIgnore]
        public ushort[] ChannelsInWords
        {
            get { return this._kanal.Select(o => (ushort)StringsConfig.RelaySignals.IndexOf(o.Channel)).ToArray(); }
        }

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelStruct[] Rows
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }
        #endregion
    }
}
