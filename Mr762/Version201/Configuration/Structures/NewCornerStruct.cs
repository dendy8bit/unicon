﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr762.Version201.Configuration.Structures
{
    public class NewCornerStruct : StructBase
    {
        [BindingProperty(0)]
        public ushort C { get; set; }

        [BindingProperty(1)]
        public ushort Cn { get; set; }

        [BindingProperty(2)]
        public ushort C0 { get; set; }

        [BindingProperty(3)]
        public ushort C2 { get; set; }

        [BindingProperty(4)]
        public ushort Cn1 { get; set; }
    }
}
