﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Block;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.External;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.F;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.I;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.I2I1;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Istar;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.Q;
using BEMN.Mr762.Version2.Configuration.Structures.Defenses.U;
using BEMN.Mr762.Version201.Configuration.Structures.Ig;

namespace BEMN.Mr762.Version201.Configuration.Structures
{
    /// <summary>
    /// список защит
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "защиты")]
    public class DefensesSetpointsStructV201 : StructBase
    {

        [Layout(0)] private CornerStruct _corner;
        [Layout(1)] private AllMtzMainStruct _mtzmain; //мтз основная
        [Layout(2)] private AllDefenseStarStruct _mtzmaini0; //мтз I*
        [Layout(3)] private DefenseI2I1Struct _mtzi2I1; //обрыв провода
        [Layout(4)] private DefenseIgStructV201 _mtzig; //гармоника
        [Layout(5)] private AllDefenceUStruct _uDefences;
        [Layout(6)] private AllDefenseFStruct _fDefenses;
        [Layout(7)] private AllDefenseQStruct _qDefenses; //мтз Q>
        [Layout(8)] private DefenseTermBlockStruct _termblock; //блокировка по тепловой модели
        [Layout(9)] private DefenseNBlockStruct _curblock; //блокировка пуска двигателя по числу пусков
        [Layout(10)] private AllDefenseExternalStruct _externalDefenses;

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public NewCornerStruct Corner
        {
            get
            {
                NewCornerStruct corners = new NewCornerStruct();
                corners.C = this._corner.C;
                corners.Cn = this._corner.Cn;
                corners.C0 = this._corner.C0;
                corners.C2 = this._corner.C2;
                corners.Cn1 = this._mtzig.Cn1;
                return corners;
            }
            set
            {
                NewCornerStruct corners = value;
                this._corner.C = corners.C;
                this._corner.Cn = corners.Cn;
                this._corner.C0 = corners.C0;
                this._corner.C2 = corners.C2;
                this._mtzig.Cn1 = corners.Cn1;
            }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "I")]
        public AllMtzMainStruct Mtzmain
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "I_Со_звездой")]
        public AllDefenseStarStruct Mtzmaini0
        {
            get { return this._mtzmaini0; }
            set { this._mtzmaini0 = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "I2I1")]
        public DefenseI2I1Struct Mtzi2I1
        {
            get { return this._mtzi2I1; }
            set { this._mtzi2I1 = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "Ig")]
        public DefenseIgStructV201 Mtzig
        {
            get { return this._mtzig; }
            set { this._mtzig = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "U")]
        public AllDefenceUStruct UDefences
        {
            get { return this._uDefences; }
            set { this._uDefences = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "F")]
        public AllDefenseFStruct FDefenses
        {
            get { return this._fDefenses; }
            set { this._fDefenses = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Q")]
        public AllDefenseQStruct QDefenses
        {
            get { return this._qDefenses; }
            set { this._qDefenses = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Тепловая")]
        public DefenseTermBlockStruct Termblock
        {
            get { return this._termblock; }
            set { this._termblock = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "По_числу_пусков")]
        public DefenseNBlockStruct Curblock
        {
            get { return this._curblock; }
            set { this._curblock = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Внешние")]
        public AllDefenseExternalStruct ExternalDefenses
        {
            get { return this._externalDefenses; }
            set { this._externalDefenses = value; }
        }
    }
}
