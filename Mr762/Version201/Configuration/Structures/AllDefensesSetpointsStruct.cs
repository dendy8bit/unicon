﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.Mr762.Version201.Configuration.Structures
{
    /// <summary>
    /// защиты по двум группам уставок
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class AllDefensesSetpointsStructV201 : StructBase, ISetpointContainer<DefensesSetpointsStructV201>
    {
        #region [Public fields]
        /// <summary>
        /// Основная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Основная_группа_уставок")]
        [Layout(0)]
        public DefensesSetpointsStructV201 MainSetpoints;
        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Резервная_группа_уставок")]
        [Layout(1)]
        public DefensesSetpointsStructV201 ReserveSetpoints;
        #endregion [Public fields]

        [XmlIgnore]
        public DefensesSetpointsStructV201[] Setpoints
        {
            get
            {
                return new[]
                    {
                        this.MainSetpoints.Clone<DefensesSetpointsStructV201>(),
                        this.ReserveSetpoints.Clone<DefensesSetpointsStructV201>()
                    };
            }
            set
            {
                this.MainSetpoints = value[0].Clone<DefensesSetpointsStructV201>();
                this.ReserveSetpoints = value[1].Clone<DefensesSetpointsStructV201>();
            }
        }
    }
}
