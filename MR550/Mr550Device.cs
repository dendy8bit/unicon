using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR550.BSBGL;
using BEMN.MR550.Version1.Configuration;
using BEMN.MR550.Version1.Configuration.Structures;
using BEMN.MR550.Version1.Osc;
using BEMN.MR550.Version2.AlarmJournal;
using BEMN.MR550.Version2.AlarmJournal.Structures;
using BEMN.MR550.Version2.Configuration;
using BEMN.MR550.Version2.Configuration.Structures;
using BEMN.MR550.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version2.Measuring;
using BEMN.MR550.Version2.Measuring.Structures;
using BEMN.MR550.Version2.Osc.Forms;
using BEMN.MR550.Version2.Osc.Structures;
using BEMN.MR550.Version2.SystemJournal;
using BEMN.MR550.Version2.SystemJournal.Structures;
using MeasureTransStructV1 = BEMN.MR550.Version1.Configuration.Structures.MeasuringTransformer.MeasureTransStruct;

namespace BEMN.MR550
{
    public class Mr550Device : Device, IDeviceView, IDeviceVersion
    {
        private MemoryEntity<ConfigurationStructV2> _configuration;
        private MemoryEntity<ConfigurationStructV1> _configurationV1;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<AlarmJournalStruct> _alarmRecord;
        private MemoryEntity<MeasureTransStruct> _measureTransAj;
        
        #region Programming
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramStorageStruct> _programStorageStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;

        public MemoryEntity<ProgramStorageStruct> ProgramStorageStruct
        {
            get { return this._programStorageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStruct; }
        }
        #endregion

        #region [Osc]
        private MemoryEntity<MeasureTransStruct> _voltageConfiguration;
        private MemoryEntity<MeasureTransStructV1> _voltageConfigurationV1;
        /// <summary>
        /// ������ �������
        /// </summary>
        private MemoryEntity<OscJournalStruct> _oscJournal;
        /// <summary>
        /// ����� ������� �� ������� ������
        /// </summary>
        private MemoryEntity<OneWordStruct> _refreshOscJournal;

        /// <summary>
        /// ��������� ��������� �������� �������������
        /// </summary>
        private MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// �������� �������������
        /// </summary>
        private MemoryEntity<OscPage> _oscPage;
        #endregion [Osc]

        #region [Measure]
        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return this._discretDataBase; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return this._analogDataBase; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return this._measureTrans; }
        }
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<GroupSetpointBaseStruct> _groupSetpointBase;
        #endregion [Measure]

        #region Properties
        public MemoryEntity<AlarmJournalStruct> AlarmRecord
        {
            get { return this._alarmRecord; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTransAj
        {
            get { return this._measureTransAj; }
        }
        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        /// <summary>
        /// ������ �������
        /// </summary>
        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        /// <summary>
        /// ����� ������� �� ������� ������
        /// </summary>
        public MemoryEntity<OneWordStruct> RefreshOscJournal
        {
            get { return this._refreshOscJournal; }
        }

        /// <summary>
        /// ��������� ��������� �������� �������������
        /// </summary>
        public MemoryEntity<OneWordStruct> SetStartPage
        {
            get { return this._setStartPage; }
        }

        /// <summary>
        /// �������� �������������
        /// </summary>
        public MemoryEntity<OscPage> OscPage
        {
            get { return this._oscPage; }
        }

        public MemoryEntity<MeasureTransStruct> VoltageConfiguration
        {
            get { return this._voltageConfiguration; }
        }
        public MemoryEntity<MeasureTransStructV1> VoltageConfigurationV1
        {
            get { return this._voltageConfigurationV1; }
        }

        public MemoryEntity<ConfigurationStructV2> Configuration
        {
            get { return this._configuration; }
        }

        public MemoryEntity<ConfigurationStructV1> ConfigurationV1
        {
            get { return this._configurationV1; }
        }

        public MemoryEntity<GroupSetpointBaseStruct> GroupSetpointBase
        {
            get { return this._groupSetpointBase; }
        }
        #endregion prop

        #region ������������ �������������

        public Mr550Device()
        {
            HaveVersion = true;
        }

        public Mr550Device(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
            InitMemoryEntity();
        }
            
        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get { return mb; }
            set { base.MB = value; }
        }

        private void InitMemoryEntity()
        {
            this._configuration = new MemoryEntity<ConfigurationStructV2>("������������", this, 0x1000);
            this._configurationV1 = new MemoryEntity<ConfigurationStructV1>("������������V1", this, 0x1000);
            this._oscJournal = new MemoryEntity<OscJournalStruct>("������ ���", this, 0x800);
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("����� ������� ���", this, 0x800);
            this._setStartPage = new MemoryEntity<OneWordStruct>("��������� �������� ������������", this, 0x900);
            this._oscPage = new MemoryEntity<OscPage>("������ �������� ������������", this, 0x900);
            this._voltageConfiguration = new MemoryEntity<MeasureTransStruct>("��������� ����������", this, 0x1000);
            this._voltageConfigurationV1 = new MemoryEntity<MeasureTransStructV1>("��������� ����������V1", this, 0x1000);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("��", this, 0x2000);
            this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
            this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("���������� ��", this, 0x1800);
            this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("���������� ��", this, 0x1900);
            this._groupSetpointBase = new MemoryEntity<GroupSetpointBaseStruct>("������ �������", this, 0x400);
            this._measureTrans = new MemoryEntity<MeasureTransStruct>("��������� ���������", this, 0x1000);
            this._alarmRecord = new MemoryEntity<AlarmJournalStruct>("��", this, 0x2800);
            this._measureTransAj = new MemoryEntity<MeasureTransStruct>("��������� ��������� ��", this, 0x1000);
            this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0xB000);
            this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0001);
            this._programStorageStruct = new MemoryEntity<ProgramStorageStruct>("SaveLoadProgramStorage", this, 0xC000);
            this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals", this, 0xA000);
        }
        #endregion


        #region ������� ��������/����������

     
        public void WriteConfiguration()
        {
            SetBit(DeviceNumber, 0, true, "��550 ������ �������������", this);
        }

        public void ReadConfiguration()
        {
            SetBit(DeviceNumber, 0, false, "��550 ������ ����������", this);
        }

        #endregion
    

        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (Mr550Device); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr550; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��550"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion

        #region IDeviceVersion
        public Type[] Forms
        {
            get
            {
                double ver = Common.VersionConverter(DeviceVersion);
                if (ver <= 1.0)
                {
                    return new[]
                    {
                      typeof(BSBGLEF),
                      typeof(Mr550MeasuringForm),
                      typeof(Mr550SystemJournalForm),
                      typeof(Mr550AlarmJournalForm),
                      typeof(Mr550ConfigurationFormV1),
                      typeof(Mr550OscilloscopeFormV1)
                    };
                }
                else
                {
                    return new[]
                    {
                      typeof(BSBGLEF),
                      typeof(Mr550MeasuringForm),
                      typeof(Mr550SystemJournalForm),
                      typeof(Mr550AlarmJournalForm),
                      typeof(Mr550ConfigurationFormV2),
                      typeof(Mr550OscilloscopeForm)
                    };
                }
            }
        }

        public List<string> Versions
        {
            get
            {
                return new List<string>
                {
                    "1.0",
                    "1.01",
                    "1.02",
                    "1.03"
                };
            }
        }
        #endregion
    }
}