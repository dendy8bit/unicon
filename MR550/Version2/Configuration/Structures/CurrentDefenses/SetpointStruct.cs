﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR550.Version2.Configuration.Structures.CurrentDefenses
{
    public class SetpointStruct : StructBase
    {
        [Layout(0)] private AllCurrentDefensesStruct _currentDefenses;
        [Layout(1)] private AllAddCurrentDefensesStruct _addCurrentDefenses;

        [BindingProperty(0)]
        public AllCurrentDefensesStruct CurrentDefenses
        {
            get { return _currentDefenses; }
            set { _currentDefenses = value; }
        }

        [BindingProperty(1)]
        public AllAddCurrentDefensesStruct AddCurrentDefenses
        {
            get { return _addCurrentDefenses; }
            set { _addCurrentDefenses = value; }
        }
    }
}
