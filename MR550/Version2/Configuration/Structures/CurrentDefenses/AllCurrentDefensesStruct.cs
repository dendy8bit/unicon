﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR550.Version2.Configuration.Structures.CurrentDefenses
{
    public class AllCurrentDefensesStruct : StructBase, IDgvRowsContainer<CurrentDefenseStruct>
    {
        [Layout(0, Count = 4)]
        private ushort[] _res;
        [Layout(1, Count = 10)]
        private CurrentDefenseStruct[] _currentDefenses;

        public CurrentDefenseStruct[] Rows
        {
            get { return this._currentDefenses; }
            set { this._currentDefenses = value; }
        }
    }
}
