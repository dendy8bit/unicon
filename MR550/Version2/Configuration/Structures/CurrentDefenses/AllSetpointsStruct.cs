﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR550.Version2.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {
        private const int DEF_COUNT = 6;

        [Layout(0, Count = DEF_COUNT)]
        private AllCurrentDefensesStruct[] _currentDefenses;
         [Layout(1, Count = DEF_COUNT)]
         private AllAddCurrentDefensesStruct[] _addCurrentDefenses;

        [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                var result = new SetpointStruct[DEF_COUNT];
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    result[i] = new SetpointStruct();
                    result[i].AddCurrentDefenses =  _addCurrentDefenses[i].Clone<AllAddCurrentDefensesStruct>();
                    result[i].CurrentDefenses =  this._currentDefenses[i].Clone<AllCurrentDefensesStruct>();
                }
                return result;
            }
            set
            {
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    this._addCurrentDefenses[i] = value[i].AddCurrentDefenses;
                    this._currentDefenses[i] = value[i].CurrentDefenses;
                }
            }
        }

        [XmlElement(ElementName = "Группа1")]
        public SetpointStruct Group1
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[0].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[0].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа2")]
        public SetpointStruct Group2
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[1].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[1].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа3")]
        public SetpointStruct Group3
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[2].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[2].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа4")]
        public SetpointStruct Group4
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[3].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[3].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа5")]
        public SetpointStruct Group5
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[4].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[4].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

        [XmlElement(ElementName = "Группа6")]
        public SetpointStruct Group6
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[5].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[5].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }


    }
}
