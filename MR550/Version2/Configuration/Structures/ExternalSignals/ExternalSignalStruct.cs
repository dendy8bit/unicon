﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR550.Version2.Configuration.Structures.ExternalSignals
{
    public class ExternalSignalStruct : StructBase
    {
        [Layout(0)] private ushort _group1;
        [Layout(1)] private ushort _group2;
        [Layout(2)] private ushort _group3;
        [Layout(3)] private ushort _group4;
        [Layout(4)] private ushort _group5;
        [Layout(5)] private ushort _group6;
        [Layout(6)] private ushort _res1;
        [Layout(7)] private ushort _res2;
        [Layout(8)] private ushort _keyOff;
        [Layout(9)] private ushort _keyOn;
        [Layout(10)] private ushort _extOff;
        [Layout(11)] private ushort _extOn;
        [Layout(12)] private ushort _inpClear; //1014
        [Layout(13, Count = 5)] private ushort[] _res; //1015-1019
        [Layout(14)] private ushort _noUacc;
        [Layout(15)] private ushort _inpDisable;
        [Layout(16)] private ushort _inpM12;   //101C

        [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOff
        {
            get { return Validator.Get(_keyOff, StringsConfig.ExternalSignals); }
            set { _keyOff = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "ключ_ВКЛ")]
        public string KeyOn
        {
            get { return Validator.Get(_keyOn, StringsConfig.ExternalSignals); }
            set { _keyOn = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "вход_внеш_выключить")]
        public string ExtOff
        {
            get { return Validator.Get(_extOff, StringsConfig.ExternalSignals); }
            set { _extOff = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "вход_внеш_включить")]
        public string ExtOn
        {
            get { return Validator.Get(_extOn, StringsConfig.ExternalSignals); }
            set { _extOn = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "сброс_сигнализации")]
        public string InpClear
        {
            get { return Validator.Get(_inpClear, StringsConfig.ExternalSignals); }
            set { _inpClear = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Группа1")]
        public string Group1
        {
            get { return Validator.Get(_group1, StringsConfig.ExternalSignals); }
            set
            {
                _group1 = Validator.Set(value, StringsConfig.ExternalSignals);
            }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Группа2")]
        public string Group2
        {
            get { return Validator.Get(_group2, StringsConfig.ExternalSignals); }
            set { _group2 = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Группа3")]
        public string Group3
        {
            get { return Validator.Get(_group3, StringsConfig.ExternalSignals); }
            set { _group3 = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Группа4")]
        public string Group4
        {
            get { return Validator.Get(_group4, StringsConfig.ExternalSignals); }
            set { _group4 = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Группа5")]
        public string Group5
        {
            get { return Validator.Get(_group5, StringsConfig.ExternalSignals); }
            set { _group5 = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Группа6")]
        public string Group6
        {
            get { return Validator.Get(_group6, StringsConfig.ExternalSignals); }
            set { _group6 = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Низкий заряд аккумулятора")]
        public double NoUacc
        {
            get { return ValuesConverterCommon.GetU(_noUacc); }
            set { _noUacc = ValuesConverterCommon.SetU(value); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Запрет команд по СДТУ")]
        public string Disable
        {
            get { return Validator.Get(_inpDisable, StringsConfig.ExternalSignals); }
            set { _inpDisable = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
    }
}
