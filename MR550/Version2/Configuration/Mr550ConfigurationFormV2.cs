using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR550.CommonStructures.Apv;
using BEMN.MR550.CommonStructures.Avr;
using BEMN.MR550.CommonStructures.FaultSignal;
using BEMN.MR550.CommonStructures.Indicators;
using BEMN.MR550.CommonStructures.Ls;
using BEMN.MR550.CommonStructures.Lzsh;
using BEMN.MR550.CommonStructures.Switch;
using BEMN.MR550.CommonStructures.Vls;
using BEMN.MR550.Version2.Configuration.Structures;
using BEMN.MR550.Version2.Configuration.Structures.CurrentDefenses;
using BEMN.MR550.Version2.Configuration.Structures.ExternalDefenses;
using BEMN.MR550.Version2.Configuration.Structures.ExternalSignals;
using BEMN.MR550.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version2.Configuration.Structures.SystenConfig;

namespace BEMN.MR550.Version2.Configuration
{
    
    public partial class Mr550ConfigurationFormV2 : Form, IFormView
    {
        #region [Const]
        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string MR550 = "MR550";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";
        #endregion

        #region [Private fields]
        private readonly Mr550Device _device;
        private readonly MemoryEntity<ConfigurationStructV2> _configuration;
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;
        private ConfigurationStructV2 _currentSetpointsStructV2;

        #region [Validators]
        private NewStructValidator<MeasureTransStruct> _measureTransValidator;
        private NewStructValidator<ExternalSignalStruct> _externalSignalsValidator;
        private NewStructValidator<FaultStruct> _faultValidator;

        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;

        private NewStructValidator<SwitchStruct> _switchValidator;
        private NewStructValidator<ApvStruct> _apvValidator;
        private NewStructValidator<AvrStruct> _avrValidator;
        private NewStructValidator<LpbStruct> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct> _externalDefenseValidatior;
        private NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> _currentDefenseValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> _addCurrentDefenseValidator;
        private SetpointsValidator< AllSetpointsStruct, SetpointStruct> _setpointsValidator;
        private StructUnion<SetpointStruct> _setpointUnion;

        #region [���]
        /// <summary>
        /// ������ ��������� ��� ���
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;
        #endregion [���]
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<SystemConfigStruct> _oscValidator;
        private StructUnion<ConfigurationStructV2> _configurationUnion;
        #endregion [Validators] 
        #endregion [Private fields]


        #region [Ctor's]
        public Mr550ConfigurationFormV2()
        {
            this.InitializeComponent();
        }

        public Mr550ConfigurationFormV2(Mr550Device device)
        {
            this.InitializeComponent();
            this.writeToHtmlItem.Visible = false;
            this._device = device;
            double ver = Common.VersionConverter(this._device.DeviceVersion);

            this._configuration = this._device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadConfigOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);

                });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => this._exchangeProgressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_WRITE_CONFIG;
                MessageBox.Show(ERROR_WRITE_CONFIG);

            });
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {

                this._device.WriteConfiguration();
                this.IsProcess = false;
                this._statusLabel.Text = "������������ ������� ��������";

            });
            this._exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            this._currentSetpointsStructV2 = new ConfigurationStructV2();
            this.Init();
            this._configurationUnion.Set(this._currentSetpointsStructV2);

            if (ver == 1.01)
            {
                this._saveToXmlButton.Enabled = false;
            }
        } 
        private void Init()
        {
            this._lsBoxes = new[]
            {
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals9,
                this._inputSignals10, this._inputSignals11, this._inputSignals12
            };

            this._measureTransValidator = new NewStructValidator<MeasureTransStruct>
                (
                this._toolTip,
                new ControlInfoText(this._TT_Box, RulesContainer.UshortTo1500),
                new ControlInfoText(this._TTNP_Box, RulesContainer.UshortTo100),
                new ControlInfoText(this._maxTok_Box, RulesContainer.Ustavka40)
                 
                );

            this._externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._keyOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._keyOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._extOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._extOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._indicationCombo, StringsConfig.ExternalSignals)  ,
                new ControlInfoCombo(this._constraintGroupCombo1, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo2, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo3, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo4, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo5, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._constraintGroupCombo6, StringsConfig.ExternalSignals),
                new ControlInfoText(this._UaccBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._inpDisableCombo, StringsConfig.ExternalSignals)
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(this._dispepairCheckList)),
                new ControlInfoText(this._dispepairReleDurationBox, RulesContainer.IntTo3M)
                );

            this._inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
              (
              this._lsBoxes[i],
              InputLogicStruct.DISCRETS_COUNT,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            this._inputLogicUnion = new StructUnion<InputLogicSignalStruct>(this._inputLogicValidator);

            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._switcherStateOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherStateOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherErrorCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._switcherBlockCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(this._switcherUrovTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherUrovBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._switcherAccelerationBox, RulesContainer.IntTo3M),
                new ControlInfoCombo(this._manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(this._manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(this._manageSignalsSDTU_Combo, StringsConfig.Zr)
                );

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._APV_ConfigCombo, StringsConfig.ApvModes),
                new ControlInfoCombo(this._APV_BlockingCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(this._APV_BlockingTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_ReadyTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat1TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat2TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat3TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(this._APV_Crat4TimeBox, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._apvStartCheckBox)

                );
            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._avrBySignalCheckBox),
                new ControlInfoCheck(this._avrByOffCheckBox),
                new ControlInfoCheck(this._avrBySelfOffCheckBox),
                new ControlInfoCheck(this._avrByDiffCheckBox),
                new ControlInfoCombo(this._avrSIGNOn, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlocking, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlockClear, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrResolve, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBack, StringsConfig.ExternalSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTBack, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTOff, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._avrClearCheckBox)
                );
            this._lpbValidator = new NewStructValidator<LpbStruct>
                  (
                  this._toolTip,
                  new ControlInfoCombo(this._lzhModes, StringsConfig.BeNo),
                  new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40)
                  );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct>
                (this._externalDefenseGrid,
                8,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff =new[]{new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.Modes[0],true,2,3,4,5,6,7,8,9,10,11,12), 
                        new TurnOffRule(5, false,false, 6, 7, 8)
                        )} 
                };

            var currentDefenseFunc = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._tokDefenseGrid1[5, this._tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                        StringsConfig.FeatureLight[1])
                    {
                        return new CustomUshortRule(0, 4000);
                    }
                    return RulesContainer.IntTo3M;
                }
                catch (Exception)
                {
                    return RulesContainer.IntTo3M;
                }
            });


            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                new[] {this._tokDefenseGrid1, this._tokDefenseGrid2, this._tokDefenseGrid3 },
                new[] {4, 4, 2},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),
                new ColumnInfoCombo(StringsConfig.TokParameter, ColumnsType.COMBO, true, false, false),
                new ColumnInfoText(RulesContainer.Ustavka40,true,true,false),
                new ColumnInfoText(RulesContainer.Ustavka5,false,false,true),
                new ColumnInfoCombo(StringsConfig.FeatureLight, ColumnsType.COMBO, true, false, false),
                new ColumnInfoTextDependent(currentDefenseFunc, true, false, false),
                new ColumnInfoText(RulesContainer.IntTo3M, false, true, true),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscV111)
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(this._tokDefenseGrid1,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10)),
                            new TurnOffDgv(this._tokDefenseGrid2,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10)),
                            new TurnOffDgv(this._tokDefenseGrid3,new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14), new TurnOffRule(9, false,false, 10))
                        }
                };

            this._addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (this._tokDefenseGrid4,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.Modes),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),

                new ColumnInfoText(RulesContainer.Ustavka100),

                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscV111)
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(this._tokDefenseGrid4,
                                new TurnOffRule(1, StringsConfig.Modes[0],true, 2, 3, 4, 5, 6, 7, 8, 9, 10), 
                                new TurnOffRule(5, false, false,6))
                        },
                    AddRules = new[] {new TextboxCellRule(0, 3, RulesContainer.Ustavka5)},
                    Disabled = new[] { new Point(5, 1), new Point(6, 1) }
                };


            this._setpointUnion = new StructUnion<SetpointStruct>
                (
                this._currentDefenseValidatior, this._addCurrentDefenseValidator
                );

            this._setpointsValidator = new SetpointsValidator<AllSetpointsStruct,SetpointStruct>
                (
                new ComboboxSelector(this._setpointsComboBox,StringsConfig.SetpointsNames), this._setpointUnion
                );
            
            #region [���] 
            this._vlsBoxes = new[]
                {
                    this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4, this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8
                };
            this._vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                this._vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i], StringsConfig.VlsSignals);
            }
            this._vlsUnion = new StructUnion<OutputLogicSignalStruct>(this._vlsValidator);
            #endregion [���]

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            this._oscValidator = new NewStructValidator<SystemConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this._oscWriteLength, RulesContainer.Ushort1To100),
                new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                 new ControlInfoCombo(this._oscLength, StringsConfig.OscSize)
                );


            this._configurationUnion = new StructUnion<ConfigurationStructV2>
                (this._measureTransValidator, this._externalSignalsValidator, this._faultValidator, this._inputLogicUnion, this._switchValidator, this._apvValidator, this._avrValidator, this._lpbValidator, this._externalDefenseValidatior, this._setpointsValidator, this._vlsUnion, this._indicatorValidator, this._oscValidator
                );
            if (Common.VersionConverter(this._device.DeviceVersion) > 1.01)
            {
                this._UaccUstavkaGroup.Visible = true;
                this._inpDisableCombo.Visible = this._inpDisableLabel.Visible = true;
            }
        }
        #endregion [Ctor's]

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName, MR550);
            } 
        }

        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName,string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                var configurationStruct = new ConfigurationStructV2();
                configurationStruct.InitStruct(values);
                this._configurationUnion.Set(configurationStruct);
               
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            string message;
            if (this._configurationUnion.Check(out message, true))
            {
                var currentStruct = this._configurationUnion.Get();
                this._saveConfigurationDlg.FileName = string.Format("��550_�������_������ {0:F1}.xml", this._device.DeviceVersion);
                if (this._saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName, currentStruct, MR550);
                }

            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ���������.");
            }  
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetConfigBut.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }


        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Serialize(string binFileName,StructBase config, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));


                var values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS",head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
                return;
            //if (/*!this._device.MB.IsPortInvalid || */this._device.MB.NetworkEnabled)
            //{
                this.StartWriteConfiguration();
            //}
            //else
            //{
                //MessageBox.Show(INVALID_PORT);
            //}
        }
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartReadConfiguration();
        }
    
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartReadConfiguration();
            else
                this._configurationUnion.Reset();
        }

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            this._configurationUnion.Set(this._configuration.Value);
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            //if (!this._device.MB.IsPortInvalid || this._device.MB.NetworkEnabled)
            //{
                this._device.ReadConfiguration();
                this.IsProcess = true;
                this._configuration.LoadStruct();
            //}
           // else
           // {
                //MessageBox.Show(INVALID_PORT);
           // }
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartWriteConfiguration()
        {
            if (MessageBox.Show("�������� ������������ �� 550?", "������ ������������", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this._exchangeProgressBar.Value = 0;
                string message;

                if (this._configurationUnion.Check(out message, true))
                {
                    this._configuration.Value = this._configurationUnion.Get();
                    this._statusLabel.Text = "��� ������ ������������";
                    this.IsProcess = true;
                    this._configuration.SaveStruct();
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                }
            }
        }

        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this._configurationUnion.Reset();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            if (this.WriteConfiguration())
            {
                ExportGroupForm exportGroup = new ExportGroupForm();
                if (exportGroup.ShowDialog() != DialogResult.OK)
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                this._currentSetpointsStructV2.DeviceVersion = this._device.DeviceVersion;
                this._currentSetpointsStructV2.DeviceNumber = this._device.DeviceNumber.ToString();
                this._currentSetpointsStructV2.Group = (int) exportGroup.SelectedGroup;
                string fileName;

                if (exportGroup.SelectedGroup == ExportStruct.ALL)
                {
                    fileName = string.Format("{0} ������� ������ {1} ��� ������", "��550", this._device.DeviceVersion);
                    this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR550MainV1_XX, fileName, this._currentSetpointsStructV2);
                }
                else
                {
                    fileName = string.Format("{0} ������� ������ {1} ������{2}", "��550", this._device.DeviceVersion, this._currentSetpointsStructV2.Group);
                    this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR550MainV1_XX, fileName, this._currentSetpointsStructV2);
                }
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (this._configurationUnion.Check(out message, true))
            {
                this._currentSetpointsStructV2 = this._configurationUnion.Get();
                return true;
            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                return false;
            }
        }
        #endregion Misc

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr550Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Version1.Configuration.Mr550ConfigurationFormV1); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configurationUnion.Reset();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void Mr550ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
