﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR550.Version2.Measuring.Structures
{
    /// <summary>
    /// МР 550 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct :StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 28;

        #endregion [Constants]


        #region [Private fields]

        [Layout(0, Count = BASE_SIZE)]
        private ushort[] _base; //бд общаяя

        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                return Common.GetBitsArray(this._base, 144, 151);
            }
        }
        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get
            {
                return Common.GetBitsArray(this._base, 160, 167);
            }
        }
        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(this._base, 168, 175); }
        }
        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(this._base, 224, 231); }
        }

        /// <summary>
        /// Защиты I
        /// </summary>
        public bool[] MaximumCurrent
        {
            get { return Common.GetBitsArray(this._base, 176, 199); }
        }
        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(this._base, 232, 255); }
        } 
        /// <summary>
        /// Реле
        /// </summary>
        public bool[] FaultSignals
        {
            get { return Common.GetBitsArray(this._base, 80, 95); }
        }
        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] Indicators
        {
            get { return Common.GetBitsArray(this._base, 40, 47); }
        } 
        /// <summary>
        /// Автоматика
        /// </summary>
        public bool[] Automatics
        {
            get { return Common.GetBitsArray(this._base, 136, 143); }
        }
        
        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get { return Common.GetBitsArray(this._base, 64, 71); }
        }

        public bool[] Manage
        {
            get
            {
                return new bool[]
                {
                    Common.GetBit(this._base[0], 0),
                    Common.GetBit(this._base[0], 1),
                    Common.GetBit(this._base[0], 6),
                    Common.GetBit(this._base[0], 7),
                    Common.GetBit(this._base[0], 2),
                };
            }
        }

        public bool WorkingLogic
        {
            get { return Common.GetBit(this._base[0], 9) && !Common.GetBit(this._base[5], 15); }
        }
        #endregion [Properties]
      

    }
}
