﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version2.Measuring.Structures
{
    public class GroupSetpointBaseStruct : StructBase
    {
        [Layout(0)] private ushort _group;
        public string GetGroup(double version)
        {
            if (version < 1.01)
            {
                return Validator.Get(this._group, StringsConfig.SetpointsNamesV1);
            }
            return Validator.Get(this._group, StringsConfig.SetpointsNames);
        }

        public void SetGroup(double version, string value)
        {
            if (version < 1.01)
            {
                this._group = Validator.Set(value, StringsConfig.SetpointsNamesV1);
            }
            else
            {
                this._group = Validator.Set(value, StringsConfig.SetpointsNames);
            }
        }
    }
}
