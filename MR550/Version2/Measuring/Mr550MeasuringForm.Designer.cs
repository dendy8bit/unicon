﻿namespace BEMN.MR550.Version2.Measuring
{
    partial class Mr550MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this._analogTabPage = new System.Windows.Forms.TabPage();
            this.UaccGroup = new System.Windows.Forms.GroupBox();
            this._UaccBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._i0TextBox = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this._icTextBox = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this._igTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._i2TextBox = new System.Windows.Forms.TextBox();
            this._inTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._ibTextBox = new System.Windows.Forms.TextBox();
            this._i1TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._iaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._discretTabPage = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this._faultSignalLed16 = new BEMN.Forms.LedControl();
            this.label11 = new System.Windows.Forms.Label();
            this._faultSignalLed15 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._faultSignalLed14 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._faultSignalLed13 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._faultSignalLed12 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._faultSignalLed11 = new BEMN.Forms.LedControl();
            this.label135 = new System.Windows.Forms.Label();
            this._faultSignalLed10 = new BEMN.Forms.LedControl();
            this.label136 = new System.Windows.Forms.Label();
            this._faultSignalLed9 = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this._faultSignalLed8 = new BEMN.Forms.LedControl();
            this.label17 = new System.Windows.Forms.Label();
            this._faultSignalLed7 = new BEMN.Forms.LedControl();
            this.label19 = new System.Windows.Forms.Label();
            this._faultSignalLed6 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._faultSignalLed5 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._faultSignalLed4 = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._faultSignalLed3 = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._faultSignalLed2 = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._faultSignalLed1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label113 = new System.Windows.Forms.Label();
            this._faultStateLed8 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._faultStateLed7 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._faultStateLed6 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._faultStateLed5 = new BEMN.Forms.LedControl();
            this.noUaccLabel = new System.Windows.Forms.Label();
            this._faultStateLed4 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._faultStateLed3 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._faultStateLed2 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._faultStateLed1 = new BEMN.Forms.LedControl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label97 = new System.Windows.Forms.Label();
            this._autoLed8 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._autoLed7 = new BEMN.Forms.LedControl();
            this.label99 = new System.Windows.Forms.Label();
            this._autoLed6 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._autoLed5 = new BEMN.Forms.LedControl();
            this.label9 = new System.Windows.Forms.Label();
            this._autoLed4 = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._autoLed3 = new BEMN.Forms.LedControl();
            this.label103 = new System.Windows.Forms.Label();
            this._autoLed2 = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this._autoLed1 = new BEMN.Forms.LedControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label270 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this._iS1 = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i7Io = new BEMN.Forms.LedControl();
            this.label83 = new System.Windows.Forms.Label();
            this._iS2 = new BEMN.Forms.LedControl();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i1 = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this._iS1Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._iS3 = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i3Io = new BEMN.Forms.LedControl();
            this._iS2Io = new BEMN.Forms.LedControl();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i7 = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._i5Io = new BEMN.Forms.LedControl();
            this._iS4 = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this._iS3Io = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._iS4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indicator6 = new BEMN.Forms.LedControl();
            this.label189 = new System.Windows.Forms.Label();
            this._indicator8 = new BEMN.Forms.LedControl();
            this.label181 = new System.Windows.Forms.Label();
            this._indicator7 = new BEMN.Forms.LedControl();
            this.label182 = new System.Windows.Forms.Label();
            this._indicator5 = new BEMN.Forms.LedControl();
            this.label184 = new System.Windows.Forms.Label();
            this._indicator4 = new BEMN.Forms.LedControl();
            this.label185 = new System.Windows.Forms.Label();
            this._indicator3 = new BEMN.Forms.LedControl();
            this.label186 = new System.Windows.Forms.Label();
            this._indicator2 = new BEMN.Forms.LedControl();
            this.label187 = new System.Windows.Forms.Label();
            this._indicator1 = new BEMN.Forms.LedControl();
            this.label188 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._d8 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._d7 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._d1 = new BEMN.Forms.LedControl();
            this._d6 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._d5 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this._setpointLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._spl = new BEMN.Forms.LedControl();
            this.label158 = new System.Windows.Forms.Label();
            this._offSplBtn = new System.Windows.Forms.Button();
            this._onSplBtn = new System.Windows.Forms.Button();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.label100 = new System.Windows.Forms.Label();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._manageLed2 = new BEMN.Forms.LedControl();
            this._manageLed1 = new BEMN.Forms.LedControl();
            this._manageLed6 = new BEMN.Forms.LedControl();
            this._manageLed4 = new BEMN.Forms.LedControl();
            this._manageLed3 = new BEMN.Forms.LedControl();
            this._resetAnButton = new System.Windows.Forms.Button();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._dataBaseTabControl.SuspendLayout();
            this._analogTabPage.SuspendLayout();
            this.UaccGroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._discretTabPage.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dataBaseTabControl.Controls.Add(this._analogTabPage);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(846, 494);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // _analogTabPage
            // 
            this._analogTabPage.Controls.Add(this.UaccGroup);
            this._analogTabPage.Controls.Add(this._dateTimeControl);
            this._analogTabPage.Controls.Add(this.groupBox1);
            this._analogTabPage.Location = new System.Drawing.Point(4, 22);
            this._analogTabPage.Name = "_analogTabPage";
            this._analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogTabPage.Size = new System.Drawing.Size(838, 468);
            this._analogTabPage.TabIndex = 0;
            this._analogTabPage.Text = "Аналоговая БД";
            this._analogTabPage.UseVisualStyleBackColor = true;
            // 
            // UaccGroup
            // 
            this.UaccGroup.Controls.Add(this._UaccBox);
            this.UaccGroup.Controls.Add(this.label20);
            this.UaccGroup.Location = new System.Drawing.Point(247, 10);
            this.UaccGroup.Name = "UaccGroup";
            this.UaccGroup.Size = new System.Drawing.Size(108, 61);
            this.UaccGroup.TabIndex = 44;
            this.UaccGroup.TabStop = false;
            this.UaccGroup.Text = "Напряжение аккумулятора";
            this.UaccGroup.Visible = false;
            // 
            // _UaccBox
            // 
            this._UaccBox.Location = new System.Drawing.Point(38, 35);
            this._UaccBox.Name = "_UaccBox";
            this._UaccBox.ReadOnly = true;
            this._UaccBox.Size = new System.Drawing.Size(61, 20);
            this._UaccBox.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(26, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Uпт";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(6, 148);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 43;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._i0TextBox);
            this.groupBox1.Controls.Add(this.label79);
            this.groupBox1.Controls.Add(this._icTextBox);
            this.groupBox1.Controls.Add(this.label80);
            this.groupBox1.Controls.Add(this._igTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._i2TextBox);
            this.groupBox1.Controls.Add(this._inTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._ibTextBox);
            this.groupBox1.Controls.Add(this._i1TextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._iaTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(235, 132);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Токи";
            // 
            // _i0TextBox
            // 
            this._i0TextBox.Location = new System.Drawing.Point(149, 71);
            this._i0TextBox.Name = "_i0TextBox";
            this._i0TextBox.ReadOnly = true;
            this._i0TextBox.Size = new System.Drawing.Size(61, 20);
            this._i0TextBox.TabIndex = 6;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(127, 74);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(16, 13);
            this.label79.TabIndex = 4;
            this.label79.Text = "I0";
            // 
            // _icTextBox
            // 
            this._icTextBox.Location = new System.Drawing.Point(38, 71);
            this._icTextBox.Name = "_icTextBox";
            this._icTextBox.ReadOnly = true;
            this._icTextBox.Size = new System.Drawing.Size(61, 20);
            this._icTextBox.TabIndex = 7;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(16, 74);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(16, 13);
            this.label80.TabIndex = 5;
            this.label80.Text = "Ic";
            // 
            // _igTextBox
            // 
            this._igTextBox.Location = new System.Drawing.Point(149, 97);
            this._igTextBox.Name = "_igTextBox";
            this._igTextBox.ReadOnly = true;
            this._igTextBox.Size = new System.Drawing.Size(61, 20);
            this._igTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Iг";
            // 
            // _i2TextBox
            // 
            this._i2TextBox.Location = new System.Drawing.Point(149, 45);
            this._i2TextBox.Name = "_i2TextBox";
            this._i2TextBox.ReadOnly = true;
            this._i2TextBox.Size = new System.Drawing.Size(61, 20);
            this._i2TextBox.TabIndex = 3;
            // 
            // _inTextBox
            // 
            this._inTextBox.Location = new System.Drawing.Point(38, 97);
            this._inTextBox.Name = "_inTextBox";
            this._inTextBox.ReadOnly = true;
            this._inTextBox.Size = new System.Drawing.Size(61, 20);
            this._inTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "I2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "In";
            // 
            // _ibTextBox
            // 
            this._ibTextBox.Location = new System.Drawing.Point(38, 45);
            this._ibTextBox.Name = "_ibTextBox";
            this._ibTextBox.ReadOnly = true;
            this._ibTextBox.Size = new System.Drawing.Size(61, 20);
            this._ibTextBox.TabIndex = 3;
            // 
            // _i1TextBox
            // 
            this._i1TextBox.Location = new System.Drawing.Point(149, 19);
            this._i1TextBox.Name = "_i1TextBox";
            this._i1TextBox.ReadOnly = true;
            this._i1TextBox.Size = new System.Drawing.Size(61, 20);
            this._i1TextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ib";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(127, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "I1";
            // 
            // _iaTextBox
            // 
            this._iaTextBox.Location = new System.Drawing.Point(38, 19);
            this._iaTextBox.Name = "_iaTextBox";
            this._iaTextBox.ReadOnly = true;
            this._iaTextBox.Size = new System.Drawing.Size(61, 20);
            this._iaTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ia";
            // 
            // _discretTabPage
            // 
            this._discretTabPage.Controls.Add(this.groupBox19);
            this._discretTabPage.Controls.Add(this.groupBox2);
            this._discretTabPage.Controls.Add(this.groupBox3);
            this._discretTabPage.Controls.Add(this.groupBox15);
            this._discretTabPage.Controls.Add(this.groupBox11);
            this._discretTabPage.Controls.Add(this.groupBox21);
            this._discretTabPage.Controls.Add(this.groupBox12);
            this._discretTabPage.Controls.Add(this.groupBox18);
            this._discretTabPage.Controls.Add(this.groupBox10);
            this._discretTabPage.Controls.Add(this.groupBox9);
            this._discretTabPage.Controls.Add(this.groupBox6);
            this._discretTabPage.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage.Name = "_discretTabPage";
            this._discretTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage.Size = new System.Drawing.Size(838, 468);
            this._discretTabPage.TabIndex = 1;
            this._discretTabPage.Text = "Дискретная БД";
            this._discretTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label10);
            this.groupBox19.Controls.Add(this._faultSignalLed16);
            this.groupBox19.Controls.Add(this.label11);
            this.groupBox19.Controls.Add(this._faultSignalLed15);
            this.groupBox19.Controls.Add(this.label12);
            this.groupBox19.Controls.Add(this._faultSignalLed14);
            this.groupBox19.Controls.Add(this.label13);
            this.groupBox19.Controls.Add(this._faultSignalLed13);
            this.groupBox19.Controls.Add(this.label14);
            this.groupBox19.Controls.Add(this._faultSignalLed12);
            this.groupBox19.Controls.Add(this.label15);
            this.groupBox19.Controls.Add(this._faultSignalLed11);
            this.groupBox19.Controls.Add(this.label135);
            this.groupBox19.Controls.Add(this._faultSignalLed10);
            this.groupBox19.Controls.Add(this.label136);
            this.groupBox19.Controls.Add(this._faultSignalLed9);
            this.groupBox19.Location = new System.Drawing.Point(327, 203);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(131, 243);
            this.groupBox19.TabIndex = 27;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Сигналы неисправности 2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 218);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "О. задачи логики";
            // 
            // _faultSignalLed16
            // 
            this._faultSignalLed16.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed16.Name = "_faultSignalLed16";
            this._faultSignalLed16.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed16.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed16.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 193);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "О.осциллографа";
            // 
            // _faultSignalLed15
            // 
            this._faultSignalLed15.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed15.Name = "_faultSignalLed15";
            this._faultSignalLed15.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed15.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed15.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 166);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "О. часов";
            // 
            // _faultSignalLed14
            // 
            this._faultSignalLed14.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed14.Name = "_faultSignalLed14";
            this._faultSignalLed14.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed14.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed14.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "О. ЖА";
            // 
            // _faultSignalLed13
            // 
            this._faultSignalLed13.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed13.Name = "_faultSignalLed13";
            this._faultSignalLed13.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed13.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed13.TabIndex = 24;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 114);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "О. ЖС";
            // 
            // _faultSignalLed12
            // 
            this._faultSignalLed12.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed12.Name = "_faultSignalLed12";
            this._faultSignalLed12.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed12.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed12.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(21, 63);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "О. коэфф. АЦП";
            // 
            // _faultSignalLed11
            // 
            this._faultSignalLed11.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed11.Name = "_faultSignalLed11";
            this._faultSignalLed11.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed11.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed11.TabIndex = 20;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(21, 89);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(99, 13);
            this.label135.TabIndex = 19;
            this.label135.Text = "О. размера ППЗУ";
            // 
            // _faultSignalLed10
            // 
            this._faultSignalLed10.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed10.Name = "_faultSignalLed10";
            this._faultSignalLed10.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed10.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed10.TabIndex = 18;
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(21, 37);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(90, 13);
            this.label136.TabIndex = 17;
            this.label136.Text = "Ошибка уставок";
            // 
            // _faultSignalLed9
            // 
            this._faultSignalLed9.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed9.Name = "_faultSignalLed9";
            this._faultSignalLed9.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed9.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed9.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this._faultSignalLed8);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this._faultSignalLed7);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._faultSignalLed6);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this._faultSignalLed5);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this._faultSignalLed4);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this._faultSignalLed3);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this._faultSignalLed2);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this._faultSignalLed1);
            this.groupBox2.Location = new System.Drawing.Point(195, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 243);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Сигналы неисправности 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 219);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Резерв";
            // 
            // _faultSignalLed8
            // 
            this._faultSignalLed8.Location = new System.Drawing.Point(6, 219);
            this._faultSignalLed8.Name = "_faultSignalLed8";
            this._faultSignalLed8.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed8.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed8.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(21, 193);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 29;
            this.label17.Text = "МСД";
            // 
            // _faultSignalLed7
            // 
            this._faultSignalLed7.Location = new System.Drawing.Point(6, 193);
            this._faultSignalLed7.Name = "_faultSignalLed7";
            this._faultSignalLed7.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed7.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed7.TabIndex = 28;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 166);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "Резерв";
            // 
            // _faultSignalLed6
            // 
            this._faultSignalLed6.Location = new System.Drawing.Point(6, 167);
            this._faultSignalLed6.Name = "_faultSignalLed6";
            this._faultSignalLed6.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed6.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed6.TabIndex = 26;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(21, 140);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(50, 13);
            this.label31.TabIndex = 25;
            this.label31.Text = "О. МСА I";
            // 
            // _faultSignalLed5
            // 
            this._faultSignalLed5.Location = new System.Drawing.Point(6, 141);
            this._faultSignalLed5.Name = "_faultSignalLed5";
            this._faultSignalLed5.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed5.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed5.TabIndex = 24;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(21, 114);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "Резерв";
            // 
            // _faultSignalLed4
            // 
            this._faultSignalLed4.Location = new System.Drawing.Point(6, 115);
            this._faultSignalLed4.Name = "_faultSignalLed4";
            this._faultSignalLed4.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed4.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed4.TabIndex = 22;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 63);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(70, 13);
            this.label33.TabIndex = 21;
            this.label33.Text = "О.  шины I2c";
            // 
            // _faultSignalLed3
            // 
            this._faultSignalLed3.Location = new System.Drawing.Point(6, 89);
            this._faultSignalLed3.Name = "_faultSignalLed3";
            this._faultSignalLed3.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed3.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed3.TabIndex = 20;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(21, 89);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(88, 13);
            this.label34.TabIndex = 19;
            this.label34.Text = "О. температуры";
            // 
            // _faultSignalLed2
            // 
            this._faultSignalLed2.Location = new System.Drawing.Point(6, 63);
            this._faultSignalLed2.Name = "_faultSignalLed2";
            this._faultSignalLed2.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed2.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed2.TabIndex = 18;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(21, 37);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Ошибка ОЗУ";
            // 
            // _faultSignalLed1
            // 
            this._faultSignalLed1.Location = new System.Drawing.Point(6, 37);
            this._faultSignalLed1.Name = "_faultSignalLed1";
            this._faultSignalLed1.Size = new System.Drawing.Size(13, 13);
            this._faultSignalLed1.State = BEMN.Forms.LedState.Off;
            this._faultSignalLed1.TabIndex = 16;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label113);
            this.groupBox3.Controls.Add(this._faultStateLed8);
            this.groupBox3.Controls.Add(this.label114);
            this.groupBox3.Controls.Add(this._faultStateLed7);
            this.groupBox3.Controls.Add(this.label115);
            this.groupBox3.Controls.Add(this._faultStateLed6);
            this.groupBox3.Controls.Add(this.label116);
            this.groupBox3.Controls.Add(this._faultStateLed5);
            this.groupBox3.Controls.Add(this.noUaccLabel);
            this.groupBox3.Controls.Add(this._faultStateLed4);
            this.groupBox3.Controls.Add(this.label118);
            this.groupBox3.Controls.Add(this._faultStateLed3);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this._faultStateLed2);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this._faultStateLed1);
            this.groupBox3.Location = new System.Drawing.Point(13, 203);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(159, 243);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Сигнал срабатывания реле неисправности";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(21, 218);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(44, 13);
            this.label113.TabIndex = 31;
            this.label113.Text = "Резерв";
            // 
            // _faultStateLed8
            // 
            this._faultStateLed8.Location = new System.Drawing.Point(6, 219);
            this._faultStateLed8.Name = "_faultStateLed8";
            this._faultStateLed8.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed8.State = BEMN.Forms.LedState.Off;
            this._faultStateLed8.TabIndex = 30;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(21, 191);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(44, 13);
            this.label114.TabIndex = 29;
            this.label114.Text = "Резерв";
            // 
            // _faultStateLed7
            // 
            this._faultStateLed7.Location = new System.Drawing.Point(6, 193);
            this._faultStateLed7.Name = "_faultStateLed7";
            this._faultStateLed7.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed7.State = BEMN.Forms.LedState.Off;
            this._faultStateLed7.TabIndex = 28;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(21, 166);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(91, 13);
            this.label115.TabIndex = 27;
            this.label115.Text = "Измерения тока";
            // 
            // _faultStateLed6
            // 
            this._faultStateLed6.Location = new System.Drawing.Point(6, 167);
            this._faultStateLed6.Name = "_faultStateLed6";
            this._faultStateLed6.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed6.State = BEMN.Forms.LedState.Off;
            this._faultStateLed6.TabIndex = 26;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(21, 140);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(76, 13);
            this.label116.TabIndex = 25;
            this.label116.Text = "Выключателя";
            // 
            // _faultStateLed5
            // 
            this._faultStateLed5.Location = new System.Drawing.Point(6, 141);
            this._faultStateLed5.Name = "_faultStateLed5";
            this._faultStateLed5.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed5.State = BEMN.Forms.LedState.Off;
            this._faultStateLed5.TabIndex = 24;
            // 
            // noUaccLabel
            // 
            this.noUaccLabel.AutoSize = true;
            this.noUaccLabel.Location = new System.Drawing.Point(21, 114);
            this.noUaccLabel.Name = "noUaccLabel";
            this.noUaccLabel.Size = new System.Drawing.Size(44, 13);
            this.noUaccLabel.TabIndex = 23;
            this.noUaccLabel.Text = "Резерв";
            // 
            // _faultStateLed4
            // 
            this._faultStateLed4.Location = new System.Drawing.Point(6, 115);
            this._faultStateLed4.Name = "_faultStateLed4";
            this._faultStateLed4.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed4.State = BEMN.Forms.LedState.Off;
            this._faultStateLed4.TabIndex = 22;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(21, 63);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(44, 13);
            this.label118.TabIndex = 21;
            this.label118.Text = "Логики";
            // 
            // _faultStateLed3
            // 
            this._faultStateLed3.Location = new System.Drawing.Point(6, 89);
            this._faultStateLed3.Name = "_faultStateLed3";
            this._faultStateLed3.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed3.State = BEMN.Forms.LedState.Off;
            this._faultStateLed3.TabIndex = 20;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(21, 89);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 13);
            this.label36.TabIndex = 19;
            this.label36.Text = "Программная";
            // 
            // _faultStateLed2
            // 
            this._faultStateLed2.Location = new System.Drawing.Point(6, 63);
            this._faultStateLed2.Name = "_faultStateLed2";
            this._faultStateLed2.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed2.State = BEMN.Forms.LedState.Off;
            this._faultStateLed2.TabIndex = 18;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(21, 37);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(67, 13);
            this.label37.TabIndex = 17;
            this.label37.Text = "Аппаратная";
            // 
            // _faultStateLed1
            // 
            this._faultStateLed1.Location = new System.Drawing.Point(6, 37);
            this._faultStateLed1.Name = "_faultStateLed1";
            this._faultStateLed1.Size = new System.Drawing.Size(13, 13);
            this._faultStateLed1.State = BEMN.Forms.LedState.Off;
            this._faultStateLed1.TabIndex = 16;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label97);
            this.groupBox15.Controls.Add(this._autoLed8);
            this.groupBox15.Controls.Add(this.label7);
            this.groupBox15.Controls.Add(this._autoLed7);
            this.groupBox15.Controls.Add(this.label99);
            this.groupBox15.Controls.Add(this._autoLed6);
            this.groupBox15.Controls.Add(this.label8);
            this.groupBox15.Controls.Add(this._autoLed5);
            this.groupBox15.Controls.Add(this.label9);
            this.groupBox15.Controls.Add(this._autoLed4);
            this.groupBox15.Controls.Add(this.label102);
            this.groupBox15.Controls.Add(this._autoLed3);
            this.groupBox15.Controls.Add(this.label103);
            this.groupBox15.Controls.Add(this._autoLed2);
            this.groupBox15.Controls.Add(this.label104);
            this.groupBox15.Controls.Add(this._autoLed1);
            this.groupBox15.Location = new System.Drawing.Point(516, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(180, 137);
            this.groupBox15.TabIndex = 24;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Автоматика";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(35, 120);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(63, 13);
            this.label97.TabIndex = 31;
            this.label97.Text = "Ускорение";
            // 
            // _autoLed8
            // 
            this._autoLed8.Location = new System.Drawing.Point(8, 120);
            this._autoLed8.Name = "_autoLed8";
            this._autoLed8.Size = new System.Drawing.Size(13, 13);
            this._autoLed8.State = BEMN.Forms.LedState.Off;
            this._autoLed8.TabIndex = 30;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Включение выключателя";
            // 
            // _autoLed7
            // 
            this._autoLed7.Location = new System.Drawing.Point(8, 105);
            this._autoLed7.Name = "_autoLed7";
            this._autoLed7.Size = new System.Drawing.Size(13, 13);
            this._autoLed7.State = BEMN.Forms.LedState.Off;
            this._autoLed7.TabIndex = 28;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(35, 90);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(76, 13);
            this.label99.TabIndex = 27;
            this.label99.Text = "Работа УРОВ";
            // 
            // _autoLed6
            // 
            this._autoLed6.Location = new System.Drawing.Point(8, 90);
            this._autoLed6.Name = "_autoLed6";
            this._autoLed6.Size = new System.Drawing.Size(13, 13);
            this._autoLed6.State = BEMN.Forms.LedState.Off;
            this._autoLed6.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Работа ЛЗШ";
            // 
            // _autoLed5
            // 
            this._autoLed5.Location = new System.Drawing.Point(8, 75);
            this._autoLed5.Name = "_autoLed5";
            this._autoLed5.Size = new System.Drawing.Size(13, 13);
            this._autoLed5.State = BEMN.Forms.LedState.Off;
            this._autoLed5.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Резерв";
            // 
            // _autoLed4
            // 
            this._autoLed4.Location = new System.Drawing.Point(8, 60);
            this._autoLed4.Name = "_autoLed4";
            this._autoLed4.Size = new System.Drawing.Size(13, 13);
            this._autoLed4.State = BEMN.Forms.LedState.Off;
            this._autoLed4.TabIndex = 22;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(35, 45);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(91, 13);
            this.label102.TabIndex = 21;
            this.label102.Text = "АВР блокировка";
            // 
            // _autoLed3
            // 
            this._autoLed3.Location = new System.Drawing.Point(8, 45);
            this._autoLed3.Name = "_autoLed3";
            this._autoLed3.Size = new System.Drawing.Size(13, 13);
            this._autoLed3.State = BEMN.Forms.LedState.Off;
            this._autoLed3.TabIndex = 20;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(35, 30);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(123, 13);
            this.label103.TabIndex = 19;
            this.label103.Text = "АВР отключить резерв";
            // 
            // _autoLed2
            // 
            this._autoLed2.Location = new System.Drawing.Point(8, 30);
            this._autoLed2.Name = "_autoLed2";
            this._autoLed2.Size = new System.Drawing.Size(13, 13);
            this._autoLed2.State = BEMN.Forms.LedState.Off;
            this._autoLed2.TabIndex = 18;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(35, 15);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(118, 13);
            this.label104.TabIndex = 17;
            this.label104.Text = "АВР включить резерв";
            // 
            // _autoLed1
            // 
            this._autoLed1.Location = new System.Drawing.Point(8, 15);
            this._autoLed1.Name = "_autoLed1";
            this._autoLed1.Size = new System.Drawing.Size(13, 13);
            this._autoLed1.State = BEMN.Forms.LedState.Off;
            this._autoLed1.TabIndex = 16;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this.label86);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this.label85);
            this.groupBox11.Controls.Add(this._iS1);
            this.groupBox11.Controls.Add(this.label84);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i7Io);
            this.groupBox11.Controls.Add(this.label83);
            this.groupBox11.Controls.Add(this._iS2);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this._iS1Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._iS3);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._iS2Io);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i7);
            this.groupBox11.Controls.Add(this.label88);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._iS4);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this._iS3Io);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._iS4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(702, 189);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(97, 268);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I";
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(47, 187);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(22, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "In>";
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(47, 206);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "In>>";
            // 
            // _iS1
            // 
            this._iS1.Location = new System.Drawing.Point(28, 111);
            this._iS1.Name = "_iS1";
            this._iS1.Size = new System.Drawing.Size(13, 13);
            this._iS1.State = BEMN.Forms.LedState.Off;
            this._iS1.TabIndex = 17;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(47, 225);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(15, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "Iг";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 92);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i7Io
            // 
            this._i7Io.Location = new System.Drawing.Point(9, 73);
            this._i7Io.Name = "_i7Io";
            this._i7Io.Size = new System.Drawing.Size(13, 13);
            this._i7Io.State = BEMN.Forms.LedState.Off;
            this._i7Io.TabIndex = 13;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(47, 244);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(30, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I2/I1";
            // 
            // _iS2
            // 
            this._iS2.Location = new System.Drawing.Point(28, 130);
            this._iS2.Name = "_iS2";
            this._iS2.Size = new System.Drawing.Size(13, 13);
            this._iS2.State = BEMN.Forms.LedState.Off;
            this._iS2.TabIndex = 19;
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 187);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 187);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 92);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 206);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // _iS1Io
            // 
            this._iS1Io.Location = new System.Drawing.Point(9, 111);
            this._iS1Io.Name = "_iS1Io";
            this._iS1Io.Size = new System.Drawing.Size(13, 13);
            this._iS1Io.State = BEMN.Forms.LedState.Off;
            this._iS1Io.TabIndex = 17;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 225);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 168);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(28, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I0>>";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 206);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _iS3
            // 
            this._iS3.Location = new System.Drawing.Point(28, 149);
            this._iS3.Name = "_iS3";
            this._iS3.Size = new System.Drawing.Size(13, 13);
            this._iS3.State = BEMN.Forms.LedState.Off;
            this._iS3.TabIndex = 21;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 244);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 54);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 225);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _iS2Io
            // 
            this._iS2Io.Location = new System.Drawing.Point(9, 130);
            this._iS2Io.Name = "_iS2Io";
            this._iS2Io.Size = new System.Drawing.Size(13, 13);
            this._iS2Io.State = BEMN.Forms.LedState.Off;
            this._iS2Io.TabIndex = 19;
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 244);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i7
            // 
            this._i7.Location = new System.Drawing.Point(28, 73);
            this._i7.Name = "_i7";
            this._i7.Size = new System.Drawing.Size(13, 13);
            this._i7.State = BEMN.Forms.LedState.Off;
            this._i7.TabIndex = 13;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 149);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(22, 13);
            this.label88.TabIndex = 12;
            this.label88.Text = "I0>";
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 35);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _iS4
            // 
            this._iS4.Location = new System.Drawing.Point(28, 168);
            this._iS4.Name = "_iS4";
            this._iS4.Size = new System.Drawing.Size(13, 13);
            this._iS4.State = BEMN.Forms.LedState.Off;
            this._iS4.TabIndex = 23;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 54);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // _iS3Io
            // 
            this._iS3Io.Location = new System.Drawing.Point(9, 149);
            this._iS3Io.Name = "_iS3Io";
            this._iS3Io.Size = new System.Drawing.Size(13, 13);
            this._iS3Io.State = BEMN.Forms.LedState.Off;
            this._iS3Io.TabIndex = 21;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 130);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(28, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I2>>";
            // 
            // _iS4Io
            // 
            this._iS4Io.Location = new System.Drawing.Point(9, 168);
            this._iS4Io.Name = "_iS4Io";
            this._iS4Io.Size = new System.Drawing.Size(13, 13);
            this._iS4Io.State = BEMN.Forms.LedState.Off;
            this._iS4Io.TabIndex = 23;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 35);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 111);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I2>";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 92);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(34, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>>>>";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 73);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>>>";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 54);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>>";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(16, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>";
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._ssl24);
            this.groupBox21.Controls.Add(this.label245);
            this.groupBox21.Controls.Add(this._ssl23);
            this.groupBox21.Controls.Add(this.label246);
            this.groupBox21.Controls.Add(this._ssl22);
            this.groupBox21.Controls.Add(this.label247);
            this.groupBox21.Controls.Add(this._ssl21);
            this.groupBox21.Controls.Add(this.label248);
            this.groupBox21.Controls.Add(this._ssl20);
            this.groupBox21.Controls.Add(this.label249);
            this.groupBox21.Controls.Add(this._ssl19);
            this.groupBox21.Controls.Add(this.label250);
            this.groupBox21.Controls.Add(this._ssl18);
            this.groupBox21.Controls.Add(this.label251);
            this.groupBox21.Controls.Add(this._ssl17);
            this.groupBox21.Controls.Add(this.label252);
            this.groupBox21.Controls.Add(this._ssl16);
            this.groupBox21.Controls.Add(this.label253);
            this.groupBox21.Controls.Add(this._ssl15);
            this.groupBox21.Controls.Add(this.label254);
            this.groupBox21.Controls.Add(this._ssl14);
            this.groupBox21.Controls.Add(this.label255);
            this.groupBox21.Controls.Add(this._ssl13);
            this.groupBox21.Controls.Add(this.label256);
            this.groupBox21.Controls.Add(this._ssl12);
            this.groupBox21.Controls.Add(this.label257);
            this.groupBox21.Controls.Add(this._ssl11);
            this.groupBox21.Controls.Add(this.label258);
            this.groupBox21.Controls.Add(this._ssl10);
            this.groupBox21.Controls.Add(this.label259);
            this.groupBox21.Controls.Add(this._ssl9);
            this.groupBox21.Controls.Add(this.label260);
            this.groupBox21.Controls.Add(this._ssl8);
            this.groupBox21.Controls.Add(this.label261);
            this.groupBox21.Controls.Add(this._ssl7);
            this.groupBox21.Controls.Add(this.label262);
            this.groupBox21.Controls.Add(this._ssl6);
            this.groupBox21.Controls.Add(this.label263);
            this.groupBox21.Controls.Add(this._ssl5);
            this.groupBox21.Controls.Add(this.label264);
            this.groupBox21.Controls.Add(this._ssl4);
            this.groupBox21.Controls.Add(this.label265);
            this.groupBox21.Controls.Add(this._ssl3);
            this.groupBox21.Controls.Add(this.label266);
            this.groupBox21.Controls.Add(this._ssl2);
            this.groupBox21.Controls.Add(this.label267);
            this.groupBox21.Controls.Add(this._ssl1);
            this.groupBox21.Controls.Add(this.label268);
            this.groupBox21.Location = new System.Drawing.Point(477, 207);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(219, 175);
            this.groupBox21.TabIndex = 14;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Сигналы СП-логики";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(142, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(161, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(142, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(161, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(142, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(161, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(142, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(161, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(142, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(162, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(142, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(161, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(142, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(161, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(142, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(161, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(71, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(90, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(71, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(90, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(71, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(90, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(71, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(90, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(71, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(90, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(71, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(90, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(71, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(90, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(71, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(90, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(702, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(122, 177);
            this.groupBox12.TabIndex = 5;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(30, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВЗ-8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(30, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВЗ-7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(30, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВЗ-6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(30, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВЗ-5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(30, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВЗ-4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(30, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВЗ-3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(30, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВЗ-2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(30, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВЗ-1";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indicator6);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this._indicator8);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this._indicator7);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this._indicator5);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this._indicator4);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this._indicator3);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this._indicator2);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this._indicator1);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(6, 6);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(65, 179);
            this.groupBox18.TabIndex = 11;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // _indicator6
            // 
            this._indicator6.Location = new System.Drawing.Point(6, 114);
            this._indicator6.Name = "_indicator6";
            this._indicator6.Size = new System.Drawing.Size(13, 13);
            this._indicator6.State = BEMN.Forms.LedState.Off;
            this._indicator6.TabIndex = 27;
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 114);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // _indicator8
            // 
            this._indicator8.Location = new System.Drawing.Point(6, 152);
            this._indicator8.Name = "_indicator8";
            this._indicator8.Size = new System.Drawing.Size(13, 13);
            this._indicator8.State = BEMN.Forms.LedState.Off;
            this._indicator8.TabIndex = 21;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(25, 152);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // _indicator7
            // 
            this._indicator7.Location = new System.Drawing.Point(6, 133);
            this._indicator7.Name = "_indicator7";
            this._indicator7.Size = new System.Drawing.Size(13, 13);
            this._indicator7.State = BEMN.Forms.LedState.Off;
            this._indicator7.TabIndex = 19;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(25, 133);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // _indicator5
            // 
            this._indicator5.Location = new System.Drawing.Point(6, 95);
            this._indicator5.Name = "_indicator5";
            this._indicator5.Size = new System.Drawing.Size(13, 13);
            this._indicator5.State = BEMN.Forms.LedState.Off;
            this._indicator5.TabIndex = 9;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 95);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // _indicator4
            // 
            this._indicator4.Location = new System.Drawing.Point(6, 76);
            this._indicator4.Name = "_indicator4";
            this._indicator4.Size = new System.Drawing.Size(13, 13);
            this._indicator4.State = BEMN.Forms.LedState.Off;
            this._indicator4.TabIndex = 7;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // _indicator3
            // 
            this._indicator3.Location = new System.Drawing.Point(6, 57);
            this._indicator3.Name = "_indicator3";
            this._indicator3.Size = new System.Drawing.Size(13, 13);
            this._indicator3.State = BEMN.Forms.LedState.Off;
            this._indicator3.TabIndex = 5;
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 57);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // _indicator2
            // 
            this._indicator2.Location = new System.Drawing.Point(6, 38);
            this._indicator2.Name = "_indicator2";
            this._indicator2.Size = new System.Drawing.Size(13, 13);
            this._indicator2.State = BEMN.Forms.LedState.Off;
            this._indicator2.TabIndex = 3;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 38);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // _indicator1
            // 
            this._indicator1.Location = new System.Drawing.Point(6, 19);
            this._indicator1.Name = "_indicator1";
            this._indicator1.Size = new System.Drawing.Size(13, 13);
            this._indicator1.State = BEMN.Forms.LedState.Off;
            this._indicator1.TabIndex = 1;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 19);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(369, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(141, 191);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные логические сигналы ВЛС";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 166);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 166);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 147);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 128);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 128);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 109);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 109);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 90);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 90);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 71);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 71);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 52);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 52);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 33);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 33);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(222, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(141, 191);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные логические сигналы ЛС";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 166);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 166);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 147);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 147);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 128);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 128);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 109);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 109);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 90);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 90);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 71);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 71);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 52);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 33);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 33);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._d8);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this._d7);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this._d1);
            this.groupBox6.Controls.Add(this._d6);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this._d5);
            this.groupBox6.Controls.Add(this._d2);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this._d4);
            this.groupBox6.Controls.Add(this._d3);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Location = new System.Drawing.Point(77, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(139, 179);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Дискретные входы";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(25, 152);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 14;
            this.label30.Text = "Д8";
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 133);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Д7";
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(25, 19);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Д1";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(25, 114);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(22, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "Д6";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(25, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Д2";
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(25, 95);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Д5";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(25, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Д3";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(25, 76);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Д4";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(838, 468);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this._setpointsComboBox);
            this.groupBox28.Controls.Add(this._setpointLabel);
            this.groupBox28.Controls.Add(this.label18);
            this.groupBox28.Controls.Add(this._mainGroupButton);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(328, 6);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(242, 78);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 42);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(103, 21);
            this._setpointsComboBox.TabIndex = 32;
            // 
            // _setpointLabel
            // 
            this._setpointLabel.AutoSize = true;
            this._setpointLabel.Location = new System.Drawing.Point(101, 24);
            this._setpointLabel.Name = "_setpointLabel";
            this._setpointLabel.Size = new System.Drawing.Size(82, 13);
            this._setpointLabel.TabIndex = 31;
            this._setpointLabel.Text = "не определена";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "Текущая группа";
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(115, 40);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.groupBox4);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._manageLed2);
            this.groupBox27.Controls.Add(this._manageLed1);
            this.groupBox27.Controls.Add(this._manageLed6);
            this.groupBox27.Controls.Add(this._manageLed4);
            this.groupBox27.Controls.Add(this._manageLed3);
            this.groupBox27.Controls.Add(this._resetAnButton);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 241);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._spl);
            this.groupBox4.Controls.Add(this.label158);
            this.groupBox4.Controls.Add(this._offSplBtn);
            this.groupBox4.Controls.Add(this._onSplBtn);
            this.groupBox4.Location = new System.Drawing.Point(6, 164);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(304, 69);
            this.groupBox4.TabIndex = 77;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Свободно программируемая логика";
            // 
            // _spl
            // 
            this._spl.Location = new System.Drawing.Point(6, 35);
            this._spl.Name = "_spl";
            this._spl.Size = new System.Drawing.Size(13, 13);
            this._spl.State = BEMN.Forms.LedState.Off;
            this._spl.TabIndex = 71;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(22, 30);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(191, 26);
            this.label158.TabIndex = 70;
            this.label158.Text = "Работа свободно программируемой\r\nлогики";
            // 
            // _offSplBtn
            // 
            this._offSplBtn.Location = new System.Drawing.Point(223, 41);
            this._offSplBtn.Name = "_offSplBtn";
            this._offSplBtn.Size = new System.Drawing.Size(75, 23);
            this._offSplBtn.TabIndex = 68;
            this._offSplBtn.Text = "Отключить";
            this._offSplBtn.UseVisualStyleBackColor = true;
            this._offSplBtn.Click += new System.EventHandler(this.OffSplBtnClock);
            // 
            // _onSplBtn
            // 
            this._onSplBtn.Location = new System.Drawing.Point(223, 19);
            this._onSplBtn.Name = "_onSplBtn";
            this._onSplBtn.Size = new System.Drawing.Size(75, 23);
            this._onSplBtn.TabIndex = 69;
            this._onSplBtn.Text = "Включить";
            this._onSplBtn.UseVisualStyleBackColor = true;
            this._onSplBtn.Click += new System.EventHandler(this.OnSplBtnClock);
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 37);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 42);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _manageLed2
            // 
            this._manageLed2.Location = new System.Drawing.Point(6, 42);
            this._manageLed2.Name = "_manageLed2";
            this._manageLed2.Size = new System.Drawing.Size(13, 13);
            this._manageLed2.State = BEMN.Forms.LedState.Off;
            this._manageLed2.TabIndex = 49;
            // 
            // _manageLed1
            // 
            this._manageLed1.Location = new System.Drawing.Point(6, 19);
            this._manageLed1.Name = "_manageLed1";
            this._manageLed1.Size = new System.Drawing.Size(13, 13);
            this._manageLed1.State = BEMN.Forms.LedState.Off;
            this._manageLed1.TabIndex = 48;
            // 
            // _manageLed6
            // 
            this._manageLed6.Location = new System.Drawing.Point(6, 111);
            this._manageLed6.Name = "_manageLed6";
            this._manageLed6.Size = new System.Drawing.Size(13, 13);
            this._manageLed6.State = BEMN.Forms.LedState.Off;
            this._manageLed6.TabIndex = 13;
            // 
            // _manageLed4
            // 
            this._manageLed4.Location = new System.Drawing.Point(6, 88);
            this._manageLed4.Name = "_manageLed4";
            this._manageLed4.Size = new System.Drawing.Size(13, 13);
            this._manageLed4.State = BEMN.Forms.LedState.Off;
            this._manageLed4.TabIndex = 11;
            // 
            // _manageLed3
            // 
            this._manageLed3.Location = new System.Drawing.Point(6, 65);
            this._manageLed3.Name = "_manageLed3";
            this._manageLed3.Size = new System.Drawing.Size(13, 13);
            this._manageLed3.State = BEMN.Forms.LedState.Off;
            this._manageLed3.TabIndex = 10;
            // 
            // _resetAnButton
            // 
            this._resetAnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAnButton.Location = new System.Drawing.Point(6, 135);
            this._resetAnButton.Name = "_resetAnButton";
            this._resetAnButton.Size = new System.Drawing.Size(304, 23);
            this._resetAnButton.TabIndex = 9;
            this._resetAnButton.Text = "Сброс индикации";
            this._resetAnButton.UseVisualStyleBackColor = true;
            this._resetAnButton.Click += new System.EventHandler(this._resetAnButton_Click);
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 106);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 83);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 60);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 111);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 88);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 65);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // Mr550MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 495);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(863, 534);
            this.Name = "Mr550MeasuringForm";
            this.Text = "MeasuringForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr550MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr550MeasuringForm_Load);
            this._dataBaseTabControl.ResumeLayout(false);
            this._analogTabPage.ResumeLayout(false);
            this.UaccGroup.ResumeLayout(false);
            this.UaccGroup.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._discretTabPage.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private System.Windows.Forms.TabPage _analogTabPage;
        private Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.TabPage _discretTabPage;
        private System.Windows.Forms.Label label86;
        private Forms.LedControl _iS1;
        private System.Windows.Forms.Label label85;
        private Forms.LedControl _iS2;
        private Forms.LedControl _iS1Io;
        private System.Windows.Forms.Label label84;
        private Forms.LedControl _iS3;
        private Forms.LedControl _iS2Io;
        private System.Windows.Forms.Label label83;
        private Forms.LedControl _iS4;
        private Forms.LedControl _iS3Io;
        private Forms.LedControl _iS4Io;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label269;
        private Forms.LedControl _i8Io;
        private Forms.LedControl _i7Io;
        private Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private Forms.LedControl _i6Io;
        private Forms.LedControl _i7;
        private System.Windows.Forms.Label label88;
        private Forms.LedControl _i5Io;
        private Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private Forms.LedControl _i4Io;
        private Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private Forms.LedControl _i3Io;
        private Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private Forms.LedControl _i2Io;
        private Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private Forms.LedControl _i1Io;
        private Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox groupBox21;
        private Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private System.Windows.Forms.GroupBox groupBox12;
        private Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox18;
        private Forms.LedControl _indicator6;
        private System.Windows.Forms.Label label189;
        private Forms.LedControl _indicator8;
        private System.Windows.Forms.Label label181;
        private Forms.LedControl _indicator7;
        private System.Windows.Forms.Label label182;
        private Forms.LedControl _indicator5;
        private System.Windows.Forms.Label label184;
        private Forms.LedControl _indicator4;
        private System.Windows.Forms.Label label185;
        private Forms.LedControl _indicator3;
        private System.Windows.Forms.Label label186;
        private Forms.LedControl _indicator2;
        private System.Windows.Forms.Label label187;
        private Forms.LedControl _indicator1;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.GroupBox groupBox10;
        private Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox6;
        private Forms.LedControl _d8;
        private System.Windows.Forms.Label label30;
        private Forms.LedControl _d7;
        private System.Windows.Forms.Label label29;
        private Forms.LedControl _d6;
        private System.Windows.Forms.Label label28;
        private Forms.LedControl _d5;
        private System.Windows.Forms.Label label27;
        private Forms.LedControl _d4;
        private System.Windows.Forms.Label label26;
        private Forms.LedControl _d3;
        private System.Windows.Forms.Label label25;
        private Forms.LedControl _d2;
        private System.Windows.Forms.Label label24;
        private Forms.LedControl _d1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.GroupBox groupBox27;
        private Forms.LedControl _manageLed6;
        private Forms.LedControl _manageLed4;
        private Forms.LedControl _manageLed3;
        private System.Windows.Forms.Button _resetAnButton;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private Forms.LedControl _manageLed2;
        private Forms.LedControl _manageLed1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox _i0TextBox;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox _icTextBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox _igTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _i2TextBox;
        private System.Windows.Forms.TextBox _inTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _ibTextBox;
        private System.Windows.Forms.TextBox _i1TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _iaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label97;
        private Forms.LedControl _autoLed8;
        private System.Windows.Forms.Label label7;
        private Forms.LedControl _autoLed7;
        private System.Windows.Forms.Label label99;
        private Forms.LedControl _autoLed6;
        private System.Windows.Forms.Label label8;
        private Forms.LedControl _autoLed5;
        private System.Windows.Forms.Label label9;
        private Forms.LedControl _autoLed4;
        private System.Windows.Forms.Label label102;
        private Forms.LedControl _autoLed3;
        private System.Windows.Forms.Label label103;
        private Forms.LedControl _autoLed2;
        private System.Windows.Forms.Label label104;
        private Forms.LedControl _autoLed1;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label10;
        private Forms.LedControl _faultSignalLed16;
        private System.Windows.Forms.Label label11;
        private Forms.LedControl _faultSignalLed15;
        private System.Windows.Forms.Label label12;
        private Forms.LedControl _faultSignalLed14;
        private System.Windows.Forms.Label label13;
        private Forms.LedControl _faultSignalLed13;
        private System.Windows.Forms.Label label14;
        private Forms.LedControl _faultSignalLed12;
        private System.Windows.Forms.Label label15;
        private Forms.LedControl _faultSignalLed11;
        private System.Windows.Forms.Label label135;
        private Forms.LedControl _faultSignalLed10;
        private System.Windows.Forms.Label label136;
        private Forms.LedControl _faultSignalLed9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private Forms.LedControl _faultSignalLed8;
        private System.Windows.Forms.Label label17;
        private Forms.LedControl _faultSignalLed7;
        private System.Windows.Forms.Label label19;
        private Forms.LedControl _faultSignalLed6;
        private System.Windows.Forms.Label label31;
        private Forms.LedControl _faultSignalLed5;
        private System.Windows.Forms.Label label32;
        private Forms.LedControl _faultSignalLed4;
        private System.Windows.Forms.Label label33;
        private Forms.LedControl _faultSignalLed3;
        private System.Windows.Forms.Label label34;
        private Forms.LedControl _faultSignalLed2;
        private System.Windows.Forms.Label label35;
        private Forms.LedControl _faultSignalLed1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label113;
        private Forms.LedControl _faultStateLed8;
        private System.Windows.Forms.Label label114;
        private Forms.LedControl _faultStateLed7;
        private System.Windows.Forms.Label label115;
        private Forms.LedControl _faultStateLed6;
        private System.Windows.Forms.Label label116;
        private Forms.LedControl _faultStateLed5;
        private System.Windows.Forms.Label noUaccLabel;
        private Forms.LedControl _faultStateLed4;
        private System.Windows.Forms.Label label118;
        private Forms.LedControl _faultStateLed3;
        private System.Windows.Forms.Label label36;
        private Forms.LedControl _faultStateLed2;
        private System.Windows.Forms.Label label37;
        private Forms.LedControl _faultStateLed1;
        private System.Windows.Forms.Label _setpointLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.GroupBox UaccGroup;
        private System.Windows.Forms.TextBox _UaccBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox4;
        private Forms.LedControl _spl;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Button _offSplBtn;
        private System.Windows.Forms.Button _onSplBtn;

    }
}