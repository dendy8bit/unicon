﻿namespace BEMN.MR550.Version2.Osc.Forms
{
    partial class Mr550OscilloscopeResultForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MAINTABLE = new System.Windows.Forms.TableLayoutPanel();
            this.hScrollBar4 = new System.Windows.Forms.HScrollBar();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MAINPANEL = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this._discrestsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._discrete5Button = new System.Windows.Forms.Button();
            this._discrete8Button = new System.Windows.Forms.Button();
            this._discrete7Button = new System.Windows.Forms.Button();
            this._discrete6Button = new System.Windows.Forms.Button();
            this._discrete2Button = new System.Windows.Forms.Button();
            this._discrete4Button = new System.Windows.Forms.Button();
            this._discrete1Button = new System.Windows.Forms.Button();
            this._discrete3Button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this._currentСonnectionsChart = new BEMN_XY_Chart.DAS_Net_XYChart();
            this._s1Scroll = new System.Windows.Forms.VScrollBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._i3Button = new System.Windows.Forms.Button();
            this._i4Button = new System.Windows.Forms.Button();
            this._i1Button = new System.Windows.Forms.Button();
            this._i2Button = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this._currentСonnectionsChartDecreaseButton = new System.Windows.Forms.Button();
            this._currentСonnectionsChartIncreaseButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.MarkersTable = new System.Windows.Forms.TableLayoutPanel();
            this._marker1TrackBar = new System.Windows.Forms.TrackBar();
            this._marker2TrackBar = new System.Windows.Forms.TrackBar();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._markerScrollPanel = new System.Windows.Forms.Panel();
            this._marker2Box = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._marker2D8 = new System.Windows.Forms.Label();
            this._marker2D7 = new System.Windows.Forms.Label();
            this._marker2D6 = new System.Windows.Forms.Label();
            this._marker2D5 = new System.Windows.Forms.Label();
            this._marker2D4 = new System.Windows.Forms.Label();
            this._marker2D3 = new System.Windows.Forms.Label();
            this._marker2D2 = new System.Windows.Forms.Label();
            this._marker2D1 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._marker2I4 = new System.Windows.Forms.Label();
            this._marker2I3 = new System.Windows.Forms.Label();
            this._marker2I2 = new System.Windows.Forms.Label();
            this._marker2I1 = new System.Windows.Forms.Label();
            this._marker1Box = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._marker1D8 = new System.Windows.Forms.Label();
            this._marker1D7 = new System.Windows.Forms.Label();
            this._marker1D6 = new System.Windows.Forms.Label();
            this._marker1D5 = new System.Windows.Forms.Label();
            this._marker1D4 = new System.Windows.Forms.Label();
            this._marker1D3 = new System.Windows.Forms.Label();
            this._marker1D2 = new System.Windows.Forms.Label();
            this._marker1D1 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._marker1I4 = new System.Windows.Forms.Label();
            this._marker1I3 = new System.Windows.Forms.Label();
            this._marker1I2 = new System.Windows.Forms.Label();
            this._marker1I1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._deltaTimeBox = new System.Windows.Forms.GroupBox();
            this._markerTimeDelta = new System.Windows.Forms.Label();
            this._marker2TimeBox = new System.Windows.Forms.GroupBox();
            this._marker2Time = new System.Windows.Forms.Label();
            this._marker1TimeBox = new System.Windows.Forms.GroupBox();
            this._marker1Time = new System.Windows.Forms.Label();
            this._newMarkToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._xIncreaseButton = new System.Windows.Forms.Button();
            this._xDecreaseButton = new System.Windows.Forms.Button();
            this._voltageСheckBox = new System.Windows.Forms.CheckBox();
            this._discrestsСheckBox = new System.Windows.Forms.CheckBox();
            this._oscRunСheckBox = new System.Windows.Forms.CheckBox();
            this._markCheckBox = new System.Windows.Forms.CheckBox();
            this._markerCheckBox = new System.Windows.Forms.CheckBox();
            this.MAINTABLE.SuspendLayout();
            this.MAINPANEL.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.MarkersTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).BeginInit();
            this.panel3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._markerScrollPanel.SuspendLayout();
            this._marker2Box.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._marker1Box.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._deltaTimeBox.SuspendLayout();
            this._marker2TimeBox.SuspendLayout();
            this._marker1TimeBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // MAINTABLE
            // 
            this.MAINTABLE.ColumnCount = 2;
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.MAINTABLE.Controls.Add(this.hScrollBar4, 0, 2);
            this.MAINTABLE.Controls.Add(this.menuStrip1, 0, 0);
            this.MAINTABLE.Controls.Add(this.MAINPANEL, 0, 1);
            this.MAINTABLE.Controls.Add(this.panel3, 1, 1);
            this.MAINTABLE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINTABLE.Location = new System.Drawing.Point(0, 0);
            this.MAINTABLE.Margin = new System.Windows.Forms.Padding(0);
            this.MAINTABLE.Name = "MAINTABLE";
            this.MAINTABLE.RowCount = 2;
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.MAINTABLE.Size = new System.Drawing.Size(1369, 749);
            this.MAINTABLE.TabIndex = 2;
            // 
            // hScrollBar4
            // 
            this.hScrollBar4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hScrollBar4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hScrollBar4.LargeChange = 1;
            this.hScrollBar4.Location = new System.Drawing.Point(0, 729);
            this.hScrollBar4.Maximum = 10;
            this.hScrollBar4.Minimum = 10;
            this.hScrollBar4.Name = "hScrollBar4";
            this.hScrollBar4.Size = new System.Drawing.Size(1069, 20);
            this.hScrollBar4.TabIndex = 20;
            this.hScrollBar4.Value = 10;
            this.hScrollBar4.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.MAINTABLE.SetColumnSpan(this.menuStrip1, 2);
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1369, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MAINPANEL
            // 
            this.MAINPANEL.Controls.Add(this.panel1);
            this.MAINPANEL.Controls.Add(this.MarkersTable);
            this.MAINPANEL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MAINPANEL.Location = new System.Drawing.Point(3, 28);
            this.MAINPANEL.Name = "MAINPANEL";
            this.MAINPANEL.Size = new System.Drawing.Size(1063, 698);
            this.MAINPANEL.TabIndex = 32;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1063, 640);
            this.panel1.TabIndex = 32;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.tableLayoutPanel14);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.tableLayoutPanel13);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1037, 746);
            this.panel2.TabIndex = 0;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Controls.Add(this._discrestsChart, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 541);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1037, 205);
            this.tableLayoutPanel14.TabIndex = 29;
            // 
            // _discrestsChart
            // 
            this._discrestsChart.BkGradient = false;
            this._discrestsChart.BkGradientAngle = 90;
            this._discrestsChart.BkGradientColor = System.Drawing.Color.White;
            this._discrestsChart.BkGradientRate = 0.5F;
            this._discrestsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._discrestsChart.BkRestrictedToChartPanel = false;
            this._discrestsChart.BkShinePosition = 1F;
            this._discrestsChart.BkTransparency = 0F;
            this._discrestsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._discrestsChart.BorderExteriorLength = 0;
            this._discrestsChart.BorderGradientAngle = 225;
            this._discrestsChart.BorderGradientLightPos1 = 1F;
            this._discrestsChart.BorderGradientLightPos2 = -1F;
            this._discrestsChart.BorderGradientRate = 0.5F;
            this._discrestsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._discrestsChart.BorderLightIntermediateBrightness = 0F;
            this._discrestsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._discrestsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._discrestsChart.ChartPanelBkTransparency = 0F;
            this._discrestsChart.ControlShadow = false;
            this._discrestsChart.CoordinateAxesVisible = true;
            this._discrestsChart.CoordinateAxisColor = System.Drawing.Color.Transparent;
            this._discrestsChart.CoordinateXOrigin = 0D;
            this._discrestsChart.CoordinateYMax = 100D;
            this._discrestsChart.CoordinateYMin = -100D;
            this._discrestsChart.CoordinateYOrigin = 0D;
            this._discrestsChart.Dock = System.Windows.Forms.DockStyle.Fill;
            this._discrestsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._discrestsChart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._discrestsChart.FooterColor = System.Drawing.Color.Black;
            this._discrestsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._discrestsChart.FooterVisible = false;
            this._discrestsChart.GridColor = System.Drawing.Color.MistyRose;
            this._discrestsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._discrestsChart.GridVisible = true;
            this._discrestsChart.GridXSubTicker = 0;
            this._discrestsChart.GridXTicker = 10;
            this._discrestsChart.GridYSubTicker = 0;
            this._discrestsChart.GridYTicker = 2;
            this._discrestsChart.HeaderColor = System.Drawing.Color.Black;
            this._discrestsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._discrestsChart.HeaderVisible = false;
            this._discrestsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.InnerBorderLength = 0;
            this._discrestsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.LegendBkColor = System.Drawing.Color.White;
            this._discrestsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._discrestsChart.LegendVisible = false;
            this._discrestsChart.Location = new System.Drawing.Point(63, 26);
            this._discrestsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._discrestsChart.MiddleBorderLength = 0;
            this._discrestsChart.Name = "_discrestsChart";
            this._discrestsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._discrestsChart.OuterBorderLength = 0;
            this._discrestsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._discrestsChart.Precision = 0;
            this._discrestsChart.RoundRadius = 10;
            this._discrestsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._discrestsChart.ShadowDepth = 8;
            this._discrestsChart.ShadowRate = 0.5F;
            this._discrestsChart.Size = new System.Drawing.Size(951, 176);
            this._discrestsChart.TabIndex = 32;
            this._discrestsChart.Text = "daS_Net_XYChart2";
            this._discrestsChart.XMax = 100D;
            this._discrestsChart.XMin = 0D;
            this._discrestsChart.XScaleColor = System.Drawing.Color.White;
            this._discrestsChart.XScaleVisible = false;
            this._discrestsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this._discrete5Button, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this._discrete8Button, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this._discrete7Button, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this._discrete6Button, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this._discrete2Button, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this._discrete4Button, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this._discrete1Button, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this._discrete3Button, 0, 2);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 26);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 9;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(54, 176);
            this.tableLayoutPanel5.TabIndex = 30;
            // 
            // _discrete5Button
            // 
            this._discrete5Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete5Button.AutoSize = true;
            this._discrete5Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete5Button.ForeColor = System.Drawing.Color.Black;
            this._discrete5Button.Location = new System.Drawing.Point(0, 88);
            this._discrete5Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete5Button.Name = "_discrete5Button";
            this._discrete5Button.Size = new System.Drawing.Size(54, 22);
            this._discrete5Button.TabIndex = 16;
            this._discrete5Button.Text = "Д5";
            this._discrete5Button.UseVisualStyleBackColor = false;
            this._discrete5Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete8Button
            // 
            this._discrete8Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete8Button.AutoSize = true;
            this._discrete8Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete8Button.ForeColor = System.Drawing.Color.Black;
            this._discrete8Button.Location = new System.Drawing.Point(0, 154);
            this._discrete8Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete8Button.Name = "_discrete8Button";
            this._discrete8Button.Size = new System.Drawing.Size(54, 22);
            this._discrete8Button.TabIndex = 19;
            this._discrete8Button.Text = "Д8";
            this._discrete8Button.UseVisualStyleBackColor = false;
            this._discrete8Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete7Button
            // 
            this._discrete7Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete7Button.AutoSize = true;
            this._discrete7Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete7Button.ForeColor = System.Drawing.Color.Black;
            this._discrete7Button.Location = new System.Drawing.Point(0, 132);
            this._discrete7Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete7Button.Name = "_discrete7Button";
            this._discrete7Button.Size = new System.Drawing.Size(54, 22);
            this._discrete7Button.TabIndex = 18;
            this._discrete7Button.Text = "Д7";
            this._discrete7Button.UseVisualStyleBackColor = false;
            this._discrete7Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete6Button
            // 
            this._discrete6Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete6Button.AutoSize = true;
            this._discrete6Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete6Button.ForeColor = System.Drawing.Color.Black;
            this._discrete6Button.Location = new System.Drawing.Point(0, 110);
            this._discrete6Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete6Button.Name = "_discrete6Button";
            this._discrete6Button.Size = new System.Drawing.Size(54, 22);
            this._discrete6Button.TabIndex = 17;
            this._discrete6Button.Text = "Д6";
            this._discrete6Button.UseVisualStyleBackColor = false;
            this._discrete6Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete2Button
            // 
            this._discrete2Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete2Button.AutoSize = true;
            this._discrete2Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete2Button.ForeColor = System.Drawing.Color.Black;
            this._discrete2Button.Location = new System.Drawing.Point(0, 22);
            this._discrete2Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete2Button.Name = "_discrete2Button";
            this._discrete2Button.Size = new System.Drawing.Size(54, 22);
            this._discrete2Button.TabIndex = 37;
            this._discrete2Button.Text = "Д2";
            this._discrete2Button.UseVisualStyleBackColor = false;
            this._discrete2Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete4Button
            // 
            this._discrete4Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete4Button.AutoSize = true;
            this._discrete4Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete4Button.ForeColor = System.Drawing.Color.Black;
            this._discrete4Button.Location = new System.Drawing.Point(0, 66);
            this._discrete4Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete4Button.Name = "_discrete4Button";
            this._discrete4Button.Size = new System.Drawing.Size(54, 22);
            this._discrete4Button.TabIndex = 39;
            this._discrete4Button.Text = "Д4";
            this._discrete4Button.UseVisualStyleBackColor = false;
            this._discrete4Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete1Button
            // 
            this._discrete1Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete1Button.AutoSize = true;
            this._discrete1Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete1Button.ForeColor = System.Drawing.Color.Black;
            this._discrete1Button.Location = new System.Drawing.Point(0, 0);
            this._discrete1Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete1Button.Name = "_discrete1Button";
            this._discrete1Button.Size = new System.Drawing.Size(54, 22);
            this._discrete1Button.TabIndex = 36;
            this._discrete1Button.Text = "Д1";
            this._discrete1Button.UseVisualStyleBackColor = false;
            this._discrete1Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // _discrete3Button
            // 
            this._discrete3Button.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._discrete3Button.AutoSize = true;
            this._discrete3Button.BackColor = System.Drawing.Color.LightSlateGray;
            this._discrete3Button.ForeColor = System.Drawing.Color.Black;
            this._discrete3Button.Location = new System.Drawing.Point(0, 44);
            this._discrete3Button.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._discrete3Button.Name = "_discrete3Button";
            this._discrete3Button.Size = new System.Drawing.Size(54, 22);
            this._discrete3Button.TabIndex = 38;
            this._discrete3Button.Text = "Д3";
            this._discrete3Button.UseVisualStyleBackColor = false;
            this._discrete3Button.Click += new System.EventHandler(this.DiscretesButton_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 5);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Дискреты";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Black;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 538);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1037, 3);
            this.splitter1.TabIndex = 28;
            this.splitter1.TabStop = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Controls.Add(this._currentСonnectionsChart, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this._s1Scroll, 2, 1);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1037, 538);
            this.tableLayoutPanel13.TabIndex = 27;
            // 
            // _currentСonnectionsChart
            // 
            this._currentСonnectionsChart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._currentСonnectionsChart.BkGradient = false;
            this._currentСonnectionsChart.BkGradientAngle = 90;
            this._currentСonnectionsChart.BkGradientColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.BkGradientRate = 0.5F;
            this._currentСonnectionsChart.BkGradientType = BEMN_XY_Chart.DAS_BkGradientStyle.BKGS_Linear;
            this._currentСonnectionsChart.BkRestrictedToChartPanel = false;
            this._currentСonnectionsChart.BkShinePosition = 1F;
            this._currentСonnectionsChart.BkTransparency = 0F;
            this._currentСonnectionsChart.BorderExteriorColor = System.Drawing.Color.Blue;
            this._currentСonnectionsChart.BorderExteriorLength = 0;
            this._currentСonnectionsChart.BorderGradientAngle = 225;
            this._currentСonnectionsChart.BorderGradientLightPos1 = 1F;
            this._currentСonnectionsChart.BorderGradientLightPos2 = -1F;
            this._currentСonnectionsChart.BorderGradientRate = 0.5F;
            this._currentСonnectionsChart.BorderGradientType = BEMN_XY_Chart.DAS_BorderGradientStyle.BGS_None;
            this._currentСonnectionsChart.BorderLightIntermediateBrightness = 0F;
            this._currentСonnectionsChart.BorderShape = BEMN_XY_Chart.DAS_BorderStyle.BS_Rect;
            this._currentСonnectionsChart.ChartPanelBackColor = System.Drawing.Color.LightGray;
            this._currentСonnectionsChart.ChartPanelBkTransparency = 0F;
            this._currentСonnectionsChart.ControlShadow = false;
            this._currentСonnectionsChart.CoordinateAxesVisible = true;
            this._currentСonnectionsChart.CoordinateAxisColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.CoordinateXOrigin = 0D;
            this._currentСonnectionsChart.CoordinateYMax = 100D;
            this._currentСonnectionsChart.CoordinateYMin = -100D;
            this._currentСonnectionsChart.CoordinateYOrigin = 0D;
            this._currentСonnectionsChart.DrawDirection = BEMN_XY_Chart.DAS_DrawDirection.BT_LEFTTORIGHT;
            this._currentСonnectionsChart.FooterColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.FooterFont = new System.Drawing.Font("Times New Roman", 16F);
            this._currentСonnectionsChart.FooterVisible = false;
            this._currentСonnectionsChart.GridColor = System.Drawing.Color.MistyRose;
            this._currentСonnectionsChart.GridStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this._currentСonnectionsChart.GridVisible = true;
            this._currentСonnectionsChart.GridXSubTicker = 0;
            this._currentСonnectionsChart.GridXTicker = 10;
            this._currentСonnectionsChart.GridYSubTicker = 0;
            this._currentСonnectionsChart.GridYTicker = 10;
            this._currentСonnectionsChart.HeaderColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.HeaderFont = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold);
            this._currentСonnectionsChart.HeaderVisible = false;
            this._currentСonnectionsChart.InnerBorderDarkColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.InnerBorderLength = 0;
            this._currentСonnectionsChart.InnerBorderLightColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.LegendBkColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.LegendPosition = BEMN_XY_Chart.DAS_LegendPosition.LP_LEFT;
            this._currentСonnectionsChart.LegendVisible = false;
            this._currentСonnectionsChart.Location = new System.Drawing.Point(63, 38);
            this._currentСonnectionsChart.MiddleBorderColor = System.Drawing.Color.Gray;
            this._currentСonnectionsChart.MiddleBorderLength = 0;
            this._currentСonnectionsChart.Name = "_currentСonnectionsChart";
            this._currentСonnectionsChart.OuterBorderDarkColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.OuterBorderLength = 0;
            this._currentСonnectionsChart.OuterBorderLightColor = System.Drawing.Color.White;
            this._currentСonnectionsChart.Precision = 0;
            this._currentСonnectionsChart.RoundRadius = 10;
            this._currentСonnectionsChart.ShadowColor = System.Drawing.Color.DimGray;
            this._currentСonnectionsChart.ShadowDepth = 8;
            this._currentСonnectionsChart.ShadowRate = 0.5F;
            this._currentСonnectionsChart.Size = new System.Drawing.Size(951, 497);
            this._currentСonnectionsChart.TabIndex = 34;
            this._currentСonnectionsChart.Text = "daS_Net_XYChart1";
            this._currentСonnectionsChart.XMax = 100D;
            this._currentСonnectionsChart.XMin = 0D;
            this._currentСonnectionsChart.XScaleColor = System.Drawing.Color.Black;
            this._currentСonnectionsChart.XScaleVisible = true;
            this._currentСonnectionsChart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Chart_MouseClick);
            // 
            // _s1Scroll
            // 
            this._s1Scroll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this._s1Scroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._s1Scroll.LargeChange = 1;
            this._s1Scroll.Location = new System.Drawing.Point(1019, 35);
            this._s1Scroll.Minimum = 100;
            this._s1Scroll.Name = "_s1Scroll";
            this._s1Scroll.Size = new System.Drawing.Size(15, 503);
            this._s1Scroll.TabIndex = 32;
            this._s1Scroll.Value = 100;
            this._s1Scroll.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 9;
            this.tableLayoutPanel13.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 501F));
            this.tableLayoutPanel3.Controls.Add(this._i3Button, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this._i4Button, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this._i1Button, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this._i2Button, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(986, 29);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // _i3Button
            // 
            this._i3Button.BackColor = System.Drawing.Color.Red;
            this._i3Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i3Button.Location = new System.Drawing.Point(213, 3);
            this._i3Button.Name = "_i3Button";
            this._i3Button.Size = new System.Drawing.Size(49, 23);
            this._i3Button.TabIndex = 13;
            this._i3Button.Text = "Ic";
            this._i3Button.UseVisualStyleBackColor = false;
            this._i3Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i4Button
            // 
            this._i4Button.BackColor = System.Drawing.Color.Indigo;
            this._i4Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i4Button.Location = new System.Drawing.Point(268, 3);
            this._i4Button.Name = "_i4Button";
            this._i4Button.Size = new System.Drawing.Size(49, 23);
            this._i4Button.TabIndex = 11;
            this._i4Button.Text = "In";
            this._i4Button.UseVisualStyleBackColor = false;
            this._i4Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i1Button
            // 
            this._i1Button.BackColor = System.Drawing.Color.Yellow;
            this._i1Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i1Button.Location = new System.Drawing.Point(103, 3);
            this._i1Button.Name = "_i1Button";
            this._i1Button.Size = new System.Drawing.Size(49, 23);
            this._i1Button.TabIndex = 10;
            this._i1Button.Text = "Ia";
            this._i1Button.UseVisualStyleBackColor = false;
            this._i1Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // _i2Button
            // 
            this._i2Button.BackColor = System.Drawing.Color.Green;
            this._i2Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this._i2Button.Location = new System.Drawing.Point(158, 3);
            this._i2Button.Name = "_i2Button";
            this._i2Button.Size = new System.Drawing.Size(49, 23);
            this._i2Button.TabIndex = 9;
            this._i2Button.Text = "Ib";
            this._i2Button.UseVisualStyleBackColor = false;
            this._i2Button.Click += new System.EventHandler(this.CurrentСonnectionsButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Токи";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this._currentСonnectionsChartDecreaseButton, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this._currentСonnectionsChartIncreaseButton, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 4;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(54, 497);
            this.tableLayoutPanel8.TabIndex = 31;
            // 
            // _currentСonnectionsChartDecreaseButton
            // 
            this._currentСonnectionsChartDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentСonnectionsChartDecreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentСonnectionsChartDecreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentСonnectionsChartDecreaseButton.Enabled = false;
            this._currentСonnectionsChartDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentСonnectionsChartDecreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentСonnectionsChartDecreaseButton.Location = new System.Drawing.Point(3, 251);
            this._currentСonnectionsChartDecreaseButton.Name = "_currentСonnectionsChartDecreaseButton";
            this._currentСonnectionsChartDecreaseButton.Size = new System.Drawing.Size(48, 24);
            this._currentСonnectionsChartDecreaseButton.TabIndex = 1;
            this._currentСonnectionsChartDecreaseButton.Text = "-";
            this._currentСonnectionsChartDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _currentСonnectionsChartIncreaseButton
            // 
            this._currentСonnectionsChartIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._currentСonnectionsChartIncreaseButton.Cursor = System.Windows.Forms.Cursors.Default;
            this._currentСonnectionsChartIncreaseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._currentСonnectionsChartIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._currentСonnectionsChartIncreaseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._currentСonnectionsChartIncreaseButton.Location = new System.Drawing.Point(3, 221);
            this._currentСonnectionsChartIncreaseButton.Name = "_currentСonnectionsChartIncreaseButton";
            this._currentСonnectionsChartIncreaseButton.Size = new System.Drawing.Size(48, 24);
            this._currentСonnectionsChartIncreaseButton.TabIndex = 0;
            this._currentСonnectionsChartIncreaseButton.Text = "+";
            this._currentСonnectionsChartIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.Location = new System.Drawing.Point(19, 203);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Y";
            // 
            // MarkersTable
            // 
            this.MarkersTable.BackColor = System.Drawing.Color.LightGray;
            this.MarkersTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.MarkersTable.ColumnCount = 3;
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.MarkersTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.MarkersTable.Controls.Add(this._marker1TrackBar, 1, 0);
            this.MarkersTable.Controls.Add(this._marker2TrackBar, 1, 1);
            this.MarkersTable.Dock = System.Windows.Forms.DockStyle.Top;
            this.MarkersTable.Location = new System.Drawing.Point(0, 0);
            this.MarkersTable.Margin = new System.Windows.Forms.Padding(0);
            this.MarkersTable.Name = "MarkersTable";
            this.MarkersTable.RowCount = 2;
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.MarkersTable.Size = new System.Drawing.Size(1063, 58);
            this.MarkersTable.TabIndex = 31;
            this.MarkersTable.Visible = false;
            // 
            // _marker1TrackBar
            // 
            this._marker1TrackBar.BackColor = System.Drawing.Color.LightSkyBlue;
            this._marker1TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker1TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker1TrackBar.Location = new System.Drawing.Point(49, 4);
            this._marker1TrackBar.Name = "_marker1TrackBar";
            this._marker1TrackBar.Size = new System.Drawing.Size(961, 24);
            this._marker1TrackBar.TabIndex = 2;
            this._marker1TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker1TrackBar.Scroll += new System.EventHandler(this._marker1TrackBar_Scroll);
            // 
            // _marker2TrackBar
            // 
            this._marker2TrackBar.BackColor = System.Drawing.Color.Violet;
            this._marker2TrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this._marker2TrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._marker2TrackBar.LargeChange = 0;
            this._marker2TrackBar.Location = new System.Drawing.Point(49, 35);
            this._marker2TrackBar.Maximum = 3400;
            this._marker2TrackBar.Name = "_marker2TrackBar";
            this._marker2TrackBar.Size = new System.Drawing.Size(961, 24);
            this._marker2TrackBar.TabIndex = 3;
            this._marker2TrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this._marker2TrackBar.Scroll += new System.EventHandler(this._marker2TrackBar_Scroll);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Silver;
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1072, 28);
            this.panel3.Name = "panel3";
            this.MAINTABLE.SetRowSpan(this.panel3, 2);
            this.panel3.Size = new System.Drawing.Size(294, 718);
            this.panel3.TabIndex = 33;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.LightGray;
            this.groupBox1.Controls.Add(this._markerScrollPanel);
            this.groupBox1.Location = new System.Drawing.Point(3, -1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(293, 716);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мгновенные значения";
            // 
            // _markerScrollPanel
            // 
            this._markerScrollPanel.AutoScroll = true;
            this._markerScrollPanel.Controls.Add(this._marker2Box);
            this._markerScrollPanel.Controls.Add(this._marker1Box);
            this._markerScrollPanel.Controls.Add(this.groupBox4);
            this._markerScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._markerScrollPanel.Location = new System.Drawing.Point(0, 13);
            this._markerScrollPanel.Name = "_markerScrollPanel";
            this._markerScrollPanel.Size = new System.Drawing.Size(293, 703);
            this._markerScrollPanel.TabIndex = 3;
            // 
            // _marker2Box
            // 
            this._marker2Box.Controls.Add(this.groupBox10);
            this._marker2Box.Controls.Add(this.groupBox12);
            this._marker2Box.Location = new System.Drawing.Point(142, 3);
            this._marker2Box.Name = "_marker2Box";
            this._marker2Box.Size = new System.Drawing.Size(133, 309);
            this._marker2Box.TabIndex = 3;
            this._marker2Box.TabStop = false;
            this._marker2Box.Text = "Маркер 2";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._marker2D8);
            this.groupBox10.Controls.Add(this._marker2D7);
            this.groupBox10.Controls.Add(this._marker2D6);
            this.groupBox10.Controls.Add(this._marker2D5);
            this.groupBox10.Controls.Add(this._marker2D4);
            this.groupBox10.Controls.Add(this._marker2D3);
            this.groupBox10.Controls.Add(this._marker2D2);
            this.groupBox10.Controls.Add(this._marker2D1);
            this.groupBox10.Location = new System.Drawing.Point(5, 113);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(119, 186);
            this.groupBox10.TabIndex = 43;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Дискреты";
            // 
            // _marker2D8
            // 
            this._marker2D8.AutoSize = true;
            this._marker2D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D8.Location = new System.Drawing.Point(6, 156);
            this._marker2D8.Name = "_marker2D8";
            this._marker2D8.Size = new System.Drawing.Size(34, 13);
            this._marker2D8.TabIndex = 23;
            this._marker2D8.Text = "Д8 = ";
            // 
            // _marker2D7
            // 
            this._marker2D7.AutoSize = true;
            this._marker2D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D7.Location = new System.Drawing.Point(6, 136);
            this._marker2D7.Name = "_marker2D7";
            this._marker2D7.Size = new System.Drawing.Size(34, 13);
            this._marker2D7.TabIndex = 22;
            this._marker2D7.Text = "Д7 = ";
            // 
            // _marker2D6
            // 
            this._marker2D6.AutoSize = true;
            this._marker2D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D6.Location = new System.Drawing.Point(6, 116);
            this._marker2D6.Name = "_marker2D6";
            this._marker2D6.Size = new System.Drawing.Size(34, 13);
            this._marker2D6.TabIndex = 21;
            this._marker2D6.Text = "Д6 = ";
            // 
            // _marker2D5
            // 
            this._marker2D5.AutoSize = true;
            this._marker2D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D5.Location = new System.Drawing.Point(6, 96);
            this._marker2D5.Name = "_marker2D5";
            this._marker2D5.Size = new System.Drawing.Size(34, 13);
            this._marker2D5.TabIndex = 20;
            this._marker2D5.Text = "Д5 = ";
            // 
            // _marker2D4
            // 
            this._marker2D4.AutoSize = true;
            this._marker2D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D4.Location = new System.Drawing.Point(6, 76);
            this._marker2D4.Name = "_marker2D4";
            this._marker2D4.Size = new System.Drawing.Size(34, 13);
            this._marker2D4.TabIndex = 19;
            this._marker2D4.Text = "Д4 = ";
            // 
            // _marker2D3
            // 
            this._marker2D3.AutoSize = true;
            this._marker2D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D3.Location = new System.Drawing.Point(6, 56);
            this._marker2D3.Name = "_marker2D3";
            this._marker2D3.Size = new System.Drawing.Size(34, 13);
            this._marker2D3.TabIndex = 18;
            this._marker2D3.Text = "Д3 = ";
            // 
            // _marker2D2
            // 
            this._marker2D2.AutoSize = true;
            this._marker2D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D2.Location = new System.Drawing.Point(6, 36);
            this._marker2D2.Name = "_marker2D2";
            this._marker2D2.Size = new System.Drawing.Size(34, 13);
            this._marker2D2.TabIndex = 17;
            this._marker2D2.Text = "Д2 = ";
            // 
            // _marker2D1
            // 
            this._marker2D1.AutoSize = true;
            this._marker2D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2D1.Location = new System.Drawing.Point(6, 16);
            this._marker2D1.Name = "_marker2D1";
            this._marker2D1.Size = new System.Drawing.Size(34, 13);
            this._marker2D1.TabIndex = 16;
            this._marker2D1.Text = "Д1 = ";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._marker2I4);
            this.groupBox12.Controls.Add(this._marker2I3);
            this.groupBox12.Controls.Add(this._marker2I2);
            this.groupBox12.Controls.Add(this._marker2I1);
            this.groupBox12.Location = new System.Drawing.Point(5, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(119, 95);
            this.groupBox12.TabIndex = 40;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Напряжения";
            // 
            // _marker2I4
            // 
            this._marker2I4.AutoSize = true;
            this._marker2I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I4.Location = new System.Drawing.Point(6, 78);
            this._marker2I4.Name = "_marker2I4";
            this._marker2I4.Size = new System.Drawing.Size(28, 13);
            this._marker2I4.TabIndex = 3;
            this._marker2I4.Text = "In = ";
            // 
            // _marker2I3
            // 
            this._marker2I3.AutoSize = true;
            this._marker2I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I3.Location = new System.Drawing.Point(6, 58);
            this._marker2I3.Name = "_marker2I3";
            this._marker2I3.Size = new System.Drawing.Size(28, 13);
            this._marker2I3.TabIndex = 2;
            this._marker2I3.Text = "Ic = ";
            // 
            // _marker2I2
            // 
            this._marker2I2.AutoSize = true;
            this._marker2I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I2.Location = new System.Drawing.Point(6, 38);
            this._marker2I2.Name = "_marker2I2";
            this._marker2I2.Size = new System.Drawing.Size(28, 13);
            this._marker2I2.TabIndex = 1;
            this._marker2I2.Text = "Ib = ";
            // 
            // _marker2I1
            // 
            this._marker2I1.AutoSize = true;
            this._marker2I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker2I1.Location = new System.Drawing.Point(6, 18);
            this._marker2I1.Name = "_marker2I1";
            this._marker2I1.Size = new System.Drawing.Size(28, 13);
            this._marker2I1.TabIndex = 0;
            this._marker2I1.Text = "Ia = ";
            // 
            // _marker1Box
            // 
            this._marker1Box.Controls.Add(this.groupBox11);
            this._marker1Box.Controls.Add(this.groupBox8);
            this._marker1Box.Location = new System.Drawing.Point(3, 3);
            this._marker1Box.Name = "_marker1Box";
            this._marker1Box.Size = new System.Drawing.Size(133, 309);
            this._marker1Box.TabIndex = 0;
            this._marker1Box.TabStop = false;
            this._marker1Box.Text = "Маркер 1";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._marker1D8);
            this.groupBox11.Controls.Add(this._marker1D7);
            this.groupBox11.Controls.Add(this._marker1D6);
            this.groupBox11.Controls.Add(this._marker1D5);
            this.groupBox11.Controls.Add(this._marker1D4);
            this.groupBox11.Controls.Add(this._marker1D3);
            this.groupBox11.Controls.Add(this._marker1D2);
            this.groupBox11.Controls.Add(this._marker1D1);
            this.groupBox11.Location = new System.Drawing.Point(5, 113);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(119, 186);
            this.groupBox11.TabIndex = 43;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Дискреты";
            // 
            // _marker1D8
            // 
            this._marker1D8.AutoSize = true;
            this._marker1D8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D8.Location = new System.Drawing.Point(6, 156);
            this._marker1D8.Name = "_marker1D8";
            this._marker1D8.Size = new System.Drawing.Size(34, 13);
            this._marker1D8.TabIndex = 23;
            this._marker1D8.Text = "Д8 = ";
            // 
            // _marker1D7
            // 
            this._marker1D7.AutoSize = true;
            this._marker1D7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D7.Location = new System.Drawing.Point(6, 136);
            this._marker1D7.Name = "_marker1D7";
            this._marker1D7.Size = new System.Drawing.Size(34, 13);
            this._marker1D7.TabIndex = 22;
            this._marker1D7.Text = "Д7 = ";
            // 
            // _marker1D6
            // 
            this._marker1D6.AutoSize = true;
            this._marker1D6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D6.Location = new System.Drawing.Point(6, 116);
            this._marker1D6.Name = "_marker1D6";
            this._marker1D6.Size = new System.Drawing.Size(34, 13);
            this._marker1D6.TabIndex = 21;
            this._marker1D6.Text = "Д6 = ";
            // 
            // _marker1D5
            // 
            this._marker1D5.AutoSize = true;
            this._marker1D5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D5.Location = new System.Drawing.Point(6, 96);
            this._marker1D5.Name = "_marker1D5";
            this._marker1D5.Size = new System.Drawing.Size(34, 13);
            this._marker1D5.TabIndex = 20;
            this._marker1D5.Text = "Д5 = ";
            // 
            // _marker1D4
            // 
            this._marker1D4.AutoSize = true;
            this._marker1D4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D4.Location = new System.Drawing.Point(6, 76);
            this._marker1D4.Name = "_marker1D4";
            this._marker1D4.Size = new System.Drawing.Size(34, 13);
            this._marker1D4.TabIndex = 19;
            this._marker1D4.Text = "Д4 = ";
            // 
            // _marker1D3
            // 
            this._marker1D3.AutoSize = true;
            this._marker1D3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D3.Location = new System.Drawing.Point(6, 56);
            this._marker1D3.Name = "_marker1D3";
            this._marker1D3.Size = new System.Drawing.Size(34, 13);
            this._marker1D3.TabIndex = 18;
            this._marker1D3.Text = "Д3 = ";
            // 
            // _marker1D2
            // 
            this._marker1D2.AutoSize = true;
            this._marker1D2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D2.Location = new System.Drawing.Point(6, 36);
            this._marker1D2.Name = "_marker1D2";
            this._marker1D2.Size = new System.Drawing.Size(34, 13);
            this._marker1D2.TabIndex = 17;
            this._marker1D2.Text = "Д2 = ";
            // 
            // _marker1D1
            // 
            this._marker1D1.AutoSize = true;
            this._marker1D1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1D1.Location = new System.Drawing.Point(6, 16);
            this._marker1D1.Name = "_marker1D1";
            this._marker1D1.Size = new System.Drawing.Size(34, 13);
            this._marker1D1.TabIndex = 16;
            this._marker1D1.Text = "Д1 = ";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._marker1I4);
            this.groupBox8.Controls.Add(this._marker1I3);
            this.groupBox8.Controls.Add(this._marker1I2);
            this.groupBox8.Controls.Add(this._marker1I1);
            this.groupBox8.Location = new System.Drawing.Point(5, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(119, 95);
            this.groupBox8.TabIndex = 40;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Напряжения";
            // 
            // _marker1I4
            // 
            this._marker1I4.AutoSize = true;
            this._marker1I4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I4.Location = new System.Drawing.Point(6, 78);
            this._marker1I4.Name = "_marker1I4";
            this._marker1I4.Size = new System.Drawing.Size(28, 13);
            this._marker1I4.TabIndex = 3;
            this._marker1I4.Text = "In = ";
            // 
            // _marker1I3
            // 
            this._marker1I3.AutoSize = true;
            this._marker1I3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I3.Location = new System.Drawing.Point(6, 58);
            this._marker1I3.Name = "_marker1I3";
            this._marker1I3.Size = new System.Drawing.Size(28, 13);
            this._marker1I3.TabIndex = 2;
            this._marker1I3.Text = "Ic = ";
            // 
            // _marker1I2
            // 
            this._marker1I2.AutoSize = true;
            this._marker1I2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I2.Location = new System.Drawing.Point(6, 38);
            this._marker1I2.Name = "_marker1I2";
            this._marker1I2.Size = new System.Drawing.Size(28, 13);
            this._marker1I2.TabIndex = 1;
            this._marker1I2.Text = "Ib = ";
            // 
            // _marker1I1
            // 
            this._marker1I1.AutoSize = true;
            this._marker1I1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._marker1I1.Location = new System.Drawing.Point(6, 18);
            this._marker1I1.Name = "_marker1I1";
            this._marker1I1.Size = new System.Drawing.Size(28, 13);
            this._marker1I1.TabIndex = 0;
            this._marker1I1.Text = "Ia = ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._deltaTimeBox);
            this.groupBox4.Controls.Add(this._marker2TimeBox);
            this.groupBox4.Controls.Add(this._marker1TimeBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 318);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(279, 85);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Время";
            // 
            // _deltaTimeBox
            // 
            this._deltaTimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._deltaTimeBox.Controls.Add(this._markerTimeDelta);
            this._deltaTimeBox.Location = new System.Drawing.Point(6, 46);
            this._deltaTimeBox.Name = "_deltaTimeBox";
            this._deltaTimeBox.Size = new System.Drawing.Size(266, 33);
            this._deltaTimeBox.TabIndex = 2;
            this._deltaTimeBox.TabStop = false;
            this._deltaTimeBox.Text = "Дельта";
            // 
            // _markerTimeDelta
            // 
            this._markerTimeDelta.AutoSize = true;
            this._markerTimeDelta.Location = new System.Drawing.Point(118, 16);
            this._markerTimeDelta.Name = "_markerTimeDelta";
            this._markerTimeDelta.Size = new System.Drawing.Size(30, 13);
            this._markerTimeDelta.TabIndex = 2;
            this._markerTimeDelta.Text = "0 мс";
            // 
            // _marker2TimeBox
            // 
            this._marker2TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker2TimeBox.Controls.Add(this._marker2Time);
            this._marker2TimeBox.Location = new System.Drawing.Point(144, 13);
            this._marker2TimeBox.Name = "_marker2TimeBox";
            this._marker2TimeBox.Size = new System.Drawing.Size(119, 33);
            this._marker2TimeBox.TabIndex = 1;
            this._marker2TimeBox.TabStop = false;
            this._marker2TimeBox.Text = "Маркер 2";
            // 
            // _marker2Time
            // 
            this._marker2Time.AutoSize = true;
            this._marker2Time.Location = new System.Drawing.Point(41, 16);
            this._marker2Time.Name = "_marker2Time";
            this._marker2Time.Size = new System.Drawing.Size(30, 13);
            this._marker2Time.TabIndex = 1;
            this._marker2Time.Text = "0 мс";
            // 
            // _marker1TimeBox
            // 
            this._marker1TimeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._marker1TimeBox.Controls.Add(this._marker1Time);
            this._marker1TimeBox.Location = new System.Drawing.Point(6, 13);
            this._marker1TimeBox.Name = "_marker1TimeBox";
            this._marker1TimeBox.Size = new System.Drawing.Size(118, 33);
            this._marker1TimeBox.TabIndex = 0;
            this._marker1TimeBox.TabStop = false;
            this._marker1TimeBox.Text = "Маркер 1";
            // 
            // _marker1Time
            // 
            this._marker1Time.AutoSize = true;
            this._marker1Time.Location = new System.Drawing.Point(39, 16);
            this._marker1Time.Name = "_marker1Time";
            this._marker1Time.Size = new System.Drawing.Size(30, 13);
            this._marker1Time.TabIndex = 0;
            this._marker1Time.Text = "0 мс";
            // 
            // _xIncreaseButton
            // 
            this._xIncreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xIncreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xIncreaseButton.Location = new System.Drawing.Point(108, 3);
            this._xIncreaseButton.Name = "_xIncreaseButton";
            this._xIncreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xIncreaseButton.TabIndex = 3;
            this._xIncreaseButton.Text = "X +";
            this._xIncreaseButton.UseVisualStyleBackColor = false;
            // 
            // _xDecreaseButton
            // 
            this._xDecreaseButton.BackColor = System.Drawing.Color.ForestGreen;
            this._xDecreaseButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._xDecreaseButton.Location = new System.Drawing.Point(148, 3);
            this._xDecreaseButton.Name = "_xDecreaseButton";
            this._xDecreaseButton.Size = new System.Drawing.Size(33, 20);
            this._xDecreaseButton.TabIndex = 4;
            this._xDecreaseButton.Text = "X -";
            this._xDecreaseButton.UseVisualStyleBackColor = false;
            // 
            // _voltageСheckBox
            // 
            this._voltageСheckBox.AutoSize = true;
            this._voltageСheckBox.BackColor = System.Drawing.Color.Silver;
            this._voltageСheckBox.Checked = true;
            this._voltageСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._voltageСheckBox.Location = new System.Drawing.Point(190, 3);
            this._voltageСheckBox.Name = "_voltageСheckBox";
            this._voltageСheckBox.Size = new System.Drawing.Size(90, 17);
            this._voltageСheckBox.TabIndex = 5;
            this._voltageСheckBox.Text = "Напряжения";
            this._voltageСheckBox.UseVisualStyleBackColor = false;
            this._voltageСheckBox.CheckedChanged += new System.EventHandler(this._currentСonnectionsСheckBox_CheckedChanged);
            // 
            // _discrestsСheckBox
            // 
            this._discrestsСheckBox.AutoSize = true;
            this._discrestsСheckBox.BackColor = System.Drawing.Color.Silver;
            this._discrestsСheckBox.Checked = true;
            this._discrestsСheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this._discrestsСheckBox.Location = new System.Drawing.Point(280, 3);
            this._discrestsСheckBox.Name = "_discrestsСheckBox";
            this._discrestsСheckBox.Size = new System.Drawing.Size(78, 17);
            this._discrestsСheckBox.TabIndex = 6;
            this._discrestsСheckBox.Text = "Дискреты";
            this._discrestsСheckBox.UseVisualStyleBackColor = false;
            this._discrestsСheckBox.CheckedChanged += new System.EventHandler(this._discrestsСheckBox_CheckedChanged);
            // 
            // _oscRunСheckBox
            // 
            this._oscRunСheckBox.AutoSize = true;
            this._oscRunСheckBox.BackColor = System.Drawing.Color.Silver;
            this._oscRunСheckBox.Location = new System.Drawing.Point(360, 3);
            this._oscRunСheckBox.Name = "_oscRunСheckBox";
            this._oscRunСheckBox.Size = new System.Drawing.Size(127, 17);
            this._oscRunСheckBox.TabIndex = 7;
            this._oscRunСheckBox.Text = "Пуск осциллографа";
            this._oscRunСheckBox.UseVisualStyleBackColor = false;
            this._oscRunСheckBox.CheckedChanged += new System.EventHandler(this._oscRunСheckBox_CheckedChanged);
            // 
            // _markCheckBox
            // 
            this._markCheckBox.AutoSize = true;
            this._markCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markCheckBox.Location = new System.Drawing.Point(630, 3);
            this._markCheckBox.Name = "_markCheckBox";
            this._markCheckBox.Size = new System.Drawing.Size(58, 17);
            this._markCheckBox.TabIndex = 8;
            this._markCheckBox.Text = "Метки";
            this._markCheckBox.UseVisualStyleBackColor = false;
            // 
            // _markerCheckBox
            // 
            this._markerCheckBox.AutoSize = true;
            this._markerCheckBox.BackColor = System.Drawing.Color.Silver;
            this._markerCheckBox.Location = new System.Drawing.Point(700, 3);
            this._markerCheckBox.Name = "_markerCheckBox";
            this._markerCheckBox.Size = new System.Drawing.Size(73, 17);
            this._markerCheckBox.TabIndex = 9;
            this._markerCheckBox.Text = "Маркеры";
            this._markerCheckBox.UseVisualStyleBackColor = false;
            this._markerCheckBox.CheckedChanged += new System.EventHandler(this._markerCheckBox_CheckedChanged);
            // 
            // Mr550OscilloscopeResultForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1369, 749);
            this.Controls.Add(this._markerCheckBox);
            this.Controls.Add(this._markCheckBox);
            this.Controls.Add(this._oscRunСheckBox);
            this.Controls.Add(this._discrestsСheckBox);
            this.Controls.Add(this._voltageСheckBox);
            this.Controls.Add(this._xDecreaseButton);
            this.Controls.Add(this._xIncreaseButton);
            this.Controls.Add(this.MAINTABLE);
            this.Name = "Mr550OscilloscopeResultForm";
            this.Text = "Mr600OscilloscopeResultFormV2";
            this.MAINTABLE.ResumeLayout(false);
            this.MAINTABLE.PerformLayout();
            this.MAINPANEL.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.MarkersTable.ResumeLayout(false);
            this.MarkersTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._marker1TrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._marker2TrackBar)).EndInit();
            this.panel3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._markerScrollPanel.ResumeLayout(false);
            this._marker2Box.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._marker1Box.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this._deltaTimeBox.ResumeLayout(false);
            this._deltaTimeBox.PerformLayout();
            this._marker2TimeBox.ResumeLayout(false);
            this._marker2TimeBox.PerformLayout();
            this._marker1TimeBox.ResumeLayout(false);
            this._marker1TimeBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MAINTABLE;
        private System.Windows.Forms.HScrollBar hScrollBar4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel MAINPANEL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button _discrete5Button;
        private System.Windows.Forms.Button _discrete8Button;
        private System.Windows.Forms.Button _discrete7Button;
        private System.Windows.Forms.Button _discrete6Button;
        private System.Windows.Forms.Button _discrete2Button;
        private System.Windows.Forms.Button _discrete3Button;
        private System.Windows.Forms.Button _discrete4Button;
        private System.Windows.Forms.Button _discrete1Button;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.VScrollBar _s1Scroll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button _i3Button;
        private System.Windows.Forms.Button _i4Button;
        private System.Windows.Forms.Button _i1Button;
        private System.Windows.Forms.Button _i2Button;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Button _currentСonnectionsChartDecreaseButton;
        private System.Windows.Forms.Button _currentСonnectionsChartIncreaseButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel MarkersTable;
        private System.Windows.Forms.TrackBar _marker1TrackBar;
        private System.Windows.Forms.TrackBar _marker2TrackBar;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel _markerScrollPanel;
        private System.Windows.Forms.GroupBox _marker2Box;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label _marker2D8;
        private System.Windows.Forms.Label _marker2D7;
        private System.Windows.Forms.Label _marker2D6;
        private System.Windows.Forms.Label _marker2D5;
        private System.Windows.Forms.Label _marker2D4;
        private System.Windows.Forms.Label _marker2D3;
        private System.Windows.Forms.Label _marker2D2;
        private System.Windows.Forms.Label _marker2D1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label _marker2I4;
        private System.Windows.Forms.Label _marker2I3;
        private System.Windows.Forms.Label _marker2I2;
        private System.Windows.Forms.Label _marker2I1;
        private System.Windows.Forms.GroupBox _marker1Box;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label _marker1D8;
        private System.Windows.Forms.Label _marker1D7;
        private System.Windows.Forms.Label _marker1D6;
        private System.Windows.Forms.Label _marker1D5;
        private System.Windows.Forms.Label _marker1D4;
        private System.Windows.Forms.Label _marker1D3;
        private System.Windows.Forms.Label _marker1D2;
        private System.Windows.Forms.Label _marker1D1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox _deltaTimeBox;
        private System.Windows.Forms.Label _markerTimeDelta;
        private System.Windows.Forms.GroupBox _marker2TimeBox;
        private System.Windows.Forms.Label _marker2Time;
        private System.Windows.Forms.GroupBox _marker1TimeBox;
        private System.Windows.Forms.Label _marker1Time;
        private System.Windows.Forms.ToolTip _newMarkToolTip;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label _marker1I4;
        private System.Windows.Forms.Label _marker1I3;
        private System.Windows.Forms.Label _marker1I2;
        private System.Windows.Forms.Label _marker1I1;
        private BEMN_XY_Chart.DAS_Net_XYChart _currentСonnectionsChart;
        private BEMN_XY_Chart.DAS_Net_XYChart _discrestsChart;
        private System.Windows.Forms.Button _xIncreaseButton;
        private System.Windows.Forms.Button _xDecreaseButton;
        private System.Windows.Forms.CheckBox _voltageСheckBox;
        private System.Windows.Forms.CheckBox _discrestsСheckBox;
        private System.Windows.Forms.CheckBox _oscRunСheckBox;
        private System.Windows.Forms.CheckBox _markCheckBox;
        private System.Windows.Forms.CheckBox _markerCheckBox;
    }
}