﻿namespace BEMN.MR550.Version2.Osc.Forms
{
    partial class Mr550OscilloscopeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this._oscJournalDataGrid = new System.Windows.Forms.DataGridView();
            this._oscNumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscReadyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscEndColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscBeginColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscPointColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscLengthColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscOtschLengthColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscJournalReadButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._stopReadOsc = new System.Windows.Forms.Button();
            this._oscShowButton = new System.Windows.Forms.Button();
            this._oscLoadButton = new System.Windows.Forms.Button();
            this._oscSaveButton = new System.Windows.Forms.Button();
            this._oscProgressBar = new System.Windows.Forms.ProgressBar();
            this._oscReadButton = new System.Windows.Forms.Button();
            this._oscilloscopeCountCb = new System.Windows.Forms.ComboBox();
            this._oscilloscopeCountLabel = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._saveOscilloscopeDlg = new System.Windows.Forms.SaveFileDialog();
            this._openOscilloscopeDlg = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(3, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountCb);
            this.splitContainer1.Panel2.Controls.Add(this._oscilloscopeCountLabel);
            this.splitContainer1.Size = new System.Drawing.Size(816, 437);
            this.splitContainer1.SplitterDistance = 317;
            this.splitContainer1.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this._oscJournalDataGrid);
            this.groupBox1.Controls.Add(this._oscJournalReadButton);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(811, 311);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Журнал осциллографа";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(459, 286);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(181, 17);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "Технологическая информация";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // _oscJournalDataGrid
            // 
            this._oscJournalDataGrid.AllowUserToAddRows = false;
            this._oscJournalDataGrid.AllowUserToDeleteRows = false;
            this._oscJournalDataGrid.AllowUserToResizeColumns = false;
            this._oscJournalDataGrid.AllowUserToResizeRows = false;
            this._oscJournalDataGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._oscJournalDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscJournalDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._oscNumColumn,
            this._dateCol,
            this._oscTimeColumn,
            this._oscStageColumn,
            this._oscReadyColumn,
            this._oscStartColumn,
            this._oscEndColumn,
            this._oscBeginColumn,
            this._oscPointColumn,
            this._oscLengthColumn,
            this._oscOtschLengthColumn});
            this._oscJournalDataGrid.Location = new System.Drawing.Point(6, 19);
            this._oscJournalDataGrid.Name = "_oscJournalDataGrid";
            this._oscJournalDataGrid.ReadOnly = true;
            this._oscJournalDataGrid.RowHeadersVisible = false;
            this._oscJournalDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscJournalDataGrid.RowTemplate.Height = 24;
            this._oscJournalDataGrid.ShowCellErrors = false;
            this._oscJournalDataGrid.ShowRowErrors = false;
            this._oscJournalDataGrid.Size = new System.Drawing.Size(799, 257);
            this._oscJournalDataGrid.TabIndex = 5;
            this._oscJournalDataGrid.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this._oscJournalDataGrid_RowEnter);
            // 
            // _oscNumColumn
            // 
            this._oscNumColumn.DataPropertyName = "_oscNumColumn";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this._oscNumColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this._oscNumColumn.Frozen = true;
            this._oscNumColumn.HeaderText = "№";
            this._oscNumColumn.Name = "_oscNumColumn";
            this._oscNumColumn.ReadOnly = true;
            this._oscNumColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscNumColumn.Width = 70;
            // 
            // _dateCol
            // 
            this._dateCol.DataPropertyName = "_dateCol";
            this._dateCol.HeaderText = "Дата";
            this._dateCol.Name = "_dateCol";
            this._dateCol.ReadOnly = true;
            this._dateCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dateCol.Width = 90;
            // 
            // _oscTimeColumn
            // 
            this._oscTimeColumn.DataPropertyName = "_oscTimeColumn";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscTimeColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._oscTimeColumn.HeaderText = "Время";
            this._oscTimeColumn.Name = "_oscTimeColumn";
            this._oscTimeColumn.ReadOnly = true;
            this._oscTimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscTimeColumn.Width = 90;
            // 
            // _oscStageColumn
            // 
            this._oscStageColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._oscStageColumn.DataPropertyName = "_oscStageColumn";
            this._oscStageColumn.HeaderText = "Ступень";
            this._oscStageColumn.Name = "_oscStageColumn";
            this._oscStageColumn.ReadOnly = true;
            this._oscStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscStageColumn.Width = 54;
            // 
            // _oscReadyColumn
            // 
            this._oscReadyColumn.DataPropertyName = "_oscReadyColumn";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscReadyColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this._oscReadyColumn.HeaderText = "Готовность";
            this._oscReadyColumn.Name = "_oscReadyColumn";
            this._oscReadyColumn.ReadOnly = true;
            this._oscReadyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscReadyColumn.Visible = false;
            this._oscReadyColumn.Width = 80;
            // 
            // _oscStartColumn
            // 
            this._oscStartColumn.DataPropertyName = "_oscStartColumn";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscStartColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this._oscStartColumn.HeaderText = "Адрес начала";
            this._oscStartColumn.Name = "_oscStartColumn";
            this._oscStartColumn.ReadOnly = true;
            this._oscStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscStartColumn.Visible = false;
            this._oscStartColumn.Width = 80;
            // 
            // _oscEndColumn
            // 
            this._oscEndColumn.DataPropertyName = "_oscEndColumn";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscEndColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this._oscEndColumn.HeaderText = "Адрес конца";
            this._oscEndColumn.Name = "_oscEndColumn";
            this._oscEndColumn.ReadOnly = true;
            this._oscEndColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscEndColumn.Visible = false;
            this._oscEndColumn.Width = 110;
            // 
            // _oscBeginColumn
            // 
            this._oscBeginColumn.DataPropertyName = "_oscBeginColumn";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscBeginColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._oscBeginColumn.HeaderText = "Начало";
            this._oscBeginColumn.Name = "_oscBeginColumn";
            this._oscBeginColumn.ReadOnly = true;
            this._oscBeginColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscBeginColumn.Visible = false;
            // 
            // _oscPointColumn
            // 
            this._oscPointColumn.DataPropertyName = "_oscPointColumn";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscPointColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._oscPointColumn.HeaderText = "Размер";
            this._oscPointColumn.MinimumWidth = 100;
            this._oscPointColumn.Name = "_oscPointColumn";
            this._oscPointColumn.ReadOnly = true;
            this._oscPointColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _oscLengthColumn
            // 
            this._oscLengthColumn.DataPropertyName = "_oscLengthColumn";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscLengthColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._oscLengthColumn.HeaderText = "После аварии";
            this._oscLengthColumn.Name = "_oscLengthColumn";
            this._oscLengthColumn.ReadOnly = true;
            this._oscLengthColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscLengthColumn.Visible = false;
            this._oscLengthColumn.Width = 70;
            // 
            // _oscOtschLengthColumn
            // 
            this._oscOtschLengthColumn.DataPropertyName = "_oscOtschLengthColumn";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscOtschLengthColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._oscOtschLengthColumn.HeaderText = "Размер отсчета";
            this._oscOtschLengthColumn.Name = "_oscOtschLengthColumn";
            this._oscOtschLengthColumn.ReadOnly = true;
            this._oscOtschLengthColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._oscOtschLengthColumn.Visible = false;
            this._oscOtschLengthColumn.Width = 75;
            // 
            // _oscJournalReadButton
            // 
            this._oscJournalReadButton.Location = new System.Drawing.Point(646, 282);
            this._oscJournalReadButton.Name = "_oscJournalReadButton";
            this._oscJournalReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscJournalReadButton.TabIndex = 4;
            this._oscJournalReadButton.Text = "Перечитать журнал";
            this._oscJournalReadButton.UseVisualStyleBackColor = true;
            this._oscJournalReadButton.Click += new System.EventHandler(this._oscJournalReadButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._stopReadOsc);
            this.groupBox2.Controls.Add(this._oscShowButton);
            this.groupBox2.Controls.Add(this._oscLoadButton);
            this.groupBox2.Controls.Add(this._oscSaveButton);
            this.groupBox2.Controls.Add(this._oscProgressBar);
            this.groupBox2.Controls.Add(this._oscReadButton);
            this.groupBox2.Location = new System.Drawing.Point(3, 34);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(811, 77);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Осциллограмма";
            // 
            // _stopReadOsc
            // 
            this._stopReadOsc.Enabled = false;
            this._stopReadOsc.Location = new System.Drawing.Point(320, 19);
            this._stopReadOsc.Name = "_stopReadOsc";
            this._stopReadOsc.Size = new System.Drawing.Size(155, 23);
            this._stopReadOsc.TabIndex = 12;
            this._stopReadOsc.Text = "Остановить чтение";
            this._stopReadOsc.UseVisualStyleBackColor = true;
            this._stopReadOsc.Click += new System.EventHandler(this._stopReadOsc_Click);
            // 
            // _oscShowButton
            // 
            this._oscShowButton.Enabled = false;
            this._oscShowButton.Location = new System.Drawing.Point(6, 48);
            this._oscShowButton.Name = "_oscShowButton";
            this._oscShowButton.Size = new System.Drawing.Size(155, 23);
            this._oscShowButton.TabIndex = 10;
            this._oscShowButton.Text = "Показать осциллограмму";
            this._oscShowButton.UseVisualStyleBackColor = true;
            this._oscShowButton.Click += new System.EventHandler(this._oscShowButton_Click);
            // 
            // _oscLoadButton
            // 
            this._oscLoadButton.Location = new System.Drawing.Point(647, 48);
            this._oscLoadButton.Name = "_oscLoadButton";
            this._oscLoadButton.Size = new System.Drawing.Size(155, 23);
            this._oscLoadButton.TabIndex = 9;
            this._oscLoadButton.Text = "Загрузить из файла";
            this._oscLoadButton.UseVisualStyleBackColor = true;
            this._oscLoadButton.Click += new System.EventHandler(this._oscLoadButton_Click);
            // 
            // _oscSaveButton
            // 
            this._oscSaveButton.Enabled = false;
            this._oscSaveButton.Location = new System.Drawing.Point(647, 19);
            this._oscSaveButton.Name = "_oscSaveButton";
            this._oscSaveButton.Size = new System.Drawing.Size(155, 23);
            this._oscSaveButton.TabIndex = 7;
            this._oscSaveButton.Text = "Сохранить в файл";
            this._oscSaveButton.UseVisualStyleBackColor = true;
            this._oscSaveButton.Click += new System.EventHandler(this._oscSaveButton_Click);
            // 
            // _oscProgressBar
            // 
            this._oscProgressBar.Location = new System.Drawing.Point(167, 48);
            this._oscProgressBar.Name = "_oscProgressBar";
            this._oscProgressBar.Size = new System.Drawing.Size(474, 23);
            this._oscProgressBar.Step = 1;
            this._oscProgressBar.TabIndex = 6;
            // 
            // _oscReadButton
            // 
            this._oscReadButton.Enabled = false;
            this._oscReadButton.Location = new System.Drawing.Point(6, 19);
            this._oscReadButton.Name = "_oscReadButton";
            this._oscReadButton.Size = new System.Drawing.Size(155, 23);
            this._oscReadButton.TabIndex = 5;
            this._oscReadButton.Text = "Прочитать осциллограмму";
            this._oscReadButton.UseVisualStyleBackColor = true;
            this._oscReadButton.Click += new System.EventHandler(this._oscReadButton_Click);
            // 
            // _oscilloscopeCountCb
            // 
            this._oscilloscopeCountCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscilloscopeCountCb.Enabled = false;
            this._oscilloscopeCountCb.FormattingEnabled = true;
            this._oscilloscopeCountCb.Location = new System.Drawing.Point(493, 7);
            this._oscilloscopeCountCb.Name = "_oscilloscopeCountCb";
            this._oscilloscopeCountCb.Size = new System.Drawing.Size(56, 21);
            this._oscilloscopeCountCb.TabIndex = 9;
            // 
            // _oscilloscopeCountLabel
            // 
            this._oscilloscopeCountLabel.AutoSize = true;
            this._oscilloscopeCountLabel.Enabled = false;
            this._oscilloscopeCountLabel.Location = new System.Drawing.Point(252, 10);
            this._oscilloscopeCountLabel.Name = "_oscilloscopeCountLabel";
            this._oscilloscopeCountLabel.Size = new System.Drawing.Size(235, 13);
            this._oscilloscopeCountLabel.TabIndex = 8;
            this._oscilloscopeCountLabel.Text = "Выберите номер читаемой осциллограммы :";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 451);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(823, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Always;
            this._statusLabel.Size = new System.Drawing.Size(16, 17);
            this._statusLabel.Text = "...";
            this._statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _saveOscilloscopeDlg
            // 
            this._saveOscilloscopeDlg.FileName = "Осциллограмма МР 550";
            this._saveOscilloscopeDlg.Filter = "Файл осциллограммы|*.hdr";
            this._saveOscilloscopeDlg.Title = "Сохранить осциллограмму";
            // 
            // _openOscilloscopeDlg
            // 
            this._openOscilloscopeDlg.Filter = "Файл осциллограммы|*.hdr";
            this._openOscilloscopeDlg.Title = "Открыть осциллограмму";
            // 
            // Mr550OscilloscopeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 473);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(839, 512);
            this.Name = "Mr550OscilloscopeForm";
            this.Text = "Mr600OscilloscopeFormV2";
            this.Load += new System.EventHandler(this.OscilloscopeForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscJournalDataGrid)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridView _oscJournalDataGrid;
        private System.Windows.Forms.Button _oscJournalReadButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _stopReadOsc;
        private System.Windows.Forms.Button _oscShowButton;
        private System.Windows.Forms.Button _oscLoadButton;
        private System.Windows.Forms.Button _oscSaveButton;
        private System.Windows.Forms.ProgressBar _oscProgressBar;
        private System.Windows.Forms.Button _oscReadButton;
        private System.Windows.Forms.ComboBox _oscilloscopeCountCb;
        private System.Windows.Forms.Label _oscilloscopeCountLabel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.SaveFileDialog _saveOscilloscopeDlg;
        private System.Windows.Forms.OpenFileDialog _openOscilloscopeDlg;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscNumColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscTimeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscStageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscReadyColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscStartColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscEndColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscBeginColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscPointColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscLengthColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _oscOtschLengthColumn;
    }
}