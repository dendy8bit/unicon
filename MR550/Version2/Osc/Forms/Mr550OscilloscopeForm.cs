﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.MR550.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version2.Osc.HelpClasses;
using BEMN.MR550.Version2.Osc.Structures;

namespace BEMN.MR550.Version2.Osc.Forms
{
    public partial class Mr550OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "Осциллограмма";
        private const string READ_OSC_FAIL = "Невозможно прочитать журнал осциллографа";
        private const string RECORDS_IN_JOURNAL = "Осциллограмм в журнале - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "Осцилограмма успешно загружена";
        private const string READ_OSC_STOPPED = "Чтение осциллограммы прекращено";
        private const string ERROR_SAVE_OSC = "Ошибка сохранения файла";
        private const string ERROR_LOAD_OSC = "Невозможно загрузить осцилограмму";
        private const string OSC_LOAD_OK_PATTERN = "Осциллограмма загружена из файла \"{0}\"";
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Загрузчик журнала
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// Загрузчик страниц
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        private OscJournalStruct _journalStruct;
        /// <summary>
        /// Данные осц
        /// </summary>
        private CountingList _countingList;
        private readonly MemoryEntity<MeasureTransStruct> _voltageConfiguration;
        private Mr550Device _device;
        /// <summary>
        /// Таблица журнала
        /// </summary>
        private readonly DataTable _table;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr550OscilloscopeForm()
        {
            this.InitializeComponent();
        }

        public Mr550OscilloscopeForm(Mr550Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._table = this.GetJournalDataTable();
            this._oscJournalDataGrid.DataSource = this._table;
            this._voltageConfiguration = device.VoltageConfiguration;
            this._voltageConfiguration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.VoltageConfigurationReadOk);
            this._voltageConfiguration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            //Загрузчик журнала
            this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            //Загрузчик страниц
            this._pageLoader = new OscPageLoader(device.SetStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, this.ReadStop);
        }

        #endregion [Ctor's]

        #region [Properties]
        /// <summary>
        /// Определяет возможность выбрать осцилограмму для чтения
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]

        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr550Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr550OscilloscopeForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr550OscilloscopeForm.OSC; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text = "Журнал пуст";
            }
            this.EnableButtons = true;
        } 

        private void VoltageConfigurationReadOk()
        {
            this._oscJournalLoader.StartReadJournal();  
        }

        /// <summary>
        /// Невозможно прочитать журнал - выводим сообщение об ошибке
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }
        
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._table.Rows.Add(this._oscJournalLoader.OscRecords[OscJournalStruct.RecordIndex].GetRecord);
            //this._oscJournalDataGrid.Refresh();
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
        }

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            //this.CountingList = this._pageLoader.CountingList;
            try
            {
                this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, this._voltageConfiguration.Value);
            }
            catch (Exception)
            {
                MessageBox.Show("Данные осциллограммы повреждены или неверны", "Внимание", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
        }

        #endregion [Help Classes Events Handlers]

        #region [Help members]
        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            this._stopReadOsc.Enabled = false;
            this._oscProgressBar.Value = 0;
        }

        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("МР550_журнал_осциллографа");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]

        /// <summary>
        /// Загрузка формы
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._voltageConfiguration.LoadStruct();
        }

        /// <summary>
        /// Перечитать журнал
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscJournalLoader.Reset();
            this._table.Clear();
            this.CanSelectOsc = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this.EnableButtons = false;
            this._voltageConfiguration.LoadStruct();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// Прочитать осциллограмму
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._voltageConfiguration.Value);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //Включаем возможность остановить чтение осцилограммы
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// Сохранить осциллограмму в файл
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this._pageLoader.CountingList.Save(this._saveOscilloscopeDlg.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show(ERROR_SAVE_OSC);
                }
            }
        }

        /// <summary>
        /// Загрузить осциллограмму из файла
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                string fileName = Path.GetFileNameWithoutExtension(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format(OSC_LOAD_OK_PATTERN, fileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = ERROR_LOAD_OSC;
            }
        }

        /// <summary>
        /// Остановить чтение осцилограммы
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            this._oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this._countingList == null)
            {
                this._countingList = new CountingList(new ushort[12000], new OscJournalStruct(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"МР550 v{this._device.DeviceVersion} Осциллограмма");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new Mr550OscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
        #endregion [Event Handlers]
    }
}
