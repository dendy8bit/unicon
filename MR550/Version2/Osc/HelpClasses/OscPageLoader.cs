﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR550.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version2.Osc.Structures;

namespace BEMN.MR550.Version2.Osc.HelpClasses
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoader
    {
        #region [Constants]
        /// <summary>
        /// Размер одной страницы в словах
        /// </summary>
        private const int PAGE_SIZE = 1024;

        public const int FULL_OSC_SIZE_IN_WORDS = 53245;
        public const int FULL_OSC_SIZE_IN_PAGES = 52;
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список занчений страниц
        /// </summary>
        private List<OscPage> _pagesWords; 
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStruct _journalStruct;
        /// <summary>
        /// Осцилограмма в виде отсчётов
        /// </summary>
        private CountingList _countingList;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;

        private readonly MemoryEntityOperationComplite _opCompleteDelegate1;
        private readonly MemoryEntityOperationComplite _opCompleteDelegate2;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        private MeasureTransStruct _oscOptions;
        #endregion [Events]



        #region [Ctor's]
        public OscPageLoader(MemoryEntity<OneWordStruct> setStartPage, MemoryEntity<OscPage> oscPage)
        {
            this._setStartPage = setStartPage;
            this._oscPage = oscPage;

            //Установка начальной страницы осциллограммы
            this._setStartPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscPage.LoadStruct);
            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
        }
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStruct">Запись журнала о осцилограмме</param>
        /// <param name="oscOptions">Параматры осцилографа</param>
        public void StartRead(OscJournalStruct journalStruct, MeasureTransStruct oscOptions)
        {
            this._oscOptions = oscOptions;
            this._needStop = false;

            this._journalStruct = journalStruct;
            this._startPage = journalStruct.OscStartIndex;

            this._startWordIndex = this._journalStruct.Point % PAGE_SIZE;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStruct.Len * this._journalStruct.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / PAGE_SIZE);
            
            this._endPage = this._startPage + this._pageCount;

            this._pagesWords = new List<OscPage>();
            this.WritePageNumber((ushort)this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        }
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            //if (this._needStop)
            //{
            //    this.OnRaiseOscReadStopped();
            //    return;
            //}
            //if(this._pagesWords == null)
            //{
            //    this._oscPage.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
            //    return;
            //}
            //this._pagesWords.Add(this._startPage == FULL_OSC_SIZE_IN_PAGES - 1
            //    ? this._oscPage.Value.Words.Take(this._oscPage.Value.Words.Length-FULL_OSC_SIZE_IN_PAGES*PAGE_SIZE-FULL_OSC_SIZE_IN_WORDS).ToArray()
            //    : this._oscPage.Value.Words);
            //this._startPage++;
            ////Читаем пока не дойдём до последней страницы.
            //if (this._startPage < this._endPage)
            //{
            //    //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
            //    if (this._startPage < FULL_OSC_SIZE_IN_PAGES)
            //    {
            //        this.WritePageNumber((ushort)this._startPage);
            //    }
            //    else
            //    {
            //        this.WritePageNumber((ushort)(this._startPage-FULL_OSC_SIZE_IN_PAGES));
            //    }
            //}
            //else
            //{
            //    this.OscReadComplite();
            //    this.OnRaiseOscReadSuccessful();
            //}
            //this.OnRaisePageRead();
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();
                return;
            }
            this._pagesWords.Add(this._oscPage.Value.Clone<OscPage>());
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < FULL_OSC_SIZE_IN_PAGES)
                {
                    this.WritePageNumber((ushort)this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort)(this._startPage - FULL_OSC_SIZE_IN_PAGES));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();
        }
        
        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {
            //Результирующий массив
            ushort[] resultMassiv = new ushort[this._resultLenInWords];
            ushort[] startPageValueArray = this._pagesWords[0].Words;
            int destanationIndex = 0;
            //Копируем часть первой страницы
            Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
            destanationIndex += startPageValueArray.Length - this._startWordIndex;

            for (int i = 1; i < this._pagesWords.Count - 1; i++)
            {
                ushort[] pageValue = this._pagesWords[i].Words;
                Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                destanationIndex += pageValue.Length;
            }

            OscPage endPage = this._pagesWords[this._pagesWords.Count - 1];
            Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);
            
            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            int b = (this._journalStruct.Len - this._journalStruct.After) * this._journalStruct.SizeReference;
            //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStruct.Begin < this._journalStruct.Point) // Если BEGIN меньше POINT, то:
            {
                //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStruct.Begin + FULL_OSC_SIZE_IN_WORDS - this._journalStruct.Point;
            }
            else //Если BEGIN больше POINT, то:
            {
                c = this._journalStruct.Begin - this._journalStruct.Point; //c = BEGIN – POINT
            }
            int start = c - b; //START = c – b
            if (start < 0) //Если START меньше 0, то:
            {
                start += this._journalStruct.Len * this._journalStruct.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
            //Перевёрнутый массив
            ushort[] invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);

            this.ResultArray = invertedMass;
            //this._countingList = new CountingList(invertedMass, this._journalStruct, this._oscOptions);
        }
        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setStartPage.Value.Word = pageNumber;
            this._setStartPage.SaveStruct6();
        }
        #endregion [Help members]


        #region [Properties]
        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }
        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        /// <summary>
        /// Осцилограмма в виде отсчётов
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
        }
        public void Close()
        {
            this._oscPage.RemoveStructQueries();
            this._setStartPage.RemoveStructQueries();
            //ОТПИСКА ОТ МЕТОДОВ
            this._setStartPage.AllWriteOk -= this._opCompleteDelegate1;
            this._oscPage.AllReadOk -= this._opCompleteDelegate2;
        }

        #endregion [Properties]
    }
}
