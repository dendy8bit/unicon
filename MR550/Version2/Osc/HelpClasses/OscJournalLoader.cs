﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MR550.Version2.Osc.Structures;

namespace BEMN.MR550.Version2.Osc.HelpClasses
{
    public class OscJournalLoader
    {
        #region [Private fields]

        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStruct> _oscJournal;

        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStruct> _oscRecords;

        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;
        
        #endregion [Private fields]


        #region [Events]

        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;

        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;

        public event Action JournalIsEmpty;

        public event Action AllJournalReadOk;

        #endregion [Events]


        #region [Ctor's]

        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="refreshOscJournal">Объект сброса журнала</param>
        public OscJournalLoader(MemoryEntity<OscJournalStruct> oscJournal, MemoryEntity<OneWordStruct> refreshOscJournal)
        {
            this._oscRecords = new List<OscJournalStruct>();
            
            //Сброс журнала на нулевую запись
            this._refreshOscJournal = refreshOscJournal;
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this.StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }

        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
        }

        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStruct> OscRecords
        {
            get { return this._oscRecords; }
        }

        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]

        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }

        /// <summary>
        /// Журнал сброшен. Запуск чтения записей
        /// </summary>
        private void StartReadOscJournal()
        {
            this._oscJournal.LoadStruct();
        }

        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStruct.RecordIndex = this._recordNumber;
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStruct>());
                this._recordNumber = this._recordNumber + 1;
                if (this.ReadRecordOk != null)
                    this.ReadRecordOk.Invoke();
                this.SaveNumberOsc();
            }
            else
            {
                if (AllJournalReadOk != null)
                    this.AllJournalReadOk.Invoke();
            }
        }

        private void SaveNumberOsc()
        {
            this._refreshOscJournal.Value.Word = (ushort) this._recordNumber;
            this._refreshOscJournal.SaveStruct6();
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]

        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            //this._oscOptions.LoadStruct();
            this._recordNumber = 0;
            this.SaveNumberOsc();
        }

        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}
