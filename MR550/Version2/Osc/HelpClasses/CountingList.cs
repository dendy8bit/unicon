﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using BEMN.MR550.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version2.Osc.Structures;

namespace BEMN.MR550.Version2.Osc.HelpClasses
{
    /// <summary>
    /// Коллекция отсчётов осцилограммы
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// Размер одного отсчёта(в словах)
        /// </summary>
        private const int COUNTING_SIZE = 5;

        private const int CURRENTS_COUNT = 4;

        private const int DISCRETS_COUNT = 8;

        #endregion [Constants]

        #region [Private fields]
        /// <summary>
        /// Массив отсчётов разбитый на слова
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// Массив дискрет 1-8
        /// </summary>
        private ushort[][] _discrets;

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        private int _count;

        /// <summary>
        /// Токи
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        private int _alarm;
        /// <summary>
        /// Минимальный ток
        /// </summary>
        private double _minI;
        /// <summary>
        /// Максимальный ток
        /// </summary>
        private double _maxI;

        private OscJournalStruct _oscJournalStruct;
        private readonly MeasureTransStruct _romMeasuringStruct;
        private static Regex FactorRegex = new Regex(@"\d+\,I\w+\,\,\,A\,(?<factorA>[0-9\.]+)\,(?<factorB>[0-9\.\-]+)");

        private short[][] _baseCurrents;
        #endregion [Private fields]

        private string[] _iNames = {"Ia", "Ib", "Ic", "In"};

        /// <summary>
        /// Дискреты
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets;}
            set { this._discrets = value; }
        }

        /// <summary>
        /// Перещитанные
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        }

        /// <summary>
        /// Общее количество отсчётов
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// Авария("Пуск осцилографа") в отсчётах
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }

        /// <summary>
        /// Минимальный ток
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// Максимальный ток
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        public bool IsLoad { get; private set; }
        public string FilePath { get; private set; }

        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        private string _stage;
        public string Stage
        {
            get { return this._stage; }
        }

        public string Hdr { get; private set; }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasureTransStruct romMeasuringStruct)
        {
            this._oscJournalStruct = oscJournalStruct;
            this._romMeasuringStruct = romMeasuringStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //Общее количество отсчётов
            this._count = pageValue.Length / COUNTING_SIZE;
            //Инициализация массива
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }
          
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short)(a - 32768)).ToArray();
                double factor = (i != CURRENTS_COUNT-1 ? 40*this._romMeasuringStruct.Tt : 5*this._romMeasuringStruct.Ttnp)/32768.0* Math.Sqrt(2);
                this._currents[i] = this._baseCurrents[i].Select(a => a  * factor).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }

            ushort[][] d1To8AndC1To8 = this.DiscretArrayInit(this._countingArray[CURRENTS_COUNT]);
            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d1To8AndC1To8.Take(8));
            this._discrets = dicrets.ToArray();
            //this._discrets = this.DiscretArrayInit(this._countingArray[CURRENTS_COUNT]);

            this.Hdr = string.Format("МР 550 {0} {1} ступень - {2}", oscJournalStruct.GetDate,
                oscJournalStruct.GetTime, oscJournalStruct.Stage);
        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// Превращает каждое слово в инвертированный массив бит(значения 0/1) 
        /// </summary>
        /// <param name="sourseArray">Массив массивов бит</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {

            ushort[][] result = new ushort[DISCRETS_COUNT][];
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        }
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine(this.Hdr);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                hdrFile.WriteLine("Stage = {0}", this._stage);
            }
            
            string cgfPath = Path.ChangeExtension(filePath, "cfg");

            using (StreamWriter cgfFile = new StreamWriter(cgfPath))
            {
                cgfFile.WriteLine("MP550,1");
                cgfFile.WriteLine("12,4A,8D");
                int index = 1;
                for (int i = 0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    double factorA = (i != 3 ? 40 * this._romMeasuringStruct.Tt : 5 * this._romMeasuringStruct.Ttnp) / 32768.0 * Math.Sqrt(2);
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index, this._iNames[i], factorA.ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i + 1);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}", i, i * 1000);
                    foreach (short[] current in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);
                    }
                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Время срабатывания : "))
            {
                timeString = hdrStrings[0].Replace("Время срабатывания : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }

            string almStr = hdrStrings.First(s => s.Contains("Alarm = "));
            int alarm = int.Parse(almStr.Replace("Alarm = ", string.Empty));

            string stage;
            try
            {
                stage = hdrStrings.First(s => s.Contains("Stage = ")).Replace("Stage = ", string.Empty);
            }
            catch
            {
                stage = string.Empty;
            }
            
            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[,] factors = new double[4,2];
            for (int i = 2; i < 2 + 4; i++)
            {
                string fA = FactorRegex.Match(cfgStrings[i]).Groups["factorA"].Value;
                string fB = FactorRegex.Match(cfgStrings[i]).Groups["factorB"].Value;
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };
                factors[i - 2, 0] = double.Parse(fA, format);
                factors[i - 2, 1] = double.Parse(fB, format);
            }
            int counts = int.Parse(cfgStrings[16].Replace("1000,", string.Empty));
            CountingList result = new CountingList(counts);
            result._alarm = alarm;
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            const string dataPattern = @"^\d+\,\d+,(?<I1>\-?\d+),(?<I2>\-?\d+),(?<I3>\-?\d+),(?<I4>\-?\d+),(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+)";
            Regex dataRegex = new Regex(dataPattern);

            double[][] currents = new double[CURRENTS_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }
            
            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j, 0] + factors[j, 1];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }
            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());
            }
            result.Currents = currents;
            result.Discrets = discrets;
            result.FilePath = filePath;
            result.IsLoad = true;
            result._dateTime = timeString;
            result._stage = stage;
            return result;
        }
        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            this._count = count;
        }

    }
}
