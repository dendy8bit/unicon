﻿using BEMN.Devices;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR550.Version2.AlarmJournal.Structures
{
    public class AlarmJournalStruct : StructBase
    {
        [Layout(0, Count = 32)] private AlarmJournalRecordStruct[] _records;

        public AlarmJournalRecordStruct[] Records
        {
            get { return this._records; }
        }

        public override object GetSlots(ushort start, bool slotArray, int len)
        {
            int arrayLength = 32;
            Device.slot[] slots = new Device.slot[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                slots[i] = new Device.slot(start, (ushort)(start + 28));
                start += 64;
            }

            return slots;
        }
    }
}
