using BEMN.MBServer;
using BMTCD.HelperClasses;
using BMTCD.�ompilationScheme.Compilers;
using SchemeEditorSystem;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using AssemblyResources;
using BEMN.Compressor;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;

namespace BEMN.MR550.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Variables

        private const int COUNT_EXCHANGES_PROGRAM = 16;
        private const int COUNT_EXCHANGES_PROGECT = 128;
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<ProgramStorageStruct> _currentStorageStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private Mr550Device _device;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private SaveFileDialog _saveFile;
        private Compiler _compiller;
        private bool _isOnSimulateMode;
        private MessageBoxForm _formCheck;
        private ushort[] binFile;
        protected OutputWindow _outputWindow;
        protected LibraryBox _libraryWindow;
        protected ToolStrip _toolBarWindow;
        protected ToolStrip _mainToolStrip;
        protected Content _outputContent;
        protected Content _libraryContent;
        protected byte[] _slot1;
        protected byte[] _slot2;
        protected bool _captionBars = true;
        protected bool _closeButtons = true;
        protected VisualStyle _style;
        protected ImageList _internalImages;
        protected StatusBar _statusBar;
        protected Crownwood.Magic.Menus.MenuControl _topMenu;
        protected Crownwood.Magic.Controls.TabControl _filler;
        protected Crownwood.Magic.Controls.TabControl.VisualAppearance _tabAppearance 
            = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiForm;
        #endregion
        
        #region Constructor
        public BSBGLEF()
        {
            InitForm();
        }
        public BSBGLEF(Mr550Device device)
        {
            _device = device;
            InitForm();
            _currentSourceProgramStruct = _device.SourceProgramStruct;
            _currentStartProgramStruct = _device.ProgramStartStruct;
            _currentSignalsStruct = _device.ProgramSignalsStruct;
            _currentStorageStruct = _device.ProgramStorageStruct;
            //���������
            _device.SourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStructWriteOk);
            _device.SourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStructWriteFail);
            _device.SourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            _device.SourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                _device.SourceProgramStruct.RemoveStructQueries();
                ProgramStructWriteFail();
            });
            //����� ���������
            _device.ProgramStorageStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageReadOk);
            _device.ProgramStorageStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageReadFail);
            _device.ProgramStorageStruct.ReadOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            _device.ProgramStorageStruct.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                _device.ProgramStorageStruct.RemoveStructQueries();
                ProgramStorageReadFail();
            });
            _device.ProgramStorageStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageWriteOk);
            _device.ProgramStorageStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStorageWriteFail);
            _device.ProgramStorageStruct.WriteOk += HandlerHelper.CreateHandler(this, ExchangeOk);
            _device.ProgramStorageStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                _device.ProgramStorageStruct.RemoveStructQueries();
                ProgramStorageWriteFail();
            });
            //����� ���������
            _device.ProgramStartStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, ProgramStartSaveOk);
            _device.ProgramStartStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, ProgramStartSaveFail);
            //�������� ��������
            _device.ProgramSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, SignalsLoadOk);
            _device.ProgramSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, ProgramSignalsLoadFail);
        }
        
        private void InitForm()
        {
            InitializeComponent();
            _style = VisualStyle.IDE;
            _manager = new DockingManager(this, _style);
            _newForm = new NewSchematicForm();
            _compiller = new Compiler("MR550");
        }

        #endregion

        #region Device events handlers
        private void ExchangeOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(() => _formCheck.ProgramExchangeOk()));
            }
            catch (InvalidOperationException)
            {}
        }
        private void ProgramStorageReadFail()
        {
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
        }

        private void ProgramStructWriteFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ProgramSaveFail));
            }
            catch (InvalidOperationException)
            {}
        }

        private void ProgramStructWriteOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(WriteOk));
            }
            catch (InvalidOperationException)
            {}
        }

        private void WriteOk()
        {
            _formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
            var values = new ushort[1];
            values[0] = 0x00FF;
            StartStruct ss = new StartStruct();
            ss.InitStruct(Common.TOBYTES(values, false));
            _currentStartProgramStruct.Value = ss;
            _currentStartProgramStruct.SaveStruct5();
        }
        private void ProgramStorageReadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(StorageReadOk));
            }
            catch (InvalidOperationException)
            { }

        }
        private void StorageReadOk()
        {
            var value = _currentStorageStruct.Values;
            UncompresseProject(value);
            _formCheck.ShowResultMessage(InformationMessages.PROJECT_LOADED_OK_OF_DEVICE);
        }

        private void ProgramStorageWriteOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(StorageWriteOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void StorageWriteOk()
        {
            _formCheck.ShowResultMessage(InformationMessages.PROGRAM_ARCHIVE_SAVE_OK_START_PROGRAM);
            ProgramSignalsStruct ps = new ProgramSignalsStruct(_compiller.GetRamRequired());
            ushort[] values = new ushort[_compiller.GetRamRequired()];
            _currentSignalsStruct.Value = ps;
            _currentSignalsStruct.Values = values;
            _currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            _currentSignalsStruct.LoadStructCycle();
        }
        private void SignalsLoadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(ProgramSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }
        
        private void ProgramStorageWriteFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(StorageWriteFail));
            }
            catch (InvalidOperationException){}
        }
        private void StorageWriteFail()
        {
            OutMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE);
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_ARCHIVE_IS_NOT_SAVE_IN_DEVICE);
            OnStop();
        }

        private void ProgramStartSaveOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(StartSaveOk));
            }
            catch (InvalidOperationException) { }
        }

        private void StartSaveOk()
        {
            OutMessage(InformationMessages.PROGRAM_START_OK);
            _formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
            ushort[] programStorageValue = CompresseProject();
            ushort[] values = new ushort[8192];
            programStorageValue.CopyTo(values, 0);
            ProgramStorageStruct pss = new ProgramStorageStruct();
            pss.InitStruct(Common.TOBYTES(values, false));
            _currentStorageStruct.Value = pss;
            _currentStorageStruct.SaveStruct();
            _formCheck.ShowMessage(InformationMessages.LOADING_ARCHIVE_IN_DEVICE);
        }

        private void ProgramStartSaveFail()
        {
            OutMessage(InformationMessages.ERROR_PROGRAM_START);
            _formCheck.Fail = false;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            OnStop();
        }

        private void ProgramSignalsLoadOk()
        {
            if (_isOnSimulateMode)
            {
                _compiller.DiskretUpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in _filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
                OutMessage(InformationMessages.VARIABLES_UPDATED);
            }
        }
        private void ProgramSignalsLoadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(SignalsLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }
        void SignalsLoadFail()
        {
            //������������� ���������
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (_isOnSimulateMode)
            {
                OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }

        private void ProgramSaveFail()
        {
            OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            _formCheck.Fail = true;
            _formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            OnStop();
        }
        #endregion

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            _filler = new Crownwood.Magic.Controls.TabControl
            {
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = _style,
                IDEPixelBorder = true
            };
            _filler.ClosePressed += new EventHandler(OnFileClose);
            Controls.Add(_filler);
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            _manager = new DockingManager(this, _style) {InnerControl = _filler};
            // Create and setup the StatusBar object
            _statusBar = new StatusBar();
            _statusBar.Dock = DockStyle.Bottom;
            _statusBar.ShowPanels = true;
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel {AutoSize = StatusBarPanelAutoSize.Spring};
            _statusBar.Panels.Add(statusBarPanel);
            Controls.Add(_statusBar);
            _mainToolStrip = CreateToolStrip();
            Controls.Add(_mainToolStrip);
            _topMenu = CreateMenus();
            _manager.OuterControl = _statusBar;
            CreateOutputWindow();
            CreateLibraryWindow();
            this.Width = 800;
            this.Height = 600;
        }       
        private void OutMessage(string str)
        {
            _outputWindow.AddMessage(str + "\r\n");
        }

        protected void DefineContentState(Content c)
        {
            c.CaptionBar = _captionBars;
            c.CloseButton = _closeButtons;
        }

        #region Docking Forms Code
        private void CreateOutputWindow()
        {
            _outputWindow = new OutputWindow();
            _outputContent = _manager.Contents.Add(_outputWindow, "���������");
            DefineContentState(_outputContent);
            _manager.AddContentWithState(_outputContent, State.DockBottom);
        }
        private void CreateLibraryWindow()
        {
            _libraryWindow = new LibraryBox(Resources.BlockLib);
            _libraryContent = _manager.Contents.Add(_libraryWindow, "����������");
            DefineContentState(_libraryContent);
            _manager.AddContentWithState(_libraryContent, State.DockRight);
        }
        #endregion

        protected void OnFileNew(object sender, EventArgs e)
        {
            _newForm.ShowDialog();
            if (DialogResult.OK == _newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(_newForm.NameOfSchema, _newForm.sheetFormat, "MR550", this._manager);
                myTab.Selected = true;
                _filler.TabPages.Add(myTab);
                OutMessage(InformationMessages.CREATED_NEW_SCHEMATIC_SHEET + _newForm.NameOfSchema);
                myTab.Schematic.Focus();
            }
        }
        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            pageSetupDialog.PageSettings = new System.Drawing.Printing.PageSettings();
            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            pageSetupDialog.PrinterSettings = new System.Drawing.Printing.PrinterSettings();
            //Do not show the network in the printer dialog.
            pageSetupDialog.ShowNetwork = false;
            //Show the dialog storing the result.
            DialogResult result = pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                printDocument.DefaultPageSettings = pageSetupDialog.PageSettings;
                printPreviewDialog.Document = printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                printPreviewDialog.ShowDialog();
                DialogResult result1 = printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    printDocument.Print();
                }
            }
        }
        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                float scale = (float)e.PageBounds.Width /(float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        protected void OnUndo(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        protected void OnRedo(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        protected void OnEditCut(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        protected void OnEditCopy(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        protected void OnEditPaste(object sender, EventArgs e)
        {
            if (_filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }
        }

        protected void OnFileOpen(object sender, EventArgs e)
        {
            openFileDialog.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(openFileDialog.FileName, SheetFormat.A0_L, "MR550", this._manager);
                myTab.Selected = true;
                _filler.TabPages.Add(myTab);
                XmlTextReader reader = new XmlTextReader(openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
                OutMessage(InformationMessages.SHEET_SCHEMATIC_LOADED);
                myTab.Schematic.Focus();
            }
        }
        protected void OnFileOpenFromDevice(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            try
            {
                if (DialogResult.Cancel == CloseFileProject())
                {
                    return;
                }
                _currentStorageStruct.LoadStruct();
                _formCheck = new MessageBoxForm();
                _formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGECT);
                _formCheck.ShowDialog(InformationMessages.DOWNLOADING_ARCHIVE_OF_DEVICE);             
            }
            catch
            {
                OutMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
                _formCheck.ShowResultMessage(InformationMessages.ERROR_DOWNLOAD_ARCHIVE_OF_DEVICE);
            }
        }
        protected void OnFileOpenProject(object sender, EventArgs e)
        {
            openFileDialog.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (_filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("��������� ������� ������ �� ����� ?", 
                        "�������� �������", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (SaveProjectDoc())
                            {
                                _filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No: _filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }
                XmlTextReader reader = new XmlTextReader(openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, "MR550", this._manager);
                        myTab.Selected = true;
                        _filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
                OutMessage(InformationMessages.PROJECT_IS_LOADED);
                reader.Close();
                if (_filler.SelectedTab != null) _filler.SelectedTab.Focus();
            }

        }

        private DialogResult CloseFileProject()
        {
            if (_filler.SelectedTab == null) return 0;
            switch (MessageBox.Show("��������� ������ �� ����� ?", "�������� �������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    SaveProjectDoc();                    
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    _filler.TabPages.Clear();                    
                    break;
                case DialogResult.No:
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    _filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    return DialogResult.Cancel;
            }
            return DialogResult.None;
        }
        protected void OnFileClose(object sender, EventArgs e)
        {
            if (_filler.SelectedTab == null) return;
            switch (MessageBox.Show("��������� �������� �� ����� ?", "�������� ���������", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (SaveActiveDoc())
                    {
                        BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                }
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        protected void OnFileCloseProject(object sender, EventArgs e)
        {
            CloseFileProject();
        }
        protected void OnFileSave(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveActiveDoc();
        }
        protected void OnFileSaveProject(object sender, EventArgs e)
        {
            if (this._filler.TabPages.Count == 0)
            {
                MessageBox.Show("������ ����", "��������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            SaveProjectDoc();
        }
        protected void OnViewToolWindow(object sender, EventArgs e)
        {
            _manager.ShowContent(_libraryContent);
        }
        protected void OnViewOutputWindow(object sender, EventArgs e)
        {
            _manager.ShowContent(_outputContent);
        }

        protected void CompileProject()
        {
            _compiller.ResetCompiller();
            foreach (BSBGLTab src in _filler.TabPages)
            {
                _compiller.AddSource(src.TabName, src.Schematic);
                OutMessage(InformationMessages.COMPILING + src.TabName);
            }
            //������ ������ ���������
            binFile = _compiller.Make();
            OutMessage(InformationMessages.PERCENTAGE_OF_FILLING_SCHEME +
                       ((float) (((float) this._compiller.Binarysize*100)/1024)).ToString() + "%.");
            if (this._compiller.Binarysize > 1024)
            {
                MessageBox.Show("��������� ������� ������ ! ", "������ ����������", MessageBoxButtons.OK);
                OnStop();
                return;
            }
            if (binFile.Length == 0)
            {
                OnStop();
                return;
            }
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            SourceProgramStruct ps = new SourceProgramStruct();
            ps.InitStruct(Common.TOBYTES(binFile,false));
            _currentSourceProgramStruct.Value = ps;
            _currentSourceProgramStruct.SaveStruct();
            _isOnSimulateMode = true;
            _formCheck = new MessageBoxForm();
            _formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            _formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
            

        }

        protected void OnCompileUpload(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            CompileProject();
        }

        void OnStop(object sender, EventArgs e)
        {
            _isOnSimulateMode = false;
            _currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        void OnStop()
        {
            _isOnSimulateMode = false;
            _currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in _filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        private bool SaveActiveDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            _saveFile = new SaveFileDialog();
            System.Windows.Forms.DialogResult SaveFileRzult;
            _saveFile.Filter = "bsbgl �����(*.bsbgl)|*.bsbgl|��� ����� (*.*)|*.*";
            SaveFileRzult = _saveFile.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = System.Xml.Formatting.Indented;
                memwriter.WriteStartDocument();
                if (_filler.SelectedTab is BSBGLTab)
                {
                    BSBGLTab _sTab = (BSBGLTab)_filler.SelectedTab;
                    _sTab.Schematic.WriteXml(memwriter);
                }
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(_saveFile.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(_saveFile.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private bool SaveProjectDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            _saveFile = new SaveFileDialog();
            System.Windows.Forms.DialogResult SaveFileRzult;
            _saveFile.Filter = "bprj �����(*.bprj)|*.bprj|��� ����� (*.*)|*.*";
            SaveFileRzult = _saveFile.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in _filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(_saveFile.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(_saveFile.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private ushort[] CompresseProject()
        {
            ZIPCompressor compr = new ZIPCompressor();
            MemoryStream memstream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
            writer.Formatting = System.Xml.Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("BSBGL_ProjectFile");
            foreach (BSBGLTab src in _filler.TabPages)
            {
                writer.WriteStartElement("Source");
                writer.WriteAttributeString("name", src.TabName);
                src.Schematic.WriteXml(writer);
                writer.WriteEndElement();

            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
            byte[] compressed = compr.Compress(memstream.ToArray());
            ushort[] compressedWords = new ushort[(compressed.Length + 1) / 2 + 3]; //������ ���������
            compressedWords[0] = (ushort)compressed.Length; // ������ ������� ������� (�����)
            compressedWords[1] = 0x0001; // ������ ����������
            compressedWords[2] = 0x0000; // �R� ������ �������
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if (((i - 2) * 2 + 1) < compressed.Length)
                {
                    compressedWords[i] = (ushort)(compressed[(i - 3) * 2 + 1] << 8);
                }
                else
                {
                    compressedWords[i] = 0;
                }
                compressedWords[i] += (ushort)(compressed[(i - 3) * 2]);
            }
            return compressedWords;
        }
        private void UncompresseProject(ushort[] compressedWords)
        {
            ZIPCompressor compr = new ZIPCompressor();
            MemoryStream memstream = new MemoryStream();
            byte[] compressed = new byte[compressedWords[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if (((i - 2) * 2 + 1) < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(compressedWords[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)compressedWords[i];
            }
            byte[] uncompressed = compr.Decompress(compressed);
            memstream.Write(uncompressed, 0, uncompressed.Length);
            if (_filler.SelectedTab != null)
            {
                switch (MessageBox.Show("��������� ������� ������ �� ����� ?", "�������� �������", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (SaveProjectDoc())
                        {
                            _filler.TabPages.Clear();
                        }
                        else
                        {
                            return;
                        }
                        break;
                    case DialogResult.No: _filler.TabPages.Clear();
                        break;
                    case DialogResult.Cancel:
                        return;
                }
            }
            memstream.Seek(0, SeekOrigin.Begin);
            XmlTextReader reader = new XmlTextReader(memstream);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            while (reader.Read())
            {
                if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                {
                    BSBGLTab myTab = new BSBGLTab();
                    myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, "MR550", this._manager);
                    myTab.Selected = true;
                    _filler.TabPages.Add(myTab);
                    myTab.Schematic.ReadXml(reader);
                }
            }
            OutMessage(InformationMessages.PROJECT_LOADED_OK_OF_DEVICE);
            reader.Close();
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr550Device); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.programming.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "����������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            _currentSignalsStruct.RemoveStructQueries();
            if (this._filler.TabPages.Count == 0) return;
            switch (MessageBox.Show("��������� ������� ������ �� ����� ?"
                                            , "�������� �������", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    if (SaveProjectDoc())
                    {
                        _filler.TabPages.Clear();
                    }
                    break;
            }
        }
    }
}
