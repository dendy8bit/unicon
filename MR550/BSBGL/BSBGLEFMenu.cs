﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AssemblyResources;
using Crownwood.Magic.Menus;

namespace BEMN.MR550.BSBGL
{
    //Инициализация полосок меню
    partial class BSBGLEF
    {
        private System.Windows.Forms.ToolStrip MainToolStrip;
        private System.Windows.Forms.ToolStripButton newToolStripButton;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripButton openProjToolStripButton;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripButton saveProjToolStripButton;
        private System.Windows.Forms.ToolStripButton printToolStripButton;

        private System.Windows.Forms.ToolStripButton undoToolStripButton;
        private System.Windows.Forms.ToolStripButton redoToolStripButton;

        private System.Windows.Forms.ToolStripButton cutToolStripButton;
        private System.Windows.Forms.ToolStripButton copyToolStripButton;
        private System.Windows.Forms.ToolStripButton pasteToolStripButton;

        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolStripButton CompilToDeviceToolStripButton;
        private System.Windows.Forms.ToolStripButton StopToolStripButton;
        private System.Windows.Forms.ToolStripButton openFromDeviceToolStripButton;


        #region Toolbox

        protected ToolStrip CreateToolStrip()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(BSBGLEF));
            List<System.Windows.Forms.ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 6; i++)
            {
                System.Windows.Forms.ToolStripSeparator tls = new System.Windows.Forms.ToolStripSeparator();
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator" + i.ToString();
                tls.Size = new System.Drawing.Size(6, 25);
            }

            this.MainToolStrip = new System.Windows.Forms.ToolStrip();
            this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.openFromDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.saveProjToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.printToolStripButton = new System.Windows.Forms.ToolStripButton();

            this.undoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.redoToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.cutToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.copyToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.pasteToolStripButton = new System.Windows.Forms.ToolStripButton();
            
            this.CompilToDeviceToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.StopToolStripButton = new System.Windows.Forms.ToolStripButton();

            this.MainToolStrip.Items.Add(this.newToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[0]);
            this.MainToolStrip.Items.Add(this.openToolStripButton);
            this.MainToolStrip.Items.Add(this.openProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[1]);
            this.MainToolStrip.Items.Add(this.saveToolStripButton);
            this.MainToolStrip.Items.Add(this.saveProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[2]);
            this.MainToolStrip.Items.Add(this.printToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[3]);
            this.MainToolStrip.Items.Add(this.undoToolStripButton);
            this.MainToolStrip.Items.Add(this.redoToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[4]);
            this.MainToolStrip.Items.Add(this.cutToolStripButton);
            this.MainToolStrip.Items.Add(this.copyToolStripButton);
            this.MainToolStrip.Items.Add(this.pasteToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[5]);
            this.MainToolStrip.Items.Add(this.CompilToDeviceToolStripButton);
            this.MainToolStrip.Items.Add(this.openFromDeviceToolStripButton);
            this.MainToolStrip.Items.Add(new ToolStripSeparator{Margin = new Padding(20,0,0,0)});
            this.MainToolStrip.Items.Add(this.StopToolStripButton);

            this.MainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new System.Drawing.Size(816, 25);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "toolStrip1";

            this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("filenew")));
            this.newToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.newToolStripButton.Text = "Новая схема";
            this.newToolStripButton.Click += new System.EventHandler(this.OnFileNew);

            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("fileopen")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Открыть документ";
            this.openToolStripButton.Click += new System.EventHandler(this.OnFileOpen);

            this.openProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("fileopenproject")));
            this.openProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openProjToolStripButton.Text = "Открыть проект";
            this.openProjToolStripButton.Click += new System.EventHandler(this.OnFileOpenProject);

            this.openFromDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openFromDeviceToolStripButton.Image = Resources.openfromdevicebmp;
            this.openFromDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openFromDeviceToolStripButton.Name = "openFromDeviceToolStripButton";
            this.openFromDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openFromDeviceToolStripButton.Text = "Загрузить архив СПЛ из устройства";
            this.openFromDeviceToolStripButton.Click += new System.EventHandler(this.OnFileOpenFromDevice);

            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("filesave")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Сохранить документ";
            this.saveToolStripButton.Click += new System.EventHandler(this.OnFileSave);

            this.saveProjToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("filesaveproject")));
            this.saveProjToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveProjToolStripButton.Text = "Сохранить проект";
            this.saveProjToolStripButton.Click += new System.EventHandler(this.OnFileSaveProject);

            this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("print")));
            this.printToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.printToolStripButton.Text = "Печать";
            this.printToolStripButton.Click += new EventHandler(printToolStripButton_Click);
            
            this.undoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("undo")));
            this.undoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.undoToolStripButton.Text = "Отмена";
            this.undoToolStripButton.Click += new EventHandler(OnUndo);

            this.redoToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("redo")));
            this.redoToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.redoToolStripButton.Text = "Восстановить отмененое";
            this.redoToolStripButton.Click += new EventHandler(OnRedo);

            this.cutToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("cut")));
            this.cutToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.cutToolStripButton.Text = "Вырезать";
            this.cutToolStripButton.Click += new EventHandler(OnEditCut);

            this.copyToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("copy")));
            this.copyToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.copyToolStripButton.Text = "Копировать";
            this.copyToolStripButton.Click += new EventHandler(OnEditCopy);

            this.pasteToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("paste")));
            this.pasteToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.pasteToolStripButton.Text = "Вставить";
            this.pasteToolStripButton.Click += new EventHandler(OnEditPaste);

            this.CompilToDeviceToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.CompilToDeviceToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("go")));
            this.CompilToDeviceToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CompilToDeviceToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.CompilToDeviceToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.CompilToDeviceToolStripButton.Text = "Загрузить программу и архив СПЛ в устройство, запустить эмулятор";
            this.CompilToDeviceToolStripButton.Click += new System.EventHandler(this.OnCompileUpload);

            this.StopToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("stop")));
            this.StopToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.StopToolStripButton.Text = "Остановка эмуляции";
            this.StopToolStripButton.Click += new EventHandler(OnStop);
            
            return MainToolStrip;
        }
        
        protected MenuControl CreateMenus()
        {
            MenuControl topMenu = new MenuControl
            {
                Style = _style,
                MultiLine = false
            };
            MenuCommand topFile = new MenuCommand("&Файл");
            MenuCommand topEdit = new MenuCommand("Правка");
            MenuCommand topView = new MenuCommand("Вид");
            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView/*, topSettings,*/ });
            //File
            MenuCommand topFileNew = new MenuCommand("Новый", new EventHandler(OnFileNew));
            MenuCommand topFileOpen = new MenuCommand("Открыть документ", new EventHandler(OnFileOpen));
            MenuCommand topFileOpenProject = new MenuCommand("Открыть проект", new EventHandler(OnFileOpenProject));
            MenuCommand topFileClose = new MenuCommand("Закрыть документ", new EventHandler(OnFileClose));
            MenuCommand topFileCloseProject = new MenuCommand("Закрыть проект", new EventHandler(OnFileCloseProject));
            MenuCommand topFileSave = new MenuCommand("Сохранить документ", new EventHandler(OnFileSave));
            MenuCommand topFileSaveProject = new MenuCommand("Сохранить проект", new EventHandler(OnFileSaveProject));
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("Копировать", new EventHandler(OnEditCopy));
            MenuCommand topEditPaste = new MenuCommand("Вставить", new EventHandler(OnEditPaste));

            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewOutputWindow = new MenuCommand("Сообщения", new EventHandler(OnViewOutputWindow));
            MenuCommand topViewToolWindow = new MenuCommand("Библиотека", new EventHandler(OnViewToolWindow));
            topView.MenuCommands.AddRange(new MenuCommand[] { topViewOutputWindow,topViewToolWindow });
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
            return topMenu;
        }
        #endregion
    }
}
