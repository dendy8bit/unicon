﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.CommonStructures.Switch
{
   public class SwitchStruct:StructBase
   {
      [Layout(0)]  private ushort _off;
       [Layout(1)]  private ushort _on;
       [Layout(2)]  private ushort _fault;
       [Layout(3)]  private ushort _block;
       [Layout(4)]  private ushort _urovTime;
       [Layout(5)]  private ushort _urovUstavka;
       [Layout(6)]  private ushort _timeSignal;
       [Layout(7)]  private ushort _timeOn;
       [Layout(8)]  private ushort _res;
       [Layout(9)]  private ushort _manageSignals;



       [BindingProperty(0)]
       [XmlElement(ElementName = "Отключено")]
       public string Off
       {
           get { return Validator.Get(_off, StringsConfig.ExternalSignals); }
           set { _off = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(1)]
       [XmlElement(ElementName = "Включено")]
       public string On
       {
           get { return Validator.Get(_on, StringsConfig.ExternalSignals); }
           set { _on = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(2)]
       [XmlElement(ElementName = "Ошибка")]
       public string Fault
       {
           get { return Validator.Get(_fault, StringsConfig.ExternalSignals); }
           set { _fault = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(3)]
       [XmlElement(ElementName = "Блокировка")]
       public string Block
       {
           get { return Validator.Get(_block, StringsConfig.ExternalSignals); }
           set { _block = Validator.Set(value, StringsConfig.ExternalSignals); }
       }

       [BindingProperty(4)]
       [XmlElement(ElementName = "tуров")]
       public int UrovTime
       {
           get { return ValuesConverterCommon.GetWaitTime(_urovTime) ; }
           set { _urovTime = ValuesConverterCommon.SetWaitTime(value); }
       }

       [BindingProperty(5)]
       [XmlElement(ElementName = "ток_УРОВ")]
       public double UrovUstavka
       {
           get
           {
               return ValuesConverterCommon.GetIn(_urovUstavka);
           }
           set { _urovUstavka = ValuesConverterCommon.SetIn(value); }
       }

       [BindingProperty(6)]
       [XmlElement(ElementName = "Импульс")]
       public int TimeSignal
       {
           get { return ValuesConverterCommon.GetWaitTime(_timeSignal); }
           set { _timeSignal = ValuesConverterCommon.SetWaitTime(value); }
       }

       [BindingProperty(7)]
       [XmlElement(ElementName = "Ускорение")]
       public int TimeOn
       {
           get { return ValuesConverterCommon.GetWaitTime(_timeOn); }
           set { _timeOn = ValuesConverterCommon.SetWaitTime(value); }
       }




       [BindingProperty(8)]
       [XmlElement(ElementName = "Меню")]
       public string FromButton
       {
           get { return Validator.Get(_manageSignals, StringsConfig.Zr, 0); }
           set { _manageSignals = Validator.Set(value, StringsConfig.Zr, _manageSignals, 0); }
       }

       [BindingProperty(9)]
       [XmlElement(ElementName = "Ключ")]
       public string FromKey
       {
           get { return Validator.Get(_manageSignals, StringsConfig.Cr,1); }
           set { _manageSignals = Validator.Set(value, StringsConfig.Cr, _manageSignals, 1); }
       }

       [BindingProperty(10)]
       [XmlElement(ElementName = "Внешнее")]
       public string External
       {
           get { return Validator.Get(_manageSignals, StringsConfig.Cr,2); }
           set { _manageSignals = Validator.Set(value, StringsConfig.Cr, _manageSignals, 2); }
       }

       [BindingProperty(11)]
       [XmlElement(ElementName = "СДТУ")]
       public string Sdtu
       {
           get { return Validator.Get(_manageSignals, StringsConfig.Zr,3); }
           set { _manageSignals = Validator.Set(value, StringsConfig.Zr, _manageSignals, 3); }
       }

   }
}
