﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.CommonStructures.Ls
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        public  const int DISCRETS_COUNT =8;
        #region [Private fields]
        [Layout(0)]
        ushort _a1;
        [Layout(1)]
        ushort _a2;

        #endregion [Private fields]
           [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                    {
                        this._a1,
                        this._a2,
                       
                    };
            }

            set
            {
                this._a1 = value[0];
                this._a2 = value[1];
               
            }
        }
     

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                if (Common.GetBit(this._a1,ls))
                {
                    if (Common.GetBit(this._a1,ls+8))
                    {
                        return StringsConfig.LsState[2];
                    }
                    else
                    {
                        return StringsConfig.LsState[1];
                    }
                }
                return StringsConfig.LsState[0];

            }

            set
            {
                if (value == StringsConfig.LsState[0])
                {
                    this._a1 = Common.SetBit(this._a1, ls, false);
                    this._a1 = Common.SetBit(this._a1, ls+8, false);
                }
                if (value == StringsConfig.LsState[1])
                {
                    this._a1 = Common.SetBit(this._a1, ls, true);
                    this._a1 = Common.SetBit(this._a1, ls + 8, false);
                }
                if (value == StringsConfig.LsState[2])
                {
                    this._a1 = Common.SetBit(this._a1, ls, true);
                    this._a1 = Common.SetBit(this._a1, ls + 8, true);
                }
            }
        }

        [XmlIgnore]
      //  [XmlArray(ElementName = "ЛС")]
           public string[] LogicSygnalXml
           {
               get
               {
                   var result = new string[8];

                   var sourse = this.Mass;
                   var enumerator = sourse.GetEnumerator();

                   int j = 0;
                   while (j < 8)
                   {
                       enumerator.MoveNext();
                       var temp = (ushort)(enumerator.Current);

                       for (int i = 0; i < 16; i += 2)
                       {
                           var number = Common.GetBits(temp, i, i + 1) >> i;
                           result[j] = StringsConfig.LsState[number];
                           j++;
                       }
                   }
                   return result;
               }

               set
               {
                   int j = 0;
                   var result = new List<ushort>();
                   while (j < 8)
                   {
                       ushort newValue = 0;

                       for (int i = 0; i < 16; i += 2)
                       {
                           var bits = (ushort)StringsConfig.LsState.IndexOf(value[j]);
                           newValue = Common.SetBits(newValue, bits, i, i + 1);
                           j++;
                       }
                       result.Add(newValue);
                   }
                   this.Mass = result.ToArray();
               }
           }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
       
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
           
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
