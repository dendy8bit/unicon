using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.Dgw;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR550.CommonStructures.Apv;
using BEMN.MR550.CommonStructures.Avr;
using BEMN.MR550.CommonStructures.FaultSignal;
using BEMN.MR550.CommonStructures.Indicators;
using BEMN.MR550.CommonStructures.Ls;
using BEMN.MR550.CommonStructures.Lzsh;
using BEMN.MR550.CommonStructures.Switch;
using BEMN.MR550.CommonStructures.Vls;
using BEMN.MR550.Version1.Configuration.Structures;
using BEMN.MR550.Version1.Configuration.Structures.CurrentDefenses;
using BEMN.MR550.Version1.Configuration.Structures.ExternalDefenses;
using BEMN.MR550.Version1.Configuration.Structures.ExternalSignals;
using BEMN.MR550.Version1.Configuration.Structures.Keys;
using BEMN.MR550.Version1.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version1.Configuration.Structures.SystenConfig;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version1.Configuration
{
    
    public partial class Mr550ConfigurationFormV1 : Form, IFormView
    {
        #region [Private fields]
        private readonly Mr550Device _device;
        private readonly MemoryEntity<ConfigurationStructV1> _configuration;
        /// <summary>
        /// ������ ��������� ��� ��
        /// </summary>
        private DataGridView[] _lsBoxes;

        private ConfigurationStructV1 _currentSetpointsStructV1;

        #region [Validators]
        private NewStructValidator<MeasureTransStruct> _measureTransValidator;
        private NewCheckedListBoxValidator<KeysStruct> _keysValidator;
        private NewStructValidator<ExternalSignalStruct> _externalSignalsValidator;
        private NewStructValidator<FaultStruct> _faultValidator;

        private NewDgwValidatior<InputLogicStruct>[] _inputLogicValidator;
        private StructUnion<InputLogicSignalStruct> _inputLogicUnion;

        private NewStructValidator<SwitchStruct> _switchValidator;
        private NewStructValidator<ApvStruct> _apvValidator;
        private NewStructValidator<AvrStruct> _avrValidator;
        private NewStructValidator<LpbStruct> _lpbValidator;
        private NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct> _externalDefenseValidatior;
        private NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct> _currentDefenseValidatior;
        private NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct> _addCurrentDefenseValidator;
        private SetpointsValidator< AllSetpointsStruct, SetpointStruct> _setpointsValidator;
        private StructUnion<SetpointStruct> _setpointUnion;

        #region [���]
        /// <summary>
        /// ������ ��������� ��� ���
        /// </summary>
        private CheckedListBox[] _vlsBoxes;
        private NewCheckedListBoxValidator<OutputLogicStruct>[] _vlsValidator;
        private StructUnion<OutputLogicSignalStruct> _vlsUnion;
        #endregion [���]
        private NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> _indicatorValidator;
        private NewStructValidator<SystemConfigStruct> _oscValidator;
        private StructUnion<ConfigurationStructV1> _configurationUnion;
        #endregion [Validators] 
        #endregion [Private fields]


        #region [Ctor's]
        public Mr550ConfigurationFormV1()
        {
            InitializeComponent();

        }

        public Mr550ConfigurationFormV1(Mr550Device device)
        {
            InitializeComponent();
            _device = device;
            this._configuration = _device.ConfigurationV1;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadConfigOk);
            this._configuration.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = ERROR_READ_CONFIG;
                    MessageBox.Show(ERROR_READ_CONFIG);

                });
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, () => _exchangeProgressBar.PerformStep());
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, () => _exchangeProgressBar.PerformStep());
            this._configuration.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this.IsProcess = false;
                this._statusLabel.Text = ERROR_WRITE_CONFIG;
                MessageBox.Show(ERROR_WRITE_CONFIG);

            });
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._device.WriteConfiguration();
                this.IsProcess = false;
                this._statusLabel.Text = "������������ ������� ��������";

            });
            _exchangeProgressBar.Maximum = this._configuration.Slots.Count;
            _currentSetpointsStructV1 = new ConfigurationStructV1();
            Init();
            _configurationUnion.Set(this._currentSetpointsStructV1);
        } 
        
        private void Init()
        {
            _lsBoxes = new[]
                {
                    _inputSignals1,
                    _inputSignals2,
                    _inputSignals3,
                    _inputSignals4,
                    _inputSignals9,
                    _inputSignals10,
                    _inputSignals11,
                    _inputSignals12

                };

            _measureTransValidator = new NewStructValidator<MeasureTransStruct>
                (
                this._toolTip,
                new ControlInfoText(_TT_Box, RulesContainer.UshortTo1500),
                new ControlInfoText(_TTNP_Box, RulesContainer.UshortTo100),
                new ControlInfoText(_maxTok_Box, RulesContainer.Ustavka40)
                );
            _externalSignalsValidator = new NewStructValidator<ExternalSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(_keyOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_keyOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_extOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_extOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_indicationCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_constraintGroupCombo1, StringsConfig.ExternalSignals)
              
                );

            _faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoValidator(new NewCheckedListBoxValidator<FaultSignalStruct>(_dispepairCheckList)),
                new ControlInfoText(_dispepairReleDurationBox, RulesContainer.IntTo3M)
                );

            _inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
              (
              this._lsBoxes[i],
       InputLogicStruct.DISCRETS_COUNT,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            _inputLogicUnion = new StructUnion<InputLogicSignalStruct>(_inputLogicValidator);

            _switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                new ControlInfoCombo(_switcherStateOffCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_switcherStateOnCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_switcherErrorCombo, StringsConfig.ExternalSignals),
                new ControlInfoCombo(_switcherBlockCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(_switcherUrovTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_switcherUrovBox, RulesContainer.Ustavka40),
                new ControlInfoText(_switcherImpulseBox, RulesContainer.IntTo3M),
                new ControlInfoText(_switcherAccelerationBox, RulesContainer.IntTo3M),

                new ControlInfoCombo(_manageSignalsButtonCombo, StringsConfig.Zr),
                new ControlInfoCombo(_manageSignalsKeyCombo, StringsConfig.Cr),
                new ControlInfoCombo(_manageSignalsExternalCombo, StringsConfig.Cr),
                new ControlInfoCombo(_manageSignalsSDTU_Combo, StringsConfig.Zr)
                );

            _apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                new ControlInfoCombo(_APV_ConfigCombo, StringsConfig.ApvModes),
                new ControlInfoCombo(_APV_BlockingCombo, StringsConfig.ExternalSignals),
                new ControlInfoText(_APV_BlockingTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_APV_ReadyTimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_APV_Crat1TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_APV_Crat2TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_APV_Crat3TimeBox, RulesContainer.IntTo3M),
                new ControlInfoText(_APV_Crat4TimeBox, RulesContainer.IntTo3M),
                new ControlInfoCheck(_apvStartCheckBox)

                );
            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._avrBySignalCheckBox),
                new ControlInfoCheck(this._avrByOffCheckBox),
                new ControlInfoCheck(this._avrBySelfOffCheckBox),
                new ControlInfoCheck(this._avrByDiffCheckBox),
                new ControlInfoCombo(this._avrSIGNOn, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlocking, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBlockClear, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrResolve, StringsConfig.ExternalSignals),
                new ControlInfoCombo(this._avrBack, StringsConfig.ExternalSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTBack, RulesContainer.IntTo3M),
                new ControlInfoText(this._avrTOff, RulesContainer.IntTo3M),
                new ControlInfoCheck(this._avrClearCheckBox)
                );
            this._lpbValidator = new NewStructValidator<LpbStruct>
                  (
                  this._toolTip,
                  new ControlInfoCombo(this._lzhModes, StringsConfig.BeNo),
                  new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40)
                  );

            this._externalDefenseValidatior = new NewDgwValidatior<AllExternalDefensesStruct, ExternalDefenseStruct>
                (
                _externalDefenseGrid,
                8,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ExtModesV1),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),

                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.ExternalDefenceSignals),
                new ColumnInfoText(RulesContainer.IntTo3M),

                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff =new[]{new TurnOffDgv
                        (
                        this._externalDefenseGrid,
                        new TurnOffRule(1, StringsConfig.ExtModesV1[0],true,2,3,4,5,6,7,8,9,10,11), 
                        new TurnOffRule(5, false,false, 6, 7, 8)
                        )} 
                };
            var currentDefenseFunc = new Func<IValidatingRule>(() =>
                {
                    try
                    {
                        if (_tokDefenseGrid1[5, _tokDefenseGrid1.CurrentCell.RowIndex].Value.ToString() ==
                            StringsConfig.FeatureLight[1])
                        {
                            return new CustomUshortRule(0, 4000); 
                        }
                        return RulesContainer.IntTo3M;
                    }
                    catch (Exception)
                    {
                        return RulesContainer.IntTo3M;
                    }
                });


            this._currentDefenseValidatior = new NewDgwValidatior<AllCurrentDefensesStruct, CurrentDefenseStruct>
                (
                new[] {_tokDefenseGrid1, _tokDefenseGrid2, _tokDefenseGrid3},
                new[] {4, 4, 2},
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.CurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),
                new ColumnInfoCombo(StringsConfig.TokParameter, ColumnsType.COMBO, true, false,false),
                new ColumnInfoText(RulesContainer.Ustavka40,true,true,false),
                new ColumnInfoText(RulesContainer.Ustavka5,false,true,true),
                new ColumnInfoCombo(StringsConfig.FeatureLight, ColumnsType.COMBO, true, false,false),
                new ColumnInfoTextDependent(currentDefenseFunc),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(this._tokDefenseGrid1,
                                           new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9,
                                                           10, 11, 12), new TurnOffRule(8, false, false, 9)),
                            new TurnOffDgv(this._tokDefenseGrid2,
                                           new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9,
                                                           10, 11, 12), new TurnOffRule(8, false, false, 9)),
                            new TurnOffDgv(this._tokDefenseGrid3,
                                           new TurnOffRule(1, StringsConfig.CurModesV1[0], true, 2, 3, 4, 5, 6, 7, 8, 9,
                                                           10, 11, 12), new TurnOffRule(8, false, false, 9))
                        }
                };

            _addCurrentDefenseValidator = new NewDgwValidatior<AllAddCurrentDefensesStruct, AddCurrentDefenseStruct>
                (
                _tokDefenseGrid4,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.AddCurrentDefensesNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.CurModesV1),
                new ColumnInfoCombo(StringsConfig.ExternalSignals),

                new ColumnInfoText(RulesContainer.Ustavka100),

                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoText(RulesContainer.IntTo3M),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                )
                {
                    TurnOff = new[]
                        {
                            new TurnOffDgv(_tokDefenseGrid4,new TurnOffRule(1, StringsConfig.CurModesV1[0],true, 2, 3, 4, 5, 6, 7, 8, 9), new TurnOffRule(5, false, false,6))
                        },
                    AddRules = new[] {new TextboxCellRule(0, 3, RulesContainer.Ustavka5)}
                };
     

            _setpointUnion = new StructUnion<SetpointStruct>
                (
                this._currentDefenseValidatior,
                _addCurrentDefenseValidator
                );

            this._setpointsValidator = new SetpointsValidator<AllSetpointsStruct,SetpointStruct>
                (
                new ComboboxSelector(this._setpointsComboBox,StringsConfig.SetpointsNamesV1),
             _setpointUnion
                );
            
            #region [���] 
            this._vlsBoxes = new[]
                {
                    VLScheckedListBox1,
                    VLScheckedListBox2,
                    VLScheckedListBox3,
                    VLScheckedListBox4,
                    VLScheckedListBox5,
                    VLScheckedListBox6,
                    VLScheckedListBox7,
                    VLScheckedListBox8
                };
            _vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i], StringsConfig.VlsSignalsV1);
            }
            _vlsUnion = new StructUnion<OutputLogicSignalStruct>(_vlsValidator);
            #endregion [���]

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                _outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndicatorNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.IndicatorSignalsV1),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCheck()
                );

            _oscValidator = new NewStructValidator<SystemConfigStruct>
                (
                this._toolTip,
                 new ControlInfoCombo(_oscLength, StringsConfig.OscSizeV1)
                );

            _keysValidator = new NewCheckedListBoxValidator<KeysStruct>(_keysCheckedListBox,StringsConfig.KeyNumbers);
            
            _configurationUnion = new StructUnion<ConfigurationStructV1>
                (
                _measureTransValidator,
                _externalSignalsValidator,
                _faultValidator,
                _inputLogicUnion,
                _switchValidator,
                _apvValidator,
                _avrValidator,
                _lpbValidator,
                _externalDefenseValidatior,
                _setpointsValidator,
                _vlsUnion,
                _indicatorValidator,
                _oscValidator,
                _keysValidator
                );
        }

        #endregion [Ctor's]

        private void ReadConfigOk()
        {
            this.IsProcess = false;
            this._statusLabel.Text = CONFIG_READ_OK;
            _configurationUnion.Set(this._configuration.Value);
        }
      
        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartReadConfiguration()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            if (!this._device.MB.IsDisconnect)
            {
                this._device.ReadConfiguration();
                this.IsProcess = true;
                this._configuration.LoadStruct();
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }

        /// <summary>
        /// ������ ������
        /// </summary>
        private void StartWriteConfiguration()
        {
            if (MessageBox.Show("�������� ������������ �� 550?","������ ������������", MessageBoxButtons.YesNo)== DialogResult.Yes)
            {
                _exchangeProgressBar.Value = 0;
                string message;
                if (this._configurationUnion.Check(out message,true))
                {
                    this._configuration.Value = this._configurationUnion.Get();
                    this._statusLabel.Text = "��� ������ ������������";
                    this.IsProcess = true;
                    this._configuration.SaveStruct();
                }
                else
                {
                    MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                }
            }
        }

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            ReadFromFile();
        }

        private void ReadFromFile()
        {
            _exchangeProgressBar.Value = 0;
            if (DialogResult.OK == _openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(_openConfigurationDlg.FileName, MR550);
            }
        }


        private const string FILE_LOAD_FAIL = "���������� ��������� ����";
        /// <summary>
        /// �������� ������������ �� �����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Deserialize(string binFileName,string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);

                var a = doc.FirstChild.SelectSingleNode(string.Format("{0}_SET_POINTS", head));
                if (a == null)
                    throw new NullReferenceException();

                var values = Convert.FromBase64String(a.InnerText);
                var configurationStruct = new ConfigurationStructV1();
                configurationStruct.InitStruct(values);
                _configurationUnion.Set(configurationStruct);
               
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            SaveinFile();
        }

        private void SaveinFile()
        {
            string message;
            if (this._configurationUnion.Check(out message, true))
            {
                var currentStruct = this._configurationUnion.Get();
                _saveConfigurationDlg.FileName = string.Format("��550_�������_������ {0:F1}.xml", this._device.DeviceVersion);
                if (_saveConfigurationDlg.ShowDialog() == DialogResult.OK)
                {
                    this.Serialize(_saveConfigurationDlg.FileName, currentStruct, MR550);
                }

            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ���������.");
            }  
        }


        private const string FILE_SAVE_FAIL = "���������� ��������� ����";
        private const string MR550 = "MR550";
        private const string INVALID_PORT = "���� ����������.";
        private const string ERROR_READ_CONFIG = "������ ������ ������������";
        private const string ERROR_WRITE_CONFIG = "������ ������ ������������";
        private const string CONFIG_READ_OK = "������������ ���������";

        private bool IsProcess
        {
            set
            {
                _resetConfigBut.Enabled = !value;
                _readConfigBut.Enabled = !value;
                _writeConfigBut.Enabled = !value;
                _resetConfigBut.Enabled = !value;
                _loadConfigBut.Enabled = !value;
                _saveConfigBut.Enabled = !value;
                _saveToXmlButton.Enabled = !value;
            }
        }


        /// <summary>
        /// ���������� ������������ � ����
        /// </summary>
        /// <param name="binFileName">��� �����</param>
        public void Serialize(string binFileName,StructBase config, string head)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement(head));


                var values = config.GetValues();

                XmlElement element = doc.CreateElement(string.Format("{0}_SET_POINTS",head));
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);

                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("���� {0} ������� ��������", binFileName);
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
            }
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (!this._device.MB.IsDisconnect)
            {
                StartWriteConfiguration();
            }
            else
            {
                MessageBox.Show(INVALID_PORT);
            }
        }
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            StartReadConfiguration();
        }
        
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig)
                StartReadConfiguration();
        }
        


        private void _resetConfigBut_Click(object sender, EventArgs e)
        {
            this._configurationUnion.Reset();
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtmlFile();
        }

        private void SaveToHtmlFile()
        {
            if (WriteConfiguration())
            {
                this._currentSetpointsStructV1.DeviceVersion = this._device.DeviceVersion;
                this._currentSetpointsStructV1.DeviceNumber = this._device.DeviceNumber.ToString();
                _statusLabel.Text = HtmlExport.Export(Resources.MR550Main, Resources.MR550Res,
                                                      _currentSetpointsStructV1, _currentSetpointsStructV1.DeviceType, _currentSetpointsStructV1.DeviceVersion);
            }
        }

        /// <summary>
        /// ������ ��� ������ � ������
        /// </summary>
        private bool WriteConfiguration()
        {
            string message;

            if (this._configurationUnion.Check(out message, true))
            {
                this._currentSetpointsStructV1 = this._configurationUnion.Get();
                return true;
            }
            else
            {
                MessageBox.Show("���������� ������������ �������. ������������ �� ����� ���� ��������.");
                return false;
            }
        }

        private void _groupChangeButton_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("�� ������� ��� ������ ���������� �������?", _groupChangeButton.Text, MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
            {
               
                this.WriteConfiguration();
                if (_setpointsComboBox.SelectedIndex == 0)
                {
                    this._currentSetpointsStructV1.AllSetpoints.Setpoints = new SetpointStruct[]
                    {
                        this._currentSetpointsStructV1.AllSetpoints.Main,
                        this._currentSetpointsStructV1.AllSetpoints.Main.Clone<SetpointStruct>()
                    };

                }
                else
                {
                    this._currentSetpointsStructV1.AllSetpoints.Setpoints = new SetpointStruct[]
                    {
                        this._currentSetpointsStructV1.AllSetpoints.Reserv,
                        this._currentSetpointsStructV1.AllSetpoints.Reserv.Clone<SetpointStruct>()
                    };
                }
            }
        }

        private void _setpointsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._groupChangeButton.Text = this._setpointsComboBox.SelectedIndex == 0 ? "�������� --> ���������" : "��������� --> ��������";
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr550Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr550ConfigurationFormV1); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartReadConfiguration();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                this._configurationUnion.Reset();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }

            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtmlFile();
                return;
            }
        }

        private void Mr550ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    WriteConfig();
                    break;
                case Keys.R:
                    StartReadConfiguration();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
