﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version1.Configuration.Structures.ExternalSignals
{
    public class ExternalSignalStruct : StructBase
    {  
        [Layout(0)] private ushort _keyOff;
        [Layout(1)] private ushort _keyOn;
        [Layout(2)] private ushort _extOff;
        [Layout(3)] private ushort _extOn;
        [Layout(4)] private ushort _inpClear;
        [Layout(5)] private ushort _inpGroup;
        [Layout(6)] private ushort _inpOsc;
        [Layout(7, Count = 6)] private ushort[] _res;

         [BindingProperty(0)]
        [XmlElement(ElementName = "ключ_ВЫКЛ")]
        public string KeyOff
        {
            get { return Validator.Get(_keyOff, StringsConfig.ExternalSignals); }
            set { _keyOff = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
         [BindingProperty(1)]
         [XmlElement(ElementName = "ключ_ВКЛ")]
         public string KeyOn
        {
            get { return Validator.Get(_keyOn, StringsConfig.ExternalSignals); }
            set { _keyOn = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
         [BindingProperty(2)]
         [XmlElement(ElementName = "вход_внеш_выключить")]
         public string ExtOff
        {
            get { return Validator.Get(_extOff, StringsConfig.ExternalSignals); }
            set { _extOff = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
         [BindingProperty(3)]
         [XmlElement(ElementName = "вход_внеш_включить")]
         public string ExtOn
        {
            get { return Validator.Get(_extOn, StringsConfig.ExternalSignals); }
            set { _extOn = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
         [BindingProperty(4)]
         [XmlElement(ElementName = "сброс_сигнализации")]
         public string InpClear
        {
            get { return Validator.Get(_inpClear, StringsConfig.ExternalSignals); }
            set { _inpClear = Validator.Set(value, StringsConfig.ExternalSignals); }
        }
         [BindingProperty(5)]
         [XmlElement(ElementName = "группа_уставок")]
         public string InpGroup
         {
             get { return Validator.Get(_inpGroup, StringsConfig.ExternalSignals); }
             set { _inpGroup = Validator.Set(value, StringsConfig.ExternalSignals); }
         }
    }
}
