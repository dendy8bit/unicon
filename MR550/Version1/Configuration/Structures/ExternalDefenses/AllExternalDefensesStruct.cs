﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR550.Version1.Configuration.Structures.ExternalDefenses
{
    public class AllExternalDefensesStruct : StructBase, IDgvRowsContainer<ExternalDefenseStruct>
    {
        [Layout(1, Count = 8)]
        private ExternalDefenseStruct[] _externalDefenses;
        [XmlArray(ElementName = "Все")]
        public ExternalDefenseStruct[] Rows
        {
            get { return _externalDefenses; }
            set { _externalDefenses = value; }
        }
    }
}
