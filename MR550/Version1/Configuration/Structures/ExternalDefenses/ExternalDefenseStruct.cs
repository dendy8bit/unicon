﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version1.Configuration.Structures.ExternalDefenses
{
    public class ExternalDefenseStruct : StructBase
    {
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
  [Layout(2)] private ushort _srab;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)

        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _u; //уставка возврата
      

        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(_config, StringsConfig.ExtModesV1, 0, 1,2,3); }
            set { _config = Validator.Set(value, StringsConfig.ExtModesV1, _config, 0, 1,2,3); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalDefenceSignals); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Вход_срабатывания")]
        public string Srab
        {
            get { return Validator.Get(this._srab, StringsConfig.ExternalDefenceSignals); }
            set { this._srab = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ust); }
            set { this._ust = ValuesConverterCommon.SetWaitTime(value); }
        }


        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Возврат")]
        public bool Return
        {
            get { return Common.GetBit(this._config, 14); }
            set { this._config = Common.SetBit(this._config, 14, value); }
        }

      
        [BindingProperty(5)]
        [XmlElement(ElementName = "АПВ_ВЗ")]
        public bool ApvReturn
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }


        [BindingProperty(6)]
        [XmlElement(ElementName = "Вход_ВЗ")]
        public string InputReturn
        {
            get { return Validator.Get(this._time, StringsConfig.ExternalDefenceSignals); }
            set { this._time = Validator.Set(value, StringsConfig.ExternalDefenceSignals); }
        }


        [BindingProperty(7)]
        [XmlElement(ElementName = "Уставка_ВЗ")]
        public int UstavkaReturn
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }


        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }



        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }

        
    }
}
