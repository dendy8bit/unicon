﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version1.Configuration.Structures.SystenConfig
{
    public class SystemConfigStruct : StructBase
    {
       

        [Layout(0)] private ushort _address ;
        [Layout(1)] private ushort _speed ;
        [Layout(2)] private ushort _delay;
        [Layout(3)] private ushort _status;
        [Layout(4)] private ushort _oscConf;

        [XmlIgnore]
        public ushort Address
        {
            get { return _address; }
            set { _address = value; }
        }
        [XmlIgnore]
        public ushort Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

  

        /// <summary>
        /// количество_осциллограм
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Количество_осциллограм")]
        public string SizeXml
        {
            get { return Validator.Get(this._oscConf, StringsConfig.OscSizeV1, 0, 1, 2); }
            set { this._oscConf = Validator.Set(value, StringsConfig.OscSizeV1, this._oscConf, 0, 1, 2); }
        }
    }
}
