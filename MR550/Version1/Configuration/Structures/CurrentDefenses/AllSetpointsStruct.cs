﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.MR550.Version1.Configuration.Structures.CurrentDefenses
{
    public class AllSetpointsStruct : StructBase, ISetpointContainer<SetpointStruct>
    {
        private const int DEF_COUNT = 2;

        [Layout(0, Count = DEF_COUNT)] private AllCurrentDefensesStruct[] _currentDefenses;
        [Layout(1, Count = DEF_COUNT)] private AllAddCurrentDefensesStruct[] _addCurrentDefenses;

        [XmlIgnore]
        public SetpointStruct[] Setpoints
        {
            get
            {
                var result = new SetpointStruct[DEF_COUNT];
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    result[i] = new SetpointStruct();
                    result[i].AddCurrentDefenses = _addCurrentDefenses[i].Clone<AllAddCurrentDefensesStruct>();
                    result[i].CurrentDefenses = this._currentDefenses[i].Clone<AllCurrentDefensesStruct>();
                }
                return result;
            }
            set
            {
                for (int i = 0; i < DEF_COUNT; i++)
                {
                    this._addCurrentDefenses[i] = value[i].AddCurrentDefenses;
                    this._currentDefenses[i] = value[i].CurrentDefenses;
                }
            }
        }

        [XmlElement(ElementName = "Основная")]
        public SetpointStruct Main
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[0].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[0].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set
            {

            }
        }

        [XmlElement(ElementName = "Резервная")]
        public SetpointStruct Reserv
        {
            get
            {
                SetpointStruct result = new SetpointStruct();
                result.AddCurrentDefenses = _addCurrentDefenses[1].Clone<AllAddCurrentDefensesStruct>();
                result.CurrentDefenses = this._currentDefenses[1].Clone<AllCurrentDefensesStruct>();
                return result;
            }
            set { }
        }

    }
}
