﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR550.Version1.Configuration.Structures.CurrentDefenses
{
    public class AllAddCurrentDefensesStruct : StructBase, IDgvRowsContainer<AddCurrentDefenseStruct>
    {
      

        [Layout(0, Count = 2)]
        private AddCurrentDefenseStruct[] _addCurrentDefenses;

        public AddCurrentDefenseStruct[] Rows
        {
            get
            {       _addCurrentDefenses[0].Index = 0;
                _addCurrentDefenses[1].Index = 1;
                return this._addCurrentDefenses;
         
            }
            set
            {
                this._addCurrentDefenses = value;
                _addCurrentDefenses[0].Index = 0;
                _addCurrentDefenses[1].Index = 1;
            }
        }
    }
}
