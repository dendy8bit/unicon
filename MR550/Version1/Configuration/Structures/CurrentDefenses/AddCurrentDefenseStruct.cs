﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR550.Version2.Configuration;

namespace BEMN.MR550.Version1.Configuration.Structures.CurrentDefenses
{
    public class AddCurrentDefenseStruct : StructBase
    {
        public int Index;
        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _block; //вход блокировки
        [Layout(2)] private ushort _srab;//конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(3)] private ushort _ust; //уставка срабатывания_
        [Layout(4)] private ushort _time; //время срабатывания_
        [Layout(5)] private ushort _u; //уставка возврата
        [Layout(6)] private ushort _res1; //уставка возврата+
        [Layout(7)] private ushort _res2; //уставка возврата
        [BindingProperty(0)]
        public string Mode
        {
            get { return Validator.Get(_config, StringsConfig.CurModesV1, 0, 1,2,3); }
            set { _config = Validator.Set(value, StringsConfig.CurModesV1, _config, 0, 1,2,3); }
        }

        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Bloc
        {
            get { return Validator.Get(this._block, StringsConfig.ExternalSignals); }
            set { this._block = Validator.Set(value, StringsConfig.ExternalSignals); }
        }

     


        
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка")]
        public double Srab
        {
            get
            {
                if (Index ==0)
                {
                    return ValuesConverterCommon.GetUstavka5(this._srab);
                }
                return ValuesConverterCommon.GetUstavka100(this._srab);
            }
            set
            {
                if (Index == 0)
                {
                    this._srab = ValuesConverterCommon.SetUstavka5(value);
                }
                else
                {
                    this._srab = ValuesConverterCommon.SetUstavka100(value);
                }
            }
        }



        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "время_срабатывания")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._ust); }
            set { this._ust = ValuesConverterCommon.SetWaitTime(value); }
        }


        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Ускорение")]
        public bool Speedup
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Уставка_ускорения")]
        public int Tspeedup
        {
            get { return ValuesConverterCommon.GetWaitTime(this._u); }
            set { this._u = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config, 6); }
            set { this._config = Common.SetBit(this._config, 6, value); }
        }



        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }

       
       
      
         
    }
}
