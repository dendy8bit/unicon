﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR550.Version1.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        #region [Public field]

        [Layout(0)]
        private ushort _ttMode;//res
        [Layout(1)]
        private ushort _tt;
        [Layout(2)]
        private ushort _ttnp;
        [Layout(3)]
        private ushort _maxI;
        [Layout(4)]
        private ushort _startI;//res
        [Layout(5)]
        private ushort _res0;
        [Layout(6)]
        private ushort _res1;
        [Layout(7)]
        private ushort _res2;
        [Layout(8)]
        private ushort _group1;
        [Layout(9)]
        private ushort _group2;
        [Layout(10)]
        private ushort _group3;
        [Layout(11)]
        private ushort _group4;
        [Layout(12)]
        private ushort _group5;
        [Layout(13)]
        private ushort _group6;
        [Layout(14)]
        private ushort _res3;
      /*  [Layout(15)]
        private ushort _freeLogic;*/




        #endregion [Public field]


         [BindingProperty(0)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Tt
        {
            get { return _tt; }
            set { _tt = value; }
        }
         [BindingProperty(1)]
         [XmlElement(ElementName = "конфигурация_ТТНП")]
         public ushort Ttnp
        {
            get { return _ttnp; }
            set { _ttnp = value; }
        }

         [BindingProperty(2)]
         [XmlElement(ElementName = "Iм")]
        public double MaxI
        {
            get
            {
                return ValuesConverterCommon.GetIn(_maxI);
            }
            set
            {
                _maxI = ValuesConverterCommon.SetIn(value);
            }

        }


    }
}
