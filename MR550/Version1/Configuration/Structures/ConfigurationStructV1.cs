﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR550.CommonStructures.Apv;
using BEMN.MR550.CommonStructures.Avr;
using BEMN.MR550.CommonStructures.FaultSignal;
using BEMN.MR550.CommonStructures.Indicators;
using BEMN.MR550.CommonStructures.Ls;
using BEMN.MR550.CommonStructures.Lzsh;
using BEMN.MR550.CommonStructures.Switch;
using BEMN.MR550.CommonStructures.Vls;
using BEMN.MR550.Version1.Configuration.Structures.CurrentDefenses;
using BEMN.MR550.Version1.Configuration.Structures.ExternalDefenses;
using BEMN.MR550.Version1.Configuration.Structures.ExternalSignals;
using BEMN.MR550.Version1.Configuration.Structures.Keys;
using BEMN.MR550.Version1.Configuration.Structures.MeasuringTransformer;
using BEMN.MR550.Version1.Configuration.Structures.SystenConfig;

namespace BEMN.MR550.Version1.Configuration.Structures
{
    [XmlRoot(ElementName = "МР550")]
    public class ConfigurationStructV1 : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public string DeviceVersion { get; set; }
        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }
        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType { get { return "МР550"; } set { } }

        #region [Private fields]
        [Layout(0)] private MeasureTransStruct _measureTrans;
        [Layout(1)] private KeysStruct _keys;
        [Layout(2)] private ExternalSignalStruct _externalSignal;
        [Layout(3)] private FaultStruct _fault;
        [Layout(4)] private InputLogicSignalStruct _inputLogicSignal;
        [Layout(5)] private SwitchStruct _switch;
        [Layout(6)] private ApvStruct _apv;
        [Layout(7)] private AvrStruct _avr;
        [Layout(8)] private LpbStruct _lzsh;
        [Layout(9)] private AllExternalDefensesStruct _allExternalDefenses;
        [Layout(10)] private AllSetpointsStruct _allSetpoints;
        [Layout(11, Count = 224)] private ushort[] _res;
        [Layout(12)] private OutputLogicSignalStruct _vls;
        [Layout(13, Count = 32)] private ushort[] _res1;
        [Layout(14)] private AllIndicatorsStruct _indicators;
        [Layout(15)] private SystemConfigStruct _systemConfig; 
        #endregion [Private fields]

        [BindingProperty(0)]
        [XmlElement(ElementName = "Измерительный_трансформатор")]
        public MeasureTransStruct MeasureTrans
        {
            get { return _measureTrans; }
            set { _measureTrans = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Внешние_сигналы")]
        public ExternalSignalStruct ExternalSignal
        {
            get { return _externalSignal; }
            set { _externalSignal = value; }
        }
         [BindingProperty(2)]
         [XmlElement(ElementName = "Реле_неисправности")]
        public FaultStruct Fault
        {
            get { return  _fault; }
            set { _fault = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Входные_логические_сигналы")]
        public InputLogicSignalStruct InputLogicSignal
        {
            get { return _inputLogicSignal; }
            set { _inputLogicSignal = value; }
        }
         [BindingProperty(4)]
         [XmlElement(ElementName = "Конфигурация_выключателя")]
        public SwitchStruct Switch
        {
            get { return _switch; }
            set { _switch = value; }
        }
         [BindingProperty(5)]
         [XmlElement(ElementName = "АПВ")]
        public ApvStruct Apv
        {
            get { return _apv; }
            set { _apv = value; }
        }
         [BindingProperty(6)]
         [XmlElement(ElementName = "АВР")]
        public AvrStruct Avr
        {
            get { return _avr; }
            set { _avr = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "ЛЗШ")]
        public LpbStruct Lzsh
        {
            get { return _lzsh; }
            set { _lzsh = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Внешние")]
        public AllExternalDefensesStruct AllExternalDefenses
        {
            get { return _allExternalDefenses; }
            set { _allExternalDefenses = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "Токовые")]
        public AllSetpointsStruct AllSetpoints
        {
            get { return _allSetpoints; }
            set { _allSetpoints = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Все_ВЛС")]
        public OutputLogicSignalStruct Vls
        {
            get { return _vls; }
            set { _vls = value; }
        }

         [BindingProperty(11)]
         [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStruct Indicators
        {
            get { return _indicators; }
            set { _indicators = value; }
        }
      [XmlElement(ElementName = "Осц")]
        [BindingProperty(12)]
        public SystemConfigStruct SystemConfig
        {
            get
            {
                if (_systemConfig.Address == 0)
                {
                    _systemConfig.Address = 1;
                    _systemConfig.Speed = 7;
                }
               
                return _systemConfig;
            }
            set { _systemConfig = value; }
        }
        [BindingProperty(13)]
        [XmlElement(ElementName = "Ключи")]
        public KeysStruct Keys
        {
            get { return _keys; }
            set { _keys = value; }
        }
    }
}
