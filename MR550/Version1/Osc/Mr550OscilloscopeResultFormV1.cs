﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Forms.Osc.UI;
using BEMN_XY_Chart;

namespace BEMN.MR550.Version1.Osc
{
    public partial class Mr550OscilloscopeResultFormV1 : Form
    {
        #region [Constants]
        //private const string MARK_TEXT_PATTERN = "{0} +/-{1}мс";
        private const string TIME_PATTERN = "{0} мс";
        private const string CHANNELS_LABEL_PATTERN = "K{0} = {1}";
        private const string GROUP_DISCRETS = "Discrets";
        private const string CURRENTS_NAME_PATTERN = "Currents {0}";
        private const string DISCRET_LABEL_PATTERN = "Д{0} = {1}";
        private const string CURRENT_LABEL_PATTERN = "U{0} = {1} В";
        private const int CURRENTS_COUNT = 4;
        private const int DISCRETS_COUNT = 8;
        #endregion [Constants]

        /// <summary>
        /// Данные
        /// </summary>
        private readonly CountingListV1 _list;

        /// <summary>
        /// Массив Label для дискрет первого маркера
        /// </summary>
        private Label[] _marker1Discrets;
        /// <summary>
        /// Массив Label для дискрет второго маркера
        /// </summary>
        private Label[] _marker2Discrets;
        /// <summary>
        /// Массив Label для токов первого маркера
        /// </summary>
        private Label[] _marker1Currents;
        /// <summary>
        /// Массив Label для токов второго маркера
        /// </summary>
        private Label[] _marker2Currents;
        
        /// <summary>
        /// Маркеры
        /// </summary>
        private TrackBar[] _markers;
        /// <summary>
        /// Все графики
        /// </summary>
        private DAS_Net_XYChart[] _charts;
        /// <summary>
        /// Линии дискрет
        /// </summary>
        readonly List<DiscretLine> _discretLines = new List<DiscretLine>();
        /// <summary>
        /// Масштабирование по X
        /// </summary>
        // ReSharper disable NotAccessedField.Local
        private readonly ChartsXZoomer _zoomer;
        // ReSharper restore NotAccessedField.Local
        /// <summary>
        /// Масштабирование по Y для токов
        /// </summary>
        // ReSharper disable NotAccessedField.Local
        private ChartYZoomer _yZoomer;
        // ReSharper restore NotAccessedField.Local
        /// <summary>
        /// Хранилище меток
        /// </summary>
        private readonly List<ToolTip> _toolTips = new List<ToolTip>();

        public Mr550OscilloscopeResultFormV1()
        {
            InitializeComponent();
        }

        public Mr550OscilloscopeResultFormV1(CountingListV1 list)
        {
            InitializeComponent();
            Text = string.Format("Осциллограмма МР 550 {0}", list.DateAndTime);
            MAINTABLE.ColumnStyles[1].Width = 0;
            this._list = list;


            this.Init();
            this.PrepareCharts();
            this.DrawAll();
            this._zoomer = new ChartsXZoomer(this._charts, hScrollBar4, this._markers, this._xIncreaseButton,
                                             this._xDecreaseButton);
            this._yZoomer = new ChartYZoomer(this._currentСonnectionsChart, this._currentСonnectionsChartIncreaseButton,
                                            this._currentСonnectionsChartDecreaseButton, this._s1Scroll);
        }
                /// <summary>
        /// Инициализация массивов
        /// </summary>
        private void Init()
        {
            this._charts = new[]
                {
                    this._currentСonnectionsChart,
                    this._discrestsChart
                };

            this._markers = new[]
                {
                    this._marker1TrackBar,
                    this._marker2TrackBar
                };

            this._marker1Currents = new[]
                {
                    this._marker1I1,
                    this._marker1I2,
                    this._marker1I3,
                    this._marker1I4
                };
            this._marker2Currents = new[]
                {
                    this._marker2I1,
                    this._marker2I2,
                    this._marker2I3,
                    this._marker2I4
                };


            this._marker1Discrets = new[]
                {
                    this._marker1D1,
                    this._marker1D2,
                    this._marker1D3,
                    this._marker1D4,
                    this._marker1D5,
                    this._marker1D6,
                    this._marker1D7,
                    this._marker1D8

                };

                    this._marker2Discrets = new[]
                        {
                            this._marker2D1,
                            this._marker2D2,
                            this._marker2D3,
                            this._marker2D4,
                            this._marker2D5,
                            this._marker2D6,
                            this._marker2D7,
                            this._marker2D8
                        };

        }
        /// <summary>
        /// Подготовка графиков
        /// </summary>
        private void PrepareCharts()
        {
            var xMin = 0;
            var xMax = this._list.Count - 1;

            if (this._list.MinI < this._list.MaxI)
            {
                var height = Math.Max(Math.Abs(this._list.MinI), Math.Abs(this._list.MaxI)) * 2;
                this._currentСonnectionsChart.CoordinateYMin = (-1) * height;
                this._currentСonnectionsChart.CoordinateYMax = height;
            }
            else
            {
                this._currentСonnectionsChart.CoordinateYMin = -1;
                this._currentСonnectionsChart.CoordinateYMax = 1;
            }


            foreach (var chart in this._charts)
            {
                chart.XMin = xMin;
                chart.XMax = xMax;
            }

            this._marker1TrackBar.Maximum = xMax;
            this._marker2TrackBar.Maximum = xMax;




            this._discrestsChart.CoordinateYMin = (-1) * DiscretLine.DISCRET_LINE_INTERVAL * (DISCRETS_COUNT);
            this._discrestsChart.Height = DiscretLine.DISCRET_LINE_INTERVAL * (DISCRETS_COUNT + 1);
            this._discrestsChart.CoordinateYMax = 0;
        }
        /// <summary>
        /// Вывод всех графиков
        /// </summary>
        private void DrawAll()
        {
         //   var maxWidth = 100;
            //График дискрет
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                var newLine = new DiscretLine(this._discrestsChart, GROUP_DISCRETS, i, this._list.Discrets[i]);
                this._discretLines.Add(newLine);
            }

            //Выравнивание

        //    tableLayoutPanel5.Width = maxWidth;
       //     tableLayoutPanel8.Width = maxWidth;
        //    panel4.Width = maxWidth - 15;

            //График токов
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                var curveName = string.Format(CURRENTS_NAME_PATTERN, i);
                OscChartHelper.AddLineCurves(this._currentСonnectionsChart, curveName, this.Colors[i], 1,
                                          this._list.Currents[i]);
            }
        }

        #region [Properties]
        /// <summary>
        /// Цвета кнопок токов
        /// </summary>
        private List<Color> Colors
        {
            get
            {
                return new List<Color>
                    {
                        Color.Yellow,
                        Color.Green,
                        Color.Red,
                        Color.Indigo
                    };
            }
        }
        #endregion [Properties]

        #region [Event Handlers]
        /// <summary>
        /// Пуск осциллографа
        /// </summary>
        private void _oscRunСheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this._oscRunСheckBox.Checked)
            {
                foreach (var chart in this._charts)
                {
                    OscChartHelper.AddMarker(chart, 2, this._list.Alarm);
                }
            }
            else
            {
                foreach (var chart in this._charts)
                {
                    OscChartHelper.RemoveMarker(chart, 2);
                }
            }


        }
        /// <summary>
        /// combo box "Токи присоединений"
        /// </summary>
        private void _currentСonnectionsСheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var check = this._voltageСheckBox.Checked;
            this.splitter1.Visible = check;
            this.tableLayoutPanel13.Visible = check;
        }
        /// <summary>
        /// combo box "Дискреты"
        /// </summary>
        private void _discrestsСheckBox_CheckedChanged(object sender, EventArgs e)
        {
            var check = this._discrestsСheckBox.Checked;
            this.tableLayoutPanel14.Visible = check;
        }


        /// <summary>
        /// combo box "Маркеры"
        /// </summary>
        private void _markerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MarkersTable.Visible = this._markerCheckBox.Checked;
            MarkersTable.Update();

            if (this._markerCheckBox.Checked)
            {
                MAINTABLE.ColumnStyles[1].Width = 310;
                this.SetMarker(true, 0, this._marker1TrackBar.Value);
                this.SetMarker(true, 1, this._marker2TrackBar.Value);
            }
            else
            {
                MAINTABLE.ColumnStyles[1].Width = 0;
                this.SetMarker(false, 0, 0);
                this.SetMarker(false, 1, 0);
            }
            MAINTABLE.Update();
            this._markerScrollPanel.Update();
        }
        /// <summary>
        /// Обработчик нажатия на график. Устанавливает/удаляет метки
        /// </summary>
        private void Chart_MouseClick(object sender, MouseEventArgs e)
        {
            if (!this._markCheckBox.Checked)
                return;

            try
            {
                var chart = sender as DAS_Net_XYChart;
                if (chart == null)
                    return;

                if (e.Button == MouseButtons.Left)
                {
                    this._newMarkToolTip = new ToolTip
                    {
                        ShowAlways = true
                    };
                    double length = Math.Abs(chart.XMax) - Math.Abs(chart.XMin);
                    double x = (length / chart.Width * e.X) + chart.XMin;

                    if (e.X < Math.Abs(chart.XMin) && chart.XMin < 0)
                    {
                        x = (Math.Round(length / chart.Width * (e.X + 1), 2) + chart.XMin);
                    }
                    //   var markText = string.Format(MARK_TEXT_PATTERN, Math.Round(x), Math.Round(length/chart.Width));
                    string markText = Math.Round(x).ToString(CultureInfo.InvariantCulture);
                    this._newMarkToolTip.Show(markText, chart, e.X, e.Y);
                    this._toolTips.Add(this._newMarkToolTip);
                }
                else
                {
                    foreach (var toolTip in this._toolTips)
                    {
                        toolTip.RemoveAll();
                    }

                }
            }
            catch
            {

            }

        }



        /// <summary>
        /// Обработчик нажатия кнопок группы "Напряженик"
        /// </summary>
        private void CurrentСonnectionsButton_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button == null) return;
var buttonNumber = 0;
            switch (button.Name)
            {
                case "_i1Button":
                    {
                        buttonNumber = 1;
                        break;
                    }
                case "_i2Button":
                    {
                        buttonNumber = 2;
                        break;
                    }
                case "_i3Button":
                    {
                        buttonNumber = 3;
                        break;
                    }
                case "_i4Button":
                    {
                        buttonNumber = 4;
                        break;
                    }
                    
            }

            

            var newState = this.ChangeButtonColor(button, buttonNumber);
            var curveNumber = buttonNumber - 1;
            var name = string.Format(CURRENTS_NAME_PATTERN, curveNumber);

            if (newState)
            {
                OscChartHelper.AddLineCurves(this._currentСonnectionsChart, name, this.Colors[curveNumber], 1, this._list.Currents[curveNumber]);
            }
            else
            {
                OscChartHelper.RemoveLineCurves(this._currentСonnectionsChart, name);
            }


        }

        /// <summary>
        /// Обработчик нажатия кнопок группы "Дискреты"
        /// </summary>
        private void DiscretesButton_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button == null) return;
            var buttonNumber = int.Parse(button.Text.Remove(0, 1));
            var newState = this.ChangeDiscretButtonColor(button);
            var lineNumber = buttonNumber - 1;
            if (newState)
            {
                this._discretLines[lineNumber].Add();
            }
            else
            {
                this._discretLines[lineNumber].Remove();
            }

        }



        /// <summary>
        /// Перемещение Маркера 1
        /// </summary>
        private void _marker1TrackBar_Scroll(object sender, EventArgs e)
        {
            var x = this._marker1TrackBar.Value;
            this._marker1Time.Text = string.Format(TIME_PATTERN, x);
            this.SetMarker(true, 0, x);
            this.CalculationDeltaTime();
            this.SetLabelText(this._marker1Discrets, this._list.Discrets, x, DISCRET_LABEL_PATTERN);
            this.SetLabelText(this._marker1Currents, this._list.Currents, x, CURRENT_LABEL_PATTERN);

        }

        /// <summary>
        /// Перемещение Маркера 2
        /// </summary>
        private void _marker2TrackBar_Scroll(object sender, EventArgs e)
        {
            var x = this._marker2TrackBar.Value;
            this._marker2Time.Text = string.Format(TIME_PATTERN, x);
            this.SetMarker(true, 1, x);
            this.CalculationDeltaTime();
            this.SetLabelText(this._marker2Discrets, this._list.Discrets, x, DISCRET_LABEL_PATTERN);
            this.SetLabelText(this._marker2Currents, this._list.Currents, x, CURRENT_LABEL_PATTERN);
        }
        
        #endregion [Event Handlers]


        #region [Help members]

        /// <summary>
        /// Изменяет состояние кнопки графика дискрет
        /// </summary>
        /// <param name="button">Кнопка</param>
        /// <returns>Новое состояние</returns>
        private bool ChangeDiscretButtonColor(Button button)
        {
            bool result = button.BackColor.Equals(Color.White);
            button.BackColor = result ? Color.LightSlateGray : Color.White;

            return result;
        }

        /// <summary>
        /// Изменяет состояние кнопки
        /// </summary>
        /// <param name="button">Кнопка</param>
        /// <param name="buttonNumber">Номер кнопки</param>
        /// <returns>Новое состояние</returns>
        private bool ChangeButtonColor(Button button, int buttonNumber)
        {
            bool result;
            if (button.BackColor.Equals(Color.White))
            {

                button.BackColor = Colors[buttonNumber - 1];
                result = true;
            }
            else
            {
                button.BackColor = Color.White;
                result = false;

            }

            button.ForeColor = button.BackColor.Equals(Color.MidnightBlue) ? Color.White : Color.Black;
            return result;
        }

        private void SetLabelText(Label[] labels, double[][] values, int index, string pattern)
        {
            for (int i = 0; i < labels.Length; i++)
            {
                string name = string.Empty;
                switch (i)
                {
                    case 0:
                        {
                            name = "a";
                            break;
                        }
                    case 1:
                        {
                            name = "b";
                            break;
                        }
                    case 2:
                        {
                            name = "c";
                            break;
                        }
                    case 3:
                        {
                            name = "n";
                            break;
                        }

                }
                labels[i].Text = string.Format(pattern, name, Math.Round(values[i][index], 4));
            }
        }

        private void SetLabelText(Label[] labels, ushort[][] values, int index, string pattern)
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Text = string.Format(pattern, i + 1, values[i][index]);
            }
        }
        /// <summary>
        /// Рассчитывает растояние между маркирами и выводит на экран
        /// </summary>
        private void CalculationDeltaTime()
        {
          //  var delta = this._zoomer.Delta;
            var delta =
    Math.Abs(int.Parse(this._marker1Time.Text.Replace("мс", string.Empty)) -
             int.Parse(this._marker2Time.Text.Replace("мс", string.Empty)));
            this._markerTimeDelta.Text = string.Format(TIME_PATTERN, delta);
        }

        /// <summary>
        /// Устанавливает или отключает маркер
        /// </summary>
        /// <param name="state">Флаг</param>
        /// <param name="markerNumber">Номер маркера</param>
        /// <param name="x">Позиция по X</param>
        private void SetMarker(bool state, int markerNumber, int x)
        {
            if (state)
            {
                foreach (var chart in this._charts)
                {
                    OscChartHelper.AddMarker(chart, markerNumber, x);
                }
            }
            else
            {
                foreach (var chart in this._charts)
                {
                    OscChartHelper.RemoveMarker(chart, markerNumber);
                }
            }
        }


        #endregion [Help members]
    }
}

