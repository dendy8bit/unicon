﻿using System;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.MBServer;

namespace BEMN.MR550.Version1.Osc
{
    /// <summary>
    /// Загружает структуру уставок осц. (ДЛЯ СТАРЫХ ОСЦ.!)
    /// </summary>
    public class OldOscOptionsLoader
    {
        #region [Private fields]
        private readonly MemoryEntity<OneWordStruct> _oscConfig;
        private readonly MemoryEntity<OneWordStruct> _oscCount; 
        #endregion [Private fields]



        #region [Events]
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail; 
        #endregion [Events]

        public int AllOscCount
        {
            get { return this._oscCount.Value.Word; }
        }

        /// <summary>
        /// Количество осц.
        /// </summary>
        public int OscCount
        {
            get
            {
                byte config = Common.TOBYTE(this._oscConfig.Value.Word)[1];//0 - старший байт, 1 - младший. нужен младший
                ushort count = this._oscCount.Value.Word;

                switch (config)
                {
                    case 0:
                        return count >= 2 ? 1 : count;
                    case 1:
                        return count >= 2 ? 1 : count;
                    case 2:
                        return count >= 3 ? 2 : count;
                }
                return count;
            }
        }


        #region [Ctor's]
        public OldOscOptionsLoader(Device device)
        {
            this._oscConfig = new MemoryEntity<OneWordStruct>("Конфигурация осциллографа", device, 0x1274);
            this._oscCount = new MemoryEntity<OneWordStruct>("Число осциллограмм", device, 0x3F00);

            this._oscConfig.AllReadOk += o => this._oscCount.LoadStruct();
            this._oscConfig.AllReadFail += OscopeAllReadFail;

            this._oscCount.AllReadOk +=  OscopeAllReadOk;
            this._oscCount.AllReadFail += OscopeAllReadFail;
        }
        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        void OscopeAllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }
        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        void OscopeAllReadOk(object sender)
        {
            if (this.LoadOk != null)
            {
                this.LoadOk.Invoke();
            }
        }  
        #endregion [Memory Entity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запускает загрузку конф. осц.
        /// </summary>
        public void StartRead()
        {
           this._oscConfig.LoadStruct();
        } 
        #endregion [Public members]
    }
}
