﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.MR550.Version1.Osc
{
    /// <summary>
    /// Загружает журнал осцилограммы(СТАРЫЙ)
    /// </summary>
    public class OldOscJournalLoaderV1
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructV1> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _refreshOscJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructV1> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитан весь журнал осциллографа
        /// </summary>
        public event Action AllReadRecordOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        private int _oscCounts;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        public OldOscJournalLoaderV1(Device device)
        {
            this._oscRecords = new List<OscJournalStructV1>();

            //Сброс журнала на нулевую запись
            this._refreshOscJournal = new MemoryEntity<OneWordStruct>("Установка номера осцилограммы", device, 0x3F02);
            this._refreshOscJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(StartReadOscJournal);
            this._refreshOscJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

            //Записи журнала
            this._oscJournal = new MemoryEntity<OscJournalStructV1>("Запись журнала осцилограммы", device, 0x3C00);
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(FailReadOscJournal);

    
        } 
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Номер текущей записи журнала осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return _recordNumber; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructV1> OscRecords
        {
            get { return _oscRecords; }
        }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }


        private void StartReadOscJournal()
        {
            this._oscJournal.LoadStruct();
        }

        public OscJournalStructV1 RecordStruct
        {
            get { return this._oscJournal.Value; }
        }


        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            OscJournalStructV1.RecordIndex = this.RecordNumber;

            this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructV1>());
            this._recordNumber = this.RecordNumber + 1;
            if (this.ReadRecordOk != null)
                this.ReadRecordOk.Invoke();

            if (this._recordNumber == this._oscCounts)
            {
                if (this.AllReadRecordOk != null)
                    this.AllReadRecordOk.Invoke();
                return;
            }

            this.WriteRecordNumber();
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal(int oscCounts)
        {
            if (oscCounts == 0)
            {
                if (this.AllReadRecordOk != null)
                    this.AllReadRecordOk.Invoke();
                return;
            }
            this._oscCounts = oscCounts;
            this._recordNumber = 0;
            this.WriteRecordNumber();
        } 

        private void WriteRecordNumber()
        {
            this._refreshOscJournal.Value = new OneWordStruct {Word = (ushort) ((ushort) this._recordNumber+1)};
            this._refreshOscJournal.SaveStruct6();
        }

        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }
    }
}
