using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MR550.Version1.Configuration.Structures.MeasuringTransformer;
using System.IO;
using System.Reflection;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR550.Version1.Osc
{
    public partial class Mr550OscilloscopeFormV1 : Form, IFormView
    {
        #region [Constants]
        private const string OSC_IS_EMPTY = "������ ������������ ����";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string FAIL_LOAD_OSC = "���������� ��������� ������������";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OldOscJournalLoaderV1 _oldOscJournalLoader;


        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV1 _countingList;

        private OscJournalStructV1 _journalStruct;
        private readonly DataTable _table;
        private readonly OldOscOptionsLoader _oscopeOptionsLoader;
        private readonly Mr550Device _device;
        private MemoryEntity<MeasureTransStruct> _voltageConfiguration;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr550OscilloscopeFormV1()
        {
            this.InitializeComponent();
        }

        public Mr550OscilloscopeFormV1(Mr550Device device)
        {
            this.InitializeComponent();

            this._device = device;

            //��������� ������� �����
            this._voltageConfiguration = device.VoltageConfigurationV1;
            this._voltageConfiguration.ReadOk +=HandlerHelper.CreateHandler(this,() => this._oscopeOptionsLoader.StartRead()) ;
            this._voltageConfiguration.ReadFail += HandlerHelper.CreateHandler(this, this.FailReadOscJournal);

            //������������ �����������
            this._oscopeOptionsLoader = new OldOscOptionsLoader(device);
            this._oscopeOptionsLoader.LoadOk += () => this._oldOscJournalLoader.StartReadJournal(this._oscopeOptionsLoader.OscCount);
            this._oscopeOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            
            //��������� �������
            this._oldOscJournalLoader = new OldOscJournalLoaderV1(device);
            this._oldOscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, this.ReadRecord);
            this._oldOscJournalLoader.AllReadRecordOk += HandlerHelper.CreateActionHandler(this, this.OscJournalLoaded);
            this._oldOscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadOscJournal);
            
            this._pageLoader = new OldOscPageLoader(this._device);
            this._pageLoader.SlotReadSuccessful += HandlerHelper.CreateActionHandler(this, () => this._oscProgressBar.PerformStep());
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadFail += HandlerHelper.CreateActionHandler(this, this.OscReadFail);

            this._table = this.GetJournalDataTable();

        }
        #endregion [Ctor's]    


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }
        
        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oldOscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add
                (
                    this._oldOscJournalLoader.RecordStruct.GetNumber,
                    this._oldOscJournalLoader.RecordStruct.GetDate,
                    this._oldOscJournalLoader.RecordStruct.GetTime,
                    this._oldOscJournalLoader.RecordStruct.SizeTime,
                    this._oldOscJournalLoader.RecordStruct.FaultTime
                );
            this._oscJournalDataGrid.Refresh();
        }

        private void OscJournalLoaded()
        {
            this.EnableButtons = true;
            if (this._oldOscJournalLoader.OscRecords.Count == 0)
            {
                this._statusLabel.Text = OSC_IS_EMPTY;
            }
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = new CountingListV1(this._pageLoader.ResultArray, this._journalStruct, this._voltageConfiguration.Value);

            this._oscSaveButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscReadButton.Enabled = true;

            this.EnableButtons = true;
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadFail()
        {
            this._statusLabel.Text = FAIL_LOAD_OSC;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }
        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV1 CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��550_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this._countingList == null)
            {
                this._countingList = new CountingListV1(new ushort[12000], new OscJournalStructV1(), new MeasureTransStruct());
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this._countingList.IsLoad)
                {
                    fileName = this._countingList.FilePath;
                }
                else
                {
                    fileName =Validator.CreateOscFileNameCfg($"��550 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                Mr550OscilloscopeResultFormV1 resForm = new Mr550OscilloscopeResultFormV1(this.CountingList);
                resForm.Show();
            }
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oldOscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this.EnableButtons = false;
            this._voltageConfiguration.LoadStruct();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oldOscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscopeOptionsLoader.AllOscCount, selectedOsc);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.SlotCount;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingListV1.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = FAIL_LOAD_OSC;
            }

        }

        #endregion [Event Handlers]


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr550Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr550OscilloscopeFormV1); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "�������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this._oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
    }
}