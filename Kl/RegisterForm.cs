﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Kl.HelpClasses;
using BEMN.Kl.Structures;

namespace BEMN.Kl
{
    public partial class RegisterForm : Form
    {
        private MemoryEntity<RegLogicProg> _reg;
        private int _nodeNumber;
        private string _oldText;
        private int _count;
        private bool _isReplace;
        private bool _validatingOk;

        public RegisterForm()
        {
            InitializeComponent();
        }

        public RegisterForm(string labelText, int maskCount, int nodeNum, MemoryEntity<RegLogicProg> reg)
        {
            InitializeComponent();
            _nodeNumber = nodeNum;
            _reg = reg;
            _regName.Text = labelText;
            InitMaskedTextBox(maskCount);
        }

        #region Function
        private void InitMaskedTextBox(int maskCount)
        {
            string mask = ">";
            for (int i = 0; i < maskCount; i++)
            {
                mask += "a";
            }
            _regValue.Mask = mask;
            _count = maskCount;
            switch (_count)
            {
                case 2:
                    _regValue.ValidatingType = typeof (byte);
                    _regValue.Size = TextBoxSizes.SByte;
                    break;
                case 4:
                    _regValue.ValidatingType = typeof (UInt16);
                    _regValue.Size = TextBoxSizes.SUInt16;
                    break;
                case 8:
                    _regValue.ValidatingType = typeof (UInt32);
                    _regValue.Size = TextBoxSizes.SUInt32;
                    break;
                case 16:
                    _regValue.ValidatingType = typeof (UInt64);
                    _regValue.Size = TextBoxSizes.SUInt64;
                    break;
            }
        }

        private void SetNewValue(int number)
        {
            RegLogicProg rg = _reg.Value;
            switch (number)
            {
                case 1:
                    rg.Alow1 = _regValue.Text;
                    break;
                case 2:
                    rg.Ahigh1 = _regValue.Text;
                    break;
                case 3:
                    rg.Alow2 = _regValue.Text;
                    break;
                case 4:
                    rg.Ahigh2 = _regValue.Text;
                    break;
                case 5:
                    rg.Alow3 = _regValue.Text;
                    break;
                case 6:
                    rg.Ahigh3 = _regValue.Text;
                    break;
                case 7:
                    rg.Alow4 = _regValue.Text;
                    break;
                case 8:
                    rg.Ahigh4 = _regValue.Text;
                    break;
                case 9:
                    rg.AXlow1 = _regValue.Text;
                    break;
                case 10:
                    rg.AXhigh1 = _regValue.Text;
                    break;
                case 11:
                    rg.AXlow2 = _regValue.Text;
                    break;
                case 12:
                    rg.AXhigh2 = _regValue.Text;
                    break;
                case 13:
                    rg.EAXlow = _regValue.Text;
                    break;
                case 14:
                    rg.EAXhigh = _regValue.Text;
                    break;
                case 15:
                    rg.RAX = _regValue.Text;
                    break;
                case 16:
                    rg.Blow1 = _regValue.Text;
                    break;
                case 17:
                    rg.Bhigh1 = _regValue.Text;
                    break;
                case 18:
                    rg.Blow2 = _regValue.Text;
                    break;
                case 19:
                    rg.Bhigh2 = _regValue.Text;
                    break;
                case 20:
                    rg.Blow3 = _regValue.Text;
                    break;
                case 21:
                    rg.Bhigh3 = _regValue.Text;
                    break;
                case 22:
                    rg.Blow4 = _regValue.Text;
                    break;
                case 23:
                    rg.Bhigh4 = _regValue.Text;
                    break;
                case 24:
                    rg.BXlow1 = _regValue.Text;
                    break;
                case 25:
                    rg.BXhigh1 = _regValue.Text;
                    break;
                case 26:
                    rg.BXlow2 = _regValue.Text;
                    break;
                case 27:
                    rg.BXhigh2 = _regValue.Text;
                    break;
                case 28:
                    rg.EBXlow = _regValue.Text;
                    break;
                case 29:
                    rg.EBXhigh = _regValue.Text;
                    break;
                case 30:
                    rg.RBX = _regValue.Text;
                    break;
                case 31:
                    rg.Clow1 = _regValue.Text;
                    break;
                case 32:
                    rg.Chigh1 = _regValue.Text;
                    break;
                case 33:
                    rg.Clow2 = _regValue.Text;
                    break;
                case 34:
                    rg.Chigh2 = _regValue.Text;
                    break;
                case 35:
                    rg.Clow3 = _regValue.Text;
                    break;
                case 36:
                    rg.Chigh3 = _regValue.Text;
                    break;
                case 37:
                    rg.Clow4 = _regValue.Text;
                    break;
                case 38:
                    rg.Chigh4 = _regValue.Text;
                    break;
                case 39:
                    rg.CXlow1 = _regValue.Text;
                    break;
                case 40:
                    rg.CXhigh1 = _regValue.Text;
                    break;
                case 41:
                    rg.CXlow2 = _regValue.Text;
                    break;
                case 42:
                    rg.CXhigh2 = _regValue.Text;
                    break;
                case 43:
                    rg.ECXlow = _regValue.Text;
                    break;
                case 44:
                    rg.ECXhigh = _regValue.Text;
                    break;
                case 45:
                    rg.RCX = _regValue.Text;
                    break;
                case 46:
                    rg.Dlow1 = _regValue.Text;
                    break;
                case 47:
                    rg.Dhigh1 = _regValue.Text;
                    break;
                case 48:
                    rg.Dlow2 = _regValue.Text;
                    break;
                case 49:
                    rg.Dhigh2 = _regValue.Text;
                    break;
                case 50:
                    rg.Dlow3 = _regValue.Text;
                    break;
                case 51:
                    rg.Dhigh3 = _regValue.Text;
                    break;
                case 52:
                    rg.Dlow4 = _regValue.Text;
                    break;
                case 53:
                    rg.Dhigh4 = _regValue.Text;
                    break;
                case 54:
                    rg.DXlow1 = _regValue.Text;
                    break;
                case 55:
                    rg.DXhigh1 = _regValue.Text;
                    break;
                case 56:
                    rg.DXlow2 = _regValue.Text;
                    break;
                case 57:
                    rg.DXhigh2 = _regValue.Text;
                    break;
                case 58:
                    rg.EDXlow = _regValue.Text;
                    break;
                case 59:
                    rg.EDXhigh = _regValue.Text;
                    break;
                case 60:
                    rg.RDX = _regValue.Text;
                    break;
                case 61:
                    rg.SP = _regValue.Text;
                    break;
                case 62:
                    rg.DP = _regValue.Text;
                    break;
                case 63:
                    rg.LP = _regValue.Text;
                    break;
                case 64:
                    rg.SC = _regValue.Text;
                    break;
                case 65:
                    rg.SCLP = _regValue.Text;
                    break;
                case 66:
                    rg.SREG = _regValue.Text;
                    break;
                case 67:
                    rg.DEBUG = _regValue.Text;
                    break;
                case 68:
                    rg.IP = _regValue.Text;
                    break;
                case 69:
                    rg.CS = _regValue.Text;
                    break;
                case 70:
                    rg.CSIP = _regValue.Text;
                    break;
                case 71:
                    rg.COMMAND = _regValue.Text;
                    break;
            }
            _reg.Value = rg;
        }
        #endregion

        #region Events
        private void _regValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'g' && e.KeyChar <= 'z') || (e.KeyChar >= 'G' && e.KeyChar <= 'Z'))
            {
                MaskedTextBox textBox = sender as MaskedTextBox;
                if (textBox != null && !_isReplace)
                {
                    _oldText = textBox.Text;
                    _isReplace = true;
                }
            }
        }

        private void _regValue_KeyUp(object sender, KeyEventArgs e)
        {
            if (_isReplace)
            {
                _regValue.Text = _oldText;
                _isReplace = false;
            }
        }

        private void _ok_Click(object sender, EventArgs e)
        {
            _regValue.ValidateText();
            if (!_validatingOk) return;
            SetNewValue(_nodeNumber);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void _regValue_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                switch (_count)
                {
                    case 2:
                    {
                        if (_regValue.Text.Length < 2)
                        {
                            _validatingOk = false;
                        }
                        else
                        {
                            byte b;
                            _validatingOk = byte.TryParse(_regValue.Text, NumberStyles.HexNumber,
                                CultureInfo.InvariantCulture, out b);
                        }
                        break;
                    }
                    case 4:
                    {
                        if (_regValue.Text.Length < 4)
                        {
                            _validatingOk = false;
                        }
                        else
                        {
                            UInt16 r16;
                            _validatingOk = UInt16.TryParse(_regValue.Text, NumberStyles.HexNumber,
                                CultureInfo.InvariantCulture, out r16);
                        }
                        break;
                    }
                    case 8:
                    {
                        if (_regValue.Text.Length < 8)
                        {
                            _validatingOk = false;
                        }
                        else
                        {
                            UInt32 r32;
                            _validatingOk = UInt32.TryParse(_regValue.Text, NumberStyles.HexNumber,
                                CultureInfo.InvariantCulture, out r32);
                        }
                        break;
                    }
                    case 16:
                    {
                        UInt64 r64;
                        if (_regValue.Text.Length < 16)
                        {
                            _validatingOk = false;
                        }
                        else
                        {
                            _validatingOk = UInt64.TryParse(_regValue.Text, NumberStyles.HexNumber,
                                CultureInfo.InvariantCulture, out r64);
                        }
                        break;
                    }
                }
            }
            catch (Exception)
            {
                _validatingOk = false;
            }
        }
        #endregion
    }
}
