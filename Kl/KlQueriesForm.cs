﻿using System;
using System.ComponentModel;
using System.Drawing;
using BEMN.Forms.Queries;
using BEMN.Interfaces;

namespace BEMN.Kl
{
    public class KlQueriesForm : QueriesForm, IFormView
    {
        public KlQueriesForm()
        {
            Multishow = true;
        }

        public KlQueriesForm(KlDevice kl) : base(kl)
        {
            Multishow = true;
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(KlDevice); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(QueriesForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.Calibrate; }
        }

        public string NodeName
        {
            get { return "Обмены"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
