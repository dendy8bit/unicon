﻿using System.Collections.Generic;

namespace BEMN.Kl.HelpClasses
{
    public class StringData
    {
        public static double DevVersion = 0;

        public static List<string> RS_SPEEDS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "600",
                    "1200",
                    "2400",
                    "4800",
                    "9600",
                    "19200",
                    "38400",
                    "76800",
                    "900",
                    "1800",
                    "3600",
                    "7200",
                    "14400",
                    "28800",
                    "57600",
                    "115200"
                });
            }
        }

        public static List<string> RS_DATA_BITS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "5 бит",
                    "6 бит",
                    "7 бит",
                    "8 бит",
                    "9 бит"
                });
            }
        }

        public static List<string> RS_STOPBITS
        {
            get
            {
                return new List<string>(new string[]
                {
                    "1 бит",
                    "2 бита"
                });
            }
        }

        public static List<string> RS_PARITET_YN
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Нет",
                    "Есть"
                });
            }
        }

        public static List<string> RS_PARITET_CHET
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Четный",
                    "Нечетный"
                });
            }
        }

        public static List<string> RS_DOUBLESPEED
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Без удвоения",
                    "С удвоением"
                });
            }
        }

        public static List<string> SystemStatusErrors
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Ошибка CRC",
                    "Ошибка таймаута",
                    "Плохой ответ",
                    "Плохой запрос",
                    "",
                    "",
                    "",
                    ""
                });
            }
        }

        public static List<string> Date
        {
            get
            {
                return new List<string>(new string[]
                {
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });
            }
        }

        public static Dictionary<ushort, string> Modules
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00, "OK"},
                    {0x01, "TWI"},
                    {0x02, "SPI"},
                    {0x03, "EXEEPROM"},
                    {0x04, "INEEPROM"},
                    {0x05, "Часы"},
                    {0x06, "Флэш"},
                    {0x07, "Диагностический порт"},
                    {0x08, "Порт связи"},
                    {0x09, "Аналоговый канал"},
                    {0x0A, "Дискреты"},
                    {0x0B, "Реле"},
                    {0x0C, "Светодиоды"},
                    {0x10, "Логическая программа"},
                    {0x40, "Порт связи(запросы)"},
                    {0xFE, "Включение устройства"},
                    {0xFF, "Выключение устройства"}
                };
            }
        }

        public static Dictionary<ushort, string> RtcMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00, "ошибка"},
                    {0x01, "некорректная конфигурация"},
                    {0x80, "текущее время"},
                    {0x81, "изменение времени"},
                    {0x82, "изменение конфигурации времени"}
                };
            }
        }

        public static Dictionary<ushort, string> Usart0Messages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00, "ошибка"},
                    {0x01, "некорректная конфигурация"},
                    {0x81, "изменение конфигурации (по умолчанию)"},
                    {0x82, "изменение конфигурации"},
                };
            }
        }
        public static Dictionary<ushort, string> DisRelayExMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00,"ошибка"},
                    {0x01,"некорректная конфигурация"},
                    {0x81,"обновление значения"},
                };
            }
        }
        public static Dictionary<ushort, string> LogicMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00, "ошибка"},
                    {0x01, "нулевая команда"},
                    {0x02, "выход за пределы программы"},
                    {0x03, "выход за пределы команд"},
                    {0x04, "ошибка (доступ)"},
                    {0x05, "ошибка (адрес)"},
                    {0x06, "ошибка (константа)"},
                    {0x07, "ошибка (стек)"},
                    {0x08, "ошибка (отладка команды)"},
                    {0x09, "ошибка (СПЛ)"},
                    {0x0A, "неверный ВР"},
                    {0x0B, "сработка watch-dog"},
                    {0x0C, "деление на ноль"},
                    {0x81, "запуск логики"},
                    {0x82, "сброс логики"},
                    {0x83, "сброс логики с ожиданием"},
                    {0x84, "остановка логики"},
                    {0x85, "обновление логики"},
                    {0x86, "изменение сигнатуры запуска"},
                };
            }
        }

        public static Dictionary<ushort, string> BaseMessages
        {
            get
            {
                return new Dictionary<ushort, string>
                {
                    {0x00,"ошибка"},
                    {0x01,"некорректная конфигурация"},
                    {0x81,"изменение конфигурации"}
                };
            }
        }

        public static List<string> Command
        {
            get
            {
                return new List<string>
                {
                    "Чтение слов",
                    "Запись слов",
                    "Чтение бит",
                    "Запись бит"
                };
            }
        }
    }
}
