﻿using System.Drawing;

namespace BEMN.Kl.HelpClasses
{
    public struct TextBoxSizes
    {
        public static Size SByte = new Size(25, 20);
        public static Size SUInt16 = new Size(40, 20);
        public static Size SUInt32 = new Size(70, 20);
        public static Size SUInt64 = new Size(135, 20);
    }
}
