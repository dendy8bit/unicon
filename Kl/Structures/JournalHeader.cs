﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct JournalHeader : IStruct, IStructInit
    {
        private ushort _currentMessage;
        private ushort _counter;       

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _currentMessage = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _counter = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_currentMessage);
            result.Add(_counter);
            return result.ToArray();
        }
        #endregion

        public ushort Counter
        {
            get { return _counter; }
            set { _counter = value; }
        }

        public ushort CurrentMessage
        {
            get { return _currentMessage; }
            set { _currentMessage = value; }
        }
    }
}
