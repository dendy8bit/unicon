﻿using System;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.MBServer;

namespace BEMN.MLK.Structures
{
    /*16-1*/
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct BEMNVersion : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public UInt16[] device;

        #region [IStruct Members]
        public StructInfo GetStructInfo()
        {
            return StructHelper.GetStructInfo(this.GetType());
        }

        public object GetSlots(ushort start, bool slotArray, out int arrayLength)
        {
            return StructHelper.GetSlots(start, slotArray, out arrayLength, this.GetType());
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            device = Common.TOWORDS(array, false);
        }

        public ushort[] GetValues()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
