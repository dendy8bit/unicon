﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    public struct UIORamV2_0 : IStruct, IStructInit
    {
        private ushort _analog;
        private ushort _discret1;
        private ushort _discret2;
        private ushort _relay1;
        private ushort _relay2;
        private ushort _diods;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            _analog = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _discret1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            _discret2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _relay1 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
            _relay2 = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _diods = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_analog);
            result.Add(_discret1);
            result.Add(_discret2);
            result.Add(_relay1);
            result.Add(_relay2);
            result.Add(_diods);
            return result.ToArray();
        }
        #endregion

        public ushort Analog
        {
            get { return _analog; }
        }
        /// <summary>
        /// Дискреты, начиная с младшего бита
        /// </summary>
        public BitArray Discrets
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_discret1).Reverse().ToArray());
                buf.AddRange(Common.TOBYTE(_discret2).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }

        public BitArray Relay
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_relay1).Reverse().ToArray());
                buf.AddRange(Common.TOBYTE(_relay2).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }
        public BitArray Diods
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_diods).Reverse().ToArray());
                BitArray ret = new BitArray(buf.ToArray());
                return ret;
            }
        }
    }
}
