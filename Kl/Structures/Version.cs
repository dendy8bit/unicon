﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct VersionDevice : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 24)]
        private ushort[] _versionInfo;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            _versionInfo = new ushort[24];
            int ind = 0;
            for (int i = 0; i < _versionInfo.Length; i++)
            {
                _versionInfo[i] = Common.TOWORD(array[ind], array[ind+1]);
                ind += sizeof (ushort);
            }
        }

        public ushort[] GetValues()
        {
            return null;
        }
        #endregion

        #region Properties for version of application
        public string AppName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appName = GetParam(words.ToArray())[0];
                return appName;
            }
        }

        public string AppPodtip
        {
            get
            {
                try
                {
                    List<ushort> words = new List<ushort>();
                    for (int i = 0; i < 8; i++)
                    {
                        words.Add(_versionInfo[i]);
                    }
                    string appPodtip = GetParam(words.ToArray())[1];
                    return appPodtip;
                }
                catch (Exception)
                {
                    return "SC";
                }
            }
        }

        public string AppMod
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appMod = GetParam(words.ToArray())[2];
                return appMod;
            }
        }

        public string AppVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 0; i < 8; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string appVersion = GetParam(words.ToArray())[3];
                return appVersion;
            }
        }
        #endregion

        #region Properties for version of device
        public string DevName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devName = GetParam(words.ToArray())[0];
                return devName;
            }
        }

        public string DevPodtip
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devPodtip = GetParam(words.ToArray())[1];
                return devPodtip;
            }
        }

        public string DevMod
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devMod = GetParam(words.ToArray())[2];
                return devMod;
            }
        }

        public string DevVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 16; i < 24; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string devVersion = GetParam(words.ToArray())[3];
                return devVersion;
            }
        }
        #endregion

        #region Properties for version of firmware

        public string LdrName
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 8; i < 16; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string ldrName = GetParam(words.ToArray())[0];
                return ldrName;
            }
        }

        public string LdrVersion
        {
            get
            {
                List<ushort> words = new List<ushort>();
                for (int i = 8; i < 16; i++)
                {
                    words.Add(_versionInfo[i]);
                }
                string ldrVers = GetParam(words.ToArray())[3];
                return ldrVers;
            }
        }
        // три байта во второй строке версии
        public string LdrCodProc
        {
            get
            {
                List<byte> buf = new List<byte>();
                List<string> cod = new List<string>();
                buf.AddRange(Common.TOBYTE(_versionInfo[10]));
                buf.Add(Common.TOBYTE(_versionInfo[11])[0]);
                foreach (var b in buf)
                {
                    if (Convert.ToString(b, 16).Length < 2)
                    {
                        cod.Add("0" + Convert.ToString(b, 16));
                    }
                    else
                    {
                        cod.Add(Convert.ToString(b, 16));
                    }
                    cod.Add(".");
                }
                cod.RemoveAt(cod.Count-1);
                string ret = string.Empty;
                foreach (var c in cod)
                {
                    ret += c;
                }
                return ret;
            }
        }

        public string LdrFuseProc
        {
            get
            {
                List<byte> buf = new List<byte>();
                buf.AddRange(Common.TOBYTE(_versionInfo[12]));
                buf.AddRange(Common.TOBYTE(_versionInfo[13]));
                string ret = "";
                ret += Convert.ToString(buf[0], 16) + "." + Convert.ToString(buf[1], 16) + "." +
                       Convert.ToString(buf[2], 16) + "." + Convert.ToString(buf[3], 16);
                return ret;
            }
        }
        #endregion

        #region Methods
        string[] GetParam(ushort[] words)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            byte[] byteBuf = Common.TOBYTES(words, true);
            char[] charBuf = new char[byteBuf.Length];
            int byteUsed = 0;
            int charUsed = 0;
            bool complete = false;

            try
            {
                dec.Convert(byteBuf, 0, byteBuf.Length, charBuf, 0, byteBuf.Length, true, out byteUsed, out charUsed,
                    out complete);
            }
            catch (ArgumentNullException)
            {
                throw new ApplicationException("Передан нулевой буфер");
            }

            string[] param = new string(charBuf, 0, 16).Split(new string[] { " ", "    " },
                StringSplitOptions.RemoveEmptyEntries);
            return param;
        }
        #endregion
    }
}
