﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Kl.HelpClasses;
using BEMN.Kl.Structures.IntermediateStructures;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigRS485 : IStruct, IStructInit
    {
        public UsartConfig configRS485;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public ushort[] reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            configRS485 = StructHelper.GetOneStruct(array, ref index, configRS485);

            reserved = new ushort[3];
            for (int i = 0; i < reserved.Length; i++)
            { reserved[i] = 0; }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(configRS485.GetValues());
            result.AddRange(new ushort[3]);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public bool RS485Mode
        {
            get
            {
                bool ret = Common.GetBits(configRS485.Hip1, 13) >> 13 != 0;
                return ret;
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)(value ? 1 : 0), 13);
            }
        }

        public string RS485Speed
        {
            get
            {
                return StringData.RS_SPEEDS[(int)Common.GetBits(configRS485.Hip1, 0, 1, 2, 3) >> 0];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_SPEEDS.IndexOf(value), 0, 1, 2, 3);
            }
        }

        public string RS485DataBits
        {
            get
            {
                string ret;
                if ((int) Common.GetBits(configRS485.Hip1, 4, 5, 6) >> 4 == 9)
                {
                    ret = StringData.RS_DATA_BITS[4];
                }
                else
                {
                    ret = StringData.RS_DATA_BITS[(int) Common.GetBits(configRS485.Hip1, 4, 5, 6) >> 4];
                }
                return ret;
            }
            set
            {
                
                if ((ushort) StringData.RS_DATA_BITS.IndexOf(value) == 4)
                {
                    ushort setValue = 7;
                    configRS485.Hip1 = Common.SetBits(configRS485.Hip1, setValue,4, 5, 6);
                }
                else
                {
                    configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_DATA_BITS.IndexOf(value), 4, 5, 6);
                }
            }
        }

        public string RS485StopBits
        {
            get
            {
                return StringData.RS_STOPBITS[(int)Common.GetBits(configRS485.Hip1, 7) >> 7];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_STOPBITS.IndexOf(value), 7);
            }
        }

        public string RS485ParitetChet
        {
            get
            {
                return StringData.RS_PARITET_CHET[(int)Common.GetBits(configRS485.Hip1, 8) >> 8];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_PARITET_CHET.IndexOf(value), 8);
            }
        }

        public string RS485ParitetOnOff
        {
            get
            {
                return StringData.RS_PARITET_YN[(int)Common.GetBits(configRS485.Hip1, 9) >> 9];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_PARITET_YN.IndexOf(value), 9);
            }
        }

        public string RS485DoubleSpeed
        {
            get
            {
                return StringData.RS_DOUBLESPEED[(int)Common.GetBits(configRS485.Hip1, 10) >> 10];
            }
            set
            {
                configRS485.Hip1 = Common.SetBits(configRS485.Hip1, (ushort)StringData.RS_DOUBLESPEED.IndexOf(value), 10);
            }
        }

        public byte RS485Address
        {
            get
            {
                return configRS485.Address;
            }
            set
            {
                configRS485.Address = value;
            }
        }

        public byte RS485ToSend
        {
            get
            {
                return configRS485.ToSend;
            }
            set
            {
                configRS485.ToSend = value;
            }
        }

        public byte RS485ToSendAfter
        {
            get
            {
                return configRS485.ToSendAfter;
            }
            set
            {
                configRS485.ToSendAfter = value;
            }
        }

        public byte RS485ToSendBefore
        {
            get
            {
                return configRS485.ToSendBefore;
            }
            set
            {
                configRS485.ToSendBefore = value;
            }
        }

        public ushort RS485Answer
        {
            get
            {
                return configRS485.ToWaitAnswer;
            }
            set
            {
                configRS485.ToWaitAnswer = value;
            }
        }
        #endregion
    }
}
