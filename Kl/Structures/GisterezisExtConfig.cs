﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    public struct GisterezisExtConfig : IStruct, IStructInit
    {
        private ushort _lowLimit;
        private ushort _highLimit;
        private byte _lowLimitSignal;
        private byte _highLimitSignal;
        private ushort _endOfScale;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region IStructInit Members
        public void InitStruct(byte[] array)
        {
            int index = 0;

            _lowLimit = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _highLimit = Common.TOWORD(array[index + 1], array[index]);
            index += sizeof(ushort);

            _lowLimitSignal = array[index];
            index += sizeof (byte);

            _highLimitSignal = array[index];
            index += sizeof (byte);

            _endOfScale = Common.TOWORD(array[index + 1], array[index]);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.Add(_lowLimit);
            result.Add(_highLimit);
            result.Add(Common.TOWORD(HighLimitSignal, LowLimitSignal));
            result.Add(_endOfScale);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public ushort LowLimit
        {
            get { return _lowLimit; }
            set { _lowLimit = value; }
        }

        public ushort HighLimit
        {
            get { return _highLimit; }
            set { _highLimit = value; }
        }

        public byte LowLimitSignal
        {
            get { return _lowLimitSignal; }
            set { _lowLimitSignal = value; }
        }

        public byte HighLimitSignal
        {
            get { return _highLimitSignal; }
            set { _highLimitSignal = value; }
        }

        public ushort EndOfScale
        {
            get { return _endOfScale; }
            set { _endOfScale = value; }
        }
        #endregion
    }
}
