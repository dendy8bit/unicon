﻿using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;

namespace BEMN.Kl.Structures
{
    public struct Journal : IStruct, IStructInit
    {
        public JournalRep[] Jrep;

        public Journal(int count)
        {
            Jrep = new JournalRep[count];
        }

        #region IStruct Members
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            StructHelper.GetArrayStruct(array, Jrep, ref index);
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            foreach (JournalRep j in Jrep)
            {
                result.AddRange(j.GetValues());
            }
            return result.ToArray();
        } 
        #endregion
    }
}
