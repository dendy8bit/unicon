﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Kl.HelpClasses;
using BEMN.Kl.Structures.IntermediateStructures;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MemConfigUSB : IStruct, IStructInit
    {
        public UsartConfig configUSB;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _reserved;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            int index = 0;
            configUSB = StructHelper.GetOneStruct(array, ref index, configUSB, 64);

            _reserved = new ushort[3];
            for (int i = 0; i < _reserved.Length; i++)
            {
                _reserved[i] = 0;
            }
        }

        public ushort[] GetValues()
        {
            List<ushort> result = new List<ushort>();
            result.AddRange(configUSB.GetValues());
            result.AddRange(_reserved);
            return result.ToArray();
        }
        #endregion

        #region Properties
        public string USBSpeed
        {
            get
            {
                return StringData.RS_SPEEDS[(int)Common.GetBits(configUSB.Hip1, 0, 1, 2, 3) >> 0];
            }
            set
            {
                configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_SPEEDS.IndexOf(value), 0, 1, 2, 3);
            }
        }

        public string USBDataBits
        {
            get
            {
                string ret;
                if ((int) Common.GetBits(configUSB.Hip1, 4, 5, 6) >> 4 == 9)
                {
                    ret = StringData.RS_DATA_BITS[4];
                }
                else
                {
                    ret = StringData.RS_DATA_BITS[(int) Common.GetBits(configUSB.Hip1, 4, 5, 6) >> 4];
                }
                return ret;
            }
            set
            {
                
                if ((ushort) StringData.RS_DATA_BITS.IndexOf(value) == 4)
                {
                    ushort setValue = 7;
                    configUSB.Hip1 = Common.SetBits(configUSB.Hip1, setValue,4, 5, 6);
                }
                else
                {
                    configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_DATA_BITS.IndexOf(value), 4, 5, 6);
                }
            }
        }

        public string USBStopBits
        {
            get
            {
                return StringData.RS_STOPBITS[(int)Common.GetBits(configUSB.Hip1, 7) >> 7];
            }
            set
            {
                configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_STOPBITS.IndexOf(value), 7);
            }
        }

        public string USBParitetChet
        {
            get
            {
                return StringData.RS_PARITET_CHET[(int)Common.GetBits(configUSB.Hip1, 8) >> 8];
            }
            set
            {
                configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_PARITET_CHET.IndexOf(value), 8);
            }
        }

        public string USBParitetOnOff
        {
            get
            {
                return StringData.RS_PARITET_YN[(int)Common.GetBits(configUSB.Hip1, 9) >> 9];
            }
            set
            {
                configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_PARITET_YN.IndexOf(value), 9);
            }
        }

        public string USBDoubleSpeed
        {
            get
            {
                return StringData.RS_DOUBLESPEED[(int)Common.GetBits(configUSB.Hip1, 10) >> 10];
            }
            set
            {
                configUSB.Hip1 = Common.SetBits(configUSB.Hip1, (ushort)StringData.RS_DOUBLESPEED.IndexOf(value), 10);
            }
        }

        public byte USBAddress
        {
            get
            {
                return configUSB.Address;
            }
            set
            {
                configUSB.Address = value;
            }
        }

        public byte USBToSend
        {
            get
            {
                return configUSB.ToSend;
            }
            set
            {
                configUSB.ToSend = value;
            }
        }

        public byte USBToSendAfter
        {
            get
            {
                return configUSB.ToSendAfter;
            }
            set
            {
                configUSB.ToSendAfter = value;
            }
        }

        public byte USBToSendBefore
        {
            get
            {
                return configUSB.ToSendBefore;
            }
            set
            {
                configUSB.ToSendBefore = value;
            }
        }
        #endregion
    }
}
