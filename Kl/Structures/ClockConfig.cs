﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using BEMN.Devices;
using BEMN.Devices.StructHelperClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.MBServer;

namespace BEMN.Kl.Structures
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ClockConfig : IStruct, IStructInit
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        private ushort[] _reserved1;
        private ushort _correction;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        private ushort[] _reserved2;

        #region [IStruct Members]
        public StructInfo GetStructInfo(int slotLen)
        {
            return StructHelper.GetStructInfo(this.GetType(), slotLen);
        }

        public object GetSlots(ushort start, bool slotArray, int slotLen)
        {
            return StructHelper.GetSlots(start, slotArray, this.GetType(), slotLen);
        }
        #endregion [IStruct Members]

        #region [IStructInit]
        public void InitStruct(byte[] array)
        {
            _reserved1 = new ushort[3];
            int ind = 0;
            ind += sizeof (ushort)*3;
            _correction = Common.TOWORD(array[ind + 1], array[ind]);
            _reserved2 = new ushort[4];
        }

        public ushort[] GetValues()
        {
            List<ushort> ret = new List<ushort>();
            ret.AddRange(_reserved1);
            ret.Add(_correction);
            ret.AddRange(_reserved2);
            return ret.ToArray();
        }
        #endregion

        #region Properties

        public bool IncDec
        {
            get { return Common.GetBit(_correction, 15); }
            set { _correction = Common.SetBit(_correction, 15, value); }
        }

        public ushort Correction
        {
            get { return Common.GetBits(_correction, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14); }
            set { _correction = Common.SetBits(_correction, value, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14); }
        }
        #endregion
    }
}
