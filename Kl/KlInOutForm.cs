﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.Kl.HelpClasses;
using BEMN.Kl.Structures;
using BEMN.Kl.Structures.IntermediateStructures;
using BEMN.MBServer;

namespace BEMN.Kl
{
    public partial class KlInOutForm : Form, IFormView
    {
        #region Поля
        private readonly KlDevice _device;
        private LedControl[] _discretLeds;
        private LedControl[] _relayLeds;
        private LedControl[] _diodLeds;
        private MemoryEntity<UIORamV2_0> _uioRamV2;
        private MemoryEntity<SomeStruct> _signature;
        private MemoryEntity<OneWordStruct> _rs485Config;
        private readonly MemoryEntity<Clock> _clock;
        private readonly MemoryEntity<SysErr> _sysErr;
        private readonly MemoryEntity<QueryCount> _countOfQuery;
        private readonly MemoryEntity<GisterezisExtConfig> _endOfScale;
        private readonly MemoryEntity<VersionDevice> _versionMkl;
        
        private bool _amplBit;
        private bool _isMaster;
        private byte _countOfDiscrets;
        private byte _countOfRelays;
        private byte _countOfDiods;
        private double _limit;
        private int _countOfRequest;
        #endregion

        #region Константы
        private const string CANAL_ERR = "Неисправен канал";
        private const string LOWER_LIMIT = "Нижний предел";
        private const string UPPER_LIMIT = "Верхний предел";
        private const string ERROR_WRITING_DATE = "Невозможно записать дату/врумя";
        private const string ERROR_WRITING = "Ошибка записи";
        private const string CONNECTION_OK = "Ошибок соединения нет";
        #endregion

        #region Внутренняя структурка
        public struct BitInfo
        {
            public ushort BitStartAddress;
            public ushort BitIndex;
            public bool BitValue;
        }
        #endregion

        #region Конструкторы
        public KlInOutForm()
        {
            this.InitializeComponent();
        }

        public KlInOutForm(KlDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ReadNewCount += this.ReadNewValueCount;
            this._device.ReadNewLimit += this.ReadNewLimit;
            this._device.ReadRs485Config += this.ReadNewRs485Config;
            this._device.CheckBit += bit => this._amplBit = bit;

            this._rs485Config = new MemoryEntity<OneWordStruct>("Режим RS-485", this._device, 0x1008);
            this._rs485Config.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._isMaster = Common.GetBit(this._rs485Config.Value.Word, 13);
                if (this._isMaster) return;
                this.statisticTab.Visible = false;
                this.tabControl.SelectedIndex = 0;
            });

            this._countOfQuery = new MemoryEntity<QueryCount>("Количество запросов статистики", this._device, 0x1018);
            this._countOfQuery.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._countOfRequest = this._countOfQuery.Value.CountOfRequest;
                this._errorsDG.Rows.Clear();
            });
            this._countOfQuery.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._countOfRequest = 0);

            this._endOfScale = new MemoryEntity<GisterezisExtConfig>("Предел шкалы", this._device, 0x1503);
            this._endOfScale.AllReadOk += HandlerHelper.CreateReadArrayHandler(this,
                () => this._limit = this._endOfScale.Value.EndOfScale);
            this._endOfScale.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => this._limit = 300);

            this._clock = this._device.Clock;
            this._clock.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ClockAstrReadComplete);
            this._clock.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ClockAstrReadFail);

            this._sysErr = this._device.SysErr;
            this._sysErr.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SysErrorReadCompleat);
            this._sysErr.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SysErrorReadFail);

            this._versionMkl = new MemoryEntity<VersionDevice>("Версия прошивки, загрузчика и устройства", this._device, 0x1F00);
            this._versionMkl.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadOk);
            this._versionMkl.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.VersionReadFail);
        }

        private void InitControls()
        {
            this._discretsGroup = this.InitGroupBox(this._countOfDiscrets, 65 - 6, 0, "Дискреты");
            this._relayGroup = this.InitGroupBox(this._countOfRelays, this._discretsGroup.Top, this._discretsGroup.Height, "Реле");

            this._discretLeds = this.InitLeds(this._discretsGroup, this._countOfDiscrets, "Д{0}");
            this._relayLeds = this.InitLeds(this._relayGroup, this._countOfRelays, "Р{0}");
            foreach (var led in this._relayLeds)
            {
                led.LedClicked += this.RelayLedMouseClick;
            }
            this._inOutTabPage.Controls.AddRange(new Control[]{this._discretsGroup, this._relayGroup});
            this._inOutTabPage.Update();
            if (this._countOfDiods == 0) return;

            this._diodGroup = this.InitGroupBox(this._countOfDiods, this._relayGroup.Top, this._relayGroup.Height, "Индикаторы");
            this._diodLeds = this.InitLeds(this._diodGroup, this._countOfDiods, "И{0}");
            foreach (var led in this._diodLeds)
            {
                led.LedClicked += this.DiodLedMouseClick;
            }
            this._inOutTabPage.Controls.Add(this._diodGroup);
            this._inOutTabPage.Update();
        }
        private GroupBox InitGroupBox(int count, int preY, int preHeigth, string title)
        {
            GroupBox box = new GroupBox();
            int t = count/12;
            box.Height = 55 + t*32;
            box.Width = count > 12 ? 310 : ((2 * count - 1) * 13 + 12);
            if (box.Width < 83)
                box.Width = 83;
            box.Location = new Point(6, preY + preHeigth + 6);
            box.Text = title;
            return box;
        }

        private LedControl[] InitLeds(GroupBox group, int count, string s)
        {
            LedControl[] leds = new LedControl[count];
            Label[] labels = new Label[count];
            int j = 0,l = 0;
            for (int i = 0; i < leds.Length; i++)
            {
                if (i%12 == 0 && i != 0)
                {
                    j++;
                    l = 0;
                }
                leds[i] = new LedControl();
                leds[i].Location = new Point(6 + l*25, 19 + j*32);
                labels[i] = new Label();
                labels[i].Location = new Point(2 + l*25, 35 + j*32);
                labels[i].Width = i < 9 ? 20 : 26;
                labels[i].Height = 15;
                labels[i].Text = string.Format(s, i + 1);
                group.Controls.Add(leds[i]);
                group.Controls.Add(labels[i]);
                l++;
            }
            return leds;
        }

        #endregion

        #region Аналоги, дискреты, реле и светодиоды

        private void SignatureReadOk()
        {
            byte[] buf = Common.TOBYTES(this._signature.Values, true);
            if(!CRC16.VerifyRespCrc(buf))
            {
                MessageBox.Show("Значение CRC не совпадает", "Ошибка CRC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Common.SwapArrayItems(ref buf);
            if (!this.VerifyConfigVersion(buf))
            {
                MessageBox.Show("В устройстве записана конфигурация версии не 1.0", "Ошибка версии конфигурации",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            byte sm1 = buf[16];
            byte sm2 = buf[17];
            byte sm3 = buf[18];
            this._countOfDiscrets = buf[sm1];
            this._countOfRelays = buf[sm2];
            this._countOfDiods = buf[sm3];
            this.InitControls();
            this._uioRamV2.LoadStructCycle();
        }

        private void SignatureReadFail()
        {
            MessageBox.Show("Не удалось прочитать конфигурацию дискрет, реле и светодиодов", "Ошибка чтения сигнатуры",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void UioRamV2ReadOk()
        {
            #region Analog

            if (this._amplBit)
            {
                this._analogU.Text = this._uioRamV2.Value.Analog.ToString();
            }
            else
            {
                switch (this._uioRamV2.Value.Analog)
                {
                    case (ushort) 0xffff:
                        this._analogU.Text = CANAL_ERR;
                        break;
                    case (ushort) 0x8000:
                        this._analogU.Text = LOWER_LIMIT;
                        break;
                    case (ushort) 0x7fff:
                        this._analogU.Text = UPPER_LIMIT;
                        break;
                    default:
                        this._analogU.Text = Math.Round(this._limit/0x7ffe*(double) this._uioRamV2.Value.Analog, 5).ToString();
                        break;
                }
            }

            #endregion

            BitArray setArray;
            #region Discret
            setArray = this.GetBitsArray(this._uioRamV2.Value.Discrets, this._countOfDiscrets);
            LedManager.SetLeds(this._discretLeds, setArray);
            #endregion
            #region Relay
            setArray = this.GetBitsArray(this._uioRamV2.Value.Relay, this._countOfRelays);
            LedManager.SetLeds(this._relayLeds, setArray);
            #endregion
            #region Diod
            setArray = this.GetBitsArray(this._uioRamV2.Value.Diods, this._countOfDiods);
            LedManager.SetLeds(this._diodLeds, setArray);
            #endregion
        }
        private void UioRamV2ReadFail()
        {
            this._analogU.Text = string.Empty;

            LedManager.TurnOffLeds(this._discretLeds);
            LedManager.TurnOffLeds(this._relayLeds);
            LedManager.TurnOffLeds(this._diodLeds);
        }
        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(KlDevice); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(KlInOutForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Ввод - вывод"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Астраномические дата/время

        public void ClockAstrReadComplete()
        {
            this._astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrDateClockTB.Text = this.PrepareGridParam(this._clock.Value.Day.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Month.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Year.ToString(), 2, 0);
            this._astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrTimeClockTB.Text = this.PrepareGridParam(this._clock.Value.Hour.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Minutes.ToString(), 2, 0) + this.PrepareGridParam(this._clock.Value.Seconds.ToString(), 2, 0);
        }

        public void ClockAstrReadFail()
        {
            this._astrDateClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrDateClockTB.Text = "00" + "00" + "00";
            this._astrTimeClockTB.TextMaskFormat = MaskFormat.IncludeLiterals;
            this._astrTimeClockTB.Text = "00" + "00" + "00";
        }

        #endregion

        #region Системные ошибки
        private void SysErrorReadCompleat() 
        {
            if (this._errorsDG.Rows.Count == 0)
            {
                this._errorsDG.Rows.Clear();
                for (int i = 0; i < this._countOfRequest; i++)
                {
                    byte buf = this._sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += StringData.SystemStatusErrors[j] + "\n";
                        }
                    }

                    this._errorsDG.Rows.Add((this._errorsDG.Rows.Count + 1).ToString(), this._sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString(), this._sysErr.Value.StatisticsRequest[i].SendMessage.ToString(), this._sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString(),
                        stat);

                    this.SetCellColor(this._sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);

                    this._errorsDG.Rows[i].Height = 20;
                }
            }
            else
            {
                for (int i = 0; i < this._countOfRequest; i++)
                {
                    byte buf = this._sysErr.Value.StatisticsRequest[i].Status;
                    string stat = string.Empty;
                    if (buf == 0)
                    {
                        stat = CONNECTION_OK;
                    }
                    else
                    {
                        BitArray bitStat = new BitArray(new byte[] { buf });
                        for (int j = 0; j < bitStat.Length; j++)
                        {
                            if (bitStat[j])
                                stat += StringData.SystemStatusErrors[j] + "/n";
                        }
                    }
                    this._errorsDG.Rows[i].Cells[1].Value = this._sysErr.Value.StatisticsRequest[i].AcceptMessage.ToString();
                    this._errorsDG.Rows[i].Cells[2].Value = this._sysErr.Value.StatisticsRequest[i].SendMessage.ToString();
                    this._errorsDG.Rows[i].Cells[3].Value = this._sysErr.Value.StatisticsRequest[i].CommunicationQuality.ToString();
                    this._errorsDG.Rows[i].Cells[4].Value = stat;

                    this.SetCellColor(this._sysErr.Value.StatisticsRequest[i].CommunicationQuality, i);
                }
            }
        }

        private void SysErrorReadFail()
        {
            this._errorsDG.Rows.Clear();
        }
        #endregion

        #region Дополнительные функции

        private void VersionReadOk()
        {
            double vers = Convert.ToDouble(this._versionMkl.Value.AppVersion, CultureInfo.InvariantCulture);
            if (vers >= 2.0)
            {
                this._uioRamV2 = this._device.UioRamV2;
                this._uioRamV2.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.UioRamV2ReadOk);
                this._uioRamV2.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.UioRamV2ReadFail);
                this._signature = this._device.Signature;
                this._signature.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadOk);
                this._signature.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.SignatureReadFail);
                this._signature.LoadStruct();
            }
            else
            {
                return; 
            }
        }

        private void VersionReadFail()
        {
            MessageBox.Show("Невозможно прочитать версию устройства", "Ошибка чтения версии", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
        }
        private void RemoveAllQuerys()
        {
            if(this._uioRamV2!=null) this._uioRamV2.RemoveStructQueries();

            this._clock.RemoveStructQueries();
            this._sysErr.RemoveStructQueries();
        }

        private string PrepareGridParam(string param, int count, int index)
        {
            if (param.Length < count)
            {
                param = "0" + param;
            }
            if (param.Length > count)
            {
                string temp = string.Empty;
                for (int i = index; i < param.Length; i++)
                {
                    temp += param[i];
                }
                param = temp;
            }
            return param;
        }

        private void SetCellColor(ushort communicationQuality, int i)
        {
            if (communicationQuality < 25)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Red;
            }
            else if (communicationQuality >= 25 && communicationQuality < 50)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightCoral;
            }
            else if (communicationQuality >= 50 && communicationQuality < 75)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.LightGreen;
            }
            else if (communicationQuality >= 75 && communicationQuality <= 100)
            {
                this._errorsDG.Rows[i].Cells[3].Style.BackColor = Color.Green;
            }
        }
        private void SetBitState(LedControl currentLed, LedControl[] leds, ushort startAddr, BitArray currentState, MouseButtons click, bool relayOrDiod)
        {
            try
            {
                List<LedControl> ledList = new List<LedControl>();
                ledList.AddRange(leds);
                int ledInd = ledList.IndexOf(currentLed);
                bool value = currentLed.State == LedState.Signaled;
                switch (click)
                {
                    //По левой кнопке инвертируем бит
                    case MouseButtons.Left:
                        this._device.SetBit(this._device.DeviceNumber, (ushort)(startAddr + ledInd), value,
                                "SetRelayValue" + this._device.DeviceNumber, this._device);
                        break;
                    //По правой показываем меню
                    case MouseButtons.Right:
                        ContextMenu menu = new ContextMenu();
                        BitInfo info;
                        info.BitIndex = (ushort)ledInd;
                        info.BitValue = value;
                        info.BitStartAddress = startAddr;
                        string contextItem;
                        if (relayOrDiod)
                        {
                             contextItem = string.Format(currentState[ledInd]
                                ? "Выключить светодиод № - {0}"
                                : "Включить светодиод № - {0}", ledInd + 1);
                        }
                        else
                        {
                            contextItem = string.Format(currentState[ledInd]
                                ? "Выключить реле № - {0}"
                                : "Включить реле № - {0}", ledInd + 1);
                        }
                        menu.MenuItems.Add(contextItem);

                        menu.MenuItems[0].Tag = info;
                        menu.MenuItems[0].Click += this.OnContextMenuItemClicked;
                        menu.Show(this._diodGroup, new Point(currentLed.Location.X+13, currentLed.Location.Y+13),
                            LeftRightAlignment.Right);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Невозможно изменить состояние реле", "Ошибка");
            }
        }

        /// <summary>
        /// Устанавливает новое значение счетчика запросов
        /// </summary>
        private void ReadNewValueCount()
        {
            this._countOfQuery.LoadStruct();
        }
        /// <summary>
        /// Читает новый предел шкалы
        /// </summary>
        private void ReadNewLimit()
        {
            this._endOfScale.LoadStruct();
        }
        /// <summary>
        /// Читает конфигурацию RS-485: устройство ведущее или ведомое
        /// </summary>
        private void ReadNewRs485Config()
        {
            this._rs485Config.LoadStruct();
        }

        private bool VerifyConfigVersion(byte[] buf)
        {
            System.Text.Decoder dec = System.Text.Encoding.ASCII.GetDecoder();
            char[] charBuf = new char[16];
            int byteUsed = 0;
            int charUsed = 0;
            bool complete = false;
            dec.Convert(buf, 0, 16, charBuf, 0, 16, true, out byteUsed, out charUsed, out complete);
            string configStr = new string(charBuf, 0, 13);
            string vers = new string(charBuf,13,3);
            if ((configStr.ToUpper() == "CONFIGVERSION") && (vers == "1.0"))
            {
                complete = true;
            }
            else
            {
                complete = false;
            }
            return complete;
        }

        private BitArray GetBitsArray(BitArray array, int length)
        {
            BitArray ret = new BitArray(length);
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = array[i];
            }
            return ret;
        }
        #endregion

        #region Обработчики событий

        private void KlInOutShown(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            MdiParent.Refresh();
            this._versionMkl.LoadStruct();
            this._countOfQuery.LoadStruct();
            this._rs485Config.LoadStruct();
            this._endOfScale.LoadStruct();
            this._clock.LoadStructCycle();
            this._sysErr.LoadStructCycle();        
        }

        private void _astrStopCB_CheckedChanged(object sender, EventArgs e)
        {
            if (this._astrStopCB.Checked)
            {
                this._clock.RemoveStructQueries();
                this._dateTimeNowButt.Enabled = false;
                this._writeDateTimeButt.Enabled = true;
            }
            else
            {
                this._clock.LoadStructCycle();
                this._dateTimeNowButt.Enabled = true;
                this._writeDateTimeButt.Enabled = false;
            }
        }

        private void MLK_InOut_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.RemoveAllQuerys();
        }
        
        private void RelayLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            double vers = Convert.ToDouble(this._versionMkl.Value.AppVersion, CultureInfo.InvariantCulture);
            if (vers >= 2.0)
            {
                this.SetBitState(led, this._relayLeds, 0x8030, this._uioRamV2.Value.Relay, e.Button, false);
            }
        }

        private void DiodLedMouseClick(object sender, MouseEventArgs e)
        {
            LedControl led = sender as LedControl;
            this.SetBitState(led, this._diodLeds, 0x8050, this._uioRamV2.Value.Diods, e.Button, true);
        }

        private void OnContextMenuItemClicked(object sender, EventArgs e)
        {
            var info = (BitInfo)(sender as MenuItem).Tag;

            this._device.SetBit(this._device.DeviceNumber, (ushort)(info.BitStartAddress + info.BitIndex), info.BitValue,
                "SetRelayValue" + this._device.DeviceNumber, this._device);
        }

        private void _dateTimeNowButt_Click(object sender, EventArgs e)
        {
            try
            {
                if (this._astrStopCB.Checked) return;
                Clock clock = this._clock.Value;
                clock.Day = (ushort) DateTime.Now.Day;
                clock.Month = (ushort) DateTime.Now.Month;
                clock.Year = (ushort) (DateTime.Now.Year - 2000);
                clock.Hour = (ushort) DateTime.Now.Hour;
                clock.Minutes = (ushort) DateTime.Now.Minute;
                clock.Seconds = (ushort) DateTime.Now.Second;
                this._clock.Value = clock;
                this._clock.SaveStruct();
            }
            catch
            {
                MessageBox.Show(ERROR_WRITING_DATE, ERROR_WRITING, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void _writeDateTimeButt_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> dateTime = new List<string>();
                dateTime.AddRange(this._astrDateClockTB.Text.Split('.'));
                dateTime.AddRange(this._astrTimeClockTB.Text.Split(':', '.', ','));
                Clock clock = this._clock.Value;
                clock.Day = Convert.ToUInt16(dateTime[0]);
                clock.Month = Convert.ToUInt16(dateTime[1]);
                clock.Year = Convert.ToUInt16(dateTime[2]);
                clock.Hour = Convert.ToUInt16(dateTime[3]);
                clock.Minutes = Convert.ToUInt16(dateTime[4]);
                clock.Seconds = Convert.ToUInt16(dateTime[5]);
                this._clock.Value = clock;
                this._clock.SaveStruct();
            }
            catch
            {
                MessageBox.Show(ERROR_WRITING_DATE, ERROR_WRITING, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        
        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (this._isMaster) return;
            this.tabControl.SelectedIndex = 0;
            this.statisticTab.Visible = false;
        }
        #endregion
    }
}