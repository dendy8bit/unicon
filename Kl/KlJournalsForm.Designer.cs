﻿namespace BEMN.Kl
{
    partial class KlJournalsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this._journalTabControl = new System.Windows.Forms.TabControl();
            this._sysPage = new System.Windows.Forms.TabPage();
            this._countOfSysMes = new System.Windows.Forms.Label();
            this._saveSysJour = new System.Windows.Forms.Button();
            this._clearSysJourBnt = new System.Windows.Forms.Button();
            this._sysJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sysJournalDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sysJournalDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sysJournalTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sysJournalEvent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._sysParameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._readSysJournalButton = new System.Windows.Forms.Button();
            this._logicPage = new System.Windows.Forms.TabPage();
            this._openXml = new System.Windows.Forms.Button();
            this._countOfLogicMes = new System.Windows.Forms.Label();
            this._saveLogicJour = new System.Windows.Forms.Button();
            this._clearLogicJourBtn = new System.Windows.Forms.Button();
            this._logicJournalGrid = new System.Windows.Forms.DataGridView();
            this._number = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dayOfWeek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._message = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._openLogicJour = new System.Windows.Forms.Button();
            this._readLogicJournalButton = new System.Windows.Forms.Button();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this._closeBtn = new System.Windows.Forms.Button();
            this._maximizeBtn = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this._openSysJour = new System.Windows.Forms.Button();
            this._openSysXml = new System.Windows.Forms.Button();
            this._journalTabControl.SuspendLayout();
            this._sysPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).BeginInit();
            this._logicPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _journalTabControl
            // 
            this._journalTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._journalTabControl.Controls.Add(this._sysPage);
            this._journalTabControl.Controls.Add(this._logicPage);
            this._journalTabControl.Location = new System.Drawing.Point(0, 0);
            this._journalTabControl.Name = "_journalTabControl";
            this._journalTabControl.SelectedIndex = 0;
            this._journalTabControl.Size = new System.Drawing.Size(689, 406);
            this._journalTabControl.TabIndex = 0;
            this._journalTabControl.SelectedIndexChanged += new System.EventHandler(this._journalTabControl_SelectedIndexChanged);
            // 
            // _sysPage
            // 
            this._sysPage.Controls.Add(this._openSysXml);
            this._sysPage.Controls.Add(this._countOfSysMes);
            this._sysPage.Controls.Add(this._saveSysJour);
            this._sysPage.Controls.Add(this._clearSysJourBnt);
            this._sysPage.Controls.Add(this._sysJournalGrid);
            this._sysPage.Controls.Add(this._openSysJour);
            this._sysPage.Controls.Add(this._readSysJournalButton);
            this._sysPage.Location = new System.Drawing.Point(4, 22);
            this._sysPage.Name = "_sysPage";
            this._sysPage.Padding = new System.Windows.Forms.Padding(3);
            this._sysPage.Size = new System.Drawing.Size(681, 380);
            this._sysPage.TabIndex = 0;
            this._sysPage.Text = "Системный журнал";
            this._sysPage.UseVisualStyleBackColor = true;
            // 
            // _countOfSysMes
            // 
            this._countOfSysMes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._countOfSysMes.AutoSize = true;
            this._countOfSysMes.Location = new System.Drawing.Point(251, 356);
            this._countOfSysMes.Name = "_countOfSysMes";
            this._countOfSysMes.Size = new System.Drawing.Size(0, 13);
            this._countOfSysMes.TabIndex = 19;
            // 
            // _saveSysJour
            // 
            this._saveSysJour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveSysJour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._saveSysJour.Location = new System.Drawing.Point(302, 351);
            this._saveSysJour.Name = "_saveSysJour";
            this._saveSysJour.Size = new System.Drawing.Size(75, 23);
            this._saveSysJour.TabIndex = 18;
            this._saveSysJour.Text = "Сохранить";
            this._saveSysJour.UseVisualStyleBackColor = true;
            this._saveSysJour.Click += new System.EventHandler(this._saveSysJour_Click);
            // 
            // _clearSysJourBnt
            // 
            this._clearSysJourBnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._clearSysJourBnt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._clearSysJourBnt.Location = new System.Drawing.Point(598, 351);
            this._clearSysJourBnt.Name = "_clearSysJourBnt";
            this._clearSysJourBnt.Size = new System.Drawing.Size(75, 23);
            this._clearSysJourBnt.TabIndex = 17;
            this._clearSysJourBnt.Text = "Очистить";
            this._clearSysJourBnt.UseVisualStyleBackColor = true;
            this._clearSysJourBnt.Click += new System.EventHandler(this._clearSysJourBnt_Click);
            // 
            // _sysJournalGrid
            // 
            this._sysJournalGrid.AllowUserToAddRows = false;
            this._sysJournalGrid.AllowUserToDeleteRows = false;
            this._sysJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._sysJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._sysJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._sysJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._sysJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._sysJournalDay,
            this._sysJournalDate,
            this._sysJournalTime,
            this._sysJournalEvent,
            this._sysParameter});
            this._sysJournalGrid.Location = new System.Drawing.Point(3, 3);
            this._sysJournalGrid.Name = "_sysJournalGrid";
            this._sysJournalGrid.RowHeadersVisible = false;
            this._sysJournalGrid.Size = new System.Drawing.Size(675, 342);
            this._sysJournalGrid.TabIndex = 16;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = "0";
            this._indexCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._indexCol.Width = 30;
            // 
            // _sysJournalDay
            // 
            this._sysJournalDay.DataPropertyName = "_sysJournalDay";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._sysJournalDay.DefaultCellStyle = dataGridViewCellStyle3;
            this._sysJournalDay.HeaderText = "День недели";
            this._sysJournalDay.Name = "_sysJournalDay";
            this._sysJournalDay.ReadOnly = true;
            this._sysJournalDay.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sysJournalDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _sysJournalDate
            // 
            this._sysJournalDate.DataPropertyName = "_sysJournalDate";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._sysJournalDate.DefaultCellStyle = dataGridViewCellStyle4;
            this._sysJournalDate.HeaderText = "Дата";
            this._sysJournalDate.Name = "_sysJournalDate";
            this._sysJournalDate.ReadOnly = true;
            this._sysJournalDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sysJournalDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._sysJournalDate.Width = 80;
            // 
            // _sysJournalTime
            // 
            this._sysJournalTime.DataPropertyName = "_sysJournalTime";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._sysJournalTime.DefaultCellStyle = dataGridViewCellStyle5;
            this._sysJournalTime.HeaderText = "Время";
            this._sysJournalTime.Name = "_sysJournalTime";
            this._sysJournalTime.ReadOnly = true;
            this._sysJournalTime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._sysJournalTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._sysJournalTime.Width = 80;
            // 
            // _sysJournalEvent
            // 
            this._sysJournalEvent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._sysJournalEvent.DataPropertyName = "_sysJournalEvent";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._sysJournalEvent.DefaultCellStyle = dataGridViewCellStyle6;
            this._sysJournalEvent.HeaderText = "Сообщение";
            this._sysJournalEvent.Name = "_sysJournalEvent";
            this._sysJournalEvent.ReadOnly = true;
            this._sysJournalEvent.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._sysJournalEvent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._sysJournalEvent.Width = 71;
            // 
            // _sysParameter
            // 
            this._sysParameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._sysParameter.DataPropertyName = "_sysParameter";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._sysParameter.DefaultCellStyle = dataGridViewCellStyle7;
            this._sysParameter.HeaderText = "Параметр";
            this._sysParameter.Name = "_sysParameter";
            this._sysParameter.ReadOnly = true;
            this._sysParameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._sysParameter.Width = 64;
            // 
            // _readSysJournalButton
            // 
            this._readSysJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readSysJournalButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readSysJournalButton.Location = new System.Drawing.Point(8, 351);
            this._readSysJournalButton.Name = "_readSysJournalButton";
            this._readSysJournalButton.Size = new System.Drawing.Size(75, 23);
            this._readSysJournalButton.TabIndex = 0;
            this._readSysJournalButton.Text = "Прочитать";
            this._readSysJournalButton.UseVisualStyleBackColor = true;
            this._readSysJournalButton.Click += new System.EventHandler(this._systemJournalButton_Click);
            // 
            // _logicPage
            // 
            this._logicPage.Controls.Add(this._openXml);
            this._logicPage.Controls.Add(this._countOfLogicMes);
            this._logicPage.Controls.Add(this._saveLogicJour);
            this._logicPage.Controls.Add(this._clearLogicJourBtn);
            this._logicPage.Controls.Add(this._logicJournalGrid);
            this._logicPage.Controls.Add(this._openLogicJour);
            this._logicPage.Controls.Add(this._readLogicJournalButton);
            this._logicPage.Location = new System.Drawing.Point(4, 22);
            this._logicPage.Name = "_logicPage";
            this._logicPage.Padding = new System.Windows.Forms.Padding(3);
            this._logicPage.Size = new System.Drawing.Size(681, 380);
            this._logicPage.TabIndex = 1;
            this._logicPage.Text = "Журнал логической программы";
            this._logicPage.UseVisualStyleBackColor = true;
            // 
            // _openXml
            // 
            this._openXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._openXml.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._openXml.Location = new System.Drawing.Point(170, 351);
            this._openXml.Name = "_openXml";
            this._openXml.Size = new System.Drawing.Size(126, 23);
            this._openXml.TabIndex = 24;
            this._openXml.Text = "Файл расшифровки";
            this._openXml.UseVisualStyleBackColor = true;
            this._openXml.Click += new System.EventHandler(this._openXml_Click);
            // 
            // _countOfLogicMes
            // 
            this._countOfLogicMes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._countOfLogicMes.AutoSize = true;
            this._countOfLogicMes.Location = new System.Drawing.Point(251, 356);
            this._countOfLogicMes.Name = "_countOfLogicMes";
            this._countOfLogicMes.Size = new System.Drawing.Size(0, 13);
            this._countOfLogicMes.TabIndex = 23;
            // 
            // _saveLogicJour
            // 
            this._saveLogicJour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._saveLogicJour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._saveLogicJour.Location = new System.Drawing.Point(302, 351);
            this._saveLogicJour.Name = "_saveLogicJour";
            this._saveLogicJour.Size = new System.Drawing.Size(75, 23);
            this._saveLogicJour.TabIndex = 22;
            this._saveLogicJour.Text = "Сохранить";
            this._saveLogicJour.UseVisualStyleBackColor = true;
            this._saveLogicJour.Click += new System.EventHandler(this._saveLogicJour_Click);
            // 
            // _clearLogicJourBtn
            // 
            this._clearLogicJourBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._clearLogicJourBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._clearLogicJourBtn.Location = new System.Drawing.Point(598, 351);
            this._clearLogicJourBtn.Name = "_clearLogicJourBtn";
            this._clearLogicJourBtn.Size = new System.Drawing.Size(75, 23);
            this._clearLogicJourBtn.TabIndex = 21;
            this._clearLogicJourBtn.Text = "Очистить";
            this._clearLogicJourBtn.UseVisualStyleBackColor = true;
            this._clearLogicJourBtn.Click += new System.EventHandler(this._clearLogicJourBtn_Click);
            // 
            // _logicJournalGrid
            // 
            this._logicJournalGrid.AllowUserToAddRows = false;
            this._logicJournalGrid.AllowUserToDeleteRows = false;
            this._logicJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._logicJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._logicJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this._logicJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._logicJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._number,
            this._dayOfWeek,
            this._date,
            this._time,
            this._message,
            this._parameter});
            this._logicJournalGrid.Location = new System.Drawing.Point(3, 3);
            this._logicJournalGrid.Name = "_logicJournalGrid";
            this._logicJournalGrid.RowHeadersVisible = false;
            this._logicJournalGrid.Size = new System.Drawing.Size(675, 342);
            this._logicJournalGrid.TabIndex = 20;
            // 
            // _number
            // 
            this._number.DataPropertyName = "_number";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._number.DefaultCellStyle = dataGridViewCellStyle9;
            this._number.Frozen = true;
            this._number.HeaderText = "№";
            this._number.Name = "_number";
            this._number.ReadOnly = true;
            this._number.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._number.Width = 30;
            // 
            // _dayOfWeek
            // 
            this._dayOfWeek.DataPropertyName = "_dayOfWeek";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._dayOfWeek.DefaultCellStyle = dataGridViewCellStyle10;
            this._dayOfWeek.HeaderText = "День недели";
            this._dayOfWeek.Name = "_dayOfWeek";
            this._dayOfWeek.ReadOnly = true;
            this._dayOfWeek.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._dayOfWeek.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _date
            // 
            this._date.DataPropertyName = "_date";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._date.DefaultCellStyle = dataGridViewCellStyle11;
            this._date.HeaderText = "Дата";
            this._date.Name = "_date";
            this._date.ReadOnly = true;
            this._date.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._date.Width = 80;
            // 
            // _time
            // 
            this._time.DataPropertyName = "_time";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._time.DefaultCellStyle = dataGridViewCellStyle12;
            this._time.HeaderText = "Время";
            this._time.Name = "_time";
            this._time.ReadOnly = true;
            this._time.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._time.Width = 80;
            // 
            // _message
            // 
            this._message.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._message.DataPropertyName = "_message";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._message.DefaultCellStyle = dataGridViewCellStyle13;
            this._message.HeaderText = "Сообщение";
            this._message.Name = "_message";
            this._message.ReadOnly = true;
            this._message.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._message.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._message.Width = 71;
            // 
            // _parameter
            // 
            this._parameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this._parameter.DataPropertyName = "_parameter";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._parameter.DefaultCellStyle = dataGridViewCellStyle14;
            this._parameter.HeaderText = "Параметр";
            this._parameter.Name = "_parameter";
            this._parameter.ReadOnly = true;
            this._parameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._parameter.Width = 64;
            // 
            // _openLogicJour
            // 
            this._openLogicJour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._openLogicJour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._openLogicJour.Location = new System.Drawing.Point(89, 351);
            this._openLogicJour.Name = "_openLogicJour";
            this._openLogicJour.Size = new System.Drawing.Size(75, 23);
            this._openLogicJour.TabIndex = 18;
            this._openLogicJour.Text = "Открыть";
            this._openLogicJour.UseVisualStyleBackColor = true;
            this._openLogicJour.Click += new System.EventHandler(this._readLogicJournalButton_Click);
            // 
            // _readLogicJournalButton
            // 
            this._readLogicJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readLogicJournalButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._readLogicJournalButton.Location = new System.Drawing.Point(8, 351);
            this._readLogicJournalButton.Name = "_readLogicJournalButton";
            this._readLogicJournalButton.Size = new System.Drawing.Size(75, 23);
            this._readLogicJournalButton.TabIndex = 18;
            this._readLogicJournalButton.Text = "Прочитать";
            this._readLogicJournalButton.UseVisualStyleBackColor = true;
            this._readLogicJournalButton.Click += new System.EventHandler(this._readLogicJournalButton_Click);
            // 
            // _progressBar
            // 
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(130, 20);
            // 
            // _statusLabel
            // 
            this._statusLabel.Font = new System.Drawing.Font("Calibri", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 21);
            this._statusLabel.Text = " ";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.statusStrip1.BackColor = System.Drawing.Color.White;
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 411);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(159, 26);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _closeBtn
            // 
            this._closeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._closeBtn.Location = new System.Drawing.Point(655, 0);
            this._closeBtn.Name = "_closeBtn";
            this._closeBtn.Size = new System.Drawing.Size(30, 20);
            this._closeBtn.TabIndex = 19;
            this._closeBtn.Text = "X";
            this._closeBtn.UseVisualStyleBackColor = true;
            this._closeBtn.Visible = false;
            this._closeBtn.Click += new System.EventHandler(this._closeBtn_Click);
            // 
            // _maximizeBtn
            // 
            this._maximizeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._maximizeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._maximizeBtn.Location = new System.Drawing.Point(625, 0);
            this._maximizeBtn.Name = "_maximizeBtn";
            this._maximizeBtn.Size = new System.Drawing.Size(30, 20);
            this._maximizeBtn.TabIndex = 20;
            this._maximizeBtn.Text = "[_]";
            this._maximizeBtn.UseVisualStyleBackColor = true;
            this._maximizeBtn.Visible = false;
            this._maximizeBtn.Click += new System.EventHandler(this._maximizeBtn_Click);
            // 
            // _openSysJour
            // 
            this._openSysJour.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._openSysJour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._openSysJour.Location = new System.Drawing.Point(89, 351);
            this._openSysJour.Name = "_openSysJour";
            this._openSysJour.Size = new System.Drawing.Size(75, 23);
            this._openSysJour.TabIndex = 0;
            this._openSysJour.Text = "Открыть";
            this._openSysJour.UseVisualStyleBackColor = true;
            this._openSysJour.Click += new System.EventHandler(this._openSysJour_Click);
            // 
            // _openSysXml
            // 
            this._openSysXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._openSysXml.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._openSysXml.Location = new System.Drawing.Point(170, 351);
            this._openSysXml.Name = "_openSysXml";
            this._openSysXml.Size = new System.Drawing.Size(126, 23);
            this._openSysXml.TabIndex = 25;
            this._openSysXml.Text = "Файл расшифровки";
            this._openSysXml.UseVisualStyleBackColor = true;
            this._openSysXml.Click += new System.EventHandler(this._openXml_Click);
            // 
            // KlJournalsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 437);
            this.Controls.Add(this._maximizeBtn);
            this.Controls.Add(this._closeBtn);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._journalTabControl);
            this.Name = "KlJournalsForm";
            this.Text = "Журналы";
            this.Load += new System.EventHandler(this.MLK_Journals_Load);
            this.Resize += new System.EventHandler(this.MlkJournals_Resize);
            this._journalTabControl.ResumeLayout(false);
            this._sysPage.ResumeLayout(false);
            this._sysPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._sysJournalGrid)).EndInit();
            this._logicPage.ResumeLayout(false);
            this._logicPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._logicJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _journalTabControl;
        private System.Windows.Forms.TabPage _sysPage;
        private System.Windows.Forms.TabPage _logicPage;
        private System.Windows.Forms.Button _readSysJournalButton;
        private System.Windows.Forms.Button _readLogicJournalButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.DataGridView _sysJournalGrid;
        private System.Windows.Forms.DataGridView _logicJournalGrid;
        private System.Windows.Forms.Button _clearSysJourBnt;
        private System.Windows.Forms.Button _clearLogicJourBtn;
        private System.Windows.Forms.Button _saveSysJour;
        private System.Windows.Forms.Button _saveLogicJour;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Label _countOfSysMes;
        private System.Windows.Forms.Label _countOfLogicMes;
        private System.Windows.Forms.Button _closeBtn;
        private System.Windows.Forms.Button _maximizeBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sysJournalDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sysJournalDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sysJournalTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sysJournalEvent;
        private System.Windows.Forms.DataGridViewTextBoxColumn _sysParameter;
        private System.Windows.Forms.Button _openXml;
        private System.Windows.Forms.DataGridViewTextBoxColumn _number;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dayOfWeek;
        private System.Windows.Forms.DataGridViewTextBoxColumn _date;
        private System.Windows.Forms.DataGridViewTextBoxColumn _time;
        private System.Windows.Forms.DataGridViewTextBoxColumn _message;
        private System.Windows.Forms.DataGridViewTextBoxColumn _parameter;
        private System.Windows.Forms.Button _openLogicJour;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button _openSysJour;
        private System.Windows.Forms.Button _openSysXml;
    }
}