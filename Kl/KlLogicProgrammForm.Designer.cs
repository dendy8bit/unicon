﻿namespace BEMN.Kl
{
    partial class KlLogicProgramm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Ah = 0x00");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Al = 0x00");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("AXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Ah = 0x00");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Al = 0x00");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("AXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("EAXhigh = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Ah = 0x00");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Al = 0x00");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("AXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Ah = 0x00");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Al = 0x00");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("AXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("EXlow = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode13});
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("RAX = 0x0000000000000000", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode14});
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Bh = 0x00");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Bl = 0x00");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("BXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Bh = 0x00");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Bl = 0x00");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("BXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("EBXhigh = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode18,
            treeNode21});
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Bh = 0x00");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Bl = 0x00");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("BXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Bh = 0x00");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Bl = 0x00");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("BXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("EBXlow = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode25,
            treeNode28});
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("RBX = 0x0000000000000000", new System.Windows.Forms.TreeNode[] {
            treeNode22,
            treeNode29});
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Ch = 0x00");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Cl = 0x00");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("CXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode31,
            treeNode32});
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("Ch = 0x00");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Cl = 0x00");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("CXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode34,
            treeNode35});
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("ECXhigh = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode33,
            treeNode36});
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("Ch = 0x00");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("Cl = 0x00");
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("CXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode38,
            treeNode39});
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("Ch = 0x00");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("Cl = 0x00");
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("CXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode41,
            treeNode42});
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("ECXlow = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode40,
            treeNode43});
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("RCX = 0x0000000000000000", new System.Windows.Forms.TreeNode[] {
            treeNode37,
            treeNode44});
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("Dh = 0x00");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("Dl = 0x00");
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("DXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode46,
            treeNode47});
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("Dh = 0x00");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("Dl = 0x00");
            System.Windows.Forms.TreeNode treeNode51 = new System.Windows.Forms.TreeNode("DXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode49,
            treeNode50});
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("EDXhigh = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode48,
            treeNode51});
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("Dh = 0x00");
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("Dl = 0x00");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("DXhigh = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode53,
            treeNode54});
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("Dh = 0x00");
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("Dl = 0x00");
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("DXlow = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode56,
            treeNode57});
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("EDXlow = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode55,
            treeNode58});
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("RDX = 0x0000000000000000", new System.Windows.Forms.TreeNode[] {
            treeNode52,
            treeNode59});
            System.Windows.Forms.TreeNode treeNode61 = new System.Windows.Forms.TreeNode("SP = 0x0000");
            System.Windows.Forms.TreeNode treeNode62 = new System.Windows.Forms.TreeNode("DP = 0x0000");
            System.Windows.Forms.TreeNode treeNode63 = new System.Windows.Forms.TreeNode("SC = 0x00");
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("LP = 0x00");
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("SCLP = 0x0000", new System.Windows.Forms.TreeNode[] {
            treeNode63,
            treeNode64});
            System.Windows.Forms.TreeNode treeNode66 = new System.Windows.Forms.TreeNode("SREG = 0x0000");
            System.Windows.Forms.TreeNode treeNode67 = new System.Windows.Forms.TreeNode("DEBUG = 0x0000");
            System.Windows.Forms.TreeNode treeNode68 = new System.Windows.Forms.TreeNode("CS = 0x0000");
            System.Windows.Forms.TreeNode treeNode69 = new System.Windows.Forms.TreeNode("IP = 0x0000");
            System.Windows.Forms.TreeNode treeNode70 = new System.Windows.Forms.TreeNode("CSIP = 0x00000000", new System.Windows.Forms.TreeNode[] {
            treeNode68,
            treeNode69});
            System.Windows.Forms.TreeNode treeNode71 = new System.Windows.Forms.TreeNode("COMMAND = 0x0000");
            this._openLogicProgDlg = new System.Windows.Forms.OpenFileDialog();
            this._saveLogicProgDlg = new System.Windows.Forms.SaveFileDialog();
            this.miniToolStrip = new System.Windows.Forms.StatusStrip();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._stateLableProgFileConst = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._stateProgressBar2 = new System.Windows.Forms.ProgressBar();
            this._readFileBtn2 = new System.Windows.Forms.Button();
            this._writeProgramBtn2 = new System.Windows.Forms.Button();
            this._saveInFileBtn2 = new System.Windows.Forms.Button();
            this._fileForWriting2 = new System.Windows.Forms.TextBox();
            this.queryCheckBox = new System.Windows.Forms.CheckBox();
            this._resetBtn = new System.Windows.Forms.Button();
            this._buttonsGroup = new System.Windows.Forms.GroupBox();
            this._stateGroup = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ledControl6 = new BEMN.Forms.LedControl();
            this.ledControl5 = new BEMN.Forms.LedControl();
            this.ledControl4 = new BEMN.Forms.LedControl();
            this.ledControl3 = new BEMN.Forms.LedControl();
            this.ledControl2 = new BEMN.Forms.LedControl();
            this.ledControl1 = new BEMN.Forms.LedControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._regTreeView = new System.Windows.Forms.TreeView();
            this._readRegBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._stateLableProgramFile = new System.Windows.Forms.Label();
            this._stateProgrBar = new System.Windows.Forms.ProgressBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._verscompl = new System.Windows.Forms.Label();
            this._vstamp = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this._saveInFileBtn = new System.Windows.Forms.Button();
            this._writeProgrammBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this._readFileBtn = new System.Windows.Forms.Button();
            this._fileForWriting = new System.Windows.Forms.MaskedTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._stateGroup.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _openLogicProgDlg
            // 
            this._openLogicProgDlg.Filter = "File BIN | *.bin|All files | *.*";
            // 
            // _saveLogicProgDlg
            // 
            this._saveLogicProgDlg.Filter = "File BIN|*.bin";
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.Location = new System.Drawing.Point(651, 23);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(649, 22);
            this.miniToolStrip.TabIndex = 6;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 562);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.queryCheckBox);
            this.tabPage3.Controls.Add(this._resetBtn);
            this.tabPage3.Controls.Add(this._buttonsGroup);
            this.tabPage3.Controls.Add(this._stateGroup);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(579, 536);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Программирование";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._stateLableProgFileConst);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this._stateProgressBar2);
            this.groupBox4.Controls.Add(this._readFileBtn2);
            this.groupBox4.Controls.Add(this._writeProgramBtn2);
            this.groupBox4.Controls.Add(this._saveInFileBtn2);
            this.groupBox4.Controls.Add(this._fileForWriting2);
            this.groupBox4.Location = new System.Drawing.Point(8, 216);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(280, 136);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Константы логической программы";
            // 
            // _stateLableProgFileConst
            // 
            this._stateLableProgFileConst.AutoSize = true;
            this._stateLableProgFileConst.Location = new System.Drawing.Point(6, 110);
            this._stateLableProgFileConst.Name = "_stateLableProgFileConst";
            this._stateLableProgFileConst.Size = new System.Drawing.Size(0, 13);
            this._stateLableProgFileConst.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Путь к файлу";
            // 
            // _stateProgressBar2
            // 
            this._stateProgressBar2.Location = new System.Drawing.Point(7, 88);
            this._stateProgressBar2.Name = "_stateProgressBar2";
            this._stateProgressBar2.Size = new System.Drawing.Size(265, 19);
            this._stateProgressBar2.TabIndex = 4;
            // 
            // _readFileBtn2
            // 
            this._readFileBtn2.Location = new System.Drawing.Point(197, 59);
            this._readFileBtn2.Name = "_readFileBtn2";
            this._readFileBtn2.Size = new System.Drawing.Size(75, 23);
            this._readFileBtn2.TabIndex = 3;
            this._readFileBtn2.Text = "Обзор";
            this._readFileBtn2.UseVisualStyleBackColor = true;
            this._readFileBtn2.Click += new System.EventHandler(this.ReadFileClick2);
            // 
            // _writeProgramBtn2
            // 
            this._writeProgramBtn2.Enabled = false;
            this._writeProgramBtn2.Location = new System.Drawing.Point(116, 59);
            this._writeProgramBtn2.Name = "_writeProgramBtn2";
            this._writeProgramBtn2.Size = new System.Drawing.Size(75, 23);
            this._writeProgramBtn2.TabIndex = 2;
            this._writeProgramBtn2.Text = "Записать";
            this._writeProgramBtn2.UseVisualStyleBackColor = true;
            this._writeProgramBtn2.Click += new System.EventHandler(this.WriteProgrammInDeviceClick2);
            // 
            // _saveInFileBtn2
            // 
            this._saveInFileBtn2.Location = new System.Drawing.Point(6, 59);
            this._saveInFileBtn2.Name = "_saveInFileBtn2";
            this._saveInFileBtn2.Size = new System.Drawing.Size(75, 23);
            this._saveInFileBtn2.TabIndex = 1;
            this._saveInFileBtn2.Text = "Прочитать";
            this._saveInFileBtn2.UseVisualStyleBackColor = true;
            this._saveInFileBtn2.Click += new System.EventHandler(this.SaveInFileBtnClick2);
            // 
            // _fileForWriting2
            // 
            this._fileForWriting2.Enabled = false;
            this._fileForWriting2.Location = new System.Drawing.Point(6, 33);
            this._fileForWriting2.Name = "_fileForWriting2";
            this._fileForWriting2.Size = new System.Drawing.Size(260, 20);
            this._fileForWriting2.TabIndex = 0;
            // 
            // queryCheckBox
            // 
            this.queryCheckBox.AutoSize = true;
            this.queryCheckBox.Checked = true;
            this.queryCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.queryCheckBox.Location = new System.Drawing.Point(14, 514);
            this.queryCheckBox.Name = "queryCheckBox";
            this.queryCheckBox.Size = new System.Drawing.Size(68, 17);
            this.queryCheckBox.TabIndex = 13;
            this.queryCheckBox.Text = "Обмены";
            this.queryCheckBox.UseVisualStyleBackColor = true;
            this.queryCheckBox.CheckedChanged += new System.EventHandler(this.QueryCheckBoxCheckedChanged);
            // 
            // _resetBtn
            // 
            this._resetBtn.Location = new System.Drawing.Point(151, 511);
            this._resetBtn.Name = "_resetBtn";
            this._resetBtn.Size = new System.Drawing.Size(137, 22);
            this._resetBtn.TabIndex = 11;
            this._resetBtn.Text = "Сброс";
            this._resetBtn.UseVisualStyleBackColor = true;
            this._resetBtn.Click += new System.EventHandler(this._resetBtn_Click);
            // 
            // _buttonsGroup
            // 
            this._buttonsGroup.Location = new System.Drawing.Point(151, 358);
            this._buttonsGroup.Name = "_buttonsGroup";
            this._buttonsGroup.Size = new System.Drawing.Size(137, 147);
            this._buttonsGroup.TabIndex = 10;
            this._buttonsGroup.TabStop = false;
            this._buttonsGroup.Text = "Кнопки режимов";
            // 
            // _stateGroup
            // 
            this._stateGroup.Controls.Add(this.label7);
            this._stateGroup.Controls.Add(this.label6);
            this._stateGroup.Controls.Add(this.label5);
            this._stateGroup.Controls.Add(this.label4);
            this._stateGroup.Controls.Add(this.label3);
            this._stateGroup.Controls.Add(this.label2);
            this._stateGroup.Controls.Add(this.ledControl6);
            this._stateGroup.Controls.Add(this.ledControl5);
            this._stateGroup.Controls.Add(this.ledControl4);
            this._stateGroup.Controls.Add(this.ledControl3);
            this._stateGroup.Controls.Add(this.ledControl2);
            this._stateGroup.Controls.Add(this.ledControl1);
            this._stateGroup.Location = new System.Drawing.Point(8, 358);
            this._stateGroup.Name = "_stateGroup";
            this._stateGroup.Size = new System.Drawing.Size(137, 147);
            this._stateGroup.TabIndex = 9;
            this._stateGroup.TabStop = false;
            this._stateGroup.Text = "Состояние логики";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Ошибка";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Обновление";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Остановлена";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Отладка";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Работает";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Стартовое";
            // 
            // ledControl6
            // 
            this.ledControl6.Location = new System.Drawing.Point(6, 124);
            this.ledControl6.Name = "ledControl6";
            this.ledControl6.Size = new System.Drawing.Size(13, 13);
            this.ledControl6.State = BEMN.Forms.LedState.Off;
            this.ledControl6.TabIndex = 5;
            // 
            // ledControl5
            // 
            this.ledControl5.Location = new System.Drawing.Point(6, 103);
            this.ledControl5.Name = "ledControl5";
            this.ledControl5.Size = new System.Drawing.Size(13, 13);
            this.ledControl5.State = BEMN.Forms.LedState.Off;
            this.ledControl5.TabIndex = 4;
            // 
            // ledControl4
            // 
            this.ledControl4.Location = new System.Drawing.Point(6, 82);
            this.ledControl4.Name = "ledControl4";
            this.ledControl4.Size = new System.Drawing.Size(13, 13);
            this.ledControl4.State = BEMN.Forms.LedState.Off;
            this.ledControl4.TabIndex = 3;
            // 
            // ledControl3
            // 
            this.ledControl3.Location = new System.Drawing.Point(6, 61);
            this.ledControl3.Name = "ledControl3";
            this.ledControl3.Size = new System.Drawing.Size(13, 13);
            this.ledControl3.State = BEMN.Forms.LedState.Off;
            this.ledControl3.TabIndex = 2;
            // 
            // ledControl2
            // 
            this.ledControl2.Location = new System.Drawing.Point(6, 40);
            this.ledControl2.Name = "ledControl2";
            this.ledControl2.Size = new System.Drawing.Size(13, 13);
            this.ledControl2.State = BEMN.Forms.LedState.Off;
            this.ledControl2.TabIndex = 1;
            // 
            // ledControl1
            // 
            this.ledControl1.Location = new System.Drawing.Point(6, 19);
            this.ledControl1.Name = "ledControl1";
            this.ledControl1.Size = new System.Drawing.Size(13, 13);
            this.ledControl1.State = BEMN.Forms.LedState.Off;
            this.ledControl1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._regTreeView);
            this.groupBox3.Controls.Add(this._readRegBtn);
            this.groupBox3.Location = new System.Drawing.Point(294, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(282, 381);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Регистры логической программы";
            // 
            // _regTreeView
            // 
            this._regTreeView.Dock = System.Windows.Forms.DockStyle.Top;
            this._regTreeView.Location = new System.Drawing.Point(3, 16);
            this._regTreeView.Name = "_regTreeView";
            treeNode1.Name = "_Ahigh1";
            treeNode1.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode1.Tag = "1";
            treeNode1.Text = "Ah = 0x00";
            treeNode2.Name = "_Alow1";
            treeNode2.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode2.Tag = "2";
            treeNode2.Text = "Al = 0x00";
            treeNode3.Name = "_AXhigh1";
            treeNode3.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode3.Tag = "9";
            treeNode3.Text = "AXhigh = 0x0000";
            treeNode4.Name = "_Ahigh2";
            treeNode4.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode4.Tag = "3";
            treeNode4.Text = "Ah = 0x00";
            treeNode5.Name = "_Alow2";
            treeNode5.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode5.Tag = "4";
            treeNode5.Text = "Al = 0x00";
            treeNode6.Name = "_AXlow1";
            treeNode6.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode6.Tag = "10";
            treeNode6.Text = "AXlow = 0x0000";
            treeNode7.Name = "_eAXhigh";
            treeNode7.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode7.Tag = "13";
            treeNode7.Text = "EAXhigh = 0x00000000";
            treeNode8.Name = "_Ahigh3";
            treeNode8.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode8.Tag = "5";
            treeNode8.Text = "Ah = 0x00";
            treeNode9.Name = "_Alow3";
            treeNode9.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode9.Tag = "6";
            treeNode9.Text = "Al = 0x00";
            treeNode10.Name = "_AXhigh2";
            treeNode10.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode10.Tag = "11";
            treeNode10.Text = "AXhigh = 0x0000";
            treeNode11.Name = "_Ahigh4";
            treeNode11.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode11.Tag = "7";
            treeNode11.Text = "Ah = 0x00";
            treeNode12.Name = "_Alow4";
            treeNode12.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode12.Tag = "8";
            treeNode12.Text = "Al = 0x00";
            treeNode13.Name = "_AXlow2";
            treeNode13.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode13.Tag = "12";
            treeNode13.Text = "AXlow = 0x0000";
            treeNode14.Name = "_eAXlow";
            treeNode14.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode14.Tag = "14";
            treeNode14.Text = "EXlow = 0x00000000";
            treeNode15.Name = "_regAX";
            treeNode15.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode15.Tag = "15";
            treeNode15.Text = "RAX = 0x0000000000000000";
            treeNode16.Name = "_Bhigh1";
            treeNode16.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode16.Tag = "16";
            treeNode16.Text = "Bh = 0x00";
            treeNode17.Name = "_Blow1";
            treeNode17.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode17.Tag = "17";
            treeNode17.Text = "Bl = 0x00";
            treeNode18.Name = "_BXhigh1";
            treeNode18.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode18.Tag = "24";
            treeNode18.Text = "BXlow = 0x0000";
            treeNode19.Name = "_Bhigh2";
            treeNode19.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode19.Tag = "18";
            treeNode19.Text = "Bh = 0x00";
            treeNode20.Name = "_Blow2";
            treeNode20.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode20.Tag = "19";
            treeNode20.Text = "Bl = 0x00";
            treeNode21.Name = "_BXlow1";
            treeNode21.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode21.Tag = "25";
            treeNode21.Text = "BXlow = 0x0000";
            treeNode22.Name = "_eBXhigh";
            treeNode22.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode22.Tag = "28";
            treeNode22.Text = "EBXhigh = 0x00000000";
            treeNode23.Name = "_Bhigh3";
            treeNode23.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode23.Tag = "20";
            treeNode23.Text = "Bh = 0x00";
            treeNode24.Name = "_Blow3";
            treeNode24.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode24.Tag = "21";
            treeNode24.Text = "Bl = 0x00";
            treeNode25.Name = "_BXhigh2";
            treeNode25.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode25.Tag = "26";
            treeNode25.Text = "BXhigh = 0x0000";
            treeNode26.Name = "_Bhigh4";
            treeNode26.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode26.Tag = "22";
            treeNode26.Text = "Bh = 0x00";
            treeNode27.Name = "_Blow4";
            treeNode27.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode27.Tag = "23";
            treeNode27.Text = "Bl = 0x00";
            treeNode28.Name = "_BXlow2";
            treeNode28.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode28.Tag = "27";
            treeNode28.Text = "BXlow = 0x0000";
            treeNode29.Name = "_eBXlow";
            treeNode29.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode29.Tag = "29";
            treeNode29.Text = "EBXlow = 0x00000000";
            treeNode30.Name = "_regBX";
            treeNode30.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode30.Tag = "30";
            treeNode30.Text = "RBX = 0x0000000000000000";
            treeNode31.Name = "_Chigh1";
            treeNode31.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode31.Tag = "31";
            treeNode31.Text = "Ch = 0x00";
            treeNode32.Name = "_Clow1";
            treeNode32.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode32.Tag = "32";
            treeNode32.Text = "Cl = 0x00";
            treeNode33.Name = "_CXhigh1";
            treeNode33.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode33.Tag = "39";
            treeNode33.Text = "CXhigh = 0x0000";
            treeNode34.Name = "_Chigh2";
            treeNode34.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode34.Tag = "33";
            treeNode34.Text = "Ch = 0x00";
            treeNode35.Name = "_Clow2";
            treeNode35.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode35.Tag = "34";
            treeNode35.Text = "Cl = 0x00";
            treeNode36.Name = "_CXlow1";
            treeNode36.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode36.Tag = "40";
            treeNode36.Text = "CXlow = 0x0000";
            treeNode37.Name = "_eCXhigh";
            treeNode37.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode37.Tag = "43";
            treeNode37.Text = "ECXhigh = 0x00000000";
            treeNode38.Name = "_Chigh3";
            treeNode38.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode38.Tag = "35";
            treeNode38.Text = "Ch = 0x00";
            treeNode39.Name = "_Clow3";
            treeNode39.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode39.Tag = "36";
            treeNode39.Text = "Cl = 0x00";
            treeNode40.Name = "_CXhigh2";
            treeNode40.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode40.Tag = "41";
            treeNode40.Text = "CXhigh = 0x0000";
            treeNode41.Name = "_Chigh4";
            treeNode41.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode41.Tag = "37";
            treeNode41.Text = "Ch = 0x00";
            treeNode42.Name = "_Clow4";
            treeNode42.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode42.Tag = "38";
            treeNode42.Text = "Cl = 0x00";
            treeNode43.Name = "_CXlow2";
            treeNode43.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode43.Tag = "42";
            treeNode43.Text = "CXlow = 0x0000";
            treeNode44.Name = "_eCXlow";
            treeNode44.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode44.Tag = "44";
            treeNode44.Text = "ECXlow = 0x00000000";
            treeNode45.Name = "_regCX";
            treeNode45.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode45.Tag = "45";
            treeNode45.Text = "RCX = 0x0000000000000000";
            treeNode46.Name = "_Dhigh1";
            treeNode46.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode46.Tag = "46";
            treeNode46.Text = "Dh = 0x00";
            treeNode47.Name = "_Dlow1";
            treeNode47.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode47.Tag = "47";
            treeNode47.Text = "Dl = 0x00";
            treeNode48.Name = "_DXhigh1";
            treeNode48.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode48.Tag = "54";
            treeNode48.Text = "DXhigh = 0x0000";
            treeNode49.Name = "_Dhigh2";
            treeNode49.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode49.Tag = "48";
            treeNode49.Text = "Dh = 0x00";
            treeNode50.Name = "_Dlow2";
            treeNode50.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode50.Tag = "49";
            treeNode50.Text = "Dl = 0x00";
            treeNode51.Name = "_DXlow1";
            treeNode51.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode51.Tag = "55";
            treeNode51.Text = "DXlow = 0x0000";
            treeNode52.Name = "_eDXhigh";
            treeNode52.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode52.Tag = "58";
            treeNode52.Text = "EDXhigh = 0x00000000";
            treeNode53.Name = "_Dhigh3";
            treeNode53.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode53.Tag = "50";
            treeNode53.Text = "Dh = 0x00";
            treeNode54.Name = "_Dlow3";
            treeNode54.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode54.Tag = "51";
            treeNode54.Text = "Dl = 0x00";
            treeNode55.Name = "_DXhigh2";
            treeNode55.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode55.Tag = "56";
            treeNode55.Text = "DXhigh = 0x0000";
            treeNode56.Name = "_Dhigh4";
            treeNode56.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode56.Tag = "52";
            treeNode56.Text = "Dh = 0x00";
            treeNode57.Name = "_Dlow4";
            treeNode57.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode57.Tag = "53";
            treeNode57.Text = "Dl = 0x00";
            treeNode58.Name = "_DXlow2";
            treeNode58.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode58.Tag = "57";
            treeNode58.Text = "DXlow = 0x0000";
            treeNode59.Name = "_eDXlow";
            treeNode59.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode59.Tag = "59";
            treeNode59.Text = "EDXlow = 0x00000000";
            treeNode60.Name = "_regDX";
            treeNode60.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode60.Tag = "60";
            treeNode60.Text = "RDX = 0x0000000000000000";
            treeNode61.Name = "_regSP";
            treeNode61.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode61.Tag = "61";
            treeNode61.Text = "SP = 0x0000";
            treeNode62.Name = "_regDP";
            treeNode62.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode62.Tag = "62";
            treeNode62.Text = "DP = 0x0000";
            treeNode63.Name = "_SC";
            treeNode63.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode63.Tag = "63";
            treeNode63.Text = "SC = 0x00";
            treeNode64.Name = "_LP";
            treeNode64.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode64.Tag = "64";
            treeNode64.Text = "LP = 0x00";
            treeNode65.Name = "_regSCLP";
            treeNode65.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode65.Tag = "65";
            treeNode65.Text = "SCLP = 0x0000";
            treeNode66.Name = "_regSREG";
            treeNode66.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode66.Tag = "66";
            treeNode66.Text = "SREG = 0x0000";
            treeNode67.Name = "_regDEBUG";
            treeNode67.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode67.Tag = "67";
            treeNode67.Text = "DEBUG = 0x0000";
            treeNode68.Name = "_CS";
            treeNode68.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode68.Tag = "68";
            treeNode68.Text = "CS = 0x0000";
            treeNode69.Name = "_IP";
            treeNode69.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode69.Tag = "69";
            treeNode69.Text = "IP = 0x0000";
            treeNode70.Name = "_regCSIP";
            treeNode70.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode70.Tag = "70";
            treeNode70.Text = "CSIP = 0x00000000";
            treeNode71.Name = "_regCOMMAND";
            treeNode71.NodeFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            treeNode71.Tag = "71";
            treeNode71.Text = "COMMAND = 0x0000";
            this._regTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode15,
            treeNode30,
            treeNode45,
            treeNode60,
            treeNode61,
            treeNode62,
            treeNode65,
            treeNode66,
            treeNode67,
            treeNode70,
            treeNode71});
            this._regTreeView.Size = new System.Drawing.Size(276, 333);
            this._regTreeView.TabIndex = 0;
            this._regTreeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this._regTreeView_BeforeCollapse);
            this._regTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this._regTreeView_BeforeExpand);
            this._regTreeView.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this._regTreeView_NodeMouseDoubleClick);
            this._regTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this._regTreeView_MouseDown);
            // 
            // _readRegBtn
            // 
            this._readRegBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._readRegBtn.Location = new System.Drawing.Point(3, 355);
            this._readRegBtn.Name = "_readRegBtn";
            this._readRegBtn.Size = new System.Drawing.Size(276, 23);
            this._readRegBtn.TabIndex = 8;
            this._readRegBtn.Text = "Отобразить считанные регистры";
            this._readRegBtn.UseVisualStyleBackColor = true;
            this._readRegBtn.Click += new System.EventHandler(this._readRegBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._stateLableProgramFile);
            this.groupBox1.Controls.Add(this._stateProgrBar);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this._saveInFileBtn);
            this.groupBox1.Controls.Add(this._writeProgrammBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._readFileBtn);
            this.groupBox1.Controls.Add(this._fileForWriting);
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(280, 207);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Логическая программа";
            // 
            // _stateLableProgramFile
            // 
            this._stateLableProgramFile.AutoSize = true;
            this._stateLableProgramFile.Location = new System.Drawing.Point(6, 110);
            this._stateLableProgramFile.Name = "_stateLableProgramFile";
            this._stateLableProgramFile.Size = new System.Drawing.Size(0, 13);
            this._stateLableProgramFile.TabIndex = 6;
            // 
            // _stateProgrBar
            // 
            this._stateProgrBar.Location = new System.Drawing.Point(6, 89);
            this._stateProgrBar.Name = "_stateProgrBar";
            this._stateProgrBar.Size = new System.Drawing.Size(268, 18);
            this._stateProgrBar.Step = 1;
            this._stateProgrBar.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._verscompl);
            this.groupBox2.Controls.Add(this._vstamp);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 139);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 62);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Информация о файле";
            // 
            // _verscompl
            // 
            this._verscompl.AutoSize = true;
            this._verscompl.Location = new System.Drawing.Point(96, 38);
            this._verscompl.Name = "_verscompl";
            this._verscompl.Size = new System.Drawing.Size(0, 13);
            this._verscompl.TabIndex = 3;
            // 
            // _vstamp
            // 
            this._vstamp.AutoSize = true;
            this._vstamp.Location = new System.Drawing.Point(96, 16);
            this._vstamp.Name = "_vstamp";
            this._vstamp.Size = new System.Drawing.Size(0, 13);
            this._vstamp.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "verscompl";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "vstamp";
            // 
            // _saveInFileBtn
            // 
            this._saveInFileBtn.Location = new System.Drawing.Point(6, 60);
            this._saveInFileBtn.Name = "_saveInFileBtn";
            this._saveInFileBtn.Size = new System.Drawing.Size(75, 23);
            this._saveInFileBtn.TabIndex = 4;
            this._saveInFileBtn.Text = "Прочитать";
            this._saveInFileBtn.UseVisualStyleBackColor = true;
            this._saveInFileBtn.Click += new System.EventHandler(this.SaveInFileBtnClick);
            // 
            // _writeProgrammBtn
            // 
            this._writeProgrammBtn.Enabled = false;
            this._writeProgrammBtn.Location = new System.Drawing.Point(118, 60);
            this._writeProgrammBtn.Name = "_writeProgrammBtn";
            this._writeProgrammBtn.Size = new System.Drawing.Size(75, 23);
            this._writeProgrammBtn.TabIndex = 3;
            this._writeProgrammBtn.Text = "Записать";
            this._writeProgrammBtn.UseVisualStyleBackColor = true;
            this._writeProgrammBtn.Click += new System.EventHandler(this.WriteProgrammInDeviceClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Путь к файлу";
            // 
            // _readFileBtn
            // 
            this._readFileBtn.Location = new System.Drawing.Point(199, 60);
            this._readFileBtn.Name = "_readFileBtn";
            this._readFileBtn.Size = new System.Drawing.Size(75, 23);
            this._readFileBtn.TabIndex = 1;
            this._readFileBtn.Text = "Обзор";
            this._readFileBtn.UseVisualStyleBackColor = true;
            this._readFileBtn.Click += new System.EventHandler(this.ReadFileClick);
            // 
            // _fileForWriting
            // 
            this._fileForWriting.AccessibleDescription = "";
            this._fileForWriting.AccessibleName = "";
            this._fileForWriting.Enabled = false;
            this._fileForWriting.Location = new System.Drawing.Point(8, 33);
            this._fileForWriting.Name = "_fileForWriting";
            this._fileForWriting.Size = new System.Drawing.Size(266, 20);
            this._fileForWriting.TabIndex = 0;
            // 
            // KlLogicProgramm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 562);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "KlLogicProgramm";
            this.Text = "Логическая программа";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.KlLogicProgrammFormClosing);
            this.Load += new System.EventHandler(this.KlLogicProgrammLoad);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._stateGroup.ResumeLayout(false);
            this._stateGroup.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog _openLogicProgDlg;
        private System.Windows.Forms.SaveFileDialog _saveLogicProgDlg;
        private System.Windows.Forms.StatusStrip miniToolStrip;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label _verscompl;
        private System.Windows.Forms.Label _vstamp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button _resetBtn;
        private System.Windows.Forms.GroupBox _buttonsGroup;
        private System.Windows.Forms.GroupBox _stateGroup;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Forms.LedControl ledControl6;
        private Forms.LedControl ledControl5;
        private Forms.LedControl ledControl4;
        private Forms.LedControl ledControl3;
        private Forms.LedControl ledControl2;
        private Forms.LedControl ledControl1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TreeView _regTreeView;
        private System.Windows.Forms.Button _readRegBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ProgressBar _stateProgrBar;
        private System.Windows.Forms.Button _saveInFileBtn;
        private System.Windows.Forms.Button _writeProgrammBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _readFileBtn;
        private System.Windows.Forms.MaskedTextBox _fileForWriting;
        private System.Windows.Forms.CheckBox queryCheckBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox _fileForWriting2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ProgressBar _stateProgressBar2;
        private System.Windows.Forms.Button _readFileBtn2;
        private System.Windows.Forms.Button _writeProgramBtn2;
        private System.Windows.Forms.Button _saveInFileBtn2;
        private System.Windows.Forms.Label _stateLableProgFileConst;
        private System.Windows.Forms.Label _stateLableProgramFile;
    }
}