using System.ComponentModel;

namespace BEMN.MR300
{
    
    public class FeatureTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.FeatureI);
        }
    }
    public class DiskretAutomaticTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.DiskretAutomatic);
        }
    }
    public class Rele2TypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Rele2);
        }
    }
    public class ReleTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Rele);
        }
    }
    public class AllSignalsTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.All);
        }
    }
    public class CratTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Crat);
        }
    }
    public class ControlTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Control);
        }
    }
    public class ForbiddenTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Forbidden);
        }
    }
    public class BlinkerTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(new string[] { "�����������", "�������" });
        }
    }
    public class ModeTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Modes);
        }
    }
    public class ModeLightTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ModesLight);
        }
    }
    public class ExternalDefensesTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.ExternalDefense);
        }
    }
    public class LogicTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Logic);
        }
    }
    public class Mode2TypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Modes2);
        }
    }

    public class DiskretTypeConverter : StringConverter
    {
        /// <summary>
        /// ����� ������������� ����� �� ������
        /// </summary>
        public override bool GetStandardValuesSupported(
          ITypeDescriptorContext context)
        {
            return true;
        }

        /// <summary>
        /// ... � ������ �� ������
        /// </summary>
        public override bool GetStandardValuesExclusive(
          ITypeDescriptorContext context)
        {
            // false - ����� ������� �������
            // true - ������ ����� �� ������
            return true;
        }

        /// <summary>
        /// � ��� � ������
        /// </summary>
        public override StandardValuesCollection GetStandardValues(
          ITypeDescriptorContext context)
        {
            // ���������� ������ ����� �� �������� ���������
            // (���� ������, �������� � �.�.)
            return new StandardValuesCollection(Strings.Diskret);
        }
    }
  
    

}