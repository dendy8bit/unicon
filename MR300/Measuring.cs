using System;

namespace BEMN.MR300
{
    public class Measuring
    {
        public static double GetI(ushort value, ushort KoeffNPTT, bool phaseTok)
        {
            int b;
            b = phaseTok ? 64 : 8;
            return (double)((double)b * (double)value / (double)0x10000 * (double)KoeffNPTT);
        }

        public static double GetU(ushort value, double Koeff)
        {
            return (double)((double)value / (double)0x100 * (double)Koeff);
        }

        public static double GetF(ushort value)
        {
            return (double)((double)value / (double)0x100);
        }

        public static ulong GetTime(ushort value)
        {
            //return value < 32768 ? (ulong)value * 10 : ((ulong)value - 32768) * 100;
            return (ulong)value * 10;
        }

        public static double GetConstraint(ushort value, ConstraintKoefficient koeff)
        {
            double temp = (double)value * (int)koeff / 65535;
            return Math.Floor(temp + 0.5) / 100;
        }

        public static ushort SetConstraint(double value, ConstraintKoefficient koeff)
        {
            return (ushort)(value * 65535 * 100 / (int)koeff);
        }

        public static ushort SetTime(ulong value)
        {
            return (ushort)(value / 10);
            //return value < 327680 ? (ushort)(value / 10) : (ushort)(value / 100 + 32768);
        }
    }
}