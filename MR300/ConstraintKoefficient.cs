namespace BEMN.MR300
{
    public enum ConstraintKoefficient
    {
        K_25600 = 25600,
        K_10000 = 10000,
        K_6400 = 6400,
        K_4000 = 4002,
        K_800 = 800,
        K_500 = 500,
        K_Undefine
    };
}