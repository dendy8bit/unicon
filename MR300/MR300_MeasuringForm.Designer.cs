using BEMN.Forms.Old;

namespace BEMN.MR300
{
    partial class MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasuringForm));
            this._images = new System.Windows.Forms.ImageList(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._I2Box = new System.Windows.Forms.TextBox();
            this._I1Box = new System.Windows.Forms.TextBox();
            this._I0Box = new System.Windows.Forms.TextBox();
            this._IgBox = new System.Windows.Forms.TextBox();
            this._IcBox = new System.Windows.Forms.TextBox();
            this._IbBox = new System.Windows.Forms.TextBox();
            this._IaBox = new System.Windows.Forms.TextBox();
            this._IoBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._conditionLed8 = new BEMN.Forms.LedControl();
            this._conditionLed7 = new BEMN.Forms.LedControl();
            this._conditionLed6 = new BEMN.Forms.LedControl();
            this._conditionLed5 = new BEMN.Forms.LedControl();
            this._conditionLed4 = new BEMN.Forms.LedControl();
            this._conditionLed3 = new BEMN.Forms.LedControl();
            this._conditionLed2 = new BEMN.Forms.LedControl();
            this._conditionLed1 = new BEMN.Forms.LedControl();
            this._releLed8 = new BEMN.Forms.LedControl();
            this._releLed7 = new BEMN.Forms.LedControl();
            this._releLed6 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._releLed5 = new BEMN.Forms.LedControl();
            this.label13 = new System.Windows.Forms.Label();
            this._releLed4 = new BEMN.Forms.LedControl();
            this.label14 = new System.Windows.Forms.Label();
            this._releLed3 = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._releLed2 = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._releLed1 = new BEMN.Forms.LedControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._inLed8 = new BEMN.Forms.LedControl();
            this.label2 = new System.Windows.Forms.Label();
            this._inLed7 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this._inLed6 = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._inLed5 = new BEMN.Forms.LedControl();
            this.label5 = new System.Windows.Forms.Label();
            this._inLed4 = new BEMN.Forms.LedControl();
            this.label6 = new System.Windows.Forms.Label();
            this._inLed3 = new BEMN.Forms.LedControl();
            this.label7 = new System.Windows.Forms.Label();
            this._inLed2 = new BEMN.Forms.LedControl();
            this.label8 = new System.Windows.Forms.Label();
            this._inLed1 = new BEMN.Forms.LedControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this._defenseLed8 = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._defenseLed7 = new BEMN.Forms.LedControl();
            this._defenseLed6 = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._defenseLed5 = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._defenseLed4 = new BEMN.Forms.LedControl();
            this.label22 = new System.Windows.Forms.Label();
            this._defenseLed3 = new BEMN.Forms.LedControl();
            this.label23 = new System.Windows.Forms.Label();
            this._defenseLed2 = new BEMN.Forms.LedControl();
            this.label24 = new System.Windows.Forms.Label();
            this._defenseLed1 = new BEMN.Forms.LedControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._crcLed8 = new BEMN.Forms.LedControl();
            this.label25 = new System.Windows.Forms.Label();
            this._crcLed7 = new BEMN.Forms.LedControl();
            this.label26 = new System.Windows.Forms.Label();
            this._crcLed6 = new BEMN.Forms.LedControl();
            this.label27 = new System.Windows.Forms.Label();
            this._crcLed5 = new BEMN.Forms.LedControl();
            this.label28 = new System.Windows.Forms.Label();
            this._crcLed4 = new BEMN.Forms.LedControl();
            this.label29 = new System.Windows.Forms.Label();
            this._crcLed3 = new BEMN.Forms.LedControl();
            this.label30 = new System.Windows.Forms.Label();
            this._crcLed2 = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._crcLed1 = new BEMN.Forms.LedControl();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this._dispepairLed4 = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._dispepairLed3 = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._dispepairLed2 = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._dispepairLed1 = new BEMN.Forms.LedControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._systemTimeCheck = new System.Windows.Forms.CheckBox();
            this._dateTimeBox = new DateTimeBox();
            this._readTimeCheck = new System.Windows.Forms.CheckBox();
            this._writeTimeBut = new System.Windows.Forms.Button();
            this._turnOnSwitchBtn = new System.Windows.Forms.Button();
            this._turnOffSwitchBtn = new System.Windows.Forms.Button();
            this._clearJSBtn = new System.Windows.Forms.Button();
            this._clearJABtn = new System.Windows.Forms.Button();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // _images
            // 
            this._images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.TransparentColor = System.Drawing.Color.Transparent;
            this._images.Images.SetKeyName(0, "analog.bmp");
            this._images.Images.SetKeyName(1, "diskret.bmp");
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._I2Box);
            this.groupBox6.Controls.Add(this._I1Box);
            this.groupBox6.Controls.Add(this._I0Box);
            this.groupBox6.Controls.Add(this._IgBox);
            this.groupBox6.Controls.Add(this._IcBox);
            this.groupBox6.Controls.Add(this._IbBox);
            this.groupBox6.Controls.Add(this._IaBox);
            this.groupBox6.Controls.Add(this._IoBox);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(156, 129);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "���������";
            // 
            // _I2Box
            // 
            this._I2Box.Enabled = false;
            this._I2Box.Location = new System.Drawing.Point(80, 86);
            this._I2Box.Name = "_I2Box";
            this._I2Box.Size = new System.Drawing.Size(68, 20);
            this._I2Box.TabIndex = 16;
            // 
            // _I1Box
            // 
            this._I1Box.Enabled = false;
            this._I1Box.Location = new System.Drawing.Point(80, 62);
            this._I1Box.Name = "_I1Box";
            this._I1Box.Size = new System.Drawing.Size(68, 20);
            this._I1Box.TabIndex = 15;
            // 
            // _I0Box
            // 
            this._I0Box.Enabled = false;
            this._I0Box.Location = new System.Drawing.Point(80, 38);
            this._I0Box.Name = "_I0Box";
            this._I0Box.Size = new System.Drawing.Size(68, 20);
            this._I0Box.TabIndex = 14;
            // 
            // _IgBox
            // 
            this._IgBox.Enabled = false;
            this._IgBox.Location = new System.Drawing.Point(80, 14);
            this._IgBox.Name = "_IgBox";
            this._IgBox.Size = new System.Drawing.Size(68, 20);
            this._IgBox.TabIndex = 13;
            // 
            // _IcBox
            // 
            this._IcBox.Enabled = false;
            this._IcBox.Location = new System.Drawing.Point(6, 62);
            this._IcBox.Name = "_IcBox";
            this._IcBox.Size = new System.Drawing.Size(68, 20);
            this._IcBox.TabIndex = 12;
            // 
            // _IbBox
            // 
            this._IbBox.Enabled = false;
            this._IbBox.Location = new System.Drawing.Point(6, 38);
            this._IbBox.Name = "_IbBox";
            this._IbBox.Size = new System.Drawing.Size(68, 20);
            this._IbBox.TabIndex = 11;
            // 
            // _IaBox
            // 
            this._IaBox.Enabled = false;
            this._IaBox.Location = new System.Drawing.Point(6, 14);
            this._IaBox.Name = "_IaBox";
            this._IaBox.Size = new System.Drawing.Size(68, 20);
            this._IaBox.TabIndex = 9;
            // 
            // _IoBox
            // 
            this._IoBox.Enabled = false;
            this._IoBox.Location = new System.Drawing.Point(6, 86);
            this._IoBox.Name = "_IoBox";
            this._IoBox.Size = new System.Drawing.Size(68, 20);
            this._IoBox.TabIndex = 6;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._conditionLed8);
            this.groupBox3.Controls.Add(this._conditionLed7);
            this.groupBox3.Controls.Add(this._conditionLed6);
            this.groupBox3.Controls.Add(this._conditionLed5);
            this.groupBox3.Controls.Add(this._conditionLed4);
            this.groupBox3.Controls.Add(this._conditionLed3);
            this.groupBox3.Controls.Add(this._conditionLed2);
            this.groupBox3.Controls.Add(this._conditionLed1);
            this.groupBox3.Controls.Add(this._releLed8);
            this.groupBox3.Controls.Add(this._releLed7);
            this.groupBox3.Controls.Add(this._releLed6);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this._releLed5);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._releLed4);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this._releLed3);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this._releLed2);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this._releLed1);
            this.groupBox3.Location = new System.Drawing.Point(260, 282);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(156, 103);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "����";
            // 
            // _conditionLed8
            // 
            this._conditionLed8.Location = new System.Drawing.Point(25, 124);
            this._conditionLed8.Name = "_conditionLed8";
            this._conditionLed8.Size = new System.Drawing.Size(13, 13);
            this._conditionLed8.State = BEMN.Forms.LedState.Off;
            this._conditionLed8.TabIndex = 39;
            this._conditionLed8.Visible = false;
            // 
            // _conditionLed7
            // 
            this._conditionLed7.Location = new System.Drawing.Point(25, 109);
            this._conditionLed7.Name = "_conditionLed7";
            this._conditionLed7.Size = new System.Drawing.Size(13, 13);
            this._conditionLed7.State = BEMN.Forms.LedState.Off;
            this._conditionLed7.TabIndex = 38;
            this._conditionLed7.Visible = false;
            // 
            // _conditionLed6
            // 
            this._conditionLed6.Location = new System.Drawing.Point(25, 94);
            this._conditionLed6.Name = "_conditionLed6";
            this._conditionLed6.Size = new System.Drawing.Size(13, 13);
            this._conditionLed6.State = BEMN.Forms.LedState.Off;
            this._conditionLed6.TabIndex = 37;
            this._conditionLed6.Visible = false;
            // 
            // _conditionLed5
            // 
            this._conditionLed5.Location = new System.Drawing.Point(25, 79);
            this._conditionLed5.Name = "_conditionLed5";
            this._conditionLed5.Size = new System.Drawing.Size(13, 13);
            this._conditionLed5.State = BEMN.Forms.LedState.Off;
            this._conditionLed5.TabIndex = 36;
            // 
            // _conditionLed4
            // 
            this._conditionLed4.Location = new System.Drawing.Point(25, 64);
            this._conditionLed4.Name = "_conditionLed4";
            this._conditionLed4.Size = new System.Drawing.Size(13, 13);
            this._conditionLed4.State = BEMN.Forms.LedState.Off;
            this._conditionLed4.TabIndex = 35;
            // 
            // _conditionLed3
            // 
            this._conditionLed3.Location = new System.Drawing.Point(25, 49);
            this._conditionLed3.Name = "_conditionLed3";
            this._conditionLed3.Size = new System.Drawing.Size(13, 13);
            this._conditionLed3.State = BEMN.Forms.LedState.Off;
            this._conditionLed3.TabIndex = 34;
            // 
            // _conditionLed2
            // 
            this._conditionLed2.Location = new System.Drawing.Point(25, 34);
            this._conditionLed2.Name = "_conditionLed2";
            this._conditionLed2.Size = new System.Drawing.Size(13, 13);
            this._conditionLed2.State = BEMN.Forms.LedState.Off;
            this._conditionLed2.TabIndex = 33;
            // 
            // _conditionLed1
            // 
            this._conditionLed1.Location = new System.Drawing.Point(25, 19);
            this._conditionLed1.Name = "_conditionLed1";
            this._conditionLed1.Size = new System.Drawing.Size(13, 13);
            this._conditionLed1.State = BEMN.Forms.LedState.Off;
            this._conditionLed1.TabIndex = 32;
            // 
            // _releLed8
            // 
            this._releLed8.Location = new System.Drawing.Point(6, 124);
            this._releLed8.Name = "_releLed8";
            this._releLed8.Size = new System.Drawing.Size(13, 13);
            this._releLed8.State = BEMN.Forms.LedState.Off;
            this._releLed8.TabIndex = 30;
            this._releLed8.Visible = false;
            // 
            // _releLed7
            // 
            this._releLed7.Location = new System.Drawing.Point(6, 109);
            this._releLed7.Name = "_releLed7";
            this._releLed7.Size = new System.Drawing.Size(13, 13);
            this._releLed7.State = BEMN.Forms.LedState.Off;
            this._releLed7.TabIndex = 28;
            this._releLed7.Visible = false;
            // 
            // _releLed6
            // 
            this._releLed6.Location = new System.Drawing.Point(6, 94);
            this._releLed6.Name = "_releLed6";
            this._releLed6.Size = new System.Drawing.Size(13, 13);
            this._releLed6.State = BEMN.Forms.LedState.Off;
            this._releLed6.TabIndex = 26;
            this._releLed6.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(42, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "��������� ���";
            // 
            // _releLed5
            // 
            this._releLed5.Location = new System.Drawing.Point(6, 79);
            this._releLed5.Name = "_releLed5";
            this._releLed5.Size = new System.Drawing.Size(13, 13);
            this._releLed5.State = BEMN.Forms.LedState.Off;
            this._releLed5.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(42, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "������";
            // 
            // _releLed4
            // 
            this._releLed4.Location = new System.Drawing.Point(6, 64);
            this._releLed4.Name = "_releLed4";
            this._releLed4.Size = new System.Drawing.Size(13, 13);
            this._releLed4.State = BEMN.Forms.LedState.Off;
            this._releLed4.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(42, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "����";
            // 
            // _releLed3
            // 
            this._releLed3.Location = new System.Drawing.Point(6, 49);
            this._releLed3.Name = "_releLed3";
            this._releLed3.Size = new System.Drawing.Size(13, 13);
            this._releLed3.State = BEMN.Forms.LedState.Off;
            this._releLed3.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(42, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 13);
            this.label15.TabIndex = 19;
            this.label15.Text = "����. �����������";
            // 
            // _releLed2
            // 
            this._releLed2.Location = new System.Drawing.Point(6, 34);
            this._releLed2.Name = "_releLed2";
            this._releLed2.Size = new System.Drawing.Size(13, 13);
            this._releLed2.State = BEMN.Forms.LedState.Off;
            this._releLed2.TabIndex = 18;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(42, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "���. �����������";
            // 
            // _releLed1
            // 
            this._releLed1.Location = new System.Drawing.Point(6, 19);
            this._releLed1.Name = "_releLed1";
            this._releLed1.Size = new System.Drawing.Size(13, 13);
            this._releLed1.State = BEMN.Forms.LedState.Off;
            this._releLed1.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._turnOffSwitchBtn);
            this.groupBox1.Controls.Add(this._turnOnSwitchBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._inLed8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._inLed7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._inLed6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._inLed5);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._inLed4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._inLed3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this._inLed2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this._inLed1);
            this.groupBox1.Location = new System.Drawing.Point(12, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 143);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������� �����";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "����������� �������";
            // 
            // _inLed8
            // 
            this._inLed8.Location = new System.Drawing.Point(11, 62);
            this._inLed8.Name = "_inLed8";
            this._inLed8.Size = new System.Drawing.Size(13, 13);
            this._inLed8.State = BEMN.Forms.LedState.Off;
            this._inLed8.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "����������� ��������";
            // 
            // _inLed7
            // 
            this._inLed7.Location = new System.Drawing.Point(11, 47);
            this._inLed7.Name = "_inLed7";
            this._inLed7.Size = new System.Drawing.Size(13, 13);
            this._inLed7.State = BEMN.Forms.LedState.Off;
            this._inLed7.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "���� �������";
            // 
            // _inLed6
            // 
            this._inLed6.Location = new System.Drawing.Point(11, 32);
            this._inLed6.Name = "_inLed6";
            this._inLed6.Size = new System.Drawing.Size(13, 13);
            this._inLed6.State = BEMN.Forms.LedState.Off;
            this._inLed6.TabIndex = 26;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "���� ��������";
            // 
            // _inLed5
            // 
            this._inLed5.Location = new System.Drawing.Point(11, 17);
            this._inLed5.Name = "_inLed5";
            this._inLed5.Size = new System.Drawing.Size(13, 13);
            this._inLed5.State = BEMN.Forms.LedState.Off;
            this._inLed5.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "�4";
            // 
            // _inLed4
            // 
            this._inLed4.Location = new System.Drawing.Point(11, 122);
            this._inLed4.Name = "_inLed4";
            this._inLed4.Size = new System.Drawing.Size(13, 13);
            this._inLed4.State = BEMN.Forms.LedState.Off;
            this._inLed4.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "�3";
            // 
            // _inLed3
            // 
            this._inLed3.Location = new System.Drawing.Point(11, 107);
            this._inLed3.Name = "_inLed3";
            this._inLed3.Size = new System.Drawing.Size(13, 13);
            this._inLed3.State = BEMN.Forms.LedState.Off;
            this._inLed3.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "�2";
            // 
            // _inLed2
            // 
            this._inLed2.Location = new System.Drawing.Point(11, 92);
            this._inLed2.Name = "_inLed2";
            this._inLed2.Size = new System.Drawing.Size(13, 13);
            this._inLed2.State = BEMN.Forms.LedState.Off;
            this._inLed2.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "�1";
            // 
            // _inLed1
            // 
            this._inLed1.Location = new System.Drawing.Point(11, 77);
            this._inLed1.Name = "_inLed1";
            this._inLed1.Size = new System.Drawing.Size(13, 13);
            this._inLed1.State = BEMN.Forms.LedState.Off;
            this._inLed1.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this._clearJABtn);
            this.groupBox2.Controls.Add(this._clearJSBtn);
            this.groupBox2.Controls.Add(this._defenseLed8);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this._defenseLed7);
            this.groupBox2.Controls.Add(this._defenseLed6);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this._defenseLed5);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this._defenseLed4);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this._defenseLed3);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this._defenseLed2);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this._defenseLed1);
            this.groupBox2.Location = new System.Drawing.Point(260, 147);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(232, 129);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "��������� �����";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(30, 107);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(122, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "����������� �������";
            // 
            // _defenseLed8
            // 
            this._defenseLed8.Location = new System.Drawing.Point(11, 107);
            this._defenseLed8.Name = "_defenseLed8";
            this._defenseLed8.Size = new System.Drawing.Size(13, 13);
            this._defenseLed8.State = BEMN.Forms.LedState.Off;
            this._defenseLed8.TabIndex = 30;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(30, 92);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "����������� ��������";
            // 
            // _defenseLed7
            // 
            this._defenseLed7.Location = new System.Drawing.Point(11, 92);
            this._defenseLed7.Name = "_defenseLed7";
            this._defenseLed7.Size = new System.Drawing.Size(13, 13);
            this._defenseLed7.State = BEMN.Forms.LedState.Off;
            this._defenseLed7.TabIndex = 28;
            // 
            // _defenseLed6
            // 
            this._defenseLed6.Location = new System.Drawing.Point(11, 77);
            this._defenseLed6.Name = "_defenseLed6";
            this._defenseLed6.Size = new System.Drawing.Size(13, 13);
            this._defenseLed6.State = BEMN.Forms.LedState.Off;
            this._defenseLed6.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(30, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "������ ���";
            // 
            // _defenseLed5
            // 
            this._defenseLed5.Location = new System.Drawing.Point(11, 62);
            this._defenseLed5.Name = "_defenseLed5";
            this._defenseLed5.Size = new System.Drawing.Size(13, 13);
            this._defenseLed5.State = BEMN.Forms.LedState.Off;
            this._defenseLed5.TabIndex = 24;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(30, 63);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(96, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "������ (���, ��)";
            // 
            // _defenseLed4
            // 
            this._defenseLed4.Location = new System.Drawing.Point(11, 47);
            this._defenseLed4.Name = "_defenseLed4";
            this._defenseLed4.Size = new System.Drawing.Size(13, 13);
            this._defenseLed4.State = BEMN.Forms.LedState.Off;
            this._defenseLed4.TabIndex = 22;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(30, 48);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(86, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "�������������";
            // 
            // _defenseLed3
            // 
            this._defenseLed3.Location = new System.Drawing.Point(210, 107);
            this._defenseLed3.Name = "_defenseLed3";
            this._defenseLed3.Size = new System.Drawing.Size(13, 13);
            this._defenseLed3.State = BEMN.Forms.LedState.Off;
            this._defenseLed3.TabIndex = 20;
            this._defenseLed3.Visible = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(30, 32);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "����� ������ ��";
            // 
            // _defenseLed2
            // 
            this._defenseLed2.Location = new System.Drawing.Point(11, 32);
            this._defenseLed2.Name = "_defenseLed2";
            this._defenseLed2.Size = new System.Drawing.Size(13, 13);
            this._defenseLed2.State = BEMN.Forms.LedState.Off;
            this._defenseLed2.TabIndex = 18;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(30, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "����� ������ ��";
            // 
            // _defenseLed1
            // 
            this._defenseLed1.Location = new System.Drawing.Point(11, 17);
            this._defenseLed1.Name = "_defenseLed1";
            this._defenseLed1.Size = new System.Drawing.Size(13, 13);
            this._defenseLed1.State = BEMN.Forms.LedState.Off;
            this._defenseLed1.TabIndex = 16;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._crcLed8);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this._crcLed7);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this._crcLed6);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this._crcLed5);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this._crcLed4);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this._crcLed3);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this._crcLed2);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this._crcLed1);
            this.groupBox4.Location = new System.Drawing.Point(174, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(216, 129);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "������ CRC";
            // 
            // _crcLed8
            // 
            this._crcLed8.Location = new System.Drawing.Point(11, 122);
            this._crcLed8.Name = "_crcLed8";
            this._crcLed8.Size = new System.Drawing.Size(13, 13);
            this._crcLed8.State = BEMN.Forms.LedState.Off;
            this._crcLed8.TabIndex = 30;
            this._crcLed8.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(29, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(181, 13);
            this.label25.TabIndex = 29;
            this.label25.Text = "���� ������������ ����� �������";
            // 
            // _crcLed7
            // 
            this._crcLed7.Location = new System.Drawing.Point(11, 107);
            this._crcLed7.Name = "_crcLed7";
            this._crcLed7.Size = new System.Drawing.Size(13, 13);
            this._crcLed7.State = BEMN.Forms.LedState.Off;
            this._crcLed7.TabIndex = 28;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(29, 92);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(146, 13);
            this.label26.TabIndex = 27;
            this.label26.Text = "���� ������ ����� �������";
            // 
            // _crcLed6
            // 
            this._crcLed6.Location = new System.Drawing.Point(11, 92);
            this._crcLed6.Name = "_crcLed6";
            this._crcLed6.Size = new System.Drawing.Size(13, 13);
            this._crcLed6.State = BEMN.Forms.LedState.Off;
            this._crcLed6.TabIndex = 26;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(29, 77);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(147, 13);
            this.label27.TabIndex = 25;
            this.label27.Text = "���� ������� �����������";
            // 
            // _crcLed5
            // 
            this._crcLed5.Location = new System.Drawing.Point(11, 77);
            this._crcLed5.Name = "_crcLed5";
            this._crcLed5.Size = new System.Drawing.Size(13, 13);
            this._crcLed5.State = BEMN.Forms.LedState.Off;
            this._crcLed5.TabIndex = 24;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(29, 62);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(131, 13);
            this.label28.TabIndex = 23;
            this.label28.Text = "���� ���������� �����";
            // 
            // _crcLed4
            // 
            this._crcLed4.Location = new System.Drawing.Point(11, 62);
            this._crcLed4.Name = "_crcLed4";
            this._crcLed4.Size = new System.Drawing.Size(13, 13);
            this._crcLed4.State = BEMN.Forms.LedState.Off;
            this._crcLed4.TabIndex = 22;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(29, 47);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(162, 13);
            this.label29.TabIndex = 21;
            this.label29.Text = "���� ���������� ���������� ";
            // 
            // _crcLed3
            // 
            this._crcLed3.Location = new System.Drawing.Point(11, 47);
            this._crcLed3.Name = "_crcLed3";
            this._crcLed3.Size = new System.Drawing.Size(13, 13);
            this._crcLed3.State = BEMN.Forms.LedState.Off;
            this._crcLed3.TabIndex = 20;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(29, 32);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(144, 13);
            this.label30.TabIndex = 19;
            this.label30.Text = "���� ���������� �������";
            // 
            // _crcLed2
            // 
            this._crcLed2.Location = new System.Drawing.Point(11, 32);
            this._crcLed2.Name = "_crcLed2";
            this._crcLed2.Size = new System.Drawing.Size(13, 13);
            this._crcLed2.State = BEMN.Forms.LedState.Off;
            this._crcLed2.TabIndex = 18;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(29, 17);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(155, 13);
            this.label31.TabIndex = 17;
            this.label31.Text = "���� ������������ �������";
            // 
            // _crcLed1
            // 
            this._crcLed1.Location = new System.Drawing.Point(11, 17);
            this._crcLed1.Name = "_crcLed1";
            this._crcLed1.Size = new System.Drawing.Size(13, 13);
            this._crcLed1.State = BEMN.Forms.LedState.Off;
            this._crcLed1.TabIndex = 16;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this._dispepairLed4);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this._dispepairLed3);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this._dispepairLed2);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this._dispepairLed1);
            this.groupBox5.Location = new System.Drawing.Point(396, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(104, 83);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "�������������";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(30, 62);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(52, 13);
            this.label35.TabIndex = 23;
            this.label35.Text = "��� 15V";
            // 
            // _dispepairLed4
            // 
            this._dispepairLed4.Location = new System.Drawing.Point(11, 62);
            this._dispepairLed4.Name = "_dispepairLed4";
            this._dispepairLed4.Size = new System.Drawing.Size(13, 13);
            this._dispepairLed4.State = BEMN.Forms.LedState.Off;
            this._dispepairLed4.TabIndex = 22;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(30, 47);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(30, 13);
            this.label36.TabIndex = 21;
            this.label36.Text = "���";
            // 
            // _dispepairLed3
            // 
            this._dispepairLed3.Location = new System.Drawing.Point(11, 47);
            this._dispepairLed3.Name = "_dispepairLed3";
            this._dispepairLed3.Size = new System.Drawing.Size(13, 13);
            this._dispepairLed3.State = BEMN.Forms.LedState.Off;
            this._dispepairLed3.TabIndex = 20;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(30, 32);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(30, 13);
            this.label37.TabIndex = 19;
            this.label37.Text = "���";
            // 
            // _dispepairLed2
            // 
            this._dispepairLed2.Location = new System.Drawing.Point(11, 32);
            this._dispepairLed2.Name = "_dispepairLed2";
            this._dispepairLed2.Size = new System.Drawing.Size(13, 13);
            this._dispepairLed2.State = BEMN.Forms.LedState.Off;
            this._dispepairLed2.TabIndex = 18;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(30, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(32, 13);
            this.label38.TabIndex = 17;
            this.label38.Text = "���";
            // 
            // _dispepairLed1
            // 
            this._dispepairLed1.Location = new System.Drawing.Point(11, 17);
            this._dispepairLed1.Name = "_dispepairLed1";
            this._dispepairLed1.Size = new System.Drawing.Size(13, 13);
            this._dispepairLed1.State = BEMN.Forms.LedState.Off;
            this._dispepairLed1.TabIndex = 16;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._systemTimeCheck);
            this.groupBox7.Controls.Add(this._dateTimeBox);
            this.groupBox7.Controls.Add(this._readTimeCheck);
            this.groupBox7.Controls.Add(this._writeTimeBut);
            this.groupBox7.Location = new System.Drawing.Point(12, 296);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(210, 100);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "���� - �����";
            // 
            // _systemTimeCheck
            // 
            this._systemTimeCheck.Appearance = System.Windows.Forms.Appearance.Button;
            this._systemTimeCheck.AutoSize = true;
            this._systemTimeCheck.Location = new System.Drawing.Point(129, 71);
            this._systemTimeCheck.Name = "_systemTimeCheck";
            this._systemTimeCheck.Size = new System.Drawing.Size(73, 23);
            this._systemTimeCheck.TabIndex = 4;
            this._systemTimeCheck.Text = "���������";
            this._systemTimeCheck.UseVisualStyleBackColor = true;
            this._systemTimeCheck.Click += new System.EventHandler(this._systemTimeCheck_CheckedChanged);
            // 
            // _dateTimeBox
            // 
            this._dateTimeBox.DateTime = new byte[] {
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(0))};
            this._dateTimeBox.Location = new System.Drawing.Point(47, 28);
            this._dateTimeBox.Mask = "00-00-00 00:00:00:00";
            this._dateTimeBox.MR700Flag = false;
            this._dateTimeBox.Name = "_dateTimeBox";
            this._dateTimeBox.PiconMicroFlag = false;
            this._dateTimeBox.PromptChar = ' ';
            this._dateTimeBox.Size = new System.Drawing.Size(112, 20);
            this._dateTimeBox.TabIndex = 3;
            this._dateTimeBox.Text = "00000000000000";
            this._dateTimeBox.TZLFlag = false;
            // 
            // _readTimeCheck
            // 
            this._readTimeCheck.Appearance = System.Windows.Forms.Appearance.Button;
            this._readTimeCheck.AutoSize = true;
            this._readTimeCheck.Checked = true;
            this._readTimeCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this._readTimeCheck.Location = new System.Drawing.Point(6, 71);
            this._readTimeCheck.Name = "_readTimeCheck";
            this._readTimeCheck.Size = new System.Drawing.Size(53, 23);
            this._readTimeCheck.TabIndex = 1;
            this._readTimeCheck.Text = "������";
            this._readTimeCheck.UseVisualStyleBackColor = true;
            this._readTimeCheck.CheckedChanged += new System.EventHandler(this._readTimeCheck_CheckedChanged);
            // 
            // _writeTimeBut
            // 
            this._writeTimeBut.Location = new System.Drawing.Point(63, 71);
            this._writeTimeBut.Name = "_writeTimeBut";
            this._writeTimeBut.Size = new System.Drawing.Size(63, 23);
            this._writeTimeBut.TabIndex = 0;
            this._writeTimeBut.Text = "��������";
            this._writeTimeBut.UseVisualStyleBackColor = true;
            this._writeTimeBut.Click += new System.EventHandler(this._writeTimeBut_Click);
            // 
            // _turnOnSwitchBtn
            // 
            this._turnOnSwitchBtn.Location = new System.Drawing.Point(163, 99);
            this._turnOnSwitchBtn.Name = "_turnOnSwitchBtn";
            this._turnOnSwitchBtn.Size = new System.Drawing.Size(70, 20);
            this._turnOnSwitchBtn.TabIndex = 32;
            this._turnOnSwitchBtn.Text = "��������";
            this._turnOnSwitchBtn.UseVisualStyleBackColor = true;
            this._turnOnSwitchBtn.Click += new System.EventHandler(this._swticherOnButton_Click);
            // 
            // _turnOffSwitchBtn
            // 
            this._turnOffSwitchBtn.Location = new System.Drawing.Point(163, 118);
            this._turnOffSwitchBtn.Name = "_turnOffSwitchBtn";
            this._turnOffSwitchBtn.Size = new System.Drawing.Size(70, 20);
            this._turnOffSwitchBtn.TabIndex = 32;
            this._turnOffSwitchBtn.Text = "���������";
            this._turnOffSwitchBtn.UseVisualStyleBackColor = true;
            this._turnOffSwitchBtn.Click += new System.EventHandler(this._switcherOffButton_Click);
            // 
            // _clearJSBtn
            // 
            this._clearJSBtn.Location = new System.Drawing.Point(135, 10);
            this._clearJSBtn.Name = "_clearJSBtn";
            this._clearJSBtn.Size = new System.Drawing.Size(65, 20);
            this._clearJSBtn.TabIndex = 32;
            this._clearJSBtn.Text = "��������";
            this._clearJSBtn.UseVisualStyleBackColor = true;
            this._clearJSBtn.Click += new System.EventHandler(this._resetJS_But_Click);
            // 
            // _clearJABtn
            // 
            this._clearJABtn.Location = new System.Drawing.Point(135, 29);
            this._clearJABtn.Name = "_clearJABtn";
            this._clearJABtn.Size = new System.Drawing.Size(65, 20);
            this._clearJABtn.TabIndex = 32;
            this._clearJABtn.Text = "��������";
            this._clearJABtn.UseVisualStyleBackColor = true;
            this._clearJABtn.Click += new System.EventHandler(this._resetJA_But_Click);
            // 
            // MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 410);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox6);
            this.MaximizeBox = false;
            this.Name = "MeasuringForm";
            this.Text = "MR700_MeasuringForm";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList _images;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox _I2Box;
        private System.Windows.Forms.TextBox _I1Box;
        private System.Windows.Forms.TextBox _I0Box;
        private System.Windows.Forms.TextBox _IgBox;
        private System.Windows.Forms.TextBox _IcBox;
        private System.Windows.Forms.TextBox _IbBox;
        private System.Windows.Forms.TextBox _IaBox;
        private System.Windows.Forms.TextBox _IoBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private BEMN.Forms.LedControl _releLed8;
        private BEMN.Forms.LedControl _releLed7;
        private BEMN.Forms.LedControl _releLed6;
        private System.Windows.Forms.Label label12;
        private BEMN.Forms.LedControl _releLed5;
        private System.Windows.Forms.Label label13;
        private BEMN.Forms.LedControl _releLed4;
        private System.Windows.Forms.Label label14;
        private BEMN.Forms.LedControl _releLed3;
        private System.Windows.Forms.Label label15;
        private BEMN.Forms.LedControl _releLed2;
        private System.Windows.Forms.Label label16;
        private BEMN.Forms.LedControl _releLed1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private BEMN.Forms.LedControl _inLed8;
        private System.Windows.Forms.Label label2;
        private BEMN.Forms.LedControl _inLed7;
        private System.Windows.Forms.Label label3;
        private BEMN.Forms.LedControl _inLed6;
        private System.Windows.Forms.Label label4;
        private BEMN.Forms.LedControl _inLed5;
        private System.Windows.Forms.Label label5;
        private BEMN.Forms.LedControl _inLed4;
        private System.Windows.Forms.Label label6;
        private BEMN.Forms.LedControl _inLed3;
        private System.Windows.Forms.Label label7;
        private BEMN.Forms.LedControl _inLed2;
        private System.Windows.Forms.Label label8;
        private BEMN.Forms.LedControl _inLed1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private BEMN.Forms.LedControl _defenseLed8;
        private System.Windows.Forms.Label label18;
        private BEMN.Forms.LedControl _defenseLed7;
        private BEMN.Forms.LedControl _defenseLed6;
        private System.Windows.Forms.Label label20;
        private BEMN.Forms.LedControl _defenseLed5;
        private System.Windows.Forms.Label label21;
        private BEMN.Forms.LedControl _defenseLed4;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _defenseLed3;
        private System.Windows.Forms.Label label23;
        private BEMN.Forms.LedControl _defenseLed2;
        private System.Windows.Forms.Label label24;
        private BEMN.Forms.LedControl _defenseLed1;
        private System.Windows.Forms.GroupBox groupBox4;
        private BEMN.Forms.LedControl _crcLed8;
        private System.Windows.Forms.Label label25;
        private BEMN.Forms.LedControl _crcLed7;
        private System.Windows.Forms.Label label26;
        private BEMN.Forms.LedControl _crcLed6;
        private System.Windows.Forms.Label label27;
        private BEMN.Forms.LedControl _crcLed5;
        private System.Windows.Forms.Label label28;
        private BEMN.Forms.LedControl _crcLed4;
        private System.Windows.Forms.Label label29;
        private BEMN.Forms.LedControl _crcLed3;
        private System.Windows.Forms.Label label30;
        private BEMN.Forms.LedControl _crcLed2;
        private System.Windows.Forms.Label label31;
        private BEMN.Forms.LedControl _crcLed1;
        private BEMN.Forms.LedControl _conditionLed8;
        private BEMN.Forms.LedControl _conditionLed7;
        private BEMN.Forms.LedControl _conditionLed6;
        private BEMN.Forms.LedControl _conditionLed5;
        private BEMN.Forms.LedControl _conditionLed4;
        private BEMN.Forms.LedControl _conditionLed3;
        private BEMN.Forms.LedControl _conditionLed2;
        private BEMN.Forms.LedControl _conditionLed1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label35;
        private BEMN.Forms.LedControl _dispepairLed4;
        private System.Windows.Forms.Label label36;
        private BEMN.Forms.LedControl _dispepairLed3;
        private System.Windows.Forms.Label label37;
        private BEMN.Forms.LedControl _dispepairLed2;
        private System.Windows.Forms.Label label38;
        private BEMN.Forms.LedControl _dispepairLed1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox _systemTimeCheck;
        private DateTimeBox _dateTimeBox;
        private System.Windows.Forms.CheckBox _readTimeCheck;
        private System.Windows.Forms.Button _writeTimeBut;
        private System.Windows.Forms.Button _turnOffSwitchBtn;
        private System.Windows.Forms.Button _turnOnSwitchBtn;
        private System.Windows.Forms.Button _clearJABtn;
        private System.Windows.Forms.Button _clearJSBtn;

    }
}