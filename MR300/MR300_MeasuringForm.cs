using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR300
{
    public partial class MeasuringForm : Form , IFormView
    {
        private MR300 _device;
        
        private LedControl[] _inputLeds;
        private LedControl[] _releLeds;
        private LedControl[] _defenseLeds;
        private LedControl[] _conditionLeds;
        private LedControl[] _crcLeds;
        private LedControl[] _dispepairLeds;
        
        public MeasuringForm()
        {
            InitializeComponent();
        }

        public MeasuringForm(MR300 device)
        {
            InitializeComponent();
            _device = device;
            this.Init();
            this._device.ConnectionModeChanged += this.StartStopLoad;
        }

        private void Init()
        {
            _inputLeds = new LedControl[]
            {
                _inLed1, _inLed2, _inLed3, _inLed4,
                _inLed5, _inLed6, _inLed7, _inLed8
            };
            _releLeds = new LedControl[]
            {
                _releLed1, _releLed2, _releLed3, _releLed4,
                _releLed5, _releLed6, _releLed7, _releLed8
            };
            _defenseLeds = new LedControl[]
            {
                _defenseLed1, _defenseLed2, _defenseLed3, _defenseLed4,
                _defenseLed5, _defenseLed6, _defenseLed7, _defenseLed8
            };
            _dispepairLeds = new LedControl[] { _dispepairLed1, _dispepairLed2, _dispepairLed3, _dispepairLed4 };
            _crcLeds = new LedControl[]
            {
                _crcLed1, _crcLed2, _crcLed3, _crcLed4,
                _crcLed5, _crcLed6, _crcLed7, _crcLed8
            };
            _conditionLeds = new LedControl[]
            {
                _conditionLed1, _conditionLed2, _conditionLed3, _conditionLed4,
                _conditionLed5, _conditionLed6, _conditionLed7, _conditionLed8
            };

            _device.TelesignalizationLoadOk += HandlerHelper.CreateHandler(this, OnTelesignalizationLoadOk);
            _device.TelesignalizationLoadFail += HandlerHelper.CreateHandler(this, OnTelesignalizationLoadFail);
            _device.AnalogSignalsLoadOk += HandlerHelper.CreateHandler(this, OnAnalogSignalsLoadOk);
            _device.AnalogSignalsLoadFail += HandlerHelper.CreateHandler(this, OnAnalogSignalsLoadFail);

            _dateTimeBox.MR700Flag = true;
            _device.DateTimeLoadOk += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadOk);
            _device.DateTimeLoadFail += HandlerHelper.CreateHandler(this, this.OnDateTimeLoadFail);
        }

        private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            _device.MakeTelesignalizationQuick();
            _device.MakeDateTimeQuick();
            _device.MakeAnalogSignalsQuick();
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            _device.MakeTelesignalizationSlow();
            _device.MakeDateTimeSlow();
            _device.MakeAnalogSignalsSlow();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _device.RemoveTelesignalization();
            _device.RemoveDateTime();
            _device.RemoveAnalogSignals();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                _device.LoadAnalogSignalsCycle();
                _device.LoadTelesignalizationCycle();
                _device.LoadTimeCycle();
            }
            else
            {
                _device.RemoveTelesignalization();
                _device.RemoveDateTime();
                _device.RemoveAnalogSignals();
                OnTelesignalizationLoadFail();
                OnAnalogSignalsLoadFail();
            }
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void OnTelesignalizationLoadOk()
        {
            LedManager.SetLeds(_inputLeds, _device.TeleInputs);
            LedManager.SetLeds(_defenseLeds, _device.TeleDefenses);
            LedManager.SetLeds(_crcLeds, _device.TeleCRC_Errors);
            LedManager.SetLeds(_dispepairLeds, _device.TeleDispepair);
            LedManager.SetLeds(_conditionLeds, _device.TeleOutputs);
            LedManager.SetLeds(_releLeds, _device.TeleRele);
        }

        private void OnTelesignalizationLoadFail()
        {
            LedManager.TurnOffLeds(_inputLeds);
            LedManager.TurnOffLeds(_defenseLeds);
            LedManager.TurnOffLeds(_crcLeds);
            LedManager.TurnOffLeds(_dispepairLeds);
            LedManager.TurnOffLeds(_conditionLeds);
            LedManager.TurnOffLeds(_releLeds);
        }
        
        private void OnAnalogSignalsLoadOk()
        {            
            _IaBox.Text = String.Format("Ia = {0:F2} �", _device.Ia);
            _IbBox.Text = String.Format("Ib = {0:F2} �", _device.Ib);
            _IcBox.Text = String.Format("Ic = {0:F2} �", _device.Ic);
            _I0Box.Text = String.Format("I0 = {0:F2} �", _device.I0);
            _I1Box.Text = String.Format("I1 = {0:F2} �", _device.I1);
            _I2Box.Text = String.Format("I2 = {0:F2} �", _device.I2);
            _IgBox.Text = String.Format("Ig = {0:F2} �", _device.Ig);
            _IoBox.Text = String.Format("Io = {0:F2} �", _device.I0);
        }

        private void OnAnalogSignalsLoadFail()
        {
            _IaBox.Text = String.Empty;
            _IbBox.Text = String.Empty;
            _IcBox.Text = String.Empty;
            _I0Box.Text = String.Empty;
            _I1Box.Text = String.Empty;
            _I2Box.Text = String.Empty;
            _IgBox.Text = String.Empty;
            _IoBox.Text = String.Empty;
        }
        
        private void OnDateTimeLoadFail()
        {
            _dateTimeBox.Text = "";
        }

        private void OnDateTimeLoadOk()
        {
            _dateTimeBox.DateTime = _device.DateTime;
        }
        
        private void _readTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            _device.SuspendDateTime(!_readTimeCheck.Checked);
            if (_readTimeCheck.Checked)
            {                
                _systemTimeCheck.Checked = false;
            }
        }

        private void _writeTimeBut_Click(object sender, EventArgs e)
        {
            _device.DateTime = _dateTimeBox.DateTime;
            _device.SaveDateTime();
            _readTimeCheck.Checked = _systemTimeCheck.Checked = false;
        }

        private void _systemTimeCheck_CheckedChanged(object sender, EventArgs e)
        {
            string[] dates = System.Text.RegularExpressions.Regex.Split(DateTime.Now.ToShortDateString(), "\\.");
            if (null != dates[2]) //Year
            {
                dates[2] = dates[2].Remove(0, 2);
            }

            string timeString = DateTime.Now.ToLongTimeString();
            if (DateTime.Now.Hour < 10)
            {
                timeString = "0" + timeString;
            }

            _dateTimeBox.Text = String.Concat(dates[0], dates[1], dates[2]) + timeString + DateTime.Now.Millisecond.ToString();
            _device.DateTime = _dateTimeBox.DateTime;
            _device.SaveDateTime();
            
            if (!_readTimeCheck.Checked)
            {
                _readTimeCheck.Checked = true;
            }
            _systemTimeCheck.Checked = false;
        }

        private void ConfirmSDTU(ushort address, string msg)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (DialogResult.Yes == 
                MessageBox.Show(msg + " ?", "������-��300", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
            {
                _device.SetBit(_device.DeviceNumber, address, true, msg + _device.DeviceNumber, _device);
            }
        }

        private void _resetJS_But_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x0006, "�������� ������ �������");
        }

        private void _resetJA_But_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x0007, "�������� ������ ������");
        }


        private void _swticherOnButton_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x0000, "�������� �����������");
        }

        private void _switcherOffButton_Click(object sender, EventArgs e)
        {
            ConfirmSDTU(0x0001, "��������� �����������");
        }


        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR300); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "���������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}