using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR300
{
    
    public partial class ConfigurationForm : Form, IFormView
    {
        private MR300 _device;
        private bool _validatingOk = true;
        private bool _connectingErrors;

        public ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public ConfigurationForm(MR300 device)
        {
            this.InitializeComponent();
            this._device = device;
            this.Init();
        }

        private void Init()
        {
            this._device.ConstraintLoadOk += HandlerHelper.CreateHandler(this, this.ConstraintLoadOk);
            this._device.ConstraintLoadFail += HandlerHelper.CreateHandler(this, this.ConstraintLoadFail);
            this.SubscriptOnSaveHandlers();
            this.PrepareDefenses();
            this.PrepareAutomaticsSignals();
            this.PrepareSystemSignals();
        }
       
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR300); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(ConfigurationForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "������������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Misc
        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            this._exchangeProgressBar.Value = 0;
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this._device.Deserialize(this._openConfigurationDlg.FileName);
                }
                catch (System.IO.FileLoadException exc)
                {
                    this._processLabel.Text = "���� " + System.IO.Path.GetFileName(exc.FileName) + " �� �������� ������ ������� ��300 ��� ���������";
                    return;
                }
                this.ShowAutomatics(this._device.Automatics);
                this.ShowDefenses(this._device.Defenses);
                this.ShowSystem();
                this._TT_Box.Text = this._device.TT.ToString();
                this._TTNP_Box.Text = this._device.TTNP.ToString();
                this._exchangeProgressBar.Value = 0;
                this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
            }   
        }
        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveinFile();
        }

        private void SaveinFile()
        {
            this.ValidateAll();

            if (this._validatingOk)
            {
                this._saveConfigurationDlg.FileName = string.Format("��300_�������_������ {0:F1}.bin", this._device.DeviceVersion);
                if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
                {
                    this._exchangeProgressBar.Value = 0;
                    this._device.Serialize(this._saveConfigurationDlg.FileName);
                    this._processLabel.Text = "���� " + this._openConfigurationDlg.FileName + " ��������";
                }
            }
        }
        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._connectingErrors = false;
            this._exchangeProgressBar.Value = 0;
            this.ValidateAll();

            if (this._validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("�������� ������������ ��300 �" + this._device.DeviceNumber + " ?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "���� ������";
                    this._device.SaveConstraint();
                }
            }
        }
        private void ValidateAll()
        {
            this._validatingOk = true;

            //�������� MaskedTextBox
            this.ValidateSystem();
            this.ValidateAutomatics();
            this.ValidateMaskedBoxes(new MaskedTextBox[] { this._TTNP_Box, this._TT_Box });
            //�������� DataGrid
            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WriteDefenses(this._device.Defenses);
                this.WriteSystem();
                this.WriteAutomaticsPage(this._device.Automatics);
            }
        }
        
        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._exchangeProgressBar.Value = 0;
            this._processLabel.Text = "���� ������...";
            this._connectingErrors = false;
            this._device.LoadConstraint();
        }

        private void OnSaveFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� �������� ������������. ��������� �����", "������-��300. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnLoadFail()
        {
            if (!this._connectingErrors)
            {
                MessageBox.Show("���������� ��������� ������������. ��������� �����", "������-��300. ������", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this._connectingErrors = true;
        }

        private void OnSaveOk()
        {
            this._exchangeProgressBar.PerformStep();
        }

        void OnLoadComplete()
        {
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";                
            }
            else
            {
                this._processLabel.Text = "������ ������� ���������";    
            }
        }

        void OnSaveComplete()
        {            
            if (this._connectingErrors)
            {
                this._processLabel.Text = "������ ��������� ���������";
            }
            else
            {
                this._exchangeProgressBar.PerformStep();
                this._processLabel.Text = "������ ������� ���������";
            }
        }

        void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                combo.Items.RemoveAt(combo.Items.Count - 1);
            }
        }
        private void ConfigurationForm_Load(object sender, EventArgs e)
        {
            if(Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartRead();
        }

        private void ConstraintLoadFail()
        {
            try
            {
                this.OnLoadFail();
                this.OnLoadComplete();
            }
            catch (InvalidOperationException)
            { }
        }

        
        private void SubscriptOnSaveHandlers()
        {
            this._device.ConstraintSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveOk));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.ConstraintSaveOk += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
            this._device.ConstraintSaveFail += delegate(object o)
           {
               try
               {
                   Invoke(new OnDeviceEventHandler(this.OnSaveFail));
               }
               catch (InvalidOperationException)
               { }
           };
            this._device.ConstraintSaveFail += delegate(object o)
            {
                try
                {
                    Invoke(new OnDeviceEventHandler(this.OnSaveComplete));
                }
                catch (InvalidOperationException)
                { }
            };
        }

        void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                }

            }
        }
        
        
        private void ShowToolTip(MaskedTextBox box)
        {
            //if (_validatingOk)
            //{
                if (box.Parent.Parent is TabPage)
                {
                    this._tabControl.SelectedTab = box.Parent.Parent as TabPage;
                }                
                this._toolTip.Show("������� ����� ����� � ��������� [0-" + box.Tag + "]", box, 2000);
                box.Focus();
                box.SelectAll();
                this._validatingOk = false;
            //}
        }

        private void ShowToolTip(string msg, DataGridViewCell cell,TabPage page)
        {
            cell.OwningColumn.DataGridView.CurrentCell.Selected = false;
            if (this._validatingOk)
            {
                this._tabControl.SelectedTab = page;    
                this._toolTip.Show(msg, this, this.Location, 2000);
                cell.Selected = true;
                this._validatingOk = false;
            }
            
        }

        void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.SelectedIndex == combo.Items.Count - 1)
            {
                combo.SelectedIndex = 0;
            }
            if (combo.Items.Contains("XXXXX"))
            {
                combo.Items.Remove("XXXXX");
            }

        }

        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndex = 0;
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes,Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void ValidateMaskedBoxes(MaskedTextBox[] boxes)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].ValidateText();
            }
        }
        #endregion
             
        #region ������� ������

        private void ConstraintLoadOk()
        {
            try
            {
                this.OnConstraintLoadOk();            
                this.OnLoadComplete();
            }
            catch (InvalidOperationException)
            { }
        }

        void OnConstraintLoadOk()
        {
            this._exchangeProgressBar.PerformStep();
            this.ClearCombos(this._automaticCombos);
            this.ClearCombos(this._systemCombos);
            this.FillAutomaticCombo();
            this.FillSystemCombo();
            this.SubscriptCombos(this._automaticCombos);
            this.SubscriptCombos(this._systemCombos);
            this.ShowAutomatics(this._device.Automatics);
            this.ShowSystem();
            this.ShowDefenses(this._device.Defenses);
            this._TT_Box.Text = this._device.TT.ToString();
            this._TTNP_Box.Text = this._device.TTNP.ToString();
        }
              
        private void PrepareDefenses()
        {
            this._defenseGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.Grid_EditingControlShowing);
            this._defensesModeColumn.Items.AddRange(Strings.Modes.ToArray());
            this._defensesFeatureColumn.Items.AddRange(Strings.FeatureI.ToArray());
            this.ShowDefenses(this._device.Defenses);
            for (int i = 0; i < MR300.CDefenses.COUNT; i++)
            {
                this.OnDefenseGridModeChanged(i);
            }
            this._defenseGrid.CellStateChanged += new DataGridViewCellStateChangedEventHandler(this._defenseGrid_CellStateChanged);
            
        }

        private bool OnDefenseGridModeChanged(int row)
        {
            int[] columns = new int[this._defenseGrid.Columns.Count - 2];
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i] = i + 2;
            }
            return GridManager.ChangeCellDisabling(this._defenseGrid, row, "��������", 1, columns);
        }

        void _defenseGrid_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e)
        {
            //���������� ����� ��� ������� 
            if (this._defensesModeColumn == e.Cell.OwningColumn )
            {
                this.OnDefenseGridModeChanged(e.Cell.RowIndex);
            }
        }

        private void ShowDefenses(MR300.CDefenses defenses)
        {
            
            this._defenseGrid.Rows.Clear();

            for (int i = 0; i < MR300.CDefenses.COUNT; i++)
            {
                this._defenseGrid.Rows.Add(new object[]{defenses[i].Name,
                                                           defenses[i].Mode,
                                                           defenses[i].WorkConstraint,
                                                           defenses[i].Feature,
                                                           defenses[i].WorkingTime,
                                                           defenses[i].U,
                                                           defenses[i].APV,
                                                           defenses[i].UROV                                                           
                                                           });
                this.OnDefenseGridModeChanged(i);
            }
        }

        private bool WriteDefenses(MR300.CDefenses defenses)
        {
            bool ret = true;

            for (int i = 0; i < this._defenseGrid.Rows.Count; i++)
            {
                defenses[i].Mode = this._defenseGrid[1, i].Value.ToString();
                bool enabled = defenses[i].Mode != "��������";
                if (enabled)
                {                    
                    try
                    {
                        double value = double.Parse(this._defenseGrid[2, i].Value.ToString());
                        if ((value > 40 || value < 0) && defenses[i].MTZ)
                        {
                            this.ShowToolTip("������� ����� � ��������� [0 - 40.00]", this._defenseGrid[2, i], this._defensePage);
                            ret = false;
                        }
                        else if ((value > 5 || value < 0) && !defenses[i].MTZ)
                        {
                            this.ShowToolTip("������� ����� � ��������� [0 - 5.00]", this._defenseGrid[2, i], this._defensePage);
                            ret = false;
                        }
                        else
                        {
                            defenses[i].WorkConstraint = value;
                        }

                    }
                    catch (Exception)
                    {
                        string msg = defenses[i].MTZ ? "������� ����� � ��������� [0 - 40.00]" : "������� ����� � ��������� [0 - 5.00]";
                        this.ShowToolTip(msg, this._defenseGrid[2, i], this._defensePage);
                        ret = false;
                    }

                    defenses[i].Feature = this._defenseGrid[3, i].Value.ToString();

                    bool useKoeff = defenses[i].Feature == "���������";
                    ulong minLimit = useKoeff ? (ulong)800 : (ulong)0;
                    ulong maxLimit = useKoeff ? (ulong)4000 : (ulong)655350;


                    try
                    {
                        ulong value = ulong.Parse(this._defenseGrid[4, i].Value.ToString());

                        if (value > maxLimit || value < minLimit)
                        {
                            this.ShowToolTip("������� ����� � ��������� [" + minLimit + "-" + maxLimit + "]", this._defenseGrid[4, i], this._defensePage);
                            ret = false;
                        }
                        else
                        {
                            defenses[i].WorkingTime = value;
                        }
                    }
                    catch (Exception)
                    {
                        this.ShowToolTip("������� ����� � ��������� [" + minLimit + "-" + maxLimit + "]", this._defenseGrid[4, i], this._defensePage);
                        ret = false;
                    }
                    defenses[i].U = (bool)this._defenseGrid[5, i].Value;
                    defenses[i].APV = (bool)this._defenseGrid[6, i].Value;
                    defenses[i].UROV = (bool)this._defenseGrid[7, i].Value;
                }
            }

           
            return ret;
        }

        #endregion

        #region ����������
       
        private ComboBox[] _automaticCombos;
        private MaskedTextBox[] _automaticsMaskedBoxes;

        private void ValidateAutomatics()
        {
            this.ValidateMaskedBoxes(this._automaticsMaskedBoxes);
        }

        private void PrepareAutomaticsSignals()
        {
            this._APV_ConfigCombo.SelectedIndexChanged += new EventHandler(this._APV_ConfigCombo_SelectedIndexChanged);
            this._MTZ_ConfigCombo.SelectedIndexChanged += new EventHandler(this._MTZ_ConfigCombo_SelectedIndexChanged);
            this._ACR_CnfCombo.SelectedIndexChanged += new EventHandler(this._ACR_CnfCombo_SelectedIndexChanged);
            this._CAPV_CnfCombo.SelectedIndexChanged += new EventHandler(this._CAPV_CnfCombo_SelectedIndexChanged);
            this._Extended1CnfCombo.SelectedIndexChanged += new EventHandler(this._Extended1CnfCombo_SelectedIndexChanged);
            this._Extended2CnfCombo.SelectedIndexChanged += new EventHandler(this._Extended2CnfCombo_SelectedIndexChanged);

            this._automaticCombos = new ComboBox[] {this._MTZ_EntryCombo,this._MTZ_ConfigCombo,
                                               this._APV_ConfigCombo,this._APV_EntryCombo,
                                               this._ACR_CnfCombo,this._ACR_EntryCombo,
                                               this._CAPV_CnfCombo,this._CAPV_EntryCombo,
                                               this._Extended1APV_Combo,this._Extended1CnfCombo,this._Extended1EntryCombo,this._Extended1UROV_Combo,
                                               this._Extended2APV_Combo,this._Extended2UROV_Combo,this._Extended2CnfCombo,this._Extended2EntryCombo};

            this._automaticsMaskedBoxes = new MaskedTextBox[]{this._MTZ_ConstraintBox,this._MTZ_DurationBox,
                                                         this._APV_1cratBox,this._APV_2cratBox,this._APV_BlockingBox,this._APV_ReadyBox,
                                                         this._ACR_TimeBox,
                                                         this._CAPV_TimeBox,
                                                         this._Extended1TimeBox,
                                                         this._Extended2TimeBox};
            this.PrepareMaskedBoxes(this._automaticsMaskedBoxes, typeof(ulong));
            this.FillAutomaticCombo();
            this.SubscriptCombos(this._automaticCombos);

        }


     

        void _Extended2CnfCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._Extended2APV_Combo.Enabled = this._Extended2UROV_Combo.Enabled = this._Extended2TimeBox.Enabled = this._Extended2EntryCombo.Enabled = 
          ("��������" != this._Extended2CnfCombo.Text);
        }

        void _Extended1CnfCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._Extended1APV_Combo.Enabled = this._Extended1UROV_Combo.Enabled = this._Extended1TimeBox.Enabled = this._Extended1EntryCombo.Enabled =
          ("��������" != this._Extended1CnfCombo.Text);
        }

        void _CAPV_CnfCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._CAPV_EntryCombo.Enabled = this._CAPV_TimeBox.Enabled =
                ("��������" != this._CAPV_CnfCombo.Text);
        }

        void _ACR_CnfCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._ACR_EntryCombo.Enabled = this._ACR_TimeBox.Enabled =
                ("��������" != this._ACR_CnfCombo.Text);
        }

        void _MTZ_ConfigCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._MTZ_ConstraintBox.Enabled = this._MTZ_DurationBox.Enabled = this._MTZ_EntryCombo.Enabled =
            ("��������" != this._MTZ_ConfigCombo.Text);
        }

        void _APV_ConfigCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this._APV_1cratBox.Enabled = this._APV_2cratBox.Enabled = this._APV_BlockingBox.Enabled = 
            this._APV_EntryCombo.Enabled = this._APV_ReadyBox.Enabled = 
            ("��������" != this._APV_ConfigCombo.Text);
        }

        private void FillAutomaticCombo()
        {
            this._MTZ_ConfigCombo.Items.AddRange(Strings.ModesLight.ToArray());
            this._MTZ_EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._APV_ConfigCombo.Items.AddRange(Strings.Crat.ToArray());
            this._APV_EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._ACR_CnfCombo.Items.AddRange(Strings.Modes.ToArray());
            this._ACR_EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._CAPV_CnfCombo.Items.AddRange(Strings.ModesLight.ToArray());
            this._CAPV_EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._Extended1CnfCombo.Items.AddRange(Strings.Modes.ToArray());
            this._Extended1EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._Extended1UROV_Combo.Items.AddRange(Strings.ModesLight.ToArray());
            this._Extended1APV_Combo.Items.AddRange(Strings.ModesLight.ToArray());
            this._Extended2CnfCombo.Items.AddRange(Strings.Modes.ToArray());
            this._Extended2EntryCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._Extended2UROV_Combo.Items.AddRange(Strings.ModesLight.ToArray());
            this._Extended2APV_Combo.Items.AddRange(Strings.ModesLight.ToArray());

        }
                       
        private void ShowAutomatics(MR300.CAutomatics automatics)
        {
            //���
            this._MTZ_ConfigCombo.Text = automatics.MTZ_Cnf;
            this._MTZ_EntryCombo.Text = automatics.MTZ_Entry;
            this._MTZ_ConstraintBox.Text = automatics.MTZ_Constraint.ToString();
            this._MTZ_DurationBox.Text = automatics.MTZ_Duration.ToString();

            //���
            this._APV_ConfigCombo.Text = automatics.APV_Cnf;
            this._APV_EntryCombo.Text = automatics.APV_Entry;
            this._APV_1cratBox.Text = automatics.APV_1crat.ToString();
            this._APV_2cratBox.Text = automatics.APV_2crat.ToString();
            this._APV_BlockingBox.Text = automatics.APV_Blocking.ToString();
            this._APV_ReadyBox.Text = automatics.APV_Ready.ToString();

            //���
            this._ACR_CnfCombo.Text = automatics.ACR_Cnf;
            this._ACR_EntryCombo.Text = automatics.ACR_Entry;
            this._ACR_TimeBox.Text = automatics.ACR_Time.ToString();

            //����
            this._CAPV_CnfCombo.Text = automatics.CAPV_Cnf;
            this._CAPV_EntryCombo.Text = automatics.CAPV_Entry;
            this._CAPV_TimeBox.Text = automatics.CAPV_Time.ToString();

            //��1
            this._Extended1APV_Combo.Text = automatics.VZ1_APV;
            this._Extended1CnfCombo.Text = automatics.VZ1_Cnf;
            this._Extended1TimeBox.Text = automatics.VZ1_Time.ToString();
            this._Extended1UROV_Combo.Text = automatics.VZ1_UROV;
            this._Extended1EntryCombo.Text = automatics.VZ1_Entry;

            //��2
            this._Extended2APV_Combo.Text = automatics.VZ2_APV;
            this._Extended2CnfCombo.Text = automatics.VZ2_Cnf;
            this._Extended2TimeBox.Text = automatics.VZ2_Time.ToString();
            this._Extended2UROV_Combo.Text = automatics.VZ2_UROV;
            this._Extended2EntryCombo.Text = automatics.VZ2_Entry;
        }

        private void WriteAutomaticsPage(MR300.CAutomatics automatics)
        {
            //���
            automatics.MTZ_Cnf = this._MTZ_ConfigCombo.Text;
            automatics.MTZ_Entry = this._MTZ_EntryCombo.Text;
            automatics.MTZ_Constraint = ulong.Parse(this._MTZ_ConstraintBox.Text);
            automatics.MTZ_Duration = ulong.Parse(this._MTZ_DurationBox.Text);

            //���
            automatics.APV_1crat = ulong.Parse(this._APV_1cratBox.Text);
            automatics.APV_2crat = ulong.Parse(this._APV_2cratBox.Text);
            automatics.APV_Blocking = ulong.Parse(this._APV_BlockingBox.Text);
            automatics.APV_Entry = this._APV_EntryCombo.Text;
            automatics.APV_Ready = ulong.Parse(this._APV_ReadyBox.Text);
            automatics.APV_Cnf = this._APV_ConfigCombo.Text;
            
            //���
            automatics.ACR_Cnf = this._ACR_CnfCombo.Text;
            automatics.ACR_Entry = this._ACR_EntryCombo.Text;
            automatics.ACR_Time = ulong.Parse(this._ACR_TimeBox.Text);

            //����
            automatics.CAPV_Cnf = this._CAPV_CnfCombo.Text;
            automatics.CAPV_Entry = this._CAPV_EntryCombo.Text;
            automatics.CAPV_Time = ulong.Parse(this._CAPV_TimeBox.Text);
            
            //��1
            automatics.VZ1_APV = this._Extended1APV_Combo.Text;
            automatics.VZ1_Cnf = this._Extended1CnfCombo.Text;
            automatics.VZ1_Entry = this._Extended1EntryCombo.Text;
            automatics.VZ1_Time = ulong.Parse(this._Extended1TimeBox.Text);
            automatics.VZ1_UROV = this._Extended1UROV_Combo.Text;

            //��2
            automatics.VZ2_APV = this._Extended2APV_Combo.Text;
            automatics.VZ2_Cnf = this._Extended2CnfCombo.Text;
            automatics.VZ2_Entry = this._Extended2EntryCombo.Text;
            automatics.VZ2_Time = ulong.Parse(this._Extended2TimeBox.Text);
            automatics.VZ2_UROV = this._Extended2UROV_Combo.Text;
                

        }

        #endregion

        #region �������

        private ComboBox[] _systemCombos;
        private MaskedTextBox[] _systemMaskedBoxes;

        private void ValidateSystem()
        {
            this.ValidateMaskedBoxes(this._systemMaskedBoxes);
        }

        private void PrepareSystemSignals()
        {

            this._systemCombos = new ComboBox[] {this._UminCombo,this._UoCombo,
                                            this._operationKeyCombo,this._operationSDTU_Combo,
                                            this._switcherBlockCombo,
                                            this._releTypeCombo1,this._releTypeCombo2,this._releTypeCombo3,this._releTypeCombo4,this._releTypeCombo5};

            this._systemMaskedBoxes = new MaskedTextBox[]{this._switcherTimeBox,this._switcherImpulseOnBox,this._switcherImpulseOffBox,
                                                     this._releImpulseBox1,this._releImpulseBox2,this._releImpulseBox3,this._releImpulseBox4,this._releImpulseBox5,
                                                     this._TT_Box,this._TTNP_Box};
            this.PrepareMaskedBoxes(this._systemMaskedBoxes, typeof(ulong));
            this.FillSystemCombo();
            this.SubscriptCombos(this._systemCombos);

        }

        private void FillSystemCombo()
        {
            this._UminCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._UoCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());
            this._operationKeyCombo.Items.AddRange(Strings.Control.ToArray());
            this._operationSDTU_Combo.Items.AddRange(Strings.Control.ToArray());
            this._switcherBlockCombo.Items.AddRange(Strings.DiskretAutomatic.ToArray());

            this._releTypeCombo1.Items.AddRange(Strings.Rele.ToArray());
            this._releTypeCombo2.Items.AddRange(Strings.Rele2.ToArray());
            this._releTypeCombo3.Items.AddRange(Strings.Rele.ToArray());
            this._releTypeCombo4.Items.AddRange(Strings.Rele.ToArray());
            this._releTypeCombo5.Items.AddRange(Strings.Rele.ToArray());
        }

        private void ShowSystem()
        {
            this._UminCombo.Text = this._device.Umin;
            this._UoCombo.Text = this._device.Uo;
            this._operationKeyCombo.Text = this._device.OperationKey;
            this._operationSDTU_Combo.Text = this._device.OperationSDTU;
            this._switcherBlockCombo.Text = this._device.SwitcherBlock;
            this._switcherTimeBox.Text = this._device.SwitcherTime.ToString();
            this._switcherImpulseOffBox.Text = this._device.SwitcherImpulseOff.ToString();
            this._switcherImpulseOnBox.Text = this._device.SwitcherImpulseOn.ToString();
            this._releTypeCombo1.Text = this._device.OutputRele[0].Type;
            this._releTypeCombo2.Text = this._device.OutputRele[1].Type;
            this._releTypeCombo3.Text = this._device.OutputRele[2].Type;
            this._releTypeCombo4.Text = this._device.OutputRele[3].Type;
            this._releTypeCombo5.Text = this._device.OutputRele[4].Type;

            this._releImpulseBox1.Text = this._device.OutputRele[0].Impulse.ToString();
            this._releImpulseBox2.Text = this._device.OutputRele[1].Impulse.ToString();
            this._releImpulseBox3.Text = this._device.OutputRele[2].Impulse.ToString();
            this._releImpulseBox4.Text = this._device.OutputRele[3].Impulse.ToString();
            this._releImpulseBox5.Text = this._device.OutputRele[4].Impulse.ToString();
        }

        private void WriteSystem()
        {
            this._device.Umin = this._UminCombo.Text;
            this._device.Uo = this._UoCombo.Text;
            this._device.OperationKey = this._operationKeyCombo.Text;
            this._device.OperationSDTU = this._operationSDTU_Combo.Text;
            this._device.SwitcherBlock = this._switcherBlockCombo.Text;
            this._device.SwitcherTime = ulong.Parse(this._switcherTimeBox.Text);
            this._device.SwitcherImpulseOn = ulong.Parse(this._switcherImpulseOnBox.Text);
            this._device.SwitcherImpulseOff = ulong.Parse(this._switcherImpulseOffBox.Text);

            this._device.OutputRele[0].Impulse = ulong.Parse(this._releImpulseBox1.Text);
            this._device.OutputRele[1].Impulse = ulong.Parse(this._releImpulseBox2.Text);
            this._device.OutputRele[2].Impulse = ulong.Parse(this._releImpulseBox3.Text);
            this._device.OutputRele[3].Impulse = ulong.Parse(this._releImpulseBox4.Text);
            this._device.OutputRele[4].Impulse = ulong.Parse(this._releImpulseBox5.Text);

            this._device.OutputRele[0].Type = this._releTypeCombo1.Text;
            this._device.OutputRele[1].Type = this._releTypeCombo2.Text;
            this._device.OutputRele[2].Type = this._releTypeCombo3.Text;
            this._device.OutputRele[3].Type = this._releTypeCombo4.Text;
            this._device.OutputRele[4].Type = this._releTypeCombo5.Text;

            this._device.TT = ushort.Parse(this._TT_Box.Text);
            this._device.TTNP = ushort.Parse(this._TTNP_Box.Text);
        }

        #endregion

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
        }

        private void Mr300ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
    }
}
