using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR300
{    
    public partial class AlarmJournalForm : Form , IFormView
    {
        private MR300 _device;

        public AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public AlarmJournalForm(MR300 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._journalProgress.Maximum = MR300.ALARMJOURNAL_RECORD_CNT;
            this._device.AlarmJournalRecordLoadOk += this._device_AlarmJournalRecordLoadOk;
            this._device.AlarmJournalRecordLoadFail += this._device_AlarmJournalRecordLoadFail;
            this._device.AlarmJournalLoadOk += HandlerHelper.CreateHandler(this, this.AlarmJournalLoadOkFullOld);
        }

        private void AlarmJournalLoadOkFullOld()
        {
            if (this._journalGrid.Rows.Count == 0)
            {
                MessageBox.Show("������ ����.", "������ �������", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("������ ��������.", "������ �������", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this._journalProgress.Value = this._journalProgress.Maximum;
            }
        }

        void _device_AlarmJournalRecordLoadFail(object sender, int index)
        {
            MessageBox.Show("���������� ��������� ������ ������. ��������� �����.", "������ - ��300. ������", 
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void _device_AlarmJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnAlarmJournalRecordLoadOk), index);
            }
            catch (InvalidOperationException)
            { }
        }

        private void OnAlarmJournalRecordLoadOk(int i)
        {
            this._journalProgress.PerformStep();
            this._journalCntLabel.Text = (i + 1).ToString();
            this._journalGrid.Rows.Add(i + 1, this._device.AlarmJournal[i].time, 
                this._device.AlarmJournal[i].msg, this._device.AlarmJournal[i].type,
                this._device.AlarmJournal[i].value, this._device.AlarmJournal[i].defense, 
                this._device.AlarmJournal[i].Ia, this._device.AlarmJournal[i].Ib, 
                this._device.AlarmJournal[i].Ic, this._device.AlarmJournal[i].Io, 
                this._device.AlarmJournal[i].Ig, this._device.AlarmJournal[i].I0, 
                this._device.AlarmJournal[i].I1, this._device.AlarmJournal[i].I2);
        }

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR300); }
        }

        public bool Multishow { get; private set; }
        
        public Type ClassType
        {
            get { return typeof(AlarmJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return "������ ������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        private void _readBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            this._device.AlarmJournal = new MR300.CAlarmJournal(this._device);
            this._device.RemoveAlarmJournal();
            this._device.AlarmJournal.Clear();
            this._journalProgress.Value = 0;
            this._journalGrid.Rows.Clear();
            this._device.LoadAlarmJournal();
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveAlarmJournal();
        }

        private void _serializeBut_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable table = CreateAlarmJournalTable();
                List<object> row = new List<object>();

                for (int i = 0; i < this._journalGrid.Rows.Count; i++)
                {
                    row.Clear();
                    for (int j = 0; j < this._journalGrid.Columns.Count; j++)
                    {
                        row.Add(this._journalGrid[j, i].Value);
                    }
                    table.Rows.Add(row.ToArray());
                }
                if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
                {
                    table.WriteXml(this._saveSysJournalDlg.FileName);
                }

                _journalCntLabel.Text = $"���� '{ _saveSysJournalDlg.FileName}' ������� ��������";
            }
            catch (Exception exception)
            {
                MessageBox.Show("���������� ��������� ������ ������!", "��������!", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                _journalCntLabel.Text = "������ ���������� ������� ������";
            }
        }

        private static DataTable CreateAlarmJournalTable()
        {
            DataTable table = new DataTable("��300_������_������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            table.Columns.Add("��� �����������");
            table.Columns.Add("�������� �����������");
            table.Columns.Add("������");
            table.Columns.Add("Ia");
            table.Columns.Add("Ib");
            table.Columns.Add("Ic");
            table.Columns.Add("Io");
            table.Columns.Add("Ig");
            table.Columns.Add("I0");
            table.Columns.Add("I1");
            table.Columns.Add("I2");
           return table;
        }

        private void _deserializeBut_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable table = CreateAlarmJournalTable();

                if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
                {
                    this._journalGrid.Rows.Clear();
                    table.ReadXml(this._openSysJounralDlg.FileName);
                }

                List<object> row = new List<object>();
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    row.Clear();
                    row.AddRange(table.Rows[i].ItemArray);
                    this._journalGrid.Rows.Add(row.ToArray());
                }

                _journalCntLabel.Text = $"���� {_saveSysJournalDlg.FileName} ������� ��������";
            }
            catch (Exception exception)
            {
                MessageBox.Show("���������� ��������� ���� ������� ������!", "��������", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                _journalCntLabel.Text = "������ �������� ����� ������� ������";
            }
           
            
        }

        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._device.AlarmJournal = new MR300.CAlarmJournal(this._device);
            this._device.RemoveAlarmJournal();
            this._journalProgress.Value = 0;
            this._journalGrid.Rows.Clear();
            if(this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this._device.LoadAlarmJournal();
        }
    }
}