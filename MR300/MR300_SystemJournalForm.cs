using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;

namespace BEMN.MR300
{
    public partial class SystemJournalForm : Form , IFormView
    {
        private MR300 _device;

        public SystemJournalForm()
        {
            this.InitializeComponent();
        }

        public SystemJournalForm(MR300 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.SystemJournalLoadOk += HandlerHelper.CreateHandler(this, this.OnSystemJournalLoadOk);
            this._device.SystemJournalRecordLoadOk += new IndexHandler(this._device_SystemJournalRecordLoadOk);

            this._device.SystemJournalRecordLoadFail += delegate
            {
                MessageBox.Show("��300 �" + this._device.DeviceNumber + ": ������ ������ ������� �������", "������ - ������",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }
        
        void OnSystemJournalLoadOk()
        {
            this._journalCntLabel.Text = "������ ������� ��������";
            this._journalProgress.Value = this._journalProgress.Maximum;
        }

        void _device_SystemJournalRecordLoadOk(object sender, int index)
        {
            try
            {
                Invoke(new OnDeviceIndexEventHandler(this.OnSysJournalIndexLoadOk), new object[] { index });
            }
            catch (InvalidOperationException)
            {}
        }

        private void OnSysJournalIndexLoadOk(int i)
        {
            this._journalProgress.PerformStep();
            this._journalCntLabel.Text = (i + 1).ToString();
            this._sysJournalGrid.Rows.Add( i + 1, this._device.SystemJournal[i].time, this._device.SystemJournal[i].msg);
        }
        
        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(MR300); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(SystemJournalForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.js; }
        }

        public string NodeName
        {
            get { return "������ �������"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
        
        private void _serializeSysJournalBut_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable("��300_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");
            for (int i = 0; i < this._sysJournalGrid.Rows.Count; i++)
            {
                table.Rows.Add(this._sysJournalGrid["_indexCol", i].Value, this._sysJournalGrid["_timeCol", i].Value,
                    this._sysJournalGrid["_msgCol", i].Value);
            }

            if (DialogResult.OK == this._saveSysJournalDlg.ShowDialog())
            {
                table.WriteXml(this._saveSysJournalDlg.FileName);
            }
        }

        private void _deserializeSysJournalBut_Click(object sender, EventArgs e)
        {            
            DataTable table = new DataTable("��300_������_�������");
            table.Columns.Add("�����");
            table.Columns.Add("�����");
            table.Columns.Add("���������");

            if (DialogResult.OK == this._openSysJounralDlg.ShowDialog())
            {
                this._sysJournalGrid.Rows.Clear();
                table.ReadXml(this._openSysJounralDlg.FileName);
            }

            for (int i = 0; i < table.Rows.Count; i++)
            {
                this._sysJournalGrid.Rows.Add(table.Rows[i].ItemArray[0], table.Rows[i].ItemArray[1],
                    table.Rows[i].ItemArray[2]);
            }
        }
        
        private void SystemJournalForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._device.RemoveSystemJournal();
        }

        private void _readSysJournalBut_Click(object sender, EventArgs e)
        {
            if(this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
                this.StartRead();
        }

        private void StartRead()
        {
            this._device.RemoveSystemJournal();
            this._device.SystemJournal.Clear();
            this._journalProgress.Value = 0;
            this._journalProgress.Maximum = MR300.SYSTEMJOURNAL_RECORD_CNT;
            this._sysJournalGrid.Rows.Clear();
            this._device.LoadSystemJournal();
        }
    }
}