namespace BEMN.MR300
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this._defensePage = new System.Windows.Forms.TabPage();
            this._defenseGrid = new System.Windows.Forms.DataGridView();
            this._defensesNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._defensesModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defensesConstraintColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._defensesFeatureColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defensesTimeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._defensesU_Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defensesAPV_Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._defensesUROV_Column = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._automaticPage = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._Extended2EntryCombo = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this._Extended2APV_Combo = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._Extended2TimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._Extended2UROV_Combo = new System.Windows.Forms.ComboBox();
            this._Extended2CnfCombo = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._Extended1EntryCombo = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this._Extended1APV_Combo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this._Extended1TimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this._Extended1UROV_Combo = new System.Windows.Forms.ComboBox();
            this._Extended1CnfCombo = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this._CAPV_TimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this._CAPV_EntryCombo = new System.Windows.Forms.ComboBox();
            this._CAPV_CnfCombo = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this._ACR_TimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._ACR_EntryCombo = new System.Windows.Forms.ComboBox();
            this._ACR_CnfCombo = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this._APV_2cratBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_1cratBox = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._APV_ReadyBox = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this._APV_BlockingBox = new System.Windows.Forms.MaskedTextBox();
            this._APV_EntryCombo = new System.Windows.Forms.ComboBox();
            this._APV_ConfigCombo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._MTZ_ConstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._MTZ_DurationBox = new System.Windows.Forms.MaskedTextBox();
            this._MTZ_EntryCombo = new System.Windows.Forms.ComboBox();
            this._MTZ_ConfigCombo = new System.Windows.Forms.ComboBox();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._releImpulseBox5 = new System.Windows.Forms.MaskedTextBox();
            this._releImpulseBox4 = new System.Windows.Forms.MaskedTextBox();
            this._releImpulseBox3 = new System.Windows.Forms.MaskedTextBox();
            this._releImpulseBox2 = new System.Windows.Forms.MaskedTextBox();
            this._releImpulseBox1 = new System.Windows.Forms.MaskedTextBox();
            this._releTypeCombo5 = new System.Windows.Forms.ComboBox();
            this._releTypeCombo4 = new System.Windows.Forms.ComboBox();
            this._releTypeCombo3 = new System.Windows.Forms.ComboBox();
            this._releTypeCombo2 = new System.Windows.Forms.ComboBox();
            this._releTypeCombo1 = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this._switcherImpulseOnBox = new System.Windows.Forms.MaskedTextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._switcherImpulseOffBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherTimeBox = new System.Windows.Forms.MaskedTextBox();
            this._switcherBlockCombo = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this._operationSDTU_Combo = new System.Windows.Forms.ComboBox();
            this._operationKeyCombo = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this._UoCombo = new System.Windows.Forms.ComboBox();
            this._UminCombo = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this._measuringChannel = new System.Windows.Forms.TabPage();
            this.label44 = new System.Windows.Forms.Label();
            this._TTNP_Box = new System.Windows.Forms.MaskedTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this._TT_Box = new System.Windows.Forms.MaskedTextBox();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._writeConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._statusStrip = new System.Windows.Forms.StatusStrip();
            this._processLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exchangeProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._tabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._defensePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._defenseGrid)).BeginInit();
            this._automaticPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._systemPage.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._measuringChannel.SuspendLayout();
            this._statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tabControl
            // 
            this._tabControl.ContextMenuStrip = this.contextMenu;
            this._tabControl.Controls.Add(this._defensePage);
            this._tabControl.Controls.Add(this._automaticPage);
            this._tabControl.Controls.Add(this._systemPage);
            this._tabControl.Controls.Add(this._measuringChannel);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(733, 305);
            this._tabControl.TabIndex = 0;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 92);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "��������� �� ����������";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "�������� � ����������";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "��������� �� �����";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "��������� � ����";
            // 
            // _defensePage
            // 
            this._defensePage.Controls.Add(this._defenseGrid);
            this._defensePage.Location = new System.Drawing.Point(4, 22);
            this._defensePage.Name = "_defensePage";
            this._defensePage.Size = new System.Drawing.Size(725, 279);
            this._defensePage.TabIndex = 2;
            this._defensePage.Text = "������";
            this._defensePage.UseVisualStyleBackColor = true;
            // 
            // _defenseGrid
            // 
            this._defenseGrid.AllowUserToAddRows = false;
            this._defenseGrid.AllowUserToDeleteRows = false;
            this._defenseGrid.AllowUserToResizeColumns = false;
            this._defenseGrid.AllowUserToResizeRows = false;
            this._defenseGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._defenseGrid.ColumnHeadersHeight = 30;
            this._defenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._defenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._defensesNameColumn,
            this._defensesModeColumn,
            this._defensesConstraintColumn,
            this._defensesFeatureColumn,
            this._defensesTimeColumn,
            this._defensesU_Column,
            this._defensesAPV_Column,
            this._defensesUROV_Column});
            this._defenseGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._defenseGrid.Location = new System.Drawing.Point(0, 0);
            this._defenseGrid.Name = "_defenseGrid";
            this._defenseGrid.RowHeadersVisible = false;
            this._defenseGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._defenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this._defenseGrid.RowTemplate.Height = 24;
            this._defenseGrid.Size = new System.Drawing.Size(725, 279);
            this._defenseGrid.TabIndex = 0;
            // 
            // _defensesNameColumn
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._defensesNameColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this._defensesNameColumn.HeaderText = "";
            this._defensesNameColumn.Name = "_defensesNameColumn";
            this._defensesNameColumn.ReadOnly = true;
            this._defensesNameColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._defensesNameColumn.Width = 50;
            // 
            // _defensesModeColumn
            // 
            this._defensesModeColumn.HeaderText = "�����";
            this._defensesModeColumn.Name = "_defensesModeColumn";
            this._defensesModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _defensesConstraintColumn
            // 
            this._defensesConstraintColumn.HeaderText = "�������";
            this._defensesConstraintColumn.Name = "_defensesConstraintColumn";
            this._defensesConstraintColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._defensesConstraintColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._defensesConstraintColumn.Width = 60;
            // 
            // _defensesFeatureColumn
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this._defensesFeatureColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this._defensesFeatureColumn.HeaderText = "��������������";
            this._defensesFeatureColumn.Name = "_defensesFeatureColumn";
            this._defensesFeatureColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // _defensesTimeColumn
            // 
            this._defensesTimeColumn.HeaderText = "t����, ��/����.";
            this._defensesTimeColumn.Name = "_defensesTimeColumn";
            this._defensesTimeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._defensesTimeColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._defensesTimeColumn.Width = 95;
            // 
            // _defensesU_Column
            // 
            this._defensesU_Column.HeaderText = "U min (3U0)";
            this._defensesU_Column.Name = "_defensesU_Column";
            this._defensesU_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._defensesU_Column.Width = 70;
            // 
            // _defensesAPV_Column
            // 
            this._defensesAPV_Column.HeaderText = "���";
            this._defensesAPV_Column.Name = "_defensesAPV_Column";
            this._defensesAPV_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._defensesAPV_Column.Width = 35;
            // 
            // _defensesUROV_Column
            // 
            this._defensesUROV_Column.HeaderText = "����";
            this._defensesUROV_Column.Name = "_defensesUROV_Column";
            this._defensesUROV_Column.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._defensesUROV_Column.Width = 40;
            // 
            // _automaticPage
            // 
            this._automaticPage.Controls.Add(this.groupBox6);
            this._automaticPage.Controls.Add(this.groupBox5);
            this._automaticPage.Controls.Add(this.groupBox4);
            this._automaticPage.Controls.Add(this.groupBox3);
            this._automaticPage.Controls.Add(this.groupBox2);
            this._automaticPage.Controls.Add(this.groupBox1);
            this._automaticPage.Location = new System.Drawing.Point(4, 22);
            this._automaticPage.Name = "_automaticPage";
            this._automaticPage.Size = new System.Drawing.Size(725, 279);
            this._automaticPage.TabIndex = 3;
            this._automaticPage.Text = "����������";
            this._automaticPage.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._Extended2EntryCombo);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this._Extended2APV_Combo);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this._Extended2TimeBox);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this._Extended2UROV_Combo);
            this.groupBox6.Controls.Add(this._Extended2CnfCombo);
            this.groupBox6.Location = new System.Drawing.Point(454, 130);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(203, 125);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "��2";
            // 
            // _Extended2EntryCombo
            // 
            this._Extended2EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended2EntryCombo.FormattingEnabled = true;
            this._Extended2EntryCombo.Location = new System.Drawing.Point(94, 34);
            this._Extended2EntryCombo.Name = "_Extended2EntryCombo";
            this._Extended2EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._Extended2EntryCombo.TabIndex = 26;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 39);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "����� �����";
            // 
            // _Extended2APV_Combo
            // 
            this._Extended2APV_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended2APV_Combo.FormattingEnabled = true;
            this._Extended2APV_Combo.Location = new System.Drawing.Point(94, 75);
            this._Extended2APV_Combo.Name = "_Extended2APV_Combo";
            this._Extended2APV_Combo.Size = new System.Drawing.Size(100, 21);
            this._Extended2APV_Combo.TabIndex = 24;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(8, 59);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "��������";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(8, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "����";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 80);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "���";
            // 
            // _Extended2TimeBox
            // 
            this._Extended2TimeBox.Location = new System.Drawing.Point(94, 55);
            this._Extended2TimeBox.Name = "_Extended2TimeBox";
            this._Extended2TimeBox.Size = new System.Drawing.Size(99, 20);
            this._Extended2TimeBox.TabIndex = 20;
            this._Extended2TimeBox.Tag = "655350";
            this._Extended2TimeBox.Text = "0";
            this._Extended2TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(8, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 13);
            this.label26.TabIndex = 19;
            this.label26.Text = "������������ ";
            // 
            // _Extended2UROV_Combo
            // 
            this._Extended2UROV_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended2UROV_Combo.FormattingEnabled = true;
            this._Extended2UROV_Combo.Location = new System.Drawing.Point(94, 96);
            this._Extended2UROV_Combo.Name = "_Extended2UROV_Combo";
            this._Extended2UROV_Combo.Size = new System.Drawing.Size(100, 21);
            this._Extended2UROV_Combo.TabIndex = 3;
            // 
            // _Extended2CnfCombo
            // 
            this._Extended2CnfCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended2CnfCombo.FormattingEnabled = true;
            this._Extended2CnfCombo.Location = new System.Drawing.Point(94, 12);
            this._Extended2CnfCombo.Name = "_Extended2CnfCombo";
            this._Extended2CnfCombo.Size = new System.Drawing.Size(100, 21);
            this._Extended2CnfCombo.TabIndex = 1;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._Extended1EntryCombo);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this._Extended1APV_Combo);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this._Extended1TimeBox);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this._Extended1UROV_Combo);
            this.groupBox5.Controls.Add(this._Extended1CnfCombo);
            this.groupBox5.Location = new System.Drawing.Point(454, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(203, 125);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "��1";
            // 
            // _Extended1EntryCombo
            // 
            this._Extended1EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended1EntryCombo.FormattingEnabled = true;
            this._Extended1EntryCombo.Location = new System.Drawing.Point(94, 33);
            this._Extended1EntryCombo.Name = "_Extended1EntryCombo";
            this._Extended1EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._Extended1EntryCombo.TabIndex = 26;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 36);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "����� �����";
            // 
            // _Extended1APV_Combo
            // 
            this._Extended1APV_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended1APV_Combo.FormattingEnabled = true;
            this._Extended1APV_Combo.Location = new System.Drawing.Point(94, 74);
            this._Extended1APV_Combo.Name = "_Extended1APV_Combo";
            this._Extended1APV_Combo.Size = new System.Drawing.Size(100, 21);
            this._Extended1APV_Combo.TabIndex = 24;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "��������";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 99);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "����";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 80);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "���";
            // 
            // _Extended1TimeBox
            // 
            this._Extended1TimeBox.Location = new System.Drawing.Point(94, 54);
            this._Extended1TimeBox.Name = "_Extended1TimeBox";
            this._Extended1TimeBox.Size = new System.Drawing.Size(99, 20);
            this._Extended1TimeBox.TabIndex = 20;
            this._Extended1TimeBox.Tag = "655350";
            this._Extended1TimeBox.Text = "0";
            this._Extended1TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "������������ ";
            // 
            // _Extended1UROV_Combo
            // 
            this._Extended1UROV_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended1UROV_Combo.FormattingEnabled = true;
            this._Extended1UROV_Combo.Location = new System.Drawing.Point(94, 95);
            this._Extended1UROV_Combo.Name = "_Extended1UROV_Combo";
            this._Extended1UROV_Combo.Size = new System.Drawing.Size(100, 21);
            this._Extended1UROV_Combo.TabIndex = 3;
            // 
            // _Extended1CnfCombo
            // 
            this._Extended1CnfCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Extended1CnfCombo.FormattingEnabled = true;
            this._Extended1CnfCombo.Location = new System.Drawing.Point(94, 12);
            this._Extended1CnfCombo.Name = "_Extended1CnfCombo";
            this._Extended1CnfCombo.Size = new System.Drawing.Size(100, 21);
            this._Extended1CnfCombo.TabIndex = 1;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._CAPV_TimeBox);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this._CAPV_EntryCombo);
            this.groupBox4.Controls.Add(this._CAPV_CnfCombo);
            this.groupBox4.Location = new System.Drawing.Point(241, 83);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(207, 83);
            this.groupBox4.TabIndex = 29;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "����";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "�������� ���.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "����� �����";
            // 
            // _CAPV_TimeBox
            // 
            this._CAPV_TimeBox.Location = new System.Drawing.Point(95, 54);
            this._CAPV_TimeBox.Name = "_CAPV_TimeBox";
            this._CAPV_TimeBox.Size = new System.Drawing.Size(99, 20);
            this._CAPV_TimeBox.TabIndex = 20;
            this._CAPV_TimeBox.Tag = "655350";
            this._CAPV_TimeBox.Text = "0";
            this._CAPV_TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "������������ ";
            // 
            // _CAPV_EntryCombo
            // 
            this._CAPV_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CAPV_EntryCombo.FormattingEnabled = true;
            this._CAPV_EntryCombo.Location = new System.Drawing.Point(95, 33);
            this._CAPV_EntryCombo.Name = "_CAPV_EntryCombo";
            this._CAPV_EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._CAPV_EntryCombo.TabIndex = 3;
            // 
            // _CAPV_CnfCombo
            // 
            this._CAPV_CnfCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CAPV_CnfCombo.FormattingEnabled = true;
            this._CAPV_CnfCombo.Location = new System.Drawing.Point(95, 12);
            this._CAPV_CnfCombo.Name = "_CAPV_CnfCombo";
            this._CAPV_CnfCombo.Size = new System.Drawing.Size(100, 21);
            this._CAPV_CnfCombo.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this._ACR_TimeBox);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this._ACR_EntryCombo);
            this.groupBox3.Controls.Add(this._ACR_CnfCombo);
            this.groupBox3.Location = new System.Drawing.Point(241, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(207, 83);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "���";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "�������� ���. ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "����� �����";
            // 
            // _ACR_TimeBox
            // 
            this._ACR_TimeBox.Location = new System.Drawing.Point(95, 54);
            this._ACR_TimeBox.Name = "_ACR_TimeBox";
            this._ACR_TimeBox.Size = new System.Drawing.Size(99, 20);
            this._ACR_TimeBox.TabIndex = 20;
            this._ACR_TimeBox.Tag = "655350";
            this._ACR_TimeBox.Text = "0";
            this._ACR_TimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "������������ ";
            // 
            // _ACR_EntryCombo
            // 
            this._ACR_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ACR_EntryCombo.FormattingEnabled = true;
            this._ACR_EntryCombo.Location = new System.Drawing.Point(95, 33);
            this._ACR_EntryCombo.Name = "_ACR_EntryCombo";
            this._ACR_EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._ACR_EntryCombo.TabIndex = 3;
            // 
            // _ACR_CnfCombo
            // 
            this._ACR_CnfCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ACR_CnfCombo.FormattingEnabled = true;
            this._ACR_CnfCombo.Location = new System.Drawing.Point(95, 12);
            this._ACR_CnfCombo.Name = "_ACR_CnfCombo";
            this._ACR_CnfCombo.Size = new System.Drawing.Size(100, 21);
            this._ACR_CnfCombo.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this._APV_2cratBox);
            this.groupBox2.Controls.Add(this._APV_1cratBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._APV_ReadyBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this._APV_BlockingBox);
            this.groupBox2.Controls.Add(this._APV_EntryCombo);
            this.groupBox2.Controls.Add(this._APV_ConfigCombo);
            this.groupBox2.Location = new System.Drawing.Point(8, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(228, 141);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "���";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "����� 2 � 3 �����";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 97);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "����� 1 �����";
            // 
            // _APV_2cratBox
            // 
            this._APV_2cratBox.Location = new System.Drawing.Point(121, 114);
            this._APV_2cratBox.Name = "_APV_2cratBox";
            this._APV_2cratBox.Size = new System.Drawing.Size(99, 20);
            this._APV_2cratBox.TabIndex = 25;
            this._APV_2cratBox.Tag = "655350";
            this._APV_2cratBox.Text = "0";
            this._APV_2cratBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_1cratBox
            // 
            this._APV_1cratBox.Location = new System.Drawing.Point(121, 94);
            this._APV_1cratBox.Name = "_APV_1cratBox";
            this._APV_1cratBox.Size = new System.Drawing.Size(99, 20);
            this._APV_1cratBox.TabIndex = 24;
            this._APV_1cratBox.Tag = "655350";
            this._APV_1cratBox.Text = "0";
            this._APV_1cratBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "����� ����������";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "����� ����������";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "����� �����";
            // 
            // _APV_ReadyBox
            // 
            this._APV_ReadyBox.Location = new System.Drawing.Point(121, 74);
            this._APV_ReadyBox.Name = "_APV_ReadyBox";
            this._APV_ReadyBox.Size = new System.Drawing.Size(99, 20);
            this._APV_ReadyBox.TabIndex = 20;
            this._APV_ReadyBox.Tag = "655350";
            this._APV_ReadyBox.Text = "0";
            this._APV_ReadyBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "������������ ";
            // 
            // _APV_BlockingBox
            // 
            this._APV_BlockingBox.Location = new System.Drawing.Point(121, 54);
            this._APV_BlockingBox.Name = "_APV_BlockingBox";
            this._APV_BlockingBox.Size = new System.Drawing.Size(99, 20);
            this._APV_BlockingBox.TabIndex = 18;
            this._APV_BlockingBox.Tag = "655350";
            this._APV_BlockingBox.Text = "0";
            this._APV_BlockingBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _APV_EntryCombo
            // 
            this._APV_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APV_EntryCombo.FormattingEnabled = true;
            this._APV_EntryCombo.Location = new System.Drawing.Point(121, 33);
            this._APV_EntryCombo.Name = "_APV_EntryCombo";
            this._APV_EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._APV_EntryCombo.TabIndex = 3;
            // 
            // _APV_ConfigCombo
            // 
            this._APV_ConfigCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APV_ConfigCombo.FormattingEnabled = true;
            this._APV_ConfigCombo.Location = new System.Drawing.Point(121, 12);
            this._APV_ConfigCombo.Name = "_APV_ConfigCombo";
            this._APV_ConfigCombo.Size = new System.Drawing.Size(100, 21);
            this._APV_ConfigCombo.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._MTZ_ConstraintBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._MTZ_DurationBox);
            this.groupBox1.Controls.Add(this._MTZ_EntryCombo);
            this.groupBox1.Controls.Add(this._MTZ_ConfigCombo);
            this.groupBox1.Location = new System.Drawing.Point(8, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 104);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "���������";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "������� ���. ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "������������ ���.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "����� �����";
            // 
            // _MTZ_ConstraintBox
            // 
            this._MTZ_ConstraintBox.Location = new System.Drawing.Point(120, 74);
            this._MTZ_ConstraintBox.Name = "_MTZ_ConstraintBox";
            this._MTZ_ConstraintBox.Size = new System.Drawing.Size(99, 20);
            this._MTZ_ConstraintBox.TabIndex = 20;
            this._MTZ_ConstraintBox.Tag = "655350";
            this._MTZ_ConstraintBox.Text = "0";
            this._MTZ_ConstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "������������ ";
            // 
            // _MTZ_DurationBox
            // 
            this._MTZ_DurationBox.Location = new System.Drawing.Point(120, 54);
            this._MTZ_DurationBox.Name = "_MTZ_DurationBox";
            this._MTZ_DurationBox.Size = new System.Drawing.Size(99, 20);
            this._MTZ_DurationBox.TabIndex = 18;
            this._MTZ_DurationBox.Tag = "655350";
            this._MTZ_DurationBox.Text = "0";
            this._MTZ_DurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _MTZ_EntryCombo
            // 
            this._MTZ_EntryCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._MTZ_EntryCombo.FormattingEnabled = true;
            this._MTZ_EntryCombo.Location = new System.Drawing.Point(120, 33);
            this._MTZ_EntryCombo.Name = "_MTZ_EntryCombo";
            this._MTZ_EntryCombo.Size = new System.Drawing.Size(100, 21);
            this._MTZ_EntryCombo.TabIndex = 3;
            // 
            // _MTZ_ConfigCombo
            // 
            this._MTZ_ConfigCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._MTZ_ConfigCombo.FormattingEnabled = true;
            this._MTZ_ConfigCombo.Location = new System.Drawing.Point(120, 12);
            this._MTZ_ConfigCombo.Name = "_MTZ_ConfigCombo";
            this._MTZ_ConfigCombo.Size = new System.Drawing.Size(100, 21);
            this._MTZ_ConfigCombo.TabIndex = 1;
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox10);
            this._systemPage.Controls.Add(this.groupBox9);
            this._systemPage.Controls.Add(this.groupBox8);
            this._systemPage.Controls.Add(this.groupBox7);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(725, 279);
            this._systemPage.TabIndex = 4;
            this._systemPage.Text = "��������� �������";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._releImpulseBox5);
            this.groupBox10.Controls.Add(this._releImpulseBox4);
            this.groupBox10.Controls.Add(this._releImpulseBox3);
            this.groupBox10.Controls.Add(this._releImpulseBox2);
            this.groupBox10.Controls.Add(this._releImpulseBox1);
            this.groupBox10.Controls.Add(this._releTypeCombo5);
            this.groupBox10.Controls.Add(this._releTypeCombo4);
            this.groupBox10.Controls.Add(this._releTypeCombo3);
            this.groupBox10.Controls.Add(this._releTypeCombo2);
            this.groupBox10.Controls.Add(this._releTypeCombo1);
            this.groupBox10.Controls.Add(this.label42);
            this.groupBox10.Controls.Add(this.label41);
            this.groupBox10.Controls.Add(this.label40);
            this.groupBox10.Controls.Add(this.label39);
            this.groupBox10.Controls.Add(this.label37);
            this.groupBox10.Controls.Add(this.label38);
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Controls.Add(this.label35);
            this.groupBox10.Location = new System.Drawing.Point(8, 77);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(330, 198);
            this.groupBox10.TabIndex = 32;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "�������� ����";
            // 
            // _releImpulseBox5
            // 
            this._releImpulseBox5.Location = new System.Drawing.Point(224, 167);
            this._releImpulseBox5.Name = "_releImpulseBox5";
            this._releImpulseBox5.Size = new System.Drawing.Size(95, 20);
            this._releImpulseBox5.TabIndex = 45;
            this._releImpulseBox5.Tag = "655350";
            this._releImpulseBox5.Text = "0";
            this._releImpulseBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _releImpulseBox4
            // 
            this._releImpulseBox4.Location = new System.Drawing.Point(224, 132);
            this._releImpulseBox4.Name = "_releImpulseBox4";
            this._releImpulseBox4.Size = new System.Drawing.Size(95, 20);
            this._releImpulseBox4.TabIndex = 44;
            this._releImpulseBox4.Tag = "655350";
            this._releImpulseBox4.Text = "0";
            this._releImpulseBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _releImpulseBox3
            // 
            this._releImpulseBox3.Location = new System.Drawing.Point(224, 97);
            this._releImpulseBox3.Name = "_releImpulseBox3";
            this._releImpulseBox3.Size = new System.Drawing.Size(95, 20);
            this._releImpulseBox3.TabIndex = 43;
            this._releImpulseBox3.Tag = "655350";
            this._releImpulseBox3.Text = "0";
            this._releImpulseBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _releImpulseBox2
            // 
            this._releImpulseBox2.Location = new System.Drawing.Point(224, 64);
            this._releImpulseBox2.Name = "_releImpulseBox2";
            this._releImpulseBox2.Size = new System.Drawing.Size(95, 20);
            this._releImpulseBox2.TabIndex = 42;
            this._releImpulseBox2.Tag = "655350";
            this._releImpulseBox2.Text = "0";
            this._releImpulseBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _releImpulseBox1
            // 
            this._releImpulseBox1.Location = new System.Drawing.Point(224, 29);
            this._releImpulseBox1.Name = "_releImpulseBox1";
            this._releImpulseBox1.Size = new System.Drawing.Size(95, 20);
            this._releImpulseBox1.TabIndex = 41;
            this._releImpulseBox1.Tag = "655350";
            this._releImpulseBox1.Text = "0";
            this._releImpulseBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _releTypeCombo5
            // 
            this._releTypeCombo5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releTypeCombo5.FormattingEnabled = true;
            this._releTypeCombo5.Location = new System.Drawing.Point(39, 169);
            this._releTypeCombo5.Name = "_releTypeCombo5";
            this._releTypeCombo5.Size = new System.Drawing.Size(160, 21);
            this._releTypeCombo5.TabIndex = 40;
            // 
            // _releTypeCombo4
            // 
            this._releTypeCombo4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releTypeCombo4.FormattingEnabled = true;
            this._releTypeCombo4.Location = new System.Drawing.Point(39, 134);
            this._releTypeCombo4.Name = "_releTypeCombo4";
            this._releTypeCombo4.Size = new System.Drawing.Size(160, 21);
            this._releTypeCombo4.TabIndex = 39;
            // 
            // _releTypeCombo3
            // 
            this._releTypeCombo3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releTypeCombo3.FormattingEnabled = true;
            this._releTypeCombo3.Location = new System.Drawing.Point(39, 99);
            this._releTypeCombo3.Name = "_releTypeCombo3";
            this._releTypeCombo3.Size = new System.Drawing.Size(160, 21);
            this._releTypeCombo3.TabIndex = 38;
            // 
            // _releTypeCombo2
            // 
            this._releTypeCombo2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releTypeCombo2.FormattingEnabled = true;
            this._releTypeCombo2.Location = new System.Drawing.Point(39, 64);
            this._releTypeCombo2.Name = "_releTypeCombo2";
            this._releTypeCombo2.Size = new System.Drawing.Size(160, 21);
            this._releTypeCombo2.TabIndex = 37;
            // 
            // _releTypeCombo1
            // 
            this._releTypeCombo1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._releTypeCombo1.FormattingEnabled = true;
            this._releTypeCombo1.Location = new System.Drawing.Point(39, 29);
            this._releTypeCombo1.Name = "_releTypeCombo1";
            this._releTypeCombo1.Size = new System.Drawing.Size(160, 21);
            this._releTypeCombo1.TabIndex = 36;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(238, 14);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(64, 13);
            this.label42.TabIndex = 35;
            this.label42.Text = "������, ��";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(105, 14);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(26, 13);
            this.label41.TabIndex = 34;
            this.label41.Text = "���";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(19, 14);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(18, 13);
            this.label40.TabIndex = 33;
            this.label40.Text = "�";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(20, 174);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 13);
            this.label39.TabIndex = 32;
            this.label39.Text = "5";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(20, 139);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 13);
            this.label37.TabIndex = 31;
            this.label37.Text = "4";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(20, 104);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "3";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(20, 69);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 13);
            this.label36.TabIndex = 29;
            this.label36.Text = "2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(20, 34);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 13);
            this.label35.TabIndex = 28;
            this.label35.Text = "1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this._switcherImpulseOnBox);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.label33);
            this.groupBox9.Controls.Add(this._switcherImpulseOffBox);
            this.groupBox9.Controls.Add(this._switcherTimeBox);
            this.groupBox9.Controls.Add(this._switcherBlockCombo);
            this.groupBox9.Controls.Add(this.label32);
            this.groupBox9.Location = new System.Drawing.Point(344, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(211, 106);
            this.groupBox9.TabIndex = 31;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "�����������";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(8, 79);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(76, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "������� ���.";
            // 
            // _switcherImpulseOnBox
            // 
            this._switcherImpulseOnBox.Location = new System.Drawing.Point(107, 75);
            this._switcherImpulseOnBox.Name = "_switcherImpulseOnBox";
            this._switcherImpulseOnBox.Size = new System.Drawing.Size(95, 20);
            this._switcherImpulseOnBox.TabIndex = 29;
            this._switcherImpulseOnBox.Tag = "655350";
            this._switcherImpulseOnBox.Text = "0";
            this._switcherImpulseOnBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 60);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(81, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "������� ����.";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 41);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(69, 13);
            this.label33.TabIndex = 27;
            this.label33.Text = "����� ����.";
            // 
            // _switcherImpulseOffBox
            // 
            this._switcherImpulseOffBox.Location = new System.Drawing.Point(107, 55);
            this._switcherImpulseOffBox.Name = "_switcherImpulseOffBox";
            this._switcherImpulseOffBox.Size = new System.Drawing.Size(95, 20);
            this._switcherImpulseOffBox.TabIndex = 26;
            this._switcherImpulseOffBox.Tag = "655350";
            this._switcherImpulseOffBox.Text = "0";
            this._switcherImpulseOffBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherTimeBox
            // 
            this._switcherTimeBox.Location = new System.Drawing.Point(107, 35);
            this._switcherTimeBox.Name = "_switcherTimeBox";
            this._switcherTimeBox.Size = new System.Drawing.Size(95, 20);
            this._switcherTimeBox.TabIndex = 25;
            this._switcherTimeBox.Tag = "655350";
            this._switcherTimeBox.Text = "0";
            this._switcherTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switcherBlockCombo
            // 
            this._switcherBlockCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switcherBlockCombo.FormattingEnabled = true;
            this._switcherBlockCombo.Location = new System.Drawing.Point(107, 14);
            this._switcherBlockCombo.Name = "_switcherBlockCombo";
            this._switcherBlockCombo.Size = new System.Drawing.Size(95, 21);
            this._switcherBlockCombo.TabIndex = 24;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(8, 22);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(93, 13);
            this.label32.TabIndex = 23;
            this.label32.Text = "����. ���������";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this._operationSDTU_Combo);
            this.groupBox8.Controls.Add(this._operationKeyCombo);
            this.groupBox8.Controls.Add(this.label30);
            this.groupBox8.Location = new System.Drawing.Point(166, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(172, 70);
            this.groupBox8.TabIndex = 30;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "��������";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(8, 42);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 26;
            this.label29.Text = "�� ����";
            // 
            // _operationSDTU_Combo
            // 
            this._operationSDTU_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._operationSDTU_Combo.FormattingEnabled = true;
            this._operationSDTU_Combo.Location = new System.Drawing.Point(69, 39);
            this._operationSDTU_Combo.Name = "_operationSDTU_Combo";
            this._operationSDTU_Combo.Size = new System.Drawing.Size(95, 21);
            this._operationSDTU_Combo.TabIndex = 25;
            // 
            // _operationKeyCombo
            // 
            this._operationKeyCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._operationKeyCombo.FormattingEnabled = true;
            this._operationKeyCombo.Location = new System.Drawing.Point(69, 18);
            this._operationKeyCombo.Name = "_operationKeyCombo";
            this._operationKeyCombo.Size = new System.Drawing.Size(95, 21);
            this._operationKeyCombo.TabIndex = 24;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(8, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 13);
            this.label30.TabIndex = 23;
            this.label30.Text = "�� �����";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this._UoCombo);
            this.groupBox7.Controls.Add(this._UminCombo);
            this.groupBox7.Controls.Add(this.label27);
            this.groupBox7.Location = new System.Drawing.Point(8, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(152, 70);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "������� �������";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(8, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 13);
            this.label28.TabIndex = 26;
            this.label28.Text = "3Uo";
            // 
            // _UoCombo
            // 
            this._UoCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UoCombo.FormattingEnabled = true;
            this._UoCombo.Location = new System.Drawing.Point(51, 39);
            this._UoCombo.Name = "_UoCombo";
            this._UoCombo.Size = new System.Drawing.Size(95, 21);
            this._UoCombo.TabIndex = 25;
            // 
            // _UminCombo
            // 
            this._UminCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UminCombo.FormattingEnabled = true;
            this._UminCombo.Location = new System.Drawing.Point(51, 18);
            this._UminCombo.Name = "_UminCombo";
            this._UminCombo.Size = new System.Drawing.Size(95, 21);
            this._UminCombo.TabIndex = 24;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(8, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 23;
            this.label27.Text = "U min";
            // 
            // _measuringChannel
            // 
            this._measuringChannel.Controls.Add(this.label44);
            this._measuringChannel.Controls.Add(this._TTNP_Box);
            this._measuringChannel.Controls.Add(this.label43);
            this._measuringChannel.Controls.Add(this._TT_Box);
            this._measuringChannel.Location = new System.Drawing.Point(4, 22);
            this._measuringChannel.Name = "_measuringChannel";
            this._measuringChannel.Size = new System.Drawing.Size(725, 279);
            this._measuringChannel.TabIndex = 5;
            this._measuringChannel.Text = "������������� �����";
            this._measuringChannel.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(21, 62);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(135, 13);
            this.label44.TabIndex = 31;
            this.label44.Text = "��������� ��� ���� {�}";
            // 
            // _TTNP_Box
            // 
            this._TTNP_Box.Location = new System.Drawing.Point(160, 56);
            this._TTNP_Box.Name = "_TTNP_Box";
            this._TTNP_Box.Size = new System.Drawing.Size(41, 20);
            this._TTNP_Box.TabIndex = 30;
            this._TTNP_Box.Tag = "200";
            this._TTNP_Box.Text = "0";
            this._TTNP_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(37, 33);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(119, 13);
            this.label43.TabIndex = 29;
            this.label43.Text = "��������� ��� �� {�}";
            // 
            // _TT_Box
            // 
            this._TT_Box.Location = new System.Drawing.Point(160, 30);
            this._TT_Box.Name = "_TT_Box";
            this._TT_Box.Size = new System.Drawing.Size(41, 20);
            this._TT_Box.TabIndex = 28;
            this._TT_Box.Tag = "1000";
            this._TT_Box.Text = "0";
            this._TT_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Location = new System.Drawing.Point(8, 310);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 1;
            this._readConfigBut.Text = "��������� �� ����������";
            this._toolTip.SetToolTip(this._readConfigBut, "��������� ������������ �� ���������� (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _toolTip
            // 
            this._toolTip.IsBalloon = true;
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Location = new System.Drawing.Point(159, 310);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(145, 23);
            this._writeConfigBut.TabIndex = 2;
            this._writeConfigBut.Text = "�������� � ����������";
            this._toolTip.SetToolTip(this._writeConfigBut, "�������� ������������ � ���������� (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.Location = new System.Drawing.Point(495, 310);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(119, 23);
            this._saveConfigBut.TabIndex = 4;
            this._saveConfigBut.Text = "��������� � ����";
            this._toolTip.SetToolTip(this._saveConfigBut, "��������� ������������ � ���� (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Location = new System.Drawing.Point(359, 310);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(119, 23);
            this._loadConfigBut.TabIndex = 3;
            this._loadConfigBut.Text = "��������� �� �����";
            this._toolTip.SetToolTip(this._loadConfigBut, "��������� ������������ �� ����� (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "bin";
            this._saveConfigurationDlg.FileName = "��300_�������";
            this._saveConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._saveConfigurationDlg.Title = "���������  ������� ��� ��300";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "bin";
            this._openConfigurationDlg.Filter = "(*.bin) | *.bin";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "������� ������� ��� ��300";
            // 
            // _statusStrip
            // 
            this._statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._processLabel,
            this._exchangeProgressBar});
            this._statusStrip.Location = new System.Drawing.Point(0, 337);
            this._statusStrip.Name = "_statusStrip";
            this._statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this._statusStrip.Size = new System.Drawing.Size(733, 22);
            this._statusStrip.TabIndex = 5;
            this._statusStrip.Text = "statusStrip1";
            // 
            // _processLabel
            // 
            this._processLabel.Name = "_processLabel";
            this._processLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _exchangeProgressBar
            // 
            this._exchangeProgressBar.Maximum = 50;
            this._exchangeProgressBar.Name = "_exchangeProgressBar";
            this._exchangeProgressBar.Size = new System.Drawing.Size(100, 16);
            this._exchangeProgressBar.Step = 50;
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 359);
            this.Controls.Add(this._statusStrip);
            this.Controls.Add(this._saveConfigBut);
            this.Controls.Add(this._loadConfigBut);
            this.Controls.Add(this._writeConfigBut);
            this.Controls.Add(this._readConfigBut);
            this.Controls.Add(this._tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr300ConfigurationForm_KeyUp);
            this._tabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._defensePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._defenseGrid)).EndInit();
            this._automaticPage.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._systemPage.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._measuringChannel.ResumeLayout(false);
            this._measuringChannel.PerformLayout();
            this._statusStrip.ResumeLayout(false);
            this._statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tabControl;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.TabPage _defensePage;
        private System.Windows.Forms.DataGridView _defenseGrid;
        private System.Windows.Forms.TabPage _automaticPage;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.StatusStrip _statusStrip;
        private System.Windows.Forms.ToolStripProgressBar _exchangeProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel _processLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox _MTZ_DurationBox;
        private System.Windows.Forms.ComboBox _MTZ_EntryCombo;
        private System.Windows.Forms.ComboBox _MTZ_ConfigCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox _MTZ_ConstraintBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox _APV_2cratBox;
        private System.Windows.Forms.MaskedTextBox _APV_1cratBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _APV_ReadyBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _APV_BlockingBox;
        private System.Windows.Forms.ComboBox _APV_EntryCombo;
        private System.Windows.Forms.ComboBox _APV_ConfigCombo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _ACR_TimeBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox _ACR_EntryCombo;
        private System.Windows.Forms.ComboBox _ACR_CnfCombo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox _CAPV_TimeBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox _CAPV_EntryCombo;
        private System.Windows.Forms.ComboBox _CAPV_CnfCombo;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _Extended1EntryCombo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox _Extended1APV_Combo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.MaskedTextBox _Extended1TimeBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox _Extended1UROV_Combo;
        private System.Windows.Forms.ComboBox _Extended1CnfCombo;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox _Extended2EntryCombo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox _Extended2APV_Combo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.MaskedTextBox _Extended2TimeBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox _Extended2UROV_Combo;
        private System.Windows.Forms.ComboBox _Extended2CnfCombo;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox _switcherBlockCombo;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox _operationSDTU_Combo;
        private System.Windows.Forms.ComboBox _operationKeyCombo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox _UoCombo;
        private System.Windows.Forms.ComboBox _UminCombo;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseOnBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _switcherImpulseOffBox;
        private System.Windows.Forms.MaskedTextBox _switcherTimeBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox _releTypeCombo5;
        private System.Windows.Forms.ComboBox _releTypeCombo4;
        private System.Windows.Forms.ComboBox _releTypeCombo3;
        private System.Windows.Forms.ComboBox _releTypeCombo2;
        private System.Windows.Forms.ComboBox _releTypeCombo1;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.MaskedTextBox _releImpulseBox5;
        private System.Windows.Forms.MaskedTextBox _releImpulseBox4;
        private System.Windows.Forms.MaskedTextBox _releImpulseBox3;
        private System.Windows.Forms.MaskedTextBox _releImpulseBox2;
        private System.Windows.Forms.MaskedTextBox _releImpulseBox1;
        private System.Windows.Forms.TabPage _measuringChannel;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.MaskedTextBox _TTNP_Box;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.MaskedTextBox _TT_Box;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn _defensesNameColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _defensesModeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _defensesConstraintColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _defensesFeatureColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _defensesTimeColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _defensesU_Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _defensesAPV_Column;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _defensesUROV_Column;
    }
}