using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Forms.Old;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MBServer.Queries;

namespace BEMN.MR300
{
    public class MR300 : Device, IDeviceView
    {
        public const ulong TIMELIMIT = 3000000;
        public const string TIMELIMIT_ERROR_MSG = "������� ����� � ��������� [0 - 3000000]��";

        private slot _constraint = new slot(0x200, 0x230);

        public void LoadConstraint()
        {            
            LoadSlot(DeviceNumber, this._constraint, "LoadConstraint" + DeviceNumber, this);
        }
        public void SaveConstraint()
        {
            this.OutputRele.MakeBuffer(this._constraint.Value);
            Array.ConstrainedCopy(Common.TOWORDS(this.Defenses.ToByte(), false), 0, this._constraint.Value, CDefenses.OFFSET / 2, CDefenses.LENGTH / 2);
            Array.ConstrainedCopy(Common.TOWORDS(this.Automatics.ToBytes(), false), 0, this._constraint.Value, CAutomatics.OFFSET / 2, CAutomatics.LENGTH / 2);
            SaveSlot(DeviceNumber, this._constraint, "SaveConstraint" + DeviceNumber, this);
        }

        //public override void LoadVersion(object deviceObj)
        //{
        //    LoadSlot(this.DeviceNumber, new slot(0x100, 0x102), "version" + this.DeviceNumber, this);
        //}

        #region ������

        public class CDefenses : ICollection
        {

            public const int LENGTH = 30;
            public const int OFFSET = 66;
            public const int COUNT = 6;

            #region ICollection Members

            public void Add(DefenseItem item)
            {
                this._defenseList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }

            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._defenseList.GetEnumerator();
            }

            #endregion

            public CDefenses()
            {
                this.SetBuffer(new byte[LENGTH]);
            }

            private List<DefenseItem> _defenseList = new List<DefenseItem>(COUNT);

            public void SetBuffer(byte[] buffer)
            {
                this._defenseList.Clear();
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense values", (object)LENGTH, "External defense length must be 48");
                }
                
                List<DefenseItem> bufferDefenseList = new List<DefenseItem>(COUNT);
                for (int i = 0; i < LENGTH; i += DefenseItem.LENGTH)
                {
                    byte[] temp = new byte[DefenseItem.LENGTH];
                    Array.ConstrainedCopy(buffer, i, temp, 0, DefenseItem.LENGTH);
                    bufferDefenseList.Add(new DefenseItem(temp));
                }


                bufferDefenseList[0].Name = "��� �";
                bufferDefenseList[0].MTZ = false;
                bufferDefenseList[1].Name = "��� 3";
                bufferDefenseList[1].MTZ = true;
                bufferDefenseList[2].Name = "��� 2";
                bufferDefenseList[2].MTZ = true;
                bufferDefenseList[3].Name = "��� 1";
                bufferDefenseList[3].MTZ = true;
                bufferDefenseList[4].Name = "���";
                bufferDefenseList[4].MTZ = true;
                bufferDefenseList[5].Name = "���";
                bufferDefenseList[5].MTZ = false;

                this._defenseList.Insert(0, bufferDefenseList[1]);
                this._defenseList.Insert(1, bufferDefenseList[2]);
                this._defenseList.Insert(2, bufferDefenseList[3]);
                this._defenseList.Insert(3, bufferDefenseList[4]);
                this._defenseList.Insert(4, bufferDefenseList[5]);
                this._defenseList.Insert(5, bufferDefenseList[0]);
                
            }
            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return this._defenseList.Count;
                }
            }

            public byte[] ToByte()
            {
                byte[] buffer = new byte[LENGTH];

                List<DefenseItem> bufferDefenseList = new List<DefenseItem>(COUNT);

                bufferDefenseList.Insert(0, this._defenseList[5]);
                bufferDefenseList.Insert(1, this._defenseList[0]);
                bufferDefenseList.Insert(2, this._defenseList[1]);
                bufferDefenseList.Insert(3, this._defenseList[2]);
                bufferDefenseList.Insert(4, this._defenseList[3]);
                bufferDefenseList.Insert(5, this._defenseList[4]);

                for (int i = 0; i < this.Count; i++)
                {
                    Array.ConstrainedCopy(bufferDefenseList[i].Values, 0, buffer, i * DefenseItem.LENGTH, DefenseItem.LENGTH);
                }
                return buffer;
            }

            public DefenseItem this[int i]
            {
                get
                {
                    return this._defenseList[i];
                }
                set
                {
                    this._defenseList[i] = value;
                }
            }
        };

        public class DefenseItem
        {
            public const int LENGTH = 5;
            private byte[] _values = new byte[LENGTH];

            public DefenseItem() { }
            public DefenseItem(byte[] buffer)
            {
                this.SetItem(buffer);
            }
            public override string ToString()
            {
                return "������";
            }

            private string _name;

            public string Name
            {
                get { return this._name; }
                set { this._name = value; }
            }
            
            private bool _mtz;
            [XmlIgnore]
            public bool MTZ
            {
                get { return this._mtz; }
                set { this._mtz = value; }
            }
	

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public byte[] Values
            {
                get
                {
                    return this._values;
                }
                set
                {
                    this.SetItem(value);
                }
            }
            public void SetItem(byte[] buffer)
            {
                if (buffer.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("External defense item value", (object)LENGTH, "External defense item length must be 6");
                }
                this._values = buffer;
            }
            [XmlAttribute("�����")]
            [DisplayName("�����")]
            [Description("����� ������ ������")]
            [Category("��������� ���")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string Mode
            {
                get
                {
                    byte index = (byte)(this._values[0] & 0x0F);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set
                {
                    this._values[0] = Common.LOBYTE(Common.SetBits((ushort) this._values[0], (ushort)Strings.Modes.IndexOf(value), 0, 1, 2));
                }
            }

            [XmlAttribute("������������_�������")]
            [DisplayName("������������ �������")]
            [Description("������� ������������")]
            [Category("��������� ���")]
            public double WorkConstraint
            {
                get
                {
                    ConstraintKoefficient koeff = this.MTZ ? ConstraintKoefficient.K_6400 : ConstraintKoefficient.K_800;
                    return Measuring.GetConstraint(Common.TOWORD(this._values[2], this._values[1]), koeff);
                }
                set
                {
                    ConstraintKoefficient koeff = this.MTZ ? ConstraintKoefficient.K_6400 : ConstraintKoefficient.K_800;
                    ushort val  = Measuring.SetConstraint(value, koeff);
                    this._values[2] = Common.HIBYTE(val);
                    this._values[1] = Common.LOBYTE(val);
                    
                }
            }

            [XmlAttribute("������������_�����")]
            [DisplayName("����� ������������")]
            [Description("�������� ������� ������������ ��")]
            [Category("��������� ���")]
            public ulong WorkingTime
            {
                get
                {
                    ulong time = Measuring.GetTime(Common.TOWORD(this._values[4], this._values[3]));
                    if ("���������" == this.Feature)
                    {
                        time /= 10;
                    }
                    return time;
                }
                set
                {
                    ushort val = Measuring.SetTime(value);
                    if ("���������" == this.Feature)
                    {
                        val *= 10;
                    }
                    this._values[4] = Common.HIBYTE(val);
                    this._values[3] = Common.LOBYTE(val);
                }
            }

            [XmlAttribute("��������������")]
            [DisplayName("��������������")]
            [Category("��������� ���")]
            [TypeConverter(typeof(FeatureTypeConverter))]
            public string Feature
            {
                get
                {
                    return Common.GetBit((ushort) this._values[0], 4) ? Strings.FeatureI[1] : Strings.FeatureI[0];
                }
                set
                {
                    this._values[0] =Common.LOBYTE(Common.SetBits((ushort) this._values[0], (ushort)Strings.FeatureI.IndexOf(value), 4));
                }
            }

            [XmlAttribute("���")]
            [DisplayName("���")]
            [Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool APV
            {
                get
                {
                    return Common.GetBit((ushort) this._values[0], 6);
                }
                set
                {
                    this._values[0] = Common.LOBYTE(Common.SetBit((ushort) this._values[0], 6, value));
                }
            }
            [XmlAttribute("����")]
            [DisplayName("����")]
            [Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool UROV
            {
                get
                {
                    return Common.GetBit((ushort) this._values[0], 7);
                }
                set
                {
                    this._values[0] = Common.LOBYTE(Common.SetBit((ushort) this._values[0], 7, value));
                }
            }
            [XmlAttribute("U")]
            [DisplayName("U")]
            [Category("��������� ���")]
            [TypeConverter(typeof(BooleanTypeConverter))]
            public bool U
            {
                get
                {
                    return Common.GetBit((ushort) this._values[0], 5);
                }
                set
                {
                    this._values[0] = Common.LOBYTE(Common.SetBit((ushort) this._values[0], 5, value));
                }
            }

        }

        private CDefenses _defenses = new CDefenses();
        
        [XmlElement("������")]
        [DisplayName("������")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [Category("������")]
        public CDefenses Defenses
        {
            get { return this._defenses; }
            set { this._defenses = value; }
        }
     

        #endregion

        #region ����������������
        private slot _telesignalization = new slot(0, 3);
        public event Handler TelesignalizationLoadOk;
        public event Handler TelesignalizationLoadFail;
        public void LoadTelesignalizationCycle()
        {
            LoadBitSlotCycle(DeviceNumber, this._telesignalization, "LoadTelesignalization" + DeviceNumber, new TimeSpan(100), 10, this);
        }

        public void MakeTelesignalizationQuick()
        {
            MakeQueryQuick("LoadTelesignalization" + DeviceNumber);
        }
        public void MakeTelesignalizationSlow()
        {
            MakeQuerySlow("LoadTelesignalization" + DeviceNumber);
        }

        public void RemoveTelesignalization()
        {
            this.MB.RemoveQuery("LoadTelesignalization" + DeviceNumber);
        }

        [XmlIgnore]
        [DisplayName("������� �������")]
        [Category("����������������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleInputs
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(this._telesignalization.Value[0])});
            }
        }

        [XmlIgnore]
        [DisplayName("��������� ������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleDefenses
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._telesignalization.Value[0]) });
            }
        }

        [XmlIgnore]
        [DisplayName("�������� ����")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleRele
        {
            get
            {
                return new BitArray(new byte[] { Common.LOBYTE(this._telesignalization.Value[1]) });
            }
        }

        [XmlIgnore]
        [DisplayName("�������� �������")]
        [Category("����������������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleOutputs
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._telesignalization.Value[1]) });
            }
        }

         [XmlIgnore]
        [DisplayName("������������� ����������")]
        [Category("����������������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleDispepair
        {
            get
            {
                BitArray bits = new BitArray(new byte[] { Common.LOBYTE(this._telesignalization.Value[2]) });
                BitArray ret = new BitArray(4);
                for (int i = 0; i < 4;i++ )
                {
                    ret[i] = bits[i];
                }
                return ret;
            }
        }

        [XmlIgnore]
        [DisplayName("������ CRC")]
        [Category("����������������")]
        [Editor(typeof(LedEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianObjectConverter))]
        public BitArray TeleCRC_Errors
        {
            get
            {
                return new BitArray(new byte[] { Common.HIBYTE(this._telesignalization.Value[2]) });
            }
        }


        #endregion

        #region ���������� �������
        private slot _analog = new slot(0x1000, 0x100A);

        public void LoadAnalogSignalsCycle()
        {
            LoadSlotCycle(DeviceNumber, this._analog, "LoadAnalogSignals" + DeviceNumber, new TimeSpan(1000), 10, this);
        }

        [Category("���������� �������")]
        public double Ia
        {
            get
            {
                return Measuring.GetI(this._analog.Value[0], this.TT, true);
            }
        }
        [Category("���������� �������")]
        public double Ib
        {
            get
            {
                return Measuring.GetI(this._analog.Value[1], this.TT, true);
            }
        }
        [Category("���������� �������")]
        public double Ic
        {
            get
            {
                return Measuring.GetI(this._analog.Value[2], this.TT, true);
            }
        }
        [Category("���������� �������")]
        public double Io
        {
            get
            {
                return Measuring.GetI(this._analog.Value[3], this.TTNP, false);
            }
        }
        [Category("���������� �������")]
        public double Ig
        {
            get
            {
                return Measuring.GetI(this._analog.Value[4], this.TTNP, false);
            }
        }
        [Category("���������� �������")]
        public double I1
        {
            get
            {
                return Measuring.GetI(this._analog.Value[7], this.TT, false);
            }
        }
        [Category("���������� �������")]
        public double I2
        {
            get
            {
                return Measuring.GetI(this._analog.Value[8], this.TT, false);
            }
        }
        [Category("���������� �������")]
        public double I0
        {
            get
            {
                return Measuring.GetI(this._analog.Value[9], this.TT, true);
            }
        }
        #endregion

        #region ��������� ������

        public const int SYSTEMJOURNAL_RECORD_CNT = 128;
        private slot[] _systemJournal = new slot[SYSTEMJOURNAL_RECORD_CNT];
        private CSystemJournal _systemJournalRecords = new CSystemJournal();
        private bool _stopSystemJournal = false;

        [Browsable(false)]
        [XmlIgnore]
        public CSystemJournal SystemJournal
        {
            get
            {
                return this._systemJournalRecords;
            }
        }

        public struct SystemRecord
        {
            public string msg;
            public string time;

        }

        public class CSystemJournal : ICollection
        {

            private List<SystemRecord> _messages = new List<SystemRecord>(SYSTEMJOURNAL_RECORD_CNT);

            public int Count
            {
                get
                {
                    return this._messages.Count;
                }
            }

            public SystemRecord this[int i]
            {
                get
                {
                    return this._messages[i];
                }
            }

            public void Clear()
            {
                this._messages.Clear();
            }        

            public bool AddMessage(byte[] value)
            {
                bool ret;
                SystemRecord msg = this.CreateMessage(value);
                if ((byte)0 == value[0] && (byte)0 == value[1] && (byte)0 == value[2])
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    this._messages.Add(msg);
                }
                return ret;

            }

            private SystemRecord CreateMessage(byte[] buffer)
            {
                SystemRecord msg = new SystemRecord();
                Common.SwapArrayItems(ref buffer);
                try
                {
                    msg.msg = Strings.SystemJournal[buffer[7]];
                }
                catch (ArgumentOutOfRangeException)
                {
                    msg.msg = "XXXXX";                    
                }
                
                msg.time = buffer[2].ToString("x2") + "-" + buffer[1].ToString("x2") + "-" + buffer[0].ToString("x2") + " " +
                          buffer[3].ToString("x2") + ":" + buffer[4].ToString("x2") + ":" + buffer[5].ToString("x2") + ":" + buffer[6].ToString("x2");
                return msg;
            }
            
            #region ICollection Members

            public void CopyTo(Array array, int index)
            {

            }

            public bool IsSynchronized
            {
                get { return false; }
            }

            public object SyncRoot
            {
                get { return this._messages; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._messages.GetEnumerator();
            }

            #endregion
        }
        #endregion

        #region ������ ������
        public const int ALARMJOURNAL_RECORD_CNT = 32;
        private slot[] _alarmJournal = new slot[ALARMJOURNAL_RECORD_CNT];
        private CAlarmJournal _alarmJournalRecords;
        private bool _stopAlarmJournal = false;
         
        [Browsable(false)]
        public CAlarmJournal AlarmJournal
        {
            get { return this._alarmJournalRecords; }
            set { this._alarmJournalRecords = value; }
        }

        public struct AlarmRecord
        {
            public string time;
            public string msg;
            public string code;
            public string type;
            public string value;
            public string defense;
            public string Ia;
            public string Ib;
            public string Ic;
            public string Io;
            public string I0;
            public string I1;
            public string I2;
            public string Ig;
        };

        public class CAlarmJournal
        {
            private Dictionary<ushort, string> _msgDictionary;
            private Dictionary<byte, string> _defenseDictionary;
            MR300 _device;

            public CAlarmJournal(MR300 device)
            {
                this._device = device;
                this._msgDictionary = new Dictionary<ushort, string>(5);
                this._defenseDictionary = new Dictionary<byte, string>();
                
                this._msgDictionary.Add(0, "������");
                this._msgDictionary.Add(1, "����������");
                this._msgDictionary.Add(2, "�����������");
                this._msgDictionary.Add(3, "���������� ���");
                this._msgDictionary.Add(255, "��� ���������");
                
                this._defenseDictionary.Add(1, "���-1");
                this._defenseDictionary.Add(2, "���-2");
                this._defenseDictionary.Add(3, "���-3");
                this._defenseDictionary.Add(4, "���-1");
                this._defenseDictionary.Add(5, "���-2");
                this._defenseDictionary.Add(6, "���-3");
                this._defenseDictionary.Add(7, "���");
                this._defenseDictionary.Add(8, "��-1");
                this._defenseDictionary.Add(9, "��-2");
            }

            private List<AlarmRecord> _messages = new List<AlarmRecord>(ALARMJOURNAL_RECORD_CNT);
            
            public AlarmRecord this[int i]
            {
                get { return this._messages[i]; }
            }

            public void Clear()
            {
                this._messages.Clear();
            }
            
            public bool AddMessage(byte[] value)
            {
                bool ret;
                Common.SwapArrayItems(ref value);
                AlarmRecord msg = this.CreateMessage(value);
              
                if (0 == value[0] && 0 == value[1] && 0 == value[2])
                {
                    ret = false;
                }
                else
                {
                    ret = true;
                    this._messages.Add(msg);
                }
                return ret;
            }

            private AlarmRecord CreateMessage(byte[] buffer)
            {
                AlarmRecord rec = new AlarmRecord();
                
                rec.time = buffer[2].ToString("x2") + "-" + buffer[1].ToString("x2") + "-" + buffer[0].ToString("x2") + " " +
                           buffer[3].ToString("x2") + ":" + buffer[4].ToString("x2") + ":" + buffer[5].ToString("x2") + ":" + buffer[6].ToString("x2");
                try
                {
                    byte msgByte = (byte)(buffer[7] & 0x7F);
                    rec.msg = this._msgDictionary[msgByte];
                    rec.defense = this._defenseDictionary[buffer[8]];
                    rec.value = Math.Round(Measuring.GetI(Common.TOWORD(buffer[11], buffer[10]), this._device.TT, true), 2).ToString("F2");
                    rec.type = (Common.GetBit(buffer[9], 7) ? "�� " : string.Empty) +
                               (Common.GetBit(buffer[9], 4) ? "A" : string.Empty) +
                               (Common.GetBit(buffer[9], 5) ? "B" : string.Empty) +
                               (Common.GetBit(buffer[9], 6) ? "C" : string.Empty);
                }
                catch (KeyNotFoundException)
                { }
                
                double Ia = Measuring.GetI(Common.TOWORD(buffer[13], buffer[12]), this._device.TT, true);
                double Ib = Measuring.GetI(Common.TOWORD(buffer[15], buffer[14]), this._device.TT, true);
                double Ic = Measuring.GetI(Common.TOWORD(buffer[17], buffer[16]), this._device.TT, true);
                double Io = Measuring.GetI(Common.TOWORD(buffer[19], buffer[18]), this._device.TTNP, false);
                double Ig = Measuring.GetI(Common.TOWORD(buffer[21], buffer[20]), this._device.TTNP, false);
                double I1 = Measuring.GetI(Common.TOWORD(buffer[27], buffer[26]), this._device.TT, true);
                double I2 = Measuring.GetI(Common.TOWORD(buffer[29], buffer[28]), this._device.TT, true);
                double I0 = Measuring.GetI(Common.TOWORD(buffer[31], buffer[30]), this._device.TT, true);


                rec.Ia = string.Format("{0:F2}", Ia);
                rec.Ib = string.Format("{0:F2}", Ib);
                rec.Ic = string.Format("{0:F2}", Ic);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.I1 = string.Format("{0:F2}", I1);
                rec.I2 = string.Format("{0:F2}", I2);
                rec.I0 = string.Format("{0:F2}", I0);
                rec.Io = string.Format("{0:F2}", Io);
                rec.Ig = string.Format("{0:F2}", Ig);
                
                return rec;
            }



        }
        #endregion

        #region ��������� �������

        #region ��������� ���� � ����������
        [XmlElement("��")]
        [DisplayName("��")]
        [Description("��������� ��� ��������������")]
        [Category("��������� ���� � ����������")]
        public ushort TT
        {
            get
            {
                return this._constraint.Value[1];
            }
            set
            {
                this._constraint.Value[1] = value;
            }
        }
        [XmlElement("����")]
        [DisplayName("����")]
        [Description("��������� ��� �������������� ������� ������������������")]
        [Category("��������� ���� � ����������")]
        public ushort TTNP
        {
            get
            {
                return this._constraint.Value[2];
            }
            set
            {
                this._constraint.Value[2] = value;
            }
        }
              
        #endregion

        #region �������� ����
        public class COutputRele : ICollection
        {            
            public const int COUNT = 5;
            public const int OFFSET = 0;
            public const int LENGTH = 16;

            public COutputRele()
            {
                this.SetBuffer(new ushort[LENGTH]);
            }
            private List<OutputReleItem> _releList = new List<OutputReleItem>(COUNT);

            [DisplayName("����������")]
            public int Count
            {
                get
                {
                    return this._releList.Count;
                }
            }

            public void SetBuffer(ushort[] values)
            {
                this._releList.Clear();
                if (values.Length != LENGTH)
                {
                    throw new ArgumentOutOfRangeException("Rele values", (object)LENGTH, "Output rele length must be 16");
                }


                byte[] bytes = Common.TOBYTES(values, true);

                this._releList.Add(new OutputReleItem(bytes[13], Common.SwapByte(values[14])));
                this._releList.Add(new OutputReleItem(bytes[14], Common.SwapByte(values[15])));
                this._releList.Add(new OutputReleItem(bytes[15], Common.SwapByte(values[13])));
                this._releList.Add(new OutputReleItem(bytes[11], Common.SwapByte(values[3])));
                this._releList.Add(new OutputReleItem(bytes[12], Common.SwapByte(values[4])));


                this._releList[1].IsShort = true;
            }

            public void MakeBuffer(ushort[] buffer)
            {                
                buffer[5] = Common.TOWORD(this[3].Value[0], Common.LOBYTE(buffer[5])); 
                buffer[6] = Common.TOWORD(this[0].Value[0], this[4].Value[0]);
                buffer[7] = Common.TOWORD(this[2].Value[0], this[1].Value[0]);

                buffer[14] = (ushort)(this[0].Impulse / 10);
                buffer[15] = (ushort)(this[1].Impulse / 10);
                buffer[13] = (ushort)(this[2].Impulse / 10);
                buffer[3] = (ushort)(this[3].Impulse / 10);
                buffer[4] = (ushort)(this[4].Impulse / 10); 
            }

            public OutputReleItem this[int i]
            {
                get
                {
                    return this._releList[i];
                }
                set
                {
                    this._releList[i] = value;
                }
            }

            #region ICollection Members

            public void Add(OutputReleItem item)
            {
                this._releList.Add(item);
            }

            public void CopyTo(Array array, int index)
            {

            }
            [Browsable(false)]
            public bool IsSynchronized
            {
                get { return false; }
            }
            [Browsable(false)]
            public object SyncRoot
            {
                get { return this; }
            }

            #endregion

            #region IEnumerable Members

            public IEnumerator GetEnumerator()
            {
                return this._releList.GetEnumerator();
            }

            #endregion
        };

        public class OutputReleItem
        {

            private ushort _impulse;
            private byte _type;

            public override string ToString()
            {
                return "�������� ����";
            }

            [XmlAttribute("��������")]
            [DisplayName("��������")]
            [Description("������ � ������� ����")]
            [Category("������� ���")]
            [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
            public byte[] Value
            {
                get
                {
                    return new byte[] {this._type, Common.HIBYTE(this._impulse),Common.LOBYTE(this._impulse) };
                }
                set
                {
                    this._type = value[0];
                    this._impulse = Common.TOWORD(value[1], value[2]);
                }
            }

            private bool _isShort = false;

            [XmlIgnore]
            [Browsable(false)]
            public bool IsShort
            {
                get { return this._isShort; }
                set { this._isShort = value; }
            }
	

            public OutputReleItem() { }

            public OutputReleItem(byte type, ushort impulse)
            {
                this._type = type;
                this._impulse = impulse;
            }

        
            [XmlAttribute("���")]
            [DisplayName("���")]
            [Description("��� ������� ����")]
            [Category("��������� ���")]
            [TypeConverter(typeof(ReleTypeConverter))]
            public string Type
            {
                get
                {
                    string ret = this._isShort ?  Strings.Rele2[Strings.Rele2.Count - 1] :  Strings.Rele[Strings.Rele.Count - 1];
                    try
                    {
                        ret = this._isShort ? Strings.Rele2[this._type] : Strings.Rele[this._type];
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        ret = this._isShort ? Strings.Rele2[Strings.Rele2.Count - 1] : Strings.Rele[Strings.Rele.Count - 1];
                    }
                    return ret;      
                
                }
                set
                {
                    byte index = this._isShort ? (byte)(Strings.Rele2.Count - 1) : (byte)(Strings.Rele.Count - 1);
                    try
                    {
                        index = this._isShort ? (byte)(Strings.Rele2.IndexOf(value)) : (byte)(Strings.Rele.IndexOf(value));
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        index = this._isShort ? (byte)(Strings.Rele2.Count - 1) : (byte)(Strings.Rele.Count - 1);
                    }
                    this._type = index;
                }
            }

          
            [XmlAttribute("�������")]
            [DisplayName("�������")]
            [Description("������������ �������� ����")]
            [Category("��������� ���")]
            public ulong Impulse
            {
                get { return (ulong)(this._impulse * 10); }
                set { this._impulse = (ushort)(value / 10); }
            }

         };

        COutputRele _outputRele = new COutputRele();
        [DisplayName("�������� ����")]
        [Editor(typeof(RussianCollectionEditor), typeof(UITypeEditor))]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [Category("��������� �������")]
        [XmlElement("��������_����")]
        public COutputRele OutputRele
        {
            get
            {
                return this._outputRele;
            }
        }

        #endregion
        
        [XmlElement("�����_�����_Umin")]
        [DisplayName("����� ����� Umin")]
        [Category("��������� �������")]
        [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
        public string Umin
        {
            get
            {
                byte index = Common.LOBYTE(this._constraint.Value[8]);                
                if (index >= Strings.DiskretAutomatic.Count)
                {
                    index = (byte)(Strings.DiskretAutomatic.Count - 1);
                }
                return Strings.DiskretAutomatic[index];
            }
            set {
                this._constraint.Value[8] = Common.TOWORD(Common.HIBYTE(this._constraint.Value[8]), (byte)Strings.DiskretAutomatic.IndexOf(value)); 
                }
        }

        [XmlElement("�����_�����_Uo")]
        [DisplayName("����� ����� Uo")]
        [Category("��������� �������")]
        [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
        public string Uo
        {
            get
            {
                byte index = Common.HIBYTE(this._constraint.Value[8]);
                if (index >= Strings.DiskretAutomatic.Count)
                {
                    index = (byte)(Strings.DiskretAutomatic.Count - 1);
                }
                return Strings.DiskretAutomatic[index];
            }
            set { this._constraint.Value[8] = Common.TOWORD((byte)Strings.DiskretAutomatic.IndexOf(value), Common.LOBYTE(this._constraint.Value[8])); }
        }

        [XmlElement("��������_��_�����")]
        [DisplayName("�������� �� �����")]
        [Category("��������� �������")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string OperationKey
        {
            get
            {
                byte index = (byte)(Common.LOBYTE(this._constraint.Value[0]) & 0x1);
                if (index >= Strings.Control.Count)
                {
                    index = (byte)(Strings.Control.Count - 1);
                }
                return Strings.Control[index];
            }
            set
            {
                this._constraint.Value[0] = (ushort)(Strings.Control.IndexOf(value) & 0x1);
            }
        }

        [XmlElement("��������_��_����")]
        [DisplayName("�������� �� ����")]
        [Category("��������� �������")]
        [TypeConverter(typeof(ControlTypeConverter))]
        public string OperationSDTU
        {
            get
            {
                byte index = (byte)((Common.LOBYTE(this._constraint.Value[32]) >> 1) & 0x1);
                if (index >= Strings.Control.Count)
                {
                    index = (byte)(Strings.Control.Count - 1);
                }
                return Strings.Control[index];
            }
            set
            {
                this._constraint.Value[32] = (ushort)((Strings.Control.IndexOf(value) << 1) & 0x2);
            }
        }

        [XmlElement("�����������_����������")]
        [DisplayName("����������� ���� ����������")]
        [Category("��������� �������")]
        [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
        public string SwitcherBlock
        {
            get
            {
                byte index = Common.HIBYTE(this._constraint.Value[9]);
                if (index >= Strings.DiskretAutomatic.Count)
                {
                    index = (byte)(Strings.DiskretAutomatic.Count - 1);
                }
                return Strings.DiskretAutomatic[index];
            }
            set { this._constraint.Value[9] = Common.TOWORD((byte)Strings.DiskretAutomatic.IndexOf(value), Common.LOBYTE(this._constraint.Value[9])); }
        }

            
        [XmlElement("�����������_�����_����������")]
        [DisplayName("����������� ����� ����������")]
        [Category("��������� �������")]
        public ulong SwitcherTime
        {
            get
            {
                return (ulong)(this._constraint.Value[10] * 10);
            }
            set
            {
                this._constraint.Value[10] = (ushort)(value / 10);
            }
        }

        [XmlElement("�����������_�������_����������")]
        [DisplayName("����������� ������� ����������")]
        [Category("��������� �������")]
        public ulong SwitcherImpulseOff
        {
            get
            {
                return (ulong)(this._constraint.Value[11] * 10);
            }
            set
            {
                this._constraint.Value[11] = (ushort)(value / 10);
            }
        }

        [XmlElement("�����������_�������_���������")]
        [DisplayName("����������� ������� ���������")]
        [Category("��������� �������")]
        public ulong SwitcherImpulseOn
        {
            get
            {
                return (ulong)(this._constraint.Value[12] * 10);
            }
            set
            {
                this._constraint.Value[12] = (ushort)(value / 10);
            }
        }

     

    
        #endregion

        #region ����������
        
        private CAutomatics _cautomatics = new CAutomatics();

        [XmlElement("����������")]
        [DisplayName("����������")]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        [Category("����������")]
        public CAutomatics Automatics
        {
            get { return this._cautomatics; }
            set { this._cautomatics = value; }
        }
             
        public class CAutomatics
        {
            public const int LENGTH = 32;
            public const int OFFSET = 32;
            private byte[] _buffer = new byte[LENGTH];
            public void SetBuffer(byte[] buffer)
            {
                this._buffer = buffer;
            }

            public byte[] ToBytes()
            {
                return this._buffer;
            }

            #region ���
            [XmlElement("������������_���")]
            [DisplayName("������������ ���")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string MTZ_Cnf
            {
                get
                {
                    byte index = (byte)(this._buffer[1] & (byte)0x3);
                    if (index >= Strings.ModesLight.Count)
                    {
                        index = (byte)(Strings.ModesLight.Count - 1);
                    }
                    return Strings.ModesLight[index];
                }
                set { this._buffer[1] = (byte)Strings.ModesLight.IndexOf(value); }
            }

            [XmlElement("�����_�����_���")]
            [DisplayName("����� ����� ���")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string MTZ_Entry
            {
                get
                {
                    byte index = this._buffer[2];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[2] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("������������_���������_���")]
            [DisplayName("������������ ��������� ���")]
            [Category("����������")]
            public ulong MTZ_Duration
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[4], this._buffer[3]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[4] = Common.HIBYTE(time);
                    this._buffer[3] = Common.LOBYTE(time);
                }
            }

            [XmlElement("�������_���������_���")]
            [DisplayName("������� ��������� ���")]
            [Category("����������")]
            public ulong MTZ_Constraint
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[6], this._buffer[5]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[6] = Common.HIBYTE(time);
                    this._buffer[5] = Common.LOBYTE(time);
                }
            }


            #endregion

            #region ���
            [XmlElement("������������_���")]
            [DisplayName("������������ ���")]
            [Category("����������")]
            [TypeConverter(typeof(CratTypeConverter))]
            public string APV_Cnf
            {
                get
                {
                    byte index = (byte)(this._buffer[7] & 0x3);
                    if (index >= Strings.Crat.Count)
                    {
                        index = (byte)(Strings.Crat.Count - 1);
                    }
                    return Strings.Crat[index];
                }
                set { this._buffer[7] = (byte)Strings.Crat.IndexOf(value); }
            }

            [XmlElement("�����_�����_���")]
            [DisplayName("����� ����� ���")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string APV_Entry
            {
                get
                {
                    byte index = this._buffer[8];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[8] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("�����_����������_���")]
            [DisplayName("����� ���������� ���")]
            [Category("����������")]
            public ulong APV_Blocking
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[10], this._buffer[9]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[10] = Common.HIBYTE(time);
                    this._buffer[9] = Common.LOBYTE(time);
                }
            }

            [XmlElement("�����_����������_���")]
            [DisplayName("����� ���������� ���")]
            [Category("����������")]
            public ulong APV_Ready
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[12], this._buffer[11]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[12] = Common.HIBYTE(time);
                    this._buffer[11] = Common.LOBYTE(time);
                }
            }

            [XmlElement("�����_1_�����_���")]
            [DisplayName("����� 1 ����� ���")]
            [Category("����������")]
            public ulong APV_1crat
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[14], this._buffer[13]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[14] = Common.HIBYTE(time);
                    this._buffer[13] = Common.LOBYTE(time);
                }
            }

            [XmlElement("�����_2_�����_���")]
            [DisplayName("����� 2 ����� ���")]
            [Category("����������")]
            public ulong APV_2crat
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[16], this._buffer[15]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[16] = Common.HIBYTE(time);
                    this._buffer[15] = Common.LOBYTE(time);
                }
            }


            #endregion

            #region ���
            [XmlElement("������������_���")]
            [DisplayName("������������ ���")]
            [Category("����������")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string ACR_Cnf
            {
                get
                {                    
                    byte index = (byte)(this._buffer[17] & 0x3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._buffer[17] = (byte)Strings.Modes.IndexOf(value); }
            }

            [XmlElement("�����_�����_���")]
            [DisplayName("����� ����� ���")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string ACR_Entry
            {
                get
                {
                    byte index = this._buffer[18];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[18] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("��������_���")]
            [DisplayName("��������_���")]
            [Category("����������")]
            public ulong ACR_Time
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[20], this._buffer[19]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[20] = Common.HIBYTE(time);
                    this._buffer[19] = Common.LOBYTE(time);
                }
            }
            
            #endregion

            #region ����
            [XmlElement("������������_����")]
            [DisplayName("������������ ����")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string CAPV_Cnf
            {
                get
                {
                    bool bit = Common.GetBit(this._buffer[17], 6);
                    return bit ? Strings.ModesLight[1] : Strings.ModesLight[0];
                }
                set
                {
                    bool bit = (value == Strings.ModesLight[1]);
                    this._buffer[17] = Common.LOBYTE(Common.SetBit(this._buffer[17], 6, bit));
                }
            }

            [XmlElement("�����_�����_����")]
            [DisplayName("����� ����� ����")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string CAPV_Entry
            {
                get
                {
                    byte index = this._buffer[21];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[21] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("��������_����")]
            [DisplayName("��������_����")]
            [Category("����������")]
            public ulong CAPV_Time
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[23], this._buffer[22]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[23] = Common.HIBYTE(time);
                    this._buffer[22] = Common.LOBYTE(time);
                }
            }

            #endregion

            #region ��1
            [XmlElement("������������_��1")]
            [DisplayName("������������ ��1")]
            [Category("����������")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string VZ1_Cnf
            {
                get
                {
                    byte index = (byte)(this._buffer[24] & 0x3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._buffer[24] = Common.LOBYTE(Common.SetBits(this._buffer[24], (ushort)Strings.Modes.IndexOf(value),0,1)); }
            }

            [XmlElement("����_��1")]
            [DisplayName("���� ��1")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string VZ1_UROV
            {
                get
                {
                    bool bit = Common.GetBit(this._buffer[24], 7);
                    return bit ? Strings.ModesLight[1] : Strings.ModesLight[0];
                }
                set
                {
                    bool bit = (value == Strings.ModesLight[1]);
                    this._buffer[24] = Common.LOBYTE(Common.SetBit(this._buffer[24], 7, bit));
                }
            }

            [XmlElement("���_��1")]
            [DisplayName("��� ��1")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string VZ1_APV
            {
                get
                {
                    bool bit = Common.GetBit(this._buffer[24], 6);
                    return bit ? Strings.ModesLight[1] : Strings.ModesLight[0];
                }
                set
                {
                    bool bit = (value == Strings.ModesLight[1]);
                    this._buffer[24] = Common.LOBYTE(Common.SetBit(this._buffer[24], 6, bit));
                }
            }

            [XmlElement("�����_�����_��1")]
            [DisplayName("����� ����� ��1")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string VZ1_Entry
            {
                get
                {
                    byte index = this._buffer[25];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[25] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("��������_��1")]
            [DisplayName("��������_��1")]
            [Category("����������")]
            public ulong VZ1_Time
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[27], this._buffer[26]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[27] = Common.HIBYTE(time);
                    this._buffer[26] = Common.LOBYTE(time);
                }
            }

            #endregion

            #region ��2
            [XmlElement("������������_��2")]
            [DisplayName("������������ ��2")]
            [Category("����������")]
            [TypeConverter(typeof(ModeTypeConverter))]
            public string VZ2_Cnf
            {
                get
                {
                    byte index = (byte)(this._buffer[28] & 0x3);
                    if (index >= Strings.Modes.Count)
                    {
                        index = (byte)(Strings.Modes.Count - 1);
                    }
                    return Strings.Modes[index];
                }
                set { this._buffer[28] = Common.LOBYTE(Common.SetBits(this._buffer[28], (ushort)Strings.Modes.IndexOf(value), 0, 1)); }
            }

            [XmlElement("����_��2")]
            [DisplayName("���� ��2")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string VZ2_UROV
            {
                get
                {
                    bool bit = Common.GetBit(this._buffer[28], 7);
                    return bit ? Strings.ModesLight[1] : Strings.ModesLight[0];
                }
                set
                {
                    bool bit = (value == Strings.ModesLight[1]);
                    this._buffer[28] = Common.LOBYTE(Common.SetBit(this._buffer[28], 7, bit));
                }
            }

            [XmlElement("���_��2")]
            [DisplayName("��� ��2")]
            [Category("����������")]
            [TypeConverter(typeof(ModeLightTypeConverter))]
            public string VZ2_APV
            {
                get
                {
                    bool bit = Common.GetBit(this._buffer[28], 6);
                    return bit ? Strings.ModesLight[1] : Strings.ModesLight[0];
                }
                set
                {
                    bool bit = (value == Strings.ModesLight[1]);
                    this._buffer[28] = Common.LOBYTE(Common.SetBit(this._buffer[28], 6, bit));
                }
            }

            [XmlElement("�����_�����_��2")]
            [DisplayName("����� ����� ��2")]
            [Category("����������")]
            [TypeConverter(typeof(DiskretAutomaticTypeConverter))]
            public string VZ2_Entry
            {
                get
                {
                    byte index = this._buffer[29];
                    if (index >= Strings.DiskretAutomatic.Count)
                    {
                        index = (byte)(Strings.DiskretAutomatic.Count - 1);
                    }
                    return Strings.DiskretAutomatic[index];
                }
                set { this._buffer[29] = (byte)Strings.DiskretAutomatic.IndexOf(value); }
            }

            [XmlElement("��������_��2")]
            [DisplayName("��������_��2")]
            [Category("����������")]
            public ulong VZ2_Time
            {
                get
                {
                    return Measuring.GetTime(Common.TOWORD(this._buffer[31], this._buffer[30]));
                }
                set
                {
                    ushort time = Measuring.SetTime(value);
                    this._buffer[31] = Common.HIBYTE(time);
                    this._buffer[30] = Common.LOBYTE(time);
                }
            }

            #endregion
                  
        };
                      
        #endregion
        
        #region �������
        public event Handler DateTimeLoadOk;
        public event Handler DateTimeLoadFail;
        public event Handler AnalogSignalsLoadOk;
        public event Handler AnalogSignalsLoadFail;
        public event Handler SystemJournalLoadOk;
        public event IndexHandler SystemJournalRecordLoadOk;
        public event IndexHandler SystemJournalRecordLoadFail;
        public event Handler AlarmJournalLoadOk;
        public event IndexHandler AlarmJournalRecordLoadOk;
        public event IndexHandler AlarmJournalRecordLoadFail;
        public event Handler ConstraintLoadOk;
        public event Handler ConstraintLoadFail;
        public event Handler ConstraintSaveOk;
        public event Handler ConstraintSaveFail;
        #endregion

        #region ������������ �������������

        public MR300()
        {
            HaveVersion = false;
        }

        public MR300(Modbus mb)
        {
            this.Init();
            this.MB = mb;
        }

        [XmlIgnore]
        [TypeConverter(typeof(RussianExpandableObjectConverter))]
        public override Modbus MB
        {
            get
            {
                return mb;
            }
            set
            {
                mb = value;
                if (null != mb)
                {
                    mb.CompleteExchange += this.mb_CompleteExchange;
                }
            }
        }
        
        private void Init()
        {
            HaveVersion = false;
            this._alarmJournalRecords = new CAlarmJournal(this);
            ushort start = 0x2801;
            ushort size = 0x4;
            for (int i = 0; i < SYSTEMJOURNAL_RECORD_CNT; i++)
            {
                this._systemJournal[i] = new slot((ushort)start, (ushort)(start + size));
                start += 1;
            }
            start = 0x2001;
            size = 0x10;
            for (int i = 0; i < ALARMJOURNAL_RECORD_CNT; i++)
            {
                this._alarmJournal[i] = new slot(start, (ushort)(start + size));
                start += 1;
            }
        }
        
        #endregion
       
        #region ���� - �����
        private slot _datetime = new slot(0x0900, 0x0907);
        public void LoadTimeCycle()
        {
            LoadSlotCycle(DeviceNumber, this._datetime, "LoadDateTime" + DeviceNumber, new TimeSpan(1000), 10, this);
        }
        [Browsable(false)]
        public byte[] DateTime
        {
            get
            {
                return Common.TOBYTES(this._datetime.Value, true);
            }
            set
            {
                this._datetime.Value = Common.TOWORDS(value, true);
            }
        }
        #endregion

        #region ������� ��������/����������
        
        public void RemoveAnalogSignals()
        {
            this.MB.RemoveQuery("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeAnalogSignalsQuick()
        {
            MakeQueryQuick("LoadAnalogSignals" + DeviceNumber);
        }

        public void MakeAnalogSignalsSlow()
        {
            MakeQuerySlow("LoadAnalogSignals");
        }

        public void RemoveDateTime()
        {
            this.MB.RemoveQuery("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeQuick()
        {
            MakeQueryQuick("LoadDateTime" + DeviceNumber);
        }

        public void MakeDateTimeSlow()
        {
            MakeQuerySlow("LoadDateTime" + DeviceNumber);
        }

        public void SuspendDateTime(bool suspend)
        {
            this.MB.SuspendQuery("LoadDateTime" + DeviceNumber, suspend);
        }

        public void RemoveAlarmJournal()
        {
            List<Query> query = this.MB.GetQueryByExpression("LoadALRecord(\\d+)_" + DeviceNumber);
            foreach (Query q in query)
            {
                this.MB.RemoveQuery(q.name);
            }
            this._stopAlarmJournal = true;

        }

        public void RemoveSystemJournal()
        {
            List<Query> query = this.MB.GetQueryByExpression("LoadSJRecord(\\d+)_" + DeviceNumber);
            foreach (Query q in query)
            {
                this.MB.RemoveQuery(q.name);
            }
            this._stopSystemJournal = true;
        }

        public void SaveDateTime()
        {
            SaveSlot(DeviceNumber, this._datetime, "SaveDateTime" + DeviceNumber, this);
        }

        public void LoadSystemJournal()
        {
            LoadSlot(DeviceNumber, this._systemJournal[0], "LoadSJRecord0_" + DeviceNumber, this);
            this._stopSystemJournal = false;
        }

        public void LoadAlarmJournal()
        {
            if (!this._constraint.Loaded)
            {
                this.LoadConstraint();
            }
            this._stopAlarmJournal = false;
            LoadSlot(DeviceNumber, this._alarmJournal[0], "LoadALRecord0_" + DeviceNumber, this);
        }
        
        #endregion

        #region ������������
        public void Deserialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(binFileName);
            }
            catch (XmlException)
            {
                throw new FileLoadException("���� ������� ��300 ���������", binFileName);
            }

            try
            {
                this.DeserializeSlot(doc, "/��300_�������/����_�����", this._datetime);
                this.DeserializeSlot(doc, "/��300_�������/�������", this._constraint);
                
            }
            catch (NullReferenceException)
            {
                throw new FileLoadException("���� ������� ��300 ���������", binFileName);
            }
            byte[] byteBuffer = Common.TOBYTES(this._constraint.Value, true);
            byte[] defensesBuffer = new byte[CDefenses.LENGTH];
            Array.ConstrainedCopy(byteBuffer, CDefenses.OFFSET, defensesBuffer, 0, CDefenses.LENGTH);
            Common.SwapArrayItems(ref defensesBuffer);
            this.Defenses.SetBuffer(defensesBuffer);

            byte[] autoBuffer = new byte[CAutomatics.LENGTH];
            Array.ConstrainedCopy(byteBuffer, CAutomatics.OFFSET, autoBuffer, 0, CAutomatics.LENGTH);
            Common.SwapArrayItems(ref autoBuffer);
            this.Automatics.SetBuffer(autoBuffer);

            ushort[] releBuffer = new ushort[COutputRele.LENGTH];
            Array.ConstrainedCopy(Common.TOWORDS(byteBuffer, false), COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
            this.OutputRele.SetBuffer(releBuffer);
            
        }

        void DeserializeSlot(XmlDocument doc, string nodePath, slot slot)
        {
            slot.Value = Common.TOWORDS(Convert.FromBase64String(doc.SelectSingleNode(nodePath).InnerText), true);
        }


        public void Serialize(string binFileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateElement("��300_�������"));

            this.OutputRele.MakeBuffer(this._constraint.Value);
            Array.ConstrainedCopy(Common.TOWORDS(this.Automatics.ToBytes(), false), 0, this._constraint.Value, CAutomatics.OFFSET / 2, CAutomatics.LENGTH / 2);
            Array.ConstrainedCopy(Common.TOWORDS(this.Defenses.ToByte(), false), 0, this._constraint.Value, CDefenses.OFFSET / 2, CDefenses.LENGTH / 2);
            
            this.SerializeSlot(doc, "����_�����", this._datetime);
            this.SerializeSlot(doc, "�������", this._constraint);
            doc.Save(binFileName);
        }

        void SerializeSlot(XmlDocument doc, string nodeName, slot slot)
        {
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(slot.Value, true));
            doc.DocumentElement.AppendChild(element);
        }
        #endregion
        
        #region INodeView Members

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof(MR300); }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr301; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "��300"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }
        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }

        #endregion
        
        protected override void mb_CompleteExchange(object sender, Query query)
        {
            //if (query.name == "version" + DeviceNumber)
            //{
            //    DeviceType = "MR300";
            //    LoadVersionOk();
            //}
            if ("LoadTelesignalization" + DeviceNumber == query.name)
            {
                Raise(query, this.TelesignalizationLoadOk, this.TelesignalizationLoadFail, ref this._telesignalization);
            }
            if ("SaveConstraint" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    if (null != this.ConstraintSaveOk)
                    {
                        this.ConstraintSaveOk(this);
                    }
                }
                else
                {
                    if (null != this.ConstraintSaveFail)
                    {
                        this.ConstraintSaveFail(this);
                    }
                }
            }
            if ("LoadConstraint" + DeviceNumber == query.name)
            {
                if (0 == query.fail)
                {
                    this._constraint.Value = Common.TOWORDS(query.readBuffer, true);
                    byte[] defensesBuffer = new byte[CDefenses.LENGTH];
                    Array.ConstrainedCopy(query.readBuffer, CDefenses.OFFSET, defensesBuffer, 0, CDefenses.LENGTH);
                    Common.SwapArrayItems(ref defensesBuffer);
                    this.Defenses.SetBuffer(defensesBuffer);

                    byte[] autoBuffer = new byte[CAutomatics.LENGTH];
                    Array.ConstrainedCopy(query.readBuffer, CAutomatics.OFFSET, autoBuffer, 0, CAutomatics.LENGTH);
                    Common.SwapArrayItems(ref autoBuffer);
                    this.Automatics.SetBuffer(autoBuffer);

                    ushort[] releBuffer = new ushort[COutputRele.LENGTH];
                    Array.ConstrainedCopy(Common.TOWORDS(query.readBuffer, false), COutputRele.OFFSET, releBuffer, 0, COutputRele.LENGTH);
                    this.OutputRele.SetBuffer(releBuffer);

                    this._constraint.Loaded = true;
                    if (null != this.ConstraintLoadOk)
                    {
                        this.ConstraintLoadOk(this);
                    }
                }
                else
                {
                    if (null != this.ConstraintLoadFail)
                    {
                        this.ConstraintLoadFail(this);
                    }
                }
            
                
            }
            
            #region ������ ������

            if (IsIndexQuery(query.name, "LoadALRecord"))
            {
                int index = GetIndex(query.name, "LoadALRecord");
                if (0 == query.fail)
                {                    
                    if (false == this._alarmJournalRecords.AddMessage(query.readBuffer))
                    {
                        if (null != this.AlarmJournalLoadOk)
                        {
                            this.AlarmJournalLoadOk(this);
                        }
                    }
                    else
                    {
                        if (null != this.AlarmJournalRecordLoadOk)
                        {
                            this.AlarmJournalRecordLoadOk(this, index);
                        }
                        index += 1;
                        if (index < ALARMJOURNAL_RECORD_CNT && !this._stopAlarmJournal)
                        {
                            LoadSlot(DeviceNumber, this._alarmJournal[index], "LoadALRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            if (null != this.AlarmJournalLoadOk)
                            {
                                this.AlarmJournalLoadOk(this);
                            }
                        }

                    }
                }
                else
                {
                    if (null != this.AlarmJournalRecordLoadFail)
                    {
                        this.AlarmJournalRecordLoadFail(this, index);
                    }
                }

            }
            #endregion

            #region ������ �������
            if (IsIndexQuery(query.name, "LoadSJRecord"))
            {
                int index = GetIndex(query.name, "LoadSJRecord");
                if (0 == query.fail)
                {
                    if (!this._systemJournalRecords.AddMessage(query.readBuffer))
                    {
                        this.SystemJournalLoadOk?.Invoke(this);
                    }
                    else
                    {
                        this.SystemJournalRecordLoadOk?.Invoke(this, index);
                        index += 1;
                        if (index < SYSTEMJOURNAL_RECORD_CNT && !this._stopSystemJournal)
                        {
                            LoadSlot(DeviceNumber, this._systemJournal[index], "LoadSJRecord" + index + "_" + DeviceNumber, this);
                        }
                        else
                        {
                            this.SystemJournalLoadOk?.Invoke(this);
                        }
                    }
                }
                else
                {
                    this.SystemJournalRecordLoadFail?.Invoke(this, index);
                }
            }
            #endregion
            
            if ("LoadDateTime" + DeviceNumber == query.name)
            {
                Raise(query, this.DateTimeLoadOk, this.DateTimeLoadFail, ref this._datetime);
            }
            
            if ("LoadAnalogSignals" + DeviceNumber == query.name)
            {
                if (!this._constraint.Loaded)
                {
                    this.LoadConstraint();
                }
                else
                {
                    Raise(query, this.AnalogSignalsLoadOk, this.AnalogSignalsLoadFail, ref this._analog);
                }
            }

            base.mb_CompleteExchange(sender, query);
        }
    }

}
