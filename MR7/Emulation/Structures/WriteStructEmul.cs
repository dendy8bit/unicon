﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR7.Configuration.Structures.Emulation;

namespace BEMN.MR7.Emulation.Structures
{
    public class WriteStructEmul : StructBase
    {
        #region [Private Fiels]
        
        public const int COUNT = 18;

        [Layout(0)] private TimeAndSignalStruct _timeAndSignal;
        [Layout(1, Count = COUNT)] private InputAnalogData[] _inputAnalog; // входные аналогивые данные
        [Layout(2)] private DiscretsSignals _diskretInput; // дискретные данные (64 сигнала)

        #endregion


        public TimeAndSignalStruct TimeSignal
        {
            get { return this._timeAndSignal; }
            set { this._timeAndSignal = value; }
        }

        /// <summary>
        /// Аналоговые сигналы
        /// </summary>
        [XmlElement(ElementName = "Аналоговые сигналы")]
        public InputAnalogData[] AllAnalogData
        {
            get { return this._inputAnalog; }
            set { this._inputAnalog = value; }
        }
        
        /// <summary>
        /// Список дискретных синалов
        /// </summary>
        [XmlElement(ElementName = "Список дискретных синалов")]
        public DiscretsSignals DiscretInputs
        {
            get { return this._diskretInput; }
            set { this._diskretInput = value; }
        }
    }
}


