﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using BEMN.MR7.Configuration;

namespace BEMN.MR7.Emulation.Structures
{
    public class TimeAndSignalStruct : StructBase
    {
        [Layout(0)] private ushort _statusTime; // 
        [Layout(1)] private ushort _signal; // сигнал в БД

        /// <summary>
        /// необходимо для обнуления 
        /// </summary>
        [XmlElement(ElementName = "Для сброса уставок")]
        public bool IsRestart
        {
            get { return true; }
            set { this._statusTime = Common.SetBit(this._statusTime, 15, value); }
        }


        /// <summary>
        /// необходимо для сброса расчета временм
        /// </summary>
        [XmlIgnore]
        public bool StopTime
        {
            get { return true; }
            set
            {
                if (value)
                {
                    this._statusTime = Common.SetBits(this._statusTime, 0, 0, 1);
                }

            }
        }
        /// <summary>
        /// необходимо для недопущения старта рассчета времени повторно,так как после выставления первого бита он автоматически сбрасывается (костыль)
        /// </summary>
        [XmlIgnore]
        public bool WriteTimeOk
        {
            set
            {
                this._statusTime = Common.SetBits(this._statusTime, 2, 0);
            }
        }

        #region [Properties] 

        /// <summary>
        /// Cтатус для расчета времени
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Cтатус для расчета времени")]
        public bool StatusTime
        {
            get
            {

                return Common.GetBit(this._statusTime, 0);
            }
            set
            {
                if (value)
                {
                    this._statusTime = 0;
                    this._statusTime = Common.SetBit(this._statusTime, 0, true);
                }
                else
                {
                    return;
                }
            }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Сигнал остановки отсчета времени")]
        public string Signal
        {
            get { return Validator.Get(this._signal, StringsConfig.RelaySignals); }
            set { this._signal = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        #endregion

    }
}
