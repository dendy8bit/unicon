﻿using System.Collections;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MBServer;

namespace BEMN.MR7.Emulation.Structures
{
    public class DiskretsSignalStruct : StructBase, IBitField//, IXmlSerializable
    {
        
        public const int LOGIC_COUNT = 4;

        public int DISCRETS_COUNT = 40;
        
        [Layout(0, Count = LOGIC_COUNT)] private ushort[] _discretsSignals;

        
        [BindingProperty(0)]
        [XmlIgnore]
        public BitArray Bits
        {
            get
            {
                var mass = Common.TOBYTES(this._discretsSignals, false);
                var result = new BitArray(mass);
                return result;
            }

            set
            {
                for (int i = 0; i < value.Count; i++)
                {
                    int x = i;
                    int y = i;
                    this._discretsSignals[x] = Common.SetBit(this._discretsSignals[x], y, value[i]);
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        //public void WriteXml(System.Xml.XmlWriter writer)
        //{

        //    for (int i = 0; i < DISCRETS_COUNT; i++)
        //    {
        //        writer.WriteElementString("Дискрет", this[i]);
        //    }
        //}

    }
}
