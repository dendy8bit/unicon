﻿namespace BEMN.MR7.Emulation
{
    partial class AnalogCtrI
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._amplitudeLabel = new System.Windows.Forms.Label();
            this._fazaLabel = new System.Windows.Forms.Label();
            this._frequencyLabel = new System.Windows.Forms.Label();
            this.IMTB = new System.Windows.Forms.MaskedTextBox();
            this.fazaMTB = new System.Windows.Forms.MaskedTextBox();
            this.FMTBA = new System.Windows.Forms.MaskedTextBox();
            this._amplitudeUp = new System.Windows.Forms.Button();
            this._amplitudeDown = new System.Windows.Forms.Button();
            this._fazaDown = new System.Windows.Forms.Button();
            this._fazaUp = new System.Windows.Forms.Button();
            this._frequencyDown = new System.Windows.Forms.Button();
            this._frequencyUp = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // _amplitudeLabel
            // 
            this._amplitudeLabel.AutoSize = true;
            this._amplitudeLabel.Location = new System.Drawing.Point(6, 7);
            this._amplitudeLabel.Name = "_amplitudeLabel";
            this._amplitudeLabel.Size = new System.Drawing.Size(10, 13);
            this._amplitudeLabel.TabIndex = 0;
            this._amplitudeLabel.Text = "I";
            // 
            // _fazaLabel
            // 
            this._fazaLabel.AutoSize = true;
            this._fazaLabel.Location = new System.Drawing.Point(125, 7);
            this._fazaLabel.Name = "_fazaLabel";
            this._fazaLabel.Size = new System.Drawing.Size(10, 13);
            this._fazaLabel.TabIndex = 1;
            this._fazaLabel.Text = "f";
            // 
            // _frequencyLabel
            // 
            this._frequencyLabel.AutoSize = true;
            this._frequencyLabel.Location = new System.Drawing.Point(245, 7);
            this._frequencyLabel.Name = "_frequencyLabel";
            this._frequencyLabel.Size = new System.Drawing.Size(13, 13);
            this._frequencyLabel.TabIndex = 2;
            this._frequencyLabel.Text = "F";
            // 
            // IMTB
            // 
            this.IMTB.Location = new System.Drawing.Point(31, 4);
            this.IMTB.Name = "IMTB";
            this.IMTB.Size = new System.Drawing.Size(64, 20);
            this.IMTB.TabIndex = 3;
            this.IMTB.Tag = "I";
            // 
            // fazaMTB
            // 
            this.fazaMTB.Location = new System.Drawing.Point(147, 4);
            this.fazaMTB.Name = "fazaMTB";
            this.fazaMTB.Size = new System.Drawing.Size(64, 20);
            this.fazaMTB.TabIndex = 4;
            // 
            // FMTBA
            // 
            this.FMTBA.Location = new System.Drawing.Point(271, 4);
            this.FMTBA.Name = "FMTBA";
            this.FMTBA.Size = new System.Drawing.Size(64, 20);
            this.FMTBA.TabIndex = 5;
            this.FMTBA.Text = "50";
            // 
            // _amplitudeUp
            // 
            this._amplitudeUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._amplitudeUp.Location = new System.Drawing.Point(94, 0);
            this._amplitudeUp.Name = "_amplitudeUp";
            this._amplitudeUp.Size = new System.Drawing.Size(22, 14);
            this._amplitudeUp.TabIndex = 122;
            this._amplitudeUp.Tag = "Ia";
            this._amplitudeUp.Text = "▲";
            this._amplitudeUp.UseMnemonic = false;
            this._amplitudeUp.UseVisualStyleBackColor = true;
            // 
            // _amplitudeDown
            // 
            this._amplitudeDown.BackColor = System.Drawing.SystemColors.Control;
            this._amplitudeDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._amplitudeDown.Location = new System.Drawing.Point(94, 13);
            this._amplitudeDown.Name = "_amplitudeDown";
            this._amplitudeDown.Size = new System.Drawing.Size(22, 14);
            this._amplitudeDown.TabIndex = 123;
            this._amplitudeDown.Tag = "Ia";
            this._amplitudeDown.Text = "\t▼";
            this._amplitudeDown.UseVisualStyleBackColor = false;
            // 
            // _fazaDown
            // 
            this._fazaDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaDown.Location = new System.Drawing.Point(210, 13);
            this._fazaDown.Name = "_fazaDown";
            this._fazaDown.Size = new System.Drawing.Size(22, 14);
            this._fazaDown.TabIndex = 125;
            this._fazaDown.Tag = "Ia";
            this._fazaDown.Text = "\t▼";
            this._fazaDown.UseVisualStyleBackColor = false;
            // 
            // _fazaUp
            // 
            this._fazaUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUp.Location = new System.Drawing.Point(210, 0);
            this._fazaUp.Name = "_fazaUp";
            this._fazaUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUp.TabIndex = 124;
            this._fazaUp.Tag = "Ia";
            this._fazaUp.Text = "▲";
            this._fazaUp.UseMnemonic = false;
            this._fazaUp.UseVisualStyleBackColor = true;
            // 
            // _frequencyDown
            // 
            this._frequencyDown.BackColor = System.Drawing.SystemColors.Control;
            this._frequencyDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._frequencyDown.Location = new System.Drawing.Point(332, 13);
            this._frequencyDown.Name = "_frequencyDown";
            this._frequencyDown.Size = new System.Drawing.Size(22, 14);
            this._frequencyDown.TabIndex = 127;
            this._frequencyDown.Tag = "Ia";
            this._frequencyDown.Text = "\t▼";
            this._frequencyDown.UseVisualStyleBackColor = false;
            // 
            // _frequencyUp
            // 
            this._frequencyUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._frequencyUp.Location = new System.Drawing.Point(332, 0);
            this._frequencyUp.Name = "_frequencyUp";
            this._frequencyUp.Size = new System.Drawing.Size(22, 14);
            this._frequencyUp.TabIndex = 126;
            this._frequencyUp.Tag = "Ia";
            this._frequencyUp.Text = "▲";
            this._frequencyUp.UseMnemonic = false;
            this._frequencyUp.UseVisualStyleBackColor = true;
            // 
            // AnalogCtrI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._frequencyDown);
            this.Controls.Add(this._frequencyUp);
            this.Controls.Add(this._fazaDown);
            this.Controls.Add(this._fazaUp);
            this.Controls.Add(this._amplitudeDown);
            this.Controls.Add(this._amplitudeUp);
            this.Controls.Add(this.FMTBA);
            this.Controls.Add(this.fazaMTB);
            this.Controls.Add(this.IMTB);
            this.Controls.Add(this._frequencyLabel);
            this.Controls.Add(this._fazaLabel);
            this.Controls.Add(this._amplitudeLabel);
            this.Name = "AnalogCtrI";
            this.Size = new System.Drawing.Size(367, 28);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _amplitudeLabel;
        private System.Windows.Forms.Label _fazaLabel;
        private System.Windows.Forms.Label _frequencyLabel;
        private System.Windows.Forms.MaskedTextBox IMTB;
        private System.Windows.Forms.MaskedTextBox fazaMTB;
        private System.Windows.Forms.MaskedTextBox FMTBA;
        private System.Windows.Forms.Button _amplitudeUp;
        private System.Windows.Forms.Button _amplitudeDown;
        private System.Windows.Forms.Button _fazaDown;
        private System.Windows.Forms.Button _fazaUp;
        private System.Windows.Forms.Button _frequencyDown;
        private System.Windows.Forms.Button _frequencyUp;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
