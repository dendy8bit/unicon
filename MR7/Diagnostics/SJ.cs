﻿using System;
using System.Xml.Serialization;

namespace BEMN.MR7.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР801_журнал_системы")]
    public class SystemJournal
    {
        [XmlElement(ElementName = "Номер")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "Время")]
        public string RecordTime { get; set; }
        [XmlElement(ElementName = "Сообщение")]
        public string RecordMessage { get; set; }

        public SystemJournal() { }

        public SystemJournal(string recordNumber, string recordTime, string recordMessage)
        {
            RecordNumber = recordNumber;
            RecordTime = recordTime;
            RecordMessage = recordMessage;
        }
    }
}
