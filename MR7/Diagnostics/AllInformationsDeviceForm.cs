﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Compressor;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR7.AlarmJournal;
using BEMN.MR7.AlarmJournal.Structures;
using BEMN.MR7.Configuration.Structures;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using BEMN.MR7.Configuration.Structures.Oscope;
using BEMN.MR7.Osc.HelpClasses;
using BEMN.MR7.Osc.Loaders;
using BEMN.MR7.Osc.Structures;
using BEMN.MR7.SystemJournal;
using BEMN.MR7.SystemJournal.Structures;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR7.Diagnostics
{
    public partial class AllInformationsDeviceForm : Form, IFormView
    {
        #region Main constant and verables

        private Mr7 _device;
        private MessageBoxForm _messageBoxForm;
        private FileDriver _fileDriver;

        private const string DEVICE_NAME = "MR801";
        private const string XML_HEAD = "MR7_SET_POINTS";
        private const string LIST_FILE_NAME = "jlist.xml";
        private const string ARH_NANE = "logarch.zip";
        private IEnumerable<CheckBox> _checkBoxs;

        //счетчик показа сообщений если равен 0 то показывае resultMessage
        private int _count;
        //счетчик читаемых осциллограмм
        private int _countOscForRead;

        #endregion

        #region SJ constant and verables

        private int _recordNumber;
        private List<SystemJournal> _systemJournalData;
        private readonly MemoryEntity<OneWordStruct> _indexSysJournal;
        private readonly MemoryEntity<SystemJournalStruct> _systemJournal;
        private JournalLoader _systemJournalLoader;

        #endregion

        #region AJ constant and verables

        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderAJ;
        private readonly List<AJ> _alarmJournalData;

        #endregion

        #region Configuration constant and verables

        private readonly MemoryEntity<ConfigurationStruct> _configuration;

        private ConfigurationStruct _currentSetpointsStruct;

        #endregion

        #region Osc constant and verables

        /// <summary>
        /// Загрузчик страниц
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// Загрузчик журнала
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoaderOsc;

        private readonly MemoryEntity<OscSettintsStruct> _oscilloscopeSettings;
        /// <summary>
        /// Данные осц
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;

        private MemoryEntity<OscopeAllChannelsStruct> _oscopeStruct;

        #endregion

        #region Constructor
        public AllInformationsDeviceForm()
        {
            InitializeComponent();
        }

        public AllInformationsDeviceForm(Mr7 device)
        {
            InitializeComponent();

            _device = device;
            _messageBoxForm = new MessageBoxForm();
            _checkBoxs = new List<CheckBox>();
            _oscCountMTB.MaxLength = 2;

            #region SJ 

            _systemJournalData = new List<SystemJournal>();

            if (this._device.Full1SysJournal024 != null)
            {
                this._systemJournalLoader = new JournalLoader(this._device.Full1SysJournal024, this._device.FullSysJournal64,
                    this._device.IndexSystemJournal);
                this._systemJournalLoader.AllJournalReadOk +=
                    HandlerHelper.CreateActionHandler(this, () => { this.AllReadSJRecords(); });
                this._systemJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this,
                    () =>
                    {
                        _count--;
                        if (_count == 0)
                        {
                            _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖС");
                        }
                        else
                        {
                            _messageBoxForm.ShowMessage("Ошибка сохранения ЖС");
                            Thread.Sleep(3000);
                        }
                    });
                //subscribe events
                this._systemJournal = null;

                this._systemJournalLoader.MessagesList = PropFormsStrings.GetNewJournalList();
            }
            else
            {
                this._systemJournalLoader = null;
                this._systemJournal = device.SystemJournal;
                this._indexSysJournal = device.IndexSystemJournal;
                this._indexSysJournal.AllWriteOk +=
                    HandlerHelper.CreateReadArrayHandler(this, () => { this.StartReadSystemJournal(); });
                this._indexSysJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () => 
                    {
                        _count--;
                        if (_count == 0)
                        {
                            _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖС");
                        }
                        else
                        {
                            _messageBoxForm.ShowMessage("Ошибка сохранения ЖС");
                            Thread.Sleep(3000);
                        }
                    });
                this._systemJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () => { this.ReadRecord(); });
                this._systemJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => 
                    {
                        _count--;
                        if (_count == 0)
                        {
                            _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖС");
                        }
                        else
                        {
                            _messageBoxForm.ShowMessage("Ошибка сохранения ЖС");
                            Thread.Sleep(3000);
                        }
                    });
                this._systemJournal.Value.MessagesList = PropFormsStrings.GetNewJournalList();
            }
           
            #endregion

            #region AJ

            _alarmJournalData = new List<AJ>();

            this._currentOptionsLoaderAJ = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoaderAJ.LoadOk += HandlerHelper.CreateActionHandler(this, this.StartReadJournal);
            this._currentOptionsLoaderAJ.LoadFail += HandlerHelper.CreateActionHandler(this, () =>
            {
                _count--;
                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖА");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Ошибка сохранения ЖА");
                    Thread.Sleep(3000);
                }
            });

            if (device.AllAlarmJournal != null)
            {
                this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageJa);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.AllReadAJRecords);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, () => 
                {
                    _count--;
                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖА");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Ошибка сохранения ЖА");
                        Thread.Sleep(3000);
                    }
                });

                _journalLoader.MessagesList = PropFormsStrings.GetNewAlarmList();
            }
            else
            {
                this._alarmJournal = device.AlarmJournal;
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadAJRecord);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _alarmJournal.RemoveStructQueries();
                    _count--;
                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖА");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Ошибка сохранения ЖА");
                        Thread.Sleep(3000);
                    }
                });
                this._setPageAlarmJournal = device.SetPageJa;
                this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
                this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    _setPageAlarmJournal.RemoveStructQueries();
                    _count--;
                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage("Ошибка сохранения ЖА");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Ошибка сохранения ЖА");
                        Thread.Sleep(3000);
                    }
                });

                _alarmJournal.Value.MessagesList = PropFormsStrings.GetNewAlarmList();
            }
            

            #endregion

            #region Configuration

            this._configuration = device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                _configuration.RemoveStructQueries();
                _count--;
                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage("Ошибка сохранения файла конфигурации");
                }
                else
                {
                    _messageBoxForm.ShowMessage("ООшибка сохранения файла конфигурации");
                    Thread.Sleep(3000);
                }
            });
            this._currentSetpointsStruct = new ConfigurationStruct();
            
            #endregion

            #region Osc

            // Загрузчик страниц
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, () => {});
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, this.OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, () =>
            {
                _count -= _countOscForRead;
                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage("Ошибка сохранения файла осциллограммы");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Ошибка сохранения файла осциллограммы");
                    Thread.Sleep(3000);
                }
            });
            // Загрузчик журнала
            if (device.AllOscJournal != null)
            {
                this._oscJournalLoader = new OscJournalLoader(device.AllOscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, () =>
                {
                    _oscCB.Enabled = false;
                    _messageBoxForm.ShowDialog();
                    _messageBoxForm.ShowResultMessage("Ошибка чтения записей осциллографа");
                });
            }
            else
            {
                this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal);
                this._oscJournalLoader.ReadJournalOk += HandlerHelper.CreateActionHandler(this, () => { });
                this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, ReadAllRecords);
                this._oscJournalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, () =>
                {
                    _oscCB.Enabled = false;
                    _messageBoxForm.ShowDialog();
                    _messageBoxForm.ShowResultMessage("Ошибка чтения записей осциллографа");
                    
                });
            }
            // Уставки осцилографа
            this._oscilloscopeSettings = device.OscOptions;
            this._oscilloscopeSettings.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                _oscJournalLoader.Clear();
                this._oscJournalLoader.StartReadJournal();
            });
            this._oscilloscopeSettings.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    _oscCB.Enabled = false;
                    _messageBoxForm.ShowDialog();
                    _messageBoxForm.ShowResultMessage("Ошибка чтения уставок осциллографа");
                });
            // Каналы осциллографа
            this._oscopeStruct = this._device.AllChannels;
            this._oscopeStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._oscilloscopeSettings.LoadStruct();
            });
            this._oscopeStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this,
                () =>
                {
                    _oscCB.Enabled = false;
                    _messageBoxForm.ShowDialog();
                    _messageBoxForm.ShowResultMessage("Ошибка чтения каналов осциллографа");
                });
            // Загрузчик уставок токов
            this._currentOptionsLoaderOsc = device.CurrentOptionsLoader;
            this._currentOptionsLoaderOsc.LoadOk += this._oscopeStruct.LoadStruct; // успешно прочитанная структура измерений вызовет метод чтения структуры
            this._currentOptionsLoaderOsc.LoadFail += () =>
            {
                _oscCB.Enabled = false;
                _messageBoxForm.ShowDialog();
                _messageBoxForm.ShowResultMessage("Ошибка чтения каналов осциллографа");
            };

            #endregion

            this._fileDriver = new FileDriver(this._device, this);
        }

        #endregion

        #region SJ Func

        /// <summary>
        /// Счётчик сообщений
        /// </summary>
        public int RecordNumber
        {
            get { return this._recordNumber; }
            set { this._recordNumber = value; }
        }

        private void AllReadSJRecords()
        {
            _count--;

            if (this._systemJournalLoader.JournalRecords.Count != 0)
            {
                for (int i = 0; i < this._systemJournalLoader.JournalRecords.Count; i++)
                {
                    SystemJournal sj = new SystemJournal((i + 1).ToString(),
                        this._systemJournalLoader.JournalRecords[i].GetRecordTime,
                        this._systemJournalLoader.JournalRecords[i].GetRecordMessage
                    );
                    _systemJournalData.Add(sj);
                }

                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(List<SystemJournal>));
                    using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Системы {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                    {
                        formatter.Serialize(file, _systemJournalData);
                    }

                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Файл ЖС сохранен");
                    }
                }
                catch (Exception e)
                {
                }
            }
            else
            {
                _sjCB.Enabled = false;
                //_messageBoxForm.ShowResultMessage("ЖC прочитан");
            }
            _fileDriver.CloseAll(OnClosedFiles);
        }

        private void ReadRecord()
        {
            if (!this._systemJournal.Value.IsEmpty)
            {
                this.RecordNumber++;
                SystemJournal sj = new SystemJournal(this.RecordNumber.ToString(CultureInfo.InvariantCulture),
                    this._systemJournal.Value.GetRecordTime,
                    this._systemJournal.Value.GetRecordMessage
                );
                _systemJournalData.Add(sj);


                this.SaveNumber();
            }
            else
            {
                _count--;
                try
                {
                    XmlSerializer formatter = new XmlSerializer(typeof(List<SystemJournal>));
                    using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Системы {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                    {
                        formatter.Serialize(file, _systemJournalData);
                    }

                    if (_count == 0)
                    {
                        _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                    }
                    else
                    {
                        _messageBoxForm.ShowMessage("Файл ЖС сохранен");
                    }
                }
                catch (Exception e)
                {
                }
                //_messageBoxForm.ShowResultMessage("ЖC прочитан");
            }
        }
        private void SaveNumber()
        {
            this._indexSysJournal.Value.Word = (ushort)this._recordNumber;
            this._indexSysJournal.SaveStruct();
        }
        private void StartReadSystemJournal()
        {
            this.RecordNumber = 0;
            this._systemJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        #endregion

        #region AJ Func

        private void StartReadJournal()
        {
            _messageBoxForm.ShowMessage("Сохранение файла ЖА");
            this._recordNumber = 0;
            if (this._device.AllAlarmJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }
        }

        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void StartReadOption()
        {
            this._currentOptionsLoaderAJ.StartRead();
        }

        private void ReadAJRecord()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ[record.GroupOfSetpoints];
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    
                            AJ alarmJournalItem = new AJ(
                            this._recordNumber.ToString(),
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56
                       );

                    _alarmJournalData.Add(alarmJournalItem);
                       

                    this.SavePageNumber();
                }
                else
                {
                    _jaCB.Enabled = false;
                }
            }
            catch (Exception e)
            {
            }
        }

        private void AllReadAJRecords()
        {
            _count--;
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                _jaCB.Enabled = false;
                return;
            }
            this._recordNumber = 1;
            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoaderAJ[record.GroupOfSetpoints];

                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    
                            AJ alarmJournalItem = new AJ(
                            this._recordNumber.ToString(),
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56
                           );
                    
                    _alarmJournalData.Add(alarmJournalItem);
                }

                catch (Exception e)
                {
                    
                }
                this._recordNumber++;
            }

            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<AJ>));
                using (FileStream file = new FileStream($"{folderBrowserDialog.SelectedPath}\\Журнал Аварий {_device.DeviceType} версия {_device.DeviceVersion}.xml", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(file, _alarmJournalData);
                }

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage($"Файл ЖА сохранен");
                }
                
            }
            catch (Exception e)
            {
            }
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            if (_device.AllAlarmJournal != null)
            {
                return record.TriggeredDefense == 71
                    ? _journalLoader.MessagesList[record.ValueOfTriggeredParametr]
                    : AjStrings.TriggeredDefense[record.TriggeredDefense];
            }
            return record.TriggeredDefense == 71
                ? _alarmJournal.Value.MessagesList[record.ValueOfTriggeredParametr]
                : AjStrings.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalRecordStruct record)
        {
            if (record.TriggeredDefense >= 50 && record.TriggeredDefense <= 65 || record.TriggeredDefense == 71 || record.TriggeredDefense == 45 || record.TriggeredDefense == 46)
            {
                return string.Empty;
            }
            return AjStrings.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, MeasureTransStruct measure)
        {
            // параметры выводятся согласно файлику jalm801 в моей папке/мр801 - коды сработавшего параметра

            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;

            if (parameter >= 0 && parameter <= 5)
            {
                return ValuesConverterCommon.Analog.GetI801Diff(value, measure.Sides[0].S, measure.Sides[0].U);
            }

            #region Side1
            if ((parameter >= 6 && parameter <= 10) || (parameter >= 12 && parameter <= 13))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[0].Ittl * 40);
            }

            if (parameter == 11)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[0].Ittx * 40);
            }
            #endregion

            #region Side2
            if ((parameter >= 14 && parameter <= 18) || (parameter >= 20 && parameter <= 21))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[1].Ittl * 40);
            }

            if (parameter == 19)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[1].Ittx * 40);
            }
            #endregion

            #region Side3

            if ((parameter >= 22 && parameter <= 26) || (parameter >= 28 && parameter <= 29))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[2].Ittl * 40);
            }

            if (parameter == 27)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[2].Ittx * 40);
            }

            #endregion

            #region Side4

            if ((parameter >= 30 && parameter <= 34) || (parameter >= 36 && parameter <= 37))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[3].Ittl * 40);
            }

            if (parameter == 35)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[3].Ittx * 40);
            }

            #endregion

            if (parameter == 75)
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetFullPower((short)value, (short)value1, measure.Uabc[0].KthValue, measure.Sides[0].Ittl);
            }

            if ((parameter >= 60) && (parameter <= 71))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1,
                    measure.Uabc[0].KthValue, measure.Sides[0].Ittl);
            }

            if (parameter >= 38 && parameter <= 45)
            {
                return ValuesConverterCommon.Analog.GetU801(value, measure.Uabc[0].KthValue);
            }

            if (parameter >= 46 && parameter <= 53)
            {
                return ValuesConverterCommon.Analog.GetU801(value, measure.Uabc[1].KthValue);
            }

            if (parameter == 54)
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.Uabc[1].KthValue);
            }

            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }

            if (parameter == 72)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }

            if (parameter >= 80 && parameter <= 84 || parameter == 73 || parameter >= 56 && parameter <= 59)
            {
                return string.Empty;
            }

            return value.ToString();
        }

        #endregion
        
        #region Configuration Func

        private void ConfigurationReadOk()
        {
            _count--;

            this._currentSetpointsStruct = this._configuration.Value;
            
            Serialize($"{folderBrowserDialog.SelectedPath}\\Конфигурация {_device.DeviceType} версия {_device.DeviceVersion}.xml");
        }

        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR7_SET_POINTS"));
                ushort[] values;

                values = this._currentSetpointsStruct.GetValues();
                
                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Файл конфигурации сохранен");
                }
            }
            catch
            {
                
            }
        }

        private void StartRead()
        {
            _messageBoxForm.ShowMessage("Сохранение файла конфигурации");
            
            this._configuration.LoadStruct();
        }

        #endregion

        #region Programming Func

        private void LogicReadOfDevice(byte[] readBytes, string mess)
        {
            _count--;

            if (readBytes != null && readBytes.Length != 0 && mess == "Операция успешно выполнена")
            {
                this.LoadProjectFromBin(this.Uncompress(readBytes));
            }
            else
            {
                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage("Файл логики не найден");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Файл логики не найден");
                    Thread.Sleep(3000);
                }
            }
            _fileDriver.CloseAll(OnClosedFiles);
        }
        private byte[] Uncompress(byte[] readBytes)
        {
            ushort[] readData = Common.TOWORDS(readBytes, false);
            ZIPCompressor compr = new ZIPCompressor();
            byte[] compressed = new byte[readData[0]];
            for (int i = 3; i < (compressed.Length + 1) / 2 + 3; i++)
            {
                if ((i - 2) * 2 + 1 < compressed.Length)
                {
                    compressed[(i - 3) * 2 + 1] = (byte)(readData[i] >> 8);
                }
                compressed[(i - 3) * 2] = (byte)readData[i];
            }
            return compr.Decompress(compressed);
        }

        private void LoadProjectFromBin(byte[] uncompressed)
        {
            //Создаем файл логики, чтобы с ним работать
            try
            {
                using (FileStream fs = new FileStream($"{folderBrowserDialog.SelectedPath}\\logic.xml",
                    FileMode.Create, FileAccess.Write,
                    FileShare.None, uncompressed.Length,
                    false))
                {
                    fs.Write(uncompressed, 0, uncompressed.Length);
                    fs.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load($"{folderBrowserDialog.SelectedPath}\\logic.xml");

                XmlNodeList aNodes = doc.SelectNodes("//Source/SchematicData");
                
                if (aNodes != null)
                {
                    foreach (XmlNode aNode in aNodes)
                    {
                        XmlAttribute device = doc.CreateAttribute("device");
                        device.Value = DEVICE_NAME;
                        aNode.Attributes?.Append(device);
                    }
                }

                doc.Save($"{folderBrowserDialog.SelectedPath}\\Проект логики {_device.DeviceType} версия {_device.DeviceVersion}.prj");

                //Удаляем файл т.к. он больше не нужен
                File.Delete($"{folderBrowserDialog.SelectedPath}\\logic.xml");

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage("Файл логики сохранен");
                }
            }
            catch (Exception e)
            {
            }
        }

        #endregion

        #region Osc Func

        /// <summary>
        /// Осцилограмма успешно загружена из устройства
        /// </summary>
        private void OscReadOk()
        {
            _count--;
            _countOscForRead++;

            int group = this._journalStruct.GroupIndex;
            MeasureTransStruct measureTransform = this._currentOptionsLoaderAJ[group];
            try
            {
                this.CountingList = new CountingList(this._device, this._pageLoader.ResultArray, this._journalStruct, measureTransform);
                Directory.CreateDirectory($"{folderBrowserDialog.SelectedPath}\\Осцилограммы");
                string date = _oscJournalLoader.OscRecords[_countOscForRead - 1].GetDate;
                string time = _oscJournalLoader.OscRecords[_countOscForRead - 1].GetTime.Replace(":", ".");
                Directory.CreateDirectory($"{folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}");
                _countingList.Save($"{folderBrowserDialog.SelectedPath}\\Осцилограммы\\Оциллограмма {date} {time}\\Осциллограмма_МР801");

                if (_countOscForRead < Convert.ToInt32(_oscCountMTB.Text))
                {
                    this._journalStruct = this._oscJournalLoader.OscRecords[_countOscForRead];
                    this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
                }

                if (_count == 0)
                {
                    _messageBoxForm.ShowResultMessage($"Сохранение данных завершено успешно");
                }
                else
                {
                    _messageBoxForm.ShowMessage($"Осциллограмма {_countOscForRead} сохранена");
                }
            }
            catch(Exception ex)
            {
                _messageBoxForm.ShowResultMessage("Ошибка сохранения осциллограммы");
            }
        }

        /// <summary>
        /// Данные осц
        /// </summary>
        public CountingList CountingList
        {
            get { return this._countingList; }
            set
            {
                this._countingList = value;
            }
        }

        /// <summary>
        /// Прочитан весь журнал
        /// </summary>
        private void ReadAllRecords()
        {
            _oscCB.Enabled = _oscJournalLoader.OscCount != 0;
            _oscCountMTB.Enabled = _oscJournalLoader.OscCount != 0;
            _oscCountMTB.Text = _oscJournalLoader.OscCount != 0 ? "1" : "";

            _oscInDeviceLabel.Text = $"В устройстве {_oscJournalLoader.OscCount} осц.";
        }

        private void StartReadOsc()
        {
            this._journalStruct = this._oscJournalLoader.OscRecords[_countOscForRead];
            this._pageLoader.StartRead(this._journalStruct, this._oscilloscopeSettings.Value);
        }

        #endregion

        #region Main Func
        private void OnClosedFiles(bool res, string message)
        {
            if (!res) _messageBoxForm.ShowMessage(@"Не удалось закрыть открытые файлы в устройстве. " + message);
        }

        public void SaveInfo(IEnumerable<System.Windows.Forms.CheckBox> checkBoxs)
        {
            _alarmJournalData.Clear();
            _systemJournalData.Clear();
            _count = 0;
            _countOscForRead = 0;

            foreach (var item in checkBoxs)
            {
                if (item.Checked && item.Text == "Журнал Системы")
                {
                    _count++;
                    StartReadSJ();
                }

                if (item.Checked && item.Text == "Журнал Аварий")
                {
                    _count++;
                    this.StartReadOption();
                }

                if (item.Checked && item.Text == "Конфигурация")
                {
                    _count++;
                    this.StartRead();
                }

                if (item.Checked && item.Text == "Программирование (логика)")
                {
                    _count++;
                    this._fileDriver.ReadFile(this.LogicReadOfDevice, ARH_NANE);
                }

                if (item.Checked && item.Text == "Осциллограммы")
                {
                    _count += Convert.ToInt32(_oscCountMTB.Text);
                    StartReadOsc();
                }
            }

            this._messageBoxForm.SetProgressBarStyle(ProgressBarStyle.Marquee);
            this._messageBoxForm.OkBtnVisibility = false;
            this._messageBoxForm.ShowDialog("Идет сохранение данных");
        }

        private void StartReadSJ()
        {
            if (this._device.Full1SysJournal024 != null)
            {
                //_messageBoxForm.ShowMessage("Сохранение файла ЖС");
                this._systemJournalLoader.StartRead();
            }
            else
            {
                //_messageBoxForm.ShowMessage("Чтение ЖС");
                this._indexSysJournal.Value.Word = 0;
                this._indexSysJournal.SaveStruct();
            }
        }

        private void CheckCB()
        {
            _checkBoxs = Controls.OfType<System.Windows.Forms.CheckBox>();

            _saveDataButton.Enabled = _checkBoxs.Any(ch => ch.Checked);
        }

        private void saveData_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                SaveInfo(_checkBoxs);
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            CheckCB();
        }

        private void AllInformationsDeviceForm_Load(object sender, EventArgs e)
        {
            //StartReadSJ();
            //this.StartReadOption();
            this._currentOptionsLoaderOsc.StartRead(); // читает первую запись
            CheckCB();
        }

        private void _oscCountMTB_TextChanged(object sender, EventArgs e)
        {
            _lastOscLabel.Visible = _oscCountMTB.Text == "1";

            try
            {
                if ( _oscCountMTB.Text.Any(char.IsLetter))
                {
                    _oscCountMTB.Text = _oscCountMTB.Text.Remove(_oscCountMTB.Text.Length - 1);
                    _oscCountMTB.SelectionStart = _oscCountMTB.Text.Length;
                }

                if (Convert.ToInt32(_oscCountMTB.Text) > _oscJournalLoader.OscCount)
                {
                    _oscCountMTB.Text = _oscJournalLoader.OscCount.ToString();
                }

                if (_oscCountMTB.Text == "" || _oscCountMTB.Text == "0")
                {
                    _oscCountMTB.Text = "1";
                }
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

        #region IFormView

        public Type FormDevice => typeof(Mr7);

        public bool Multishow { get; private set; }

        public Type ClassType => typeof(AllInformationsDeviceForm);

        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.information_icon.ToBitmap();

        public string NodeName => "Данные из устройства";

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => false;

        #endregion
    }
}
