﻿using System;
using System.Xml.Serialization;

namespace BEMN.MR7.Diagnostics
{
    [Serializable]
    [XmlRoot("DocumentElement")]
    [XmlType("МР801_журнал_аварий")]
    public class AJ
    {
        [XmlElement(ElementName = "№")]
        public string RecordNumber { get; set; }
        [XmlElement(ElementName = "Дата/Время")]
        public string Time { get; set; }
        [XmlElement(ElementName = "Сообщение")]
        public string Message { get; set; }
        [XmlElement(ElementName = "Сработавшая защита")]
        public string TriggeredDefence { get; set; }
        [XmlElement(ElementName = "Параметр срабатывания")]
        public string Parametr { get; set; }
        [XmlElement(ElementName = "Значение параметра срабатывания")]
        public string ParametrValue { get; set; }
        [XmlElement(ElementName = "Группа уставок")]
        public string Group { get; set; }
        [XmlElement(ElementName = "Ia диф.")]
        public string Ida { get; set; }
        [XmlElement(ElementName = "Ib диф.")]
        public string Idb { get; set; }
        [XmlElement(ElementName = "Ic диф.")]
        public string Idc { get; set; }
        [XmlElement(ElementName = "Ia торм.")]
        public string Ita { get; set; }
        [XmlElement(ElementName = "Ib торм.")]
        public string Itb { get; set; }
        [XmlElement(ElementName = "Ic торм.")]
        public string Itc { get; set; }
        [XmlElement(ElementName = "Ia s1")]
        public string Is1a { get; set; }
        [XmlElement(ElementName = "Ib s1")]
        public string Is1b { get; set; }
        [XmlElement(ElementName = "Ic s1")]
        public string Is1c { get; set; }
        [XmlElement(ElementName = "3I0 s1")]
        public string Is10 { get; set; }
        [XmlElement(ElementName = "I2 s1")]
        public string Is12 { get; set; }
        [XmlElement(ElementName = "In s1")]
        public string Is1n { get; set; }
        [XmlElement(ElementName = "Ia s2")]
        public string Is2a { get; set; }
        [XmlElement(ElementName = "Ib s2")]
        public string Is2b { get; set; }
        [XmlElement(ElementName = "Ic s2")]
        public string Is2c { get; set; }
        [XmlElement(ElementName = "3I0 s2")]
        public string Is20 { get; set; }
        [XmlElement(ElementName = "I2 s2")]
        public string Is22 { get; set; }
        [XmlElement(ElementName = "In s2")]
        public string Is2n { get; set; }
        [XmlElement(ElementName = "Ia s3")]
        public string Is3a { get; set; }
        [XmlElement(ElementName = "Ib s3")]
        public string Is3b { get; set; }
        [XmlElement(ElementName = "Ic s3")]
        public string Is3c { get; set; }
        [XmlElement(ElementName = "3I0 s3")]
        public string Is30 { get; set; }
        [XmlElement(ElementName = "I2 s3")]
        public string Is32 { get; set; }
        [XmlElement(ElementName = "In s3")]
        public string Is3n { get; set; }
        [XmlElement(ElementName = "Ia s4")]
        public string Is4a { get; set; }
        [XmlElement(ElementName = "Ib s4")]
        public string Is4b { get; set; }
        [XmlElement(ElementName = "Ic s4")]
        public string Is4c { get; set; }
        [XmlElement(ElementName = "3I0 s4")]
        public string Is40 { get; set; }
        [XmlElement(ElementName = "I2 s4")]
        public string Is42 { get; set; }
        [XmlElement(ElementName = "In s4")]
        public string Is4n { get; set; }
        [XmlElement(ElementName = "Ua1")]
        public string U1a { get; set; }
        [XmlElement(ElementName = "Ub1")]
        public string U1b { get; set; }
        [XmlElement(ElementName = "Uc1")]
        public string U1c { get; set; }
        [XmlElement(ElementName = "Uab1")]
        public string U1ab { get; set; }
        [XmlElement(ElementName = "Ubc1")]
        public string U1bc { get; set; }
        [XmlElement(ElementName = "Uca1")]
        public string U1ca { get; set; }
        [XmlElement(ElementName = "U2-1")]
        public string U12 { get; set; }
        [XmlElement(ElementName = "3U0-1")]
        public string U10 { get; set; }
        [XmlElement(ElementName = "Ua2")]
        public string U2a { get; set; }
        [XmlElement(ElementName = "Ub2")]
        public string U2b { get; set; }
        [XmlElement(ElementName = "Uc2")]
        public string U2c { get; set; }
        [XmlElement(ElementName = "Uab2")]
        public string U2ab { get; set; }
        [XmlElement(ElementName = "Ubc2")]
        public string U2bc { get; set; }
        [XmlElement(ElementName = "Uca2")]
        public string U2ca { get; set; }
        [XmlElement(ElementName = "U2-2")]
        public string U22 { get; set; }
        [XmlElement(ElementName = "3U0-2")]
        public string U20 { get; set; }
        [XmlElement(ElementName = "F")]
        public string F { get; set; }
        [XmlElement(ElementName = "Un")]
        public string Un { get; set; }
        [XmlElement(ElementName = "Д1-Д8")]
        public string D1To8 { get; set; }
        [XmlElement(ElementName = "Д9-Д16")]
        public string D9To16 { get; set; }
        [XmlElement(ElementName = "Д17-Д24")]
        public string D17To24 { get; set; }
        [XmlElement(ElementName = "Д25-Д32")]
        public string D25To32 { get; set; }
        [XmlElement(ElementName = "Д33-Д40")]
        public string D33To40 { get; set; }
        [XmlElement(ElementName = "Д41-Д48")]
        public string D41To48 { get; set; }
        [XmlElement(ElementName = "Д49-Д56")]
        public string D49To56 { get; set; }

        public AJ() { }

        public AJ(string recordNumber, string time, string message, string triggeredDefence, string parametr,
            string parametrValue, string group, string ida, string idb, string idc, string ita, string itb,string itc, 
            string is1a, string is1b, string is1c, string is10, string is12, string is1n,
            string is2a, string is2b, string is2c, string is20, string is22, string is2n,
            string is3a, string is3b, string is3c, string is30, string is32, string is3n,
            string is4a, string is4b, string is4c, string is40, string is42, string is4n,
            string u1a, string u1b, string u1c, string u1ab, string u1bc, string u1ca, string u12, string u10,
            string u2a, string u2b, string u2c, string u2ab, string u2bc, string u2ca, string u22, string u20,
            string f, string un, string d1to8, string d9to16, string d17to24, string d25to32, string d33to40, 
            string d41to48, string d49to56)
        {
            RecordNumber = recordNumber;
            Time = time;
            Message = message;
            TriggeredDefence = triggeredDefence;
            Parametr = parametr;
            ParametrValue = parametrValue;
            Group = group;

            Ida = ida;
            Idb = idb;
            Idc = idc;
            Ita = ita;
            Itb = itb;
            Itc = itc;

            Is1a = is1a;
            Is1b = is1b;
            Is1c = is1c;
            Is10 = is10;
            Is12 = is12;
            Is1n = is1n;

            Is2a = is2a;
            Is2b = is2b;
            Is2c = is2c;
            Is20 = is20;
            Is22 = is22;
            Is2n = is2n;

            Is3a = is3a;
            Is3b = is3b;
            Is3c = is3c;
            Is30 = is30;
            Is32 = is32;
            Is3n = is3n;

            Is4a = is4a;
            Is4b = is4b;
            Is4c = is4c;
            Is40 = is40;
            Is42 = is42;
            Is4n = is4n;

            U1a = u1a;
            U1b = u1b;
            U1c = u1c;
            U1ab = u1ab;
            U1bc = u1bc;
            U1ca = u1ca;
            U12 = u12;
            U10 = u10;

            U2a = u2a;
            U2b = u2b;
            U2c = u2c;
            U2ab = u2ab;
            U2bc = u2bc;
            U2ca = u2ca;
            U22 = u22;
            U20 = u20;

            F = f;
            Un = un;

            D1To8 = d1to8;
            D9To16 = d9to16;
            D17To24 = d17to24;
            D25To32 = d25to32;
            D33To40 = d33to40;
            D41To48 = d41to48;
            D49To56 = d49to56;
        }

        //public AJ(string recordNumber, string time, string message, string triggeredDefence, string parametr,
        //    string parametrValue, string group, string ida, string idb, string idc, string ita, string itb, string itc,
        //    string is1a, string is1b, string is1c, string is10, string is12, string is1n,
        //    string is2a, string is2b, string is2c, string is20, string is22, string is2n,
        //    string is3a, string is3b, string is3c, string is30, string is32, string is3n,
        //    string is4a, string is4b, string is4c, string is40, string is42, string is4n,
        //    string u1a, string u1b, string u1c, string u1ab, string u1bc, string u1ca, string u12, string u10,
        //    string u2a, string u2b, string u2c, string u2ab, string u2bc, string u2ca, string u22, string u20,
        //    string f, string un, string d1to8, string d9to16, string d17to24)
        //{
        //    RecordNumber = recordNumber;
        //    Time = time;
        //    Message = message;
        //    TriggeredDefence = triggeredDefence;
        //    Parametr = parametr;
        //    ParametrValue = parametrValue;
        //    Group = group;

        //    Ida = ida;
        //    Idb = idb;
        //    Idc = idc;
        //    Ita = ita;
        //    Itb = itb;
        //    Itc = itc;

        //    Is1a = is1a;
        //    Is1b = is1b;
        //    Is1b = is1b;
        //    Is10 = is10;
        //    Is12 = is12;
        //    Is1n = is1n;

        //    Is2a = is2a;
        //    Is2b = is2b;
        //    Is2b = is2b;
        //    Is20 = is20;
        //    Is22 = is22;
        //    Is2n = is2n;

        //    Is3a = is3a;
        //    Is3b = is3b;
        //    Is3b = is3b;
        //    Is30 = is30;
        //    Is32 = is32;
        //    Is3n = is3n;

        //    Is4a = is4a;
        //    Is4b = is4b;
        //    Is4b = is4b;
        //    Is40 = is40;
        //    Is42 = is42;
        //    Is4n = is4n;

        //    U1a = u1a;
        //    U1b = u1b;
        //    U1c = u1c;
        //    U1ab = u1ab;
        //    U1bc = u1bc;
        //    U1ca = u1ca;
        //    U12 = u12;
        //    U10 = u10;

        //    U2a = u2a;
        //    U2b = u2b;
        //    U2c = u2c;
        //    U2ab = u2ab;
        //    U2bc = u2bc;
        //    U2ca = u2ca;
        //    U22 = u22;
        //    U20 = u20;

        //    F = f;
        //    Un = un;

        //    D1To8 = d1to8;
        //    D9To16 = d9to16;
        //    D17To24 = d17to24;
        //}

        //public AJ(string recordNumber, string time, string message, string triggeredDefence, string parametr,
        //    string parametrValue, string group, string ida, string idb, string idc, string ita, string itb, string itc,
        //    string is1a, string is1b, string is1c, string is10, string is12, string is1n,
        //    string is2a, string is2b, string is2c, string is20, string is22, string is2n,
        //    string is3a, string is3b, string is3c, string is30, string is32, string is3n,
        //    string is4a, string is4b, string is4c, string is40, string is42, string is4n,
        //    string u1a, string u1b, string u1c, string u1ab, string u1bc, string u1ca, string u12, string u10,
        //    string u2a, string u2b, string u2c, string u2ab, string u2bc, string u2ca, string u22, string u20,
        //    string f, string un, string d1to8, string d9to16, string d17to24, string d25to32, string d33to40)
        //{
        //    RecordNumber = recordNumber;
        //    Time = time;
        //    Message = message;
        //    TriggeredDefence = triggeredDefence;
        //    Parametr = parametr;
        //    ParametrValue = parametrValue;
        //    Group = group;

        //    Ida = ida;
        //    Idb = idb;
        //    Idc = idc;
        //    Ita = ita;
        //    Itb = itb;
        //    Itc = itc;

        //    Is1a = is1a;
        //    Is1b = is1b;
        //    Is1b = is1b;
        //    Is10 = is10;
        //    Is12 = is12;
        //    Is1n = is1n;

        //    Is2a = is2a;
        //    Is2b = is2b;
        //    Is2b = is2b;
        //    Is20 = is20;
        //    Is22 = is22;
        //    Is2n = is2n;

        //    Is3a = is3a;
        //    Is3b = is3b;
        //    Is3b = is3b;
        //    Is30 = is30;
        //    Is32 = is32;
        //    Is3n = is3n;

        //    Is4a = is4a;
        //    Is4b = is4b;
        //    Is4b = is4b;
        //    Is40 = is40;
        //    Is42 = is42;
        //    Is4n = is4n;

        //    U1a = u1a;
        //    U1b = u1b;
        //    U1c = u1c;
        //    U1ab = u1ab;
        //    U1bc = u1bc;
        //    U1ca = u1ca;
        //    U12 = u12;
        //    U10 = u10;

        //    U2a = u2a;
        //    U2b = u2b;
        //    U2c = u2c;
        //    U2ab = u2ab;
        //    U2bc = u2bc;
        //    U2ca = u2ca;
        //    U22 = u22;
        //    U20 = u20;

        //    F = f;
        //    Un = un;

        //    D1To8 = d1to8;
        //    D9To16 = d9to16;
        //    D17To24 = d17to24;
        //    D25To32 = d25to32;
        //    D33To40 = d33to40;
        //}
    }
}
