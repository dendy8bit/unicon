﻿using BEMN.Devices;
using BEMN.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BEMN.MBServer;
using BEMN.MR7.Osc;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.MBServer.Queries;
using BEMN.MR7.AlarmJournal;
using BEMN.MR7.AlarmJournal.Structures;
using BEMN.MR7.BSBGL;
using BEMN.MR7.Configuration;
using BEMN.MR7.Configuration.Structures;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using BEMN.MR7.Configuration.Structures.Oscope;
using BEMN.MR7.Diagnostics;
using BEMN.MR7.Emulation;
using BEMN.MR7.Emulation.Structures;
using BEMN.MR7.FileSharingService;
using BEMN.MR7.Measuring;
using BEMN.MR7.Measuring.Structures;
using BEMN.MR7.Osc.Loaders;
using BEMN.MR7.Osc.Structures;
using BEMN.MR7.SystemJournal;
using BEMN.MR7.SystemJournal.Structures;

namespace BEMN.MR7
{
    public class Mr7 : Device, IDeviceView, IDeviceVersion
    {
        private const string NODE_NAME = "МР801 (в.3.xx)";
        private const string ERROR_SAVE_CONFIG = "Ошибка сохранения конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";

        public const int BAUDE_RATE_MAX = 921600;
        public const ushort MEASURE_START_ADDRESS = 0x14FE;
        public const ushort GROUP_SETPOINT_SIZE = 0x53A;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int ANALOG_DATABASE_START_ADRESS_PERV = 0x6700;
        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;

        private MemoryEntity<OneWordStruct> _iMinWord;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<OneWordStruct> _groupUstavki;
        private MemoryEntity<DiscretDataBaseStruct> _discrets;
        private MemoryEntity<AnalogDataBaseStruct> _analog;
        private MemoryEntity<SomeStruct> _stateSpl;

        private MemoryEntity<WriteStructEmul> _writeSructEmulation;
        private MemoryEntity<WriteStructEmul> _writeSructEmulationNull;
        private MemoryEntity<ReadStructEmul> _readSructEmulation;

        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<OneWordStruct> _indexSystemJournal;

        private MemoryEntity<OscJournalStruct> _oscJournal;

        private ushort _groupSetPointSize;
        public Mr7()
        {
            HaveVersion = true;
        }

        public Mr7(Modbus mb)
        {
            this.MB = mb;
            HaveVersion = true;
        }

        public void InitMemoryEntity()
        {
            int slotLen = this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 72;

            this.SetOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);
            this.OscOptions = new MemoryEntity<OscSettintsStruct>("Параметры осциллографа", this, 0x05A0);
            
            this.AllChannels = new MemoryEntity<OscopeAllChannelsStruct>("Все каналы осциллографа", this, 0x1A90, slotLen);
            this.CurrentOptionsLoader = new CurrentOptionsLoader(this);
            this.CurrentOptionsLoaderAj = new CurrentOptionsLoader(this);
            this.OscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900, slotLen);
            this.RefreshOscJournal = new MemoryEntity<OneWordStruct>("Индекс журнала осциллографа", this, 0x0800);

            this.MeasureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений",this, MEASURE_START_ADDRESS, slotLen);

            if (this.MB.BaudeRate == BAUDE_RATE_MAX)
            {
                this.AllAlarmJournal = new MemoryEntity<AllAlarmJournalStruct>("Журнал аварий", this, 0x0700, slotLen);
                this.AllOscJournal = new MemoryEntity<AllOscJournalStruct>("Журнал осциллографа (весь)", this, 0x0800, slotLen);
                this.Full1SysJournal024 = new MemoryEntity<FullSystemJournalStruct1024>("Запись журнала системы 1024", this, 0x0600, slotLen);
                this.FullSysJournal64 = null;
            }
            else
            {
                this.Full1SysJournal024 = null;
                this.FullSysJournal64 = new MemoryEntity<FullSystemJournalStruct64>("Запись журнала системы 64", this, 0x0600);
            }

            this.AlarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Запись журнала аварий", this, 0x0700, slotLen);
            this.SetPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА", this, 0x0700);

            this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, 0x0800);

            this._dateTime = new MemoryEntity<DateTimeStruct>("Время и дата в устройстве", this, 0x0200);
            this._writeSructEmulation = new MemoryEntity<WriteStructEmul>("Запись аналоговых сигналов ", this, 0x5800, slotLen);
            this._writeSructEmulationNull = new MemoryEntity<WriteStructEmul>("Запись нулевых аналоговых сигналов ", this, 0x5800, slotLen);
            this._readSructEmulation = new MemoryEntity<ReadStructEmul>("Чтение времени и статуса эмуляции", this, 0x584E, slotLen);


            this._indexSystemJournal = new MemoryEntity<OneWordStruct>("Номер журнала системы v3", this, SYSTEM_JOURNAL_START_ADRESS, slotLen);
            this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы v3", this, SYSTEM_JOURNAL_START_ADRESS, slotLen);
            this._iMinWord = new MemoryEntity<OneWordStruct>("Минимально отображаемый ток V3", this, 0xFC05, slotLen);
            this._groupUstavki = new MemoryEntity<OneWordStruct>("Группа уставок", this, 0x0400);
            this.Configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация устройства", this, 0x1000, slotLen);
            this._discrets = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS, slotLen);
            this._analog = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, ANALOG_DATABASE_START_ADRESS, slotLen);

            this.SourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300, slotLen);
            this.ProgramSignalsStruct = new MemoryEntity<LogicProgramSignals>("LoadProgramSignals", this, 0x4100, slotLen);
            this.ProgramPageStruct = new MemoryEntity<OneWordStruct>("SaveProgrammPage", this, 0x4000, slotLen);
            this.ProgramStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
            this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программы", this, 0x0D0C);
            this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программы", this, 0x0D0D);

            this._stateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики", this, 0x0D17);
            ushort[] values = new ushort[4];
            this._stateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
            this._stateSpl.Values = values;
        }
        
        public int CurrentsCount
        {
            get
            {
                string count = GetCountFromString("T");

                return int.Parse(count);
            }
        }

        public int VoltagesCount
        {
            get
            {
                string count = GetCountFromString("N");

                return int.Parse(count);
            }
        }

        public int DiskretsCount
        {
            get
            {
                string count = GetCountFromString("D");

                return int.Parse(count);
            }
        }

        private string GetCountFromString(string chanelName)
        {
            string conf = Info.DeviceConfiguration;
            string count = string.Empty;
            int index = conf.IndexOf(chanelName, StringComparison.InvariantCulture);
            while (true)
            {
                char d = conf[++index];
                if (char.IsDigit(d))
                {
                    count += d;
                }
                else
                {
                    break;
                }
            }

            return count;
        }

        public override void LoadVersion(object deviceObj)
        {
            _infoSlot = new slot(0x500, 0x500 + 0x20);
            //this.LoadSlot(this.DeviceNumber, this._infoSlot, "version" + this.DeviceNumber, deviceObj);
            base.LoadVersion(deviceObj);
        }

        public MemoryEntity<SomeStruct> StateSpl
        {
            get { return this._stateSpl; }
        }
        public MemoryEntity<OneWordStruct> StopSpl { get; private set; }
        public MemoryEntity<OneWordStruct> StartSpl { get; private set; }

        public MemoryEntity<StartStruct> ProgramStartStruct { get; private set; }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct { get; private set; }

        public MemoryEntity<LogicProgramSignals> ProgramSignalsStruct { get; private set; }

        public MemoryEntity<OneWordStruct> ProgramPageStruct { get; private set; }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return this._oscJournal; }
        }

        public CurrentOptionsLoader CurrentOptionsLoaderAj { get; set; }

        public MemoryEntity<WriteStructEmul> WriteStructEmulation
        {
            get { return this._writeSructEmulation; }
        }

        public MemoryEntity<WriteStructEmul> WriteStructEmulationNull
        {
            get { return this._writeSructEmulationNull; }
        }
        public MemoryEntity<ReadStructEmul> ReadStructEmulation
        {
            get { return this._readSructEmulation; }
        }

        public CurrentOptionsLoader CurrentOptionsLoader { get; private set; }

        public MemoryEntity<OneWordStruct> Imin
        {
            get { return this._iMinWord; }
        }
        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTime; }
        }

        public MemoryEntity<OneWordStruct> GroupUstavki
        {
            get { return this._groupUstavki; }
        }

        public MemoryEntity<DiscretDataBaseStruct> Discret
        {
            get { return this._discrets; }
        }

        public MemoryEntity<AnalogDataBaseStruct> Analog
        {
            get { return this._analog; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return this._systemJournal; }
        }

        public MemoryEntity<OneWordStruct> IndexSystemJournal
        {
            get { return this._indexSystemJournal; }
        }

        public MemoryEntity<FullSystemJournalStruct1024> Full1SysJournal024 { get; private set; }

        public MemoryEntity<FullSystemJournalStruct64> FullSysJournal64 { get; private set; }

        public MemoryEntity<OneWordStruct> RefreshOscJournal { get; private set; }

        public MemoryEntity<OscPage> OscPage { get; private set; }
        
        public MemoryEntity<AllOscJournalStruct> AllOscJournal { get; private set; }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage { get; private set; }

        public MemoryEntity<OscSettintsStruct> OscOptions { get; private set; }

        public MemoryEntity<OscopeAllChannelsStruct> AllChannels { get; private set; }

        public MemoryEntity<ConfigurationStruct> Configuration {get ;private set;}
        
        public MemoryEntity<MeasureTransStruct> MeasureTrans {get ;private set;}

        public MemoryEntity<AllAlarmJournalStruct> AllAlarmJournal { get; private set; }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal { get; private set; }

        public MemoryEntity<OneWordStruct> SetPageJa { get; private set; }

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(DeviceVersion)) return MEASURE_START_ADDRESS;
            return (ushort)(MEASURE_START_ADDRESS + this._groupSetPointSize * group);

            //return (ushort)(MEASURE_START_ADDRESS + GROUP_SETPOINT_SIZE * group);
        }

        public sealed override Modbus MB
        {
            get { return mb; }
            set
            {
                if (value == null) return;
                if (mb != null)
                {
                    mb.CompleteExchange -= this.CompleteExchange;
                }
                mb = value;
                mb.CompleteExchange += this.CompleteExchange;
            }
        }

        private void CompleteExchange(object sender, Query query)
        {
            if (query.name == "Сохранить конфигурацию")
            {
                MessageBox.Show(query.error ? ERROR_SAVE_CONFIG : WRITE_OK);
            }

            mb_CompleteExchange(sender, query);
        }

        #region IDeviceVersion

        public const string T12N5D58R51 = "T12N5D58R51";
        public const string T12N6D58R51 = "T12N6D58R51";
        public const string T9N8D58R51 = "T9N8D58R51";
        public const string T6N3D42R35 = "T6N3D42R35";
        public const string T12N4D26R19 = "T12N4D26R19";

        private static readonly string[] _deviceApparatConfig =
        {
            T12N5D58R51,
            T12N6D58R51,
            T9N8D58R51,
            T6N3D42R35,
            T12N4D26R19
        };

        private static readonly string[] _deviceOutString = { T12N5D58R51, T12N6D58R51, T9N8D58R51, T6N3D42R35, T12N4D26R19 };

        public Type[] Forms
        {
            get
            {
                this._groupSetPointSize = new GroupSetpoint().GetStructInfo(this.MB.BaudeRate == BAUDE_RATE_MAX ? 1024 : 72).FullSize;
                StringsConfig.CurrentVersion = Common.VersionConverter(DeviceVersion);
                this.InitMemoryEntity();
                if ((!DeviceDlgInfo.IsConnectionMode || !IsConnect) && !Framework.Framework.IsProjectOpening)
                {
                    //ChoiceDeviceType choice = new ChoiceDeviceType(_deviceApparatConfig, _deviceOutString);
                    //choice.ShowDialog();
                    //DevicePlant = choice.DeviceType;

                    return new[]
                    {
                        typeof (Mr7OscilloscopeForm),
                        typeof (Mr7ConfigurationForm),
                        typeof (BSBGLEF),
                        typeof (EmulationForm),
                        typeof (Mr7SystemJournalForm),
                        typeof (Mr7MeasuringForm),
                        typeof (Mr7AlarmJournalForm),
                        typeof (FileSharingForm)
                    };
                }

                return new[]
                {
                    typeof (Mr7OscilloscopeForm),
                    typeof (Mr7ConfigurationForm),
                    typeof (BSBGLEF),
                    typeof (EmulationForm),
                    typeof (Mr7SystemJournalForm),
                    typeof (Mr7MeasuringForm),
                    typeof (Mr7AlarmJournalForm),
                    typeof (FileSharingForm),
                    typeof (AllInformationsDeviceForm)
                };

            }
        }

        public List<string> Versions => new List<string>
        {
            "3.00",
            "3.01",
            "3.02"
        };

        #endregion

        #region [IDeviceView members]
        public Type ClassType => typeof(Mr7);

        public bool ForceShow => false;

        public Image NodeImage => Framework.Properties.Resources.mr801; 

        public string NodeName => NODE_NAME;

        public INodeView[] ChildNodes => new INodeView[] { };

        public bool Deletable => true;
        #endregion
    }
}
