﻿using System;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses;
using BEMN.MR7.Configuration.Structures.Defenses.Z;

namespace BEMN.MR7.Configuration
{
    public partial class CalcKoefForm : Form
    {
        private Complex N1Z0;
        private Complex N1Z1;

        public CalcKoefForm(ResistanceStruct resist)
        {
            InitializeComponent();
            this.N1Z0 = new Complex(resist.R0Step1, resist.X0Step1);
            this.N1Z1 = new Complex(resist.R1Step1, resist.X1Step1);
        }

        private void CloseClick(object sender, EventArgs e)
        {
            Close();
        }

        private void CalcKoefForm_Load(object sender, EventArgs e)
        {
            this.Ko1Amp.Text = ((this.N1Z0 - this.N1Z1)/(this.N1Z1*3)).Mod;
            this.Ko1Angle.Text =  ((this.N1Z0 - this.N1Z1)/(this.N1Z1*3)).Argument;
            this.Z0Z1Amp1.Text = (this.N1Z0/this.N1Z1).Mod;
            this.Z0Z1Angle1.Text = (this.N1Z0/this.N1Z1).Argument;
        }
    }
}
