﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR7.Configuration.Structures.ConfigSystem
{
    /// <summary>
    /// конфигурациия системы
    /// </summary>
     public class ConfigRS485Struct : StructBase
    {
        [Layout(0)] private ushort _adr; //сетевой адрес устройства (1-247)
        [Layout(1)] private ushort _spd; //скорость работы (1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200)
        [Layout(2)] private ushort _pause; //пауза ответа (мс)
        [Layout(3)] private ushort _rez;
    }
}
