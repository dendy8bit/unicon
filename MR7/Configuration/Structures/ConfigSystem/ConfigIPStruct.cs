﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.ConfigSystem
{
    public class ConfigIPStruct : StructBase
    {
        [Layout(0)] private ushort ip_lo;      //сетевой IP адрес устройства
        [Layout(1)] private ushort ip_hi;
        //[Layout(2)] private ushort sntpip_lo;  // IP адрес сервера реального времени для синхронизации
        //[Layout(3)] private ushort sntpip_hi;
        //[Layout(4)] private ushort config;     //тестирование, резервирование, свойства MAC адреса
        //[Layout(5)] private ushort mac_lo;
        //[Layout(6)] private ushort mac_md;
        //[Layout(7)] private ushort mac_hi;
        //[Layout(8)] private ushort period;     //период обновления времени 0-9999 мин
        //[Layout(9)] private ushort timezone;   //часовой пояс

        [BindingProperty(0)]
        public ushort IpLo1
        {
            get { return Common.GetBits(this.ip_lo, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this.ip_lo = Common.SetBits(this.ip_lo, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }
        [BindingProperty(1)]
        public ushort IpLo2
        {
            get { return (ushort)(Common.GetBits(this.ip_lo, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this.ip_lo = Common.SetBits(this.ip_lo, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }

        [BindingProperty(2)]
        public ushort IpHi1
        {
            get { return Common.GetBits(this.ip_hi, 0, 1, 2, 3, 4, 5, 6, 7); }
            set { this.ip_hi = Common.SetBits(this.ip_hi, value, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        [BindingProperty(3)]
        public ushort IpHi2
        {
            get { return (ushort)(Common.GetBits(this.ip_hi, 8, 9, 10, 11, 12, 13, 14, 15) >> 8); }
            set { this.ip_hi = Common.SetBits(this.ip_hi, value, 8, 9, 10, 11, 12, 13, 14, 15); }
        }
    }
}
