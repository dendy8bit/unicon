﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Swing
{
    public class SwingStruct : StructBase
    {
        [Layout(0)] private ushort _config;
	    [Layout(1)] private ushort _x1;			//уставка x
	    [Layout(2)] private ushort _r1;			//уставка r 
	    [Layout(3)] private ushort _c1;			//угол
	    [Layout(4)] private ushort _dr;			//дельта зоны
	    [Layout(5)] private ushort _time;		//время срабатывания_
	    [Layout(6)] private ushort _i0;			//
	    [Layout(7)] private ushort _timeReset;	//время через которое произойдет сброс
	    [Layout(8)] private ushort _iMin;		//min ток при котором мы обнаружываем качание
	    [Layout(9)] private ushort _rez;		//

        [BindingProperty(0)]
        public string Type
        {
            get { return Validator.Get(this._config, StringsConfig.ResistDefType, 4); }
            set { this._config = Validator.Set(value, StringsConfig.ResistDefType, this._config, 4); }
        }

        [BindingProperty(1)]
        public double R1
        {
            get
            {
                double value = ValuesConverterCommon.GetUstavka256(this._r1);
                if (SignR == "+") return value;
                if (SignR == "-")
                {
                    var stringValue = Convert.ToString(value);
                    if (Type == StringsConfig.ResistDefType[0])
                    {
                        return value;
                    }
                    stringValue = SignR + stringValue;
                    value = Convert.ToDouble(stringValue);
                    return value;
                }
                return value;
            }
            set
            {
                if (value < 0)
                {
                    SignR = "-";
                    value *= -1;
                }
                else
                {
                    SignR = "+";
                }
                this._r1 = ValuesConverterCommon.SetUstavka256(value);
            }
        }
        [BindingProperty(2)]
        public double X1
        {
            get
            {
                double value = ValuesConverterCommon.GetUstavka256(this._x1);
                if (SignX == "+") return value;
                if (SignX == "-")
                {
                    var stringValue = Convert.ToString(value);
                    if (Type == StringsConfig.ResistDefType[0])
                    {
                        return value;
                    }
                    stringValue = SignX + stringValue;
                    value = Convert.ToDouble(stringValue);
                    return value;
                }
                return value;
            }
            set
            {
                if (value < 0)
                {
                    SignX = "-";
                    value *= -1;
                }
                else
                {
                    SignX = "+";
                }
                this._x1 = ValuesConverterCommon.SetUstavka256(value);
            }
        }
        [BindingProperty(3)]
        public double Dr
        {
            get { return ValuesConverterCommon.GetUstavka256(this._dr); }
            set { this._dr = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(4)]
        public double C1
        {
            get { return Common.GetBit(this._config, 4) ? Math.Round((double) this._c1/256, 2) : this._c1; }
            set { this._c1 = Common.GetBit(this._config, 4) ? (ushort) (value*256) : (ushort) value; }
        }

        [BindingProperty(5)]
        public int Tsrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(6)]
        public double I0
        {
            get { return ValuesConverterCommon.GetUstavka40(this._i0); }
            set { this._i0 = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(7)]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 0); }
            set { this._config = Common.SetBit(this._config, 0, value); }
        }

        [BindingProperty(8)]
        public int ResetTime
        {
            get { return ValuesConverterCommon.GetWaitTime(this._timeReset); }
            set { this._timeReset = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(9)]
        public double Imin
        {
            get { return ValuesConverterCommon.GetUstavka40(this._iMin); }
            set { this._iMin = ValuesConverterCommon.SetUstavka40(value); }
        }
        [BindingProperty(10)]
        public string SignR
        {
            get { return Validator.Get(this._config, StringsConfig.Sign, 14); }
            set { this._config = Validator.Set(value, StringsConfig.Sign, this._config, 14); }
        }
        [BindingProperty(11)]
        public string SignX
        {
            get { return Validator.Get(this._config, StringsConfig.Sign, 15); }
            set { this._config = Validator.Set(value, StringsConfig.Sign, this._config, 15); }
        }
    }
}
