﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR7.Configuration.Structures.ConfigSystem;
using BEMN.MR7.Configuration.Structures.Goose;
using BEMN.MR7.Configuration.Structures.InputSignals;
using BEMN.MR7.Configuration.Structures.Oscope;
using BEMN.MR7.Configuration.Structures.RelayInd;
using BEMN.MR7.Configuration.Structures.Switch;
using BEMN.MR7.Configuration.Structures.UROV;

namespace BEMN.MR7.Configuration.Structures
{
    [Serializable]
    [XmlRoot(ElementName = "МР801")]
    public class ConfigurationStruct : StructBase
    {
        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType => "МР801";

        [XmlElement(ElementName = "Первичные_уставки")]
        public bool Primary { get; set; }

        [XmlElement(ElementName = "Размер_осциллограмы")]
        public string SizeOsc { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        [XmlElement(ElementName = "Конфигурация")]
        public string Config { get; set; }

        #region [Private fields]

        [Layout(0)] private AllGroupSetpointStruct _allGroupSetpoints;
        [Layout(1)] private SwitchStruct _sw;
        [Layout(2)] private InputSignalStruct _impsg;
        [Layout(3)] private OscopeStruct _osc;
        [Layout(4)] private AutomaticsParametersStruct _automatics;
        [Layout(5, Ignore = true)] private ConfigRS485Struct _configNet;
        [Layout(6)] private ConfigIPStruct _configIPStruct;
        [Layout(7, Count = 8, Ignore = true)] private ushort[] _mac; // MAC-адрес и всякая фигня
        [Layout(8, Count = 126, Ignore = true)] private ushort[] _res;
        [Layout(9)] private ConfigAddStruct _cnfAdd;
        [Layout(10)] private UrovStruct _urov;
        [Layout(11)] private GooseConfig _gooseConfig;

        #endregion
        /// <summary>
        /// Группы уставок
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_всех_групп_уставок")]
        [BindingProperty(0)]
        public AllGroupSetpointStruct AllGroupSetpoints
        {
            get { return this._allGroupSetpoints; }
            set { this._allGroupSetpoints = value; }
        }
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(1)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(2)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(3)]
        public OscopeStruct Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }

        [XmlElement(ElementName = "Конфигурация_реле,индикаторов,неисправностей")]
        [BindingProperty(4)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }

        [XmlElement(ElementName = "IP")]
        [BindingProperty(5)]
        public ConfigIPStruct ConfigIPStruct
        {
            get { return this._configIPStruct; }
            set { this._configIPStruct = value; }
        }
        
        [XmlElement(ElementName = "Вход_опорного_канала")]
        [BindingProperty(6)]
        public ConfigAddStruct ConfigAdd
        {
            get { return this._cnfAdd; }
            set { this._cnfAdd = value; }
        }

        /// <summary>
        /// УРОВ
        /// </summary>
        [XmlElement(ElementName = "УРОВ")]
        [BindingProperty(7)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        /// <summary>
        /// Конфигурация гусов
        /// </summary>
        [XmlElement(ElementName = "Гусы")]
        [BindingProperty(8)]
        public GooseConfig GooseConfig
        {
            get { return this._gooseConfig; }
            set { this._gooseConfig = value; }
        }

    }
}
