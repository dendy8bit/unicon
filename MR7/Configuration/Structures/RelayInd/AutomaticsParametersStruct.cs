﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR7.Configuration.Structures.RSTriggers;

namespace BEMN.MR7.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры автоматики(Реле и индикаторы)
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "Реле_и_Индикаторы")]
    public class AutomaticsParametersStruct : StructBase
    {
        #region [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [Layout(0)] private AllReleOutputStruct _relays;
        /// <summary>
        /// RS-триггеры
        /// </summary>
        [Layout(1)] private AllRsTriggersStruct _rsTriggers;
        /// <summary>
        /// индикаторы
        /// </summary>
        [Layout(2)] private AllIndicatorsStruct _indicators;

        /// <summary>
        /// реле неисправность
        /// </summary>
        [Layout(3)] private FaultStruct _fault;

       

        #endregion [Private fields]

        /// <summary>
        /// Реле
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Реле")]
        public AllReleOutputStruct Relays
        {
            get { return this._relays; }
            set { this._relays = value; }
        }
        /// <summary>
        /// RS-Триггеры
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "RS_триггеры")]
        public AllRsTriggersStruct RsTriggers
        {
            get { return this._rsTriggers; }
            set { this._rsTriggers = value; }
        }
        /// <summary>
        /// индикаторы
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Индикаторы")]
        public AllIndicatorsStruct Indicators
        {
            get { return this._indicators; }
            set { this._indicators = value; }
        }

        /// <summary>
        /// реле неисправность
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Реле_неисправности")]
        public FaultStruct Fault
        {
            get { return this._fault; }
            set { this._fault = value; }
        }
    }
}
