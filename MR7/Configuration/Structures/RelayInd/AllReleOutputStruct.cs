using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR7.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_COUNT_CONST = 80;

        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT_CONST)] private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]

        public ReleOutputStruct[] Rows
        {
            get { return this._relays; }
            set { this._relays = value; }
        }

        public static int RelaysCount
        {
            get
            {
                switch (StringsConfig.DeviceType)
                {
                    case Mr7.T12N5D58R51:
                    case Mr7.T12N6D58R51:
                    case Mr7.T9N8D58R51:
                        return 50;
                    case Mr7.T12N4D26R19:
                        return 18;
                    case Mr7.T6N3D42R35:
                        return 34;
                    default:
                        return 34;
                }
                    
            }
        }
    }
}