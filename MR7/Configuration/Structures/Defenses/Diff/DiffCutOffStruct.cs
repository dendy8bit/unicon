﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR7.Configuration.Structures.Defenses.Diff
{
    public class DiffCutOffStruct : StructBase
    {
        [Layout(0)] private ushort _config;                 //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;                //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;                  //вход блокировки
        [Layout(3)] private ushort _ust;                    //уставка срабатывания_
        [Layout(4)] private ushort _time;                   //время срабатывания_
        [Layout(5)] private ushort _rez;                    //резерв

        [BindingProperty(0)]
        [XmlElement(ElementName = "Состояние")]
        public string State
        {
            get => Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1);
            set => this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1);
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get => Validator.Get(this._block, StringsConfig.SwitchSignals);
            set => this._block = Validator.Set(value, StringsConfig.SwitchSignals);
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка_Iср")]
        public double Icp
        {
            get => ValuesConverterCommon.GetUstavka40(this._ust);
            set => this._ust = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Выдержка_времени_tср")]
        public int Tcp
        {
            get => ValuesConverterCommon.GetWaitTime(this._time);
            set => this._time = ValuesConverterCommon.SetWaitTime(value);
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Ступень_по_мгновенным_значениям")]
        public string StapByInstantValues
        {
            get => Validator.Get(this._config, StringsConfig.OffOn, 3);
            set => this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 3);
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Осциллограф")]
        public string Osc
        {
            get => Validator.Get(this._config1, StringsConfig.OscModes, 4, 5);
            set => this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5);
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Уров")]
        public string Urov
        {
            get => Validator.Get(this._config, StringsConfig.OffOn, 2);
            set => this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 2);
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 0);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0);
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "АВР")]
        public string Avr
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 1);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1);
        }

    }
    
}
