﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using System.Xml.Serialization;

namespace BEMN.MR7.Configuration.Structures.Defenses.Diff
{
    public class DiffIzeroStruct : StructBase
    {
        [Layout(0)] private ushort _config;                 //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;                //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;                  //вход блокировки
        [Layout(3)] private ushort _ust;                    //уставка срабатывания_
        [Layout(4)] private ushort _time;                   //время срабатывания_ //характеристика торможения_
        [Layout(5)] private ushort _Ib1;                   // начало  1
        [Layout(6)] private ushort _K1;                    // тангенс 1
        [Layout(7)] private ushort _Ib2;                   // начало  2
        [Layout(8)] private ushort _K2;                    // тангенс 2
        [Layout(9)] private ushort _rez;                    //резерв

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string State
        {
            get => Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1);
            set => this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1);
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get => Validator.Get(this._block, StringsConfig.SwitchSignals);
            set => this._block = Validator.Set(value, StringsConfig.SwitchSignals);
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка_Iср")]
        public double Icp
        {
            get => ValuesConverterCommon.GetUstavka40(this._ust);
            set => this._ust = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Сторона")]
        public string Side
        {
            get => Validator.Get(this._config, StringsConfig.Binding, 4, 5);
            set => this._config = Validator.Set(value, StringsConfig.Binding, this._config, 4, 5);
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "tср")]
        public int Tcp
        {
            get => ValuesConverterCommon.GetWaitTime(this._time);
            set => this._time = ValuesConverterCommon.SetWaitTime(value);
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Iб1_начало")]
        public double Ib1Start
        {
            get => ValuesConverterCommon.GetUstavka40(this._Ib1);
            set => this._Ib1 = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "f1_угол_наклона")]
        public ushort F1
        {
            get => this._K1;
            set => this._K1 = value;
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Iб2_начало")]
        public double Ib2Start
        {
            get => ValuesConverterCommon.GetUstavka40(this._Ib2);
            set => this._Ib2 = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "f2_угол_наклона")]
        public ushort F2
        {
            get => this._K2;
            set => this._K2 = value;
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "Осциллограф")]
        public string Osc
        {
            get => Validator.Get(this._config1, StringsConfig.OscModes, 4, 5);
            set => this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5);
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Уров")]
        public string Urov
        {
            get => Validator.Get(this._config, StringsConfig.OffOn, 2);
            set => this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 2);
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 0);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0);
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "АВР")]
        public string Avr
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 1);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1);
        }
    }
}
