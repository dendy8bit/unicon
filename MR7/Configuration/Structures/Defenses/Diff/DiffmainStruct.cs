﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using System.Xml.Serialization;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Defenses.Diff
{
    public class DiffmainStruct : StructBase
    {
        [Layout(0)] private ushort _config;   //конфигурация выведено/введено (Уров - выведено/введено)
        [Layout(1)] private ushort _config1;  //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;    //вход блокировки
        [Layout(3)] private ushort _ust;      //уставка срабатывания
        [Layout(4)] private ushort _time;     //время срабатывания
        [Layout(5)] private ushort _Ib1;      //начало 1
        [Layout(6)] private ushort _K1;       //тангенс 1
        [Layout(7)] private ushort _Ib2;      //начало 2
        [Layout(8)] private ushort _K2;       //тангенс 2 
        [Layout(9)] private ushort _I21;      //%
        [Layout(10)] private ushort _I51;     //%
        [Layout(11)] private ushort _rez;     //резерв

        [BindingProperty(0)]
        [XmlElement(ElementName = "Состояние")]
        public string State
        {
            get => Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1);
            set => this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1);
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get => Validator.Get(this._block, StringsConfig.SwitchSignals);
            set => this._block = Validator.Set(value, StringsConfig.SwitchSignals);
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Уставка_Iср")]
        public double Icp
        {
            get => ValuesConverterCommon.GetUstavka40(this._ust);
            set => this._ust = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Выдержка_времени_tср")]
        public int Tcp
        {
            get => ValuesConverterCommon.GetWaitTime(this._time);
            set => this._time = ValuesConverterCommon.SetWaitTime(value);
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Блокировка_I2/I1")]
        public bool BlockI2I1
        {
            get => Common.GetBit(this._config, 10);
            set => this._config = Common.SetBit(this._config, 10, value);
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Перекр_блок_I2I1")]
        public string SwitchBlockI2I1
        {
            get => Validator.Get(this._config, StringsConfig.BeNo, 7);
            set => this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 7);
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "I2I1")]
        public ushort I2I1
        {
            get => this._I21;
            set => this._I21 = value;
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Блокировка_I5/I1")]
        public bool BlockI5I1
        {
            get => Common.GetBit(this._config, 9);
            set => this._config = Common.SetBit(this._config, 9, value);
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Перекр_блок_I5I1")]
        public string SwitchBlockI5I1
        {
            get => Validator.Get(this._config, StringsConfig.BeNo, 6);
            set => this._config = Validator.Set(value, StringsConfig.BeNo, this._config, 6);
        }

        [BindingProperty(9)]
        [XmlElement(ElementName = "I5I1")]
        public ushort I5I1
        {
            get => this._I51;
            set => this._I51 = value;
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Iб1_начало")]
        public double Ib1Start
        {
            get => ValuesConverterCommon.GetUstavka40(this._Ib1);
            set => this._Ib1 = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "f1_угол_наклона")]
        public ushort F1
        {
            get => this._K1;
            set => this._K1 = value;
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Iб2_начало")]
        public double Ib2Start
        {
            get => ValuesConverterCommon.GetUstavka40(this._Ib2);
            set => this._Ib2 = ValuesConverterCommon.SetUstavka40(value);
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "f2_угол_наклона")]
        public ushort F2
        {
            get => this._K2;
            set => this._K2 = value;
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Осциллограф")]
        public string Osc
        {
            get => Validator.Get(this._config1, StringsConfig.OscModes, 4, 5);
            set => this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5);
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "Уров")]
        public string Urov
        {
            get => Validator.Get(this._config, StringsConfig.OffOn, 2);
            set => this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 2);
        }

        [BindingProperty(16)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 0);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0);
        }

        [BindingProperty(17)]
        [XmlElement(ElementName = "АВР")]
        public string Avr
        {
            get => Validator.Get(this._config1, StringsConfig.Apv, 1);
            set => this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1);
        }

    }


}
