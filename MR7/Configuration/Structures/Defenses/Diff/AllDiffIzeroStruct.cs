﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR7.Configuration.Structures.Defenses.Diff
{
    public class AllDiffIzeroStruct : StructBase, IDgvRowsContainer<DiffIzeroStruct>
    {
        public const int DEF_COUNT = 3;
        [Layout(0, Count = DEF_COUNT)] private DiffIzeroStruct[] _diffIzero; 

        [XmlArray(ElementName = "Все_Iд*")]
        public DiffIzeroStruct[] Rows
        {
            get { return this._diffIzero; }
            set { this._diffIzero = value; }
        }
    }
}
