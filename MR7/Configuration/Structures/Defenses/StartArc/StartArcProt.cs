﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;
using System.Xml.Serialization;

namespace BEMN.MR7.Configuration.Structures.Defenses.StartArc
{
    public class StartArcProt : StructBase
    {
        [Layout(0)] private ushort _config;         //конфигурация вывудено/ввудено
        [Layout(1)] private ushort _ust;            //уставка срабатывания
        [Layout(2)] private ushort _res;            
        [Layout(3)] private ushort _block;          //вход блокировки

        [BindingProperty(0)]
        [XmlElement("Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModesShort, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModesShort, this._config, 0, 1); }
        }
        [BindingProperty(1)]
        [XmlElement("Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        [BindingProperty(2)]
        [XmlElement("Уставка")]
        public double Srab
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(3)]
        [XmlElement("Привязка")]
        public string Binding
        {
            get { return Validator.Get(this._config, StringsConfig.Binding, 2, 3); }
            set { this._config = Validator.Set(value, StringsConfig.Binding, this._config, 2, 3); }
        }
        [BindingProperty(4)]
        [XmlElement("Осциллограф")]
        public bool Osc
        {
            get { return Common.GetBit(this._config, 4); }
            set { this._config = Common.SetBit(this._config, 4, value); }
        }
    }
}
