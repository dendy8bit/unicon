﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Defenses.Q
{
    /// <summary>
    /// конфигурациия основной ступени защиты Q>
    /// </summary>
    [XmlRoot(ElementName = "защита_Q")]
    public class DefenseQStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;     //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;    //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;      //вход блокировки
        [Layout(3)] private ushort _ust;        //уставка срабатывания_ 
        #endregion [Private fields]


        #region [Properties]
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        /// <summary>
        /// Уставка срабатывания
        /// </summary>
        [BindingProperty(1)]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ust); }
            set { this._ust = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Блокировка")]
        public string Block
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "Осц")]
        public string Osc
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "АПВ_режим")]
        public string ApvMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0); }
        }

        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "АВР_режим")]
        public string AvrMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1); }
        }
        #endregion [Properties]
    }
}
