﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Defenses.Istar
{
    /// <summary>
    /// I*
    /// </summary>
    [XmlRoot(ElementName = "защиты_I*")]
    public class DefenseStarStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config;     //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;    //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;      //вход блокировки
        [Layout(3)] private ushort _ust;        //уставка срабатывания_
        [Layout(4)] private ushort _time;       //время срабатывания_
        [Layout(5)] private ushort _k;          //коэфиц.зависимой хар-ки(в I2/I1  - вход для синхронизма U1нет/U2нет)
        [Layout(6)] private ushort _u;          //уставка пуска по напряжению
        [Layout(7)] private ushort _tu;         //время ускорения_
        [Layout(8)] private ushort _i21;        // уставка в %  (в I2/I1  - для 762 угол In1 )
        [Layout(9)] private ushort _usk;        //вход ускорения 

        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }
        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Привязка")]
        public string Binding
        {
            get { return Validator.Get(this._config, StringsConfig.Binding, 14, 15); }
            set { this._config = Validator.Set(value, StringsConfig.Binding, this._config, 14, 15); }
        }
        /// <summary>
        /// Uпуск (есть/нет)
        /// </summary>
        [XmlElement(ElementName = "Uпуск")]
        [BindingProperty(3)]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }
        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }
        
        /// <summary>
        /// Направление
        /// </summary>
        [XmlElement(ElementName = "Направление")]
        [BindingProperty(5)]
        public string DirectionXml
        {
            get { return Validator.Get(this._config, StringsConfig.Direction, 6, 7); }
            set { this._config = Validator.Set(value, StringsConfig.Direction, this._config, 6, 7); }
        }
        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Недост_напр(I*)")]
        public string UndirXml
        {
            get { return Validator.Get(this._config, StringsConfig.Undir, 8); }
            set { this._config = Validator.Set(value, StringsConfig.Undir, this._config, 8); }
        }

        /// <summary>
        /// Выбор
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Выбор")]
        public string Choose
        {
            get { return Validator.Get(this._config, StringsConfig.Choose, 11); }
            set { this._config = Validator.Set(value, StringsConfig.Choose, this._config, 11); }
        }

        /// <summary>
        /// I*
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "I*")]
        public string IstarXml
        {
            get
            {
                if (Validator.Get(this._config, StringsConfig.Choose, 11) == StringsConfig.Choose[1])
                {
                    return Validator.Get(this._config, StringsConfig.Logic, 12, 13);
                }
                return Validator.Get(this._config, StringsConfig.I, 12, 13);
            }
            set
            {
                if (value == StringsConfig.Logic[0] || value == StringsConfig.Logic[1] ||
                    value == StringsConfig.Logic[2])
                {
                    this._config = Validator.Set(value, StringsConfig.Logic, this._config, 12, 13);
                }
                else
                {
                    this._config = Validator.Set(value, StringsConfig.I, this._config, 12, 13);
                }
            }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Характеристика")]
        public string CharacteristicXml
        {
            get { return Validator.Get(this._config, StringsConfig.Characteristic, 4); }
            set { this._config = Validator.Set(value, StringsConfig.Characteristic, this._config, 4); }
        }
        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// K
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "K")]
        public ushort K
        {
            get { return this._k; }
            set { this._k = value; }
        }
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(13)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        [BindingProperty(14)]
        public string Uskor
        {
            get { return Validator.Get(this._usk, StringsConfig.RelaySignals); }
            set { this._usk = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// Ty (есть/нет)
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "Ненапр.приУСКОР")]
        public bool NenaprUskor
        {
            get { return Common.GetBit(this._config1, 7); }
            set { this._config1 = Common.SetBit(this._config1, 7, value); }
        }
        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(18)]
        [XmlElement(ElementName = "АПВ_режим")]
        public string ApvMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0); }
        }

        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(19)]
        [XmlElement(ElementName = "АВР_режим")]
        public string AvrMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1); }
        }
        #endregion [Properties]
    }
}
