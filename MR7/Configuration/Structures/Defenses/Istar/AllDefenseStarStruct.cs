﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR7.Configuration.Structures.Defenses.Istar
{
    public class AllDefenseStarStruct : StructBase, IDgvRowsContainer<DefenseStarStruct>
    {
        public const int DEF_COUNT = 8;
        [Layout(0, Count = DEF_COUNT)] private DefenseStarStruct[] _mtzmaini0; //мтз I*

        [XmlArray(ElementName = "Все_I*")]
        public DefenseStarStruct[] Rows
        {
            get { return this._mtzmaini0; }
            set { this._mtzmaini0 = value; }
        }
    }
}
