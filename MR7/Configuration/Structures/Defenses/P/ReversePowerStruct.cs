﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Defenses.P
{
    [Serializable]
    [XmlRoot(ElementName = "конфигурация_внешней_защиты")]
    public class ReversePowerStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;//конфигурация дополнительная (АПВ - выведено/введено, осциллограф)
        [Layout(2)] private ushort _block; //вход блокировки
        [Layout(3)] private ushort _x1; //уставка срабатывания_
        [Layout(4)] private ushort _time1; //время срабатывания_
        [Layout(5)] private ushort _corner1; //угол срабатывания_
        [Layout(6)] private ushort _x2; //уставка возврата
        [Layout(7)] private ushort _time2; //время вовзрата
        [Layout(8)] private ushort _usti; // угол возврата
        [Layout(9)] private ushort _tu; //время ускорения_  (резерв)
        [Layout(10)] private ushort _usk; //вход ускорения (резерв)
        [Layout(11)] private ushort _rez; //резерв

        #endregion

        #region [Properties]

        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка_срабатывания")]
        public double UstavkaSrab
        {
            get
            {
                double val = (short)_x1 * 2.5 / 32767;
                return Math.Round(val, 2);
            }
            set
            {
                this._x1 = (ushort)(value / 2.5 * 32767);
            }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Угол_срабатывания")]
        public ushort CornerStart
        {
            get { return _corner1; }
            set { this._corner1 = value; }
        }

        [BindingProperty(3)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time1); }
            set { this._time1 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "tвз")]
        public int TimeVoz
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time2); }
            set { this._time2 = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "Уставка_возврата")]
        public double UstavkaVoz
        {
            get
            {
                double val = (short)_x2 * 2.5 / 32767;
                return Math.Round(val, 2);
            }
            set
            {
                this._x2 = (ushort)(value / 2.5 * 32767);
            }
        }

        [BindingProperty(6)]
        [XmlElement(ElementName = "Уставка_возврата_есть_нет")]
        public bool UstavkaVozBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        [BindingProperty(7)]
        [XmlElement(ElementName = "Ток_срабатывания")]
        public double Usti
        {
            get { return ValuesConverterCommon.GetUstavka40(this._usti); }
            set { this._usti = ValuesConverterCommon.SetUstavka40(value); }
        }

        [BindingProperty(8)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [XmlElement(ElementName = "БЛК")]
        [BindingProperty(9)]
        public string BlockTn
        {
            get { return Validator.Get(this._config1, StringsConfig.BlockTn,9,10); }
            set { this._config1 = Validator.Set(value, StringsConfig.BlockTn, this._config1, 9, 10); }
        }

        [BindingProperty(10)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "Апв_возврат")]
        public string ApvBack
        {
            get { return Validator.Get(this._config1, StringsConfig.OffOn, 2); }
            set { this._config1 = Validator.Set(value, StringsConfig.OffOn,_config1, 2); }
        }

        [BindingProperty(12)]
        [XmlElement(ElementName = "Уров")]
        public string Urov
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 2); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, _config, 2); }
        }

        [BindingProperty(13)]
        [XmlElement(ElementName = "АПВ")]
        public string Apv
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0); }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "Сброс_ступени")]
        public bool Reset
        {
            get { return Common.GetBit(this._config, 15); }
            set { this._config = Common.SetBit(this._config, 15, value); }
        }
    
        #endregion
    }
}
