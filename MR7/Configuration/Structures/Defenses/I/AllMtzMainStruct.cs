﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.MR7.Configuration.Structures.Defenses.I
{
    public class AllMtzMainStruct : StructBase, IDgvRowsContainer<MtzMainStruct>
    {
        [Layout(1, Count = 7)] private MtzMainStruct[] _mtzmain;

        [XmlArray(ElementName = "Все_МТЗ")]
        public MtzMainStruct[] Rows
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
    }
}
