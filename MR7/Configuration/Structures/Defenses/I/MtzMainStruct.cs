﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.Defenses.I
{
    /// <summary>
    /// конфигурациия основной ступени МТЗ защиты
    /// </summary>
    [XmlRoot(ElementName = "конфигурациия_МТЗ")]
    public class MtzMainStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _config;     //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)] private ushort _config1;    //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)] private ushort _block;      //вход блокировки
        [Layout(3)] private ushort _ust;        //уставка срабатывания_
        [Layout(4)] private ushort _time;       //время срабатывания_
        [Layout(5)] private ushort _k;          //коэфиц.зависимой хар-ки(в I2/I1  - вход для синхронизма U1нет/U2нет)
        [Layout(6)] private ushort _u;          //уставка пуска по напряжению
        [Layout(7)] private ushort _tu;         //время ускорения_
        [Layout(8)] private ushort _i21;        // уставка в %  (в I2/I1  - для 762 угол In1 )
        [Layout(9)] private ushort _usk;        //вход ускорения 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }

        [BindingProperty(2)]
        [XmlElement(ElementName = "Условие")]
        public string Condition
        {
            get { return Validator.Get(this._config1, StringsConfig.Condition, 10, 11); }
            set { this._config1 = Validator.Set(value, StringsConfig.Condition, this._config1, 10, 11); }
        }

        /// <summary>
        /// Привязка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Привязка")]
        public string Binding
        {
            get { return Validator.Get(this._config, StringsConfig.Binding, 14, 15); }
            set { this._config = Validator.Set(value, StringsConfig.Binding, this._config, 14, 15); }
        }

        /// <summary>
        /// Uпуск (есть/нет)
        /// </summary>
        [XmlElement(ElementName = "Uпуск")]
        [BindingProperty(4)]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Блокировка от неисправности ТН
        /// </summary>
        [XmlElement(ElementName = "Блок_от_неиспр_ТН")]
        [BindingProperty(6)]
        public string BlockTn
        {
            get { return Validator.Get(this._config1, StringsConfig.BlockTn, 8,9); }
            set { this._config1 = Validator.Set(value, StringsConfig.BlockTn, this._config1, 8,9); }
        }

        /// <summary>
        /// Направление
        /// </summary>
        [XmlElement(ElementName = "Направление")]
        [BindingProperty(7)]
        public string DirectionXml
        {
            get { return Validator.Get(this._config, StringsConfig.Direction, 6, 7); }
            set { this._config = Validator.Set(value, StringsConfig.Direction, this._config, 6, 7); }
        }

        /// <summary>
        /// Недост. напр(I*)
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Недост_напр(I*)")]
        public string UndirXml
        {
            get { return Validator.Get(this._config, StringsConfig.Undir, 8); }
            set { this._config = Validator.Set(value, StringsConfig.Undir, this._config, 8); }
        }
        
        /// <summary>
        /// Логика
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Логика")]
        public string LogicXml
        {
            get { return Validator.Get(this._config, StringsConfig.Logic, 12, 13); }
            set { this._config = Validator.Set(value, StringsConfig.Logic, this._config, 12, 13); }
        }

        /// <summary>
        /// Характеристика
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "Характеристика")]
        public string CharacteristicXml
        {
            get { return Validator.Get(this._config, StringsConfig.Characteristic, 4); }
            set { this._config = Validator.Set(value, StringsConfig.Characteristic, this._config, 4); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// K
        /// </summary>
        [BindingProperty(12)]
        [XmlElement(ElementName = "K")]
        public ushort K
        {
            get { return this._k; }
            set { this._k = value; }
        }

        [BindingProperty(13)]
        public string Uskor
        {
            get { return Validator.Get(this._usk, StringsConfig.RelaySignals); }
            set { this._usk = Validator.Set(value, StringsConfig.RelaySignals); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(14)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }
        
        /// <summary>
        /// Блокировка
        /// </summary>
        [BindingProperty(15)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return Validator.Get(this._block, StringsConfig.SwitchSignals); }
            set { this._block = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        /// <summary>
        /// Уставка I2/I1
        /// </summary>
        [BindingProperty(16)]
        [XmlElement(ElementName = "Уставка_2г_1г")]
        public double UstavkaPersent2G1G
        {
            get { return ValuesConverterCommon.GetUstavka100(this._i21); }
            set { this._i21 = ValuesConverterCommon.SetUstavka100(value); }
        }

        /// <summary>
        /// Пуск 2г/1г (I)
        /// </summary>
        [BindingProperty(17)]
        [XmlElement(ElementName = "Пуск_2г_1г")]
        public bool Start2G1GXml
        {
            get { return Common.GetBit(this._config, 9); }
            set { this._config = Common.SetBit(this._config, 9, value); }
        }

        /// <summary>
        /// Перекл. блок
        /// </summary>
        [BindingProperty(18)]
        [XmlElement(ElementName = "Перекл_блок")]
        public bool SwitchBlockXml
        {
            get { return Common.GetBit(this._config, 10); }
            set { this._config = Common.SetBit(this._config, 10, value); }
        }

        [BindingProperty(19)]
        [XmlElement(ElementName = "Ненапр.приУСКОР")]
        public bool NenaprUskor
        {
            get { return Common.GetBit(this._config1, 7); }
            set { this._config1 = Common.SetBit(this._config1, 7, value); }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(20)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(21)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }
        
        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(22)]
        [XmlElement(ElementName = "АПВ_режим")]
        public string ApvMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 0); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 0); }
        }
        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(23)]
        [XmlElement(ElementName = "АВР_режим")]
        public string AvrMode
        {
            get { return Validator.Get(this._config1, StringsConfig.Apv, 1); }
            set { this._config1 = Validator.Set(value, StringsConfig.Apv, this._config1, 1); }
        }
        #endregion [Properties]
    }
}
