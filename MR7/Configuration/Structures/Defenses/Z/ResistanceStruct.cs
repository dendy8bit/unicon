﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;

namespace BEMN.MR7.Configuration.Structures.Defenses.Z
{
    [XmlRoot(ElementName = "конфигурациия_конпенсации_НП_и_углов")]
    public class ResistanceStruct : StructBase
    {
        [Layout(0)] private ushort _r0Step1;
        [Layout(1)] private ushort _x0Step1;
        [Layout(2)] private ushort _r1Step1;
        [Layout(3)] private ushort _x1Step1;
        [Layout(4)] private ushort _r0Step2;
        [Layout(5)] private ushort _x0Step2;
        [Layout(6)] private ushort _r1Step2;
        [Layout(7)] private ushort _x1Step2;
        [Layout(8)] private ushort _r0Step3;
        [Layout(9)] private ushort _x0Step3;
        [Layout(10)] private ushort _r1Step3;
        [Layout(11)] private ushort _x1Step3;
        [Layout(12)] private ushort _r0Step4;
        [Layout(13)] private ushort _x0Step4;
        [Layout(14)] private ushort _r1Step4;
        [Layout(15)] private ushort _x1Step4;
        [Layout(16)] private ushort _r0Step5;
        [Layout(17)] private ushort _x0Step5;
        [Layout(18)] private ushort _r1Step5;
        [Layout(19)] private ushort _x1Step5;
        [Layout(20)] private ushort _c1; //угол 1 (начало зоны)
        [Layout(21)] private ushort _c2; //угол 2 (конец зоны)

        [BindingProperty(0)]
        public double R0Step1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r0Step1); }
            set { this._r0Step1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(1)]
        public double X0Step1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._x0Step1); }
            set { this._x0Step1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(2)]
        public double R1Step1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._r1Step1); }
            set { this._r1Step1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(3)]
        public double X1Step1
        {
            get { return ValuesConverterCommon.GetUstavka256(this._x1Step1); }
            set { this._x1Step1 = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(4)]
        public ushort C1
        {
            get { return this._c1; }
            set { this._c1 = value; }
        }

        [BindingProperty(5)]
        public ushort C2
        {
            get { return this._c2; }
            set { this._c2 = value; }
        }
    }
}
