﻿using System.Drawing;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR7.Configuration.Structures.AcCountLoad;
using BEMN.MR7.Configuration.Structures.Swing;

namespace BEMN.MR7.Configuration.Structures.Defenses.Z
{
    public class ConfigResistDiagramStruct : StructBase
    {
        [BindingProperty(0)]
        public ResistanceStruct ResistanceStruct { get; set; }

        [BindingProperty(1)]
        public AcCountLoadStruct AcCountLoadStruct { get; set; }

        [BindingProperty(2)]
        public SwingStruct Oscillation { get; set; }

        [BindingProperty(3)]
        public AllResistanceDefensesStruct ResistanceDefensesStruct { get; set; }
        
        #region SaveOptions

        public PieChartOptions GetPieChartOptions()
        {
            PieChartOptions options = new PieChartOptions();
            this.SetChannelPieChartOptions(options);
            this.SetZnOptions(options);
            this.SetPieChartDirectionCharacteristicOption(options);
            this.SetSwingOptions(options);
            this.SetPieChartDefChaaracteristics(options);

            return options;
        }

        private void SetChannelPieChartOptions(PieChartOptions options)
        {
            options.IaChannel = new ChannelPieChartOptions("Ia")
            {
                A = true
            };
            options.IbChannel = new ChannelPieChartOptions("Ib")
            {
                B = true
            };
            options.IcChannel = new ChannelPieChartOptions("Ic")
            {
                C = true
            };
          
            options.UaChannel = new ChannelPieChartOptions("Ua")
            {
                A = true
            };
            options.UbChannel = new ChannelPieChartOptions("Ub")
            {
                B = true
            };
            options.UcChannel = new ChannelPieChartOptions("Uc")
            {
                C = true
            };
        }

        private void SetZnOptions(PieChartOptions options)
        {
            ZnOptions z1 = new ZnOptions
            {
                R0 = this.ResistanceStruct.R0Step1,
                X0 = this.ResistanceStruct.X0Step1,
                R1 = this.ResistanceStruct.R1Step1,
                X1 = this.ResistanceStruct.X1Step1
            };
            options.Z1 = z1;
        }

        private void SetPieChartDirectionCharacteristicOption(PieChartOptions options)
        {
            PieChartDirectionCharacteristicOption corners = new PieChartDirectionCharacteristicOption();
            corners.F1 = this.ResistanceStruct.C1;
            corners.F2 = this.ResistanceStruct.C2;
            options.Corners = corners;

            PieChartChargeCharacteristicOption lin = new PieChartChargeCharacteristicOption();
            lin.R1 = this.AcCountLoadStruct.R1Line ;
            lin.R2 = this.AcCountLoadStruct.R2Line ;
            lin.F = this.AcCountLoadStruct.CornerLine;
            PieChartChargeCharacteristicOption faz = new PieChartChargeCharacteristicOption();
            faz.R1 = this.AcCountLoadStruct.R1Faza ;
            faz.R2 = this.AcCountLoadStruct.R2Faza ;
            faz.F = this.AcCountLoadStruct.CornerFaza;
            options.Linear = lin;
            options.Phase = faz;
        }

        private void SetSwingOptions(PieChartOptions options)
        {
            if (this.Oscillation.Type == StringsConfig.ResistDefType[0])
            {
                PieChartPolyCharacteristicOption oscillationPoly = new PieChartPolyCharacteristicOption();
                oscillationPoly.R = this.Oscillation.R1 ;
                oscillationPoly.X = this.Oscillation.X1 ;
                oscillationPoly.F = this.Oscillation.C1;
              
                oscillationPoly.BlockFromLoad = Block.NONE;
                oscillationPoly.Direction = Direction.NONE;
                oscillationPoly.Color = Color.FromArgb(-12703965);
                options.Kachanie = oscillationPoly;

                if( (this.Oscillation.R1 !=0)&&(this.Oscillation.X1 !=0))
                {
                    
              
                oscillationPoly = new PieChartPolyCharacteristicOption();
                oscillationPoly.R = (this.Oscillation.R1 + this.Oscillation.Dr) ;
                oscillationPoly.X = (this.Oscillation.X1 + this.Oscillation.Dr) ;
                oscillationPoly.F = this.Oscillation.C1;
              
                oscillationPoly.BlockFromLoad = Block.NONE;
                oscillationPoly.Direction = Direction.NONE;
                oscillationPoly.Color = Color.FromArgb(-12703965);
                    }  
                options.Kachanie2 = oscillationPoly;
            }
            else
            {
                PieChartRoundCharacteristicOption oscillationRound = new PieChartRoundCharacteristicOption();
                oscillationRound.R = this.Oscillation.R1 ;
                oscillationRound.X = this.Oscillation.X1 ;
                oscillationRound.Radius = this.Oscillation.C1;
                oscillationRound.BlockFromLoad = Block.NONE;
                oscillationRound.Direction = Direction.NONE;
                oscillationRound.Color = Color.FromArgb(-12703965);
                options.Kachanie = oscillationRound;

                if (this.Oscillation.C1 != 0) 
                {
                    oscillationRound = new PieChartRoundCharacteristicOption();
                    oscillationRound.R = this.Oscillation.R1 ;
                    oscillationRound.X = this.Oscillation.X1 ;
                    oscillationRound.Radius = (this.Oscillation.C1 + this.Oscillation.Dr) ;
                    oscillationRound.BlockFromLoad = Block.NONE;
                    oscillationRound.Color = Color.FromArgb(-12703965);
                    oscillationRound.Direction = Direction.NONE;
                }
                options.Kachanie2 = oscillationRound;
            }
        }

        private void SetPieChartDefChaaracteristics(PieChartOptions options)
        {
            Color[] colors = new Color[]
                {
                    Color.FromArgb(0x50dc143c),
                    Color.FromArgb(0x50aa00ff),
                    Color.FromArgb(0x502196f3),
                    Color.FromArgb(0x5000796b),
                    Color.FromArgb(0x50cddc39),
                    Color.FromArgb(0x50ff9800),
                    Color.FromArgb(0x50607d8b),
                    Color.FromArgb(0x508d6e63),
                    Color.FromArgb(0x5064dd17),
                    Color.FromArgb(0x50ffff00)
                };
            for (int i = 0; i < AllResistanceDefensesStruct.RESIST_DEF_COUNT; i++)
            {
                if (this.ResistanceDefensesStruct[i].Mode == StringsConfig.DefenseModes[0])
                    continue;
                if (this.ResistanceDefensesStruct[i].Type == StringsConfig.ResistDefType[0])
                {
                    PieChartPolyCharacteristicOption poly = new PieChartPolyCharacteristicOption();
                    poly.R = this.ResistanceDefensesStruct[i].UstR ;
                    poly.X = this.ResistanceDefensesStruct[i].UstX ;
                    poly.F = this.ResistanceDefensesStruct[i].Ustr;
                    poly.BlockFromLoad = this.ResistanceDefensesStruct[i].BlockFromLoad 
                        ? this.GetBlockEnum(this.ResistanceDefensesStruct[i].ContureInd) 
                        : Block.NONE;
                    poly.Direction = this.GetDirectionEnum(this.ResistanceDefensesStruct[i].DirectionInd);
                    poly.Name = string.Format("Ступень Z {0}", i + 1);
                    poly.Color = colors[i];                    
                    options.Characteristics.Add(poly);
            
                }
                else
                {
                    PieChartRoundCharacteristicOption round = new PieChartRoundCharacteristicOption();
                    round.R = this.ResistanceDefensesStruct[i].UstR ;
                    round.X = this.ResistanceDefensesStruct[i].UstX ;
                    round.Radius = this.ResistanceDefensesStruct[i].Ustr ;
                    round.BlockFromLoad = this.ResistanceDefensesStruct[i].BlockFromLoad
                        ? this.GetBlockEnum(this.ResistanceDefensesStruct[i].ContureInd)
                        : Block.NONE;
                    round.Direction = this.GetDirectionEnum(this.ResistanceDefensesStruct[i].DirectionInd);
                    round.Name = string.Format("Ступень Z {0}", i + 1);
                    round.Color = colors[i];
                    options.Characteristics.Add(round);
                  
                }
            }
        }

        private Block GetBlockEnum(int ind)
        {
            return ind == 0 ? Block.LINE : Block.PHASE;
        }

        private Direction GetDirectionEnum(int ind)
        {
            switch (ind)
            {
                case 0: return Direction.NONE;
                case 1: return Direction.DIRECT;
                case 2: return Direction.INVERSION;
                default: return Direction.NONE;
            }
        }
        #endregion SaveOptions
    }
}

