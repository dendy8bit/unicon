﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MR7.Configuration.Structures.Defenses.BlockDefense;
using BEMN.MR7.Configuration.Structures.Defenses.Diff;
using BEMN.MR7.Configuration.Structures.Defenses.External;
using BEMN.MR7.Configuration.Structures.Defenses.F;
using BEMN.MR7.Configuration.Structures.Defenses.I;
using BEMN.MR7.Configuration.Structures.Defenses.I2I1;
using BEMN.MR7.Configuration.Structures.Defenses.Istar;
using BEMN.MR7.Configuration.Structures.Defenses.P;
using BEMN.MR7.Configuration.Structures.Defenses.Q;
using BEMN.MR7.Configuration.Structures.Defenses.StartArc;
using BEMN.MR7.Configuration.Structures.Defenses.U;
using BEMN.MR7.Configuration.Structures.Defenses.Z;

namespace BEMN.MR7.Configuration.Structures.Defenses
{
    /// <summary>
    /// список защит
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class DefensesSetpointsStruct : StructBase
    {
        [Layout(0)] private CornerStruct _corner;                           //углы
        [Layout(1)] private DiffmainStruct _diffmain;                       //диф ступень
        [Layout(2)] private DiffCutOffStruct _cutOff;                       //диф отсечка
        [Layout(3)] private AllDiffIzeroStruct _diffIzero;                  //диф нулевой последовательности
        [Layout(4)] private AllMtzMainStruct _mtzmain;                      //мтз основная
        [Layout(5)] private AllDefenseStarStruct _mtzmaini0;                //мтз I*
        [Layout(6)] private DefenseI2I1Struct _mtzi2I1;                     //обрыв провода
        [Layout(7)] private StartArcProt _arc;                              //конфигурация пуска дуговой защиты
        [Layout(8)] private AllDefenceUStruct _u;                           //защиты по напряжению >
        [Layout(9)] private AllDefenseFStruct _fB;                          //защиты по частоте >
        [Layout(10)] private AllDefenseFStruct _fM;                         //защиты по частоте <
        [Layout(11)] private AllDefenseQStruct _qDefenses;                  //мтз Q>
        [Layout(12)] private DefenseTermBlockStruct _termblock;             //блокировка по тепловой модели
        [Layout(13)] private AllDefenseExternalStruct _allExternalDefenses; //Внешние защиты
        [Layout(14)] private AllResistanceDefensesStruct _allResistDefense;//Защиты по сопротивлению
        [Layout(15)] private ResistanceDefensesStruct _reserve;             //резерв на месте защиты Z>7
        [Layout(16)] private AllDefenseReversPower _power;                  //Power def

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornerStruct Corner
        {
            get { return this._corner; }
            set { this._corner = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "Диф_ступень")]
        public DiffmainStruct Diffmain
        {
            get { return this._diffmain; }
            set { this._diffmain = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "Диф_отсечка")]
        public DiffCutOffStruct DiffCutOff
        {
            get { return this._cutOff; }
            set { this._cutOff = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "Диф_нулевой_последовательности")]
        public AllDiffIzeroStruct AllDiffIzero
        {
            get { return this._diffIzero; }
            set { this._diffIzero = value; }
        }
        [BindingProperty(4)]
        [XmlElement(ElementName = "I")]
        public AllMtzMainStruct Mtzmain
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
        [BindingProperty(5)]
        [XmlElement(ElementName = "I*")]
        public AllDefenseStarStruct Mtzmaini0
        {
            get { return this._mtzmaini0; }
            set { this._mtzmaini0 = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "I2I1")]
        public DefenseI2I1Struct Mtzi2I1
        {
            get { return this._mtzi2I1; }
            set { this._mtzi2I1 = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Конфигурация_пуска_дуговой_защиты")]
        public StartArcProt StartArc
        {
            get { return this._arc; }
            set { this._arc = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Ub")]
        public AllDefenceUStruct Ub
        {
            get { return this._u; }
            set { this._u = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "Fb")]
        public AllDefenseFStruct Fb
        {
            get { return this._fB; }
            set { this._fB = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Fm")]
        public AllDefenseFStruct Fm
        {
            get { return this._fM; }
            set { this._fM = value; }
        }
        [BindingProperty(11)]
        [XmlElement(ElementName = "Q")]
        public AllDefenseQStruct QDefenses
        {
            get { return this._qDefenses; }
            set { this._qDefenses = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "Тепловая")]
        public DefenseTermBlockStruct Termblock
        {
            get { return this._termblock; }
            set { this._termblock = value; }
        }
        [BindingProperty(13)]
        [XmlElement(ElementName = "Внешние")]
        public AllDefenseExternalStruct ExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }

        [BindingProperty(14)]
        [XmlElement(ElementName = "По_сопротивлению")]
        public AllResistanceDefensesStruct ResistanceDefenses
        {
            get { return this._allResistDefense; }
            set { this._allResistDefense = value; }
        }

        [BindingProperty(15)]
        [XmlElement(ElementName = "По_мощности")]
        public AllDefenseReversPower PowerDefenses
        {
            get { return this._power; }
            set { this._power = value; }
        }
    }
}
