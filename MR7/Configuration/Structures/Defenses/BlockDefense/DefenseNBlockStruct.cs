﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR7.Configuration.Structures.Defenses.BlockDefense
{
    /// <summary>
    /// блокировка пуска двигателя по числу пусков
    /// </summary>
    [Serializable]
    public class DefenseNBlockStruct :StructBase
    {
        [Layout(0)] private ushort _numHot;
        [Layout(1)] private ushort _numCold;
        [Layout(2)] private ushort _weitBlk;
        [Layout(3)] private ushort _rez; 


        /// <summary>
        /// число горячих пусков двигателя
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "число_горячих_пусков_двигателя")]
        public ushort HotStarts
        {
            get { return _numHot; }
            set { _numHot = value; }
        }

        /// <summary>
        /// число холодных пусков двигателя
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "число_холодных_пусков_двигателя")]
        public ushort ColdStarts
        {
            get { return _numCold; }
            set { _numCold = value; }
        }

        /// <summary>
        /// время блокировки
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "время_блокировки")]
        public ushort BlockTime
        {
            get { return _weitBlk; }
            set { _weitBlk = value; }
        }
    }
}
