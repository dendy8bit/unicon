﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.HFL
{
    public class DzStruct : StructBase
    {
        [Layout(0)] private ushort _config;             //конфигурация
        [Layout(1)] private ushort _hfs;                //вход высокочастотного сигнала
        [Layout(2)] private ushort _time_echo;          //время задержки ВЧС (tср ТС)
        [Layout(3)] private ushort _umin_echo;          //пуск по напряжению
        [Layout(4)] private ushort _block_echo;         //блокировка логики эхо сигнала
        [Layout(5)] private ushort _time_hfs;           //время возврата ВЧС (tвз ТС)
        [Layout(6)] private ushort _ctrl_hls;           //телесигнал контроля ВЧ по ДЗ или по ТЗНП
        [Layout(7)] private ushort _time_initdeblock;   //время перезапуска схемы деблокировки
        [Layout(8)] private ushort _time_fics;          //время фиксации ступеней с сокр. и обратной зоной в схемах блок.
        [Layout(9)] private ushort _time_off;           //импульс отключения выкл.
        [Layout(10)] private ushort _time_fics_ext;     //время фиксации ИО для расширеной зоны в логике ВЧБ
        [Layout(11)] private ushort _block_off;         //блокировка выходного сигнала отключения
        [Layout(12)] private ushort _block_hfs;         //блокировка выходного сигнала отправки ВЧ
        [Layout(13)] private ushort _time_deblock;      //время срабатывания схемы деблокировки
        [Layout(14)] private ushort _block_reverse;     //блокировка логики определения реверса тока
        [Layout(15)] private ushort _abbreviatf;        //сокращенная зона F-F для ДЗ, или сокращенная зона для ТЗНП
        [Layout(16)] private ushort _extendf;           //расширеная зона F-F для ДЗ, или расширеная зона для ТЗНП
        [Layout(17)] private ushort _reversef;          //обратная зона F-F для ДЗ, или обратная зона для ТЗНП
        [Layout(18)] private ushort _abbreviatn;        //сокращенная зона F-N для ДЗ, или резерв для ТЗНП
        [Layout(19)] private ushort _extendn;           //расширеная зона F-N для ДЗ, или резерв для ТЗНП
        [Layout(20)] private ushort _reversen;          //обратная зона F- N для ДЗ, или резерв для ТЗНП
        [Layout(21)] private ushort _time_rev;          //время реверса

        [BindingProperty(0)]
        public string DzMode
        {
            get { return Validator.Get(this._config, StringsConfig.HflModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.HflModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        public bool KonturFF
        {
            get { return Common.GetBit(this._config, 10); }
            set { this._config = Common.SetBit(this._config, 10, value); }
        }
        
        [BindingProperty(2)]
        public string AbbrevFF
        {
            get { return Validator.Get(this._abbreviatf > 0? (ushort)(this._abbreviatf - 136) : this._abbreviatf, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._abbreviatf = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(3)]
        public string ExtendFF
        {
            get { return Validator.Get(this._extendf > 0 ? (ushort)(this._extendf - 136) : this._extendf, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._extendf = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(4)]
        public string ReverseFF
        {
            get { return Validator.Get(this._reversef > 0 ? (ushort)(this._reversef - 136) : this._reversef, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._reversef = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(5)]
        public bool KonturFN
        {
            get { return Common.GetBit(this._config, 11); }
            set { this._config = Common.SetBit(this._config, 11, value); }
        }

        [BindingProperty(6)]
        public string AbbrevFN
        {
            get { return Validator.Get(this._abbreviatn > 0 ? (ushort)(this._abbreviatn - 136) : this._abbreviatn, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._abbreviatn = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(7)]
        public string ExtendFN
        {
            get { return Validator.Get(this._extendn > 0 ? (ushort)(this._extendn - 136) : this._extendn, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._extendn = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(8)]
        public string ReverseFN
        {
            get { return Validator.Get(this._reversen > 0 ? (ushort)(this._reversen - 136) : this._reversen, StringsConfig.HflSignalsDZ); }
            set
            {
                ushort val = Validator.Set(value, StringsConfig.HflSignalsDZ);
                this._reversen = val > 0 ? (ushort)(val + 136) : val;
            }
        }

        [BindingProperty(9)]
        public string TS
        {
            get { return Validator.Get(this._hfs, StringsConfig.SwitchSignals); }
            set { this._hfs = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(10)]
        public int TimeReturnTs
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_hfs); }
            set { this._time_hfs = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(11)]
        public string Deblock
        {
            get { return Validator.Get(this._config, StringsConfig.HflDeblock, 4, 5); }
            set { this._config = Validator.Set(value, StringsConfig.HflDeblock, this._config, 4, 5); }
        }

        [BindingProperty(12)]
        public string ControlTS
        {
            get { return Validator.Get(this._ctrl_hls, StringsConfig.SwitchSignals); }
            set { this._ctrl_hls = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(13)]
        public int TimeDeblock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_initdeblock); }
            set { this._time_initdeblock = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(14)]
        public int TimeMinImpulse
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_fics); }
            set { this._time_fics = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(15)]
        public int TimeOff
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_off); }
            set { this._time_off = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(16)]
        public int TimeOffTB
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_fics_ext); }
            set { this._time_fics_ext = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(17)]
        public string BlockOff
        {
            get { return Validator.Get(this._block_off, StringsConfig.SwitchSignals); }
            set { this._block_off = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(18)]
        public string BlockTS
        {
            get { return Validator.Get(this._block_hfs, StringsConfig.SwitchSignals); }
            set { this._block_hfs = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        
        [BindingProperty(19)]
        public string Osc
        {
            get { return Validator.Get(this._config, StringsConfig.Apv, 14); }
            set { this._config = Validator.Set(value, StringsConfig.Apv, this._config, 14); }
        }

        [BindingProperty(20)]
        public string UROV
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 12); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 12); }
        }

        [BindingProperty(21)]
        public string APV
        {
            get { return Validator.Get(this._config, StringsConfig.Apv, 13); }
            set { this._config = Validator.Set(value, StringsConfig.Apv, this._config, 13); }
        }

        [BindingProperty(22)]
        public string ModeEcho
        {
            get { return Validator.Get(this._config, StringsConfig.HflEcho, 2, 3); }
            set { this._config = Validator.Set(value, StringsConfig.HflEcho, this._config, 2, 3); }
        }

        [BindingProperty(23)]
        public int OnTS
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_echo); }
            set { this._time_echo = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(24)]
        public double Umin
        {
            get { return ValuesConverterCommon.GetUstavka256(this._umin_echo); }
            set { this._umin_echo = ValuesConverterCommon.SetUstavka256(value); }
        }

        [BindingProperty(25)]
        public string BlockEcho
        {
            get { return Validator.Get(this._block_echo, StringsConfig.SwitchSignals); }
            set { this._block_echo = Validator.Set(value, StringsConfig.SwitchSignals); }
        }

        [BindingProperty(26)]
        public string BlockMode
        {
            get { return Validator.Get(this._config, StringsConfig.BlockTn, 8, 9); }
            set { this._config = Validator.Set(value, StringsConfig.BlockTn, this._config, 8, 9); }
        }

        [BindingProperty(27)]
        public string ModeReverse
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 6); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 6); }
        }

        [BindingProperty(28)]
        public int Tdeblock
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_deblock); }
            set { this._time_deblock = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(29)]
        public int Tfics
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time_rev); }
            set { this._time_rev = ValuesConverterCommon.SetWaitTime(value); }
        }

        [BindingProperty(30)]
        public bool Zone
        {
            get { return Common.GetBit(this._config, 7); }
            set { this._config = Common.SetBit(this._config, 7, value); }
        }

        [BindingProperty(31)]
        public string BlockReverse
        {
            get { return Validator.Get(this._block_reverse, StringsConfig.SwitchSignals); }
            set { this._block_reverse = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
