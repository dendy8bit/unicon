﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR7.Configuration.Structures.Engine
{
    /// <summary>
    /// Конфигурация тепловой модели
    /// </summary>
    [XmlRoot(ElementName = "конфигурациия_тепловой_модели")]
    public class TermConfigStruct : StructBase 
    {
        #region [Private fields]

        [Layout(0)] private ushort _config;             //привязка к стороне 0,1,2 РЕЗЕРВ!!!
        [Layout(1)] private ushort _timeHot;            //постоянная времени нагрева
        [Layout(2)] private ushort _timeCold;           //постоянная времени охлаждения
        [Layout(3)] private ushort _termNom;            //номинальный ток
        [Layout(4)] private ushort _i_val;              //уставка срабатывания пуска двигателя
        [Layout(5)] private ushort _i_wait;             //время пуска двигателя
        [Layout(6)] private ushort _hotpusk;            //уставка для горячего пуска
        [Layout(7)] private ushort _reset;              //вход сброса тепловой модели (теплового состояния)
        [Layout(8)] private ushort _numReset;           //вход сброса тепловой модели (числа пусков)
        [Layout(9)] private ushort _wait;				//время за которое считается число пусков
        #endregion [Private fields]


        #region [Properties]
         /// <summary>
        /// постоянная времени нагрева
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Tгор")]
        public ushort TimeHot
        {
            get { return this._timeHot; }
            set { this._timeHot = value; }
        }
        /// <summary>
        /// постоянная времени охлаждения
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Tхол")]
        public ushort TimeCold
        {
            get { return this._timeCold; }
            set { this._timeCold = value; }
        }
        /// <summary>
        /// Ток двигателя
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "Iдв")]
        public double Idv
        {
            get { return ValuesConverterCommon.GetIn(this._termNom); }
            set { this._termNom = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Ток пуска
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Iпуск")]
        public double Istart
        {
            get { return ValuesConverterCommon.GetIn(this._i_val); }
            set { this._i_val = ValuesConverterCommon.SetIn(value); }
        }
        /// <summary>
        /// Время пуска двигателя
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "Tпуск")]
        public int TimeStart
        {
            get { return ValuesConverterCommon.GetWaitTime(this._i_wait); }
            set { this._i_wait = ValuesConverterCommon.SetWaitTime(value); }
        }
        /// <summary>
        /// время длительности
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Tдлит")]
        public ushort Time
        {
            get { return this._wait; }
            set { this._wait = value; }
        }
        /// <summary>
        /// Уставка Q
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "Q")]
        public double Q
        {
            get { return ValuesConverterCommon.GetUstavka256(this._hotpusk); }
            set { this._hotpusk = ValuesConverterCommon.SetUstavka256(value); }
        }
        /// <summary>
        /// Q сброс
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Q_сброс")]
        public string Qreset
        {
            get { return Validator.Get(this._reset, StringsConfig.SwitchSignals); }
            set { this._reset = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        /// <summary>
        /// N сброс
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "N_сброс")]
        public string Nreset
        {
            get { return Validator.Get(this._numReset, StringsConfig.SwitchSignals); }
            set { this._numReset = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        #endregion [Properties]

    }
}
