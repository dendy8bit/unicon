﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR7.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурация входного логического сигнала
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        private const int COUNT = 12;

        public static int DiscretsCount
        {
            get
            {
                switch (StringsConfig.DeviceType)
                {
                    case Mr7.T12N5D58R51:
                    case Mr7.T12N6D58R51:
                    case Mr7.T9N8D58R51:
                        return 56;
                    case Mr7.T12N4D26R19:
                        return 24;
                    case Mr7.T6N3D42R35:
                        return 40;
                    default:
                        return 40;
                }
            }
        }


        #region [Private fields]

        [Layout(0, Count = COUNT)] private ushort[] _a;

        #endregion [Private fields]

        [XmlIgnore]
        private ushort[] Mass
        {
            get { return this._a; }
            set { this._a = value; }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                return Validator.Get(sourse, StringsConfig.LsState, pos, pos + 1);
            }

            set
            {
                ushort sourse = this.Mass[ls/8];
                int pos = (ls*2)%16;
                sourse = Validator.Set(value, StringsConfig.LsState, sourse, pos, pos + 1);
                ushort[] mass = this.Mass;
                mass[ls/8] = sourse;
                this.Mass = mass;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {

            for (int i = 0; i < DiscretsCount; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
