﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using System.Xml.Serialization;

namespace BEMN.MR7.Configuration.Structures.MeasuringTransformer
{
    public class AllKanalsStruct : StructBase
    {
        [Layout(0, Count = 3)] private KanalTransU[] _uabc;

        [BindingProperty(0)]
        public KanalTransU this[int index]
        {
            get { return this._uabc[index]; }
            set { this._uabc[index] = value; }
        }

        public int Length { get { return this._uabc.Length; } }
        
        [XmlElement(ElementName = "Все_каналы")]
        public KanalTransU[] AllUchannels
        {
            get { return this._uabc; }
            set { this._uabc = value; }
        }
    }
}
