﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.MR7.Configuration.Structures.MeasuringTransformer
{
    public class AllSidesStruct : StructBase
    {
        [Layout(0, Count = 4)] private SideStruct[] _sides;
        
        [BindingProperty(0)]
        public SideStruct this[int index]
        {
            get { return this._sides[index]; }
            set { this._sides[index] = value; }
        }

        public int Length { get { return this._sides.Length; } }

        [XmlElement(ElementName = "Все_стороны")]
        public SideStruct[] Sides
        {
            get { return this._sides; }
            set { this._sides = value; }
        }

    }
}
