﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.MR7.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурациия измерительных трансформаторов
    /// </summary>
    public class MeasureTransStruct : StructBase
    {
        [Layout(0)] private AllSidesStruct _sides;
        [Layout(1)] private AllKanalsStruct _uabc;
        [Layout(2)] private ushort _type;
        [Layout(3)] private ushort _res;

        [BindingProperty(0)]
        public AllSidesStruct Sides
        {
            get { return this._sides; }
            set { this._sides = value; }
        }

        [BindingProperty(1)]
        public AllKanalsStruct Uabc
        {
            get { return this._uabc; }
            set { this._uabc = value; }
        }

        [BindingProperty(2)]
        public string CountWinding
        {
            get { return Validator.Get(this._type, StringsConfig.CountWinding); }
            set { this._type = Validator.Set(value, StringsConfig.CountWinding); }
        }

        
    }
}
