﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.MR7.Configuration.Structures.MeasuringTransformer
{
    public class KanalTransU : StructBase
    {
        [Layout(0)] private ushort _inputUa;    //номер входа Ua группы Uabc1
        [Layout(1)] private ushort _inputUb;    //номер входа Ub группы Uabc1
        [Layout(2)] private ushort _inputUc;    //номер входа Uc группы Uabc1
        [Layout(3)] private ushort _Utt;        //Ктн
        [Layout(4)] private ushort _alminp;     //вход внешней неисправности ТН
        [Layout(5)] private ushort _config;     //конфигурация ТН

        [BindingProperty(0)]
        public string KanalUType
        {
            get { return Validator.Get(this._config, StringsConfig.KanalUtype, 3); }
            set { this._config = Validator.Set(value, StringsConfig.KanalUtype, this._config, 3); }
        }

        [BindingProperty(1)]
        public string KanalUSide
        {
            get { return Validator.Get(this._config, StringsConfig.KanalUside, 0, 1, 2); }
            set { this._config = Validator.Set(value, StringsConfig.KanalUside, this._config, 0, 1, 2); }
        }

        [BindingProperty(2)]
        public string InpUaStr
        {
            get { return Validator.Get(this._inputUa, StringsConfig.NumberInputU); }
            set { this._inputUa = Validator.Set(value, StringsConfig.NumberInputU); }
        }

        public int InpUa
        {
            get { return this._inputUa; }
        }

        [BindingProperty(3)]
        public string InpUbStr
        {
            get { return Validator.Get(this._inputUb, StringsConfig.NumberInputU); }
            set { this._inputUb = Validator.Set(value, StringsConfig.NumberInputU); }
        }

        public int InpUb
        {
            get { return this._inputUb; }
        }

        [BindingProperty(4)]
        public string InpUcStr
        {
            get { return Validator.Get(this._inputUc, StringsConfig.NumberInputU); }
            set { this._inputUc = Validator.Set(value, StringsConfig.NumberInputU); }
        }

        public int InpUc
        {
            get { return this._inputUc; }
        }

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(5)]
        [XmlIgnore]
        public double Kth
        {
            get { return ValuesConverterCommon.GetKth(this._Utt); }
            set { this._Utt = ValuesConverterCommon.SetKthRound(value); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthValue
        {
            get
            {
                double ktn = Common.SetBit(this._Utt, 15, false);
                return Common.GetBit(this._Utt, 15)
                    ? ktn * 1000 / 256
                    : ktn / 256;
            }
        }
        /// <summary>
        /// KTHL Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTH")]
        public double KthXml
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._Utt);
                return Common.GetBit(this._Utt, 15)
                    ? ktn * 1000
                    : ktn;
            }
            set { }
        }

        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(6)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._Utt, StringsConfig.KthKoefs, 15); }
            set { this._Utt = Validator.Set(value, StringsConfig.KthKoefs, this._Utt, 15); }
        }

        /// <summary>
        /// Вход неисправности ТН
        /// </summary>
        [BindingProperty(7)]
        public string AlmInp
        {
            get { return Validator.Get(this._alminp, StringsConfig.SwitchSignals); }
            set { this._alminp = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
    }
}
