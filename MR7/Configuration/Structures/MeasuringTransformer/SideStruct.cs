﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using System.Xml.Serialization;

namespace BEMN.MR7.Configuration.Structures.MeasuringTransformer
{
    public class SideStruct : StructBase
    {
        [Layout(0)] private ushort _urw;
        [Layout(1)] private ushort _srw;
        [Layout(2)] private ushort _inputIa;
        [Layout(3)] private ushort _inputIb;
        [Layout(4)] private ushort _inputIc;
        [Layout(5)] private ushort _inputIn;
        [Layout(6)] private ushort _ittl;
        [Layout(7)] private ushort _ittx;
        [Layout(8)] private ushort _config;
        [Layout(9)] private ushort _res;

        [BindingProperty(0)]
        public string InpIaStr
        {
            get { return Validator.Get(this._inputIa, StringsConfig.NumberInputI); }
            set { this._inputIa = Validator.Set(value, StringsConfig.NumberInputI); }
        }

        public int InpIa
        {
            get { return this._inputIa; }
        }

        [BindingProperty(1)]
        public string InpIbStr
        {
            get { return Validator.Get(this._inputIb, StringsConfig.NumberInputI); }
            set { this._inputIb = Validator.Set(value, StringsConfig.NumberInputI); }
        }
        public int InpIb
        {
            get { return this._inputIb; }
        }

        [BindingProperty(2)]
        public string InpIcStr
        {
            get { return Validator.Get(this._inputIc, StringsConfig.NumberInputI); }
            set { this._inputIc = Validator.Set(value, StringsConfig.NumberInputI); }
        }
        public int InpIc
        {
            get { return this._inputIc; }
        }

        [BindingProperty(3)]
        public string InpInStr
        {
            get { return Validator.Get(this._inputIn, StringsConfig.NumberInputI); }
            set { this._inputIn = Validator.Set(value, StringsConfig.NumberInputI); }
        }
        public int InpIn
        {
            get { return this._inputIn; }
        }

        [BindingProperty(4)]
        public double S
        {
            get { return this._srw == ushort.MaxValue ? 1024 : (double)this._srw / 64; }
            set { this._srw = 1024.0 - value <= 0.1 ? ushort.MaxValue : (ushort)(value * 64); }
        }

        [BindingProperty(5)]
        public double U
        {
            get { return Math.Round(this._urw == ushort.MaxValue ? 1024 : (double) this._urw / 64, 1, MidpointRounding.AwayFromZero); }
            set { this._urw = 1024.0 - value < 0.1 ? ushort.MaxValue : (ushort) (value * 64); }
        }

        [BindingProperty(6)]
        public string SideType
        {
            get { return Validator.Get(this._config, StringsConfig.SideType, 4, 5); }
            set { this._config = Validator.Set(value, StringsConfig.SideType, this._config, 4, 5); }
        }

        [BindingProperty(7)]
        public string Group
        {
            get { return Validator.Get(this._config, StringsConfig.SideGroup, 6, 7, 8, 9); }
            set { this._config = Validator.Set(value, StringsConfig.SideGroup, this._config, 6, 7, 8, 9); }
        }

        [BindingProperty(8)]
        public string SignTT
        {
            get { return Validator.Get(this._config, StringsConfig.SignTT, 1); }
            set { this._config = Validator.Set(value, StringsConfig.SignTT, this._config, 1); }
        }
        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "конфигурация_ТТ")]
        public ushort Ittl
        {
            get { return this._ittl; }
            set { this._ittl = value; }
        }

        [BindingProperty(10)]
        public string SignTTNP
        {
            get { return Validator.Get(this._config, StringsConfig.SignTT, 2); }
            set { this._config = Validator.Set(value, StringsConfig.SignTT, this._config, 2); }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "конфигурация_ТТНП")]
        public ushort Ittx
        {
            get { return this._ittx; }
            set { this._ittx = value; }
        }

        [BindingProperty(12)]
        public string CoeffTT
        {
            get { return Validator.Get(this._config, StringsConfig.CoefSchemaTT, 3); }
            set { this._config = Validator.Set(value, StringsConfig.CoefSchemaTT, this._config, 3); }
        }

        [BindingProperty(13)]
        public string InputCurrentStr
        {
            get { return Validator.Get(this._config, StringsConfig.InpIn, 10); }
            set { this._config = Validator.Set(value, StringsConfig.InpIn, this._config, 10); }
        }

        [XmlIgnore]
        public int InputCurrent
        {
            get { return Validator.Get(this._config, StringsConfig.InpIn, 10) == StringsConfig.InpIn[0] ? 1 : 5; }
        }

        [BindingProperty(14)]
        public string TypeTT
        {
            get { return Validator.Get(this._config, StringsConfig.TtType, 11); }
            set { this._config = Validator.Set(value, StringsConfig.TtType, this._config, 11); }
        }

        [BindingProperty(15)]
        public string PolarisUabc
        {
            get { return Validator.Get(this._config, StringsConfig.PolarisUabc, 12, 13); }
            set { this._config = Validator.Set(value, StringsConfig.PolarisUabc, this._config, 12, 13); }
        }

        [BindingProperty(16)]
        public string PolarisUn
        {
            get { return Validator.Get(this._config, StringsConfig.PolarisUn, 14, 15); }
            set { this._config = Validator.Set(value, StringsConfig.PolarisUn, this._config, 14, 15); }
        }
    }
}
