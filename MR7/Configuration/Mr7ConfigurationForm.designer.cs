﻿namespace BEMN.MR7.Configuration
{
    partial class Mr7ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle124 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle125 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle126 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle127 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle128 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle129 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle130 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle131 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle132 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle133 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle134 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle135 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle136 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle137 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle138 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle139 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle140 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle141 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle142 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle143 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle144 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle145 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSetpointsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label109 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._clearSetpointBtn = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveFileResistCharacteristic = new System.Windows.Forms.SaveFileDialog();
            this._gooseTabPage = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.goin64 = new System.Windows.Forms.ComboBox();
            this.goin48 = new System.Windows.Forms.ComboBox();
            this.goin32 = new System.Windows.Forms.ComboBox();
            this.label281 = new System.Windows.Forms.Label();
            this.label282 = new System.Windows.Forms.Label();
            this.label283 = new System.Windows.Forms.Label();
            this.goin63 = new System.Windows.Forms.ComboBox();
            this.goin47 = new System.Windows.Forms.ComboBox();
            this.goin31 = new System.Windows.Forms.ComboBox();
            this.label284 = new System.Windows.Forms.Label();
            this.label285 = new System.Windows.Forms.Label();
            this.label286 = new System.Windows.Forms.Label();
            this.goin62 = new System.Windows.Forms.ComboBox();
            this.goin46 = new System.Windows.Forms.ComboBox();
            this.goin30 = new System.Windows.Forms.ComboBox();
            this.label287 = new System.Windows.Forms.Label();
            this.label288 = new System.Windows.Forms.Label();
            this.label289 = new System.Windows.Forms.Label();
            this.goin61 = new System.Windows.Forms.ComboBox();
            this.goin45 = new System.Windows.Forms.ComboBox();
            this.goin29 = new System.Windows.Forms.ComboBox();
            this.label290 = new System.Windows.Forms.Label();
            this.label291 = new System.Windows.Forms.Label();
            this.label292 = new System.Windows.Forms.Label();
            this.goin60 = new System.Windows.Forms.ComboBox();
            this.goin44 = new System.Windows.Forms.ComboBox();
            this.goin28 = new System.Windows.Forms.ComboBox();
            this.label293 = new System.Windows.Forms.Label();
            this.label294 = new System.Windows.Forms.Label();
            this.label295 = new System.Windows.Forms.Label();
            this.goin59 = new System.Windows.Forms.ComboBox();
            this.goin43 = new System.Windows.Forms.ComboBox();
            this.goin27 = new System.Windows.Forms.ComboBox();
            this.label296 = new System.Windows.Forms.Label();
            this.label297 = new System.Windows.Forms.Label();
            this.label298 = new System.Windows.Forms.Label();
            this.goin58 = new System.Windows.Forms.ComboBox();
            this.goin42 = new System.Windows.Forms.ComboBox();
            this.goin26 = new System.Windows.Forms.ComboBox();
            this.label299 = new System.Windows.Forms.Label();
            this.label300 = new System.Windows.Forms.Label();
            this.label301 = new System.Windows.Forms.Label();
            this.goin57 = new System.Windows.Forms.ComboBox();
            this.goin41 = new System.Windows.Forms.ComboBox();
            this.goin25 = new System.Windows.Forms.ComboBox();
            this.label302 = new System.Windows.Forms.Label();
            this.label303 = new System.Windows.Forms.Label();
            this.label304 = new System.Windows.Forms.Label();
            this.goin56 = new System.Windows.Forms.ComboBox();
            this.goin40 = new System.Windows.Forms.ComboBox();
            this.goin24 = new System.Windows.Forms.ComboBox();
            this.label305 = new System.Windows.Forms.Label();
            this.label306 = new System.Windows.Forms.Label();
            this.label307 = new System.Windows.Forms.Label();
            this.goin55 = new System.Windows.Forms.ComboBox();
            this.goin39 = new System.Windows.Forms.ComboBox();
            this.goin23 = new System.Windows.Forms.ComboBox();
            this.label308 = new System.Windows.Forms.Label();
            this.label309 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.goin54 = new System.Windows.Forms.ComboBox();
            this.goin38 = new System.Windows.Forms.ComboBox();
            this.goin22 = new System.Windows.Forms.ComboBox();
            this.label311 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.goin53 = new System.Windows.Forms.ComboBox();
            this.goin37 = new System.Windows.Forms.ComboBox();
            this.goin21 = new System.Windows.Forms.ComboBox();
            this.label314 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.goin52 = new System.Windows.Forms.ComboBox();
            this.goin36 = new System.Windows.Forms.ComboBox();
            this.goin20 = new System.Windows.Forms.ComboBox();
            this.label317 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.goin51 = new System.Windows.Forms.ComboBox();
            this.goin35 = new System.Windows.Forms.ComboBox();
            this.goin19 = new System.Windows.Forms.ComboBox();
            this.label320 = new System.Windows.Forms.Label();
            this.label321 = new System.Windows.Forms.Label();
            this.label322 = new System.Windows.Forms.Label();
            this.goin50 = new System.Windows.Forms.ComboBox();
            this.goin34 = new System.Windows.Forms.ComboBox();
            this.goin18 = new System.Windows.Forms.ComboBox();
            this.label323 = new System.Windows.Forms.Label();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.goin49 = new System.Windows.Forms.ComboBox();
            this.label326 = new System.Windows.Forms.Label();
            this.goin33 = new System.Windows.Forms.ComboBox();
            this.label327 = new System.Windows.Forms.Label();
            this.goin17 = new System.Windows.Forms.ComboBox();
            this.label328 = new System.Windows.Forms.Label();
            this.goin16 = new System.Windows.Forms.ComboBox();
            this.label329 = new System.Windows.Forms.Label();
            this.goin15 = new System.Windows.Forms.ComboBox();
            this.label330 = new System.Windows.Forms.Label();
            this.goin14 = new System.Windows.Forms.ComboBox();
            this.label331 = new System.Windows.Forms.Label();
            this.goin13 = new System.Windows.Forms.ComboBox();
            this.label332 = new System.Windows.Forms.Label();
            this.goin12 = new System.Windows.Forms.ComboBox();
            this.label333 = new System.Windows.Forms.Label();
            this.goin11 = new System.Windows.Forms.ComboBox();
            this.label334 = new System.Windows.Forms.Label();
            this.goin10 = new System.Windows.Forms.ComboBox();
            this.label335 = new System.Windows.Forms.Label();
            this.goin9 = new System.Windows.Forms.ComboBox();
            this.label336 = new System.Windows.Forms.Label();
            this.goin8 = new System.Windows.Forms.ComboBox();
            this.label337 = new System.Windows.Forms.Label();
            this.goin7 = new System.Windows.Forms.ComboBox();
            this.label338 = new System.Windows.Forms.Label();
            this.goin6 = new System.Windows.Forms.ComboBox();
            this.label339 = new System.Windows.Forms.Label();
            this.goin5 = new System.Windows.Forms.ComboBox();
            this.label340 = new System.Windows.Forms.Label();
            this.goin4 = new System.Windows.Forms.ComboBox();
            this.label341 = new System.Windows.Forms.Label();
            this.goin3 = new System.Windows.Forms.ComboBox();
            this.label342 = new System.Windows.Forms.Label();
            this.goin2 = new System.Windows.Forms.ComboBox();
            this.label343 = new System.Windows.Forms.Label();
            this.goin1 = new System.Windows.Forms.ComboBox();
            this.label344 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.operationBGS = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.currentBGS = new System.Windows.Forms.ComboBox();
            this._automatPage = new System.Windows.Forms.TabPage();
            this._switchYesNo = new System.Windows.Forms.CheckBox();
            this.UROVgroupBox = new System.Windows.Forms.GroupBox();
            this._urovSelf = new System.Windows.Forms.CheckBox();
            this._urovBkCheck = new System.Windows.Forms.CheckBox();
            this._urovIcheck = new System.Windows.Forms.CheckBox();
            this._tUrov1 = new System.Windows.Forms.MaskedTextBox();
            this._urovBlock = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this._tUrov2 = new System.Windows.Forms.MaskedTextBox();
            this._Iurov = new System.Windows.Forms.MaskedTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this._urovPusk = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this._switchConfigGroupBox = new System.Windows.Forms.GroupBox();
            this._switchSideCB = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this._comandOtkl = new System.Windows.Forms.ComboBox();
            this._controlSolenoidCombo = new System.Windows.Forms.ComboBox();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._blockSDTU = new System.Windows.Forms.ComboBox();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._blockSDTUlabel = new System.Windows.Forms.Label();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._oscChannelsGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._baseCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._oscSygnal = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.oscStartCb = new System.Windows.Forms.ComboBox();
            this.label65 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox54 = new System.Windows.Forms.GroupBox();
            this.label193 = new System.Windows.Forms.Label();
            this._fixErrorFCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this._resetAlarmCheckBox = new System.Windows.Forms.CheckBox();
            this._resetSystemCheckBox = new System.Windows.Forms.CheckBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this._fault6CheckBox = new System.Windows.Forms.CheckBox();
            this.label121 = new System.Windows.Forms.Label();
            this._fault5CheckBox = new System.Windows.Forms.CheckBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this.label111 = new System.Windows.Forms.Label();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox175 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._out1IndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignal2Col = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox176 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._inputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this._grUst2ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this._grUst1ComboBox = new System.Windows.Forms.ComboBox();
            this._setpointPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._inpAddCombo = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._resistCheck = new System.Windows.Forms.CheckBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._applyCopySetpoinsButton = new System.Windows.Forms.Button();
            this._copySetpoinsGroupComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._setpointsTab = new System.Windows.Forms.TabControl();
            this._measureTransPage = new System.Windows.Forms.TabPage();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this._transformPage = new System.Windows.Forms.TabPage();
            this._numberOfWindingComboBox = new System.Windows.Forms.ComboBox();
            this.label237 = new System.Windows.Forms.Label();
            this.groupBox56 = new System.Windows.Forms.GroupBox();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.sideTransformControl1 = new BEMN.MR7.Configuration.SideTransformControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.sideTransformControl2 = new BEMN.MR7.Configuration.SideTransformControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.sideTransformControl3 = new BEMN.MR7.Configuration.SideTransformControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.sideTransformControl4 = new BEMN.MR7.Configuration.SideTransformControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.groupParamSideControl1 = new BEMN.MR7.Configuration.GroupParamSideControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.groupParamSideControl2 = new BEMN.MR7.Configuration.GroupParamSideControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.groupParamSideControl3 = new BEMN.MR7.Configuration.GroupParamSideControl();
            this._enginePage = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this._engineNreset = new System.Windows.Forms.ComboBox();
            this._engineQreset = new System.Windows.Forms.ComboBox();
            this._qBox = new System.Windows.Forms.MaskedTextBox();
            this.label215 = new System.Windows.Forms.Label();
            this._tCount = new System.Windows.Forms.MaskedTextBox();
            this.label216 = new System.Windows.Forms.Label();
            this._tStartBox = new System.Windows.Forms.MaskedTextBox();
            this.label217 = new System.Windows.Forms.Label();
            this._iStartBox = new System.Windows.Forms.MaskedTextBox();
            this.label218 = new System.Windows.Forms.Label();
            this._engineIdv = new System.Windows.Forms.MaskedTextBox();
            this.label219 = new System.Windows.Forms.Label();
            this._engineCoolingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label220 = new System.Windows.Forms.Label();
            this._engineHeatingTimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label221 = new System.Windows.Forms.Label();
            this._controlPage = new System.Windows.Forms.TabPage();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label139 = new System.Windows.Forms.Label();
            this._resetTnGr1 = new System.Windows.Forms.ComboBox();
            this._i0u0CheckGr1 = new System.Windows.Forms.CheckBox();
            this._i2u2CheckGr1 = new System.Windows.Forms.CheckBox();
            this._tdContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label137 = new System.Windows.Forms.Label();
            this._iMinContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label130 = new System.Windows.Forms.Label();
            this._uMinContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label125 = new System.Windows.Forms.Label();
            this._u0ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this._u2ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._tsContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._uDelContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label128 = new System.Windows.Forms.Label();
            this._iDelContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._iMaxContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label123 = new System.Windows.Forms.Label();
            this._uMaxContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label120 = new System.Windows.Forms.Label();
            this._i0ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label122 = new System.Windows.Forms.Label();
            this._i2ContrCepGr1 = new System.Windows.Forms.MaskedTextBox();
            this._diffPage = new System.Windows.Forms.TabPage();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this._diffDefPage = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._AVRDTZComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label113 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this._APVDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label91 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._modeI5I1 = new System.Windows.Forms.CheckBox();
            this._perBlockI5I1 = new System.Windows.Forms.ComboBox();
            this.label92 = new System.Windows.Forms.Label();
            this._I5I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this._UROVDTZComboBox = new System.Windows.Forms.ComboBox();
            this._blockingDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this._timeEnduranceDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._modeI2I1 = new System.Windows.Forms.CheckBox();
            this._perBlockI2I1 = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this._I2I1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this._constraintDTZTextBox = new System.Windows.Forms.MaskedTextBox();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label119 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._K2TangensTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label133 = new System.Windows.Forms.Label();
            this._Ib2BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label134 = new System.Windows.Forms.Label();
            this._K1AngleOfSlopeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._Ib1BeginTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label135 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this._modeDTZComboBox = new System.Windows.Forms.ComboBox();
            this.label160 = new System.Windows.Forms.Label();
            this._diffCutOffPage = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._AVRDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._oscDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this._APVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this._timeEnduranceDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this._constraintDTOBTTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._blockingDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this._UROVDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._stepOnInstantValuesDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this._modeDTOBTComboBox = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this._diffZeroPage = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._dif0DataGreed = new System.Windows.Forms.DataGridView();
            this._dif0StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0IdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0InColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0TdColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Intg1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Ib2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0Intg2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dif0OscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0UrovColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0APVColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._dif0AVRColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defResistGr1 = new System.Windows.Forms.TabPage();
            this.resistanceDefTabCtr1 = new BEMN.MR7.Configuration.ResistanceDefTabCtr();
            this._defIPage = new System.Windows.Forms.TabPage();
            this._startArcDefPage = new System.Windows.Forms.TabControl();
            this._cornerGr1Page = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this._i2CornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._inCornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._i0CornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._iCornerGr1 = new System.Windows.Forms.MaskedTextBox();
            this._defIGr1Page = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this._difensesI56DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn53 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn54 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn55 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn56 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn57 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn58 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn59 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn60 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn61 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn15 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn19 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn62 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn20 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn63 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn64 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._difensesI14DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.inIm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.avrIb = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._difensesI7DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn31 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.inImm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn12 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn32 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn33 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn34 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn35 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn36 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn37 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn38 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn39 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn40 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn13 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn41 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn14 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn42 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.avrImm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defIStarGr1 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._difensesI0DataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn43 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn16 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn44 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn45 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._chooseCBColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iComboBoxCell = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn47 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn48 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn49 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn50 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn17 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn18 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn51 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._avrI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defI2I1Gr1 = new System.Windows.Forms.TabPage();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this._urovI2I1CheckBox = new System.Windows.Forms.CheckBox();
            this._sideI2I1ComboBox = new System.Windows.Forms.ComboBox();
            this.i2i1AVR = new System.Windows.Forms.ComboBox();
            this.i2i1APV1 = new System.Windows.Forms.ComboBox();
            this.I2I1OscComboGr1 = new System.Windows.Forms.ComboBox();
            this.I2I1tcpTBGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.I2I1BlockingComboGr1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.I2I1TBGr1 = new System.Windows.Forms.MaskedTextBox();
            this.I2I1ModeComboGr1 = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this.OscArc = new System.Windows.Forms.CheckBox();
            this.SideArcComboBox = new System.Windows.Forms.ComboBox();
            this.StartBlockArcComboBox = new System.Windows.Forms.ComboBox();
            this.label167 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.IcpArcTextBox = new System.Windows.Forms.MaskedTextBox();
            this.StartArcModeComboBox = new System.Windows.Forms.ComboBox();
            this.label171 = new System.Windows.Forms.Label();
            this._defUGr1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._difensesUBDataGrid = new System.Windows.Forms.DataGridView();
            this._uBStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sideUb = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAPVColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrUmax = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._difensesUMDataGrid = new System.Windows.Forms.DataGridView();
            this._uMStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sideUm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._block5V = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMBlockingUMColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMAPVColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._avrMax = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defFGr1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._difensesFBDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn70 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn71 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn21 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn72 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn73 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn22 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn23 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn74 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn24 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn75 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._difensesFMDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMAPVColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.avrFM = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._defDvigGr1 = new System.Windows.Forms.TabPage();
            this.groupBox52 = new System.Windows.Forms.GroupBox();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this._numHot = new System.Windows.Forms.MaskedTextBox();
            this._numCold = new System.Windows.Forms.MaskedTextBox();
            this.label187 = new System.Windows.Forms.Label();
            this._waitNumBlock = new System.Windows.Forms.MaskedTextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this._engineDefensesGrid = new System.Windows.Forms.DataGridView();
            this._engineDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseBlockCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseOscCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAPV1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.avrQ = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this._engineQconstraintGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._engineQtimeGr1 = new System.Windows.Forms.MaskedTextBox();
            this._engineQmodeGr1 = new System.Windows.Forms.ComboBox();
            this._defExtGr1 = new System.Windows.Forms.TabPage();
            this._externalDefenses = new System.Windows.Forms.DataGridView();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPV1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.avrExt = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._powerPage = new System.Windows.Forms.TabPage();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this._reversePowerGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn52 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._ssr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._usr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tsr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tvz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._svz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._svzbeno = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._isr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._block = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._blk = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._osc = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._apvvoz = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._urov = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._apv = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._resetstep = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._lsGr = new System.Windows.Forms.TabPage();
            this.groupBox58 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsOR = new System.Windows.Forms.TreeView();
            this.groupBox57 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsAND = new System.Windows.Forms.TreeView();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this._lsOrDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn15 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this._lsOrDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn16 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this._lsOrDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn17 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this._lsOrDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn18 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this._lsOrDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn19 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this._lsOrDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn20 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this._lsOrDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn21 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage38 = new System.Windows.Forms.TabPage();
            this._lsOrDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn22 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._clearCurrentLsnButton = new System.Windows.Forms.Button();
            this._clearAllLsnSygnalButton = new System.Windows.Forms.Button();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this._lsAndDgv1Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn23 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage40 = new System.Windows.Forms.TabPage();
            this._lsAndDgv2Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn24 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage41 = new System.Windows.Forms.TabPage();
            this._lsAndDgv3Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn25 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage42 = new System.Windows.Forms.TabPage();
            this._lsAndDgv4Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn26 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage43 = new System.Windows.Forms.TabPage();
            this._lsAndDgv5Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn27 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage44 = new System.Windows.Forms.TabPage();
            this._lsAndDgv6Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn28 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage45 = new System.Windows.Forms.TabPage();
            this._lsAndDgv7Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn29 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage46 = new System.Windows.Forms.TabPage();
            this._lsAndDgv8Gr1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn30 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._clearCurrentLsButton = new System.Windows.Forms.Button();
            this._clearAllLsSygnalButton = new System.Windows.Forms.Button();
            this._VLSGr = new System.Windows.Forms.TabPage();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this._wrapBtn = new System.Windows.Forms.Button();
            this.treeViewForVLS = new System.Windows.Forms.TreeView();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLSclb1Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLSclb2Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLSclb3Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLSclb4Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLSclb5Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLSclb6Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLSclb7Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLSclb8Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLSclb9Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLSclb10Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLSclb11Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLSclb12Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLSclb13Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLSclb14Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLSclb15Gr1 = new System.Windows.Forms.CheckedListBox();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this.VLSclb16Gr1 = new System.Windows.Forms.CheckedListBox();
            this._resetCurrentVlsButton = new System.Windows.Forms.Button();
            this._resetAllVlsButton = new System.Windows.Forms.Button();
            this._apvGr1 = new System.Windows.Forms.TabPage();
            this.groupBox50 = new System.Windows.Forms.GroupBox();
            this.label166 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this._avrClear = new System.Windows.Forms.ComboBox();
            this.label173 = new System.Windows.Forms.Label();
            this._avrTOff = new System.Windows.Forms.MaskedTextBox();
            this.label174 = new System.Windows.Forms.Label();
            this._avrTBack = new System.Windows.Forms.MaskedTextBox();
            this.label175 = new System.Windows.Forms.Label();
            this._avrBack = new System.Windows.Forms.ComboBox();
            this.label176 = new System.Windows.Forms.Label();
            this._avrTSr = new System.Windows.Forms.MaskedTextBox();
            this.label177 = new System.Windows.Forms.Label();
            this._avrResolve = new System.Windows.Forms.ComboBox();
            this.label178 = new System.Windows.Forms.Label();
            this._avrBlockClear = new System.Windows.Forms.ComboBox();
            this.label179 = new System.Windows.Forms.Label();
            this._avrBlocking = new System.Windows.Forms.ComboBox();
            this.label180 = new System.Windows.Forms.Label();
            this._avrSIGNOn = new System.Windows.Forms.ComboBox();
            this.label181 = new System.Windows.Forms.Label();
            this._avrByDiff = new System.Windows.Forms.ComboBox();
            this.label182 = new System.Windows.Forms.Label();
            this._avrBySelfOff = new System.Windows.Forms.ComboBox();
            this.label183 = new System.Windows.Forms.Label();
            this._avrByOff = new System.Windows.Forms.ComboBox();
            this.label184 = new System.Windows.Forms.Label();
            this._avrBySignal = new System.Windows.Forms.ComboBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._blokFromUrov = new System.Windows.Forms.CheckBox();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this._apvKrat4Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvKrat3Gr1 = new System.Windows.Forms.MaskedTextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this._apvSwitchOffGr1 = new System.Windows.Forms.ComboBox();
            this._apvKrat2Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvKrat1Gr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvTreadyGr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvTblockGr1 = new System.Windows.Forms.MaskedTextBox();
            this._apvBlockGr1 = new System.Windows.Forms.ComboBox();
            this._timeDisable = new System.Windows.Forms.MaskedTextBox();
            this.label87 = new System.Windows.Forms.Label();
            this._typeDisable = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this._disableApv = new System.Windows.Forms.ComboBox();
            this.label149 = new System.Windows.Forms.Label();
            this._apvModeGr1 = new System.Windows.Forms.ComboBox();
            this.label150 = new System.Windows.Forms.Label();
            this._ksuppnGr1 = new System.Windows.Forms.TabPage();
            this.label82 = new System.Windows.Forms.Label();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._blockTNauto = new System.Windows.Forms.CheckBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._catchSinchrAuto = new System.Windows.Forms.CheckBox();
            this._sinhrAutodFnoGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this._waitSinchrAuto = new System.Windows.Forms.CheckBox();
            this._sinhrAutodFGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this._sinhrAutodFiGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this._sinhrAutoNoNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrAutoYesNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrAutoNoYesGr1 = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this._sinhrAutoUmaxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrAutoModeGr1 = new System.Windows.Forms.ComboBox();
            this._dUmaxAutoLabel = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this._blockTNmanual = new System.Windows.Forms.CheckBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this._catchSinchrManual = new System.Windows.Forms.CheckBox();
            this._sinhrManualdFnoGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this._waitSinchrManual = new System.Windows.Forms.CheckBox();
            this._sinhrManualdFGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this._sinhrManualdFiGr1 = new System.Windows.Forms.MaskedTextBox();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._sinhrManualNoNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrManualYesNoGr1 = new System.Windows.Forms.ComboBox();
            this._sinhrManualNoYesGr1 = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this._sinhrManualUmaxGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrManualModeGr1 = new System.Windows.Forms.ComboBox();
            this._dUmaxManualLabel = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this._discretIn3Cmb = new System.Windows.Forms.ComboBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._sinhrF = new System.Windows.Forms.MaskedTextBox();
            this._discretIn2Cmb = new System.Windows.Forms.ComboBox();
            this._sinhrKamp = new System.Windows.Forms.MaskedTextBox();
            this._sinhrTonGr1 = new System.Windows.Forms.MaskedTextBox();
            this._discretIn1Cmb = new System.Windows.Forms.ComboBox();
            this.label151 = new System.Windows.Forms.Label();
            this._blockSinhCmb = new System.Windows.Forms.ComboBox();
            this._sinhrTsinhrGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label141 = new System.Windows.Forms.Label();
            this._blockSinhLabel = new System.Windows.Forms.Label();
            this._sinhrTwaitGr1 = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._sinhrUmaxNalGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminNalGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminOtsGr1 = new System.Windows.Forms.MaskedTextBox();
            this._sinhrU2Gr1 = new System.Windows.Forms.ComboBox();
            this._sinhrU1Gr1 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._setpointsComboBox = new System.Windows.Forms.ComboBox();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this._logicElements = new System.Windows.Forms.TabPage();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this._virtualReleDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn68 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn69 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox53 = new System.Windows.Forms.GroupBox();
            this._rsTriggersDataGrid = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn65 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn66 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxColumn67 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._ethernetPage = new System.Windows.Forms.TabPage();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this._ipLo1 = new System.Windows.Forms.MaskedTextBox();
            this._ipLo2 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi1 = new System.Windows.Forms.MaskedTextBox();
            this._ipHi2 = new System.Windows.Forms.MaskedTextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.label213 = new System.Windows.Forms.Label();
            this.label212 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.contextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this._gooseTabPage.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._automatPage.SuspendLayout();
            this.UROVgroupBox.SuspendLayout();
            this._switchConfigGroupBox.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this._systemPage.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).BeginInit();
            this.groupBox3.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox54.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox175.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox176.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._inputSignalsPage.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this._setpointPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this._setpointsTab.SuspendLayout();
            this._measureTransPage.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this._transformPage.SuspendLayout();
            this.groupBox56.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this._enginePage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._controlPage.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this._diffPage.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this._diffDefPage.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this._diffCutOffPage.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this._diffZeroPage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).BeginInit();
            this._defResistGr1.SuspendLayout();
            this._defIPage.SuspendLayout();
            this._startArcDefPage.SuspendLayout();
            this._cornerGr1Page.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._defIGr1Page.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI56DataGrid)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI14DataGrid)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI7DataGrid)).BeginInit();
            this._defIStarGr1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).BeginInit();
            this._defI2I1Gr1.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this._defUGr1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).BeginInit();
            this._defFGr1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).BeginInit();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).BeginInit();
            this._defDvigGr1.SuspendLayout();
            this.groupBox52.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).BeginInit();
            this.groupBox43.SuspendLayout();
            this._defExtGr1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenses)).BeginInit();
            this._powerPage.SuspendLayout();
            this.groupBox47.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._reversePowerGrid)).BeginInit();
            this._lsGr.SuspendLayout();
            this.groupBox58.SuspendLayout();
            this.groupBox57.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).BeginInit();
            this.tabPage32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).BeginInit();
            this.tabPage33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).BeginInit();
            this.tabPage34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).BeginInit();
            this.tabPage35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).BeginInit();
            this.tabPage36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).BeginInit();
            this.tabPage37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).BeginInit();
            this.tabPage38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).BeginInit();
            this.groupBox45.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).BeginInit();
            this.tabPage40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).BeginInit();
            this.tabPage41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).BeginInit();
            this.tabPage42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).BeginInit();
            this.tabPage43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).BeginInit();
            this.tabPage44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).BeginInit();
            this.tabPage45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).BeginInit();
            this.tabPage46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).BeginInit();
            this._VLSGr.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this._apvGr1.SuspendLayout();
            this.groupBox50.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this._ksuppnGr1.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._configurationTabControl.SuspendLayout();
            this._logicElements.SuspendLayout();
            this.groupBox55.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).BeginInit();
            this.groupBox53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).BeginInit();
            this._ethernetPage.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenu
            // 
            this.contextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.resetSetpointsItem,
            this.clearSetpointsItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(223, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(222, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // resetSetpointsItem
            // 
            this.resetSetpointsItem.Name = "resetSetpointsItem";
            this.resetSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.resetSetpointsItem.Text = "Загрузить базовые уставки";
            // 
            // clearSetpointsItem
            // 
            this.clearSetpointsItem.Name = "clearSetpointsItem";
            this.clearSetpointsItem.Size = new System.Drawing.Size(222, 22);
            this.clearSetpointsItem.Text = "Обнулить уставки";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(222, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(222, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(222, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 116);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(169, 13);
            this.label109.TabIndex = 19;
            this.label109.Text = "5. Неисправность выключателя";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._clearSetpointBtn);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(984, 52);
            this.panel1.TabIndex = 0;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(857, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(123, 23);
            this._saveToXmlButton.TabIndex = 0;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._toolTip.SetToolTip(this._saveToXmlButton, "Сохранить конфигурацию в HTML файл");
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _clearSetpointBtn
            // 
            this._clearSetpointBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._clearSetpointBtn.AutoSize = true;
            this._clearSetpointBtn.Location = new System.Drawing.Point(435, 5);
            this._clearSetpointBtn.Name = "_clearSetpointBtn";
            this._clearSetpointBtn.Size = new System.Drawing.Size(119, 23);
            this._clearSetpointBtn.TabIndex = 3;
            this._clearSetpointBtn.Text = "Обнулить уставки";
            this._clearSetpointBtn.UseVisualStyleBackColor = true;
            this._clearSetpointBtn.Click += new System.EventHandler(this._clearSetpointsButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(292, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(137, 23);
            this._resetSetpointsButton.TabIndex = 4;
            this._resetSetpointsButton.Text = "Загрузить баз. уставки";
            this._toolTip.SetToolTip(this._resetSetpointsButton, "Загрузить базовые уставки");
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(152, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(134, 23);
            this._writeConfigBut.TabIndex = 5;
            this._writeConfigBut.Text = "Записать в устройство";
            this._toolTip.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 30);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(984, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(3, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(145, 23);
            this._readConfigBut.TabIndex = 6;
            this._readConfigBut.Text = "Прочитать из устройства";
            this._toolTip.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(731, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(120, 23);
            this._saveConfigBut.TabIndex = 1;
            this._saveConfigBut.Text = "Сохранить в файл";
            this._toolTip.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(602, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(123, 23);
            this._loadConfigBut.TabIndex = 2;
            this._loadConfigBut.Text = "Загрузить из файла";
            this._toolTip.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.Filter = "Уставки МР801 (*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР801";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.Filter = "Уставки МР801 (*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР801";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // saveFileResistCharacteristic
            // 
            this.saveFileResistCharacteristic.Filter = "(*.pco) | *.pco";
            // 
            // _gooseTabPage
            // 
            this._gooseTabPage.Controls.Add(this.groupBox14);
            this._gooseTabPage.Controls.Add(this.label36);
            this._gooseTabPage.Controls.Add(this.currentBGS);
            this._gooseTabPage.Location = new System.Drawing.Point(4, 22);
            this._gooseTabPage.Name = "_gooseTabPage";
            this._gooseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._gooseTabPage.Size = new System.Drawing.Size(976, 553);
            this._gooseTabPage.TabIndex = 11;
            this._gooseTabPage.Text = "БГС";
            this._gooseTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.groupBox19);
            this.groupBox14.Controls.Add(this.label345);
            this.groupBox14.Controls.Add(this.operationBGS);
            this.groupBox14.Location = new System.Drawing.Point(6, 36);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(950, 511);
            this.groupBox14.TabIndex = 9;
            this.groupBox14.TabStop = false;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.goin64);
            this.groupBox19.Controls.Add(this.goin48);
            this.groupBox19.Controls.Add(this.goin32);
            this.groupBox19.Controls.Add(this.label281);
            this.groupBox19.Controls.Add(this.label282);
            this.groupBox19.Controls.Add(this.label283);
            this.groupBox19.Controls.Add(this.goin63);
            this.groupBox19.Controls.Add(this.goin47);
            this.groupBox19.Controls.Add(this.goin31);
            this.groupBox19.Controls.Add(this.label284);
            this.groupBox19.Controls.Add(this.label285);
            this.groupBox19.Controls.Add(this.label286);
            this.groupBox19.Controls.Add(this.goin62);
            this.groupBox19.Controls.Add(this.goin46);
            this.groupBox19.Controls.Add(this.goin30);
            this.groupBox19.Controls.Add(this.label287);
            this.groupBox19.Controls.Add(this.label288);
            this.groupBox19.Controls.Add(this.label289);
            this.groupBox19.Controls.Add(this.goin61);
            this.groupBox19.Controls.Add(this.goin45);
            this.groupBox19.Controls.Add(this.goin29);
            this.groupBox19.Controls.Add(this.label290);
            this.groupBox19.Controls.Add(this.label291);
            this.groupBox19.Controls.Add(this.label292);
            this.groupBox19.Controls.Add(this.goin60);
            this.groupBox19.Controls.Add(this.goin44);
            this.groupBox19.Controls.Add(this.goin28);
            this.groupBox19.Controls.Add(this.label293);
            this.groupBox19.Controls.Add(this.label294);
            this.groupBox19.Controls.Add(this.label295);
            this.groupBox19.Controls.Add(this.goin59);
            this.groupBox19.Controls.Add(this.goin43);
            this.groupBox19.Controls.Add(this.goin27);
            this.groupBox19.Controls.Add(this.label296);
            this.groupBox19.Controls.Add(this.label297);
            this.groupBox19.Controls.Add(this.label298);
            this.groupBox19.Controls.Add(this.goin58);
            this.groupBox19.Controls.Add(this.goin42);
            this.groupBox19.Controls.Add(this.goin26);
            this.groupBox19.Controls.Add(this.label299);
            this.groupBox19.Controls.Add(this.label300);
            this.groupBox19.Controls.Add(this.label301);
            this.groupBox19.Controls.Add(this.goin57);
            this.groupBox19.Controls.Add(this.goin41);
            this.groupBox19.Controls.Add(this.goin25);
            this.groupBox19.Controls.Add(this.label302);
            this.groupBox19.Controls.Add(this.label303);
            this.groupBox19.Controls.Add(this.label304);
            this.groupBox19.Controls.Add(this.goin56);
            this.groupBox19.Controls.Add(this.goin40);
            this.groupBox19.Controls.Add(this.goin24);
            this.groupBox19.Controls.Add(this.label305);
            this.groupBox19.Controls.Add(this.label306);
            this.groupBox19.Controls.Add(this.label307);
            this.groupBox19.Controls.Add(this.goin55);
            this.groupBox19.Controls.Add(this.goin39);
            this.groupBox19.Controls.Add(this.goin23);
            this.groupBox19.Controls.Add(this.label308);
            this.groupBox19.Controls.Add(this.label309);
            this.groupBox19.Controls.Add(this.label310);
            this.groupBox19.Controls.Add(this.goin54);
            this.groupBox19.Controls.Add(this.goin38);
            this.groupBox19.Controls.Add(this.goin22);
            this.groupBox19.Controls.Add(this.label311);
            this.groupBox19.Controls.Add(this.label312);
            this.groupBox19.Controls.Add(this.label313);
            this.groupBox19.Controls.Add(this.goin53);
            this.groupBox19.Controls.Add(this.goin37);
            this.groupBox19.Controls.Add(this.goin21);
            this.groupBox19.Controls.Add(this.label314);
            this.groupBox19.Controls.Add(this.label315);
            this.groupBox19.Controls.Add(this.label316);
            this.groupBox19.Controls.Add(this.goin52);
            this.groupBox19.Controls.Add(this.goin36);
            this.groupBox19.Controls.Add(this.goin20);
            this.groupBox19.Controls.Add(this.label317);
            this.groupBox19.Controls.Add(this.label318);
            this.groupBox19.Controls.Add(this.label319);
            this.groupBox19.Controls.Add(this.goin51);
            this.groupBox19.Controls.Add(this.goin35);
            this.groupBox19.Controls.Add(this.goin19);
            this.groupBox19.Controls.Add(this.label320);
            this.groupBox19.Controls.Add(this.label321);
            this.groupBox19.Controls.Add(this.label322);
            this.groupBox19.Controls.Add(this.goin50);
            this.groupBox19.Controls.Add(this.goin34);
            this.groupBox19.Controls.Add(this.goin18);
            this.groupBox19.Controls.Add(this.label323);
            this.groupBox19.Controls.Add(this.label324);
            this.groupBox19.Controls.Add(this.label325);
            this.groupBox19.Controls.Add(this.goin49);
            this.groupBox19.Controls.Add(this.label326);
            this.groupBox19.Controls.Add(this.goin33);
            this.groupBox19.Controls.Add(this.label327);
            this.groupBox19.Controls.Add(this.goin17);
            this.groupBox19.Controls.Add(this.label328);
            this.groupBox19.Controls.Add(this.goin16);
            this.groupBox19.Controls.Add(this.label329);
            this.groupBox19.Controls.Add(this.goin15);
            this.groupBox19.Controls.Add(this.label330);
            this.groupBox19.Controls.Add(this.goin14);
            this.groupBox19.Controls.Add(this.label331);
            this.groupBox19.Controls.Add(this.goin13);
            this.groupBox19.Controls.Add(this.label332);
            this.groupBox19.Controls.Add(this.goin12);
            this.groupBox19.Controls.Add(this.label333);
            this.groupBox19.Controls.Add(this.goin11);
            this.groupBox19.Controls.Add(this.label334);
            this.groupBox19.Controls.Add(this.goin10);
            this.groupBox19.Controls.Add(this.label335);
            this.groupBox19.Controls.Add(this.goin9);
            this.groupBox19.Controls.Add(this.label336);
            this.groupBox19.Controls.Add(this.goin8);
            this.groupBox19.Controls.Add(this.label337);
            this.groupBox19.Controls.Add(this.goin7);
            this.groupBox19.Controls.Add(this.label338);
            this.groupBox19.Controls.Add(this.goin6);
            this.groupBox19.Controls.Add(this.label339);
            this.groupBox19.Controls.Add(this.goin5);
            this.groupBox19.Controls.Add(this.label340);
            this.groupBox19.Controls.Add(this.goin4);
            this.groupBox19.Controls.Add(this.label341);
            this.groupBox19.Controls.Add(this.goin3);
            this.groupBox19.Controls.Add(this.label342);
            this.groupBox19.Controls.Add(this.goin2);
            this.groupBox19.Controls.Add(this.label343);
            this.groupBox19.Controls.Add(this.goin1);
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Location = new System.Drawing.Point(6, 50);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(645, 455);
            this.groupBox19.TabIndex = 8;
            this.groupBox19.TabStop = false;
            // 
            // goin64
            // 
            this.goin64.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin64.FormattingEnabled = true;
            this.goin64.Location = new System.Drawing.Point(553, 424);
            this.goin64.Name = "goin64";
            this.goin64.Size = new System.Drawing.Size(75, 21);
            this.goin64.TabIndex = 33;
            // 
            // goin48
            // 
            this.goin48.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin48.FormattingEnabled = true;
            this.goin48.Location = new System.Drawing.Point(384, 424);
            this.goin48.Name = "goin48";
            this.goin48.Size = new System.Drawing.Size(75, 21);
            this.goin48.TabIndex = 33;
            // 
            // goin32
            // 
            this.goin32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin32.FormattingEnabled = true;
            this.goin32.Location = new System.Drawing.Point(217, 424);
            this.goin32.Name = "goin32";
            this.goin32.Size = new System.Drawing.Size(75, 21);
            this.goin32.TabIndex = 33;
            // 
            // label281
            // 
            this.label281.AutoSize = true;
            this.label281.Location = new System.Drawing.Point(505, 427);
            this.label281.Name = "label281";
            this.label281.Size = new System.Drawing.Size(42, 13);
            this.label281.TabIndex = 14;
            this.label281.Text = "GoIn64";
            // 
            // label282
            // 
            this.label282.AutoSize = true;
            this.label282.Location = new System.Drawing.Point(336, 427);
            this.label282.Name = "label282";
            this.label282.Size = new System.Drawing.Size(42, 13);
            this.label282.TabIndex = 14;
            this.label282.Text = "GoIn48";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.Location = new System.Drawing.Point(169, 427);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(42, 13);
            this.label283.TabIndex = 14;
            this.label283.Text = "GoIn32";
            // 
            // goin63
            // 
            this.goin63.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin63.FormattingEnabled = true;
            this.goin63.Location = new System.Drawing.Point(553, 397);
            this.goin63.Name = "goin63";
            this.goin63.Size = new System.Drawing.Size(75, 21);
            this.goin63.TabIndex = 29;
            // 
            // goin47
            // 
            this.goin47.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin47.FormattingEnabled = true;
            this.goin47.Location = new System.Drawing.Point(384, 397);
            this.goin47.Name = "goin47";
            this.goin47.Size = new System.Drawing.Size(75, 21);
            this.goin47.TabIndex = 29;
            // 
            // goin31
            // 
            this.goin31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin31.FormattingEnabled = true;
            this.goin31.Location = new System.Drawing.Point(217, 397);
            this.goin31.Name = "goin31";
            this.goin31.Size = new System.Drawing.Size(75, 21);
            this.goin31.TabIndex = 29;
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.Location = new System.Drawing.Point(505, 400);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(42, 13);
            this.label284.TabIndex = 15;
            this.label284.Text = "GoIn63";
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(336, 400);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(42, 13);
            this.label285.TabIndex = 15;
            this.label285.Text = "GoIn47";
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(169, 400);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(42, 13);
            this.label286.TabIndex = 15;
            this.label286.Text = "GoIn31";
            // 
            // goin62
            // 
            this.goin62.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin62.FormattingEnabled = true;
            this.goin62.Location = new System.Drawing.Point(553, 370);
            this.goin62.Name = "goin62";
            this.goin62.Size = new System.Drawing.Size(75, 21);
            this.goin62.TabIndex = 25;
            // 
            // goin46
            // 
            this.goin46.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin46.FormattingEnabled = true;
            this.goin46.Location = new System.Drawing.Point(384, 370);
            this.goin46.Name = "goin46";
            this.goin46.Size = new System.Drawing.Size(75, 21);
            this.goin46.TabIndex = 25;
            // 
            // goin30
            // 
            this.goin30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin30.FormattingEnabled = true;
            this.goin30.Location = new System.Drawing.Point(217, 370);
            this.goin30.Name = "goin30";
            this.goin30.Size = new System.Drawing.Size(75, 21);
            this.goin30.TabIndex = 25;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(505, 373);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(42, 13);
            this.label287.TabIndex = 17;
            this.label287.Text = "GoIn62";
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(336, 373);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(42, 13);
            this.label288.TabIndex = 17;
            this.label288.Text = "GoIn46";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.Location = new System.Drawing.Point(169, 373);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(42, 13);
            this.label289.TabIndex = 17;
            this.label289.Text = "GoIn30";
            // 
            // goin61
            // 
            this.goin61.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin61.FormattingEnabled = true;
            this.goin61.Location = new System.Drawing.Point(553, 343);
            this.goin61.Name = "goin61";
            this.goin61.Size = new System.Drawing.Size(75, 21);
            this.goin61.TabIndex = 21;
            // 
            // goin45
            // 
            this.goin45.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin45.FormattingEnabled = true;
            this.goin45.Location = new System.Drawing.Point(384, 343);
            this.goin45.Name = "goin45";
            this.goin45.Size = new System.Drawing.Size(75, 21);
            this.goin45.TabIndex = 21;
            // 
            // goin29
            // 
            this.goin29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin29.FormattingEnabled = true;
            this.goin29.Location = new System.Drawing.Point(217, 343);
            this.goin29.Name = "goin29";
            this.goin29.Size = new System.Drawing.Size(75, 21);
            this.goin29.TabIndex = 21;
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.Location = new System.Drawing.Point(505, 346);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(42, 13);
            this.label290.TabIndex = 16;
            this.label290.Text = "GoIn61";
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(336, 346);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(42, 13);
            this.label291.TabIndex = 16;
            this.label291.Text = "GoIn45";
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(169, 346);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(42, 13);
            this.label292.TabIndex = 16;
            this.label292.Text = "GoIn29";
            // 
            // goin60
            // 
            this.goin60.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin60.FormattingEnabled = true;
            this.goin60.Location = new System.Drawing.Point(553, 316);
            this.goin60.Name = "goin60";
            this.goin60.Size = new System.Drawing.Size(75, 21);
            this.goin60.TabIndex = 27;
            // 
            // goin44
            // 
            this.goin44.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin44.FormattingEnabled = true;
            this.goin44.Location = new System.Drawing.Point(384, 316);
            this.goin44.Name = "goin44";
            this.goin44.Size = new System.Drawing.Size(75, 21);
            this.goin44.TabIndex = 27;
            // 
            // goin28
            // 
            this.goin28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin28.FormattingEnabled = true;
            this.goin28.Location = new System.Drawing.Point(217, 316);
            this.goin28.Name = "goin28";
            this.goin28.Size = new System.Drawing.Size(75, 21);
            this.goin28.TabIndex = 27;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(505, 319);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(42, 13);
            this.label293.TabIndex = 13;
            this.label293.Text = "GoIn60";
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(336, 319);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(42, 13);
            this.label294.TabIndex = 13;
            this.label294.Text = "GoIn44";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.Location = new System.Drawing.Point(169, 319);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(42, 13);
            this.label295.TabIndex = 13;
            this.label295.Text = "GoIn28";
            // 
            // goin59
            // 
            this.goin59.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin59.FormattingEnabled = true;
            this.goin59.Location = new System.Drawing.Point(553, 289);
            this.goin59.Name = "goin59";
            this.goin59.Size = new System.Drawing.Size(75, 21);
            this.goin59.TabIndex = 23;
            // 
            // goin43
            // 
            this.goin43.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin43.FormattingEnabled = true;
            this.goin43.Location = new System.Drawing.Point(384, 289);
            this.goin43.Name = "goin43";
            this.goin43.Size = new System.Drawing.Size(75, 21);
            this.goin43.TabIndex = 23;
            // 
            // goin27
            // 
            this.goin27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin27.FormattingEnabled = true;
            this.goin27.Location = new System.Drawing.Point(217, 289);
            this.goin27.Name = "goin27";
            this.goin27.Size = new System.Drawing.Size(75, 21);
            this.goin27.TabIndex = 23;
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.Location = new System.Drawing.Point(505, 292);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(42, 13);
            this.label296.TabIndex = 11;
            this.label296.Text = "GoIn59";
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(336, 292);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(42, 13);
            this.label297.TabIndex = 11;
            this.label297.Text = "GoIn43";
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(169, 292);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(42, 13);
            this.label298.TabIndex = 11;
            this.label298.Text = "GoIn27";
            // 
            // goin58
            // 
            this.goin58.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin58.FormattingEnabled = true;
            this.goin58.Location = new System.Drawing.Point(553, 262);
            this.goin58.Name = "goin58";
            this.goin58.Size = new System.Drawing.Size(75, 21);
            this.goin58.TabIndex = 31;
            // 
            // goin42
            // 
            this.goin42.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin42.FormattingEnabled = true;
            this.goin42.Location = new System.Drawing.Point(384, 262);
            this.goin42.Name = "goin42";
            this.goin42.Size = new System.Drawing.Size(75, 21);
            this.goin42.TabIndex = 31;
            // 
            // goin26
            // 
            this.goin26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin26.FormattingEnabled = true;
            this.goin26.Location = new System.Drawing.Point(217, 262);
            this.goin26.Name = "goin26";
            this.goin26.Size = new System.Drawing.Size(75, 21);
            this.goin26.TabIndex = 31;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(505, 265);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(42, 13);
            this.label299.TabIndex = 10;
            this.label299.Text = "GoIn58";
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(336, 265);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(42, 13);
            this.label300.TabIndex = 10;
            this.label300.Text = "GoIn42";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.Location = new System.Drawing.Point(169, 265);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(42, 13);
            this.label301.TabIndex = 10;
            this.label301.Text = "GoIn26";
            // 
            // goin57
            // 
            this.goin57.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin57.FormattingEnabled = true;
            this.goin57.Location = new System.Drawing.Point(553, 235);
            this.goin57.Name = "goin57";
            this.goin57.Size = new System.Drawing.Size(75, 21);
            this.goin57.TabIndex = 19;
            // 
            // goin41
            // 
            this.goin41.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin41.FormattingEnabled = true;
            this.goin41.Location = new System.Drawing.Point(384, 235);
            this.goin41.Name = "goin41";
            this.goin41.Size = new System.Drawing.Size(75, 21);
            this.goin41.TabIndex = 19;
            // 
            // goin25
            // 
            this.goin25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin25.FormattingEnabled = true;
            this.goin25.Location = new System.Drawing.Point(217, 235);
            this.goin25.Name = "goin25";
            this.goin25.Size = new System.Drawing.Size(75, 21);
            this.goin25.TabIndex = 19;
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.Location = new System.Drawing.Point(505, 238);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(42, 13);
            this.label302.TabIndex = 2;
            this.label302.Text = "GoIn57";
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(336, 238);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(42, 13);
            this.label303.TabIndex = 2;
            this.label303.Text = "GoIn41";
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(169, 238);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(42, 13);
            this.label304.TabIndex = 2;
            this.label304.Text = "GoIn25";
            // 
            // goin56
            // 
            this.goin56.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin56.FormattingEnabled = true;
            this.goin56.Location = new System.Drawing.Point(553, 208);
            this.goin56.Name = "goin56";
            this.goin56.Size = new System.Drawing.Size(75, 21);
            this.goin56.TabIndex = 18;
            // 
            // goin40
            // 
            this.goin40.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin40.FormattingEnabled = true;
            this.goin40.Location = new System.Drawing.Point(384, 208);
            this.goin40.Name = "goin40";
            this.goin40.Size = new System.Drawing.Size(75, 21);
            this.goin40.TabIndex = 18;
            // 
            // goin24
            // 
            this.goin24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin24.FormattingEnabled = true;
            this.goin24.Location = new System.Drawing.Point(217, 208);
            this.goin24.Name = "goin24";
            this.goin24.Size = new System.Drawing.Size(75, 21);
            this.goin24.TabIndex = 18;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(505, 211);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(42, 13);
            this.label305.TabIndex = 9;
            this.label305.Text = "GoIn56";
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(336, 211);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(42, 13);
            this.label306.TabIndex = 9;
            this.label306.Text = "GoIn40";
            // 
            // label307
            // 
            this.label307.AutoSize = true;
            this.label307.Location = new System.Drawing.Point(169, 211);
            this.label307.Name = "label307";
            this.label307.Size = new System.Drawing.Size(42, 13);
            this.label307.TabIndex = 9;
            this.label307.Text = "GoIn24";
            // 
            // goin55
            // 
            this.goin55.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin55.FormattingEnabled = true;
            this.goin55.Location = new System.Drawing.Point(553, 181);
            this.goin55.Name = "goin55";
            this.goin55.Size = new System.Drawing.Size(75, 21);
            this.goin55.TabIndex = 20;
            // 
            // goin39
            // 
            this.goin39.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin39.FormattingEnabled = true;
            this.goin39.Location = new System.Drawing.Point(384, 181);
            this.goin39.Name = "goin39";
            this.goin39.Size = new System.Drawing.Size(75, 21);
            this.goin39.TabIndex = 20;
            // 
            // goin23
            // 
            this.goin23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin23.FormattingEnabled = true;
            this.goin23.Location = new System.Drawing.Point(217, 181);
            this.goin23.Name = "goin23";
            this.goin23.Size = new System.Drawing.Size(75, 21);
            this.goin23.TabIndex = 20;
            // 
            // label308
            // 
            this.label308.AutoSize = true;
            this.label308.Location = new System.Drawing.Point(505, 184);
            this.label308.Name = "label308";
            this.label308.Size = new System.Drawing.Size(42, 13);
            this.label308.TabIndex = 8;
            this.label308.Text = "GoIn55";
            // 
            // label309
            // 
            this.label309.AutoSize = true;
            this.label309.Location = new System.Drawing.Point(336, 184);
            this.label309.Name = "label309";
            this.label309.Size = new System.Drawing.Size(42, 13);
            this.label309.TabIndex = 8;
            this.label309.Text = "GoIn39";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Location = new System.Drawing.Point(169, 184);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(42, 13);
            this.label310.TabIndex = 8;
            this.label310.Text = "GoIn23";
            // 
            // goin54
            // 
            this.goin54.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin54.FormattingEnabled = true;
            this.goin54.Location = new System.Drawing.Point(553, 154);
            this.goin54.Name = "goin54";
            this.goin54.Size = new System.Drawing.Size(75, 21);
            this.goin54.TabIndex = 22;
            // 
            // goin38
            // 
            this.goin38.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin38.FormattingEnabled = true;
            this.goin38.Location = new System.Drawing.Point(384, 154);
            this.goin38.Name = "goin38";
            this.goin38.Size = new System.Drawing.Size(75, 21);
            this.goin38.TabIndex = 22;
            // 
            // goin22
            // 
            this.goin22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin22.FormattingEnabled = true;
            this.goin22.Location = new System.Drawing.Point(217, 154);
            this.goin22.Name = "goin22";
            this.goin22.Size = new System.Drawing.Size(75, 21);
            this.goin22.TabIndex = 22;
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(505, 157);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(42, 13);
            this.label311.TabIndex = 7;
            this.label311.Text = "GoIn54";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(336, 157);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(42, 13);
            this.label312.TabIndex = 7;
            this.label312.Text = "GoIn38";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(169, 157);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(42, 13);
            this.label313.TabIndex = 7;
            this.label313.Text = "GoIn22";
            // 
            // goin53
            // 
            this.goin53.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin53.FormattingEnabled = true;
            this.goin53.Location = new System.Drawing.Point(553, 127);
            this.goin53.Name = "goin53";
            this.goin53.Size = new System.Drawing.Size(75, 21);
            this.goin53.TabIndex = 24;
            // 
            // goin37
            // 
            this.goin37.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin37.FormattingEnabled = true;
            this.goin37.Location = new System.Drawing.Point(384, 127);
            this.goin37.Name = "goin37";
            this.goin37.Size = new System.Drawing.Size(75, 21);
            this.goin37.TabIndex = 24;
            // 
            // goin21
            // 
            this.goin21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin21.FormattingEnabled = true;
            this.goin21.Location = new System.Drawing.Point(217, 127);
            this.goin21.Name = "goin21";
            this.goin21.Size = new System.Drawing.Size(75, 21);
            this.goin21.TabIndex = 24;
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Location = new System.Drawing.Point(505, 130);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(42, 13);
            this.label314.TabIndex = 6;
            this.label314.Text = "GoIn53";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(336, 130);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(42, 13);
            this.label315.TabIndex = 6;
            this.label315.Text = "GoIn37";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(169, 130);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(42, 13);
            this.label316.TabIndex = 6;
            this.label316.Text = "GoIn21";
            // 
            // goin52
            // 
            this.goin52.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin52.FormattingEnabled = true;
            this.goin52.Location = new System.Drawing.Point(553, 100);
            this.goin52.Name = "goin52";
            this.goin52.Size = new System.Drawing.Size(75, 21);
            this.goin52.TabIndex = 26;
            // 
            // goin36
            // 
            this.goin36.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin36.FormattingEnabled = true;
            this.goin36.Location = new System.Drawing.Point(384, 100);
            this.goin36.Name = "goin36";
            this.goin36.Size = new System.Drawing.Size(75, 21);
            this.goin36.TabIndex = 26;
            // 
            // goin20
            // 
            this.goin20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin20.FormattingEnabled = true;
            this.goin20.Location = new System.Drawing.Point(217, 100);
            this.goin20.Name = "goin20";
            this.goin20.Size = new System.Drawing.Size(75, 21);
            this.goin20.TabIndex = 26;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(505, 103);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(42, 13);
            this.label317.TabIndex = 5;
            this.label317.Text = "GoIn52";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Location = new System.Drawing.Point(336, 103);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(42, 13);
            this.label318.TabIndex = 5;
            this.label318.Text = "GoIn36";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(169, 103);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(42, 13);
            this.label319.TabIndex = 5;
            this.label319.Text = "GoIn20";
            // 
            // goin51
            // 
            this.goin51.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin51.FormattingEnabled = true;
            this.goin51.Location = new System.Drawing.Point(553, 73);
            this.goin51.Name = "goin51";
            this.goin51.Size = new System.Drawing.Size(75, 21);
            this.goin51.TabIndex = 28;
            // 
            // goin35
            // 
            this.goin35.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin35.FormattingEnabled = true;
            this.goin35.Location = new System.Drawing.Point(384, 73);
            this.goin35.Name = "goin35";
            this.goin35.Size = new System.Drawing.Size(75, 21);
            this.goin35.TabIndex = 28;
            // 
            // goin19
            // 
            this.goin19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin19.FormattingEnabled = true;
            this.goin19.Location = new System.Drawing.Point(217, 73);
            this.goin19.Name = "goin19";
            this.goin19.Size = new System.Drawing.Size(75, 21);
            this.goin19.TabIndex = 28;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(505, 76);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(42, 13);
            this.label320.TabIndex = 4;
            this.label320.Text = "GoIn51";
            // 
            // label321
            // 
            this.label321.AutoSize = true;
            this.label321.Location = new System.Drawing.Point(336, 76);
            this.label321.Name = "label321";
            this.label321.Size = new System.Drawing.Size(42, 13);
            this.label321.TabIndex = 4;
            this.label321.Text = "GoIn35";
            // 
            // label322
            // 
            this.label322.AutoSize = true;
            this.label322.Location = new System.Drawing.Point(169, 76);
            this.label322.Name = "label322";
            this.label322.Size = new System.Drawing.Size(42, 13);
            this.label322.TabIndex = 4;
            this.label322.Text = "GoIn19";
            // 
            // goin50
            // 
            this.goin50.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin50.FormattingEnabled = true;
            this.goin50.Location = new System.Drawing.Point(553, 46);
            this.goin50.Name = "goin50";
            this.goin50.Size = new System.Drawing.Size(75, 21);
            this.goin50.TabIndex = 30;
            // 
            // goin34
            // 
            this.goin34.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin34.FormattingEnabled = true;
            this.goin34.Location = new System.Drawing.Point(384, 46);
            this.goin34.Name = "goin34";
            this.goin34.Size = new System.Drawing.Size(75, 21);
            this.goin34.TabIndex = 30;
            // 
            // goin18
            // 
            this.goin18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin18.FormattingEnabled = true;
            this.goin18.Location = new System.Drawing.Point(217, 46);
            this.goin18.Name = "goin18";
            this.goin18.Size = new System.Drawing.Size(75, 21);
            this.goin18.TabIndex = 30;
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(505, 49);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(42, 13);
            this.label323.TabIndex = 3;
            this.label323.Text = "GoIn50";
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(336, 49);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(42, 13);
            this.label324.TabIndex = 3;
            this.label324.Text = "GoIn34";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(169, 49);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(42, 13);
            this.label325.TabIndex = 3;
            this.label325.Text = "GoIn18";
            // 
            // goin49
            // 
            this.goin49.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin49.FormattingEnabled = true;
            this.goin49.Location = new System.Drawing.Point(553, 19);
            this.goin49.Name = "goin49";
            this.goin49.Size = new System.Drawing.Size(75, 21);
            this.goin49.TabIndex = 32;
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(505, 22);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(42, 13);
            this.label326.TabIndex = 12;
            this.label326.Text = "GoIn49";
            // 
            // goin33
            // 
            this.goin33.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin33.FormattingEnabled = true;
            this.goin33.Location = new System.Drawing.Point(384, 19);
            this.goin33.Name = "goin33";
            this.goin33.Size = new System.Drawing.Size(75, 21);
            this.goin33.TabIndex = 32;
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(336, 22);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(42, 13);
            this.label327.TabIndex = 12;
            this.label327.Text = "GoIn33";
            // 
            // goin17
            // 
            this.goin17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin17.FormattingEnabled = true;
            this.goin17.Location = new System.Drawing.Point(217, 19);
            this.goin17.Name = "goin17";
            this.goin17.Size = new System.Drawing.Size(75, 21);
            this.goin17.TabIndex = 32;
            // 
            // label328
            // 
            this.label328.AutoSize = true;
            this.label328.Location = new System.Drawing.Point(169, 22);
            this.label328.Name = "label328";
            this.label328.Size = new System.Drawing.Size(42, 13);
            this.label328.TabIndex = 12;
            this.label328.Text = "GoIn17";
            // 
            // goin16
            // 
            this.goin16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin16.FormattingEnabled = true;
            this.goin16.Location = new System.Drawing.Point(54, 424);
            this.goin16.Name = "goin16";
            this.goin16.Size = new System.Drawing.Size(75, 21);
            this.goin16.TabIndex = 1;
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 427);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(42, 13);
            this.label329.TabIndex = 0;
            this.label329.Text = "GoIn16";
            // 
            // goin15
            // 
            this.goin15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin15.FormattingEnabled = true;
            this.goin15.Location = new System.Drawing.Point(54, 397);
            this.goin15.Name = "goin15";
            this.goin15.Size = new System.Drawing.Size(75, 21);
            this.goin15.TabIndex = 1;
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(6, 400);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(42, 13);
            this.label330.TabIndex = 0;
            this.label330.Text = "GoIn15";
            // 
            // goin14
            // 
            this.goin14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin14.FormattingEnabled = true;
            this.goin14.Location = new System.Drawing.Point(54, 370);
            this.goin14.Name = "goin14";
            this.goin14.Size = new System.Drawing.Size(75, 21);
            this.goin14.TabIndex = 1;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(6, 373);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(42, 13);
            this.label331.TabIndex = 0;
            this.label331.Text = "GoIn14";
            // 
            // goin13
            // 
            this.goin13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin13.FormattingEnabled = true;
            this.goin13.Location = new System.Drawing.Point(54, 343);
            this.goin13.Name = "goin13";
            this.goin13.Size = new System.Drawing.Size(75, 21);
            this.goin13.TabIndex = 1;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(6, 346);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(42, 13);
            this.label332.TabIndex = 0;
            this.label332.Text = "GoIn13";
            // 
            // goin12
            // 
            this.goin12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin12.FormattingEnabled = true;
            this.goin12.Location = new System.Drawing.Point(54, 316);
            this.goin12.Name = "goin12";
            this.goin12.Size = new System.Drawing.Size(75, 21);
            this.goin12.TabIndex = 1;
            // 
            // label333
            // 
            this.label333.AutoSize = true;
            this.label333.Location = new System.Drawing.Point(6, 319);
            this.label333.Name = "label333";
            this.label333.Size = new System.Drawing.Size(42, 13);
            this.label333.TabIndex = 0;
            this.label333.Text = "GoIn12";
            // 
            // goin11
            // 
            this.goin11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin11.FormattingEnabled = true;
            this.goin11.Location = new System.Drawing.Point(54, 289);
            this.goin11.Name = "goin11";
            this.goin11.Size = new System.Drawing.Size(75, 21);
            this.goin11.TabIndex = 1;
            // 
            // label334
            // 
            this.label334.AutoSize = true;
            this.label334.Location = new System.Drawing.Point(6, 292);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(42, 13);
            this.label334.TabIndex = 0;
            this.label334.Text = "GoIn11";
            // 
            // goin10
            // 
            this.goin10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin10.FormattingEnabled = true;
            this.goin10.Location = new System.Drawing.Point(54, 262);
            this.goin10.Name = "goin10";
            this.goin10.Size = new System.Drawing.Size(75, 21);
            this.goin10.TabIndex = 1;
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(6, 265);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(42, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "GoIn10";
            // 
            // goin9
            // 
            this.goin9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin9.FormattingEnabled = true;
            this.goin9.Location = new System.Drawing.Point(54, 235);
            this.goin9.Name = "goin9";
            this.goin9.Size = new System.Drawing.Size(75, 21);
            this.goin9.TabIndex = 1;
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(6, 238);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(36, 13);
            this.label336.TabIndex = 0;
            this.label336.Text = "GoIn9";
            // 
            // goin8
            // 
            this.goin8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin8.FormattingEnabled = true;
            this.goin8.Location = new System.Drawing.Point(54, 208);
            this.goin8.Name = "goin8";
            this.goin8.Size = new System.Drawing.Size(75, 21);
            this.goin8.TabIndex = 1;
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(6, 211);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(36, 13);
            this.label337.TabIndex = 0;
            this.label337.Text = "GoIn8";
            // 
            // goin7
            // 
            this.goin7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin7.FormattingEnabled = true;
            this.goin7.Location = new System.Drawing.Point(54, 181);
            this.goin7.Name = "goin7";
            this.goin7.Size = new System.Drawing.Size(75, 21);
            this.goin7.TabIndex = 1;
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(6, 184);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(36, 13);
            this.label338.TabIndex = 0;
            this.label338.Text = "GoIn7";
            // 
            // goin6
            // 
            this.goin6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin6.FormattingEnabled = true;
            this.goin6.Location = new System.Drawing.Point(54, 154);
            this.goin6.Name = "goin6";
            this.goin6.Size = new System.Drawing.Size(75, 21);
            this.goin6.TabIndex = 1;
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(6, 157);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(36, 13);
            this.label339.TabIndex = 0;
            this.label339.Text = "GoIn6";
            // 
            // goin5
            // 
            this.goin5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin5.FormattingEnabled = true;
            this.goin5.Location = new System.Drawing.Point(54, 127);
            this.goin5.Name = "goin5";
            this.goin5.Size = new System.Drawing.Size(75, 21);
            this.goin5.TabIndex = 1;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(6, 130);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(36, 13);
            this.label340.TabIndex = 0;
            this.label340.Text = "GoIn5";
            // 
            // goin4
            // 
            this.goin4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin4.FormattingEnabled = true;
            this.goin4.Location = new System.Drawing.Point(54, 100);
            this.goin4.Name = "goin4";
            this.goin4.Size = new System.Drawing.Size(75, 21);
            this.goin4.TabIndex = 1;
            // 
            // label341
            // 
            this.label341.AutoSize = true;
            this.label341.Location = new System.Drawing.Point(6, 103);
            this.label341.Name = "label341";
            this.label341.Size = new System.Drawing.Size(36, 13);
            this.label341.TabIndex = 0;
            this.label341.Text = "GoIn4";
            // 
            // goin3
            // 
            this.goin3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin3.FormattingEnabled = true;
            this.goin3.Location = new System.Drawing.Point(54, 73);
            this.goin3.Name = "goin3";
            this.goin3.Size = new System.Drawing.Size(75, 21);
            this.goin3.TabIndex = 1;
            // 
            // label342
            // 
            this.label342.AutoSize = true;
            this.label342.Location = new System.Drawing.Point(6, 76);
            this.label342.Name = "label342";
            this.label342.Size = new System.Drawing.Size(36, 13);
            this.label342.TabIndex = 0;
            this.label342.Text = "GoIn3";
            // 
            // goin2
            // 
            this.goin2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin2.FormattingEnabled = true;
            this.goin2.Location = new System.Drawing.Point(54, 46);
            this.goin2.Name = "goin2";
            this.goin2.Size = new System.Drawing.Size(75, 21);
            this.goin2.TabIndex = 1;
            // 
            // label343
            // 
            this.label343.AutoSize = true;
            this.label343.Location = new System.Drawing.Point(6, 49);
            this.label343.Name = "label343";
            this.label343.Size = new System.Drawing.Size(36, 13);
            this.label343.TabIndex = 0;
            this.label343.Text = "GoIn2";
            // 
            // goin1
            // 
            this.goin1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.goin1.FormattingEnabled = true;
            this.goin1.Location = new System.Drawing.Point(54, 19);
            this.goin1.Name = "goin1";
            this.goin1.Size = new System.Drawing.Size(75, 21);
            this.goin1.TabIndex = 1;
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(6, 22);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(36, 13);
            this.label344.TabIndex = 0;
            this.label344.Text = "GoIn1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(6, 22);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(57, 13);
            this.label345.TabIndex = 6;
            this.label345.Text = "Операция";
            // 
            // operationBGS
            // 
            this.operationBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.operationBGS.FormattingEnabled = true;
            this.operationBGS.Location = new System.Drawing.Point(69, 19);
            this.operationBGS.Name = "operationBGS";
            this.operationBGS.Size = new System.Drawing.Size(112, 21);
            this.operationBGS.TabIndex = 7;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 12);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(89, 13);
            this.label36.TabIndex = 6;
            this.label36.Text = "Выбранный БГС";
            // 
            // currentBGS
            // 
            this.currentBGS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentBGS.FormattingEnabled = true;
            this.currentBGS.Location = new System.Drawing.Point(101, 9);
            this.currentBGS.Name = "currentBGS";
            this.currentBGS.Size = new System.Drawing.Size(102, 21);
            this.currentBGS.TabIndex = 7;
            // 
            // _automatPage
            // 
            this._automatPage.Controls.Add(this._switchYesNo);
            this._automatPage.Controls.Add(this.UROVgroupBox);
            this._automatPage.Controls.Add(this._switchConfigGroupBox);
            this._automatPage.Controls.Add(this.groupBox33);
            this._automatPage.Location = new System.Drawing.Point(4, 22);
            this._automatPage.Name = "_automatPage";
            this._automatPage.Size = new System.Drawing.Size(976, 553);
            this._automatPage.TabIndex = 9;
            this._automatPage.Text = "Выключатель";
            this._automatPage.UseVisualStyleBackColor = true;
            // 
            // _switchYesNo
            // 
            this._switchYesNo.AutoSize = true;
            this._switchYesNo.Location = new System.Drawing.Point(94, 3);
            this._switchYesNo.Name = "_switchYesNo";
            this._switchYesNo.Size = new System.Drawing.Size(15, 14);
            this._switchYesNo.TabIndex = 28;
            this._switchYesNo.UseVisualStyleBackColor = true;
            this._switchYesNo.CheckedChanged += new System.EventHandler(this._switchYesNo_CheckedChanged);
            // 
            // UROVgroupBox
            // 
            this.UROVgroupBox.Controls.Add(this._urovSelf);
            this.UROVgroupBox.Controls.Add(this._urovBkCheck);
            this.UROVgroupBox.Controls.Add(this._urovIcheck);
            this.UROVgroupBox.Controls.Add(this._tUrov1);
            this.UROVgroupBox.Controls.Add(this._urovBlock);
            this.UROVgroupBox.Controls.Add(this.label39);
            this.UROVgroupBox.Controls.Add(this._tUrov2);
            this.UROVgroupBox.Controls.Add(this._Iurov);
            this.UROVgroupBox.Controls.Add(this.label38);
            this.UROVgroupBox.Controls.Add(this._urovPusk);
            this.UROVgroupBox.Controls.Add(this.label37);
            this.UROVgroupBox.Controls.Add(this.label126);
            this.UROVgroupBox.Controls.Add(this.label35);
            this.UROVgroupBox.Controls.Add(this.label34);
            this.UROVgroupBox.Controls.Add(this.label33);
            this.UROVgroupBox.Controls.Add(this.label127);
            this.UROVgroupBox.Location = new System.Drawing.Point(267, 3);
            this.UROVgroupBox.Name = "UROVgroupBox";
            this.UROVgroupBox.Size = new System.Drawing.Size(218, 188);
            this.UROVgroupBox.TabIndex = 14;
            this.UROVgroupBox.TabStop = false;
            this.UROVgroupBox.Text = "УРОВ";
            // 
            // _urovSelf
            // 
            this._urovSelf.AutoSize = true;
            this._urovSelf.Location = new System.Drawing.Point(106, 57);
            this._urovSelf.Name = "_urovSelf";
            this._urovSelf.Size = new System.Drawing.Size(15, 14);
            this._urovSelf.TabIndex = 27;
            this._urovSelf.UseVisualStyleBackColor = true;
            // 
            // _urovBkCheck
            // 
            this._urovBkCheck.AutoSize = true;
            this._urovBkCheck.Location = new System.Drawing.Point(106, 37);
            this._urovBkCheck.Name = "_urovBkCheck";
            this._urovBkCheck.Size = new System.Drawing.Size(15, 14);
            this._urovBkCheck.TabIndex = 27;
            this._urovBkCheck.UseVisualStyleBackColor = true;
            // 
            // _urovIcheck
            // 
            this._urovIcheck.AutoSize = true;
            this._urovIcheck.Location = new System.Drawing.Point(106, 19);
            this._urovIcheck.Name = "_urovIcheck";
            this._urovIcheck.Size = new System.Drawing.Size(15, 14);
            this._urovIcheck.TabIndex = 27;
            this._urovIcheck.UseVisualStyleBackColor = true;
            // 
            // _tUrov1
            // 
            this._tUrov1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tUrov1.Location = new System.Drawing.Point(106, 138);
            this._tUrov1.Name = "_tUrov1";
            this._tUrov1.Size = new System.Drawing.Size(105, 20);
            this._tUrov1.TabIndex = 22;
            this._tUrov1.Tag = "3276700";
            this._tUrov1.Text = "0";
            this._tUrov1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _urovBlock
            // 
            this._urovBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._urovBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._urovBlock.FormattingEnabled = true;
            this._urovBlock.Location = new System.Drawing.Point(106, 98);
            this._urovBlock.Name = "_urovBlock";
            this._urovBlock.Size = new System.Drawing.Size(105, 21);
            this._urovBlock.TabIndex = 26;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 140);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 13);
            this.label39.TabIndex = 13;
            this.label39.Text = "tуров1, мс";
            // 
            // _tUrov2
            // 
            this._tUrov2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tUrov2.Location = new System.Drawing.Point(106, 157);
            this._tUrov2.Name = "_tUrov2";
            this._tUrov2.Size = new System.Drawing.Size(105, 20);
            this._tUrov2.TabIndex = 23;
            this._tUrov2.Tag = "40";
            this._tUrov2.Text = "0";
            this._tUrov2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Iurov
            // 
            this._Iurov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Iurov.Location = new System.Drawing.Point(106, 119);
            this._Iurov.Name = "_Iurov";
            this._Iurov.Size = new System.Drawing.Size(105, 20);
            this._Iurov.TabIndex = 24;
            this._Iurov.Tag = "3276700";
            this._Iurov.Text = "0";
            this._Iurov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 159);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 13);
            this.label38.TabIndex = 14;
            this.label38.Text = "tуров2, мс";
            // 
            // _urovPusk
            // 
            this._urovPusk.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._urovPusk.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._urovPusk.BackColor = System.Drawing.SystemColors.Window;
            this._urovPusk.FormattingEnabled = true;
            this._urovPusk.Location = new System.Drawing.Point(106, 77);
            this._urovPusk.Name = "_urovPusk";
            this._urovPusk.Size = new System.Drawing.Size(105, 21);
            this._urovPusk.TabIndex = 21;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 121);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(48, 13);
            this.label37.TabIndex = 15;
            this.label37.Text = "Iуров, Iн";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 101);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(94, 13);
            this.label126.TabIndex = 12;
            this.label126.Text = "Вход блокировки";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 57);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(48, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "На себя";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 37);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(38, 13);
            this.label34.TabIndex = 11;
            this.label34.Text = "По БК";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(46, 13);
            this.label33.TabIndex = 11;
            this.label33.Text = "По току";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 80);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(63, 13);
            this.label127.TabIndex = 11;
            this.label127.Text = "Вход пуска";
            // 
            // _switchConfigGroupBox
            // 
            this._switchConfigGroupBox.Controls.Add(this._switchSideCB);
            this._switchConfigGroupBox.Controls.Add(this.label6);
            this._switchConfigGroupBox.Controls.Add(this.label96);
            this._switchConfigGroupBox.Controls.Add(this._comandOtkl);
            this._switchConfigGroupBox.Controls.Add(this._controlSolenoidCombo);
            this._switchConfigGroupBox.Controls.Add(this._switchKontCep);
            this._switchConfigGroupBox.Controls.Add(this._switchTUskor);
            this._switchConfigGroupBox.Controls.Add(this._switchImp);
            this._switchConfigGroupBox.Controls.Add(this._switchBlock);
            this._switchConfigGroupBox.Controls.Add(this._switchError);
            this._switchConfigGroupBox.Controls.Add(this._switchOn);
            this._switchConfigGroupBox.Controls.Add(this.label51);
            this._switchConfigGroupBox.Controls.Add(this._switchOff);
            this._switchConfigGroupBox.Controls.Add(this.label97);
            this._switchConfigGroupBox.Controls.Add(this.label98);
            this._switchConfigGroupBox.Controls.Add(this.label99);
            this._switchConfigGroupBox.Controls.Add(this.label93);
            this._switchConfigGroupBox.Controls.Add(this.label90);
            this._switchConfigGroupBox.Controls.Add(this.label89);
            this._switchConfigGroupBox.Controls.Add(this.label88);
            this._switchConfigGroupBox.Location = new System.Drawing.Point(8, 3);
            this._switchConfigGroupBox.Name = "_switchConfigGroupBox";
            this._switchConfigGroupBox.Size = new System.Drawing.Size(253, 249);
            this._switchConfigGroupBox.TabIndex = 14;
            this._switchConfigGroupBox.TabStop = false;
            this._switchConfigGroupBox.Text = "Выключатель";
            // 
            // _switchSideCB
            // 
            this._switchSideCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSideCB.FormattingEnabled = true;
            this._switchSideCB.Location = new System.Drawing.Point(142, 218);
            this._switchSideCB.Name = "_switchSideCB";
            this._switchSideCB.Size = new System.Drawing.Size(48, 21);
            this._switchSideCB.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Сторона";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 118);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(115, 13);
            this.label96.TabIndex = 16;
            this.label96.Text = "Команда отключения";
            // 
            // _comandOtkl
            // 
            this._comandOtkl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comandOtkl.FormattingEnabled = true;
            this._comandOtkl.Location = new System.Drawing.Point(142, 116);
            this._comandOtkl.Name = "_comandOtkl";
            this._comandOtkl.Size = new System.Drawing.Size(105, 21);
            this._comandOtkl.TabIndex = 27;
            // 
            // _controlSolenoidCombo
            // 
            this._controlSolenoidCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._controlSolenoidCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._controlSolenoidCombo.FormattingEnabled = true;
            this._controlSolenoidCombo.Location = new System.Drawing.Point(142, 191);
            this._controlSolenoidCombo.Name = "_controlSolenoidCombo";
            this._controlSolenoidCombo.Size = new System.Drawing.Size(105, 21);
            this._controlSolenoidCombo.TabIndex = 26;
            // 
            // _switchKontCep
            // 
            this._switchKontCep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(142, 157);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(142, 137);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "20";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(142, 96);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchBlock
            // 
            this._switchBlock.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchBlock.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchBlock.BackColor = System.Drawing.SystemColors.Window;
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(142, 75);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // _switchError
            // 
            this._switchError.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchError.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(142, 55);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _switchOn
            // 
            this._switchOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(142, 35);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(6, 189);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(124, 26);
            this.label51.TabIndex = 17;
            this.label51.Text = "Контроль второго\r\nсоленоида отключения";
            // 
            // _switchOff
            // 
            this._switchOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(142, 15);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(7, 156);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(88, 26);
            this.label97.TabIndex = 17;
            this.label97.Text = "Контроль цепей\r\nуправления";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(8, 139);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(59, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "tускор, мс";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 98);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(139, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс сигнала упр., мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 78);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(94, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Вход блокировки";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(111, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Вход неисправности";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(6, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(124, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Состояние \"Включено\"";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(6, 18);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(130, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Состояние \"Отключено\"";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._blockSDTU);
            this.groupBox33.Controls.Add(this._switchSDTU);
            this.groupBox33.Controls.Add(this._switchVnesh);
            this.groupBox33.Controls.Add(this._switchKey);
            this.groupBox33.Controls.Add(this._switchButtons);
            this.groupBox33.Controls.Add(this._switchVneshOff);
            this.groupBox33.Controls.Add(this._switchVneshOn);
            this.groupBox33.Controls.Add(this._switchKeyOff);
            this.groupBox33.Controls.Add(this._blockSDTUlabel);
            this.groupBox33.Controls.Add(this._switchKeyOn);
            this.groupBox33.Controls.Add(this.label101);
            this.groupBox33.Controls.Add(this.label102);
            this.groupBox33.Controls.Add(this.label103);
            this.groupBox33.Controls.Add(this.label104);
            this.groupBox33.Controls.Add(this.label105);
            this.groupBox33.Controls.Add(this.label106);
            this.groupBox33.Controls.Add(this.label107);
            this.groupBox33.Controls.Add(this.label108);
            this.groupBox33.Location = new System.Drawing.Point(8, 258);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(253, 200);
            this.groupBox33.TabIndex = 15;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Управление";
            // 
            // _blockSDTU
            // 
            this._blockSDTU.FormattingEnabled = true;
            this._blockSDTU.Location = new System.Drawing.Point(142, 174);
            this._blockSDTU.Name = "_blockSDTU";
            this._blockSDTU.Size = new System.Drawing.Size(105, 21);
            this._blockSDTU.TabIndex = 34;
            // 
            // _switchSDTU
            // 
            this._switchSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(142, 154);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(142, 134);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(142, 115);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(142, 95);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(142, 75);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchVneshOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(142, 55);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(142, 35);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _blockSDTUlabel
            // 
            this._blockSDTUlabel.AutoSize = true;
            this._blockSDTUlabel.Location = new System.Drawing.Point(6, 177);
            this._blockSDTUlabel.Name = "_blockSDTUlabel";
            this._blockSDTUlabel.Size = new System.Drawing.Size(102, 13);
            this._blockSDTUlabel.TabIndex = 25;
            this._blockSDTUlabel.Text = "Блокировка СДТУ";
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._switchKeyOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(142, 15);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 157);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(54, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "От СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 137);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 118);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(54, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "От ключа";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 98);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(96, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "От кнопок пульта";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(108, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внешнее отключить";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 58);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(103, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внешнее включить";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 38);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(89, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ отключить";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 18);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(84, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ включить";
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox11);
            this._systemPage.Controls.Add(this.groupBox3);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(976, 553);
            this._systemPage.TabIndex = 8;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._oscChannelsGrid);
            this.groupBox11.Location = new System.Drawing.Point(291, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(347, 547);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannelsGrid
            // 
            this._oscChannelsGrid.AllowUserToAddRows = false;
            this._oscChannelsGrid.AllowUserToDeleteRows = false;
            this._oscChannelsGrid.AllowUserToResizeColumns = false;
            this._oscChannelsGrid.AllowUserToResizeRows = false;
            this._oscChannelsGrid.BackgroundColor = System.Drawing.Color.White;
            this._oscChannelsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannelsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this._baseCol,
            this._oscSygnal});
            this._oscChannelsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._oscChannelsGrid.Location = new System.Drawing.Point(3, 16);
            this._oscChannelsGrid.Name = "_oscChannelsGrid";
            this._oscChannelsGrid.RowHeadersVisible = false;
            this._oscChannelsGrid.RowHeadersWidth = 51;
            this._oscChannelsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannelsGrid.RowTemplate.Height = 24;
            this._oscChannelsGrid.ShowCellErrors = false;
            this._oscChannelsGrid.ShowRowErrors = false;
            this._oscChannelsGrid.Size = new System.Drawing.Size(341, 528);
            this._oscChannelsGrid.TabIndex = 27;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn17.Frozen = true;
            this.dataGridViewTextBoxColumn17.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn17.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 70;
            // 
            // _baseCol
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._baseCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._baseCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._baseCol.HeaderText = "База";
            this._baseCol.MinimumWidth = 6;
            this._baseCol.Name = "_baseCol";
            this._baseCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._baseCol.Width = 50;
            // 
            // _oscSygnal
            // 
            this._oscSygnal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._oscSygnal.DefaultCellStyle = dataGridViewCellStyle3;
            this._oscSygnal.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._oscSygnal.HeaderText = "Сигнал";
            this._oscSygnal.MinimumWidth = 6;
            this._oscSygnal.Name = "_oscSygnal";
            this._oscSygnal.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.oscStartCb);
            this.groupBox3.Controls.Add(this.label65);
            this.groupBox3.Controls.Add(this._oscSizeTextBox);
            this.groupBox3.Controls.Add(this._oscWriteLength);
            this.groupBox3.Controls.Add(this._oscFix);
            this.groupBox3.Controls.Add(this._oscLength);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 124);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Осциллограф";
            // 
            // oscStartCb
            // 
            this.oscStartCb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.oscStartCb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.oscStartCb.FormattingEnabled = true;
            this.oscStartCb.Location = new System.Drawing.Point(134, 91);
            this.oscStartCb.Name = "oscStartCb";
            this.oscStartCb.Size = new System.Drawing.Size(121, 21);
            this.oscStartCb.TabIndex = 30;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(18, 94);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(87, 13);
            this.label65.TabIndex = 29;
            this.label65.Text = "Вход пуска осц.";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(184, 23);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 28;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(134, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(134, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(134, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(18, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Фиксац. по";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(18, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Длит. предзаписи, %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(18, 26);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Размер, мс";
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox54);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox31);
            this._outputSignalsPage.Controls.Add(this.groupBox175);
            this._outputSignalsPage.Controls.Add(this.groupBox176);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Padding = new System.Windows.Forms.Padding(3);
            this._outputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._outputSignalsPage.TabIndex = 10;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox54
            // 
            this.groupBox54.Controls.Add(this.label193);
            this.groupBox54.Controls.Add(this._fixErrorFCheckBox);
            this.groupBox54.Location = new System.Drawing.Point(413, 412);
            this.groupBox54.Name = "groupBox54";
            this.groupBox54.Size = new System.Drawing.Size(215, 50);
            this.groupBox54.TabIndex = 22;
            this.groupBox54.TabStop = false;
            this.groupBox54.Text = "Работа частоты";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(6, 23);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(132, 13);
            this.label193.TabIndex = 22;
            this.label193.Text = "Фикс. ошибка расчета F";
            // 
            // _fixErrorFCheckBox
            // 
            this._fixErrorFCheckBox.AutoSize = true;
            this._fixErrorFCheckBox.Location = new System.Drawing.Point(185, 23);
            this._fixErrorFCheckBox.Name = "_fixErrorFCheckBox";
            this._fixErrorFCheckBox.Size = new System.Drawing.Size(15, 14);
            this._fixErrorFCheckBox.TabIndex = 23;
            this._fixErrorFCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this._resetAlarmCheckBox);
            this.groupBox13.Controls.Add(this._resetSystemCheckBox);
            this.groupBox13.Controls.Add(this.label110);
            this.groupBox13.Controls.Add(this.label52);
            this.groupBox13.Location = new System.Drawing.Point(413, 342);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(215, 71);
            this.groupBox13.TabIndex = 22;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сброс индикаторов";
            // 
            // _resetAlarmCheckBox
            // 
            this._resetAlarmCheckBox.AutoSize = true;
            this._resetAlarmCheckBox.Location = new System.Drawing.Point(185, 46);
            this._resetAlarmCheckBox.Name = "_resetAlarmCheckBox";
            this._resetAlarmCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetAlarmCheckBox.TabIndex = 21;
            this._resetAlarmCheckBox.UseVisualStyleBackColor = true;
            // 
            // _resetSystemCheckBox
            // 
            this._resetSystemCheckBox.AutoSize = true;
            this._resetSystemCheckBox.Location = new System.Drawing.Point(185, 21);
            this._resetSystemCheckBox.Name = "_resetSystemCheckBox";
            this._resetSystemCheckBox.Size = new System.Drawing.Size(15, 14);
            this._resetSystemCheckBox.TabIndex = 19;
            this._resetSystemCheckBox.UseVisualStyleBackColor = true;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 45);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(152, 13);
            this.label110.TabIndex = 20;
            this.label110.Text = "2. По входу в журнал аварий";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 22);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(161, 13);
            this.label52.TabIndex = 19;
            this.label52.Text = "1. По входу в журнал системы";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this._fault6CheckBox);
            this.groupBox31.Controls.Add(this.label121);
            this.groupBox31.Controls.Add(this._fault5CheckBox);
            this.groupBox31.Controls.Add(this._fault4CheckBox);
            this.groupBox31.Controls.Add(this._fault3CheckBox);
            this.groupBox31.Controls.Add(this._fault2CheckBox);
            this.groupBox31.Controls.Add(this.label111);
            this.groupBox31.Controls.Add(this._fault1CheckBox);
            this.groupBox31.Controls.Add(this.label18);
            this.groupBox31.Controls.Add(this.label23);
            this.groupBox31.Controls.Add(this.label44);
            this.groupBox31.Controls.Add(this.label45);
            this.groupBox31.Controls.Add(this._impTB);
            this.groupBox31.Controls.Add(this.label46);
            this.groupBox31.Location = new System.Drawing.Point(634, 342);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(317, 199);
            this.groupBox31.TabIndex = 21;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Реле неисправность";
            // 
            // _fault6CheckBox
            // 
            this._fault6CheckBox.AutoSize = true;
            this._fault6CheckBox.Location = new System.Drawing.Point(180, 140);
            this._fault6CheckBox.Name = "_fault6CheckBox";
            this._fault6CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault6CheckBox.TabIndex = 20;
            this._fault6CheckBox.UseVisualStyleBackColor = true;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(6, 141);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(136, 13);
            this.label121.TabIndex = 19;
            this.label121.Text = "6. Неисправность логики";
            // 
            // _fault5CheckBox
            // 
            this._fault5CheckBox.AutoSize = true;
            this._fault5CheckBox.Location = new System.Drawing.Point(181, 117);
            this._fault5CheckBox.Name = "_fault5CheckBox";
            this._fault5CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault5CheckBox.TabIndex = 18;
            this._fault5CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(181, 93);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 18;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(181, 69);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 17;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(181, 45);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 16;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(7, 118);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(169, 13);
            this.label111.TabIndex = 14;
            this.label111.Text = "5. Неисправность выключателя";
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(181, 21);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 15;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(166, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "4. Неисправность измерений F";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 70);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(168, 13);
            this.label23.TabIndex = 13;
            this.label23.Text = "3. Неисправность измерений U";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 46);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(170, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "2. Программная неисправность";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(159, 13);
            this.label45.TabIndex = 11;
            this.label45.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(92, 161);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(104, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 163);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(67, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Tвозвр., мс";
            // 
            // groupBox175
            // 
            this.groupBox175.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox175.Location = new System.Drawing.Point(407, 6);
            this.groupBox175.Name = "groupBox175";
            this.groupBox175.Size = new System.Drawing.Size(544, 330);
            this.groupBox175.TabIndex = 20;
            this.groupBox175.TabStop = false;
            this.groupBox175.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._out1IndSignalCol,
            this._outIndSignal2Col});
            this._outputIndicatorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(3, 16);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidth = 51;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(538, 311);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.MinimumWidth = 6;
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.MinimumWidth = 6;
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndTypeCol.Width = 125;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал \"зеленый\"";
            this._outIndSignalCol.MinimumWidth = 6;
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignalCol.Width = 130;
            // 
            // _out1IndSignalCol
            // 
            this._out1IndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._out1IndSignalCol.HeaderText = "Сигнал \"красный\"";
            this._out1IndSignalCol.MinimumWidth = 6;
            this._out1IndSignalCol.Name = "_out1IndSignalCol";
            this._out1IndSignalCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._out1IndSignalCol.Width = 130;
            // 
            // _outIndSignal2Col
            // 
            this._outIndSignal2Col.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignal2Col.HeaderText = "Режим работы";
            this._outIndSignal2Col.MinimumWidth = 6;
            this._outIndSignal2Col.Name = "_outIndSignal2Col";
            this._outIndSignal2Col.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._outIndSignal2Col.Width = 150;
            // 
            // groupBox176
            // 
            this.groupBox176.Controls.Add(this._outputReleGrid);
            this.groupBox176.Location = new System.Drawing.Point(8, 6);
            this.groupBox176.Name = "groupBox176";
            this.groupBox176.Size = new System.Drawing.Size(393, 541);
            this.groupBox176.TabIndex = 19;
            this.groupBox176.TabStop = false;
            this.groupBox176.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 13);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidth = 51;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(376, 522);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.MinimumWidth = 6;
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.MinimumWidth = 6;
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.MinimumWidth = 6;
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 140;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Tвозвр., мс";
            this._releWaitCol.MinimumWidth = 6;
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.Width = 70;
            // 
            // _inputSignalsPage
            // 
            this._inputSignalsPage.Controls.Add(this.groupBox18);
            this._inputSignalsPage.Controls.Add(this.groupBox15);
            this._inputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSignalsPage.Name = "_inputSignalsPage";
            this._inputSignalsPage.Size = new System.Drawing.Size(976, 553);
            this._inputSignalsPage.TabIndex = 7;
            this._inputSignalsPage.Text = "Входные сигналы";
            this._inputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(8, 86);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(254, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс блинкеров";
            // 
            // _indComboBox
            // 
            this._indComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._indComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(76, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this._grUst2ComboBox);
            this.groupBox15.Controls.Add(this.label1);
            this.groupBox15.Controls.Add(this._grUst1ComboBox);
            this.groupBox15.Location = new System.Drawing.Point(8, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(254, 77);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Группы уставок";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 48);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(51, 13);
            this.label76.TabIndex = 3;
            this.label76.Text = "Группа 2";
            // 
            // _grUst2ComboBox
            // 
            this._grUst2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst2ComboBox.FormattingEnabled = true;
            this._grUst2ComboBox.Location = new System.Drawing.Point(76, 45);
            this._grUst2ComboBox.Name = "_grUst2ComboBox";
            this._grUst2ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst2ComboBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Группа 1";
            // 
            // _grUst1ComboBox
            // 
            this._grUst1ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._grUst1ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._grUst1ComboBox.FormattingEnabled = true;
            this._grUst1ComboBox.Location = new System.Drawing.Point(76, 18);
            this._grUst1ComboBox.Name = "_grUst1ComboBox";
            this._grUst1ComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUst1ComboBox.TabIndex = 0;
            // 
            // _setpointPage
            // 
            this._setpointPage.Controls.Add(this.groupBox5);
            this._setpointPage.Controls.Add(this.groupBox25);
            this._setpointPage.Controls.Add(this.groupBox24);
            this._setpointPage.Controls.Add(this.groupBox1);
            this._setpointPage.Controls.Add(this.groupBox7);
            this._setpointPage.Location = new System.Drawing.Point(4, 22);
            this._setpointPage.Name = "_setpointPage";
            this._setpointPage.Size = new System.Drawing.Size(976, 553);
            this._setpointPage.TabIndex = 4;
            this._setpointPage.Text = "Уставки";
            this._setpointPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._inpAddCombo);
            this.groupBox5.Location = new System.Drawing.Point(562, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(139, 54);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Вход опорного сигнала";
            // 
            // _inpAddCombo
            // 
            this._inpAddCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inpAddCombo.FormattingEnabled = true;
            this._inpAddCombo.Location = new System.Drawing.Point(6, 21);
            this._inpAddCombo.Name = "_inpAddCombo";
            this._inpAddCombo.Size = new System.Drawing.Size(127, 21);
            this._inpAddCombo.TabIndex = 0;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._resistCheck);
            this.groupBox25.Location = new System.Drawing.Point(292, 3);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(264, 54);
            this.groupBox25.TabIndex = 5;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Выводимое сопротивление";
            // 
            // _resistCheck
            // 
            this._resistCheck.AutoSize = true;
            this._resistCheck.Location = new System.Drawing.Point(6, 21);
            this._resistCheck.Name = "_resistCheck";
            this._resistCheck.Size = new System.Drawing.Size(252, 17);
            this._resistCheck.TabIndex = 4;
            this._resistCheck.Text = "Ввод сопротивления в первичных значениях";
            this._resistCheck.UseVisualStyleBackColor = true;
            this._resistCheck.CheckedChanged += new System.EventHandler(this._resistCheck_CheckedChanged);
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._applyCopySetpoinsButton);
            this.groupBox24.Controls.Add(this._copySetpoinsGroupComboBox);
            this.groupBox24.Location = new System.Drawing.Point(112, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(174, 54);
            this.groupBox24.TabIndex = 3;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Копировать в";
            // 
            // _applyCopySetpoinsButton
            // 
            this._applyCopySetpoinsButton.Location = new System.Drawing.Point(95, 17);
            this._applyCopySetpoinsButton.Name = "_applyCopySetpoinsButton";
            this._applyCopySetpoinsButton.Size = new System.Drawing.Size(75, 23);
            this._applyCopySetpoinsButton.TabIndex = 1;
            this._applyCopySetpoinsButton.Text = "Применить";
            this._applyCopySetpoinsButton.UseVisualStyleBackColor = true;
            this._applyCopySetpoinsButton.Click += new System.EventHandler(this._applyCopySetpoinsButton_Click);
            // 
            // _copySetpoinsGroupComboBox
            // 
            this._copySetpoinsGroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._copySetpoinsGroupComboBox.FormattingEnabled = true;
            this._copySetpoinsGroupComboBox.Location = new System.Drawing.Point(6, 19);
            this._copySetpoinsGroupComboBox.Name = "_copySetpoinsGroupComboBox";
            this._copySetpoinsGroupComboBox.Size = new System.Drawing.Size(83, 21);
            this._copySetpoinsGroupComboBox.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._setpointsTab);
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(970, 491);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // _setpointsTab
            // 
            this._setpointsTab.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._setpointsTab.Controls.Add(this._measureTransPage);
            this._setpointsTab.Controls.Add(this._diffPage);
            this._setpointsTab.Controls.Add(this._defResistGr1);
            this._setpointsTab.Controls.Add(this._defIPage);
            this._setpointsTab.Controls.Add(this._defUGr1);
            this._setpointsTab.Controls.Add(this._defFGr1);
            this._setpointsTab.Controls.Add(this._defDvigGr1);
            this._setpointsTab.Controls.Add(this._defExtGr1);
            this._setpointsTab.Controls.Add(this._powerPage);
            this._setpointsTab.Controls.Add(this._lsGr);
            this._setpointsTab.Controls.Add(this._VLSGr);
            this._setpointsTab.Controls.Add(this._apvGr1);
            this._setpointsTab.Controls.Add(this._ksuppnGr1);
            this._setpointsTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this._setpointsTab.Location = new System.Drawing.Point(3, 16);
            this._setpointsTab.Name = "_setpointsTab";
            this._setpointsTab.SelectedIndex = 0;
            this._setpointsTab.Size = new System.Drawing.Size(964, 472);
            this._setpointsTab.TabIndex = 2;
            // 
            // _measureTransPage
            // 
            this._measureTransPage.Controls.Add(this.tabControl5);
            this._measureTransPage.Location = new System.Drawing.Point(4, 25);
            this._measureTransPage.Name = "_measureTransPage";
            this._measureTransPage.Size = new System.Drawing.Size(956, 443);
            this._measureTransPage.TabIndex = 15;
            this._measureTransPage.Text = "Параметры измерений";
            this._measureTransPage.UseVisualStyleBackColor = true;
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this._transformPage);
            this.tabControl5.Controls.Add(this._enginePage);
            this.tabControl5.Controls.Add(this._controlPage);
            this.tabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl5.Location = new System.Drawing.Point(0, 0);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(956, 443);
            this.tabControl5.TabIndex = 33;
            // 
            // _transformPage
            // 
            this._transformPage.BackColor = System.Drawing.SystemColors.Control;
            this._transformPage.Controls.Add(this._numberOfWindingComboBox);
            this._transformPage.Controls.Add(this.label237);
            this._transformPage.Controls.Add(this.groupBox56);
            this._transformPage.Location = new System.Drawing.Point(4, 22);
            this._transformPage.Name = "_transformPage";
            this._transformPage.Padding = new System.Windows.Forms.Padding(3);
            this._transformPage.Size = new System.Drawing.Size(948, 417);
            this._transformPage.TabIndex = 1;
            this._transformPage.Text = "Параметры сторон";
            // 
            // _numberOfWindingComboBox
            // 
            this._numberOfWindingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._numberOfWindingComboBox.FormattingEnabled = true;
            this._numberOfWindingComboBox.Location = new System.Drawing.Point(98, 9);
            this._numberOfWindingComboBox.Name = "_numberOfWindingComboBox";
            this._numberOfWindingComboBox.Size = new System.Drawing.Size(48, 21);
            this._numberOfWindingComboBox.TabIndex = 4;
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(7, 12);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(77, 13);
            this.label237.TabIndex = 2;
            this.label237.Text = "Число сторон";
            // 
            // groupBox56
            // 
            this.groupBox56.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox56.Controls.Add(this.tabControl4);
            this.groupBox56.Location = new System.Drawing.Point(3, 31);
            this.groupBox56.Name = "groupBox56";
            this.groupBox56.Size = new System.Drawing.Size(942, 383);
            this.groupBox56.TabIndex = 3;
            this.groupBox56.TabStop = false;
            // 
            // tabControl4
            // 
            this.tabControl4.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl4.Controls.Add(this.tabPage5);
            this.tabControl4.Controls.Add(this.tabPage6);
            this.tabControl4.Controls.Add(this.tabPage7);
            this.tabControl4.Controls.Add(this.tabPage8);
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Controls.Add(this.tabPage10);
            this.tabControl4.Controls.Add(this.tabPage11);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(3, 16);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(936, 364);
            this.tabControl4.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.sideTransformControl1);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(928, 335);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Сторона 1";
            // 
            // sideTransformControl1
            // 
            this.sideTransformControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sideTransformControl1.IsSideN = false;
            this.sideTransformControl1.Location = new System.Drawing.Point(3, 3);
            this.sideTransformControl1.Margin = new System.Windows.Forms.Padding(4);
            this.sideTransformControl1.Name = "sideTransformControl1";
            this.sideTransformControl1.Size = new System.Drawing.Size(922, 329);
            this.sideTransformControl1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage6.Controls.Add(this.sideTransformControl2);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(928, 335);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Сторона 2";
            // 
            // sideTransformControl2
            // 
            this.sideTransformControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sideTransformControl2.IsSideN = true;
            this.sideTransformControl2.Location = new System.Drawing.Point(3, 3);
            this.sideTransformControl2.Margin = new System.Windows.Forms.Padding(4);
            this.sideTransformControl2.Name = "sideTransformControl2";
            this.sideTransformControl2.Size = new System.Drawing.Size(922, 329);
            this.sideTransformControl2.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage7.Controls.Add(this.sideTransformControl3);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(928, 335);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Сторона 3";
            // 
            // sideTransformControl3
            // 
            this.sideTransformControl3.IsSideN = true;
            this.sideTransformControl3.Location = new System.Drawing.Point(3, 3);
            this.sideTransformControl3.Margin = new System.Windows.Forms.Padding(4);
            this.sideTransformControl3.Name = "sideTransformControl3";
            this.sideTransformControl3.Size = new System.Drawing.Size(922, 329);
            this.sideTransformControl3.TabIndex = 0;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage8.Controls.Add(this.sideTransformControl4);
            this.tabPage8.Location = new System.Drawing.Point(4, 25);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(928, 335);
            this.tabPage8.TabIndex = 3;
            this.tabPage8.Text = "Сторона 4";
            // 
            // sideTransformControl4
            // 
            this.sideTransformControl4.IsSideN = true;
            this.sideTransformControl4.Location = new System.Drawing.Point(3, 3);
            this.sideTransformControl4.Margin = new System.Windows.Forms.Padding(4);
            this.sideTransformControl4.Name = "sideTransformControl4";
            this.sideTransformControl4.Size = new System.Drawing.Size(922, 329);
            this.sideTransformControl4.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage9.Controls.Add(this.groupParamSideControl1);
            this.tabPage9.Location = new System.Drawing.Point(4, 25);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(928, 335);
            this.tabPage9.TabIndex = 4;
            this.tabPage9.Text = "Группа Uabc1";
            // 
            // groupParamSideControl1
            // 
            this.groupParamSideControl1.IsUnChannel = false;
            this.groupParamSideControl1.Location = new System.Drawing.Point(3, 3);
            this.groupParamSideControl1.Margin = new System.Windows.Forms.Padding(4);
            this.groupParamSideControl1.Name = "groupParamSideControl1";
            this.groupParamSideControl1.Size = new System.Drawing.Size(285, 201);
            this.groupParamSideControl1.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage10.Controls.Add(this.groupParamSideControl2);
            this.tabPage10.Location = new System.Drawing.Point(4, 25);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(928, 335);
            this.tabPage10.TabIndex = 5;
            this.tabPage10.Text = "Группа Uabc2";
            // 
            // groupParamSideControl2
            // 
            this.groupParamSideControl2.IsUnChannel = false;
            this.groupParamSideControl2.Location = new System.Drawing.Point(3, 3);
            this.groupParamSideControl2.Margin = new System.Windows.Forms.Padding(4);
            this.groupParamSideControl2.Name = "groupParamSideControl2";
            this.groupParamSideControl2.Size = new System.Drawing.Size(285, 201);
            this.groupParamSideControl2.TabIndex = 0;
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage11.Controls.Add(this.groupParamSideControl3);
            this.tabPage11.Location = new System.Drawing.Point(4, 25);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(928, 335);
            this.tabPage11.TabIndex = 6;
            this.tabPage11.Text = "Канал Un";
            // 
            // groupParamSideControl3
            // 
            this.groupParamSideControl3.IsUnChannel = true;
            this.groupParamSideControl3.Location = new System.Drawing.Point(3, 3);
            this.groupParamSideControl3.Margin = new System.Windows.Forms.Padding(4);
            this.groupParamSideControl3.Name = "groupParamSideControl3";
            this.groupParamSideControl3.Size = new System.Drawing.Size(285, 123);
            this.groupParamSideControl3.TabIndex = 0;
            // 
            // _enginePage
            // 
            this._enginePage.BackColor = System.Drawing.SystemColors.Control;
            this._enginePage.Controls.Add(this.groupBox9);
            this._enginePage.Location = new System.Drawing.Point(4, 22);
            this._enginePage.Name = "_enginePage";
            this._enginePage.Size = new System.Drawing.Size(948, 417);
            this._enginePage.TabIndex = 2;
            this._enginePage.Text = "Двигатель";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label86);
            this.groupBox9.Controls.Add(this.label152);
            this.groupBox9.Controls.Add(this._engineNreset);
            this.groupBox9.Controls.Add(this._engineQreset);
            this.groupBox9.Controls.Add(this._qBox);
            this.groupBox9.Controls.Add(this.label215);
            this.groupBox9.Controls.Add(this._tCount);
            this.groupBox9.Controls.Add(this.label216);
            this.groupBox9.Controls.Add(this._tStartBox);
            this.groupBox9.Controls.Add(this.label217);
            this.groupBox9.Controls.Add(this._iStartBox);
            this.groupBox9.Controls.Add(this.label218);
            this.groupBox9.Controls.Add(this._engineIdv);
            this.groupBox9.Controls.Add(this.label219);
            this.groupBox9.Controls.Add(this._engineCoolingTimeGr1);
            this.groupBox9.Controls.Add(this.label220);
            this.groupBox9.Controls.Add(this._engineHeatingTimeGr1);
            this.groupBox9.Controls.Add(this.label221);
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(224, 217);
            this.groupBox9.TabIndex = 41;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Параметры двигателя";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(12, 175);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(75, 13);
            this.label86.TabIndex = 14;
            this.label86.Text = "Вход N сброс";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(12, 155);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(75, 13);
            this.label152.TabIndex = 14;
            this.label152.Text = "Вход Q сброс";
            // 
            // _engineNreset
            // 
            this._engineNreset.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineNreset.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineNreset.FormattingEnabled = true;
            this._engineNreset.Location = new System.Drawing.Point(95, 172);
            this._engineNreset.Name = "_engineNreset";
            this._engineNreset.Size = new System.Drawing.Size(109, 21);
            this._engineNreset.TabIndex = 12;
            // 
            // _engineQreset
            // 
            this._engineQreset.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._engineQreset.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._engineQreset.FormattingEnabled = true;
            this._engineQreset.Location = new System.Drawing.Point(95, 152);
            this._engineQreset.Name = "_engineQreset";
            this._engineQreset.Size = new System.Drawing.Size(109, 21);
            this._engineQreset.TabIndex = 12;
            // 
            // _qBox
            // 
            this._qBox.Location = new System.Drawing.Point(135, 133);
            this._qBox.Name = "_qBox";
            this._qBox.Size = new System.Drawing.Size(69, 20);
            this._qBox.TabIndex = 3;
            this._qBox.Tag = "65000";
            this._qBox.Text = "0";
            this._qBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(12, 136);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(46, 13);
            this.label215.TabIndex = 2;
            this.label215.Text = "Qгор, %";
            // 
            // _tCount
            // 
            this._tCount.Location = new System.Drawing.Point(135, 114);
            this._tCount.Name = "_tCount";
            this._tCount.Size = new System.Drawing.Size(69, 20);
            this._tCount.TabIndex = 3;
            this._tCount.Tag = "65000";
            this._tCount.Text = "0";
            this._tCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(12, 117);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(49, 13);
            this.label216.TabIndex = 2;
            this.label216.Text = "Tдлит, с";
            // 
            // _tStartBox
            // 
            this._tStartBox.Location = new System.Drawing.Point(135, 95);
            this._tStartBox.Name = "_tStartBox";
            this._tStartBox.Size = new System.Drawing.Size(69, 20);
            this._tStartBox.TabIndex = 3;
            this._tStartBox.Tag = "65000";
            this._tStartBox.Text = "0";
            this._tStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(12, 98);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(57, 13);
            this.label217.TabIndex = 2;
            this.label217.Text = "Tпуск, мс";
            // 
            // _iStartBox
            // 
            this._iStartBox.Location = new System.Drawing.Point(135, 76);
            this._iStartBox.Name = "_iStartBox";
            this._iStartBox.Size = new System.Drawing.Size(69, 20);
            this._iStartBox.TabIndex = 3;
            this._iStartBox.Tag = "65000";
            this._iStartBox.Text = "0";
            this._iStartBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(12, 79);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(48, 13);
            this.label218.TabIndex = 2;
            this.label218.Text = "Iпуск, Iн";
            // 
            // _engineIdv
            // 
            this._engineIdv.Location = new System.Drawing.Point(135, 57);
            this._engineIdv.Name = "_engineIdv";
            this._engineIdv.Size = new System.Drawing.Size(69, 20);
            this._engineIdv.TabIndex = 3;
            this._engineIdv.Tag = "65000";
            this._engineIdv.Text = "0";
            this._engineIdv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(12, 60);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(37, 13);
            this.label219.TabIndex = 2;
            this.label219.Text = "Iдв, Iн";
            // 
            // _engineCoolingTimeGr1
            // 
            this._engineCoolingTimeGr1.Location = new System.Drawing.Point(135, 38);
            this._engineCoolingTimeGr1.Name = "_engineCoolingTimeGr1";
            this._engineCoolingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineCoolingTimeGr1.TabIndex = 3;
            this._engineCoolingTimeGr1.Tag = "65000";
            this._engineCoolingTimeGr1.Text = "0";
            this._engineCoolingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(12, 41);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(90, 13);
            this.label220.TabIndex = 2;
            this.label220.Text = "Т охлаждения, с";
            // 
            // _engineHeatingTimeGr1
            // 
            this._engineHeatingTimeGr1.Location = new System.Drawing.Point(135, 19);
            this._engineHeatingTimeGr1.Name = "_engineHeatingTimeGr1";
            this._engineHeatingTimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineHeatingTimeGr1.TabIndex = 1;
            this._engineHeatingTimeGr1.Tag = "65000";
            this._engineHeatingTimeGr1.Text = "0";
            this._engineHeatingTimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(12, 22);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(67, 13);
            this.label221.TabIndex = 0;
            this.label221.Text = "T нагрева,c";
            // 
            // _controlPage
            // 
            this._controlPage.BackColor = System.Drawing.SystemColors.Control;
            this._controlPage.Controls.Add(this.groupBox48);
            this._controlPage.Location = new System.Drawing.Point(4, 22);
            this._controlPage.Name = "_controlPage";
            this._controlPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlPage.Size = new System.Drawing.Size(948, 417);
            this._controlPage.TabIndex = 0;
            this._controlPage.Text = "Контроль цепей TH";
            // 
            // groupBox48
            // 
            this.groupBox48.Controls.Add(this.checkBox1);
            this.groupBox48.Controls.Add(this.label139);
            this.groupBox48.Controls.Add(this._resetTnGr1);
            this.groupBox48.Controls.Add(this._i0u0CheckGr1);
            this.groupBox48.Controls.Add(this._i2u2CheckGr1);
            this.groupBox48.Controls.Add(this._tdContrCepGr1);
            this.groupBox48.Controls.Add(this.label137);
            this.groupBox48.Controls.Add(this._iMinContrCepGr1);
            this.groupBox48.Controls.Add(this.label130);
            this.groupBox48.Controls.Add(this._uMinContrCepGr1);
            this.groupBox48.Controls.Add(this.label125);
            this.groupBox48.Controls.Add(this._u0ContrCepGr1);
            this.groupBox48.Controls.Add(this.label138);
            this.groupBox48.Controls.Add(this.label136);
            this.groupBox48.Controls.Add(this.label124);
            this.groupBox48.Controls.Add(this.label131);
            this.groupBox48.Controls.Add(this.label129);
            this.groupBox48.Controls.Add(this._u2ContrCepGr1);
            this.groupBox48.Controls.Add(this._tsContrCepGr1);
            this.groupBox48.Controls.Add(this._uDelContrCepGr1);
            this.groupBox48.Controls.Add(this.label128);
            this.groupBox48.Controls.Add(this._iDelContrCepGr1);
            this.groupBox48.Controls.Add(this._iMaxContrCepGr1);
            this.groupBox48.Controls.Add(this.label123);
            this.groupBox48.Controls.Add(this._uMaxContrCepGr1);
            this.groupBox48.Controls.Add(this.label120);
            this.groupBox48.Controls.Add(this._i0ContrCepGr1);
            this.groupBox48.Controls.Add(this.label122);
            this.groupBox48.Controls.Add(this._i2ContrCepGr1);
            this.groupBox48.Location = new System.Drawing.Point(6, 6);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(234, 240);
            this.groupBox48.TabIndex = 36;
            this.groupBox48.TabStop = false;
            this.groupBox48.Text = "Контроль цепей ТН";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(18, 165);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(89, 17);
            this.checkBox1.TabIndex = 43;
            this.checkBox1.Text = "Обр. 3-х фаз";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(15, 143);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(38, 13);
            this.label139.TabIndex = 42;
            this.label139.Text = "Сброс";
            // 
            // _resetTnGr1
            // 
            this._resetTnGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._resetTnGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._resetTnGr1.FormattingEnabled = true;
            this._resetTnGr1.Location = new System.Drawing.Point(74, 140);
            this._resetTnGr1.Name = "_resetTnGr1";
            this._resetTnGr1.Size = new System.Drawing.Size(107, 21);
            this._resetTnGr1.TabIndex = 41;
            // 
            // _i0u0CheckGr1
            // 
            this._i0u0CheckGr1.AutoSize = true;
            this._i0u0CheckGr1.Location = new System.Drawing.Point(17, 42);
            this._i0u0CheckGr1.Name = "_i0u0CheckGr1";
            this._i0u0CheckGr1.Size = new System.Drawing.Size(15, 14);
            this._i0u0CheckGr1.TabIndex = 3;
            this._i0u0CheckGr1.UseVisualStyleBackColor = true;
            // 
            // _i2u2CheckGr1
            // 
            this._i2u2CheckGr1.AutoSize = true;
            this._i2u2CheckGr1.Location = new System.Drawing.Point(17, 21);
            this._i2u2CheckGr1.Name = "_i2u2CheckGr1";
            this._i2u2CheckGr1.Size = new System.Drawing.Size(15, 14);
            this._i2u2CheckGr1.TabIndex = 3;
            this._i2u2CheckGr1.UseVisualStyleBackColor = true;
            // 
            // _tdContrCepGr1
            // 
            this._tdContrCepGr1.Location = new System.Drawing.Point(74, 99);
            this._tdContrCepGr1.Name = "_tdContrCepGr1";
            this._tdContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._tdContrCepGr1.TabIndex = 2;
            this._tdContrCepGr1.Text = "0";
            this._tdContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(15, 102);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(40, 13);
            this.label137.TabIndex = 1;
            this.label137.Text = "Td, мс";
            // 
            // _iMinContrCepGr1
            // 
            this._iMinContrCepGr1.Location = new System.Drawing.Point(173, 79);
            this._iMinContrCepGr1.Name = "_iMinContrCepGr1";
            this._iMinContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iMinContrCepGr1.TabIndex = 2;
            this._iMinContrCepGr1.Text = "0";
            this._iMinContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(127, 82);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(41, 13);
            this.label130.TabIndex = 1;
            this.label130.Text = "Imin, Iн";
            // 
            // _uMinContrCepGr1
            // 
            this._uMinContrCepGr1.Location = new System.Drawing.Point(173, 59);
            this._uMinContrCepGr1.Name = "_uMinContrCepGr1";
            this._uMinContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uMinContrCepGr1.TabIndex = 2;
            this._uMinContrCepGr1.Text = "0";
            this._uMinContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(127, 62);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(44, 13);
            this.label125.TabIndex = 1;
            this.label125.Text = "Umin, В";
            // 
            // _u0ContrCepGr1
            // 
            this._u0ContrCepGr1.Location = new System.Drawing.Point(173, 39);
            this._u0ContrCepGr1.Name = "_u0ContrCepGr1";
            this._u0ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._u0ContrCepGr1.TabIndex = 2;
            this._u0ContrCepGr1.Text = "0";
            this._u0ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(15, 123);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(39, 13);
            this.label138.TabIndex = 1;
            this.label138.Text = "Ts, мс";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(15, 207);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(35, 13);
            this.label136.TabIndex = 1;
            this.label136.Text = "dU, %";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(127, 42);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(40, 13);
            this.label124.TabIndex = 1;
            this.label124.Text = "3U0, В";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(15, 187);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(30, 13);
            this.label131.TabIndex = 1;
            this.label131.Text = "dI, %";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(15, 81);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(44, 13);
            this.label129.TabIndex = 1;
            this.label129.Text = "Imax, Iн";
            // 
            // _u2ContrCepGr1
            // 
            this._u2ContrCepGr1.Location = new System.Drawing.Point(173, 19);
            this._u2ContrCepGr1.Name = "_u2ContrCepGr1";
            this._u2ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._u2ContrCepGr1.TabIndex = 2;
            this._u2ContrCepGr1.Text = "0";
            this._u2ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _tsContrCepGr1
            // 
            this._tsContrCepGr1.Location = new System.Drawing.Point(74, 119);
            this._tsContrCepGr1.Name = "_tsContrCepGr1";
            this._tsContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._tsContrCepGr1.TabIndex = 2;
            this._tsContrCepGr1.Text = "0";
            this._tsContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _uDelContrCepGr1
            // 
            this._uDelContrCepGr1.Location = new System.Drawing.Point(74, 204);
            this._uDelContrCepGr1.Name = "_uDelContrCepGr1";
            this._uDelContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uDelContrCepGr1.TabIndex = 2;
            this._uDelContrCepGr1.Text = "0";
            this._uDelContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(15, 62);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(44, 13);
            this.label128.TabIndex = 1;
            this.label128.Text = "Umax,В";
            // 
            // _iDelContrCepGr1
            // 
            this._iDelContrCepGr1.Location = new System.Drawing.Point(74, 184);
            this._iDelContrCepGr1.Name = "_iDelContrCepGr1";
            this._iDelContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iDelContrCepGr1.TabIndex = 2;
            this._iDelContrCepGr1.Text = "0";
            this._iDelContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iMaxContrCepGr1
            // 
            this._iMaxContrCepGr1.Location = new System.Drawing.Point(74, 79);
            this._iMaxContrCepGr1.Name = "_iMaxContrCepGr1";
            this._iMaxContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._iMaxContrCepGr1.TabIndex = 2;
            this._iMaxContrCepGr1.Text = "0";
            this._iMaxContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(31, 42);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(37, 13);
            this.label123.TabIndex = 1;
            this.label123.Text = "3I0, Iн";
            // 
            // _uMaxContrCepGr1
            // 
            this._uMaxContrCepGr1.Location = new System.Drawing.Point(74, 59);
            this._uMaxContrCepGr1.Name = "_uMaxContrCepGr1";
            this._uMaxContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._uMaxContrCepGr1.TabIndex = 2;
            this._uMaxContrCepGr1.Text = "0";
            this._uMaxContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(133, 23);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(34, 13);
            this.label120.TabIndex = 1;
            this.label120.Text = "U2, В";
            // 
            // _i0ContrCepGr1
            // 
            this._i0ContrCepGr1.Location = new System.Drawing.Point(74, 39);
            this._i0ContrCepGr1.Name = "_i0ContrCepGr1";
            this._i0ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._i0ContrCepGr1.TabIndex = 2;
            this._i0ContrCepGr1.Text = "0";
            this._i0ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(31, 22);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(31, 13);
            this.label122.TabIndex = 1;
            this.label122.Text = "I2, Iн";
            // 
            // _i2ContrCepGr1
            // 
            this._i2ContrCepGr1.Location = new System.Drawing.Point(74, 19);
            this._i2ContrCepGr1.Name = "_i2ContrCepGr1";
            this._i2ContrCepGr1.Size = new System.Drawing.Size(47, 20);
            this._i2ContrCepGr1.TabIndex = 2;
            this._i2ContrCepGr1.Text = "0";
            this._i2ContrCepGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _diffPage
            // 
            this._diffPage.Controls.Add(this.tabControl6);
            this._diffPage.Location = new System.Drawing.Point(4, 25);
            this._diffPage.Name = "_diffPage";
            this._diffPage.Size = new System.Drawing.Size(956, 443);
            this._diffPage.TabIndex = 22;
            this._diffPage.Text = "Дифф. защ.";
            this._diffPage.UseVisualStyleBackColor = true;
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this._diffDefPage);
            this.tabControl6.Controls.Add(this._diffCutOffPage);
            this.tabControl6.Controls.Add(this._diffZeroPage);
            this.tabControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl6.Location = new System.Drawing.Point(0, 0);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(956, 443);
            this.tabControl6.TabIndex = 0;
            // 
            // _diffDefPage
            // 
            this._diffDefPage.BackColor = System.Drawing.SystemColors.Control;
            this._diffDefPage.Controls.Add(this.groupBox16);
            this._diffDefPage.Location = new System.Drawing.Point(4, 22);
            this._diffDefPage.Name = "_diffDefPage";
            this._diffDefPage.Padding = new System.Windows.Forms.Padding(3);
            this._diffDefPage.Size = new System.Drawing.Size(948, 417);
            this._diffDefPage.TabIndex = 0;
            this._diffDefPage.Text = "Дифф. защита";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._AVRDTZComboBox);
            this.groupBox16.Controls.Add(this._oscDTZComboBox);
            this.groupBox16.Controls.Add(this.label113);
            this.groupBox16.Controls.Add(this.label81);
            this.groupBox16.Controls.Add(this._APVDTZComboBox);
            this.groupBox16.Controls.Add(this.label91);
            this.groupBox16.Controls.Add(this.groupBox20);
            this.groupBox16.Controls.Add(this._UROVDTZComboBox);
            this.groupBox16.Controls.Add(this._blockingDTZComboBox);
            this.groupBox16.Controls.Add(this.label114);
            this.groupBox16.Controls.Add(this.label115);
            this.groupBox16.Controls.Add(this._timeEnduranceDTZTextBox);
            this.groupBox16.Controls.Add(this.groupBox21);
            this.groupBox16.Controls.Add(this._constraintDTZTextBox);
            this.groupBox16.Controls.Add(this.groupBox30);
            this.groupBox16.Controls.Add(this.label155);
            this.groupBox16.Controls.Add(this.label156);
            this.groupBox16.Controls.Add(this.label157);
            this.groupBox16.Controls.Add(this.label158);
            this.groupBox16.Controls.Add(this._modeDTZComboBox);
            this.groupBox16.Controls.Add(this.label160);
            this.groupBox16.Location = new System.Drawing.Point(6, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(611, 273);
            this.groupBox16.TabIndex = 14;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Диф. токовая защита";
            // 
            // _AVRDTZComboBox
            // 
            this._AVRDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTZComboBox.FormattingEnabled = true;
            this._AVRDTZComboBox.Location = new System.Drawing.Point(427, 183);
            this._AVRDTZComboBox.Name = "_AVRDTZComboBox";
            this._AVRDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTZComboBox.TabIndex = 46;
            // 
            // _oscDTZComboBox
            // 
            this._oscDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTZComboBox.FormattingEnabled = true;
            this._oscDTZComboBox.Location = new System.Drawing.Point(427, 123);
            this._oscDTZComboBox.Name = "_oscDTZComboBox";
            this._oscDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTZComboBox.TabIndex = 29;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(292, 126);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(76, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "Осциллограф";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(292, 186);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 45;
            this.label81.Text = "АВР";
            // 
            // _APVDTZComboBox
            // 
            this._APVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTZComboBox.FormattingEnabled = true;
            this._APVDTZComboBox.Location = new System.Drawing.Point(427, 163);
            this._APVDTZComboBox.Name = "_APVDTZComboBox";
            this._APVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTZComboBox.TabIndex = 44;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(292, 166);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(29, 13);
            this.label91.TabIndex = 43;
            this.label91.Text = "АПВ";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._modeI5I1);
            this.groupBox20.Controls.Add(this._perBlockI5I1);
            this.groupBox20.Controls.Add(this.label92);
            this.groupBox20.Controls.Add(this._I5I1TextBox);
            this.groupBox20.Controls.Add(this.label100);
            this.groupBox20.Controls.Add(this.label112);
            this.groupBox20.Location = new System.Drawing.Point(6, 185);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(261, 73);
            this.groupBox20.TabIndex = 41;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Блокировка I5/I1";
            // 
            // _modeI5I1
            // 
            this._modeI5I1.AutoSize = true;
            this._modeI5I1.Location = new System.Drawing.Point(142, 0);
            this._modeI5I1.Name = "_modeI5I1";
            this._modeI5I1.Size = new System.Drawing.Size(15, 14);
            this._modeI5I1.TabIndex = 15;
            this._modeI5I1.UseVisualStyleBackColor = true;
            // 
            // _perBlockI5I1
            // 
            this._perBlockI5I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI5I1.FormattingEnabled = true;
            this._perBlockI5I1.Location = new System.Drawing.Point(142, 21);
            this._perBlockI5I1.Name = "_perBlockI5I1";
            this._perBlockI5I1.Size = new System.Drawing.Size(89, 21);
            this._perBlockI5I1.TabIndex = 42;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 24);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(78, 13);
            this.label92.TabIndex = 41;
            this.label92.Text = "Перекр. блок.";
            // 
            // _I5I1TextBox
            // 
            this._I5I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I5I1TextBox.Location = new System.Drawing.Point(142, 46);
            this._I5I1TextBox.Name = "_I5I1TextBox";
            this._I5I1TextBox.Size = new System.Drawing.Size(89, 20);
            this._I5I1TextBox.TabIndex = 37;
            this._I5I1TextBox.Tag = "100";
            this._I5I1TextBox.Text = "0";
            this._I5I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.ForeColor = System.Drawing.Color.Red;
            this.label100.Location = new System.Drawing.Point(233, 49);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(15, 13);
            this.label100.TabIndex = 39;
            this.label100.Text = "%";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 49);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(30, 13);
            this.label112.TabIndex = 38;
            this.label112.Text = "I5/I1";
            // 
            // _UROVDTZComboBox
            // 
            this._UROVDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTZComboBox.FormattingEnabled = true;
            this._UROVDTZComboBox.Location = new System.Drawing.Point(427, 144);
            this._UROVDTZComboBox.Name = "_UROVDTZComboBox";
            this._UROVDTZComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTZComboBox.TabIndex = 28;
            // 
            // _blockingDTZComboBox
            // 
            this._blockingDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTZComboBox.FormattingEnabled = true;
            this._blockingDTZComboBox.Location = new System.Drawing.Point(148, 33);
            this._blockingDTZComboBox.Name = "_blockingDTZComboBox";
            this._blockingDTZComboBox.Size = new System.Drawing.Size(106, 21);
            this._blockingDTZComboBox.TabIndex = 27;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(292, 147);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(37, 13);
            this.label114.TabIndex = 25;
            this.label114.Text = "УРОВ";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 36);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(68, 13);
            this.label115.TabIndex = 24;
            this.label115.Text = "Блокировка";
            // 
            // _timeEnduranceDTZTextBox
            // 
            this._timeEnduranceDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTZTextBox.Location = new System.Drawing.Point(148, 72);
            this._timeEnduranceDTZTextBox.Name = "_timeEnduranceDTZTextBox";
            this._timeEnduranceDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTZTextBox.TabIndex = 14;
            this._timeEnduranceDTZTextBox.Tag = "3276700";
            this._timeEnduranceDTZTextBox.Text = "0";
            this._timeEnduranceDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._modeI2I1);
            this.groupBox21.Controls.Add(this._perBlockI2I1);
            this.groupBox21.Controls.Add(this.label116);
            this.groupBox21.Controls.Add(this._I2I1TextBox);
            this.groupBox21.Controls.Add(this.label117);
            this.groupBox21.Controls.Add(this.label118);
            this.groupBox21.Location = new System.Drawing.Point(6, 104);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(261, 73);
            this.groupBox21.TabIndex = 23;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Блокировка I2/I1";
            // 
            // _modeI2I1
            // 
            this._modeI2I1.AutoSize = true;
            this._modeI2I1.Location = new System.Drawing.Point(142, 0);
            this._modeI2I1.Name = "_modeI2I1";
            this._modeI2I1.Size = new System.Drawing.Size(15, 14);
            this._modeI2I1.TabIndex = 15;
            this._modeI2I1.UseVisualStyleBackColor = true;
            // 
            // _perBlockI2I1
            // 
            this._perBlockI2I1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._perBlockI2I1.FormattingEnabled = true;
            this._perBlockI2I1.Location = new System.Drawing.Point(142, 22);
            this._perBlockI2I1.Name = "_perBlockI2I1";
            this._perBlockI2I1.Size = new System.Drawing.Size(90, 21);
            this._perBlockI2I1.TabIndex = 36;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 25);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(75, 13);
            this.label116.TabIndex = 30;
            this.label116.Text = "Перекр. блок";
            // 
            // _I2I1TextBox
            // 
            this._I2I1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._I2I1TextBox.Location = new System.Drawing.Point(142, 47);
            this._I2I1TextBox.Name = "_I2I1TextBox";
            this._I2I1TextBox.Size = new System.Drawing.Size(90, 20);
            this._I2I1TextBox.TabIndex = 19;
            this._I2I1TextBox.Tag = "100";
            this._I2I1TextBox.Text = "0";
            this._I2I1TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.ForeColor = System.Drawing.Color.Red;
            this.label117.Location = new System.Drawing.Point(233, 50);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(15, 13);
            this.label117.TabIndex = 24;
            this.label117.Text = "%";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(6, 49);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(33, 13);
            this.label118.TabIndex = 22;
            this.label118.Text = "I2/I1 ";
            // 
            // _constraintDTZTextBox
            // 
            this._constraintDTZTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTZTextBox.Location = new System.Drawing.Point(148, 53);
            this._constraintDTZTextBox.Name = "_constraintDTZTextBox";
            this._constraintDTZTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTZTextBox.TabIndex = 13;
            this._constraintDTZTextBox.Tag = "40";
            this._constraintDTZTextBox.Text = "0";
            this._constraintDTZTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label119);
            this.groupBox30.Controls.Add(this.label132);
            this.groupBox30.Controls.Add(this._K2TangensTextBox);
            this.groupBox30.Controls.Add(this.label133);
            this.groupBox30.Controls.Add(this._Ib2BeginTextBox);
            this.groupBox30.Controls.Add(this.label134);
            this.groupBox30.Controls.Add(this._K1AngleOfSlopeTextBox);
            this.groupBox30.Controls.Add(this._Ib1BeginTextBox);
            this.groupBox30.Controls.Add(this.label135);
            this.groupBox30.Controls.Add(this.label140);
            this.groupBox30.Controls.Add(this.label153);
            this.groupBox30.Controls.Add(this.label154);
            this.groupBox30.Location = new System.Drawing.Point(286, 13);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(261, 107);
            this.groupBox30.TabIndex = 22;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Характеристика торможения";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.ForeColor = System.Drawing.Color.Red;
            this.label119.Location = new System.Drawing.Point(232, 81);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(11, 13);
            this.label119.TabIndex = 24;
            this.label119.Text = "˚";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.ForeColor = System.Drawing.Color.Red;
            this.label132.Location = new System.Drawing.Point(232, 43);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(11, 13);
            this.label132.TabIndex = 23;
            this.label132.Text = "˚";
            // 
            // _K2TangensTextBox
            // 
            this._K2TangensTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K2TangensTextBox.Location = new System.Drawing.Point(141, 78);
            this._K2TangensTextBox.Name = "_K2TangensTextBox";
            this._K2TangensTextBox.Size = new System.Drawing.Size(90, 20);
            this._K2TangensTextBox.TabIndex = 18;
            this._K2TangensTextBox.Tag = "89";
            this._K2TangensTextBox.Text = "0";
            this._K2TangensTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.ForeColor = System.Drawing.Color.Red;
            this.label133.Location = new System.Drawing.Point(232, 62);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(22, 13);
            this.label133.TabIndex = 22;
            this.label133.Text = "[Iн]";
            // 
            // _Ib2BeginTextBox
            // 
            this._Ib2BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib2BeginTextBox.Location = new System.Drawing.Point(141, 59);
            this._Ib2BeginTextBox.Name = "_Ib2BeginTextBox";
            this._Ib2BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib2BeginTextBox.TabIndex = 17;
            this._Ib2BeginTextBox.Tag = "40";
            this._Ib2BeginTextBox.Text = "0";
            this._Ib2BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.ForeColor = System.Drawing.Color.Red;
            this.label134.Location = new System.Drawing.Point(232, 24);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(22, 13);
            this.label134.TabIndex = 21;
            this.label134.Text = "[Iн]";
            // 
            // _K1AngleOfSlopeTextBox
            // 
            this._K1AngleOfSlopeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._K1AngleOfSlopeTextBox.Location = new System.Drawing.Point(141, 40);
            this._K1AngleOfSlopeTextBox.Name = "_K1AngleOfSlopeTextBox";
            this._K1AngleOfSlopeTextBox.Size = new System.Drawing.Size(90, 20);
            this._K1AngleOfSlopeTextBox.TabIndex = 16;
            this._K1AngleOfSlopeTextBox.Tag = "89";
            this._K1AngleOfSlopeTextBox.Text = "0";
            this._K1AngleOfSlopeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Ib1BeginTextBox
            // 
            this._Ib1BeginTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ib1BeginTextBox.Location = new System.Drawing.Point(141, 21);
            this._Ib1BeginTextBox.Name = "_Ib1BeginTextBox";
            this._Ib1BeginTextBox.Size = new System.Drawing.Size(90, 20);
            this._Ib1BeginTextBox.TabIndex = 15;
            this._Ib1BeginTextBox.Tag = "40";
            this._Ib1BeginTextBox.Text = "0";
            this._Ib1BeginTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(6, 81);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(92, 13);
            this.label135.TabIndex = 5;
            this.label135.Text = "f2 - угол наклона";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(6, 62);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(66, 13);
            this.label140.TabIndex = 4;
            this.label140.Text = "Iб2 - начало";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(6, 43);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(92, 13);
            this.label153.TabIndex = 3;
            this.label153.Text = "f1 - угол наклона";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(6, 24);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(66, 13);
            this.label154.TabIndex = 2;
            this.label154.Text = "Iб1 - начало";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.ForeColor = System.Drawing.Color.Red;
            this.label155.Location = new System.Drawing.Point(239, 76);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(27, 13);
            this.label155.TabIndex = 21;
            this.label155.Text = "[мс]";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.ForeColor = System.Drawing.Color.Red;
            this.label156.Location = new System.Drawing.Point(239, 57);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(22, 13);
            this.label156.TabIndex = 20;
            this.label156.Text = "[Iн]";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(6, 76);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(129, 13);
            this.label157.TabIndex = 17;
            this.label157.Text = "Выдержка времени Tср";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(6, 57);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(68, 13);
            this.label158.TabIndex = 16;
            this.label158.Text = "Уставка Iср";
            // 
            // _modeDTZComboBox
            // 
            this._modeDTZComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTZComboBox.FormattingEnabled = true;
            this._modeDTZComboBox.Location = new System.Drawing.Point(148, 13);
            this._modeDTZComboBox.Name = "_modeDTZComboBox";
            this._modeDTZComboBox.Size = new System.Drawing.Size(106, 21);
            this._modeDTZComboBox.TabIndex = 9;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(6, 16);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(61, 13);
            this.label160.TabIndex = 8;
            this.label160.Text = "Состояние";
            // 
            // _diffCutOffPage
            // 
            this._diffCutOffPage.BackColor = System.Drawing.SystemColors.Control;
            this._diffCutOffPage.Controls.Add(this.groupBox8);
            this._diffCutOffPage.Location = new System.Drawing.Point(4, 22);
            this._diffCutOffPage.Name = "_diffCutOffPage";
            this._diffCutOffPage.Padding = new System.Windows.Forms.Padding(3);
            this._diffCutOffPage.Size = new System.Drawing.Size(184, 45);
            this._diffCutOffPage.TabIndex = 1;
            this._diffCutOffPage.Text = "Дифф. отсечка";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._AVRDTOBTComboBox);
            this.groupBox8.Controls.Add(this._oscDTOBTComboBox);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this._APVDTOBTComboBox);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this._timeEnduranceDTOBTTextBox);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this._constraintDTOBTTextBox);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this._blockingDTOBTComboBox);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this._UROVDTOBTComboBox);
            this.groupBox8.Controls.Add(this._stepOnInstantValuesDTOBTComboBox);
            this.groupBox8.Controls.Add(this._modeDTOBTComboBox);
            this.groupBox8.Controls.Add(this.label28);
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.label32);
            this.groupBox8.Controls.Add(this.label74);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(319, 206);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Диф. токовая отсечка без торможения";
            // 
            // _AVRDTOBTComboBox
            // 
            this._AVRDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._AVRDTOBTComboBox.FormattingEnabled = true;
            this._AVRDTOBTComboBox.Location = new System.Drawing.Point(196, 173);
            this._AVRDTOBTComboBox.Name = "_AVRDTOBTComboBox";
            this._AVRDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._AVRDTOBTComboBox.TabIndex = 20;
            // 
            // _oscDTOBTComboBox
            // 
            this._oscDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscDTOBTComboBox.FormattingEnabled = true;
            this._oscDTOBTComboBox.Location = new System.Drawing.Point(196, 113);
            this._oscDTOBTComboBox.Name = "_oscDTOBTComboBox";
            this._oscDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._oscDTOBTComboBox.TabIndex = 13;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 116);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(76, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Осциллограф";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 176);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "АВР";
            // 
            // _APVDTOBTComboBox
            // 
            this._APVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._APVDTOBTComboBox.FormattingEnabled = true;
            this._APVDTOBTComboBox.Location = new System.Drawing.Point(196, 153);
            this._APVDTOBTComboBox.Name = "_APVDTOBTComboBox";
            this._APVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._APVDTOBTComboBox.TabIndex = 18;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 156);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "АПВ";
            // 
            // _timeEnduranceDTOBTTextBox
            // 
            this._timeEnduranceDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeEnduranceDTOBTTextBox.Location = new System.Drawing.Point(196, 74);
            this._timeEnduranceDTOBTTextBox.Name = "_timeEnduranceDTOBTTextBox";
            this._timeEnduranceDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._timeEnduranceDTOBTTextBox.TabIndex = 16;
            this._timeEnduranceDTOBTTextBox.Tag = "3276700";
            this._timeEnduranceDTOBTTextBox.Text = "0";
            this._timeEnduranceDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(287, 77);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(27, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "[мс]";
            // 
            // _constraintDTOBTTextBox
            // 
            this._constraintDTOBTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._constraintDTOBTTextBox.Location = new System.Drawing.Point(196, 55);
            this._constraintDTOBTTextBox.Name = "_constraintDTOBTTextBox";
            this._constraintDTOBTTextBox.Size = new System.Drawing.Size(90, 20);
            this._constraintDTOBTTextBox.TabIndex = 15;
            this._constraintDTOBTTextBox.Tag = "40";
            this._constraintDTOBTTextBox.Text = "0";
            this._constraintDTOBTTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 38);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Блокировка";
            // 
            // _blockingDTOBTComboBox
            // 
            this._blockingDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockingDTOBTComboBox.FormattingEnabled = true;
            this._blockingDTOBTComboBox.Location = new System.Drawing.Point(196, 35);
            this._blockingDTOBTComboBox.Name = "_blockingDTOBTComboBox";
            this._blockingDTOBTComboBox.Size = new System.Drawing.Size(113, 21);
            this._blockingDTOBTComboBox.TabIndex = 11;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(287, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(22, 13);
            this.label26.TabIndex = 14;
            this.label26.Text = "[Iн]";
            // 
            // _UROVDTOBTComboBox
            // 
            this._UROVDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._UROVDTOBTComboBox.FormattingEnabled = true;
            this._UROVDTOBTComboBox.Location = new System.Drawing.Point(196, 133);
            this._UROVDTOBTComboBox.Name = "_UROVDTOBTComboBox";
            this._UROVDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._UROVDTOBTComboBox.TabIndex = 12;
            // 
            // _stepOnInstantValuesDTOBTComboBox
            // 
            this._stepOnInstantValuesDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._stepOnInstantValuesDTOBTComboBox.FormattingEnabled = true;
            this._stepOnInstantValuesDTOBTComboBox.Location = new System.Drawing.Point(196, 93);
            this._stepOnInstantValuesDTOBTComboBox.Name = "_stepOnInstantValuesDTOBTComboBox";
            this._stepOnInstantValuesDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._stepOnInstantValuesDTOBTComboBox.TabIndex = 8;
            // 
            // _modeDTOBTComboBox
            // 
            this._modeDTOBTComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._modeDTOBTComboBox.FormattingEnabled = true;
            this._modeDTOBTComboBox.Location = new System.Drawing.Point(196, 15);
            this._modeDTOBTComboBox.Name = "_modeDTOBTComboBox";
            this._modeDTOBTComboBox.Size = new System.Drawing.Size(90, 21);
            this._modeDTOBTComboBox.TabIndex = 7;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 136);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(37, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "УРОВ";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 77);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(129, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Выдержка времени Tср";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 58);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(68, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Уставка Iср";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 96);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(189, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "Ступень по мгновенным значениям";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(6, 18);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(61, 13);
            this.label74.TabIndex = 0;
            this.label74.Text = "Состояние";
            // 
            // _diffZeroPage
            // 
            this._diffZeroPage.BackColor = System.Drawing.SystemColors.Control;
            this._diffZeroPage.Controls.Add(this.groupBox6);
            this._diffZeroPage.Location = new System.Drawing.Point(4, 22);
            this._diffZeroPage.Name = "_diffZeroPage";
            this._diffZeroPage.Size = new System.Drawing.Size(184, 45);
            this._diffZeroPage.TabIndex = 2;
            this._diffZeroPage.Text = "Дифф. нул. посл-сти";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._dif0DataGreed);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(184, 45);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Диф. нул. последовательности";
            // 
            // _dif0DataGreed
            // 
            this._dif0DataGreed.AllowUserToAddRows = false;
            this._dif0DataGreed.AllowUserToDeleteRows = false;
            this._dif0DataGreed.AllowUserToResizeColumns = false;
            this._dif0DataGreed.AllowUserToResizeRows = false;
            this._dif0DataGreed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._dif0DataGreed.BackgroundColor = System.Drawing.Color.White;
            this._dif0DataGreed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dif0DataGreed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._dif0StageColumn,
            this.Column8,
            this._dif0BlockingColumn,
            this._dif0IdColumn,
            this._dif0InColumn,
            this._dif0TdColumn,
            this._dif0Ib1Column,
            this._dif0Intg1Column,
            this._dif0Ib2Column,
            this._dif0Intg2Column,
            this._dif0OscColumn,
            this._dif0UrovColumn,
            this._dif0APVColumn,
            this._dif0AVRColumn});
            this._dif0DataGreed.Location = new System.Drawing.Point(6, 21);
            this._dif0DataGreed.MultiSelect = false;
            this._dif0DataGreed.Name = "_dif0DataGreed";
            this._dif0DataGreed.RowHeadersVisible = false;
            this._dif0DataGreed.RowHeadersWidth = 51;
            this._dif0DataGreed.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._dif0DataGreed.RowTemplate.Height = 24;
            this._dif0DataGreed.ShowCellErrors = false;
            this._dif0DataGreed.ShowRowErrors = false;
            this._dif0DataGreed.Size = new System.Drawing.Size(172, 112);
            this._dif0DataGreed.TabIndex = 2;
            // 
            // _dif0StageColumn
            // 
            this._dif0StageColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this._dif0StageColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this._dif0StageColumn.Frozen = true;
            this._dif0StageColumn.HeaderText = "Ступень";
            this._dif0StageColumn.MinimumWidth = 6;
            this._dif0StageColumn.Name = "_dif0StageColumn";
            this._dif0StageColumn.ReadOnly = true;
            this._dif0StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0StageColumn.Width = 54;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Режим";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.Width = 110;
            // 
            // _dif0BlockingColumn
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.NullValue = "XXXXX";
            this._dif0BlockingColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this._dif0BlockingColumn.HeaderText = "Блокировка";
            this._dif0BlockingColumn.MaxDropDownItems = 15;
            this._dif0BlockingColumn.MinimumWidth = 6;
            this._dif0BlockingColumn.Name = "_dif0BlockingColumn";
            this._dif0BlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0BlockingColumn.Width = 107;
            // 
            // _dif0IdColumn
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0IdColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this._dif0IdColumn.HeaderText = "Iср, Iн";
            this._dif0IdColumn.MinimumWidth = 6;
            this._dif0IdColumn.Name = "_dif0IdColumn";
            this._dif0IdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0IdColumn.Width = 60;
            // 
            // _dif0InColumn
            // 
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle9.NullValue = "XXXXX";
            this._dif0InColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this._dif0InColumn.HeaderText = "Сторона";
            this._dif0InColumn.MaxDropDownItems = 15;
            this._dif0InColumn.MinimumWidth = 6;
            this._dif0InColumn.Name = "_dif0InColumn";
            this._dif0InColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dif0InColumn.Width = 85;
            // 
            // _dif0TdColumn
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0TdColumn.DefaultCellStyle = dataGridViewCellStyle10;
            this._dif0TdColumn.HeaderText = "tср, мс";
            this._dif0TdColumn.MinimumWidth = 6;
            this._dif0TdColumn.Name = "_dif0TdColumn";
            this._dif0TdColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0TdColumn.Width = 70;
            // 
            // _dif0Ib1Column
            // 
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib1Column.DefaultCellStyle = dataGridViewCellStyle11;
            this._dif0Ib1Column.HeaderText = "Iб1, Iн стороны";
            this._dif0Ib1Column.MinimumWidth = 6;
            this._dif0Ib1Column.Name = "_dif0Ib1Column";
            this._dif0Ib1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib1Column.Width = 115;
            // 
            // _dif0Intg1Column
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg1Column.DefaultCellStyle = dataGridViewCellStyle12;
            this._dif0Intg1Column.HeaderText = "Угол f1";
            this._dif0Intg1Column.MinimumWidth = 6;
            this._dif0Intg1Column.Name = "_dif0Intg1Column";
            this._dif0Intg1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg1Column.Width = 55;
            // 
            // _dif0Ib2Column
            // 
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Ib2Column.DefaultCellStyle = dataGridViewCellStyle13;
            this._dif0Ib2Column.HeaderText = "Iб2, Iн стороны";
            this._dif0Ib2Column.MinimumWidth = 6;
            this._dif0Ib2Column.Name = "_dif0Ib2Column";
            this._dif0Ib2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Ib2Column.Width = 115;
            // 
            // _dif0Intg2Column
            // 
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0Intg2Column.DefaultCellStyle = dataGridViewCellStyle14;
            this._dif0Intg2Column.HeaderText = "Угол f2";
            this._dif0Intg2Column.MinimumWidth = 6;
            this._dif0Intg2Column.Name = "_dif0Intg2Column";
            this._dif0Intg2Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._dif0Intg2Column.Width = 55;
            // 
            // _dif0OscColumn
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._dif0OscColumn.DefaultCellStyle = dataGridViewCellStyle15;
            this._dif0OscColumn.HeaderText = "Осциллограф";
            this._dif0OscColumn.MaxDropDownItems = 15;
            this._dif0OscColumn.MinimumWidth = 6;
            this._dif0OscColumn.Name = "_dif0OscColumn";
            this._dif0OscColumn.Width = 90;
            // 
            // _dif0UrovColumn
            // 
            this._dif0UrovColumn.HeaderText = "УРОВ";
            this._dif0UrovColumn.MinimumWidth = 6;
            this._dif0UrovColumn.Name = "_dif0UrovColumn";
            this._dif0UrovColumn.Width = 90;
            // 
            // _dif0APVColumn
            // 
            this._dif0APVColumn.HeaderText = "АПВ";
            this._dif0APVColumn.MinimumWidth = 6;
            this._dif0APVColumn.Name = "_dif0APVColumn";
            this._dif0APVColumn.Width = 90;
            // 
            // _dif0AVRColumn
            // 
            this._dif0AVRColumn.HeaderText = "АВР";
            this._dif0AVRColumn.MinimumWidth = 6;
            this._dif0AVRColumn.Name = "_dif0AVRColumn";
            this._dif0AVRColumn.Width = 90;
            // 
            // _defResistGr1
            // 
            this._defResistGr1.Controls.Add(this.resistanceDefTabCtr1);
            this._defResistGr1.Location = new System.Drawing.Point(4, 25);
            this._defResistGr1.Name = "_defResistGr1";
            this._defResistGr1.Padding = new System.Windows.Forms.Padding(3);
            this._defResistGr1.Size = new System.Drawing.Size(956, 443);
            this._defResistGr1.TabIndex = 17;
            this._defResistGr1.Text = "Дистанц. защ.";
            this._defResistGr1.UseVisualStyleBackColor = true;
            // 
            // resistanceDefTabCtr1
            // 
            this.resistanceDefTabCtr1.ChannelUMeasureTrans = null;
            this.resistanceDefTabCtr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resistanceDefTabCtr1.IsPrimary = false;
            this.resistanceDefTabCtr1.Location = new System.Drawing.Point(3, 3);
            this.resistanceDefTabCtr1.Margin = new System.Windows.Forms.Padding(4);
            this.resistanceDefTabCtr1.Name = "resistanceDefTabCtr1";
            this.resistanceDefTabCtr1.ResistConfigUnion = null;
            this.resistanceDefTabCtr1.SideMeasureTrans = null;
            this.resistanceDefTabCtr1.Size = new System.Drawing.Size(950, 437);
            this.resistanceDefTabCtr1.TabIndex = 0;
            // 
            // _defIPage
            // 
            this._defIPage.Controls.Add(this._startArcDefPage);
            this._defIPage.Location = new System.Drawing.Point(4, 25);
            this._defIPage.Name = "_defIPage";
            this._defIPage.Padding = new System.Windows.Forms.Padding(3);
            this._defIPage.Size = new System.Drawing.Size(956, 443);
            this._defIPage.TabIndex = 25;
            this._defIPage.Text = "Защ. по току";
            this._defIPage.UseVisualStyleBackColor = true;
            // 
            // _startArcDefPage
            // 
            this._startArcDefPage.Controls.Add(this._cornerGr1Page);
            this._startArcDefPage.Controls.Add(this._defIGr1Page);
            this._startArcDefPage.Controls.Add(this._defIStarGr1);
            this._startArcDefPage.Controls.Add(this._defI2I1Gr1);
            this._startArcDefPage.Controls.Add(this.tabPage2);
            this._startArcDefPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this._startArcDefPage.Location = new System.Drawing.Point(3, 3);
            this._startArcDefPage.Name = "_startArcDefPage";
            this._startArcDefPage.SelectedIndex = 0;
            this._startArcDefPage.Size = new System.Drawing.Size(950, 437);
            this._startArcDefPage.TabIndex = 0;
            // 
            // _cornerGr1Page
            // 
            this._cornerGr1Page.BackColor = System.Drawing.SystemColors.Control;
            this._cornerGr1Page.Controls.Add(this.groupBox2);
            this._cornerGr1Page.Location = new System.Drawing.Point(4, 22);
            this._cornerGr1Page.Name = "_cornerGr1Page";
            this._cornerGr1Page.Padding = new System.Windows.Forms.Padding(3);
            this._cornerGr1Page.Size = new System.Drawing.Size(942, 411);
            this._cornerGr1Page.TabIndex = 2;
            this._cornerGr1Page.Text = "Углы линии";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this._i2CornerGr1);
            this.groupBox2.Controls.Add(this._inCornerGr1);
            this.groupBox2.Controls.Add(this._i0CornerGr1);
            this.groupBox2.Controls.Add(this._iCornerGr1);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(333, 124);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Углы линии для токовых направленных защит";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "в режиме по I2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "в режиме по In";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(165, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "в режиме по 3I0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = " fin для ступеней I*>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(166, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "fi2 для ступеней I*>";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(165, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "fi0 для ступеней I*>";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "fi1 для ступеней I>";
            // 
            // _i2CornerGr1
            // 
            this._i2CornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i2CornerGr1.Location = new System.Drawing.Point(278, 81);
            this._i2CornerGr1.Name = "_i2CornerGr1";
            this._i2CornerGr1.Size = new System.Drawing.Size(38, 20);
            this._i2CornerGr1.TabIndex = 5;
            this._i2CornerGr1.Tag = "360";
            this._i2CornerGr1.Text = "0";
            this._i2CornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inCornerGr1
            // 
            this._inCornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._inCornerGr1.Location = new System.Drawing.Point(119, 81);
            this._inCornerGr1.Name = "_inCornerGr1";
            this._inCornerGr1.Size = new System.Drawing.Size(38, 20);
            this._inCornerGr1.TabIndex = 4;
            this._inCornerGr1.Tag = "360";
            this._inCornerGr1.Text = "0";
            this._inCornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _i0CornerGr1
            // 
            this._i0CornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i0CornerGr1.Location = new System.Drawing.Point(278, 37);
            this._i0CornerGr1.Name = "_i0CornerGr1";
            this._i0CornerGr1.Size = new System.Drawing.Size(38, 20);
            this._i0CornerGr1.TabIndex = 3;
            this._i0CornerGr1.Tag = "360";
            this._i0CornerGr1.Text = "0";
            this._i0CornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iCornerGr1
            // 
            this._iCornerGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._iCornerGr1.Location = new System.Drawing.Point(119, 37);
            this._iCornerGr1.Name = "_iCornerGr1";
            this._iCornerGr1.Size = new System.Drawing.Size(38, 20);
            this._iCornerGr1.TabIndex = 2;
            this._iCornerGr1.Tag = "360";
            this._iCornerGr1.Text = "0";
            this._iCornerGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _defIGr1Page
            // 
            this._defIGr1Page.Controls.Add(this.panel7);
            this._defIGr1Page.Location = new System.Drawing.Point(4, 22);
            this._defIGr1Page.Name = "_defIGr1Page";
            this._defIGr1Page.Size = new System.Drawing.Size(178, 39);
            this._defIGr1Page.TabIndex = 5;
            this._defIGr1Page.Text = "Защ. I";
            this._defIGr1Page.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.Controls.Add(this.groupBox46);
            this.panel7.Controls.Add(this.groupBox4);
            this.panel7.Controls.Add(this.groupBox10);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(178, 39);
            this.panel7.TabIndex = 7;
            // 
            // groupBox46
            // 
            this.groupBox46.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox46.Controls.Add(this._difensesI56DataGrid);
            this.groupBox46.Location = new System.Drawing.Point(3, 177);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(169, 120);
            this.groupBox46.TabIndex = 4;
            this.groupBox46.TabStop = false;
            this.groupBox46.Text = "Защиты I>5, I>6 максимального тока";
            // 
            // _difensesI56DataGrid
            // 
            this._difensesI56DataGrid.AllowUserToAddRows = false;
            this._difensesI56DataGrid.AllowUserToDeleteRows = false;
            this._difensesI56DataGrid.AllowUserToResizeColumns = false;
            this._difensesI56DataGrid.AllowUserToResizeRows = false;
            this._difensesI56DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI56DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI56DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn53,
            this.dataGridViewTextBoxColumn45,
            this.Column3,
            this.dataGridViewComboBoxColumn54,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewComboBoxColumn55,
            this.dataGridViewComboBoxColumn56,
            this.dataGridViewComboBoxColumn57,
            this.dataGridViewComboBoxColumn58,
            this.dataGridViewComboBoxColumn59,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewComboBoxColumn60,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewComboBoxColumn61,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewCheckBoxColumn11,
            this.dataGridViewCheckBoxColumn15,
            this.dataGridViewCheckBoxColumn19,
            this.dataGridViewComboBoxColumn62,
            this.dataGridViewCheckBoxColumn20,
            this.dataGridViewComboBoxColumn63,
            this.dataGridViewComboBoxColumn64});
            this._difensesI56DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI56DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI56DataGrid.MultiSelect = false;
            this._difensesI56DataGrid.Name = "_difensesI56DataGrid";
            this._difensesI56DataGrid.RowHeadersVisible = false;
            this._difensesI56DataGrid.RowHeadersWidth = 51;
            this._difensesI56DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI56DataGrid.RowTemplate.Height = 24;
            this._difensesI56DataGrid.ShowCellErrors = false;
            this._difensesI56DataGrid.ShowRowErrors = false;
            this._difensesI56DataGrid.Size = new System.Drawing.Size(163, 101);
            this._difensesI56DataGrid.TabIndex = 3;
            this._difensesI56DataGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this._difensesI56DataGrid_CellValueChanged);
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 30;
            // 
            // dataGridViewComboBoxColumn53
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn53.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewComboBoxColumn53.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn53.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn53.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn53.Name = "dataGridViewComboBoxColumn53";
            this.dataGridViewComboBoxColumn53.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn53.Width = 90;
            // 
            // dataGridViewTextBoxColumn45
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn45.DefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridViewTextBoxColumn45.HeaderText = "Iср, Iн ";
            this.dataGridViewTextBoxColumn45.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn45.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn45.Width = 40;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Условие";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.Width = 70;
            // 
            // dataGridViewComboBoxColumn54
            // 
            this.dataGridViewComboBoxColumn54.HeaderText = "Сторона";
            this.dataGridViewComboBoxColumn54.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn54.Name = "dataGridViewComboBoxColumn54";
            this.dataGridViewComboBoxColumn54.Width = 60;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn3.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn3.Width = 40;
            // 
            // dataGridViewTextBoxColumn46
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn46.DefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridViewTextBoxColumn46.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn46.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn46.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn46.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn46.Width = 50;
            // 
            // dataGridViewComboBoxColumn55
            // 
            this.dataGridViewComboBoxColumn55.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn55.HeaderText = "Блк.от неиспр.ТН";
            this.dataGridViewComboBoxColumn55.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn55.Name = "dataGridViewComboBoxColumn55";
            this.dataGridViewComboBoxColumn55.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn55.Visible = false;
            this.dataGridViewComboBoxColumn55.Width = 120;
            // 
            // dataGridViewComboBoxColumn56
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn56.DefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridViewComboBoxColumn56.HeaderText = "Направл.";
            this.dataGridViewComboBoxColumn56.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn56.Name = "dataGridViewComboBoxColumn56";
            this.dataGridViewComboBoxColumn56.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn56.Width = 70;
            // 
            // dataGridViewComboBoxColumn57
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn57.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewComboBoxColumn57.HeaderText = "Недост. направл.";
            this.dataGridViewComboBoxColumn57.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn57.Name = "dataGridViewComboBoxColumn57";
            this.dataGridViewComboBoxColumn57.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn57.Width = 70;
            // 
            // dataGridViewComboBoxColumn58
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn58.DefaultCellStyle = dataGridViewCellStyle22;
            this.dataGridViewComboBoxColumn58.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn58.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn58.Name = "dataGridViewComboBoxColumn58";
            this.dataGridViewComboBoxColumn58.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn58.Width = 80;
            // 
            // dataGridViewComboBoxColumn59
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn59.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewComboBoxColumn59.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn59.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn59.Name = "dataGridViewComboBoxColumn59";
            this.dataGridViewComboBoxColumn59.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn59.Width = 80;
            // 
            // dataGridViewTextBoxColumn47
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn47.DefaultCellStyle = dataGridViewCellStyle24;
            this.dataGridViewTextBoxColumn47.HeaderText = "tср.,мс/ коэф";
            this.dataGridViewTextBoxColumn47.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn47.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn47.Width = 50;
            // 
            // dataGridViewTextBoxColumn48
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn48.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn48.HeaderText = "kз.х.";
            this.dataGridViewTextBoxColumn48.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn48.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn48.Width = 40;
            // 
            // dataGridViewComboBoxColumn60
            // 
            this.dataGridViewComboBoxColumn60.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn60.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn60.Name = "dataGridViewComboBoxColumn60";
            this.dataGridViewComboBoxColumn60.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn60.Width = 120;
            // 
            // dataGridViewTextBoxColumn49
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn49.HeaderText = "Ty, мс";
            this.dataGridViewTextBoxColumn49.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn49.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn49.Width = 50;
            // 
            // dataGridViewComboBoxColumn61
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn61.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewComboBoxColumn61.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn61.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn61.Name = "dataGridViewComboBoxColumn61";
            this.dataGridViewComboBoxColumn61.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn61.Width = 90;
            // 
            // dataGridViewTextBoxColumn50
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn50.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn50.HeaderText = "I2г/I1г, %";
            this.dataGridViewTextBoxColumn50.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn50.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn50.Width = 50;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "Блк. I2г/I1г";
            this.dataGridViewCheckBoxColumn11.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn11.Width = 50;
            // 
            // dataGridViewCheckBoxColumn15
            // 
            this.dataGridViewCheckBoxColumn15.HeaderText = "Перекр. блок.";
            this.dataGridViewCheckBoxColumn15.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn15.Name = "dataGridViewCheckBoxColumn15";
            this.dataGridViewCheckBoxColumn15.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn15.Width = 50;
            // 
            // dataGridViewCheckBoxColumn19
            // 
            this.dataGridViewCheckBoxColumn19.HeaderText = "Ненапр. при ускор.";
            this.dataGridViewCheckBoxColumn19.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn19.Name = "dataGridViewCheckBoxColumn19";
            this.dataGridViewCheckBoxColumn19.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn19.Width = 70;
            // 
            // dataGridViewComboBoxColumn62
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn62.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewComboBoxColumn62.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn62.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn62.Name = "dataGridViewComboBoxColumn62";
            this.dataGridViewComboBoxColumn62.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn62.Width = 125;
            // 
            // dataGridViewCheckBoxColumn20
            // 
            this.dataGridViewCheckBoxColumn20.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn20.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn20.Name = "dataGridViewCheckBoxColumn20";
            this.dataGridViewCheckBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn20.Width = 45;
            // 
            // dataGridViewComboBoxColumn63
            // 
            this.dataGridViewComboBoxColumn63.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn63.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn63.Name = "dataGridViewComboBoxColumn63";
            this.dataGridViewComboBoxColumn63.Width = 70;
            // 
            // dataGridViewComboBoxColumn64
            // 
            this.dataGridViewComboBoxColumn64.HeaderText = "АВР";
            this.dataGridViewComboBoxColumn64.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn64.Name = "dataGridViewComboBoxColumn64";
            this.dataGridViewComboBoxColumn64.Width = 70;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this._difensesI14DataGrid);
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(172, 168);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Защиты I>1 - I>4 максимального тока";
            // 
            // _difensesI14DataGrid
            // 
            this._difensesI14DataGrid.AllowUserToAddRows = false;
            this._difensesI14DataGrid.AllowUserToDeleteRows = false;
            this._difensesI14DataGrid.AllowUserToResizeColumns = false;
            this._difensesI14DataGrid.AllowUserToResizeRows = false;
            this._difensesI14DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI14DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI14DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn5,
            this.dataGridViewTextBoxColumn8,
            this.Column5,
            this.inIm,
            this.dataGridViewCheckBoxColumn6,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn6,
            this.dataGridViewComboBoxColumn7,
            this.dataGridViewComboBoxColumn8,
            this.dataGridViewComboBoxColumn9,
            this.dataGridViewComboBoxColumn10,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewComboBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewCheckBoxColumn7,
            this.dataGridViewCheckBoxColumn8,
            this.dataGridViewCheckBoxColumn9,
            this.dataGridViewComboBoxColumn13,
            this.dataGridViewCheckBoxColumn10,
            this.dataGridViewComboBoxColumn14,
            this.avrIb});
            this._difensesI14DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI14DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI14DataGrid.MultiSelect = false;
            this._difensesI14DataGrid.Name = "_difensesI14DataGrid";
            this._difensesI14DataGrid.RowHeadersVisible = false;
            this._difensesI14DataGrid.RowHeadersWidth = 51;
            this._difensesI14DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI14DataGrid.RowTemplate.Height = 24;
            this._difensesI14DataGrid.ShowCellErrors = false;
            this._difensesI14DataGrid.ShowRowErrors = false;
            this._difensesI14DataGrid.Size = new System.Drawing.Size(166, 149);
            this._difensesI14DataGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewTextBoxColumn7.Frozen = true;
            this.dataGridViewTextBoxColumn7.HeaderText = "";
            this.dataGridViewTextBoxColumn7.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 30;
            // 
            // dataGridViewComboBoxColumn5
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn5.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn5.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            this.dataGridViewComboBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn8.HeaderText = "Iср, Iн ";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 40;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.Visible = false;
            this.Column5.Width = 125;
            // 
            // inIm
            // 
            this.inIm.HeaderText = "Сторона";
            this.inIm.MinimumWidth = 6;
            this.inIm.Name = "inIm";
            this.inIm.Width = 60;
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn6.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn6.Width = 40;
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn9.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn9.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "Блк.от неиспр.ТН";
            this.dataGridViewComboBoxColumn6.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            this.dataGridViewComboBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn6.Visible = false;
            this.dataGridViewComboBoxColumn6.Width = 120;
            // 
            // dataGridViewComboBoxColumn7
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn7.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewComboBoxColumn7.HeaderText = "Направл.";
            this.dataGridViewComboBoxColumn7.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            this.dataGridViewComboBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn7.Width = 70;
            // 
            // dataGridViewComboBoxColumn8
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn8.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewComboBoxColumn8.HeaderText = "Недост. направл.";
            this.dataGridViewComboBoxColumn8.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            this.dataGridViewComboBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn8.Width = 70;
            // 
            // dataGridViewComboBoxColumn9
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn9.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewComboBoxColumn9.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn9.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            this.dataGridViewComboBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn9.Width = 80;
            // 
            // dataGridViewComboBoxColumn10
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn10.DefaultCellStyle = dataGridViewCellStyle37;
            this.dataGridViewComboBoxColumn10.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn10.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            this.dataGridViewComboBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn10.Width = 80;
            // 
            // dataGridViewTextBoxColumn10
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn10.DefaultCellStyle = dataGridViewCellStyle38;
            this.dataGridViewTextBoxColumn10.HeaderText = "tср.,мс/ коэф";
            this.dataGridViewTextBoxColumn10.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn11.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn11.HeaderText = "kз.х.";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 40;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn11.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            this.dataGridViewComboBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn11.Width = 120;
            // 
            // dataGridViewTextBoxColumn12
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn12.DefaultCellStyle = dataGridViewCellStyle40;
            this.dataGridViewTextBoxColumn12.HeaderText = "Ty, мс";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 50;
            // 
            // dataGridViewComboBoxColumn12
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn12.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewComboBoxColumn12.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn12.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            this.dataGridViewComboBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn12.Width = 90;
            // 
            // dataGridViewTextBoxColumn13
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle42;
            this.dataGridViewTextBoxColumn13.HeaderText = "I2г/I1г, %";
            this.dataGridViewTextBoxColumn13.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "Блк. I2г/I1г";
            this.dataGridViewCheckBoxColumn7.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn7.Width = 50;
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "Перекр. блок.";
            this.dataGridViewCheckBoxColumn8.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn8.Width = 50;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "Ненапр. при ускор.";
            this.dataGridViewCheckBoxColumn9.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn9.Width = 70;
            // 
            // dataGridViewComboBoxColumn13
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn13.DefaultCellStyle = dataGridViewCellStyle43;
            this.dataGridViewComboBoxColumn13.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn13.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            this.dataGridViewComboBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn13.Width = 125;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn10.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn10.Width = 45;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn14.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            this.dataGridViewComboBoxColumn14.Width = 70;
            // 
            // avrIb
            // 
            this.avrIb.HeaderText = "АВР";
            this.avrIb.MinimumWidth = 6;
            this.avrIb.Name = "avrIb";
            this.avrIb.Width = 70;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this._difensesI7DataGrid);
            this.groupBox10.Location = new System.Drawing.Point(3, 300);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(166, 65);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Защита I< минимального тока";
            // 
            // _difensesI7DataGrid
            // 
            this._difensesI7DataGrid.AllowUserToAddRows = false;
            this._difensesI7DataGrid.AllowUserToDeleteRows = false;
            this._difensesI7DataGrid.AllowUserToResizeColumns = false;
            this._difensesI7DataGrid.AllowUserToResizeRows = false;
            this._difensesI7DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI7DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI7DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.Column6,
            this.inImm,
            this.dataGridViewCheckBoxColumn12,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewComboBoxColumn32,
            this.dataGridViewComboBoxColumn33,
            this.dataGridViewComboBoxColumn34,
            this.dataGridViewComboBoxColumn35,
            this.dataGridViewComboBoxColumn36,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewComboBoxColumn37,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewComboBoxColumn38,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewComboBoxColumn39,
            this.dataGridViewComboBoxColumn40,
            this.dataGridViewCheckBoxColumn13,
            this.dataGridViewComboBoxColumn41,
            this.dataGridViewCheckBoxColumn14,
            this.dataGridViewComboBoxColumn42,
            this.avrImm});
            this._difensesI7DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI7DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI7DataGrid.MultiSelect = false;
            this._difensesI7DataGrid.Name = "_difensesI7DataGrid";
            this._difensesI7DataGrid.RowHeadersVisible = false;
            this._difensesI7DataGrid.RowHeadersWidth = 51;
            this._difensesI7DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI7DataGrid.RowTemplate.Height = 24;
            this._difensesI7DataGrid.ShowCellErrors = false;
            this._difensesI7DataGrid.ShowRowErrors = false;
            this._difensesI7DataGrid.Size = new System.Drawing.Size(160, 46);
            this._difensesI7DataGrid.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn14
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle44;
            this.dataGridViewTextBoxColumn14.Frozen = true;
            this.dataGridViewTextBoxColumn14.HeaderText = "";
            this.dataGridViewTextBoxColumn14.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 30;
            // 
            // dataGridViewComboBoxColumn31
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn31.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewComboBoxColumn31.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn31.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn31.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn31.Name = "dataGridViewComboBoxColumn31";
            this.dataGridViewComboBoxColumn31.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn31.Width = 125;
            // 
            // dataGridViewTextBoxColumn32
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn32.DefaultCellStyle = dataGridViewCellStyle46;
            this.dataGridViewTextBoxColumn32.HeaderText = "Iср, Iн ";
            this.dataGridViewTextBoxColumn32.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn32.Width = 65;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Column6";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.Visible = false;
            this.Column6.Width = 125;
            // 
            // inImm
            // 
            this.inImm.HeaderText = "Сторона";
            this.inImm.MinimumWidth = 6;
            this.inImm.Name = "inImm";
            this.inImm.Width = 60;
            // 
            // dataGridViewCheckBoxColumn12
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle47.NullValue = false;
            this.dataGridViewCheckBoxColumn12.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewCheckBoxColumn12.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn12.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn12.Name = "dataGridViewCheckBoxColumn12";
            this.dataGridViewCheckBoxColumn12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn12.Visible = false;
            this.dataGridViewCheckBoxColumn12.Width = 80;
            // 
            // dataGridViewTextBoxColumn33
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle48;
            this.dataGridViewTextBoxColumn33.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn33.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn33.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn33.Visible = false;
            this.dataGridViewTextBoxColumn33.Width = 80;
            // 
            // dataGridViewComboBoxColumn32
            // 
            this.dataGridViewComboBoxColumn32.HeaderText = "Блок.от неиспр.ТН";
            this.dataGridViewComboBoxColumn32.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn32.Name = "dataGridViewComboBoxColumn32";
            this.dataGridViewComboBoxColumn32.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn32.Visible = false;
            this.dataGridViewComboBoxColumn32.Width = 125;
            // 
            // dataGridViewComboBoxColumn33
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn33.DefaultCellStyle = dataGridViewCellStyle49;
            this.dataGridViewComboBoxColumn33.HeaderText = "Направление";
            this.dataGridViewComboBoxColumn33.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn33.Name = "dataGridViewComboBoxColumn33";
            this.dataGridViewComboBoxColumn33.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn33.Visible = false;
            this.dataGridViewComboBoxColumn33.Width = 85;
            // 
            // dataGridViewComboBoxColumn34
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn34.DefaultCellStyle = dataGridViewCellStyle50;
            this.dataGridViewComboBoxColumn34.HeaderText = "Недост.напр";
            this.dataGridViewComboBoxColumn34.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn34.Name = "dataGridViewComboBoxColumn34";
            this.dataGridViewComboBoxColumn34.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn34.Visible = false;
            this.dataGridViewComboBoxColumn34.Width = 80;
            // 
            // dataGridViewComboBoxColumn35
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn35.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewComboBoxColumn35.HeaderText = "Логика";
            this.dataGridViewComboBoxColumn35.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn35.Name = "dataGridViewComboBoxColumn35";
            this.dataGridViewComboBoxColumn35.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn35.Width = 90;
            // 
            // dataGridViewComboBoxColumn36
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn36.DefaultCellStyle = dataGridViewCellStyle52;
            this.dataGridViewComboBoxColumn36.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn36.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn36.Name = "dataGridViewComboBoxColumn36";
            this.dataGridViewComboBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn36.Visible = false;
            this.dataGridViewComboBoxColumn36.Width = 125;
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn34.HeaderText = "t, мс";
            this.dataGridViewTextBoxColumn34.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn34.Width = 40;
            // 
            // dataGridViewTextBoxColumn35
            // 
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn35.DefaultCellStyle = dataGridViewCellStyle54;
            this.dataGridViewTextBoxColumn35.HeaderText = "k завис. хар-ки";
            this.dataGridViewTextBoxColumn35.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn35.Visible = false;
            this.dataGridViewTextBoxColumn35.Width = 110;
            // 
            // dataGridViewComboBoxColumn37
            // 
            this.dataGridViewComboBoxColumn37.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn37.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn37.Name = "dataGridViewComboBoxColumn37";
            this.dataGridViewComboBoxColumn37.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn37.Visible = false;
            this.dataGridViewComboBoxColumn37.Width = 125;
            // 
            // dataGridViewTextBoxColumn36
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn36.DefaultCellStyle = dataGridViewCellStyle55;
            this.dataGridViewTextBoxColumn36.HeaderText = "Ty, мс";
            this.dataGridViewTextBoxColumn36.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn36.Visible = false;
            this.dataGridViewTextBoxColumn36.Width = 65;
            // 
            // dataGridViewComboBoxColumn38
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn38.DefaultCellStyle = dataGridViewCellStyle56;
            this.dataGridViewComboBoxColumn38.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn38.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn38.Name = "dataGridViewComboBoxColumn38";
            this.dataGridViewComboBoxColumn38.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn38.Width = 90;
            // 
            // dataGridViewTextBoxColumn37
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn37.HeaderText = "I2г/I1г, %";
            this.dataGridViewTextBoxColumn37.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn37.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn37.Visible = false;
            this.dataGridViewTextBoxColumn37.Width = 65;
            // 
            // dataGridViewComboBoxColumn39
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle58.NullValue = false;
            this.dataGridViewComboBoxColumn39.DefaultCellStyle = dataGridViewCellStyle58;
            this.dataGridViewComboBoxColumn39.HeaderText = "Блок. по I2г/I1г";
            this.dataGridViewComboBoxColumn39.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn39.Name = "dataGridViewComboBoxColumn39";
            this.dataGridViewComboBoxColumn39.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn39.Visible = false;
            this.dataGridViewComboBoxColumn39.Width = 90;
            // 
            // dataGridViewComboBoxColumn40
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle59.NullValue = false;
            this.dataGridViewComboBoxColumn40.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewComboBoxColumn40.HeaderText = "Перекр. Блок.";
            this.dataGridViewComboBoxColumn40.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn40.Name = "dataGridViewComboBoxColumn40";
            this.dataGridViewComboBoxColumn40.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn40.Visible = false;
            this.dataGridViewComboBoxColumn40.Width = 90;
            // 
            // dataGridViewCheckBoxColumn13
            // 
            this.dataGridViewCheckBoxColumn13.HeaderText = "Column6";
            this.dataGridViewCheckBoxColumn13.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn13.Name = "dataGridViewCheckBoxColumn13";
            this.dataGridViewCheckBoxColumn13.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn13.Visible = false;
            this.dataGridViewCheckBoxColumn13.Width = 125;
            // 
            // dataGridViewComboBoxColumn41
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn41.DefaultCellStyle = dataGridViewCellStyle60;
            this.dataGridViewComboBoxColumn41.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn41.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn41.Name = "dataGridViewComboBoxColumn41";
            this.dataGridViewComboBoxColumn41.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn41.Width = 110;
            // 
            // dataGridViewCheckBoxColumn14
            // 
            this.dataGridViewCheckBoxColumn14.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn14.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn14.Name = "dataGridViewCheckBoxColumn14";
            this.dataGridViewCheckBoxColumn14.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn14.Width = 50;
            // 
            // dataGridViewComboBoxColumn42
            // 
            this.dataGridViewComboBoxColumn42.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn42.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn42.Name = "dataGridViewComboBoxColumn42";
            this.dataGridViewComboBoxColumn42.Width = 70;
            // 
            // avrImm
            // 
            this.avrImm.HeaderText = "АВР";
            this.avrImm.MinimumWidth = 6;
            this.avrImm.Name = "avrImm";
            this.avrImm.Width = 70;
            // 
            // _defIStarGr1
            // 
            this._defIStarGr1.Controls.Add(this.panel8);
            this._defIStarGr1.Location = new System.Drawing.Point(4, 22);
            this._defIStarGr1.Name = "_defIStarGr1";
            this._defIStarGr1.Size = new System.Drawing.Size(942, 411);
            this._defIStarGr1.TabIndex = 6;
            this._defIStarGr1.Text = "Защ. I*";
            this._defIStarGr1.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.Controls.Add(this.groupBox17);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(942, 411);
            this.panel8.TabIndex = 5;
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this._difensesI0DataGrid);
            this.groupBox17.Location = new System.Drawing.Point(0, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(939, 265);
            this.groupBox17.TabIndex = 4;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Защиты I*";
            // 
            // _difensesI0DataGrid
            // 
            this._difensesI0DataGrid.AllowUserToAddRows = false;
            this._difensesI0DataGrid.AllowUserToDeleteRows = false;
            this._difensesI0DataGrid.AllowUserToResizeColumns = false;
            this._difensesI0DataGrid.AllowUserToResizeRows = false;
            this._difensesI0DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI0DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI0DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewComboBoxColumn43,
            this.dataGridViewTextBoxColumn39,
            this.inI,
            this.dataGridViewCheckBoxColumn16,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewComboBoxColumn44,
            this.dataGridViewComboBoxColumn45,
            this._chooseCBColumn,
            this._iComboBoxCell,
            this.dataGridViewComboBoxColumn47,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewComboBoxColumn48,
            this.dataGridViewComboBoxColumn49,
            this.dataGridViewComboBoxColumn50,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewCheckBoxColumn17,
            this.dataGridViewCheckBoxColumn18,
            this.dataGridViewComboBoxColumn51,
            this._avrI});
            this._difensesI0DataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesI0DataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesI0DataGrid.MultiSelect = false;
            this._difensesI0DataGrid.Name = "_difensesI0DataGrid";
            this._difensesI0DataGrid.RowHeadersVisible = false;
            this._difensesI0DataGrid.RowHeadersWidth = 51;
            this._difensesI0DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI0DataGrid.RowTemplate.Height = 24;
            this._difensesI0DataGrid.ShowCellErrors = false;
            this._difensesI0DataGrid.ShowRowErrors = false;
            this._difensesI0DataGrid.Size = new System.Drawing.Size(933, 246);
            this._difensesI0DataGrid.TabIndex = 3;
            this._difensesI0DataGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this._difensesI0DataGrid_CellValueChanged);
            this._difensesI0DataGrid.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._difensesI0DataGrid_DataError);
            // 
            // dataGridViewTextBoxColumn38
            // 
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn38.DefaultCellStyle = dataGridViewCellStyle61;
            this.dataGridViewTextBoxColumn38.Frozen = true;
            this.dataGridViewTextBoxColumn38.HeaderText = "";
            this.dataGridViewTextBoxColumn38.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            this.dataGridViewTextBoxColumn38.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn38.Width = 30;
            // 
            // dataGridViewComboBoxColumn43
            // 
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn43.DefaultCellStyle = dataGridViewCellStyle62;
            this.dataGridViewComboBoxColumn43.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn43.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn43.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn43.Name = "dataGridViewComboBoxColumn43";
            this.dataGridViewComboBoxColumn43.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn43.Width = 125;
            // 
            // dataGridViewTextBoxColumn39
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn39.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn39.HeaderText = "Iср, Iн тт";
            this.dataGridViewTextBoxColumn39.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn39.Width = 40;
            // 
            // inI
            // 
            this.inI.HeaderText = "Сторона";
            this.inI.MinimumWidth = 6;
            this.inI.Name = "inI";
            this.inI.Width = 60;
            // 
            // dataGridViewCheckBoxColumn16
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle64.NullValue = false;
            this.dataGridViewCheckBoxColumn16.DefaultCellStyle = dataGridViewCellStyle64;
            this.dataGridViewCheckBoxColumn16.HeaderText = "Пуск по U";
            this.dataGridViewCheckBoxColumn16.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn16.Name = "dataGridViewCheckBoxColumn16";
            this.dataGridViewCheckBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn16.Width = 50;
            // 
            // dataGridViewTextBoxColumn40
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn40.DefaultCellStyle = dataGridViewCellStyle65;
            this.dataGridViewTextBoxColumn40.HeaderText = "Uпуск, В";
            this.dataGridViewTextBoxColumn40.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn40.Width = 50;
            // 
            // dataGridViewComboBoxColumn44
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn44.DefaultCellStyle = dataGridViewCellStyle66;
            this.dataGridViewComboBoxColumn44.HeaderText = "Направл.";
            this.dataGridViewComboBoxColumn44.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn44.Name = "dataGridViewComboBoxColumn44";
            this.dataGridViewComboBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn44.Width = 70;
            // 
            // dataGridViewComboBoxColumn45
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn45.DefaultCellStyle = dataGridViewCellStyle67;
            this.dataGridViewComboBoxColumn45.HeaderText = "Недост. направл.";
            this.dataGridViewComboBoxColumn45.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn45.Name = "dataGridViewComboBoxColumn45";
            this.dataGridViewComboBoxColumn45.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn45.Width = 70;
            // 
            // _chooseCBColumn
            // 
            this._chooseCBColumn.HeaderText = "Выбор";
            this._chooseCBColumn.MinimumWidth = 6;
            this._chooseCBColumn.Name = "_chooseCBColumn";
            this._chooseCBColumn.Visible = false;
            this._chooseCBColumn.Width = 125;
            // 
            // _iComboBoxCell
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iComboBoxCell.DefaultCellStyle = dataGridViewCellStyle68;
            this._iComboBoxCell.HeaderText = "Параметр";
            this._iComboBoxCell.MinimumWidth = 6;
            this._iComboBoxCell.Name = "_iComboBoxCell";
            this._iComboBoxCell.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iComboBoxCell.Width = 90;
            // 
            // dataGridViewComboBoxColumn47
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn47.DefaultCellStyle = dataGridViewCellStyle69;
            this.dataGridViewComboBoxColumn47.HeaderText = "Характ-ка";
            this.dataGridViewComboBoxColumn47.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn47.Name = "dataGridViewComboBoxColumn47";
            this.dataGridViewComboBoxColumn47.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn47.Width = 80;
            // 
            // dataGridViewTextBoxColumn41
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle70;
            this.dataGridViewTextBoxColumn41.HeaderText = "tср.,мс/ коэф";
            this.dataGridViewTextBoxColumn41.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn41.Width = 50;
            // 
            // dataGridViewTextBoxColumn42
            // 
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn42.DefaultCellStyle = dataGridViewCellStyle71;
            this.dataGridViewTextBoxColumn42.HeaderText = "kз.х.";
            this.dataGridViewTextBoxColumn42.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn42.Width = 40;
            // 
            // dataGridViewComboBoxColumn48
            // 
            this.dataGridViewComboBoxColumn48.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn48.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn48.Name = "dataGridViewComboBoxColumn48";
            this.dataGridViewComboBoxColumn48.Width = 107;
            // 
            // dataGridViewComboBoxColumn49
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn49.DefaultCellStyle = dataGridViewCellStyle72;
            this.dataGridViewComboBoxColumn49.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn49.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn49.Name = "dataGridViewComboBoxColumn49";
            this.dataGridViewComboBoxColumn49.Width = 110;
            // 
            // dataGridViewComboBoxColumn50
            // 
            this.dataGridViewComboBoxColumn50.HeaderText = "Вход ускорения";
            this.dataGridViewComboBoxColumn50.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn50.Name = "dataGridViewComboBoxColumn50";
            this.dataGridViewComboBoxColumn50.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn50.Width = 120;
            // 
            // dataGridViewTextBoxColumn43
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn43.DefaultCellStyle = dataGridViewCellStyle73;
            this.dataGridViewTextBoxColumn43.HeaderText = "ty, мс";
            this.dataGridViewTextBoxColumn43.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.Width = 50;
            // 
            // dataGridViewCheckBoxColumn17
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle74.NullValue = false;
            this.dataGridViewCheckBoxColumn17.DefaultCellStyle = dataGridViewCellStyle74;
            this.dataGridViewCheckBoxColumn17.FillWeight = 80F;
            this.dataGridViewCheckBoxColumn17.HeaderText = "Ненаправл. при ускор.";
            this.dataGridViewCheckBoxColumn17.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn17.Name = "dataGridViewCheckBoxColumn17";
            this.dataGridViewCheckBoxColumn17.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn17.Width = 70;
            // 
            // dataGridViewCheckBoxColumn18
            // 
            this.dataGridViewCheckBoxColumn18.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn18.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn18.Name = "dataGridViewCheckBoxColumn18";
            this.dataGridViewCheckBoxColumn18.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCheckBoxColumn18.Width = 45;
            // 
            // dataGridViewComboBoxColumn51
            // 
            this.dataGridViewComboBoxColumn51.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn51.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn51.Name = "dataGridViewComboBoxColumn51";
            this.dataGridViewComboBoxColumn51.Width = 70;
            // 
            // _avrI
            // 
            this._avrI.HeaderText = "АВР";
            this._avrI.MinimumWidth = 6;
            this._avrI.Name = "_avrI";
            this._avrI.Width = 70;
            // 
            // _defI2I1Gr1
            // 
            this._defI2I1Gr1.BackColor = System.Drawing.SystemColors.Control;
            this._defI2I1Gr1.Controls.Add(this.groupBox29);
            this._defI2I1Gr1.Location = new System.Drawing.Point(4, 22);
            this._defI2I1Gr1.Name = "_defI2I1Gr1";
            this._defI2I1Gr1.Size = new System.Drawing.Size(178, 39);
            this._defI2I1Gr1.TabIndex = 7;
            this._defI2I1Gr1.Text = "I2I1";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this._urovI2I1CheckBox);
            this.groupBox29.Controls.Add(this._sideI2I1ComboBox);
            this.groupBox29.Controls.Add(this.i2i1AVR);
            this.groupBox29.Controls.Add(this.i2i1APV1);
            this.groupBox29.Controls.Add(this.I2I1OscComboGr1);
            this.groupBox29.Controls.Add(this.I2I1tcpTBGr1);
            this.groupBox29.Controls.Add(this.label161);
            this.groupBox29.Controls.Add(this.I2I1BlockingComboGr1);
            this.groupBox29.Controls.Add(this.label11);
            this.groupBox29.Controls.Add(this.label12);
            this.groupBox29.Controls.Add(this.label13);
            this.groupBox29.Controls.Add(this.label25);
            this.groupBox29.Controls.Add(this.label47);
            this.groupBox29.Controls.Add(this.label50);
            this.groupBox29.Controls.Add(this.I2I1TBGr1);
            this.groupBox29.Controls.Add(this.I2I1ModeComboGr1);
            this.groupBox29.Controls.Add(this.label53);
            this.groupBox29.Location = new System.Drawing.Point(3, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(318, 199);
            this.groupBox29.TabIndex = 12;
            this.groupBox29.TabStop = false;
            // 
            // _urovI2I1CheckBox
            // 
            this._urovI2I1CheckBox.AutoSize = true;
            this._urovI2I1CheckBox.Location = new System.Drawing.Point(125, 118);
            this._urovI2I1CheckBox.Name = "_urovI2I1CheckBox";
            this._urovI2I1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._urovI2I1CheckBox.TabIndex = 37;
            this._urovI2I1CheckBox.UseVisualStyleBackColor = true;
            // 
            // _sideI2I1ComboBox
            // 
            this._sideI2I1ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sideI2I1ComboBox.FormattingEnabled = true;
            this._sideI2I1ComboBox.Location = new System.Drawing.Point(250, 53);
            this._sideI2I1ComboBox.Name = "_sideI2I1ComboBox";
            this._sideI2I1ComboBox.Size = new System.Drawing.Size(46, 21);
            this._sideI2I1ComboBox.TabIndex = 36;
            // 
            // i2i1AVR
            // 
            this.i2i1AVR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.i2i1AVR.FormattingEnabled = true;
            this.i2i1AVR.Location = new System.Drawing.Point(125, 159);
            this.i2i1AVR.Name = "i2i1AVR";
            this.i2i1AVR.Size = new System.Drawing.Size(119, 21);
            this.i2i1AVR.TabIndex = 34;
            // 
            // i2i1APV1
            // 
            this.i2i1APV1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.i2i1APV1.FormattingEnabled = true;
            this.i2i1APV1.Location = new System.Drawing.Point(125, 137);
            this.i2i1APV1.Name = "i2i1APV1";
            this.i2i1APV1.Size = new System.Drawing.Size(119, 21);
            this.i2i1APV1.TabIndex = 34;
            // 
            // I2I1OscComboGr1
            // 
            this.I2I1OscComboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1OscComboGr1.FormattingEnabled = true;
            this.I2I1OscComboGr1.Location = new System.Drawing.Point(125, 94);
            this.I2I1OscComboGr1.Name = "I2I1OscComboGr1";
            this.I2I1OscComboGr1.Size = new System.Drawing.Size(119, 21);
            this.I2I1OscComboGr1.TabIndex = 34;
            // 
            // I2I1tcpTBGr1
            // 
            this.I2I1tcpTBGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1tcpTBGr1.Location = new System.Drawing.Point(125, 74);
            this.I2I1tcpTBGr1.Name = "I2I1tcpTBGr1";
            this.I2I1tcpTBGr1.Size = new System.Drawing.Size(119, 20);
            this.I2I1tcpTBGr1.TabIndex = 33;
            this.I2I1tcpTBGr1.Tag = "1500";
            this.I2I1tcpTBGr1.Text = "20";
            this.I2I1tcpTBGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(6, 164);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(28, 13);
            this.label161.TabIndex = 30;
            this.label161.Text = "АВР";
            // 
            // I2I1BlockingComboGr1
            // 
            this.I2I1BlockingComboGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.I2I1BlockingComboGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.I2I1BlockingComboGr1.FormattingEnabled = true;
            this.I2I1BlockingComboGr1.Location = new System.Drawing.Point(125, 33);
            this.I2I1BlockingComboGr1.Name = "I2I1BlockingComboGr1";
            this.I2I1BlockingComboGr1.Size = new System.Drawing.Size(119, 21);
            this.I2I1BlockingComboGr1.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "АПВ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "УРОВ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "ОСЦ.";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 76);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(42, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "tcp, мс";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 56);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(44, 13);
            this.label47.TabIndex = 26;
            this.label47.Text = "I2/I1, %";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 36);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(68, 13);
            this.label50.TabIndex = 25;
            this.label50.Text = "Блокировка";
            // 
            // I2I1TBGr1
            // 
            this.I2I1TBGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1TBGr1.Location = new System.Drawing.Point(125, 54);
            this.I2I1TBGr1.Name = "I2I1TBGr1";
            this.I2I1TBGr1.Size = new System.Drawing.Size(119, 20);
            this.I2I1TBGr1.TabIndex = 24;
            this.I2I1TBGr1.Tag = "1500";
            this.I2I1TBGr1.Text = "0";
            this.I2I1TBGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // I2I1ModeComboGr1
            // 
            this.I2I1ModeComboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1ModeComboGr1.FormattingEnabled = true;
            this.I2I1ModeComboGr1.Location = new System.Drawing.Point(125, 13);
            this.I2I1ModeComboGr1.Name = "I2I1ModeComboGr1";
            this.I2I1ModeComboGr1.Size = new System.Drawing.Size(119, 21);
            this.I2I1ModeComboGr1.TabIndex = 23;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(42, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Режим";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox44);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(178, 39);
            this.tabPage2.TabIndex = 24;
            this.tabPage2.Text = "Пуск дуг. защ.";
            // 
            // groupBox44
            // 
            this.groupBox44.Controls.Add(this.OscArc);
            this.groupBox44.Controls.Add(this.SideArcComboBox);
            this.groupBox44.Controls.Add(this.StartBlockArcComboBox);
            this.groupBox44.Controls.Add(this.label167);
            this.groupBox44.Controls.Add(this.label169);
            this.groupBox44.Controls.Add(this.label170);
            this.groupBox44.Controls.Add(this.IcpArcTextBox);
            this.groupBox44.Controls.Add(this.StartArcModeComboBox);
            this.groupBox44.Controls.Add(this.label171);
            this.groupBox44.Location = new System.Drawing.Point(3, 3);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(318, 106);
            this.groupBox44.TabIndex = 37;
            this.groupBox44.TabStop = false;
            // 
            // OscArc
            // 
            this.OscArc.AutoSize = true;
            this.OscArc.Location = new System.Drawing.Point(125, 77);
            this.OscArc.Name = "OscArc";
            this.OscArc.Size = new System.Drawing.Size(15, 14);
            this.OscArc.TabIndex = 37;
            this.OscArc.UseVisualStyleBackColor = true;
            // 
            // SideArcComboBox
            // 
            this.SideArcComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SideArcComboBox.FormattingEnabled = true;
            this.SideArcComboBox.Location = new System.Drawing.Point(250, 53);
            this.SideArcComboBox.Name = "SideArcComboBox";
            this.SideArcComboBox.Size = new System.Drawing.Size(46, 21);
            this.SideArcComboBox.TabIndex = 36;
            // 
            // StartBlockArcComboBox
            // 
            this.StartBlockArcComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.StartBlockArcComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.StartBlockArcComboBox.FormattingEnabled = true;
            this.StartBlockArcComboBox.Location = new System.Drawing.Point(125, 33);
            this.StartBlockArcComboBox.Name = "StartBlockArcComboBox";
            this.StartBlockArcComboBox.Size = new System.Drawing.Size(119, 21);
            this.StartBlockArcComboBox.TabIndex = 32;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(6, 78);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(76, 13);
            this.label167.TabIndex = 28;
            this.label167.Text = "Осциллограф";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(6, 56);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(37, 13);
            this.label169.TabIndex = 26;
            this.label169.Text = "Iср, Iн";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(6, 36);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(68, 13);
            this.label170.TabIndex = 25;
            this.label170.Text = "Блокировка";
            // 
            // IcpArcTextBox
            // 
            this.IcpArcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IcpArcTextBox.Location = new System.Drawing.Point(125, 54);
            this.IcpArcTextBox.Name = "IcpArcTextBox";
            this.IcpArcTextBox.Size = new System.Drawing.Size(119, 20);
            this.IcpArcTextBox.TabIndex = 24;
            this.IcpArcTextBox.Tag = "1500";
            this.IcpArcTextBox.Text = "0";
            this.IcpArcTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // StartArcModeComboBox
            // 
            this.StartArcModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartArcModeComboBox.FormattingEnabled = true;
            this.StartArcModeComboBox.Location = new System.Drawing.Point(125, 13);
            this.StartArcModeComboBox.Name = "StartArcModeComboBox";
            this.StartArcModeComboBox.Size = new System.Drawing.Size(119, 21);
            this.StartArcModeComboBox.TabIndex = 23;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(6, 16);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(42, 13);
            this.label171.TabIndex = 0;
            this.label171.Text = "Режим";
            // 
            // _defUGr1
            // 
            this._defUGr1.Controls.Add(this.panel4);
            this._defUGr1.Location = new System.Drawing.Point(4, 25);
            this._defUGr1.Name = "_defUGr1";
            this._defUGr1.Size = new System.Drawing.Size(956, 443);
            this._defUGr1.TabIndex = 6;
            this._defUGr1.Text = "Защ. U";
            this._defUGr1.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.groupBox22);
            this.panel4.Controls.Add(this.groupBox23);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(956, 443);
            this.panel4.TabIndex = 6;
            // 
            // groupBox22
            // 
            this.groupBox22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox22.Controls.Add(this._difensesUBDataGrid);
            this.groupBox22.Location = new System.Drawing.Point(3, 3);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(950, 169);
            this.groupBox22.TabIndex = 4;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Защиты U>";
            // 
            // _difensesUBDataGrid
            // 
            this._difensesUBDataGrid.AllowUserToAddRows = false;
            this._difensesUBDataGrid.AllowUserToDeleteRows = false;
            this._difensesUBDataGrid.AllowUserToResizeColumns = false;
            this._difensesUBDataGrid.AllowUserToResizeRows = false;
            this._difensesUBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._uBStageColumn,
            this._uBModesColumn,
            this._uBTypeColumn,
            this.Column1,
            this.sideUb,
            this._uBUsrColumn,
            this._uBTsrColumn,
            this._uBTvzColumn,
            this._uBUvzColumn,
            this._uBUvzYNColumn,
            this.Column7,
            this.Column4,
            this._uBBlockingColumn,
            this._uBOscColumn,
            this._uBAPVRetColumn,
            this._uBUROVColumn,
            this._uBAPVColumn1,
            this._uBAPVColumn,
            this._avrUmax});
            this._difensesUBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesUBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesUBDataGrid.MultiSelect = false;
            this._difensesUBDataGrid.Name = "_difensesUBDataGrid";
            this._difensesUBDataGrid.RowHeadersVisible = false;
            this._difensesUBDataGrid.RowHeadersWidth = 51;
            this._difensesUBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUBDataGrid.RowTemplate.Height = 24;
            this._difensesUBDataGrid.ShowCellErrors = false;
            this._difensesUBDataGrid.ShowRowErrors = false;
            this._difensesUBDataGrid.Size = new System.Drawing.Size(944, 150);
            this._difensesUBDataGrid.TabIndex = 3;
            // 
            // _uBStageColumn
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._uBStageColumn.DefaultCellStyle = dataGridViewCellStyle75;
            this._uBStageColumn.HeaderText = "";
            this._uBStageColumn.MinimumWidth = 6;
            this._uBStageColumn.Name = "_uBStageColumn";
            this._uBStageColumn.ReadOnly = true;
            this._uBStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBStageColumn.Width = 40;
            // 
            // _uBModesColumn
            // 
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBModesColumn.DefaultCellStyle = dataGridViewCellStyle76;
            this._uBModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._uBModesColumn.HeaderText = "Режим";
            this._uBModesColumn.MinimumWidth = 6;
            this._uBModesColumn.Name = "_uBModesColumn";
            this._uBModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBModesColumn.Width = 125;
            // 
            // _uBTypeColumn
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTypeColumn.DefaultCellStyle = dataGridViewCellStyle77;
            this._uBTypeColumn.HeaderText = "Тип";
            this._uBTypeColumn.MinimumWidth = 6;
            this._uBTypeColumn.Name = "_uBTypeColumn";
            this._uBTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTypeColumn.Width = 90;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            this.Column1.Width = 125;
            // 
            // sideUb
            // 
            this.sideUb.HeaderText = "Группа";
            this.sideUb.MinimumWidth = 6;
            this.sideUb.Name = "sideUb";
            this.sideUb.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sideUb.Width = 60;
            // 
            // _uBUsrColumn
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUsrColumn.DefaultCellStyle = dataGridViewCellStyle78;
            this._uBUsrColumn.HeaderText = "Uср, В";
            this._uBUsrColumn.MinimumWidth = 6;
            this._uBUsrColumn.Name = "_uBUsrColumn";
            this._uBUsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUsrColumn.Width = 50;
            // 
            // _uBTsrColumn
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTsrColumn.DefaultCellStyle = dataGridViewCellStyle79;
            this._uBTsrColumn.HeaderText = "tср, мс";
            this._uBTsrColumn.MinimumWidth = 6;
            this._uBTsrColumn.Name = "_uBTsrColumn";
            this._uBTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTsrColumn.Width = 50;
            // 
            // _uBTvzColumn
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTvzColumn.DefaultCellStyle = dataGridViewCellStyle80;
            this._uBTvzColumn.HeaderText = "tвз, мс";
            this._uBTvzColumn.MinimumWidth = 6;
            this._uBTvzColumn.Name = "_uBTvzColumn";
            this._uBTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTvzColumn.Width = 50;
            // 
            // _uBUvzColumn
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUvzColumn.DefaultCellStyle = dataGridViewCellStyle81;
            this._uBUvzColumn.HeaderText = "Uвз, В";
            this._uBUvzColumn.MinimumWidth = 6;
            this._uBUvzColumn.Name = "_uBUvzColumn";
            this._uBUvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUvzColumn.Width = 50;
            // 
            // _uBUvzYNColumn
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle82.NullValue = false;
            this._uBUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle82;
            this._uBUvzYNColumn.HeaderText = "Возврат";
            this._uBUvzYNColumn.MinimumWidth = 6;
            this._uBUvzYNColumn.Name = "_uBUvzYNColumn";
            this._uBUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUvzYNColumn.Width = 55;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Column7";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            this.Column7.Width = 125;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.Visible = false;
            this.Column4.Width = 90;
            // 
            // _uBBlockingColumn
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBBlockingColumn.DefaultCellStyle = dataGridViewCellStyle83;
            this._uBBlockingColumn.HeaderText = "Блокировка";
            this._uBBlockingColumn.MinimumWidth = 6;
            this._uBBlockingColumn.Name = "_uBBlockingColumn";
            this._uBBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBBlockingColumn.Width = 107;
            // 
            // _uBOscColumn
            // 
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBOscColumn.DefaultCellStyle = dataGridViewCellStyle84;
            this._uBOscColumn.HeaderText = "Осциллограф";
            this._uBOscColumn.MinimumWidth = 6;
            this._uBOscColumn.Name = "_uBOscColumn";
            this._uBOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBOscColumn.Width = 110;
            // 
            // _uBAPVRetColumn
            // 
            this._uBAPVRetColumn.HeaderText = "АПВ вз.";
            this._uBAPVRetColumn.MinimumWidth = 6;
            this._uBAPVRetColumn.Name = "_uBAPVRetColumn";
            this._uBAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBAPVRetColumn.Width = 55;
            // 
            // _uBUROVColumn
            // 
            this._uBUROVColumn.HeaderText = "УРОВ";
            this._uBUROVColumn.MinimumWidth = 6;
            this._uBUROVColumn.Name = "_uBUROVColumn";
            this._uBUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBUROVColumn.Width = 45;
            // 
            // _uBAPVColumn1
            // 
            this._uBAPVColumn1.HeaderText = "АПВ";
            this._uBAPVColumn1.MinimumWidth = 6;
            this._uBAPVColumn1.Name = "_uBAPVColumn1";
            this._uBAPVColumn1.Width = 70;
            // 
            // _uBAPVColumn
            // 
            this._uBAPVColumn.HeaderText = "Сброс ступени";
            this._uBAPVColumn.MinimumWidth = 6;
            this._uBAPVColumn.Name = "_uBAPVColumn";
            this._uBAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uBAPVColumn.Width = 60;
            // 
            // _avrUmax
            // 
            this._avrUmax.HeaderText = "АВР";
            this._avrUmax.MinimumWidth = 6;
            this._avrUmax.Name = "_avrUmax";
            this._avrUmax.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._avrUmax.Width = 70;
            // 
            // groupBox23
            // 
            this.groupBox23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox23.Controls.Add(this._difensesUMDataGrid);
            this.groupBox23.Location = new System.Drawing.Point(3, 175);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(950, 169);
            this.groupBox23.TabIndex = 5;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Защиты U<";
            // 
            // _difensesUMDataGrid
            // 
            this._difensesUMDataGrid.AllowUserToAddRows = false;
            this._difensesUMDataGrid.AllowUserToDeleteRows = false;
            this._difensesUMDataGrid.AllowUserToResizeColumns = false;
            this._difensesUMDataGrid.AllowUserToResizeRows = false;
            this._difensesUMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._uMStageColumn,
            this._uMModesColumn,
            this.Column2,
            this._uMTypeColumn,
            this.sideUm,
            this._uMUsrColumn,
            this._uMTsrColumn,
            this._uMTvzColumn,
            this._uMUvzColumn,
            this._uMUvzYNColumn,
            this._block5V,
            this._uMBlockingUMColumn,
            this._uMBlockingColumn,
            this._uMOscColumn,
            this._uMAPVRetColumn,
            this._uMUROVColumn,
            this._uMAPVColumn1,
            this._uMAPVColumn,
            this._avrMax});
            this._difensesUMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesUMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesUMDataGrid.MultiSelect = false;
            this._difensesUMDataGrid.Name = "_difensesUMDataGrid";
            this._difensesUMDataGrid.RowHeadersVisible = false;
            this._difensesUMDataGrid.RowHeadersWidth = 51;
            this._difensesUMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUMDataGrid.RowTemplate.Height = 24;
            this._difensesUMDataGrid.ShowCellErrors = false;
            this._difensesUMDataGrid.ShowRowErrors = false;
            this._difensesUMDataGrid.Size = new System.Drawing.Size(944, 150);
            this._difensesUMDataGrid.TabIndex = 4;
            // 
            // _uMStageColumn
            // 
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._uMStageColumn.DefaultCellStyle = dataGridViewCellStyle85;
            this._uMStageColumn.Frozen = true;
            this._uMStageColumn.HeaderText = "";
            this._uMStageColumn.MinimumWidth = 6;
            this._uMStageColumn.Name = "_uMStageColumn";
            this._uMStageColumn.ReadOnly = true;
            this._uMStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMStageColumn.Width = 40;
            // 
            // _uMModesColumn
            // 
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMModesColumn.DefaultCellStyle = dataGridViewCellStyle86;
            this._uMModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._uMModesColumn.HeaderText = "Режим";
            this._uMModesColumn.MinimumWidth = 6;
            this._uMModesColumn.Name = "_uMModesColumn";
            this._uMModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMModesColumn.Width = 125;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            this.Column2.Width = 125;
            // 
            // _uMTypeColumn
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTypeColumn.DefaultCellStyle = dataGridViewCellStyle87;
            this._uMTypeColumn.HeaderText = "Тип";
            this._uMTypeColumn.MinimumWidth = 6;
            this._uMTypeColumn.Name = "_uMTypeColumn";
            this._uMTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMTypeColumn.Width = 90;
            // 
            // sideUm
            // 
            this.sideUm.HeaderText = "Группа";
            this.sideUm.MinimumWidth = 6;
            this.sideUm.Name = "sideUm";
            this.sideUm.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.sideUm.Width = 60;
            // 
            // _uMUsrColumn
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMUsrColumn.DefaultCellStyle = dataGridViewCellStyle88;
            this._uMUsrColumn.HeaderText = "Uср, В";
            this._uMUsrColumn.MinimumWidth = 6;
            this._uMUsrColumn.Name = "_uMUsrColumn";
            this._uMUsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMUsrColumn.Width = 50;
            // 
            // _uMTsrColumn
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTsrColumn.DefaultCellStyle = dataGridViewCellStyle89;
            this._uMTsrColumn.HeaderText = "tср, мс";
            this._uMTsrColumn.MinimumWidth = 6;
            this._uMTsrColumn.Name = "_uMTsrColumn";
            this._uMTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMTsrColumn.Width = 50;
            // 
            // _uMTvzColumn
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTvzColumn.DefaultCellStyle = dataGridViewCellStyle90;
            this._uMTvzColumn.HeaderText = "tвз, мс";
            this._uMTvzColumn.MinimumWidth = 6;
            this._uMTvzColumn.Name = "_uMTvzColumn";
            this._uMTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMTvzColumn.Width = 50;
            // 
            // _uMUvzColumn
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMUvzColumn.DefaultCellStyle = dataGridViewCellStyle91;
            this._uMUvzColumn.HeaderText = "Uвз, В";
            this._uMUvzColumn.MinimumWidth = 6;
            this._uMUvzColumn.Name = "_uMUvzColumn";
            this._uMUvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMUvzColumn.Width = 50;
            // 
            // _uMUvzYNColumn
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle92.NullValue = false;
            this._uMUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle92;
            this._uMUvzYNColumn.HeaderText = "Возврат";
            this._uMUvzYNColumn.MinimumWidth = 6;
            this._uMUvzYNColumn.Name = "_uMUvzYNColumn";
            this._uMUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMUvzYNColumn.Width = 55;
            // 
            // _block5V
            // 
            this._block5V.HeaderText = "Блок. U<5В";
            this._block5V.MinimumWidth = 6;
            this._block5V.Name = "_block5V";
            this._block5V.Width = 50;
            // 
            // _uMBlockingUMColumn
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMBlockingUMColumn.DefaultCellStyle = dataGridViewCellStyle93;
            this._uMBlockingUMColumn.HeaderText = "Блок. от неиспр. ТН";
            this._uMBlockingUMColumn.MinimumWidth = 6;
            this._uMBlockingUMColumn.Name = "_uMBlockingUMColumn";
            this._uMBlockingUMColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMBlockingUMColumn.Visible = false;
            this._uMBlockingUMColumn.Width = 120;
            // 
            // _uMBlockingColumn
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMBlockingColumn.DefaultCellStyle = dataGridViewCellStyle94;
            this._uMBlockingColumn.HeaderText = "Блокировка";
            this._uMBlockingColumn.MinimumWidth = 6;
            this._uMBlockingColumn.Name = "_uMBlockingColumn";
            this._uMBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMBlockingColumn.Width = 107;
            // 
            // _uMOscColumn
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMOscColumn.DefaultCellStyle = dataGridViewCellStyle95;
            this._uMOscColumn.HeaderText = "Осциллограф";
            this._uMOscColumn.MinimumWidth = 6;
            this._uMOscColumn.Name = "_uMOscColumn";
            this._uMOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMOscColumn.Width = 110;
            // 
            // _uMAPVRetColumn
            // 
            this._uMAPVRetColumn.HeaderText = "АПВ вз.";
            this._uMAPVRetColumn.MinimumWidth = 6;
            this._uMAPVRetColumn.Name = "_uMAPVRetColumn";
            this._uMAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMAPVRetColumn.Width = 50;
            // 
            // _uMUROVColumn
            // 
            this._uMUROVColumn.HeaderText = "УРОВ";
            this._uMUROVColumn.MinimumWidth = 6;
            this._uMUROVColumn.Name = "_uMUROVColumn";
            this._uMUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMUROVColumn.Width = 45;
            // 
            // _uMAPVColumn1
            // 
            this._uMAPVColumn1.HeaderText = "АПВ";
            this._uMAPVColumn1.MinimumWidth = 6;
            this._uMAPVColumn1.Name = "_uMAPVColumn1";
            this._uMAPVColumn1.Width = 70;
            // 
            // _uMAPVColumn
            // 
            this._uMAPVColumn.HeaderText = "Сброс ступени";
            this._uMAPVColumn.MinimumWidth = 6;
            this._uMAPVColumn.Name = "_uMAPVColumn";
            this._uMAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._uMAPVColumn.Width = 60;
            // 
            // _avrMax
            // 
            this._avrMax.HeaderText = "АВР";
            this._avrMax.MinimumWidth = 6;
            this._avrMax.Name = "_avrMax";
            this._avrMax.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._avrMax.Width = 70;
            // 
            // _defFGr1
            // 
            this._defFGr1.Controls.Add(this.panel5);
            this._defFGr1.Location = new System.Drawing.Point(4, 25);
            this._defFGr1.Name = "_defFGr1";
            this._defFGr1.Size = new System.Drawing.Size(956, 443);
            this._defFGr1.TabIndex = 9;
            this._defFGr1.Text = "Защ. F";
            this._defFGr1.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.groupBox27);
            this.panel5.Controls.Add(this.groupBox28);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(956, 443);
            this.panel5.TabIndex = 7;
            // 
            // groupBox27
            // 
            this.groupBox27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox27.Controls.Add(this._difensesFBDataGrid);
            this.groupBox27.Location = new System.Drawing.Point(3, 3);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(950, 169);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Защиты F>";
            // 
            // _difensesFBDataGrid
            // 
            this._difensesFBDataGrid.AllowUserToAddRows = false;
            this._difensesFBDataGrid.AllowUserToDeleteRows = false;
            this._difensesFBDataGrid.AllowUserToResizeColumns = false;
            this._difensesFBDataGrid.AllowUserToResizeRows = false;
            this._difensesFBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn70,
            this.dataGridViewComboBoxColumn71,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewCheckBoxColumn21,
            this.dataGridViewComboBoxColumn72,
            this.dataGridViewComboBoxColumn73,
            this.dataGridViewCheckBoxColumn22,
            this.dataGridViewCheckBoxColumn23,
            this.dataGridViewComboBoxColumn74,
            this.dataGridViewCheckBoxColumn24,
            this.dataGridViewComboBoxColumn75});
            this._difensesFBDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFBDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFBDataGrid.MultiSelect = false;
            this._difensesFBDataGrid.Name = "_difensesFBDataGrid";
            this._difensesFBDataGrid.RowHeadersVisible = false;
            this._difensesFBDataGrid.RowHeadersWidth = 51;
            this._difensesFBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFBDataGrid.RowTemplate.Height = 24;
            this._difensesFBDataGrid.ShowCellErrors = false;
            this._difensesFBDataGrid.ShowRowErrors = false;
            this._difensesFBDataGrid.Size = new System.Drawing.Size(944, 150);
            this._difensesFBDataGrid.TabIndex = 5;
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle96;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 40;
            // 
            // dataGridViewComboBoxColumn70
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn70.DefaultCellStyle = dataGridViewCellStyle97;
            this.dataGridViewComboBoxColumn70.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn70.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn70.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn70.Name = "dataGridViewComboBoxColumn70";
            this.dataGridViewComboBoxColumn70.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn70.Width = 125;
            // 
            // dataGridViewComboBoxColumn71
            // 
            this.dataGridViewComboBoxColumn71.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn71.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn71.Name = "dataGridViewComboBoxColumn71";
            this.dataGridViewComboBoxColumn71.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn71.Width = 70;
            // 
            // dataGridViewTextBoxColumn54
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn54.DefaultCellStyle = dataGridViewCellStyle98;
            this.dataGridViewTextBoxColumn54.HeaderText = "Fср [Гц] | dF/dt [Гц/с]";
            this.dataGridViewTextBoxColumn54.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn54.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn54.Width = 125;
            // 
            // dataGridViewTextBoxColumn55
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.dataGridViewTextBoxColumn55.DefaultCellStyle = dataGridViewCellStyle99;
            this.dataGridViewTextBoxColumn55.HeaderText = "U1, В";
            this.dataGridViewTextBoxColumn55.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn55.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn55.Width = 70;
            // 
            // dataGridViewTextBoxColumn56
            // 
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn56.DefaultCellStyle = dataGridViewCellStyle100;
            this.dataGridViewTextBoxColumn56.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn56.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn56.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn56.Width = 55;
            // 
            // dataGridViewTextBoxColumn57
            // 
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn57.DefaultCellStyle = dataGridViewCellStyle101;
            this.dataGridViewTextBoxColumn57.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn57.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn57.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn57.Width = 55;
            // 
            // dataGridViewTextBoxColumn58
            // 
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn58.DefaultCellStyle = dataGridViewCellStyle102;
            this.dataGridViewTextBoxColumn58.HeaderText = "Fвз, Гц";
            this.dataGridViewTextBoxColumn58.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn58.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn58.Width = 55;
            // 
            // dataGridViewCheckBoxColumn21
            // 
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle103.NullValue = false;
            this.dataGridViewCheckBoxColumn21.DefaultCellStyle = dataGridViewCellStyle103;
            this.dataGridViewCheckBoxColumn21.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn21.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn21.Name = "dataGridViewCheckBoxColumn21";
            this.dataGridViewCheckBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn21.Width = 55;
            // 
            // dataGridViewComboBoxColumn72
            // 
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn72.DefaultCellStyle = dataGridViewCellStyle104;
            this.dataGridViewComboBoxColumn72.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn72.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn72.Name = "dataGridViewComboBoxColumn72";
            this.dataGridViewComboBoxColumn72.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn72.Width = 107;
            // 
            // dataGridViewComboBoxColumn73
            // 
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn73.DefaultCellStyle = dataGridViewCellStyle105;
            this.dataGridViewComboBoxColumn73.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn73.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn73.Name = "dataGridViewComboBoxColumn73";
            this.dataGridViewComboBoxColumn73.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn73.Width = 110;
            // 
            // dataGridViewCheckBoxColumn22
            // 
            this.dataGridViewCheckBoxColumn22.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn22.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn22.Name = "dataGridViewCheckBoxColumn22";
            this.dataGridViewCheckBoxColumn22.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn22.Width = 50;
            // 
            // dataGridViewCheckBoxColumn23
            // 
            this.dataGridViewCheckBoxColumn23.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn23.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn23.Name = "dataGridViewCheckBoxColumn23";
            this.dataGridViewCheckBoxColumn23.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn23.Width = 45;
            // 
            // dataGridViewComboBoxColumn74
            // 
            this.dataGridViewComboBoxColumn74.HeaderText = "АПВ";
            this.dataGridViewComboBoxColumn74.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn74.Name = "dataGridViewComboBoxColumn74";
            this.dataGridViewComboBoxColumn74.Width = 70;
            // 
            // dataGridViewCheckBoxColumn24
            // 
            this.dataGridViewCheckBoxColumn24.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn24.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn24.Name = "dataGridViewCheckBoxColumn24";
            this.dataGridViewCheckBoxColumn24.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn24.Width = 60;
            // 
            // dataGridViewComboBoxColumn75
            // 
            this.dataGridViewComboBoxColumn75.HeaderText = "АВР";
            this.dataGridViewComboBoxColumn75.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn75.Name = "dataGridViewComboBoxColumn75";
            this.dataGridViewComboBoxColumn75.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn75.Width = 70;
            // 
            // groupBox28
            // 
            this.groupBox28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox28.Controls.Add(this._difensesFMDataGrid);
            this.groupBox28.Location = new System.Drawing.Point(3, 175);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(950, 169);
            this.groupBox28.TabIndex = 6;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Защиты F<";
            // 
            // _difensesFMDataGrid
            // 
            this._difensesFMDataGrid.AllowUserToAddRows = false;
            this._difensesFMDataGrid.AllowUserToDeleteRows = false;
            this._difensesFMDataGrid.AllowUserToResizeColumns = false;
            this._difensesFMDataGrid.AllowUserToResizeRows = false;
            this._difensesFMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1,
            this.dataGridViewComboBoxColumn2,
            this.Column10,
            this.Column12,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewComboBoxColumn3,
            this.dataGridViewComboBoxColumn4,
            this.dataGridViewCheckBoxColumn4,
            this.dataGridViewCheckBoxColumn2,
            this._fMAPVColumn1,
            this.dataGridViewCheckBoxColumn5,
            this.avrFM});
            this._difensesFMDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._difensesFMDataGrid.Location = new System.Drawing.Point(3, 16);
            this._difensesFMDataGrid.MultiSelect = false;
            this._difensesFMDataGrid.Name = "_difensesFMDataGrid";
            this._difensesFMDataGrid.RowHeadersVisible = false;
            this._difensesFMDataGrid.RowHeadersWidth = 51;
            this._difensesFMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFMDataGrid.RowTemplate.Height = 24;
            this._difensesFMDataGrid.ShowCellErrors = false;
            this._difensesFMDataGrid.ShowRowErrors = false;
            this._difensesFMDataGrid.Size = new System.Drawing.Size(944, 150);
            this._difensesFMDataGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle106;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewComboBoxColumn1
            // 
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn1.DefaultCellStyle = dataGridViewCellStyle107;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn1.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn1.Width = 125;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn2.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            this.dataGridViewComboBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn2.Width = 70;
            // 
            // Column10
            // 
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column10.DefaultCellStyle = dataGridViewCellStyle108;
            this.Column10.HeaderText = "Fср [Гц] | dF/dt [Гц/с]";
            this.Column10.MinimumWidth = 6;
            this.Column10.Name = "Column10";
            this.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column10.Width = 125;
            // 
            // Column12
            // 
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight;
            this.Column12.DefaultCellStyle = dataGridViewCellStyle109;
            this.Column12.HeaderText = "U1, В";
            this.Column12.MinimumWidth = 6;
            this.Column12.Name = "Column12";
            this.Column12.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column12.Width = 70;
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle110;
            this.dataGridViewTextBoxColumn4.HeaderText = "tср, мс";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 55;
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle111;
            this.dataGridViewTextBoxColumn5.HeaderText = "tвз, мс";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 55;
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle112;
            this.dataGridViewTextBoxColumn6.HeaderText = "Fвз, Гц";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 55;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle113.NullValue = false;
            this.dataGridViewCheckBoxColumn1.DefaultCellStyle = dataGridViewCellStyle113;
            this.dataGridViewCheckBoxColumn1.HeaderText = "Возврат";
            this.dataGridViewCheckBoxColumn1.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn1.Width = 55;
            // 
            // dataGridViewComboBoxColumn3
            // 
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn3.DefaultCellStyle = dataGridViewCellStyle114;
            this.dataGridViewComboBoxColumn3.HeaderText = "Блокировка";
            this.dataGridViewComboBoxColumn3.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            this.dataGridViewComboBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn3.Width = 107;
            // 
            // dataGridViewComboBoxColumn4
            // 
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn4.DefaultCellStyle = dataGridViewCellStyle115;
            this.dataGridViewComboBoxColumn4.HeaderText = "Осциллограф";
            this.dataGridViewComboBoxColumn4.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            this.dataGridViewComboBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn4.Width = 110;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "АПВ возвр.";
            this.dataGridViewCheckBoxColumn4.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn4.Width = 50;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "УРОВ";
            this.dataGridViewCheckBoxColumn2.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn2.Width = 45;
            // 
            // _fMAPVColumn1
            // 
            this._fMAPVColumn1.HeaderText = "АПВ";
            this._fMAPVColumn1.MinimumWidth = 6;
            this._fMAPVColumn1.Name = "_fMAPVColumn1";
            this._fMAPVColumn1.Width = 70;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "Сброс ступени";
            this.dataGridViewCheckBoxColumn5.MinimumWidth = 6;
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCheckBoxColumn5.Width = 60;
            // 
            // avrFM
            // 
            this.avrFM.HeaderText = "АВР";
            this.avrFM.MinimumWidth = 6;
            this.avrFM.Name = "avrFM";
            this.avrFM.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.avrFM.Width = 70;
            // 
            // _defDvigGr1
            // 
            this._defDvigGr1.Controls.Add(this.groupBox52);
            this._defDvigGr1.Controls.Add(this.panel6);
            this._defDvigGr1.Controls.Add(this.groupBox43);
            this._defDvigGr1.Location = new System.Drawing.Point(4, 25);
            this._defDvigGr1.Name = "_defDvigGr1";
            this._defDvigGr1.Padding = new System.Windows.Forms.Padding(3);
            this._defDvigGr1.Size = new System.Drawing.Size(956, 443);
            this._defDvigGr1.TabIndex = 11;
            this._defDvigGr1.Text = "Защ. двигателя";
            this._defDvigGr1.UseVisualStyleBackColor = true;
            // 
            // groupBox52
            // 
            this.groupBox52.Controls.Add(this.label185);
            this.groupBox52.Controls.Add(this.label186);
            this.groupBox52.Controls.Add(this._numHot);
            this.groupBox52.Controls.Add(this._numCold);
            this.groupBox52.Controls.Add(this.label187);
            this.groupBox52.Controls.Add(this._waitNumBlock);
            this.groupBox52.Location = new System.Drawing.Point(236, 6);
            this.groupBox52.Name = "groupBox52";
            this.groupBox52.Size = new System.Drawing.Size(242, 108);
            this.groupBox52.TabIndex = 6;
            this.groupBox52.TabStop = false;
            this.groupBox52.Text = "Блокировка по числу пусков";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(6, 50);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(150, 13);
            this.label185.TabIndex = 14;
            this.label185.Text = "Число горячих пусков Nгор.";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(6, 24);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(146, 13);
            this.label186.TabIndex = 0;
            this.label186.Text = "Общее число пусков Nпуск";
            // 
            // _numHot
            // 
            this._numHot.Location = new System.Drawing.Point(162, 47);
            this._numHot.Name = "_numHot";
            this._numHot.Size = new System.Drawing.Size(69, 20);
            this._numHot.TabIndex = 1;
            this._numHot.Tag = "256";
            this._numHot.Text = "0";
            this._numHot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _numCold
            // 
            this._numCold.Location = new System.Drawing.Point(162, 21);
            this._numCold.Name = "_numCold";
            this._numCold.Size = new System.Drawing.Size(69, 20);
            this._numCold.TabIndex = 1;
            this._numCold.Tag = "256";
            this._numCold.Text = "0";
            this._numCold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(6, 76);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(76, 13);
            this.label187.TabIndex = 6;
            this.label187.Text = "Время tблк, c";
            // 
            // _waitNumBlock
            // 
            this._waitNumBlock.Location = new System.Drawing.Point(162, 73);
            this._waitNumBlock.Name = "_waitNumBlock";
            this._waitNumBlock.Size = new System.Drawing.Size(69, 20);
            this._waitNumBlock.TabIndex = 7;
            this._waitNumBlock.Tag = "65000";
            this._waitNumBlock.Text = "0";
            this._waitNumBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this._engineDefensesGrid);
            this.panel6.Location = new System.Drawing.Point(6, 120);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(695, 80);
            this.panel6.TabIndex = 5;
            // 
            // _engineDefensesGrid
            // 
            this._engineDefensesGrid.AllowUserToAddRows = false;
            this._engineDefensesGrid.AllowUserToDeleteRows = false;
            this._engineDefensesGrid.AllowUserToResizeColumns = false;
            this._engineDefensesGrid.AllowUserToResizeRows = false;
            this._engineDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._engineDefensesGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._engineDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._engineDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._engineDefenseNameCol,
            this._engineDefenseModeCol,
            this._engineDefenseConstraintCol,
            this._engineDefenseBlockCol,
            this._engineDefenseOscCol,
            this._engineDefenseUROVcol,
            this._engineDefenseAPV1,
            this.avrQ});
            this._engineDefensesGrid.Location = new System.Drawing.Point(3, 3);
            this._engineDefensesGrid.Name = "_engineDefensesGrid";
            this._engineDefensesGrid.RowHeadersVisible = false;
            this._engineDefensesGrid.RowHeadersWidth = 51;
            this._engineDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._engineDefensesGrid.Size = new System.Drawing.Size(689, 74);
            this._engineDefensesGrid.TabIndex = 3;
            // 
            // _engineDefenseNameCol
            // 
            this._engineDefenseNameCol.HeaderText = "";
            this._engineDefenseNameCol.MinimumWidth = 40;
            this._engineDefenseNameCol.Name = "_engineDefenseNameCol";
            this._engineDefenseNameCol.ReadOnly = true;
            this._engineDefenseNameCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseNameCol.Width = 40;
            // 
            // _engineDefenseModeCol
            // 
            this._engineDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._engineDefenseModeCol.HeaderText = "Режим";
            this._engineDefenseModeCol.MinimumWidth = 120;
            this._engineDefenseModeCol.Name = "_engineDefenseModeCol";
            this._engineDefenseModeCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseModeCol.Width = 120;
            // 
            // _engineDefenseConstraintCol
            // 
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle116.NullValue = null;
            this._engineDefenseConstraintCol.DefaultCellStyle = dataGridViewCellStyle116;
            this._engineDefenseConstraintCol.HeaderText = "Уставка Q, %";
            this._engineDefenseConstraintCol.MinimumWidth = 50;
            this._engineDefenseConstraintCol.Name = "_engineDefenseConstraintCol";
            this._engineDefenseConstraintCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseConstraintCol.Width = 85;
            // 
            // _engineDefenseBlockCol
            // 
            this._engineDefenseBlockCol.HeaderText = "Блокировка";
            this._engineDefenseBlockCol.MinimumWidth = 100;
            this._engineDefenseBlockCol.Name = "_engineDefenseBlockCol";
            this._engineDefenseBlockCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseBlockCol.Width = 125;
            // 
            // _engineDefenseOscCol
            // 
            this._engineDefenseOscCol.HeaderText = "Осциллограф";
            this._engineDefenseOscCol.MinimumWidth = 150;
            this._engineDefenseOscCol.Name = "_engineDefenseOscCol";
            this._engineDefenseOscCol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseOscCol.Width = 150;
            // 
            // _engineDefenseUROVcol
            // 
            this._engineDefenseUROVcol.HeaderText = "УРОВ";
            this._engineDefenseUROVcol.MinimumWidth = 50;
            this._engineDefenseUROVcol.Name = "_engineDefenseUROVcol";
            this._engineDefenseUROVcol.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._engineDefenseUROVcol.Width = 50;
            // 
            // _engineDefenseAPV1
            // 
            this._engineDefenseAPV1.HeaderText = "АПВ";
            this._engineDefenseAPV1.MinimumWidth = 6;
            this._engineDefenseAPV1.Name = "_engineDefenseAPV1";
            this._engineDefenseAPV1.Width = 72;
            // 
            // avrQ
            // 
            this.avrQ.HeaderText = "АВР";
            this.avrQ.MinimumWidth = 6;
            this.avrQ.Name = "avrQ";
            this.avrQ.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.avrQ.Width = 70;
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this.label54);
            this.groupBox43.Controls.Add(this.label56);
            this.groupBox43.Controls.Add(this._engineQconstraintGr1);
            this.groupBox43.Controls.Add(this.label55);
            this.groupBox43.Controls.Add(this._engineQtimeGr1);
            this.groupBox43.Controls.Add(this._engineQmodeGr1);
            this.groupBox43.Location = new System.Drawing.Point(6, 6);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Size = new System.Drawing.Size(224, 108);
            this.groupBox43.TabIndex = 4;
            this.groupBox43.TabStop = false;
            this.groupBox43.Text = "Блокировка по тепловому состоянию Q";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(9, 24);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(42, 13);
            this.label54.TabIndex = 14;
            this.label54.Text = "Режим";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(9, 51);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(93, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Уставка Qблк, %";
            // 
            // _engineQconstraintGr1
            // 
            this._engineQconstraintGr1.Location = new System.Drawing.Point(111, 44);
            this._engineQconstraintGr1.Name = "_engineQconstraintGr1";
            this._engineQconstraintGr1.Size = new System.Drawing.Size(69, 20);
            this._engineQconstraintGr1.TabIndex = 1;
            this._engineQconstraintGr1.Tag = "256";
            this._engineQconstraintGr1.Text = "0";
            this._engineQconstraintGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(9, 70);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(76, 13);
            this.label55.TabIndex = 6;
            this.label55.Text = "Время tблк, c";
            // 
            // _engineQtimeGr1
            // 
            this._engineQtimeGr1.Location = new System.Drawing.Point(111, 64);
            this._engineQtimeGr1.Name = "_engineQtimeGr1";
            this._engineQtimeGr1.Size = new System.Drawing.Size(69, 20);
            this._engineQtimeGr1.TabIndex = 7;
            this._engineQtimeGr1.Tag = "65000";
            this._engineQtimeGr1.Text = "0";
            this._engineQtimeGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineQmodeGr1
            // 
            this._engineQmodeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQmodeGr1.FormattingEnabled = true;
            this._engineQmodeGr1.Location = new System.Drawing.Point(90, 19);
            this._engineQmodeGr1.Name = "_engineQmodeGr1";
            this._engineQmodeGr1.Size = new System.Drawing.Size(90, 21);
            this._engineQmodeGr1.TabIndex = 12;
            // 
            // _defExtGr1
            // 
            this._defExtGr1.Controls.Add(this._externalDefenses);
            this._defExtGr1.Location = new System.Drawing.Point(4, 25);
            this._defExtGr1.Name = "_defExtGr1";
            this._defExtGr1.Size = new System.Drawing.Size(956, 443);
            this._defExtGr1.TabIndex = 8;
            this._defExtGr1.Text = "Внешние";
            this._defExtGr1.UseVisualStyleBackColor = true;
            // 
            // _externalDefenses
            // 
            this._externalDefenses.AllowUserToAddRows = false;
            this._externalDefenses.AllowUserToDeleteRows = false;
            this._externalDefenses.AllowUserToResizeColumns = false;
            this._externalDefenses.AllowUserToResizeRows = false;
            this._externalDefenses.BackgroundColor = System.Drawing.Color.White;
            this._externalDefenses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDefenses.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._externalDifStageColumn,
            this._externalDifModesColumn,
            this._externalDifSrabColumn,
            this._externalDifTsrColumn,
            this._externalDifTvzColumn,
            this._externalDifVozvrColumn,
            this._externalDifVozvrYNColumn,
            this._externalDifBlockingColumn,
            this._externalDifOscColumn,
            this._externalDifAPVRetColumn,
            this._externalDifUROVColumn,
            this._externalDifAPV1,
            this._externalDifSbrosColumn,
            this.avrExt});
            this._externalDefenses.Dock = System.Windows.Forms.DockStyle.Fill;
            this._externalDefenses.Location = new System.Drawing.Point(0, 0);
            this._externalDefenses.MultiSelect = false;
            this._externalDefenses.Name = "_externalDefenses";
            this._externalDefenses.RowHeadersVisible = false;
            this._externalDefenses.RowHeadersWidth = 51;
            this._externalDefenses.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDefenses.RowTemplate.Height = 24;
            this._externalDefenses.ShowCellErrors = false;
            this._externalDefenses.ShowRowErrors = false;
            this._externalDefenses.Size = new System.Drawing.Size(956, 443);
            this._externalDefenses.TabIndex = 4;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle117;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "";
            this._externalDifStageColumn.MinimumWidth = 6;
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifStageColumn.Width = 50;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle118;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Режим";
            this._externalDifModesColumn.MinimumWidth = 6;
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifModesColumn.Width = 125;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle119;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.MinimumWidth = 6;
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle120;
            this._externalDifTsrColumn.HeaderText = "tср, мс";
            this._externalDifTsrColumn.MinimumWidth = 6;
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 50;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle121;
            this._externalDifTvzColumn.HeaderText = "tвз, мс";
            this._externalDifTvzColumn.MinimumWidth = 6;
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 50;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle122;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.MinimumWidth = 6;
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrColumn.Width = 130;
            // 
            // _externalDifVozvrYNColumn
            // 
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.MinimumWidth = 6;
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifVozvrYNColumn.Width = 53;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle123;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.MinimumWidth = 6;
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifBlockingColumn.Width = 130;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle124;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.MinimumWidth = 6;
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifAPVRetColumn
            // 
            this._externalDifAPVRetColumn.HeaderText = "АПВ вз.";
            this._externalDifAPVRetColumn.MinimumWidth = 6;
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifAPVRetColumn.Width = 35;
            // 
            // _externalDifUROVColumn
            // 
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.MinimumWidth = 6;
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifUROVColumn.Width = 40;
            // 
            // _externalDifAPV1
            // 
            this._externalDifAPV1.HeaderText = "АПВ";
            this._externalDifAPV1.MinimumWidth = 6;
            this._externalDifAPV1.Name = "_externalDifAPV1";
            this._externalDifAPV1.Width = 70;
            // 
            // _externalDifSbrosColumn
            // 
            this._externalDifSbrosColumn.HeaderText = "Сброс ступени";
            this._externalDifSbrosColumn.MinimumWidth = 6;
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._externalDifSbrosColumn.Width = 40;
            // 
            // avrExt
            // 
            this.avrExt.HeaderText = "АВР";
            this.avrExt.MinimumWidth = 6;
            this.avrExt.Name = "avrExt";
            this.avrExt.Width = 70;
            // 
            // _powerPage
            // 
            this._powerPage.Controls.Add(this.groupBox47);
            this._powerPage.Location = new System.Drawing.Point(4, 25);
            this._powerPage.Name = "_powerPage";
            this._powerPage.Size = new System.Drawing.Size(956, 443);
            this._powerPage.TabIndex = 24;
            this._powerPage.Text = "Защ. P";
            this._powerPage.UseVisualStyleBackColor = true;
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this._reversePowerGrid);
            this.groupBox47.Location = new System.Drawing.Point(3, 3);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(950, 126);
            this.groupBox47.TabIndex = 6;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Защиты P";
            // 
            // _reversePowerGrid
            // 
            this._reversePowerGrid.AllowUserToAddRows = false;
            this._reversePowerGrid.AllowUserToDeleteRows = false;
            this._reversePowerGrid.AllowUserToResizeColumns = false;
            this._reversePowerGrid.AllowUserToResizeRows = false;
            this._reversePowerGrid.BackgroundColor = System.Drawing.Color.White;
            this._reversePowerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._reversePowerGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewComboBoxColumn52,
            this._ssr,
            this._usr,
            this._tsr,
            this._tvz,
            this._svz,
            this._svzbeno,
            this._isr,
            this._block,
            this._blk,
            this._osc,
            this._apvvoz,
            this._urov,
            this._apv,
            this._resetstep});
            this._reversePowerGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._reversePowerGrid.Location = new System.Drawing.Point(3, 16);
            this._reversePowerGrid.MultiSelect = false;
            this._reversePowerGrid.Name = "_reversePowerGrid";
            this._reversePowerGrid.RowHeadersVisible = false;
            this._reversePowerGrid.RowHeadersWidth = 51;
            this._reversePowerGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._reversePowerGrid.RowTemplate.Height = 24;
            this._reversePowerGrid.ShowCellErrors = false;
            this._reversePowerGrid.ShowRowErrors = false;
            this._reversePowerGrid.Size = new System.Drawing.Size(944, 107);
            this._reversePowerGrid.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn44
            // 
            dataGridViewCellStyle125.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dataGridViewTextBoxColumn44.DefaultCellStyle = dataGridViewCellStyle125;
            this.dataGridViewTextBoxColumn44.Frozen = true;
            this.dataGridViewTextBoxColumn44.HeaderText = "";
            this.dataGridViewTextBoxColumn44.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn44.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn44.Width = 75;
            // 
            // dataGridViewComboBoxColumn52
            // 
            dataGridViewCellStyle126.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewComboBoxColumn52.DefaultCellStyle = dataGridViewCellStyle126;
            this.dataGridViewComboBoxColumn52.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn52.HeaderText = "Режим";
            this.dataGridViewComboBoxColumn52.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn52.Name = "dataGridViewComboBoxColumn52";
            this.dataGridViewComboBoxColumn52.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn52.Width = 125;
            // 
            // _ssr
            // 
            this._ssr.HeaderText = "Sср, Sн";
            this._ssr.MinimumWidth = 6;
            this._ssr.Name = "_ssr";
            this._ssr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._ssr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ssr.Width = 125;
            // 
            // _usr
            // 
            this._usr.HeaderText = "Уср, \'";
            this._usr.MinimumWidth = 6;
            this._usr.Name = "_usr";
            this._usr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._usr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._usr.Width = 125;
            // 
            // _tsr
            // 
            this._tsr.HeaderText = "tср, мс";
            this._tsr.MinimumWidth = 6;
            this._tsr.Name = "_tsr";
            this._tsr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._tsr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tsr.Width = 125;
            // 
            // _tvz
            // 
            this._tvz.HeaderText = "tвз, мс";
            this._tvz.MinimumWidth = 6;
            this._tvz.Name = "_tvz";
            this._tvz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._tvz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._tvz.Width = 125;
            // 
            // _svz
            // 
            this._svz.HeaderText = "Sвз, Sн";
            this._svz.MinimumWidth = 6;
            this._svz.Name = "_svz";
            this._svz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._svz.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._svz.Width = 125;
            // 
            // _svzbeno
            // 
            this._svzbeno.HeaderText = "Sвз, (Есть, Нет)";
            this._svzbeno.MinimumWidth = 6;
            this._svzbeno.Name = "_svzbeno";
            this._svzbeno.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._svzbeno.Width = 125;
            // 
            // _isr
            // 
            this._isr.HeaderText = "Iср, н";
            this._isr.MinimumWidth = 6;
            this._isr.Name = "_isr";
            this._isr.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._isr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._isr.Width = 125;
            // 
            // _block
            // 
            this._block.HeaderText = "Блокировка";
            this._block.MinimumWidth = 6;
            this._block.Name = "_block";
            this._block.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._block.Width = 125;
            // 
            // _blk
            // 
            this._blk.HeaderText = "Блк. от неиспр. ТН";
            this._blk.MinimumWidth = 6;
            this._blk.Name = "_blk";
            this._blk.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._blk.Width = 125;
            // 
            // _osc
            // 
            this._osc.HeaderText = "Осц.";
            this._osc.MinimumWidth = 6;
            this._osc.Name = "_osc";
            this._osc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._osc.Width = 125;
            // 
            // _apvvoz
            // 
            this._apvvoz.HeaderText = "АПВвозвр";
            this._apvvoz.MinimumWidth = 6;
            this._apvvoz.Name = "_apvvoz";
            this._apvvoz.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._apvvoz.Width = 125;
            // 
            // _urov
            // 
            this._urov.HeaderText = "УРОВ";
            this._urov.MinimumWidth = 6;
            this._urov.Name = "_urov";
            this._urov.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._urov.Width = 125;
            // 
            // _apv
            // 
            this._apv.HeaderText = "АПВ";
            this._apv.MinimumWidth = 6;
            this._apv.Name = "_apv";
            this._apv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._apv.Width = 125;
            // 
            // _resetstep
            // 
            this._resetstep.HeaderText = "Сброс ступени";
            this._resetstep.MinimumWidth = 6;
            this._resetstep.Name = "_resetstep";
            this._resetstep.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this._resetstep.Width = 60;
            // 
            // _lsGr
            // 
            this._lsGr.Controls.Add(this.groupBox58);
            this._lsGr.Controls.Add(this.groupBox57);
            this._lsGr.Controls.Add(this.groupBox26);
            this._lsGr.Controls.Add(this.groupBox45);
            this._lsGr.Location = new System.Drawing.Point(4, 25);
            this._lsGr.Name = "_lsGr";
            this._lsGr.Padding = new System.Windows.Forms.Padding(3);
            this._lsGr.Size = new System.Drawing.Size(956, 443);
            this._lsGr.TabIndex = 19;
            this._lsGr.Text = "ЛС";
            this._lsGr.UseVisualStyleBackColor = true;
            // 
            // groupBox58
            // 
            this.groupBox58.Controls.Add(this.treeViewForLsOR);
            this.groupBox58.Location = new System.Drawing.Point(664, 3);
            this.groupBox58.Name = "groupBox58";
            this.groupBox58.Size = new System.Drawing.Size(203, 437);
            this.groupBox58.TabIndex = 12;
            this.groupBox58.TabStop = false;
            this.groupBox58.Text = "Логические сигналы ИЛИ";
            // 
            // treeViewForLsOR
            // 
            this.treeViewForLsOR.Location = new System.Drawing.Point(15, 16);
            this.treeViewForLsOR.Name = "treeViewForLsOR";
            this.treeViewForLsOR.Size = new System.Drawing.Size(170, 412);
            this.treeViewForLsOR.TabIndex = 0;
            this.treeViewForLsOR.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsOR_NodeMouseClick);
            // 
            // groupBox57
            // 
            this.groupBox57.Controls.Add(this.treeViewForLsAND);
            this.groupBox57.Location = new System.Drawing.Point(421, 3);
            this.groupBox57.Name = "groupBox57";
            this.groupBox57.Size = new System.Drawing.Size(200, 437);
            this.groupBox57.TabIndex = 11;
            this.groupBox57.TabStop = false;
            this.groupBox57.Text = "Логические сигналы И";
            // 
            // treeViewForLsAND
            // 
            this.treeViewForLsAND.Location = new System.Drawing.Point(18, 16);
            this.treeViewForLsAND.Name = "treeViewForLsAND";
            this.treeViewForLsAND.Size = new System.Drawing.Size(165, 412);
            this.treeViewForLsAND.TabIndex = 0;
            this.treeViewForLsAND.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsAND_NodeMouseClick);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.tabControl1);
            this.groupBox26.Controls.Add(this._clearCurrentLsnButton);
            this.groupBox26.Controls.Add(this._clearAllLsnSygnalButton);
            this.groupBox26.Location = new System.Drawing.Point(205, 3);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(193, 437);
            this.groupBox26.TabIndex = 10;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage31);
            this.tabControl1.Controls.Add(this.tabPage32);
            this.tabControl1.Controls.Add(this.tabPage33);
            this.tabControl1.Controls.Add(this.tabPage34);
            this.tabControl1.Controls.Add(this.tabPage35);
            this.tabControl1.Controls.Add(this.tabPage36);
            this.tabControl1.Controls.Add(this.tabPage37);
            this.tabControl1.Controls.Add(this.tabPage38);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(187, 355);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this._lsOrDgv1Gr1);
            this.tabPage31.Location = new System.Drawing.Point(4, 49);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage31.Size = new System.Drawing.Size(179, 302);
            this.tabPage31.TabIndex = 0;
            this.tabPage31.Text = "ЛС9";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv1Gr1
            // 
            this._lsOrDgv1Gr1.AllowUserToAddRows = false;
            this._lsOrDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv1Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewComboBoxColumn15});
            this._lsOrDgv1Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv1Gr1.Location = new System.Drawing.Point(3, 3);
            this._lsOrDgv1Gr1.MultiSelect = false;
            this._lsOrDgv1Gr1.Name = "_lsOrDgv1Gr1";
            this._lsOrDgv1Gr1.RowHeadersVisible = false;
            this._lsOrDgv1Gr1.RowHeadersWidth = 51;
            this._lsOrDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle127.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle127;
            this._lsOrDgv1Gr1.RowTemplate.Height = 20;
            this._lsOrDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv1Gr1.ShowCellErrors = false;
            this._lsOrDgv1Gr1.ShowCellToolTips = false;
            this._lsOrDgv1Gr1.ShowEditingIcon = false;
            this._lsOrDgv1Gr1.ShowRowErrors = false;
            this._lsOrDgv1Gr1.Size = new System.Drawing.Size(173, 296);
            this._lsOrDgv1Gr1.TabIndex = 2;
            this._lsOrDgv1Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "№";
            this.dataGridViewTextBoxColumn15.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Width = 24;
            // 
            // dataGridViewComboBoxColumn15
            // 
            this.dataGridViewComboBoxColumn15.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn15.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn15.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn15.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn15.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn15.Name = "dataGridViewComboBoxColumn15";
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this._lsOrDgv2Gr1);
            this.tabPage32.Location = new System.Drawing.Point(4, 49);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage32.Size = new System.Drawing.Size(179, 302);
            this.tabPage32.TabIndex = 1;
            this.tabPage32.Text = "ЛС10";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv2Gr1
            // 
            this._lsOrDgv2Gr1.AllowUserToAddRows = false;
            this._lsOrDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv2Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewComboBoxColumn16});
            this._lsOrDgv2Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv2Gr1.Location = new System.Drawing.Point(3, 3);
            this._lsOrDgv2Gr1.MultiSelect = false;
            this._lsOrDgv2Gr1.Name = "_lsOrDgv2Gr1";
            this._lsOrDgv2Gr1.RowHeadersVisible = false;
            this._lsOrDgv2Gr1.RowHeadersWidth = 51;
            this._lsOrDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle128.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle128;
            this._lsOrDgv2Gr1.RowTemplate.Height = 20;
            this._lsOrDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv2Gr1.ShowCellErrors = false;
            this._lsOrDgv2Gr1.ShowCellToolTips = false;
            this._lsOrDgv2Gr1.ShowEditingIcon = false;
            this._lsOrDgv2Gr1.ShowRowErrors = false;
            this._lsOrDgv2Gr1.Size = new System.Drawing.Size(173, 296);
            this._lsOrDgv2Gr1.TabIndex = 4;
            this._lsOrDgv2Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "№";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Width = 24;
            // 
            // dataGridViewComboBoxColumn16
            // 
            this.dataGridViewComboBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn16.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn16.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn16.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn16.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn16.Name = "dataGridViewComboBoxColumn16";
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this._lsOrDgv3Gr1);
            this.tabPage33.Location = new System.Drawing.Point(4, 49);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Size = new System.Drawing.Size(179, 302);
            this.tabPage33.TabIndex = 2;
            this.tabPage33.Text = "ЛС11";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv3Gr1
            // 
            this._lsOrDgv3Gr1.AllowUserToAddRows = false;
            this._lsOrDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv3Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewComboBoxColumn17});
            this._lsOrDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv3Gr1.MultiSelect = false;
            this._lsOrDgv3Gr1.Name = "_lsOrDgv3Gr1";
            this._lsOrDgv3Gr1.RowHeadersVisible = false;
            this._lsOrDgv3Gr1.RowHeadersWidth = 51;
            this._lsOrDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle129.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle129;
            this._lsOrDgv3Gr1.RowTemplate.Height = 20;
            this._lsOrDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv3Gr1.ShowCellErrors = false;
            this._lsOrDgv3Gr1.ShowCellToolTips = false;
            this._lsOrDgv3Gr1.ShowEditingIcon = false;
            this._lsOrDgv3Gr1.ShowRowErrors = false;
            this._lsOrDgv3Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv3Gr1.TabIndex = 4;
            this._lsOrDgv3Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "№";
            this.dataGridViewTextBoxColumn18.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 24;
            // 
            // dataGridViewComboBoxColumn17
            // 
            this.dataGridViewComboBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn17.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn17.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn17.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn17.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn17.Name = "dataGridViewComboBoxColumn17";
            // 
            // tabPage34
            // 
            this.tabPage34.Controls.Add(this._lsOrDgv4Gr1);
            this.tabPage34.Location = new System.Drawing.Point(4, 49);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Size = new System.Drawing.Size(179, 302);
            this.tabPage34.TabIndex = 3;
            this.tabPage34.Text = "ЛС12";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv4Gr1
            // 
            this._lsOrDgv4Gr1.AllowUserToAddRows = false;
            this._lsOrDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv4Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewComboBoxColumn18});
            this._lsOrDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv4Gr1.MultiSelect = false;
            this._lsOrDgv4Gr1.Name = "_lsOrDgv4Gr1";
            this._lsOrDgv4Gr1.RowHeadersVisible = false;
            this._lsOrDgv4Gr1.RowHeadersWidth = 51;
            this._lsOrDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle130.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle130;
            this._lsOrDgv4Gr1.RowTemplate.Height = 20;
            this._lsOrDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv4Gr1.ShowCellErrors = false;
            this._lsOrDgv4Gr1.ShowCellToolTips = false;
            this._lsOrDgv4Gr1.ShowEditingIcon = false;
            this._lsOrDgv4Gr1.ShowRowErrors = false;
            this._lsOrDgv4Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv4Gr1.TabIndex = 4;
            this._lsOrDgv4Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "№";
            this.dataGridViewTextBoxColumn19.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 24;
            // 
            // dataGridViewComboBoxColumn18
            // 
            this.dataGridViewComboBoxColumn18.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn18.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn18.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn18.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn18.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn18.Name = "dataGridViewComboBoxColumn18";
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this._lsOrDgv5Gr1);
            this.tabPage35.Location = new System.Drawing.Point(4, 49);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(179, 302);
            this.tabPage35.TabIndex = 4;
            this.tabPage35.Text = "ЛС13";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv5Gr1
            // 
            this._lsOrDgv5Gr1.AllowUserToAddRows = false;
            this._lsOrDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv5Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewComboBoxColumn19});
            this._lsOrDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv5Gr1.MultiSelect = false;
            this._lsOrDgv5Gr1.Name = "_lsOrDgv5Gr1";
            this._lsOrDgv5Gr1.RowHeadersVisible = false;
            this._lsOrDgv5Gr1.RowHeadersWidth = 51;
            this._lsOrDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle131.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle131;
            this._lsOrDgv5Gr1.RowTemplate.Height = 20;
            this._lsOrDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv5Gr1.ShowCellErrors = false;
            this._lsOrDgv5Gr1.ShowCellToolTips = false;
            this._lsOrDgv5Gr1.ShowEditingIcon = false;
            this._lsOrDgv5Gr1.ShowRowErrors = false;
            this._lsOrDgv5Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv5Gr1.TabIndex = 4;
            this._lsOrDgv5Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "№";
            this.dataGridViewTextBoxColumn20.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 24;
            // 
            // dataGridViewComboBoxColumn19
            // 
            this.dataGridViewComboBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn19.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn19.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn19.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn19.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn19.Name = "dataGridViewComboBoxColumn19";
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this._lsOrDgv6Gr1);
            this.tabPage36.Location = new System.Drawing.Point(4, 49);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Size = new System.Drawing.Size(179, 302);
            this.tabPage36.TabIndex = 5;
            this.tabPage36.Text = "ЛС14";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv6Gr1
            // 
            this._lsOrDgv6Gr1.AllowUserToAddRows = false;
            this._lsOrDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv6Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewComboBoxColumn20});
            this._lsOrDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv6Gr1.MultiSelect = false;
            this._lsOrDgv6Gr1.Name = "_lsOrDgv6Gr1";
            this._lsOrDgv6Gr1.RowHeadersVisible = false;
            this._lsOrDgv6Gr1.RowHeadersWidth = 51;
            this._lsOrDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle132.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle132;
            this._lsOrDgv6Gr1.RowTemplate.Height = 20;
            this._lsOrDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv6Gr1.ShowCellErrors = false;
            this._lsOrDgv6Gr1.ShowCellToolTips = false;
            this._lsOrDgv6Gr1.ShowEditingIcon = false;
            this._lsOrDgv6Gr1.ShowRowErrors = false;
            this._lsOrDgv6Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv6Gr1.TabIndex = 4;
            this._lsOrDgv6Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "№";
            this.dataGridViewTextBoxColumn21.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 24;
            // 
            // dataGridViewComboBoxColumn20
            // 
            this.dataGridViewComboBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn20.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn20.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn20.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn20.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn20.Name = "dataGridViewComboBoxColumn20";
            // 
            // tabPage37
            // 
            this.tabPage37.Controls.Add(this._lsOrDgv7Gr1);
            this.tabPage37.Location = new System.Drawing.Point(4, 49);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Size = new System.Drawing.Size(179, 302);
            this.tabPage37.TabIndex = 6;
            this.tabPage37.Text = "ЛС15";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv7Gr1
            // 
            this._lsOrDgv7Gr1.AllowUserToAddRows = false;
            this._lsOrDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv7Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewComboBoxColumn21});
            this._lsOrDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv7Gr1.MultiSelect = false;
            this._lsOrDgv7Gr1.Name = "_lsOrDgv7Gr1";
            this._lsOrDgv7Gr1.RowHeadersVisible = false;
            this._lsOrDgv7Gr1.RowHeadersWidth = 51;
            this._lsOrDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle133.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle133;
            this._lsOrDgv7Gr1.RowTemplate.Height = 20;
            this._lsOrDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv7Gr1.ShowCellErrors = false;
            this._lsOrDgv7Gr1.ShowCellToolTips = false;
            this._lsOrDgv7Gr1.ShowEditingIcon = false;
            this._lsOrDgv7Gr1.ShowRowErrors = false;
            this._lsOrDgv7Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv7Gr1.TabIndex = 4;
            this._lsOrDgv7Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "№";
            this.dataGridViewTextBoxColumn22.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 24;
            // 
            // dataGridViewComboBoxColumn21
            // 
            this.dataGridViewComboBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn21.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn21.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn21.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn21.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn21.Name = "dataGridViewComboBoxColumn21";
            // 
            // tabPage38
            // 
            this.tabPage38.Controls.Add(this._lsOrDgv8Gr1);
            this.tabPage38.Location = new System.Drawing.Point(4, 49);
            this.tabPage38.Name = "tabPage38";
            this.tabPage38.Size = new System.Drawing.Size(179, 302);
            this.tabPage38.TabIndex = 7;
            this.tabPage38.Text = "ЛС16";
            this.tabPage38.UseVisualStyleBackColor = true;
            // 
            // _lsOrDgv8Gr1
            // 
            this._lsOrDgv8Gr1.AllowUserToAddRows = false;
            this._lsOrDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsOrDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsOrDgv8Gr1.AllowUserToResizeRows = false;
            this._lsOrDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsOrDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsOrDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsOrDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewComboBoxColumn22});
            this._lsOrDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsOrDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsOrDgv8Gr1.MultiSelect = false;
            this._lsOrDgv8Gr1.Name = "_lsOrDgv8Gr1";
            this._lsOrDgv8Gr1.RowHeadersVisible = false;
            this._lsOrDgv8Gr1.RowHeadersWidth = 51;
            this._lsOrDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle134.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsOrDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle134;
            this._lsOrDgv8Gr1.RowTemplate.Height = 20;
            this._lsOrDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsOrDgv8Gr1.ShowCellErrors = false;
            this._lsOrDgv8Gr1.ShowCellToolTips = false;
            this._lsOrDgv8Gr1.ShowEditingIcon = false;
            this._lsOrDgv8Gr1.ShowRowErrors = false;
            this._lsOrDgv8Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsOrDgv8Gr1.TabIndex = 4;
            this._lsOrDgv8Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsOrDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "№";
            this.dataGridViewTextBoxColumn23.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.ReadOnly = true;
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Width = 24;
            // 
            // dataGridViewComboBoxColumn22
            // 
            this.dataGridViewComboBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn22.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn22.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn22.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn22.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn22.Name = "dataGridViewComboBoxColumn22";
            // 
            // _clearCurrentLsnButton
            // 
            this._clearCurrentLsnButton.Location = new System.Drawing.Point(6, 377);
            this._clearCurrentLsnButton.Name = "_clearCurrentLsnButton";
            this._clearCurrentLsnButton.Size = new System.Drawing.Size(180, 23);
            this._clearCurrentLsnButton.TabIndex = 8;
            this._clearCurrentLsnButton.Text = "Очистить текущий сигнал";
            this._clearCurrentLsnButton.UseVisualStyleBackColor = true;
            this._clearCurrentLsnButton.Visible = false;
            this._clearCurrentLsnButton.Click += new System.EventHandler(this._clearCurrentLsnButton_Click);
            // 
            // _clearAllLsnSygnalButton
            // 
            this._clearAllLsnSygnalButton.Location = new System.Drawing.Point(6, 405);
            this._clearAllLsnSygnalButton.Name = "_clearAllLsnSygnalButton";
            this._clearAllLsnSygnalButton.Size = new System.Drawing.Size(180, 23);
            this._clearAllLsnSygnalButton.TabIndex = 8;
            this._clearAllLsnSygnalButton.Text = "Очистить все сигналы";
            this._clearAllLsnSygnalButton.UseVisualStyleBackColor = true;
            this._clearAllLsnSygnalButton.Visible = false;
            this._clearAllLsnSygnalButton.Click += new System.EventHandler(this._clearAllLsnSygnalButton_Click);
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this.tabControl);
            this.groupBox45.Controls.Add(this._clearCurrentLsButton);
            this.groupBox45.Controls.Add(this._clearAllLsSygnalButton);
            this.groupBox45.Location = new System.Drawing.Point(6, 3);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(193, 437);
            this.groupBox45.TabIndex = 9;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Логические сигналы И";
            // 
            // tabControl
            // 
            this.tabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl.Controls.Add(this.tabPage39);
            this.tabControl.Controls.Add(this.tabPage40);
            this.tabControl.Controls.Add(this.tabPage41);
            this.tabControl.Controls.Add(this.tabPage42);
            this.tabControl.Controls.Add(this.tabPage43);
            this.tabControl.Controls.Add(this.tabPage44);
            this.tabControl.Controls.Add(this.tabPage45);
            this.tabControl.Controls.Add(this.tabPage46);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl.Location = new System.Drawing.Point(3, 16);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(187, 355);
            this.tabControl.TabIndex = 2;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage39
            // 
            this.tabPage39.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage39.Controls.Add(this._lsAndDgv1Gr1);
            this.tabPage39.Location = new System.Drawing.Point(4, 49);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage39.Size = new System.Drawing.Size(179, 302);
            this.tabPage39.TabIndex = 0;
            this.tabPage39.Text = "ЛС1";
            // 
            // _lsAndDgv1Gr1
            // 
            this._lsAndDgv1Gr1.AllowUserToAddRows = false;
            this._lsAndDgv1Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv1Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv1Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv1Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv1Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv1Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv1Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewComboBoxColumn23});
            this._lsAndDgv1Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv1Gr1.Location = new System.Drawing.Point(3, 3);
            this._lsAndDgv1Gr1.MultiSelect = false;
            this._lsAndDgv1Gr1.Name = "_lsAndDgv1Gr1";
            this._lsAndDgv1Gr1.RowHeadersVisible = false;
            this._lsAndDgv1Gr1.RowHeadersWidth = 51;
            this._lsAndDgv1Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle135.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv1Gr1.RowsDefaultCellStyle = dataGridViewCellStyle135;
            this._lsAndDgv1Gr1.RowTemplate.Height = 20;
            this._lsAndDgv1Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv1Gr1.ShowCellErrors = false;
            this._lsAndDgv1Gr1.ShowCellToolTips = false;
            this._lsAndDgv1Gr1.ShowEditingIcon = false;
            this._lsAndDgv1Gr1.ShowRowErrors = false;
            this._lsAndDgv1Gr1.Size = new System.Drawing.Size(173, 296);
            this._lsAndDgv1Gr1.TabIndex = 2;
            this._lsAndDgv1Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "№";
            this.dataGridViewTextBoxColumn24.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Width = 24;
            // 
            // dataGridViewComboBoxColumn23
            // 
            this.dataGridViewComboBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn23.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn23.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn23.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn23.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn23.Name = "dataGridViewComboBoxColumn23";
            // 
            // tabPage40
            // 
            this.tabPage40.Controls.Add(this._lsAndDgv2Gr1);
            this.tabPage40.Location = new System.Drawing.Point(4, 49);
            this.tabPage40.Name = "tabPage40";
            this.tabPage40.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage40.Size = new System.Drawing.Size(179, 302);
            this.tabPage40.TabIndex = 1;
            this.tabPage40.Text = "ЛС2";
            this.tabPage40.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv2Gr1
            // 
            this._lsAndDgv2Gr1.AllowUserToAddRows = false;
            this._lsAndDgv2Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv2Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv2Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv2Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv2Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv2Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv2Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewComboBoxColumn24});
            this._lsAndDgv2Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv2Gr1.Location = new System.Drawing.Point(3, 3);
            this._lsAndDgv2Gr1.MultiSelect = false;
            this._lsAndDgv2Gr1.Name = "_lsAndDgv2Gr1";
            this._lsAndDgv2Gr1.RowHeadersVisible = false;
            this._lsAndDgv2Gr1.RowHeadersWidth = 51;
            this._lsAndDgv2Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle136.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv2Gr1.RowsDefaultCellStyle = dataGridViewCellStyle136;
            this._lsAndDgv2Gr1.RowTemplate.Height = 20;
            this._lsAndDgv2Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv2Gr1.ShowCellErrors = false;
            this._lsAndDgv2Gr1.ShowCellToolTips = false;
            this._lsAndDgv2Gr1.ShowEditingIcon = false;
            this._lsAndDgv2Gr1.ShowRowErrors = false;
            this._lsAndDgv2Gr1.Size = new System.Drawing.Size(173, 296);
            this._lsAndDgv2Gr1.TabIndex = 3;
            this._lsAndDgv2Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "№";
            this.dataGridViewTextBoxColumn25.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Width = 24;
            // 
            // dataGridViewComboBoxColumn24
            // 
            this.dataGridViewComboBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn24.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn24.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn24.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn24.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn24.Name = "dataGridViewComboBoxColumn24";
            // 
            // tabPage41
            // 
            this.tabPage41.Controls.Add(this._lsAndDgv3Gr1);
            this.tabPage41.Location = new System.Drawing.Point(4, 49);
            this.tabPage41.Name = "tabPage41";
            this.tabPage41.Size = new System.Drawing.Size(179, 302);
            this.tabPage41.TabIndex = 2;
            this.tabPage41.Text = "ЛС3";
            this.tabPage41.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv3Gr1
            // 
            this._lsAndDgv3Gr1.AllowUserToAddRows = false;
            this._lsAndDgv3Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv3Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv3Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv3Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv3Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv3Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv3Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewComboBoxColumn25});
            this._lsAndDgv3Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv3Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv3Gr1.MultiSelect = false;
            this._lsAndDgv3Gr1.Name = "_lsAndDgv3Gr1";
            this._lsAndDgv3Gr1.RowHeadersVisible = false;
            this._lsAndDgv3Gr1.RowHeadersWidth = 51;
            this._lsAndDgv3Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle137.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv3Gr1.RowsDefaultCellStyle = dataGridViewCellStyle137;
            this._lsAndDgv3Gr1.RowTemplate.Height = 20;
            this._lsAndDgv3Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv3Gr1.ShowCellErrors = false;
            this._lsAndDgv3Gr1.ShowCellToolTips = false;
            this._lsAndDgv3Gr1.ShowEditingIcon = false;
            this._lsAndDgv3Gr1.ShowRowErrors = false;
            this._lsAndDgv3Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv3Gr1.TabIndex = 3;
            this._lsAndDgv3Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.HeaderText = "№";
            this.dataGridViewTextBoxColumn26.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 24;
            // 
            // dataGridViewComboBoxColumn25
            // 
            this.dataGridViewComboBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn25.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn25.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn25.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn25.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn25.Name = "dataGridViewComboBoxColumn25";
            // 
            // tabPage42
            // 
            this.tabPage42.Controls.Add(this._lsAndDgv4Gr1);
            this.tabPage42.Location = new System.Drawing.Point(4, 49);
            this.tabPage42.Name = "tabPage42";
            this.tabPage42.Size = new System.Drawing.Size(179, 302);
            this.tabPage42.TabIndex = 3;
            this.tabPage42.Text = "ЛС4";
            this.tabPage42.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv4Gr1
            // 
            this._lsAndDgv4Gr1.AllowUserToAddRows = false;
            this._lsAndDgv4Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv4Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv4Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv4Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv4Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv4Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv4Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewComboBoxColumn26});
            this._lsAndDgv4Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv4Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv4Gr1.MultiSelect = false;
            this._lsAndDgv4Gr1.Name = "_lsAndDgv4Gr1";
            this._lsAndDgv4Gr1.RowHeadersVisible = false;
            this._lsAndDgv4Gr1.RowHeadersWidth = 51;
            this._lsAndDgv4Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle138.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv4Gr1.RowsDefaultCellStyle = dataGridViewCellStyle138;
            this._lsAndDgv4Gr1.RowTemplate.Height = 20;
            this._lsAndDgv4Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv4Gr1.ShowCellErrors = false;
            this._lsAndDgv4Gr1.ShowCellToolTips = false;
            this._lsAndDgv4Gr1.ShowEditingIcon = false;
            this._lsAndDgv4Gr1.ShowRowErrors = false;
            this._lsAndDgv4Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv4Gr1.TabIndex = 3;
            this._lsAndDgv4Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "№";
            this.dataGridViewTextBoxColumn27.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 24;
            // 
            // dataGridViewComboBoxColumn26
            // 
            this.dataGridViewComboBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn26.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn26.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn26.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn26.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn26.Name = "dataGridViewComboBoxColumn26";
            // 
            // tabPage43
            // 
            this.tabPage43.Controls.Add(this._lsAndDgv5Gr1);
            this.tabPage43.Location = new System.Drawing.Point(4, 49);
            this.tabPage43.Name = "tabPage43";
            this.tabPage43.Size = new System.Drawing.Size(179, 302);
            this.tabPage43.TabIndex = 4;
            this.tabPage43.Text = "ЛС5";
            this.tabPage43.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv5Gr1
            // 
            this._lsAndDgv5Gr1.AllowUserToAddRows = false;
            this._lsAndDgv5Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv5Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv5Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv5Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv5Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv5Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv5Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewComboBoxColumn27});
            this._lsAndDgv5Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv5Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv5Gr1.MultiSelect = false;
            this._lsAndDgv5Gr1.Name = "_lsAndDgv5Gr1";
            this._lsAndDgv5Gr1.RowHeadersVisible = false;
            this._lsAndDgv5Gr1.RowHeadersWidth = 51;
            this._lsAndDgv5Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle139.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv5Gr1.RowsDefaultCellStyle = dataGridViewCellStyle139;
            this._lsAndDgv5Gr1.RowTemplate.Height = 20;
            this._lsAndDgv5Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv5Gr1.ShowCellErrors = false;
            this._lsAndDgv5Gr1.ShowCellToolTips = false;
            this._lsAndDgv5Gr1.ShowEditingIcon = false;
            this._lsAndDgv5Gr1.ShowRowErrors = false;
            this._lsAndDgv5Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv5Gr1.TabIndex = 3;
            this._lsAndDgv5Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "№";
            this.dataGridViewTextBoxColumn28.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Width = 24;
            // 
            // dataGridViewComboBoxColumn27
            // 
            this.dataGridViewComboBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn27.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn27.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn27.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn27.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn27.Name = "dataGridViewComboBoxColumn27";
            // 
            // tabPage44
            // 
            this.tabPage44.Controls.Add(this._lsAndDgv6Gr1);
            this.tabPage44.Location = new System.Drawing.Point(4, 49);
            this.tabPage44.Name = "tabPage44";
            this.tabPage44.Size = new System.Drawing.Size(179, 302);
            this.tabPage44.TabIndex = 5;
            this.tabPage44.Text = "ЛС6";
            this.tabPage44.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv6Gr1
            // 
            this._lsAndDgv6Gr1.AllowUserToAddRows = false;
            this._lsAndDgv6Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv6Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv6Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv6Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv6Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv6Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv6Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewComboBoxColumn28});
            this._lsAndDgv6Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv6Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv6Gr1.MultiSelect = false;
            this._lsAndDgv6Gr1.Name = "_lsAndDgv6Gr1";
            this._lsAndDgv6Gr1.RowHeadersVisible = false;
            this._lsAndDgv6Gr1.RowHeadersWidth = 51;
            this._lsAndDgv6Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle140.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv6Gr1.RowsDefaultCellStyle = dataGridViewCellStyle140;
            this._lsAndDgv6Gr1.RowTemplate.Height = 20;
            this._lsAndDgv6Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv6Gr1.ShowCellErrors = false;
            this._lsAndDgv6Gr1.ShowCellToolTips = false;
            this._lsAndDgv6Gr1.ShowEditingIcon = false;
            this._lsAndDgv6Gr1.ShowRowErrors = false;
            this._lsAndDgv6Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv6Gr1.TabIndex = 3;
            this._lsAndDgv6Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "№";
            this.dataGridViewTextBoxColumn29.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.ReadOnly = true;
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 24;
            // 
            // dataGridViewComboBoxColumn28
            // 
            this.dataGridViewComboBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn28.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn28.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn28.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn28.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn28.Name = "dataGridViewComboBoxColumn28";
            // 
            // tabPage45
            // 
            this.tabPage45.Controls.Add(this._lsAndDgv7Gr1);
            this.tabPage45.Location = new System.Drawing.Point(4, 49);
            this.tabPage45.Name = "tabPage45";
            this.tabPage45.Size = new System.Drawing.Size(179, 302);
            this.tabPage45.TabIndex = 6;
            this.tabPage45.Text = "ЛС7";
            this.tabPage45.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv7Gr1
            // 
            this._lsAndDgv7Gr1.AllowUserToAddRows = false;
            this._lsAndDgv7Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv7Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv7Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv7Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv7Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv7Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv7Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewComboBoxColumn29});
            this._lsAndDgv7Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv7Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv7Gr1.MultiSelect = false;
            this._lsAndDgv7Gr1.Name = "_lsAndDgv7Gr1";
            this._lsAndDgv7Gr1.RowHeadersVisible = false;
            this._lsAndDgv7Gr1.RowHeadersWidth = 51;
            this._lsAndDgv7Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle141.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv7Gr1.RowsDefaultCellStyle = dataGridViewCellStyle141;
            this._lsAndDgv7Gr1.RowTemplate.Height = 20;
            this._lsAndDgv7Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv7Gr1.ShowCellErrors = false;
            this._lsAndDgv7Gr1.ShowCellToolTips = false;
            this._lsAndDgv7Gr1.ShowEditingIcon = false;
            this._lsAndDgv7Gr1.ShowRowErrors = false;
            this._lsAndDgv7Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv7Gr1.TabIndex = 3;
            this._lsAndDgv7Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "№";
            this.dataGridViewTextBoxColumn30.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 24;
            // 
            // dataGridViewComboBoxColumn29
            // 
            this.dataGridViewComboBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn29.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn29.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn29.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn29.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn29.Name = "dataGridViewComboBoxColumn29";
            // 
            // tabPage46
            // 
            this.tabPage46.Controls.Add(this._lsAndDgv8Gr1);
            this.tabPage46.Location = new System.Drawing.Point(4, 49);
            this.tabPage46.Name = "tabPage46";
            this.tabPage46.Size = new System.Drawing.Size(179, 302);
            this.tabPage46.TabIndex = 7;
            this.tabPage46.Text = "ЛС8";
            this.tabPage46.UseVisualStyleBackColor = true;
            // 
            // _lsAndDgv8Gr1
            // 
            this._lsAndDgv8Gr1.AllowUserToAddRows = false;
            this._lsAndDgv8Gr1.AllowUserToDeleteRows = false;
            this._lsAndDgv8Gr1.AllowUserToResizeColumns = false;
            this._lsAndDgv8Gr1.AllowUserToResizeRows = false;
            this._lsAndDgv8Gr1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._lsAndDgv8Gr1.BackgroundColor = System.Drawing.Color.White;
            this._lsAndDgv8Gr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._lsAndDgv8Gr1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewComboBoxColumn30});
            this._lsAndDgv8Gr1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lsAndDgv8Gr1.Location = new System.Drawing.Point(0, 0);
            this._lsAndDgv8Gr1.MultiSelect = false;
            this._lsAndDgv8Gr1.Name = "_lsAndDgv8Gr1";
            this._lsAndDgv8Gr1.RowHeadersVisible = false;
            this._lsAndDgv8Gr1.RowHeadersWidth = 51;
            this._lsAndDgv8Gr1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle142.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._lsAndDgv8Gr1.RowsDefaultCellStyle = dataGridViewCellStyle142;
            this._lsAndDgv8Gr1.RowTemplate.Height = 20;
            this._lsAndDgv8Gr1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._lsAndDgv8Gr1.ShowCellErrors = false;
            this._lsAndDgv8Gr1.ShowCellToolTips = false;
            this._lsAndDgv8Gr1.ShowEditingIcon = false;
            this._lsAndDgv8Gr1.ShowRowErrors = false;
            this._lsAndDgv8Gr1.Size = new System.Drawing.Size(179, 302);
            this._lsAndDgv8Gr1.TabIndex = 3;
            this._lsAndDgv8Gr1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._lsAndDgvGr1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "№";
            this.dataGridViewTextBoxColumn31.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Width = 24;
            // 
            // dataGridViewComboBoxColumn30
            // 
            this.dataGridViewComboBoxColumn30.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn30.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn30.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn30.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn30.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn30.Name = "dataGridViewComboBoxColumn30";
            // 
            // _clearCurrentLsButton
            // 
            this._clearCurrentLsButton.Location = new System.Drawing.Point(7, 377);
            this._clearCurrentLsButton.Name = "_clearCurrentLsButton";
            this._clearCurrentLsButton.Size = new System.Drawing.Size(178, 23);
            this._clearCurrentLsButton.TabIndex = 8;
            this._clearCurrentLsButton.Text = "Очистить текущий сигнал";
            this._clearCurrentLsButton.UseVisualStyleBackColor = true;
            this._clearCurrentLsButton.Visible = false;
            this._clearCurrentLsButton.Click += new System.EventHandler(this._clearCurrentLsButton_Click);
            // 
            // _clearAllLsSygnalButton
            // 
            this._clearAllLsSygnalButton.Location = new System.Drawing.Point(7, 405);
            this._clearAllLsSygnalButton.Name = "_clearAllLsSygnalButton";
            this._clearAllLsSygnalButton.Size = new System.Drawing.Size(179, 23);
            this._clearAllLsSygnalButton.TabIndex = 8;
            this._clearAllLsSygnalButton.Text = "Очистить все сигналы";
            this._clearAllLsSygnalButton.UseVisualStyleBackColor = true;
            this._clearAllLsSygnalButton.Visible = false;
            this._clearAllLsSygnalButton.Click += new System.EventHandler(this._clearAllLsSygnalButton_Click);
            // 
            // _VLSGr
            // 
            this._VLSGr.Controls.Add(this.groupBox32);
            this._VLSGr.Controls.Add(this.groupBox49);
            this._VLSGr.Location = new System.Drawing.Point(4, 25);
            this._VLSGr.Name = "_VLSGr";
            this._VLSGr.Padding = new System.Windows.Forms.Padding(3);
            this._VLSGr.Size = new System.Drawing.Size(956, 443);
            this._VLSGr.TabIndex = 20;
            this._VLSGr.Text = "ВЛС";
            this._VLSGr.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this._wrapBtn);
            this.groupBox32.Controls.Add(this.treeViewForVLS);
            this.groupBox32.Location = new System.Drawing.Point(434, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(240, 437);
            this.groupBox32.TabIndex = 12;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Список всех ВЛС";
            // 
            // _wrapBtn
            // 
            this._wrapBtn.Location = new System.Drawing.Point(67, 411);
            this._wrapBtn.Name = "_wrapBtn";
            this._wrapBtn.Size = new System.Drawing.Size(102, 23);
            this._wrapBtn.TabIndex = 1;
            this._wrapBtn.Text = "Свернуть";
            this._wrapBtn.UseVisualStyleBackColor = true;
            this._wrapBtn.Click += new System.EventHandler(this._wrapBtn_Click);
            // 
            // treeViewForVLS
            // 
            this.treeViewForVLS.Location = new System.Drawing.Point(6, 16);
            this.treeViewForVLS.Name = "treeViewForVLS";
            this.treeViewForVLS.Size = new System.Drawing.Size(216, 382);
            this.treeViewForVLS.TabIndex = 0;
            this.treeViewForVLS.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForVLS_NodeMouseClick_1);
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.tabControl2);
            this.groupBox49.Controls.Add(this._resetCurrentVlsButton);
            this.groupBox49.Controls.Add(this._resetAllVlsButton);
            this.groupBox49.Location = new System.Drawing.Point(3, 3);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(405, 436);
            this.groupBox49.TabIndex = 11;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "ВЛС";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.VLS1);
            this.tabControl2.Controls.Add(this.VLS2);
            this.tabControl2.Controls.Add(this.VLS3);
            this.tabControl2.Controls.Add(this.VLS4);
            this.tabControl2.Controls.Add(this.VLS5);
            this.tabControl2.Controls.Add(this.VLS6);
            this.tabControl2.Controls.Add(this.VLS7);
            this.tabControl2.Controls.Add(this.VLS8);
            this.tabControl2.Controls.Add(this.VLS9);
            this.tabControl2.Controls.Add(this.VLS10);
            this.tabControl2.Controls.Add(this.VLS11);
            this.tabControl2.Controls.Add(this.VLS12);
            this.tabControl2.Controls.Add(this.VLS13);
            this.tabControl2.Controls.Add(this.VLS14);
            this.tabControl2.Controls.Add(this.VLS15);
            this.tabControl2.Controls.Add(this.VLS16);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(399, 389);
            this.tabControl2.TabIndex = 0;
            this.tabControl2.SelectedIndexChanged += new System.EventHandler(this.tabControl2_SelectedIndexChanged);
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLSclb1Gr1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(391, 336);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLSclb1Gr1
            // 
            this.VLSclb1Gr1.CheckOnClick = true;
            this.VLSclb1Gr1.FormattingEnabled = true;
            this.VLSclb1Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb1Gr1.Name = "VLSclb1Gr1";
            this.VLSclb1Gr1.ScrollAlwaysVisible = true;
            this.VLSclb1Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb1Gr1.TabIndex = 6;
            this.VLSclb1Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLSclb2Gr1);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(391, 336);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLSclb2Gr1
            // 
            this.VLSclb2Gr1.CheckOnClick = true;
            this.VLSclb2Gr1.FormattingEnabled = true;
            this.VLSclb2Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb2Gr1.Name = "VLSclb2Gr1";
            this.VLSclb2Gr1.ScrollAlwaysVisible = true;
            this.VLSclb2Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb2Gr1.TabIndex = 6;
            this.VLSclb2Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLSclb3Gr1);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(391, 336);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLSclb3Gr1
            // 
            this.VLSclb3Gr1.CheckOnClick = true;
            this.VLSclb3Gr1.FormattingEnabled = true;
            this.VLSclb3Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb3Gr1.Name = "VLSclb3Gr1";
            this.VLSclb3Gr1.ScrollAlwaysVisible = true;
            this.VLSclb3Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb3Gr1.TabIndex = 6;
            this.VLSclb3Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLSclb4Gr1);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(391, 336);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLSclb4Gr1
            // 
            this.VLSclb4Gr1.CheckOnClick = true;
            this.VLSclb4Gr1.FormattingEnabled = true;
            this.VLSclb4Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb4Gr1.Name = "VLSclb4Gr1";
            this.VLSclb4Gr1.ScrollAlwaysVisible = true;
            this.VLSclb4Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb4Gr1.TabIndex = 6;
            this.VLSclb4Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLSclb5Gr1);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(391, 336);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLSclb5Gr1
            // 
            this.VLSclb5Gr1.CheckOnClick = true;
            this.VLSclb5Gr1.FormattingEnabled = true;
            this.VLSclb5Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb5Gr1.Name = "VLSclb5Gr1";
            this.VLSclb5Gr1.ScrollAlwaysVisible = true;
            this.VLSclb5Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb5Gr1.TabIndex = 6;
            this.VLSclb5Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLSclb6Gr1);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(391, 336);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLSclb6Gr1
            // 
            this.VLSclb6Gr1.CheckOnClick = true;
            this.VLSclb6Gr1.FormattingEnabled = true;
            this.VLSclb6Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb6Gr1.Name = "VLSclb6Gr1";
            this.VLSclb6Gr1.ScrollAlwaysVisible = true;
            this.VLSclb6Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb6Gr1.TabIndex = 6;
            this.VLSclb6Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLSclb7Gr1);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(391, 336);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLSclb7Gr1
            // 
            this.VLSclb7Gr1.CheckOnClick = true;
            this.VLSclb7Gr1.FormattingEnabled = true;
            this.VLSclb7Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb7Gr1.Name = "VLSclb7Gr1";
            this.VLSclb7Gr1.ScrollAlwaysVisible = true;
            this.VLSclb7Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb7Gr1.TabIndex = 6;
            this.VLSclb7Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLSclb8Gr1);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(391, 336);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLSclb8Gr1
            // 
            this.VLSclb8Gr1.CheckOnClick = true;
            this.VLSclb8Gr1.FormattingEnabled = true;
            this.VLSclb8Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb8Gr1.Name = "VLSclb8Gr1";
            this.VLSclb8Gr1.ScrollAlwaysVisible = true;
            this.VLSclb8Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb8Gr1.TabIndex = 6;
            this.VLSclb8Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLSclb9Gr1);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(391, 336);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLSclb9Gr1
            // 
            this.VLSclb9Gr1.CheckOnClick = true;
            this.VLSclb9Gr1.FormattingEnabled = true;
            this.VLSclb9Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb9Gr1.Name = "VLSclb9Gr1";
            this.VLSclb9Gr1.ScrollAlwaysVisible = true;
            this.VLSclb9Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb9Gr1.TabIndex = 6;
            this.VLSclb9Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLSclb10Gr1);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(391, 336);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLSclb10Gr1
            // 
            this.VLSclb10Gr1.CheckOnClick = true;
            this.VLSclb10Gr1.FormattingEnabled = true;
            this.VLSclb10Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb10Gr1.Name = "VLSclb10Gr1";
            this.VLSclb10Gr1.ScrollAlwaysVisible = true;
            this.VLSclb10Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb10Gr1.TabIndex = 6;
            this.VLSclb10Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLSclb11Gr1);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(391, 336);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLSclb11Gr1
            // 
            this.VLSclb11Gr1.CheckOnClick = true;
            this.VLSclb11Gr1.FormattingEnabled = true;
            this.VLSclb11Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb11Gr1.Name = "VLSclb11Gr1";
            this.VLSclb11Gr1.ScrollAlwaysVisible = true;
            this.VLSclb11Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb11Gr1.TabIndex = 6;
            this.VLSclb11Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLSclb12Gr1);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(391, 336);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLSclb12Gr1
            // 
            this.VLSclb12Gr1.CheckOnClick = true;
            this.VLSclb12Gr1.FormattingEnabled = true;
            this.VLSclb12Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb12Gr1.Name = "VLSclb12Gr1";
            this.VLSclb12Gr1.ScrollAlwaysVisible = true;
            this.VLSclb12Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb12Gr1.TabIndex = 6;
            this.VLSclb12Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLSclb13Gr1);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(391, 336);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLSclb13Gr1
            // 
            this.VLSclb13Gr1.CheckOnClick = true;
            this.VLSclb13Gr1.FormattingEnabled = true;
            this.VLSclb13Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb13Gr1.Name = "VLSclb13Gr1";
            this.VLSclb13Gr1.ScrollAlwaysVisible = true;
            this.VLSclb13Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb13Gr1.TabIndex = 6;
            this.VLSclb13Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLSclb14Gr1);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(391, 336);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLSclb14Gr1
            // 
            this.VLSclb14Gr1.CheckOnClick = true;
            this.VLSclb14Gr1.FormattingEnabled = true;
            this.VLSclb14Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb14Gr1.Name = "VLSclb14Gr1";
            this.VLSclb14Gr1.ScrollAlwaysVisible = true;
            this.VLSclb14Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb14Gr1.TabIndex = 6;
            this.VLSclb14Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLSclb15Gr1);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(391, 336);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLSclb15Gr1
            // 
            this.VLSclb15Gr1.CheckOnClick = true;
            this.VLSclb15Gr1.FormattingEnabled = true;
            this.VLSclb15Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb15Gr1.Name = "VLSclb15Gr1";
            this.VLSclb15Gr1.ScrollAlwaysVisible = true;
            this.VLSclb15Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb15Gr1.TabIndex = 6;
            this.VLSclb15Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLSclb16Gr1);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(391, 336);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // VLSclb16Gr1
            // 
            this.VLSclb16Gr1.CheckOnClick = true;
            this.VLSclb16Gr1.FormattingEnabled = true;
            this.VLSclb16Gr1.Location = new System.Drawing.Point(110, 3);
            this.VLSclb16Gr1.Name = "VLSclb16Gr1";
            this.VLSclb16Gr1.ScrollAlwaysVisible = true;
            this.VLSclb16Gr1.Size = new System.Drawing.Size(191, 304);
            this.VLSclb16Gr1.TabIndex = 6;
            this.VLSclb16Gr1.SelectedValueChanged += new System.EventHandler(this.VLSclbGr1_SelectedValueChanged);
            // 
            // _resetCurrentVlsButton
            // 
            this._resetCurrentVlsButton.Location = new System.Drawing.Point(6, 411);
            this._resetCurrentVlsButton.Name = "_resetCurrentVlsButton";
            this._resetCurrentVlsButton.Size = new System.Drawing.Size(172, 23);
            this._resetCurrentVlsButton.TabIndex = 10;
            this._resetCurrentVlsButton.Text = "Очистить текущий сигнал";
            this._resetCurrentVlsButton.UseVisualStyleBackColor = true;
            this._resetCurrentVlsButton.Visible = false;
            this._resetCurrentVlsButton.Click += new System.EventHandler(this._resetCurrentVlsButton_Click);
            // 
            // _resetAllVlsButton
            // 
            this._resetAllVlsButton.Location = new System.Drawing.Point(227, 411);
            this._resetAllVlsButton.Name = "_resetAllVlsButton";
            this._resetAllVlsButton.Size = new System.Drawing.Size(172, 23);
            this._resetAllVlsButton.TabIndex = 9;
            this._resetAllVlsButton.Text = "Очистить все сигналы";
            this._resetAllVlsButton.UseVisualStyleBackColor = true;
            this._resetAllVlsButton.Visible = false;
            this._resetAllVlsButton.Click += new System.EventHandler(this._resetAllVlsButton_Click);
            // 
            // _apvGr1
            // 
            this._apvGr1.Controls.Add(this.groupBox50);
            this._apvGr1.Controls.Add(this.groupBox12);
            this._apvGr1.Location = new System.Drawing.Point(4, 25);
            this._apvGr1.Name = "_apvGr1";
            this._apvGr1.Size = new System.Drawing.Size(956, 443);
            this._apvGr1.TabIndex = 16;
            this._apvGr1.Text = "АПВ и АВР";
            this._apvGr1.UseVisualStyleBackColor = true;
            // 
            // groupBox50
            // 
            this.groupBox50.Controls.Add(this.label166);
            this.groupBox50.Controls.Add(this.label168);
            this.groupBox50.Controls.Add(this._avrClear);
            this.groupBox50.Controls.Add(this.label173);
            this.groupBox50.Controls.Add(this._avrTOff);
            this.groupBox50.Controls.Add(this.label174);
            this.groupBox50.Controls.Add(this._avrTBack);
            this.groupBox50.Controls.Add(this.label175);
            this.groupBox50.Controls.Add(this._avrBack);
            this.groupBox50.Controls.Add(this.label176);
            this.groupBox50.Controls.Add(this._avrTSr);
            this.groupBox50.Controls.Add(this.label177);
            this.groupBox50.Controls.Add(this._avrResolve);
            this.groupBox50.Controls.Add(this.label178);
            this.groupBox50.Controls.Add(this._avrBlockClear);
            this.groupBox50.Controls.Add(this.label179);
            this.groupBox50.Controls.Add(this._avrBlocking);
            this.groupBox50.Controls.Add(this.label180);
            this.groupBox50.Controls.Add(this._avrSIGNOn);
            this.groupBox50.Controls.Add(this.label181);
            this.groupBox50.Controls.Add(this._avrByDiff);
            this.groupBox50.Controls.Add(this.label182);
            this.groupBox50.Controls.Add(this._avrBySelfOff);
            this.groupBox50.Controls.Add(this.label183);
            this.groupBox50.Controls.Add(this._avrByOff);
            this.groupBox50.Controls.Add(this.label184);
            this.groupBox50.Controls.Add(this._avrBySignal);
            this.groupBox50.Location = new System.Drawing.Point(257, 3);
            this.groupBox50.Name = "groupBox50";
            this.groupBox50.Size = new System.Drawing.Size(241, 302);
            this.groupBox50.TabIndex = 2;
            this.groupBox50.TabStop = false;
            this.groupBox50.Text = "АВР";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(5, 35);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(71, 13);
            this.label166.TabIndex = 26;
            this.label166.Text = "(по питанию)";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(6, 275);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(38, 13);
            this.label168.TabIndex = 25;
            this.label168.Text = "Сброс";
            // 
            // _avrClear
            // 
            this._avrClear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrClear.FormattingEnabled = true;
            this._avrClear.Location = new System.Drawing.Point(122, 272);
            this._avrClear.Name = "_avrClear";
            this._avrClear.Size = new System.Drawing.Size(113, 21);
            this._avrClear.TabIndex = 24;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(6, 256);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(53, 13);
            this.label173.TabIndex = 23;
            this.label173.Text = "tоткл, мс";
            // 
            // _avrTOff
            // 
            this._avrTOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTOff.Location = new System.Drawing.Point(122, 253);
            this._avrTOff.Name = "_avrTOff";
            this._avrTOff.Size = new System.Drawing.Size(113, 20);
            this._avrTOff.TabIndex = 22;
            this._avrTOff.Tag = "3276700";
            this._avrTOff.Text = "0";
            this._avrTOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(6, 237);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(48, 13);
            this.label174.TabIndex = 21;
            this.label174.Text = "tвоз, мс";
            // 
            // _avrTBack
            // 
            this._avrTBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTBack.Location = new System.Drawing.Point(122, 234);
            this._avrTBack.Name = "_avrTBack";
            this._avrTBack.Size = new System.Drawing.Size(113, 20);
            this._avrTBack.TabIndex = 20;
            this._avrTBack.Tag = "3276700";
            this._avrTBack.Text = "0";
            this._avrTBack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(6, 217);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(49, 13);
            this.label175.TabIndex = 19;
            this.label175.Text = "Возврат";
            // 
            // _avrBack
            // 
            this._avrBack.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBack.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBack.FormattingEnabled = true;
            this._avrBack.Location = new System.Drawing.Point(122, 214);
            this._avrBack.Name = "_avrBack";
            this._avrBack.Size = new System.Drawing.Size(113, 21);
            this._avrBack.TabIndex = 18;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(6, 198);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(42, 13);
            this.label176.TabIndex = 17;
            this.label176.Text = "tср, мс";
            // 
            // _avrTSr
            // 
            this._avrTSr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTSr.Location = new System.Drawing.Point(122, 195);
            this._avrTSr.Name = "_avrTSr";
            this._avrTSr.Size = new System.Drawing.Size(113, 20);
            this._avrTSr.TabIndex = 16;
            this._avrTSr.Tag = "3276700";
            this._avrTSr.Text = "0";
            this._avrTSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(6, 178);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(87, 13);
            this.label177.TabIndex = 15;
            this.label177.Text = "АВР разрешено";
            // 
            // _avrResolve
            // 
            this._avrResolve.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrResolve.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrResolve.FormattingEnabled = true;
            this._avrResolve.Location = new System.Drawing.Point(122, 175);
            this._avrResolve.Name = "_avrResolve";
            this._avrResolve.Size = new System.Drawing.Size(113, 21);
            this._avrResolve.TabIndex = 14;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(6, 158);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(101, 13);
            this.label178.TabIndex = 13;
            this.label178.Text = "Сброс блокировки";
            // 
            // _avrBlockClear
            // 
            this._avrBlockClear.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBlockClear.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBlockClear.FormattingEnabled = true;
            this._avrBlockClear.Location = new System.Drawing.Point(122, 155);
            this._avrBlockClear.Name = "_avrBlockClear";
            this._avrBlockClear.Size = new System.Drawing.Size(113, 21);
            this._avrBlockClear.TabIndex = 12;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(6, 138);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(68, 13);
            this.label179.TabIndex = 11;
            this.label179.Text = "Блокировка";
            // 
            // _avrBlocking
            // 
            this._avrBlocking.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrBlocking.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrBlocking.FormattingEnabled = true;
            this._avrBlocking.Location = new System.Drawing.Point(122, 135);
            this._avrBlocking.Name = "_avrBlocking";
            this._avrBlocking.Size = new System.Drawing.Size(113, 21);
            this._avrBlocking.TabIndex = 10;
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(6, 118);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(75, 13);
            this.label180.TabIndex = 9;
            this.label180.Text = "Сигнал пуска";
            // 
            // _avrSIGNOn
            // 
            this._avrSIGNOn.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._avrSIGNOn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._avrSIGNOn.FormattingEnabled = true;
            this._avrSIGNOn.Location = new System.Drawing.Point(122, 115);
            this._avrSIGNOn.Name = "_avrSIGNOn";
            this._avrSIGNOn.Size = new System.Drawing.Size(113, 21);
            this._avrSIGNOn.TabIndex = 8;
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(6, 98);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(62, 13);
            this.label181.TabIndex = 7;
            this.label181.Text = "По защите";
            // 
            // _avrByDiff
            // 
            this._avrByDiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByDiff.FormattingEnabled = true;
            this._avrByDiff.Location = new System.Drawing.Point(122, 95);
            this._avrByDiff.Name = "_avrByDiff";
            this._avrByDiff.Size = new System.Drawing.Size(113, 21);
            this._avrByDiff.TabIndex = 6;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(6, 78);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(112, 13);
            this.label182.TabIndex = 5;
            this.label182.Text = "По самоотключению";
            // 
            // _avrBySelfOff
            // 
            this._avrBySelfOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySelfOff.FormattingEnabled = true;
            this._avrBySelfOff.Location = new System.Drawing.Point(122, 75);
            this._avrBySelfOff.Name = "_avrBySelfOff";
            this._avrBySelfOff.Size = new System.Drawing.Size(113, 21);
            this._avrBySelfOff.TabIndex = 4;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(6, 58);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(86, 13);
            this.label183.TabIndex = 3;
            this.label183.Text = "По отключению";
            // 
            // _avrByOff
            // 
            this._avrByOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByOff.FormattingEnabled = true;
            this._avrByOff.Location = new System.Drawing.Point(122, 55);
            this._avrByOff.Name = "_avrByOff";
            this._avrByOff.Size = new System.Drawing.Size(113, 21);
            this._avrByOff.TabIndex = 2;
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(6, 21);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(64, 13);
            this.label184.TabIndex = 1;
            this.label184.Text = "От сигнала";
            // 
            // _avrBySignal
            // 
            this._avrBySignal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySignal.FormattingEnabled = true;
            this._avrBySignal.Location = new System.Drawing.Point(122, 25);
            this._avrBySignal.Name = "_avrBySignal";
            this._avrBySignal.Size = new System.Drawing.Size(113, 21);
            this._avrBySignal.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._blokFromUrov);
            this.groupBox12.Controls.Add(this.label142);
            this.groupBox12.Controls.Add(this.label143);
            this.groupBox12.Controls.Add(this._apvKrat4Gr1);
            this.groupBox12.Controls.Add(this._apvKrat3Gr1);
            this.groupBox12.Controls.Add(this.label144);
            this.groupBox12.Controls.Add(this.label145);
            this.groupBox12.Controls.Add(this.label146);
            this.groupBox12.Controls.Add(this.label147);
            this.groupBox12.Controls.Add(this.label94);
            this.groupBox12.Controls.Add(this.label148);
            this.groupBox12.Controls.Add(this._apvSwitchOffGr1);
            this.groupBox12.Controls.Add(this._apvKrat2Gr1);
            this.groupBox12.Controls.Add(this._apvKrat1Gr1);
            this.groupBox12.Controls.Add(this._apvTreadyGr1);
            this.groupBox12.Controls.Add(this._apvTblockGr1);
            this.groupBox12.Controls.Add(this._apvBlockGr1);
            this.groupBox12.Controls.Add(this._timeDisable);
            this.groupBox12.Controls.Add(this.label87);
            this.groupBox12.Controls.Add(this._typeDisable);
            this.groupBox12.Controls.Add(this.label95);
            this.groupBox12.Controls.Add(this._disableApv);
            this.groupBox12.Controls.Add(this.label149);
            this.groupBox12.Controls.Add(this._apvModeGr1);
            this.groupBox12.Controls.Add(this.label150);
            this.groupBox12.Location = new System.Drawing.Point(3, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(248, 284);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "АПВ";
            // 
            // _blokFromUrov
            // 
            this._blokFromUrov.AutoSize = true;
            this._blokFromUrov.Location = new System.Drawing.Point(9, 43);
            this._blokFromUrov.Name = "_blokFromUrov";
            this._blokFromUrov.Size = new System.Drawing.Size(134, 17);
            this._blokFromUrov.TabIndex = 18;
            this._blokFromUrov.Text = "Блокировка от УРОВ";
            this._blokFromUrov.UseVisualStyleBackColor = true;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(6, 240);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(60, 13);
            this.label142.TabIndex = 17;
            this.label142.Text = "4 Крат, мс";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(6, 221);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(60, 13);
            this.label143.TabIndex = 16;
            this.label143.Text = "3 Крат, мс";
            // 
            // _apvKrat4Gr1
            // 
            this._apvKrat4Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat4Gr1.Location = new System.Drawing.Point(95, 238);
            this._apvKrat4Gr1.Name = "_apvKrat4Gr1";
            this._apvKrat4Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat4Gr1.TabIndex = 15;
            this._apvKrat4Gr1.Tag = "3276700";
            this._apvKrat4Gr1.Text = "20";
            this._apvKrat4Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvKrat3Gr1
            // 
            this._apvKrat3Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat3Gr1.Location = new System.Drawing.Point(95, 219);
            this._apvKrat3Gr1.Name = "_apvKrat3Gr1";
            this._apvKrat3Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat3Gr1.TabIndex = 14;
            this._apvKrat3Gr1.Tag = "3276700";
            this._apvKrat3Gr1.Text = "20";
            this._apvKrat3Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(6, 259);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(73, 13);
            this.label144.TabIndex = 13;
            this.label144.Text = "Самоотключ.";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(6, 203);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(60, 13);
            this.label145.TabIndex = 12;
            this.label145.Text = "2 Крат, мс";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(6, 184);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(60, 13);
            this.label146.TabIndex = 11;
            this.label146.Text = "1 Крат, мс";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(6, 165);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(64, 13);
            this.label147.TabIndex = 10;
            this.label147.Text = "t готов., мс";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(6, 146);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(60, 13);
            this.label94.TabIndex = 9;
            this.label94.Text = "t блок., мс";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(6, 87);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(74, 13);
            this.label148.TabIndex = 9;
            this.label148.Text = "t запрета, мс";
            // 
            // _apvSwitchOffGr1
            // 
            this._apvSwitchOffGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvSwitchOffGr1.FormattingEnabled = true;
            this._apvSwitchOffGr1.Location = new System.Drawing.Point(95, 257);
            this._apvSwitchOffGr1.Name = "_apvSwitchOffGr1";
            this._apvSwitchOffGr1.Size = new System.Drawing.Size(147, 21);
            this._apvSwitchOffGr1.TabIndex = 8;
            // 
            // _apvKrat2Gr1
            // 
            this._apvKrat2Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat2Gr1.Location = new System.Drawing.Point(95, 200);
            this._apvKrat2Gr1.Name = "_apvKrat2Gr1";
            this._apvKrat2Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat2Gr1.TabIndex = 7;
            this._apvKrat2Gr1.Tag = "3276700";
            this._apvKrat2Gr1.Text = "20";
            this._apvKrat2Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvKrat1Gr1
            // 
            this._apvKrat1Gr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvKrat1Gr1.Location = new System.Drawing.Point(95, 181);
            this._apvKrat1Gr1.Name = "_apvKrat1Gr1";
            this._apvKrat1Gr1.Size = new System.Drawing.Size(82, 20);
            this._apvKrat1Gr1.TabIndex = 6;
            this._apvKrat1Gr1.Tag = "3276700";
            this._apvKrat1Gr1.Text = "20";
            this._apvKrat1Gr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTreadyGr1
            // 
            this._apvTreadyGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTreadyGr1.Location = new System.Drawing.Point(95, 162);
            this._apvTreadyGr1.Name = "_apvTreadyGr1";
            this._apvTreadyGr1.Size = new System.Drawing.Size(82, 20);
            this._apvTreadyGr1.TabIndex = 5;
            this._apvTreadyGr1.Tag = "3276700";
            this._apvTreadyGr1.Text = "20";
            this._apvTreadyGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTblockGr1
            // 
            this._apvTblockGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTblockGr1.Location = new System.Drawing.Point(95, 143);
            this._apvTblockGr1.Name = "_apvTblockGr1";
            this._apvTblockGr1.Size = new System.Drawing.Size(82, 20);
            this._apvTblockGr1.TabIndex = 4;
            this._apvTblockGr1.Tag = "3276700";
            this._apvTblockGr1.Text = "20";
            this._apvTblockGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvBlockGr1
            // 
            this._apvBlockGr1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._apvBlockGr1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._apvBlockGr1.FormattingEnabled = true;
            this._apvBlockGr1.Location = new System.Drawing.Point(95, 123);
            this._apvBlockGr1.Name = "_apvBlockGr1";
            this._apvBlockGr1.Size = new System.Drawing.Size(147, 21);
            this._apvBlockGr1.TabIndex = 3;
            // 
            // _timeDisable
            // 
            this._timeDisable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._timeDisable.Location = new System.Drawing.Point(95, 84);
            this._timeDisable.Name = "_timeDisable";
            this._timeDisable.Size = new System.Drawing.Size(82, 20);
            this._timeDisable.TabIndex = 4;
            this._timeDisable.Tag = "3276700";
            this._timeDisable.Text = "20";
            this._timeDisable.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 126);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(68, 13);
            this.label87.TabIndex = 2;
            this.label87.Text = "Блокировка";
            // 
            // _typeDisable
            // 
            this._typeDisable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typeDisable.FormattingEnabled = true;
            this._typeDisable.Location = new System.Drawing.Point(95, 103);
            this._typeDisable.Name = "_typeDisable";
            this._typeDisable.Size = new System.Drawing.Size(147, 21);
            this._typeDisable.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(6, 106);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(70, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Вид запрета";
            // 
            // _disableApv
            // 
            this._disableApv.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._disableApv.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._disableApv.FormattingEnabled = true;
            this._disableApv.Location = new System.Drawing.Point(95, 64);
            this._disableApv.Name = "_disableApv";
            this._disableApv.Size = new System.Drawing.Size(147, 21);
            this._disableApv.TabIndex = 3;
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(6, 67);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(68, 13);
            this.label149.TabIndex = 2;
            this.label149.Text = "Запрет АПВ";
            // 
            // _apvModeGr1
            // 
            this._apvModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvModeGr1.FormattingEnabled = true;
            this._apvModeGr1.Location = new System.Drawing.Point(95, 19);
            this._apvModeGr1.Name = "_apvModeGr1";
            this._apvModeGr1.Size = new System.Drawing.Size(147, 21);
            this._apvModeGr1.TabIndex = 1;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(6, 22);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(42, 13);
            this.label150.TabIndex = 0;
            this.label150.Text = "Режим";
            // 
            // _ksuppnGr1
            // 
            this._ksuppnGr1.Controls.Add(this.label82);
            this._ksuppnGr1.Controls.Add(this.groupBox39);
            this._ksuppnGr1.Controls.Add(this.groupBox35);
            this._ksuppnGr1.Controls.Add(this.groupBox34);
            this._ksuppnGr1.Controls.Add(this.label30);
            this._ksuppnGr1.Location = new System.Drawing.Point(4, 25);
            this._ksuppnGr1.Name = "_ksuppnGr1";
            this._ksuppnGr1.Padding = new System.Windows.Forms.Padding(3);
            this._ksuppnGr1.Size = new System.Drawing.Size(956, 443);
            this._ksuppnGr1.TabIndex = 12;
            this._ksuppnGr1.Text = "КС и УППН";
            this._ksuppnGr1.UseVisualStyleBackColor = true;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(18, 3);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(393, 13);
            this.label82.TabIndex = 28;
            this.label82.Text = "Контроль синхронизма и условий постановки под напряжение (КС и УППН)";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._blockTNauto);
            this.groupBox39.Controls.Add(this.groupBox40);
            this.groupBox39.Controls.Add(this.groupBox41);
            this.groupBox39.Controls.Add(this.groupBox42);
            this.groupBox39.Controls.Add(this._sinhrAutoUmaxGr1);
            this.groupBox39.Controls.Add(this._sinhrAutoModeGr1);
            this.groupBox39.Controls.Add(this._dUmaxAutoLabel);
            this.groupBox39.Controls.Add(this.label75);
            this.groupBox39.Location = new System.Drawing.Point(592, 23);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(288, 303);
            this.groupBox39.TabIndex = 17;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Уставки автоматического включения";
            // 
            // _blockTNauto
            // 
            this._blockTNauto.AutoSize = true;
            this._blockTNauto.Location = new System.Drawing.Point(12, 43);
            this._blockTNauto.Name = "_blockTNauto";
            this._blockTNauto.Size = new System.Drawing.Size(200, 17);
            this._blockTNauto.TabIndex = 34;
            this._blockTNauto.Text = "Блокировка по неисправности ТН";
            this._blockTNauto.UseVisualStyleBackColor = true;
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._catchSinchrAuto);
            this.groupBox40.Controls.Add(this._sinhrAutodFnoGr1);
            this.groupBox40.Controls.Add(this.label17);
            this.groupBox40.Location = new System.Drawing.Point(6, 252);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(276, 45);
            this.groupBox40.TabIndex = 33;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "       Улавливание синхронизма(несихр. режим)";
            // 
            // _catchSinchrAuto
            // 
            this._catchSinchrAuto.AutoSize = true;
            this._catchSinchrAuto.Location = new System.Drawing.Point(6, 0);
            this._catchSinchrAuto.Name = "_catchSinchrAuto";
            this._catchSinchrAuto.Size = new System.Drawing.Size(15, 14);
            this._catchSinchrAuto.TabIndex = 25;
            this._catchSinchrAuto.UseVisualStyleBackColor = true;
            // 
            // _sinhrAutodFnoGr1
            // 
            this._sinhrAutodFnoGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFnoGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodFnoGr1.Name = "_sinhrAutodFnoGr1";
            this._sinhrAutodFnoGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFnoGr1.TabIndex = 23;
            this._sinhrAutodFnoGr1.Tag = "40";
            this._sinhrAutodFnoGr1.Text = "0";
            this._sinhrAutodFnoGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "dF, Гц";
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this._waitSinchrAuto);
            this.groupBox41.Controls.Add(this._sinhrAutodFGr1);
            this.groupBox41.Controls.Add(this.label61);
            this.groupBox41.Controls.Add(this.label62);
            this.groupBox41.Controls.Add(this._sinhrAutodFiGr1);
            this.groupBox41.Location = new System.Drawing.Point(6, 184);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(276, 64);
            this.groupBox41.TabIndex = 32;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "       Ожидание синхронизма(синхр. режим)";
            // 
            // _waitSinchrAuto
            // 
            this._waitSinchrAuto.AutoSize = true;
            this._waitSinchrAuto.Location = new System.Drawing.Point(6, 0);
            this._waitSinchrAuto.Name = "_waitSinchrAuto";
            this._waitSinchrAuto.Size = new System.Drawing.Size(15, 14);
            this._waitSinchrAuto.TabIndex = 25;
            this._waitSinchrAuto.UseVisualStyleBackColor = true;
            // 
            // _sinhrAutodFGr1
            // 
            this._sinhrAutodFGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodFGr1.Name = "_sinhrAutodFGr1";
            this._sinhrAutodFGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFGr1.TabIndex = 23;
            this._sinhrAutodFGr1.Tag = "40";
            this._sinhrAutodFGr1.Text = "0";
            this._sinhrAutodFGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(43, 21);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(37, 13);
            this.label61.TabIndex = 12;
            this.label61.Text = "dF, Гц";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(43, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(47, 13);
            this.label62.TabIndex = 13;
            this.label62.Text = "dfi, град";
            // 
            // _sinhrAutodFiGr1
            // 
            this._sinhrAutodFiGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFiGr1.Location = new System.Drawing.Point(99, 38);
            this._sinhrAutodFiGr1.Name = "_sinhrAutodFiGr1";
            this._sinhrAutodFiGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFiGr1.TabIndex = 24;
            this._sinhrAutodFiGr1.Tag = "3276700";
            this._sinhrAutodFiGr1.Text = "0";
            this._sinhrAutodFiGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this._sinhrAutoNoNoGr1);
            this.groupBox42.Controls.Add(this._sinhrAutoYesNoGr1);
            this.groupBox42.Controls.Add(this._sinhrAutoNoYesGr1);
            this.groupBox42.Controls.Add(this.label66);
            this.groupBox42.Controls.Add(this.label72);
            this.groupBox42.Controls.Add(this.label73);
            this.groupBox42.Location = new System.Drawing.Point(6, 94);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(276, 82);
            this.groupBox42.TabIndex = 31;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Разрешение включения";
            // 
            // _sinhrAutoNoNoGr1
            // 
            this._sinhrAutoNoNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoNoGr1.FormattingEnabled = true;
            this._sinhrAutoNoNoGr1.Location = new System.Drawing.Point(99, 54);
            this._sinhrAutoNoNoGr1.Name = "_sinhrAutoNoNoGr1";
            this._sinhrAutoNoNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoNoGr1.TabIndex = 35;
            // 
            // _sinhrAutoYesNoGr1
            // 
            this._sinhrAutoYesNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoYesNoGr1.FormattingEnabled = true;
            this._sinhrAutoYesNoGr1.Location = new System.Drawing.Point(99, 34);
            this._sinhrAutoYesNoGr1.Name = "_sinhrAutoYesNoGr1";
            this._sinhrAutoYesNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoYesNoGr1.TabIndex = 34;
            // 
            // _sinhrAutoNoYesGr1
            // 
            this._sinhrAutoNoYesGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoYesGr1.FormattingEnabled = true;
            this._sinhrAutoNoYesGr1.Location = new System.Drawing.Point(99, 14);
            this._sinhrAutoNoYesGr1.Name = "_sinhrAutoNoYesGr1";
            this._sinhrAutoNoYesGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoYesGr1.TabIndex = 33;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 57);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(83, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "Uш нет, Uл нет";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(6, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(89, 13);
            this.label72.TabIndex = 31;
            this.label72.Text = "Uш есть, Uл нет";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 17);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(89, 13);
            this.label73.TabIndex = 30;
            this.label73.Text = "Uш нет, Uл есть";
            // 
            // _sinhrAutoUmaxGr1
            // 
            this._sinhrAutoUmaxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutoUmaxGr1.Location = new System.Drawing.Point(89, 66);
            this._sinhrAutoUmaxGr1.Name = "_sinhrAutoUmaxGr1";
            this._sinhrAutoUmaxGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutoUmaxGr1.TabIndex = 22;
            this._sinhrAutoUmaxGr1.Tag = "3276700";
            this._sinhrAutoUmaxGr1.Text = "0";
            this._sinhrAutoUmaxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrAutoModeGr1
            // 
            this._sinhrAutoModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoModeGr1.FormattingEnabled = true;
            this._sinhrAutoModeGr1.Location = new System.Drawing.Point(89, 16);
            this._sinhrAutoModeGr1.Name = "_sinhrAutoModeGr1";
            this._sinhrAutoModeGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoModeGr1.TabIndex = 18;
            // 
            // _dUmaxAutoLabel
            // 
            this._dUmaxAutoLabel.AutoSize = true;
            this._dUmaxAutoLabel.Location = new System.Drawing.Point(12, 67);
            this._dUmaxAutoLabel.Name = "_dUmaxAutoLabel";
            this._dUmaxAutoLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxAutoLabel.TabIndex = 11;
            this._dUmaxAutoLabel.Text = "dUmax., В";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(12, 18);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Режим";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this._blockTNmanual);
            this.groupBox35.Controls.Add(this.groupBox38);
            this.groupBox35.Controls.Add(this.groupBox37);
            this.groupBox35.Controls.Add(this.groupBox36);
            this.groupBox35.Controls.Add(this._sinhrManualUmaxGr1);
            this.groupBox35.Controls.Add(this._sinhrManualModeGr1);
            this.groupBox35.Controls.Add(this._dUmaxManualLabel);
            this.groupBox35.Controls.Add(this.label67);
            this.groupBox35.Location = new System.Drawing.Point(298, 23);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(288, 303);
            this.groupBox35.TabIndex = 16;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Уставки ручного включения";
            // 
            // _blockTNmanual
            // 
            this._blockTNmanual.AutoSize = true;
            this._blockTNmanual.Location = new System.Drawing.Point(15, 43);
            this._blockTNmanual.Name = "_blockTNmanual";
            this._blockTNmanual.Size = new System.Drawing.Size(200, 17);
            this._blockTNmanual.TabIndex = 34;
            this._blockTNmanual.Text = "Блокировка по неисправности ТН";
            this._blockTNmanual.UseVisualStyleBackColor = true;
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this._catchSinchrManual);
            this.groupBox38.Controls.Add(this._sinhrManualdFnoGr1);
            this.groupBox38.Controls.Add(this.label71);
            this.groupBox38.Location = new System.Drawing.Point(6, 252);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(276, 45);
            this.groupBox38.TabIndex = 33;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "      Улавливание синхронизма(несинхр. режим)";
            // 
            // _catchSinchrManual
            // 
            this._catchSinchrManual.AutoSize = true;
            this._catchSinchrManual.Location = new System.Drawing.Point(6, 0);
            this._catchSinchrManual.Name = "_catchSinchrManual";
            this._catchSinchrManual.Size = new System.Drawing.Size(15, 14);
            this._catchSinchrManual.TabIndex = 25;
            this._catchSinchrManual.UseVisualStyleBackColor = true;
            // 
            // _sinhrManualdFnoGr1
            // 
            this._sinhrManualdFnoGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFnoGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdFnoGr1.Name = "_sinhrManualdFnoGr1";
            this._sinhrManualdFnoGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFnoGr1.TabIndex = 23;
            this._sinhrManualdFnoGr1.Tag = "40";
            this._sinhrManualdFnoGr1.Text = "0";
            this._sinhrManualdFnoGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(43, 21);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "dF, Гц";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this._waitSinchrManual);
            this.groupBox37.Controls.Add(this._sinhrManualdFGr1);
            this.groupBox37.Controls.Add(this.label64);
            this.groupBox37.Controls.Add(this.label63);
            this.groupBox37.Controls.Add(this._sinhrManualdFiGr1);
            this.groupBox37.Location = new System.Drawing.Point(6, 182);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(276, 64);
            this.groupBox37.TabIndex = 32;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "       Ожидание синхронизма(синхр. режим)";
            // 
            // _waitSinchrManual
            // 
            this._waitSinchrManual.AutoSize = true;
            this._waitSinchrManual.Location = new System.Drawing.Point(6, 0);
            this._waitSinchrManual.Name = "_waitSinchrManual";
            this._waitSinchrManual.Size = new System.Drawing.Size(15, 14);
            this._waitSinchrManual.TabIndex = 25;
            this._waitSinchrManual.UseVisualStyleBackColor = true;
            // 
            // _sinhrManualdFGr1
            // 
            this._sinhrManualdFGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFGr1.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdFGr1.Name = "_sinhrManualdFGr1";
            this._sinhrManualdFGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFGr1.TabIndex = 23;
            this._sinhrManualdFGr1.Tag = "40";
            this._sinhrManualdFGr1.Text = "0";
            this._sinhrManualdFGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(43, 21);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 13);
            this.label64.TabIndex = 12;
            this.label64.Text = "dF, Гц";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(43, 40);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(47, 13);
            this.label63.TabIndex = 13;
            this.label63.Text = "dfi, град";
            // 
            // _sinhrManualdFiGr1
            // 
            this._sinhrManualdFiGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFiGr1.Location = new System.Drawing.Point(99, 38);
            this._sinhrManualdFiGr1.Name = "_sinhrManualdFiGr1";
            this._sinhrManualdFiGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFiGr1.TabIndex = 24;
            this._sinhrManualdFiGr1.Tag = "3276700";
            this._sinhrManualdFiGr1.Text = "0";
            this._sinhrManualdFiGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._sinhrManualNoNoGr1);
            this.groupBox36.Controls.Add(this._sinhrManualYesNoGr1);
            this.groupBox36.Controls.Add(this._sinhrManualNoYesGr1);
            this.groupBox36.Controls.Add(this.label68);
            this.groupBox36.Controls.Add(this.label69);
            this.groupBox36.Controls.Add(this.label70);
            this.groupBox36.Location = new System.Drawing.Point(6, 94);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(276, 82);
            this.groupBox36.TabIndex = 31;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Разрешение включения";
            // 
            // _sinhrManualNoNoGr1
            // 
            this._sinhrManualNoNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoNoGr1.FormattingEnabled = true;
            this._sinhrManualNoNoGr1.Location = new System.Drawing.Point(99, 54);
            this._sinhrManualNoNoGr1.Name = "_sinhrManualNoNoGr1";
            this._sinhrManualNoNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoNoGr1.TabIndex = 35;
            // 
            // _sinhrManualYesNoGr1
            // 
            this._sinhrManualYesNoGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualYesNoGr1.FormattingEnabled = true;
            this._sinhrManualYesNoGr1.Location = new System.Drawing.Point(99, 34);
            this._sinhrManualYesNoGr1.Name = "_sinhrManualYesNoGr1";
            this._sinhrManualYesNoGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualYesNoGr1.TabIndex = 34;
            // 
            // _sinhrManualNoYesGr1
            // 
            this._sinhrManualNoYesGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoYesGr1.FormattingEnabled = true;
            this._sinhrManualNoYesGr1.Location = new System.Drawing.Point(99, 14);
            this._sinhrManualNoYesGr1.Name = "_sinhrManualNoYesGr1";
            this._sinhrManualNoYesGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoYesGr1.TabIndex = 33;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 57);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(83, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "Uш нет, Uл нет";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 37);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(89, 13);
            this.label69.TabIndex = 31;
            this.label69.Text = "Uш есть, Uл нет";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 17);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(89, 13);
            this.label70.TabIndex = 30;
            this.label70.Text = "Uш нет, Uл есть";
            // 
            // _sinhrManualUmaxGr1
            // 
            this._sinhrManualUmaxGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualUmaxGr1.Location = new System.Drawing.Point(89, 66);
            this._sinhrManualUmaxGr1.Name = "_sinhrManualUmaxGr1";
            this._sinhrManualUmaxGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualUmaxGr1.TabIndex = 22;
            this._sinhrManualUmaxGr1.Tag = "3276700";
            this._sinhrManualUmaxGr1.Text = "0";
            this._sinhrManualUmaxGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrManualModeGr1
            // 
            this._sinhrManualModeGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualModeGr1.FormattingEnabled = true;
            this._sinhrManualModeGr1.Location = new System.Drawing.Point(89, 16);
            this._sinhrManualModeGr1.Name = "_sinhrManualModeGr1";
            this._sinhrManualModeGr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualModeGr1.TabIndex = 18;
            // 
            // _dUmaxManualLabel
            // 
            this._dUmaxManualLabel.AutoSize = true;
            this._dUmaxManualLabel.Location = new System.Drawing.Point(12, 67);
            this._dUmaxManualLabel.Name = "_dUmaxManualLabel";
            this._dUmaxManualLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxManualLabel.TabIndex = 11;
            this._dUmaxManualLabel.Text = "dUmax., В";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(12, 18);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(42, 13);
            this.label67.TabIndex = 9;
            this.label67.Text = "Режим";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this._discretIn3Cmb);
            this.groupBox34.Controls.Add(this.label85);
            this.groupBox34.Controls.Add(this.label84);
            this.groupBox34.Controls.Add(this.label83);
            this.groupBox34.Controls.Add(this._sinhrF);
            this.groupBox34.Controls.Add(this._discretIn2Cmb);
            this.groupBox34.Controls.Add(this._sinhrKamp);
            this.groupBox34.Controls.Add(this._sinhrTonGr1);
            this.groupBox34.Controls.Add(this._discretIn1Cmb);
            this.groupBox34.Controls.Add(this.label151);
            this.groupBox34.Controls.Add(this._blockSinhCmb);
            this.groupBox34.Controls.Add(this._sinhrTsinhrGr1);
            this.groupBox34.Controls.Add(this.label141);
            this.groupBox34.Controls.Add(this._blockSinhLabel);
            this.groupBox34.Controls.Add(this._sinhrTwaitGr1);
            this.groupBox34.Controls.Add(this.label14);
            this.groupBox34.Controls.Add(this.label15);
            this.groupBox34.Controls.Add(this.label16);
            this.groupBox34.Controls.Add(this._sinhrUmaxNalGr1);
            this.groupBox34.Controls.Add(this._sinhrUminNalGr1);
            this.groupBox34.Controls.Add(this._sinhrUminOtsGr1);
            this.groupBox34.Controls.Add(this._sinhrU2Gr1);
            this.groupBox34.Controls.Add(this._sinhrU1Gr1);
            this.groupBox34.Controls.Add(this.label40);
            this.groupBox34.Controls.Add(this.label57);
            this.groupBox34.Controls.Add(this.label58);
            this.groupBox34.Controls.Add(this.label59);
            this.groupBox34.Controls.Add(this.label60);
            this.groupBox34.Location = new System.Drawing.Point(6, 23);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(286, 303);
            this.groupBox34.TabIndex = 15;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Общие уставки";
            // 
            // _discretIn3Cmb
            // 
            this._discretIn3Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn3Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn3Cmb.FormattingEnabled = true;
            this._discretIn3Cmb.Location = new System.Drawing.Point(165, 270);
            this._discretIn3Cmb.Name = "_discretIn3Cmb";
            this._discretIn3Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn3Cmb.TabIndex = 38;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(12, 273);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(143, 13);
            this.label85.TabIndex = 37;
            this.label85.Text = "Вход ввода Uш нет, Uл нет";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(12, 253);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(149, 13);
            this.label84.TabIndex = 36;
            this.label84.Text = "Вход ввода Uш есть, Uл нет";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(12, 233);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(149, 13);
            this.label83.TabIndex = 35;
            this.label83.Text = "Вход ввода Uш нет, Uл есть";
            // 
            // _sinhrF
            // 
            this._sinhrF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrF.Location = new System.Drawing.Point(165, 191);
            this._sinhrF.Name = "_sinhrF";
            this._sinhrF.Size = new System.Drawing.Size(105, 20);
            this._sinhrF.TabIndex = 30;
            this._sinhrF.Tag = "3276700";
            this._sinhrF.Text = "0";
            this._sinhrF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _discretIn2Cmb
            // 
            this._discretIn2Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn2Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn2Cmb.FormattingEnabled = true;
            this._discretIn2Cmb.Location = new System.Drawing.Point(165, 250);
            this._discretIn2Cmb.Name = "_discretIn2Cmb";
            this._discretIn2Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn2Cmb.TabIndex = 34;
            // 
            // _sinhrKamp
            // 
            this._sinhrKamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrKamp.Location = new System.Drawing.Point(165, 172);
            this._sinhrKamp.Name = "_sinhrKamp";
            this._sinhrKamp.Size = new System.Drawing.Size(105, 20);
            this._sinhrKamp.TabIndex = 30;
            this._sinhrKamp.Tag = "3276700";
            this._sinhrKamp.Text = "0";
            this._sinhrKamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrTonGr1
            // 
            this._sinhrTonGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTonGr1.Location = new System.Drawing.Point(165, 153);
            this._sinhrTonGr1.Name = "_sinhrTonGr1";
            this._sinhrTonGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTonGr1.TabIndex = 30;
            this._sinhrTonGr1.Tag = "3276700";
            this._sinhrTonGr1.Text = "0";
            this._sinhrTonGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _discretIn1Cmb
            // 
            this._discretIn1Cmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._discretIn1Cmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._discretIn1Cmb.FormattingEnabled = true;
            this._discretIn1Cmb.Location = new System.Drawing.Point(165, 230);
            this._discretIn1Cmb.Name = "_discretIn1Cmb";
            this._discretIn1Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn1Cmb.TabIndex = 33;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(12, 191);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(78, 13);
            this.label151.TabIndex = 27;
            this.label151.Text = "f(Uш;Uл), град";
            // 
            // _blockSinhCmb
            // 
            this._blockSinhCmb.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._blockSinhCmb.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._blockSinhCmb.FormattingEnabled = true;
            this._blockSinhCmb.Location = new System.Drawing.Point(165, 210);
            this._blockSinhCmb.Name = "_blockSinhCmb";
            this._blockSinhCmb.Size = new System.Drawing.Size(105, 21);
            this._blockSinhCmb.TabIndex = 18;
            // 
            // _sinhrTsinhrGr1
            // 
            this._sinhrTsinhrGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTsinhrGr1.Location = new System.Drawing.Point(165, 134);
            this._sinhrTsinhrGr1.Name = "_sinhrTsinhrGr1";
            this._sinhrTsinhrGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTsinhrGr1.TabIndex = 29;
            this._sinhrTsinhrGr1.Tag = "40";
            this._sinhrTsinhrGr1.Text = "20";
            this._sinhrTsinhrGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(12, 172);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(48, 13);
            this.label141.TabIndex = 27;
            this.label141.Text = "Камп, %";
            // 
            // _blockSinhLabel
            // 
            this._blockSinhLabel.AutoSize = true;
            this._blockSinhLabel.Location = new System.Drawing.Point(12, 212);
            this._blockSinhLabel.Name = "_blockSinhLabel";
            this._blockSinhLabel.Size = new System.Drawing.Size(111, 13);
            this._blockSinhLabel.TabIndex = 9;
            this._blockSinhLabel.Text = "Вход блокировки КС";
            // 
            // _sinhrTwaitGr1
            // 
            this._sinhrTwaitGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTwaitGr1.Location = new System.Drawing.Point(165, 115);
            this._sinhrTwaitGr1.Name = "_sinhrTwaitGr1";
            this._sinhrTwaitGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrTwaitGr1.TabIndex = 28;
            this._sinhrTwaitGr1.Tag = "3276700";
            this._sinhrTwaitGr1.Text = "20";
            this._sinhrTwaitGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 153);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "t вкл, мс";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 134);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "tсинхр, мс";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "tож, мс";
            // 
            // _sinhrUmaxNalGr1
            // 
            this._sinhrUmaxNalGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUmaxNalGr1.Location = new System.Drawing.Point(165, 96);
            this._sinhrUmaxNalGr1.Name = "_sinhrUmaxNalGr1";
            this._sinhrUmaxNalGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUmaxNalGr1.TabIndex = 24;
            this._sinhrUmaxNalGr1.Tag = "3276700";
            this._sinhrUmaxNalGr1.Text = "0";
            this._sinhrUmaxNalGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminNalGr1
            // 
            this._sinhrUminNalGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminNalGr1.Location = new System.Drawing.Point(165, 77);
            this._sinhrUminNalGr1.Name = "_sinhrUminNalGr1";
            this._sinhrUminNalGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminNalGr1.TabIndex = 23;
            this._sinhrUminNalGr1.Tag = "40";
            this._sinhrUminNalGr1.Text = "0";
            this._sinhrUminNalGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminOtsGr1
            // 
            this._sinhrUminOtsGr1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminOtsGr1.Location = new System.Drawing.Point(165, 58);
            this._sinhrUminOtsGr1.Name = "_sinhrUminOtsGr1";
            this._sinhrUminOtsGr1.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminOtsGr1.TabIndex = 22;
            this._sinhrUminOtsGr1.Tag = "3276700";
            this._sinhrUminOtsGr1.Text = "0";
            this._sinhrUminOtsGr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrU2Gr1
            // 
            this._sinhrU2Gr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU2Gr1.FormattingEnabled = true;
            this._sinhrU2Gr1.Location = new System.Drawing.Point(165, 38);
            this._sinhrU2Gr1.Name = "_sinhrU2Gr1";
            this._sinhrU2Gr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrU2Gr1.TabIndex = 19;
            // 
            // _sinhrU1Gr1
            // 
            this._sinhrU1Gr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU1Gr1.FormattingEnabled = true;
            this._sinhrU1Gr1.Location = new System.Drawing.Point(165, 18);
            this._sinhrU1Gr1.Name = "_sinhrU1Gr1";
            this._sinhrU1Gr1.Size = new System.Drawing.Size(105, 21);
            this._sinhrU1Gr1.TabIndex = 18;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 96);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Umax. нал*, В";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(12, 77);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(72, 13);
            this.label57.TabIndex = 12;
            this.label57.Text = "Umin. нал*, В";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(12, 57);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(71, 13);
            this.label58.TabIndex = 11;
            this.label58.Text = "Umin. отс*, В";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 38);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(34, 13);
            this.label59.TabIndex = 10;
            this.label59.Text = "Uл, В";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(12, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(36, 13);
            this.label60.TabIndex = 9;
            this.label60.Text = "Uш, В";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 329);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(441, 13);
            this.label30.TabIndex = 27;
            this.label30.Text = "* - данные уставки используются для сравнения с напряжениями Uш и Uл*Камп/100";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._setpointsComboBox);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(103, 54);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Группы уставок";
            // 
            // _setpointsComboBox
            // 
            this._setpointsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._setpointsComboBox.FormattingEnabled = true;
            this._setpointsComboBox.Location = new System.Drawing.Point(6, 19);
            this._setpointsComboBox.Name = "_setpointsComboBox";
            this._setpointsComboBox.Size = new System.Drawing.Size(88, 21);
            this._setpointsComboBox.TabIndex = 0;
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._setpointPage);
            this._configurationTabControl.Controls.Add(this._automatPage);
            this._configurationTabControl.Controls.Add(this._inputSignalsPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._logicElements);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._gooseTabPage);
            this._configurationTabControl.Controls.Add(this._ethernetPage);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(984, 579);
            this._configurationTabControl.TabIndex = 0;
            // 
            // _logicElements
            // 
            this._logicElements.Controls.Add(this.groupBox55);
            this._logicElements.Controls.Add(this.groupBox53);
            this._logicElements.Location = new System.Drawing.Point(4, 22);
            this._logicElements.Name = "_logicElements";
            this._logicElements.Padding = new System.Windows.Forms.Padding(3);
            this._logicElements.Size = new System.Drawing.Size(976, 553);
            this._logicElements.TabIndex = 13;
            this._logicElements.Text = "Логические элементы";
            this._logicElements.UseVisualStyleBackColor = true;
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this._virtualReleDataGrid);
            this.groupBox55.Location = new System.Drawing.Point(8, 6);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(393, 541);
            this.groupBox55.TabIndex = 22;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Виртуальные  реле";
            // 
            // _virtualReleDataGrid
            // 
            this._virtualReleDataGrid.AllowUserToAddRows = false;
            this._virtualReleDataGrid.AllowUserToDeleteRows = false;
            this._virtualReleDataGrid.AllowUserToResizeColumns = false;
            this._virtualReleDataGrid.AllowUserToResizeRows = false;
            this._virtualReleDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._virtualReleDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._virtualReleDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewComboBoxColumn68,
            this.dataGridViewComboBoxColumn69,
            this.dataGridViewTextBoxColumn53});
            this._virtualReleDataGrid.Location = new System.Drawing.Point(9, 13);
            this._virtualReleDataGrid.Name = "_virtualReleDataGrid";
            this._virtualReleDataGrid.RowHeadersVisible = false;
            this._virtualReleDataGrid.RowHeadersWidth = 51;
            this._virtualReleDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle143.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._virtualReleDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle143;
            this._virtualReleDataGrid.RowTemplate.Height = 24;
            this._virtualReleDataGrid.ShowCellErrors = false;
            this._virtualReleDataGrid.ShowRowErrors = false;
            this._virtualReleDataGrid.Size = new System.Drawing.Size(376, 522);
            this._virtualReleDataGrid.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.HeaderText = "№";
            this.dataGridViewTextBoxColumn52.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn52.Width = 25;
            // 
            // dataGridViewComboBoxColumn68
            // 
            this.dataGridViewComboBoxColumn68.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn68.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn68.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn68.Name = "dataGridViewComboBoxColumn68";
            this.dataGridViewComboBoxColumn68.Width = 120;
            // 
            // dataGridViewComboBoxColumn69
            // 
            this.dataGridViewComboBoxColumn69.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn69.HeaderText = "Сигнал";
            this.dataGridViewComboBoxColumn69.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn69.Name = "dataGridViewComboBoxColumn69";
            this.dataGridViewComboBoxColumn69.Width = 140;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.HeaderText = "Tвозвр., мс";
            this.dataGridViewTextBoxColumn53.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 70;
            // 
            // groupBox53
            // 
            this.groupBox53.Controls.Add(this._rsTriggersDataGrid);
            this.groupBox53.Location = new System.Drawing.Point(407, 6);
            this.groupBox53.Name = "groupBox53";
            this.groupBox53.Size = new System.Drawing.Size(292, 427);
            this.groupBox53.TabIndex = 21;
            this.groupBox53.TabStop = false;
            this.groupBox53.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rsTriggersDataGrid
            // 
            this._rsTriggersDataGrid.AllowUserToAddRows = false;
            this._rsTriggersDataGrid.AllowUserToDeleteRows = false;
            this._rsTriggersDataGrid.AllowUserToResizeColumns = false;
            this._rsTriggersDataGrid.AllowUserToResizeRows = false;
            this._rsTriggersDataGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle144.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle144.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle144.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle144.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle144.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle144.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle144.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._rsTriggersDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle144;
            this._rsTriggersDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._rsTriggersDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewComboBoxColumn65,
            this.dataGridViewComboBoxColumn66,
            this.dataGridViewComboBoxColumn67});
            this._rsTriggersDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rsTriggersDataGrid.Location = new System.Drawing.Point(3, 16);
            this._rsTriggersDataGrid.Name = "_rsTriggersDataGrid";
            this._rsTriggersDataGrid.RowHeadersVisible = false;
            this._rsTriggersDataGrid.RowHeadersWidth = 51;
            this._rsTriggersDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle145.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this._rsTriggersDataGrid.RowsDefaultCellStyle = dataGridViewCellStyle145;
            this._rsTriggersDataGrid.RowTemplate.Height = 24;
            this._rsTriggersDataGrid.ShowCellErrors = false;
            this._rsTriggersDataGrid.ShowRowErrors = false;
            this._rsTriggersDataGrid.Size = new System.Drawing.Size(286, 408);
            this._rsTriggersDataGrid.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.HeaderText = "№";
            this.dataGridViewTextBoxColumn51.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 25;
            // 
            // dataGridViewComboBoxColumn65
            // 
            this.dataGridViewComboBoxColumn65.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn65.HeaderText = "Тип";
            this.dataGridViewComboBoxColumn65.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn65.Name = "dataGridViewComboBoxColumn65";
            this.dataGridViewComboBoxColumn65.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn65.Width = 35;
            // 
            // dataGridViewComboBoxColumn66
            // 
            this.dataGridViewComboBoxColumn66.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn66.HeaderText = "Вход R";
            this.dataGridViewComboBoxColumn66.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn66.Name = "dataGridViewComboBoxColumn66";
            this.dataGridViewComboBoxColumn66.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn66.Width = 110;
            // 
            // dataGridViewComboBoxColumn67
            // 
            this.dataGridViewComboBoxColumn67.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn67.HeaderText = "Вход S";
            this.dataGridViewComboBoxColumn67.MinimumWidth = 6;
            this.dataGridViewComboBoxColumn67.Name = "dataGridViewComboBoxColumn67";
            this.dataGridViewComboBoxColumn67.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewComboBoxColumn67.Width = 110;
            // 
            // _ethernetPage
            // 
            this._ethernetPage.Controls.Add(this.groupBox51);
            this._ethernetPage.Location = new System.Drawing.Point(4, 22);
            this._ethernetPage.Name = "_ethernetPage";
            this._ethernetPage.Padding = new System.Windows.Forms.Padding(3);
            this._ethernetPage.Size = new System.Drawing.Size(976, 553);
            this._ethernetPage.TabIndex = 12;
            this._ethernetPage.Text = "Конфигурация Ethernet";
            this._ethernetPage.UseVisualStyleBackColor = true;
            // 
            // groupBox51
            // 
            this.groupBox51.Controls.Add(this._ipLo1);
            this.groupBox51.Controls.Add(this._ipLo2);
            this.groupBox51.Controls.Add(this._ipHi1);
            this.groupBox51.Controls.Add(this._ipHi2);
            this.groupBox51.Controls.Add(this.label214);
            this.groupBox51.Controls.Add(this.label213);
            this.groupBox51.Controls.Add(this.label212);
            this.groupBox51.Controls.Add(this.label159);
            this.groupBox51.Location = new System.Drawing.Point(8, 6);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(261, 52);
            this.groupBox51.TabIndex = 0;
            this.groupBox51.TabStop = false;
            this.groupBox51.Text = "Конфигурация Ethernet";
            // 
            // _ipLo1
            // 
            this._ipLo1.Location = new System.Drawing.Point(215, 19);
            this._ipLo1.Name = "_ipLo1";
            this._ipLo1.Size = new System.Drawing.Size(38, 20);
            this._ipLo1.TabIndex = 3;
            // 
            // _ipLo2
            // 
            this._ipLo2.Location = new System.Drawing.Point(163, 19);
            this._ipLo2.Name = "_ipLo2";
            this._ipLo2.Size = new System.Drawing.Size(38, 20);
            this._ipLo2.TabIndex = 2;
            // 
            // _ipHi1
            // 
            this._ipHi1.Location = new System.Drawing.Point(112, 19);
            this._ipHi1.Name = "_ipHi1";
            this._ipHi1.Size = new System.Drawing.Size(38, 20);
            this._ipHi1.TabIndex = 1;
            // 
            // _ipHi2
            // 
            this._ipHi2.Location = new System.Drawing.Point(60, 19);
            this._ipHi2.Name = "_ipHi2";
            this._ipHi2.Size = new System.Drawing.Size(38, 20);
            this._ipHi2.TabIndex = 0;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(152, 26);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(10, 13);
            this.label214.TabIndex = 9;
            this.label214.Text = ".";
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(203, 26);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(10, 13);
            this.label213.TabIndex = 9;
            this.label213.Text = ".";
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(100, 26);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(10, 13);
            this.label212.TabIndex = 9;
            this.label212.Text = ".";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(6, 22);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(50, 13);
            this.label159.TabIndex = 9;
            this.label159.Text = "IP-адрес";
            // 
            // Mr7ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 634);
            this.ContextMenuStrip = this.contextMenu;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 669);
            this.Name = "Mr7ConfigurationForm";
            this.Text = "Mr7ConfigurationForm";
            this.Activated += new System.EventHandler(this.Mr7ConfigurationForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr7ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr7ConfigurationForm_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr7ConfigurationForm_KeyUp);
            this.contextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this._gooseTabPage.ResumeLayout(false);
            this._gooseTabPage.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._automatPage.ResumeLayout(false);
            this._automatPage.PerformLayout();
            this.UROVgroupBox.ResumeLayout(false);
            this.UROVgroupBox.PerformLayout();
            this._switchConfigGroupBox.ResumeLayout(false);
            this._switchConfigGroupBox.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this._systemPage.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannelsGrid)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox54.ResumeLayout(false);
            this.groupBox54.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox175.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox176.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._inputSignalsPage.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this._setpointPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this._setpointsTab.ResumeLayout(false);
            this._measureTransPage.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this._transformPage.ResumeLayout(false);
            this._transformPage.PerformLayout();
            this.groupBox56.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this._enginePage.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._controlPage.ResumeLayout(false);
            this.groupBox48.ResumeLayout(false);
            this.groupBox48.PerformLayout();
            this._diffPage.ResumeLayout(false);
            this.tabControl6.ResumeLayout(false);
            this._diffDefPage.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this._diffCutOffPage.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this._diffZeroPage.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dif0DataGreed)).EndInit();
            this._defResistGr1.ResumeLayout(false);
            this._defIPage.ResumeLayout(false);
            this._startArcDefPage.ResumeLayout(false);
            this._cornerGr1Page.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._defIGr1Page.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.groupBox46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI56DataGrid)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI14DataGrid)).EndInit();
            this.groupBox10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI7DataGrid)).EndInit();
            this._defIStarGr1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).EndInit();
            this._defI2I1Gr1.ResumeLayout(false);
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this._defUGr1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).EndInit();
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).EndInit();
            this._defFGr1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).EndInit();
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).EndInit();
            this._defDvigGr1.ResumeLayout(false);
            this.groupBox52.ResumeLayout(false);
            this.groupBox52.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).EndInit();
            this.groupBox43.ResumeLayout(false);
            this.groupBox43.PerformLayout();
            this._defExtGr1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDefenses)).EndInit();
            this._powerPage.ResumeLayout(false);
            this.groupBox47.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._reversePowerGrid)).EndInit();
            this._lsGr.ResumeLayout(false);
            this.groupBox58.ResumeLayout(false);
            this.groupBox57.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage31.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv1Gr1)).EndInit();
            this.tabPage32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv2Gr1)).EndInit();
            this.tabPage33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv3Gr1)).EndInit();
            this.tabPage34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv4Gr1)).EndInit();
            this.tabPage35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv5Gr1)).EndInit();
            this.tabPage36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv6Gr1)).EndInit();
            this.tabPage37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv7Gr1)).EndInit();
            this.tabPage38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsOrDgv8Gr1)).EndInit();
            this.groupBox45.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv1Gr1)).EndInit();
            this.tabPage40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv2Gr1)).EndInit();
            this.tabPage41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv3Gr1)).EndInit();
            this.tabPage42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv4Gr1)).EndInit();
            this.tabPage43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv5Gr1)).EndInit();
            this.tabPage44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv6Gr1)).EndInit();
            this.tabPage45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv7Gr1)).EndInit();
            this.tabPage46.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._lsAndDgv8Gr1)).EndInit();
            this._VLSGr.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox49.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this._apvGr1.ResumeLayout(false);
            this.groupBox50.ResumeLayout(false);
            this.groupBox50.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this._ksuppnGr1.ResumeLayout(false);
            this._ksuppnGr1.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this._configurationTabControl.ResumeLayout(false);
            this._logicElements.ResumeLayout(false);
            this.groupBox55.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._virtualReleDataGrid)).EndInit();
            this.groupBox53.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._rsTriggersDataGrid)).EndInit();
            this._ethernetPage.ResumeLayout(false);
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _writeConfigBut;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _progressBar;
        private System.Windows.Forms.Button _readConfigBut;
        private System.Windows.Forms.Button _saveConfigBut;
        private System.Windows.Forms.Button _loadConfigBut;
        private System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        private System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.ToolTip _toolTip;
        private System.Windows.Forms.Button _resetSetpointsButton;
        private System.Windows.Forms.Button _saveToXmlButton;
        private System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        private System.Windows.Forms.SaveFileDialog saveFileResistCharacteristic;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem resetSetpointsItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.TabPage _gooseTabPage;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox goin64;
        private System.Windows.Forms.ComboBox goin48;
        private System.Windows.Forms.ComboBox goin32;
        private System.Windows.Forms.Label label281;
        private System.Windows.Forms.Label label282;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.ComboBox goin63;
        private System.Windows.Forms.ComboBox goin47;
        private System.Windows.Forms.ComboBox goin31;
        private System.Windows.Forms.Label label284;
        private System.Windows.Forms.Label label285;
        private System.Windows.Forms.Label label286;
        private System.Windows.Forms.ComboBox goin62;
        private System.Windows.Forms.ComboBox goin46;
        private System.Windows.Forms.ComboBox goin30;
        private System.Windows.Forms.Label label287;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.ComboBox goin61;
        private System.Windows.Forms.ComboBox goin45;
        private System.Windows.Forms.ComboBox goin29;
        private System.Windows.Forms.Label label290;
        private System.Windows.Forms.Label label291;
        private System.Windows.Forms.Label label292;
        private System.Windows.Forms.ComboBox goin60;
        private System.Windows.Forms.ComboBox goin44;
        private System.Windows.Forms.ComboBox goin28;
        private System.Windows.Forms.Label label293;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.ComboBox goin59;
        private System.Windows.Forms.ComboBox goin43;
        private System.Windows.Forms.ComboBox goin27;
        private System.Windows.Forms.Label label296;
        private System.Windows.Forms.Label label297;
        private System.Windows.Forms.Label label298;
        private System.Windows.Forms.ComboBox goin58;
        private System.Windows.Forms.ComboBox goin42;
        private System.Windows.Forms.ComboBox goin26;
        private System.Windows.Forms.Label label299;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.ComboBox goin57;
        private System.Windows.Forms.ComboBox goin41;
        private System.Windows.Forms.ComboBox goin25;
        private System.Windows.Forms.Label label302;
        private System.Windows.Forms.Label label303;
        private System.Windows.Forms.Label label304;
        private System.Windows.Forms.ComboBox goin56;
        private System.Windows.Forms.ComboBox goin40;
        private System.Windows.Forms.ComboBox goin24;
        private System.Windows.Forms.Label label305;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.Label label307;
        private System.Windows.Forms.ComboBox goin55;
        private System.Windows.Forms.ComboBox goin39;
        private System.Windows.Forms.ComboBox goin23;
        private System.Windows.Forms.Label label308;
        private System.Windows.Forms.Label label309;
        private System.Windows.Forms.Label label310;
        private System.Windows.Forms.ComboBox goin54;
        private System.Windows.Forms.ComboBox goin38;
        private System.Windows.Forms.ComboBox goin22;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.Label label313;
        private System.Windows.Forms.ComboBox goin53;
        private System.Windows.Forms.ComboBox goin37;
        private System.Windows.Forms.ComboBox goin21;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private System.Windows.Forms.ComboBox goin52;
        private System.Windows.Forms.ComboBox goin36;
        private System.Windows.Forms.ComboBox goin20;
        private System.Windows.Forms.Label label317;
        private System.Windows.Forms.Label label318;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.ComboBox goin51;
        private System.Windows.Forms.ComboBox goin35;
        private System.Windows.Forms.ComboBox goin19;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label321;
        private System.Windows.Forms.Label label322;
        private System.Windows.Forms.ComboBox goin50;
        private System.Windows.Forms.ComboBox goin34;
        private System.Windows.Forms.ComboBox goin18;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.ComboBox goin49;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.ComboBox goin33;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.ComboBox goin17;
        private System.Windows.Forms.Label label328;
        private System.Windows.Forms.ComboBox goin16;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.ComboBox goin15;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.ComboBox goin14;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.ComboBox goin13;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.ComboBox goin12;
        private System.Windows.Forms.Label label333;
        private System.Windows.Forms.ComboBox goin11;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.ComboBox goin10;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.ComboBox goin9;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.ComboBox goin8;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.ComboBox goin7;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.ComboBox goin6;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.ComboBox goin5;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.ComboBox goin4;
        private System.Windows.Forms.Label label341;
        private System.Windows.Forms.ComboBox goin3;
        private System.Windows.Forms.Label label342;
        private System.Windows.Forms.ComboBox goin2;
        private System.Windows.Forms.Label label343;
        private System.Windows.Forms.ComboBox goin1;
        private System.Windows.Forms.Label label344;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.ComboBox operationBGS;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox currentBGS;
        private System.Windows.Forms.TabPage _automatPage;
        private System.Windows.Forms.GroupBox UROVgroupBox;
        private System.Windows.Forms.CheckBox _urovSelf;
        private System.Windows.Forms.CheckBox _urovBkCheck;
        private System.Windows.Forms.CheckBox _urovIcheck;
        private System.Windows.Forms.ComboBox _urovBlock;
        private System.Windows.Forms.MaskedTextBox _Iurov;
        private System.Windows.Forms.MaskedTextBox _tUrov2;
        private System.Windows.Forms.MaskedTextBox _tUrov1;
        private System.Windows.Forms.ComboBox _urovPusk;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.GroupBox _switchConfigGroupBox;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.ComboBox _comandOtkl;
        private System.Windows.Forms.ComboBox _controlSolenoidCombo;
        private System.Windows.Forms.ComboBox _switchKontCep;
        private System.Windows.Forms.MaskedTextBox _switchTUskor;
        private System.Windows.Forms.MaskedTextBox _switchImp;
        private System.Windows.Forms.ComboBox _switchBlock;
        private System.Windows.Forms.ComboBox _switchError;
        private System.Windows.Forms.ComboBox _switchOn;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox _switchOff;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.ComboBox _blockSDTU;
        private System.Windows.Forms.ComboBox _switchSDTU;
        private System.Windows.Forms.ComboBox _switchVnesh;
        private System.Windows.Forms.ComboBox _switchKey;
        private System.Windows.Forms.ComboBox _switchButtons;
        private System.Windows.Forms.ComboBox _switchVneshOff;
        private System.Windows.Forms.ComboBox _switchVneshOn;
        private System.Windows.Forms.ComboBox _switchKeyOff;
        private System.Windows.Forms.Label _blockSDTUlabel;
        private System.Windows.Forms.ComboBox _switchKeyOn;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TabPage _systemPage;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView _oscChannelsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewComboBoxColumn _baseCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _oscSygnal;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox oscStartCb;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        private System.Windows.Forms.MaskedTextBox _oscWriteLength;
        private System.Windows.Forms.ComboBox _oscFix;
        private System.Windows.Forms.ComboBox _oscLength;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage _outputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.CheckBox _resetAlarmCheckBox;
        private System.Windows.Forms.CheckBox _resetSystemCheckBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.CheckBox _fault6CheckBox;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.CheckBox _fault5CheckBox;
        private System.Windows.Forms.CheckBox _fault4CheckBox;
        private System.Windows.Forms.CheckBox _fault3CheckBox;
        private System.Windows.Forms.CheckBox _fault2CheckBox;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox _fault1CheckBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox _impTB;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox175;
        private System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _out1IndSignalCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignal2Col;
        private System.Windows.Forms.GroupBox groupBox176;
        private System.Windows.Forms.DataGridView _outputReleGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
        private System.Windows.Forms.TabPage _inputSignalsPage;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.ComboBox _indComboBox;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox _grUst2ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _grUst1ComboBox;
        private System.Windows.Forms.TabPage _setpointPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox _inpAddCombo;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.CheckBox _resistCheck;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button _applyCopySetpoinsButton;
        private System.Windows.Forms.ComboBox _copySetpoinsGroupComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox _setpointsComboBox;
        private System.Windows.Forms.TabControl _configurationTabControl;
        private System.Windows.Forms.TabPage _ethernetPage;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.MaskedTextBox _ipLo1;
        private System.Windows.Forms.MaskedTextBox _ipLo2;
        private System.Windows.Forms.MaskedTextBox _ipHi1;
        private System.Windows.Forms.MaskedTextBox _ipHi2;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Button _clearSetpointBtn;
        private System.Windows.Forms.ToolStripMenuItem clearSetpointsItem;
        private System.Windows.Forms.CheckBox _fixErrorFCheckBox;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.GroupBox groupBox54;
        private System.Windows.Forms.TabControl _setpointsTab;
        private System.Windows.Forms.TabPage _measureTransPage;
        private System.Windows.Forms.TabPage _defUGr1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.DataGridView _difensesUBDataGrid;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.DataGridView _difensesUMDataGrid;
        private System.Windows.Forms.TabPage _defFGr1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.DataGridView _difensesFMDataGrid;
        private System.Windows.Forms.TabPage _defDvigGr1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DataGridView _engineDefensesGrid;
        private System.Windows.Forms.GroupBox groupBox43;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.MaskedTextBox _engineQconstraintGr1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.MaskedTextBox _engineQtimeGr1;
        private System.Windows.Forms.ComboBox _engineQmodeGr1;
        private System.Windows.Forms.TabPage _defExtGr1;
        private System.Windows.Forms.DataGridView _externalDefenses;
        private System.Windows.Forms.TabPage _lsGr;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.DataGridView _lsOrDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn15;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.DataGridView _lsOrDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn16;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.DataGridView _lsOrDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn17;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.DataGridView _lsOrDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn18;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.DataGridView _lsOrDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn19;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.DataGridView _lsOrDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn20;
        private System.Windows.Forms.TabPage tabPage37;
        private System.Windows.Forms.DataGridView _lsOrDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn21;
        private System.Windows.Forms.TabPage tabPage38;
        private System.Windows.Forms.DataGridView _lsOrDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn22;
        private System.Windows.Forms.Button _clearCurrentLsnButton;
        private System.Windows.Forms.Button _clearAllLsnSygnalButton;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.DataGridView _lsAndDgv1Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn23;
        private System.Windows.Forms.TabPage tabPage40;
        private System.Windows.Forms.DataGridView _lsAndDgv2Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn24;
        private System.Windows.Forms.TabPage tabPage41;
        private System.Windows.Forms.DataGridView _lsAndDgv3Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn25;
        private System.Windows.Forms.TabPage tabPage42;
        private System.Windows.Forms.DataGridView _lsAndDgv4Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn26;
        private System.Windows.Forms.TabPage tabPage43;
        private System.Windows.Forms.DataGridView _lsAndDgv5Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn27;
        private System.Windows.Forms.TabPage tabPage44;
        private System.Windows.Forms.DataGridView _lsAndDgv6Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn28;
        private System.Windows.Forms.TabPage tabPage45;
        private System.Windows.Forms.DataGridView _lsAndDgv7Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn29;
        private System.Windows.Forms.TabPage tabPage46;
        private System.Windows.Forms.DataGridView _lsAndDgv8Gr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn30;
        private System.Windows.Forms.Button _clearCurrentLsButton;
        private System.Windows.Forms.Button _clearAllLsSygnalButton;
        private System.Windows.Forms.TabPage _VLSGr;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage VLS1;
        private System.Windows.Forms.CheckedListBox VLSclb1Gr1;
        private System.Windows.Forms.TabPage VLS2;
        private System.Windows.Forms.CheckedListBox VLSclb2Gr1;
        private System.Windows.Forms.TabPage VLS3;
        private System.Windows.Forms.CheckedListBox VLSclb3Gr1;
        private System.Windows.Forms.TabPage VLS4;
        private System.Windows.Forms.CheckedListBox VLSclb4Gr1;
        private System.Windows.Forms.TabPage VLS5;
        private System.Windows.Forms.CheckedListBox VLSclb5Gr1;
        private System.Windows.Forms.TabPage VLS6;
        private System.Windows.Forms.CheckedListBox VLSclb6Gr1;
        private System.Windows.Forms.TabPage VLS7;
        private System.Windows.Forms.CheckedListBox VLSclb7Gr1;
        private System.Windows.Forms.TabPage VLS8;
        private System.Windows.Forms.CheckedListBox VLSclb8Gr1;
        private System.Windows.Forms.TabPage VLS9;
        private System.Windows.Forms.CheckedListBox VLSclb9Gr1;
        private System.Windows.Forms.TabPage VLS10;
        private System.Windows.Forms.CheckedListBox VLSclb10Gr1;
        private System.Windows.Forms.TabPage VLS11;
        private System.Windows.Forms.CheckedListBox VLSclb11Gr1;
        private System.Windows.Forms.TabPage VLS12;
        private System.Windows.Forms.CheckedListBox VLSclb12Gr1;
        private System.Windows.Forms.TabPage VLS13;
        private System.Windows.Forms.CheckedListBox VLSclb13Gr1;
        private System.Windows.Forms.TabPage VLS14;
        private System.Windows.Forms.CheckedListBox VLSclb14Gr1;
        private System.Windows.Forms.TabPage VLS15;
        private System.Windows.Forms.CheckedListBox VLSclb15Gr1;
        private System.Windows.Forms.TabPage VLS16;
        private System.Windows.Forms.CheckedListBox VLSclb16Gr1;
        private System.Windows.Forms.Button _resetAllVlsButton;
        private System.Windows.Forms.Button _resetCurrentVlsButton;
        private System.Windows.Forms.TabPage _apvGr1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.CheckBox _blokFromUrov;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.MaskedTextBox _apvKrat4Gr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat3Gr1;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.ComboBox _apvSwitchOffGr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat2Gr1;
        private System.Windows.Forms.MaskedTextBox _apvKrat1Gr1;
        private System.Windows.Forms.MaskedTextBox _apvTreadyGr1;
        private System.Windows.Forms.MaskedTextBox _apvTblockGr1;
        private System.Windows.Forms.ComboBox _apvBlockGr1;
        private System.Windows.Forms.MaskedTextBox _timeDisable;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.ComboBox _typeDisable;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.ComboBox _disableApv;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.ComboBox _apvModeGr1;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TabPage _ksuppnGr1;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.CheckBox _blockTNauto;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.CheckBox _catchSinchrAuto;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFnoGr1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.CheckBox _waitSinchrAuto;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFGr1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.MaskedTextBox _sinhrAutodFiGr1;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.ComboBox _sinhrAutoNoNoGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoYesNoGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoNoYesGr1;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.MaskedTextBox _sinhrAutoUmaxGr1;
        private System.Windows.Forms.ComboBox _sinhrAutoModeGr1;
        private System.Windows.Forms.Label _dUmaxAutoLabel;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.CheckBox _blockTNmanual;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.CheckBox _catchSinchrManual;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFnoGr1;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.CheckBox _waitSinchrManual;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFGr1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.MaskedTextBox _sinhrManualdFiGr1;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.ComboBox _sinhrManualNoNoGr1;
        private System.Windows.Forms.ComboBox _sinhrManualYesNoGr1;
        private System.Windows.Forms.ComboBox _sinhrManualNoYesGr1;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.MaskedTextBox _sinhrManualUmaxGr1;
        private System.Windows.Forms.ComboBox _sinhrManualModeGr1;
        private System.Windows.Forms.Label _dUmaxManualLabel;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.ComboBox _discretIn3Cmb;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.MaskedTextBox _sinhrF;
        private System.Windows.Forms.ComboBox _discretIn2Cmb;
        private System.Windows.Forms.MaskedTextBox _sinhrKamp;
        private System.Windows.Forms.MaskedTextBox _sinhrTonGr1;
        private System.Windows.Forms.ComboBox _discretIn1Cmb;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.ComboBox _blockSinhCmb;
        private System.Windows.Forms.MaskedTextBox _sinhrTsinhrGr1;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label _blockSinhLabel;
        private System.Windows.Forms.MaskedTextBox _sinhrTwaitGr1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox _sinhrUmaxNalGr1;
        private System.Windows.Forms.MaskedTextBox _sinhrUminNalGr1;
        private System.Windows.Forms.MaskedTextBox _sinhrUminOtsGr1;
        private System.Windows.Forms.ComboBox _sinhrU2Gr1;
        private System.Windows.Forms.ComboBox _sinhrU1Gr1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage _transformPage;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.GroupBox groupBox56;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage _enginePage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.ComboBox _engineNreset;
        private System.Windows.Forms.ComboBox _engineQreset;
        private System.Windows.Forms.MaskedTextBox _qBox;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.MaskedTextBox _tCount;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.MaskedTextBox _tStartBox;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.MaskedTextBox _iStartBox;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.MaskedTextBox _engineIdv;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.MaskedTextBox _engineCoolingTimeGr1;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.MaskedTextBox _engineHeatingTimeGr1;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.TabPage _controlPage;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.ComboBox _resetTnGr1;
        private System.Windows.Forms.CheckBox _i0u0CheckGr1;
        private System.Windows.Forms.CheckBox _i2u2CheckGr1;
        private System.Windows.Forms.MaskedTextBox _tdContrCepGr1;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.MaskedTextBox _iMinContrCepGr1;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.MaskedTextBox _uMinContrCepGr1;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.MaskedTextBox _u0ContrCepGr1;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.MaskedTextBox _u2ContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _tsContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _uDelContrCepGr1;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.MaskedTextBox _iDelContrCepGr1;
        private System.Windows.Forms.MaskedTextBox _iMaxContrCepGr1;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.MaskedTextBox _uMaxContrCepGr1;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.MaskedTextBox _i0ContrCepGr1;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.MaskedTextBox _i2ContrCepGr1;
        private System.Windows.Forms.TabPage _diffPage;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage _diffDefPage;
        private System.Windows.Forms.TabPage _diffCutOffPage;
        private System.Windows.Forms.TabPage _diffZeroPage;
        private System.Windows.Forms.TabPage _defIPage;
        private System.Windows.Forms.TabControl _startArcDefPage;
        private System.Windows.Forms.TabPage _cornerGr1Page;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox _i2CornerGr1;
        private System.Windows.Forms.MaskedTextBox _inCornerGr1;
        private System.Windows.Forms.MaskedTextBox _i0CornerGr1;
        private System.Windows.Forms.MaskedTextBox _iCornerGr1;
        private System.Windows.Forms.TabPage _defIGr1Page;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView _difensesI14DataGrid;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DataGridView _difensesI7DataGrid;
        private System.Windows.Forms.TabPage _defIStarGr1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.DataGridView _difensesI0DataGrid;
        private System.Windows.Forms.TabPage _defI2I1Gr1;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.ComboBox i2i1APV1;
        private System.Windows.Forms.ComboBox I2I1OscComboGr1;
        private System.Windows.Forms.MaskedTextBox I2I1tcpTBGr1;
        private System.Windows.Forms.ComboBox I2I1BlockingComboGr1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.MaskedTextBox I2I1TBGr1;
        private System.Windows.Forms.ComboBox I2I1ModeComboGr1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage _powerPage;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox _AVRDTOBTComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox _APVDTOBTComboBox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTOBTTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.MaskedTextBox _constraintDTOBTTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox _blockingDTOBTComboBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox _oscDTOBTComboBox;
        private System.Windows.Forms.ComboBox _UROVDTOBTComboBox;
        private System.Windows.Forms.ComboBox _stepOnInstantValuesDTOBTComboBox;
        private System.Windows.Forms.ComboBox _modeDTOBTComboBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.ComboBox _AVRDTZComboBox;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ComboBox _APVDTZComboBox;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.ComboBox _perBlockI5I1;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.MaskedTextBox _I5I1TextBox;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.ComboBox _oscDTZComboBox;
        private System.Windows.Forms.ComboBox _UROVDTZComboBox;
        private System.Windows.Forms.ComboBox _blockingDTZComboBox;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.MaskedTextBox _timeEnduranceDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.ComboBox _perBlockI2I1;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.MaskedTextBox _I2I1TextBox;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.MaskedTextBox _constraintDTZTextBox;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.MaskedTextBox _K2TangensTextBox;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.MaskedTextBox _Ib2BeginTextBox;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.MaskedTextBox _K1AngleOfSlopeTextBox;
        private System.Windows.Forms.MaskedTextBox _Ib1BeginTextBox;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.ComboBox _modeDTZComboBox;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.ComboBox _sideI2I1ComboBox;
        private System.Windows.Forms.ComboBox i2i1AVR;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.GroupBox groupBox44;
        private System.Windows.Forms.ComboBox SideArcComboBox;
        private System.Windows.Forms.ComboBox StartBlockArcComboBox;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.MaskedTextBox IcpArcTextBox;
        private System.Windows.Forms.ComboBox StartArcModeComboBox;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.DataGridView _reversePowerGrid;
        private System.Windows.Forms.TabPage _defResistGr1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView _dif0DataGreed;
        private System.Windows.Forms.CheckBox OscArc;
        private System.Windows.Forms.GroupBox groupBox50;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.ComboBox _avrClear;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.MaskedTextBox _avrTOff;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.MaskedTextBox _avrTBack;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.ComboBox _avrBack;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.MaskedTextBox _avrTSr;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.ComboBox _avrResolve;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.ComboBox _avrBlockClear;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.ComboBox _avrBlocking;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.ComboBox _avrSIGNOn;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.ComboBox _avrByDiff;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.ComboBox _avrBySelfOff;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.ComboBox _avrByOff;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.ComboBox _avrBySignal;
        private System.Windows.Forms.ComboBox _numberOfWindingComboBox;
        private System.Windows.Forms.GroupBox groupBox52;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.MaskedTextBox _numHot;
        private System.Windows.Forms.MaskedTextBox _numCold;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.MaskedTextBox _waitNumBlock;
        private System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseNameCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseModeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseConstraintCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseBlockCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseOscCol;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseUROVcol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseAPV1;
        private System.Windows.Forms.DataGridViewComboBoxColumn avrQ;
        private System.Windows.Forms.CheckBox _urovI2I1CheckBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifAPV1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn avrExt;
        private SideTransformControl sideTransformControl1;
        private SideTransformControl sideTransformControl2;
        private SideTransformControl sideTransformControl3;
        private SideTransformControl sideTransformControl4;
        private GroupParamSideControl groupParamSideControl1;
        private GroupParamSideControl groupParamSideControl2;
        private GroupParamSideControl groupParamSideControl3;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.DataGridView _difensesI56DataGrid;
        private System.Windows.Forms.CheckBox _modeI2I1;
        private System.Windows.Forms.CheckBox _modeI5I1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column6;
        private System.Windows.Forms.DataGridViewComboBoxColumn inImm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn32;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn33;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn34;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn35;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn39;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewComboBoxColumn40;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn13;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn41;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn42;
        private System.Windows.Forms.DataGridViewComboBoxColumn avrImm;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ssr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _usr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tsr;
        private System.Windows.Forms.DataGridViewTextBoxColumn _tvz;
        private System.Windows.Forms.DataGridViewTextBoxColumn _svz;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _svzbeno;
        private System.Windows.Forms.DataGridViewTextBoxColumn _isr;
        private System.Windows.Forms.DataGridViewComboBoxColumn _block;
        private System.Windows.Forms.DataGridViewComboBoxColumn _blk;
        private System.Windows.Forms.DataGridViewComboBoxColumn _osc;
        private System.Windows.Forms.DataGridViewComboBoxColumn _apvvoz;
        private System.Windows.Forms.DataGridViewComboBoxColumn _urov;
        private System.Windows.Forms.DataGridViewComboBoxColumn _apv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _resetstep;
        private System.Windows.Forms.ComboBox _switchSideCB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox _switchYesNo;
        private System.Windows.Forms.TabPage _logicElements;
        private System.Windows.Forms.GroupBox groupBox53;
        private System.Windows.Forms.DataGridView _rsTriggersDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn65;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn66;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn67;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.DataGridView _virtualReleDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn68;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private ResistanceDefTabCtr resistanceDefTabCtr1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0StageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column8;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0BlockingColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0IdColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0InColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0TdColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg1Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Ib2Column;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dif0Intg2Column;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0OscColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0UrovColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0APVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _dif0AVRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBTypeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column1;
        private System.Windows.Forms.DataGridViewComboBoxColumn sideUb;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUvzYNColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column7;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column4;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBAPVColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrUmax;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn54;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn55;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn56;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn57;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn58;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn15;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn19;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn62;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn20;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn63;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column5;
        private System.Windows.Forms.DataGridViewComboBoxColumn inIm;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        private System.Windows.Forms.DataGridViewComboBoxColumn avrIb;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMTypeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn sideUm;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMUvzYNColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _block5V;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMBlockingUMColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMUROVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMAPVColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMAPVColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fMAPVColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.DataGridViewComboBoxColumn avrFM;
        private System.Windows.Forms.DataGridView _difensesFBDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn70;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn21;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn72;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn73;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn22;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn23;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn74;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn24;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewComboBoxColumn inI;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn44;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn45;
        private System.Windows.Forms.DataGridViewComboBoxColumn _chooseCBColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _iComboBoxCell;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn48;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn49;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn17;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn18;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn51;
        private System.Windows.Forms.DataGridViewComboBoxColumn _avrI;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.TreeView treeViewForVLS;
        private System.Windows.Forms.Button _wrapBtn;
        private System.Windows.Forms.GroupBox groupBox57;
        private System.Windows.Forms.TreeView treeViewForLsAND;
        private System.Windows.Forms.GroupBox groupBox58;
        private System.Windows.Forms.TreeView treeViewForLsOR;
    }
}