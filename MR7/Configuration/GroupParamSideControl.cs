﻿using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace BEMN.MR7.Configuration
{
    public partial class GroupParamSideControl : UserControl
    {
        private bool _isUnChannel;

        private Point _label2Point;
        private Point _label3Point;
        private Point _label6Point;
        private Point _label7Point;
        private Point _label8Point;
        private Point _groupSideComboPoint;
        private Point _textPoint;
        private Point _kthCoefPoint;
        private Point _inputUaGroupSidePoint;
        private Point _almInpPoint;
        private string _label3Str;

        public GroupParamSideControl()
        {
            this.InitializeComponent();

            this._label2Point = this.label2.Location;
            this._label3Str = this.label3.Text;
            this._label3Point = this.label3.Location;
            this._label6Point = this.label6.Location;
            this._label7Point = this.label7.Location;
            this._label8Point = this.label8.Location;
            this._inputUaGroupSidePoint = this._inputUaGroupSideComboBox.Location;
            this._groupSideComboPoint = this._displayGroupSideComboBox.Location;
            this._textPoint = this._kthGroupSideTextBox.Location;
            this._kthCoefPoint = this._KTHLkoef_comboGr1.Location;
            this._almInpPoint = this.almInpTn.Location;

            this.Init();
        }

        public void Init()
        {
            this.GroupUabcValidator = new NewStructValidator<KanalTransU>(
                new ToolTip(),
                new ControlInfoCombo(this._measurGroupSideComboBox, StringsConfig.KanalUtype),
                new ControlInfoCombo(this._displayGroupSideComboBox, StringsConfig.KanalUside),
                new ControlInfoCombo(this._inputUaGroupSideComboBox, StringsConfig.NumberInputU4),
                new ControlInfoCombo(this._inputUbGroupSideComboBox, StringsConfig.NumberInputU4),
                new ControlInfoCombo(this._inputUcGroupSideComboBox, StringsConfig.NumberInputU4),
                new ControlInfoText(this._kthGroupSideTextBox, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._KTHLkoef_comboGr1, StringsConfig.KthKoefs),
                new ControlInfoCombo(this.almInpTn, StringsConfig.SwitchSignals)
            );
        }

        public NewStructValidator<KanalTransU> GroupUabcValidator { get; private set; }

        [Browsable(true)]
        public bool IsUnChannel
        {
            get => this._isUnChannel;
            set
            {
                if (this._isUnChannel == value) return;

                this._isUnChannel = value;

                this.label1.Visible = !this._isUnChannel;
                this._measurGroupSideComboBox.Visible = !this._isUnChannel;

                this.label4.Visible = !this._isUnChannel;
                this._inputUbGroupSideComboBox.Visible = !this._isUnChannel;

                this.label5.Visible = !this._isUnChannel;
                this._inputUcGroupSideComboBox.Visible = !this._isUnChannel;

                if (this._isUnChannel)
                {
                    this.label2.Location = new Point(16, 11);
                    this._displayGroupSideComboBox.Location = new Point(113, 8);

                    this.label3.Text = "Вх. Un";
                    this.label3.Location = new Point(16, 38);
                    this._inputUaGroupSideComboBox.Location = new Point(113, 35);

                    this.label6.Location = new Point(16, 63);
                    this._kthGroupSideTextBox.Location = new Point(113, 62);
                    this.label8.Location = new Point(208, 63);
                    this._KTHLkoef_comboGr1.Location = new Point(224, 62);

                    this.label7.Location = new Point(16, 92);
                    this.almInpTn.Location = new Point(113, 89);
                }
                else
                {
                    this.label2.Location = this._label2Point;
                    this._displayGroupSideComboBox.Location = this._groupSideComboPoint;

                    this.label3.Text = this._label3Str;
                    this.label3.Location = this._label3Point;
                    this._inputUaGroupSideComboBox.Location = this._inputUaGroupSidePoint;

                    this.label6.Location = this._label6Point;
                    this._kthGroupSideTextBox.Location = this._textPoint;
                    this.label8.Location = this._label8Point;
                    this._KTHLkoef_comboGr1.Location = this._kthCoefPoint;

                    this.label7.Location = this._label7Point;
                    this.almInpTn.Location = this._almInpPoint;

                }
                this.Refresh();
            }
        }
    }
}
