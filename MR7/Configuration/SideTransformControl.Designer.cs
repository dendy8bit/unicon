﻿namespace BEMN.MR7.Configuration
{
    partial class SideTransformControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this._unPolaryComboBox = new System.Windows.Forms.ComboBox();
            this.label239 = new System.Windows.Forms.Label();
            this._uabcPolaryComboBox = new System.Windows.Forms.ComboBox();
            this.label240 = new System.Windows.Forms.Label();
            this._koefttComboBox = new System.Windows.Forms.ComboBox();
            this.label242 = new System.Windows.Forms.Label();
            this._ittnpComboBox = new System.Windows.Forms.ComboBox();
            this._ittnpTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label243 = new System.Windows.Forms.Label();
            this._ittComboBox = new System.Windows.Forms.ComboBox();
            this._ittTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label244 = new System.Windows.Forms.Label();
            this._typeYnComboBox = new System.Windows.Forms.ComboBox();
            this.label245 = new System.Windows.Forms.Label();
            this._inlnComboBox = new System.Windows.Forms.ComboBox();
            this._inlcComboBox = new System.Windows.Forms.ComboBox();
            this._inlbComboBox = new System.Windows.Forms.ComboBox();
            this._inlaComboBox = new System.Windows.Forms.ComboBox();
            this.label248 = new System.Windows.Forms.Label();
            this.label249 = new System.Windows.Forms.Label();
            this.label250 = new System.Windows.Forms.Label();
            this.label251 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupCombo = new System.Windows.Forms.ComboBox();
            this.label238 = new System.Windows.Forms.Label();
            this._typettComboBox = new System.Windows.Forms.ComboBox();
            this.label241 = new System.Windows.Forms.Label();
            this._tokinComboBox = new System.Windows.Forms.ComboBox();
            this._u1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.label246 = new System.Windows.Forms.Label();
            this.label247 = new System.Windows.Forms.Label();
            this._s1TextBox = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // _unPolaryComboBox
            // 
            this._unPolaryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._unPolaryComboBox.FormattingEnabled = true;
            this._unPolaryComboBox.Location = new System.Drawing.Point(96, 198);
            this._unPolaryComboBox.Name = "_unPolaryComboBox";
            this._unPolaryComboBox.Size = new System.Drawing.Size(68, 21);
            this._unPolaryComboBox.TabIndex = 72;
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(21, 202);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(69, 13);
            this.label239.TabIndex = 69;
            this.label239.Text = "Un поляриз.";
            // 
            // _uabcPolaryComboBox
            // 
            this._uabcPolaryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._uabcPolaryComboBox.FormattingEnabled = true;
            this._uabcPolaryComboBox.Location = new System.Drawing.Point(96, 171);
            this._uabcPolaryComboBox.Name = "_uabcPolaryComboBox";
            this._uabcPolaryComboBox.Size = new System.Drawing.Size(68, 21);
            this._uabcPolaryComboBox.TabIndex = 71;
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(21, 168);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(52, 26);
            this.label240.TabIndex = 68;
            this.label240.Text = "Uabc \r\nполяриз.";
            // 
            // _koefttComboBox
            // 
            this._koefttComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._koefttComboBox.FormattingEnabled = true;
            this._koefttComboBox.Location = new System.Drawing.Point(277, 65);
            this._koefttComboBox.Name = "_koefttComboBox";
            this._koefttComboBox.Size = new System.Drawing.Size(68, 21);
            this._koefttComboBox.TabIndex = 66;
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(190, 68);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(73, 13);
            this.label242.TabIndex = 65;
            this.label242.Text = "Коэф. схемы";
            // 
            // _ittnpComboBox
            // 
            this._ittnpComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ittnpComboBox.FormattingEnabled = true;
            this._ittnpComboBox.Location = new System.Drawing.Point(241, 38);
            this._ittnpComboBox.Name = "_ittnpComboBox";
            this._ittnpComboBox.Size = new System.Drawing.Size(33, 21);
            this._ittnpComboBox.TabIndex = 64;
            // 
            // _ittnpTextBox
            // 
            this._ittnpTextBox.Location = new System.Drawing.Point(277, 39);
            this._ittnpTextBox.Name = "_ittnpTextBox";
            this._ittnpTextBox.Size = new System.Drawing.Size(68, 20);
            this._ittnpTextBox.TabIndex = 62;
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(190, 42);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(45, 13);
            this.label243.TabIndex = 63;
            this.label243.Text = "Iттнп, A";
            // 
            // _ittComboBox
            // 
            this._ittComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ittComboBox.FormattingEnabled = true;
            this._ittComboBox.Location = new System.Drawing.Point(241, 14);
            this._ittComboBox.Name = "_ittComboBox";
            this._ittComboBox.Size = new System.Drawing.Size(33, 21);
            this._ittComboBox.TabIndex = 61;
            // 
            // _ittTextBox
            // 
            this._ittTextBox.Location = new System.Drawing.Point(277, 13);
            this._ittTextBox.Name = "_ittTextBox";
            this._ittTextBox.Size = new System.Drawing.Size(68, 20);
            this._ittTextBox.TabIndex = 59;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(190, 17);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(33, 13);
            this.label244.TabIndex = 60;
            this.label244.Text = "Iтт, A";
            // 
            // _typeYnComboBox
            // 
            this._typeYnComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typeYnComboBox.FormattingEnabled = true;
            this._typeYnComboBox.Location = new System.Drawing.Point(222, 146);
            this._typeYnComboBox.Name = "_typeYnComboBox";
            this._typeYnComboBox.Size = new System.Drawing.Size(178, 21);
            this._typeYnComboBox.TabIndex = 58;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(190, 149);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(26, 13);
            this.label245.TabIndex = 57;
            this.label245.Text = "Тип";
            // 
            // _inlnComboBox
            // 
            this._inlnComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inlnComboBox.FormattingEnabled = true;
            this._inlnComboBox.Location = new System.Drawing.Point(96, 92);
            this._inlnComboBox.Name = "_inlnComboBox";
            this._inlnComboBox.Size = new System.Drawing.Size(80, 21);
            this._inlnComboBox.TabIndex = 53;
            // 
            // _inlcComboBox
            // 
            this._inlcComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inlcComboBox.FormattingEnabled = true;
            this._inlcComboBox.Location = new System.Drawing.Point(96, 65);
            this._inlcComboBox.Name = "_inlcComboBox";
            this._inlcComboBox.Size = new System.Drawing.Size(80, 21);
            this._inlcComboBox.TabIndex = 52;
            // 
            // _inlbComboBox
            // 
            this._inlbComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inlbComboBox.FormattingEnabled = true;
            this._inlbComboBox.Location = new System.Drawing.Point(96, 39);
            this._inlbComboBox.Name = "_inlbComboBox";
            this._inlbComboBox.Size = new System.Drawing.Size(80, 21);
            this._inlbComboBox.TabIndex = 51;
            // 
            // _inlaComboBox
            // 
            this._inlaComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inlaComboBox.FormattingEnabled = true;
            this._inlaComboBox.Location = new System.Drawing.Point(96, 14);
            this._inlaComboBox.Name = "_inlaComboBox";
            this._inlaComboBox.Size = new System.Drawing.Size(80, 21);
            this._inlaComboBox.TabIndex = 50;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(21, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(34, 13);
            this.label248.TabIndex = 49;
            this.label248.Text = "Вх. In";
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(21, 68);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(34, 13);
            this.label249.TabIndex = 47;
            this.label249.Text = "Вх. Ic";
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(21, 42);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(34, 13);
            this.label250.TabIndex = 46;
            this.label250.Text = "Вх. Ib";
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(21, 17);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(34, 13);
            this.label251.TabIndex = 45;
            this.label251.Text = "Вх. Ia";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(190, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Группа";
            // 
            // groupCombo
            // 
            this.groupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupCombo.FormattingEnabled = true;
            this.groupCombo.Location = new System.Drawing.Point(277, 173);
            this.groupCombo.Name = "groupCombo";
            this.groupCombo.Size = new System.Drawing.Size(68, 21);
            this.groupCombo.TabIndex = 58;
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(190, 122);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(43, 13);
            this.label238.TabIndex = 73;
            this.label238.Text = "Тип ТТ";
            // 
            // _typettComboBox
            // 
            this._typettComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._typettComboBox.FormattingEnabled = true;
            this._typettComboBox.Location = new System.Drawing.Point(256, 119);
            this._typettComboBox.Name = "_typettComboBox";
            this._typettComboBox.Size = new System.Drawing.Size(89, 21);
            this._typettComboBox.TabIndex = 74;
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(190, 95);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(78, 13);
            this.label241.TabIndex = 67;
            this.label241.Text = "Токовый вход";
            // 
            // _tokinComboBox
            // 
            this._tokinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tokinComboBox.FormattingEnabled = true;
            this._tokinComboBox.Location = new System.Drawing.Point(277, 92);
            this._tokinComboBox.Name = "_tokinComboBox";
            this._tokinComboBox.Size = new System.Drawing.Size(68, 21);
            this._tokinComboBox.TabIndex = 70;
            // 
            // _u1TextBox
            // 
            this._u1TextBox.Location = new System.Drawing.Point(96, 147);
            this._u1TextBox.Name = "_u1TextBox";
            this._u1TextBox.Size = new System.Drawing.Size(68, 20);
            this._u1TextBox.TabIndex = 55;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(21, 149);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(40, 13);
            this.label246.TabIndex = 56;
            this.label246.Text = "Uн, кВ";
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(21, 122);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(49, 13);
            this.label247.TabIndex = 54;
            this.label247.Text = "Sн, МВА";
            // 
            // _s1TextBox
            // 
            this._s1TextBox.Location = new System.Drawing.Point(96, 119);
            this._s1TextBox.Name = "_s1TextBox";
            this._s1TextBox.Size = new System.Drawing.Size(68, 20);
            this._s1TextBox.TabIndex = 48;
            // 
            // SideTransformControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._typettComboBox);
            this.Controls.Add(this.label238);
            this.Controls.Add(this._unPolaryComboBox);
            this.Controls.Add(this.label239);
            this.Controls.Add(this._uabcPolaryComboBox);
            this.Controls.Add(this.label240);
            this.Controls.Add(this._tokinComboBox);
            this.Controls.Add(this.label241);
            this.Controls.Add(this._koefttComboBox);
            this.Controls.Add(this.label242);
            this.Controls.Add(this._ittnpComboBox);
            this.Controls.Add(this._ittnpTextBox);
            this.Controls.Add(this.label243);
            this.Controls.Add(this._ittComboBox);
            this.Controls.Add(this._ittTextBox);
            this.Controls.Add(this.label244);
            this.Controls.Add(this.groupCombo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._typeYnComboBox);
            this.Controls.Add(this.label245);
            this.Controls.Add(this._u1TextBox);
            this.Controls.Add(this.label246);
            this.Controls.Add(this._s1TextBox);
            this.Controls.Add(this.label247);
            this.Controls.Add(this._inlnComboBox);
            this.Controls.Add(this._inlcComboBox);
            this.Controls.Add(this._inlbComboBox);
            this.Controls.Add(this._inlaComboBox);
            this.Controls.Add(this.label248);
            this.Controls.Add(this.label249);
            this.Controls.Add(this.label250);
            this.Controls.Add(this.label251);
            this.Name = "SideTransformControl";
            this.Size = new System.Drawing.Size(433, 262);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox _unPolaryComboBox;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.ComboBox _uabcPolaryComboBox;
        private System.Windows.Forms.Label label240;
        private System.Windows.Forms.ComboBox _koefttComboBox;
        private System.Windows.Forms.Label label242;
        private System.Windows.Forms.ComboBox _ittnpComboBox;
        private System.Windows.Forms.MaskedTextBox _ittnpTextBox;
        private System.Windows.Forms.Label label243;
        private System.Windows.Forms.ComboBox _ittComboBox;
        private System.Windows.Forms.MaskedTextBox _ittTextBox;
        private System.Windows.Forms.Label label244;
        private System.Windows.Forms.ComboBox _typeYnComboBox;
        private System.Windows.Forms.Label label245;
        private System.Windows.Forms.ComboBox _inlnComboBox;
        private System.Windows.Forms.ComboBox _inlcComboBox;
        private System.Windows.Forms.ComboBox _inlbComboBox;
        private System.Windows.Forms.ComboBox _inlaComboBox;
        private System.Windows.Forms.Label label248;
        private System.Windows.Forms.Label label249;
        private System.Windows.Forms.Label label250;
        private System.Windows.Forms.Label label251;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox groupCombo;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.ComboBox _typettComboBox;
        private System.Windows.Forms.Label label241;
        private System.Windows.Forms.ComboBox _tokinComboBox;
        private System.Windows.Forms.MaskedTextBox _u1TextBox;
        private System.Windows.Forms.Label label246;
        private System.Windows.Forms.Label label247;
        private System.Windows.Forms.MaskedTextBox _s1TextBox;
    }
}
