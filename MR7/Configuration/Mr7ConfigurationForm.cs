﻿using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.TreeView;
using BEMN.Forms.Export;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR7.Configuration.Structures;
using BEMN.MR7.Configuration.Structures.Apv;
using BEMN.MR7.Configuration.Structures.Avr;
using BEMN.MR7.Configuration.Structures.CheckTn;
using BEMN.MR7.Configuration.Structures.ConfigSystem;
using BEMN.MR7.Configuration.Structures.Defenses;
using BEMN.MR7.Configuration.Structures.Defenses.BlockDefense;
using BEMN.MR7.Configuration.Structures.Defenses.Diff;
using BEMN.MR7.Configuration.Structures.Defenses.External;
using BEMN.MR7.Configuration.Structures.Defenses.F;
using BEMN.MR7.Configuration.Structures.Defenses.I;
using BEMN.MR7.Configuration.Structures.Defenses.I2I1;
using BEMN.MR7.Configuration.Structures.Defenses.Istar;
using BEMN.MR7.Configuration.Structures.Defenses.P;
using BEMN.MR7.Configuration.Structures.Defenses.Q;
using BEMN.MR7.Configuration.Structures.Defenses.StartArc;
using BEMN.MR7.Configuration.Structures.Defenses.U;
using BEMN.MR7.Configuration.Structures.Defenses.Z;
using BEMN.MR7.Configuration.Structures.Engine;
using BEMN.MR7.Configuration.Structures.Goose;
using BEMN.MR7.Configuration.Structures.InputSignals;
using BEMN.MR7.Configuration.Structures.Ls;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using BEMN.MR7.Configuration.Structures.Oscope;
using BEMN.MR7.Configuration.Structures.RelayInd;
using BEMN.MR7.Configuration.Structures.Sihronizm;
using BEMN.MR7.Configuration.Structures.Switch;
using BEMN.MR7.Configuration.Structures.UROV;
using BEMN.MR7.Configuration.Structures.Vls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.MR7.Configuration.Structures.RSTriggers;
using BEMN.MR7.Properties;

namespace BEMN.MR7.Configuration
{
    public partial class Mr7ConfigurationForm : Form, IFormView
    {
        #region [Constants]

        private const string READING_CONFIG = "Чтение конфигурации";
        private const string READ_OK = "Конфигурация прочитана";
        private const string READ_FAIL = "Конфигурация не может быть прочитана";
        private const string WRITING_CONFIG = "Идет запись конфигурации";
        private const string WRITE_OK = "Конфигурация записана в устройство";
        private const string WRITE_FAIL = "Конфигурация не может быть записана";
        private const string FILE_SAVE_FAIL = "Невозможно сохранить файл";
        private const string FILE_LOAD_FAIL = "Невозможно загрузить файл";
        private const string XML_HEAD = "MR7_SET_POINTS";
        private const string INVALID_PORT = "Порт недоступен.";
        private const string MR7_BASE_CONFIG_PATH = "\\MR801\\MR7_BaseConfig_v{0}_{1}.xml";
        private const string BASE_CONFIG_LOAD_SUCCESSFULLY = "Базовые уставки успешно загружены";
        private string _ipHi2Mem;
        private string _ipHi1Mem;
        private string _ipLo2Mem;
        private string _ipLo1Mem;
        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();
        #endregion [Constants]

        #region [Private fields]

        private double _version;
        private string _config;
        private readonly MemoryEntity<ConfigurationStruct> _configuration;

        /// <summary>
        /// Текущие уставки
        /// </summary>
        private ConfigurationStruct _currentSetpointsStruct;
        private readonly Mr7 _device;
        private bool _return;

        private ComboboxSelector _groupSetpointSelector;

        #region [Валидаторы]

        private StructUnion<AllOutputLogicSignalStruct> _vlsUnion;
        private StructUnion<AllKanalsStruct> _groupParamStructUnion;
        private StructUnion<AllSidesStruct> _sideStructUnion;

        private StructUnion<GroupSetpoint> _setpointUnion;
        private SetpointsValidator<AllGroupSetpointStruct, GroupSetpoint> _setpointsValidator;
        private NewStructValidator<MeasureTransStruct> _measureTransUnion;
        private StructUnion<ConfigurationStruct> _configurationValidator;
        #endregion [Валидаторы]

        #endregion [Private fields]

        #region [Ctor's]

        public Mr7ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr7ConfigurationForm(Mr7 device)
        {
            this._device = device;
            this._version = Common.VersionConverter(this._device.DeviceVersion);
            StringsConfig.CurrentVersion = this._version;

            this._config = this._device.DevicePlant;

            StringsConfig.DeviceType = !_device.IsConnect ? this._device.Info.Plant : this._device.Info.DeviceConfiguration;

            this.InitializeComponent();
            this.resistanceDefTabCtr1.Initialization();
            this.resistanceDefTabCtr1.IsPrimary = false;

            this._configuration = device.Configuration;
            this._configuration.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationReadOk);
            this._configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ConfigurationWriteOk);
            this._configuration.ReadOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.WriteOk += HandlerHelper.CreateHandler(this, this._progressBar.PerformStep);
            this._configuration.ReadFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationReadFail();
            });
            this._configuration.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._configuration.RemoveStructQueries();
                this.ConfigurationWriteFail();
            });
            this._currentSetpointsStruct = new ConfigurationStruct();
            this._progressBar.Maximum = this._configuration.Slots.Count;

            this.Init();
        }

        private void Init()
        {
            if (StringsConfig.CurrentVersion >= 3.02)
            {
                this._difensesI0DataGrid.Columns["_chooseCBColumn"].Visible = true;
                //this._difensesI0DataGrid.Columns["_iComboBoxCell"].Name = "I*";
            }

            this._copySetpoinsGroupComboBox.DataSource = StringsConfig.CopyGroupsNames;

            this.InitGroupParamSide();
            this.InitSideTransform();

            this._groupSetpointSelector = new ComboboxSelector(this._setpointsComboBox, StringsConfig.GroupsNames);
            this._groupSetpointSelector.OnRefreshInfoTable += OnRefreshInfoTable;

            #region Уставки

            #region [Защиты]

            //углы
            NewStructValidator<CornerStruct> cornerValidator = new NewStructValidator<CornerStruct>
                (
                this._toolTip,
                new ControlInfoText(this._iCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i0CornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._inCornerGr1, RulesContainer.UshortTo360),
                new ControlInfoText(this._i2CornerGr1, RulesContainer.UshortTo360)
                );

            NewStructValidator<DiffmainStruct> diffMainValidator = new NewStructValidator<DiffmainStruct>(
                this._toolTip,
                new ControlInfoCombo(this._modeDTZComboBox, StringsConfig.DefenseModes),
                new ControlInfoCombo(this._blockingDTZComboBox, StringsConfig.SwitchSignals),
                new ControlInfoText(this._constraintDTZTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._timeEnduranceDTZTextBox, RulesContainer.TimeRule),
                new ControlInfoCheck(this._modeI2I1),
                new ControlInfoCombo(this._perBlockI2I1, StringsConfig.BeNo),
                new ControlInfoText(this._I2I1TextBox, RulesContainer.UshortTo100),
                new ControlInfoCheck(this._modeI5I1),
                new ControlInfoCombo(this._perBlockI5I1, StringsConfig.BeNo),
                new ControlInfoText(this._I5I1TextBox, RulesContainer.UshortTo100),
                new ControlInfoText(this._Ib1BeginTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._K1AngleOfSlopeTextBox, new CustomUshortRule(0, 89)),
                new ControlInfoText(this._Ib2BeginTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._K2TangensTextBox, new CustomUshortRule(0, 89)),
                new ControlInfoCombo(this._oscDTZComboBox, StringsConfig.OscModes),
                new ControlInfoCombo(this._UROVDTZComboBox, StringsConfig.OffOn),
                new ControlInfoCombo(this._APVDTZComboBox, StringsConfig.Apv),
                new ControlInfoCombo(this._AVRDTZComboBox, StringsConfig.Apv)
            );

            NewStructValidator<DiffCutOffStruct> diffCutOffValidator = new NewStructValidator<DiffCutOffStruct>(
                this._toolTip,
                new ControlInfoCombo(this._modeDTOBTComboBox, StringsConfig.DefenseModes),
                new ControlInfoCombo(this._blockingDTOBTComboBox, StringsConfig.SwitchSignals),
                new ControlInfoText(this._constraintDTOBTTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._timeEnduranceDTOBTTextBox, RulesContainer.TimeRule),
                new ControlInfoCombo(this._stepOnInstantValuesDTOBTComboBox, StringsConfig.OffOn),
                new ControlInfoCombo(this._oscDTOBTComboBox, StringsConfig.OscModes),
                new ControlInfoCombo(this._UROVDTOBTComboBox, StringsConfig.OffOn),
                new ControlInfoCombo(this._APVDTOBTComboBox, StringsConfig.Apv),
                new ControlInfoCombo(this._AVRDTOBTComboBox, StringsConfig.Apv)
            );

            NewDgwValidatior<AllDiffIzeroStruct, DiffIzeroStruct> diffIdValidator = new NewDgwValidatior<AllDiffIzeroStruct, DiffIzeroStruct>(
                this._dif0DataGreed,
                3,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.DiffZeroStape, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.Binding),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(new CustomUshortRule(0, 89)),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(new CustomUshortRule(0, 89)),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCombo(StringsConfig.OffOn),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCombo(StringsConfig.Apv)
            );

            // I
            NewDgwValidatior<AllMtzMainStruct, MtzMainStruct> iValidator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                (
                new[] { this._difensesI14DataGrid, _difensesI56DataGrid, this._difensesI7DataGrid },
                new[] { 4, 2, 1 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesI, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.Condition, ColumnsType.COMBO, false, true, false),
                new ColumnInfoCombo(StringsConfig.Binding),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoText(RulesContainer.Ustavka256, true, true, false), //4
                new ColumnInfoCombo(StringsConfig.BlockTn),
                new ColumnInfoCombo(StringsConfig.Direction, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Undir, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Logic),
                new ColumnInfoCombo(StringsConfig.Characteristic, ColumnsType.COMBO, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000, true, true, false),
                new ColumnInfoCombo(StringsConfig.RelaySignals, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule, true, true, false),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka100, true, true, false),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCheck(true, true, false),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI14DataGrid, 
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24),
                        new TurnOffRule(5, false, false, 6)
                        ),
                    new TurnOffDgv
                    (
                        this._difensesI56DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24),
                        new TurnOffRule(5, false, false, 6)
					), 
                    new TurnOffDgv
                        (
                        this._difensesI7DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,24)
                        )
                }
            };
            //I*
            NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct> iStarValidator = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
               (
               this._difensesI0DataGrid,
               AllDefenseStarStruct.DEF_COUNT,
               this._toolTip,
               new ColumnInfoCombo(StringsConfig.NamesIStar, ColumnsType.NAME),
               new ColumnInfoCombo(StringsConfig.DefenseModes),
               new ColumnInfoText(RulesContainer.Ustavka40),
               new ColumnInfoCombo(StringsConfig.Binding),
               new ColumnInfoCheck(),
               new ColumnInfoText(RulesContainer.Ustavka256),
               new ColumnInfoCombo(StringsConfig.Direction),
               new ColumnInfoCombo(StringsConfig.Undir),
               new ColumnInfoCombo(StringsConfig.Choose),
               new ColumnInfoCombo(StringsConfig.LogycAndCurrent),
               new ColumnInfoCombo(StringsConfig.Characteristic),
               new ColumnInfoText(RulesContainer.TimeRule),
               new ColumnInfoText(RulesContainer.Ushort100To4000),
               new ColumnInfoCombo(StringsConfig.SwitchSignals),
               new ColumnInfoCombo(StringsConfig.OscModes),
               new ColumnInfoCombo(StringsConfig.RelaySignals),
               new ColumnInfoText(RulesContainer.TimeRule),
               new ColumnInfoCheck(),
               new ColumnInfoCheck(),
               new ColumnInfoCombo(StringsConfig.Apv),
               new ColumnInfoCombo(StringsConfig.Apv)
               )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesI0DataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19)
                        )
                }
            };

            //I2I1
            NewStructValidator<DefenseI2I1Struct> i2I1Validator = new NewStructValidator<DefenseI2I1Struct>
               (
               this._toolTip,
               new ControlInfoCombo(this.I2I1ModeComboGr1, StringsConfig.DefenseModes),
               new ControlInfoCombo(this.I2I1BlockingComboGr1, StringsConfig.SwitchSignals),
               new ControlInfoText(this.I2I1TBGr1, RulesContainer.Ustavka100),
               new ControlInfoCombo(this._sideI2I1ComboBox, StringsConfig.Binding),
               new ControlInfoText(this.I2I1tcpTBGr1, RulesContainer.TimeRule),
               new ControlInfoCombo(this.I2I1OscComboGr1, StringsConfig.OscModes),
               new ControlInfoCheck(this._urovI2I1CheckBox),
               new ControlInfoCombo(this.i2i1APV1, StringsConfig.Apv),
               new ControlInfoCombo(this.i2i1AVR, StringsConfig.Apv),
               new ControlInfoCombo(this._discretIn3Cmb, StringsConfig.SwitchSignals)
               );

            //Id
            NewStructValidator<StartArcProt> startArcProtValidator = new NewStructValidator<StartArcProt>(
                this._toolTip,
                new ControlInfoCombo(this.StartArcModeComboBox, StringsConfig.DefenseModesDug),
                new ControlInfoCombo(this.StartBlockArcComboBox, StringsConfig.SwitchSignals),
                new ControlInfoText(this.IcpArcTextBox, RulesContainer.Ustavka40),
                new ControlInfoCombo(this.SideArcComboBox, StringsConfig.Binding),
                new ControlInfoCheck(this.OscArc)
                );

            //U
            NewDgwValidatior<AllDefenceUStruct, DefenceUStruct> uValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
                (
                new[] { this._difensesUBDataGrid, this._difensesUMDataGrid },
                new[] { 4, 4 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.UStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true), //2
                new ColumnInfoCombo(StringsConfig.BindingU), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka256), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCheck(false, true), //9
                new ColumnInfoCombo(StringsConfig.BlockTn, ColumnsType.COMBO, false, true), //10
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //11
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheck(),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesUBDataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        ),
                    new TurnOffDgv
                        (
                        this._difensesUMDataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                            2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18)
                        )
                }
            };

            Func<IValidatingRule> fBDefFunc = () =>
            {
                if (!this._difensesFBDataGrid.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFBDataGrid.Tag as DataGridViewCell;
                    if (cell == null || 
                        this._difensesFBDataGrid[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };

            //F>
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fBValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFBDataGrid, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FBStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fBDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(StringsConfig.Apv),//13
                new ColumnInfoCheck(), //14
                new ColumnInfoCombo(StringsConfig.Apv)  //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFBDataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            Func<IValidatingRule> fMDefFunc = () =>
            {
                if (!this._difensesFMDataGrid.Columns[2].Visible)
                {
                    return RulesContainer.Ustavka40To60;
                }
                try
                {
                    DataGridViewCell cell = this._difensesFMDataGrid.Tag as DataGridViewCell;
                    if (cell == null || this._difensesFMDataGrid[2, cell.RowIndex].Value.ToString() == StringsConfig.FreqDefType[0])
                        return RulesContainer.Ustavka40To60;
                    return new CustomDoubleRule(0.05, 10);
                }
                catch
                {
                    return RulesContainer.Ustavka40To60;
                }
            };
            //F<
            NewDgwValidatior<AllDefenseFStruct, DefenseFStruct> fMValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                this._difensesFMDataGrid, 4, this._toolTip,
                new ColumnInfoCombo(StringsConfig.FMStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes), //1
                new ColumnInfoCombo(StringsConfig.FreqDefType), //2
                new ColumnInfoTextDependent(fMDefFunc), //3
                new ColumnInfoText(RulesContainer.Ustavka256), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.TimeRule), //6
                new ColumnInfoText(RulesContainer.Ustavka40To60), //7
                new ColumnInfoCheck(), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes), //10
                new ColumnInfoCheck(), //11
                new ColumnInfoCheck(), //12
                new ColumnInfoCombo(StringsConfig.Apv),//13
                new ColumnInfoCheck(), //14
                new ColumnInfoCombo(StringsConfig.Apv)  //15
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._difensesFMDataGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15),
                        new TurnOffRule(2, StringsConfig.FreqDefType[0], false, 4)
                        )
                }
            };

            NewDgwValidatior<AllDefenseReversPower, ReversePowerStruct> reversPowerValidator = new NewDgwValidatior
                <AllDefenseReversPower, ReversePowerStruct>
                (
                    this._reversePowerGrid,
                    2,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.StageDefType, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.DefenseModes),
                    new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                    new ColumnInfoText(RulesContainer.UshortFazaRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(RulesContainer.TimeRule),
                    new ColumnInfoText(new SignValidatingRule(-2.50, 2.50)),
                    new ColumnInfoCheck(),
                    new ColumnInfoText(RulesContainer.Ustavka40),
                    new ColumnInfoCombo(StringsConfig.SwitchSignals),
                    new ColumnInfoCombo(StringsConfig.BlockTn),
                    new ColumnInfoCombo(StringsConfig.OscModes),
                    new ColumnInfoCombo(StringsConfig.OffOn),
                    new ColumnInfoCombo(StringsConfig.OffOn),
                    new ColumnInfoCombo(StringsConfig.Apv),
                    new ColumnInfoCheck()
                )
            {
                TurnOff = new[]
                    {
                        new TurnOffDgv
                        (
                            this._reversePowerGrid,
                            new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
                        )
                    }
            };

            NewDgwValidatior<AllDefenseQStruct, DefenseQStruct> qValidator = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
               this._engineDefensesGrid,
               2,
               this._toolTip,
               new ColumnInfoCombo(StringsConfig.QStages, ColumnsType.NAME), //0
               new ColumnInfoCombo(StringsConfig.DefenseModes),
               new ColumnInfoText(RulesContainer.Ustavka256), //2
               new ColumnInfoCombo(StringsConfig.SwitchSignals), //3
               new ColumnInfoCombo(StringsConfig.OscModes),
               new ColumnInfoCheck(),
               new ColumnInfoCombo(StringsConfig.Apv),
               new ColumnInfoCombo(StringsConfig.Apv)
               )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._engineDefensesGrid,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7)
                        )
                }
            };

            NewStructValidator<DefenseTermBlockStruct> termBlockValidator = new NewStructValidator<DefenseTermBlockStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._engineQmodeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._engineQconstraintGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._engineQtimeGr1, RulesContainer.UshortRule),
                new ControlInfoText(this._numHot, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._numCold, new CustomUshortRule(0, 10)),
                new ControlInfoText(this._waitNumBlock, new CustomUshortRule(ushort.MinValue, ushort.MaxValue))
                );

            NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct> externalValidator = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                this._externalDefenses,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),//5
                new ColumnInfoCheck(), //6
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //7
                new ColumnInfoCombo(StringsConfig.OscModes),//8
                new ColumnInfoCheck(),//9
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv),
                new ColumnInfoCheck(),
                new ColumnInfoCombo(StringsConfig.Apv)
                )
            {
                TurnOff = new[]
                {
                    new TurnOffDgv
                        (
                        this._externalDefenses,
                        new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                        )
                }
            };

            StructUnion<DefensesSetpointsStruct> defensesUnion = new StructUnion<DefensesSetpointsStruct>
                (
                cornerValidator,
                diffMainValidator,
                diffCutOffValidator,
                diffIdValidator,
                iValidator,
                iStarValidator,
                i2I1Validator,
                startArcProtValidator,
                uValidator,
                fBValidator,
                fMValidator,
                qValidator,
                termBlockValidator,
                externalValidator,
                this.resistanceDefTabCtr1.ResistanceUnion,
                reversPowerValidator
                );

            #endregion [защиты]

            // Контроль цепей ТН
            NewStructValidator<CheckTnStruct> chechTnValidator = new NewStructValidator<CheckTnStruct>(
                this._toolTip,
                new ControlInfoCheck(this._i2u2CheckGr1),
                new ControlInfoText(this._u2ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i2ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._i0u0CheckGr1),
                new ControlInfoText(this._u0ContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._i0ContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._uMinContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._uMaxContrCepGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._iMinContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iMaxContrCepGr1, RulesContainer.Ustavka40),
                new ControlInfoText(this._iDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._uDelContrCepGr1, RulesContainer.Ustavka100),
                new ControlInfoText(this._tdContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._tsContrCepGr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._resetTnGr1, StringsConfig.SwitchSignals),
                new ControlInfoCheck(this.checkBox1)
                );
            //АПВ
            NewStructValidator<ApvStruct> apvValidator = new NewStructValidator<ApvStruct>(
                this._toolTip,
                new ControlInfoCombo(this._apvModeGr1, StringsConfig.ApvModes),
                new ControlInfoCheck(this._blokFromUrov),
                new ControlInfoCombo(this._disableApv, StringsConfig.SwitchSignals),
                new ControlInfoText(this._timeDisable, RulesContainer.TimeRule),
                new ControlInfoCombo(this._typeDisable, StringsConfig.DisableType),
                new ControlInfoCombo(this._apvBlockGr1, StringsConfig.SwitchSignals),
                new ControlInfoText(this._apvTblockGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvTreadyGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat1Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat2Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat3Gr1, RulesContainer.TimeRule),
                new ControlInfoText(this._apvKrat4Gr1, RulesContainer.TimeRule),
                new ControlInfoCombo(this._apvSwitchOffGr1, StringsConfig.BeNo)
                );

            // АВР
            NewStructValidator<AvrStruct> avrValidator = new NewStructValidator<AvrStruct>
            (
                this._toolTip,
                new ControlInfoCombo(this._avrBySignal, StringsConfig.BeNo),
                new ControlInfoCombo(this._avrByOff, StringsConfig.BeNo),
                new ControlInfoCombo(this._avrBySelfOff, StringsConfig.BeNo),
                new ControlInfoCombo(this._avrByDiff, StringsConfig.BeNo),
                new ControlInfoCombo(this._avrSIGNOn, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrBlocking, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrBlockClear, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrResolve, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._avrBack, StringsConfig.SwitchSignals),
                new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                new ControlInfoCombo(this._avrClear, StringsConfig.ForbiddenAllowed)
            );

            //Тепловая модель
            NewStructValidator<TermConfigStruct> termConfValidator = new NewStructValidator<TermConfigStruct>(
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeGr1, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineIdv, RulesContainer.Ustavka40),
                new ControlInfoText(this._iStartBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._tStartBox, RulesContainer.TimeRule),
                new ControlInfoText(this._tCount, RulesContainer.UshortTo65534),
                new ControlInfoText(this._qBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQreset, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._engineNreset, StringsConfig.SwitchSignals)
                );

                        // ЛС
            DataGridView[] lsBoxes =
            {
                this._lsAndDgv1Gr1, this._lsAndDgv2Gr1, this._lsAndDgv3Gr1, this._lsAndDgv4Gr1, this._lsAndDgv5Gr1, this._lsAndDgv6Gr1, this._lsAndDgv7Gr1,
                this._lsAndDgv8Gr1,
                this._lsOrDgv1Gr1, this._lsOrDgv2Gr1, this._lsOrDgv3Gr1, this._lsOrDgv4Gr1, this._lsOrDgv5Gr1, this._lsOrDgv6Gr1, this._lsOrDgv7Gr1,
                this._lsOrDgv8Gr1
            };

            foreach (DataGridView gridView in lsBoxes)
            {
                gridView.CellBeginEdit += this.GridOnCellBeginEdit;
            }

            NewDgwValidatior<InputLogicStruct>[] inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[AllInputLogicStruct.LOGIC_COUNT];
            for (int i = 0; i < AllInputLogicStruct.LOGIC_COUNT; i++)
            {
                inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
                    (
                    lsBoxes[i],
                    InputLogicStruct.DiscretsCount,
                    this._toolTip,
                    new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
                    new ColumnInfoCombo(StringsConfig.LsState)
                    );
            }
            StructUnion<AllInputLogicStruct> inputLogicUnion = new StructUnion<AllInputLogicStruct>(inputLogicValidator);

            // ВЛС
            CheckedListBox[] vlsBoxes =
                {
                    this.VLSclb1Gr1, this.VLSclb2Gr1, this.VLSclb3Gr1, this.VLSclb4Gr1, this.VLSclb5Gr1, this.VLSclb6Gr1, this.VLSclb7Gr1, this.VLSclb8Gr1,
                    this.VLSclb9Gr1, this.VLSclb10Gr1, this.VLSclb11Gr1, this.VLSclb12Gr1, this.VLSclb13Gr1, this.VLSclb14Gr1, this.VLSclb15Gr1,
                    this.VLSclb16Gr1
                };
            NewCheckedListBoxValidator<OutputLogicStruct>[] vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[AllOutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < AllOutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(vlsBoxes[i], StringsConfig.VlsSignals);
            }
            this._vlsUnion = new StructUnion<AllOutputLogicSignalStruct>(vlsValidator);



            // Синхронизм
            NewStructValidator<SinhronizmAddStruct> manualSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrManualUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrManualNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrManualdFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrManualdFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrManualdFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrManual),
                new ControlInfoCheck(this._catchSinchrManual),
                new ControlInfoCheck(this._blockTNmanual)
                );

            NewStructValidator<SinhronizmAddStruct> autoSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoModeGr1, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrAutoUmaxGr1, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYesGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoYesNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoCombo(this._sinhrAutoNoNoGr1, StringsConfig.NoYesDiscret),
                new ControlInfoText(this._sinhrAutodFGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrAutodFiGr1, new CustomUshortRule(0, 100)),
                new ControlInfoText(this._sinhrAutodFnoGr1, new CustomDoubleRule(0, 0.5)),
                new ControlInfoCheck(this._waitSinchrAuto),
                new ControlInfoCheck(this._catchSinchrAuto),
                new ControlInfoCheck(this._blockTNauto)
                );

            NewStructValidator<SinhronizmStruct> sinhronizmValidator = new NewStructValidator<SinhronizmStruct>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOtsGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNalGr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwaitGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTonGr1, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1Gr1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2Gr1, StringsConfig.Usinhr),
                new ControlInfoValidator(manualSinhronizmValidator),
                new ControlInfoValidator(autoSinhronizmValidator),
                new ControlInfoCombo(this._blockSinhCmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn1Cmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._discretIn2Cmb, StringsConfig.SwitchSignals),
                new ControlInfoText(this._sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrF, new CustomUshortRule(0, 360))
                );

            _measureTransUnion = new NewStructValidator<MeasureTransStruct>(
                this._toolTip,
                new ControlInfoValidator(this._sideStructUnion),
                new ControlInfoValidator(this._groupParamStructUnion),
                new ControlInfoCombo(this._numberOfWindingComboBox, StringsConfig.CountWinding)
                );
            
            this._setpointUnion = new StructUnion<GroupSetpoint>(
                defensesUnion,
                avrValidator,
                this.resistanceDefTabCtr1.ResistValidator,
                this.resistanceDefTabCtr1.LoadValidator,
                chechTnValidator,
                this.resistanceDefTabCtr1.SwingValidator,
                apvValidator,
                termConfValidator,
                inputLogicUnion,
                this._vlsUnion,
                sinhronizmValidator,
                this._measureTransUnion
                );

            this._setpointsValidator = new SetpointsValidator<AllGroupSetpointStruct, GroupSetpoint>
                (this._groupSetpointSelector, this._setpointUnion);


            this._clearCurrentLsButton.Visible = true;
            this._clearAllLsSygnalButton.Visible = true;
            this._clearCurrentLsnButton.Visible = true;
            this._clearAllLsnSygnalButton.Visible = true;
            this._resetCurrentVlsButton.Visible = true;
            this._resetAllVlsButton.Visible = true;

            #endregion Уставки

            // Выключатель
            NewStructValidator<SwitchStruct> switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._controlSolenoidCombo, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._comandOtkl, StringsConfig.ImpDlit),
                   new ControlInfoCombo(this._blockSDTU, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchSideCB, StringsConfig.Binding),
                   new ControlInfoCheck(this._switchYesNo)
                   );
            // Входные сигналы
            NewStructValidator<InputSignalStruct> inputSignalsValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUst1ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._grUst2ComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );

            // ВЛС

            allVlsCheckedListBoxs.Add(this.VLSclb1Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb2Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb3Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb4Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb5Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb6Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb7Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb8Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb9Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb10Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb11Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb12Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb13Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb14Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb15Gr1);
            allVlsCheckedListBoxs.Add(this.VLSclb16Gr1);

            // ЛС

            dataGridsViewLsAND.Add(this._lsAndDgv1Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv2Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv3Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv4Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv5Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv6Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv7Gr1);
            dataGridsViewLsAND.Add(this._lsAndDgv8Gr1);

            dataGridsViewLsOR.Add(this._lsOrDgv1Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv2Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv3Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv4Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv5Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv6Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv7Gr1);
            dataGridsViewLsOR.Add(this._lsOrDgv8Gr1);

            #region [Осц]
            NewStructValidator<OscopeConfigStruct> oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(this._oscWriteLength, new CustomUshortRule(0, 99)),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscCount)
                     );

            Func<string, Dictionary<ushort, string>> func = str =>
            {
                if (string.IsNullOrEmpty(str)) return StringsConfig.OscChannelSignals[0];
                int index = StringsConfig.OscBases.IndexOf(str);
                return index != -1 ? StringsConfig.OscChannelSignals[index] : StringsConfig.OscChannelSignals[0];
            };

            DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase> channelsValidator = new DgvValidatorWithDepend<OscopeAllChannelsStruct, ChannelWithBase>
                (
                this._oscChannelsGrid,
                OscopeAllChannelsStruct.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.OscChannelNames, ColumnsType.NAME),
                new ColumnInfoComboControl(StringsConfig.OscBases, 2),
                new ColumnInfoDictionaryComboDependent(func, 1)
                );
            this._oscChannelsGrid.CellBeginEdit += this.GridOnCellBeginEdit;
            NewStructValidator<ChannelStruct> startOscChannelValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            StructUnion<OscopeStruct> oscopeUnion = new StructUnion<OscopeStruct>(oscopeConfigValidator, channelsValidator, startOscChannelValidator);

            #endregion [Осц]

            #region [Реле и Индикаторы]

            NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct> releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                new [] { this._outputReleGrid, this._virtualReleDataGrid },
                new [] { AllReleOutputStruct.RelaysCount, AllReleOutputStruct.RELAY_COUNT_CONST - AllReleOutputStruct.RelaysCount },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct> indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                this._outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.Mode)
                );

            NewDgwValidatior<AllRsTriggersStruct, RsTriggersStruct> rsTriggersValidatior = new NewDgwValidatior<AllRsTriggersStruct, RsTriggersStruct>
            (
                this._rsTriggersDataGrid,
                AllRsTriggersStruct.RSTRIGGERS_COUNT_CONST,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RsTriggersName, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.RSPriority),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoCombo(StringsConfig.RelaySignals)
            );

            NewStructValidator<FaultStruct> faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._fault1CheckBox),
                new ControlInfoCheck(this._fault2CheckBox),
                new ControlInfoCheck(this._fault3CheckBox),
                new ControlInfoCheck(this._fault4CheckBox),
                new ControlInfoCheck(this._fault5CheckBox),
                new ControlInfoCheck(this._fault6CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            StructUnion<AutomaticsParametersStruct> automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>
                (releyValidator, rsTriggersValidatior, indicatorValidator, faultValidator);

            #endregion [Реле и Индикаторы]

            NewStructValidator<ConfigAddStruct> configAddValidator = new NewStructValidator<ConfigAddStruct>
                (this._toolTip,
                new ControlInfoCombo(this._inpAddCombo, StringsConfig.InpOporSignals),
                new ControlInfoCheck(this._resetSystemCheckBox),
                new ControlInfoCheck(this._resetAlarmCheckBox),
                new ControlInfoCheck(this._fixErrorFCheckBox)
                );

            StructUnion<ConfigResistDiagramStruct> resistConfigUnion = new StructUnion<ConfigResistDiagramStruct>
                (
                this.resistanceDefTabCtr1.ResistValidator,
                this.resistanceDefTabCtr1.LoadValidator,
                this.resistanceDefTabCtr1.SwingValidator,
                this.resistanceDefTabCtr1.ResistanceUnion
                );
            this.resistanceDefTabCtr1.ResistConfigUnion = resistConfigUnion;

            NewStructValidator<UrovStruct> urovValidator = new NewStructValidator<UrovStruct>
                (
                this._toolTip,
                new ControlInfoCheck(this._urovIcheck),
                new ControlInfoCheck(this._urovBkCheck),
                new ControlInfoCheck(this._urovSelf),
                new ControlInfoText(this._tUrov1, RulesContainer.TimeRule),
                new ControlInfoText(this._tUrov2, RulesContainer.TimeRule),
                new ControlInfoText(this._Iurov, RulesContainer.Ustavka40),
                new ControlInfoCombo(this._urovPusk, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._urovBlock, StringsConfig.SwitchSignals)
                );


            ComboBox[] bgs =
            {
                this.goin1,this.goin2,this.goin3,this.goin4,this.goin5,this.goin6,this.goin7,this.goin8,this.goin9,this.goin10,
                this.goin11,this.goin12,this.goin13,this.goin14,this.goin15,this.goin16,this.goin17,this.goin18,this.goin19,this.goin20,
                this.goin21,this.goin22,this.goin23,this.goin24,this.goin25,this.goin26,this.goin27,this.goin28,this.goin29,this.goin30,
                this.goin31,this.goin32,this.goin33,this.goin34,this.goin35,this.goin36,this.goin37,this.goin38,this.goin39,this.goin40,
                this.goin41,this.goin42,this.goin43,this.goin44,this.goin45,this.goin46,this.goin47,this.goin48,this.goin49,this.goin50,
                this.goin51,this.goin52,this.goin53,this.goin54,this.goin55,this.goin56,this.goin57,this.goin58,this.goin59,this.goin60,
                this.goin61,this.goin62,this.goin63,this.goin64
            };

            NewStructValidator<ConfigIPStruct> ethernetValidator = new NewStructValidator<ConfigIPStruct>(
                this._toolTip,
                new ControlInfoText(this._ipLo1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipLo2, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi1, new CustomUshortRule(0, 255)),
                new ControlInfoText(this._ipHi2, new CustomUshortRule(0, 255)));

            NewStructValidator<Goose> gooseValidator = new NewStructValidator<Goose>
                (
                this._toolTip,
                new ControlInfoCombo(this.operationBGS, StringsConfig.GooseConfig),
                new ControlInfoMultiCombo(bgs, StringsConfig.GooseSignal)
                );

            SetpointsValidator<GooseConfig, Goose> allGooseSetpointValidator = new SetpointsValidator<GooseConfig, Goose>
                (new ComboboxSelector(this.currentBGS, StringsConfig.GooseNames), gooseValidator);

            this._configurationValidator = new StructUnion<ConfigurationStruct>
                (
                this._setpointsValidator,
                switchValidator,
                inputSignalsValidator,
                oscopeUnion,
                automaticsParametersUnion,
                ethernetValidator,
                configAddValidator,
                urovValidator,
                allGooseSetpointValidator
                );

            this.HideAndShow();
        }


        private void InitSideTransform()
        {
            var sideTransformControlsPage = new[]
            {
                this.sideTransformControl1, this.sideTransformControl2, this.sideTransformControl3, this.sideTransformControl4
            };

            for (int i = 0; i < sideTransformControlsPage.Length; i++)
            {
                sideTransformControlsPage[i].IsSide1 = i == 0;
            }

            IValidator[] sideParamValidators =
            {
                this.sideTransformControl1.SideStructValidator, this.sideTransformControl2.SideStructValidator,
                this.sideTransformControl3.SideStructValidator, this.sideTransformControl4.SideStructValidator
            };
            this._sideStructUnion = new StructUnion<AllSidesStruct>(sideParamValidators);
        }


        private void InitGroupParamSide()
        {
            var groupParamSidePage = new[]
            {
                this.groupParamSideControl1, this.groupParamSideControl2, this.groupParamSideControl3
            };

            for(int i = 0; i < groupParamSidePage.Length; i++)
            {
                groupParamSidePage[i].IsUnChannel = i == groupParamSidePage.Length - 1;
            }

            IValidator[] groupUabcValidators =
            {
                this.groupParamSideControl1.GroupUabcValidator, this.groupParamSideControl2.GroupUabcValidator, this.groupParamSideControl3.GroupUabcValidator
            };

            this._groupParamStructUnion = new StructUnion<AllKanalsStruct>(groupUabcValidators);
        }

        #endregion [Ctor's]

        #region [MemoryEntity Events Handlers]

        /// <summary>
        /// Конфигурация успешно прочитана
        /// </summary>
        private void ConfigurationReadOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_OK;
            this._currentSetpointsStruct = this._configuration.Value;
            this.ShowConfiguration();
            MessageBox.Show(READ_OK);
        }

        /// <summary>
        /// Ошибка чтения конфигурации
        /// </summary>
        private void ConfigurationReadFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = READ_FAIL;
            MessageBox.Show(READ_FAIL);
        }

        /// <summary>
        /// Конфигурация успешно записана
        /// </summary>
        private void ConfigurationWriteOk()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_OK;
            this._device.SetBit(this._device.DeviceNumber, 0xd00, true, "Сохранить конфигурацию", this._device);
        }

        /// <summary>
        /// Ошибка записи конфигурации
        /// </summary>
        private void ConfigurationWriteFail()
        {
            this.IsProcess = false;
            this._progressBar.Value = this._progressBar.Maximum;
            this._statusLabel.Text = WRITE_FAIL;
            MessageBox.Show(WRITE_FAIL);
        }

        #endregion [MemoryEntity Events Handlers]

        #region [Help members]

        private void GridOnCellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.contextMenu.Tag = sender;
        }

        /// <summary>
        /// Выводит все данные на экран
        /// </summary>
        private void ShowConfiguration()
        {
            this._configurationValidator.Set(this._currentSetpointsStruct);
            if (this._resistCheck.Checked) this.GetPrimaryResistValue();
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
        }

        /// <summary>
        /// Сохранение значений IP
        /// </summary>
        public void GetIpAddress()
        {
            this._ipHi2Mem = this._ipHi2.Text;
            this._ipHi1Mem = this._ipHi1.Text;
            this._ipLo2Mem = this._ipLo2.Text;
            this._ipLo1Mem = this._ipLo1.Text;
        }

        public void SetIpAddress()
        {
            this._ipHi2.Text = this._ipHi2Mem;
            this._ipHi1.Text = this._ipHi1Mem;
            this._ipLo2.Text = this._ipLo2Mem;
            this._ipLo1.Text = this._ipLo1Mem;
        }

        private bool IsProcess
        {
            set
            {
                this._readConfigBut.Enabled = !value;
                this._writeConfigBut.Enabled = !value;
                this._resetSetpointsButton.Enabled = !value;
                this._clearSetpointBtn.Enabled = !value;
                this._loadConfigBut.Enabled = !value;
                this._saveConfigBut.Enabled = !value;
                this._saveToXmlButton.Enabled = !value;
            }
        }
        
        //todo сделано специально для FBGrid т.к. обычный валидатор пропускал почему-то
        private bool CheckFbGrid
        {
            get
            {
                bool error = false; 

                var cashe1 = _difensesFBDataGrid[2, 0].Value;
                var cache1 = _difensesFBDataGrid[0, 0].Value;
                string value1 = cashe1.ToString();
                string val1 = cache1.ToString();

                var cashe2 = _difensesFBDataGrid[2, 1].Value;
                var cache2 = _difensesFBDataGrid[0, 1].Value;
                string value2 = cashe2.ToString();
                string val2 = cache2.ToString();

                var cashe3 = _difensesFBDataGrid[2, 2].Value;
                var cache3 = _difensesFBDataGrid[0, 2].Value;
                string value3 = cashe3.ToString();
                string val3 = cache3.ToString();

                var cashe4 = _difensesFBDataGrid[2, 3].Value;
                var cache4 = _difensesFBDataGrid[0, 3].Value;
                string value4 = cashe4.ToString();
                string val4 = cache4.ToString();

                var cashe5 = _difensesFBDataGrid[3, 0].Value;
                double value5 = Convert.ToDouble(cashe5);

                var cashe6 = _difensesFBDataGrid[3, 1].Value;
                double value6 = Convert.ToDouble(cashe6);

                var cashe7 = _difensesFBDataGrid[3, 2].Value;
                double value7 = Convert.ToDouble(cashe7);

                var cashe8 = _difensesFBDataGrid[3, 3].Value;
                double value8 = Convert.ToDouble(cashe8);

                cashe5 = value1 == "Частота" && value5 == 0 ? 40 : cashe5;
                cashe6 = value2 == "Частота" && value6 == 0 ? 40 : cashe6;
                cashe7 = value3 == "Частота" && value7 == 0 ? 40 : cashe7;
                cashe8 = value4 == "Частота" && value8 == 0 ? 40 : cashe8;

                value5 = Convert.ToDouble(cashe5);
                value6 = Convert.ToDouble(cashe6);
                value7 = Convert.ToDouble(cashe7);
                value8 = Convert.ToDouble(cashe8);

                _difensesFBDataGrid[3, 0].Value = cashe5;
                _difensesFBDataGrid[3, 1].Value = cashe6;
                _difensesFBDataGrid[3, 2].Value = cashe7;
                _difensesFBDataGrid[3, 3].Value = cashe8;

                if (value1 == "Частота" && value5 < 40 && val1 != "Выведено") 
                {
                    _difensesFBDataGrid.Rows[0].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }

                if (value1 == "dF/dt" && value5 > 10 && val1 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[0].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }
                
                if (value2 == "Частота" && value6 < 40 && val2 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[1].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }

                if (value2 == "dF/dt" && value6 > 10 && val2 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[1].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }
                
                if (value3 == "Частота" && value7 < 40 && val3 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[2].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }

                if (value3 == "dF/dt" && value7 > 10 && val3 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[2].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }
                
                if (value4 == "Частота" && value8 < 40 && val4 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[3].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }

                if (value4 == "dF/dt" && value8 > 10 && val4 != "Выведено")
                {
                    _difensesFBDataGrid.Rows[3].Cells[3].Style.BackColor = Color.Red;
                    _setpointsTab.SelectedIndex = 5;
                    error = true;
                }

                if (error) return false;

                return true;
            }
        }

        /// <summary>
        /// Читает все данные с экрана
        /// </summary>
        private bool WriteConfiguration()
        {
            this.IsProcess = true;
            string message;
            this._statusLabel.Text = "Проверка уставок";
            this.statusStrip1.Update();

            if (this._configurationValidator.Check(out message, true) && CheckFbGrid)
            {
                if (this._resistCheck.Checked)
                    this.GetSecondResistValue(); //если галочка стоит, то переводим во вторичные единицы
                this._statusLabel.Text = WRITING_CONFIG;
                this._currentSetpointsStruct = this._configurationValidator.Get();
                this._configuration.Value = this._currentSetpointsStruct;
                return true;
            }

            MessageBox.Show("Обнаружены неккоректные уставки. Конфигурация не может быть записана.");
            this.IsProcess = false;
            return false;
        }

        /// <summary>
        /// Запуск чтения конфигурации
        /// </summary>
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.IsProcess = true;
            this._statusLabel.Text = READING_CONFIG;
            this._progressBar.Value = 0;
            this._configuration.LoadStruct();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }
        
        private void GetSecondResistValue()
        {
            this.IsProcess = true;
            this.resistanceDefTabCtr1.GetSecondValue();
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        private void GetPrimaryResistValue()
        {
            this.IsProcess = true;
            this.resistanceDefTabCtr1.GetPrimaryValue();
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            this.IsProcess = false;
        }

        #endregion [Help members]

        #region [Event handlers]
        
        private void OnRefreshInfoTable()
        {
            this.resistanceDefTabCtr1.RefreshResistInfoTable();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _difensesI56DataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            int rowIndex = e.RowIndex;
            DataGridView dataGridView = (DataGridView)sender;
            if (dataGridView.Rows[rowIndex].Cells[1].Value.ToString() == StringsConfig.DefenseModes[0])
            {
                Validator.CellEnabled(this._difensesI56DataGrid.Rows[rowIndex].Cells[4], false);
                return;
            }
            if (dataGridView.Rows[rowIndex].Cells[1].Value.ToString() != StringsConfig.DefenseModes[0] &&
                dataGridView.Rows[rowIndex].Cells[3].Value.ToString() == StringsConfig.Condition[0])
            {
                Validator.CellEnabled(this._difensesI56DataGrid.Rows[rowIndex].Cells[4], true);
            }
            else
            {
                Validator.CellEnabled(this._difensesI56DataGrid.Rows[rowIndex].Cells[4], false);
            }
        }

        private void _difensesI0DataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (StringsConfig.CurrentVersion < 3.02) return;
            try
            {
                string value = (string) _difensesI0DataGrid.Rows[e.RowIndex].Cells[8].Value;
                if (StringsConfig.Choose[1] == value)
                {
                    RefreshValueInDataGrid(_difensesI0DataGrid, e.RowIndex, 1, StringsConfig.Logic, 9);
                }
                else
                {
                    RefreshValueInDataGrid(_difensesI0DataGrid, e.RowIndex, 1, StringsConfig.I, 9);
                }

            }
            catch (Exception ex)
            {
            }

        }

        public void RefreshValueInDataGrid(DataGridView dataGrid, int startIndex, int countInDataGrid, List<string> list, params int[] cell)
        {
            //пробегаемся по всем строкам в data grid
            for (int count = startIndex; count < countInDataGrid + startIndex; count++)
            {
                //бежим марафон по ячейкам
                for (int param = 0; param < cell.Length; param++)
                {
                    var cellTemp = dataGrid.Rows[count].Cells[cell[param]];
                    var cellCb = cellTemp as DataGridViewComboBoxCell;

                    cellCb.Items.Clear();

                    //заполняем ячейку сигналами из устройства
                    for (int j = 0; j < list.Count; j++)
                    {
                        cellCb.Items.Add(list[j]);
                    }
                }
            }
        }

        private void _difensesI0DataGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void Mr7ConfigurationForm_Load(object sender, EventArgs e)
        {
            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this.StartRead();
            }
            this.ResetSetpoints(false);
        }

        private void Mr7ConfigurationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._configuration.RemoveStructQueries();
        }

        private void Mr7ConfigurationForm_Activated(object sender, EventArgs e)
        {
            StringsConfig.CurrentVersion = Common.VersionConverter(this._device.DeviceVersion);
            StringsConfig.DeviceType = !_device.IsConnect ? this._device.Info.Plant : this._device.Info.DeviceConfiguration;
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            this.WriteConfig();
        }

        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;

            //if (this._device.MB.IsPortInvalid)
            //{
            //    MessageBox.Show(INVALID_PORT);
            //    return;
            //}

            DialogResult result = MessageBox.Show(string.Format("Записать конфигурацию МР 801 №{0}? " +
                                                                "\nВ устройство будет записан IP-адрес: {1}.{2}.{3}.{4}!",
                    this._device.DeviceNumber, this._ipHi2.Text, this._ipHi1.Text, this._ipLo2.Text, this._ipLo1.Text),
                "Запись", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes) return;
            this._progressBar.Value = 0;
            if (!this.WriteConfiguration()) return;
            this._configuration.SaveStruct();

        }
        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox == null) return;
            int ind = comboBox.SelectedIndex;
            int OSC_SIZE = 0;

            switch (_device.Info.DeviceConfiguration)
            {
                case "T12N5D58R51":
                case "T9N8D58R51":
                case "T12N6D58R51":
                    OSC_SIZE = 36352 * 2;
                    break;
                case "T6N3D42R35":
                    OSC_SIZE = 54528 * 2;
                    break;
                default:
                    OSC_SIZE = 39656 * 2;
                    break;
            }

            this._oscSizeTextBox.Text = (OSC_SIZE / (ind + 2)).ToString(); //StringsConfig.OscLen[ind];
        }

        private void _resetSetpointsButton_Click(object sender, EventArgs e)
        {
            this.GetIpAddress();
            this.ResetSetpoints(true);
            this.SetIpAddress();
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
        }

        private void _clearSetpointsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this._configurationValidator.Reset("MR7");
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            MessageBox.Show("Обнуление уставок завершено успешно!");
        }

        private void _switchYesNo_CheckedChanged(object sender, EventArgs e)
        {
            this.HideAndShow();
        }

        private void HideAndShow()
        {
            if (!this._switchYesNo.Checked)
            {
                this.HideAndShowPages();
                this.HideAndShowParamSwitch(true);
            }
            else
            {
                this.HideAndShowPages();
                this.HideAndShowParamSwitch(false);
            }
        }

        private void HideAndShowParamSwitch(bool value)
        {
            this._switchConfigGroupBox.Enabled = !value;
            //this.UROVgroupBox.Enabled = !value;
        }

        private void HideAndShowPages()
        {
            if (this._setpointsTab.TabPages.Contains(_apvGr1) && this._setpointsTab.TabPages.Contains(_ksuppnGr1))
            {
                this._setpointsTab.TabPages.Remove(_apvGr1);
                this._setpointsTab.TabPages.Remove(_ksuppnGr1);
            }
            else
            {
                this._setpointsTab.TabPages.Add(_apvGr1);
                this._setpointsTab.TabPages.Add(_ksuppnGr1);
            }
        }
        
        private void _resistCheck_CheckedChanged(object sender, EventArgs e)
        {
            if (this._return)
            {
                this._return = false;
                return;
            }

            if (this._measureTransUnion.Check(false))
            {
                MeasureTransStruct curTrans = this._measureTransUnion.Get();
                KanalTransU channelU = this.GetChannelU(curTrans);
                if (channelU == null)
                {
                    MessageBox.Show("Сторона 1 не привязана к группе Uabc", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._return = true;
                    this._resistCheck.Checked = !this._resistCheck.Checked;
                    return;
                }
                this.ResistValueCheck(curTrans.Sides[0], channelU);
            }
            else
            {
                MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки параметров сторон!", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._return = true;
                this._resistCheck.Checked = !this._resistCheck.Checked;
            }
        }

        private void ResistValueCheck(SideStruct side, KanalTransU channelU)
        {
            if (this._resistCheck.Checked)
            {
                DialogResult res =
                    MessageBox.Show("Сопротивления будут пересчитаны в первичные величины. Обратите внимание на корректность введенных значений Iтт и Kтн",
                        "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);

                bool check = this.resistanceDefTabCtr1.ResistValidator.Check(out string mes, false)
                             && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);

                if (check)
                {
                    if (res == DialogResult.OK)
                    {
                        this.resistanceDefTabCtr1.SideMeasureTrans = side;
                        this.resistanceDefTabCtr1.ChannelUMeasureTrans = channelU;
                        this.resistanceDefTabCtr1.IsPrimary = true;
                        this.GetPrimaryResistValue();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this._return = true;
                this._resistCheck.Checked = false;
            }
            else
            {
                DialogResult res = MessageBox.Show("Сопротивления будут пересчитаны во вторичные величины к размерности Ом*Iн.вт. Обратите внимание на корректность введенных значений Iтт и Kтн",
                    "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                bool check = this.resistanceDefTabCtr1.ResistValidator.Check(out string mes, false)
                             && this.resistanceDefTabCtr1.LoadValidator.Check(out mes, false)
                             && this.resistanceDefTabCtr1.SwingValidator.Check(out mes, false);
                if (check)
                {
                    if (res == DialogResult.OK)
                    {
                        this.resistanceDefTabCtr1.SideMeasureTrans = side;
                        this.resistanceDefTabCtr1.ChannelUMeasureTrans = channelU;
                        this.resistanceDefTabCtr1.IsPrimary = false;
                        this.GetSecondResistValue();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Невозможно произвести пересчет - обнаружены некорректные уставки", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                this._return = true;
                this._resistCheck.Checked = true;
            }
        }

        private KanalTransU GetChannelU(MeasureTransStruct curTrans)
        {
            SideStruct side = curTrans.Sides[0];
            int index = StringsConfig.PolarisUabc.IndexOf(side.PolarisUabc);
            if (index == 0)
            {
                return null;
            }
            return curTrans.Uabc[index - 1];
        }
        
        private void Mr7ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.StartRead();
                    break;
                case Keys.S:
                    this.SaveInFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }
        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            if (menu == null) return;
            DataGridView dgv = menu.Tag as DataGridView;
            dgv?.EndEdit();
            menu.Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.StartRead();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveInFile();
                return;
            }
            if (e.ClickedItem == this.resetSetpointsItem)
            {
                this.GetIpAddress();
                this.ResetSetpoints(true);
                this.SetIpAddress();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
                return;
            }
            if (e.ClickedItem == this.clearSetpointsItem)
            {
                DialogResult res = MessageBox.Show("Обнулить уставки?", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;                
                this._configurationValidator.Reset();
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            }
            if (e.ClickedItem == this.writeToHtmlItem)
            {
                this.SaveToHtml();
            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]{
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.clearSetpointsItem,
                this.resetSetpointsItem,
                this.readFromFileItem,
                this.writeToFileItem,
                this.writeToHtmlItem});
            this.readFromDeviceItem.Enabled = this.writeToDeviceItem.Enabled = this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode;
        }


        #region [Обнуление сигналов ЛС 1-8]

        /// <summary>
        /// Отслеживаем, какой из табов выбран в ЛС 1-8
        /// </summary>

        public bool _tP39;
        public bool _tP40;
        public bool _tP41;
        public bool _tP42;
        public bool _tP43;
        public bool _tP44;
        public bool _tP45;
        public bool _tP46;

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl.SelectedTab == this.tabPage39)
            {
                this._tP39 = true;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage40)
            {
                this._tP39 = false;
                this._tP40 = true;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage41)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = true;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage42)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = true;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage43)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = true;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage44)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = true;
                this._tP45 = false;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage45)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = true;
                this._tP46 = false;
                return;
            }
            if (this.tabControl.SelectedTab == this.tabPage46)
            {
                this._tP39 = false;
                this._tP40 = false;
                this._tP41 = false;
                this._tP42 = false;
                this._tP43 = false;
                this._tP44 = false;
                this._tP45 = false;
                this._tP46 = true;
                return;
            }
        }

        /// <summary>
        /// Обнуление сигналов ЛС 1-8
        /// </summary>
        private void ResetCurrentLs()
        {
            if (this._tP39)
            {
                for (int i = 0; i < this._lsAndDgv1Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP40)
            {
                for (int i = 0; i < this._lsAndDgv2Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP41)
            {
                for (int i = 0; i < this._lsAndDgv3Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP42)
            {
                for (int i = 0; i < this._lsAndDgv4Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP43)
            {
                for (int i = 0; i < this._lsAndDgv5Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP44)
            {
                for (int i = 0; i < this._lsAndDgv6Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP45)
            {
                for (int i = 0; i < this._lsAndDgv7Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP46)
            {
                for (int i = 0; i < this._lsAndDgv8Gr1.Rows.Count; i++)
                {
                    this._lsAndDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
        }

        private void ClearAllLs()
        {
            for (int i = 0; i < this._lsAndDgv1Gr1.Rows.Count; i++)
            {
                this._lsAndDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsAndDgv2Gr1.Rows.Count; i++)
            {
                this._lsAndDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < this._lsAndDgv3Gr1.Rows.Count; i++)
            {
                this._lsAndDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < this._lsAndDgv4Gr1.Rows.Count; i++)
            {
                this._lsAndDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsAndDgv5Gr1.Rows.Count; i++)
            {
                this._lsAndDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsAndDgv6Gr1.Rows.Count; i++)
            {
                this._lsAndDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsAndDgv7Gr1.Rows.Count; i++)
            {
                this._lsAndDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsAndDgv8Gr1.Rows.Count; i++)
            {
                this._lsAndDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

        }

        private void _clearCurrentLsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            if (this.tabControl.SelectedTab == this.tabPage39)
            {
                this._tP39 = true;
            }
            this.ResetCurrentLs();
        }

        private void _clearAllLsSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 1-8?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLs();
        }

        #endregion

        #region [Обнуление сигналов ЛС 9-16]

        public bool _tP31;
        public bool _tP32;
        public bool _tP33;
        public bool _tP34;
        public bool _tP35;
        public bool _tP36;
        public bool _tP37;
        public bool _tP38;

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedTab == this.tabPage31)
            {
                this._tP31 = true;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage32)
            {
                this._tP31 = false;
                this._tP32 = true;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage33)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = true;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage34)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = true;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage35)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = true;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage36)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = true;
                this._tP37 = false;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage37)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = true;
                this._tP38 = false;
                return;
            }
            if (this.tabControl1.SelectedTab == this.tabPage38)
            {
                this._tP31 = false;
                this._tP32 = false;
                this._tP33 = false;
                this._tP34 = false;
                this._tP35 = false;
                this._tP36 = false;
                this._tP37 = false;
                this._tP38 = true;
                return;
            }
        }

        /// <summary>
        /// Обнуление сигналов ЛС 9-16
        /// </summary>
        private void ResetCurrentLsn()
        {
            if (this._tP31)
            {
                for (int i = 0; i < this._lsOrDgv1Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP32)
            {
                for (int i = 0; i < this._lsOrDgv2Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP33)
            {
                for (int i = 0; i < this._lsOrDgv3Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP34)
            {
                for (int i = 0; i < this._lsOrDgv4Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP35)
            {
                for (int i = 0; i < this._lsOrDgv5Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP36)
            {
                for (int i = 0; i < this._lsOrDgv6Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP37)
            {
                for (int i = 0; i < this._lsOrDgv7Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
            if (this._tP38)
            {
                for (int i = 0; i < this._lsOrDgv8Gr1.Rows.Count; i++)
                {
                    this._lsOrDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
                }
            }
        }

        private void ClearAllLsn()
        {
            for (int i = 0; i < this._lsOrDgv1Gr1.Rows.Count; i++)
            {
                this._lsOrDgv1Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsOrDgv2Gr1.Rows.Count; i++)
            {
                this._lsOrDgv2Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < this._lsOrDgv3Gr1.Rows.Count; i++)
            {
                this._lsOrDgv3Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }


            for (int i = 0; i < this._lsOrDgv4Gr1.Rows.Count; i++)
            {
                this._lsOrDgv4Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsOrDgv5Gr1.Rows.Count; i++)
            {
                this._lsOrDgv5Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsOrDgv6Gr1.Rows.Count; i++)
            {
                this._lsOrDgv6Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsOrDgv7Gr1.Rows.Count; i++)
            {
                this._lsOrDgv7Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

            for (int i = 0; i < this._lsOrDgv8Gr1.Rows.Count; i++)
            {
                this._lsOrDgv8Gr1.Rows[i].Cells[1].Value = StringsConfig.LsState[0];
            }

        }

        private void _clearCurrentLsnButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (this.tabControl1.SelectedTab == this.tabPage31)
            {
                this._tP31 = true;
            }

            this.ResetCurrentLsn();
        }

        private void _clearAllLsnSygnalButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигналы ЛС 9-16?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this.ClearAllLsn();
        }

        #endregion


        #region [Обнуление сигналов ВЛС]

        private bool _tBVLS1;
        private bool _tBVLS2;
        private bool _tBVLS3;
        private bool _tBVLS4;
        private bool _tBVLS5;
        private bool _tBVLS6;
        private bool _tBVLS7;
        private bool _tBVLS8;
        private bool _tBVLS9;
        private bool _tBVLS10;
        private bool _tBVLS11;
        private bool _tBVLS12;
        private bool _tBVLS13;
        private bool _tBVLS14;
        private bool _tBVLS15;
        private bool _tBVLS16;

        private void ChangeTextVls()
        {
            this.tabControl2.TabPages[0].Text = this.VLSclb1Gr1.CheckedItems.Count != 0 ? "ВЛС1*" : "ВЛС 1";
            this.tabControl2.TabPages[1].Text = this.VLSclb2Gr1.CheckedItems.Count != 0 ? "ВЛС2*" : "ВЛС 2";
            this.tabControl2.TabPages[2].Text = this.VLSclb3Gr1.CheckedItems.Count != 0 ? "ВЛС3*" : "ВЛС 3";
            this.tabControl2.TabPages[3].Text = this.VLSclb4Gr1.CheckedItems.Count != 0 ? "ВЛС4*" : "ВЛС 4";
            this.tabControl2.TabPages[4].Text = this.VLSclb5Gr1.CheckedItems.Count != 0 ? "ВЛС5*" : "ВЛС 5";
            this.tabControl2.TabPages[5].Text = this.VLSclb6Gr1.CheckedItems.Count != 0 ? "ВЛС6*" : "ВЛС 6";
            this.tabControl2.TabPages[6].Text = this.VLSclb7Gr1.CheckedItems.Count != 0 ? "ВЛС7*" : "ВЛС 7";
            this.tabControl2.TabPages[7].Text = this.VLSclb8Gr1.CheckedItems.Count != 0 ? "ВЛС8*" : "ВЛС 8";
            this.tabControl2.TabPages[8].Text = this.VLSclb9Gr1.CheckedItems.Count != 0 ? "ВЛС9*" : "ВЛС9";
            this.tabControl2.TabPages[9].Text = this.VLSclb10Gr1.CheckedItems.Count != 0 ? "ВЛС10*" : "ВЛС10";
            this.tabControl2.TabPages[10].Text = this.VLSclb11Gr1.CheckedItems.Count != 0 ? "ВЛС11*" : "ВЛС11";
            this.tabControl2.TabPages[11].Text = this.VLSclb12Gr1.CheckedItems.Count != 0 ? "ВЛС12*" : "ВЛС12";
            this.tabControl2.TabPages[12].Text = this.VLSclb13Gr1.CheckedItems.Count != 0 ? "ВЛС13*" : "ВЛС13";
            this.tabControl2.TabPages[13].Text = this.VLSclb14Gr1.CheckedItems.Count != 0 ? "ВЛС14*" : "ВЛС14";
            this.tabControl2.TabPages[14].Text = this.VLSclb15Gr1.CheckedItems.Count != 0 ? "ВЛС15*" : "ВЛС15";
            this.tabControl2.TabPages[15].Text = this.VLSclb16Gr1.CheckedItems.Count != 0 ? "ВЛС16*" : "ВЛС16";

        }

        private void ResetCurrentVlsSignal()
        {
            if (this._tBVLS1)
            {
                for (int i = 0; i < this.VLSclb1Gr1.Items.Count; i++)
                {
                    this.VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS2)
            {
                for (int i = 0; i < this.VLSclb2Gr1.Items.Count; i++)
                {
                    this.VLSclb2Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS3)
            {
                for (int i = 0; i < this.VLSclb3Gr1.Items.Count; i++)
                {
                    this.VLSclb3Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS4)
            {
                for (int i = 0; i < this.VLSclb4Gr1.Items.Count; i++)
                {
                    this.VLSclb4Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS5)
            {
                for (int i = 0; i < this.VLSclb5Gr1.Items.Count; i++)
                {
                    this.VLSclb5Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS6)
            {
                for (int i = 0; i < this.VLSclb6Gr1.Items.Count; i++)
                {
                    this.VLSclb6Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS7)
            {
                for (int i = 0; i < this.VLSclb7Gr1.Items.Count; i++)
                {
                    this.VLSclb7Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS8)
            {
                for (int i = 0; i < this.VLSclb8Gr1.Items.Count; i++)
                {
                    this.VLSclb8Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS9)
            {
                for (int i = 0; i < this.VLSclb9Gr1.Items.Count; i++)
                {
                    this.VLSclb9Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS10)
            {
                for (int i = 0; i < this.VLSclb10Gr1.Items.Count; i++)
                {
                    this.VLSclb10Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS11)
            {
                for (int i = 0; i < this.VLSclb11Gr1.Items.Count; i++)
                {
                    this.VLSclb11Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS12)
            {
                for (int i = 0; i < this.VLSclb12Gr1.Items.Count; i++)
                {
                    this.VLSclb12Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS13)
            {
                for (int i = 0; i < this.VLSclb13Gr1.Items.Count; i++)
                {
                    this.VLSclb13Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS14)
            {
                for (int i = 0; i < this.VLSclb14Gr1.Items.Count; i++)
                {
                    this.VLSclb14Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS15)
            {
                for (int i = 0; i < this.VLSclb15Gr1.Items.Count; i++)
                {
                    this.VLSclb1Gr1.SetItemChecked(i, false);
                }
            }
            if (this._tBVLS16)
            {
                for (int i = 0; i < this.VLSclb16Gr1.Items.Count; i++)
                {
                    this.VLSclb16Gr1.SetItemChecked(i, false);
                }
            }
        }

        private void _resetCurrentVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;

            if (this.tabControl2.SelectedTab == this.VLS1)
            {
                this._tBVLS1 = true;
            }
            this.ResetCurrentVlsSignal();
        }

        private void _resetAllVlsButton_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Сбросить все сигнал ВЛС?", "Внимание", MessageBoxButtons.YesNo);
            if (res != DialogResult.Yes) return;
            this._vlsUnion.Reset();
        }

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.ChangeTextVls();

            if (this.tabControl2.SelectedTab == this.VLS1)
            {
                this._tBVLS1 = true;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS2)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = true;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS3)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = true;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS4)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = true;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS5)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = true;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS6)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = true;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS7)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = true;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS8)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = true;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS9)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = true;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS10)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = true;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS11)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = true;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS12)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = true;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS13)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = true;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS14)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = true;
                this._tBVLS15 = false;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS15)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = true;
                this._tBVLS16 = false;
                return;
            }
            if (this.tabControl2.SelectedTab == this.VLS16)
            {
                this._tBVLS1 = false;
                this._tBVLS2 = false;
                this._tBVLS3 = false;
                this._tBVLS4 = false;
                this._tBVLS5 = false;
                this._tBVLS6 = false;
                this._tBVLS7 = false;
                this._tBVLS8 = false;
                this._tBVLS9 = false;
                this._tBVLS10 = false;
                this._tBVLS11 = false;
                this._tBVLS12 = false;
                this._tBVLS13 = false;
                this._tBVLS14 = false;
                this._tBVLS15 = false;
                this._tBVLS16 = true;
                return;
            }

        }

        #endregion


        #endregion [Event handlers]

        #region [Save/Load File Members]

        private void ResetSetpoints(bool isDialog)   //загрузка базовых уставок из файла, isDialog - показывает будет ли диалог
        {
            if (isDialog)
            {
                DialogResult res = MessageBox.Show("Загрузить базовые уставки", "Внимание", MessageBoxButtons.YesNo);
                if (res != DialogResult.Yes) return;
            }
            try
            {
                XmlDocument doc = new XmlDocument();

                //doc.Load(Path.GetDirectoryName(Application.ExecutablePath) + string.Format(MR7_BASE_CONFIG_PATH, this._device.DeviceVersion));
                if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR7_BASE_CONFIG_PATH, this._device.DeviceVersion, this._device.DevicePlant));

                }
                else
                {
                    doc.Load(Path.GetDirectoryName(Application.ExecutablePath) +
                             string.Format(MR7_BASE_CONFIG_PATH, this._device.DeviceVersion, this._device.Info.DeviceConfiguration));
                   
                }

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = BASE_CONFIG_LOAD_SUCCESSFULLY;
            }
            catch
            {
                if (isDialog)
                {
                    if (MessageBox.Show("Невозможно загрузить файл стандартной конфигурации. Обнулить уставки?",
                            "Внимание", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        return;
                    }
                }

                this._configurationValidator.Reset();
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            this.SaveInFile();
        }

        private void SaveInFile()
        {
            if (this.WriteConfiguration())
            {
                this._statusLabel.Text = "Идет запись конфигурации в файл";
                this._saveConfigurationDlg.FileName = this._device.DevicePlant != null ? $"МР801_Уставки_версия_{this._device.DeviceVersion}_{this._device.DevicePlant}.xml" : $"МР801_Уставки_версия_{this._device.DeviceVersion}.xml";
                if (DialogResult.OK != this._saveConfigurationDlg.ShowDialog())
                {
                    this.IsProcess = false;
                    this._statusLabel.Text = string.Empty;
                    return;
                }
                this.Serialize(this._saveConfigurationDlg.FileName);
            }
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }

        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                this.Deserialize(this._openConfigurationDlg.FileName);
                TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
                TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
                TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            }
        }
        /// <summary>
        /// Сохранение конфигурации в файл
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Serialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("Mr7"));
                ushort[] values;

                values = this._currentSetpointsStruct.GetValues();

                XmlElement element = doc.CreateElement(XML_HEAD);
                element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
                if (doc.DocumentElement == null)
                {
                    throw new NullReferenceException();
                }
                doc.DocumentElement.AppendChild(element);
                doc.Save(binFileName);
                this._statusLabel.Text = string.Format("Файл {0} успешно сохранён", binFileName);
                this.IsProcess = false;
            }
            catch
            {
                MessageBox.Show(FILE_SAVE_FAIL);
                this.IsProcess = false;
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(binFileName);

                XmlNode a = doc.FirstChild.SelectSingleNode(XML_HEAD);
                if (a == null)
                    throw new NullReferenceException();

                byte[] values = Convert.FromBase64String(a.InnerText);
                this._currentSetpointsStruct.InitStruct(values);
                this.ShowConfiguration();
                this._statusLabel.Text = string.Format("Файл {0} успешно загружен", binFileName);
            }
            catch (Exception e)
            {
                MessageBox.Show(FILE_LOAD_FAIL);
            }
        }

        private void _saveToXmlButton_Click(object sender, EventArgs e)
        {
            this.SaveToHtml();
        }

        private void SaveToHtml()
        {
            try
            {
                if (this.WriteConfiguration())
                {
                    this._statusLabel.Text = "Идет запись конфигурации в HTML";
                    ExportGroupForm exportGroup = new ExportGroupForm();
                    exportGroup.IsExport = true;
                    if (exportGroup.ShowDialog() != DialogResult.OK)
                    {
                        exportGroup.IsExport = false;
                        this.IsProcess = false;
                        this._statusLabel.Text = string.Empty;
                        return;
                    }

                    this._currentSetpointsStruct.DeviceVersion = this._version;
                    this._currentSetpointsStruct.DeviceNumber = this._device.DeviceNumber.ToString();
                    this._currentSetpointsStruct.Primary = this._resistCheck.Checked;
                    this._currentSetpointsStruct.SizeOsc = this._oscSizeTextBox.Text;
                    this._currentSetpointsStruct.Group = (int)exportGroup.SelectedGroup;
                    this._currentSetpointsStruct.Config = this._config;
                    this.SaveToHtml(this._currentSetpointsStruct, exportGroup.SelectedGroup);
                    exportGroup.IsExport = false;
                }
                this.IsProcess = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения");
                this.IsProcess = false;
            }
        }

        private void SaveToHtml(StructBase str, ExportStruct group)
        {
            string fileName;
            if (group == ExportStruct.ALL)
            {
                fileName = string.Format("{0} Уставки версия {1} все группы", "МР801", this._device.DeviceVersion);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR7, fileName, str);
            }
            else
            {
                fileName = string.Format("{0} Уставки версия {1} группа{2}", "МР801", this._device.DeviceVersion, (int)group);
                this._statusLabel.Text = HtmlExport.ExportGroupMain(Resources.MR7Group, fileName, str);
            }
        }

        #endregion [Save/Load File Members]

        #region [Switch setpoints]

        private void _applyCopySetpoinsButton_Click(object sender, EventArgs e)
        {
            string message;

            if (this._setpointUnion.Check(out message, true))
            {
                GroupSetpoint[] allSetpoints =
                    this.CopySetpoints<AllGroupSetpointStruct, GroupSetpoint>(this._setpointUnion, this._setpointsValidator);
                this._currentSetpointsStruct.AllGroupSetpoints.Setpoints = allSetpoints;
                this._setpointsValidator.Set(this._currentSetpointsStruct.AllGroupSetpoints);
            }
            else
            {
                MessageBox.Show("Обнаружены некорректные уставки");
                return;
            }

            MessageBox.Show("Копирование завершено");
        }

        private T2[] CopySetpoints<T1, T2>(IValidator union, IValidator setpointValidator)
            where T1 : StructBase, ISetpointContainer<T2> where T2 : StructBase
        {
            T2 temp = (T2)union.Get();
            T2[] allSetpoints = ((T1)setpointValidator.Get()).Setpoints;
            if ((string)this._copySetpoinsGroupComboBox.SelectedItem == StringsConfig.CopyGroupsNames.Last())
            {
                for (int i = 0; i < allSetpoints.Length; i++)
                {
                    allSetpoints[i] = temp.Clone<T2>();
                }
            }
            else
            {
                allSetpoints[this._copySetpoinsGroupComboBox.SelectedIndex] = temp.Clone<T2>();
                allSetpoints[this._setpointsComboBox.SelectedIndex] = temp.Clone<T2>();
            }
            return allSetpoints;
        }

        #endregion

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr7); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr7ConfigurationForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }





        #endregion [IFormView Members]

        #region [Events CreateTree]

        private void VLSclbGr1_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.tabControl2, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeViewForVLS_NodeMouseClick_1(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.tabControl2, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _lsAndDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void _lsOrDgvGr1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl1, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl1, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        #endregion [Events CreateTree]

        private void _wrapBtn_Click(object sender, EventArgs e)
        {
            if (this._wrapBtn.Text == "Свернуть")
            {
                this.treeViewForVLS.CollapseAll();
                this._wrapBtn.Text = "Развернуть";
            }
            else
            {
                this.treeViewForVLS.ExpandAll();
                this._wrapBtn.Text = "Свернуть";
            }
        }
    }
}