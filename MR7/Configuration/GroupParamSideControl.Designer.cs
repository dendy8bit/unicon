﻿namespace BEMN.MR7.Configuration
{
    partial class GroupParamSideControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._measurGroupSideComboBox = new System.Windows.Forms.ComboBox();
            this._displayGroupSideComboBox = new System.Windows.Forms.ComboBox();
            this._inputUaGroupSideComboBox = new System.Windows.Forms.ComboBox();
            this._inputUbGroupSideComboBox = new System.Windows.Forms.ComboBox();
            this._inputUcGroupSideComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._kthGroupSideTextBox = new System.Windows.Forms.MaskedTextBox();
            this._KTHLkoef_comboGr1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.almInpTn = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Измерение ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Отображение";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Вх. Ua";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Вх. Ub";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Вх. Uc";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Ктн";
            // 
            // _measurGroupSideComboBox
            // 
            this._measurGroupSideComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._measurGroupSideComboBox.FormattingEnabled = true;
            this._measurGroupSideComboBox.Location = new System.Drawing.Point(113, 8);
            this._measurGroupSideComboBox.Name = "_measurGroupSideComboBox";
            this._measurGroupSideComboBox.Size = new System.Drawing.Size(90, 21);
            this._measurGroupSideComboBox.TabIndex = 13;
            // 
            // _displayGroupSideComboBox
            // 
            this._displayGroupSideComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._displayGroupSideComboBox.FormattingEnabled = true;
            this._displayGroupSideComboBox.Location = new System.Drawing.Point(113, 35);
            this._displayGroupSideComboBox.Name = "_displayGroupSideComboBox";
            this._displayGroupSideComboBox.Size = new System.Drawing.Size(90, 21);
            this._displayGroupSideComboBox.TabIndex = 14;
            // 
            // _inputUaGroupSideComboBox
            // 
            this._inputUaGroupSideComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputUaGroupSideComboBox.FormattingEnabled = true;
            this._inputUaGroupSideComboBox.Location = new System.Drawing.Point(113, 62);
            this._inputUaGroupSideComboBox.Name = "_inputUaGroupSideComboBox";
            this._inputUaGroupSideComboBox.Size = new System.Drawing.Size(90, 21);
            this._inputUaGroupSideComboBox.TabIndex = 13;
            // 
            // _inputUbGroupSideComboBox
            // 
            this._inputUbGroupSideComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputUbGroupSideComboBox.FormattingEnabled = true;
            this._inputUbGroupSideComboBox.Location = new System.Drawing.Point(113, 89);
            this._inputUbGroupSideComboBox.Name = "_inputUbGroupSideComboBox";
            this._inputUbGroupSideComboBox.Size = new System.Drawing.Size(90, 21);
            this._inputUbGroupSideComboBox.TabIndex = 14;
            // 
            // _inputUcGroupSideComboBox
            // 
            this._inputUcGroupSideComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._inputUcGroupSideComboBox.FormattingEnabled = true;
            this._inputUcGroupSideComboBox.Location = new System.Drawing.Point(113, 116);
            this._inputUcGroupSideComboBox.Name = "_inputUcGroupSideComboBox";
            this._inputUcGroupSideComboBox.Size = new System.Drawing.Size(90, 21);
            this._inputUcGroupSideComboBox.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(208, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "x";
            // 
            // _kthGroupSideTextBox
            // 
            this._kthGroupSideTextBox.Location = new System.Drawing.Point(113, 143);
            this._kthGroupSideTextBox.Name = "_kthGroupSideTextBox";
            this._kthGroupSideTextBox.Size = new System.Drawing.Size(90, 20);
            this._kthGroupSideTextBox.TabIndex = 34;
            // 
            // _KTHLkoef_comboGr1
            // 
            this._KTHLkoef_comboGr1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHLkoef_comboGr1.FormattingEnabled = true;
            this._KTHLkoef_comboGr1.Location = new System.Drawing.Point(224, 142);
            this._KTHLkoef_comboGr1.Name = "_KTHLkoef_comboGr1";
            this._KTHLkoef_comboGr1.Size = new System.Drawing.Size(51, 21);
            this._KTHLkoef_comboGr1.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Вход неиспр. ТН";
            // 
            // almInpTn
            // 
            this.almInpTn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.almInpTn.FormattingEnabled = true;
            this.almInpTn.Location = new System.Drawing.Point(113, 169);
            this.almInpTn.Name = "almInpTn";
            this.almInpTn.Size = new System.Drawing.Size(107, 21);
            this.almInpTn.TabIndex = 14;
            // 
            // GroupParamSideControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label7);
            this.Controls.Add(this._KTHLkoef_comboGr1);
            this.Controls.Add(this._kthGroupSideTextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.almInpTn);
            this.Controls.Add(this._inputUcGroupSideComboBox);
            this.Controls.Add(this._inputUbGroupSideComboBox);
            this.Controls.Add(this._inputUaGroupSideComboBox);
            this.Controls.Add(this._displayGroupSideComboBox);
            this.Controls.Add(this._measurGroupSideComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GroupParamSideControl";
            this.Size = new System.Drawing.Size(285, 201);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox _measurGroupSideComboBox;
        private System.Windows.Forms.ComboBox _displayGroupSideComboBox;
        private System.Windows.Forms.ComboBox _inputUaGroupSideComboBox;
        private System.Windows.Forms.ComboBox _inputUbGroupSideComboBox;
        private System.Windows.Forms.ComboBox _inputUcGroupSideComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox _kthGroupSideTextBox;
        private System.Windows.Forms.ComboBox _KTHLkoef_comboGr1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox almInpTn;
    }
}
