﻿using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.MR7.Configuration.Structures.Defenses.Z;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace BEMN.MR7.Configuration
{
    public partial class ResistanceDefTabPage : UserControl
    {
        private const string PRIMARY_STR = "r,Ом перв.";
        private const string SECOND_STR = "r,Ом втор.";
        private NewStructValidator<ResistanceDefensesStruct> _resistDefValidator;
        private bool _isPrimary;
        private double _koef;

        public ResistanceDefTabPage()
        {
            this.InitializeComponent();
        }

        public void Initialization()
        {
            Func<IValidatingRule> currentDefenseFunc1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new SignValidatingRule(-256.00, 256.00);
                    }
                    return RulesContainer.Ustavka256;
                }
                catch (Exception)
                {
                    return RulesContainer.Ustavka256;
                }
            });

            Func<IValidatingRule> currentDefenseFunc2 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return RulesContainer.Ustavka256;
                    }
                    return new CustomUshortRule(0, 89);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(0, 89);
                }
            });

            Func<IValidatingRule> currentFuncResistPrimary = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new SignValidatingRule(-256 * this._koef, 256 * this._koef);
                    }
                    else
                    {
                        return new CustomDoubleRule(0, 256 * this._koef);
                    }
                }
                catch (Exception)
                {
                    return new CustomDoubleRule(0, 256 * this._koef);
                }
            });

            Func<IValidatingRule> currentFuncResistPrimary1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new CustomDoubleRule(0, 256 * this._koef);
                    }
                    return new CustomUshortRule(0, 89);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(0, 89);
                }
            });

            this._resistDefValidator = new NewStructValidator<ResistanceDefensesStruct>
                (
                this.toolTip,
                new ControlInfoCombo(this.modeCombo, StringsConfig.DefenseModes),
                new ControlInfoCombo(this.typeCombo, StringsConfig.ResistDefType),
                new ControlInfoCombo(this.blockCombo, StringsConfig.SwitchSignals),
                new ControlInfoTextDependent(this.rTextBox1, currentDefenseFunc1),
                new ControlInfoTextDependent(this.xTextBox1, currentDefenseFunc1),
                new ControlInfoTextDependent(this.cornerRadTextBox1, currentDefenseFunc2),
                new ControlInfoCombo(this.directionCombo, StringsConfig.ResistDefDirection),
                new ControlInfoCheck(this.uStartCheck),
                new ControlInfoText(this.uStartMaskedText, RulesContainer.Ustavka256),
                new ControlInfoText(this.icpTextBox, RulesContainer.Ustavka40),
                new ControlInfoText(this.tcpTextBox, RulesContainer.TimeRule),
                new ControlInfoCombo(this.inpAccelerationCombo, StringsConfig.RelaySignals),
                new ControlInfoText(this.tyTextBox, RulesContainer.TimeRule),
                new ControlInfoCombo(this.logicCombo, StringsConfig.Conture),
                new ControlInfoCombo(this.blockFromTnCmb, StringsConfig.BlockTn),
                new ControlInfoCheck(this.blockFromLoadCheck),
                new ControlInfoCheck(this.blockFromSwingCheck),
                new ControlInfoCheck(this._steeredModeAccelerChBox),
                new ControlInfoCheck(this._damageFaza),
                new ControlInfoCheck(this._resetStep),
                new ControlInfoCombo(this.oscilloscopeCombo, StringsConfig.OscModes),
                new ControlInfoCheck(this.urovCheck),
                new ControlInfoCombo(this.ApvCombo, StringsConfig.Apv),
                new ControlInfoCombo(this.AvrCombo, StringsConfig.Apv)
                );
            NewStructValidator<OneWordStruct> _validator = new NewStructValidator<OneWordStruct>(
                this.toolTip,
                new ControlInfoTextDependent(this.rTextBox2, currentFuncResistPrimary),
                new ControlInfoTextDependent(this.xTextBox2, currentFuncResistPrimary),
                new ControlInfoTextDependent(this.cornerRadTextBox2, currentFuncResistPrimary1)
                );
        }

        public NewStructValidator<ResistanceDefensesStruct> ResistDefValidator
        {
            get { return this._resistDefValidator; }
        }

        [Browsable(false)]
        public bool IsPrimary
        {
            get { return this._isPrimary; }
            set
            {
                if(this.typeCombo.SelectedItem == null) return;
                this._isPrimary = value;
                this.panel1.Visible = this.cornerRadTextBox1.Visible = !value;
                this.panel2.Visible = this.cornerRadTextBox2.Visible = value;
                if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
                {
                    this._frLabel.Text = "f, град.";
                }
                else
                {
                    this._frLabel.Text = value ? PRIMARY_STR : SECOND_STR;
                }
            }
        }

        public string Mode
        {
            get { return this.modeCombo.SelectedItem.ToString(); }
        }

        public string Type
        {
            get { return this.typeCombo.SelectedItem.ToString(); }
        }

        public string Block
        {
            get { return this.blockCombo.SelectedItem.ToString(); }
        }

        public string R
        {
            get { return this.IsPrimary ? this.rTextBox2.Text : this.rTextBox1.Text; }
        }

        public string X
        {
            get { return this.IsPrimary ? this.xTextBox2.Text : this.xTextBox1.Text; }
        }

        public string CornerRadius
        {
            get { return this.IsPrimary ? this.cornerRadTextBox2.Text : this.cornerRadTextBox1.Text; }
        }

        public string Tcp
        {
            get { return this.tcpTextBox.Text; }
        }

        public string Icp
        {
            get { return this.icpTextBox.Text; }
        }

        public string Accleration
        {
            get { return this.inpAccelerationCombo.SelectedItem.ToString(); }
        }

        public string Tu
        {
            get { return this.tyTextBox.Text; }
        }

        public string Direction
        {
            get { return this.directionCombo.SelectedItem.ToString(); }
        }

        public bool StartOnU
        {
            get { return this.uStartCheck.Checked; }
        }

        public string Ustart
        {
            get { return this.uStartMaskedText.Text; }
        }

        public string Logic
        {
            get { return this.logicCombo.SelectedItem.ToString(); }
        }

        public string BlockFromTn
        {
            get { return this.blockFromTnCmb.SelectedItem.ToString(); }
        }

        public bool BlockFromLoad
        {
            get { return this.blockFromLoadCheck.Checked; }
        }

        public bool BlockFromSwing
        {
            get { return this.blockFromSwingCheck.Checked; }
        }

        public bool DamageFaza
        {
            get { return this._damageFaza.Checked; }
        }

        public bool ResetStep
        {
            get { return this._resetStep.Checked; }
        }

        public string Oscilloscope
        {
            get { return this.oscilloscopeCombo.SelectedItem.ToString(); }
        }

        public bool Urov
        {
            get { return this.urovCheck.Checked; }
        }

        public string Apv
        {
            get { return this.ApvCombo.SelectedItem.ToString(); }
        }

        public bool SteeredModeAcceler
        {
            get { return this._steeredModeAccelerChBox.Checked; }
        }

        public void GetPrimaryValue(double koef)
        {
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this._frLabel.Text = "f, град.";
            }
            else
            {
                this._frLabel.Text = "r, Ом перв.";
            }
            this._koef = koef;
            double val = Math.Round(Convert.ToDouble(this.rTextBox1.Text) * koef, 2);
            this.rTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            val = Math.Round(Convert.ToDouble(this.xTextBox1.Text) * koef, 2);
            this.xTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this.cornerRadTextBox1.Text) * koef, 2);
                this.cornerRadTextBox2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this.cornerRadTextBox2.Text = this.cornerRadTextBox1.Text;
            }
        }

        public void GetSecondValue(double koef)
        {
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this._frLabel.Text = "f, град.";
            }
            else
            {
                this._frLabel.Text = "r, Ом втор.";
            }
            double val = Math.Round(Convert.ToDouble(this.rTextBox2.Text) * koef, 2);
            this.rTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            val = Math.Round(Convert.ToDouble(this.xTextBox2.Text) * koef, 2);
            this.xTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this.cornerRadTextBox2.Text) * koef, 2);
                this.cornerRadTextBox1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this.cornerRadTextBox1.Text = this.cornerRadTextBox2.Text;
            }
        }

        private void typeCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.typeCombo.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this._frLabel.Text = "f, град.";
            }
            else
            {
                if (!this.IsPrimary)
                {
                    this._frLabel.Text = "r, Ом втор.";
                }
                else
                {
                    this._frLabel.Text = "r, Ом перв.";
                }
            }
        }
    }
}
