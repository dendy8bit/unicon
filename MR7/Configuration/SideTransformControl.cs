﻿using System.Drawing;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using System.Windows.Forms;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;

namespace BEMN.MR7.Configuration
{
    public partial class SideTransformControl : UserControl
    {

        private bool _isSideN;

        private Point _label239Point;
        private Point _label240Point;
        private Point _label1Point;
        private Point _label245Point;
        private Point _label246Point;
        private Point _u1TextBoxPoint;
        private Point _unPolaryComboBoxPoint;
        private Point _uabcPolaryComboBoxPont;
        private Point _groupComboPoint;
        private Point _typeYnComboBoxPoint;


        public SideTransformControl()
        {
            this.InitializeComponent();
            this.Init();
        }

        public void Init()
        {
            this.SideStructValidator = new NewStructValidator<SideStruct>(
                new ToolTip(),
                new ControlInfoCombo(this._inlaComboBox, StringsConfig.NumberInputI),
                new ControlInfoCombo(this._inlbComboBox, StringsConfig.NumberInputI),
                new ControlInfoCombo(this._inlcComboBox, StringsConfig.NumberInputI),
                new ControlInfoCombo(this._inlnComboBox, StringsConfig.NumberInputI),
                new ControlInfoText(this._s1TextBox, new CustomDoubleRule(0, 1024)),
                new ControlInfoText(this._u1TextBox, new CustomDoubleRule(0, 1024)),
                new ControlInfoCombo(this._typeYnComboBox, StringsConfig.SideType),
                new ControlInfoCombo(this.groupCombo, StringsConfig.SideGroup),
                new ControlInfoCombo(this._ittComboBox, StringsConfig.SignTT),
                new ControlInfoText(this._ittTextBox, RulesContainer.UshortRule),
                new ControlInfoCombo(this._ittnpComboBox, StringsConfig.SignTT),
                new ControlInfoText(this._ittnpTextBox, RulesContainer.UshortRule),
                new ControlInfoCombo(this._koefttComboBox, StringsConfig.CoefSchemaTT),
                new ControlInfoCombo(this._tokinComboBox, StringsConfig.InpIn),
                new ControlInfoCombo(this._typettComboBox, StringsConfig.TtType),
                new ControlInfoCombo(this._uabcPolaryComboBox, StringsConfig.PolarisUabc),
                new ControlInfoCombo(this._unPolaryComboBox, StringsConfig.PolarisUn)
            );
        }

        public NewStructValidator<SideStruct> SideStructValidator { get; private set; }
        
        public bool IsSide1
        {
            set
            {
                this.label1.Visible = !value;
                this.groupCombo.Visible = !value;
            }
        }

        public bool IsSideN
        {
            get => _isSideN;
            set
            {
                if (this._isSideN == value) return;

                this._isSideN = value;

                this.label247.Visible = !this._isSideN;
                this._s1TextBox.Visible = !this._isSideN;

                this.label241.Visible = !this._isSideN;
                this._tokinComboBox.Visible = !this._isSideN;

                this.label238.Visible = !this._isSideN;
                this._typettComboBox.Visible = !this._isSideN;

                if (this._isSideN)
                {
                    this.label246.Location = new Point(21, 119);
                    this._u1TextBox.Location = new Point(96, 117);

                    this.label240.Location = new Point(21, 140);
                    this._uabcPolaryComboBox.Location = new Point(96, 143);

                    this.label239.Location = new Point(21, 174);
                    this._unPolaryComboBox.Location = new Point(96, 170);

                    this.label245.Location = new Point(191, 98);
                    this._typeYnComboBox.Location = new Point(223, 95);

                    this.label1.Location = new Point(190, 125);
                    this.groupCombo.Location = new Point(277, 122); 

                }

                else
                {
                    this.label246.Location = this._label246Point;
                    this._u1TextBox.Location = this._u1TextBoxPoint;

                    this.label240.Location = this._label240Point;
                    this._uabcPolaryComboBox.Location = this._uabcPolaryComboBoxPont;

                    this.label239.Location = this._label239Point;
                    this._unPolaryComboBox.Location = this._unPolaryComboBoxPoint;

                    this.label245.Location = this._label245Point;
                    this._typeYnComboBox.Location = this._typeYnComboBoxPoint;

                    this.label1.Location = this._label1Point;
                    this.groupCombo.Location = this._groupComboPoint;
                }

                Refresh();
            }
            
        }
    }
}
