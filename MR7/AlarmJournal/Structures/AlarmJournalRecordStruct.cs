﻿using System;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.MR7.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d3}";
        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _numOfDefAndNumOfTrigParam;
        [Layout(9)] private ushort _groupOfSetpointsAndTypeDmg;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _valueOfTriggeredParametr1;

        [Layout(12)] private ushort _ida;
        [Layout(13)] private ushort _idb;
        [Layout(14)] private ushort _idc;

        [Layout(15)] private ushort _ita;
        [Layout(16)] private ushort _itb;
        [Layout(17)] private ushort _itc;

        [Layout(18)] private ushort _is1a;
        [Layout(19)] private ushort _is1b;
        [Layout(20)] private ushort _is1c;
        [Layout(21)] private ushort _is10;
        [Layout(22)] private ushort _is12;
        [Layout(23)] private ushort _is1n;
        [Layout(24)] private ushort _is1D0;
        [Layout(25)] private ushort _is1T0;

        [Layout(26)] private ushort _is2a;
        [Layout(27)] private ushort _is2b;
        [Layout(28)] private ushort _is2c;
        [Layout(29)] private ushort _is20;
        [Layout(30)] private ushort _is22;
        [Layout(31)] private ushort _is2n;
        [Layout(32)] private ushort _is2D0;
        [Layout(33)] private ushort _is2T0;

        [Layout(34)] private ushort _is3a;
        [Layout(35)] private ushort _is3b;
        [Layout(36)] private ushort _is3c;
        [Layout(37)] private ushort _is30;
        [Layout(38)] private ushort _is32;
        [Layout(39)] private ushort _is3n;
        [Layout(40)] private ushort _is3D0;
        [Layout(41)] private ushort _is3T0;

        [Layout(42)] private ushort _is4a;
        [Layout(43)] private ushort _is4b;
        [Layout(44)] private ushort _is4c;
        [Layout(45)] private ushort _is40;
        [Layout(46)] private ushort _is42;
        [Layout(47)] private ushort _is4n;
        [Layout(48)] private ushort _is4D0;
        [Layout(49)] private ushort _is4T0;

        [Layout(50)] private ushort _u1ab;
        [Layout(51)] private ushort _u1bc;
        [Layout(52)] private ushort _u1ca;
        [Layout(53)] private ushort _u12;
        [Layout(54)] private ushort _u1a;
        [Layout(55)] private ushort _u1b;
        [Layout(56)] private ushort _u1c;
        [Layout(57)] private ushort _u10;

        [Layout(58)] private ushort _u2ab;
        [Layout(59)] private ushort _u2bc;
        [Layout(60)] private ushort _u2ca;
        [Layout(61)] private ushort _u22;
        [Layout(62)] private ushort _u2a;
        [Layout(63)] private ushort _u2b;
        [Layout(64)] private ushort _u2c;
        [Layout(65)] private ushort _u20;

        [Layout(66)] private ushort _un;
        [Layout(67)] private ushort _f;
        [Layout(68)] private ushort _d1;
        [Layout(69)] private ushort _d2;
        [Layout(70)] private ushort _d3;
        [Layout(71)] private ushort _d4;

        //[Layout(72)] private ushort _rab;
        //[Layout(73)] private ushort _xab;
        //[Layout(74)] private ushort _rbc;
        //[Layout(75)] private ushort _xbc;
        //[Layout(76)] private ushort _rca;
        //[Layout(77)] private ushort _xca;
        //[Layout(78)] private ushort _ra1;
        //[Layout(79)] private ushort _xa1;
        //[Layout(80)] private ushort _rb1;
        //[Layout(81)] private ushort _xb1;
        //[Layout(82)] private ushort _rc1;
        //[Layout(83)] private ushort _xc1;

        //[Layout(84)] private ushort _q;
        //[Layout(85)] private ushort _spl;
        //[Layout(86)] private ushort _dfdt;
        //[Layout(87)] private ushort _arc;
        //[Layout(88)] private ushort _pow;

        public List<string> MessagesList { get; set; }

        #endregion [Private fields]


        #region [Properties]
        public ushort Year
        {
            get { return this._year; }
        }

        public ushort Month
        {
            get { return this._month; }
        }

        public ushort Date
        {
            get { return this._date; }
        }

        public ushort Hour
        {
            get { return this._hour; }
        }

        public ushort Minute
        {
            get { return this._minute; }
        }

        public ushort Second
        {
            get { return this._second; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
        }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year + this.Month + this.Date + this.Hour + this.Minute + this.Second + this.Millisecond
                    + this._message + this._numOfDefAndNumOfTrigParam + this._groupOfSetpointsAndTypeDmg + this._valueOfTriggeredParametr;
                return sum == 0;
            }
        }
        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );
            }
        }

        public ushort Message
        {
            get { return this._message; }
        }

        public int TriggeredDefense
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public int NumberOfTriggeredParametr
        {
            get { return Common.GetBits(this._numOfDefAndNumOfTrigParam, 8, 9, 10, 11, 12, 13, 14, 15)>>8; }
        }

        public ushort ValueOfTriggeredParametr
        {
            get { return this._valueOfTriggeredParametr; }
        }

        public ushort ValueOfTriggeredParametr1
        {
            get { return this._valueOfTriggeredParametr1; }
        }

        public int GroupOfSetpoints
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 0, 1, 2); }
        }
        
        public int TypeOfDmg
        {
            get { return Common.GetBits(this._groupOfSetpointsAndTypeDmg, 8,9,10,11)>>8; }
        }

        public ushort Ida
        {
            get { return this._ida; }
        }

        public ushort Idb
        {
            get { return this._idb; }
        }

        public ushort Idc
        {
            get { return this._idc; }
        }

        public ushort Ita
        {
            get { return this._ita; }
        }

        public ushort Itb
        {
            get { return this._itb; }
        }

        public ushort Itc
        {
            get { return this._itc; }
        }

        public ushort Is1a
        {
            get { return this._is1a; }
        }

        public ushort Is1b
        {
            get { return this._is1b; }
        }

        public ushort Is1c
        {
            get { return this._is1c; }
        }

        public ushort Is10
        {
            get { return this._is10; }
        }

        public ushort Is12
        {
            get { return this._is12; }
        }

        public ushort Is1n
        {
            get { return this._is1n; }
        }

        public ushort Is1d0
        {
            get { return this._is1D0; }
        }

        public ushort Is1t0
        {
            get { return this._is1T0; }
        }

        public ushort Is2a
        {
            get { return this._is2a; }
        }

        public ushort Is2b
        {
            get { return this._is2b; }
        }

        public ushort Is2c
        {
            get { return this._is2c; }
        }

        public ushort Is20
        {
            get { return this._is20; }
        }

        public ushort Is22
        {
            get { return this._is22; }
        }

        public ushort Is2n
        {
            get { return this._is2n; }
        }

        public ushort Is2d0
        {
            get { return this._is2D0; }
        }

        public ushort Is2t0
        {
            get { return this._is2T0; }
        }

        public ushort Is3a
        {
            get { return this._is3a; }
        }

        public ushort Is3b
        {
            get { return this._is3b; }
        }

        public ushort Is3c
        {
            get { return this._is3c; }
        }

        public ushort Is30
        {
            get { return this._is30; }
        }

        public ushort Is32
        {
            get { return this._is32; }
        }

        public ushort Is3n
        {
            get { return this._is3n; }
        }

        public ushort Is3d0
        {
            get { return this._is3D0; }
        }

        public ushort Is3t0
        {
            get { return this._is3T0; }
        }

        public ushort Is4a
        {
            get { return this._is4a; }
        }

        public ushort Is4b
        {
            get { return this._is4b; }
        }

        public ushort Is4c
        {
            get { return this._is4c; }
        }

        public ushort Is40
        {
            get { return this._is40; }
        }

        public ushort Is42
        {
            get { return this._is42; }
        }

        public ushort Is4n
        {
            get { return this._is4n; }
        }

        public ushort Is4d0
        {
            get { return this._is4D0; }
        }

        public ushort Is4t0
        {
            get { return this._is4T0; }
        }

        public ushort U1ab
        {
            get { return this._u1ab; }
        }

        public ushort U1bc
        {
            get { return this._u1bc; }
        }

        public ushort U1ca
        {
            get { return this._u1ca; }
        }

        public ushort U12
        {
            get { return this._u12; }
        }

        public ushort U1a
        {
            get { return this._u1a; }
        }

        public ushort U1b
        {
            get { return this._u1b; }
        }

        public ushort U1c
        {
            get { return this._u1c; }
        }

        public ushort U10
        {
            get { return this._u10; }
        }

        public ushort U2ab
        {
            get { return this._u2ab; }
        }

        public ushort U2bc
        {
            get { return this._u2bc; }
        }

        public ushort U2ca
        {
            get { return this._u2ca; }
        }

        public ushort U22
        {
            get { return this._u22; }
        }

        public ushort U2a
        {
            get { return this._u2a; }
        }

        public ushort U2b
        {
            get { return this._u2b; }
        }

        public ushort U2c
        {
            get { return this._u2c; }
        }

        public ushort U20
        {
            get { return this._u20; }
        }

        public ushort Un
        {
            get { return this._un; }
        }

        public ushort F
        {
            get { return this._f; }
        }

        public ushort D1
        {
            get { return this._d1; }
        }

        public ushort D2
        {
            get { return this._d2; }
        }

        public ushort D3
        {
            get { return this._d3; }
        }

        public ushort D4
        {
            get { return this._d4; }
        }

        //public ushort Rab
        //{
        //    get { return this._rab; }
        //}

        //public ushort Xab
        //{
        //    get { return this._xab; }
        //}

        //public ushort Rbc
        //{
        //    get { return this._rbc; }
        //}

        //public ushort Xbc
        //{
        //    get { return this._xbc; }
        //}

        //public ushort Rca
        //{
        //    get { return this._rca; }
        //}

        //public ushort Xca
        //{
        //    get { return this._xca; }
        //}

        //public ushort Ra1
        //{
        //    get { return this._ra1; }
        //}

        //public ushort Xa1
        //{
        //    get { return this._xa1; }
        //}

        //public ushort Rb1
        //{
        //    get { return this._rb1; }
        //}

        //public ushort Xb1
        //{
        //    get { return this._xb1; }
        //}

        //public ushort Rc1
        //{
        //    get { return this._rc1; }
        //}

        //public ushort Xc1
        //{
        //    get { return this._xc1; }
        //}

        //public ushort Q
        //{
        //    get { return this._q; }
        //}

        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1), true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D2), true); }
        }

        public string D25To32
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D2), true); }
        }

        public string D33To40
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D3), true); }
        }

        public string D41To48
        {
            get { return GetMask(Common.HIBYTE(this.D3)); }
        }

        public string D49To56
        {
            get { return GetMask(Common.LOBYTE(this.D4)); }
        }

        private string GetMask(ushort value)
        {
            var chars = Convert.ToString(value, 2).PadLeft(8, '0').ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }
        
        #endregion [Properties]

    }
}
