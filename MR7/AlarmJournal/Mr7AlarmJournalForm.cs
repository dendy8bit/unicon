﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.MemoryEntityClasses.FileOperations;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR7.AlarmJournal.Structures;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using BEMN.MR7.Osc.Loaders;
using BEMN.MR7.Properties;
using SchemeEditorSystem.ResourceLibs;

namespace BEMN.MR7.AlarmJournal
{
    public partial class Mr7AlarmJournalForm : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР801_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        private const string READING_LIST_FILE = "Идет чтение файла списка подписей сигналов ЖА СПЛ";
        private const string DEVICE_NAME = "MR801";
        private const string FAIL_READ =
            "Невозможно прочитать список подписей сигналов ЖА СПЛ. Списки будут сформированы по умолчанию.";
        private const string LIST_FILE_NAME = "jlist.xml";
        #endregion [Constants]

        #region [Private fields]
        private readonly AlarmJournalLoader _journalLoader;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly MemoryEntity<OneWordStruct> _setPageAlarmJournal;
        private SideStruct _currentSideStruct;
        private SideStruct _currentSideStruct1;
        private SideStruct _currentSideStruct2;
        private SideStruct _currentSideStruct3;
        private KanalTransU _currentKanalTransU;
        private KanalTransU _currentKanalTransU2;
        private KanalTransU _currentKanalTransU3;
        private DataTable _table;
        private int _recordNumber;
        private int _failCounter;
        private Mr7 _device;
        private List<string> _messages;
        private FileDriver _fileDriver;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr7AlarmJournalForm()
        {
            this.InitializeComponent();
        }

        public Mr7AlarmJournalForm(Mr7 device)
        {
            this.InitializeComponent();
            this._device = device;

            this._currentOptionsLoader = device.CurrentOptionsLoaderAj;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this.CurrenOptionLoaded);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            //Новая МР801, кака и ИР771, может читать за раз пачку сообщений, а не по одной штуке. читаемый размер не должен превышать 1024 слова.
            //Поэтому в лоадере идет чтение стректуры с пачкой до 14 сообщений(стерктура All. потому что в одном сообщении 72 слова. 72*14 = 1008 слов - влазим в размер.

            if (_device.AllAlarmJournal != null)
            {
                this._journalLoader = new AlarmJournalLoader(device.AllAlarmJournal, device.SetPageJa);
                this._journalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.ReadAllRecords);
                this._journalLoader.ReadJournalFail += HandlerHelper.CreateActionHandler(this, this.FailReadJournal);
            }
            else
            {
                this._alarmJournal = device.AlarmJournal;
                this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.ReadRecords);
                this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    if (this._failCounter < 80)
                    {
                        this._failCounter++;
                    }
                    else
                    {
                        this._alarmJournal.RemoveStructQueries();
                        this._failCounter = 0;
                        this.ButtonsEnabled = true;
                    }
                });
                this._setPageAlarmJournal = device.SetPageJa;
                this._setPageAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this._alarmJournal.LoadStruct);
                this._setPageAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadJournal);
            }

            this._fileDriver = new FileDriver(device, this);
        }
        #endregion [Ctor's]


        #region [Help members]

        private bool ButtonsEnabled
        {
            set
            {
                this._readAlarmJournalButton.Enabled = this._saveAlarmJournalButton.Enabled =
                    this._loadAlarmJournalButton.Enabled = this._exportButton.Enabled = value;
            }
        }

        private void OnListRead(byte[] readBytes, string mes)
        {
            try
            {
                if (readBytes != null && readBytes.Length != 0 && mes == "Операция успешно выполнена")
                {
                    using (StreamReader streamReader = new StreamReader(new MemoryStream(readBytes),
                        Encoding.GetEncoding("windows-1251")))
                    {
                        using (XmlTextReader reader = new XmlTextReader(streamReader))
                        {
                            XmlRootAttribute root = new XmlRootAttribute(DEVICE_NAME);
                            XmlSerializer serializer = new XmlSerializer(typeof(ListsOfJournals), root);
                            ListsOfJournals lists = (ListsOfJournals)serializer.Deserialize(reader);
                            this._messages = lists.AlarmJournal.MessagesList;
                        }
                    }
                }
                else
                {
                    MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this._messages = PropFormsStrings.GetNewAlarmList();
                }
            }
            catch
            {
                MessageBox.Show(FAIL_READ, "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this._messages = PropFormsStrings.GetNewAlarmList();
            }
            this.StartReadOption();
        }

        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
            this.ButtonsEnabled = true;
        }

        private void CurrenOptionLoaded()
        {
            this._table.Clear();
            this._recordNumber = this._failCounter = 0;
            if (this._device.AllAlarmJournal != null)
            {
                this._journalLoader.Clear();
                this._journalLoader.StartRead();
            }
            else
            {
                this.SavePageNumber();
            }

        }

        private void ReadRecords()
        {
            try
            {
                if (!this._alarmJournal.Value.IsEmpty)
                {
                    this._recordNumber++;
                    this._failCounter = this._recordNumber;

                    AlarmJournalRecordStruct record = this._alarmJournal.Value;
                    MeasureTransStruct measure = this._currentOptionsLoader[record.GroupOfSetpoints];
                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);
                    if (_device.Info.DeviceConfiguration == "T6N3D42R35")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],

                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                    }
                    else if (_device.Info.DeviceConfiguration == "T12N4D26R19")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24
                        );
                    }
                    else
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56
                        );
                    }
                    this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                    this.statusStrip1.Update();
                    this.SavePageNumber();
                }
                else
                {
                    if (this._table.Rows.Count == 0)
                    {
                        this._statusLabel.Text = JOURNAL_IS_EMPTY;
                    }
                    this.ButtonsEnabled = true;
                }
            }
            catch (Exception e)
            {
                this._table.Rows.Add
                (
                    this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                );
            }
        }

        private void ReadAllRecords()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;

            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoader[record.GroupOfSetpoints];

                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);

                    // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                    if (_device.Info.DeviceConfiguration == "T6N3D42R35")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],

                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                            
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue)
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            
                        );
                    }
                    else if (_device.Info.DeviceConfiguration == "T12N4D26R19")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24

                            //ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue)
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),

                        );
                    }
                    else
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],

                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56

                            //ValuesConverterCommon.Analog.GetResist((short)record.Rab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue)
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xab, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xbc, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xca, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Ra1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xa1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xb1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Rc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),
                            //ValuesConverterCommon.Analog.GetResist((short)record.Xc1, measure.Sides[0].Ittl, measure.Uabc[0].KthValue),

                        );
                    }
                }
                catch (Exception e)
                {
                    this._table.Rows.Add
                    (
                        this._recordNumber - 1, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
                    );
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber - 1);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }
        private void SavePageNumber()
        {
            this._setPageAlarmJournal.Value.Word = (ushort)this._recordNumber;
            this._setPageAlarmJournal.SaveStruct();
        }

        private void ReadRecordFromBinFile()
        {
            if (this._journalLoader.JournalRecords.Count == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
                this.ButtonsEnabled = true;
                return;
            }

            this._recordNumber = 1;

            foreach (AlarmJournalRecordStruct record in this._journalLoader.JournalRecords)
            {
                try
                {
                    MeasureTransStruct measure = this._currentOptionsLoader[record.GroupOfSetpoints];

                    string triggeredDef = this.GetTriggeredDefence(record);
                    string parameter = this.GetParameter(record);
                    string parametrValue = this.GetParametrValue(record, measure);

                    // вывод в таблицу. скорее всего, размер таблицы, столбцы, которые надо будет выводить, нужно будет менять динамически

                    if (_device.Info.DeviceConfiguration == "T6N3D42R35")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40
                        );
                    } else if (_device.Info.DeviceConfiguration == "T12N4D26R19")
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, _currentKanalTransU3.KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24
                        );
                    }
                    else
                    {
                        this._table.Rows.Add
                        (
                            this._recordNumber,
                            record.GetTime,
                            AjStrings.Message[record.Message],
                            triggeredDef,
                            parameter,
                            parametrValue,
                            AjStrings.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ida, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Idc, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Ita, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itb, measure.Sides[0].S, measure.Sides[0].U),
                            ValuesConverterCommon.Analog.GetI801Diff(record.Itc, measure.Sides[0].S, measure.Sides[0].U),

                            ValuesConverterCommon.Analog.GetI801(record.Is1a, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1b, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1c, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is10, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is12, measure.Sides[0].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is1n, measure.Sides[0].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is2a, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2b, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2c, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is20, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is22, measure.Sides[1].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is2n, measure.Sides[1].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is3a, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3b, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3c, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is30, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is32, measure.Sides[2].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is3n, measure.Sides[2].Ittx * 40),

                            ValuesConverterCommon.Analog.GetI801(record.Is4a, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4b, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4c, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is40, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is42, measure.Sides[3].Ittl * 40),
                            ValuesConverterCommon.Analog.GetI801(record.Is4n, measure.Sides[3].Ittx * 40),

                            ValuesConverterCommon.Analog.GetU801(record.U1a, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1b, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1c, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ab, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1bc, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U1ca, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U12, measure.Uabc[0].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U10, measure.Uabc[0].KthValue),

                            ValuesConverterCommon.Analog.GetU801(record.U2a, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2b, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2c, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ab, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2bc, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U2ca, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U22, measure.Uabc[1].KthValue),
                            ValuesConverterCommon.Analog.GetU801(record.U20, measure.Uabc[1].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, measure.Uabc[2].KthValue),

                            ValuesConverterCommon.Analog.GetF801(record.F),
                            ValuesConverterCommon.Analog.GetU801(record.Un, _currentKanalTransU3.KthValue),

                            record.D1To8,
                            record.D9To16,
                            record.D17To24,
                            record.D25To32,
                            record.D33To40,
                            record.D41To48,
                            record.D49To56
                        );
                    }

                }
                catch (Exception e)
                {
                    this._table.Rows.Add
                    (
                        this._recordNumber, "", "Ошибка (" + e.Message + ")", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                        "", "", "", ""
                    );
                }
                this._recordNumber++;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
            this.statusStrip1.Update();
            this.ButtonsEnabled = true;
        }

        private string GetTriggeredDefence(AlarmJournalRecordStruct record)
        {
            return record.TriggeredDefense == 71 ? this._messages[record.ValueOfTriggeredParametr] : AjStrings.TriggeredDefense[record.TriggeredDefense];
        }

        private string GetParameter(AlarmJournalRecordStruct record)
        {
            //if (record.TriggeredDefense >= 38 && record.TriggeredDefense <= 65 || record.TriggeredDefense == 71)
            //{
            //    return string.Empty;
            //}
            if (record.TriggeredDefense >= 50 && record.TriggeredDefense <= 65 || record.TriggeredDefense == 71 || record.TriggeredDefense == 45 || record.TriggeredDefense == 46)
            {
                return string.Empty;
            }
            return AjStrings.Parametr[record.NumberOfTriggeredParametr];
        }

        private string GetParametrValue(AlarmJournalRecordStruct record, MeasureTransStruct measure)
        {
            // параметры выводятся согласно файлику jalm801 в моей папке/мр801 - коды сработавшего параметра

            int parameter = record.NumberOfTriggeredParametr;
            ushort value = record.ValueOfTriggeredParametr;

            if (parameter >= 0 && parameter <= 5)
            {
                return ValuesConverterCommon.Analog.GetI801Diff(value, measure.Sides[0].S, measure.Sides[0].U);
            }

            #region Side1
            if ((parameter >= 6 && parameter <= 10) || (parameter >= 12 && parameter <= 13))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[0].Ittl * 40);
            }

            if (parameter == 11)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[0].Ittx * 40);
            }
            #endregion

            #region Side2
            if ((parameter >= 14 && parameter <= 18) || (parameter >= 20 && parameter <= 21))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[1].Ittl * 40);
            }

            if (parameter == 19)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[1].Ittx * 40);
            }
            #endregion

            #region Side3

            if ((parameter >= 22 && parameter <= 26) || (parameter >= 28 && parameter <= 29))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[2].Ittl * 40);
            }

            if (parameter == 27)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[2].Ittx * 40);
            }

            #endregion

            #region Side4

            if ((parameter >= 30 && parameter <= 34) || (parameter >= 36 && parameter <= 37))
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[3].Ittl * 40);
            }

            if (parameter == 35)
            {
                return ValuesConverterCommon.Analog.GetI801(value, measure.Sides[3].Ittx * 40);
            }

            #endregion

            if (parameter == 75)
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetFullPower((short)value, (short)value1, measure.Uabc[0].KthValue, measure.Sides[0].Ittl);
            }

            if ((parameter >= 60) && (parameter <= 71))
            {
                ushort value1 = record.ValueOfTriggeredParametr1;
                return ValuesConverterCommon.Analog.GetZ((short)value, (short)value1,
                    measure.Uabc[0].KthValue, measure.Sides[0].Ittl);
            }

            if (parameter >= 38 && parameter <= 45)
            {
                return ValuesConverterCommon.Analog.GetU801(value, measure.Uabc[0].KthValue);
            }

            if (parameter >= 46 && parameter <= 53)
            {
                return ValuesConverterCommon.Analog.GetU801(value, measure.Uabc[1].KthValue);
            }

            if (parameter == 54)
            {
                return ValuesConverterCommon.Analog.GetU(value, _currentKanalTransU.KthValue);
            }

            if (parameter == 55)
            {
                return ValuesConverterCommon.Analog.GetF(value);
            }

            if (parameter == 72)
            {
                return ValuesConverterCommon.Analog.GetQ(value);
            }

            if (parameter >= 80 && parameter <= 84 || parameter == 73 || parameter >= 56 && parameter <= 59)
            {
                return string.Empty;
            }

            return value.ToString();
            return "0";
        }

        private DataTable GetJournalDataTable()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode)
            {
                _device.Info.DeviceConfiguration = _device.DevicePlant;
            }

            switch (_device.Info.DeviceConfiguration)
            {
                case "T6N3D42R35":
                    this._alarmJournalGrid.Columns.Remove(this._d5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    break;
                case "T12N4D26R19":
                    this._alarmJournalGrid.Columns.Remove(this._D3Col);
                    this._alarmJournalGrid.Columns.Remove(this._D4Col);
                    this._alarmJournalGrid.Columns.Remove(this._d5Col);
                    this._alarmJournalGrid.Columns.Remove(this._D6Col);
                    break;
                default:
                    break;
            }

            DataTable table = new DataTable(TABLE_NAME);
            for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
            }
            return table;
        }

        #endregion [Help members]

        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;

            if (Device.AutoloadConfig && this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void _readAlarmJournalButtonClick(object sender, EventArgs e)
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._fileDriver.ReadFile(this.OnListRead, LIST_FILE_NAME);
                this.ButtonsEnabled = false;
                this._statusLabel.Text = READING_LIST_FILE;
                this.statusStrip1.Update();
            }
        }

        private void StartReadOption()
        {
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }

        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            int j = 0;
            if (this._openAlarmJournalDialog.ShowDialog() != DialogResult.OK) return;
            if (Path.GetExtension(this._openAlarmJournalDialog.FileName).ToLower().Contains("bin"))
            {
                byte[] file = File.ReadAllBytes(this._openAlarmJournalDialog.FileName);
                AlarmJournalRecordStruct journal = new AlarmJournalRecordStruct();
                int size = journal.GetSize();
                List<byte> journalBytes = new List<byte>();
                journalBytes.AddRange(Common.SwapArrayItems(file));
                int countRecord = journalBytes.Count / size;
                for (int i = 0; j < countRecord - 2; i = i + size)
                {
                    journal.InitStruct(journalBytes.GetRange(i, size).ToArray());
                    //this._alarmJournal.Value = journal;
                    this.ReadRecordFromBinFile();
                    j++;
                }
            }
            else
            {
                this._table.Clear();
                try
                {
                    this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка открытия файла","Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveAlarmJournalDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }

            this._saveJournalHtmlDialog.FileName = $"Журнал аварий МР801 {this._device.DevicePlant} v{this._device.DeviceVersion.Replace(".", "_")}";

            if (this._saveJournalHtmlDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string xml;

                    using (StringWriter writer = new StringWriter())
                    {
                        this._table.WriteXml(writer);
                        xml = writer.ToString();
                    }

                    HtmlExport.Export(this._saveJournalHtmlDialog.FileName, "МР901. Журнал аварий", xml);
                    this._statusLabel.Text = "Журнал успешно сохранен.";

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }

            //if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            //{
            //    HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR7AJ);
            //    this._statusLabel.Text = JOURNAL_SAVED;
            //}
        }

        private void AlarmJournalForm_FormClosing(object sender, FormClosedEventArgs e)
        {
            if (this._alarmJournal != null)
            {
                this._alarmJournal.RemoveStructQueries();
            }
            if (this._journalLoader != null)
            {
                this._journalLoader.ClearEvents();
            }
        }
        #endregion [Event Handlers]
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr7); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr7AlarmJournalForm); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]
        
    }

}
