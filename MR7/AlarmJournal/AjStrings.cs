﻿using System.Collections.Generic;

namespace BEMN.MR7.AlarmJournal
{
    public static class AjStrings
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Группа 1",
                        "Группа 2"
                    };
            }
        }

        /// <summary>
        /// Сработавшая защита
        /// </summary>
        public static List<string> TriggeredDefense
        {
            get
            {
                return new List<string>
                {
                    "Iд>> мгн.",
                    "Iд>>",
                    "Iд>",
                    "Iд0> 1",
                    "Iд0> 2",
                    "Iд0> 3",
                    "I> 1",
                    "I> 2",
                    "I> 3",
                    "I> 4",
                    "I> 5",
                    "I> 6",
                    "I< ",
                    "I*> 1",
                    "I*> 2",
                    "I*> 3",
                    "I*> 4",
                    "I*> 5",
                    "I*> 6",
                    "I*> 7",
                    "I*> 8",
                    "I2/I1",
                    "U> 1",
                    "U> 2",
                    "U> 3",
                    "U> 4",
                    "U< 1",
                    "U< 2",
                    "U< 3",
                    "U< 4",
                    "F> 1",
                    "F> 2",
                    "F> 3",
                    "F> 4",
                    "F< 1",
                    "F< 2",
                    "F< 3",
                    "F< 4",
                    "Z> 1",
                    "Z> 2",
                    "Z> 3",
                    "Z> 4",
                    "Z> 5",
                    "Z> 6",
                    "РЕЗЕРВ",
                    "P1",
                    "P2",
                    "РЕЗЕРВ",
                    "Q>",
                    "Q>>",
                    "ВНЕШ. 1",
                    "ВНЕШ. 2",
                    "ВНЕШ. 3",
                    "ВНЕШ. 4",
                    "ВНЕШ. 5",
                    "ВНЕШ. 6",
                    "ВНЕШ. 7",
                    "ВНЕШ. 8",
                    "ВНЕШ. 9",
                    "ВНЕШ. 10",
                    "ВНЕШ. 11",
                    "ВНЕШ. 12",
                    "ВНЕШ. 13",
                    "ВНЕШ. 14",
                    "ВНЕШ. 15",
                    "ВНЕШ. 16",
                    "Блок. по Q",
                    "Блок. по N",
                    "ПУСК ДУГОВОЙ ЗАЩИТЫ"
                };
            }
        }

        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message
        {
            get
            {
                return new List<string>
                {
                    "ОШИБКА СООБЩЕНИЯ",
                    "СИГНАЛ-ЦИЯ",
                    "РАБОТА",
                    "ОТКЛЮЧЕНИЕ",
                    "НЕУСПЕШНОЕ АПВ",
                    "ВОЗВРАТ",
                    "ВКЛЮЧЕНИЕ",
                    "ОМП",
                    "ОМП *",
                    "ОМП ОШИБКА",
                    "СООБЩЕНИЕ СПЛ"
                };
            }
        }

        /// <summary>
        /// Параметр срабатывания
        /// </summary>
        public static List<string> Parametr
        {
            get
            {
                return new List<string>
                   {
                       "Ia диф.",//0
                       "Ib диф.",
                       "Iс диф.",
                       "Ia торм.",
                       "Ib торм.",
                       "Ic торм.",

                       "Iа s1",
                       "Ib s1",
                       "Ic s1",
                       "3I0 s1",
                       "I2 s1",
                       "In s1",
                       "диф. I0 s1",
                       "торм. I0 s1",

                       "Iа s2",
                       "Ib s2",
                       "Ic s2",
                       "3I0 s2",
                       "I2 s2",
                       "In s2",
                       "диф. I0 s2",
                       "торм. I0 s2",

                       "Iа s3",
                       "Ib s3",
                       "Ic s3",
                       "3I0 s3",
                       "I2 s3",
                       "In s3",
                       "диф. I0 s3",
                       "торм. I0 s3",

                       "Iа s4",
                       "Ib s4",
                       "Ic s4",
                       "3I0 s4",
                       "I2 s4",
                       "In s4",
                       "диф. I0 s4",
                       "торм. I0 s4",

                       "Uab1",
                       "Ubc1",
                       "Uca1",
                       "U2-1",
                       "Ua1",
                       "Ub1",
                       "Uc1",
                       "3U0-1",

                       "Uab2",
                       "Ubc2",
                       "Uca2",
                       "U2-2",
                       "Ua2",
                       "Ub2",
                       "Uc2",
                       "3U0-2",

                       "Un",
                       "F",
                       "D1",
                       "D2",
                       "D3",
                       "D4",
                       "Zab",
                       "Zab",
                       "Zbс",
                       "Zbс",
                       "Zсa",
                       "Zca",
                       "ZA1",
                       "ZA1",
                       "ZB1", 
                       "ZB1",
                       "ZC1",
                       "ZC1",
                       "Q",
                       "СПЛ",
                       "dF/dt",
                       "Пуск дуговой защиты",
                       "P",
                       "Резерв",
                       "Резерв",
                       "Резерв",
                       "Ст. 1",
                       "Ст. 2",
                       "Ст. 3",
                       "Ст. 4"
                   };
            }
        }
    }
}
