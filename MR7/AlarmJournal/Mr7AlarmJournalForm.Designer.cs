﻿namespace BEMN.MR7.AlarmJournal
{
    partial class Mr7AlarmJournalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IdcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ItcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is13I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is1InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is2cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s13I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s2I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s2InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is3cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s33I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s3I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s3InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is4aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is4bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Is4cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s43I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s4I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._s4InCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ua1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ub1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Uc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Uab1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ubc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Uca1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U12Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U130Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2aCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2bCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2cCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Uab2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ubc2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Ucb2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U22Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U230Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Un = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._d5Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D6Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rbc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xbc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ra1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xa1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rb1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xb1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rc1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xc1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать журнал";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButtonClick);
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(536, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(402, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(128, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР801";
            this._openAlarmJournalDialog.Filter = "МР801 Журнал аварий(*.xml)|*.xml|МР801 Журнал аварий(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР 761";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР801";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР801) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР 761";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(659, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР801";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР801 | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР761";
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._groupCol,
            this._IdaCol,
            this._IdbCol,
            this._IdcCol,
            this._ItaCol,
            this._ItbCol,
            this._ItcCol,
            this._Is1aCol,
            this._Is1bCol,
            this._Is1cCol,
            this._Is13I0Col,
            this._Is1I2Col,
            this._Is1InCol,
            this._Is2aCol,
            this._Is2bCol,
            this._Is2cCol,
            this._s13I0Col,
            this._s2I2Col,
            this._s2InCol,
            this._Is3aCol,
            this._Is3bCol,
            this._Is3cCol,
            this._s33I0Col,
            this._s3I2Col,
            this._s3InCol,
            this._Is4aCol,
            this._Is4bCol,
            this._Is4cCol,
            this._s43I0Col,
            this._s4I2Col,
            this._s4InCol,
            this._Ua1Col,
            this._Ub1Col,
            this._Uc1Col,
            this._Uab1Col,
            this._Ubc1Col,
            this._Uca1Col,
            this._U12Col,
            this._U130Col,
            this._U2aCol,
            this._U2bCol,
            this._U2cCol,
            this._Uab2Col,
            this._Ubc2Col,
            this._Ucb2Col,
            this._U22Col,
            this._U230Col,
            this._FCol,
            this._Un,
            this._D0Col,
            this._D1Col,
            this._D2Col,
            this._D3Col,
            this._D4Col,
            this._d5Col,
            this._D6Col,
            this._rab,
            this._xab,
            this._rbc,
            this._xbc,
            this._rca,
            this._xca,
            this._ra1,
            this._xa1,
            this._rb1,
            this._xb1,
            this._rc1,
            this._xc1});
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle75.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle75.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle75.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle75.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle75.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle75.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._alarmJournalGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle75;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 24;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "№";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "Дата/Время";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._timeCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._msg1Col.DataPropertyName = "Сообщение";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle3;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msg1Col.Width = 71;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "Сработавшая защита";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 140;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "Параметр срабатывания";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "Значение параметра срабатывания";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle6;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._groupCol.DataPropertyName = "Группа уставок";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle7;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _IdaCol
            // 
            this._IdaCol.DataPropertyName = "Ia диф.";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdaCol.DefaultCellStyle = dataGridViewCellStyle8;
            this._IdaCol.HeaderText = "Ia диф.";
            this._IdaCol.Name = "_IdaCol";
            this._IdaCol.ReadOnly = true;
            this._IdaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdaCol.Width = 43;
            // 
            // _IdbCol
            // 
            this._IdbCol.DataPropertyName = "Ib диф.";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdbCol.DefaultCellStyle = dataGridViewCellStyle9;
            this._IdbCol.HeaderText = "Ib диф.";
            this._IdbCol.Name = "_IdbCol";
            this._IdbCol.ReadOnly = true;
            this._IdbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdbCol.Width = 43;
            // 
            // _IdcCol
            // 
            this._IdcCol.DataPropertyName = "Ic диф.";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._IdcCol.DefaultCellStyle = dataGridViewCellStyle10;
            this._IdcCol.HeaderText = "Ic диф.";
            this._IdcCol.Name = "_IdcCol";
            this._IdcCol.ReadOnly = true;
            this._IdcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IdcCol.Width = 43;
            // 
            // _ItaCol
            // 
            this._ItaCol.DataPropertyName = "Ia торм.";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ItaCol.DefaultCellStyle = dataGridViewCellStyle11;
            this._ItaCol.HeaderText = "Ia торм.";
            this._ItaCol.Name = "_ItaCol";
            this._ItaCol.ReadOnly = true;
            this._ItaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItaCol.Width = 48;
            // 
            // _ItbCol
            // 
            this._ItbCol.DataPropertyName = "Ib торм.";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ItbCol.DefaultCellStyle = dataGridViewCellStyle12;
            this._ItbCol.HeaderText = "Ib торм.";
            this._ItbCol.Name = "_ItbCol";
            this._ItbCol.ReadOnly = true;
            this._ItbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItbCol.Width = 48;
            // 
            // _ItcCol
            // 
            this._ItcCol.DataPropertyName = "Ic торм.";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ItcCol.DefaultCellStyle = dataGridViewCellStyle13;
            this._ItcCol.HeaderText = "Ic торм.";
            this._ItcCol.Name = "_ItcCol";
            this._ItcCol.ReadOnly = true;
            this._ItcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ItcCol.Width = 48;
            // 
            // _Is1aCol
            // 
            this._Is1aCol.DataPropertyName = "Ia s1";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is1aCol.DefaultCellStyle = dataGridViewCellStyle14;
            this._Is1aCol.HeaderText = "Ia s1";
            this._Is1aCol.Name = "_Is1aCol";
            this._Is1aCol.ReadOnly = true;
            this._Is1aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1aCol.Width = 32;
            // 
            // _Is1bCol
            // 
            this._Is1bCol.DataPropertyName = "Ib s1";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is1bCol.DefaultCellStyle = dataGridViewCellStyle15;
            this._Is1bCol.HeaderText = "Ib s1";
            this._Is1bCol.Name = "_Is1bCol";
            this._Is1bCol.ReadOnly = true;
            this._Is1bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1bCol.Width = 32;
            // 
            // _Is1cCol
            // 
            this._Is1cCol.DataPropertyName = "Ic s1";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is1cCol.DefaultCellStyle = dataGridViewCellStyle16;
            this._Is1cCol.HeaderText = "Ic s1";
            this._Is1cCol.Name = "_Is1cCol";
            this._Is1cCol.ReadOnly = true;
            this._Is1cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1cCol.Width = 32;
            // 
            // _Is13I0Col
            // 
            this._Is13I0Col.DataPropertyName = "3I0 s1";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is13I0Col.DefaultCellStyle = dataGridViewCellStyle17;
            this._Is13I0Col.HeaderText = "3I0 s1";
            this._Is13I0Col.Name = "_Is13I0Col";
            this._Is13I0Col.ReadOnly = true;
            this._Is13I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is13I0Col.Width = 28;
            // 
            // _Is1I2Col
            // 
            this._Is1I2Col.DataPropertyName = "I2 s1";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is1I2Col.DefaultCellStyle = dataGridViewCellStyle18;
            this._Is1I2Col.HeaderText = "I2 s1";
            this._Is1I2Col.Name = "_Is1I2Col";
            this._Is1I2Col.ReadOnly = true;
            this._Is1I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1I2Col.Width = 32;
            // 
            // _Is1InCol
            // 
            this._Is1InCol.DataPropertyName = "In s1";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is1InCol.DefaultCellStyle = dataGridViewCellStyle19;
            this._Is1InCol.HeaderText = "In s1";
            this._Is1InCol.Name = "_Is1InCol";
            this._Is1InCol.ReadOnly = true;
            this._Is1InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is1InCol.Width = 32;
            // 
            // _Is2aCol
            // 
            this._Is2aCol.DataPropertyName = "Ia s2";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is2aCol.DefaultCellStyle = dataGridViewCellStyle20;
            this._Is2aCol.HeaderText = "Ia s2";
            this._Is2aCol.Name = "_Is2aCol";
            this._Is2aCol.ReadOnly = true;
            this._Is2aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2aCol.Width = 32;
            // 
            // _Is2bCol
            // 
            this._Is2bCol.DataPropertyName = "Ib s2";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is2bCol.DefaultCellStyle = dataGridViewCellStyle21;
            this._Is2bCol.HeaderText = "Ib s2";
            this._Is2bCol.Name = "_Is2bCol";
            this._Is2bCol.ReadOnly = true;
            this._Is2bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2bCol.Width = 32;
            // 
            // _Is2cCol
            // 
            this._Is2cCol.DataPropertyName = "Ic s2";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is2cCol.DefaultCellStyle = dataGridViewCellStyle22;
            this._Is2cCol.HeaderText = "Ic s2";
            this._Is2cCol.Name = "_Is2cCol";
            this._Is2cCol.ReadOnly = true;
            this._Is2cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is2cCol.Width = 32;
            // 
            // _s13I0Col
            // 
            this._s13I0Col.DataPropertyName = "3I0 s2";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s13I0Col.DefaultCellStyle = dataGridViewCellStyle23;
            this._s13I0Col.HeaderText = "3I0 s2";
            this._s13I0Col.Name = "_s13I0Col";
            this._s13I0Col.ReadOnly = true;
            this._s13I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s13I0Col.Width = 28;
            // 
            // _s2I2Col
            // 
            this._s2I2Col.DataPropertyName = "I2 s2";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s2I2Col.DefaultCellStyle = dataGridViewCellStyle24;
            this._s2I2Col.HeaderText = "I2 s2";
            this._s2I2Col.Name = "_s2I2Col";
            this._s2I2Col.ReadOnly = true;
            this._s2I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s2I2Col.Width = 32;
            // 
            // _s2InCol
            // 
            this._s2InCol.DataPropertyName = "In s2";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s2InCol.DefaultCellStyle = dataGridViewCellStyle25;
            this._s2InCol.HeaderText = "In s2";
            this._s2InCol.Name = "_s2InCol";
            this._s2InCol.ReadOnly = true;
            this._s2InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s2InCol.Width = 32;
            // 
            // _Is3aCol
            // 
            this._Is3aCol.DataPropertyName = "Ia s3";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is3aCol.DefaultCellStyle = dataGridViewCellStyle26;
            this._Is3aCol.HeaderText = "Ia s3";
            this._Is3aCol.Name = "_Is3aCol";
            this._Is3aCol.ReadOnly = true;
            this._Is3aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3aCol.Width = 32;
            // 
            // _Is3bCol
            // 
            this._Is3bCol.DataPropertyName = "Ib s3";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is3bCol.DefaultCellStyle = dataGridViewCellStyle27;
            this._Is3bCol.HeaderText = "Ib s3";
            this._Is3bCol.Name = "_Is3bCol";
            this._Is3bCol.ReadOnly = true;
            this._Is3bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3bCol.Width = 32;
            // 
            // _Is3cCol
            // 
            this._Is3cCol.DataPropertyName = "Ic s3";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is3cCol.DefaultCellStyle = dataGridViewCellStyle28;
            this._Is3cCol.HeaderText = "Ic s3";
            this._Is3cCol.Name = "_Is3cCol";
            this._Is3cCol.ReadOnly = true;
            this._Is3cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is3cCol.Width = 32;
            // 
            // _s33I0Col
            // 
            this._s33I0Col.DataPropertyName = "3I0 s3";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s33I0Col.DefaultCellStyle = dataGridViewCellStyle29;
            this._s33I0Col.HeaderText = "3I0 s3";
            this._s33I0Col.Name = "_s33I0Col";
            this._s33I0Col.ReadOnly = true;
            this._s33I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s33I0Col.Width = 28;
            // 
            // _s3I2Col
            // 
            this._s3I2Col.DataPropertyName = "I2 s3";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s3I2Col.DefaultCellStyle = dataGridViewCellStyle30;
            this._s3I2Col.HeaderText = "I2 s3";
            this._s3I2Col.Name = "_s3I2Col";
            this._s3I2Col.ReadOnly = true;
            this._s3I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s3I2Col.Width = 32;
            // 
            // _s3InCol
            // 
            this._s3InCol.DataPropertyName = "In s3";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s3InCol.DefaultCellStyle = dataGridViewCellStyle31;
            this._s3InCol.HeaderText = "In s3";
            this._s3InCol.Name = "_s3InCol";
            this._s3InCol.ReadOnly = true;
            this._s3InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s3InCol.Width = 32;
            // 
            // _Is4aCol
            // 
            this._Is4aCol.DataPropertyName = "Ia s4";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is4aCol.DefaultCellStyle = dataGridViewCellStyle32;
            this._Is4aCol.HeaderText = "Ia s4";
            this._Is4aCol.Name = "_Is4aCol";
            this._Is4aCol.ReadOnly = true;
            this._Is4aCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is4aCol.Width = 32;
            // 
            // _Is4bCol
            // 
            this._Is4bCol.DataPropertyName = "Ib s4";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is4bCol.DefaultCellStyle = dataGridViewCellStyle33;
            this._Is4bCol.HeaderText = "Ib s4";
            this._Is4bCol.Name = "_Is4bCol";
            this._Is4bCol.ReadOnly = true;
            this._Is4bCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is4bCol.Width = 32;
            // 
            // _Is4cCol
            // 
            this._Is4cCol.DataPropertyName = "Ic s4";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Is4cCol.DefaultCellStyle = dataGridViewCellStyle34;
            this._Is4cCol.HeaderText = "Ic s4";
            this._Is4cCol.Name = "_Is4cCol";
            this._Is4cCol.ReadOnly = true;
            this._Is4cCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._Is4cCol.Width = 32;
            // 
            // _s43I0Col
            // 
            this._s43I0Col.DataPropertyName = "3I0 s4";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s43I0Col.DefaultCellStyle = dataGridViewCellStyle35;
            this._s43I0Col.HeaderText = "3I0 s4";
            this._s43I0Col.Name = "_s43I0Col";
            this._s43I0Col.ReadOnly = true;
            this._s43I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s43I0Col.Width = 28;
            // 
            // _s4I2Col
            // 
            this._s4I2Col.DataPropertyName = "I2 s4";
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s4I2Col.DefaultCellStyle = dataGridViewCellStyle36;
            this._s4I2Col.HeaderText = "I2 s4";
            this._s4I2Col.Name = "_s4I2Col";
            this._s4I2Col.ReadOnly = true;
            this._s4I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s4I2Col.Width = 32;
            // 
            // _s4InCol
            // 
            this._s4InCol.DataPropertyName = "In s4";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._s4InCol.DefaultCellStyle = dataGridViewCellStyle37;
            this._s4InCol.HeaderText = "In s4";
            this._s4InCol.Name = "_s4InCol";
            this._s4InCol.ReadOnly = true;
            this._s4InCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._s4InCol.Width = 32;
            // 
            // _Ua1Col
            // 
            this._Ua1Col.DataPropertyName = "Ua1";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ua1Col.DefaultCellStyle = dataGridViewCellStyle38;
            this._Ua1Col.HeaderText = "Ua1";
            this._Ua1Col.Name = "_Ua1Col";
            this._Ua1Col.ReadOnly = true;
            this._Ua1Col.Width = 52;
            // 
            // _Ub1Col
            // 
            this._Ub1Col.DataPropertyName = "Ub1";
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ub1Col.DefaultCellStyle = dataGridViewCellStyle39;
            this._Ub1Col.HeaderText = "Ub1";
            this._Ub1Col.Name = "_Ub1Col";
            this._Ub1Col.ReadOnly = true;
            this._Ub1Col.Width = 52;
            // 
            // _Uc1Col
            // 
            this._Uc1Col.DataPropertyName = "Uc1";
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Uc1Col.DefaultCellStyle = dataGridViewCellStyle40;
            this._Uc1Col.HeaderText = "Uc1";
            this._Uc1Col.Name = "_Uc1Col";
            this._Uc1Col.ReadOnly = true;
            this._Uc1Col.Width = 52;
            // 
            // _Uab1Col
            // 
            this._Uab1Col.DataPropertyName = "Uab1";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Uab1Col.DefaultCellStyle = dataGridViewCellStyle41;
            this._Uab1Col.HeaderText = "Uab1";
            this._Uab1Col.Name = "_Uab1Col";
            this._Uab1Col.ReadOnly = true;
            this._Uab1Col.Width = 58;
            // 
            // _Ubc1Col
            // 
            this._Ubc1Col.DataPropertyName = "Ubc1";
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ubc1Col.DefaultCellStyle = dataGridViewCellStyle42;
            this._Ubc1Col.HeaderText = "Ubc1";
            this._Ubc1Col.Name = "_Ubc1Col";
            this._Ubc1Col.ReadOnly = true;
            this._Ubc1Col.Width = 58;
            // 
            // _Uca1Col
            // 
            this._Uca1Col.DataPropertyName = "Uca1";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Uca1Col.DefaultCellStyle = dataGridViewCellStyle43;
            this._Uca1Col.HeaderText = "Uca1";
            this._Uca1Col.Name = "_Uca1Col";
            this._Uca1Col.ReadOnly = true;
            this._Uca1Col.Width = 58;
            // 
            // _U12Col
            // 
            this._U12Col.DataPropertyName = "U2-1";
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U12Col.DefaultCellStyle = dataGridViewCellStyle44;
            this._U12Col.HeaderText = "U2-1";
            this._U12Col.Name = "_U12Col";
            this._U12Col.ReadOnly = true;
            this._U12Col.Width = 55;
            // 
            // _U130Col
            // 
            this._U130Col.DataPropertyName = "3U0-1";
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U130Col.DefaultCellStyle = dataGridViewCellStyle45;
            this._U130Col.HeaderText = "3U0-1";
            this._U130Col.Name = "_U130Col";
            this._U130Col.ReadOnly = true;
            this._U130Col.Width = 61;
            // 
            // _U2aCol
            // 
            this._U2aCol.DataPropertyName = "Ua2";
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U2aCol.DefaultCellStyle = dataGridViewCellStyle46;
            this._U2aCol.HeaderText = "Ua2";
            this._U2aCol.Name = "_U2aCol";
            this._U2aCol.ReadOnly = true;
            this._U2aCol.Width = 52;
            // 
            // _U2bCol
            // 
            this._U2bCol.DataPropertyName = "Ub2";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U2bCol.DefaultCellStyle = dataGridViewCellStyle47;
            this._U2bCol.HeaderText = "Ub2";
            this._U2bCol.Name = "_U2bCol";
            this._U2bCol.ReadOnly = true;
            this._U2bCol.Width = 52;
            // 
            // _U2cCol
            // 
            this._U2cCol.DataPropertyName = "Uc2";
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U2cCol.DefaultCellStyle = dataGridViewCellStyle48;
            this._U2cCol.HeaderText = "Uc2";
            this._U2cCol.Name = "_U2cCol";
            this._U2cCol.ReadOnly = true;
            this._U2cCol.Width = 52;
            // 
            // _Uab2Col
            // 
            this._Uab2Col.DataPropertyName = "Uab2";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Uab2Col.DefaultCellStyle = dataGridViewCellStyle49;
            this._Uab2Col.HeaderText = "Uab2";
            this._Uab2Col.Name = "_Uab2Col";
            this._Uab2Col.ReadOnly = true;
            this._Uab2Col.Width = 58;
            // 
            // _Ubc2Col
            // 
            this._Ubc2Col.DataPropertyName = "Ubc2";
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ubc2Col.DefaultCellStyle = dataGridViewCellStyle50;
            this._Ubc2Col.HeaderText = "Ubc2";
            this._Ubc2Col.Name = "_Ubc2Col";
            this._Ubc2Col.ReadOnly = true;
            this._Ubc2Col.Width = 58;
            // 
            // _Ucb2Col
            // 
            this._Ucb2Col.DataPropertyName = "Uca2";
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Ucb2Col.DefaultCellStyle = dataGridViewCellStyle51;
            this._Ucb2Col.HeaderText = "Uca2";
            this._Ucb2Col.Name = "_Ucb2Col";
            this._Ucb2Col.ReadOnly = true;
            this._Ucb2Col.Width = 58;
            // 
            // _U22Col
            // 
            this._U22Col.DataPropertyName = "U2-2";
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U22Col.DefaultCellStyle = dataGridViewCellStyle52;
            this._U22Col.HeaderText = "U2-2";
            this._U22Col.Name = "_U22Col";
            this._U22Col.ReadOnly = true;
            this._U22Col.Width = 55;
            // 
            // _U230Col
            // 
            this._U230Col.DataPropertyName = "3U0-2";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._U230Col.DefaultCellStyle = dataGridViewCellStyle53;
            this._U230Col.HeaderText = "3U0-2";
            this._U230Col.Name = "_U230Col";
            this._U230Col.ReadOnly = true;
            this._U230Col.Width = 61;
            // 
            // _FCol
            // 
            this._FCol.DataPropertyName = "F";
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._FCol.DefaultCellStyle = dataGridViewCellStyle54;
            this._FCol.HeaderText = "F";
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 19;
            // 
            // _Un
            // 
            this._Un.DataPropertyName = "Un";
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._Un.DefaultCellStyle = dataGridViewCellStyle55;
            this._Un.HeaderText = "Un";
            this._Un.Name = "_Un";
            this._Un.ReadOnly = true;
            this._Un.Width = 46;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "Д1-Д8";
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D0Col.DefaultCellStyle = dataGridViewCellStyle56;
            this._D0Col.HeaderText = "Д1-Д8";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 46;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "Д9-Д16";
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D1Col.DefaultCellStyle = dataGridViewCellStyle57;
            this._D1Col.HeaderText = "Д9-Д16";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 52;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "Д17-Д24";
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D2Col.DefaultCellStyle = dataGridViewCellStyle58;
            this._D2Col.HeaderText = "Д17-Д24";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 58;
            // 
            // _D3Col
            // 
            this._D3Col.DataPropertyName = "Д25-Д32";
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D3Col.DefaultCellStyle = dataGridViewCellStyle59;
            this._D3Col.HeaderText = "Д25-Д32";
            this._D3Col.Name = "_D3Col";
            this._D3Col.ReadOnly = true;
            this._D3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D3Col.Width = 58;
            // 
            // _D4Col
            // 
            this._D4Col.DataPropertyName = "Д33-Д40";
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D4Col.DefaultCellStyle = dataGridViewCellStyle60;
            this._D4Col.HeaderText = "Д33-Д40";
            this._D4Col.Name = "_D4Col";
            this._D4Col.ReadOnly = true;
            this._D4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D4Col.Width = 58;
            // 
            // _d5Col
            // 
            this._d5Col.DataPropertyName = "Д41-Д48";
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._d5Col.DefaultCellStyle = dataGridViewCellStyle61;
            this._d5Col.HeaderText = "Д41-Д48";
            this._d5Col.Name = "_d5Col";
            this._d5Col.ReadOnly = true;
            this._d5Col.Width = 77;
            // 
            // _D6Col
            // 
            this._D6Col.DataPropertyName = "Д49-Д56";
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._D6Col.DefaultCellStyle = dataGridViewCellStyle62;
            this._D6Col.HeaderText = "Д49-Д56";
            this._D6Col.Name = "_D6Col";
            this._D6Col.ReadOnly = true;
            this._D6Col.Width = 77;
            // 
            // _rab
            // 
            this._rab.DataPropertyName = "Rab";
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._rab.DefaultCellStyle = dataGridViewCellStyle63;
            this._rab.HeaderText = "Rab";
            this._rab.Name = "_rab";
            this._rab.ReadOnly = true;
            this._rab.Visible = false;
            this._rab.Width = 52;
            // 
            // _xab
            // 
            this._xab.DataPropertyName = "Xab";
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xab.DefaultCellStyle = dataGridViewCellStyle64;
            this._xab.HeaderText = "Xab";
            this._xab.Name = "_xab";
            this._xab.ReadOnly = true;
            this._xab.Visible = false;
            this._xab.Width = 51;
            // 
            // _rbc
            // 
            this._rbc.DataPropertyName = "Rbc";
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._rbc.DefaultCellStyle = dataGridViewCellStyle65;
            this._rbc.HeaderText = "Rbc";
            this._rbc.Name = "_rbc";
            this._rbc.ReadOnly = true;
            this._rbc.Visible = false;
            this._rbc.Width = 52;
            // 
            // _xbc
            // 
            this._xbc.DataPropertyName = "Xbc";
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xbc.DefaultCellStyle = dataGridViewCellStyle66;
            this._xbc.HeaderText = "Xbc";
            this._xbc.Name = "_xbc";
            this._xbc.ReadOnly = true;
            this._xbc.Visible = false;
            this._xbc.Width = 51;
            // 
            // _rca
            // 
            this._rca.DataPropertyName = "Rca";
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._rca.DefaultCellStyle = dataGridViewCellStyle67;
            this._rca.HeaderText = "Rca";
            this._rca.Name = "_rca";
            this._rca.ReadOnly = true;
            this._rca.Visible = false;
            this._rca.Width = 52;
            // 
            // _xca
            // 
            this._xca.DataPropertyName = "Xca";
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xca.DefaultCellStyle = dataGridViewCellStyle68;
            this._xca.HeaderText = "Xca";
            this._xca.Name = "_xca";
            this._xca.ReadOnly = true;
            this._xca.Visible = false;
            this._xca.Width = 51;
            // 
            // _ra1
            // 
            this._ra1.DataPropertyName = "RA1";
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._ra1.DefaultCellStyle = dataGridViewCellStyle69;
            this._ra1.HeaderText = "RA1";
            this._ra1.Name = "_ra1";
            this._ra1.ReadOnly = true;
            this._ra1.Visible = false;
            this._ra1.Width = 53;
            // 
            // _xa1
            // 
            this._xa1.DataPropertyName = "XA1";
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xa1.DefaultCellStyle = dataGridViewCellStyle70;
            this._xa1.HeaderText = "XA1";
            this._xa1.Name = "_xa1";
            this._xa1.ReadOnly = true;
            this._xa1.Visible = false;
            this._xa1.Width = 52;
            // 
            // _rb1
            // 
            this._rb1.DataPropertyName = "RB1";
            dataGridViewCellStyle71.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._rb1.DefaultCellStyle = dataGridViewCellStyle71;
            this._rb1.HeaderText = "RB1";
            this._rb1.Name = "_rb1";
            this._rb1.ReadOnly = true;
            this._rb1.Visible = false;
            this._rb1.Width = 53;
            // 
            // _xb1
            // 
            this._xb1.DataPropertyName = "XB1";
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xb1.DefaultCellStyle = dataGridViewCellStyle72;
            this._xb1.HeaderText = "XB1";
            this._xb1.Name = "_xb1";
            this._xb1.ReadOnly = true;
            this._xb1.Visible = false;
            this._xb1.Width = 52;
            // 
            // _rc1
            // 
            this._rc1.DataPropertyName = "RC1";
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._rc1.DefaultCellStyle = dataGridViewCellStyle73;
            this._rc1.HeaderText = "RC1";
            this._rc1.Name = "_rc1";
            this._rc1.ReadOnly = true;
            this._rc1.Visible = false;
            this._rc1.Width = 53;
            // 
            // _xc1
            // 
            this._xc1.DataPropertyName = "XC1";
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._xc1.DefaultCellStyle = dataGridViewCellStyle74;
            this._xc1.HeaderText = "XC1";
            this._xc1.Name = "_xc1";
            this._xc1.ReadOnly = true;
            this._xc1.Visible = false;
            this._xc1.Width = 52;
            // 
            // Mr7AlarmJournalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Mr7AlarmJournalForm";
            this.Text = "AlarmJournalForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IdcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ItcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is13I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is1InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is2cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s13I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s2I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s2InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is3cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s33I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s3I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s3InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is4aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is4bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Is4cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s43I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s4I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _s4InCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ua1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ub1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Uc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Uab1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ubc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Uca1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U12Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U130Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2aCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2bCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2cCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Uab2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ubc2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Ucb2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U22Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U230Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Un;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D4Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _d5Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D6Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rab;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xab;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rbc;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xbc;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rca;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xca;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ra1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xa1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rb1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xb1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rc1;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xc1;
    }
}