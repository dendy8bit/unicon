﻿namespace BEMN.MR7.Measuring
{
    partial class Mr7MeasuringForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BEMN.Devices.Structures.DateTimeStruct dateTimeStruct1 = new BEMN.Devices.Structures.DateTimeStruct();
            this._analogTabPage = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._dU = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this._dFi = new System.Windows.Forms.TextBox();
            this._dF = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this._dateTimeControl = new BEMN.Forms.DateTimeControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this._voltageTB = new System.Windows.Forms.TextBox();
            this.groupBox67 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label504 = new System.Windows.Forms.Label();
            this._U2abc0 = new System.Windows.Forms.TextBox();
            this._U2abcC = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label502 = new System.Windows.Forms.Label();
            this._U2abc2 = new System.Windows.Forms.TextBox();
            this._U2abcB = new System.Windows.Forms.TextBox();
            this._U2abc1 = new System.Windows.Forms.TextBox();
            this._U2abcA = new System.Windows.Forms.TextBox();
            this._U2ca = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label500 = new System.Windows.Forms.Label();
            this._U2bc = new System.Windows.Forms.TextBox();
            this._U2ab = new System.Windows.Forms.TextBox();
            this.label498 = new System.Windows.Forms.Label();
            this.label496 = new System.Windows.Forms.Label();
            this.label494 = new System.Windows.Forms.Label();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this._U1abc0 = new System.Windows.Forms.TextBox();
            this._U1abc2 = new System.Windows.Forms.TextBox();
            this._U1abc1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label495 = new System.Windows.Forms.Label();
            this.label497 = new System.Windows.Forms.Label();
            this._U1abcC = new System.Windows.Forms.TextBox();
            this._U1abcB = new System.Windows.Forms.TextBox();
            this._U1abcA = new System.Windows.Forms.TextBox();
            this._U1ca = new System.Windows.Forms.TextBox();
            this._U10 = new System.Windows.Forms.Label();
            this._U1ab = new System.Windows.Forms.TextBox();
            this._U1bc = new System.Windows.Forms.TextBox();
            this.label503 = new System.Windows.Forms.Label();
            this.label501 = new System.Windows.Forms.Label();
            this.label499 = new System.Windows.Forms.Label();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.label482 = new System.Windows.Forms.Label();
            this._Is42 = new System.Windows.Forms.TextBox();
            this._Is4I1 = new System.Windows.Forms.TextBox();
            this._Is40 = new System.Windows.Forms.TextBox();
            this._Is4n = new System.Windows.Forms.TextBox();
            this._Is4c = new System.Windows.Forms.TextBox();
            this._Is4b = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this._Is4a = new System.Windows.Forms.TextBox();
            this.label483 = new System.Windows.Forms.Label();
            this.label484 = new System.Windows.Forms.Label();
            this.label485 = new System.Windows.Forms.Label();
            this.label486 = new System.Windows.Forms.Label();
            this.label488 = new System.Windows.Forms.Label();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.label470 = new System.Windows.Forms.Label();
            this._Is32 = new System.Windows.Forms.TextBox();
            this._Is3I1 = new System.Windows.Forms.TextBox();
            this._Is30 = new System.Windows.Forms.TextBox();
            this._Is3n = new System.Windows.Forms.TextBox();
            this._Is3c = new System.Windows.Forms.TextBox();
            this._Is3b = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this._Is3a = new System.Windows.Forms.TextBox();
            this.label471 = new System.Windows.Forms.Label();
            this.label472 = new System.Windows.Forms.Label();
            this.label473 = new System.Windows.Forms.Label();
            this.label474 = new System.Windows.Forms.Label();
            this.label476 = new System.Windows.Forms.Label();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.label323 = new System.Windows.Forms.Label();
            this._Is22 = new System.Windows.Forms.TextBox();
            this._Is2I1 = new System.Windows.Forms.TextBox();
            this._Is20 = new System.Windows.Forms.TextBox();
            this._Is2n = new System.Windows.Forms.TextBox();
            this._Is2c = new System.Windows.Forms.TextBox();
            this._Is2b = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this._Is2a = new System.Windows.Forms.TextBox();
            this.label324 = new System.Windows.Forms.Label();
            this.label325 = new System.Windows.Forms.Label();
            this.label326 = new System.Windows.Forms.Label();
            this.label327 = new System.Windows.Forms.Label();
            this.label329 = new System.Windows.Forms.Label();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this._Is12 = new System.Windows.Forms.TextBox();
            this._Is1I1 = new System.Windows.Forms.TextBox();
            this._Is10 = new System.Windows.Forms.TextBox();
            this._Is1n = new System.Windows.Forms.TextBox();
            this._Is1c = new System.Windows.Forms.TextBox();
            this._Is1b = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._Is1a = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._frequency = new System.Windows.Forms.TextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._percentIdc = new System.Windows.Forms.TextBox();
            this._percentIdb = new System.Windows.Forms.TextBox();
            this._percentIda = new System.Windows.Forms.TextBox();
            this._Idc = new System.Windows.Forms.TextBox();
            this._Idb = new System.Windows.Forms.TextBox();
            this._Ida = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this._percentIbc = new System.Windows.Forms.TextBox();
            this._percentIbb = new System.Windows.Forms.TextBox();
            this._percentIba = new System.Windows.Forms.TextBox();
            this._Ibc = new System.Windows.Forms.TextBox();
            this._Ibb = new System.Windows.Forms.TextBox();
            this._Iba = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this.CN = new System.Windows.Forms.TextBox();
            this.CA = new System.Windows.Forms.TextBox();
            this.BN = new System.Windows.Forms.TextBox();
            this.AN = new System.Windows.Forms.TextBox();
            this.BC = new System.Windows.Forms.TextBox();
            this.AB = new System.Windows.Forms.TextBox();
            this.label340 = new System.Windows.Forms.Label();
            this.label339 = new System.Windows.Forms.Label();
            this.label338 = new System.Windows.Forms.Label();
            this.label337 = new System.Windows.Forms.Label();
            this.label336 = new System.Windows.Forms.Label();
            this.label335 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._cosfTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this._qTextBox = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this._pTextBox = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._za1Box = new System.Windows.Forms.TextBox();
            this._zc1Box = new System.Windows.Forms.TextBox();
            this.label137 = new System.Windows.Forms.Label();
            this._zb1Box = new System.Windows.Forms.TextBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._zabBox = new System.Windows.Forms.TextBox();
            this._zcaBox = new System.Windows.Forms.TextBox();
            this._zbcBox = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.rxcaLabel = new System.Windows.Forms.Label();
            this.rxbcLabel = new System.Windows.Forms.Label();
            this.rxabLabel = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this._iZa1Box = new System.Windows.Forms.TextBox();
            this._iZc1Box = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this._iZb1Box = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this._iZabBox = new System.Windows.Forms.TextBox();
            this._iZcaBox = new System.Windows.Forms.TextBox();
            this._iZbcBox = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label330 = new System.Windows.Forms.Label();
            this.label331 = new System.Windows.Forms.Label();
            this.label332 = new System.Windows.Forms.Label();
            this._dataBaseTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._rsTriggersGB = new System.Windows.Forms.GroupBox();
            this._rst8 = new BEMN.Forms.LedControl();
            this.label32 = new System.Windows.Forms.Label();
            this._rst7 = new BEMN.Forms.LedControl();
            this.label40 = new System.Windows.Forms.Label();
            this._rst1 = new BEMN.Forms.LedControl();
            this._rst6 = new BEMN.Forms.LedControl();
            this.label133 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this._rst5 = new BEMN.Forms.LedControl();
            this._rst2 = new BEMN.Forms.LedControl();
            this.label150 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this._rst4 = new BEMN.Forms.LedControl();
            this._rst16 = new BEMN.Forms.LedControl();
            this._rst3 = new BEMN.Forms.LedControl();
            this.label207 = new System.Windows.Forms.Label();
            this.label208 = new System.Windows.Forms.Label();
            this._rst15 = new BEMN.Forms.LedControl();
            this._rst9 = new BEMN.Forms.LedControl();
            this.label209 = new System.Windows.Forms.Label();
            this.label211 = new System.Windows.Forms.Label();
            this._rst14 = new BEMN.Forms.LedControl();
            this.label214 = new System.Windows.Forms.Label();
            this.label215 = new System.Windows.Forms.Label();
            this._rst10 = new BEMN.Forms.LedControl();
            this._rst13 = new BEMN.Forms.LedControl();
            this.label216 = new System.Windows.Forms.Label();
            this.label217 = new System.Windows.Forms.Label();
            this._rst11 = new BEMN.Forms.LedControl();
            this._rst12 = new BEMN.Forms.LedControl();
            this.label219 = new System.Windows.Forms.Label();
            this._r56Label = new System.Windows.Forms.Label();
            this._r58Label = new System.Windows.Forms.Label();
            this._r70Label = new System.Windows.Forms.Label();
            this._r18Label = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.diod12 = new BEMN.Forms.Diod();
            this.diod11 = new BEMN.Forms.Diod();
            this.diod10 = new BEMN.Forms.Diod();
            this.diod9 = new BEMN.Forms.Diod();
            this.diod8 = new BEMN.Forms.Diod();
            this.diod7 = new BEMN.Forms.Diod();
            this.diod6 = new BEMN.Forms.Diod();
            this.diod5 = new BEMN.Forms.Diod();
            this.diod4 = new BEMN.Forms.Diod();
            this.diod3 = new BEMN.Forms.Diod();
            this.diod2 = new BEMN.Forms.Diod();
            this.diod1 = new BEMN.Forms.Diod();
            this.label190 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this._r59Label = new System.Windows.Forms.Label();
            this._r71Label = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this._diskretInputGB = new System.Windows.Forms.GroupBox();
            this._d48 = new BEMN.Forms.LedControl();
            this._d40 = new BEMN.Forms.LedControl();
            this._d24 = new BEMN.Forms.LedControl();
            this._d48Label = new System.Windows.Forms.Label();
            this._d40Label = new System.Windows.Forms.Label();
            this._d8 = new BEMN.Forms.LedControl();
            this._d47 = new BEMN.Forms.LedControl();
            this._d39 = new BEMN.Forms.LedControl();
            this._d24Label = new System.Windows.Forms.Label();
            this._d47Label = new System.Windows.Forms.Label();
            this._d39Label = new System.Windows.Forms.Label();
            this._d54 = new BEMN.Forms.LedControl();
            this._d46 = new BEMN.Forms.LedControl();
            this._d38 = new BEMN.Forms.LedControl();
            this._d23 = new BEMN.Forms.LedControl();
            this._d46Label = new System.Windows.Forms.Label();
            this._d38Label = new System.Windows.Forms.Label();
            this._d54Label = new System.Windows.Forms.Label();
            this._d8Label = new System.Windows.Forms.Label();
            this._d45 = new BEMN.Forms.LedControl();
            this._d37 = new BEMN.Forms.LedControl();
            this._d23Label = new System.Windows.Forms.Label();
            this._d45Label = new System.Windows.Forms.Label();
            this._d53 = new BEMN.Forms.LedControl();
            this._d37Label = new System.Windows.Forms.Label();
            this._d22 = new BEMN.Forms.LedControl();
            this._d52 = new BEMN.Forms.LedControl();
            this._d44 = new BEMN.Forms.LedControl();
            this._d53Label = new System.Windows.Forms.Label();
            this._d7 = new BEMN.Forms.LedControl();
            this._d52Label = new System.Windows.Forms.Label();
            this._d44Label = new System.Windows.Forms.Label();
            this._d22Label = new System.Windows.Forms.Label();
            this._d51 = new BEMN.Forms.LedControl();
            this._d43 = new BEMN.Forms.LedControl();
            this._d7Label = new System.Windows.Forms.Label();
            this._d51Label = new System.Windows.Forms.Label();
            this._d36 = new BEMN.Forms.LedControl();
            this._d43Label = new System.Windows.Forms.Label();
            this._d21 = new BEMN.Forms.LedControl();
            this._d50 = new BEMN.Forms.LedControl();
            this._d42 = new BEMN.Forms.LedControl();
            this._d36Label = new System.Windows.Forms.Label();
            this._d34 = new BEMN.Forms.LedControl();
            this._d1 = new BEMN.Forms.LedControl();
            this._d35 = new BEMN.Forms.LedControl();
            this._d50Label = new System.Windows.Forms.Label();
            this._d42Label = new System.Windows.Forms.Label();
            this._d34Label = new System.Windows.Forms.Label();
            this._d21Label = new System.Windows.Forms.Label();
            this._k2 = new BEMN.Forms.LedControl();
            this._d35Label = new System.Windows.Forms.Label();
            this._k2Label = new System.Windows.Forms.Label();
            this._k1 = new BEMN.Forms.LedControl();
            this._k1Label = new System.Windows.Forms.Label();
            this._d49 = new BEMN.Forms.LedControl();
            this._d49Label = new System.Windows.Forms.Label();
            this._d41 = new BEMN.Forms.LedControl();
            this._d41Label = new System.Windows.Forms.Label();
            this._d33 = new BEMN.Forms.LedControl();
            this._d33Label = new System.Windows.Forms.Label();
            this._d6 = new BEMN.Forms.LedControl();
            this._d20 = new BEMN.Forms.LedControl();
            this._d32 = new BEMN.Forms.LedControl();
            this._d1Label = new System.Windows.Forms.Label();
            this._d32Label = new System.Windows.Forms.Label();
            this._d20Label = new System.Windows.Forms.Label();
            this._d31 = new BEMN.Forms.LedControl();
            this._d6Label = new System.Windows.Forms.Label();
            this._d31Label = new System.Windows.Forms.Label();
            this._d19 = new BEMN.Forms.LedControl();
            this._d30 = new BEMN.Forms.LedControl();
            this._d2Label = new System.Windows.Forms.Label();
            this._d30Label = new System.Windows.Forms.Label();
            this._d19Label = new System.Windows.Forms.Label();
            this._d29 = new BEMN.Forms.LedControl();
            this._d5 = new BEMN.Forms.LedControl();
            this._d29Label = new System.Windows.Forms.Label();
            this._d28 = new BEMN.Forms.LedControl();
            this._d2 = new BEMN.Forms.LedControl();
            this._d28Label = new System.Windows.Forms.Label();
            this._d27 = new BEMN.Forms.LedControl();
            this._d5Label = new System.Windows.Forms.Label();
            this._d27Label = new System.Windows.Forms.Label();
            this._d17 = new BEMN.Forms.LedControl();
            this._d26 = new BEMN.Forms.LedControl();
            this._d18 = new BEMN.Forms.LedControl();
            this._d17Label = new System.Windows.Forms.Label();
            this._d26Label = new System.Windows.Forms.Label();
            this._d3Label = new System.Windows.Forms.Label();
            this._d25 = new BEMN.Forms.LedControl();
            this._d25Label = new System.Windows.Forms.Label();
            this._d18Label = new System.Windows.Forms.Label();
            this._d4 = new BEMN.Forms.LedControl();
            this._d16 = new BEMN.Forms.LedControl();
            this._d3 = new BEMN.Forms.LedControl();
            this._d16Label = new System.Windows.Forms.Label();
            this._d4Label = new System.Windows.Forms.Label();
            this._d15 = new BEMN.Forms.LedControl();
            this._d9 = new BEMN.Forms.LedControl();
            this._d15Label = new System.Windows.Forms.Label();
            this._d9Label = new System.Windows.Forms.Label();
            this._d14 = new BEMN.Forms.LedControl();
            this._d10Label = new System.Windows.Forms.Label();
            this._d14Label = new System.Windows.Forms.Label();
            this._d10 = new BEMN.Forms.LedControl();
            this._d13 = new BEMN.Forms.LedControl();
            this._d11Label = new System.Windows.Forms.Label();
            this._d13Label = new System.Windows.Forms.Label();
            this._d11 = new BEMN.Forms.LedControl();
            this._d12 = new BEMN.Forms.LedControl();
            this._d12Label = new System.Windows.Forms.Label();
            this._r64Label = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this._r76Label = new System.Windows.Forms.Label();
            this._r40Label = new System.Windows.Forms.Label();
            this._r69Label = new System.Windows.Forms.Label();
            this._r55Label = new System.Windows.Forms.Label();
            this._r51Label = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this._r60Label = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this._r72Label = new System.Windows.Forms.Label();
            this._r57Label = new System.Windows.Forms.Label();
            this._r65Label = new System.Windows.Forms.Label();
            this._r80Label = new System.Windows.Forms.Label();
            this._r77Label = new System.Windows.Forms.Label();
            this._r68Label = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this._r39Label = new System.Windows.Forms.Label();
            this._r75Label = new System.Windows.Forms.Label();
            this._r54Label = new System.Windows.Forms.Label();
            this._r61Label = new System.Windows.Forms.Label();
            this._r63Label = new System.Windows.Forms.Label();
            this._r73Label = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this._r66Label = new System.Windows.Forms.Label();
            this._r78Label = new System.Windows.Forms.Label();
            this._r46Label = new System.Windows.Forms.Label();
            this.label170 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this._r53Label = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this._r62Label = new System.Windows.Forms.Label();
            this._r74Label = new System.Windows.Forms.Label();
            this._r67Label = new System.Windows.Forms.Label();
            this._r34Label = new System.Windows.Forms.Label();
            this._r79Label = new System.Windows.Forms.Label();
            this._r52Label = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this._r50Label = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this._r38Label = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this._r45Label = new System.Windows.Forms.Label();
            this._r30Label = new System.Windows.Forms.Label();
            this._r29Label = new System.Windows.Forms.Label();
            this._r28Label = new System.Windows.Forms.Label();
            this._r27Label = new System.Windows.Forms.Label();
            this._r33Label = new System.Windows.Forms.Label();
            this._r26Label = new System.Windows.Forms.Label();
            this._r25Label = new System.Windows.Forms.Label();
            this._r24Label = new System.Windows.Forms.Label();
            this._r23Label = new System.Windows.Forms.Label();
            this._r31Label = new System.Windows.Forms.Label();
            this._r49Label = new System.Windows.Forms.Label();
            this._r22Label = new System.Windows.Forms.Label();
            this._r21Label = new System.Windows.Forms.Label();
            this._r37Label = new System.Windows.Forms.Label();
            this._r20Label = new System.Windows.Forms.Label();
            this._r44Label = new System.Windows.Forms.Label();
            this._r19Label = new System.Windows.Forms.Label();
            this._r32Label = new System.Windows.Forms.Label();
            this._r42Label = new System.Windows.Forms.Label();
            this._r48Label = new System.Windows.Forms.Label();
            this._r41Label = new System.Windows.Forms.Label();
            this._r36Label = new System.Windows.Forms.Label();
            this._r35Label = new System.Windows.Forms.Label();
            this._r47Label = new System.Windows.Forms.Label();
            this._r43Label = new System.Windows.Forms.Label();
            this._module56 = new BEMN.Forms.LedControl();
            this._module40 = new BEMN.Forms.LedControl();
            this._module57 = new BEMN.Forms.LedControl();
            this._module18 = new BEMN.Forms.LedControl();
            this._module10 = new BEMN.Forms.LedControl();
            this._module69 = new BEMN.Forms.LedControl();
            this._module58 = new BEMN.Forms.LedControl();
            this._module70 = new BEMN.Forms.LedControl();
            this._module39 = new BEMN.Forms.LedControl();
            this._module80 = new BEMN.Forms.LedControl();
            this._module17 = new BEMN.Forms.LedControl();
            this._module51 = new BEMN.Forms.LedControl();
            this._module68 = new BEMN.Forms.LedControl();
            this._module55 = new BEMN.Forms.LedControl();
            this._module59 = new BEMN.Forms.LedControl();
            this._module9 = new BEMN.Forms.LedControl();
            this._module71 = new BEMN.Forms.LedControl();
            this._module75 = new BEMN.Forms.LedControl();
            this._module16 = new BEMN.Forms.LedControl();
            this._module64 = new BEMN.Forms.LedControl();
            this._module76 = new BEMN.Forms.LedControl();
            this._module46 = new BEMN.Forms.LedControl();
            this._module63 = new BEMN.Forms.LedControl();
            this._module15 = new BEMN.Forms.LedControl();
            this._module5 = new BEMN.Forms.LedControl();
            this._module54 = new BEMN.Forms.LedControl();
            this._module34 = new BEMN.Forms.LedControl();
            this._module60 = new BEMN.Forms.LedControl();
            this._module72 = new BEMN.Forms.LedControl();
            this._module65 = new BEMN.Forms.LedControl();
            this._module8 = new BEMN.Forms.LedControl();
            this._module77 = new BEMN.Forms.LedControl();
            this._module14 = new BEMN.Forms.LedControl();
            this._module53 = new BEMN.Forms.LedControl();
            this._module79 = new BEMN.Forms.LedControl();
            this._module61 = new BEMN.Forms.LedControl();
            this._module73 = new BEMN.Forms.LedControl();
            this._module67 = new BEMN.Forms.LedControl();
            this._module66 = new BEMN.Forms.LedControl();
            this._module13 = new BEMN.Forms.LedControl();
            this._module78 = new BEMN.Forms.LedControl();
            this._module1 = new BEMN.Forms.LedControl();
            this._module74 = new BEMN.Forms.LedControl();
            this._module50 = new BEMN.Forms.LedControl();
            this._module62 = new BEMN.Forms.LedControl();
            this._module52 = new BEMN.Forms.LedControl();
            this._module7 = new BEMN.Forms.LedControl();
            this._module12 = new BEMN.Forms.LedControl();
            this._module38 = new BEMN.Forms.LedControl();
            this._module45 = new BEMN.Forms.LedControl();
            this._module11 = new BEMN.Forms.LedControl();
            this._module2 = new BEMN.Forms.LedControl();
            this._module6 = new BEMN.Forms.LedControl();
            this._module3 = new BEMN.Forms.LedControl();
            this._module33 = new BEMN.Forms.LedControl();
            this._module4 = new BEMN.Forms.LedControl();
            this._module19 = new BEMN.Forms.LedControl();
            this._module29 = new BEMN.Forms.LedControl();
            this._module28 = new BEMN.Forms.LedControl();
            this._module27 = new BEMN.Forms.LedControl();
            this._module30 = new BEMN.Forms.LedControl();
            this._module49 = new BEMN.Forms.LedControl();
            this._module26 = new BEMN.Forms.LedControl();
            this._module37 = new BEMN.Forms.LedControl();
            this._module25 = new BEMN.Forms.LedControl();
            this._module44 = new BEMN.Forms.LedControl();
            this._module24 = new BEMN.Forms.LedControl();
            this._module23 = new BEMN.Forms.LedControl();
            this._module22 = new BEMN.Forms.LedControl();
            this._module21 = new BEMN.Forms.LedControl();
            this._module20 = new BEMN.Forms.LedControl();
            this._module32 = new BEMN.Forms.LedControl();
            this._module31 = new BEMN.Forms.LedControl();
            this._module35 = new BEMN.Forms.LedControl();
            this._module41 = new BEMN.Forms.LedControl();
            this._module48 = new BEMN.Forms.LedControl();
            this._module43 = new BEMN.Forms.LedControl();
            this._module36 = new BEMN.Forms.LedControl();
            this._module47 = new BEMN.Forms.LedControl();
            this._module42 = new BEMN.Forms.LedControl();
            this._releGB = new System.Windows.Forms.GroupBox();
            this._virtualReleGB = new System.Windows.Forms.GroupBox();
            this._discretTabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this._vz16m = new BEMN.Forms.LedControl();
            this.label4 = new System.Windows.Forms.Label();
            this._vz15m = new BEMN.Forms.LedControl();
            this.label15 = new System.Windows.Forms.Label();
            this._vz14m = new BEMN.Forms.LedControl();
            this.label16 = new System.Windows.Forms.Label();
            this._vz13m = new BEMN.Forms.LedControl();
            this.label17 = new System.Windows.Forms.Label();
            this._vz12m = new BEMN.Forms.LedControl();
            this.label18 = new System.Windows.Forms.Label();
            this._vz11m = new BEMN.Forms.LedControl();
            this.label20 = new System.Windows.Forms.Label();
            this._vz10m = new BEMN.Forms.LedControl();
            this.label31 = new System.Windows.Forms.Label();
            this._vz9m = new BEMN.Forms.LedControl();
            this.label33 = new System.Windows.Forms.Label();
            this._vz8m = new BEMN.Forms.LedControl();
            this.label34 = new System.Windows.Forms.Label();
            this._vz7m = new BEMN.Forms.LedControl();
            this.label35 = new System.Windows.Forms.Label();
            this._vz6m = new BEMN.Forms.LedControl();
            this.label36 = new System.Windows.Forms.Label();
            this._vz5m = new BEMN.Forms.LedControl();
            this.label37 = new System.Windows.Forms.Label();
            this._vz4m = new BEMN.Forms.LedControl();
            this.label38 = new System.Windows.Forms.Label();
            this._vz3m = new BEMN.Forms.LedControl();
            this.label39 = new System.Windows.Forms.Label();
            this._vz2m = new BEMN.Forms.LedControl();
            this.label41 = new System.Windows.Forms.Label();
            this._vz1m = new BEMN.Forms.LedControl();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._IdMax1Main = new BEMN.Forms.LedControl();
            this._IdMax2Main = new BEMN.Forms.LedControl();
            this._IdMax2IOMain = new BEMN.Forms.LedControl();
            this._IdMax1IOMain = new BEMN.Forms.LedControl();
            this._IdMax2MgnMain = new BEMN.Forms.LedControl();
            this._IdMax2L = new System.Windows.Forms.Label();
            this._IdMax2IOL = new System.Windows.Forms.Label();
            this._IdMax1L = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._IdMax1IOL = new System.Windows.Forms.Label();
            this.groupBox54 = new System.Windows.Forms.GroupBox();
            this._faultHardware1 = new BEMN.Forms.LedControl();
            this.label372 = new System.Windows.Forms.Label();
            this._faultLogic1 = new BEMN.Forms.LedControl();
            this.label12 = new System.Windows.Forms.Label();
            this._faultSwitchOff1 = new BEMN.Forms.LedControl();
            this.label378 = new System.Windows.Forms.Label();
            this._faultMeasuringF1 = new BEMN.Forms.LedControl();
            this.label375 = new System.Windows.Forms.Label();
            this._faultMeasuringU1 = new BEMN.Forms.LedControl();
            this.label376 = new System.Windows.Forms.Label();
            this._faultSoftware1 = new BEMN.Forms.LedControl();
            this.label377 = new System.Windows.Forms.Label();
            this.BGS_Group = new System.Windows.Forms.GroupBox();
            this._bgs16 = new BEMN.Forms.LedControl();
            this.label352 = new System.Windows.Forms.Label();
            this._bgs15 = new BEMN.Forms.LedControl();
            this.label412 = new System.Windows.Forms.Label();
            this._bgs14 = new BEMN.Forms.LedControl();
            this.label454 = new System.Windows.Forms.Label();
            this._bgs13 = new BEMN.Forms.LedControl();
            this.label457 = new System.Windows.Forms.Label();
            this._bgs12 = new BEMN.Forms.LedControl();
            this.label458 = new System.Windows.Forms.Label();
            this._bgs11 = new BEMN.Forms.LedControl();
            this.label459 = new System.Windows.Forms.Label();
            this._bgs10 = new BEMN.Forms.LedControl();
            this.label460 = new System.Windows.Forms.Label();
            this._bgs9 = new BEMN.Forms.LedControl();
            this.label461 = new System.Windows.Forms.Label();
            this._bgs8 = new BEMN.Forms.LedControl();
            this.label462 = new System.Windows.Forms.Label();
            this._bgs7 = new BEMN.Forms.LedControl();
            this.label463 = new System.Windows.Forms.Label();
            this._bgs6 = new BEMN.Forms.LedControl();
            this.label464 = new System.Windows.Forms.Label();
            this._bgs5 = new BEMN.Forms.LedControl();
            this.label465 = new System.Windows.Forms.Label();
            this._bgs4 = new BEMN.Forms.LedControl();
            this.label466 = new System.Windows.Forms.Label();
            this._bgs3 = new BEMN.Forms.LedControl();
            this.label467 = new System.Windows.Forms.Label();
            this._bgs2 = new BEMN.Forms.LedControl();
            this.label468 = new System.Windows.Forms.Label();
            this._bgs1 = new BEMN.Forms.LedControl();
            this.label469 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this._faultMain = new BEMN.Forms.LedControl();
            this.label210 = new System.Windows.Forms.Label();
            this._faultOffMain = new BEMN.Forms.LedControl();
            this.label21 = new System.Windows.Forms.Label();
            this._accelerationMain = new BEMN.Forms.LedControl();
            this._auto4Led = new System.Windows.Forms.Label();
            this.splGroupBox = new System.Windows.Forms.GroupBox();
            this._ssl48 = new BEMN.Forms.LedControl();
            this._ssl40 = new BEMN.Forms.LedControl();
            this._ssl32 = new BEMN.Forms.LedControl();
            this._sslLabel48 = new System.Windows.Forms.Label();
            this._sslLabel40 = new System.Windows.Forms.Label();
            this.label237 = new System.Windows.Forms.Label();
            this._ssl47 = new BEMN.Forms.LedControl();
            this._ssl39 = new BEMN.Forms.LedControl();
            this._ssl31 = new BEMN.Forms.LedControl();
            this._sslLabel47 = new System.Windows.Forms.Label();
            this._sslLabel39 = new System.Windows.Forms.Label();
            this.label238 = new System.Windows.Forms.Label();
            this._ssl46 = new BEMN.Forms.LedControl();
            this._ssl38 = new BEMN.Forms.LedControl();
            this._ssl30 = new BEMN.Forms.LedControl();
            this._sslLabel46 = new System.Windows.Forms.Label();
            this._sslLabel38 = new System.Windows.Forms.Label();
            this.label239 = new System.Windows.Forms.Label();
            this._ssl45 = new BEMN.Forms.LedControl();
            this._ssl37 = new BEMN.Forms.LedControl();
            this._ssl29 = new BEMN.Forms.LedControl();
            this._sslLabel45 = new System.Windows.Forms.Label();
            this._sslLabel37 = new System.Windows.Forms.Label();
            this.label240 = new System.Windows.Forms.Label();
            this._ssl44 = new BEMN.Forms.LedControl();
            this._ssl36 = new BEMN.Forms.LedControl();
            this._ssl28 = new BEMN.Forms.LedControl();
            this._sslLabel44 = new System.Windows.Forms.Label();
            this._sslLabel36 = new System.Windows.Forms.Label();
            this.label241 = new System.Windows.Forms.Label();
            this._ssl43 = new BEMN.Forms.LedControl();
            this._ssl35 = new BEMN.Forms.LedControl();
            this._ssl27 = new BEMN.Forms.LedControl();
            this._sslLabel43 = new System.Windows.Forms.Label();
            this._sslLabel35 = new System.Windows.Forms.Label();
            this.label242 = new System.Windows.Forms.Label();
            this._ssl42 = new BEMN.Forms.LedControl();
            this._ssl34 = new BEMN.Forms.LedControl();
            this._ssl26 = new BEMN.Forms.LedControl();
            this._sslLabel42 = new System.Windows.Forms.Label();
            this._sslLabel34 = new System.Windows.Forms.Label();
            this.label243 = new System.Windows.Forms.Label();
            this._ssl41 = new BEMN.Forms.LedControl();
            this._sslLabel41 = new System.Windows.Forms.Label();
            this._ssl33 = new BEMN.Forms.LedControl();
            this._sslLabel33 = new System.Windows.Forms.Label();
            this._ssl25 = new BEMN.Forms.LedControl();
            this.label244 = new System.Windows.Forms.Label();
            this._ssl24 = new BEMN.Forms.LedControl();
            this.label245 = new System.Windows.Forms.Label();
            this._ssl23 = new BEMN.Forms.LedControl();
            this.label246 = new System.Windows.Forms.Label();
            this._ssl22 = new BEMN.Forms.LedControl();
            this.label247 = new System.Windows.Forms.Label();
            this._ssl21 = new BEMN.Forms.LedControl();
            this.label248 = new System.Windows.Forms.Label();
            this._ssl20 = new BEMN.Forms.LedControl();
            this.label249 = new System.Windows.Forms.Label();
            this._ssl19 = new BEMN.Forms.LedControl();
            this.label250 = new System.Windows.Forms.Label();
            this._ssl18 = new BEMN.Forms.LedControl();
            this.label251 = new System.Windows.Forms.Label();
            this._ssl17 = new BEMN.Forms.LedControl();
            this.label252 = new System.Windows.Forms.Label();
            this._ssl16 = new BEMN.Forms.LedControl();
            this.label253 = new System.Windows.Forms.Label();
            this._ssl15 = new BEMN.Forms.LedControl();
            this.label254 = new System.Windows.Forms.Label();
            this._ssl14 = new BEMN.Forms.LedControl();
            this.label255 = new System.Windows.Forms.Label();
            this._ssl13 = new BEMN.Forms.LedControl();
            this.label256 = new System.Windows.Forms.Label();
            this._ssl12 = new BEMN.Forms.LedControl();
            this.label257 = new System.Windows.Forms.Label();
            this._ssl11 = new BEMN.Forms.LedControl();
            this.label258 = new System.Windows.Forms.Label();
            this._ssl10 = new BEMN.Forms.LedControl();
            this.label259 = new System.Windows.Forms.Label();
            this._ssl9 = new BEMN.Forms.LedControl();
            this.label260 = new System.Windows.Forms.Label();
            this._ssl8 = new BEMN.Forms.LedControl();
            this.label261 = new System.Windows.Forms.Label();
            this._ssl7 = new BEMN.Forms.LedControl();
            this.label262 = new System.Windows.Forms.Label();
            this._ssl6 = new BEMN.Forms.LedControl();
            this.label263 = new System.Windows.Forms.Label();
            this._ssl5 = new BEMN.Forms.LedControl();
            this.label264 = new System.Windows.Forms.Label();
            this._ssl4 = new BEMN.Forms.LedControl();
            this.label265 = new System.Windows.Forms.Label();
            this._ssl3 = new BEMN.Forms.LedControl();
            this.label266 = new System.Windows.Forms.Label();
            this._ssl2 = new BEMN.Forms.LedControl();
            this.label267 = new System.Windows.Forms.Label();
            this._ssl1 = new BEMN.Forms.LedControl();
            this.label268 = new System.Windows.Forms.Label();
            this.dugGroup = new System.Windows.Forms.GroupBox();
            this.dugPusk = new BEMN.Forms.LedControl();
            this.label446 = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label413 = new System.Windows.Forms.Label();
            this._iS8 = new BEMN.Forms.LedControl();
            this._iS8Io = new BEMN.Forms.LedControl();
            this.label88 = new System.Windows.Forms.Label();
            this._iS7 = new BEMN.Forms.LedControl();
            this._iS7Io = new BEMN.Forms.LedControl();
            this.label229 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this._iS1 = new BEMN.Forms.LedControl();
            this.label85 = new System.Windows.Forms.Label();
            this._iS2 = new BEMN.Forms.LedControl();
            this._iS1Io = new BEMN.Forms.LedControl();
            this.label84 = new System.Windows.Forms.Label();
            this._iS3 = new BEMN.Forms.LedControl();
            this._iS2Io = new BEMN.Forms.LedControl();
            this.label230 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this._iS4 = new BEMN.Forms.LedControl();
            this._iS3Io = new BEMN.Forms.LedControl();
            this.label82 = new System.Windows.Forms.Label();
            this._iS5 = new BEMN.Forms.LedControl();
            this._iS4Io = new BEMN.Forms.LedControl();
            this.label81 = new System.Windows.Forms.Label();
            this._iS6 = new BEMN.Forms.LedControl();
            this._iS5Io = new BEMN.Forms.LedControl();
            this._iS6Io = new BEMN.Forms.LedControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label270 = new System.Windows.Forms.Label();
            this.label269 = new System.Windows.Forms.Label();
            this._i8Io = new BEMN.Forms.LedControl();
            this._i8 = new BEMN.Forms.LedControl();
            this.label87 = new System.Windows.Forms.Label();
            this._i6Io = new BEMN.Forms.LedControl();
            this._i5Io = new BEMN.Forms.LedControl();
            this._i6 = new BEMN.Forms.LedControl();
            this.label89 = new System.Windows.Forms.Label();
            this._i4Io = new BEMN.Forms.LedControl();
            this._i5 = new BEMN.Forms.LedControl();
            this.label90 = new System.Windows.Forms.Label();
            this._i3Io = new BEMN.Forms.LedControl();
            this._i4 = new BEMN.Forms.LedControl();
            this.label91 = new System.Windows.Forms.Label();
            this._i2Io = new BEMN.Forms.LedControl();
            this._i3 = new BEMN.Forms.LedControl();
            this.label92 = new System.Windows.Forms.Label();
            this._i1Io = new BEMN.Forms.LedControl();
            this._i2 = new BEMN.Forms.LedControl();
            this.label93 = new System.Windows.Forms.Label();
            this._i1 = new BEMN.Forms.LedControl();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._vls16 = new BEMN.Forms.LedControl();
            this.label63 = new System.Windows.Forms.Label();
            this._vls15 = new BEMN.Forms.LedControl();
            this.label64 = new System.Windows.Forms.Label();
            this._vls14 = new BEMN.Forms.LedControl();
            this.label65 = new System.Windows.Forms.Label();
            this._vls13 = new BEMN.Forms.LedControl();
            this.label66 = new System.Windows.Forms.Label();
            this._vls12 = new BEMN.Forms.LedControl();
            this.label67 = new System.Windows.Forms.Label();
            this._vls11 = new BEMN.Forms.LedControl();
            this.label68 = new System.Windows.Forms.Label();
            this._vls10 = new BEMN.Forms.LedControl();
            this.label69 = new System.Windows.Forms.Label();
            this._vls9 = new BEMN.Forms.LedControl();
            this.label70 = new System.Windows.Forms.Label();
            this._vls8 = new BEMN.Forms.LedControl();
            this.label71 = new System.Windows.Forms.Label();
            this._vls7 = new BEMN.Forms.LedControl();
            this.label72 = new System.Windows.Forms.Label();
            this._vls6 = new BEMN.Forms.LedControl();
            this.label73 = new System.Windows.Forms.Label();
            this._vls5 = new BEMN.Forms.LedControl();
            this.label74 = new System.Windows.Forms.Label();
            this._vls4 = new BEMN.Forms.LedControl();
            this.label75 = new System.Windows.Forms.Label();
            this._vls3 = new BEMN.Forms.LedControl();
            this.label76 = new System.Windows.Forms.Label();
            this._vls2 = new BEMN.Forms.LedControl();
            this.label77 = new System.Windows.Forms.Label();
            this._vls1 = new BEMN.Forms.LedControl();
            this.label78 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._ls16 = new BEMN.Forms.LedControl();
            this.label47 = new System.Windows.Forms.Label();
            this._ls15 = new BEMN.Forms.LedControl();
            this.label48 = new System.Windows.Forms.Label();
            this._ls14 = new BEMN.Forms.LedControl();
            this.label49 = new System.Windows.Forms.Label();
            this._ls13 = new BEMN.Forms.LedControl();
            this.label50 = new System.Windows.Forms.Label();
            this._ls12 = new BEMN.Forms.LedControl();
            this.label51 = new System.Windows.Forms.Label();
            this._ls11 = new BEMN.Forms.LedControl();
            this.label52 = new System.Windows.Forms.Label();
            this._ls10 = new BEMN.Forms.LedControl();
            this.label53 = new System.Windows.Forms.Label();
            this._ls9 = new BEMN.Forms.LedControl();
            this.label54 = new System.Windows.Forms.Label();
            this._ls8 = new BEMN.Forms.LedControl();
            this.label55 = new System.Windows.Forms.Label();
            this._ls7 = new BEMN.Forms.LedControl();
            this.label56 = new System.Windows.Forms.Label();
            this._ls6 = new BEMN.Forms.LedControl();
            this.label57 = new System.Windows.Forms.Label();
            this._ls5 = new BEMN.Forms.LedControl();
            this.label58 = new System.Windows.Forms.Label();
            this._ls4 = new BEMN.Forms.LedControl();
            this.label59 = new System.Windows.Forms.Label();
            this._ls3 = new BEMN.Forms.LedControl();
            this.label60 = new System.Windows.Forms.Label();
            this._ls2 = new BEMN.Forms.LedControl();
            this.label61 = new System.Windows.Forms.Label();
            this._ls1 = new BEMN.Forms.LedControl();
            this.label62 = new System.Windows.Forms.Label();
            this._discretTabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this._Id0Max2 = new BEMN.Forms.LedControl();
            this._Id0Max2IO = new BEMN.Forms.LedControl();
            this._Id0Max3 = new BEMN.Forms.LedControl();
            this._Id0Max3IO = new BEMN.Forms.LedControl();
            this._Id0Max1 = new BEMN.Forms.LedControl();
            this._Id0Max1IO = new BEMN.Forms.LedControl();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._IdMax1 = new BEMN.Forms.LedControl();
            this._IdMax2 = new BEMN.Forms.LedControl();
            this._IdMax2IO = new BEMN.Forms.LedControl();
            this._IdMax1IO = new BEMN.Forms.LedControl();
            this._IdMax2Mgn = new BEMN.Forms.LedControl();
            this.label46 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._fault = new BEMN.Forms.LedControl();
            this.label43 = new System.Windows.Forms.Label();
            this._faultOff = new BEMN.Forms.LedControl();
            this.label44 = new System.Windows.Forms.Label();
            this._acceleration = new BEMN.Forms.LedControl();
            this.label45 = new System.Windows.Forms.Label();
            this.reversGroup1 = new System.Windows.Forms.GroupBox();
            this.label450 = new System.Windows.Forms.Label();
            this.label451 = new System.Windows.Forms.Label();
            this._P2Io1 = new BEMN.Forms.LedControl();
            this._P1Io1 = new BEMN.Forms.LedControl();
            this._P21 = new BEMN.Forms.LedControl();
            this.label452 = new System.Windows.Forms.Label();
            this._P11 = new BEMN.Forms.LedControl();
            this.label453 = new System.Windows.Forms.Label();
            this.dugGroup1 = new System.Windows.Forms.GroupBox();
            this.dugPusk1 = new BEMN.Forms.LedControl();
            this.label447 = new System.Windows.Forms.Label();
            this.groupBox66 = new System.Windows.Forms.GroupBox();
            this.dvPusk1 = new BEMN.Forms.LedControl();
            this.label442 = new System.Windows.Forms.Label();
            this.dvBlockN1 = new BEMN.Forms.LedControl();
            this.dvBlockQ1 = new BEMN.Forms.LedControl();
            this.label443 = new System.Windows.Forms.Label();
            this.label444 = new System.Windows.Forms.Label();
            this.dvWork1 = new BEMN.Forms.LedControl();
            this.label445 = new System.Windows.Forms.Label();
            this.groupBox64 = new System.Windows.Forms.GroupBox();
            this._avrOn1 = new BEMN.Forms.LedControl();
            this.label385 = new System.Windows.Forms.Label();
            this._avrBlock1 = new BEMN.Forms.LedControl();
            this.label386 = new System.Windows.Forms.Label();
            this._avrOff1 = new BEMN.Forms.LedControl();
            this.label387 = new System.Windows.Forms.Label();
            this.urovGroup1 = new System.Windows.Forms.GroupBox();
            this.urov1Led1 = new BEMN.Forms.LedControl();
            this.label417 = new System.Windows.Forms.Label();
            this.blockUrovLed1 = new BEMN.Forms.LedControl();
            this.label418 = new System.Windows.Forms.Label();
            this.urov2Led1 = new BEMN.Forms.LedControl();
            this.label419 = new System.Windows.Forms.Label();
            this.groupBox61 = new System.Windows.Forms.GroupBox();
            this._OnKsAndYppn1 = new BEMN.Forms.LedControl();
            this.label423 = new System.Windows.Forms.Label();
            this._UnoUno1 = new BEMN.Forms.LedControl();
            this.label424 = new System.Windows.Forms.Label();
            this._UyUno1 = new BEMN.Forms.LedControl();
            this.label425 = new System.Windows.Forms.Label();
            this._US1 = new BEMN.Forms.LedControl();
            this._U1noU2y1 = new BEMN.Forms.LedControl();
            this.label426 = new System.Windows.Forms.Label();
            this.label427 = new System.Windows.Forms.Label();
            this.label428 = new System.Windows.Forms.Label();
            this._OS1 = new BEMN.Forms.LedControl();
            this.label429 = new System.Windows.Forms.Label();
            this._autoSinchr1 = new BEMN.Forms.LedControl();
            this.groupBox62 = new System.Windows.Forms.GroupBox();
            this._readyApv1 = new BEMN.Forms.LedControl();
            this.label430 = new System.Windows.Forms.Label();
            this._blockApv1 = new BEMN.Forms.LedControl();
            this.label431 = new System.Windows.Forms.Label();
            this.label432 = new System.Windows.Forms.Label();
            this._puskApv1 = new BEMN.Forms.LedControl();
            this._krat41 = new BEMN.Forms.LedControl();
            this.label433 = new System.Windows.Forms.Label();
            this.label434 = new System.Windows.Forms.Label();
            this._turnOnApv1 = new BEMN.Forms.LedControl();
            this._krat31 = new BEMN.Forms.LedControl();
            this.label435 = new System.Windows.Forms.Label();
            this.label436 = new System.Windows.Forms.Label();
            this._krat11 = new BEMN.Forms.LedControl();
            this._zapretApv1 = new BEMN.Forms.LedControl();
            this.label437 = new System.Windows.Forms.Label();
            this._krat21 = new BEMN.Forms.LedControl();
            this.label438 = new System.Windows.Forms.Label();
            this.groupBox58 = new System.Windows.Forms.GroupBox();
            this._damageA1 = new BEMN.Forms.LedControl();
            this.label409 = new System.Windows.Forms.Label();
            this.label410 = new System.Windows.Forms.Label();
            this._damageB1 = new BEMN.Forms.LedControl();
            this._damageC1 = new BEMN.Forms.LedControl();
            this.label411 = new System.Windows.Forms.Label();
            this.groupBox59 = new System.Windows.Forms.GroupBox();
            this._externalTnUn1 = new BEMN.Forms.LedControl();
            this._externalTnUn1Label = new System.Windows.Forms.Label();
            this._tnObivFaz = new BEMN.Forms.LedControl();
            this.l3 = new System.Windows.Forms.Label();
            this._faultTn3U0 = new BEMN.Forms.LedControl();
            this.l4 = new System.Windows.Forms.Label();
            this._externalTnUn = new BEMN.Forms.LedControl();
            this._faultTnU2 = new BEMN.Forms.LedControl();
            this.l5 = new System.Windows.Forms.Label();
            this.label416 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this._faultTNmgn1 = new BEMN.Forms.LedControl();
            this.l6 = new System.Windows.Forms.Label();
            this._externalTn = new BEMN.Forms.LedControl();
            this.l2 = new System.Windows.Forms.Label();
            this._faultTN1 = new BEMN.Forms.LedControl();
            this.groupBox60 = new System.Windows.Forms.GroupBox();
            this._kachanie1 = new BEMN.Forms.LedControl();
            this.label420 = new System.Windows.Forms.Label();
            this._inZone1 = new BEMN.Forms.LedControl();
            this.label421 = new System.Windows.Forms.Label();
            this._outZone1 = new BEMN.Forms.LedControl();
            this.label422 = new System.Windows.Forms.Label();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this.label379 = new System.Windows.Forms.Label();
            this.label380 = new System.Windows.Forms.Label();
            this.label381 = new System.Windows.Forms.Label();
            this.label382 = new System.Windows.Forms.Label();
            this._r4IoLed1 = new BEMN.Forms.LedControl();
            this._r5Led1 = new BEMN.Forms.LedControl();
            this._r1Led1 = new BEMN.Forms.LedControl();
            this._r3IoLed1 = new BEMN.Forms.LedControl();
            this.label383 = new System.Windows.Forms.Label();
            this.label384 = new System.Windows.Forms.Label();
            this._r6IoLed1 = new BEMN.Forms.LedControl();
            this._r2IoLed1 = new BEMN.Forms.LedControl();
            this._r6Led1 = new BEMN.Forms.LedControl();
            this._r2Led1 = new BEMN.Forms.LedControl();
            this._r4Led1 = new BEMN.Forms.LedControl();
            this.label388 = new System.Windows.Forms.Label();
            this.label390 = new System.Windows.Forms.Label();
            this._r3Led1 = new BEMN.Forms.LedControl();
            this._r5IoLed1 = new BEMN.Forms.LedControl();
            this._r1IoLed1 = new BEMN.Forms.LedControl();
            this.groupBox56 = new System.Windows.Forms.GroupBox();
            this.label414 = new System.Windows.Forms.Label();
            this._iS81 = new BEMN.Forms.LedControl();
            this._iS8Io1 = new BEMN.Forms.LedControl();
            this.label402 = new System.Windows.Forms.Label();
            this._iS71 = new BEMN.Forms.LedControl();
            this._iS7Io1 = new BEMN.Forms.LedControl();
            this.label391 = new System.Windows.Forms.Label();
            this.label392 = new System.Windows.Forms.Label();
            this._iS11 = new BEMN.Forms.LedControl();
            this.label393 = new System.Windows.Forms.Label();
            this._iS21 = new BEMN.Forms.LedControl();
            this._iS1Io1 = new BEMN.Forms.LedControl();
            this.label394 = new System.Windows.Forms.Label();
            this._iS31 = new BEMN.Forms.LedControl();
            this._iS2Io1 = new BEMN.Forms.LedControl();
            this.label395 = new System.Windows.Forms.Label();
            this.label396 = new System.Windows.Forms.Label();
            this._iS41 = new BEMN.Forms.LedControl();
            this._iS3Io1 = new BEMN.Forms.LedControl();
            this.label397 = new System.Windows.Forms.Label();
            this._iS51 = new BEMN.Forms.LedControl();
            this._iS4Io1 = new BEMN.Forms.LedControl();
            this.label398 = new System.Windows.Forms.Label();
            this._iS61 = new BEMN.Forms.LedControl();
            this._iS5Io1 = new BEMN.Forms.LedControl();
            this._iS6Io1 = new BEMN.Forms.LedControl();
            this.groupBox57 = new System.Windows.Forms.GroupBox();
            this.label399 = new System.Windows.Forms.Label();
            this.label400 = new System.Windows.Forms.Label();
            this._i8Io1 = new BEMN.Forms.LedControl();
            this._i6Io1 = new BEMN.Forms.LedControl();
            this._i81 = new BEMN.Forms.LedControl();
            this._i5Io1 = new BEMN.Forms.LedControl();
            this._i61 = new BEMN.Forms.LedControl();
            this.label130 = new System.Windows.Forms.Label();
            this.label403 = new System.Windows.Forms.Label();
            this._i4Io1 = new BEMN.Forms.LedControl();
            this._i51 = new BEMN.Forms.LedControl();
            this.label404 = new System.Windows.Forms.Label();
            this._i3Io1 = new BEMN.Forms.LedControl();
            this._i41 = new BEMN.Forms.LedControl();
            this.label405 = new System.Windows.Forms.Label();
            this._i2Io1 = new BEMN.Forms.LedControl();
            this._i31 = new BEMN.Forms.LedControl();
            this.label406 = new System.Windows.Forms.Label();
            this._i1Io1 = new BEMN.Forms.LedControl();
            this._i21 = new BEMN.Forms.LedControl();
            this.label407 = new System.Windows.Forms.Label();
            this._i11 = new BEMN.Forms.LedControl();
            this.label408 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._vz16 = new BEMN.Forms.LedControl();
            this.label111 = new System.Windows.Forms.Label();
            this._vz15 = new BEMN.Forms.LedControl();
            this.label112 = new System.Windows.Forms.Label();
            this._vz14 = new BEMN.Forms.LedControl();
            this.label113 = new System.Windows.Forms.Label();
            this._vz13 = new BEMN.Forms.LedControl();
            this.label114 = new System.Windows.Forms.Label();
            this._vz12 = new BEMN.Forms.LedControl();
            this.label115 = new System.Windows.Forms.Label();
            this._vz11 = new BEMN.Forms.LedControl();
            this.label116 = new System.Windows.Forms.Label();
            this._vz10 = new BEMN.Forms.LedControl();
            this.label117 = new System.Windows.Forms.Label();
            this._vz9 = new BEMN.Forms.LedControl();
            this.label118 = new System.Windows.Forms.Label();
            this._vz8 = new BEMN.Forms.LedControl();
            this.label119 = new System.Windows.Forms.Label();
            this._vz7 = new BEMN.Forms.LedControl();
            this.label120 = new System.Windows.Forms.Label();
            this._vz6 = new BEMN.Forms.LedControl();
            this.label121 = new System.Windows.Forms.Label();
            this._vz5 = new BEMN.Forms.LedControl();
            this.label122 = new System.Windows.Forms.Label();
            this._vz4 = new BEMN.Forms.LedControl();
            this.label123 = new System.Windows.Forms.Label();
            this._vz3 = new BEMN.Forms.LedControl();
            this.label124 = new System.Windows.Forms.Label();
            this._vz2 = new BEMN.Forms.LedControl();
            this.label125 = new System.Windows.Forms.Label();
            this._vz1 = new BEMN.Forms.LedControl();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this._fSpl1 = new BEMN.Forms.LedControl();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this._fSpl2 = new BEMN.Forms.LedControl();
            this.label317 = new System.Windows.Forms.Label();
            this._fSpl3 = new BEMN.Forms.LedControl();
            this._fSpl4 = new BEMN.Forms.LedControl();
            this.label320 = new System.Windows.Forms.Label();
            this.label319 = new System.Windows.Forms.Label();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.label347 = new System.Windows.Forms.Label();
            this._faultDisable2 = new BEMN.Forms.LedControl();
            this.label346 = new System.Windows.Forms.Label();
            this.label345 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label220 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this._faultDisable1 = new BEMN.Forms.LedControl();
            this._faultSwithON = new BEMN.Forms.LedControl();
            this._faultManage = new BEMN.Forms.LedControl();
            this._faultOtkaz = new BEMN.Forms.LedControl();
            this._faultBlockCon = new BEMN.Forms.LedControl();
            this._faultOut = new BEMN.Forms.LedControl();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.label228 = new System.Windows.Forms.Label();
            this._UabcLow10 = new BEMN.Forms.LedControl();
            this.label313 = new System.Windows.Forms.Label();
            this._freqLow40 = new BEMN.Forms.LedControl();
            this._freqHiger60 = new BEMN.Forms.LedControl();
            this.label312 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._q2Led = new BEMN.Forms.LedControl();
            this.label102 = new System.Windows.Forms.Label();
            this._q1Led = new BEMN.Forms.LedControl();
            this.label104 = new System.Windows.Forms.Label();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label301 = new System.Windows.Forms.Label();
            this.label302 = new System.Windows.Forms.Label();
            this._f4IoLessLed = new BEMN.Forms.LedControl();
            this._f3IoLessLed = new BEMN.Forms.LedControl();
            this._f2IoLessLed = new BEMN.Forms.LedControl();
            this._f4LessLed = new BEMN.Forms.LedControl();
            this.label303 = new System.Windows.Forms.Label();
            this._f1IoLessLed = new BEMN.Forms.LedControl();
            this._f3LessLed = new BEMN.Forms.LedControl();
            this.label304 = new System.Windows.Forms.Label();
            this._f2LessLed = new BEMN.Forms.LedControl();
            this.label305 = new System.Windows.Forms.Label();
            this._f1LessLed = new BEMN.Forms.LedControl();
            this.label306 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label295 = new System.Windows.Forms.Label();
            this.label296 = new System.Windows.Forms.Label();
            this._f4IoMoreLed = new BEMN.Forms.LedControl();
            this._f3IoMoreLed = new BEMN.Forms.LedControl();
            this._f2IoMoreLed = new BEMN.Forms.LedControl();
            this._f4MoreLed = new BEMN.Forms.LedControl();
            this.label297 = new System.Windows.Forms.Label();
            this._f1IoMoreLed = new BEMN.Forms.LedControl();
            this._f3MoreLed = new BEMN.Forms.LedControl();
            this.label298 = new System.Windows.Forms.Label();
            this._f2MoreLed = new BEMN.Forms.LedControl();
            this.label299 = new System.Windows.Forms.Label();
            this._f1MoreLed = new BEMN.Forms.LedControl();
            this.label300 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label289 = new System.Windows.Forms.Label();
            this.label290 = new System.Windows.Forms.Label();
            this._u4IoLessLed = new BEMN.Forms.LedControl();
            this._u3IoLessLed = new BEMN.Forms.LedControl();
            this._u2IoLessLed = new BEMN.Forms.LedControl();
            this._u4LessLed = new BEMN.Forms.LedControl();
            this.label291 = new System.Windows.Forms.Label();
            this._u1IoLessLed = new BEMN.Forms.LedControl();
            this._u3LessLed = new BEMN.Forms.LedControl();
            this.label292 = new System.Windows.Forms.Label();
            this._u2LessLed = new BEMN.Forms.LedControl();
            this.label293 = new System.Windows.Forms.Label();
            this._u1LessLed = new BEMN.Forms.LedControl();
            this.label294 = new System.Windows.Forms.Label();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label283 = new System.Windows.Forms.Label();
            this.label284 = new System.Windows.Forms.Label();
            this._u4IoMoreLed = new BEMN.Forms.LedControl();
            this._u3IoMoreLed = new BEMN.Forms.LedControl();
            this._u2IoMoreLed = new BEMN.Forms.LedControl();
            this._u4MoreLed = new BEMN.Forms.LedControl();
            this.label285 = new System.Windows.Forms.Label();
            this._u1IoMoreLed = new BEMN.Forms.LedControl();
            this._u3MoreLed = new BEMN.Forms.LedControl();
            this.label286 = new System.Windows.Forms.Label();
            this._u2MoreLed = new BEMN.Forms.LedControl();
            this.label287 = new System.Windows.Forms.Label();
            this._u1MoreLed = new BEMN.Forms.LedControl();
            this.label288 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this._faultModule6 = new BEMN.Forms.LedControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label344 = new System.Windows.Forms.Label();
            this._faultSwitchOff = new BEMN.Forms.LedControl();
            this._faultMeasuringf = new BEMN.Forms.LedControl();
            this.label96 = new System.Windows.Forms.Label();
            this._faultAlarmJournal = new BEMN.Forms.LedControl();
            this.label192 = new System.Windows.Forms.Label();
            this._faultOsc = new BEMN.Forms.LedControl();
            this.label193 = new System.Windows.Forms.Label();
            this._faultModule5 = new BEMN.Forms.LedControl();
            this._faultSystemJournal = new BEMN.Forms.LedControl();
            this.label200 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this._faultGroupsOfSetpoints = new BEMN.Forms.LedControl();
            this.label201 = new System.Windows.Forms.Label();
            this._faultModule4 = new BEMN.Forms.LedControl();
            this._faultLogic = new BEMN.Forms.LedControl();
            this.label79 = new System.Windows.Forms.Label();
            this._faultSetpoints = new BEMN.Forms.LedControl();
            this.label202 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this._faultPass = new BEMN.Forms.LedControl();
            this.label203 = new System.Windows.Forms.Label();
            this._faultModule3 = new BEMN.Forms.LedControl();
            this._faultMeasuringU = new BEMN.Forms.LedControl();
            this.label204 = new System.Windows.Forms.Label();
            this.label196 = new System.Windows.Forms.Label();
            this._faultSoftware = new BEMN.Forms.LedControl();
            this.label205 = new System.Windows.Forms.Label();
            this._faultModule2 = new BEMN.Forms.LedControl();
            this._faultHardware = new BEMN.Forms.LedControl();
            this.label206 = new System.Windows.Forms.Label();
            this.label197 = new System.Windows.Forms.Label();
            this._faultModule1 = new BEMN.Forms.LedControl();
            this.label198 = new System.Windows.Forms.Label();
            this._controlSignalsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._commandBtn = new System.Windows.Forms.Button();
            this._commandComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._reserveGroupButton = new System.Windows.Forms.Button();
            this._mainGroupButton = new System.Windows.Forms.Button();
            this.label490 = new System.Windows.Forms.Label();
            this.label491 = new System.Windows.Forms.Label();
            this._reservedGroup = new BEMN.Forms.LedControl();
            this._mainGroup = new BEMN.Forms.LedControl();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this._currenGroupLabel = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._groupCombo = new System.Windows.Forms.ComboBox();
            this._switchGroupButton = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.ResetSamopodBtn = new System.Windows.Forms.Button();
            this.ResetInd = new System.Windows.Forms.Button();
            this._breakerOnBut = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this._startOscBtn = new System.Windows.Forms.Button();
            this._breakerOffBut = new System.Windows.Forms.Button();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._logicState = new BEMN.Forms.LedControl();
            this.stopLogic = new System.Windows.Forms.Button();
            this.startLogic = new System.Windows.Forms.Button();
            this.label314 = new System.Windows.Forms.Label();
            this._switchOn = new BEMN.Forms.LedControl();
            this._availabilityFaultSystemJournal = new BEMN.Forms.LedControl();
            this.label100 = new System.Windows.Forms.Label();
            this._newRecordOscJournal = new BEMN.Forms.LedControl();
            this._switchOff = new BEMN.Forms.LedControl();
            this._newRecordAlarmJournal = new BEMN.Forms.LedControl();
            this._newRecordSystemJournal = new BEMN.Forms.LedControl();
            this._resetAvailabilityFaultSystemJournalButton = new System.Windows.Forms.Button();
            this._resetOscJournalButton = new System.Windows.Forms.Button();
            this._resetAlarmJournalButton = new System.Windows.Forms.Button();
            this._resetSystemJournalButton = new System.Windows.Forms.Button();
            this.label227 = new System.Windows.Forms.Label();
            this.label225 = new System.Windows.Forms.Label();
            this.label226 = new System.Windows.Forms.Label();
            this.label231 = new System.Windows.Forms.Label();
            this._analogTabPage.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox67.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox45.SuspendLayout();
            this.groupBox46.SuspendLayout();
            this._dataBaseTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this._rsTriggersGB.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this._diskretInputGB.SuspendLayout();
            this._discretTabPage1.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox54.SuspendLayout();
            this.BGS_Group.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.splGroupBox.SuspendLayout();
            this.dugGroup.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._discretTabPage2.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.reversGroup1.SuspendLayout();
            this.dugGroup1.SuspendLayout();
            this.groupBox66.SuspendLayout();
            this.groupBox64.SuspendLayout();
            this.urovGroup1.SuspendLayout();
            this.groupBox61.SuspendLayout();
            this.groupBox62.SuspendLayout();
            this.groupBox58.SuspendLayout();
            this.groupBox59.SuspendLayout();
            this.groupBox60.SuspendLayout();
            this.groupBox55.SuspendLayout();
            this.groupBox56.SuspendLayout();
            this.groupBox57.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this._controlSignalsTabPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.SuspendLayout();
            // 
            // _analogTabPage
            // 
            this._analogTabPage.Controls.Add(this.groupBox23);
            this._analogTabPage.Controls.Add(this._dateTimeControl);
            this._analogTabPage.Controls.Add(this.groupBox1);
            this._analogTabPage.Controls.Add(this.groupBox67);
            this._analogTabPage.Controls.Add(this.groupBox44);
            this._analogTabPage.Controls.Add(this.groupBox42);
            this._analogTabPage.Controls.Add(this.groupBox41);
            this._analogTabPage.Controls.Add(this.groupBox37);
            this._analogTabPage.Controls.Add(this.groupBox36);
            this._analogTabPage.Controls.Add(this.groupBox);
            this._analogTabPage.Controls.Add(this.groupBox21);
            this._analogTabPage.Controls.Add(this.groupBox25);
            this._analogTabPage.Controls.Add(this.groupBox47);
            this._analogTabPage.Controls.Add(this.groupBox2);
            this._analogTabPage.Controls.Add(this.groupBox7);
            this._analogTabPage.Controls.Add(this.groupBox13);
            this._analogTabPage.Location = new System.Drawing.Point(4, 22);
            this._analogTabPage.Name = "_analogTabPage";
            this._analogTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._analogTabPage.Size = new System.Drawing.Size(911, 599);
            this._analogTabPage.TabIndex = 0;
            this._analogTabPage.Text = "Аналоговая БД";
            this._analogTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this._dU);
            this.groupBox23.Controls.Add(this.label28);
            this.groupBox23.Controls.Add(this._dFi);
            this.groupBox23.Controls.Add(this._dF);
            this.groupBox23.Controls.Add(this.label29);
            this.groupBox23.Controls.Add(this.label30);
            this.groupBox23.Location = new System.Drawing.Point(438, 220);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(264, 100);
            this.groupBox23.TabIndex = 64;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Контроль синхронизма";
            // 
            // _dU
            // 
            this._dU.Location = new System.Drawing.Point(171, 18);
            this._dU.Name = "_dU";
            this._dU.ReadOnly = true;
            this._dU.Size = new System.Drawing.Size(84, 20);
            this._dU.TabIndex = 9;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 74);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 13);
            this.label28.TabIndex = 22;
            this.label28.Text = "Разность частот dF";
            // 
            // _dFi
            // 
            this._dFi.Location = new System.Drawing.Point(171, 44);
            this._dFi.Name = "_dFi";
            this._dFi.ReadOnly = true;
            this._dFi.Size = new System.Drawing.Size(84, 20);
            this._dFi.TabIndex = 11;
            // 
            // _dF
            // 
            this._dF.Location = new System.Drawing.Point(171, 70);
            this._dF.Name = "_dF";
            this._dF.ReadOnly = true;
            this._dF.Size = new System.Drawing.Size(84, 20);
            this._dF.TabIndex = 13;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 48);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(161, 13);
            this.label29.TabIndex = 20;
            this.label29.Text = "Разность фазовых сдвигов dfi";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(137, 13);
            this.label30.TabIndex = 18;
            this.label30.Text = "Разность напряжений dU";
            // 
            // _dateTimeControl
            // 
            this._dateTimeControl.DateTime = dateTimeStruct1;
            this._dateTimeControl.Location = new System.Drawing.Point(705, 7);
            this._dateTimeControl.Name = "_dateTimeControl";
            this._dateTimeControl.Size = new System.Drawing.Size(201, 166);
            this._dateTimeControl.TabIndex = 43;
            this._dateTimeControl.TimeChanged += new System.Action(this.dateTimeControl_TimeChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._voltageTB);
            this.groupBox1.Location = new System.Drawing.Point(325, 498);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(107, 41);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Напряжение";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Un";
            // 
            // _voltageTB
            // 
            this._voltageTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._voltageTB.Location = new System.Drawing.Point(37, 12);
            this._voltageTB.Name = "_voltageTB";
            this._voltageTB.ReadOnly = true;
            this._voltageTB.Size = new System.Drawing.Size(61, 20);
            this._voltageTB.TabIndex = 57;
            // 
            // groupBox67
            // 
            this.groupBox67.Controls.Add(this.label10);
            this.groupBox67.Controls.Add(this.label504);
            this.groupBox67.Controls.Add(this._U2abc0);
            this.groupBox67.Controls.Add(this._U2abcC);
            this.groupBox67.Controls.Add(this.label9);
            this.groupBox67.Controls.Add(this.label502);
            this.groupBox67.Controls.Add(this._U2abc2);
            this.groupBox67.Controls.Add(this._U2abcB);
            this.groupBox67.Controls.Add(this._U2abc1);
            this.groupBox67.Controls.Add(this._U2abcA);
            this.groupBox67.Controls.Add(this._U2ca);
            this.groupBox67.Controls.Add(this.label8);
            this.groupBox67.Controls.Add(this.label500);
            this.groupBox67.Controls.Add(this._U2bc);
            this.groupBox67.Controls.Add(this._U2ab);
            this.groupBox67.Controls.Add(this.label498);
            this.groupBox67.Controls.Add(this.label496);
            this.groupBox67.Controls.Add(this.label494);
            this.groupBox67.Location = new System.Drawing.Point(8, 450);
            this.groupBox67.Name = "groupBox67";
            this.groupBox67.Size = new System.Drawing.Size(311, 89);
            this.groupBox67.TabIndex = 63;
            this.groupBox67.TabStop = false;
            this.groupBox67.Text = "Напряжение abc2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(202, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "3U0";
            // 
            // label504
            // 
            this.label504.AutoSize = true;
            this.label504.Location = new System.Drawing.Point(6, 61);
            this.label504.Name = "label504";
            this.label504.Size = new System.Drawing.Size(21, 13);
            this.label504.TabIndex = 39;
            this.label504.Text = "Uc";
            // 
            // _U2abc0
            // 
            this._U2abc0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abc0.Location = new System.Drawing.Point(231, 59);
            this._U2abc0.Name = "_U2abc0";
            this._U2abc0.ReadOnly = true;
            this._U2abc0.Size = new System.Drawing.Size(68, 20);
            this._U2abc0.TabIndex = 38;
            // 
            // _U2abcC
            // 
            this._U2abcC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abcC.Location = new System.Drawing.Point(28, 60);
            this._U2abcC.Name = "_U2abcC";
            this._U2abcC.ReadOnly = true;
            this._U2abcC.Size = new System.Drawing.Size(68, 20);
            this._U2abcC.TabIndex = 38;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(202, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "U2";
            // 
            // label502
            // 
            this.label502.AutoSize = true;
            this.label502.Location = new System.Drawing.Point(6, 39);
            this.label502.Name = "label502";
            this.label502.Size = new System.Drawing.Size(21, 13);
            this.label502.TabIndex = 39;
            this.label502.Text = "Ub";
            // 
            // _U2abc2
            // 
            this._U2abc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abc2.Location = new System.Drawing.Point(231, 36);
            this._U2abc2.Name = "_U2abc2";
            this._U2abc2.ReadOnly = true;
            this._U2abc2.Size = new System.Drawing.Size(68, 20);
            this._U2abc2.TabIndex = 38;
            // 
            // _U2abcB
            // 
            this._U2abcB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abcB.Location = new System.Drawing.Point(28, 38);
            this._U2abcB.Name = "_U2abcB";
            this._U2abcB.ReadOnly = true;
            this._U2abcB.Size = new System.Drawing.Size(68, 20);
            this._U2abcB.TabIndex = 38;
            // 
            // _U2abc1
            // 
            this._U2abc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abc1.Location = new System.Drawing.Point(231, 13);
            this._U2abc1.Name = "_U2abc1";
            this._U2abc1.ReadOnly = true;
            this._U2abc1.Size = new System.Drawing.Size(68, 20);
            this._U2abc1.TabIndex = 37;
            // 
            // _U2abcA
            // 
            this._U2abcA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2abcA.Location = new System.Drawing.Point(28, 14);
            this._U2abcA.Name = "_U2abcA";
            this._U2abcA.ReadOnly = true;
            this._U2abcA.Size = new System.Drawing.Size(68, 20);
            this._U2abcA.TabIndex = 37;
            // 
            // _U2ca
            // 
            this._U2ca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2ca.Location = new System.Drawing.Point(128, 59);
            this._U2ca.Name = "_U2ca";
            this._U2ca.ReadOnly = true;
            this._U2ca.Size = new System.Drawing.Size(68, 20);
            this._U2ca.TabIndex = 35;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(202, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "U1";
            // 
            // label500
            // 
            this.label500.AutoSize = true;
            this.label500.Location = new System.Drawing.Point(6, 17);
            this.label500.Name = "label500";
            this.label500.Size = new System.Drawing.Size(21, 13);
            this.label500.TabIndex = 32;
            this.label500.Text = "Ua";
            // 
            // _U2bc
            // 
            this._U2bc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2bc.Location = new System.Drawing.Point(128, 36);
            this._U2bc.Name = "_U2bc";
            this._U2bc.ReadOnly = true;
            this._U2bc.Size = new System.Drawing.Size(68, 20);
            this._U2bc.TabIndex = 34;
            // 
            // _U2ab
            // 
            this._U2ab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U2ab.Location = new System.Drawing.Point(128, 13);
            this._U2ab.Name = "_U2ab";
            this._U2ab.ReadOnly = true;
            this._U2ab.Size = new System.Drawing.Size(68, 20);
            this._U2ab.TabIndex = 33;
            // 
            // label498
            // 
            this.label498.AutoSize = true;
            this.label498.Location = new System.Drawing.Point(100, 62);
            this.label498.Name = "label498";
            this.label498.Size = new System.Drawing.Size(27, 13);
            this.label498.TabIndex = 28;
            this.label498.Text = "Uca";
            // 
            // label496
            // 
            this.label496.AutoSize = true;
            this.label496.Location = new System.Drawing.Point(101, 39);
            this.label496.Name = "label496";
            this.label496.Size = new System.Drawing.Size(27, 13);
            this.label496.TabIndex = 26;
            this.label496.Text = "Ubc";
            // 
            // label494
            // 
            this.label494.AutoSize = true;
            this.label494.Location = new System.Drawing.Point(101, 16);
            this.label494.Name = "label494";
            this.label494.Size = new System.Drawing.Size(27, 13);
            this.label494.TabIndex = 17;
            this.label494.Text = "Uab";
            // 
            // groupBox44
            // 
            this.groupBox44.Controls.Add(this.label5);
            this.groupBox44.Controls.Add(this.label6);
            this.groupBox44.Controls.Add(this._U1abc0);
            this.groupBox44.Controls.Add(this._U1abc2);
            this.groupBox44.Controls.Add(this._U1abc1);
            this.groupBox44.Controls.Add(this.label7);
            this.groupBox44.Controls.Add(this.label495);
            this.groupBox44.Controls.Add(this.label497);
            this.groupBox44.Controls.Add(this._U1abcC);
            this.groupBox44.Controls.Add(this._U1abcB);
            this.groupBox44.Controls.Add(this._U1abcA);
            this.groupBox44.Controls.Add(this._U1ca);
            this.groupBox44.Controls.Add(this._U10);
            this.groupBox44.Controls.Add(this._U1ab);
            this.groupBox44.Controls.Add(this._U1bc);
            this.groupBox44.Controls.Add(this.label503);
            this.groupBox44.Controls.Add(this.label501);
            this.groupBox44.Controls.Add(this.label499);
            this.groupBox44.Location = new System.Drawing.Point(8, 355);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(311, 89);
            this.groupBox44.TabIndex = 63;
            this.groupBox44.TabStop = false;
            this.groupBox44.Text = "Напряжение abc1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(202, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "3U0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "U2";
            // 
            // _U1abc0
            // 
            this._U1abc0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abc0.Location = new System.Drawing.Point(231, 60);
            this._U1abc0.Name = "_U1abc0";
            this._U1abc0.ReadOnly = true;
            this._U1abc0.Size = new System.Drawing.Size(68, 20);
            this._U1abc0.TabIndex = 42;
            // 
            // _U1abc2
            // 
            this._U1abc2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abc2.Location = new System.Drawing.Point(231, 37);
            this._U1abc2.Name = "_U1abc2";
            this._U1abc2.ReadOnly = true;
            this._U1abc2.Size = new System.Drawing.Size(68, 20);
            this._U1abc2.TabIndex = 43;
            // 
            // _U1abc1
            // 
            this._U1abc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abc1.Location = new System.Drawing.Point(231, 14);
            this._U1abc1.Name = "_U1abc1";
            this._U1abc1.ReadOnly = true;
            this._U1abc1.Size = new System.Drawing.Size(68, 20);
            this._U1abc1.TabIndex = 41;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(202, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 40;
            this.label7.Text = "U1";
            // 
            // label495
            // 
            this.label495.AutoSize = true;
            this.label495.Location = new System.Drawing.Point(6, 62);
            this.label495.Name = "label495";
            this.label495.Size = new System.Drawing.Size(21, 13);
            this.label495.TabIndex = 39;
            this.label495.Text = "Uc";
            // 
            // label497
            // 
            this.label497.AutoSize = true;
            this.label497.Location = new System.Drawing.Point(6, 40);
            this.label497.Name = "label497";
            this.label497.Size = new System.Drawing.Size(21, 13);
            this.label497.TabIndex = 39;
            this.label497.Text = "Ub";
            // 
            // _U1abcC
            // 
            this._U1abcC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abcC.Location = new System.Drawing.Point(28, 61);
            this._U1abcC.Name = "_U1abcC";
            this._U1abcC.ReadOnly = true;
            this._U1abcC.Size = new System.Drawing.Size(68, 20);
            this._U1abcC.TabIndex = 38;
            // 
            // _U1abcB
            // 
            this._U1abcB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abcB.Location = new System.Drawing.Point(28, 39);
            this._U1abcB.Name = "_U1abcB";
            this._U1abcB.ReadOnly = true;
            this._U1abcB.Size = new System.Drawing.Size(68, 20);
            this._U1abcB.TabIndex = 38;
            // 
            // _U1abcA
            // 
            this._U1abcA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1abcA.Location = new System.Drawing.Point(28, 15);
            this._U1abcA.Name = "_U1abcA";
            this._U1abcA.ReadOnly = true;
            this._U1abcA.Size = new System.Drawing.Size(68, 20);
            this._U1abcA.TabIndex = 37;
            // 
            // _U1ca
            // 
            this._U1ca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1ca.Location = new System.Drawing.Point(128, 60);
            this._U1ca.Name = "_U1ca";
            this._U1ca.ReadOnly = true;
            this._U1ca.Size = new System.Drawing.Size(68, 20);
            this._U1ca.TabIndex = 35;
            // 
            // _U10
            // 
            this._U10.AutoSize = true;
            this._U10.Location = new System.Drawing.Point(6, 18);
            this._U10.Name = "_U10";
            this._U10.Size = new System.Drawing.Size(21, 13);
            this._U10.TabIndex = 32;
            this._U10.Text = "Ua";
            // 
            // _U1ab
            // 
            this._U1ab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1ab.Location = new System.Drawing.Point(128, 14);
            this._U1ab.Name = "_U1ab";
            this._U1ab.ReadOnly = true;
            this._U1ab.Size = new System.Drawing.Size(68, 20);
            this._U1ab.TabIndex = 33;
            // 
            // _U1bc
            // 
            this._U1bc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._U1bc.Location = new System.Drawing.Point(128, 37);
            this._U1bc.Name = "_U1bc";
            this._U1bc.ReadOnly = true;
            this._U1bc.Size = new System.Drawing.Size(68, 20);
            this._U1bc.TabIndex = 34;
            // 
            // label503
            // 
            this.label503.AutoSize = true;
            this.label503.Location = new System.Drawing.Point(101, 17);
            this.label503.Name = "label503";
            this.label503.Size = new System.Drawing.Size(27, 13);
            this.label503.TabIndex = 17;
            this.label503.Text = "Uab";
            // 
            // label501
            // 
            this.label501.AutoSize = true;
            this.label501.Location = new System.Drawing.Point(101, 40);
            this.label501.Name = "label501";
            this.label501.Size = new System.Drawing.Size(27, 13);
            this.label501.TabIndex = 26;
            this.label501.Text = "Ubc";
            // 
            // label499
            // 
            this.label499.AutoSize = true;
            this.label499.Location = new System.Drawing.Point(100, 63);
            this.label499.Name = "label499";
            this.label499.Size = new System.Drawing.Size(27, 13);
            this.label499.TabIndex = 28;
            this.label499.Text = "Uca";
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this.label482);
            this.groupBox42.Controls.Add(this._Is42);
            this.groupBox42.Controls.Add(this._Is4I1);
            this.groupBox42.Controls.Add(this._Is40);
            this.groupBox42.Controls.Add(this._Is4n);
            this.groupBox42.Controls.Add(this._Is4c);
            this.groupBox42.Controls.Add(this._Is4b);
            this.groupBox42.Controls.Add(this.label27);
            this.groupBox42.Controls.Add(this._Is4a);
            this.groupBox42.Controls.Add(this.label483);
            this.groupBox42.Controls.Add(this.label484);
            this.groupBox42.Controls.Add(this.label485);
            this.groupBox42.Controls.Add(this.label486);
            this.groupBox42.Controls.Add(this.label488);
            this.groupBox42.Location = new System.Drawing.Point(223, 235);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(209, 114);
            this.groupBox42.TabIndex = 62;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Ток ст. 4";
            // 
            // label482
            // 
            this.label482.AutoSize = true;
            this.label482.Location = new System.Drawing.Point(102, 41);
            this.label482.Name = "label482";
            this.label482.Size = new System.Drawing.Size(16, 13);
            this.label482.TabIndex = 39;
            this.label482.Text = "I2";
            // 
            // _Is42
            // 
            this._Is42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is42.Location = new System.Drawing.Point(124, 38);
            this._Is42.Name = "_Is42";
            this._Is42.ReadOnly = true;
            this._Is42.Size = new System.Drawing.Size(68, 20);
            this._Is42.TabIndex = 38;
            // 
            // _Is4I1
            // 
            this._Is4I1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is4I1.Location = new System.Drawing.Point(124, 15);
            this._Is4I1.Name = "_Is4I1";
            this._Is4I1.ReadOnly = true;
            this._Is4I1.Size = new System.Drawing.Size(68, 20);
            this._Is4I1.TabIndex = 37;
            // 
            // _Is40
            // 
            this._Is40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is40.Location = new System.Drawing.Point(124, 61);
            this._Is40.Name = "_Is40";
            this._Is40.ReadOnly = true;
            this._Is40.Size = new System.Drawing.Size(68, 20);
            this._Is40.TabIndex = 37;
            // 
            // _Is4n
            // 
            this._Is4n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is4n.Location = new System.Drawing.Point(124, 84);
            this._Is4n.Name = "_Is4n";
            this._Is4n.ReadOnly = true;
            this._Is4n.Size = new System.Drawing.Size(68, 20);
            this._Is4n.TabIndex = 36;
            // 
            // _Is4c
            // 
            this._Is4c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is4c.Location = new System.Drawing.Point(28, 61);
            this._Is4c.Name = "_Is4c";
            this._Is4c.ReadOnly = true;
            this._Is4c.Size = new System.Drawing.Size(68, 20);
            this._Is4c.TabIndex = 35;
            // 
            // _Is4b
            // 
            this._Is4b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is4b.Location = new System.Drawing.Point(28, 38);
            this._Is4b.Name = "_Is4b";
            this._Is4b.ReadOnly = true;
            this._Is4b.Size = new System.Drawing.Size(68, 20);
            this._Is4b.TabIndex = 34;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(102, 18);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(16, 13);
            this.label27.TabIndex = 32;
            this.label27.Text = "I1";
            // 
            // _Is4a
            // 
            this._Is4a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is4a.Location = new System.Drawing.Point(28, 15);
            this._Is4a.Name = "_Is4a";
            this._Is4a.ReadOnly = true;
            this._Is4a.Size = new System.Drawing.Size(68, 20);
            this._Is4a.TabIndex = 33;
            // 
            // label483
            // 
            this.label483.AutoSize = true;
            this.label483.Location = new System.Drawing.Point(101, 64);
            this.label483.Name = "label483";
            this.label483.Size = new System.Drawing.Size(22, 13);
            this.label483.TabIndex = 32;
            this.label483.Text = "3I0";
            // 
            // label484
            // 
            this.label484.AutoSize = true;
            this.label484.Location = new System.Drawing.Point(6, 64);
            this.label484.Name = "label484";
            this.label484.Size = new System.Drawing.Size(16, 13);
            this.label484.TabIndex = 28;
            this.label484.Text = "Ic";
            // 
            // label485
            // 
            this.label485.AutoSize = true;
            this.label485.Location = new System.Drawing.Point(102, 86);
            this.label485.Name = "label485";
            this.label485.Size = new System.Drawing.Size(16, 13);
            this.label485.TabIndex = 30;
            this.label485.Text = "In";
            // 
            // label486
            // 
            this.label486.AutoSize = true;
            this.label486.Location = new System.Drawing.Point(6, 41);
            this.label486.Name = "label486";
            this.label486.Size = new System.Drawing.Size(16, 13);
            this.label486.TabIndex = 26;
            this.label486.Text = "Ib";
            // 
            // label488
            // 
            this.label488.AutoSize = true;
            this.label488.Location = new System.Drawing.Point(6, 18);
            this.label488.Name = "label488";
            this.label488.Size = new System.Drawing.Size(16, 13);
            this.label488.TabIndex = 17;
            this.label488.Text = "Ia";
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this.label470);
            this.groupBox41.Controls.Add(this._Is32);
            this.groupBox41.Controls.Add(this._Is3I1);
            this.groupBox41.Controls.Add(this._Is30);
            this.groupBox41.Controls.Add(this._Is3n);
            this.groupBox41.Controls.Add(this._Is3c);
            this.groupBox41.Controls.Add(this._Is3b);
            this.groupBox41.Controls.Add(this.label26);
            this.groupBox41.Controls.Add(this._Is3a);
            this.groupBox41.Controls.Add(this.label471);
            this.groupBox41.Controls.Add(this.label472);
            this.groupBox41.Controls.Add(this.label473);
            this.groupBox41.Controls.Add(this.label474);
            this.groupBox41.Controls.Add(this.label476);
            this.groupBox41.Location = new System.Drawing.Point(8, 235);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(209, 114);
            this.groupBox41.TabIndex = 61;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "Ток ст. 3";
            // 
            // label470
            // 
            this.label470.AutoSize = true;
            this.label470.Location = new System.Drawing.Point(102, 41);
            this.label470.Name = "label470";
            this.label470.Size = new System.Drawing.Size(16, 13);
            this.label470.TabIndex = 39;
            this.label470.Text = "I2";
            // 
            // _Is32
            // 
            this._Is32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is32.Location = new System.Drawing.Point(124, 38);
            this._Is32.Name = "_Is32";
            this._Is32.ReadOnly = true;
            this._Is32.Size = new System.Drawing.Size(68, 20);
            this._Is32.TabIndex = 38;
            // 
            // _Is3I1
            // 
            this._Is3I1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3I1.Location = new System.Drawing.Point(124, 15);
            this._Is3I1.Name = "_Is3I1";
            this._Is3I1.ReadOnly = true;
            this._Is3I1.Size = new System.Drawing.Size(68, 20);
            this._Is3I1.TabIndex = 37;
            // 
            // _Is30
            // 
            this._Is30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is30.Location = new System.Drawing.Point(124, 61);
            this._Is30.Name = "_Is30";
            this._Is30.ReadOnly = true;
            this._Is30.Size = new System.Drawing.Size(68, 20);
            this._Is30.TabIndex = 37;
            // 
            // _Is3n
            // 
            this._Is3n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3n.Location = new System.Drawing.Point(124, 84);
            this._Is3n.Name = "_Is3n";
            this._Is3n.ReadOnly = true;
            this._Is3n.Size = new System.Drawing.Size(68, 20);
            this._Is3n.TabIndex = 36;
            // 
            // _Is3c
            // 
            this._Is3c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3c.Location = new System.Drawing.Point(28, 61);
            this._Is3c.Name = "_Is3c";
            this._Is3c.ReadOnly = true;
            this._Is3c.Size = new System.Drawing.Size(68, 20);
            this._Is3c.TabIndex = 35;
            // 
            // _Is3b
            // 
            this._Is3b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3b.Location = new System.Drawing.Point(28, 38);
            this._Is3b.Name = "_Is3b";
            this._Is3b.ReadOnly = true;
            this._Is3b.Size = new System.Drawing.Size(68, 20);
            this._Is3b.TabIndex = 34;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(102, 18);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(16, 13);
            this.label26.TabIndex = 32;
            this.label26.Text = "I1";
            // 
            // _Is3a
            // 
            this._Is3a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is3a.Location = new System.Drawing.Point(28, 15);
            this._Is3a.Name = "_Is3a";
            this._Is3a.ReadOnly = true;
            this._Is3a.Size = new System.Drawing.Size(68, 20);
            this._Is3a.TabIndex = 33;
            // 
            // label471
            // 
            this.label471.AutoSize = true;
            this.label471.Location = new System.Drawing.Point(101, 64);
            this.label471.Name = "label471";
            this.label471.Size = new System.Drawing.Size(22, 13);
            this.label471.TabIndex = 32;
            this.label471.Text = "3I0";
            // 
            // label472
            // 
            this.label472.AutoSize = true;
            this.label472.Location = new System.Drawing.Point(6, 64);
            this.label472.Name = "label472";
            this.label472.Size = new System.Drawing.Size(16, 13);
            this.label472.TabIndex = 28;
            this.label472.Text = "Ic";
            // 
            // label473
            // 
            this.label473.AutoSize = true;
            this.label473.Location = new System.Drawing.Point(102, 86);
            this.label473.Name = "label473";
            this.label473.Size = new System.Drawing.Size(16, 13);
            this.label473.TabIndex = 30;
            this.label473.Text = "In";
            // 
            // label474
            // 
            this.label474.AutoSize = true;
            this.label474.Location = new System.Drawing.Point(6, 41);
            this.label474.Name = "label474";
            this.label474.Size = new System.Drawing.Size(16, 13);
            this.label474.TabIndex = 26;
            this.label474.Text = "Ib";
            // 
            // label476
            // 
            this.label476.AutoSize = true;
            this.label476.Location = new System.Drawing.Point(6, 18);
            this.label476.Name = "label476";
            this.label476.Size = new System.Drawing.Size(16, 13);
            this.label476.TabIndex = 17;
            this.label476.Text = "Ia";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this.label323);
            this.groupBox37.Controls.Add(this._Is22);
            this.groupBox37.Controls.Add(this._Is2I1);
            this.groupBox37.Controls.Add(this._Is20);
            this.groupBox37.Controls.Add(this._Is2n);
            this.groupBox37.Controls.Add(this._Is2c);
            this.groupBox37.Controls.Add(this._Is2b);
            this.groupBox37.Controls.Add(this.label25);
            this.groupBox37.Controls.Add(this._Is2a);
            this.groupBox37.Controls.Add(this.label324);
            this.groupBox37.Controls.Add(this.label325);
            this.groupBox37.Controls.Add(this.label326);
            this.groupBox37.Controls.Add(this.label327);
            this.groupBox37.Controls.Add(this.label329);
            this.groupBox37.Location = new System.Drawing.Point(223, 116);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(209, 114);
            this.groupBox37.TabIndex = 62;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Ток ст. 2";
            // 
            // label323
            // 
            this.label323.AutoSize = true;
            this.label323.Location = new System.Drawing.Point(102, 41);
            this.label323.Name = "label323";
            this.label323.Size = new System.Drawing.Size(16, 13);
            this.label323.TabIndex = 39;
            this.label323.Text = "I2";
            // 
            // _Is22
            // 
            this._Is22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is22.Location = new System.Drawing.Point(124, 38);
            this._Is22.Name = "_Is22";
            this._Is22.ReadOnly = true;
            this._Is22.Size = new System.Drawing.Size(68, 20);
            this._Is22.TabIndex = 38;
            // 
            // _Is2I1
            // 
            this._Is2I1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2I1.Location = new System.Drawing.Point(124, 15);
            this._Is2I1.Name = "_Is2I1";
            this._Is2I1.ReadOnly = true;
            this._Is2I1.Size = new System.Drawing.Size(68, 20);
            this._Is2I1.TabIndex = 37;
            // 
            // _Is20
            // 
            this._Is20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is20.Location = new System.Drawing.Point(124, 61);
            this._Is20.Name = "_Is20";
            this._Is20.ReadOnly = true;
            this._Is20.Size = new System.Drawing.Size(68, 20);
            this._Is20.TabIndex = 37;
            // 
            // _Is2n
            // 
            this._Is2n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2n.Location = new System.Drawing.Point(124, 83);
            this._Is2n.Name = "_Is2n";
            this._Is2n.ReadOnly = true;
            this._Is2n.Size = new System.Drawing.Size(68, 20);
            this._Is2n.TabIndex = 36;
            // 
            // _Is2c
            // 
            this._Is2c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2c.Location = new System.Drawing.Point(28, 61);
            this._Is2c.Name = "_Is2c";
            this._Is2c.ReadOnly = true;
            this._Is2c.Size = new System.Drawing.Size(68, 20);
            this._Is2c.TabIndex = 35;
            // 
            // _Is2b
            // 
            this._Is2b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2b.Location = new System.Drawing.Point(28, 38);
            this._Is2b.Name = "_Is2b";
            this._Is2b.ReadOnly = true;
            this._Is2b.Size = new System.Drawing.Size(68, 20);
            this._Is2b.TabIndex = 34;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(102, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(16, 13);
            this.label25.TabIndex = 32;
            this.label25.Text = "I1";
            // 
            // _Is2a
            // 
            this._Is2a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is2a.Location = new System.Drawing.Point(28, 15);
            this._Is2a.Name = "_Is2a";
            this._Is2a.ReadOnly = true;
            this._Is2a.Size = new System.Drawing.Size(68, 20);
            this._Is2a.TabIndex = 33;
            // 
            // label324
            // 
            this.label324.AutoSize = true;
            this.label324.Location = new System.Drawing.Point(102, 64);
            this.label324.Name = "label324";
            this.label324.Size = new System.Drawing.Size(22, 13);
            this.label324.TabIndex = 32;
            this.label324.Text = "3I0";
            // 
            // label325
            // 
            this.label325.AutoSize = true;
            this.label325.Location = new System.Drawing.Point(6, 64);
            this.label325.Name = "label325";
            this.label325.Size = new System.Drawing.Size(16, 13);
            this.label325.TabIndex = 28;
            this.label325.Text = "Ic";
            // 
            // label326
            // 
            this.label326.AutoSize = true;
            this.label326.Location = new System.Drawing.Point(102, 85);
            this.label326.Name = "label326";
            this.label326.Size = new System.Drawing.Size(16, 13);
            this.label326.TabIndex = 30;
            this.label326.Text = "In";
            // 
            // label327
            // 
            this.label327.AutoSize = true;
            this.label327.Location = new System.Drawing.Point(6, 41);
            this.label327.Name = "label327";
            this.label327.Size = new System.Drawing.Size(16, 13);
            this.label327.TabIndex = 26;
            this.label327.Text = "Ib";
            // 
            // label329
            // 
            this.label329.AutoSize = true;
            this.label329.Location = new System.Drawing.Point(6, 18);
            this.label329.Name = "label329";
            this.label329.Size = new System.Drawing.Size(16, 13);
            this.label329.TabIndex = 17;
            this.label329.Text = "Ia";
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this.label11);
            this.groupBox36.Controls.Add(this._Is12);
            this.groupBox36.Controls.Add(this._Is1I1);
            this.groupBox36.Controls.Add(this._Is10);
            this.groupBox36.Controls.Add(this._Is1n);
            this.groupBox36.Controls.Add(this._Is1c);
            this.groupBox36.Controls.Add(this._Is1b);
            this.groupBox36.Controls.Add(this.label24);
            this.groupBox36.Controls.Add(this._Is1a);
            this.groupBox36.Controls.Add(this.label14);
            this.groupBox36.Controls.Add(this.label148);
            this.groupBox36.Controls.Add(this.label80);
            this.groupBox36.Controls.Add(this.label146);
            this.groupBox36.Controls.Add(this.label145);
            this.groupBox36.Location = new System.Drawing.Point(8, 116);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(209, 114);
            this.groupBox36.TabIndex = 61;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Ток ст. 1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(102, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 39;
            this.label11.Text = "I2";
            // 
            // _Is12
            // 
            this._Is12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is12.Location = new System.Drawing.Point(124, 38);
            this._Is12.Name = "_Is12";
            this._Is12.ReadOnly = true;
            this._Is12.Size = new System.Drawing.Size(68, 20);
            this._Is12.TabIndex = 38;
            // 
            // _Is1I1
            // 
            this._Is1I1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1I1.Location = new System.Drawing.Point(124, 15);
            this._Is1I1.Name = "_Is1I1";
            this._Is1I1.ReadOnly = true;
            this._Is1I1.Size = new System.Drawing.Size(68, 20);
            this._Is1I1.TabIndex = 37;
            // 
            // _Is10
            // 
            this._Is10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is10.Location = new System.Drawing.Point(124, 61);
            this._Is10.Name = "_Is10";
            this._Is10.ReadOnly = true;
            this._Is10.Size = new System.Drawing.Size(68, 20);
            this._Is10.TabIndex = 37;
            // 
            // _Is1n
            // 
            this._Is1n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1n.Location = new System.Drawing.Point(124, 85);
            this._Is1n.Name = "_Is1n";
            this._Is1n.ReadOnly = true;
            this._Is1n.Size = new System.Drawing.Size(68, 20);
            this._Is1n.TabIndex = 36;
            // 
            // _Is1c
            // 
            this._Is1c.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1c.Location = new System.Drawing.Point(28, 61);
            this._Is1c.Name = "_Is1c";
            this._Is1c.ReadOnly = true;
            this._Is1c.Size = new System.Drawing.Size(68, 20);
            this._Is1c.TabIndex = 35;
            // 
            // _Is1b
            // 
            this._Is1b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1b.Location = new System.Drawing.Point(28, 38);
            this._Is1b.Name = "_Is1b";
            this._Is1b.ReadOnly = true;
            this._Is1b.Size = new System.Drawing.Size(68, 20);
            this._Is1b.TabIndex = 34;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(102, 18);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 32;
            this.label24.Text = "I1";
            // 
            // _Is1a
            // 
            this._Is1a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Is1a.Location = new System.Drawing.Point(28, 15);
            this._Is1a.Name = "_Is1a";
            this._Is1a.ReadOnly = true;
            this._Is1a.Size = new System.Drawing.Size(68, 20);
            this._Is1a.TabIndex = 33;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(102, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 32;
            this.label14.Text = "3I0";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(6, 64);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(16, 13);
            this.label148.TabIndex = 28;
            this.label148.Text = "Ic";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(102, 87);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(16, 13);
            this.label80.TabIndex = 30;
            this.label80.Text = "In";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(6, 41);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(16, 13);
            this.label146.TabIndex = 26;
            this.label146.Text = "Ib";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(6, 18);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(16, 13);
            this.label145.TabIndex = 17;
            this.label145.Text = "Ia";
            // 
            // groupBox
            // 
            this.groupBox.Controls.Add(this.label1);
            this.groupBox.Controls.Add(this._frequency);
            this.groupBox.Location = new System.Drawing.Point(325, 450);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(107, 41);
            this.groupBox.TabIndex = 60;
            this.groupBox.TabStop = false;
            this.groupBox.Text = "Частота";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "F";
            // 
            // _frequency
            // 
            this._frequency.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._frequency.Location = new System.Drawing.Point(37, 13);
            this._frequency.Name = "_frequency";
            this._frequency.ReadOnly = true;
            this._frequency.Size = new System.Drawing.Size(61, 20);
            this._frequency.TabIndex = 57;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this._percentIdc);
            this.groupBox21.Controls.Add(this._percentIdb);
            this.groupBox21.Controls.Add(this._percentIda);
            this.groupBox21.Controls.Add(this._Idc);
            this.groupBox21.Controls.Add(this._Idb);
            this.groupBox21.Controls.Add(this._Ida);
            this.groupBox21.Controls.Add(this.label13);
            this.groupBox21.Controls.Add(this.label99);
            this.groupBox21.Controls.Add(this.label103);
            this.groupBox21.Location = new System.Drawing.Point(8, 10);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(209, 100);
            this.groupBox21.TabIndex = 50;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Диф. ток осн. гармоника";
            // 
            // _percentIdc
            // 
            this._percentIdc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIdc.Location = new System.Drawing.Point(124, 74);
            this._percentIdc.Name = "_percentIdc";
            this._percentIdc.ReadOnly = true;
            this._percentIdc.Size = new System.Drawing.Size(68, 20);
            this._percentIdc.TabIndex = 39;
            // 
            // _percentIdb
            // 
            this._percentIdb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIdb.Location = new System.Drawing.Point(124, 45);
            this._percentIdb.Name = "_percentIdb";
            this._percentIdb.ReadOnly = true;
            this._percentIdb.Size = new System.Drawing.Size(68, 20);
            this._percentIdb.TabIndex = 38;
            // 
            // _percentIda
            // 
            this._percentIda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIda.Location = new System.Drawing.Point(124, 16);
            this._percentIda.Name = "_percentIda";
            this._percentIda.ReadOnly = true;
            this._percentIda.Size = new System.Drawing.Size(68, 20);
            this._percentIda.TabIndex = 37;
            // 
            // _Idc
            // 
            this._Idc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Idc.Location = new System.Drawing.Point(28, 74);
            this._Idc.Name = "_Idc";
            this._Idc.ReadOnly = true;
            this._Idc.Size = new System.Drawing.Size(68, 20);
            this._Idc.TabIndex = 36;
            // 
            // _Idb
            // 
            this._Idb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Idb.Location = new System.Drawing.Point(28, 45);
            this._Idb.Name = "_Idb";
            this._Idb.ReadOnly = true;
            this._Idb.Size = new System.Drawing.Size(68, 20);
            this._Idb.TabIndex = 35;
            // 
            // _Ida
            // 
            this._Ida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ida.Location = new System.Drawing.Point(28, 16);
            this._Ida.Name = "_Ida";
            this._Ida.ReadOnly = true;
            this._Ida.Size = new System.Drawing.Size(68, 20);
            this._Ida.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Ic";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 48);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(16, 13);
            this.label99.TabIndex = 26;
            this.label99.Text = "Ib";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 19);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(16, 13);
            this.label103.TabIndex = 17;
            this.label103.Text = "Ia";
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this._percentIbc);
            this.groupBox25.Controls.Add(this._percentIbb);
            this.groupBox25.Controls.Add(this._percentIba);
            this.groupBox25.Controls.Add(this._Ibc);
            this.groupBox25.Controls.Add(this._Ibb);
            this.groupBox25.Controls.Add(this._Iba);
            this.groupBox25.Controls.Add(this.label108);
            this.groupBox25.Controls.Add(this.label109);
            this.groupBox25.Controls.Add(this.label135);
            this.groupBox25.Location = new System.Drawing.Point(223, 10);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(209, 100);
            this.groupBox25.TabIndex = 49;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Тормозной ток";
            // 
            // _percentIbc
            // 
            this._percentIbc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIbc.Location = new System.Drawing.Point(124, 74);
            this._percentIbc.Name = "_percentIbc";
            this._percentIbc.ReadOnly = true;
            this._percentIbc.Size = new System.Drawing.Size(68, 20);
            this._percentIbc.TabIndex = 40;
            // 
            // _percentIbb
            // 
            this._percentIbb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIbb.Location = new System.Drawing.Point(124, 45);
            this._percentIbb.Name = "_percentIbb";
            this._percentIbb.ReadOnly = true;
            this._percentIbb.Size = new System.Drawing.Size(68, 20);
            this._percentIbb.TabIndex = 39;
            // 
            // _percentIba
            // 
            this._percentIba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._percentIba.Location = new System.Drawing.Point(124, 16);
            this._percentIba.Name = "_percentIba";
            this._percentIba.ReadOnly = true;
            this._percentIba.Size = new System.Drawing.Size(68, 20);
            this._percentIba.TabIndex = 38;
            // 
            // _Ibc
            // 
            this._Ibc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibc.Location = new System.Drawing.Point(28, 74);
            this._Ibc.Name = "_Ibc";
            this._Ibc.ReadOnly = true;
            this._Ibc.Size = new System.Drawing.Size(68, 20);
            this._Ibc.TabIndex = 37;
            // 
            // _Ibb
            // 
            this._Ibb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Ibb.Location = new System.Drawing.Point(28, 45);
            this._Ibb.Name = "_Ibb";
            this._Ibb.ReadOnly = true;
            this._Ibb.Size = new System.Drawing.Size(68, 20);
            this._Ibb.TabIndex = 36;
            // 
            // _Iba
            // 
            this._Iba.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Iba.Location = new System.Drawing.Point(28, 16);
            this._Iba.Name = "_Iba";
            this._Iba.ReadOnly = true;
            this._Iba.Size = new System.Drawing.Size(68, 20);
            this._Iba.TabIndex = 35;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 77);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(16, 13);
            this.label108.TabIndex = 28;
            this.label108.Text = "Ic";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 48);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(16, 13);
            this.label109.TabIndex = 26;
            this.label109.Text = "Ib";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(6, 18);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(16, 13);
            this.label135.TabIndex = 17;
            this.label135.Text = "Ia";
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this.CN);
            this.groupBox47.Controls.Add(this.CA);
            this.groupBox47.Controls.Add(this.BN);
            this.groupBox47.Controls.Add(this.AN);
            this.groupBox47.Controls.Add(this.BC);
            this.groupBox47.Controls.Add(this.AB);
            this.groupBox47.Controls.Add(this.label340);
            this.groupBox47.Controls.Add(this.label339);
            this.groupBox47.Controls.Add(this.label338);
            this.groupBox47.Controls.Add(this.label337);
            this.groupBox47.Controls.Add(this.label336);
            this.groupBox47.Controls.Add(this.label335);
            this.groupBox47.Location = new System.Drawing.Point(598, 10);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(104, 193);
            this.groupBox47.TabIndex = 48;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Направления Z";
            // 
            // CN
            // 
            this.CN.BackColor = System.Drawing.SystemColors.Control;
            this.CN.Location = new System.Drawing.Point(29, 167);
            this.CN.Name = "CN";
            this.CN.Size = new System.Drawing.Size(51, 20);
            this.CN.TabIndex = 11;
            // 
            // CA
            // 
            this.CA.BackColor = System.Drawing.SystemColors.Control;
            this.CA.Location = new System.Drawing.Point(29, 74);
            this.CA.Name = "CA";
            this.CA.Size = new System.Drawing.Size(51, 20);
            this.CA.TabIndex = 10;
            // 
            // BN
            // 
            this.BN.BackColor = System.Drawing.SystemColors.Control;
            this.BN.Location = new System.Drawing.Point(29, 143);
            this.BN.Name = "BN";
            this.BN.Size = new System.Drawing.Size(51, 20);
            this.BN.TabIndex = 9;
            // 
            // AN
            // 
            this.AN.BackColor = System.Drawing.SystemColors.Control;
            this.AN.Location = new System.Drawing.Point(29, 120);
            this.AN.Name = "AN";
            this.AN.Size = new System.Drawing.Size(51, 20);
            this.AN.TabIndex = 8;
            // 
            // BC
            // 
            this.BC.BackColor = System.Drawing.SystemColors.Control;
            this.BC.Location = new System.Drawing.Point(29, 45);
            this.BC.Name = "BC";
            this.BC.Size = new System.Drawing.Size(51, 20);
            this.BC.TabIndex = 7;
            // 
            // AB
            // 
            this.AB.BackColor = System.Drawing.SystemColors.Control;
            this.AB.Location = new System.Drawing.Point(29, 16);
            this.AB.Name = "AB";
            this.AB.Size = new System.Drawing.Size(51, 20);
            this.AB.TabIndex = 6;
            // 
            // label340
            // 
            this.label340.AutoSize = true;
            this.label340.Location = new System.Drawing.Point(1, 170);
            this.label340.Name = "label340";
            this.label340.Size = new System.Drawing.Size(26, 13);
            this.label340.TabIndex = 5;
            this.label340.Text = "Zc1";
            // 
            // label339
            // 
            this.label339.AutoSize = true;
            this.label339.Location = new System.Drawing.Point(1, 146);
            this.label339.Name = "label339";
            this.label339.Size = new System.Drawing.Size(26, 13);
            this.label339.TabIndex = 4;
            this.label339.Text = "Zb1";
            // 
            // label338
            // 
            this.label338.AutoSize = true;
            this.label338.Location = new System.Drawing.Point(1, 123);
            this.label338.Name = "label338";
            this.label338.Size = new System.Drawing.Size(26, 13);
            this.label338.TabIndex = 3;
            this.label338.Text = "Za1";
            // 
            // label337
            // 
            this.label337.AutoSize = true;
            this.label337.Location = new System.Drawing.Point(1, 76);
            this.label337.Name = "label337";
            this.label337.Size = new System.Drawing.Size(26, 13);
            this.label337.TabIndex = 2;
            this.label337.Text = "Zca";
            // 
            // label336
            // 
            this.label336.AutoSize = true;
            this.label336.Location = new System.Drawing.Point(1, 48);
            this.label336.Name = "label336";
            this.label336.Size = new System.Drawing.Size(26, 13);
            this.label336.TabIndex = 1;
            this.label336.Text = "Zbc";
            // 
            // label335
            // 
            this.label335.AutoSize = true;
            this.label335.Location = new System.Drawing.Point(1, 19);
            this.label335.Name = "label335";
            this.label335.Size = new System.Drawing.Size(26, 13);
            this.label335.TabIndex = 0;
            this.label335.Text = "Zab";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._cosfTextBox);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this._qTextBox);
            this.groupBox2.Controls.Add(this.label95);
            this.groupBox2.Controls.Add(this._pTextBox);
            this.groupBox2.Controls.Add(this.label97);
            this.groupBox2.Location = new System.Drawing.Point(325, 355);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(107, 89);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Мощности";
            // 
            // _cosfTextBox
            // 
            this._cosfTextBox.Location = new System.Drawing.Point(37, 60);
            this._cosfTextBox.Name = "_cosfTextBox";
            this._cosfTextBox.ReadOnly = true;
            this._cosfTextBox.Size = new System.Drawing.Size(61, 20);
            this._cosfTextBox.TabIndex = 7;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 63);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "cos f";
            // 
            // _qTextBox
            // 
            this._qTextBox.Location = new System.Drawing.Point(37, 37);
            this._qTextBox.Name = "_qTextBox";
            this._qTextBox.ReadOnly = true;
            this._qTextBox.Size = new System.Drawing.Size(61, 20);
            this._qTextBox.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(15, 41);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(15, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Q";
            // 
            // _pTextBox
            // 
            this._pTextBox.Location = new System.Drawing.Point(37, 14);
            this._pTextBox.Name = "_pTextBox";
            this._pTextBox.ReadOnly = true;
            this._pTextBox.Size = new System.Drawing.Size(61, 20);
            this._pTextBox.TabIndex = 1;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(15, 16);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(14, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "P";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox24);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Location = new System.Drawing.Point(438, 10);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(154, 202);
            this.groupBox7.TabIndex = 46;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Сопротивления, Ом";
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this._za1Box);
            this.groupBox24.Controls.Add(this._zc1Box);
            this.groupBox24.Controls.Add(this.label137);
            this.groupBox24.Controls.Add(this._zb1Box);
            this.groupBox24.Controls.Add(this.label147);
            this.groupBox24.Controls.Add(this.label149);
            this.groupBox24.Location = new System.Drawing.Point(6, 106);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(142, 87);
            this.groupBox24.TabIndex = 8;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Фазные N1";
            // 
            // _za1Box
            // 
            this._za1Box.Location = new System.Drawing.Point(47, 19);
            this._za1Box.Name = "_za1Box";
            this._za1Box.ReadOnly = true;
            this._za1Box.Size = new System.Drawing.Size(87, 20);
            this._za1Box.TabIndex = 1;
            // 
            // _zc1Box
            // 
            this._zc1Box.Location = new System.Drawing.Point(47, 59);
            this._zc1Box.Name = "_zc1Box";
            this._zc1Box.ReadOnly = true;
            this._zc1Box.Size = new System.Drawing.Size(87, 20);
            this._zc1Box.TabIndex = 1;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(6, 22);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(29, 13);
            this.label137.TabIndex = 5;
            this.label137.Text = "Za =";
            // 
            // _zb1Box
            // 
            this._zb1Box.Location = new System.Drawing.Point(47, 39);
            this._zb1Box.Name = "_zb1Box";
            this._zb1Box.ReadOnly = true;
            this._zb1Box.Size = new System.Drawing.Size(87, 20);
            this._zb1Box.TabIndex = 1;
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(6, 42);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(29, 13);
            this.label147.TabIndex = 5;
            this.label147.Text = "Zb =";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(6, 62);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(29, 13);
            this.label149.TabIndex = 5;
            this.label149.Text = "Zc =";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._zabBox);
            this.groupBox8.Controls.Add(this._zcaBox);
            this.groupBox8.Controls.Add(this._zbcBox);
            this.groupBox8.Controls.Add(this.label106);
            this.groupBox8.Controls.Add(this.label107);
            this.groupBox8.Controls.Add(this.label110);
            this.groupBox8.Controls.Add(this.rxcaLabel);
            this.groupBox8.Controls.Add(this.rxbcLabel);
            this.groupBox8.Controls.Add(this.rxabLabel);
            this.groupBox8.Location = new System.Drawing.Point(6, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(142, 87);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Межфазные";
            // 
            // _zabBox
            // 
            this._zabBox.Location = new System.Drawing.Point(47, 19);
            this._zabBox.Name = "_zabBox";
            this._zabBox.ReadOnly = true;
            this._zabBox.Size = new System.Drawing.Size(87, 20);
            this._zabBox.TabIndex = 1;
            // 
            // _zcaBox
            // 
            this._zcaBox.Location = new System.Drawing.Point(47, 59);
            this._zcaBox.Name = "_zcaBox";
            this._zcaBox.ReadOnly = true;
            this._zcaBox.Size = new System.Drawing.Size(87, 20);
            this._zcaBox.TabIndex = 1;
            // 
            // _zbcBox
            // 
            this._zbcBox.Location = new System.Drawing.Point(47, 39);
            this._zbcBox.Name = "_zbcBox";
            this._zbcBox.ReadOnly = true;
            this._zbcBox.Size = new System.Drawing.Size(87, 20);
            this._zbcBox.TabIndex = 1;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 22);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(35, 13);
            this.label106.TabIndex = 5;
            this.label106.Text = "Zab =";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 42);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(35, 13);
            this.label107.TabIndex = 5;
            this.label107.Text = "Zbc =";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 62);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(35, 13);
            this.label110.TabIndex = 5;
            this.label110.Text = "Zca =";
            // 
            // rxcaLabel
            // 
            this.rxcaLabel.AutoSize = true;
            this.rxcaLabel.Location = new System.Drawing.Point(110, 74);
            this.rxcaLabel.Name = "rxcaLabel";
            this.rxcaLabel.Size = new System.Drawing.Size(0, 13);
            this.rxcaLabel.TabIndex = 2;
            // 
            // rxbcLabel
            // 
            this.rxbcLabel.AutoSize = true;
            this.rxbcLabel.Location = new System.Drawing.Point(110, 48);
            this.rxbcLabel.Name = "rxbcLabel";
            this.rxbcLabel.Size = new System.Drawing.Size(0, 13);
            this.rxbcLabel.TabIndex = 2;
            // 
            // rxabLabel
            // 
            this.rxabLabel.AutoSize = true;
            this.rxabLabel.Location = new System.Drawing.Point(110, 22);
            this.rxabLabel.Name = "rxabLabel";
            this.rxabLabel.Size = new System.Drawing.Size(0, 13);
            this.rxabLabel.TabIndex = 2;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.groupBox45);
            this.groupBox13.Controls.Add(this.groupBox46);
            this.groupBox13.Location = new System.Drawing.Point(733, 369);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(154, 202);
            this.groupBox13.TabIndex = 46;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Сопротивления, Ом перв.";
            this.groupBox13.Visible = false;
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this._iZa1Box);
            this.groupBox45.Controls.Add(this._iZc1Box);
            this.groupBox45.Controls.Add(this.label151);
            this.groupBox45.Controls.Add(this._iZb1Box);
            this.groupBox45.Controls.Add(this.label153);
            this.groupBox45.Controls.Add(this.label155);
            this.groupBox45.Location = new System.Drawing.Point(6, 106);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(142, 87);
            this.groupBox45.TabIndex = 8;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Фазные N1";
            // 
            // _iZa1Box
            // 
            this._iZa1Box.Location = new System.Drawing.Point(47, 19);
            this._iZa1Box.Name = "_iZa1Box";
            this._iZa1Box.ReadOnly = true;
            this._iZa1Box.Size = new System.Drawing.Size(87, 20);
            this._iZa1Box.TabIndex = 1;
            // 
            // _iZc1Box
            // 
            this._iZc1Box.Location = new System.Drawing.Point(47, 59);
            this._iZc1Box.Name = "_iZc1Box";
            this._iZc1Box.ReadOnly = true;
            this._iZc1Box.Size = new System.Drawing.Size(87, 20);
            this._iZc1Box.TabIndex = 1;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(6, 22);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(29, 13);
            this.label151.TabIndex = 5;
            this.label151.Text = "Za =";
            // 
            // _iZb1Box
            // 
            this._iZb1Box.Location = new System.Drawing.Point(47, 39);
            this._iZb1Box.Name = "_iZb1Box";
            this._iZb1Box.ReadOnly = true;
            this._iZb1Box.Size = new System.Drawing.Size(87, 20);
            this._iZb1Box.TabIndex = 1;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(6, 42);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(29, 13);
            this.label153.TabIndex = 5;
            this.label153.Text = "Zb =";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(6, 62);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(29, 13);
            this.label155.TabIndex = 5;
            this.label155.Text = "Zc =";
            // 
            // groupBox46
            // 
            this.groupBox46.Controls.Add(this._iZabBox);
            this.groupBox46.Controls.Add(this._iZcaBox);
            this.groupBox46.Controls.Add(this._iZbcBox);
            this.groupBox46.Controls.Add(this.label156);
            this.groupBox46.Controls.Add(this.label157);
            this.groupBox46.Controls.Add(this.label158);
            this.groupBox46.Controls.Add(this.label330);
            this.groupBox46.Controls.Add(this.label331);
            this.groupBox46.Controls.Add(this.label332);
            this.groupBox46.Location = new System.Drawing.Point(6, 19);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(142, 87);
            this.groupBox46.TabIndex = 8;
            this.groupBox46.TabStop = false;
            this.groupBox46.Text = "Межфазные";
            // 
            // _iZabBox
            // 
            this._iZabBox.Location = new System.Drawing.Point(47, 19);
            this._iZabBox.Name = "_iZabBox";
            this._iZabBox.ReadOnly = true;
            this._iZabBox.Size = new System.Drawing.Size(87, 20);
            this._iZabBox.TabIndex = 1;
            // 
            // _iZcaBox
            // 
            this._iZcaBox.Location = new System.Drawing.Point(47, 59);
            this._iZcaBox.Name = "_iZcaBox";
            this._iZcaBox.ReadOnly = true;
            this._iZcaBox.Size = new System.Drawing.Size(87, 20);
            this._iZcaBox.TabIndex = 1;
            // 
            // _iZbcBox
            // 
            this._iZbcBox.Location = new System.Drawing.Point(47, 39);
            this._iZbcBox.Name = "_iZbcBox";
            this._iZbcBox.ReadOnly = true;
            this._iZbcBox.Size = new System.Drawing.Size(87, 20);
            this._iZbcBox.TabIndex = 1;
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(6, 22);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(35, 13);
            this.label156.TabIndex = 5;
            this.label156.Text = "Zab =";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(6, 42);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(35, 13);
            this.label157.TabIndex = 5;
            this.label157.Text = "Zbc =";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(6, 62);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(35, 13);
            this.label158.TabIndex = 5;
            this.label158.Text = "Zca =";
            // 
            // label330
            // 
            this.label330.AutoSize = true;
            this.label330.Location = new System.Drawing.Point(110, 74);
            this.label330.Name = "label330";
            this.label330.Size = new System.Drawing.Size(0, 13);
            this.label330.TabIndex = 2;
            // 
            // label331
            // 
            this.label331.AutoSize = true;
            this.label331.Location = new System.Drawing.Point(110, 48);
            this.label331.Name = "label331";
            this.label331.Size = new System.Drawing.Size(0, 13);
            this.label331.TabIndex = 2;
            // 
            // label332
            // 
            this.label332.AutoSize = true;
            this.label332.Location = new System.Drawing.Point(110, 22);
            this.label332.Name = "label332";
            this.label332.Size = new System.Drawing.Size(0, 13);
            this.label332.TabIndex = 2;
            // 
            // _dataBaseTabControl
            // 
            this._dataBaseTabControl.Controls.Add(this._analogTabPage);
            this._dataBaseTabControl.Controls.Add(this.tabPage1);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage1);
            this._dataBaseTabControl.Controls.Add(this._discretTabPage2);
            this._dataBaseTabControl.Controls.Add(this._controlSignalsTabPage);
            this._dataBaseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataBaseTabControl.Location = new System.Drawing.Point(0, 0);
            this._dataBaseTabControl.Name = "_dataBaseTabControl";
            this._dataBaseTabControl.SelectedIndex = 0;
            this._dataBaseTabControl.Size = new System.Drawing.Size(919, 625);
            this._dataBaseTabControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._rsTriggersGB);
            this.tabPage1.Controls.Add(this._r56Label);
            this.tabPage1.Controls.Add(this._r58Label);
            this.tabPage1.Controls.Add(this._r70Label);
            this.tabPage1.Controls.Add(this._r18Label);
            this.tabPage1.Controls.Add(this.groupBox18);
            this.tabPage1.Controls.Add(this._r59Label);
            this.tabPage1.Controls.Add(this._r71Label);
            this.tabPage1.Controls.Add(this.label162);
            this.tabPage1.Controls.Add(this._diskretInputGB);
            this.tabPage1.Controls.Add(this._r64Label);
            this.tabPage1.Controls.Add(this.label164);
            this.tabPage1.Controls.Add(this._r76Label);
            this.tabPage1.Controls.Add(this._r40Label);
            this.tabPage1.Controls.Add(this._r69Label);
            this.tabPage1.Controls.Add(this._r55Label);
            this.tabPage1.Controls.Add(this._r51Label);
            this.tabPage1.Controls.Add(this.label165);
            this.tabPage1.Controls.Add(this._r60Label);
            this.tabPage1.Controls.Add(this.label163);
            this.tabPage1.Controls.Add(this._r72Label);
            this.tabPage1.Controls.Add(this._r57Label);
            this.tabPage1.Controls.Add(this._r65Label);
            this.tabPage1.Controls.Add(this._r80Label);
            this.tabPage1.Controls.Add(this._r77Label);
            this.tabPage1.Controls.Add(this._r68Label);
            this.tabPage1.Controls.Add(this.label169);
            this.tabPage1.Controls.Add(this._r39Label);
            this.tabPage1.Controls.Add(this._r75Label);
            this.tabPage1.Controls.Add(this._r54Label);
            this.tabPage1.Controls.Add(this._r61Label);
            this.tabPage1.Controls.Add(this._r63Label);
            this.tabPage1.Controls.Add(this._r73Label);
            this.tabPage1.Controls.Add(this.label176);
            this.tabPage1.Controls.Add(this._r66Label);
            this.tabPage1.Controls.Add(this._r78Label);
            this.tabPage1.Controls.Add(this._r46Label);
            this.tabPage1.Controls.Add(this.label170);
            this.tabPage1.Controls.Add(this.label166);
            this.tabPage1.Controls.Add(this._r53Label);
            this.tabPage1.Controls.Add(this.label171);
            this.tabPage1.Controls.Add(this._r62Label);
            this.tabPage1.Controls.Add(this._r74Label);
            this.tabPage1.Controls.Add(this._r67Label);
            this.tabPage1.Controls.Add(this._r34Label);
            this.tabPage1.Controls.Add(this._r79Label);
            this.tabPage1.Controls.Add(this._r52Label);
            this.tabPage1.Controls.Add(this.label175);
            this.tabPage1.Controls.Add(this.label177);
            this.tabPage1.Controls.Add(this.label167);
            this.tabPage1.Controls.Add(this.label178);
            this.tabPage1.Controls.Add(this.label168);
            this.tabPage1.Controls.Add(this._r50Label);
            this.tabPage1.Controls.Add(this.label174);
            this.tabPage1.Controls.Add(this._r38Label);
            this.tabPage1.Controls.Add(this.label173);
            this.tabPage1.Controls.Add(this.label172);
            this.tabPage1.Controls.Add(this._r45Label);
            this.tabPage1.Controls.Add(this._r30Label);
            this.tabPage1.Controls.Add(this._r29Label);
            this.tabPage1.Controls.Add(this._r28Label);
            this.tabPage1.Controls.Add(this._r27Label);
            this.tabPage1.Controls.Add(this._r33Label);
            this.tabPage1.Controls.Add(this._r26Label);
            this.tabPage1.Controls.Add(this._r25Label);
            this.tabPage1.Controls.Add(this._r24Label);
            this.tabPage1.Controls.Add(this._r23Label);
            this.tabPage1.Controls.Add(this._r31Label);
            this.tabPage1.Controls.Add(this._r49Label);
            this.tabPage1.Controls.Add(this._r22Label);
            this.tabPage1.Controls.Add(this._r21Label);
            this.tabPage1.Controls.Add(this._r37Label);
            this.tabPage1.Controls.Add(this._r20Label);
            this.tabPage1.Controls.Add(this._r44Label);
            this.tabPage1.Controls.Add(this._r19Label);
            this.tabPage1.Controls.Add(this._r32Label);
            this.tabPage1.Controls.Add(this._r42Label);
            this.tabPage1.Controls.Add(this._r48Label);
            this.tabPage1.Controls.Add(this._r41Label);
            this.tabPage1.Controls.Add(this._r36Label);
            this.tabPage1.Controls.Add(this._r35Label);
            this.tabPage1.Controls.Add(this._r47Label);
            this.tabPage1.Controls.Add(this._r43Label);
            this.tabPage1.Controls.Add(this._module56);
            this.tabPage1.Controls.Add(this._module40);
            this.tabPage1.Controls.Add(this._module57);
            this.tabPage1.Controls.Add(this._module18);
            this.tabPage1.Controls.Add(this._module10);
            this.tabPage1.Controls.Add(this._module69);
            this.tabPage1.Controls.Add(this._module58);
            this.tabPage1.Controls.Add(this._module70);
            this.tabPage1.Controls.Add(this._module39);
            this.tabPage1.Controls.Add(this._module80);
            this.tabPage1.Controls.Add(this._module17);
            this.tabPage1.Controls.Add(this._module51);
            this.tabPage1.Controls.Add(this._module68);
            this.tabPage1.Controls.Add(this._module55);
            this.tabPage1.Controls.Add(this._module59);
            this.tabPage1.Controls.Add(this._module9);
            this.tabPage1.Controls.Add(this._module71);
            this.tabPage1.Controls.Add(this._module75);
            this.tabPage1.Controls.Add(this._module16);
            this.tabPage1.Controls.Add(this._module64);
            this.tabPage1.Controls.Add(this._module76);
            this.tabPage1.Controls.Add(this._module46);
            this.tabPage1.Controls.Add(this._module63);
            this.tabPage1.Controls.Add(this._module15);
            this.tabPage1.Controls.Add(this._module5);
            this.tabPage1.Controls.Add(this._module54);
            this.tabPage1.Controls.Add(this._module34);
            this.tabPage1.Controls.Add(this._module60);
            this.tabPage1.Controls.Add(this._module72);
            this.tabPage1.Controls.Add(this._module65);
            this.tabPage1.Controls.Add(this._module8);
            this.tabPage1.Controls.Add(this._module77);
            this.tabPage1.Controls.Add(this._module14);
            this.tabPage1.Controls.Add(this._module53);
            this.tabPage1.Controls.Add(this._module79);
            this.tabPage1.Controls.Add(this._module61);
            this.tabPage1.Controls.Add(this._module73);
            this.tabPage1.Controls.Add(this._module67);
            this.tabPage1.Controls.Add(this._module66);
            this.tabPage1.Controls.Add(this._module13);
            this.tabPage1.Controls.Add(this._module78);
            this.tabPage1.Controls.Add(this._module1);
            this.tabPage1.Controls.Add(this._module74);
            this.tabPage1.Controls.Add(this._module50);
            this.tabPage1.Controls.Add(this._module62);
            this.tabPage1.Controls.Add(this._module52);
            this.tabPage1.Controls.Add(this._module7);
            this.tabPage1.Controls.Add(this._module12);
            this.tabPage1.Controls.Add(this._module38);
            this.tabPage1.Controls.Add(this._module45);
            this.tabPage1.Controls.Add(this._module11);
            this.tabPage1.Controls.Add(this._module2);
            this.tabPage1.Controls.Add(this._module6);
            this.tabPage1.Controls.Add(this._module3);
            this.tabPage1.Controls.Add(this._module33);
            this.tabPage1.Controls.Add(this._module4);
            this.tabPage1.Controls.Add(this._module19);
            this.tabPage1.Controls.Add(this._module29);
            this.tabPage1.Controls.Add(this._module28);
            this.tabPage1.Controls.Add(this._module27);
            this.tabPage1.Controls.Add(this._module30);
            this.tabPage1.Controls.Add(this._module49);
            this.tabPage1.Controls.Add(this._module26);
            this.tabPage1.Controls.Add(this._module37);
            this.tabPage1.Controls.Add(this._module25);
            this.tabPage1.Controls.Add(this._module44);
            this.tabPage1.Controls.Add(this._module24);
            this.tabPage1.Controls.Add(this._module23);
            this.tabPage1.Controls.Add(this._module22);
            this.tabPage1.Controls.Add(this._module21);
            this.tabPage1.Controls.Add(this._module20);
            this.tabPage1.Controls.Add(this._module32);
            this.tabPage1.Controls.Add(this._module31);
            this.tabPage1.Controls.Add(this._module35);
            this.tabPage1.Controls.Add(this._module41);
            this.tabPage1.Controls.Add(this._module48);
            this.tabPage1.Controls.Add(this._module43);
            this.tabPage1.Controls.Add(this._module36);
            this.tabPage1.Controls.Add(this._module47);
            this.tabPage1.Controls.Add(this._module42);
            this.tabPage1.Controls.Add(this._releGB);
            this.tabPage1.Controls.Add(this._virtualReleGB);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(911, 599);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Дискретные входы, индикаторы и реле";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _rsTriggersGB
            // 
            this._rsTriggersGB.Controls.Add(this._rst8);
            this._rsTriggersGB.Controls.Add(this.label32);
            this._rsTriggersGB.Controls.Add(this._rst7);
            this._rsTriggersGB.Controls.Add(this.label40);
            this._rsTriggersGB.Controls.Add(this._rst1);
            this._rsTriggersGB.Controls.Add(this._rst6);
            this._rsTriggersGB.Controls.Add(this.label133);
            this._rsTriggersGB.Controls.Add(this.label138);
            this._rsTriggersGB.Controls.Add(this.label140);
            this._rsTriggersGB.Controls.Add(this._rst5);
            this._rsTriggersGB.Controls.Add(this._rst2);
            this._rsTriggersGB.Controls.Add(this.label150);
            this._rsTriggersGB.Controls.Add(this.label161);
            this._rsTriggersGB.Controls.Add(this._rst4);
            this._rsTriggersGB.Controls.Add(this._rst16);
            this._rsTriggersGB.Controls.Add(this._rst3);
            this._rsTriggersGB.Controls.Add(this.label207);
            this._rsTriggersGB.Controls.Add(this.label208);
            this._rsTriggersGB.Controls.Add(this._rst15);
            this._rsTriggersGB.Controls.Add(this._rst9);
            this._rsTriggersGB.Controls.Add(this.label209);
            this._rsTriggersGB.Controls.Add(this.label211);
            this._rsTriggersGB.Controls.Add(this._rst14);
            this._rsTriggersGB.Controls.Add(this.label214);
            this._rsTriggersGB.Controls.Add(this.label215);
            this._rsTriggersGB.Controls.Add(this._rst10);
            this._rsTriggersGB.Controls.Add(this._rst13);
            this._rsTriggersGB.Controls.Add(this.label216);
            this._rsTriggersGB.Controls.Add(this.label217);
            this._rsTriggersGB.Controls.Add(this._rst11);
            this._rsTriggersGB.Controls.Add(this._rst12);
            this._rsTriggersGB.Controls.Add(this.label219);
            this._rsTriggersGB.Location = new System.Drawing.Point(553, 3);
            this._rsTriggersGB.Name = "_rsTriggersGB";
            this._rsTriggersGB.Size = new System.Drawing.Size(136, 199);
            this._rsTriggersGB.TabIndex = 56;
            this._rsTriggersGB.TabStop = false;
            this._rsTriggersGB.Text = "Энергонезависимые RS-триггеры";
            // 
            // _rst8
            // 
            this._rst8.Location = new System.Drawing.Point(6, 171);
            this._rst8.Name = "_rst8";
            this._rst8.Size = new System.Drawing.Size(13, 13);
            this._rst8.State = BEMN.Forms.LedState.Off;
            this._rst8.TabIndex = 15;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(21, 171);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 13);
            this.label32.TabIndex = 14;
            this.label32.Text = "RST8";
            // 
            // _rst7
            // 
            this._rst7.Location = new System.Drawing.Point(6, 152);
            this._rst7.Name = "_rst7";
            this._rst7.Size = new System.Drawing.Size(13, 13);
            this._rst7.State = BEMN.Forms.LedState.Off;
            this._rst7.TabIndex = 13;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(21, 152);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 13);
            this.label40.TabIndex = 12;
            this.label40.Text = "RST7";
            // 
            // _rst1
            // 
            this._rst1.Location = new System.Drawing.Point(6, 38);
            this._rst1.Name = "_rst1";
            this._rst1.Size = new System.Drawing.Size(13, 13);
            this._rst1.State = BEMN.Forms.LedState.Off;
            this._rst1.TabIndex = 1;
            // 
            // _rst6
            // 
            this._rst6.Location = new System.Drawing.Point(6, 133);
            this._rst6.Name = "_rst6";
            this._rst6.Size = new System.Drawing.Size(13, 13);
            this._rst6.State = BEMN.Forms.LedState.Off;
            this._rst6.TabIndex = 11;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(21, 38);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(35, 13);
            this.label133.TabIndex = 0;
            this.label133.Text = "RST1";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(21, 133);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(35, 13);
            this.label138.TabIndex = 10;
            this.label138.Text = "RST6";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(21, 57);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(35, 13);
            this.label140.TabIndex = 2;
            this.label140.Text = "RST2";
            // 
            // _rst5
            // 
            this._rst5.Location = new System.Drawing.Point(6, 114);
            this._rst5.Name = "_rst5";
            this._rst5.Size = new System.Drawing.Size(13, 13);
            this._rst5.State = BEMN.Forms.LedState.Off;
            this._rst5.TabIndex = 9;
            // 
            // _rst2
            // 
            this._rst2.Location = new System.Drawing.Point(6, 57);
            this._rst2.Name = "_rst2";
            this._rst2.Size = new System.Drawing.Size(13, 13);
            this._rst2.State = BEMN.Forms.LedState.Off;
            this._rst2.TabIndex = 3;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(21, 115);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(35, 13);
            this.label150.TabIndex = 8;
            this.label150.Text = "RST5";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(21, 76);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(35, 13);
            this.label161.TabIndex = 4;
            this.label161.Text = "RST3";
            // 
            // _rst4
            // 
            this._rst4.Location = new System.Drawing.Point(6, 95);
            this._rst4.Name = "_rst4";
            this._rst4.Size = new System.Drawing.Size(13, 13);
            this._rst4.State = BEMN.Forms.LedState.Off;
            this._rst4.TabIndex = 7;
            // 
            // _rst16
            // 
            this._rst16.Location = new System.Drawing.Point(62, 171);
            this._rst16.Name = "_rst16";
            this._rst16.Size = new System.Drawing.Size(13, 13);
            this._rst16.State = BEMN.Forms.LedState.Off;
            this._rst16.TabIndex = 15;
            // 
            // _rst3
            // 
            this._rst3.Location = new System.Drawing.Point(6, 76);
            this._rst3.Name = "_rst3";
            this._rst3.Size = new System.Drawing.Size(13, 13);
            this._rst3.State = BEMN.Forms.LedState.Off;
            this._rst3.TabIndex = 5;
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(78, 171);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(41, 13);
            this.label207.TabIndex = 14;
            this.label207.Text = "RST16";
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(21, 94);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(35, 13);
            this.label208.TabIndex = 6;
            this.label208.Text = "RST4";
            // 
            // _rst15
            // 
            this._rst15.Location = new System.Drawing.Point(62, 152);
            this._rst15.Name = "_rst15";
            this._rst15.Size = new System.Drawing.Size(13, 13);
            this._rst15.State = BEMN.Forms.LedState.Off;
            this._rst15.TabIndex = 13;
            // 
            // _rst9
            // 
            this._rst9.Location = new System.Drawing.Point(62, 38);
            this._rst9.Name = "_rst9";
            this._rst9.Size = new System.Drawing.Size(13, 13);
            this._rst9.State = BEMN.Forms.LedState.Off;
            this._rst9.TabIndex = 1;
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(78, 152);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(41, 13);
            this.label209.TabIndex = 12;
            this.label209.Text = "RST15";
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(78, 38);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(35, 13);
            this.label211.TabIndex = 0;
            this.label211.Text = "RST9";
            // 
            // _rst14
            // 
            this._rst14.Location = new System.Drawing.Point(62, 133);
            this._rst14.Name = "_rst14";
            this._rst14.Size = new System.Drawing.Size(13, 13);
            this._rst14.State = BEMN.Forms.LedState.Off;
            this._rst14.TabIndex = 11;
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(78, 57);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(41, 13);
            this.label214.TabIndex = 2;
            this.label214.Text = "RST10";
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(78, 133);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(41, 13);
            this.label215.TabIndex = 10;
            this.label215.Text = "RST14";
            // 
            // _rst10
            // 
            this._rst10.Location = new System.Drawing.Point(62, 57);
            this._rst10.Name = "_rst10";
            this._rst10.Size = new System.Drawing.Size(13, 13);
            this._rst10.State = BEMN.Forms.LedState.Off;
            this._rst10.TabIndex = 3;
            // 
            // _rst13
            // 
            this._rst13.Location = new System.Drawing.Point(62, 114);
            this._rst13.Name = "_rst13";
            this._rst13.Size = new System.Drawing.Size(13, 13);
            this._rst13.State = BEMN.Forms.LedState.Off;
            this._rst13.TabIndex = 9;
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(78, 76);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(41, 13);
            this.label216.TabIndex = 4;
            this.label216.Text = "RST11";
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(78, 114);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(41, 13);
            this.label217.TabIndex = 8;
            this.label217.Text = "RST13";
            // 
            // _rst11
            // 
            this._rst11.Location = new System.Drawing.Point(62, 76);
            this._rst11.Name = "_rst11";
            this._rst11.Size = new System.Drawing.Size(13, 13);
            this._rst11.State = BEMN.Forms.LedState.Off;
            this._rst11.TabIndex = 5;
            // 
            // _rst12
            // 
            this._rst12.Location = new System.Drawing.Point(62, 95);
            this._rst12.Name = "_rst12";
            this._rst12.Size = new System.Drawing.Size(13, 13);
            this._rst12.State = BEMN.Forms.LedState.Off;
            this._rst12.TabIndex = 7;
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(78, 95);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(41, 13);
            this.label219.TabIndex = 6;
            this.label219.Text = "RST12";
            // 
            // _r56Label
            // 
            this._r56Label.AutoSize = true;
            this._r56Label.Location = new System.Drawing.Point(227, 116);
            this._r56Label.Name = "_r56Label";
            this._r56Label.Size = new System.Drawing.Size(19, 13);
            this._r56Label.TabIndex = 32;
            this._r56Label.Text = "56";
            // 
            // _r58Label
            // 
            this._r58Label.AutoSize = true;
            this._r58Label.Location = new System.Drawing.Point(227, 154);
            this._r58Label.Name = "_r58Label";
            this._r58Label.Size = new System.Drawing.Size(19, 13);
            this._r58Label.TabIndex = 20;
            this._r58Label.Text = "58";
            // 
            // _r70Label
            // 
            this._r70Label.AutoSize = true;
            this._r70Label.Location = new System.Drawing.Point(275, 61);
            this._r70Label.Name = "_r70Label";
            this._r70Label.Size = new System.Drawing.Size(19, 13);
            this._r70Label.TabIndex = 20;
            this._r70Label.Text = "70";
            // 
            // _r18Label
            // 
            this._r18Label.AutoSize = true;
            this._r18Label.Location = new System.Drawing.Point(126, 22);
            this._r18Label.Name = "_r18Label";
            this._r18Label.Size = new System.Drawing.Size(19, 13);
            this._r18Label.TabIndex = 14;
            this._r18Label.Text = "18";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.diod12);
            this.groupBox18.Controls.Add(this.diod11);
            this.groupBox18.Controls.Add(this.diod10);
            this.groupBox18.Controls.Add(this.diod9);
            this.groupBox18.Controls.Add(this.diod8);
            this.groupBox18.Controls.Add(this.diod7);
            this.groupBox18.Controls.Add(this.diod6);
            this.groupBox18.Controls.Add(this.diod5);
            this.groupBox18.Controls.Add(this.diod4);
            this.groupBox18.Controls.Add(this.diod3);
            this.groupBox18.Controls.Add(this.diod2);
            this.groupBox18.Controls.Add(this.diod1);
            this.groupBox18.Controls.Add(this.label190);
            this.groupBox18.Controls.Add(this.label189);
            this.groupBox18.Controls.Add(this.label179);
            this.groupBox18.Controls.Add(this.label180);
            this.groupBox18.Controls.Add(this.label181);
            this.groupBox18.Controls.Add(this.label182);
            this.groupBox18.Controls.Add(this.label183);
            this.groupBox18.Controls.Add(this.label184);
            this.groupBox18.Controls.Add(this.label185);
            this.groupBox18.Controls.Add(this.label186);
            this.groupBox18.Controls.Add(this.label187);
            this.groupBox18.Controls.Add(this.label188);
            this.groupBox18.Location = new System.Drawing.Point(8, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(48, 259);
            this.groupBox18.TabIndex = 55;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Инд.";
            // 
            // diod12
            // 
            this.diod12.BackColor = System.Drawing.Color.Transparent;
            this.diod12.Location = new System.Drawing.Point(6, 235);
            this.diod12.Name = "diod12";
            this.diod12.Size = new System.Drawing.Size(14, 14);
            this.diod12.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod12.TabIndex = 30;
            // 
            // diod11
            // 
            this.diod11.BackColor = System.Drawing.Color.Transparent;
            this.diod11.Location = new System.Drawing.Point(6, 215);
            this.diod11.Name = "diod11";
            this.diod11.Size = new System.Drawing.Size(14, 14);
            this.diod11.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod11.TabIndex = 31;
            // 
            // diod10
            // 
            this.diod10.BackColor = System.Drawing.Color.Transparent;
            this.diod10.Location = new System.Drawing.Point(6, 195);
            this.diod10.Name = "diod10";
            this.diod10.Size = new System.Drawing.Size(14, 14);
            this.diod10.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod10.TabIndex = 32;
            // 
            // diod9
            // 
            this.diod9.BackColor = System.Drawing.Color.Transparent;
            this.diod9.Location = new System.Drawing.Point(6, 175);
            this.diod9.Name = "diod9";
            this.diod9.Size = new System.Drawing.Size(14, 14);
            this.diod9.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod9.TabIndex = 33;
            // 
            // diod8
            // 
            this.diod8.BackColor = System.Drawing.Color.Transparent;
            this.diod8.Location = new System.Drawing.Point(6, 155);
            this.diod8.Name = "diod8";
            this.diod8.Size = new System.Drawing.Size(14, 14);
            this.diod8.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod8.TabIndex = 34;
            // 
            // diod7
            // 
            this.diod7.BackColor = System.Drawing.Color.Transparent;
            this.diod7.Location = new System.Drawing.Point(6, 135);
            this.diod7.Name = "diod7";
            this.diod7.Size = new System.Drawing.Size(14, 14);
            this.diod7.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod7.TabIndex = 35;
            // 
            // diod6
            // 
            this.diod6.BackColor = System.Drawing.Color.Transparent;
            this.diod6.Location = new System.Drawing.Point(6, 115);
            this.diod6.Name = "diod6";
            this.diod6.Size = new System.Drawing.Size(14, 14);
            this.diod6.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod6.TabIndex = 36;
            // 
            // diod5
            // 
            this.diod5.BackColor = System.Drawing.Color.Transparent;
            this.diod5.Location = new System.Drawing.Point(6, 95);
            this.diod5.Name = "diod5";
            this.diod5.Size = new System.Drawing.Size(14, 14);
            this.diod5.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod5.TabIndex = 37;
            // 
            // diod4
            // 
            this.diod4.BackColor = System.Drawing.Color.Transparent;
            this.diod4.Location = new System.Drawing.Point(6, 75);
            this.diod4.Name = "diod4";
            this.diod4.Size = new System.Drawing.Size(14, 14);
            this.diod4.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod4.TabIndex = 38;
            // 
            // diod3
            // 
            this.diod3.BackColor = System.Drawing.Color.Transparent;
            this.diod3.Location = new System.Drawing.Point(6, 55);
            this.diod3.Name = "diod3";
            this.diod3.Size = new System.Drawing.Size(14, 14);
            this.diod3.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod3.TabIndex = 39;
            // 
            // diod2
            // 
            this.diod2.BackColor = System.Drawing.Color.Transparent;
            this.diod2.Location = new System.Drawing.Point(6, 35);
            this.diod2.Name = "diod2";
            this.diod2.Size = new System.Drawing.Size(14, 14);
            this.diod2.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod2.TabIndex = 40;
            // 
            // diod1
            // 
            this.diod1.BackColor = System.Drawing.Color.Transparent;
            this.diod1.Location = new System.Drawing.Point(6, 15);
            this.diod1.Name = "diod1";
            this.diod1.Size = new System.Drawing.Size(14, 14);
            this.diod1.State = BEMN.Forms.DiodState.NOT_SET;
            this.diod1.TabIndex = 41;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(25, 216);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(19, 13);
            this.label190.TabIndex = 28;
            this.label190.Text = "11";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(25, 116);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(13, 13);
            this.label189.TabIndex = 26;
            this.label189.Text = "6";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(25, 196);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(19, 13);
            this.label179.TabIndex = 24;
            this.label179.Text = "10";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(25, 176);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(13, 13);
            this.label180.TabIndex = 22;
            this.label180.Text = "9";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(25, 156);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(13, 13);
            this.label181.TabIndex = 20;
            this.label181.Text = "8";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(25, 136);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(13, 13);
            this.label182.TabIndex = 18;
            this.label182.Text = "7";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(25, 236);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(19, 13);
            this.label183.TabIndex = 16;
            this.label183.Text = "12";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(25, 96);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(13, 13);
            this.label184.TabIndex = 8;
            this.label184.Text = "5";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(25, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(13, 13);
            this.label185.TabIndex = 6;
            this.label185.Text = "4";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(25, 56);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(13, 13);
            this.label186.TabIndex = 4;
            this.label186.Text = "3";
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(25, 36);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(13, 13);
            this.label187.TabIndex = 2;
            this.label187.Text = "2";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(25, 16);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(13, 13);
            this.label188.TabIndex = 0;
            this.label188.Text = "1";
            // 
            // _r59Label
            // 
            this._r59Label.AutoSize = true;
            this._r59Label.Location = new System.Drawing.Point(227, 173);
            this._r59Label.Name = "_r59Label";
            this._r59Label.Size = new System.Drawing.Size(19, 13);
            this._r59Label.TabIndex = 22;
            this._r59Label.Text = "59";
            // 
            // _r71Label
            // 
            this._r71Label.AutoSize = true;
            this._r71Label.Location = new System.Drawing.Point(275, 79);
            this._r71Label.Name = "_r71Label";
            this._r71Label.Size = new System.Drawing.Size(19, 13);
            this._r71Label.TabIndex = 22;
            this._r71Label.Text = "71";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(84, 326);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(19, 13);
            this.label162.TabIndex = 12;
            this.label162.Text = "17";
            // 
            // _diskretInputGB
            // 
            this._diskretInputGB.Controls.Add(this._d48);
            this._diskretInputGB.Controls.Add(this._d40);
            this._diskretInputGB.Controls.Add(this._d24);
            this._diskretInputGB.Controls.Add(this._d48Label);
            this._diskretInputGB.Controls.Add(this._d40Label);
            this._diskretInputGB.Controls.Add(this._d8);
            this._diskretInputGB.Controls.Add(this._d47);
            this._diskretInputGB.Controls.Add(this._d39);
            this._diskretInputGB.Controls.Add(this._d24Label);
            this._diskretInputGB.Controls.Add(this._d47Label);
            this._diskretInputGB.Controls.Add(this._d39Label);
            this._diskretInputGB.Controls.Add(this._d54);
            this._diskretInputGB.Controls.Add(this._d46);
            this._diskretInputGB.Controls.Add(this._d38);
            this._diskretInputGB.Controls.Add(this._d23);
            this._diskretInputGB.Controls.Add(this._d46Label);
            this._diskretInputGB.Controls.Add(this._d38Label);
            this._diskretInputGB.Controls.Add(this._d54Label);
            this._diskretInputGB.Controls.Add(this._d8Label);
            this._diskretInputGB.Controls.Add(this._d45);
            this._diskretInputGB.Controls.Add(this._d37);
            this._diskretInputGB.Controls.Add(this._d23Label);
            this._diskretInputGB.Controls.Add(this._d45Label);
            this._diskretInputGB.Controls.Add(this._d53);
            this._diskretInputGB.Controls.Add(this._d37Label);
            this._diskretInputGB.Controls.Add(this._d22);
            this._diskretInputGB.Controls.Add(this._d52);
            this._diskretInputGB.Controls.Add(this._d44);
            this._diskretInputGB.Controls.Add(this._d53Label);
            this._diskretInputGB.Controls.Add(this._d7);
            this._diskretInputGB.Controls.Add(this._d52Label);
            this._diskretInputGB.Controls.Add(this._d44Label);
            this._diskretInputGB.Controls.Add(this._d22Label);
            this._diskretInputGB.Controls.Add(this._d51);
            this._diskretInputGB.Controls.Add(this._d43);
            this._diskretInputGB.Controls.Add(this._d7Label);
            this._diskretInputGB.Controls.Add(this._d51Label);
            this._diskretInputGB.Controls.Add(this._d36);
            this._diskretInputGB.Controls.Add(this._d43Label);
            this._diskretInputGB.Controls.Add(this._d21);
            this._diskretInputGB.Controls.Add(this._d50);
            this._diskretInputGB.Controls.Add(this._d42);
            this._diskretInputGB.Controls.Add(this._d36Label);
            this._diskretInputGB.Controls.Add(this._d34);
            this._diskretInputGB.Controls.Add(this._d1);
            this._diskretInputGB.Controls.Add(this._d35);
            this._diskretInputGB.Controls.Add(this._d50Label);
            this._diskretInputGB.Controls.Add(this._d42Label);
            this._diskretInputGB.Controls.Add(this._d34Label);
            this._diskretInputGB.Controls.Add(this._d21Label);
            this._diskretInputGB.Controls.Add(this._k2);
            this._diskretInputGB.Controls.Add(this._d35Label);
            this._diskretInputGB.Controls.Add(this._k2Label);
            this._diskretInputGB.Controls.Add(this._k1);
            this._diskretInputGB.Controls.Add(this._k1Label);
            this._diskretInputGB.Controls.Add(this._d49);
            this._diskretInputGB.Controls.Add(this._d49Label);
            this._diskretInputGB.Controls.Add(this._d41);
            this._diskretInputGB.Controls.Add(this._d41Label);
            this._diskretInputGB.Controls.Add(this._d33);
            this._diskretInputGB.Controls.Add(this._d33Label);
            this._diskretInputGB.Controls.Add(this._d6);
            this._diskretInputGB.Controls.Add(this._d20);
            this._diskretInputGB.Controls.Add(this._d32);
            this._diskretInputGB.Controls.Add(this._d1Label);
            this._diskretInputGB.Controls.Add(this._d32Label);
            this._diskretInputGB.Controls.Add(this._d20Label);
            this._diskretInputGB.Controls.Add(this._d31);
            this._diskretInputGB.Controls.Add(this._d6Label);
            this._diskretInputGB.Controls.Add(this._d31Label);
            this._diskretInputGB.Controls.Add(this._d19);
            this._diskretInputGB.Controls.Add(this._d30);
            this._diskretInputGB.Controls.Add(this._d2Label);
            this._diskretInputGB.Controls.Add(this._d30Label);
            this._diskretInputGB.Controls.Add(this._d19Label);
            this._diskretInputGB.Controls.Add(this._d29);
            this._diskretInputGB.Controls.Add(this._d5);
            this._diskretInputGB.Controls.Add(this._d29Label);
            this._diskretInputGB.Controls.Add(this._d28);
            this._diskretInputGB.Controls.Add(this._d2);
            this._diskretInputGB.Controls.Add(this._d28Label);
            this._diskretInputGB.Controls.Add(this._d27);
            this._diskretInputGB.Controls.Add(this._d5Label);
            this._diskretInputGB.Controls.Add(this._d27Label);
            this._diskretInputGB.Controls.Add(this._d17);
            this._diskretInputGB.Controls.Add(this._d26);
            this._diskretInputGB.Controls.Add(this._d18);
            this._diskretInputGB.Controls.Add(this._d17Label);
            this._diskretInputGB.Controls.Add(this._d26Label);
            this._diskretInputGB.Controls.Add(this._d3Label);
            this._diskretInputGB.Controls.Add(this._d25);
            this._diskretInputGB.Controls.Add(this._d25Label);
            this._diskretInputGB.Controls.Add(this._d18Label);
            this._diskretInputGB.Controls.Add(this._d4);
            this._diskretInputGB.Controls.Add(this._d16);
            this._diskretInputGB.Controls.Add(this._d3);
            this._diskretInputGB.Controls.Add(this._d16Label);
            this._diskretInputGB.Controls.Add(this._d4Label);
            this._diskretInputGB.Controls.Add(this._d15);
            this._diskretInputGB.Controls.Add(this._d9);
            this._diskretInputGB.Controls.Add(this._d15Label);
            this._diskretInputGB.Controls.Add(this._d9Label);
            this._diskretInputGB.Controls.Add(this._d14);
            this._diskretInputGB.Controls.Add(this._d10Label);
            this._diskretInputGB.Controls.Add(this._d14Label);
            this._diskretInputGB.Controls.Add(this._d10);
            this._diskretInputGB.Controls.Add(this._d13);
            this._diskretInputGB.Controls.Add(this._d11Label);
            this._diskretInputGB.Controls.Add(this._d13Label);
            this._diskretInputGB.Controls.Add(this._d11);
            this._diskretInputGB.Controls.Add(this._d12);
            this._diskretInputGB.Controls.Add(this._d12Label);
            this._diskretInputGB.Location = new System.Drawing.Point(329, 3);
            this._diskretInputGB.Name = "_diskretInputGB";
            this._diskretInputGB.Size = new System.Drawing.Size(218, 352);
            this._diskretInputGB.TabIndex = 53;
            this._diskretInputGB.TabStop = false;
            this._diskretInputGB.Text = "Дискретные входы";
            // 
            // _d48
            // 
            this._d48.Location = new System.Drawing.Point(109, 266);
            this._d48.Name = "_d48";
            this._d48.Size = new System.Drawing.Size(13, 13);
            this._d48.State = BEMN.Forms.LedState.Off;
            this._d48.TabIndex = 31;
            // 
            // _d40
            // 
            this._d40.Location = new System.Drawing.Point(109, 114);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(13, 13);
            this._d40.State = BEMN.Forms.LedState.Off;
            this._d40.TabIndex = 31;
            // 
            // _d24
            // 
            this._d24.Location = new System.Drawing.Point(59, 133);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(13, 13);
            this._d24.State = BEMN.Forms.LedState.Off;
            this._d24.TabIndex = 31;
            // 
            // _d48Label
            // 
            this._d48Label.AutoSize = true;
            this._d48Label.Location = new System.Drawing.Point(125, 266);
            this._d48Label.Name = "_d48Label";
            this._d48Label.Size = new System.Drawing.Size(28, 13);
            this._d48Label.TabIndex = 30;
            this._d48Label.Text = "Д48";
            // 
            // _d40Label
            // 
            this._d40Label.AutoSize = true;
            this._d40Label.Location = new System.Drawing.Point(125, 114);
            this._d40Label.Name = "_d40Label";
            this._d40Label.Size = new System.Drawing.Size(28, 13);
            this._d40Label.TabIndex = 30;
            this._d40Label.Text = "Д40";
            // 
            // _d8
            // 
            this._d8.Location = new System.Drawing.Point(6, 152);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(13, 13);
            this._d8.State = BEMN.Forms.LedState.Off;
            this._d8.TabIndex = 15;
            // 
            // _d47
            // 
            this._d47.Location = new System.Drawing.Point(109, 247);
            this._d47.Name = "_d47";
            this._d47.Size = new System.Drawing.Size(13, 13);
            this._d47.State = BEMN.Forms.LedState.Off;
            this._d47.TabIndex = 29;
            // 
            // _d39
            // 
            this._d39.Location = new System.Drawing.Point(109, 95);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(13, 13);
            this._d39.State = BEMN.Forms.LedState.Off;
            this._d39.TabIndex = 29;
            // 
            // _d24Label
            // 
            this._d24Label.AutoSize = true;
            this._d24Label.Location = new System.Drawing.Point(75, 133);
            this._d24Label.Name = "_d24Label";
            this._d24Label.Size = new System.Drawing.Size(28, 13);
            this._d24Label.TabIndex = 30;
            this._d24Label.Text = "Д24";
            // 
            // _d47Label
            // 
            this._d47Label.AutoSize = true;
            this._d47Label.Location = new System.Drawing.Point(125, 247);
            this._d47Label.Name = "_d47Label";
            this._d47Label.Size = new System.Drawing.Size(28, 13);
            this._d47Label.TabIndex = 28;
            this._d47Label.Text = "Д47";
            // 
            // _d39Label
            // 
            this._d39Label.AutoSize = true;
            this._d39Label.Location = new System.Drawing.Point(125, 95);
            this._d39Label.Name = "_d39Label";
            this._d39Label.Size = new System.Drawing.Size(28, 13);
            this._d39Label.TabIndex = 28;
            this._d39Label.Text = "Д39";
            // 
            // _d54
            // 
            this._d54.Location = new System.Drawing.Point(159, 57);
            this._d54.Name = "_d54";
            this._d54.Size = new System.Drawing.Size(13, 13);
            this._d54.State = BEMN.Forms.LedState.Off;
            this._d54.TabIndex = 27;
            // 
            // _d46
            // 
            this._d46.Location = new System.Drawing.Point(109, 228);
            this._d46.Name = "_d46";
            this._d46.Size = new System.Drawing.Size(13, 13);
            this._d46.State = BEMN.Forms.LedState.Off;
            this._d46.TabIndex = 27;
            // 
            // _d38
            // 
            this._d38.Location = new System.Drawing.Point(109, 76);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(13, 13);
            this._d38.State = BEMN.Forms.LedState.Off;
            this._d38.TabIndex = 27;
            // 
            // _d23
            // 
            this._d23.Location = new System.Drawing.Point(59, 114);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(13, 13);
            this._d23.State = BEMN.Forms.LedState.Off;
            this._d23.TabIndex = 29;
            // 
            // _d46Label
            // 
            this._d46Label.AutoSize = true;
            this._d46Label.Location = new System.Drawing.Point(125, 228);
            this._d46Label.Name = "_d46Label";
            this._d46Label.Size = new System.Drawing.Size(28, 13);
            this._d46Label.TabIndex = 26;
            this._d46Label.Text = "Д46";
            // 
            // _d38Label
            // 
            this._d38Label.AutoSize = true;
            this._d38Label.Location = new System.Drawing.Point(125, 76);
            this._d38Label.Name = "_d38Label";
            this._d38Label.Size = new System.Drawing.Size(28, 13);
            this._d38Label.TabIndex = 26;
            this._d38Label.Text = "Д38";
            // 
            // _d54Label
            // 
            this._d54Label.AutoSize = true;
            this._d54Label.Location = new System.Drawing.Point(175, 57);
            this._d54Label.Name = "_d54Label";
            this._d54Label.Size = new System.Drawing.Size(28, 13);
            this._d54Label.TabIndex = 26;
            this._d54Label.Text = "Д54";
            // 
            // _d8Label
            // 
            this._d8Label.AutoSize = true;
            this._d8Label.Location = new System.Drawing.Point(25, 152);
            this._d8Label.Name = "_d8Label";
            this._d8Label.Size = new System.Drawing.Size(22, 13);
            this._d8Label.TabIndex = 14;
            this._d8Label.Text = "Д8";
            // 
            // _d45
            // 
            this._d45.Location = new System.Drawing.Point(109, 209);
            this._d45.Name = "_d45";
            this._d45.Size = new System.Drawing.Size(13, 13);
            this._d45.State = BEMN.Forms.LedState.Off;
            this._d45.TabIndex = 25;
            // 
            // _d37
            // 
            this._d37.Location = new System.Drawing.Point(109, 57);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(13, 13);
            this._d37.State = BEMN.Forms.LedState.Off;
            this._d37.TabIndex = 25;
            // 
            // _d23Label
            // 
            this._d23Label.AutoSize = true;
            this._d23Label.Location = new System.Drawing.Point(75, 114);
            this._d23Label.Name = "_d23Label";
            this._d23Label.Size = new System.Drawing.Size(28, 13);
            this._d23Label.TabIndex = 28;
            this._d23Label.Text = "Д23";
            // 
            // _d45Label
            // 
            this._d45Label.AutoSize = true;
            this._d45Label.Location = new System.Drawing.Point(125, 209);
            this._d45Label.Name = "_d45Label";
            this._d45Label.Size = new System.Drawing.Size(28, 13);
            this._d45Label.TabIndex = 24;
            this._d45Label.Text = "Д45";
            // 
            // _d53
            // 
            this._d53.Location = new System.Drawing.Point(159, 38);
            this._d53.Name = "_d53";
            this._d53.Size = new System.Drawing.Size(13, 13);
            this._d53.State = BEMN.Forms.LedState.Off;
            this._d53.TabIndex = 25;
            // 
            // _d37Label
            // 
            this._d37Label.AutoSize = true;
            this._d37Label.Location = new System.Drawing.Point(125, 57);
            this._d37Label.Name = "_d37Label";
            this._d37Label.Size = new System.Drawing.Size(28, 13);
            this._d37Label.TabIndex = 24;
            this._d37Label.Text = "Д37";
            // 
            // _d22
            // 
            this._d22.Location = new System.Drawing.Point(59, 95);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(13, 13);
            this._d22.State = BEMN.Forms.LedState.Off;
            this._d22.TabIndex = 27;
            // 
            // _d52
            // 
            this._d52.Location = new System.Drawing.Point(159, 19);
            this._d52.Name = "_d52";
            this._d52.Size = new System.Drawing.Size(13, 13);
            this._d52.State = BEMN.Forms.LedState.Off;
            this._d52.TabIndex = 23;
            // 
            // _d44
            // 
            this._d44.Location = new System.Drawing.Point(109, 190);
            this._d44.Name = "_d44";
            this._d44.Size = new System.Drawing.Size(13, 13);
            this._d44.State = BEMN.Forms.LedState.Off;
            this._d44.TabIndex = 23;
            // 
            // _d53Label
            // 
            this._d53Label.AutoSize = true;
            this._d53Label.Location = new System.Drawing.Point(175, 38);
            this._d53Label.Name = "_d53Label";
            this._d53Label.Size = new System.Drawing.Size(28, 13);
            this._d53Label.TabIndex = 24;
            this._d53Label.Text = "Д53";
            // 
            // _d7
            // 
            this._d7.Location = new System.Drawing.Point(6, 133);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(13, 13);
            this._d7.State = BEMN.Forms.LedState.Off;
            this._d7.TabIndex = 13;
            // 
            // _d52Label
            // 
            this._d52Label.AutoSize = true;
            this._d52Label.Location = new System.Drawing.Point(175, 19);
            this._d52Label.Name = "_d52Label";
            this._d52Label.Size = new System.Drawing.Size(28, 13);
            this._d52Label.TabIndex = 22;
            this._d52Label.Text = "Д52";
            // 
            // _d44Label
            // 
            this._d44Label.AutoSize = true;
            this._d44Label.Location = new System.Drawing.Point(125, 190);
            this._d44Label.Name = "_d44Label";
            this._d44Label.Size = new System.Drawing.Size(28, 13);
            this._d44Label.TabIndex = 22;
            this._d44Label.Text = "Д44";
            // 
            // _d22Label
            // 
            this._d22Label.AutoSize = true;
            this._d22Label.Location = new System.Drawing.Point(75, 95);
            this._d22Label.Name = "_d22Label";
            this._d22Label.Size = new System.Drawing.Size(28, 13);
            this._d22Label.TabIndex = 26;
            this._d22Label.Text = "Д22";
            // 
            // _d51
            // 
            this._d51.Location = new System.Drawing.Point(109, 323);
            this._d51.Name = "_d51";
            this._d51.Size = new System.Drawing.Size(13, 13);
            this._d51.State = BEMN.Forms.LedState.Off;
            this._d51.TabIndex = 21;
            // 
            // _d43
            // 
            this._d43.Location = new System.Drawing.Point(109, 171);
            this._d43.Name = "_d43";
            this._d43.Size = new System.Drawing.Size(13, 13);
            this._d43.State = BEMN.Forms.LedState.Off;
            this._d43.TabIndex = 21;
            // 
            // _d7Label
            // 
            this._d7Label.AutoSize = true;
            this._d7Label.Location = new System.Drawing.Point(25, 133);
            this._d7Label.Name = "_d7Label";
            this._d7Label.Size = new System.Drawing.Size(22, 13);
            this._d7Label.TabIndex = 12;
            this._d7Label.Text = "Д7";
            // 
            // _d51Label
            // 
            this._d51Label.AutoSize = true;
            this._d51Label.Location = new System.Drawing.Point(125, 323);
            this._d51Label.Name = "_d51Label";
            this._d51Label.Size = new System.Drawing.Size(28, 13);
            this._d51Label.TabIndex = 20;
            this._d51Label.Text = "Д51";
            // 
            // _d36
            // 
            this._d36.Location = new System.Drawing.Point(109, 38);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(13, 13);
            this._d36.State = BEMN.Forms.LedState.Off;
            this._d36.TabIndex = 23;
            // 
            // _d43Label
            // 
            this._d43Label.AutoSize = true;
            this._d43Label.Location = new System.Drawing.Point(125, 171);
            this._d43Label.Name = "_d43Label";
            this._d43Label.Size = new System.Drawing.Size(28, 13);
            this._d43Label.TabIndex = 20;
            this._d43Label.Text = "Д43";
            // 
            // _d21
            // 
            this._d21.Location = new System.Drawing.Point(59, 76);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(13, 13);
            this._d21.State = BEMN.Forms.LedState.Off;
            this._d21.TabIndex = 25;
            // 
            // _d50
            // 
            this._d50.Location = new System.Drawing.Point(109, 304);
            this._d50.Name = "_d50";
            this._d50.Size = new System.Drawing.Size(13, 13);
            this._d50.State = BEMN.Forms.LedState.Off;
            this._d50.TabIndex = 19;
            // 
            // _d42
            // 
            this._d42.Location = new System.Drawing.Point(109, 152);
            this._d42.Name = "_d42";
            this._d42.Size = new System.Drawing.Size(13, 13);
            this._d42.State = BEMN.Forms.LedState.Off;
            this._d42.TabIndex = 19;
            // 
            // _d36Label
            // 
            this._d36Label.AutoSize = true;
            this._d36Label.Location = new System.Drawing.Point(125, 38);
            this._d36Label.Name = "_d36Label";
            this._d36Label.Size = new System.Drawing.Size(28, 13);
            this._d36Label.TabIndex = 22;
            this._d36Label.Text = "Д36";
            // 
            // _d34
            // 
            this._d34.Location = new System.Drawing.Point(59, 323);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(13, 13);
            this._d34.State = BEMN.Forms.LedState.Off;
            this._d34.TabIndex = 19;
            // 
            // _d1
            // 
            this._d1.Location = new System.Drawing.Point(6, 19);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(13, 13);
            this._d1.State = BEMN.Forms.LedState.Off;
            this._d1.TabIndex = 1;
            // 
            // _d35
            // 
            this._d35.Location = new System.Drawing.Point(109, 19);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(13, 13);
            this._d35.State = BEMN.Forms.LedState.Off;
            this._d35.TabIndex = 21;
            // 
            // _d50Label
            // 
            this._d50Label.AutoSize = true;
            this._d50Label.Location = new System.Drawing.Point(125, 304);
            this._d50Label.Name = "_d50Label";
            this._d50Label.Size = new System.Drawing.Size(28, 13);
            this._d50Label.TabIndex = 18;
            this._d50Label.Text = "Д50";
            // 
            // _d42Label
            // 
            this._d42Label.AutoSize = true;
            this._d42Label.Location = new System.Drawing.Point(125, 152);
            this._d42Label.Name = "_d42Label";
            this._d42Label.Size = new System.Drawing.Size(28, 13);
            this._d42Label.TabIndex = 18;
            this._d42Label.Text = "Д42";
            // 
            // _d34Label
            // 
            this._d34Label.AutoSize = true;
            this._d34Label.Location = new System.Drawing.Point(75, 323);
            this._d34Label.Name = "_d34Label";
            this._d34Label.Size = new System.Drawing.Size(28, 13);
            this._d34Label.TabIndex = 18;
            this._d34Label.Text = "Д34";
            // 
            // _d21Label
            // 
            this._d21Label.AutoSize = true;
            this._d21Label.Location = new System.Drawing.Point(75, 76);
            this._d21Label.Name = "_d21Label";
            this._d21Label.Size = new System.Drawing.Size(28, 13);
            this._d21Label.TabIndex = 24;
            this._d21Label.Text = "Д21";
            // 
            // _k2
            // 
            this._k2.Location = new System.Drawing.Point(159, 95);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(13, 13);
            this._k2.State = BEMN.Forms.LedState.Off;
            this._k2.TabIndex = 17;
            // 
            // _d35Label
            // 
            this._d35Label.AutoSize = true;
            this._d35Label.Location = new System.Drawing.Point(125, 19);
            this._d35Label.Name = "_d35Label";
            this._d35Label.Size = new System.Drawing.Size(28, 13);
            this._d35Label.TabIndex = 20;
            this._d35Label.Text = "Д35";
            // 
            // _k2Label
            // 
            this._k2Label.AutoSize = true;
            this._k2Label.Location = new System.Drawing.Point(178, 94);
            this._k2Label.Name = "_k2Label";
            this._k2Label.Size = new System.Drawing.Size(20, 13);
            this._k2Label.TabIndex = 16;
            this._k2Label.Text = "K2";
            // 
            // _k1
            // 
            this._k1.Location = new System.Drawing.Point(159, 76);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(13, 13);
            this._k1.State = BEMN.Forms.LedState.Off;
            this._k1.TabIndex = 17;
            // 
            // _k1Label
            // 
            this._k1Label.AutoSize = true;
            this._k1Label.Location = new System.Drawing.Point(178, 75);
            this._k1Label.Name = "_k1Label";
            this._k1Label.Size = new System.Drawing.Size(20, 13);
            this._k1Label.TabIndex = 16;
            this._k1Label.Text = "K1";
            // 
            // _d49
            // 
            this._d49.Location = new System.Drawing.Point(109, 285);
            this._d49.Name = "_d49";
            this._d49.Size = new System.Drawing.Size(13, 13);
            this._d49.State = BEMN.Forms.LedState.Off;
            this._d49.TabIndex = 17;
            // 
            // _d49Label
            // 
            this._d49Label.AutoSize = true;
            this._d49Label.Location = new System.Drawing.Point(125, 285);
            this._d49Label.Name = "_d49Label";
            this._d49Label.Size = new System.Drawing.Size(28, 13);
            this._d49Label.TabIndex = 16;
            this._d49Label.Text = "Д49";
            // 
            // _d41
            // 
            this._d41.Location = new System.Drawing.Point(109, 133);
            this._d41.Name = "_d41";
            this._d41.Size = new System.Drawing.Size(13, 13);
            this._d41.State = BEMN.Forms.LedState.Off;
            this._d41.TabIndex = 17;
            // 
            // _d41Label
            // 
            this._d41Label.AutoSize = true;
            this._d41Label.Location = new System.Drawing.Point(125, 133);
            this._d41Label.Name = "_d41Label";
            this._d41Label.Size = new System.Drawing.Size(28, 13);
            this._d41Label.TabIndex = 16;
            this._d41Label.Text = "Д41";
            // 
            // _d33
            // 
            this._d33.Location = new System.Drawing.Point(59, 304);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(13, 13);
            this._d33.State = BEMN.Forms.LedState.Off;
            this._d33.TabIndex = 17;
            // 
            // _d33Label
            // 
            this._d33Label.AutoSize = true;
            this._d33Label.Location = new System.Drawing.Point(75, 304);
            this._d33Label.Name = "_d33Label";
            this._d33Label.Size = new System.Drawing.Size(28, 13);
            this._d33Label.TabIndex = 16;
            this._d33Label.Text = "Д33";
            // 
            // _d6
            // 
            this._d6.Location = new System.Drawing.Point(6, 114);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(13, 13);
            this._d6.State = BEMN.Forms.LedState.Off;
            this._d6.TabIndex = 11;
            // 
            // _d20
            // 
            this._d20.Location = new System.Drawing.Point(59, 57);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(13, 13);
            this._d20.State = BEMN.Forms.LedState.Off;
            this._d20.TabIndex = 23;
            // 
            // _d32
            // 
            this._d32.Location = new System.Drawing.Point(59, 285);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(13, 13);
            this._d32.State = BEMN.Forms.LedState.Off;
            this._d32.TabIndex = 15;
            // 
            // _d1Label
            // 
            this._d1Label.AutoSize = true;
            this._d1Label.Location = new System.Drawing.Point(25, 19);
            this._d1Label.Name = "_d1Label";
            this._d1Label.Size = new System.Drawing.Size(22, 13);
            this._d1Label.TabIndex = 0;
            this._d1Label.Text = "Д1";
            // 
            // _d32Label
            // 
            this._d32Label.AutoSize = true;
            this._d32Label.Location = new System.Drawing.Point(75, 285);
            this._d32Label.Name = "_d32Label";
            this._d32Label.Size = new System.Drawing.Size(28, 13);
            this._d32Label.TabIndex = 14;
            this._d32Label.Text = "Д32";
            // 
            // _d20Label
            // 
            this._d20Label.AutoSize = true;
            this._d20Label.Location = new System.Drawing.Point(75, 57);
            this._d20Label.Name = "_d20Label";
            this._d20Label.Size = new System.Drawing.Size(28, 13);
            this._d20Label.TabIndex = 22;
            this._d20Label.Text = "Д20";
            // 
            // _d31
            // 
            this._d31.Location = new System.Drawing.Point(59, 266);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(13, 13);
            this._d31.State = BEMN.Forms.LedState.Off;
            this._d31.TabIndex = 13;
            // 
            // _d6Label
            // 
            this._d6Label.AutoSize = true;
            this._d6Label.Location = new System.Drawing.Point(25, 114);
            this._d6Label.Name = "_d6Label";
            this._d6Label.Size = new System.Drawing.Size(22, 13);
            this._d6Label.TabIndex = 10;
            this._d6Label.Text = "Д6";
            // 
            // _d31Label
            // 
            this._d31Label.AutoSize = true;
            this._d31Label.Location = new System.Drawing.Point(75, 266);
            this._d31Label.Name = "_d31Label";
            this._d31Label.Size = new System.Drawing.Size(28, 13);
            this._d31Label.TabIndex = 12;
            this._d31Label.Text = "Д31";
            // 
            // _d19
            // 
            this._d19.Location = new System.Drawing.Point(59, 38);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(13, 13);
            this._d19.State = BEMN.Forms.LedState.Off;
            this._d19.TabIndex = 21;
            // 
            // _d30
            // 
            this._d30.Location = new System.Drawing.Point(59, 247);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(13, 13);
            this._d30.State = BEMN.Forms.LedState.Off;
            this._d30.TabIndex = 11;
            // 
            // _d2Label
            // 
            this._d2Label.AutoSize = true;
            this._d2Label.Location = new System.Drawing.Point(25, 38);
            this._d2Label.Name = "_d2Label";
            this._d2Label.Size = new System.Drawing.Size(22, 13);
            this._d2Label.TabIndex = 2;
            this._d2Label.Text = "Д2";
            // 
            // _d30Label
            // 
            this._d30Label.AutoSize = true;
            this._d30Label.Location = new System.Drawing.Point(75, 247);
            this._d30Label.Name = "_d30Label";
            this._d30Label.Size = new System.Drawing.Size(28, 13);
            this._d30Label.TabIndex = 10;
            this._d30Label.Text = "Д30";
            // 
            // _d19Label
            // 
            this._d19Label.AutoSize = true;
            this._d19Label.Location = new System.Drawing.Point(75, 38);
            this._d19Label.Name = "_d19Label";
            this._d19Label.Size = new System.Drawing.Size(28, 13);
            this._d19Label.TabIndex = 20;
            this._d19Label.Text = "Д19";
            // 
            // _d29
            // 
            this._d29.Location = new System.Drawing.Point(59, 228);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(13, 13);
            this._d29.State = BEMN.Forms.LedState.Off;
            this._d29.TabIndex = 9;
            // 
            // _d5
            // 
            this._d5.Location = new System.Drawing.Point(6, 95);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(13, 13);
            this._d5.State = BEMN.Forms.LedState.Off;
            this._d5.TabIndex = 9;
            // 
            // _d29Label
            // 
            this._d29Label.AutoSize = true;
            this._d29Label.Location = new System.Drawing.Point(75, 228);
            this._d29Label.Name = "_d29Label";
            this._d29Label.Size = new System.Drawing.Size(28, 13);
            this._d29Label.TabIndex = 8;
            this._d29Label.Text = "Д29";
            // 
            // _d28
            // 
            this._d28.Location = new System.Drawing.Point(59, 209);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(13, 13);
            this._d28.State = BEMN.Forms.LedState.Off;
            this._d28.TabIndex = 7;
            // 
            // _d2
            // 
            this._d2.Location = new System.Drawing.Point(6, 38);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(13, 13);
            this._d2.State = BEMN.Forms.LedState.Off;
            this._d2.TabIndex = 3;
            // 
            // _d28Label
            // 
            this._d28Label.AutoSize = true;
            this._d28Label.Location = new System.Drawing.Point(75, 209);
            this._d28Label.Name = "_d28Label";
            this._d28Label.Size = new System.Drawing.Size(28, 13);
            this._d28Label.TabIndex = 6;
            this._d28Label.Text = "Д28";
            // 
            // _d27
            // 
            this._d27.Location = new System.Drawing.Point(59, 190);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(13, 13);
            this._d27.State = BEMN.Forms.LedState.Off;
            this._d27.TabIndex = 5;
            // 
            // _d5Label
            // 
            this._d5Label.AutoSize = true;
            this._d5Label.Location = new System.Drawing.Point(25, 96);
            this._d5Label.Name = "_d5Label";
            this._d5Label.Size = new System.Drawing.Size(22, 13);
            this._d5Label.TabIndex = 8;
            this._d5Label.Text = "Д5";
            // 
            // _d27Label
            // 
            this._d27Label.AutoSize = true;
            this._d27Label.Location = new System.Drawing.Point(75, 190);
            this._d27Label.Name = "_d27Label";
            this._d27Label.Size = new System.Drawing.Size(28, 13);
            this._d27Label.TabIndex = 4;
            this._d27Label.Text = "Д27";
            // 
            // _d17
            // 
            this._d17.Location = new System.Drawing.Point(6, 323);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(13, 13);
            this._d17.State = BEMN.Forms.LedState.Off;
            this._d17.TabIndex = 17;
            // 
            // _d26
            // 
            this._d26.Location = new System.Drawing.Point(59, 171);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(13, 13);
            this._d26.State = BEMN.Forms.LedState.Off;
            this._d26.TabIndex = 3;
            // 
            // _d18
            // 
            this._d18.Location = new System.Drawing.Point(59, 19);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(13, 13);
            this._d18.State = BEMN.Forms.LedState.Off;
            this._d18.TabIndex = 19;
            // 
            // _d17Label
            // 
            this._d17Label.AutoSize = true;
            this._d17Label.Location = new System.Drawing.Point(22, 323);
            this._d17Label.Name = "_d17Label";
            this._d17Label.Size = new System.Drawing.Size(28, 13);
            this._d17Label.TabIndex = 16;
            this._d17Label.Text = "Д17";
            // 
            // _d26Label
            // 
            this._d26Label.AutoSize = true;
            this._d26Label.Location = new System.Drawing.Point(75, 171);
            this._d26Label.Name = "_d26Label";
            this._d26Label.Size = new System.Drawing.Size(28, 13);
            this._d26Label.TabIndex = 2;
            this._d26Label.Text = "Д26";
            // 
            // _d3Label
            // 
            this._d3Label.AutoSize = true;
            this._d3Label.Location = new System.Drawing.Point(25, 57);
            this._d3Label.Name = "_d3Label";
            this._d3Label.Size = new System.Drawing.Size(22, 13);
            this._d3Label.TabIndex = 4;
            this._d3Label.Text = "Д3";
            // 
            // _d25
            // 
            this._d25.Location = new System.Drawing.Point(59, 152);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(13, 13);
            this._d25.State = BEMN.Forms.LedState.Off;
            this._d25.TabIndex = 1;
            // 
            // _d25Label
            // 
            this._d25Label.AutoSize = true;
            this._d25Label.Location = new System.Drawing.Point(75, 152);
            this._d25Label.Name = "_d25Label";
            this._d25Label.Size = new System.Drawing.Size(28, 13);
            this._d25Label.TabIndex = 0;
            this._d25Label.Text = "Д25";
            // 
            // _d18Label
            // 
            this._d18Label.AutoSize = true;
            this._d18Label.Location = new System.Drawing.Point(75, 19);
            this._d18Label.Name = "_d18Label";
            this._d18Label.Size = new System.Drawing.Size(28, 13);
            this._d18Label.TabIndex = 18;
            this._d18Label.Text = "Д18";
            // 
            // _d4
            // 
            this._d4.Location = new System.Drawing.Point(6, 76);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(13, 13);
            this._d4.State = BEMN.Forms.LedState.Off;
            this._d4.TabIndex = 7;
            // 
            // _d16
            // 
            this._d16.Location = new System.Drawing.Point(6, 304);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(13, 13);
            this._d16.State = BEMN.Forms.LedState.Off;
            this._d16.TabIndex = 15;
            // 
            // _d3
            // 
            this._d3.Location = new System.Drawing.Point(6, 57);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(13, 13);
            this._d3.State = BEMN.Forms.LedState.Off;
            this._d3.TabIndex = 5;
            // 
            // _d16Label
            // 
            this._d16Label.AutoSize = true;
            this._d16Label.Location = new System.Drawing.Point(22, 304);
            this._d16Label.Name = "_d16Label";
            this._d16Label.Size = new System.Drawing.Size(28, 13);
            this._d16Label.TabIndex = 14;
            this._d16Label.Text = "Д16";
            // 
            // _d4Label
            // 
            this._d4Label.AutoSize = true;
            this._d4Label.Location = new System.Drawing.Point(25, 75);
            this._d4Label.Name = "_d4Label";
            this._d4Label.Size = new System.Drawing.Size(22, 13);
            this._d4Label.TabIndex = 6;
            this._d4Label.Text = "Д4";
            // 
            // _d15
            // 
            this._d15.Location = new System.Drawing.Point(6, 285);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(13, 13);
            this._d15.State = BEMN.Forms.LedState.Off;
            this._d15.TabIndex = 13;
            // 
            // _d9
            // 
            this._d9.Location = new System.Drawing.Point(6, 171);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(13, 13);
            this._d9.State = BEMN.Forms.LedState.Off;
            this._d9.TabIndex = 1;
            // 
            // _d15Label
            // 
            this._d15Label.AutoSize = true;
            this._d15Label.Location = new System.Drawing.Point(22, 285);
            this._d15Label.Name = "_d15Label";
            this._d15Label.Size = new System.Drawing.Size(28, 13);
            this._d15Label.TabIndex = 12;
            this._d15Label.Text = "Д15";
            // 
            // _d9Label
            // 
            this._d9Label.AutoSize = true;
            this._d9Label.Location = new System.Drawing.Point(25, 171);
            this._d9Label.Name = "_d9Label";
            this._d9Label.Size = new System.Drawing.Size(22, 13);
            this._d9Label.TabIndex = 0;
            this._d9Label.Text = "Д9";
            // 
            // _d14
            // 
            this._d14.Location = new System.Drawing.Point(6, 266);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(13, 13);
            this._d14.State = BEMN.Forms.LedState.Off;
            this._d14.TabIndex = 11;
            // 
            // _d10Label
            // 
            this._d10Label.AutoSize = true;
            this._d10Label.Location = new System.Drawing.Point(22, 190);
            this._d10Label.Name = "_d10Label";
            this._d10Label.Size = new System.Drawing.Size(28, 13);
            this._d10Label.TabIndex = 2;
            this._d10Label.Text = "Д10";
            // 
            // _d14Label
            // 
            this._d14Label.AutoSize = true;
            this._d14Label.Location = new System.Drawing.Point(22, 266);
            this._d14Label.Name = "_d14Label";
            this._d14Label.Size = new System.Drawing.Size(28, 13);
            this._d14Label.TabIndex = 10;
            this._d14Label.Text = "Д14";
            // 
            // _d10
            // 
            this._d10.Location = new System.Drawing.Point(6, 190);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(13, 13);
            this._d10.State = BEMN.Forms.LedState.Off;
            this._d10.TabIndex = 3;
            // 
            // _d13
            // 
            this._d13.Location = new System.Drawing.Point(6, 247);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(13, 13);
            this._d13.State = BEMN.Forms.LedState.Off;
            this._d13.TabIndex = 9;
            // 
            // _d11Label
            // 
            this._d11Label.AutoSize = true;
            this._d11Label.Location = new System.Drawing.Point(22, 209);
            this._d11Label.Name = "_d11Label";
            this._d11Label.Size = new System.Drawing.Size(28, 13);
            this._d11Label.TabIndex = 4;
            this._d11Label.Text = "Д11";
            // 
            // _d13Label
            // 
            this._d13Label.AutoSize = true;
            this._d13Label.Location = new System.Drawing.Point(22, 247);
            this._d13Label.Name = "_d13Label";
            this._d13Label.Size = new System.Drawing.Size(28, 13);
            this._d13Label.TabIndex = 8;
            this._d13Label.Text = "Д13";
            // 
            // _d11
            // 
            this._d11.Location = new System.Drawing.Point(6, 209);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(13, 13);
            this._d11.State = BEMN.Forms.LedState.Off;
            this._d11.TabIndex = 5;
            // 
            // _d12
            // 
            this._d12.Location = new System.Drawing.Point(6, 228);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(13, 13);
            this._d12.State = BEMN.Forms.LedState.Off;
            this._d12.TabIndex = 7;
            // 
            // _d12Label
            // 
            this._d12Label.AutoSize = true;
            this._d12Label.Location = new System.Drawing.Point(22, 228);
            this._d12Label.Name = "_d12Label";
            this._d12Label.Size = new System.Drawing.Size(28, 13);
            this._d12Label.TabIndex = 6;
            this._d12Label.Text = "Д12";
            // 
            // _r64Label
            // 
            this._r64Label.AutoSize = true;
            this._r64Label.Location = new System.Drawing.Point(227, 268);
            this._r64Label.Name = "_r64Label";
            this._r64Label.Size = new System.Drawing.Size(19, 13);
            this._r64Label.TabIndex = 22;
            this._r64Label.Text = "64";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(84, 193);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(19, 13);
            this.label164.TabIndex = 24;
            this.label164.Text = "10";
            // 
            // _r76Label
            // 
            this._r76Label.AutoSize = true;
            this._r76Label.Location = new System.Drawing.Point(275, 173);
            this._r76Label.Name = "_r76Label";
            this._r76Label.Size = new System.Drawing.Size(19, 13);
            this._r76Label.TabIndex = 22;
            this._r76Label.Text = "76";
            // 
            // _r40Label
            // 
            this._r40Label.AutoSize = true;
            this._r40Label.Location = new System.Drawing.Point(169, 116);
            this._r40Label.Name = "_r40Label";
            this._r40Label.Size = new System.Drawing.Size(19, 13);
            this._r40Label.TabIndex = 30;
            this._r40Label.Text = "40";
            // 
            // _r69Label
            // 
            this._r69Label.AutoSize = true;
            this._r69Label.Location = new System.Drawing.Point(275, 41);
            this._r69Label.Name = "_r69Label";
            this._r69Label.Size = new System.Drawing.Size(19, 13);
            this._r69Label.TabIndex = 30;
            this._r69Label.Text = "69";
            // 
            // _r55Label
            // 
            this._r55Label.AutoSize = true;
            this._r55Label.Location = new System.Drawing.Point(227, 98);
            this._r55Label.Name = "_r55Label";
            this._r55Label.Size = new System.Drawing.Size(19, 13);
            this._r55Label.TabIndex = 28;
            this._r55Label.Text = "55";
            // 
            // _r51Label
            // 
            this._r51Label.AutoSize = true;
            this._r51Label.Location = new System.Drawing.Point(227, 22);
            this._r51Label.Name = "_r51Label";
            this._r51Label.Size = new System.Drawing.Size(19, 13);
            this._r51Label.TabIndex = 30;
            this._r51Label.Text = "51";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(84, 174);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(13, 13);
            this.label165.TabIndex = 22;
            this.label165.Text = "9";
            // 
            // _r60Label
            // 
            this._r60Label.AutoSize = true;
            this._r60Label.Location = new System.Drawing.Point(227, 192);
            this._r60Label.Name = "_r60Label";
            this._r60Label.Size = new System.Drawing.Size(19, 13);
            this._r60Label.TabIndex = 24;
            this._r60Label.Text = "60";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(84, 307);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(19, 13);
            this.label163.TabIndex = 10;
            this.label163.Text = "16";
            // 
            // _r72Label
            // 
            this._r72Label.AutoSize = true;
            this._r72Label.Location = new System.Drawing.Point(275, 98);
            this._r72Label.Name = "_r72Label";
            this._r72Label.Size = new System.Drawing.Size(19, 13);
            this._r72Label.TabIndex = 24;
            this._r72Label.Text = "72";
            // 
            // _r57Label
            // 
            this._r57Label.AutoSize = true;
            this._r57Label.Location = new System.Drawing.Point(227, 135);
            this._r57Label.Name = "_r57Label";
            this._r57Label.Size = new System.Drawing.Size(19, 13);
            this._r57Label.TabIndex = 30;
            this._r57Label.Text = "57";
            // 
            // _r65Label
            // 
            this._r65Label.AutoSize = true;
            this._r65Label.Location = new System.Drawing.Point(227, 287);
            this._r65Label.Name = "_r65Label";
            this._r65Label.Size = new System.Drawing.Size(19, 13);
            this._r65Label.TabIndex = 24;
            this._r65Label.Text = "65";
            // 
            // _r80Label
            // 
            this._r80Label.AutoSize = true;
            this._r80Label.Location = new System.Drawing.Point(275, 249);
            this._r80Label.Name = "_r80Label";
            this._r80Label.Size = new System.Drawing.Size(19, 13);
            this._r80Label.TabIndex = 30;
            this._r80Label.Text = "80";
            // 
            // _r77Label
            // 
            this._r77Label.AutoSize = true;
            this._r77Label.Location = new System.Drawing.Point(275, 192);
            this._r77Label.Name = "_r77Label";
            this._r77Label.Size = new System.Drawing.Size(19, 13);
            this._r77Label.TabIndex = 24;
            this._r77Label.Text = "77";
            // 
            // _r68Label
            // 
            this._r68Label.AutoSize = true;
            this._r68Label.Location = new System.Drawing.Point(275, 22);
            this._r68Label.Name = "_r68Label";
            this._r68Label.Size = new System.Drawing.Size(19, 13);
            this._r68Label.TabIndex = 30;
            this._r68Label.Text = "68";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(84, 288);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(19, 13);
            this.label169.TabIndex = 8;
            this.label169.Text = "15";
            // 
            // _r39Label
            // 
            this._r39Label.AutoSize = true;
            this._r39Label.Location = new System.Drawing.Point(169, 98);
            this._r39Label.Name = "_r39Label";
            this._r39Label.Size = new System.Drawing.Size(19, 13);
            this._r39Label.TabIndex = 30;
            this._r39Label.Text = "39";
            // 
            // _r75Label
            // 
            this._r75Label.AutoSize = true;
            this._r75Label.Location = new System.Drawing.Point(275, 154);
            this._r75Label.Name = "_r75Label";
            this._r75Label.Size = new System.Drawing.Size(19, 13);
            this._r75Label.TabIndex = 30;
            this._r75Label.Text = "75";
            // 
            // _r54Label
            // 
            this._r54Label.AutoSize = true;
            this._r54Label.Location = new System.Drawing.Point(227, 80);
            this._r54Label.Name = "_r54Label";
            this._r54Label.Size = new System.Drawing.Size(19, 13);
            this._r54Label.TabIndex = 26;
            this._r54Label.Text = "54";
            // 
            // _r61Label
            // 
            this._r61Label.AutoSize = true;
            this._r61Label.Location = new System.Drawing.Point(227, 211);
            this._r61Label.Name = "_r61Label";
            this._r61Label.Size = new System.Drawing.Size(19, 13);
            this._r61Label.TabIndex = 26;
            this._r61Label.Text = "61";
            // 
            // _r63Label
            // 
            this._r63Label.AutoSize = true;
            this._r63Label.Location = new System.Drawing.Point(227, 249);
            this._r63Label.Name = "_r63Label";
            this._r63Label.Size = new System.Drawing.Size(19, 13);
            this._r63Label.TabIndex = 30;
            this._r63Label.Text = "63";
            // 
            // _r73Label
            // 
            this._r73Label.AutoSize = true;
            this._r73Label.Location = new System.Drawing.Point(275, 116);
            this._r73Label.Name = "_r73Label";
            this._r73Label.Size = new System.Drawing.Size(19, 13);
            this._r73Label.TabIndex = 26;
            this._r73Label.Text = "73";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(84, 22);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(13, 13);
            this.label176.TabIndex = 0;
            this.label176.Text = "1";
            // 
            // _r66Label
            // 
            this._r66Label.AutoSize = true;
            this._r66Label.Location = new System.Drawing.Point(227, 306);
            this._r66Label.Name = "_r66Label";
            this._r66Label.Size = new System.Drawing.Size(19, 13);
            this._r66Label.TabIndex = 26;
            this._r66Label.Text = "66";
            // 
            // _r78Label
            // 
            this._r78Label.AutoSize = true;
            this._r78Label.Location = new System.Drawing.Point(275, 211);
            this._r78Label.Name = "_r78Label";
            this._r78Label.Size = new System.Drawing.Size(19, 13);
            this._r78Label.TabIndex = 26;
            this._r78Label.Text = "78";
            // 
            // _r46Label
            // 
            this._r46Label.AutoSize = true;
            this._r46Label.Location = new System.Drawing.Point(169, 231);
            this._r46Label.Name = "_r46Label";
            this._r46Label.Size = new System.Drawing.Size(19, 13);
            this._r46Label.TabIndex = 30;
            this._r46Label.Text = "46";
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(84, 269);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(19, 13);
            this.label170.TabIndex = 6;
            this.label170.Text = "14";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(84, 155);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(13, 13);
            this.label166.TabIndex = 20;
            this.label166.Text = "8";
            // 
            // _r53Label
            // 
            this._r53Label.AutoSize = true;
            this._r53Label.Location = new System.Drawing.Point(227, 61);
            this._r53Label.Name = "_r53Label";
            this._r53Label.Size = new System.Drawing.Size(19, 13);
            this._r53Label.TabIndex = 24;
            this._r53Label.Text = "53";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(84, 250);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(19, 13);
            this.label171.TabIndex = 4;
            this.label171.Text = "13";
            // 
            // _r62Label
            // 
            this._r62Label.AutoSize = true;
            this._r62Label.Location = new System.Drawing.Point(227, 230);
            this._r62Label.Name = "_r62Label";
            this._r62Label.Size = new System.Drawing.Size(19, 13);
            this._r62Label.TabIndex = 28;
            this._r62Label.Text = "62";
            // 
            // _r74Label
            // 
            this._r74Label.AutoSize = true;
            this._r74Label.Location = new System.Drawing.Point(275, 135);
            this._r74Label.Name = "_r74Label";
            this._r74Label.Size = new System.Drawing.Size(19, 13);
            this._r74Label.TabIndex = 28;
            this._r74Label.Text = "74";
            // 
            // _r67Label
            // 
            this._r67Label.AutoSize = true;
            this._r67Label.Location = new System.Drawing.Point(227, 325);
            this._r67Label.Name = "_r67Label";
            this._r67Label.Size = new System.Drawing.Size(19, 13);
            this._r67Label.TabIndex = 28;
            this._r67Label.Text = "67";
            // 
            // _r34Label
            // 
            this._r34Label.AutoSize = true;
            this._r34Label.Location = new System.Drawing.Point(126, 326);
            this._r34Label.Name = "_r34Label";
            this._r34Label.Size = new System.Drawing.Size(19, 13);
            this._r34Label.TabIndex = 30;
            this._r34Label.Text = "34";
            // 
            // _r79Label
            // 
            this._r79Label.AutoSize = true;
            this._r79Label.Location = new System.Drawing.Point(275, 230);
            this._r79Label.Name = "_r79Label";
            this._r79Label.Size = new System.Drawing.Size(19, 13);
            this._r79Label.TabIndex = 28;
            this._r79Label.Text = "79";
            // 
            // _r52Label
            // 
            this._r52Label.AutoSize = true;
            this._r52Label.Location = new System.Drawing.Point(227, 42);
            this._r52Label.Name = "_r52Label";
            this._r52Label.Size = new System.Drawing.Size(19, 13);
            this._r52Label.TabIndex = 22;
            this._r52Label.Text = "52";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(84, 41);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(13, 13);
            this.label175.TabIndex = 2;
            this.label175.Text = "2";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(84, 231);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(19, 13);
            this.label177.TabIndex = 2;
            this.label177.Text = "12";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(84, 136);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(13, 13);
            this.label167.TabIndex = 18;
            this.label167.Text = "7";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(84, 212);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(19, 13);
            this.label178.TabIndex = 0;
            this.label178.Text = "11";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(84, 117);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(13, 13);
            this.label168.TabIndex = 16;
            this.label168.Text = "6";
            // 
            // _r50Label
            // 
            this._r50Label.AutoSize = true;
            this._r50Label.Location = new System.Drawing.Point(169, 307);
            this._r50Label.Name = "_r50Label";
            this._r50Label.Size = new System.Drawing.Size(19, 13);
            this._r50Label.TabIndex = 28;
            this._r50Label.Text = "50";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(84, 60);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(13, 13);
            this.label174.TabIndex = 4;
            this.label174.Text = "3";
            // 
            // _r38Label
            // 
            this._r38Label.AutoSize = true;
            this._r38Label.Location = new System.Drawing.Point(169, 79);
            this._r38Label.Name = "_r38Label";
            this._r38Label.Size = new System.Drawing.Size(19, 13);
            this._r38Label.TabIndex = 28;
            this._r38Label.Text = "38";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(84, 79);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(13, 13);
            this.label173.TabIndex = 6;
            this.label173.Text = "4";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(84, 98);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(13, 13);
            this.label172.TabIndex = 8;
            this.label172.Text = "5";
            // 
            // _r45Label
            // 
            this._r45Label.AutoSize = true;
            this._r45Label.Location = new System.Drawing.Point(169, 212);
            this._r45Label.Name = "_r45Label";
            this._r45Label.Size = new System.Drawing.Size(19, 13);
            this._r45Label.TabIndex = 28;
            this._r45Label.Text = "45";
            // 
            // _r30Label
            // 
            this._r30Label.AutoSize = true;
            this._r30Label.Location = new System.Drawing.Point(126, 250);
            this._r30Label.Name = "_r30Label";
            this._r30Label.Size = new System.Drawing.Size(19, 13);
            this._r30Label.TabIndex = 22;
            this._r30Label.Text = "30";
            // 
            // _r29Label
            // 
            this._r29Label.AutoSize = true;
            this._r29Label.Location = new System.Drawing.Point(126, 231);
            this._r29Label.Name = "_r29Label";
            this._r29Label.Size = new System.Drawing.Size(19, 13);
            this._r29Label.TabIndex = 20;
            this._r29Label.Text = "29";
            // 
            // _r28Label
            // 
            this._r28Label.AutoSize = true;
            this._r28Label.Location = new System.Drawing.Point(126, 212);
            this._r28Label.Name = "_r28Label";
            this._r28Label.Size = new System.Drawing.Size(19, 13);
            this._r28Label.TabIndex = 18;
            this._r28Label.Text = "28";
            // 
            // _r27Label
            // 
            this._r27Label.AutoSize = true;
            this._r27Label.Location = new System.Drawing.Point(126, 193);
            this._r27Label.Name = "_r27Label";
            this._r27Label.Size = new System.Drawing.Size(19, 13);
            this._r27Label.TabIndex = 16;
            this._r27Label.Text = "27";
            // 
            // _r33Label
            // 
            this._r33Label.AutoSize = true;
            this._r33Label.Location = new System.Drawing.Point(126, 307);
            this._r33Label.Name = "_r33Label";
            this._r33Label.Size = new System.Drawing.Size(19, 13);
            this._r33Label.TabIndex = 28;
            this._r33Label.Text = "33";
            // 
            // _r26Label
            // 
            this._r26Label.AutoSize = true;
            this._r26Label.Location = new System.Drawing.Point(126, 174);
            this._r26Label.Name = "_r26Label";
            this._r26Label.Size = new System.Drawing.Size(19, 13);
            this._r26Label.TabIndex = 14;
            this._r26Label.Text = "26";
            // 
            // _r25Label
            // 
            this._r25Label.AutoSize = true;
            this._r25Label.Location = new System.Drawing.Point(126, 155);
            this._r25Label.Name = "_r25Label";
            this._r25Label.Size = new System.Drawing.Size(19, 13);
            this._r25Label.TabIndex = 12;
            this._r25Label.Text = "25";
            // 
            // _r24Label
            // 
            this._r24Label.AutoSize = true;
            this._r24Label.Location = new System.Drawing.Point(126, 136);
            this._r24Label.Name = "_r24Label";
            this._r24Label.Size = new System.Drawing.Size(19, 13);
            this._r24Label.TabIndex = 10;
            this._r24Label.Text = "24";
            // 
            // _r23Label
            // 
            this._r23Label.AutoSize = true;
            this._r23Label.Location = new System.Drawing.Point(126, 117);
            this._r23Label.Name = "_r23Label";
            this._r23Label.Size = new System.Drawing.Size(19, 13);
            this._r23Label.TabIndex = 8;
            this._r23Label.Text = "23";
            // 
            // _r31Label
            // 
            this._r31Label.AutoSize = true;
            this._r31Label.Location = new System.Drawing.Point(126, 269);
            this._r31Label.Name = "_r31Label";
            this._r31Label.Size = new System.Drawing.Size(19, 13);
            this._r31Label.TabIndex = 24;
            this._r31Label.Text = "31";
            // 
            // _r49Label
            // 
            this._r49Label.AutoSize = true;
            this._r49Label.Location = new System.Drawing.Point(169, 288);
            this._r49Label.Name = "_r49Label";
            this._r49Label.Size = new System.Drawing.Size(19, 13);
            this._r49Label.TabIndex = 26;
            this._r49Label.Text = "49";
            // 
            // _r22Label
            // 
            this._r22Label.AutoSize = true;
            this._r22Label.Location = new System.Drawing.Point(126, 99);
            this._r22Label.Name = "_r22Label";
            this._r22Label.Size = new System.Drawing.Size(19, 13);
            this._r22Label.TabIndex = 6;
            this._r22Label.Text = "22";
            // 
            // _r21Label
            // 
            this._r21Label.AutoSize = true;
            this._r21Label.Location = new System.Drawing.Point(126, 79);
            this._r21Label.Name = "_r21Label";
            this._r21Label.Size = new System.Drawing.Size(19, 13);
            this._r21Label.TabIndex = 4;
            this._r21Label.Text = "21";
            // 
            // _r37Label
            // 
            this._r37Label.AutoSize = true;
            this._r37Label.Location = new System.Drawing.Point(169, 60);
            this._r37Label.Name = "_r37Label";
            this._r37Label.Size = new System.Drawing.Size(19, 13);
            this._r37Label.TabIndex = 26;
            this._r37Label.Text = "37";
            // 
            // _r20Label
            // 
            this._r20Label.AutoSize = true;
            this._r20Label.Location = new System.Drawing.Point(126, 60);
            this._r20Label.Name = "_r20Label";
            this._r20Label.Size = new System.Drawing.Size(19, 13);
            this._r20Label.TabIndex = 2;
            this._r20Label.Text = "20";
            // 
            // _r44Label
            // 
            this._r44Label.AutoSize = true;
            this._r44Label.Location = new System.Drawing.Point(169, 193);
            this._r44Label.Name = "_r44Label";
            this._r44Label.Size = new System.Drawing.Size(19, 13);
            this._r44Label.TabIndex = 26;
            this._r44Label.Text = "44";
            // 
            // _r19Label
            // 
            this._r19Label.AutoSize = true;
            this._r19Label.Location = new System.Drawing.Point(126, 41);
            this._r19Label.Name = "_r19Label";
            this._r19Label.Size = new System.Drawing.Size(19, 13);
            this._r19Label.TabIndex = 0;
            this._r19Label.Text = "19";
            // 
            // _r32Label
            // 
            this._r32Label.AutoSize = true;
            this._r32Label.Location = new System.Drawing.Point(126, 288);
            this._r32Label.Name = "_r32Label";
            this._r32Label.Size = new System.Drawing.Size(19, 13);
            this._r32Label.TabIndex = 26;
            this._r32Label.Text = "32";
            // 
            // _r42Label
            // 
            this._r42Label.AutoSize = true;
            this._r42Label.Location = new System.Drawing.Point(169, 155);
            this._r42Label.Name = "_r42Label";
            this._r42Label.Size = new System.Drawing.Size(19, 13);
            this._r42Label.TabIndex = 22;
            this._r42Label.Text = "42";
            // 
            // _r48Label
            // 
            this._r48Label.AutoSize = true;
            this._r48Label.Location = new System.Drawing.Point(169, 269);
            this._r48Label.Name = "_r48Label";
            this._r48Label.Size = new System.Drawing.Size(19, 13);
            this._r48Label.TabIndex = 24;
            this._r48Label.Text = "48";
            // 
            // _r41Label
            // 
            this._r41Label.AutoSize = true;
            this._r41Label.Location = new System.Drawing.Point(169, 136);
            this._r41Label.Name = "_r41Label";
            this._r41Label.Size = new System.Drawing.Size(19, 13);
            this._r41Label.TabIndex = 20;
            this._r41Label.Text = "41";
            // 
            // _r36Label
            // 
            this._r36Label.AutoSize = true;
            this._r36Label.Location = new System.Drawing.Point(169, 41);
            this._r36Label.Name = "_r36Label";
            this._r36Label.Size = new System.Drawing.Size(19, 13);
            this._r36Label.TabIndex = 24;
            this._r36Label.Text = "36";
            // 
            // _r35Label
            // 
            this._r35Label.AutoSize = true;
            this._r35Label.Location = new System.Drawing.Point(169, 22);
            this._r35Label.Name = "_r35Label";
            this._r35Label.Size = new System.Drawing.Size(19, 13);
            this._r35Label.TabIndex = 22;
            this._r35Label.Text = "35";
            // 
            // _r47Label
            // 
            this._r47Label.AutoSize = true;
            this._r47Label.Location = new System.Drawing.Point(169, 250);
            this._r47Label.Name = "_r47Label";
            this._r47Label.Size = new System.Drawing.Size(19, 13);
            this._r47Label.TabIndex = 22;
            this._r47Label.Text = "47";
            // 
            // _r43Label
            // 
            this._r43Label.AutoSize = true;
            this._r43Label.Location = new System.Drawing.Point(169, 174);
            this._r43Label.Name = "_r43Label";
            this._r43Label.Size = new System.Drawing.Size(19, 13);
            this._r43Label.TabIndex = 24;
            this._r43Label.Text = "43";
            // 
            // _module56
            // 
            this._module56.Location = new System.Drawing.Point(208, 116);
            this._module56.Name = "_module56";
            this._module56.Size = new System.Drawing.Size(13, 13);
            this._module56.State = BEMN.Forms.LedState.Off;
            this._module56.TabIndex = 33;
            // 
            // _module40
            // 
            this._module40.Location = new System.Drawing.Point(150, 116);
            this._module40.Name = "_module40";
            this._module40.Size = new System.Drawing.Size(13, 13);
            this._module40.State = BEMN.Forms.LedState.Off;
            this._module40.TabIndex = 31;
            // 
            // _module57
            // 
            this._module57.Location = new System.Drawing.Point(208, 135);
            this._module57.Name = "_module57";
            this._module57.Size = new System.Drawing.Size(13, 13);
            this._module57.State = BEMN.Forms.LedState.Off;
            this._module57.TabIndex = 31;
            // 
            // _module18
            // 
            this._module18.Location = new System.Drawing.Point(107, 22);
            this._module18.Name = "_module18";
            this._module18.Size = new System.Drawing.Size(13, 13);
            this._module18.State = BEMN.Forms.LedState.Off;
            this._module18.TabIndex = 15;
            // 
            // _module10
            // 
            this._module10.Location = new System.Drawing.Point(65, 193);
            this._module10.Name = "_module10";
            this._module10.Size = new System.Drawing.Size(13, 13);
            this._module10.State = BEMN.Forms.LedState.Off;
            this._module10.TabIndex = 25;
            // 
            // _module69
            // 
            this._module69.Location = new System.Drawing.Point(256, 41);
            this._module69.Name = "_module69";
            this._module69.Size = new System.Drawing.Size(13, 13);
            this._module69.State = BEMN.Forms.LedState.Off;
            this._module69.TabIndex = 31;
            // 
            // _module58
            // 
            this._module58.Location = new System.Drawing.Point(208, 154);
            this._module58.Name = "_module58";
            this._module58.Size = new System.Drawing.Size(13, 13);
            this._module58.State = BEMN.Forms.LedState.Off;
            this._module58.TabIndex = 21;
            // 
            // _module70
            // 
            this._module70.Location = new System.Drawing.Point(256, 60);
            this._module70.Name = "_module70";
            this._module70.Size = new System.Drawing.Size(13, 13);
            this._module70.State = BEMN.Forms.LedState.Off;
            this._module70.TabIndex = 21;
            // 
            // _module39
            // 
            this._module39.Location = new System.Drawing.Point(150, 98);
            this._module39.Name = "_module39";
            this._module39.Size = new System.Drawing.Size(13, 13);
            this._module39.State = BEMN.Forms.LedState.Off;
            this._module39.TabIndex = 31;
            // 
            // _module80
            // 
            this._module80.Location = new System.Drawing.Point(256, 249);
            this._module80.Name = "_module80";
            this._module80.Size = new System.Drawing.Size(13, 13);
            this._module80.State = BEMN.Forms.LedState.Off;
            this._module80.TabIndex = 31;
            // 
            // _module17
            // 
            this._module17.Location = new System.Drawing.Point(65, 326);
            this._module17.Name = "_module17";
            this._module17.Size = new System.Drawing.Size(13, 13);
            this._module17.State = BEMN.Forms.LedState.Off;
            this._module17.TabIndex = 13;
            // 
            // _module51
            // 
            this._module51.Location = new System.Drawing.Point(208, 22);
            this._module51.Name = "_module51";
            this._module51.Size = new System.Drawing.Size(13, 13);
            this._module51.State = BEMN.Forms.LedState.Off;
            this._module51.TabIndex = 31;
            // 
            // _module68
            // 
            this._module68.Location = new System.Drawing.Point(256, 22);
            this._module68.Name = "_module68";
            this._module68.Size = new System.Drawing.Size(13, 13);
            this._module68.State = BEMN.Forms.LedState.Off;
            this._module68.TabIndex = 31;
            // 
            // _module55
            // 
            this._module55.Location = new System.Drawing.Point(208, 98);
            this._module55.Name = "_module55";
            this._module55.Size = new System.Drawing.Size(13, 13);
            this._module55.State = BEMN.Forms.LedState.Off;
            this._module55.TabIndex = 29;
            // 
            // _module59
            // 
            this._module59.Location = new System.Drawing.Point(208, 173);
            this._module59.Name = "_module59";
            this._module59.Size = new System.Drawing.Size(13, 13);
            this._module59.State = BEMN.Forms.LedState.Off;
            this._module59.TabIndex = 23;
            // 
            // _module9
            // 
            this._module9.Location = new System.Drawing.Point(65, 174);
            this._module9.Name = "_module9";
            this._module9.Size = new System.Drawing.Size(13, 13);
            this._module9.State = BEMN.Forms.LedState.Off;
            this._module9.TabIndex = 23;
            // 
            // _module71
            // 
            this._module71.Location = new System.Drawing.Point(256, 79);
            this._module71.Name = "_module71";
            this._module71.Size = new System.Drawing.Size(13, 13);
            this._module71.State = BEMN.Forms.LedState.Off;
            this._module71.TabIndex = 23;
            // 
            // _module75
            // 
            this._module75.Location = new System.Drawing.Point(256, 154);
            this._module75.Name = "_module75";
            this._module75.Size = new System.Drawing.Size(13, 13);
            this._module75.State = BEMN.Forms.LedState.Off;
            this._module75.TabIndex = 31;
            // 
            // _module16
            // 
            this._module16.Location = new System.Drawing.Point(65, 307);
            this._module16.Name = "_module16";
            this._module16.Size = new System.Drawing.Size(13, 13);
            this._module16.State = BEMN.Forms.LedState.Off;
            this._module16.TabIndex = 11;
            // 
            // _module64
            // 
            this._module64.Location = new System.Drawing.Point(208, 268);
            this._module64.Name = "_module64";
            this._module64.Size = new System.Drawing.Size(13, 13);
            this._module64.State = BEMN.Forms.LedState.Off;
            this._module64.TabIndex = 23;
            // 
            // _module76
            // 
            this._module76.Location = new System.Drawing.Point(256, 173);
            this._module76.Name = "_module76";
            this._module76.Size = new System.Drawing.Size(13, 13);
            this._module76.State = BEMN.Forms.LedState.Off;
            this._module76.TabIndex = 23;
            // 
            // _module46
            // 
            this._module46.Location = new System.Drawing.Point(150, 231);
            this._module46.Name = "_module46";
            this._module46.Size = new System.Drawing.Size(13, 13);
            this._module46.State = BEMN.Forms.LedState.Off;
            this._module46.TabIndex = 31;
            // 
            // _module63
            // 
            this._module63.Location = new System.Drawing.Point(208, 249);
            this._module63.Name = "_module63";
            this._module63.Size = new System.Drawing.Size(13, 13);
            this._module63.State = BEMN.Forms.LedState.Off;
            this._module63.TabIndex = 31;
            // 
            // _module15
            // 
            this._module15.Location = new System.Drawing.Point(65, 288);
            this._module15.Name = "_module15";
            this._module15.Size = new System.Drawing.Size(13, 13);
            this._module15.State = BEMN.Forms.LedState.Off;
            this._module15.TabIndex = 9;
            // 
            // _module5
            // 
            this._module5.Location = new System.Drawing.Point(65, 98);
            this._module5.Name = "_module5";
            this._module5.Size = new System.Drawing.Size(13, 13);
            this._module5.State = BEMN.Forms.LedState.Off;
            this._module5.TabIndex = 9;
            // 
            // _module54
            // 
            this._module54.Location = new System.Drawing.Point(208, 80);
            this._module54.Name = "_module54";
            this._module54.Size = new System.Drawing.Size(13, 13);
            this._module54.State = BEMN.Forms.LedState.Off;
            this._module54.TabIndex = 27;
            // 
            // _module34
            // 
            this._module34.Location = new System.Drawing.Point(107, 326);
            this._module34.Name = "_module34";
            this._module34.Size = new System.Drawing.Size(13, 13);
            this._module34.State = BEMN.Forms.LedState.Off;
            this._module34.TabIndex = 31;
            // 
            // _module60
            // 
            this._module60.Location = new System.Drawing.Point(208, 192);
            this._module60.Name = "_module60";
            this._module60.Size = new System.Drawing.Size(13, 13);
            this._module60.State = BEMN.Forms.LedState.Off;
            this._module60.TabIndex = 25;
            // 
            // _module72
            // 
            this._module72.Location = new System.Drawing.Point(256, 98);
            this._module72.Name = "_module72";
            this._module72.Size = new System.Drawing.Size(13, 13);
            this._module72.State = BEMN.Forms.LedState.Off;
            this._module72.TabIndex = 25;
            // 
            // _module65
            // 
            this._module65.Location = new System.Drawing.Point(208, 287);
            this._module65.Name = "_module65";
            this._module65.Size = new System.Drawing.Size(13, 13);
            this._module65.State = BEMN.Forms.LedState.Off;
            this._module65.TabIndex = 25;
            // 
            // _module8
            // 
            this._module8.Location = new System.Drawing.Point(65, 155);
            this._module8.Name = "_module8";
            this._module8.Size = new System.Drawing.Size(13, 13);
            this._module8.State = BEMN.Forms.LedState.Off;
            this._module8.TabIndex = 21;
            // 
            // _module77
            // 
            this._module77.Location = new System.Drawing.Point(256, 192);
            this._module77.Name = "_module77";
            this._module77.Size = new System.Drawing.Size(13, 13);
            this._module77.State = BEMN.Forms.LedState.Off;
            this._module77.TabIndex = 25;
            // 
            // _module14
            // 
            this._module14.Location = new System.Drawing.Point(65, 269);
            this._module14.Name = "_module14";
            this._module14.Size = new System.Drawing.Size(13, 13);
            this._module14.State = BEMN.Forms.LedState.Off;
            this._module14.TabIndex = 7;
            // 
            // _module53
            // 
            this._module53.Location = new System.Drawing.Point(208, 61);
            this._module53.Name = "_module53";
            this._module53.Size = new System.Drawing.Size(13, 13);
            this._module53.State = BEMN.Forms.LedState.Off;
            this._module53.TabIndex = 25;
            // 
            // _module79
            // 
            this._module79.Location = new System.Drawing.Point(256, 230);
            this._module79.Name = "_module79";
            this._module79.Size = new System.Drawing.Size(13, 13);
            this._module79.State = BEMN.Forms.LedState.Off;
            this._module79.TabIndex = 29;
            // 
            // _module61
            // 
            this._module61.Location = new System.Drawing.Point(208, 211);
            this._module61.Name = "_module61";
            this._module61.Size = new System.Drawing.Size(13, 13);
            this._module61.State = BEMN.Forms.LedState.Off;
            this._module61.TabIndex = 27;
            // 
            // _module73
            // 
            this._module73.Location = new System.Drawing.Point(256, 116);
            this._module73.Name = "_module73";
            this._module73.Size = new System.Drawing.Size(13, 13);
            this._module73.State = BEMN.Forms.LedState.Off;
            this._module73.TabIndex = 27;
            // 
            // _module67
            // 
            this._module67.Location = new System.Drawing.Point(208, 325);
            this._module67.Name = "_module67";
            this._module67.Size = new System.Drawing.Size(13, 13);
            this._module67.State = BEMN.Forms.LedState.Off;
            this._module67.TabIndex = 29;
            // 
            // _module66
            // 
            this._module66.Location = new System.Drawing.Point(208, 306);
            this._module66.Name = "_module66";
            this._module66.Size = new System.Drawing.Size(13, 13);
            this._module66.State = BEMN.Forms.LedState.Off;
            this._module66.TabIndex = 27;
            // 
            // _module13
            // 
            this._module13.Location = new System.Drawing.Point(65, 250);
            this._module13.Name = "_module13";
            this._module13.Size = new System.Drawing.Size(13, 13);
            this._module13.State = BEMN.Forms.LedState.Off;
            this._module13.TabIndex = 5;
            // 
            // _module78
            // 
            this._module78.Location = new System.Drawing.Point(256, 211);
            this._module78.Name = "_module78";
            this._module78.Size = new System.Drawing.Size(13, 13);
            this._module78.State = BEMN.Forms.LedState.Off;
            this._module78.TabIndex = 27;
            // 
            // _module1
            // 
            this._module1.Location = new System.Drawing.Point(65, 22);
            this._module1.Name = "_module1";
            this._module1.Size = new System.Drawing.Size(13, 13);
            this._module1.State = BEMN.Forms.LedState.Off;
            this._module1.TabIndex = 1;
            // 
            // _module74
            // 
            this._module74.Location = new System.Drawing.Point(256, 135);
            this._module74.Name = "_module74";
            this._module74.Size = new System.Drawing.Size(13, 13);
            this._module74.State = BEMN.Forms.LedState.Off;
            this._module74.TabIndex = 29;
            // 
            // _module50
            // 
            this._module50.Location = new System.Drawing.Point(150, 307);
            this._module50.Name = "_module50";
            this._module50.Size = new System.Drawing.Size(13, 13);
            this._module50.State = BEMN.Forms.LedState.Off;
            this._module50.TabIndex = 29;
            // 
            // _module62
            // 
            this._module62.Location = new System.Drawing.Point(208, 230);
            this._module62.Name = "_module62";
            this._module62.Size = new System.Drawing.Size(13, 13);
            this._module62.State = BEMN.Forms.LedState.Off;
            this._module62.TabIndex = 29;
            // 
            // _module52
            // 
            this._module52.Location = new System.Drawing.Point(208, 42);
            this._module52.Name = "_module52";
            this._module52.Size = new System.Drawing.Size(13, 13);
            this._module52.State = BEMN.Forms.LedState.Off;
            this._module52.TabIndex = 23;
            // 
            // _module7
            // 
            this._module7.Location = new System.Drawing.Point(65, 136);
            this._module7.Name = "_module7";
            this._module7.Size = new System.Drawing.Size(13, 13);
            this._module7.State = BEMN.Forms.LedState.Off;
            this._module7.TabIndex = 19;
            // 
            // _module12
            // 
            this._module12.Location = new System.Drawing.Point(65, 231);
            this._module12.Name = "_module12";
            this._module12.Size = new System.Drawing.Size(13, 13);
            this._module12.State = BEMN.Forms.LedState.Off;
            this._module12.TabIndex = 3;
            // 
            // _module38
            // 
            this._module38.Location = new System.Drawing.Point(150, 79);
            this._module38.Name = "_module38";
            this._module38.Size = new System.Drawing.Size(13, 13);
            this._module38.State = BEMN.Forms.LedState.Off;
            this._module38.TabIndex = 29;
            // 
            // _module45
            // 
            this._module45.Location = new System.Drawing.Point(150, 212);
            this._module45.Name = "_module45";
            this._module45.Size = new System.Drawing.Size(13, 13);
            this._module45.State = BEMN.Forms.LedState.Off;
            this._module45.TabIndex = 29;
            // 
            // _module11
            // 
            this._module11.Location = new System.Drawing.Point(65, 212);
            this._module11.Name = "_module11";
            this._module11.Size = new System.Drawing.Size(13, 13);
            this._module11.State = BEMN.Forms.LedState.Off;
            this._module11.TabIndex = 1;
            // 
            // _module2
            // 
            this._module2.Location = new System.Drawing.Point(65, 41);
            this._module2.Name = "_module2";
            this._module2.Size = new System.Drawing.Size(13, 13);
            this._module2.State = BEMN.Forms.LedState.Off;
            this._module2.TabIndex = 3;
            // 
            // _module6
            // 
            this._module6.Location = new System.Drawing.Point(65, 117);
            this._module6.Name = "_module6";
            this._module6.Size = new System.Drawing.Size(13, 13);
            this._module6.State = BEMN.Forms.LedState.Off;
            this._module6.TabIndex = 17;
            // 
            // _module3
            // 
            this._module3.Location = new System.Drawing.Point(65, 60);
            this._module3.Name = "_module3";
            this._module3.Size = new System.Drawing.Size(13, 13);
            this._module3.State = BEMN.Forms.LedState.Off;
            this._module3.TabIndex = 5;
            // 
            // _module33
            // 
            this._module33.Location = new System.Drawing.Point(107, 307);
            this._module33.Name = "_module33";
            this._module33.Size = new System.Drawing.Size(13, 13);
            this._module33.State = BEMN.Forms.LedState.Off;
            this._module33.TabIndex = 29;
            // 
            // _module4
            // 
            this._module4.Location = new System.Drawing.Point(65, 79);
            this._module4.Name = "_module4";
            this._module4.Size = new System.Drawing.Size(13, 13);
            this._module4.State = BEMN.Forms.LedState.Off;
            this._module4.TabIndex = 7;
            // 
            // _module19
            // 
            this._module19.Location = new System.Drawing.Point(107, 41);
            this._module19.Name = "_module19";
            this._module19.Size = new System.Drawing.Size(13, 13);
            this._module19.State = BEMN.Forms.LedState.Off;
            this._module19.TabIndex = 1;
            // 
            // _module29
            // 
            this._module29.Location = new System.Drawing.Point(107, 231);
            this._module29.Name = "_module29";
            this._module29.Size = new System.Drawing.Size(13, 13);
            this._module29.State = BEMN.Forms.LedState.Off;
            this._module29.TabIndex = 21;
            // 
            // _module28
            // 
            this._module28.Location = new System.Drawing.Point(107, 212);
            this._module28.Name = "_module28";
            this._module28.Size = new System.Drawing.Size(13, 13);
            this._module28.State = BEMN.Forms.LedState.Off;
            this._module28.TabIndex = 19;
            // 
            // _module27
            // 
            this._module27.Location = new System.Drawing.Point(107, 193);
            this._module27.Name = "_module27";
            this._module27.Size = new System.Drawing.Size(13, 13);
            this._module27.State = BEMN.Forms.LedState.Off;
            this._module27.TabIndex = 17;
            // 
            // _module30
            // 
            this._module30.Location = new System.Drawing.Point(107, 250);
            this._module30.Name = "_module30";
            this._module30.Size = new System.Drawing.Size(13, 13);
            this._module30.State = BEMN.Forms.LedState.Off;
            this._module30.TabIndex = 23;
            // 
            // _module49
            // 
            this._module49.Location = new System.Drawing.Point(150, 288);
            this._module49.Name = "_module49";
            this._module49.Size = new System.Drawing.Size(13, 13);
            this._module49.State = BEMN.Forms.LedState.Off;
            this._module49.TabIndex = 27;
            // 
            // _module26
            // 
            this._module26.Location = new System.Drawing.Point(107, 174);
            this._module26.Name = "_module26";
            this._module26.Size = new System.Drawing.Size(13, 13);
            this._module26.State = BEMN.Forms.LedState.Off;
            this._module26.TabIndex = 15;
            // 
            // _module37
            // 
            this._module37.Location = new System.Drawing.Point(150, 60);
            this._module37.Name = "_module37";
            this._module37.Size = new System.Drawing.Size(13, 13);
            this._module37.State = BEMN.Forms.LedState.Off;
            this._module37.TabIndex = 27;
            // 
            // _module25
            // 
            this._module25.Location = new System.Drawing.Point(107, 155);
            this._module25.Name = "_module25";
            this._module25.Size = new System.Drawing.Size(13, 13);
            this._module25.State = BEMN.Forms.LedState.Off;
            this._module25.TabIndex = 13;
            // 
            // _module44
            // 
            this._module44.Location = new System.Drawing.Point(150, 193);
            this._module44.Name = "_module44";
            this._module44.Size = new System.Drawing.Size(13, 13);
            this._module44.State = BEMN.Forms.LedState.Off;
            this._module44.TabIndex = 27;
            // 
            // _module24
            // 
            this._module24.Location = new System.Drawing.Point(107, 136);
            this._module24.Name = "_module24";
            this._module24.Size = new System.Drawing.Size(13, 13);
            this._module24.State = BEMN.Forms.LedState.Off;
            this._module24.TabIndex = 11;
            // 
            // _module23
            // 
            this._module23.Location = new System.Drawing.Point(107, 117);
            this._module23.Name = "_module23";
            this._module23.Size = new System.Drawing.Size(13, 13);
            this._module23.State = BEMN.Forms.LedState.Off;
            this._module23.TabIndex = 9;
            // 
            // _module22
            // 
            this._module22.Location = new System.Drawing.Point(107, 98);
            this._module22.Name = "_module22";
            this._module22.Size = new System.Drawing.Size(13, 13);
            this._module22.State = BEMN.Forms.LedState.Off;
            this._module22.TabIndex = 7;
            // 
            // _module21
            // 
            this._module21.Location = new System.Drawing.Point(107, 79);
            this._module21.Name = "_module21";
            this._module21.Size = new System.Drawing.Size(13, 13);
            this._module21.State = BEMN.Forms.LedState.Off;
            this._module21.TabIndex = 5;
            // 
            // _module20
            // 
            this._module20.Location = new System.Drawing.Point(107, 60);
            this._module20.Name = "_module20";
            this._module20.Size = new System.Drawing.Size(13, 13);
            this._module20.State = BEMN.Forms.LedState.Off;
            this._module20.TabIndex = 3;
            // 
            // _module32
            // 
            this._module32.Location = new System.Drawing.Point(107, 288);
            this._module32.Name = "_module32";
            this._module32.Size = new System.Drawing.Size(13, 13);
            this._module32.State = BEMN.Forms.LedState.Off;
            this._module32.TabIndex = 27;
            // 
            // _module31
            // 
            this._module31.Location = new System.Drawing.Point(107, 269);
            this._module31.Name = "_module31";
            this._module31.Size = new System.Drawing.Size(13, 13);
            this._module31.State = BEMN.Forms.LedState.Off;
            this._module31.TabIndex = 25;
            // 
            // _module35
            // 
            this._module35.Location = new System.Drawing.Point(150, 22);
            this._module35.Name = "_module35";
            this._module35.Size = new System.Drawing.Size(13, 13);
            this._module35.State = BEMN.Forms.LedState.Off;
            this._module35.TabIndex = 23;
            // 
            // _module41
            // 
            this._module41.Location = new System.Drawing.Point(150, 136);
            this._module41.Name = "_module41";
            this._module41.Size = new System.Drawing.Size(13, 13);
            this._module41.State = BEMN.Forms.LedState.Off;
            this._module41.TabIndex = 21;
            // 
            // _module48
            // 
            this._module48.Location = new System.Drawing.Point(150, 269);
            this._module48.Name = "_module48";
            this._module48.Size = new System.Drawing.Size(13, 13);
            this._module48.State = BEMN.Forms.LedState.Off;
            this._module48.TabIndex = 25;
            // 
            // _module43
            // 
            this._module43.Location = new System.Drawing.Point(150, 174);
            this._module43.Name = "_module43";
            this._module43.Size = new System.Drawing.Size(13, 13);
            this._module43.State = BEMN.Forms.LedState.Off;
            this._module43.TabIndex = 25;
            // 
            // _module36
            // 
            this._module36.Location = new System.Drawing.Point(150, 41);
            this._module36.Name = "_module36";
            this._module36.Size = new System.Drawing.Size(13, 13);
            this._module36.State = BEMN.Forms.LedState.Off;
            this._module36.TabIndex = 25;
            // 
            // _module47
            // 
            this._module47.Location = new System.Drawing.Point(150, 250);
            this._module47.Name = "_module47";
            this._module47.Size = new System.Drawing.Size(13, 13);
            this._module47.State = BEMN.Forms.LedState.Off;
            this._module47.TabIndex = 23;
            // 
            // _module42
            // 
            this._module42.Location = new System.Drawing.Point(150, 155);
            this._module42.Name = "_module42";
            this._module42.Size = new System.Drawing.Size(13, 13);
            this._module42.State = BEMN.Forms.LedState.Off;
            this._module42.TabIndex = 23;
            // 
            // _releGB
            // 
            this._releGB.Location = new System.Drawing.Point(58, 3);
            this._releGB.Name = "_releGB";
            this._releGB.Size = new System.Drawing.Size(136, 355);
            this._releGB.TabIndex = 54;
            this._releGB.TabStop = false;
            this._releGB.Text = "Реле";
            // 
            // _virtualReleGB
            // 
            this._virtualReleGB.Location = new System.Drawing.Point(200, 3);
            this._virtualReleGB.Name = "_virtualReleGB";
            this._virtualReleGB.Size = new System.Drawing.Size(123, 355);
            this._virtualReleGB.TabIndex = 55;
            this._virtualReleGB.TabStop = false;
            this._virtualReleGB.Text = "Виртуальные реле";
            // 
            // _discretTabPage1
            // 
            this._discretTabPage1.Controls.Add(this.groupBox17);
            this._discretTabPage1.Controls.Add(this.groupBox6);
            this._discretTabPage1.Controls.Add(this.groupBox54);
            this._discretTabPage1.Controls.Add(this.BGS_Group);
            this._discretTabPage1.Controls.Add(this.groupBox14);
            this._discretTabPage1.Controls.Add(this.splGroupBox);
            this._discretTabPage1.Controls.Add(this.dugGroup);
            this._discretTabPage1.Controls.Add(this.groupBox26);
            this._discretTabPage1.Controls.Add(this.groupBox11);
            this._discretTabPage1.Controls.Add(this.groupBox10);
            this._discretTabPage1.Controls.Add(this.groupBox9);
            this._discretTabPage1.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage1.Name = "_discretTabPage1";
            this._discretTabPage1.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage1.Size = new System.Drawing.Size(911, 599);
            this._discretTabPage1.TabIndex = 1;
            this._discretTabPage1.Text = "Основные сигналы";
            this._discretTabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this._vz16m);
            this.groupBox17.Controls.Add(this.label4);
            this.groupBox17.Controls.Add(this._vz15m);
            this.groupBox17.Controls.Add(this.label15);
            this.groupBox17.Controls.Add(this._vz14m);
            this.groupBox17.Controls.Add(this.label16);
            this.groupBox17.Controls.Add(this._vz13m);
            this.groupBox17.Controls.Add(this.label17);
            this.groupBox17.Controls.Add(this._vz12m);
            this.groupBox17.Controls.Add(this.label18);
            this.groupBox17.Controls.Add(this._vz11m);
            this.groupBox17.Controls.Add(this.label20);
            this.groupBox17.Controls.Add(this._vz10m);
            this.groupBox17.Controls.Add(this.label31);
            this.groupBox17.Controls.Add(this._vz9m);
            this.groupBox17.Controls.Add(this.label33);
            this.groupBox17.Controls.Add(this._vz8m);
            this.groupBox17.Controls.Add(this.label34);
            this.groupBox17.Controls.Add(this._vz7m);
            this.groupBox17.Controls.Add(this.label35);
            this.groupBox17.Controls.Add(this._vz6m);
            this.groupBox17.Controls.Add(this.label36);
            this.groupBox17.Controls.Add(this._vz5m);
            this.groupBox17.Controls.Add(this.label37);
            this.groupBox17.Controls.Add(this._vz4m);
            this.groupBox17.Controls.Add(this.label38);
            this.groupBox17.Controls.Add(this._vz3m);
            this.groupBox17.Controls.Add(this.label39);
            this.groupBox17.Controls.Add(this._vz2m);
            this.groupBox17.Controls.Add(this.label41);
            this.groupBox17.Controls.Add(this._vz1m);
            this.groupBox17.Controls.Add(this.label42);
            this.groupBox17.Location = new System.Drawing.Point(188, 177);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(172, 175);
            this.groupBox17.TabIndex = 73;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Внешние защиты";
            // 
            // _vz16m
            // 
            this._vz16m.Location = new System.Drawing.Point(84, 152);
            this._vz16m.Name = "_vz16m";
            this._vz16m.Size = new System.Drawing.Size(13, 13);
            this._vz16m.State = BEMN.Forms.LedState.Off;
            this._vz16m.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(103, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "ВНЕШ. 16";
            // 
            // _vz15m
            // 
            this._vz15m.Location = new System.Drawing.Point(84, 133);
            this._vz15m.Name = "_vz15m";
            this._vz15m.Size = new System.Drawing.Size(13, 13);
            this._vz15m.State = BEMN.Forms.LedState.Off;
            this._vz15m.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(103, 133);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "ВНЕШ. 15";
            // 
            // _vz14m
            // 
            this._vz14m.Location = new System.Drawing.Point(84, 114);
            this._vz14m.Name = "_vz14m";
            this._vz14m.Size = new System.Drawing.Size(13, 13);
            this._vz14m.State = BEMN.Forms.LedState.Off;
            this._vz14m.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(103, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 26;
            this.label16.Text = "ВНЕШ. 14";
            // 
            // _vz13m
            // 
            this._vz13m.Location = new System.Drawing.Point(84, 95);
            this._vz13m.Name = "_vz13m";
            this._vz13m.Size = new System.Drawing.Size(13, 13);
            this._vz13m.State = BEMN.Forms.LedState.Off;
            this._vz13m.TabIndex = 25;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(103, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "ВНЕШ. 13";
            // 
            // _vz12m
            // 
            this._vz12m.Location = new System.Drawing.Point(84, 76);
            this._vz12m.Name = "_vz12m";
            this._vz12m.Size = new System.Drawing.Size(13, 13);
            this._vz12m.State = BEMN.Forms.LedState.Off;
            this._vz12m.TabIndex = 23;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(103, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "ВНЕШ. 12";
            // 
            // _vz11m
            // 
            this._vz11m.Location = new System.Drawing.Point(84, 57);
            this._vz11m.Name = "_vz11m";
            this._vz11m.Size = new System.Drawing.Size(13, 13);
            this._vz11m.State = BEMN.Forms.LedState.Off;
            this._vz11m.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(103, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "ВНЕШ. 11";
            // 
            // _vz10m
            // 
            this._vz10m.Location = new System.Drawing.Point(84, 38);
            this._vz10m.Name = "_vz10m";
            this._vz10m.Size = new System.Drawing.Size(13, 13);
            this._vz10m.State = BEMN.Forms.LedState.Off;
            this._vz10m.TabIndex = 19;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(103, 38);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(56, 13);
            this.label31.TabIndex = 18;
            this.label31.Text = "ВНЕШ. 10";
            // 
            // _vz9m
            // 
            this._vz9m.Location = new System.Drawing.Point(84, 19);
            this._vz9m.Name = "_vz9m";
            this._vz9m.Size = new System.Drawing.Size(13, 13);
            this._vz9m.State = BEMN.Forms.LedState.Off;
            this._vz9m.TabIndex = 17;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(103, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(50, 13);
            this.label33.TabIndex = 16;
            this.label33.Text = "ВНЕШ. 9";
            // 
            // _vz8m
            // 
            this._vz8m.Location = new System.Drawing.Point(6, 152);
            this._vz8m.Name = "_vz8m";
            this._vz8m.Size = new System.Drawing.Size(13, 13);
            this._vz8m.State = BEMN.Forms.LedState.Off;
            this._vz8m.TabIndex = 15;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(25, 152);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(50, 13);
            this.label34.TabIndex = 14;
            this.label34.Text = "ВНЕШ. 8";
            // 
            // _vz7m
            // 
            this._vz7m.Location = new System.Drawing.Point(6, 133);
            this._vz7m.Name = "_vz7m";
            this._vz7m.Size = new System.Drawing.Size(13, 13);
            this._vz7m.State = BEMN.Forms.LedState.Off;
            this._vz7m.TabIndex = 13;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(25, 133);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 13);
            this.label35.TabIndex = 12;
            this.label35.Text = "ВНЕШ. 7";
            // 
            // _vz6m
            // 
            this._vz6m.Location = new System.Drawing.Point(6, 114);
            this._vz6m.Name = "_vz6m";
            this._vz6m.Size = new System.Drawing.Size(13, 13);
            this._vz6m.State = BEMN.Forms.LedState.Off;
            this._vz6m.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(25, 114);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(50, 13);
            this.label36.TabIndex = 10;
            this.label36.Text = "ВНЕШ. 6";
            // 
            // _vz5m
            // 
            this._vz5m.Location = new System.Drawing.Point(6, 95);
            this._vz5m.Name = "_vz5m";
            this._vz5m.Size = new System.Drawing.Size(13, 13);
            this._vz5m.State = BEMN.Forms.LedState.Off;
            this._vz5m.TabIndex = 9;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(25, 95);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(50, 13);
            this.label37.TabIndex = 8;
            this.label37.Text = "ВНЕШ. 5";
            // 
            // _vz4m
            // 
            this._vz4m.Location = new System.Drawing.Point(6, 76);
            this._vz4m.Name = "_vz4m";
            this._vz4m.Size = new System.Drawing.Size(13, 13);
            this._vz4m.State = BEMN.Forms.LedState.Off;
            this._vz4m.TabIndex = 7;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(25, 76);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 13);
            this.label38.TabIndex = 6;
            this.label38.Text = "ВНЕШ. 4";
            // 
            // _vz3m
            // 
            this._vz3m.Location = new System.Drawing.Point(6, 57);
            this._vz3m.Name = "_vz3m";
            this._vz3m.Size = new System.Drawing.Size(13, 13);
            this._vz3m.State = BEMN.Forms.LedState.Off;
            this._vz3m.TabIndex = 5;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(25, 57);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(50, 13);
            this.label39.TabIndex = 4;
            this.label39.Text = "ВНЕШ. 3";
            // 
            // _vz2m
            // 
            this._vz2m.Location = new System.Drawing.Point(6, 38);
            this._vz2m.Name = "_vz2m";
            this._vz2m.Size = new System.Drawing.Size(13, 13);
            this._vz2m.State = BEMN.Forms.LedState.Off;
            this._vz2m.TabIndex = 3;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(25, 38);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(50, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "ВНЕШ. 2";
            // 
            // _vz1m
            // 
            this._vz1m.Location = new System.Drawing.Point(6, 19);
            this._vz1m.Name = "_vz1m";
            this._vz1m.Size = new System.Drawing.Size(13, 13);
            this._vz1m.State = BEMN.Forms.LedState.Off;
            this._vz1m.TabIndex = 1;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(25, 19);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(50, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "ВНЕШ. 1";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._IdMax1Main);
            this.groupBox6.Controls.Add(this._IdMax2Main);
            this.groupBox6.Controls.Add(this._IdMax2IOMain);
            this.groupBox6.Controls.Add(this._IdMax1IOMain);
            this.groupBox6.Controls.Add(this._IdMax2MgnMain);
            this.groupBox6.Controls.Add(this._IdMax2L);
            this.groupBox6.Controls.Add(this._IdMax2IOL);
            this.groupBox6.Controls.Add(this._IdMax1L);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this._IdMax1IOL);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.groupBox6.Location = new System.Drawing.Point(8, 177);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(84, 99);
            this.groupBox6.TabIndex = 72;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Защиты Iд";
            // 
            // _IdMax1Main
            // 
            this._IdMax1Main.Location = new System.Drawing.Point(66, 32);
            this._IdMax1Main.Name = "_IdMax1Main";
            this._IdMax1Main.Size = new System.Drawing.Size(13, 13);
            this._IdMax1Main.State = BEMN.Forms.LedState.Off;
            this._IdMax1Main.TabIndex = 62;
            // 
            // _IdMax2Main
            // 
            this._IdMax2Main.Location = new System.Drawing.Point(66, 60);
            this._IdMax2Main.Name = "_IdMax2Main";
            this._IdMax2Main.Size = new System.Drawing.Size(13, 13);
            this._IdMax2Main.State = BEMN.Forms.LedState.Off;
            this._IdMax2Main.TabIndex = 60;
            // 
            // _IdMax2IOMain
            // 
            this._IdMax2IOMain.Location = new System.Drawing.Point(66, 46);
            this._IdMax2IOMain.Name = "_IdMax2IOMain";
            this._IdMax2IOMain.Size = new System.Drawing.Size(13, 13);
            this._IdMax2IOMain.State = BEMN.Forms.LedState.Off;
            this._IdMax2IOMain.TabIndex = 59;
            // 
            // _IdMax1IOMain
            // 
            this._IdMax1IOMain.Location = new System.Drawing.Point(66, 18);
            this._IdMax1IOMain.Name = "_IdMax1IOMain";
            this._IdMax1IOMain.Size = new System.Drawing.Size(13, 13);
            this._IdMax1IOMain.State = BEMN.Forms.LedState.Off;
            this._IdMax1IOMain.TabIndex = 61;
            // 
            // _IdMax2MgnMain
            // 
            this._IdMax2MgnMain.Location = new System.Drawing.Point(66, 74);
            this._IdMax2MgnMain.Name = "_IdMax2MgnMain";
            this._IdMax2MgnMain.Size = new System.Drawing.Size(13, 13);
            this._IdMax2MgnMain.State = BEMN.Forms.LedState.Off;
            this._IdMax2MgnMain.TabIndex = 58;
            // 
            // _IdMax2L
            // 
            this._IdMax2L.AutoSize = true;
            this._IdMax2L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax2L.Location = new System.Drawing.Point(6, 60);
            this._IdMax2L.Name = "_IdMax2L";
            this._IdMax2L.Size = new System.Drawing.Size(31, 13);
            this._IdMax2L.TabIndex = 55;
            this._IdMax2L.Text = "Iд >>";
            // 
            // _IdMax2IOL
            // 
            this._IdMax2IOL.AutoSize = true;
            this._IdMax2IOL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax2IOL.Location = new System.Drawing.Point(6, 45);
            this._IdMax2IOL.Name = "_IdMax2IOL";
            this._IdMax2IOL.Size = new System.Drawing.Size(53, 13);
            this._IdMax2IOL.TabIndex = 54;
            this._IdMax2IOL.Text = "Iд >>  ИО";
            // 
            // _IdMax1L
            // 
            this._IdMax1L.AutoSize = true;
            this._IdMax1L.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax1L.Location = new System.Drawing.Point(6, 32);
            this._IdMax1L.Name = "_IdMax1L";
            this._IdMax1L.Size = new System.Drawing.Size(25, 13);
            this._IdMax1L.TabIndex = 57;
            this._IdMax1L.Text = "Iд >";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(6, 73);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(59, 13);
            this.label23.TabIndex = 53;
            this.label23.Text = "Iд >>  мгн.";
            // 
            // _IdMax1IOL
            // 
            this._IdMax1IOL.AutoSize = true;
            this._IdMax1IOL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._IdMax1IOL.Location = new System.Drawing.Point(6, 18);
            this._IdMax1IOL.Name = "_IdMax1IOL";
            this._IdMax1IOL.Size = new System.Drawing.Size(53, 13);
            this._IdMax1IOL.TabIndex = 56;
            this._IdMax1IOL.Text = "Iд >    ИО";
            // 
            // groupBox54
            // 
            this.groupBox54.Controls.Add(this._faultHardware1);
            this.groupBox54.Controls.Add(this.label372);
            this.groupBox54.Controls.Add(this._faultLogic1);
            this.groupBox54.Controls.Add(this.label12);
            this.groupBox54.Controls.Add(this._faultSwitchOff1);
            this.groupBox54.Controls.Add(this.label378);
            this.groupBox54.Controls.Add(this._faultMeasuringF1);
            this.groupBox54.Controls.Add(this.label375);
            this.groupBox54.Controls.Add(this._faultMeasuringU1);
            this.groupBox54.Controls.Add(this.label376);
            this.groupBox54.Controls.Add(this._faultSoftware1);
            this.groupBox54.Controls.Add(this.label377);
            this.groupBox54.Location = new System.Drawing.Point(366, 177);
            this.groupBox54.Name = "groupBox54";
            this.groupBox54.Size = new System.Drawing.Size(113, 134);
            this.groupBox54.TabIndex = 69;
            this.groupBox54.TabStop = false;
            this.groupBox54.Text = "Неисправности";
            // 
            // _faultHardware1
            // 
            this._faultHardware1.Location = new System.Drawing.Point(6, 19);
            this._faultHardware1.Name = "_faultHardware1";
            this._faultHardware1.Size = new System.Drawing.Size(13, 13);
            this._faultHardware1.State = BEMN.Forms.LedState.Off;
            this._faultHardware1.TabIndex = 37;
            // 
            // label372
            // 
            this.label372.AutoSize = true;
            this.label372.Location = new System.Drawing.Point(25, 19);
            this.label372.Name = "label372";
            this.label372.Size = new System.Drawing.Size(67, 13);
            this.label372.TabIndex = 36;
            this.label372.Text = "Аппаратная";
            // 
            // _faultLogic1
            // 
            this._faultLogic1.Location = new System.Drawing.Point(6, 114);
            this._faultLogic1.Name = "_faultLogic1";
            this._faultLogic1.Size = new System.Drawing.Size(13, 13);
            this._faultLogic1.State = BEMN.Forms.LedState.Off;
            this._faultLogic1.TabIndex = 35;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Логики";
            // 
            // _faultSwitchOff1
            // 
            this._faultSwitchOff1.Location = new System.Drawing.Point(6, 95);
            this._faultSwitchOff1.Name = "_faultSwitchOff1";
            this._faultSwitchOff1.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff1.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff1.TabIndex = 35;
            // 
            // label378
            // 
            this.label378.AutoSize = true;
            this.label378.Location = new System.Drawing.Point(25, 95);
            this.label378.Name = "label378";
            this.label378.Size = new System.Drawing.Size(76, 13);
            this.label378.TabIndex = 34;
            this.label378.Text = "Выключателя";
            // 
            // _faultMeasuringF1
            // 
            this._faultMeasuringF1.Location = new System.Drawing.Point(6, 76);
            this._faultMeasuringF1.Name = "_faultMeasuringF1";
            this._faultMeasuringF1.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringF1.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringF1.TabIndex = 35;
            // 
            // label375
            // 
            this.label375.AutoSize = true;
            this.label375.Location = new System.Drawing.Point(25, 76);
            this.label375.Name = "label375";
            this.label375.Size = new System.Drawing.Size(74, 13);
            this.label375.TabIndex = 34;
            this.label375.Text = "Измерения F";
            // 
            // _faultMeasuringU1
            // 
            this._faultMeasuringU1.Location = new System.Drawing.Point(6, 57);
            this._faultMeasuringU1.Name = "_faultMeasuringU1";
            this._faultMeasuringU1.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringU1.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringU1.TabIndex = 31;
            // 
            // label376
            // 
            this.label376.AutoSize = true;
            this.label376.Location = new System.Drawing.Point(25, 57);
            this.label376.Name = "label376";
            this.label376.Size = new System.Drawing.Size(76, 13);
            this.label376.TabIndex = 30;
            this.label376.Text = "Измерения U";
            // 
            // _faultSoftware1
            // 
            this._faultSoftware1.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware1.Name = "_faultSoftware1";
            this._faultSoftware1.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware1.State = BEMN.Forms.LedState.Off;
            this._faultSoftware1.TabIndex = 27;
            // 
            // label377
            // 
            this.label377.AutoSize = true;
            this.label377.Location = new System.Drawing.Point(25, 38);
            this.label377.Name = "label377";
            this.label377.Size = new System.Drawing.Size(78, 13);
            this.label377.TabIndex = 26;
            this.label377.Text = "Программная";
            // 
            // BGS_Group
            // 
            this.BGS_Group.Controls.Add(this._bgs16);
            this.BGS_Group.Controls.Add(this.label352);
            this.BGS_Group.Controls.Add(this._bgs15);
            this.BGS_Group.Controls.Add(this.label412);
            this.BGS_Group.Controls.Add(this._bgs14);
            this.BGS_Group.Controls.Add(this.label454);
            this.BGS_Group.Controls.Add(this._bgs13);
            this.BGS_Group.Controls.Add(this.label457);
            this.BGS_Group.Controls.Add(this._bgs12);
            this.BGS_Group.Controls.Add(this.label458);
            this.BGS_Group.Controls.Add(this._bgs11);
            this.BGS_Group.Controls.Add(this.label459);
            this.BGS_Group.Controls.Add(this._bgs10);
            this.BGS_Group.Controls.Add(this.label460);
            this.BGS_Group.Controls.Add(this._bgs9);
            this.BGS_Group.Controls.Add(this.label461);
            this.BGS_Group.Controls.Add(this._bgs8);
            this.BGS_Group.Controls.Add(this.label462);
            this.BGS_Group.Controls.Add(this._bgs7);
            this.BGS_Group.Controls.Add(this.label463);
            this.BGS_Group.Controls.Add(this._bgs6);
            this.BGS_Group.Controls.Add(this.label464);
            this.BGS_Group.Controls.Add(this._bgs5);
            this.BGS_Group.Controls.Add(this.label465);
            this.BGS_Group.Controls.Add(this._bgs4);
            this.BGS_Group.Controls.Add(this.label466);
            this.BGS_Group.Controls.Add(this._bgs3);
            this.BGS_Group.Controls.Add(this.label467);
            this.BGS_Group.Controls.Add(this._bgs2);
            this.BGS_Group.Controls.Add(this.label468);
            this.BGS_Group.Controls.Add(this._bgs1);
            this.BGS_Group.Controls.Add(this.label469);
            this.BGS_Group.Location = new System.Drawing.Point(702, 3);
            this.BGS_Group.Name = "BGS_Group";
            this.BGS_Group.Size = new System.Drawing.Size(141, 172);
            this.BGS_Group.TabIndex = 67;
            this.BGS_Group.TabStop = false;
            this.BGS_Group.Text = "Сигналы БГС";
            // 
            // _bgs16
            // 
            this._bgs16.Location = new System.Drawing.Point(71, 152);
            this._bgs16.Name = "_bgs16";
            this._bgs16.Size = new System.Drawing.Size(13, 13);
            this._bgs16.State = BEMN.Forms.LedState.Off;
            this._bgs16.TabIndex = 31;
            // 
            // label352
            // 
            this.label352.AutoSize = true;
            this.label352.Location = new System.Drawing.Point(90, 152);
            this.label352.Name = "label352";
            this.label352.Size = new System.Drawing.Size(39, 13);
            this.label352.TabIndex = 30;
            this.label352.Text = "БГС16";
            // 
            // _bgs15
            // 
            this._bgs15.Location = new System.Drawing.Point(71, 133);
            this._bgs15.Name = "_bgs15";
            this._bgs15.Size = new System.Drawing.Size(13, 13);
            this._bgs15.State = BEMN.Forms.LedState.Off;
            this._bgs15.TabIndex = 29;
            // 
            // label412
            // 
            this.label412.AutoSize = true;
            this.label412.Location = new System.Drawing.Point(90, 133);
            this.label412.Name = "label412";
            this.label412.Size = new System.Drawing.Size(39, 13);
            this.label412.TabIndex = 28;
            this.label412.Text = "БГС15";
            // 
            // _bgs14
            // 
            this._bgs14.Location = new System.Drawing.Point(71, 114);
            this._bgs14.Name = "_bgs14";
            this._bgs14.Size = new System.Drawing.Size(13, 13);
            this._bgs14.State = BEMN.Forms.LedState.Off;
            this._bgs14.TabIndex = 27;
            // 
            // label454
            // 
            this.label454.AutoSize = true;
            this.label454.Location = new System.Drawing.Point(90, 114);
            this.label454.Name = "label454";
            this.label454.Size = new System.Drawing.Size(39, 13);
            this.label454.TabIndex = 26;
            this.label454.Text = "БГС14";
            // 
            // _bgs13
            // 
            this._bgs13.Location = new System.Drawing.Point(71, 95);
            this._bgs13.Name = "_bgs13";
            this._bgs13.Size = new System.Drawing.Size(13, 13);
            this._bgs13.State = BEMN.Forms.LedState.Off;
            this._bgs13.TabIndex = 25;
            // 
            // label457
            // 
            this.label457.AutoSize = true;
            this.label457.Location = new System.Drawing.Point(90, 95);
            this.label457.Name = "label457";
            this.label457.Size = new System.Drawing.Size(39, 13);
            this.label457.TabIndex = 24;
            this.label457.Text = "БГС13";
            // 
            // _bgs12
            // 
            this._bgs12.Location = new System.Drawing.Point(71, 76);
            this._bgs12.Name = "_bgs12";
            this._bgs12.Size = new System.Drawing.Size(13, 13);
            this._bgs12.State = BEMN.Forms.LedState.Off;
            this._bgs12.TabIndex = 23;
            // 
            // label458
            // 
            this.label458.AutoSize = true;
            this.label458.Location = new System.Drawing.Point(90, 76);
            this.label458.Name = "label458";
            this.label458.Size = new System.Drawing.Size(39, 13);
            this.label458.TabIndex = 22;
            this.label458.Text = "БГС12";
            // 
            // _bgs11
            // 
            this._bgs11.Location = new System.Drawing.Point(71, 57);
            this._bgs11.Name = "_bgs11";
            this._bgs11.Size = new System.Drawing.Size(13, 13);
            this._bgs11.State = BEMN.Forms.LedState.Off;
            this._bgs11.TabIndex = 21;
            // 
            // label459
            // 
            this.label459.AutoSize = true;
            this.label459.Location = new System.Drawing.Point(90, 57);
            this.label459.Name = "label459";
            this.label459.Size = new System.Drawing.Size(39, 13);
            this.label459.TabIndex = 20;
            this.label459.Text = "БГС11";
            // 
            // _bgs10
            // 
            this._bgs10.Location = new System.Drawing.Point(71, 38);
            this._bgs10.Name = "_bgs10";
            this._bgs10.Size = new System.Drawing.Size(13, 13);
            this._bgs10.State = BEMN.Forms.LedState.Off;
            this._bgs10.TabIndex = 19;
            // 
            // label460
            // 
            this.label460.AutoSize = true;
            this.label460.Location = new System.Drawing.Point(90, 38);
            this.label460.Name = "label460";
            this.label460.Size = new System.Drawing.Size(39, 13);
            this.label460.TabIndex = 18;
            this.label460.Text = "БГС10";
            // 
            // _bgs9
            // 
            this._bgs9.Location = new System.Drawing.Point(71, 19);
            this._bgs9.Name = "_bgs9";
            this._bgs9.Size = new System.Drawing.Size(13, 13);
            this._bgs9.State = BEMN.Forms.LedState.Off;
            this._bgs9.TabIndex = 17;
            // 
            // label461
            // 
            this.label461.AutoSize = true;
            this.label461.Location = new System.Drawing.Point(90, 19);
            this.label461.Name = "label461";
            this.label461.Size = new System.Drawing.Size(33, 13);
            this.label461.TabIndex = 16;
            this.label461.Text = "БГС9";
            // 
            // _bgs8
            // 
            this._bgs8.Location = new System.Drawing.Point(6, 152);
            this._bgs8.Name = "_bgs8";
            this._bgs8.Size = new System.Drawing.Size(13, 13);
            this._bgs8.State = BEMN.Forms.LedState.Off;
            this._bgs8.TabIndex = 15;
            // 
            // label462
            // 
            this.label462.AutoSize = true;
            this.label462.Location = new System.Drawing.Point(25, 152);
            this.label462.Name = "label462";
            this.label462.Size = new System.Drawing.Size(33, 13);
            this.label462.TabIndex = 14;
            this.label462.Text = "БГС8";
            // 
            // _bgs7
            // 
            this._bgs7.Location = new System.Drawing.Point(6, 133);
            this._bgs7.Name = "_bgs7";
            this._bgs7.Size = new System.Drawing.Size(13, 13);
            this._bgs7.State = BEMN.Forms.LedState.Off;
            this._bgs7.TabIndex = 13;
            // 
            // label463
            // 
            this.label463.AutoSize = true;
            this.label463.Location = new System.Drawing.Point(25, 133);
            this.label463.Name = "label463";
            this.label463.Size = new System.Drawing.Size(33, 13);
            this.label463.TabIndex = 12;
            this.label463.Text = "БГС7";
            // 
            // _bgs6
            // 
            this._bgs6.Location = new System.Drawing.Point(6, 114);
            this._bgs6.Name = "_bgs6";
            this._bgs6.Size = new System.Drawing.Size(13, 13);
            this._bgs6.State = BEMN.Forms.LedState.Off;
            this._bgs6.TabIndex = 11;
            // 
            // label464
            // 
            this.label464.AutoSize = true;
            this.label464.Location = new System.Drawing.Point(25, 114);
            this.label464.Name = "label464";
            this.label464.Size = new System.Drawing.Size(33, 13);
            this.label464.TabIndex = 10;
            this.label464.Text = "БГС6";
            // 
            // _bgs5
            // 
            this._bgs5.Location = new System.Drawing.Point(6, 95);
            this._bgs5.Name = "_bgs5";
            this._bgs5.Size = new System.Drawing.Size(13, 13);
            this._bgs5.State = BEMN.Forms.LedState.Off;
            this._bgs5.TabIndex = 9;
            // 
            // label465
            // 
            this.label465.AutoSize = true;
            this.label465.Location = new System.Drawing.Point(25, 95);
            this.label465.Name = "label465";
            this.label465.Size = new System.Drawing.Size(33, 13);
            this.label465.TabIndex = 8;
            this.label465.Text = "БГС5";
            // 
            // _bgs4
            // 
            this._bgs4.Location = new System.Drawing.Point(6, 76);
            this._bgs4.Name = "_bgs4";
            this._bgs4.Size = new System.Drawing.Size(13, 13);
            this._bgs4.State = BEMN.Forms.LedState.Off;
            this._bgs4.TabIndex = 7;
            // 
            // label466
            // 
            this.label466.AutoSize = true;
            this.label466.Location = new System.Drawing.Point(25, 76);
            this.label466.Name = "label466";
            this.label466.Size = new System.Drawing.Size(33, 13);
            this.label466.TabIndex = 6;
            this.label466.Text = "БГС4";
            // 
            // _bgs3
            // 
            this._bgs3.Location = new System.Drawing.Point(6, 57);
            this._bgs3.Name = "_bgs3";
            this._bgs3.Size = new System.Drawing.Size(13, 13);
            this._bgs3.State = BEMN.Forms.LedState.Off;
            this._bgs3.TabIndex = 5;
            // 
            // label467
            // 
            this.label467.AutoSize = true;
            this.label467.Location = new System.Drawing.Point(25, 57);
            this.label467.Name = "label467";
            this.label467.Size = new System.Drawing.Size(33, 13);
            this.label467.TabIndex = 4;
            this.label467.Text = "БГС3";
            // 
            // _bgs2
            // 
            this._bgs2.Location = new System.Drawing.Point(6, 38);
            this._bgs2.Name = "_bgs2";
            this._bgs2.Size = new System.Drawing.Size(13, 13);
            this._bgs2.State = BEMN.Forms.LedState.Off;
            this._bgs2.TabIndex = 3;
            // 
            // label468
            // 
            this.label468.AutoSize = true;
            this.label468.Location = new System.Drawing.Point(25, 38);
            this.label468.Name = "label468";
            this.label468.Size = new System.Drawing.Size(33, 13);
            this.label468.TabIndex = 2;
            this.label468.Text = "БГС2";
            // 
            // _bgs1
            // 
            this._bgs1.Location = new System.Drawing.Point(6, 19);
            this._bgs1.Name = "_bgs1";
            this._bgs1.Size = new System.Drawing.Size(13, 13);
            this._bgs1.State = BEMN.Forms.LedState.Off;
            this._bgs1.TabIndex = 1;
            // 
            // label469
            // 
            this.label469.AutoSize = true;
            this.label469.Location = new System.Drawing.Point(25, 19);
            this.label469.Name = "label469";
            this.label469.Size = new System.Drawing.Size(33, 13);
            this.label469.TabIndex = 0;
            this.label469.Text = "БГС1";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this._faultMain);
            this.groupBox14.Controls.Add(this.label210);
            this.groupBox14.Controls.Add(this._faultOffMain);
            this.groupBox14.Controls.Add(this.label21);
            this.groupBox14.Controls.Add(this._accelerationMain);
            this.groupBox14.Controls.Add(this._auto4Led);
            this.groupBox14.Location = new System.Drawing.Point(485, 177);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(141, 86);
            this.groupBox14.TabIndex = 71;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Общие сигналы";
            // 
            // _faultMain
            // 
            this._faultMain.Location = new System.Drawing.Point(6, 19);
            this._faultMain.Name = "_faultMain";
            this._faultMain.Size = new System.Drawing.Size(13, 13);
            this._faultMain.State = BEMN.Forms.LedState.Off;
            this._faultMain.TabIndex = 37;
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(25, 19);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(86, 13);
            this.label210.TabIndex = 36;
            this.label210.Text = "Неисправность";
            // 
            // _faultOffMain
            // 
            this._faultOffMain.Location = new System.Drawing.Point(6, 57);
            this._faultOffMain.Name = "_faultOffMain";
            this._faultOffMain.Size = new System.Drawing.Size(13, 13);
            this._faultOffMain.State = BEMN.Forms.LedState.Off;
            this._faultOffMain.TabIndex = 35;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 57);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Авар. отключение";
            // 
            // _accelerationMain
            // 
            this._accelerationMain.Location = new System.Drawing.Point(6, 38);
            this._accelerationMain.Name = "_accelerationMain";
            this._accelerationMain.Size = new System.Drawing.Size(13, 13);
            this._accelerationMain.State = BEMN.Forms.LedState.Off;
            this._accelerationMain.TabIndex = 31;
            // 
            // _auto4Led
            // 
            this._auto4Led.AutoSize = true;
            this._auto4Led.Location = new System.Drawing.Point(25, 38);
            this._auto4Led.Name = "_auto4Led";
            this._auto4Led.Size = new System.Drawing.Size(81, 13);
            this._auto4Led.TabIndex = 30;
            this._auto4Led.Text = "Ускор. по вкл.";
            // 
            // splGroupBox
            // 
            this.splGroupBox.Controls.Add(this._ssl48);
            this.splGroupBox.Controls.Add(this._ssl40);
            this.splGroupBox.Controls.Add(this._ssl32);
            this.splGroupBox.Controls.Add(this._sslLabel48);
            this.splGroupBox.Controls.Add(this._sslLabel40);
            this.splGroupBox.Controls.Add(this.label237);
            this.splGroupBox.Controls.Add(this._ssl47);
            this.splGroupBox.Controls.Add(this._ssl39);
            this.splGroupBox.Controls.Add(this._ssl31);
            this.splGroupBox.Controls.Add(this._sslLabel47);
            this.splGroupBox.Controls.Add(this._sslLabel39);
            this.splGroupBox.Controls.Add(this.label238);
            this.splGroupBox.Controls.Add(this._ssl46);
            this.splGroupBox.Controls.Add(this._ssl38);
            this.splGroupBox.Controls.Add(this._ssl30);
            this.splGroupBox.Controls.Add(this._sslLabel46);
            this.splGroupBox.Controls.Add(this._sslLabel38);
            this.splGroupBox.Controls.Add(this.label239);
            this.splGroupBox.Controls.Add(this._ssl45);
            this.splGroupBox.Controls.Add(this._ssl37);
            this.splGroupBox.Controls.Add(this._ssl29);
            this.splGroupBox.Controls.Add(this._sslLabel45);
            this.splGroupBox.Controls.Add(this._sslLabel37);
            this.splGroupBox.Controls.Add(this.label240);
            this.splGroupBox.Controls.Add(this._ssl44);
            this.splGroupBox.Controls.Add(this._ssl36);
            this.splGroupBox.Controls.Add(this._ssl28);
            this.splGroupBox.Controls.Add(this._sslLabel44);
            this.splGroupBox.Controls.Add(this._sslLabel36);
            this.splGroupBox.Controls.Add(this.label241);
            this.splGroupBox.Controls.Add(this._ssl43);
            this.splGroupBox.Controls.Add(this._ssl35);
            this.splGroupBox.Controls.Add(this._ssl27);
            this.splGroupBox.Controls.Add(this._sslLabel43);
            this.splGroupBox.Controls.Add(this._sslLabel35);
            this.splGroupBox.Controls.Add(this.label242);
            this.splGroupBox.Controls.Add(this._ssl42);
            this.splGroupBox.Controls.Add(this._ssl34);
            this.splGroupBox.Controls.Add(this._ssl26);
            this.splGroupBox.Controls.Add(this._sslLabel42);
            this.splGroupBox.Controls.Add(this._sslLabel34);
            this.splGroupBox.Controls.Add(this.label243);
            this.splGroupBox.Controls.Add(this._ssl41);
            this.splGroupBox.Controls.Add(this._sslLabel41);
            this.splGroupBox.Controls.Add(this._ssl33);
            this.splGroupBox.Controls.Add(this._sslLabel33);
            this.splGroupBox.Controls.Add(this._ssl25);
            this.splGroupBox.Controls.Add(this.label244);
            this.splGroupBox.Controls.Add(this._ssl24);
            this.splGroupBox.Controls.Add(this.label245);
            this.splGroupBox.Controls.Add(this._ssl23);
            this.splGroupBox.Controls.Add(this.label246);
            this.splGroupBox.Controls.Add(this._ssl22);
            this.splGroupBox.Controls.Add(this.label247);
            this.splGroupBox.Controls.Add(this._ssl21);
            this.splGroupBox.Controls.Add(this.label248);
            this.splGroupBox.Controls.Add(this._ssl20);
            this.splGroupBox.Controls.Add(this.label249);
            this.splGroupBox.Controls.Add(this._ssl19);
            this.splGroupBox.Controls.Add(this.label250);
            this.splGroupBox.Controls.Add(this._ssl18);
            this.splGroupBox.Controls.Add(this.label251);
            this.splGroupBox.Controls.Add(this._ssl17);
            this.splGroupBox.Controls.Add(this.label252);
            this.splGroupBox.Controls.Add(this._ssl16);
            this.splGroupBox.Controls.Add(this.label253);
            this.splGroupBox.Controls.Add(this._ssl15);
            this.splGroupBox.Controls.Add(this.label254);
            this.splGroupBox.Controls.Add(this._ssl14);
            this.splGroupBox.Controls.Add(this.label255);
            this.splGroupBox.Controls.Add(this._ssl13);
            this.splGroupBox.Controls.Add(this.label256);
            this.splGroupBox.Controls.Add(this._ssl12);
            this.splGroupBox.Controls.Add(this.label257);
            this.splGroupBox.Controls.Add(this._ssl11);
            this.splGroupBox.Controls.Add(this.label258);
            this.splGroupBox.Controls.Add(this._ssl10);
            this.splGroupBox.Controls.Add(this.label259);
            this.splGroupBox.Controls.Add(this._ssl9);
            this.splGroupBox.Controls.Add(this.label260);
            this.splGroupBox.Controls.Add(this._ssl8);
            this.splGroupBox.Controls.Add(this.label261);
            this.splGroupBox.Controls.Add(this._ssl7);
            this.splGroupBox.Controls.Add(this.label262);
            this.splGroupBox.Controls.Add(this._ssl6);
            this.splGroupBox.Controls.Add(this.label263);
            this.splGroupBox.Controls.Add(this._ssl5);
            this.splGroupBox.Controls.Add(this.label264);
            this.splGroupBox.Controls.Add(this._ssl4);
            this.splGroupBox.Controls.Add(this.label265);
            this.splGroupBox.Controls.Add(this._ssl3);
            this.splGroupBox.Controls.Add(this.label266);
            this.splGroupBox.Controls.Add(this._ssl2);
            this.splGroupBox.Controls.Add(this.label267);
            this.splGroupBox.Controls.Add(this._ssl1);
            this.splGroupBox.Controls.Add(this.label268);
            this.splGroupBox.Location = new System.Drawing.Point(145, 3);
            this.splGroupBox.Name = "splGroupBox";
            this.splGroupBox.Size = new System.Drawing.Size(395, 172);
            this.splGroupBox.TabIndex = 68;
            this.splGroupBox.TabStop = false;
            this.splGroupBox.Text = "Сигналы СП-логики";
            // 
            // _ssl48
            // 
            this._ssl48.Location = new System.Drawing.Point(330, 152);
            this._ssl48.Name = "_ssl48";
            this._ssl48.Size = new System.Drawing.Size(13, 13);
            this._ssl48.State = BEMN.Forms.LedState.Off;
            this._ssl48.TabIndex = 63;
            // 
            // _ssl40
            // 
            this._ssl40.Location = new System.Drawing.Point(264, 152);
            this._ssl40.Name = "_ssl40";
            this._ssl40.Size = new System.Drawing.Size(13, 13);
            this._ssl40.State = BEMN.Forms.LedState.Off;
            this._ssl40.TabIndex = 63;
            // 
            // _ssl32
            // 
            this._ssl32.Location = new System.Drawing.Point(198, 152);
            this._ssl32.Name = "_ssl32";
            this._ssl32.Size = new System.Drawing.Size(13, 13);
            this._ssl32.State = BEMN.Forms.LedState.Off;
            this._ssl32.TabIndex = 63;
            // 
            // _sslLabel48
            // 
            this._sslLabel48.AutoSize = true;
            this._sslLabel48.Location = new System.Drawing.Point(349, 152);
            this._sslLabel48.Name = "_sslLabel48";
            this._sslLabel48.Size = new System.Drawing.Size(41, 13);
            this._sslLabel48.TabIndex = 62;
            this._sslLabel48.Text = "ССЛ48";
            // 
            // _sslLabel40
            // 
            this._sslLabel40.AutoSize = true;
            this._sslLabel40.Location = new System.Drawing.Point(283, 152);
            this._sslLabel40.Name = "_sslLabel40";
            this._sslLabel40.Size = new System.Drawing.Size(41, 13);
            this._sslLabel40.TabIndex = 62;
            this._sslLabel40.Text = "ССЛ40";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(217, 152);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(41, 13);
            this.label237.TabIndex = 62;
            this.label237.Text = "ССЛ32";
            // 
            // _ssl47
            // 
            this._ssl47.Location = new System.Drawing.Point(330, 133);
            this._ssl47.Name = "_ssl47";
            this._ssl47.Size = new System.Drawing.Size(13, 13);
            this._ssl47.State = BEMN.Forms.LedState.Off;
            this._ssl47.TabIndex = 61;
            // 
            // _ssl39
            // 
            this._ssl39.Location = new System.Drawing.Point(264, 133);
            this._ssl39.Name = "_ssl39";
            this._ssl39.Size = new System.Drawing.Size(13, 13);
            this._ssl39.State = BEMN.Forms.LedState.Off;
            this._ssl39.TabIndex = 61;
            // 
            // _ssl31
            // 
            this._ssl31.Location = new System.Drawing.Point(198, 133);
            this._ssl31.Name = "_ssl31";
            this._ssl31.Size = new System.Drawing.Size(13, 13);
            this._ssl31.State = BEMN.Forms.LedState.Off;
            this._ssl31.TabIndex = 61;
            // 
            // _sslLabel47
            // 
            this._sslLabel47.AutoSize = true;
            this._sslLabel47.Location = new System.Drawing.Point(349, 133);
            this._sslLabel47.Name = "_sslLabel47";
            this._sslLabel47.Size = new System.Drawing.Size(41, 13);
            this._sslLabel47.TabIndex = 60;
            this._sslLabel47.Text = "ССЛ47";
            // 
            // _sslLabel39
            // 
            this._sslLabel39.AutoSize = true;
            this._sslLabel39.Location = new System.Drawing.Point(283, 133);
            this._sslLabel39.Name = "_sslLabel39";
            this._sslLabel39.Size = new System.Drawing.Size(41, 13);
            this._sslLabel39.TabIndex = 60;
            this._sslLabel39.Text = "ССЛ39";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(217, 133);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(41, 13);
            this.label238.TabIndex = 60;
            this.label238.Text = "ССЛ31";
            // 
            // _ssl46
            // 
            this._ssl46.Location = new System.Drawing.Point(330, 114);
            this._ssl46.Name = "_ssl46";
            this._ssl46.Size = new System.Drawing.Size(13, 13);
            this._ssl46.State = BEMN.Forms.LedState.Off;
            this._ssl46.TabIndex = 59;
            // 
            // _ssl38
            // 
            this._ssl38.Location = new System.Drawing.Point(264, 114);
            this._ssl38.Name = "_ssl38";
            this._ssl38.Size = new System.Drawing.Size(13, 13);
            this._ssl38.State = BEMN.Forms.LedState.Off;
            this._ssl38.TabIndex = 59;
            // 
            // _ssl30
            // 
            this._ssl30.Location = new System.Drawing.Point(198, 114);
            this._ssl30.Name = "_ssl30";
            this._ssl30.Size = new System.Drawing.Size(13, 13);
            this._ssl30.State = BEMN.Forms.LedState.Off;
            this._ssl30.TabIndex = 59;
            // 
            // _sslLabel46
            // 
            this._sslLabel46.AutoSize = true;
            this._sslLabel46.Location = new System.Drawing.Point(349, 114);
            this._sslLabel46.Name = "_sslLabel46";
            this._sslLabel46.Size = new System.Drawing.Size(41, 13);
            this._sslLabel46.TabIndex = 58;
            this._sslLabel46.Text = "ССЛ46";
            // 
            // _sslLabel38
            // 
            this._sslLabel38.AutoSize = true;
            this._sslLabel38.Location = new System.Drawing.Point(283, 114);
            this._sslLabel38.Name = "_sslLabel38";
            this._sslLabel38.Size = new System.Drawing.Size(41, 13);
            this._sslLabel38.TabIndex = 58;
            this._sslLabel38.Text = "ССЛ38";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(217, 114);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(41, 13);
            this.label239.TabIndex = 58;
            this.label239.Text = "ССЛ30";
            // 
            // _ssl45
            // 
            this._ssl45.Location = new System.Drawing.Point(330, 95);
            this._ssl45.Name = "_ssl45";
            this._ssl45.Size = new System.Drawing.Size(13, 13);
            this._ssl45.State = BEMN.Forms.LedState.Off;
            this._ssl45.TabIndex = 57;
            // 
            // _ssl37
            // 
            this._ssl37.Location = new System.Drawing.Point(264, 95);
            this._ssl37.Name = "_ssl37";
            this._ssl37.Size = new System.Drawing.Size(13, 13);
            this._ssl37.State = BEMN.Forms.LedState.Off;
            this._ssl37.TabIndex = 57;
            // 
            // _ssl29
            // 
            this._ssl29.Location = new System.Drawing.Point(198, 95);
            this._ssl29.Name = "_ssl29";
            this._ssl29.Size = new System.Drawing.Size(13, 13);
            this._ssl29.State = BEMN.Forms.LedState.Off;
            this._ssl29.TabIndex = 57;
            // 
            // _sslLabel45
            // 
            this._sslLabel45.AutoSize = true;
            this._sslLabel45.Location = new System.Drawing.Point(349, 95);
            this._sslLabel45.Name = "_sslLabel45";
            this._sslLabel45.Size = new System.Drawing.Size(41, 13);
            this._sslLabel45.TabIndex = 56;
            this._sslLabel45.Text = "ССЛ45";
            // 
            // _sslLabel37
            // 
            this._sslLabel37.AutoSize = true;
            this._sslLabel37.Location = new System.Drawing.Point(283, 95);
            this._sslLabel37.Name = "_sslLabel37";
            this._sslLabel37.Size = new System.Drawing.Size(41, 13);
            this._sslLabel37.TabIndex = 56;
            this._sslLabel37.Text = "ССЛ37";
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Location = new System.Drawing.Point(217, 95);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(41, 13);
            this.label240.TabIndex = 56;
            this.label240.Text = "ССЛ29";
            // 
            // _ssl44
            // 
            this._ssl44.Location = new System.Drawing.Point(330, 76);
            this._ssl44.Name = "_ssl44";
            this._ssl44.Size = new System.Drawing.Size(13, 13);
            this._ssl44.State = BEMN.Forms.LedState.Off;
            this._ssl44.TabIndex = 55;
            // 
            // _ssl36
            // 
            this._ssl36.Location = new System.Drawing.Point(264, 76);
            this._ssl36.Name = "_ssl36";
            this._ssl36.Size = new System.Drawing.Size(13, 13);
            this._ssl36.State = BEMN.Forms.LedState.Off;
            this._ssl36.TabIndex = 55;
            // 
            // _ssl28
            // 
            this._ssl28.Location = new System.Drawing.Point(198, 76);
            this._ssl28.Name = "_ssl28";
            this._ssl28.Size = new System.Drawing.Size(13, 13);
            this._ssl28.State = BEMN.Forms.LedState.Off;
            this._ssl28.TabIndex = 55;
            // 
            // _sslLabel44
            // 
            this._sslLabel44.AutoSize = true;
            this._sslLabel44.Location = new System.Drawing.Point(349, 76);
            this._sslLabel44.Name = "_sslLabel44";
            this._sslLabel44.Size = new System.Drawing.Size(41, 13);
            this._sslLabel44.TabIndex = 54;
            this._sslLabel44.Text = "ССЛ44";
            // 
            // _sslLabel36
            // 
            this._sslLabel36.AutoSize = true;
            this._sslLabel36.Location = new System.Drawing.Point(283, 76);
            this._sslLabel36.Name = "_sslLabel36";
            this._sslLabel36.Size = new System.Drawing.Size(41, 13);
            this._sslLabel36.TabIndex = 54;
            this._sslLabel36.Text = "ССЛ36";
            // 
            // label241
            // 
            this.label241.AutoSize = true;
            this.label241.Location = new System.Drawing.Point(217, 76);
            this.label241.Name = "label241";
            this.label241.Size = new System.Drawing.Size(41, 13);
            this.label241.TabIndex = 54;
            this.label241.Text = "ССЛ28";
            // 
            // _ssl43
            // 
            this._ssl43.Location = new System.Drawing.Point(330, 57);
            this._ssl43.Name = "_ssl43";
            this._ssl43.Size = new System.Drawing.Size(13, 13);
            this._ssl43.State = BEMN.Forms.LedState.Off;
            this._ssl43.TabIndex = 53;
            // 
            // _ssl35
            // 
            this._ssl35.Location = new System.Drawing.Point(264, 57);
            this._ssl35.Name = "_ssl35";
            this._ssl35.Size = new System.Drawing.Size(13, 13);
            this._ssl35.State = BEMN.Forms.LedState.Off;
            this._ssl35.TabIndex = 53;
            // 
            // _ssl27
            // 
            this._ssl27.Location = new System.Drawing.Point(198, 57);
            this._ssl27.Name = "_ssl27";
            this._ssl27.Size = new System.Drawing.Size(13, 13);
            this._ssl27.State = BEMN.Forms.LedState.Off;
            this._ssl27.TabIndex = 53;
            // 
            // _sslLabel43
            // 
            this._sslLabel43.AutoSize = true;
            this._sslLabel43.Location = new System.Drawing.Point(349, 57);
            this._sslLabel43.Name = "_sslLabel43";
            this._sslLabel43.Size = new System.Drawing.Size(41, 13);
            this._sslLabel43.TabIndex = 52;
            this._sslLabel43.Text = "ССЛ43";
            // 
            // _sslLabel35
            // 
            this._sslLabel35.AutoSize = true;
            this._sslLabel35.Location = new System.Drawing.Point(283, 57);
            this._sslLabel35.Name = "_sslLabel35";
            this._sslLabel35.Size = new System.Drawing.Size(41, 13);
            this._sslLabel35.TabIndex = 52;
            this._sslLabel35.Text = "ССЛ35";
            // 
            // label242
            // 
            this.label242.AutoSize = true;
            this.label242.Location = new System.Drawing.Point(217, 57);
            this.label242.Name = "label242";
            this.label242.Size = new System.Drawing.Size(41, 13);
            this.label242.TabIndex = 52;
            this.label242.Text = "ССЛ27";
            // 
            // _ssl42
            // 
            this._ssl42.Location = new System.Drawing.Point(330, 38);
            this._ssl42.Name = "_ssl42";
            this._ssl42.Size = new System.Drawing.Size(13, 13);
            this._ssl42.State = BEMN.Forms.LedState.Off;
            this._ssl42.TabIndex = 51;
            // 
            // _ssl34
            // 
            this._ssl34.Location = new System.Drawing.Point(264, 38);
            this._ssl34.Name = "_ssl34";
            this._ssl34.Size = new System.Drawing.Size(13, 13);
            this._ssl34.State = BEMN.Forms.LedState.Off;
            this._ssl34.TabIndex = 51;
            // 
            // _ssl26
            // 
            this._ssl26.Location = new System.Drawing.Point(198, 38);
            this._ssl26.Name = "_ssl26";
            this._ssl26.Size = new System.Drawing.Size(13, 13);
            this._ssl26.State = BEMN.Forms.LedState.Off;
            this._ssl26.TabIndex = 51;
            // 
            // _sslLabel42
            // 
            this._sslLabel42.AutoSize = true;
            this._sslLabel42.Location = new System.Drawing.Point(349, 38);
            this._sslLabel42.Name = "_sslLabel42";
            this._sslLabel42.Size = new System.Drawing.Size(41, 13);
            this._sslLabel42.TabIndex = 50;
            this._sslLabel42.Text = "ССЛ42";
            // 
            // _sslLabel34
            // 
            this._sslLabel34.AutoSize = true;
            this._sslLabel34.Location = new System.Drawing.Point(283, 38);
            this._sslLabel34.Name = "_sslLabel34";
            this._sslLabel34.Size = new System.Drawing.Size(41, 13);
            this._sslLabel34.TabIndex = 50;
            this._sslLabel34.Text = "ССЛ34";
            // 
            // label243
            // 
            this.label243.AutoSize = true;
            this.label243.Location = new System.Drawing.Point(217, 38);
            this.label243.Name = "label243";
            this.label243.Size = new System.Drawing.Size(41, 13);
            this.label243.TabIndex = 50;
            this.label243.Text = "ССЛ26";
            // 
            // _ssl41
            // 
            this._ssl41.Location = new System.Drawing.Point(330, 19);
            this._ssl41.Name = "_ssl41";
            this._ssl41.Size = new System.Drawing.Size(13, 13);
            this._ssl41.State = BEMN.Forms.LedState.Off;
            this._ssl41.TabIndex = 49;
            // 
            // _sslLabel41
            // 
            this._sslLabel41.AutoSize = true;
            this._sslLabel41.Location = new System.Drawing.Point(349, 19);
            this._sslLabel41.Name = "_sslLabel41";
            this._sslLabel41.Size = new System.Drawing.Size(41, 13);
            this._sslLabel41.TabIndex = 48;
            this._sslLabel41.Text = "ССЛ41";
            // 
            // _ssl33
            // 
            this._ssl33.Location = new System.Drawing.Point(264, 19);
            this._ssl33.Name = "_ssl33";
            this._ssl33.Size = new System.Drawing.Size(13, 13);
            this._ssl33.State = BEMN.Forms.LedState.Off;
            this._ssl33.TabIndex = 49;
            // 
            // _sslLabel33
            // 
            this._sslLabel33.AutoSize = true;
            this._sslLabel33.Location = new System.Drawing.Point(283, 19);
            this._sslLabel33.Name = "_sslLabel33";
            this._sslLabel33.Size = new System.Drawing.Size(41, 13);
            this._sslLabel33.TabIndex = 48;
            this._sslLabel33.Text = "ССЛ33";
            // 
            // _ssl25
            // 
            this._ssl25.Location = new System.Drawing.Point(198, 19);
            this._ssl25.Name = "_ssl25";
            this._ssl25.Size = new System.Drawing.Size(13, 13);
            this._ssl25.State = BEMN.Forms.LedState.Off;
            this._ssl25.TabIndex = 49;
            // 
            // label244
            // 
            this.label244.AutoSize = true;
            this.label244.Location = new System.Drawing.Point(217, 19);
            this.label244.Name = "label244";
            this.label244.Size = new System.Drawing.Size(41, 13);
            this.label244.TabIndex = 48;
            this.label244.Text = "ССЛ25";
            // 
            // _ssl24
            // 
            this._ssl24.Location = new System.Drawing.Point(132, 152);
            this._ssl24.Name = "_ssl24";
            this._ssl24.Size = new System.Drawing.Size(13, 13);
            this._ssl24.State = BEMN.Forms.LedState.Off;
            this._ssl24.TabIndex = 47;
            // 
            // label245
            // 
            this.label245.AutoSize = true;
            this.label245.Location = new System.Drawing.Point(151, 152);
            this.label245.Name = "label245";
            this.label245.Size = new System.Drawing.Size(41, 13);
            this.label245.TabIndex = 46;
            this.label245.Text = "ССЛ24";
            // 
            // _ssl23
            // 
            this._ssl23.Location = new System.Drawing.Point(132, 133);
            this._ssl23.Name = "_ssl23";
            this._ssl23.Size = new System.Drawing.Size(13, 13);
            this._ssl23.State = BEMN.Forms.LedState.Off;
            this._ssl23.TabIndex = 45;
            // 
            // label246
            // 
            this.label246.AutoSize = true;
            this.label246.Location = new System.Drawing.Point(151, 133);
            this.label246.Name = "label246";
            this.label246.Size = new System.Drawing.Size(41, 13);
            this.label246.TabIndex = 44;
            this.label246.Text = "ССЛ23";
            // 
            // _ssl22
            // 
            this._ssl22.Location = new System.Drawing.Point(132, 114);
            this._ssl22.Name = "_ssl22";
            this._ssl22.Size = new System.Drawing.Size(13, 13);
            this._ssl22.State = BEMN.Forms.LedState.Off;
            this._ssl22.TabIndex = 43;
            // 
            // label247
            // 
            this.label247.AutoSize = true;
            this.label247.Location = new System.Drawing.Point(151, 114);
            this.label247.Name = "label247";
            this.label247.Size = new System.Drawing.Size(41, 13);
            this.label247.TabIndex = 42;
            this.label247.Text = "ССЛ22";
            // 
            // _ssl21
            // 
            this._ssl21.Location = new System.Drawing.Point(132, 95);
            this._ssl21.Name = "_ssl21";
            this._ssl21.Size = new System.Drawing.Size(13, 13);
            this._ssl21.State = BEMN.Forms.LedState.Off;
            this._ssl21.TabIndex = 41;
            // 
            // label248
            // 
            this.label248.AutoSize = true;
            this.label248.Location = new System.Drawing.Point(151, 95);
            this.label248.Name = "label248";
            this.label248.Size = new System.Drawing.Size(41, 13);
            this.label248.TabIndex = 40;
            this.label248.Text = "ССЛ21";
            // 
            // _ssl20
            // 
            this._ssl20.Location = new System.Drawing.Point(132, 76);
            this._ssl20.Name = "_ssl20";
            this._ssl20.Size = new System.Drawing.Size(13, 13);
            this._ssl20.State = BEMN.Forms.LedState.Off;
            this._ssl20.TabIndex = 39;
            // 
            // label249
            // 
            this.label249.AutoSize = true;
            this.label249.Location = new System.Drawing.Point(152, 76);
            this.label249.Name = "label249";
            this.label249.Size = new System.Drawing.Size(41, 13);
            this.label249.TabIndex = 38;
            this.label249.Text = "ССЛ20";
            // 
            // _ssl19
            // 
            this._ssl19.Location = new System.Drawing.Point(132, 57);
            this._ssl19.Name = "_ssl19";
            this._ssl19.Size = new System.Drawing.Size(13, 13);
            this._ssl19.State = BEMN.Forms.LedState.Off;
            this._ssl19.TabIndex = 37;
            // 
            // label250
            // 
            this.label250.AutoSize = true;
            this.label250.Location = new System.Drawing.Point(151, 57);
            this.label250.Name = "label250";
            this.label250.Size = new System.Drawing.Size(41, 13);
            this.label250.TabIndex = 36;
            this.label250.Text = "ССЛ19";
            // 
            // _ssl18
            // 
            this._ssl18.Location = new System.Drawing.Point(132, 38);
            this._ssl18.Name = "_ssl18";
            this._ssl18.Size = new System.Drawing.Size(13, 13);
            this._ssl18.State = BEMN.Forms.LedState.Off;
            this._ssl18.TabIndex = 35;
            // 
            // label251
            // 
            this.label251.AutoSize = true;
            this.label251.Location = new System.Drawing.Point(151, 38);
            this.label251.Name = "label251";
            this.label251.Size = new System.Drawing.Size(41, 13);
            this.label251.TabIndex = 34;
            this.label251.Text = "ССЛ18";
            // 
            // _ssl17
            // 
            this._ssl17.Location = new System.Drawing.Point(132, 19);
            this._ssl17.Name = "_ssl17";
            this._ssl17.Size = new System.Drawing.Size(13, 13);
            this._ssl17.State = BEMN.Forms.LedState.Off;
            this._ssl17.TabIndex = 33;
            // 
            // label252
            // 
            this.label252.AutoSize = true;
            this.label252.Location = new System.Drawing.Point(151, 19);
            this.label252.Name = "label252";
            this.label252.Size = new System.Drawing.Size(41, 13);
            this.label252.TabIndex = 32;
            this.label252.Text = "ССЛ17";
            // 
            // _ssl16
            // 
            this._ssl16.Location = new System.Drawing.Point(66, 152);
            this._ssl16.Name = "_ssl16";
            this._ssl16.Size = new System.Drawing.Size(13, 13);
            this._ssl16.State = BEMN.Forms.LedState.Off;
            this._ssl16.TabIndex = 31;
            // 
            // label253
            // 
            this.label253.AutoSize = true;
            this.label253.Location = new System.Drawing.Point(85, 152);
            this.label253.Name = "label253";
            this.label253.Size = new System.Drawing.Size(41, 13);
            this.label253.TabIndex = 30;
            this.label253.Text = "ССЛ16";
            // 
            // _ssl15
            // 
            this._ssl15.Location = new System.Drawing.Point(66, 133);
            this._ssl15.Name = "_ssl15";
            this._ssl15.Size = new System.Drawing.Size(13, 13);
            this._ssl15.State = BEMN.Forms.LedState.Off;
            this._ssl15.TabIndex = 29;
            // 
            // label254
            // 
            this.label254.AutoSize = true;
            this.label254.Location = new System.Drawing.Point(85, 133);
            this.label254.Name = "label254";
            this.label254.Size = new System.Drawing.Size(41, 13);
            this.label254.TabIndex = 28;
            this.label254.Text = "ССЛ15";
            // 
            // _ssl14
            // 
            this._ssl14.Location = new System.Drawing.Point(66, 114);
            this._ssl14.Name = "_ssl14";
            this._ssl14.Size = new System.Drawing.Size(13, 13);
            this._ssl14.State = BEMN.Forms.LedState.Off;
            this._ssl14.TabIndex = 27;
            // 
            // label255
            // 
            this.label255.AutoSize = true;
            this.label255.Location = new System.Drawing.Point(85, 114);
            this.label255.Name = "label255";
            this.label255.Size = new System.Drawing.Size(41, 13);
            this.label255.TabIndex = 26;
            this.label255.Text = "ССЛ14";
            // 
            // _ssl13
            // 
            this._ssl13.Location = new System.Drawing.Point(66, 95);
            this._ssl13.Name = "_ssl13";
            this._ssl13.Size = new System.Drawing.Size(13, 13);
            this._ssl13.State = BEMN.Forms.LedState.Off;
            this._ssl13.TabIndex = 25;
            // 
            // label256
            // 
            this.label256.AutoSize = true;
            this.label256.Location = new System.Drawing.Point(85, 95);
            this.label256.Name = "label256";
            this.label256.Size = new System.Drawing.Size(41, 13);
            this.label256.TabIndex = 24;
            this.label256.Text = "ССЛ13";
            // 
            // _ssl12
            // 
            this._ssl12.Location = new System.Drawing.Point(66, 76);
            this._ssl12.Name = "_ssl12";
            this._ssl12.Size = new System.Drawing.Size(13, 13);
            this._ssl12.State = BEMN.Forms.LedState.Off;
            this._ssl12.TabIndex = 23;
            // 
            // label257
            // 
            this.label257.AutoSize = true;
            this.label257.Location = new System.Drawing.Point(85, 76);
            this.label257.Name = "label257";
            this.label257.Size = new System.Drawing.Size(41, 13);
            this.label257.TabIndex = 22;
            this.label257.Text = "ССЛ12";
            // 
            // _ssl11
            // 
            this._ssl11.Location = new System.Drawing.Point(66, 57);
            this._ssl11.Name = "_ssl11";
            this._ssl11.Size = new System.Drawing.Size(13, 13);
            this._ssl11.State = BEMN.Forms.LedState.Off;
            this._ssl11.TabIndex = 21;
            // 
            // label258
            // 
            this.label258.AutoSize = true;
            this.label258.Location = new System.Drawing.Point(85, 57);
            this.label258.Name = "label258";
            this.label258.Size = new System.Drawing.Size(41, 13);
            this.label258.TabIndex = 20;
            this.label258.Text = "ССЛ11";
            // 
            // _ssl10
            // 
            this._ssl10.Location = new System.Drawing.Point(66, 38);
            this._ssl10.Name = "_ssl10";
            this._ssl10.Size = new System.Drawing.Size(13, 13);
            this._ssl10.State = BEMN.Forms.LedState.Off;
            this._ssl10.TabIndex = 19;
            // 
            // label259
            // 
            this.label259.AutoSize = true;
            this.label259.Location = new System.Drawing.Point(85, 38);
            this.label259.Name = "label259";
            this.label259.Size = new System.Drawing.Size(41, 13);
            this.label259.TabIndex = 18;
            this.label259.Text = "ССЛ10";
            // 
            // _ssl9
            // 
            this._ssl9.Location = new System.Drawing.Point(66, 19);
            this._ssl9.Name = "_ssl9";
            this._ssl9.Size = new System.Drawing.Size(13, 13);
            this._ssl9.State = BEMN.Forms.LedState.Off;
            this._ssl9.TabIndex = 17;
            // 
            // label260
            // 
            this.label260.AutoSize = true;
            this.label260.Location = new System.Drawing.Point(85, 19);
            this.label260.Name = "label260";
            this.label260.Size = new System.Drawing.Size(35, 13);
            this.label260.TabIndex = 16;
            this.label260.Text = "ССЛ9";
            // 
            // _ssl8
            // 
            this._ssl8.Location = new System.Drawing.Point(6, 152);
            this._ssl8.Name = "_ssl8";
            this._ssl8.Size = new System.Drawing.Size(13, 13);
            this._ssl8.State = BEMN.Forms.LedState.Off;
            this._ssl8.TabIndex = 15;
            // 
            // label261
            // 
            this.label261.AutoSize = true;
            this.label261.Location = new System.Drawing.Point(25, 152);
            this.label261.Name = "label261";
            this.label261.Size = new System.Drawing.Size(35, 13);
            this.label261.TabIndex = 14;
            this.label261.Text = "ССЛ8";
            // 
            // _ssl7
            // 
            this._ssl7.Location = new System.Drawing.Point(6, 133);
            this._ssl7.Name = "_ssl7";
            this._ssl7.Size = new System.Drawing.Size(13, 13);
            this._ssl7.State = BEMN.Forms.LedState.Off;
            this._ssl7.TabIndex = 13;
            // 
            // label262
            // 
            this.label262.AutoSize = true;
            this.label262.Location = new System.Drawing.Point(25, 133);
            this.label262.Name = "label262";
            this.label262.Size = new System.Drawing.Size(35, 13);
            this.label262.TabIndex = 12;
            this.label262.Text = "ССЛ7";
            // 
            // _ssl6
            // 
            this._ssl6.Location = new System.Drawing.Point(6, 114);
            this._ssl6.Name = "_ssl6";
            this._ssl6.Size = new System.Drawing.Size(13, 13);
            this._ssl6.State = BEMN.Forms.LedState.Off;
            this._ssl6.TabIndex = 11;
            // 
            // label263
            // 
            this.label263.AutoSize = true;
            this.label263.Location = new System.Drawing.Point(25, 114);
            this.label263.Name = "label263";
            this.label263.Size = new System.Drawing.Size(35, 13);
            this.label263.TabIndex = 10;
            this.label263.Text = "ССЛ6";
            // 
            // _ssl5
            // 
            this._ssl5.Location = new System.Drawing.Point(6, 95);
            this._ssl5.Name = "_ssl5";
            this._ssl5.Size = new System.Drawing.Size(13, 13);
            this._ssl5.State = BEMN.Forms.LedState.Off;
            this._ssl5.TabIndex = 9;
            // 
            // label264
            // 
            this.label264.AutoSize = true;
            this.label264.Location = new System.Drawing.Point(25, 95);
            this.label264.Name = "label264";
            this.label264.Size = new System.Drawing.Size(35, 13);
            this.label264.TabIndex = 8;
            this.label264.Text = "ССЛ5";
            // 
            // _ssl4
            // 
            this._ssl4.Location = new System.Drawing.Point(6, 76);
            this._ssl4.Name = "_ssl4";
            this._ssl4.Size = new System.Drawing.Size(13, 13);
            this._ssl4.State = BEMN.Forms.LedState.Off;
            this._ssl4.TabIndex = 7;
            // 
            // label265
            // 
            this.label265.AutoSize = true;
            this.label265.Location = new System.Drawing.Point(26, 76);
            this.label265.Name = "label265";
            this.label265.Size = new System.Drawing.Size(35, 13);
            this.label265.TabIndex = 6;
            this.label265.Text = "ССЛ4";
            // 
            // _ssl3
            // 
            this._ssl3.Location = new System.Drawing.Point(6, 57);
            this._ssl3.Name = "_ssl3";
            this._ssl3.Size = new System.Drawing.Size(13, 13);
            this._ssl3.State = BEMN.Forms.LedState.Off;
            this._ssl3.TabIndex = 5;
            // 
            // label266
            // 
            this.label266.AutoSize = true;
            this.label266.Location = new System.Drawing.Point(25, 57);
            this.label266.Name = "label266";
            this.label266.Size = new System.Drawing.Size(35, 13);
            this.label266.TabIndex = 4;
            this.label266.Text = "ССЛ3";
            // 
            // _ssl2
            // 
            this._ssl2.Location = new System.Drawing.Point(6, 38);
            this._ssl2.Name = "_ssl2";
            this._ssl2.Size = new System.Drawing.Size(13, 13);
            this._ssl2.State = BEMN.Forms.LedState.Off;
            this._ssl2.TabIndex = 3;
            // 
            // label267
            // 
            this.label267.AutoSize = true;
            this.label267.Location = new System.Drawing.Point(25, 38);
            this.label267.Name = "label267";
            this.label267.Size = new System.Drawing.Size(35, 13);
            this.label267.TabIndex = 2;
            this.label267.Text = "ССЛ2";
            // 
            // _ssl1
            // 
            this._ssl1.Location = new System.Drawing.Point(6, 19);
            this._ssl1.Name = "_ssl1";
            this._ssl1.Size = new System.Drawing.Size(13, 13);
            this._ssl1.State = BEMN.Forms.LedState.Off;
            this._ssl1.TabIndex = 1;
            // 
            // label268
            // 
            this.label268.AutoSize = true;
            this.label268.Location = new System.Drawing.Point(25, 19);
            this.label268.Name = "label268";
            this.label268.Size = new System.Drawing.Size(35, 13);
            this.label268.TabIndex = 0;
            this.label268.Text = "ССЛ1";
            // 
            // dugGroup
            // 
            this.dugGroup.Controls.Add(this.dugPusk);
            this.dugGroup.Controls.Add(this.label446);
            this.dugGroup.Location = new System.Drawing.Point(98, 177);
            this.dugGroup.Name = "dugGroup";
            this.dugGroup.Size = new System.Drawing.Size(84, 45);
            this.dugGroup.TabIndex = 24;
            this.dugGroup.TabStop = false;
            // 
            // dugPusk
            // 
            this.dugPusk.Location = new System.Drawing.Point(6, 18);
            this.dugPusk.Name = "dugPusk";
            this.dugPusk.Size = new System.Drawing.Size(13, 13);
            this.dugPusk.State = BEMN.Forms.LedState.Off;
            this.dugPusk.TabIndex = 37;
            // 
            // label446
            // 
            this.label446.AutoSize = true;
            this.label446.Location = new System.Drawing.Point(25, 10);
            this.label446.Name = "label446";
            this.label446.Size = new System.Drawing.Size(57, 26);
            this.label446.TabIndex = 36;
            this.label446.Text = "Пуск дуг. \r\nзащиты";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label413);
            this.groupBox26.Controls.Add(this._iS8);
            this.groupBox26.Controls.Add(this._iS8Io);
            this.groupBox26.Controls.Add(this.label88);
            this.groupBox26.Controls.Add(this._iS7);
            this.groupBox26.Controls.Add(this._iS7Io);
            this.groupBox26.Controls.Add(this.label229);
            this.groupBox26.Controls.Add(this.label86);
            this.groupBox26.Controls.Add(this._iS1);
            this.groupBox26.Controls.Add(this.label85);
            this.groupBox26.Controls.Add(this._iS2);
            this.groupBox26.Controls.Add(this._iS1Io);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Controls.Add(this._iS3);
            this.groupBox26.Controls.Add(this._iS2Io);
            this.groupBox26.Controls.Add(this.label230);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this._iS4);
            this.groupBox26.Controls.Add(this._iS3Io);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this._iS5);
            this.groupBox26.Controls.Add(this._iS4Io);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._iS6);
            this.groupBox26.Controls.Add(this._iS5Io);
            this.groupBox26.Controls.Add(this._iS6Io);
            this.groupBox26.Location = new System.Drawing.Point(98, 244);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(84, 189);
            this.groupBox26.TabIndex = 17;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Защиты I*";
            // 
            // label413
            // 
            this.label413.AutoSize = true;
            this.label413.Location = new System.Drawing.Point(47, 167);
            this.label413.Name = "label413";
            this.label413.Size = new System.Drawing.Size(26, 13);
            this.label413.TabIndex = 71;
            this.label413.Text = "I*>8";
            // 
            // _iS8
            // 
            this._iS8.Location = new System.Drawing.Point(28, 167);
            this._iS8.Name = "_iS8";
            this._iS8.Size = new System.Drawing.Size(13, 13);
            this._iS8.State = BEMN.Forms.LedState.Off;
            this._iS8.TabIndex = 72;
            // 
            // _iS8Io
            // 
            this._iS8Io.Location = new System.Drawing.Point(9, 167);
            this._iS8Io.Name = "_iS8Io";
            this._iS8Io.Size = new System.Drawing.Size(13, 13);
            this._iS8Io.State = BEMN.Forms.LedState.Off;
            this._iS8Io.TabIndex = 73;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(47, 149);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(26, 13);
            this.label88.TabIndex = 68;
            this.label88.Text = "I*>7";
            // 
            // _iS7
            // 
            this._iS7.Location = new System.Drawing.Point(28, 149);
            this._iS7.Name = "_iS7";
            this._iS7.Size = new System.Drawing.Size(13, 13);
            this._iS7.State = BEMN.Forms.LedState.Off;
            this._iS7.TabIndex = 69;
            // 
            // _iS7Io
            // 
            this._iS7Io.Location = new System.Drawing.Point(9, 149);
            this._iS7Io.Name = "_iS7Io";
            this._iS7Io.Size = new System.Drawing.Size(13, 13);
            this._iS7Io.State = BEMN.Forms.LedState.Off;
            this._iS7Io.TabIndex = 70;
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.ForeColor = System.Drawing.Color.Blue;
            this.label229.Location = new System.Drawing.Point(25, 16);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(32, 13);
            this.label229.TabIndex = 67;
            this.label229.Text = "Сраб";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(47, 35);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(26, 13);
            this.label86.TabIndex = 16;
            this.label86.Text = "I*>1";
            // 
            // _iS1
            // 
            this._iS1.Location = new System.Drawing.Point(28, 35);
            this._iS1.Name = "_iS1";
            this._iS1.Size = new System.Drawing.Size(13, 13);
            this._iS1.State = BEMN.Forms.LedState.Off;
            this._iS1.TabIndex = 17;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(47, 54);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(26, 13);
            this.label85.TabIndex = 18;
            this.label85.Text = "I*>2";
            // 
            // _iS2
            // 
            this._iS2.Location = new System.Drawing.Point(28, 54);
            this._iS2.Name = "_iS2";
            this._iS2.Size = new System.Drawing.Size(13, 13);
            this._iS2.State = BEMN.Forms.LedState.Off;
            this._iS2.TabIndex = 19;
            // 
            // _iS1Io
            // 
            this._iS1Io.Location = new System.Drawing.Point(9, 35);
            this._iS1Io.Name = "_iS1Io";
            this._iS1Io.Size = new System.Drawing.Size(13, 13);
            this._iS1Io.State = BEMN.Forms.LedState.Off;
            this._iS1Io.TabIndex = 17;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(47, 73);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(26, 13);
            this.label84.TabIndex = 20;
            this.label84.Text = "I*>3";
            // 
            // _iS3
            // 
            this._iS3.Location = new System.Drawing.Point(28, 73);
            this._iS3.Name = "_iS3";
            this._iS3.Size = new System.Drawing.Size(13, 13);
            this._iS3.State = BEMN.Forms.LedState.Off;
            this._iS3.TabIndex = 21;
            // 
            // _iS2Io
            // 
            this._iS2Io.Location = new System.Drawing.Point(9, 54);
            this._iS2Io.Name = "_iS2Io";
            this._iS2Io.Size = new System.Drawing.Size(13, 13);
            this._iS2Io.State = BEMN.Forms.LedState.Off;
            this._iS2Io.TabIndex = 19;
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.ForeColor = System.Drawing.Color.Red;
            this.label230.Location = new System.Drawing.Point(6, 16);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(23, 13);
            this.label230.TabIndex = 66;
            this.label230.Text = "ИО";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(47, 92);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(26, 13);
            this.label83.TabIndex = 22;
            this.label83.Text = "I*>4";
            // 
            // _iS4
            // 
            this._iS4.Location = new System.Drawing.Point(28, 92);
            this._iS4.Name = "_iS4";
            this._iS4.Size = new System.Drawing.Size(13, 13);
            this._iS4.State = BEMN.Forms.LedState.Off;
            this._iS4.TabIndex = 23;
            // 
            // _iS3Io
            // 
            this._iS3Io.Location = new System.Drawing.Point(9, 73);
            this._iS3Io.Name = "_iS3Io";
            this._iS3Io.Size = new System.Drawing.Size(13, 13);
            this._iS3Io.State = BEMN.Forms.LedState.Off;
            this._iS3Io.TabIndex = 21;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(47, 111);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(26, 13);
            this.label82.TabIndex = 24;
            this.label82.Text = "I*>5";
            // 
            // _iS5
            // 
            this._iS5.Location = new System.Drawing.Point(28, 111);
            this._iS5.Name = "_iS5";
            this._iS5.Size = new System.Drawing.Size(13, 13);
            this._iS5.State = BEMN.Forms.LedState.Off;
            this._iS5.TabIndex = 25;
            // 
            // _iS4Io
            // 
            this._iS4Io.Location = new System.Drawing.Point(9, 92);
            this._iS4Io.Name = "_iS4Io";
            this._iS4Io.Size = new System.Drawing.Size(13, 13);
            this._iS4Io.State = BEMN.Forms.LedState.Off;
            this._iS4Io.TabIndex = 23;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(47, 130);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(26, 13);
            this.label81.TabIndex = 26;
            this.label81.Text = "I*>6";
            // 
            // _iS6
            // 
            this._iS6.Location = new System.Drawing.Point(28, 130);
            this._iS6.Name = "_iS6";
            this._iS6.Size = new System.Drawing.Size(13, 13);
            this._iS6.State = BEMN.Forms.LedState.Off;
            this._iS6.TabIndex = 27;
            // 
            // _iS5Io
            // 
            this._iS5Io.Location = new System.Drawing.Point(9, 111);
            this._iS5Io.Name = "_iS5Io";
            this._iS5Io.Size = new System.Drawing.Size(13, 13);
            this._iS5Io.State = BEMN.Forms.LedState.Off;
            this._iS5Io.TabIndex = 25;
            // 
            // _iS6Io
            // 
            this._iS6Io.Location = new System.Drawing.Point(9, 130);
            this._iS6Io.Name = "_iS6Io";
            this._iS6Io.Size = new System.Drawing.Size(13, 13);
            this._iS6Io.State = BEMN.Forms.LedState.Off;
            this._iS6Io.TabIndex = 27;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label270);
            this.groupBox11.Controls.Add(this.label269);
            this.groupBox11.Controls.Add(this._i8Io);
            this.groupBox11.Controls.Add(this._i8);
            this.groupBox11.Controls.Add(this.label87);
            this.groupBox11.Controls.Add(this._i6Io);
            this.groupBox11.Controls.Add(this._i5Io);
            this.groupBox11.Controls.Add(this._i6);
            this.groupBox11.Controls.Add(this.label89);
            this.groupBox11.Controls.Add(this._i4Io);
            this.groupBox11.Controls.Add(this._i5);
            this.groupBox11.Controls.Add(this.label90);
            this.groupBox11.Controls.Add(this._i3Io);
            this.groupBox11.Controls.Add(this._i4);
            this.groupBox11.Controls.Add(this.label91);
            this.groupBox11.Controls.Add(this._i2Io);
            this.groupBox11.Controls.Add(this._i3);
            this.groupBox11.Controls.Add(this.label92);
            this.groupBox11.Controls.Add(this._i1Io);
            this.groupBox11.Controls.Add(this._i2);
            this.groupBox11.Controls.Add(this.label93);
            this.groupBox11.Controls.Add(this._i1);
            this.groupBox11.Controls.Add(this.label94);
            this.groupBox11.Location = new System.Drawing.Point(8, 282);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(84, 151);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Защиты I";
            // 
            // label270
            // 
            this.label270.AutoSize = true;
            this.label270.ForeColor = System.Drawing.Color.Blue;
            this.label270.Location = new System.Drawing.Point(25, 16);
            this.label270.Name = "label270";
            this.label270.Size = new System.Drawing.Size(32, 13);
            this.label270.TabIndex = 65;
            this.label270.Text = "Сраб";
            // 
            // label269
            // 
            this.label269.AutoSize = true;
            this.label269.ForeColor = System.Drawing.Color.Red;
            this.label269.Location = new System.Drawing.Point(6, 16);
            this.label269.Name = "label269";
            this.label269.Size = new System.Drawing.Size(23, 13);
            this.label269.TabIndex = 64;
            this.label269.Text = "ИО";
            // 
            // _i8Io
            // 
            this._i8Io.Location = new System.Drawing.Point(9, 131);
            this._i8Io.Name = "_i8Io";
            this._i8Io.Size = new System.Drawing.Size(13, 13);
            this._i8Io.State = BEMN.Forms.LedState.Off;
            this._i8Io.TabIndex = 15;
            // 
            // _i8
            // 
            this._i8.Location = new System.Drawing.Point(28, 131);
            this._i8.Name = "_i8";
            this._i8.Size = new System.Drawing.Size(13, 13);
            this._i8.State = BEMN.Forms.LedState.Off;
            this._i8.TabIndex = 15;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(47, 131);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(16, 13);
            this.label87.TabIndex = 14;
            this.label87.Text = "I<";
            // 
            // _i6Io
            // 
            this._i6Io.Location = new System.Drawing.Point(9, 115);
            this._i6Io.Name = "_i6Io";
            this._i6Io.Size = new System.Drawing.Size(13, 13);
            this._i6Io.State = BEMN.Forms.LedState.Off;
            this._i6Io.TabIndex = 11;
            // 
            // _i5Io
            // 
            this._i5Io.Location = new System.Drawing.Point(9, 99);
            this._i5Io.Name = "_i5Io";
            this._i5Io.Size = new System.Drawing.Size(13, 13);
            this._i5Io.State = BEMN.Forms.LedState.Off;
            this._i5Io.TabIndex = 9;
            // 
            // _i6
            // 
            this._i6.Location = new System.Drawing.Point(28, 115);
            this._i6.Name = "_i6";
            this._i6.Size = new System.Drawing.Size(13, 13);
            this._i6.State = BEMN.Forms.LedState.Off;
            this._i6.TabIndex = 11;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(47, 115);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(22, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "I>6";
            // 
            // _i4Io
            // 
            this._i4Io.Location = new System.Drawing.Point(9, 83);
            this._i4Io.Name = "_i4Io";
            this._i4Io.Size = new System.Drawing.Size(13, 13);
            this._i4Io.State = BEMN.Forms.LedState.Off;
            this._i4Io.TabIndex = 7;
            // 
            // _i5
            // 
            this._i5.Location = new System.Drawing.Point(28, 99);
            this._i5.Name = "_i5";
            this._i5.Size = new System.Drawing.Size(13, 13);
            this._i5.State = BEMN.Forms.LedState.Off;
            this._i5.TabIndex = 9;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(47, 99);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(22, 13);
            this.label90.TabIndex = 8;
            this.label90.Text = "I>5";
            // 
            // _i3Io
            // 
            this._i3Io.Location = new System.Drawing.Point(9, 67);
            this._i3Io.Name = "_i3Io";
            this._i3Io.Size = new System.Drawing.Size(13, 13);
            this._i3Io.State = BEMN.Forms.LedState.Off;
            this._i3Io.TabIndex = 5;
            // 
            // _i4
            // 
            this._i4.Location = new System.Drawing.Point(28, 83);
            this._i4.Name = "_i4";
            this._i4.Size = new System.Drawing.Size(13, 13);
            this._i4.State = BEMN.Forms.LedState.Off;
            this._i4.TabIndex = 7;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(48, 83);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(22, 13);
            this.label91.TabIndex = 6;
            this.label91.Text = "I>4";
            // 
            // _i2Io
            // 
            this._i2Io.Location = new System.Drawing.Point(9, 51);
            this._i2Io.Name = "_i2Io";
            this._i2Io.Size = new System.Drawing.Size(13, 13);
            this._i2Io.State = BEMN.Forms.LedState.Off;
            this._i2Io.TabIndex = 3;
            // 
            // _i3
            // 
            this._i3.Location = new System.Drawing.Point(28, 67);
            this._i3.Name = "_i3";
            this._i3.Size = new System.Drawing.Size(13, 13);
            this._i3.State = BEMN.Forms.LedState.Off;
            this._i3.TabIndex = 5;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(47, 67);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(22, 13);
            this.label92.TabIndex = 4;
            this.label92.Text = "I>3";
            // 
            // _i1Io
            // 
            this._i1Io.Location = new System.Drawing.Point(9, 35);
            this._i1Io.Name = "_i1Io";
            this._i1Io.Size = new System.Drawing.Size(13, 13);
            this._i1Io.State = BEMN.Forms.LedState.Off;
            this._i1Io.TabIndex = 1;
            // 
            // _i2
            // 
            this._i2.Location = new System.Drawing.Point(28, 51);
            this._i2.Name = "_i2";
            this._i2.Size = new System.Drawing.Size(13, 13);
            this._i2.State = BEMN.Forms.LedState.Off;
            this._i2.TabIndex = 3;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(47, 51);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(22, 13);
            this.label93.TabIndex = 2;
            this.label93.Text = "I>2";
            // 
            // _i1
            // 
            this._i1.Location = new System.Drawing.Point(28, 35);
            this._i1.Name = "_i1";
            this._i1.Size = new System.Drawing.Size(13, 13);
            this._i1.State = BEMN.Forms.LedState.Off;
            this._i1.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(47, 35);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(22, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "I>1";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this._vls16);
            this.groupBox10.Controls.Add(this.label63);
            this.groupBox10.Controls.Add(this._vls15);
            this.groupBox10.Controls.Add(this.label64);
            this.groupBox10.Controls.Add(this._vls14);
            this.groupBox10.Controls.Add(this.label65);
            this.groupBox10.Controls.Add(this._vls13);
            this.groupBox10.Controls.Add(this.label66);
            this.groupBox10.Controls.Add(this._vls12);
            this.groupBox10.Controls.Add(this.label67);
            this.groupBox10.Controls.Add(this._vls11);
            this.groupBox10.Controls.Add(this.label68);
            this.groupBox10.Controls.Add(this._vls10);
            this.groupBox10.Controls.Add(this.label69);
            this.groupBox10.Controls.Add(this._vls9);
            this.groupBox10.Controls.Add(this.label70);
            this.groupBox10.Controls.Add(this._vls8);
            this.groupBox10.Controls.Add(this.label71);
            this.groupBox10.Controls.Add(this._vls7);
            this.groupBox10.Controls.Add(this.label72);
            this.groupBox10.Controls.Add(this._vls6);
            this.groupBox10.Controls.Add(this.label73);
            this.groupBox10.Controls.Add(this._vls5);
            this.groupBox10.Controls.Add(this.label74);
            this.groupBox10.Controls.Add(this._vls4);
            this.groupBox10.Controls.Add(this.label75);
            this.groupBox10.Controls.Add(this._vls3);
            this.groupBox10.Controls.Add(this.label76);
            this.groupBox10.Controls.Add(this._vls2);
            this.groupBox10.Controls.Add(this.label77);
            this.groupBox10.Controls.Add(this._vls1);
            this.groupBox10.Controls.Add(this.label78);
            this.groupBox10.Location = new System.Drawing.Point(546, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(150, 172);
            this.groupBox10.TabIndex = 3;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Выходные сигналы ВЛС";
            // 
            // _vls16
            // 
            this._vls16.Location = new System.Drawing.Point(71, 152);
            this._vls16.Name = "_vls16";
            this._vls16.Size = new System.Drawing.Size(13, 13);
            this._vls16.State = BEMN.Forms.LedState.Off;
            this._vls16.TabIndex = 31;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(90, 152);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 30;
            this.label63.Text = "ВЛС16";
            // 
            // _vls15
            // 
            this._vls15.Location = new System.Drawing.Point(71, 133);
            this._vls15.Name = "_vls15";
            this._vls15.Size = new System.Drawing.Size(13, 13);
            this._vls15.State = BEMN.Forms.LedState.Off;
            this._vls15.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(90, 133);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 28;
            this.label64.Text = "ВЛС15";
            // 
            // _vls14
            // 
            this._vls14.Location = new System.Drawing.Point(71, 114);
            this._vls14.Name = "_vls14";
            this._vls14.Size = new System.Drawing.Size(13, 13);
            this._vls14.State = BEMN.Forms.LedState.Off;
            this._vls14.TabIndex = 27;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(90, 114);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "ВЛС14";
            // 
            // _vls13
            // 
            this._vls13.Location = new System.Drawing.Point(71, 95);
            this._vls13.Name = "_vls13";
            this._vls13.Size = new System.Drawing.Size(13, 13);
            this._vls13.State = BEMN.Forms.LedState.Off;
            this._vls13.TabIndex = 25;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(90, 95);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 24;
            this.label66.Text = "ВЛС13";
            // 
            // _vls12
            // 
            this._vls12.Location = new System.Drawing.Point(71, 76);
            this._vls12.Name = "_vls12";
            this._vls12.Size = new System.Drawing.Size(13, 13);
            this._vls12.State = BEMN.Forms.LedState.Off;
            this._vls12.TabIndex = 23;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(90, 76);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 22;
            this.label67.Text = "ВЛС12";
            // 
            // _vls11
            // 
            this._vls11.Location = new System.Drawing.Point(71, 57);
            this._vls11.Name = "_vls11";
            this._vls11.Size = new System.Drawing.Size(13, 13);
            this._vls11.State = BEMN.Forms.LedState.Off;
            this._vls11.TabIndex = 21;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(90, 57);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "ВЛС11";
            // 
            // _vls10
            // 
            this._vls10.Location = new System.Drawing.Point(71, 38);
            this._vls10.Name = "_vls10";
            this._vls10.Size = new System.Drawing.Size(13, 13);
            this._vls10.State = BEMN.Forms.LedState.Off;
            this._vls10.TabIndex = 19;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(90, 38);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 18;
            this.label69.Text = "ВЛС10";
            // 
            // _vls9
            // 
            this._vls9.Location = new System.Drawing.Point(71, 19);
            this._vls9.Name = "_vls9";
            this._vls9.Size = new System.Drawing.Size(13, 13);
            this._vls9.State = BEMN.Forms.LedState.Off;
            this._vls9.TabIndex = 17;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(90, 19);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(35, 13);
            this.label70.TabIndex = 16;
            this.label70.Text = "ВЛС9";
            // 
            // _vls8
            // 
            this._vls8.Location = new System.Drawing.Point(6, 152);
            this._vls8.Name = "_vls8";
            this._vls8.Size = new System.Drawing.Size(13, 13);
            this._vls8.State = BEMN.Forms.LedState.Off;
            this._vls8.TabIndex = 15;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(25, 152);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(35, 13);
            this.label71.TabIndex = 14;
            this.label71.Text = "ВЛС8";
            // 
            // _vls7
            // 
            this._vls7.Location = new System.Drawing.Point(6, 133);
            this._vls7.Name = "_vls7";
            this._vls7.Size = new System.Drawing.Size(13, 13);
            this._vls7.State = BEMN.Forms.LedState.Off;
            this._vls7.TabIndex = 13;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(25, 133);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 13);
            this.label72.TabIndex = 12;
            this.label72.Text = "ВЛС7";
            // 
            // _vls6
            // 
            this._vls6.Location = new System.Drawing.Point(6, 114);
            this._vls6.Name = "_vls6";
            this._vls6.Size = new System.Drawing.Size(13, 13);
            this._vls6.State = BEMN.Forms.LedState.Off;
            this._vls6.TabIndex = 11;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(25, 114);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(35, 13);
            this.label73.TabIndex = 10;
            this.label73.Text = "ВЛС6";
            // 
            // _vls5
            // 
            this._vls5.Location = new System.Drawing.Point(6, 95);
            this._vls5.Name = "_vls5";
            this._vls5.Size = new System.Drawing.Size(13, 13);
            this._vls5.State = BEMN.Forms.LedState.Off;
            this._vls5.TabIndex = 9;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(25, 95);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(35, 13);
            this.label74.TabIndex = 8;
            this.label74.Text = "ВЛС5";
            // 
            // _vls4
            // 
            this._vls4.Location = new System.Drawing.Point(6, 76);
            this._vls4.Name = "_vls4";
            this._vls4.Size = new System.Drawing.Size(13, 13);
            this._vls4.State = BEMN.Forms.LedState.Off;
            this._vls4.TabIndex = 7;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(25, 76);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(35, 13);
            this.label75.TabIndex = 6;
            this.label75.Text = "ВЛС4";
            // 
            // _vls3
            // 
            this._vls3.Location = new System.Drawing.Point(6, 57);
            this._vls3.Name = "_vls3";
            this._vls3.Size = new System.Drawing.Size(13, 13);
            this._vls3.State = BEMN.Forms.LedState.Off;
            this._vls3.TabIndex = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(25, 57);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 4;
            this.label76.Text = "ВЛС3";
            // 
            // _vls2
            // 
            this._vls2.Location = new System.Drawing.Point(6, 38);
            this._vls2.Name = "_vls2";
            this._vls2.Size = new System.Drawing.Size(13, 13);
            this._vls2.State = BEMN.Forms.LedState.Off;
            this._vls2.TabIndex = 3;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(25, 38);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(35, 13);
            this.label77.TabIndex = 2;
            this.label77.Text = "ВЛС2";
            // 
            // _vls1
            // 
            this._vls1.Location = new System.Drawing.Point(6, 19);
            this._vls1.Name = "_vls1";
            this._vls1.Size = new System.Drawing.Size(13, 13);
            this._vls1.State = BEMN.Forms.LedState.Off;
            this._vls1.TabIndex = 1;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(25, 19);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(35, 13);
            this.label78.TabIndex = 0;
            this.label78.Text = "ВЛС1";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this._ls16);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._ls15);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._ls14);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Controls.Add(this._ls13);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._ls12);
            this.groupBox9.Controls.Add(this.label51);
            this.groupBox9.Controls.Add(this._ls11);
            this.groupBox9.Controls.Add(this.label52);
            this.groupBox9.Controls.Add(this._ls10);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this._ls9);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this._ls8);
            this.groupBox9.Controls.Add(this.label55);
            this.groupBox9.Controls.Add(this._ls7);
            this.groupBox9.Controls.Add(this.label56);
            this.groupBox9.Controls.Add(this._ls6);
            this.groupBox9.Controls.Add(this.label57);
            this.groupBox9.Controls.Add(this._ls5);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this._ls4);
            this.groupBox9.Controls.Add(this.label59);
            this.groupBox9.Controls.Add(this._ls3);
            this.groupBox9.Controls.Add(this.label60);
            this.groupBox9.Controls.Add(this._ls2);
            this.groupBox9.Controls.Add(this.label61);
            this.groupBox9.Controls.Add(this._ls1);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Location = new System.Drawing.Point(8, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(131, 172);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные сигналы ЛС";
            // 
            // _ls16
            // 
            this._ls16.Location = new System.Drawing.Point(71, 152);
            this._ls16.Name = "_ls16";
            this._ls16.Size = new System.Drawing.Size(13, 13);
            this._ls16.State = BEMN.Forms.LedState.Off;
            this._ls16.TabIndex = 31;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 152);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(34, 13);
            this.label47.TabIndex = 30;
            this.label47.Text = "ЛС16";
            // 
            // _ls15
            // 
            this._ls15.Location = new System.Drawing.Point(71, 133);
            this._ls15.Name = "_ls15";
            this._ls15.Size = new System.Drawing.Size(13, 13);
            this._ls15.State = BEMN.Forms.LedState.Off;
            this._ls15.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(90, 133);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(34, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "ЛС15";
            // 
            // _ls14
            // 
            this._ls14.Location = new System.Drawing.Point(71, 114);
            this._ls14.Name = "_ls14";
            this._ls14.Size = new System.Drawing.Size(13, 13);
            this._ls14.State = BEMN.Forms.LedState.Off;
            this._ls14.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(90, 114);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(34, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "ЛС14";
            // 
            // _ls13
            // 
            this._ls13.Location = new System.Drawing.Point(71, 95);
            this._ls13.Name = "_ls13";
            this._ls13.Size = new System.Drawing.Size(13, 13);
            this._ls13.State = BEMN.Forms.LedState.Off;
            this._ls13.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(90, 95);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "ЛС13";
            // 
            // _ls12
            // 
            this._ls12.Location = new System.Drawing.Point(71, 76);
            this._ls12.Name = "_ls12";
            this._ls12.Size = new System.Drawing.Size(13, 13);
            this._ls12.State = BEMN.Forms.LedState.Off;
            this._ls12.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(90, 76);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(34, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "ЛС12";
            // 
            // _ls11
            // 
            this._ls11.Location = new System.Drawing.Point(71, 57);
            this._ls11.Name = "_ls11";
            this._ls11.Size = new System.Drawing.Size(13, 13);
            this._ls11.State = BEMN.Forms.LedState.Off;
            this._ls11.TabIndex = 21;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(90, 57);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 13);
            this.label52.TabIndex = 20;
            this.label52.Text = "ЛС11";
            // 
            // _ls10
            // 
            this._ls10.Location = new System.Drawing.Point(71, 38);
            this._ls10.Name = "_ls10";
            this._ls10.Size = new System.Drawing.Size(13, 13);
            this._ls10.State = BEMN.Forms.LedState.Off;
            this._ls10.TabIndex = 19;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(90, 38);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 18;
            this.label53.Text = "ЛС10";
            // 
            // _ls9
            // 
            this._ls9.Location = new System.Drawing.Point(71, 19);
            this._ls9.Name = "_ls9";
            this._ls9.Size = new System.Drawing.Size(13, 13);
            this._ls9.State = BEMN.Forms.LedState.Off;
            this._ls9.TabIndex = 17;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(90, 19);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(28, 13);
            this.label54.TabIndex = 16;
            this.label54.Text = "ЛС9";
            // 
            // _ls8
            // 
            this._ls8.Location = new System.Drawing.Point(6, 152);
            this._ls8.Name = "_ls8";
            this._ls8.Size = new System.Drawing.Size(13, 13);
            this._ls8.State = BEMN.Forms.LedState.Off;
            this._ls8.TabIndex = 15;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 152);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(28, 13);
            this.label55.TabIndex = 14;
            this.label55.Text = "ЛС8";
            // 
            // _ls7
            // 
            this._ls7.Location = new System.Drawing.Point(6, 133);
            this._ls7.Name = "_ls7";
            this._ls7.Size = new System.Drawing.Size(13, 13);
            this._ls7.State = BEMN.Forms.LedState.Off;
            this._ls7.TabIndex = 13;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(25, 133);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 12;
            this.label56.Text = "ЛС7";
            // 
            // _ls6
            // 
            this._ls6.Location = new System.Drawing.Point(6, 114);
            this._ls6.Name = "_ls6";
            this._ls6.Size = new System.Drawing.Size(13, 13);
            this._ls6.State = BEMN.Forms.LedState.Off;
            this._ls6.TabIndex = 11;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 114);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 13);
            this.label57.TabIndex = 10;
            this.label57.Text = "ЛС6";
            // 
            // _ls5
            // 
            this._ls5.Location = new System.Drawing.Point(6, 95);
            this._ls5.Name = "_ls5";
            this._ls5.Size = new System.Drawing.Size(13, 13);
            this._ls5.State = BEMN.Forms.LedState.Off;
            this._ls5.TabIndex = 9;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(25, 95);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 13);
            this.label58.TabIndex = 8;
            this.label58.Text = "ЛС5";
            // 
            // _ls4
            // 
            this._ls4.Location = new System.Drawing.Point(6, 76);
            this._ls4.Name = "_ls4";
            this._ls4.Size = new System.Drawing.Size(13, 13);
            this._ls4.State = BEMN.Forms.LedState.Off;
            this._ls4.TabIndex = 7;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(25, 76);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "ЛС4";
            // 
            // _ls3
            // 
            this._ls3.Location = new System.Drawing.Point(6, 57);
            this._ls3.Name = "_ls3";
            this._ls3.Size = new System.Drawing.Size(13, 13);
            this._ls3.State = BEMN.Forms.LedState.Off;
            this._ls3.TabIndex = 5;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(25, 57);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(28, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "ЛС3";
            // 
            // _ls2
            // 
            this._ls2.Location = new System.Drawing.Point(6, 38);
            this._ls2.Name = "_ls2";
            this._ls2.Size = new System.Drawing.Size(13, 13);
            this._ls2.State = BEMN.Forms.LedState.Off;
            this._ls2.TabIndex = 3;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(25, 38);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(28, 13);
            this.label61.TabIndex = 2;
            this.label61.Text = "ЛС2";
            // 
            // _ls1
            // 
            this._ls1.Location = new System.Drawing.Point(6, 19);
            this._ls1.Name = "_ls1";
            this._ls1.Size = new System.Drawing.Size(13, 13);
            this._ls1.State = BEMN.Forms.LedState.Off;
            this._ls1.TabIndex = 1;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(25, 19);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 13);
            this.label62.TabIndex = 0;
            this.label62.Text = "ЛС1";
            // 
            // _discretTabPage2
            // 
            this._discretTabPage2.Controls.Add(this.groupBox35);
            this._discretTabPage2.Controls.Add(this.groupBox22);
            this._discretTabPage2.Controls.Add(this.groupBox20);
            this._discretTabPage2.Controls.Add(this.reversGroup1);
            this._discretTabPage2.Controls.Add(this.dugGroup1);
            this._discretTabPage2.Controls.Add(this.groupBox66);
            this._discretTabPage2.Controls.Add(this.groupBox64);
            this._discretTabPage2.Controls.Add(this.urovGroup1);
            this._discretTabPage2.Controls.Add(this.groupBox61);
            this._discretTabPage2.Controls.Add(this.groupBox62);
            this._discretTabPage2.Controls.Add(this.groupBox58);
            this._discretTabPage2.Controls.Add(this.groupBox59);
            this._discretTabPage2.Controls.Add(this.groupBox60);
            this._discretTabPage2.Controls.Add(this.groupBox55);
            this._discretTabPage2.Controls.Add(this.groupBox56);
            this._discretTabPage2.Controls.Add(this.groupBox57);
            this._discretTabPage2.Controls.Add(this.groupBox12);
            this._discretTabPage2.Controls.Add(this.groupBox38);
            this._discretTabPage2.Controls.Add(this.groupBox49);
            this._discretTabPage2.Controls.Add(this.groupBox48);
            this._discretTabPage2.Controls.Add(this.groupBox4);
            this._discretTabPage2.Controls.Add(this.groupBox32);
            this._discretTabPage2.Controls.Add(this.groupBox31);
            this._discretTabPage2.Controls.Add(this.groupBox30);
            this._discretTabPage2.Controls.Add(this.groupBox29);
            this._discretTabPage2.Controls.Add(this.groupBox19);
            this._discretTabPage2.Location = new System.Drawing.Point(4, 22);
            this._discretTabPage2.Name = "_discretTabPage2";
            this._discretTabPage2.Padding = new System.Windows.Forms.Padding(3);
            this._discretTabPage2.Size = new System.Drawing.Size(911, 599);
            this._discretTabPage2.TabIndex = 3;
            this._discretTabPage2.Text = "Неисправности и защиты";
            this._discretTabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.label139);
            this.groupBox35.Controls.Add(this.label134);
            this.groupBox35.Controls.Add(this.label136);
            this.groupBox35.Controls.Add(this.label131);
            this.groupBox35.Controls.Add(this.label132);
            this.groupBox35.Controls.Add(this._Id0Max2);
            this.groupBox35.Controls.Add(this._Id0Max2IO);
            this.groupBox35.Controls.Add(this._Id0Max3);
            this.groupBox35.Controls.Add(this._Id0Max3IO);
            this.groupBox35.Controls.Add(this._Id0Max1);
            this.groupBox35.Controls.Add(this._Id0Max1IO);
            this.groupBox35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.groupBox35.Location = new System.Drawing.Point(8, 476);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(84, 79);
            this.groupBox35.TabIndex = 74;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Защиты Iд0";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label139.Location = new System.Drawing.Point(41, 60);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(43, 13);
            this.label139.TabIndex = 86;
            this.label139.Text = "Iд0 >>>";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label134.Location = new System.Drawing.Point(41, 46);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(37, 13);
            this.label134.TabIndex = 86;
            this.label134.Text = "Iд0 >>";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label136.Location = new System.Drawing.Point(41, 32);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(31, 13);
            this.label136.TabIndex = 87;
            this.label136.Text = "Iд0 >";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.ForeColor = System.Drawing.Color.Blue;
            this.label131.Location = new System.Drawing.Point(25, 15);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(32, 13);
            this.label131.TabIndex = 85;
            this.label131.Text = "Сраб";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.ForeColor = System.Drawing.Color.Red;
            this.label132.Location = new System.Drawing.Point(6, 15);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(23, 13);
            this.label132.TabIndex = 84;
            this.label132.Text = "ИО";
            // 
            // _Id0Max2
            // 
            this._Id0Max2.Location = new System.Drawing.Point(28, 46);
            this._Id0Max2.Name = "_Id0Max2";
            this._Id0Max2.Size = new System.Drawing.Size(13, 13);
            this._Id0Max2.State = BEMN.Forms.LedState.Off;
            this._Id0Max2.TabIndex = 62;
            // 
            // _Id0Max2IO
            // 
            this._Id0Max2IO.Location = new System.Drawing.Point(9, 46);
            this._Id0Max2IO.Name = "_Id0Max2IO";
            this._Id0Max2IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max2IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max2IO.TabIndex = 62;
            // 
            // _Id0Max3
            // 
            this._Id0Max3.Location = new System.Drawing.Point(28, 60);
            this._Id0Max3.Name = "_Id0Max3";
            this._Id0Max3.Size = new System.Drawing.Size(13, 13);
            this._Id0Max3.State = BEMN.Forms.LedState.Off;
            this._Id0Max3.TabIndex = 59;
            // 
            // _Id0Max3IO
            // 
            this._Id0Max3IO.Location = new System.Drawing.Point(9, 60);
            this._Id0Max3IO.Name = "_Id0Max3IO";
            this._Id0Max3IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max3IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max3IO.TabIndex = 59;
            // 
            // _Id0Max1
            // 
            this._Id0Max1.Location = new System.Drawing.Point(28, 32);
            this._Id0Max1.Name = "_Id0Max1";
            this._Id0Max1.Size = new System.Drawing.Size(13, 13);
            this._Id0Max1.State = BEMN.Forms.LedState.Off;
            this._Id0Max1.TabIndex = 61;
            // 
            // _Id0Max1IO
            // 
            this._Id0Max1IO.Location = new System.Drawing.Point(9, 32);
            this._Id0Max1IO.Name = "_Id0Max1IO";
            this._Id0Max1IO.Size = new System.Drawing.Size(13, 13);
            this._Id0Max1IO.State = BEMN.Forms.LedState.Off;
            this._Id0Max1IO.TabIndex = 61;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this._IdMax1);
            this.groupBox22.Controls.Add(this._IdMax2);
            this.groupBox22.Controls.Add(this._IdMax2IO);
            this.groupBox22.Controls.Add(this._IdMax1IO);
            this.groupBox22.Controls.Add(this._IdMax2Mgn);
            this.groupBox22.Controls.Add(this.label46);
            this.groupBox22.Controls.Add(this.label105);
            this.groupBox22.Controls.Add(this.label127);
            this.groupBox22.Controls.Add(this.label128);
            this.groupBox22.Controls.Add(this.label129);
            this.groupBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.groupBox22.Location = new System.Drawing.Point(8, 6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(84, 99);
            this.groupBox22.TabIndex = 73;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Защиты I д";
            // 
            // _IdMax1
            // 
            this._IdMax1.Location = new System.Drawing.Point(66, 32);
            this._IdMax1.Name = "_IdMax1";
            this._IdMax1.Size = new System.Drawing.Size(13, 13);
            this._IdMax1.State = BEMN.Forms.LedState.Off;
            this._IdMax1.TabIndex = 62;
            // 
            // _IdMax2
            // 
            this._IdMax2.Location = new System.Drawing.Point(66, 60);
            this._IdMax2.Name = "_IdMax2";
            this._IdMax2.Size = new System.Drawing.Size(13, 13);
            this._IdMax2.State = BEMN.Forms.LedState.Off;
            this._IdMax2.TabIndex = 60;
            // 
            // _IdMax2IO
            // 
            this._IdMax2IO.Location = new System.Drawing.Point(66, 46);
            this._IdMax2IO.Name = "_IdMax2IO";
            this._IdMax2IO.Size = new System.Drawing.Size(13, 13);
            this._IdMax2IO.State = BEMN.Forms.LedState.Off;
            this._IdMax2IO.TabIndex = 59;
            // 
            // _IdMax1IO
            // 
            this._IdMax1IO.Location = new System.Drawing.Point(66, 18);
            this._IdMax1IO.Name = "_IdMax1IO";
            this._IdMax1IO.Size = new System.Drawing.Size(13, 13);
            this._IdMax1IO.State = BEMN.Forms.LedState.Off;
            this._IdMax1IO.TabIndex = 61;
            // 
            // _IdMax2Mgn
            // 
            this._IdMax2Mgn.Location = new System.Drawing.Point(66, 74);
            this._IdMax2Mgn.Name = "_IdMax2Mgn";
            this._IdMax2Mgn.Size = new System.Drawing.Size(13, 13);
            this._IdMax2Mgn.State = BEMN.Forms.LedState.Off;
            this._IdMax2Mgn.TabIndex = 58;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(6, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(31, 13);
            this.label46.TabIndex = 55;
            this.label46.Text = "Iд >>";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label105.Location = new System.Drawing.Point(6, 45);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(53, 13);
            this.label105.TabIndex = 54;
            this.label105.Text = "Iд >>  ИО";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label127.Location = new System.Drawing.Point(6, 32);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(25, 13);
            this.label127.TabIndex = 57;
            this.label127.Text = "Iд >";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label128.Location = new System.Drawing.Point(6, 73);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(59, 13);
            this.label128.TabIndex = 53;
            this.label128.Text = "Iд >>  мгн.";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label129.Location = new System.Drawing.Point(6, 18);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(53, 13);
            this.label129.TabIndex = 56;
            this.label129.Text = "Iд >    ИО";
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this._fault);
            this.groupBox20.Controls.Add(this.label43);
            this.groupBox20.Controls.Add(this._faultOff);
            this.groupBox20.Controls.Add(this.label44);
            this.groupBox20.Controls.Add(this._acceleration);
            this.groupBox20.Controls.Add(this.label45);
            this.groupBox20.Location = new System.Drawing.Point(726, 376);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(177, 80);
            this.groupBox20.TabIndex = 72;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Общие сигналы";
            // 
            // _fault
            // 
            this._fault.Location = new System.Drawing.Point(6, 19);
            this._fault.Name = "_fault";
            this._fault.Size = new System.Drawing.Size(13, 13);
            this._fault.State = BEMN.Forms.LedState.Off;
            this._fault.TabIndex = 37;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(25, 19);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(86, 13);
            this.label43.TabIndex = 36;
            this.label43.Text = "Неисправность";
            // 
            // _faultOff
            // 
            this._faultOff.Location = new System.Drawing.Point(6, 57);
            this._faultOff.Name = "_faultOff";
            this._faultOff.Size = new System.Drawing.Size(13, 13);
            this._faultOff.State = BEMN.Forms.LedState.Off;
            this._faultOff.TabIndex = 35;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(25, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(98, 13);
            this.label44.TabIndex = 34;
            this.label44.Text = "Авар. отключение";
            // 
            // _acceleration
            // 
            this._acceleration.Location = new System.Drawing.Point(6, 38);
            this._acceleration.Name = "_acceleration";
            this._acceleration.Size = new System.Drawing.Size(13, 13);
            this._acceleration.State = BEMN.Forms.LedState.Off;
            this._acceleration.TabIndex = 31;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(25, 38);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(81, 13);
            this.label45.TabIndex = 30;
            this.label45.Text = "Ускор. по вкл.";
            // 
            // reversGroup1
            // 
            this.reversGroup1.Controls.Add(this.label450);
            this.reversGroup1.Controls.Add(this.label451);
            this.reversGroup1.Controls.Add(this._P2Io1);
            this.reversGroup1.Controls.Add(this._P1Io1);
            this.reversGroup1.Controls.Add(this._P21);
            this.reversGroup1.Controls.Add(this.label452);
            this.reversGroup1.Controls.Add(this._P11);
            this.reversGroup1.Controls.Add(this.label453);
            this.reversGroup1.Location = new System.Drawing.Point(98, 424);
            this.reversGroup1.Name = "reversGroup1";
            this.reversGroup1.Size = new System.Drawing.Size(92, 71);
            this.reversGroup1.TabIndex = 67;
            this.reversGroup1.TabStop = false;
            this.reversGroup1.Text = "Защиты P";
            // 
            // label450
            // 
            this.label450.AutoSize = true;
            this.label450.ForeColor = System.Drawing.Color.Blue;
            this.label450.Location = new System.Drawing.Point(25, 16);
            this.label450.Name = "label450";
            this.label450.Size = new System.Drawing.Size(32, 13);
            this.label450.TabIndex = 65;
            this.label450.Text = "Сраб";
            // 
            // label451
            // 
            this.label451.AutoSize = true;
            this.label451.ForeColor = System.Drawing.Color.Red;
            this.label451.Location = new System.Drawing.Point(6, 16);
            this.label451.Name = "label451";
            this.label451.Size = new System.Drawing.Size(23, 13);
            this.label451.TabIndex = 64;
            this.label451.Text = "ИО";
            // 
            // _P2Io1
            // 
            this._P2Io1.Location = new System.Drawing.Point(9, 54);
            this._P2Io1.Name = "_P2Io1";
            this._P2Io1.Size = new System.Drawing.Size(13, 13);
            this._P2Io1.State = BEMN.Forms.LedState.Off;
            this._P2Io1.TabIndex = 3;
            // 
            // _P1Io1
            // 
            this._P1Io1.Location = new System.Drawing.Point(9, 35);
            this._P1Io1.Name = "_P1Io1";
            this._P1Io1.Size = new System.Drawing.Size(13, 13);
            this._P1Io1.State = BEMN.Forms.LedState.Off;
            this._P1Io1.TabIndex = 1;
            // 
            // _P21
            // 
            this._P21.Location = new System.Drawing.Point(28, 54);
            this._P21.Name = "_P21";
            this._P21.Size = new System.Drawing.Size(13, 13);
            this._P21.State = BEMN.Forms.LedState.Off;
            this._P21.TabIndex = 3;
            // 
            // label452
            // 
            this.label452.AutoSize = true;
            this.label452.Location = new System.Drawing.Point(47, 54);
            this.label452.Name = "label452";
            this.label452.Size = new System.Drawing.Size(23, 13);
            this.label452.TabIndex = 2;
            this.label452.Text = "P 2";
            // 
            // _P11
            // 
            this._P11.Location = new System.Drawing.Point(28, 35);
            this._P11.Name = "_P11";
            this._P11.Size = new System.Drawing.Size(13, 13);
            this._P11.State = BEMN.Forms.LedState.Off;
            this._P11.TabIndex = 1;
            // 
            // label453
            // 
            this.label453.AutoSize = true;
            this.label453.Location = new System.Drawing.Point(47, 35);
            this.label453.Name = "label453";
            this.label453.Size = new System.Drawing.Size(23, 13);
            this.label453.TabIndex = 0;
            this.label453.Text = "P 1";
            // 
            // dugGroup1
            // 
            this.dugGroup1.Controls.Add(this.dugPusk1);
            this.dugGroup1.Controls.Add(this.label447);
            this.dugGroup1.Location = new System.Drawing.Point(8, 424);
            this.dugGroup1.Name = "dugGroup1";
            this.dugGroup1.Size = new System.Drawing.Size(84, 45);
            this.dugGroup1.TabIndex = 50;
            this.dugGroup1.TabStop = false;
            // 
            // dugPusk1
            // 
            this.dugPusk1.Location = new System.Drawing.Point(6, 19);
            this.dugPusk1.Name = "dugPusk1";
            this.dugPusk1.Size = new System.Drawing.Size(13, 13);
            this.dugPusk1.State = BEMN.Forms.LedState.Off;
            this.dugPusk1.TabIndex = 37;
            // 
            // label447
            // 
            this.label447.AutoSize = true;
            this.label447.Location = new System.Drawing.Point(25, 13);
            this.label447.Name = "label447";
            this.label447.Size = new System.Drawing.Size(54, 26);
            this.label447.TabIndex = 36;
            this.label447.Text = "Пуск дуг.\r\nзащиты";
            // 
            // groupBox66
            // 
            this.groupBox66.Controls.Add(this.dvPusk1);
            this.groupBox66.Controls.Add(this.label442);
            this.groupBox66.Controls.Add(this.dvBlockN1);
            this.groupBox66.Controls.Add(this.dvBlockQ1);
            this.groupBox66.Controls.Add(this.label443);
            this.groupBox66.Controls.Add(this.label444);
            this.groupBox66.Controls.Add(this.dvWork1);
            this.groupBox66.Controls.Add(this.label445);
            this.groupBox66.Location = new System.Drawing.Point(286, 206);
            this.groupBox66.Name = "groupBox66";
            this.groupBox66.Size = new System.Drawing.Size(82, 108);
            this.groupBox66.TabIndex = 49;
            this.groupBox66.TabStop = false;
            this.groupBox66.Text = "Двигатель";
            // 
            // dvPusk1
            // 
            this.dvPusk1.Location = new System.Drawing.Point(6, 31);
            this.dvPusk1.Name = "dvPusk1";
            this.dvPusk1.Size = new System.Drawing.Size(13, 13);
            this.dvPusk1.State = BEMN.Forms.LedState.Off;
            this.dvPusk1.TabIndex = 37;
            // 
            // label442
            // 
            this.label442.AutoSize = true;
            this.label442.Location = new System.Drawing.Point(21, 31);
            this.label442.Name = "label442";
            this.label442.Size = new System.Drawing.Size(32, 13);
            this.label442.TabIndex = 36;
            this.label442.Text = "Пуск";
            // 
            // dvBlockN1
            // 
            this.dvBlockN1.Location = new System.Drawing.Point(6, 88);
            this.dvBlockN1.Name = "dvBlockN1";
            this.dvBlockN1.Size = new System.Drawing.Size(13, 13);
            this.dvBlockN1.State = BEMN.Forms.LedState.Off;
            this.dvBlockN1.TabIndex = 35;
            // 
            // dvBlockQ1
            // 
            this.dvBlockQ1.Location = new System.Drawing.Point(6, 69);
            this.dvBlockQ1.Name = "dvBlockQ1";
            this.dvBlockQ1.Size = new System.Drawing.Size(13, 13);
            this.dvBlockQ1.State = BEMN.Forms.LedState.Off;
            this.dvBlockQ1.TabIndex = 35;
            // 
            // label443
            // 
            this.label443.AutoSize = true;
            this.label443.Location = new System.Drawing.Point(21, 88);
            this.label443.Name = "label443";
            this.label443.Size = new System.Drawing.Size(61, 13);
            this.label443.TabIndex = 34;
            this.label443.Text = "Блок. по N";
            // 
            // label444
            // 
            this.label444.AutoSize = true;
            this.label444.Location = new System.Drawing.Point(21, 69);
            this.label444.Name = "label444";
            this.label444.Size = new System.Drawing.Size(61, 13);
            this.label444.TabIndex = 34;
            this.label444.Text = "Блок. по Q";
            // 
            // dvWork1
            // 
            this.dvWork1.Location = new System.Drawing.Point(6, 50);
            this.dvWork1.Name = "dvWork1";
            this.dvWork1.Size = new System.Drawing.Size(13, 13);
            this.dvWork1.State = BEMN.Forms.LedState.Off;
            this.dvWork1.TabIndex = 31;
            // 
            // label445
            // 
            this.label445.AutoSize = true;
            this.label445.Location = new System.Drawing.Point(21, 50);
            this.label445.Name = "label445";
            this.label445.Size = new System.Drawing.Size(43, 13);
            this.label445.TabIndex = 30;
            this.label445.Text = "Работа";
            // 
            // groupBox64
            // 
            this.groupBox64.Controls.Add(this._avrOn1);
            this.groupBox64.Controls.Add(this.label385);
            this.groupBox64.Controls.Add(this._avrBlock1);
            this.groupBox64.Controls.Add(this.label386);
            this.groupBox64.Controls.Add(this._avrOff1);
            this.groupBox64.Controls.Add(this.label387);
            this.groupBox64.Location = new System.Drawing.Point(374, 459);
            this.groupBox64.Name = "groupBox64";
            this.groupBox64.Size = new System.Drawing.Size(160, 96);
            this.groupBox64.TabIndex = 48;
            this.groupBox64.TabStop = false;
            this.groupBox64.Text = "АВР";
            // 
            // _avrOn1
            // 
            this._avrOn1.Location = new System.Drawing.Point(6, 23);
            this._avrOn1.Name = "_avrOn1";
            this._avrOn1.Size = new System.Drawing.Size(13, 13);
            this._avrOn1.State = BEMN.Forms.LedState.Off;
            this._avrOn1.TabIndex = 37;
            // 
            // label385
            // 
            this.label385.AutoSize = true;
            this.label385.Location = new System.Drawing.Point(25, 23);
            this.label385.Name = "label385";
            this.label385.Size = new System.Drawing.Size(52, 13);
            this.label385.TabIndex = 36;
            this.label385.Text = "АВР вкл.";
            // 
            // _avrBlock1
            // 
            this._avrBlock1.Location = new System.Drawing.Point(6, 61);
            this._avrBlock1.Name = "_avrBlock1";
            this._avrBlock1.Size = new System.Drawing.Size(13, 13);
            this._avrBlock1.State = BEMN.Forms.LedState.Off;
            this._avrBlock1.TabIndex = 35;
            // 
            // label386
            // 
            this.label386.AutoSize = true;
            this.label386.Location = new System.Drawing.Point(25, 61);
            this.label386.Name = "label386";
            this.label386.Size = new System.Drawing.Size(58, 13);
            this.label386.TabIndex = 34;
            this.label386.Text = "АВР блок.";
            // 
            // _avrOff1
            // 
            this._avrOff1.Location = new System.Drawing.Point(6, 42);
            this._avrOff1.Name = "_avrOff1";
            this._avrOff1.Size = new System.Drawing.Size(13, 13);
            this._avrOff1.State = BEMN.Forms.LedState.Off;
            this._avrOff1.TabIndex = 31;
            // 
            // label387
            // 
            this.label387.AutoSize = true;
            this.label387.Location = new System.Drawing.Point(25, 42);
            this.label387.Name = "label387";
            this.label387.Size = new System.Drawing.Size(57, 13);
            this.label387.TabIndex = 30;
            this.label387.Text = "АВР откл.";
            // 
            // urovGroup1
            // 
            this.urovGroup1.Controls.Add(this.urov1Led1);
            this.urovGroup1.Controls.Add(this.label417);
            this.urovGroup1.Controls.Add(this.blockUrovLed1);
            this.urovGroup1.Controls.Add(this.label418);
            this.urovGroup1.Controls.Add(this.urov2Led1);
            this.urovGroup1.Controls.Add(this.label419);
            this.urovGroup1.Location = new System.Drawing.Point(374, 376);
            this.urovGroup1.Name = "urovGroup1";
            this.urovGroup1.Size = new System.Drawing.Size(160, 80);
            this.urovGroup1.TabIndex = 47;
            this.urovGroup1.TabStop = false;
            this.urovGroup1.Text = "УРОВ";
            // 
            // urov1Led1
            // 
            this.urov1Led1.Location = new System.Drawing.Point(6, 19);
            this.urov1Led1.Name = "urov1Led1";
            this.urov1Led1.Size = new System.Drawing.Size(13, 13);
            this.urov1Led1.State = BEMN.Forms.LedState.Off;
            this.urov1Led1.TabIndex = 37;
            // 
            // label417
            // 
            this.label417.AutoSize = true;
            this.label417.Location = new System.Drawing.Point(25, 19);
            this.label417.Name = "label417";
            this.label417.Size = new System.Drawing.Size(43, 13);
            this.label417.TabIndex = 36;
            this.label417.Text = "УРОВ1";
            // 
            // blockUrovLed1
            // 
            this.blockUrovLed1.Location = new System.Drawing.Point(6, 57);
            this.blockUrovLed1.Name = "blockUrovLed1";
            this.blockUrovLed1.Size = new System.Drawing.Size(13, 13);
            this.blockUrovLed1.State = BEMN.Forms.LedState.Off;
            this.blockUrovLed1.TabIndex = 35;
            // 
            // label418
            // 
            this.label418.AutoSize = true;
            this.label418.Location = new System.Drawing.Point(25, 57);
            this.label418.Name = "label418";
            this.label418.Size = new System.Drawing.Size(68, 13);
            this.label418.TabIndex = 34;
            this.label418.Text = "Блок. УРОВ";
            // 
            // urov2Led1
            // 
            this.urov2Led1.Location = new System.Drawing.Point(6, 38);
            this.urov2Led1.Name = "urov2Led1";
            this.urov2Led1.Size = new System.Drawing.Size(13, 13);
            this.urov2Led1.State = BEMN.Forms.LedState.Off;
            this.urov2Led1.TabIndex = 31;
            // 
            // label419
            // 
            this.label419.AutoSize = true;
            this.label419.Location = new System.Drawing.Point(25, 38);
            this.label419.Name = "label419";
            this.label419.Size = new System.Drawing.Size(43, 13);
            this.label419.TabIndex = 30;
            this.label419.Text = "УРОВ2";
            // 
            // groupBox61
            // 
            this.groupBox61.Controls.Add(this._OnKsAndYppn1);
            this.groupBox61.Controls.Add(this.label423);
            this.groupBox61.Controls.Add(this._UnoUno1);
            this.groupBox61.Controls.Add(this.label424);
            this.groupBox61.Controls.Add(this._UyUno1);
            this.groupBox61.Controls.Add(this.label425);
            this.groupBox61.Controls.Add(this._US1);
            this.groupBox61.Controls.Add(this._U1noU2y1);
            this.groupBox61.Controls.Add(this.label426);
            this.groupBox61.Controls.Add(this.label427);
            this.groupBox61.Controls.Add(this.label428);
            this.groupBox61.Controls.Add(this._OS1);
            this.groupBox61.Controls.Add(this.label429);
            this.groupBox61.Controls.Add(this._autoSinchr1);
            this.groupBox61.Location = new System.Drawing.Point(374, 206);
            this.groupBox61.Name = "groupBox61";
            this.groupBox61.Size = new System.Drawing.Size(160, 155);
            this.groupBox61.TabIndex = 46;
            this.groupBox61.TabStop = false;
            this.groupBox61.Text = "Сигналы КС и УППН";
            // 
            // _OnKsAndYppn1
            // 
            this._OnKsAndYppn1.Location = new System.Drawing.Point(6, 132);
            this._OnKsAndYppn1.Name = "_OnKsAndYppn1";
            this._OnKsAndYppn1.Size = new System.Drawing.Size(13, 13);
            this._OnKsAndYppn1.State = BEMN.Forms.LedState.Off;
            this._OnKsAndYppn1.TabIndex = 39;
            // 
            // label423
            // 
            this.label423.AutoSize = true;
            this.label423.Location = new System.Drawing.Point(25, 132);
            this.label423.Name = "label423";
            this.label423.Size = new System.Drawing.Size(132, 13);
            this.label423.TabIndex = 38;
            this.label423.Text = "Включить по КС и УППН";
            // 
            // _UnoUno1
            // 
            this._UnoUno1.Location = new System.Drawing.Point(6, 75);
            this._UnoUno1.Name = "_UnoUno1";
            this._UnoUno1.Size = new System.Drawing.Size(13, 13);
            this._UnoUno1.State = BEMN.Forms.LedState.Off;
            this._UnoUno1.TabIndex = 37;
            // 
            // label424
            // 
            this.label424.AutoSize = true;
            this.label424.Location = new System.Drawing.Point(25, 75);
            this.label424.Name = "label424";
            this.label424.Size = new System.Drawing.Size(49, 13);
            this.label424.TabIndex = 36;
            this.label424.Text = "Uш-, Uл-";
            // 
            // _UyUno1
            // 
            this._UyUno1.Location = new System.Drawing.Point(6, 56);
            this._UyUno1.Name = "_UyUno1";
            this._UyUno1.Size = new System.Drawing.Size(13, 13);
            this._UyUno1.State = BEMN.Forms.LedState.Off;
            this._UyUno1.TabIndex = 35;
            // 
            // label425
            // 
            this.label425.AutoSize = true;
            this.label425.Location = new System.Drawing.Point(25, 56);
            this.label425.Name = "label425";
            this.label425.Size = new System.Drawing.Size(52, 13);
            this.label425.TabIndex = 34;
            this.label425.Text = "Uш+, Uл-";
            // 
            // _US1
            // 
            this._US1.Location = new System.Drawing.Point(6, 113);
            this._US1.Name = "_US1";
            this._US1.Size = new System.Drawing.Size(13, 13);
            this._US1.State = BEMN.Forms.LedState.Off;
            this._US1.TabIndex = 29;
            // 
            // _U1noU2y1
            // 
            this._U1noU2y1.Location = new System.Drawing.Point(6, 38);
            this._U1noU2y1.Name = "_U1noU2y1";
            this._U1noU2y1.Size = new System.Drawing.Size(13, 13);
            this._U1noU2y1.State = BEMN.Forms.LedState.Off;
            this._U1noU2y1.TabIndex = 33;
            // 
            // label426
            // 
            this.label426.AutoSize = true;
            this.label426.Location = new System.Drawing.Point(25, 38);
            this.label426.Name = "label426";
            this.label426.Size = new System.Drawing.Size(52, 13);
            this.label426.TabIndex = 32;
            this.label426.Text = "Uш-, Uл+";
            // 
            // label427
            // 
            this.label427.AutoSize = true;
            this.label427.Location = new System.Drawing.Point(25, 19);
            this.label427.Name = "label427";
            this.label427.Size = new System.Drawing.Size(128, 13);
            this.label427.TabIndex = 30;
            this.label427.Text = "Автоматический режим";
            // 
            // label428
            // 
            this.label428.AutoSize = true;
            this.label428.Location = new System.Drawing.Point(25, 113);
            this.label428.Name = "label428";
            this.label428.Size = new System.Drawing.Size(69, 13);
            this.label428.TabIndex = 28;
            this.label428.Text = "Условия УС";
            // 
            // _OS1
            // 
            this._OS1.Location = new System.Drawing.Point(6, 94);
            this._OS1.Name = "_OS1";
            this._OS1.Size = new System.Drawing.Size(13, 13);
            this._OS1.State = BEMN.Forms.LedState.Off;
            this._OS1.TabIndex = 11;
            // 
            // label429
            // 
            this.label429.AutoSize = true;
            this.label429.Location = new System.Drawing.Point(25, 94);
            this.label429.Name = "label429";
            this.label429.Size = new System.Drawing.Size(69, 13);
            this.label429.TabIndex = 10;
            this.label429.Text = "Условия ОС";
            // 
            // _autoSinchr1
            // 
            this._autoSinchr1.Location = new System.Drawing.Point(6, 19);
            this._autoSinchr1.Name = "_autoSinchr1";
            this._autoSinchr1.Size = new System.Drawing.Size(13, 13);
            this._autoSinchr1.State = BEMN.Forms.LedState.Off;
            this._autoSinchr1.TabIndex = 13;
            // 
            // groupBox62
            // 
            this.groupBox62.Controls.Add(this._readyApv1);
            this.groupBox62.Controls.Add(this.label430);
            this.groupBox62.Controls.Add(this._blockApv1);
            this.groupBox62.Controls.Add(this.label431);
            this.groupBox62.Controls.Add(this.label432);
            this.groupBox62.Controls.Add(this._puskApv1);
            this.groupBox62.Controls.Add(this._krat41);
            this.groupBox62.Controls.Add(this.label433);
            this.groupBox62.Controls.Add(this.label434);
            this.groupBox62.Controls.Add(this._turnOnApv1);
            this.groupBox62.Controls.Add(this._krat31);
            this.groupBox62.Controls.Add(this.label435);
            this.groupBox62.Controls.Add(this.label436);
            this.groupBox62.Controls.Add(this._krat11);
            this.groupBox62.Controls.Add(this._zapretApv1);
            this.groupBox62.Controls.Add(this.label437);
            this.groupBox62.Controls.Add(this._krat21);
            this.groupBox62.Controls.Add(this.label438);
            this.groupBox62.Location = new System.Drawing.Point(374, 6);
            this.groupBox62.Name = "groupBox62";
            this.groupBox62.Size = new System.Drawing.Size(160, 194);
            this.groupBox62.TabIndex = 45;
            this.groupBox62.TabStop = false;
            this.groupBox62.Text = "Сигналы АПВ";
            // 
            // _readyApv1
            // 
            this._readyApv1.Location = new System.Drawing.Point(8, 170);
            this._readyApv1.Name = "_readyApv1";
            this._readyApv1.Size = new System.Drawing.Size(13, 13);
            this._readyApv1.State = BEMN.Forms.LedState.Off;
            this._readyApv1.TabIndex = 57;
            // 
            // label430
            // 
            this.label430.AutoSize = true;
            this.label430.Location = new System.Drawing.Point(27, 170);
            this.label430.Name = "label430";
            this.label430.Size = new System.Drawing.Size(65, 13);
            this.label430.TabIndex = 56;
            this.label430.Text = "Готовность";
            // 
            // _blockApv1
            // 
            this._blockApv1.Location = new System.Drawing.Point(8, 151);
            this._blockApv1.Name = "_blockApv1";
            this._blockApv1.Size = new System.Drawing.Size(13, 13);
            this._blockApv1.State = BEMN.Forms.LedState.Off;
            this._blockApv1.TabIndex = 55;
            // 
            // label431
            // 
            this.label431.AutoSize = true;
            this.label431.Location = new System.Drawing.Point(27, 18);
            this.label431.Name = "label431";
            this.label431.Size = new System.Drawing.Size(32, 13);
            this.label431.TabIndex = 47;
            this.label431.Text = "Пуск";
            // 
            // label432
            // 
            this.label432.AutoSize = true;
            this.label432.Location = new System.Drawing.Point(27, 151);
            this.label432.Name = "label432";
            this.label432.Size = new System.Drawing.Size(68, 13);
            this.label432.TabIndex = 54;
            this.label432.Text = "Блокировка";
            // 
            // _puskApv1
            // 
            this._puskApv1.Location = new System.Drawing.Point(8, 18);
            this._puskApv1.Name = "_puskApv1";
            this._puskApv1.Size = new System.Drawing.Size(13, 13);
            this._puskApv1.State = BEMN.Forms.LedState.Off;
            this._puskApv1.TabIndex = 42;
            // 
            // _krat41
            // 
            this._krat41.Location = new System.Drawing.Point(8, 94);
            this._krat41.Name = "_krat41";
            this._krat41.Size = new System.Drawing.Size(13, 13);
            this._krat41.State = BEMN.Forms.LedState.Off;
            this._krat41.TabIndex = 53;
            // 
            // label433
            // 
            this.label433.AutoSize = true;
            this.label433.Location = new System.Drawing.Point(27, 113);
            this.label433.Name = "label433";
            this.label433.Size = new System.Drawing.Size(96, 13);
            this.label433.TabIndex = 40;
            this.label433.Text = "Включить по АПВ";
            // 
            // label434
            // 
            this.label434.AutoSize = true;
            this.label434.Location = new System.Drawing.Point(27, 94);
            this.label434.Name = "label434";
            this.label434.Size = new System.Drawing.Size(39, 13);
            this.label434.TabIndex = 52;
            this.label434.Text = "4 крат";
            // 
            // _turnOnApv1
            // 
            this._turnOnApv1.Location = new System.Drawing.Point(8, 113);
            this._turnOnApv1.Name = "_turnOnApv1";
            this._turnOnApv1.Size = new System.Drawing.Size(13, 13);
            this._turnOnApv1.State = BEMN.Forms.LedState.Off;
            this._turnOnApv1.TabIndex = 41;
            // 
            // _krat31
            // 
            this._krat31.Location = new System.Drawing.Point(8, 75);
            this._krat31.Name = "_krat31";
            this._krat31.Size = new System.Drawing.Size(13, 13);
            this._krat31.State = BEMN.Forms.LedState.Off;
            this._krat31.TabIndex = 51;
            // 
            // label435
            // 
            this.label435.AutoSize = true;
            this.label435.Location = new System.Drawing.Point(27, 37);
            this.label435.Name = "label435";
            this.label435.Size = new System.Drawing.Size(39, 13);
            this.label435.TabIndex = 44;
            this.label435.Text = "1 крат";
            // 
            // label436
            // 
            this.label436.AutoSize = true;
            this.label436.Location = new System.Drawing.Point(27, 75);
            this.label436.Name = "label436";
            this.label436.Size = new System.Drawing.Size(39, 13);
            this.label436.TabIndex = 50;
            this.label436.Text = "3 крат";
            // 
            // _krat11
            // 
            this._krat11.Location = new System.Drawing.Point(8, 37);
            this._krat11.Name = "_krat11";
            this._krat11.Size = new System.Drawing.Size(13, 13);
            this._krat11.State = BEMN.Forms.LedState.Off;
            this._krat11.TabIndex = 46;
            // 
            // _zapretApv1
            // 
            this._zapretApv1.Location = new System.Drawing.Point(8, 132);
            this._zapretApv1.Name = "_zapretApv1";
            this._zapretApv1.Size = new System.Drawing.Size(13, 13);
            this._zapretApv1.State = BEMN.Forms.LedState.Off;
            this._zapretApv1.TabIndex = 45;
            // 
            // label437
            // 
            this.label437.AutoSize = true;
            this.label437.Location = new System.Drawing.Point(27, 132);
            this.label437.Name = "label437";
            this.label437.Size = new System.Drawing.Size(43, 13);
            this.label437.TabIndex = 43;
            this.label437.Text = "Запрет";
            // 
            // _krat21
            // 
            this._krat21.Location = new System.Drawing.Point(8, 56);
            this._krat21.Name = "_krat21";
            this._krat21.Size = new System.Drawing.Size(13, 13);
            this._krat21.State = BEMN.Forms.LedState.Off;
            this._krat21.TabIndex = 49;
            // 
            // label438
            // 
            this.label438.AutoSize = true;
            this.label438.Location = new System.Drawing.Point(27, 56);
            this.label438.Name = "label438";
            this.label438.Size = new System.Drawing.Size(39, 13);
            this.label438.TabIndex = 48;
            this.label438.Text = "2 крат";
            // 
            // groupBox58
            // 
            this.groupBox58.Controls.Add(this._damageA1);
            this.groupBox58.Controls.Add(this.label409);
            this.groupBox58.Controls.Add(this.label410);
            this.groupBox58.Controls.Add(this._damageB1);
            this.groupBox58.Controls.Add(this._damageC1);
            this.groupBox58.Controls.Add(this.label411);
            this.groupBox58.Location = new System.Drawing.Point(196, 376);
            this.groupBox58.Name = "groupBox58";
            this.groupBox58.Size = new System.Drawing.Size(172, 79);
            this.groupBox58.TabIndex = 44;
            this.groupBox58.TabStop = false;
            this.groupBox58.Text = "Определение повр. фазы";
            // 
            // _damageA1
            // 
            this._damageA1.Location = new System.Drawing.Point(6, 19);
            this._damageA1.Name = "_damageA1";
            this._damageA1.Size = new System.Drawing.Size(13, 13);
            this._damageA1.State = BEMN.Forms.LedState.Off;
            this._damageA1.TabIndex = 13;
            // 
            // label409
            // 
            this.label409.AutoSize = true;
            this.label409.Location = new System.Drawing.Point(25, 20);
            this.label409.Name = "label409";
            this.label409.Size = new System.Drawing.Size(90, 13);
            this.label409.TabIndex = 30;
            this.label409.Text = "Повр. по фазе А";
            // 
            // label410
            // 
            this.label410.AutoSize = true;
            this.label410.Location = new System.Drawing.Point(25, 38);
            this.label410.Name = "label410";
            this.label410.Size = new System.Drawing.Size(90, 13);
            this.label410.TabIndex = 32;
            this.label410.Text = "Повр. по фазе В";
            // 
            // _damageB1
            // 
            this._damageB1.Location = new System.Drawing.Point(6, 38);
            this._damageB1.Name = "_damageB1";
            this._damageB1.Size = new System.Drawing.Size(13, 13);
            this._damageB1.State = BEMN.Forms.LedState.Off;
            this._damageB1.TabIndex = 33;
            // 
            // _damageC1
            // 
            this._damageC1.Location = new System.Drawing.Point(6, 57);
            this._damageC1.Name = "_damageC1";
            this._damageC1.Size = new System.Drawing.Size(13, 13);
            this._damageC1.State = BEMN.Forms.LedState.Off;
            this._damageC1.TabIndex = 35;
            // 
            // label411
            // 
            this.label411.AutoSize = true;
            this.label411.Location = new System.Drawing.Point(25, 57);
            this.label411.Name = "label411";
            this.label411.Size = new System.Drawing.Size(90, 13);
            this.label411.TabIndex = 34;
            this.label411.Text = "Повр. по фазе С";
            // 
            // groupBox59
            // 
            this.groupBox59.Controls.Add(this._externalTnUn1);
            this.groupBox59.Controls.Add(this._externalTnUn1Label);
            this.groupBox59.Controls.Add(this._tnObivFaz);
            this.groupBox59.Controls.Add(this.l3);
            this.groupBox59.Controls.Add(this._faultTn3U0);
            this.groupBox59.Controls.Add(this.l4);
            this.groupBox59.Controls.Add(this._externalTnUn);
            this.groupBox59.Controls.Add(this._faultTnU2);
            this.groupBox59.Controls.Add(this.l5);
            this.groupBox59.Controls.Add(this.label416);
            this.groupBox59.Controls.Add(this.l1);
            this.groupBox59.Controls.Add(this._faultTNmgn1);
            this.groupBox59.Controls.Add(this.l6);
            this.groupBox59.Controls.Add(this._externalTn);
            this.groupBox59.Controls.Add(this.l2);
            this.groupBox59.Controls.Add(this._faultTN1);
            this.groupBox59.Location = new System.Drawing.Point(540, 206);
            this.groupBox59.Name = "groupBox59";
            this.groupBox59.Size = new System.Drawing.Size(174, 170);
            this.groupBox59.TabIndex = 43;
            this.groupBox59.TabStop = false;
            this.groupBox59.Text = "Неисправности измерения U";
            // 
            // _externalTnUn1
            // 
            this._externalTnUn1.Location = new System.Drawing.Point(6, 132);
            this._externalTnUn1.Name = "_externalTnUn1";
            this._externalTnUn1.Size = new System.Drawing.Size(13, 13);
            this._externalTnUn1.State = BEMN.Forms.LedState.Off;
            this._externalTnUn1.TabIndex = 39;
            // 
            // _externalTnUn1Label
            // 
            this._externalTnUn1Label.AutoSize = true;
            this._externalTnUn1Label.Location = new System.Drawing.Point(25, 132);
            this._externalTnUn1Label.Name = "_externalTnUn1Label";
            this._externalTnUn1Label.Size = new System.Drawing.Size(39, 13);
            this._externalTnUn1Label.TabIndex = 38;
            this._externalTnUn1Label.Text = "Uabc2";
            // 
            // _tnObivFaz
            // 
            this._tnObivFaz.Location = new System.Drawing.Point(6, 94);
            this._tnObivFaz.Name = "_tnObivFaz";
            this._tnObivFaz.Size = new System.Drawing.Size(13, 13);
            this._tnObivFaz.State = BEMN.Forms.LedState.Off;
            this._tnObivFaz.TabIndex = 37;
            // 
            // l3
            // 
            this.l3.AutoSize = true;
            this.l3.Location = new System.Drawing.Point(25, 94);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(105, 13);
            this.l3.TabIndex = 36;
            this.l3.Text = "ТН обрыв трёх фаз";
            // 
            // _faultTn3U0
            // 
            this._faultTn3U0.Location = new System.Drawing.Point(6, 75);
            this._faultTn3U0.Name = "_faultTn3U0";
            this._faultTn3U0.Size = new System.Drawing.Size(13, 13);
            this._faultTn3U0.State = BEMN.Forms.LedState.Off;
            this._faultTn3U0.TabIndex = 35;
            // 
            // l4
            // 
            this.l4.AutoSize = true;
            this.l4.Location = new System.Drawing.Point(25, 75);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(60, 13);
            this.l4.TabIndex = 34;
            this.l4.Text = "ТН по 3U0";
            // 
            // _externalTnUn
            // 
            this._externalTnUn.Location = new System.Drawing.Point(6, 151);
            this._externalTnUn.Name = "_externalTnUn";
            this._externalTnUn.Size = new System.Drawing.Size(13, 13);
            this._externalTnUn.State = BEMN.Forms.LedState.Off;
            this._externalTnUn.TabIndex = 29;
            // 
            // _faultTnU2
            // 
            this._faultTnU2.Location = new System.Drawing.Point(6, 56);
            this._faultTnU2.Name = "_faultTnU2";
            this._faultTnU2.Size = new System.Drawing.Size(13, 13);
            this._faultTnU2.State = BEMN.Forms.LedState.Off;
            this._faultTnU2.TabIndex = 33;
            // 
            // l5
            // 
            this.l5.AutoSize = true;
            this.l5.Location = new System.Drawing.Point(25, 56);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(54, 13);
            this.l5.TabIndex = 32;
            this.l5.Text = "ТН по U2";
            // 
            // label416
            // 
            this.label416.AutoSize = true;
            this.label416.Location = new System.Drawing.Point(25, 19);
            this.label416.Name = "label416";
            this.label416.Size = new System.Drawing.Size(119, 13);
            this.label416.TabIndex = 30;
            this.label416.Text = "ТН с задержкой и с/п";
            // 
            // l1
            // 
            this.l1.AutoSize = true;
            this.l1.Location = new System.Drawing.Point(25, 151);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(21, 13);
            this.l1.TabIndex = 28;
            this.l1.Text = "Un";
            // 
            // _faultTNmgn1
            // 
            this._faultTNmgn1.Location = new System.Drawing.Point(6, 38);
            this._faultTNmgn1.Name = "_faultTNmgn1";
            this._faultTNmgn1.Size = new System.Drawing.Size(13, 13);
            this._faultTNmgn1.State = BEMN.Forms.LedState.Off;
            this._faultTNmgn1.TabIndex = 29;
            // 
            // l6
            // 
            this.l6.AutoSize = true;
            this.l6.Location = new System.Drawing.Point(25, 38);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(47, 13);
            this.l6.TabIndex = 28;
            this.l6.Text = "ТН мгн.";
            // 
            // _externalTn
            // 
            this._externalTn.Location = new System.Drawing.Point(6, 113);
            this._externalTn.Name = "_externalTn";
            this._externalTn.Size = new System.Drawing.Size(13, 13);
            this._externalTn.State = BEMN.Forms.LedState.Off;
            this._externalTn.TabIndex = 11;
            // 
            // l2
            // 
            this.l2.AutoSize = true;
            this.l2.Location = new System.Drawing.Point(25, 113);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(39, 13);
            this.l2.TabIndex = 10;
            this.l2.Text = "Uabc1";
            // 
            // _faultTN1
            // 
            this._faultTN1.Location = new System.Drawing.Point(6, 19);
            this._faultTN1.Name = "_faultTN1";
            this._faultTN1.Size = new System.Drawing.Size(13, 13);
            this._faultTN1.State = BEMN.Forms.LedState.Off;
            this._faultTN1.TabIndex = 13;
            // 
            // groupBox60
            // 
            this.groupBox60.Controls.Add(this._kachanie1);
            this.groupBox60.Controls.Add(this.label420);
            this.groupBox60.Controls.Add(this._inZone1);
            this.groupBox60.Controls.Add(this.label421);
            this.groupBox60.Controls.Add(this._outZone1);
            this.groupBox60.Controls.Add(this.label422);
            this.groupBox60.Location = new System.Drawing.Point(196, 459);
            this.groupBox60.Name = "groupBox60";
            this.groupBox60.Size = new System.Drawing.Size(172, 96);
            this.groupBox60.TabIndex = 42;
            this.groupBox60.TabStop = false;
            this.groupBox60.Text = "Сигналы качаний";
            // 
            // _kachanie1
            // 
            this._kachanie1.Location = new System.Drawing.Point(6, 23);
            this._kachanie1.Name = "_kachanie1";
            this._kachanie1.Size = new System.Drawing.Size(13, 13);
            this._kachanie1.State = BEMN.Forms.LedState.Off;
            this._kachanie1.TabIndex = 37;
            // 
            // label420
            // 
            this.label420.AutoSize = true;
            this.label420.Location = new System.Drawing.Point(25, 23);
            this.label420.Name = "label420";
            this.label420.Size = new System.Drawing.Size(49, 13);
            this.label420.TabIndex = 36;
            this.label420.Text = "Качание";
            // 
            // _inZone1
            // 
            this._inZone1.Location = new System.Drawing.Point(6, 61);
            this._inZone1.Name = "_inZone1";
            this._inZone1.Size = new System.Drawing.Size(13, 13);
            this._inZone1.State = BEMN.Forms.LedState.Off;
            this._inZone1.TabIndex = 31;
            // 
            // label421
            // 
            this.label421.AutoSize = true;
            this.label421.Location = new System.Drawing.Point(25, 61);
            this.label421.Name = "label421";
            this.label421.Size = new System.Drawing.Size(93, 13);
            this.label421.TabIndex = 30;
            this.label421.Text = "Внутренняя зона";
            // 
            // _outZone1
            // 
            this._outZone1.Location = new System.Drawing.Point(6, 42);
            this._outZone1.Name = "_outZone1";
            this._outZone1.Size = new System.Drawing.Size(13, 13);
            this._outZone1.State = BEMN.Forms.LedState.Off;
            this._outZone1.TabIndex = 27;
            // 
            // label422
            // 
            this.label422.AutoSize = true;
            this.label422.Location = new System.Drawing.Point(25, 42);
            this.label422.Name = "label422";
            this.label422.Size = new System.Drawing.Size(79, 13);
            this.label422.TabIndex = 26;
            this.label422.Text = "Внешняя зона";
            // 
            // groupBox55
            // 
            this.groupBox55.Controls.Add(this.label379);
            this.groupBox55.Controls.Add(this.label380);
            this.groupBox55.Controls.Add(this.label381);
            this.groupBox55.Controls.Add(this.label382);
            this.groupBox55.Controls.Add(this._r4IoLed1);
            this.groupBox55.Controls.Add(this._r5Led1);
            this.groupBox55.Controls.Add(this._r1Led1);
            this.groupBox55.Controls.Add(this._r3IoLed1);
            this.groupBox55.Controls.Add(this.label383);
            this.groupBox55.Controls.Add(this.label384);
            this.groupBox55.Controls.Add(this._r6IoLed1);
            this.groupBox55.Controls.Add(this._r2IoLed1);
            this.groupBox55.Controls.Add(this._r6Led1);
            this.groupBox55.Controls.Add(this._r2Led1);
            this.groupBox55.Controls.Add(this._r4Led1);
            this.groupBox55.Controls.Add(this.label388);
            this.groupBox55.Controls.Add(this.label390);
            this.groupBox55.Controls.Add(this._r3Led1);
            this.groupBox55.Controls.Add(this._r5IoLed1);
            this.groupBox55.Controls.Add(this._r1IoLed1);
            this.groupBox55.Location = new System.Drawing.Point(196, 206);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(84, 146);
            this.groupBox55.TabIndex = 40;
            this.groupBox55.TabStop = false;
            this.groupBox55.Text = "Защиты Z";
            // 
            // label379
            // 
            this.label379.AutoSize = true;
            this.label379.ForeColor = System.Drawing.Color.Blue;
            this.label379.Location = new System.Drawing.Point(25, 12);
            this.label379.Name = "label379";
            this.label379.Size = new System.Drawing.Size(32, 13);
            this.label379.TabIndex = 83;
            this.label379.Text = "Сраб";
            // 
            // label380
            // 
            this.label380.AutoSize = true;
            this.label380.ForeColor = System.Drawing.Color.Red;
            this.label380.Location = new System.Drawing.Point(6, 12);
            this.label380.Name = "label380";
            this.label380.Size = new System.Drawing.Size(23, 13);
            this.label380.TabIndex = 82;
            this.label380.Text = "ИО";
            // 
            // label381
            // 
            this.label381.AutoSize = true;
            this.label381.Location = new System.Drawing.Point(47, 107);
            this.label381.Name = "label381";
            this.label381.Size = new System.Drawing.Size(23, 13);
            this.label381.TabIndex = 70;
            this.label381.Text = "Z 5";
            // 
            // label382
            // 
            this.label382.AutoSize = true;
            this.label382.Location = new System.Drawing.Point(47, 31);
            this.label382.Name = "label382";
            this.label382.Size = new System.Drawing.Size(23, 13);
            this.label382.TabIndex = 70;
            this.label382.Text = "Z 1";
            // 
            // _r4IoLed1
            // 
            this._r4IoLed1.Location = new System.Drawing.Point(9, 88);
            this._r4IoLed1.Name = "_r4IoLed1";
            this._r4IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r4IoLed1.State = BEMN.Forms.LedState.Off;
            this._r4IoLed1.TabIndex = 80;
            // 
            // _r5Led1
            // 
            this._r5Led1.Location = new System.Drawing.Point(28, 107);
            this._r5Led1.Name = "_r5Led1";
            this._r5Led1.Size = new System.Drawing.Size(13, 13);
            this._r5Led1.State = BEMN.Forms.LedState.Off;
            this._r5Led1.TabIndex = 72;
            // 
            // _r1Led1
            // 
            this._r1Led1.Location = new System.Drawing.Point(28, 31);
            this._r1Led1.Name = "_r1Led1";
            this._r1Led1.Size = new System.Drawing.Size(13, 13);
            this._r1Led1.State = BEMN.Forms.LedState.Off;
            this._r1Led1.TabIndex = 72;
            // 
            // _r3IoLed1
            // 
            this._r3IoLed1.Location = new System.Drawing.Point(9, 69);
            this._r3IoLed1.Name = "_r3IoLed1";
            this._r3IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r3IoLed1.State = BEMN.Forms.LedState.Off;
            this._r3IoLed1.TabIndex = 77;
            // 
            // label383
            // 
            this.label383.AutoSize = true;
            this.label383.Location = new System.Drawing.Point(47, 126);
            this.label383.Name = "label383";
            this.label383.Size = new System.Drawing.Size(23, 13);
            this.label383.TabIndex = 73;
            this.label383.Text = "Z 6";
            // 
            // label384
            // 
            this.label384.AutoSize = true;
            this.label384.Location = new System.Drawing.Point(47, 50);
            this.label384.Name = "label384";
            this.label384.Size = new System.Drawing.Size(23, 13);
            this.label384.TabIndex = 73;
            this.label384.Text = "Z 2";
            // 
            // _r6IoLed1
            // 
            this._r6IoLed1.Location = new System.Drawing.Point(9, 126);
            this._r6IoLed1.Name = "_r6IoLed1";
            this._r6IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r6IoLed1.State = BEMN.Forms.LedState.Off;
            this._r6IoLed1.TabIndex = 74;
            // 
            // _r2IoLed1
            // 
            this._r2IoLed1.Location = new System.Drawing.Point(9, 50);
            this._r2IoLed1.Name = "_r2IoLed1";
            this._r2IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r2IoLed1.State = BEMN.Forms.LedState.Off;
            this._r2IoLed1.TabIndex = 74;
            // 
            // _r6Led1
            // 
            this._r6Led1.Location = new System.Drawing.Point(28, 126);
            this._r6Led1.Name = "_r6Led1";
            this._r6Led1.Size = new System.Drawing.Size(13, 13);
            this._r6Led1.State = BEMN.Forms.LedState.Off;
            this._r6Led1.TabIndex = 75;
            // 
            // _r2Led1
            // 
            this._r2Led1.Location = new System.Drawing.Point(28, 50);
            this._r2Led1.Name = "_r2Led1";
            this._r2Led1.Size = new System.Drawing.Size(13, 13);
            this._r2Led1.State = BEMN.Forms.LedState.Off;
            this._r2Led1.TabIndex = 75;
            // 
            // _r4Led1
            // 
            this._r4Led1.Location = new System.Drawing.Point(28, 88);
            this._r4Led1.Name = "_r4Led1";
            this._r4Led1.Size = new System.Drawing.Size(13, 13);
            this._r4Led1.State = BEMN.Forms.LedState.Off;
            this._r4Led1.TabIndex = 81;
            // 
            // label388
            // 
            this.label388.AutoSize = true;
            this.label388.Location = new System.Drawing.Point(47, 69);
            this.label388.Name = "label388";
            this.label388.Size = new System.Drawing.Size(23, 13);
            this.label388.TabIndex = 76;
            this.label388.Text = "Z 3";
            // 
            // label390
            // 
            this.label390.AutoSize = true;
            this.label390.Location = new System.Drawing.Point(48, 88);
            this.label390.Name = "label390";
            this.label390.Size = new System.Drawing.Size(23, 13);
            this.label390.TabIndex = 79;
            this.label390.Text = "Z 4";
            // 
            // _r3Led1
            // 
            this._r3Led1.Location = new System.Drawing.Point(28, 69);
            this._r3Led1.Name = "_r3Led1";
            this._r3Led1.Size = new System.Drawing.Size(13, 13);
            this._r3Led1.State = BEMN.Forms.LedState.Off;
            this._r3Led1.TabIndex = 78;
            // 
            // _r5IoLed1
            // 
            this._r5IoLed1.Location = new System.Drawing.Point(9, 107);
            this._r5IoLed1.Name = "_r5IoLed1";
            this._r5IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r5IoLed1.State = BEMN.Forms.LedState.Off;
            this._r5IoLed1.TabIndex = 71;
            // 
            // _r1IoLed1
            // 
            this._r1IoLed1.Location = new System.Drawing.Point(9, 31);
            this._r1IoLed1.Name = "_r1IoLed1";
            this._r1IoLed1.Size = new System.Drawing.Size(13, 13);
            this._r1IoLed1.State = BEMN.Forms.LedState.Off;
            this._r1IoLed1.TabIndex = 71;
            // 
            // groupBox56
            // 
            this.groupBox56.Controls.Add(this.label414);
            this.groupBox56.Controls.Add(this._iS81);
            this.groupBox56.Controls.Add(this._iS8Io1);
            this.groupBox56.Controls.Add(this.label402);
            this.groupBox56.Controls.Add(this._iS71);
            this.groupBox56.Controls.Add(this._iS7Io1);
            this.groupBox56.Controls.Add(this.label391);
            this.groupBox56.Controls.Add(this.label392);
            this.groupBox56.Controls.Add(this._iS11);
            this.groupBox56.Controls.Add(this.label393);
            this.groupBox56.Controls.Add(this._iS21);
            this.groupBox56.Controls.Add(this._iS1Io1);
            this.groupBox56.Controls.Add(this.label394);
            this.groupBox56.Controls.Add(this._iS31);
            this.groupBox56.Controls.Add(this._iS2Io1);
            this.groupBox56.Controls.Add(this.label395);
            this.groupBox56.Controls.Add(this.label396);
            this.groupBox56.Controls.Add(this._iS41);
            this.groupBox56.Controls.Add(this._iS3Io1);
            this.groupBox56.Controls.Add(this.label397);
            this.groupBox56.Controls.Add(this._iS51);
            this.groupBox56.Controls.Add(this._iS4Io1);
            this.groupBox56.Controls.Add(this.label398);
            this.groupBox56.Controls.Add(this._iS61);
            this.groupBox56.Controls.Add(this._iS5Io1);
            this.groupBox56.Controls.Add(this._iS6Io1);
            this.groupBox56.Location = new System.Drawing.Point(8, 252);
            this.groupBox56.Name = "groupBox56";
            this.groupBox56.Size = new System.Drawing.Size(84, 171);
            this.groupBox56.TabIndex = 39;
            this.groupBox56.TabStop = false;
            this.groupBox56.Text = "Защиты I*";
            // 
            // label414
            // 
            this.label414.AutoSize = true;
            this.label414.Location = new System.Drawing.Point(47, 149);
            this.label414.Name = "label414";
            this.label414.Size = new System.Drawing.Size(26, 13);
            this.label414.TabIndex = 71;
            this.label414.Text = "I*>8";
            // 
            // _iS81
            // 
            this._iS81.Location = new System.Drawing.Point(28, 149);
            this._iS81.Name = "_iS81";
            this._iS81.Size = new System.Drawing.Size(13, 13);
            this._iS81.State = BEMN.Forms.LedState.Off;
            this._iS81.TabIndex = 72;
            // 
            // _iS8Io1
            // 
            this._iS8Io1.Location = new System.Drawing.Point(9, 149);
            this._iS8Io1.Name = "_iS8Io1";
            this._iS8Io1.Size = new System.Drawing.Size(13, 13);
            this._iS8Io1.State = BEMN.Forms.LedState.Off;
            this._iS8Io1.TabIndex = 73;
            // 
            // label402
            // 
            this.label402.AutoSize = true;
            this.label402.Location = new System.Drawing.Point(47, 133);
            this.label402.Name = "label402";
            this.label402.Size = new System.Drawing.Size(26, 13);
            this.label402.TabIndex = 68;
            this.label402.Text = "I*>7";
            // 
            // _iS71
            // 
            this._iS71.Location = new System.Drawing.Point(28, 133);
            this._iS71.Name = "_iS71";
            this._iS71.Size = new System.Drawing.Size(13, 13);
            this._iS71.State = BEMN.Forms.LedState.Off;
            this._iS71.TabIndex = 69;
            // 
            // _iS7Io1
            // 
            this._iS7Io1.Location = new System.Drawing.Point(9, 133);
            this._iS7Io1.Name = "_iS7Io1";
            this._iS7Io1.Size = new System.Drawing.Size(13, 13);
            this._iS7Io1.State = BEMN.Forms.LedState.Off;
            this._iS7Io1.TabIndex = 70;
            // 
            // label391
            // 
            this.label391.AutoSize = true;
            this.label391.ForeColor = System.Drawing.Color.Blue;
            this.label391.Location = new System.Drawing.Point(25, 16);
            this.label391.Name = "label391";
            this.label391.Size = new System.Drawing.Size(32, 13);
            this.label391.TabIndex = 67;
            this.label391.Text = "Сраб";
            // 
            // label392
            // 
            this.label392.AutoSize = true;
            this.label392.Location = new System.Drawing.Point(47, 35);
            this.label392.Name = "label392";
            this.label392.Size = new System.Drawing.Size(26, 13);
            this.label392.TabIndex = 16;
            this.label392.Text = "I*>1";
            // 
            // _iS11
            // 
            this._iS11.Location = new System.Drawing.Point(28, 35);
            this._iS11.Name = "_iS11";
            this._iS11.Size = new System.Drawing.Size(13, 13);
            this._iS11.State = BEMN.Forms.LedState.Off;
            this._iS11.TabIndex = 17;
            // 
            // label393
            // 
            this.label393.AutoSize = true;
            this.label393.Location = new System.Drawing.Point(47, 52);
            this.label393.Name = "label393";
            this.label393.Size = new System.Drawing.Size(26, 13);
            this.label393.TabIndex = 18;
            this.label393.Text = "I*>2";
            // 
            // _iS21
            // 
            this._iS21.Location = new System.Drawing.Point(28, 52);
            this._iS21.Name = "_iS21";
            this._iS21.Size = new System.Drawing.Size(13, 13);
            this._iS21.State = BEMN.Forms.LedState.Off;
            this._iS21.TabIndex = 19;
            // 
            // _iS1Io1
            // 
            this._iS1Io1.Location = new System.Drawing.Point(9, 35);
            this._iS1Io1.Name = "_iS1Io1";
            this._iS1Io1.Size = new System.Drawing.Size(13, 13);
            this._iS1Io1.State = BEMN.Forms.LedState.Off;
            this._iS1Io1.TabIndex = 17;
            // 
            // label394
            // 
            this.label394.AutoSize = true;
            this.label394.Location = new System.Drawing.Point(47, 69);
            this.label394.Name = "label394";
            this.label394.Size = new System.Drawing.Size(26, 13);
            this.label394.TabIndex = 20;
            this.label394.Text = "I*>3";
            // 
            // _iS31
            // 
            this._iS31.Location = new System.Drawing.Point(28, 69);
            this._iS31.Name = "_iS31";
            this._iS31.Size = new System.Drawing.Size(13, 13);
            this._iS31.State = BEMN.Forms.LedState.Off;
            this._iS31.TabIndex = 21;
            // 
            // _iS2Io1
            // 
            this._iS2Io1.Location = new System.Drawing.Point(9, 52);
            this._iS2Io1.Name = "_iS2Io1";
            this._iS2Io1.Size = new System.Drawing.Size(13, 13);
            this._iS2Io1.State = BEMN.Forms.LedState.Off;
            this._iS2Io1.TabIndex = 19;
            // 
            // label395
            // 
            this.label395.AutoSize = true;
            this.label395.ForeColor = System.Drawing.Color.Red;
            this.label395.Location = new System.Drawing.Point(6, 16);
            this.label395.Name = "label395";
            this.label395.Size = new System.Drawing.Size(23, 13);
            this.label395.TabIndex = 66;
            this.label395.Text = "ИО";
            // 
            // label396
            // 
            this.label396.AutoSize = true;
            this.label396.Location = new System.Drawing.Point(47, 85);
            this.label396.Name = "label396";
            this.label396.Size = new System.Drawing.Size(26, 13);
            this.label396.TabIndex = 22;
            this.label396.Text = "I*>4";
            // 
            // _iS41
            // 
            this._iS41.Location = new System.Drawing.Point(28, 85);
            this._iS41.Name = "_iS41";
            this._iS41.Size = new System.Drawing.Size(13, 13);
            this._iS41.State = BEMN.Forms.LedState.Off;
            this._iS41.TabIndex = 23;
            // 
            // _iS3Io1
            // 
            this._iS3Io1.Location = new System.Drawing.Point(9, 69);
            this._iS3Io1.Name = "_iS3Io1";
            this._iS3Io1.Size = new System.Drawing.Size(13, 13);
            this._iS3Io1.State = BEMN.Forms.LedState.Off;
            this._iS3Io1.TabIndex = 21;
            // 
            // label397
            // 
            this.label397.AutoSize = true;
            this.label397.Location = new System.Drawing.Point(47, 101);
            this.label397.Name = "label397";
            this.label397.Size = new System.Drawing.Size(26, 13);
            this.label397.TabIndex = 24;
            this.label397.Text = "I*>5";
            // 
            // _iS51
            // 
            this._iS51.Location = new System.Drawing.Point(28, 101);
            this._iS51.Name = "_iS51";
            this._iS51.Size = new System.Drawing.Size(13, 13);
            this._iS51.State = BEMN.Forms.LedState.Off;
            this._iS51.TabIndex = 25;
            // 
            // _iS4Io1
            // 
            this._iS4Io1.Location = new System.Drawing.Point(9, 85);
            this._iS4Io1.Name = "_iS4Io1";
            this._iS4Io1.Size = new System.Drawing.Size(13, 13);
            this._iS4Io1.State = BEMN.Forms.LedState.Off;
            this._iS4Io1.TabIndex = 23;
            // 
            // label398
            // 
            this.label398.AutoSize = true;
            this.label398.Location = new System.Drawing.Point(47, 117);
            this.label398.Name = "label398";
            this.label398.Size = new System.Drawing.Size(26, 13);
            this.label398.TabIndex = 26;
            this.label398.Text = "I*>6";
            // 
            // _iS61
            // 
            this._iS61.Location = new System.Drawing.Point(28, 117);
            this._iS61.Name = "_iS61";
            this._iS61.Size = new System.Drawing.Size(13, 13);
            this._iS61.State = BEMN.Forms.LedState.Off;
            this._iS61.TabIndex = 27;
            // 
            // _iS5Io1
            // 
            this._iS5Io1.Location = new System.Drawing.Point(9, 101);
            this._iS5Io1.Name = "_iS5Io1";
            this._iS5Io1.Size = new System.Drawing.Size(13, 13);
            this._iS5Io1.State = BEMN.Forms.LedState.Off;
            this._iS5Io1.TabIndex = 25;
            // 
            // _iS6Io1
            // 
            this._iS6Io1.Location = new System.Drawing.Point(9, 117);
            this._iS6Io1.Name = "_iS6Io1";
            this._iS6Io1.Size = new System.Drawing.Size(13, 13);
            this._iS6Io1.State = BEMN.Forms.LedState.Off;
            this._iS6Io1.TabIndex = 27;
            // 
            // groupBox57
            // 
            this.groupBox57.Controls.Add(this.label399);
            this.groupBox57.Controls.Add(this.label400);
            this.groupBox57.Controls.Add(this._i8Io1);
            this.groupBox57.Controls.Add(this._i6Io1);
            this.groupBox57.Controls.Add(this._i81);
            this.groupBox57.Controls.Add(this._i5Io1);
            this.groupBox57.Controls.Add(this._i61);
            this.groupBox57.Controls.Add(this.label130);
            this.groupBox57.Controls.Add(this.label403);
            this.groupBox57.Controls.Add(this._i4Io1);
            this.groupBox57.Controls.Add(this._i51);
            this.groupBox57.Controls.Add(this.label404);
            this.groupBox57.Controls.Add(this._i3Io1);
            this.groupBox57.Controls.Add(this._i41);
            this.groupBox57.Controls.Add(this.label405);
            this.groupBox57.Controls.Add(this._i2Io1);
            this.groupBox57.Controls.Add(this._i31);
            this.groupBox57.Controls.Add(this.label406);
            this.groupBox57.Controls.Add(this._i1Io1);
            this.groupBox57.Controls.Add(this._i21);
            this.groupBox57.Controls.Add(this.label407);
            this.groupBox57.Controls.Add(this._i11);
            this.groupBox57.Controls.Add(this.label408);
            this.groupBox57.Location = new System.Drawing.Point(8, 106);
            this.groupBox57.Name = "groupBox57";
            this.groupBox57.Size = new System.Drawing.Size(84, 144);
            this.groupBox57.TabIndex = 38;
            this.groupBox57.TabStop = false;
            this.groupBox57.Text = "Защиты I";
            // 
            // label399
            // 
            this.label399.AutoSize = true;
            this.label399.ForeColor = System.Drawing.Color.Blue;
            this.label399.Location = new System.Drawing.Point(25, 16);
            this.label399.Name = "label399";
            this.label399.Size = new System.Drawing.Size(32, 13);
            this.label399.TabIndex = 65;
            this.label399.Text = "Сраб";
            // 
            // label400
            // 
            this.label400.AutoSize = true;
            this.label400.ForeColor = System.Drawing.Color.Red;
            this.label400.Location = new System.Drawing.Point(6, 16);
            this.label400.Name = "label400";
            this.label400.Size = new System.Drawing.Size(23, 13);
            this.label400.TabIndex = 64;
            this.label400.Text = "ИО";
            // 
            // _i8Io1
            // 
            this._i8Io1.Location = new System.Drawing.Point(9, 123);
            this._i8Io1.Name = "_i8Io1";
            this._i8Io1.Size = new System.Drawing.Size(13, 13);
            this._i8Io1.State = BEMN.Forms.LedState.Off;
            this._i8Io1.TabIndex = 15;
            // 
            // _i6Io1
            // 
            this._i6Io1.Location = new System.Drawing.Point(9, 108);
            this._i6Io1.Name = "_i6Io1";
            this._i6Io1.Size = new System.Drawing.Size(13, 13);
            this._i6Io1.State = BEMN.Forms.LedState.Off;
            this._i6Io1.TabIndex = 11;
            // 
            // _i81
            // 
            this._i81.Location = new System.Drawing.Point(28, 123);
            this._i81.Name = "_i81";
            this._i81.Size = new System.Drawing.Size(13, 13);
            this._i81.State = BEMN.Forms.LedState.Off;
            this._i81.TabIndex = 15;
            // 
            // _i5Io1
            // 
            this._i5Io1.Location = new System.Drawing.Point(9, 93);
            this._i5Io1.Name = "_i5Io1";
            this._i5Io1.Size = new System.Drawing.Size(13, 13);
            this._i5Io1.State = BEMN.Forms.LedState.Off;
            this._i5Io1.TabIndex = 9;
            // 
            // _i61
            // 
            this._i61.Location = new System.Drawing.Point(28, 108);
            this._i61.Name = "_i61";
            this._i61.Size = new System.Drawing.Size(13, 13);
            this._i61.State = BEMN.Forms.LedState.Off;
            this._i61.TabIndex = 11;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(47, 123);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(16, 13);
            this.label130.TabIndex = 10;
            this.label130.Text = "I<";
            // 
            // label403
            // 
            this.label403.AutoSize = true;
            this.label403.Location = new System.Drawing.Point(47, 108);
            this.label403.Name = "label403";
            this.label403.Size = new System.Drawing.Size(22, 13);
            this.label403.TabIndex = 10;
            this.label403.Text = "I>6";
            // 
            // _i4Io1
            // 
            this._i4Io1.Location = new System.Drawing.Point(9, 78);
            this._i4Io1.Name = "_i4Io1";
            this._i4Io1.Size = new System.Drawing.Size(13, 13);
            this._i4Io1.State = BEMN.Forms.LedState.Off;
            this._i4Io1.TabIndex = 7;
            // 
            // _i51
            // 
            this._i51.Location = new System.Drawing.Point(28, 93);
            this._i51.Name = "_i51";
            this._i51.Size = new System.Drawing.Size(13, 13);
            this._i51.State = BEMN.Forms.LedState.Off;
            this._i51.TabIndex = 9;
            // 
            // label404
            // 
            this.label404.AutoSize = true;
            this.label404.Location = new System.Drawing.Point(47, 93);
            this.label404.Name = "label404";
            this.label404.Size = new System.Drawing.Size(22, 13);
            this.label404.TabIndex = 8;
            this.label404.Text = "I>5";
            // 
            // _i3Io1
            // 
            this._i3Io1.Location = new System.Drawing.Point(9, 63);
            this._i3Io1.Name = "_i3Io1";
            this._i3Io1.Size = new System.Drawing.Size(13, 13);
            this._i3Io1.State = BEMN.Forms.LedState.Off;
            this._i3Io1.TabIndex = 5;
            // 
            // _i41
            // 
            this._i41.Location = new System.Drawing.Point(28, 78);
            this._i41.Name = "_i41";
            this._i41.Size = new System.Drawing.Size(13, 13);
            this._i41.State = BEMN.Forms.LedState.Off;
            this._i41.TabIndex = 7;
            // 
            // label405
            // 
            this.label405.AutoSize = true;
            this.label405.Location = new System.Drawing.Point(48, 78);
            this.label405.Name = "label405";
            this.label405.Size = new System.Drawing.Size(22, 13);
            this.label405.TabIndex = 6;
            this.label405.Text = "I>4";
            // 
            // _i2Io1
            // 
            this._i2Io1.Location = new System.Drawing.Point(9, 48);
            this._i2Io1.Name = "_i2Io1";
            this._i2Io1.Size = new System.Drawing.Size(13, 13);
            this._i2Io1.State = BEMN.Forms.LedState.Off;
            this._i2Io1.TabIndex = 3;
            // 
            // _i31
            // 
            this._i31.Location = new System.Drawing.Point(28, 63);
            this._i31.Name = "_i31";
            this._i31.Size = new System.Drawing.Size(13, 13);
            this._i31.State = BEMN.Forms.LedState.Off;
            this._i31.TabIndex = 5;
            // 
            // label406
            // 
            this.label406.AutoSize = true;
            this.label406.Location = new System.Drawing.Point(47, 63);
            this.label406.Name = "label406";
            this.label406.Size = new System.Drawing.Size(22, 13);
            this.label406.TabIndex = 4;
            this.label406.Text = "I>3";
            // 
            // _i1Io1
            // 
            this._i1Io1.Location = new System.Drawing.Point(9, 33);
            this._i1Io1.Name = "_i1Io1";
            this._i1Io1.Size = new System.Drawing.Size(13, 13);
            this._i1Io1.State = BEMN.Forms.LedState.Off;
            this._i1Io1.TabIndex = 1;
            // 
            // _i21
            // 
            this._i21.Location = new System.Drawing.Point(28, 48);
            this._i21.Name = "_i21";
            this._i21.Size = new System.Drawing.Size(13, 13);
            this._i21.State = BEMN.Forms.LedState.Off;
            this._i21.TabIndex = 3;
            // 
            // label407
            // 
            this.label407.AutoSize = true;
            this.label407.Location = new System.Drawing.Point(47, 48);
            this.label407.Name = "label407";
            this.label407.Size = new System.Drawing.Size(22, 13);
            this.label407.TabIndex = 2;
            this.label407.Text = "I>2";
            // 
            // _i11
            // 
            this._i11.Location = new System.Drawing.Point(28, 33);
            this._i11.Name = "_i11";
            this._i11.Size = new System.Drawing.Size(13, 13);
            this._i11.State = BEMN.Forms.LedState.Off;
            this._i11.TabIndex = 1;
            // 
            // label408
            // 
            this.label408.AutoSize = true;
            this.label408.Location = new System.Drawing.Point(47, 33);
            this.label408.Name = "label408";
            this.label408.Size = new System.Drawing.Size(22, 13);
            this.label408.TabIndex = 0;
            this.label408.Text = "I>1";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._vz16);
            this.groupBox12.Controls.Add(this.label111);
            this.groupBox12.Controls.Add(this._vz15);
            this.groupBox12.Controls.Add(this.label112);
            this.groupBox12.Controls.Add(this._vz14);
            this.groupBox12.Controls.Add(this.label113);
            this.groupBox12.Controls.Add(this._vz13);
            this.groupBox12.Controls.Add(this.label114);
            this.groupBox12.Controls.Add(this._vz12);
            this.groupBox12.Controls.Add(this.label115);
            this.groupBox12.Controls.Add(this._vz11);
            this.groupBox12.Controls.Add(this.label116);
            this.groupBox12.Controls.Add(this._vz10);
            this.groupBox12.Controls.Add(this.label117);
            this.groupBox12.Controls.Add(this._vz9);
            this.groupBox12.Controls.Add(this.label118);
            this.groupBox12.Controls.Add(this._vz8);
            this.groupBox12.Controls.Add(this.label119);
            this.groupBox12.Controls.Add(this._vz7);
            this.groupBox12.Controls.Add(this.label120);
            this.groupBox12.Controls.Add(this._vz6);
            this.groupBox12.Controls.Add(this.label121);
            this.groupBox12.Controls.Add(this._vz5);
            this.groupBox12.Controls.Add(this.label122);
            this.groupBox12.Controls.Add(this._vz4);
            this.groupBox12.Controls.Add(this.label123);
            this.groupBox12.Controls.Add(this._vz3);
            this.groupBox12.Controls.Add(this.label124);
            this.groupBox12.Controls.Add(this._vz2);
            this.groupBox12.Controls.Add(this.label125);
            this.groupBox12.Controls.Add(this._vz1);
            this.groupBox12.Controls.Add(this.label126);
            this.groupBox12.Location = new System.Drawing.Point(196, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(172, 194);
            this.groupBox12.TabIndex = 37;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Внешние защиты";
            // 
            // _vz16
            // 
            this._vz16.Location = new System.Drawing.Point(84, 152);
            this._vz16.Name = "_vz16";
            this._vz16.Size = new System.Drawing.Size(13, 13);
            this._vz16.State = BEMN.Forms.LedState.Off;
            this._vz16.TabIndex = 31;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(103, 152);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(56, 13);
            this.label111.TabIndex = 30;
            this.label111.Text = "ВНЕШ. 16";
            // 
            // _vz15
            // 
            this._vz15.Location = new System.Drawing.Point(84, 133);
            this._vz15.Name = "_vz15";
            this._vz15.Size = new System.Drawing.Size(13, 13);
            this._vz15.State = BEMN.Forms.LedState.Off;
            this._vz15.TabIndex = 29;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(103, 133);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(56, 13);
            this.label112.TabIndex = 28;
            this.label112.Text = "ВНЕШ. 15";
            // 
            // _vz14
            // 
            this._vz14.Location = new System.Drawing.Point(84, 114);
            this._vz14.Name = "_vz14";
            this._vz14.Size = new System.Drawing.Size(13, 13);
            this._vz14.State = BEMN.Forms.LedState.Off;
            this._vz14.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(103, 114);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(56, 13);
            this.label113.TabIndex = 26;
            this.label113.Text = "ВНЕШ. 14";
            // 
            // _vz13
            // 
            this._vz13.Location = new System.Drawing.Point(84, 95);
            this._vz13.Name = "_vz13";
            this._vz13.Size = new System.Drawing.Size(13, 13);
            this._vz13.State = BEMN.Forms.LedState.Off;
            this._vz13.TabIndex = 25;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(103, 95);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(56, 13);
            this.label114.TabIndex = 24;
            this.label114.Text = "ВНЕШ. 13";
            // 
            // _vz12
            // 
            this._vz12.Location = new System.Drawing.Point(84, 76);
            this._vz12.Name = "_vz12";
            this._vz12.Size = new System.Drawing.Size(13, 13);
            this._vz12.State = BEMN.Forms.LedState.Off;
            this._vz12.TabIndex = 23;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(103, 76);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(56, 13);
            this.label115.TabIndex = 22;
            this.label115.Text = "ВНЕШ. 12";
            // 
            // _vz11
            // 
            this._vz11.Location = new System.Drawing.Point(84, 57);
            this._vz11.Name = "_vz11";
            this._vz11.Size = new System.Drawing.Size(13, 13);
            this._vz11.State = BEMN.Forms.LedState.Off;
            this._vz11.TabIndex = 21;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(103, 57);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(56, 13);
            this.label116.TabIndex = 20;
            this.label116.Text = "ВНЕШ. 11";
            // 
            // _vz10
            // 
            this._vz10.Location = new System.Drawing.Point(84, 38);
            this._vz10.Name = "_vz10";
            this._vz10.Size = new System.Drawing.Size(13, 13);
            this._vz10.State = BEMN.Forms.LedState.Off;
            this._vz10.TabIndex = 19;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(103, 38);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(56, 13);
            this.label117.TabIndex = 18;
            this.label117.Text = "ВНЕШ. 10";
            // 
            // _vz9
            // 
            this._vz9.Location = new System.Drawing.Point(84, 19);
            this._vz9.Name = "_vz9";
            this._vz9.Size = new System.Drawing.Size(13, 13);
            this._vz9.State = BEMN.Forms.LedState.Off;
            this._vz9.TabIndex = 17;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(103, 19);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "ВНЕШ. 9";
            // 
            // _vz8
            // 
            this._vz8.Location = new System.Drawing.Point(6, 152);
            this._vz8.Name = "_vz8";
            this._vz8.Size = new System.Drawing.Size(13, 13);
            this._vz8.State = BEMN.Forms.LedState.Off;
            this._vz8.TabIndex = 15;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(25, 152);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(50, 13);
            this.label119.TabIndex = 14;
            this.label119.Text = "ВНЕШ. 8";
            // 
            // _vz7
            // 
            this._vz7.Location = new System.Drawing.Point(6, 133);
            this._vz7.Name = "_vz7";
            this._vz7.Size = new System.Drawing.Size(13, 13);
            this._vz7.State = BEMN.Forms.LedState.Off;
            this._vz7.TabIndex = 13;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(25, 133);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(50, 13);
            this.label120.TabIndex = 12;
            this.label120.Text = "ВНЕШ. 7";
            // 
            // _vz6
            // 
            this._vz6.Location = new System.Drawing.Point(6, 114);
            this._vz6.Name = "_vz6";
            this._vz6.Size = new System.Drawing.Size(13, 13);
            this._vz6.State = BEMN.Forms.LedState.Off;
            this._vz6.TabIndex = 11;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(25, 114);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(50, 13);
            this.label121.TabIndex = 10;
            this.label121.Text = "ВНЕШ. 6";
            // 
            // _vz5
            // 
            this._vz5.Location = new System.Drawing.Point(6, 95);
            this._vz5.Name = "_vz5";
            this._vz5.Size = new System.Drawing.Size(13, 13);
            this._vz5.State = BEMN.Forms.LedState.Off;
            this._vz5.TabIndex = 9;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(25, 95);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(50, 13);
            this.label122.TabIndex = 8;
            this.label122.Text = "ВНЕШ. 5";
            // 
            // _vz4
            // 
            this._vz4.Location = new System.Drawing.Point(6, 76);
            this._vz4.Name = "_vz4";
            this._vz4.Size = new System.Drawing.Size(13, 13);
            this._vz4.State = BEMN.Forms.LedState.Off;
            this._vz4.TabIndex = 7;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(25, 76);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(50, 13);
            this.label123.TabIndex = 6;
            this.label123.Text = "ВНЕШ. 4";
            // 
            // _vz3
            // 
            this._vz3.Location = new System.Drawing.Point(6, 57);
            this._vz3.Name = "_vz3";
            this._vz3.Size = new System.Drawing.Size(13, 13);
            this._vz3.State = BEMN.Forms.LedState.Off;
            this._vz3.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(25, 57);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(50, 13);
            this.label124.TabIndex = 4;
            this.label124.Text = "ВНЕШ. 3";
            // 
            // _vz2
            // 
            this._vz2.Location = new System.Drawing.Point(6, 38);
            this._vz2.Name = "_vz2";
            this._vz2.Size = new System.Drawing.Size(13, 13);
            this._vz2.State = BEMN.Forms.LedState.Off;
            this._vz2.TabIndex = 3;
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(25, 38);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(50, 13);
            this.label125.TabIndex = 2;
            this.label125.Text = "ВНЕШ. 2";
            // 
            // _vz1
            // 
            this._vz1.Location = new System.Drawing.Point(6, 19);
            this._vz1.Name = "_vz1";
            this._vz1.Size = new System.Drawing.Size(13, 13);
            this._vz1.State = BEMN.Forms.LedState.Off;
            this._vz1.TabIndex = 1;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(25, 19);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(50, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "ВНЕШ. 1";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this.groupBox39);
            this.groupBox38.Controls.Add(this._fSpl4);
            this.groupBox38.Controls.Add(this.label320);
            this.groupBox38.Controls.Add(this.label319);
            this.groupBox38.Location = new System.Drawing.Point(772, 6);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(122, 127);
            this.groupBox38.TabIndex = 36;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Ошибки СПЛ";
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this._fSpl1);
            this.groupBox39.Controls.Add(this.label315);
            this.groupBox39.Controls.Add(this.label316);
            this.groupBox39.Controls.Add(this._fSpl2);
            this.groupBox39.Controls.Add(this.label317);
            this.groupBox39.Controls.Add(this._fSpl3);
            this.groupBox39.Location = new System.Drawing.Point(6, 44);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(110, 77);
            this.groupBox39.TabIndex = 32;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Ошибки CRC";
            // 
            // _fSpl1
            // 
            this._fSpl1.Location = new System.Drawing.Point(6, 19);
            this._fSpl1.Name = "_fSpl1";
            this._fSpl1.Size = new System.Drawing.Size(13, 13);
            this._fSpl1.State = BEMN.Forms.LedState.Off;
            this._fSpl1.TabIndex = 25;
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Location = new System.Drawing.Point(25, 19);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(54, 13);
            this.label315.TabIndex = 24;
            this.label315.Text = "Констант";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Location = new System.Drawing.Point(25, 38);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(70, 13);
            this.label316.TabIndex = 26;
            this.label316.Text = "Разрешения";
            // 
            // _fSpl2
            // 
            this._fSpl2.Location = new System.Drawing.Point(6, 38);
            this._fSpl2.Name = "_fSpl2";
            this._fSpl2.Size = new System.Drawing.Size(13, 13);
            this._fSpl2.State = BEMN.Forms.LedState.Off;
            this._fSpl2.TabIndex = 27;
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Location = new System.Drawing.Point(25, 57);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(68, 13);
            this.label317.TabIndex = 28;
            this.label317.Text = "Программы";
            // 
            // _fSpl3
            // 
            this._fSpl3.Location = new System.Drawing.Point(6, 57);
            this._fSpl3.Name = "_fSpl3";
            this._fSpl3.Size = new System.Drawing.Size(13, 13);
            this._fSpl3.State = BEMN.Forms.LedState.Off;
            this._fSpl3.TabIndex = 29;
            // 
            // _fSpl4
            // 
            this._fSpl4.Location = new System.Drawing.Point(4, 19);
            this._fSpl4.Name = "_fSpl4";
            this._fSpl4.Size = new System.Drawing.Size(13, 13);
            this._fSpl4.State = BEMN.Forms.LedState.Off;
            this._fSpl4.TabIndex = 31;
            // 
            // label320
            // 
            this.label320.AutoSize = true;
            this.label320.Location = new System.Drawing.Point(17, 28);
            this.label320.Name = "label320";
            this.label320.Size = new System.Drawing.Size(66, 13);
            this.label320.TabIndex = 30;
            this.label320.Text = "программы";
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Location = new System.Drawing.Point(16, 14);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(105, 13);
            this.label319.TabIndex = 30;
            this.label319.Text = "В ходе выполнения";
            // 
            // groupBox49
            // 
            this.groupBox49.Controls.Add(this.label347);
            this.groupBox49.Controls.Add(this._faultDisable2);
            this.groupBox49.Controls.Add(this.label346);
            this.groupBox49.Controls.Add(this.label345);
            this.groupBox49.Controls.Add(this.label311);
            this.groupBox49.Controls.Add(this.label220);
            this.groupBox49.Controls.Add(this.label98);
            this.groupBox49.Controls.Add(this.label22);
            this.groupBox49.Controls.Add(this._faultDisable1);
            this.groupBox49.Controls.Add(this._faultSwithON);
            this.groupBox49.Controls.Add(this._faultManage);
            this.groupBox49.Controls.Add(this._faultOtkaz);
            this.groupBox49.Controls.Add(this._faultBlockCon);
            this.groupBox49.Controls.Add(this._faultOut);
            this.groupBox49.Location = new System.Drawing.Point(726, 206);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(177, 170);
            this.groupBox49.TabIndex = 35;
            this.groupBox49.TabStop = false;
            this.groupBox49.Text = "Неисправности выключателя";
            // 
            // label347
            // 
            this.label347.AutoSize = true;
            this.label347.Location = new System.Drawing.Point(25, 132);
            this.label347.Name = "label347";
            this.label347.Size = new System.Drawing.Size(105, 13);
            this.label347.TabIndex = 13;
            this.label347.Text = "Цепи отключения 2";
            // 
            // _faultDisable2
            // 
            this._faultDisable2.Location = new System.Drawing.Point(6, 132);
            this._faultDisable2.Name = "_faultDisable2";
            this._faultDisable2.Size = new System.Drawing.Size(13, 13);
            this._faultDisable2.State = BEMN.Forms.LedState.Off;
            this._faultDisable2.TabIndex = 12;
            // 
            // label346
            // 
            this.label346.AutoSize = true;
            this.label346.Location = new System.Drawing.Point(25, 113);
            this.label346.Name = "label346";
            this.label346.Size = new System.Drawing.Size(105, 13);
            this.label346.TabIndex = 11;
            this.label346.Text = "Цепи отключения 1";
            // 
            // label345
            // 
            this.label345.AutoSize = true;
            this.label345.Location = new System.Drawing.Point(25, 94);
            this.label345.Name = "label345";
            this.label345.Size = new System.Drawing.Size(91, 13);
            this.label345.TabIndex = 10;
            this.label345.Text = "Цепи включения";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Location = new System.Drawing.Point(25, 56);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(69, 13);
            this.label311.TabIndex = 9;
            this.label311.Text = "Управления";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(25, 75);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(109, 13);
            this.label220.TabIndex = 8;
            this.label220.Text = "Отказ выключателя";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(25, 38);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(105, 13);
            this.label98.TabIndex = 7;
            this.label98.Text = "По блок-контактам";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Внешняя ";
            // 
            // _faultDisable1
            // 
            this._faultDisable1.Location = new System.Drawing.Point(6, 113);
            this._faultDisable1.Name = "_faultDisable1";
            this._faultDisable1.Size = new System.Drawing.Size(13, 13);
            this._faultDisable1.State = BEMN.Forms.LedState.Off;
            this._faultDisable1.TabIndex = 5;
            // 
            // _faultSwithON
            // 
            this._faultSwithON.Location = new System.Drawing.Point(6, 94);
            this._faultSwithON.Name = "_faultSwithON";
            this._faultSwithON.Size = new System.Drawing.Size(13, 13);
            this._faultSwithON.State = BEMN.Forms.LedState.Off;
            this._faultSwithON.TabIndex = 4;
            // 
            // _faultManage
            // 
            this._faultManage.Location = new System.Drawing.Point(6, 56);
            this._faultManage.Name = "_faultManage";
            this._faultManage.Size = new System.Drawing.Size(13, 13);
            this._faultManage.State = BEMN.Forms.LedState.Off;
            this._faultManage.TabIndex = 3;
            // 
            // _faultOtkaz
            // 
            this._faultOtkaz.Location = new System.Drawing.Point(6, 75);
            this._faultOtkaz.Name = "_faultOtkaz";
            this._faultOtkaz.Size = new System.Drawing.Size(13, 13);
            this._faultOtkaz.State = BEMN.Forms.LedState.Off;
            this._faultOtkaz.TabIndex = 2;
            // 
            // _faultBlockCon
            // 
            this._faultBlockCon.Location = new System.Drawing.Point(6, 38);
            this._faultBlockCon.Name = "_faultBlockCon";
            this._faultBlockCon.Size = new System.Drawing.Size(13, 13);
            this._faultBlockCon.State = BEMN.Forms.LedState.Off;
            this._faultBlockCon.TabIndex = 1;
            // 
            // _faultOut
            // 
            this._faultOut.Location = new System.Drawing.Point(6, 19);
            this._faultOut.Name = "_faultOut";
            this._faultOut.Size = new System.Drawing.Size(13, 13);
            this._faultOut.State = BEMN.Forms.LedState.Off;
            this._faultOut.TabIndex = 0;
            // 
            // groupBox48
            // 
            this.groupBox48.Controls.Add(this.label228);
            this.groupBox48.Controls.Add(this._UabcLow10);
            this.groupBox48.Controls.Add(this.label313);
            this.groupBox48.Controls.Add(this._freqLow40);
            this.groupBox48.Controls.Add(this._freqHiger60);
            this.groupBox48.Controls.Add(this.label312);
            this.groupBox48.Location = new System.Drawing.Point(540, 376);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(174, 80);
            this.groupBox48.TabIndex = 34;
            this.groupBox48.TabStop = false;
            this.groupBox48.Text = "Неисправности измерения F";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(25, 19);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(64, 13);
            this.label228.TabIndex = 26;
            this.label228.Text = "Uabc < 10B";
            // 
            // _UabcLow10
            // 
            this._UabcLow10.Location = new System.Drawing.Point(6, 19);
            this._UabcLow10.Name = "_UabcLow10";
            this._UabcLow10.Size = new System.Drawing.Size(13, 13);
            this._UabcLow10.State = BEMN.Forms.LedState.Off;
            this._UabcLow10.TabIndex = 27;
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Location = new System.Drawing.Point(25, 38);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(85, 13);
            this.label313.TabIndex = 6;
            this.label313.Text = "Частота > 60Гц";
            // 
            // _freqLow40
            // 
            this._freqLow40.Location = new System.Drawing.Point(6, 57);
            this._freqLow40.Name = "_freqLow40";
            this._freqLow40.Size = new System.Drawing.Size(13, 13);
            this._freqLow40.State = BEMN.Forms.LedState.Off;
            this._freqLow40.TabIndex = 13;
            // 
            // _freqHiger60
            // 
            this._freqHiger60.Location = new System.Drawing.Point(6, 38);
            this._freqHiger60.Name = "_freqHiger60";
            this._freqHiger60.Size = new System.Drawing.Size(13, 13);
            this._freqHiger60.State = BEMN.Forms.LedState.Off;
            this._freqHiger60.TabIndex = 7;
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Location = new System.Drawing.Point(25, 57);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(85, 13);
            this.label312.TabIndex = 12;
            this.label312.Text = "Частота < 40Гц";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._q2Led);
            this.groupBox4.Controls.Add(this.label102);
            this.groupBox4.Controls.Add(this._q1Led);
            this.groupBox4.Controls.Add(this.label104);
            this.groupBox4.Location = new System.Drawing.Point(98, 497);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(92, 58);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Защиты Q";
            // 
            // _q2Led
            // 
            this._q2Led.Location = new System.Drawing.Point(9, 37);
            this._q2Led.Name = "_q2Led";
            this._q2Led.Size = new System.Drawing.Size(13, 13);
            this._q2Led.State = BEMN.Forms.LedState.Off;
            this._q2Led.TabIndex = 75;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(28, 37);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(27, 13);
            this.label102.TabIndex = 73;
            this.label102.Text = "Q>>";
            // 
            // _q1Led
            // 
            this._q1Led.Location = new System.Drawing.Point(9, 19);
            this._q1Led.Name = "_q1Led";
            this._q1Led.Size = new System.Drawing.Size(13, 13);
            this._q1Led.State = BEMN.Forms.LedState.Off;
            this._q1Led.TabIndex = 72;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(28, 19);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(21, 13);
            this.label104.TabIndex = 70;
            this.label104.Text = "Q>";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label301);
            this.groupBox32.Controls.Add(this.label302);
            this.groupBox32.Controls.Add(this._f4IoLessLed);
            this.groupBox32.Controls.Add(this._f3IoLessLed);
            this.groupBox32.Controls.Add(this._f2IoLessLed);
            this.groupBox32.Controls.Add(this._f4LessLed);
            this.groupBox32.Controls.Add(this.label303);
            this.groupBox32.Controls.Add(this._f1IoLessLed);
            this.groupBox32.Controls.Add(this._f3LessLed);
            this.groupBox32.Controls.Add(this.label304);
            this.groupBox32.Controls.Add(this._f2LessLed);
            this.groupBox32.Controls.Add(this.label305);
            this.groupBox32.Controls.Add(this._f1LessLed);
            this.groupBox32.Controls.Add(this.label306);
            this.groupBox32.Location = new System.Drawing.Point(98, 315);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(92, 108);
            this.groupBox32.TabIndex = 29;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Защиты F<";
            // 
            // label301
            // 
            this.label301.AutoSize = true;
            this.label301.ForeColor = System.Drawing.Color.Blue;
            this.label301.Location = new System.Drawing.Point(25, 12);
            this.label301.Name = "label301";
            this.label301.Size = new System.Drawing.Size(32, 13);
            this.label301.TabIndex = 83;
            this.label301.Text = "Сраб";
            // 
            // label302
            // 
            this.label302.AutoSize = true;
            this.label302.ForeColor = System.Drawing.Color.Red;
            this.label302.Location = new System.Drawing.Point(6, 12);
            this.label302.Name = "label302";
            this.label302.Size = new System.Drawing.Size(23, 13);
            this.label302.TabIndex = 82;
            this.label302.Text = "ИО";
            // 
            // _f4IoLessLed
            // 
            this._f4IoLessLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoLessLed.Name = "_f4IoLessLed";
            this._f4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f4IoLessLed.TabIndex = 80;
            // 
            // _f3IoLessLed
            // 
            this._f3IoLessLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoLessLed.Name = "_f3IoLessLed";
            this._f3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f3IoLessLed.TabIndex = 77;
            // 
            // _f2IoLessLed
            // 
            this._f2IoLessLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoLessLed.Name = "_f2IoLessLed";
            this._f2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f2IoLessLed.TabIndex = 74;
            // 
            // _f4LessLed
            // 
            this._f4LessLed.Location = new System.Drawing.Point(28, 88);
            this._f4LessLed.Name = "_f4LessLed";
            this._f4LessLed.Size = new System.Drawing.Size(13, 13);
            this._f4LessLed.State = BEMN.Forms.LedState.Off;
            this._f4LessLed.TabIndex = 81;
            // 
            // label303
            // 
            this.label303.AutoSize = true;
            this.label303.Location = new System.Drawing.Point(48, 88);
            this.label303.Name = "label303";
            this.label303.Size = new System.Drawing.Size(25, 13);
            this.label303.TabIndex = 79;
            this.label303.Text = "F<4";
            // 
            // _f1IoLessLed
            // 
            this._f1IoLessLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoLessLed.Name = "_f1IoLessLed";
            this._f1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._f1IoLessLed.TabIndex = 71;
            // 
            // _f3LessLed
            // 
            this._f3LessLed.Location = new System.Drawing.Point(28, 69);
            this._f3LessLed.Name = "_f3LessLed";
            this._f3LessLed.Size = new System.Drawing.Size(13, 13);
            this._f3LessLed.State = BEMN.Forms.LedState.Off;
            this._f3LessLed.TabIndex = 78;
            // 
            // label304
            // 
            this.label304.AutoSize = true;
            this.label304.Location = new System.Drawing.Point(47, 69);
            this.label304.Name = "label304";
            this.label304.Size = new System.Drawing.Size(25, 13);
            this.label304.TabIndex = 76;
            this.label304.Text = "F<3";
            // 
            // _f2LessLed
            // 
            this._f2LessLed.Location = new System.Drawing.Point(28, 50);
            this._f2LessLed.Name = "_f2LessLed";
            this._f2LessLed.Size = new System.Drawing.Size(13, 13);
            this._f2LessLed.State = BEMN.Forms.LedState.Off;
            this._f2LessLed.TabIndex = 75;
            // 
            // label305
            // 
            this.label305.AutoSize = true;
            this.label305.Location = new System.Drawing.Point(47, 50);
            this.label305.Name = "label305";
            this.label305.Size = new System.Drawing.Size(25, 13);
            this.label305.TabIndex = 73;
            this.label305.Text = "F<2";
            // 
            // _f1LessLed
            // 
            this._f1LessLed.Location = new System.Drawing.Point(28, 31);
            this._f1LessLed.Name = "_f1LessLed";
            this._f1LessLed.Size = new System.Drawing.Size(13, 13);
            this._f1LessLed.State = BEMN.Forms.LedState.Off;
            this._f1LessLed.TabIndex = 72;
            // 
            // label306
            // 
            this.label306.AutoSize = true;
            this.label306.Location = new System.Drawing.Point(47, 31);
            this.label306.Name = "label306";
            this.label306.Size = new System.Drawing.Size(25, 13);
            this.label306.TabIndex = 70;
            this.label306.Text = "F<1";
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label295);
            this.groupBox31.Controls.Add(this.label296);
            this.groupBox31.Controls.Add(this._f4IoMoreLed);
            this.groupBox31.Controls.Add(this._f3IoMoreLed);
            this.groupBox31.Controls.Add(this._f2IoMoreLed);
            this.groupBox31.Controls.Add(this._f4MoreLed);
            this.groupBox31.Controls.Add(this.label297);
            this.groupBox31.Controls.Add(this._f1IoMoreLed);
            this.groupBox31.Controls.Add(this._f3MoreLed);
            this.groupBox31.Controls.Add(this.label298);
            this.groupBox31.Controls.Add(this._f2MoreLed);
            this.groupBox31.Controls.Add(this.label299);
            this.groupBox31.Controls.Add(this._f1MoreLed);
            this.groupBox31.Controls.Add(this.label300);
            this.groupBox31.Location = new System.Drawing.Point(98, 206);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(92, 108);
            this.groupBox31.TabIndex = 28;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "Защиты F>";
            // 
            // label295
            // 
            this.label295.AutoSize = true;
            this.label295.ForeColor = System.Drawing.Color.Blue;
            this.label295.Location = new System.Drawing.Point(25, 12);
            this.label295.Name = "label295";
            this.label295.Size = new System.Drawing.Size(32, 13);
            this.label295.TabIndex = 83;
            this.label295.Text = "Сраб";
            // 
            // label296
            // 
            this.label296.AutoSize = true;
            this.label296.ForeColor = System.Drawing.Color.Red;
            this.label296.Location = new System.Drawing.Point(6, 12);
            this.label296.Name = "label296";
            this.label296.Size = new System.Drawing.Size(23, 13);
            this.label296.TabIndex = 82;
            this.label296.Text = "ИО";
            // 
            // _f4IoMoreLed
            // 
            this._f4IoMoreLed.Location = new System.Drawing.Point(9, 88);
            this._f4IoMoreLed.Name = "_f4IoMoreLed";
            this._f4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f4IoMoreLed.TabIndex = 80;
            // 
            // _f3IoMoreLed
            // 
            this._f3IoMoreLed.Location = new System.Drawing.Point(9, 69);
            this._f3IoMoreLed.Name = "_f3IoMoreLed";
            this._f3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f3IoMoreLed.TabIndex = 77;
            // 
            // _f2IoMoreLed
            // 
            this._f2IoMoreLed.Location = new System.Drawing.Point(9, 50);
            this._f2IoMoreLed.Name = "_f2IoMoreLed";
            this._f2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f2IoMoreLed.TabIndex = 74;
            // 
            // _f4MoreLed
            // 
            this._f4MoreLed.Location = new System.Drawing.Point(28, 88);
            this._f4MoreLed.Name = "_f4MoreLed";
            this._f4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f4MoreLed.State = BEMN.Forms.LedState.Off;
            this._f4MoreLed.TabIndex = 81;
            // 
            // label297
            // 
            this.label297.AutoSize = true;
            this.label297.Location = new System.Drawing.Point(48, 88);
            this.label297.Name = "label297";
            this.label297.Size = new System.Drawing.Size(25, 13);
            this.label297.TabIndex = 79;
            this.label297.Text = "F>4";
            // 
            // _f1IoMoreLed
            // 
            this._f1IoMoreLed.Location = new System.Drawing.Point(9, 31);
            this._f1IoMoreLed.Name = "_f1IoMoreLed";
            this._f1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._f1IoMoreLed.TabIndex = 71;
            // 
            // _f3MoreLed
            // 
            this._f3MoreLed.Location = new System.Drawing.Point(28, 69);
            this._f3MoreLed.Name = "_f3MoreLed";
            this._f3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f3MoreLed.State = BEMN.Forms.LedState.Off;
            this._f3MoreLed.TabIndex = 78;
            // 
            // label298
            // 
            this.label298.AutoSize = true;
            this.label298.Location = new System.Drawing.Point(47, 69);
            this.label298.Name = "label298";
            this.label298.Size = new System.Drawing.Size(25, 13);
            this.label298.TabIndex = 76;
            this.label298.Text = "F>3";
            // 
            // _f2MoreLed
            // 
            this._f2MoreLed.Location = new System.Drawing.Point(28, 50);
            this._f2MoreLed.Name = "_f2MoreLed";
            this._f2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f2MoreLed.State = BEMN.Forms.LedState.Off;
            this._f2MoreLed.TabIndex = 75;
            // 
            // label299
            // 
            this.label299.AutoSize = true;
            this.label299.Location = new System.Drawing.Point(47, 50);
            this.label299.Name = "label299";
            this.label299.Size = new System.Drawing.Size(25, 13);
            this.label299.TabIndex = 73;
            this.label299.Text = "F>2";
            // 
            // _f1MoreLed
            // 
            this._f1MoreLed.Location = new System.Drawing.Point(28, 31);
            this._f1MoreLed.Name = "_f1MoreLed";
            this._f1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._f1MoreLed.State = BEMN.Forms.LedState.Off;
            this._f1MoreLed.TabIndex = 72;
            // 
            // label300
            // 
            this.label300.AutoSize = true;
            this.label300.Location = new System.Drawing.Point(47, 31);
            this.label300.Name = "label300";
            this.label300.Size = new System.Drawing.Size(25, 13);
            this.label300.TabIndex = 70;
            this.label300.Text = "F>1";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label289);
            this.groupBox30.Controls.Add(this.label290);
            this.groupBox30.Controls.Add(this._u4IoLessLed);
            this.groupBox30.Controls.Add(this._u3IoLessLed);
            this.groupBox30.Controls.Add(this._u2IoLessLed);
            this.groupBox30.Controls.Add(this._u4LessLed);
            this.groupBox30.Controls.Add(this.label291);
            this.groupBox30.Controls.Add(this._u1IoLessLed);
            this.groupBox30.Controls.Add(this._u3LessLed);
            this.groupBox30.Controls.Add(this.label292);
            this.groupBox30.Controls.Add(this._u2LessLed);
            this.groupBox30.Controls.Add(this.label293);
            this.groupBox30.Controls.Add(this._u1LessLed);
            this.groupBox30.Controls.Add(this.label294);
            this.groupBox30.Location = new System.Drawing.Point(98, 106);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(92, 91);
            this.groupBox30.TabIndex = 27;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "Защиты U<";
            // 
            // label289
            // 
            this.label289.AutoSize = true;
            this.label289.ForeColor = System.Drawing.Color.Blue;
            this.label289.Location = new System.Drawing.Point(25, 12);
            this.label289.Name = "label289";
            this.label289.Size = new System.Drawing.Size(32, 13);
            this.label289.TabIndex = 83;
            this.label289.Text = "Сраб";
            // 
            // label290
            // 
            this.label290.AutoSize = true;
            this.label290.ForeColor = System.Drawing.Color.Red;
            this.label290.Location = new System.Drawing.Point(6, 12);
            this.label290.Name = "label290";
            this.label290.Size = new System.Drawing.Size(23, 13);
            this.label290.TabIndex = 82;
            this.label290.Text = "ИО";
            // 
            // _u4IoLessLed
            // 
            this._u4IoLessLed.Location = new System.Drawing.Point(9, 74);
            this._u4IoLessLed.Name = "_u4IoLessLed";
            this._u4IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u4IoLessLed.TabIndex = 80;
            // 
            // _u3IoLessLed
            // 
            this._u3IoLessLed.Location = new System.Drawing.Point(9, 59);
            this._u3IoLessLed.Name = "_u3IoLessLed";
            this._u3IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u3IoLessLed.TabIndex = 77;
            // 
            // _u2IoLessLed
            // 
            this._u2IoLessLed.Location = new System.Drawing.Point(9, 44);
            this._u2IoLessLed.Name = "_u2IoLessLed";
            this._u2IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u2IoLessLed.TabIndex = 74;
            // 
            // _u4LessLed
            // 
            this._u4LessLed.Location = new System.Drawing.Point(28, 74);
            this._u4LessLed.Name = "_u4LessLed";
            this._u4LessLed.Size = new System.Drawing.Size(13, 13);
            this._u4LessLed.State = BEMN.Forms.LedState.Off;
            this._u4LessLed.TabIndex = 81;
            // 
            // label291
            // 
            this.label291.AutoSize = true;
            this.label291.Location = new System.Drawing.Point(48, 74);
            this.label291.Name = "label291";
            this.label291.Size = new System.Drawing.Size(27, 13);
            this.label291.TabIndex = 79;
            this.label291.Text = "U<4";
            // 
            // _u1IoLessLed
            // 
            this._u1IoLessLed.Location = new System.Drawing.Point(9, 29);
            this._u1IoLessLed.Name = "_u1IoLessLed";
            this._u1IoLessLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoLessLed.State = BEMN.Forms.LedState.Off;
            this._u1IoLessLed.TabIndex = 71;
            // 
            // _u3LessLed
            // 
            this._u3LessLed.Location = new System.Drawing.Point(28, 59);
            this._u3LessLed.Name = "_u3LessLed";
            this._u3LessLed.Size = new System.Drawing.Size(13, 13);
            this._u3LessLed.State = BEMN.Forms.LedState.Off;
            this._u3LessLed.TabIndex = 78;
            // 
            // label292
            // 
            this.label292.AutoSize = true;
            this.label292.Location = new System.Drawing.Point(47, 59);
            this.label292.Name = "label292";
            this.label292.Size = new System.Drawing.Size(27, 13);
            this.label292.TabIndex = 76;
            this.label292.Text = "U<3";
            // 
            // _u2LessLed
            // 
            this._u2LessLed.Location = new System.Drawing.Point(28, 44);
            this._u2LessLed.Name = "_u2LessLed";
            this._u2LessLed.Size = new System.Drawing.Size(13, 13);
            this._u2LessLed.State = BEMN.Forms.LedState.Off;
            this._u2LessLed.TabIndex = 75;
            // 
            // label293
            // 
            this.label293.AutoSize = true;
            this.label293.Location = new System.Drawing.Point(47, 44);
            this.label293.Name = "label293";
            this.label293.Size = new System.Drawing.Size(27, 13);
            this.label293.TabIndex = 73;
            this.label293.Text = "U<2";
            // 
            // _u1LessLed
            // 
            this._u1LessLed.Location = new System.Drawing.Point(28, 29);
            this._u1LessLed.Name = "_u1LessLed";
            this._u1LessLed.Size = new System.Drawing.Size(13, 13);
            this._u1LessLed.State = BEMN.Forms.LedState.Off;
            this._u1LessLed.TabIndex = 72;
            // 
            // label294
            // 
            this.label294.AutoSize = true;
            this.label294.Location = new System.Drawing.Point(47, 29);
            this.label294.Name = "label294";
            this.label294.Size = new System.Drawing.Size(27, 13);
            this.label294.TabIndex = 70;
            this.label294.Text = "U<1";
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label283);
            this.groupBox29.Controls.Add(this.label284);
            this.groupBox29.Controls.Add(this._u4IoMoreLed);
            this.groupBox29.Controls.Add(this._u3IoMoreLed);
            this.groupBox29.Controls.Add(this._u2IoMoreLed);
            this.groupBox29.Controls.Add(this._u4MoreLed);
            this.groupBox29.Controls.Add(this.label285);
            this.groupBox29.Controls.Add(this._u1IoMoreLed);
            this.groupBox29.Controls.Add(this._u3MoreLed);
            this.groupBox29.Controls.Add(this.label286);
            this.groupBox29.Controls.Add(this._u2MoreLed);
            this.groupBox29.Controls.Add(this.label287);
            this.groupBox29.Controls.Add(this._u1MoreLed);
            this.groupBox29.Controls.Add(this.label288);
            this.groupBox29.Location = new System.Drawing.Point(98, 6);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(92, 99);
            this.groupBox29.TabIndex = 26;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "Защиты U>";
            // 
            // label283
            // 
            this.label283.AutoSize = true;
            this.label283.ForeColor = System.Drawing.Color.Blue;
            this.label283.Location = new System.Drawing.Point(25, 12);
            this.label283.Name = "label283";
            this.label283.Size = new System.Drawing.Size(32, 13);
            this.label283.TabIndex = 83;
            this.label283.Text = "Сраб";
            // 
            // label284
            // 
            this.label284.AutoSize = true;
            this.label284.ForeColor = System.Drawing.Color.Red;
            this.label284.Location = new System.Drawing.Point(6, 12);
            this.label284.Name = "label284";
            this.label284.Size = new System.Drawing.Size(23, 13);
            this.label284.TabIndex = 82;
            this.label284.Text = "ИО";
            // 
            // _u4IoMoreLed
            // 
            this._u4IoMoreLed.Location = new System.Drawing.Point(9, 77);
            this._u4IoMoreLed.Name = "_u4IoMoreLed";
            this._u4IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u4IoMoreLed.TabIndex = 80;
            // 
            // _u3IoMoreLed
            // 
            this._u3IoMoreLed.Location = new System.Drawing.Point(9, 61);
            this._u3IoMoreLed.Name = "_u3IoMoreLed";
            this._u3IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u3IoMoreLed.TabIndex = 77;
            // 
            // _u2IoMoreLed
            // 
            this._u2IoMoreLed.Location = new System.Drawing.Point(9, 45);
            this._u2IoMoreLed.Name = "_u2IoMoreLed";
            this._u2IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u2IoMoreLed.TabIndex = 74;
            // 
            // _u4MoreLed
            // 
            this._u4MoreLed.Location = new System.Drawing.Point(28, 77);
            this._u4MoreLed.Name = "_u4MoreLed";
            this._u4MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u4MoreLed.State = BEMN.Forms.LedState.Off;
            this._u4MoreLed.TabIndex = 81;
            // 
            // label285
            // 
            this.label285.AutoSize = true;
            this.label285.Location = new System.Drawing.Point(48, 77);
            this.label285.Name = "label285";
            this.label285.Size = new System.Drawing.Size(27, 13);
            this.label285.TabIndex = 79;
            this.label285.Text = "U>4";
            // 
            // _u1IoMoreLed
            // 
            this._u1IoMoreLed.Location = new System.Drawing.Point(9, 29);
            this._u1IoMoreLed.Name = "_u1IoMoreLed";
            this._u1IoMoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1IoMoreLed.State = BEMN.Forms.LedState.Off;
            this._u1IoMoreLed.TabIndex = 71;
            // 
            // _u3MoreLed
            // 
            this._u3MoreLed.Location = new System.Drawing.Point(28, 61);
            this._u3MoreLed.Name = "_u3MoreLed";
            this._u3MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u3MoreLed.State = BEMN.Forms.LedState.Off;
            this._u3MoreLed.TabIndex = 78;
            // 
            // label286
            // 
            this.label286.AutoSize = true;
            this.label286.Location = new System.Drawing.Point(47, 61);
            this.label286.Name = "label286";
            this.label286.Size = new System.Drawing.Size(27, 13);
            this.label286.TabIndex = 76;
            this.label286.Text = "U>3";
            // 
            // _u2MoreLed
            // 
            this._u2MoreLed.Location = new System.Drawing.Point(28, 45);
            this._u2MoreLed.Name = "_u2MoreLed";
            this._u2MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u2MoreLed.State = BEMN.Forms.LedState.Off;
            this._u2MoreLed.TabIndex = 75;
            // 
            // label287
            // 
            this.label287.AutoSize = true;
            this.label287.Location = new System.Drawing.Point(47, 45);
            this.label287.Name = "label287";
            this.label287.Size = new System.Drawing.Size(27, 13);
            this.label287.TabIndex = 73;
            this.label287.Text = "U>2";
            // 
            // _u1MoreLed
            // 
            this._u1MoreLed.Location = new System.Drawing.Point(28, 29);
            this._u1MoreLed.Name = "_u1MoreLed";
            this._u1MoreLed.Size = new System.Drawing.Size(13, 13);
            this._u1MoreLed.State = BEMN.Forms.LedState.Off;
            this._u1MoreLed.TabIndex = 72;
            // 
            // label288
            // 
            this.label288.AutoSize = true;
            this.label288.Location = new System.Drawing.Point(47, 29);
            this.label288.Name = "label288";
            this.label288.Size = new System.Drawing.Size(27, 13);
            this.label288.TabIndex = 70;
            this.label288.Text = "U>1";
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Transparent;
            this.groupBox19.Controls.Add(this._faultModule6);
            this.groupBox19.Controls.Add(this.label3);
            this.groupBox19.Controls.Add(this.label344);
            this.groupBox19.Controls.Add(this._faultSwitchOff);
            this.groupBox19.Controls.Add(this._faultMeasuringf);
            this.groupBox19.Controls.Add(this.label96);
            this.groupBox19.Controls.Add(this._faultAlarmJournal);
            this.groupBox19.Controls.Add(this.label192);
            this.groupBox19.Controls.Add(this._faultOsc);
            this.groupBox19.Controls.Add(this.label193);
            this.groupBox19.Controls.Add(this._faultModule5);
            this.groupBox19.Controls.Add(this._faultSystemJournal);
            this.groupBox19.Controls.Add(this.label200);
            this.groupBox19.Controls.Add(this.label194);
            this.groupBox19.Controls.Add(this._faultGroupsOfSetpoints);
            this.groupBox19.Controls.Add(this.label201);
            this.groupBox19.Controls.Add(this._faultModule4);
            this.groupBox19.Controls.Add(this._faultLogic);
            this.groupBox19.Controls.Add(this.label79);
            this.groupBox19.Controls.Add(this._faultSetpoints);
            this.groupBox19.Controls.Add(this.label202);
            this.groupBox19.Controls.Add(this.label195);
            this.groupBox19.Controls.Add(this._faultPass);
            this.groupBox19.Controls.Add(this.label203);
            this.groupBox19.Controls.Add(this._faultModule3);
            this.groupBox19.Controls.Add(this._faultMeasuringU);
            this.groupBox19.Controls.Add(this.label204);
            this.groupBox19.Controls.Add(this.label196);
            this.groupBox19.Controls.Add(this._faultSoftware);
            this.groupBox19.Controls.Add(this.label205);
            this.groupBox19.Controls.Add(this._faultModule2);
            this.groupBox19.Controls.Add(this._faultHardware);
            this.groupBox19.Controls.Add(this.label206);
            this.groupBox19.Controls.Add(this.label197);
            this.groupBox19.Controls.Add(this._faultModule1);
            this.groupBox19.Controls.Add(this.label198);
            this.groupBox19.Location = new System.Drawing.Point(540, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(226, 194);
            this.groupBox19.TabIndex = 13;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Неисправности";
            // 
            // _faultModule6
            // 
            this._faultModule6.Location = new System.Drawing.Point(117, 171);
            this._faultModule6.Name = "_faultModule6";
            this._faultModule6.Size = new System.Drawing.Size(13, 13);
            this._faultModule6.State = BEMN.Forms.LedState.Off;
            this._faultModule6.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(136, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Модуля 6";
            // 
            // label344
            // 
            this.label344.AutoSize = true;
            this.label344.Location = new System.Drawing.Point(25, 95);
            this.label344.Name = "label344";
            this.label344.Size = new System.Drawing.Size(76, 13);
            this.label344.TabIndex = 35;
            this.label344.Text = "Выключателя";
            // 
            // _faultSwitchOff
            // 
            this._faultSwitchOff.Location = new System.Drawing.Point(6, 95);
            this._faultSwitchOff.Name = "_faultSwitchOff";
            this._faultSwitchOff.Size = new System.Drawing.Size(13, 13);
            this._faultSwitchOff.State = BEMN.Forms.LedState.Off;
            this._faultSwitchOff.TabIndex = 34;
            // 
            // _faultMeasuringf
            // 
            this._faultMeasuringf.Location = new System.Drawing.Point(6, 76);
            this._faultMeasuringf.Name = "_faultMeasuringf";
            this._faultMeasuringf.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringf.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringf.TabIndex = 31;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(25, 76);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(74, 13);
            this.label96.TabIndex = 30;
            this.label96.Text = "Измерения F";
            // 
            // _faultAlarmJournal
            // 
            this._faultAlarmJournal.Location = new System.Drawing.Point(117, 57);
            this._faultAlarmJournal.Name = "_faultAlarmJournal";
            this._faultAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._faultAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._faultAlarmJournal.TabIndex = 29;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(136, 57);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(25, 13);
            this.label192.TabIndex = 28;
            this.label192.Text = "ЖА";
            // 
            // _faultOsc
            // 
            this._faultOsc.Location = new System.Drawing.Point(117, 19);
            this._faultOsc.Name = "_faultOsc";
            this._faultOsc.Size = new System.Drawing.Size(13, 13);
            this._faultOsc.State = BEMN.Forms.LedState.Off;
            this._faultOsc.TabIndex = 27;
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(136, 19);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(84, 13);
            this.label193.TabIndex = 26;
            this.label193.Text = "Осциллограмы";
            // 
            // _faultModule5
            // 
            this._faultModule5.Location = new System.Drawing.Point(117, 152);
            this._faultModule5.Name = "_faultModule5";
            this._faultModule5.Size = new System.Drawing.Size(13, 13);
            this._faultModule5.State = BEMN.Forms.LedState.Off;
            this._faultModule5.TabIndex = 25;
            // 
            // _faultSystemJournal
            // 
            this._faultSystemJournal.Location = new System.Drawing.Point(117, 38);
            this._faultSystemJournal.Name = "_faultSystemJournal";
            this._faultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._faultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._faultSystemJournal.TabIndex = 13;
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(136, 38);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(25, 13);
            this.label200.TabIndex = 12;
            this.label200.Text = "ЖС";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(136, 152);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(54, 13);
            this.label194.TabIndex = 24;
            this.label194.Text = "Модуля 5";
            // 
            // _faultGroupsOfSetpoints
            // 
            this._faultGroupsOfSetpoints.Location = new System.Drawing.Point(6, 152);
            this._faultGroupsOfSetpoints.Name = "_faultGroupsOfSetpoints";
            this._faultGroupsOfSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultGroupsOfSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultGroupsOfSetpoints.TabIndex = 11;
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(25, 152);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(79, 13);
            this.label201.TabIndex = 10;
            this.label201.Text = "Групп уставок";
            // 
            // _faultModule4
            // 
            this._faultModule4.Location = new System.Drawing.Point(117, 133);
            this._faultModule4.Name = "_faultModule4";
            this._faultModule4.Size = new System.Drawing.Size(13, 13);
            this._faultModule4.State = BEMN.Forms.LedState.Off;
            this._faultModule4.TabIndex = 23;
            // 
            // _faultLogic
            // 
            this._faultLogic.Location = new System.Drawing.Point(6, 114);
            this._faultLogic.Name = "_faultLogic";
            this._faultLogic.Size = new System.Drawing.Size(13, 13);
            this._faultLogic.State = BEMN.Forms.LedState.Off;
            this._faultLogic.TabIndex = 9;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(25, 114);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(44, 13);
            this.label79.TabIndex = 8;
            this.label79.Text = "Логики";
            // 
            // _faultSetpoints
            // 
            this._faultSetpoints.Location = new System.Drawing.Point(6, 133);
            this._faultSetpoints.Name = "_faultSetpoints";
            this._faultSetpoints.Size = new System.Drawing.Size(13, 13);
            this._faultSetpoints.State = BEMN.Forms.LedState.Off;
            this._faultSetpoints.TabIndex = 9;
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(25, 133);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(50, 13);
            this.label202.TabIndex = 8;
            this.label202.Text = "Уставок";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(136, 133);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(54, 13);
            this.label195.TabIndex = 22;
            this.label195.Text = "Модуля 4";
            // 
            // _faultPass
            // 
            this._faultPass.Location = new System.Drawing.Point(6, 171);
            this._faultPass.Name = "_faultPass";
            this._faultPass.Size = new System.Drawing.Size(13, 13);
            this._faultPass.State = BEMN.Forms.LedState.Off;
            this._faultPass.TabIndex = 7;
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(25, 171);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(45, 13);
            this.label203.TabIndex = 6;
            this.label203.Text = "Пароля";
            // 
            // _faultModule3
            // 
            this._faultModule3.Location = new System.Drawing.Point(117, 114);
            this._faultModule3.Name = "_faultModule3";
            this._faultModule3.Size = new System.Drawing.Size(13, 13);
            this._faultModule3.State = BEMN.Forms.LedState.Off;
            this._faultModule3.TabIndex = 21;
            // 
            // _faultMeasuringU
            // 
            this._faultMeasuringU.Location = new System.Drawing.Point(6, 57);
            this._faultMeasuringU.Name = "_faultMeasuringU";
            this._faultMeasuringU.Size = new System.Drawing.Size(13, 13);
            this._faultMeasuringU.State = BEMN.Forms.LedState.Off;
            this._faultMeasuringU.TabIndex = 5;
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(25, 57);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(76, 13);
            this.label204.TabIndex = 4;
            this.label204.Text = "Измерения U";
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(136, 114);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(54, 13);
            this.label196.TabIndex = 20;
            this.label196.Text = "Модуля 3";
            // 
            // _faultSoftware
            // 
            this._faultSoftware.Location = new System.Drawing.Point(6, 38);
            this._faultSoftware.Name = "_faultSoftware";
            this._faultSoftware.Size = new System.Drawing.Size(13, 13);
            this._faultSoftware.State = BEMN.Forms.LedState.Off;
            this._faultSoftware.TabIndex = 3;
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(25, 38);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(78, 13);
            this.label205.TabIndex = 2;
            this.label205.Text = "Программная";
            // 
            // _faultModule2
            // 
            this._faultModule2.Location = new System.Drawing.Point(117, 95);
            this._faultModule2.Name = "_faultModule2";
            this._faultModule2.Size = new System.Drawing.Size(13, 13);
            this._faultModule2.State = BEMN.Forms.LedState.Off;
            this._faultModule2.TabIndex = 19;
            // 
            // _faultHardware
            // 
            this._faultHardware.Location = new System.Drawing.Point(6, 19);
            this._faultHardware.Name = "_faultHardware";
            this._faultHardware.Size = new System.Drawing.Size(13, 13);
            this._faultHardware.State = BEMN.Forms.LedState.Off;
            this._faultHardware.TabIndex = 1;
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(25, 19);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(67, 13);
            this.label206.TabIndex = 0;
            this.label206.Text = "Аппаратная";
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(136, 95);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(54, 13);
            this.label197.TabIndex = 18;
            this.label197.Text = "Модуля 2";
            // 
            // _faultModule1
            // 
            this._faultModule1.Location = new System.Drawing.Point(117, 76);
            this._faultModule1.Name = "_faultModule1";
            this._faultModule1.Size = new System.Drawing.Size(13, 13);
            this._faultModule1.State = BEMN.Forms.LedState.Off;
            this._faultModule1.TabIndex = 17;
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(136, 76);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 16;
            this.label198.Text = "Модуля 1";
            // 
            // _controlSignalsTabPage
            // 
            this._controlSignalsTabPage.Controls.Add(this.groupBox5);
            this._controlSignalsTabPage.Controls.Add(this.groupBox3);
            this._controlSignalsTabPage.Controls.Add(this.groupBox28);
            this._controlSignalsTabPage.Controls.Add(this.groupBox27);
            this._controlSignalsTabPage.Location = new System.Drawing.Point(4, 22);
            this._controlSignalsTabPage.Name = "_controlSignalsTabPage";
            this._controlSignalsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this._controlSignalsTabPage.Size = new System.Drawing.Size(911, 599);
            this._controlSignalsTabPage.TabIndex = 2;
            this._controlSignalsTabPage.Text = "Управляющие сигналы";
            this._controlSignalsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._commandBtn);
            this.groupBox5.Controls.Add(this._commandComboBox);
            this.groupBox5.Location = new System.Drawing.Point(328, 91);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(193, 57);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Команды";
            // 
            // _commandBtn
            // 
            this._commandBtn.Location = new System.Drawing.Point(112, 17);
            this._commandBtn.Name = "_commandBtn";
            this._commandBtn.Size = new System.Drawing.Size(75, 23);
            this._commandBtn.TabIndex = 1;
            this._commandBtn.Text = "Выполнить";
            this._commandBtn.UseVisualStyleBackColor = true;
            this._commandBtn.Click += new System.EventHandler(this.CommandRunClick);
            // 
            // _commandComboBox
            // 
            this._commandComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._commandComboBox.FormattingEnabled = true;
            this._commandComboBox.Items.AddRange(new object[] {
            "Команда 1",
            "Команда 2",
            "Команда 3",
            "Команда 4",
            "Команда 5",
            "Команда 6",
            "Команда 7",
            "Команда 8",
            "Команда 9",
            "Команда 10",
            "Команда 11",
            "Команда 12",
            "Команда 13",
            "Команда 14",
            "Команда 15",
            "Команда 16",
            "Команда 17",
            "Команда 18",
            "Команда 19",
            "Команда 20",
            "Команда 21",
            "Команда 22",
            "Команда 23",
            "Команда 24"});
            this._commandComboBox.Location = new System.Drawing.Point(7, 18);
            this._commandComboBox.Name = "_commandComboBox";
            this._commandComboBox.Size = new System.Drawing.Size(99, 21);
            this._commandComboBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this._reserveGroupButton);
            this.groupBox3.Controls.Add(this._mainGroupButton);
            this.groupBox3.Controls.Add(this.label490);
            this.groupBox3.Controls.Add(this.label491);
            this.groupBox3.Controls.Add(this._reservedGroup);
            this.groupBox3.Controls.Add(this._mainGroup);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(328, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 83);
            this.groupBox3.TabIndex = 26;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Группа уставок";
            // 
            // _reserveGroupButton
            // 
            this._reserveGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._reserveGroupButton.Location = new System.Drawing.Point(101, 56);
            this._reserveGroupButton.Name = "_reserveGroupButton";
            this._reserveGroupButton.Size = new System.Drawing.Size(86, 23);
            this._reserveGroupButton.TabIndex = 30;
            this._reserveGroupButton.Text = "Переключить";
            this._reserveGroupButton.UseVisualStyleBackColor = true;
            this._reserveGroupButton.Click += new System.EventHandler(this._reserveGroupButton_Click);
            // 
            // _mainGroupButton
            // 
            this._mainGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mainGroupButton.Location = new System.Drawing.Point(6, 56);
            this._mainGroupButton.Name = "_mainGroupButton";
            this._mainGroupButton.Size = new System.Drawing.Size(86, 23);
            this._mainGroupButton.TabIndex = 29;
            this._mainGroupButton.Text = "Переключить";
            this._mainGroupButton.UseVisualStyleBackColor = true;
            this._mainGroupButton.Click += new System.EventHandler(this._mainGroupButton_Click);
            // 
            // label490
            // 
            this.label490.AutoSize = true;
            this.label490.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label490.Location = new System.Drawing.Point(118, 39);
            this.label490.Name = "label490";
            this.label490.Size = new System.Drawing.Size(51, 13);
            this.label490.TabIndex = 28;
            this.label490.Text = "Группа 2";
            // 
            // label491
            // 
            this.label491.AutoSize = true;
            this.label491.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label491.Location = new System.Drawing.Point(22, 39);
            this.label491.Name = "label491";
            this.label491.Size = new System.Drawing.Size(51, 13);
            this.label491.TabIndex = 27;
            this.label491.Text = "Группа 1";
            // 
            // _reservedGroup
            // 
            this._reservedGroup.Location = new System.Drawing.Point(136, 19);
            this._reservedGroup.Name = "_reservedGroup";
            this._reservedGroup.Size = new System.Drawing.Size(13, 13);
            this._reservedGroup.State = BEMN.Forms.LedState.Off;
            this._reservedGroup.TabIndex = 24;
            // 
            // _mainGroup
            // 
            this._mainGroup.Location = new System.Drawing.Point(41, 19);
            this._mainGroup.Name = "_mainGroup";
            this._mainGroup.Size = new System.Drawing.Size(14, 13);
            this._mainGroup.State = BEMN.Forms.LedState.Off;
            this._mainGroup.TabIndex = 23;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.groupBox16);
            this.groupBox28.Controls.Add(this.groupBox15);
            this.groupBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox28.Location = new System.Drawing.Point(555, 20);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(223, 101);
            this.groupBox28.TabIndex = 24;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Группа уставок";
            this.groupBox28.Visible = false;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this._currenGroupLabel);
            this.groupBox16.Location = new System.Drawing.Point(114, 14);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(103, 81);
            this.groupBox16.TabIndex = 32;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Текущая группа";
            // 
            // _currenGroupLabel
            // 
            this._currenGroupLabel.AutoSize = true;
            this._currenGroupLabel.Location = new System.Drawing.Point(17, 33);
            this._currenGroupLabel.Name = "_currenGroupLabel";
            this._currenGroupLabel.Size = new System.Drawing.Size(62, 13);
            this._currenGroupLabel.TabIndex = 0;
            this._currenGroupLabel.Text = "Группа №1";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._groupCombo);
            this.groupBox15.Controls.Add(this._switchGroupButton);
            this.groupBox15.Location = new System.Drawing.Point(6, 14);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(102, 81);
            this.groupBox15.TabIndex = 32;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Выбрать группу";
            // 
            // _groupCombo
            // 
            this._groupCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._groupCombo.FormattingEnabled = true;
            this._groupCombo.Items.AddRange(new object[] {
            "Группа 1",
            "Группа 2"});
            this._groupCombo.Location = new System.Drawing.Point(6, 25);
            this._groupCombo.Name = "_groupCombo";
            this._groupCombo.Size = new System.Drawing.Size(86, 21);
            this._groupCombo.TabIndex = 31;
            // 
            // _switchGroupButton
            // 
            this._switchGroupButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._switchGroupButton.Location = new System.Drawing.Point(6, 52);
            this._switchGroupButton.Name = "_switchGroupButton";
            this._switchGroupButton.Size = new System.Drawing.Size(86, 23);
            this._switchGroupButton.TabIndex = 30;
            this._switchGroupButton.Text = "Переключить";
            this._switchGroupButton.UseVisualStyleBackColor = true;
            this._switchGroupButton.Click += new System.EventHandler(this._switchGroupButton_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.ResetSamopodBtn);
            this.groupBox27.Controls.Add(this.ResetInd);
            this.groupBox27.Controls.Add(this._breakerOnBut);
            this.groupBox27.Controls.Add(this.label101);
            this.groupBox27.Controls.Add(this._startOscBtn);
            this.groupBox27.Controls.Add(this._breakerOffBut);
            this.groupBox27.Controls.Add(this.groupBox40);
            this.groupBox27.Controls.Add(this._switchOn);
            this.groupBox27.Controls.Add(this._availabilityFaultSystemJournal);
            this.groupBox27.Controls.Add(this.label100);
            this.groupBox27.Controls.Add(this._newRecordOscJournal);
            this.groupBox27.Controls.Add(this._switchOff);
            this.groupBox27.Controls.Add(this._newRecordAlarmJournal);
            this.groupBox27.Controls.Add(this._newRecordSystemJournal);
            this.groupBox27.Controls.Add(this._resetAvailabilityFaultSystemJournalButton);
            this.groupBox27.Controls.Add(this._resetOscJournalButton);
            this.groupBox27.Controls.Add(this._resetAlarmJournalButton);
            this.groupBox27.Controls.Add(this._resetSystemJournalButton);
            this.groupBox27.Controls.Add(this.label227);
            this.groupBox27.Controls.Add(this.label225);
            this.groupBox27.Controls.Add(this.label226);
            this.groupBox27.Controls.Add(this.label231);
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox27.Location = new System.Drawing.Point(6, 6);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(316, 308);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Управляющие сигналы";
            // 
            // ResetSamopodBtn
            // 
            this.ResetSamopodBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetSamopodBtn.Location = new System.Drawing.Point(6, 245);
            this.ResetSamopodBtn.Name = "ResetSamopodBtn";
            this.ResetSamopodBtn.Size = new System.Drawing.Size(304, 23);
            this.ResetSamopodBtn.TabIndex = 56;
            this.ResetSamopodBtn.Text = "Сброс самоподхвата";
            this.ResetSamopodBtn.UseVisualStyleBackColor = true;
            this.ResetSamopodBtn.Click += new System.EventHandler(this.ResetSamopodBtn_Click);
            // 
            // ResetInd
            // 
            this.ResetInd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ResetInd.Location = new System.Drawing.Point(6, 216);
            this.ResetInd.Name = "ResetInd";
            this.ResetInd.Size = new System.Drawing.Size(304, 23);
            this.ResetInd.TabIndex = 55;
            this.ResetInd.Text = "Сброс блинкеров";
            this.ResetInd.UseVisualStyleBackColor = true;
            this.ResetInd.Click += new System.EventHandler(this.ResetInd_Click);
            // 
            // _breakerOnBut
            // 
            this._breakerOnBut.Location = new System.Drawing.Point(235, 14);
            this._breakerOnBut.Name = "_breakerOnBut";
            this._breakerOnBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOnBut.TabIndex = 51;
            this._breakerOnBut.Text = "Включить";
            this._breakerOnBut.UseVisualStyleBackColor = true;
            this._breakerOnBut.Click += new System.EventHandler(this._breakerOnBut_Click);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(30, 19);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(127, 13);
            this.label101.TabIndex = 50;
            this.label101.Text = "Выключатель отключен";
            // 
            // _startOscBtn
            // 
            this._startOscBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._startOscBtn.Location = new System.Drawing.Point(6, 274);
            this._startOscBtn.Name = "_startOscBtn";
            this._startOscBtn.Size = new System.Drawing.Size(304, 23);
            this._startOscBtn.TabIndex = 14;
            this._startOscBtn.Text = "Пуск осциллографа";
            this._startOscBtn.UseVisualStyleBackColor = true;
            this._startOscBtn.Click += new System.EventHandler(this._startOscBtnClick);
            // 
            // _breakerOffBut
            // 
            this._breakerOffBut.Location = new System.Drawing.Point(235, 36);
            this._breakerOffBut.Name = "_breakerOffBut";
            this._breakerOffBut.Size = new System.Drawing.Size(75, 23);
            this._breakerOffBut.TabIndex = 53;
            this._breakerOffBut.Text = "Отключить";
            this._breakerOffBut.UseVisualStyleBackColor = true;
            this._breakerOffBut.Click += new System.EventHandler(this._breakerOffBut_Click);
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._logicState);
            this.groupBox40.Controls.Add(this.stopLogic);
            this.groupBox40.Controls.Add(this.startLogic);
            this.groupBox40.Controls.Add(this.label314);
            this.groupBox40.Location = new System.Drawing.Point(6, 150);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(304, 60);
            this.groupBox40.TabIndex = 54;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Свободно программируемая логика";
            // 
            // _logicState
            // 
            this._logicState.Location = new System.Drawing.Point(6, 25);
            this._logicState.Name = "_logicState";
            this._logicState.Size = new System.Drawing.Size(13, 13);
            this._logicState.State = BEMN.Forms.LedState.Off;
            this._logicState.TabIndex = 17;
            // 
            // stopLogic
            // 
            this.stopLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stopLogic.Location = new System.Drawing.Point(223, 32);
            this.stopLogic.Name = "stopLogic";
            this.stopLogic.Size = new System.Drawing.Size(75, 23);
            this.stopLogic.TabIndex = 15;
            this.stopLogic.Text = "Остановить";
            this.stopLogic.UseVisualStyleBackColor = true;
            this.stopLogic.Click += new System.EventHandler(this.StopLogicBtnClick);
            // 
            // startLogic
            // 
            this.startLogic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startLogic.Location = new System.Drawing.Point(223, 10);
            this.startLogic.Name = "startLogic";
            this.startLogic.Size = new System.Drawing.Size(75, 23);
            this.startLogic.TabIndex = 16;
            this.startLogic.Text = "Запустить";
            this.startLogic.UseVisualStyleBackColor = true;
            this.startLogic.Click += new System.EventHandler(this.StartLogicBtnClick);
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label314.Location = new System.Drawing.Point(30, 25);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(137, 13);
            this.label314.TabIndex = 14;
            this.label314.Text = "Состояние задачи логики";
            // 
            // _switchOn
            // 
            this._switchOn.Location = new System.Drawing.Point(6, 41);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(13, 13);
            this._switchOn.State = BEMN.Forms.LedState.Off;
            this._switchOn.TabIndex = 49;
            // 
            // _availabilityFaultSystemJournal
            // 
            this._availabilityFaultSystemJournal.Location = new System.Drawing.Point(6, 131);
            this._availabilityFaultSystemJournal.Name = "_availabilityFaultSystemJournal";
            this._availabilityFaultSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._availabilityFaultSystemJournal.State = BEMN.Forms.LedState.Off;
            this._availabilityFaultSystemJournal.TabIndex = 13;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(30, 41);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(122, 13);
            this.label100.TabIndex = 52;
            this.label100.Text = "Выключатель включен";
            // 
            // _newRecordOscJournal
            // 
            this._newRecordOscJournal.Location = new System.Drawing.Point(6, 108);
            this._newRecordOscJournal.Name = "_newRecordOscJournal";
            this._newRecordOscJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordOscJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordOscJournal.TabIndex = 12;
            // 
            // _switchOff
            // 
            this._switchOff.Location = new System.Drawing.Point(6, 19);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(13, 13);
            this._switchOff.State = BEMN.Forms.LedState.Off;
            this._switchOff.TabIndex = 48;
            // 
            // _newRecordAlarmJournal
            // 
            this._newRecordAlarmJournal.Location = new System.Drawing.Point(6, 85);
            this._newRecordAlarmJournal.Name = "_newRecordAlarmJournal";
            this._newRecordAlarmJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordAlarmJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordAlarmJournal.TabIndex = 11;
            // 
            // _newRecordSystemJournal
            // 
            this._newRecordSystemJournal.Location = new System.Drawing.Point(6, 62);
            this._newRecordSystemJournal.Name = "_newRecordSystemJournal";
            this._newRecordSystemJournal.Size = new System.Drawing.Size(13, 13);
            this._newRecordSystemJournal.State = BEMN.Forms.LedState.Off;
            this._newRecordSystemJournal.TabIndex = 10;
            // 
            // _resetAvailabilityFaultSystemJournalButton
            // 
            this._resetAvailabilityFaultSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAvailabilityFaultSystemJournalButton.Location = new System.Drawing.Point(235, 126);
            this._resetAvailabilityFaultSystemJournalButton.Name = "_resetAvailabilityFaultSystemJournalButton";
            this._resetAvailabilityFaultSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAvailabilityFaultSystemJournalButton.TabIndex = 8;
            this._resetAvailabilityFaultSystemJournalButton.Text = "Сбросить";
            this._resetAvailabilityFaultSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetAvailabilityFaultSystemJournalButton.Click += new System.EventHandler(this._resetAvailabilityFaultSystemJournalButton_Click);
            // 
            // _resetOscJournalButton
            // 
            this._resetOscJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetOscJournalButton.Location = new System.Drawing.Point(235, 103);
            this._resetOscJournalButton.Name = "_resetOscJournalButton";
            this._resetOscJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetOscJournalButton.TabIndex = 7;
            this._resetOscJournalButton.Text = "Сбросить";
            this._resetOscJournalButton.UseVisualStyleBackColor = true;
            this._resetOscJournalButton.Click += new System.EventHandler(this._resetOscJournalButton_Click);
            // 
            // _resetAlarmJournalButton
            // 
            this._resetAlarmJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetAlarmJournalButton.Location = new System.Drawing.Point(235, 80);
            this._resetAlarmJournalButton.Name = "_resetAlarmJournalButton";
            this._resetAlarmJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetAlarmJournalButton.TabIndex = 6;
            this._resetAlarmJournalButton.Text = "Сбросить";
            this._resetAlarmJournalButton.UseVisualStyleBackColor = true;
            this._resetAlarmJournalButton.Click += new System.EventHandler(this._resetAlarmJournalButton_Click);
            // 
            // _resetSystemJournalButton
            // 
            this._resetSystemJournalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._resetSystemJournalButton.Location = new System.Drawing.Point(235, 57);
            this._resetSystemJournalButton.Name = "_resetSystemJournalButton";
            this._resetSystemJournalButton.Size = new System.Drawing.Size(75, 23);
            this._resetSystemJournalButton.TabIndex = 5;
            this._resetSystemJournalButton.Text = "Сбросить";
            this._resetSystemJournalButton.UseVisualStyleBackColor = true;
            this._resetSystemJournalButton.Click += new System.EventHandler(this._resetSystemJournalButton_Click);
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label227.Location = new System.Drawing.Point(30, 131);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(166, 13);
            this.label227.TabIndex = 3;
            this.label227.Text = "Наличие неисправности по ЖС";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label225.Location = new System.Drawing.Point(30, 108);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(200, 13);
            this.label225.TabIndex = 2;
            this.label225.Text = "Новая запись журнала осциллографа";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label226.Location = new System.Drawing.Point(30, 85);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(172, 13);
            this.label226.TabIndex = 1;
            this.label226.Text = "Новая запись в журнале аварий";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label231.Location = new System.Drawing.Point(30, 62);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(181, 13);
            this.label231.TabIndex = 0;
            this.label231.Text = "Новая запись в журнале системы";
            // 
            // Mr7MeasuringForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(919, 625);
            this.Controls.Add(this._dataBaseTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Mr7MeasuringForm";
            this.Text = "MeasuringForm";
            this.Activated += new System.EventHandler(this.MeasuringForm_Activated);
            this.Deactivate += new System.EventHandler(this.MeasuringForm_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeasuringForm_FormClosing);
            this.Load += new System.EventHandler(this.MeasuringForm_Load);
            this._analogTabPage.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox67.ResumeLayout(false);
            this.groupBox67.PerformLayout();
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox.ResumeLayout(false);
            this.groupBox.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox47.ResumeLayout(false);
            this.groupBox47.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox45.ResumeLayout(false);
            this.groupBox45.PerformLayout();
            this.groupBox46.ResumeLayout(false);
            this.groupBox46.PerformLayout();
            this._dataBaseTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this._rsTriggersGB.ResumeLayout(false);
            this._rsTriggersGB.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this._diskretInputGB.ResumeLayout(false);
            this._diskretInputGB.PerformLayout();
            this._discretTabPage1.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox54.ResumeLayout(false);
            this.groupBox54.PerformLayout();
            this.BGS_Group.ResumeLayout(false);
            this.BGS_Group.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.splGroupBox.ResumeLayout(false);
            this.splGroupBox.PerformLayout();
            this.dugGroup.ResumeLayout(false);
            this.dugGroup.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._discretTabPage2.ResumeLayout(false);
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.reversGroup1.ResumeLayout(false);
            this.reversGroup1.PerformLayout();
            this.dugGroup1.ResumeLayout(false);
            this.dugGroup1.PerformLayout();
            this.groupBox66.ResumeLayout(false);
            this.groupBox66.PerformLayout();
            this.groupBox64.ResumeLayout(false);
            this.groupBox64.PerformLayout();
            this.urovGroup1.ResumeLayout(false);
            this.urovGroup1.PerformLayout();
            this.groupBox61.ResumeLayout(false);
            this.groupBox61.PerformLayout();
            this.groupBox62.ResumeLayout(false);
            this.groupBox62.PerformLayout();
            this.groupBox58.ResumeLayout(false);
            this.groupBox58.PerformLayout();
            this.groupBox59.ResumeLayout(false);
            this.groupBox59.PerformLayout();
            this.groupBox60.ResumeLayout(false);
            this.groupBox60.PerformLayout();
            this.groupBox55.ResumeLayout(false);
            this.groupBox55.PerformLayout();
            this.groupBox56.ResumeLayout(false);
            this.groupBox56.PerformLayout();
            this.groupBox57.ResumeLayout(false);
            this.groupBox57.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox49.ResumeLayout(false);
            this.groupBox49.PerformLayout();
            this.groupBox48.ResumeLayout(false);
            this.groupBox48.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this._controlSignalsTabPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _dataBaseTabControl;
        private BEMN.Forms.DateTimeControl _dateTimeControl;
        private System.Windows.Forms.TabPage _discretTabPage1;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.Label label86;
        private BEMN.Forms.LedControl _iS1;
        private System.Windows.Forms.Label label85;
        private BEMN.Forms.LedControl _iS2;
        private BEMN.Forms.LedControl _iS1Io;
        private System.Windows.Forms.Label label84;
        private BEMN.Forms.LedControl _iS3;
        private BEMN.Forms.LedControl _iS2Io;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.Label label83;
        private BEMN.Forms.LedControl _iS4;
        private BEMN.Forms.LedControl _iS3Io;
        private System.Windows.Forms.Label label82;
        private BEMN.Forms.LedControl _iS5;
        private BEMN.Forms.LedControl _iS4Io;
        private System.Windows.Forms.Label label81;
        private BEMN.Forms.LedControl _iS6;
        private BEMN.Forms.LedControl _iS5Io;
        private BEMN.Forms.LedControl _iS6Io;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label270;
        private System.Windows.Forms.Label label269;
        private BEMN.Forms.LedControl _i8Io;
        private BEMN.Forms.LedControl _i8;
        private System.Windows.Forms.Label label87;
        private BEMN.Forms.LedControl _i6Io;
        private BEMN.Forms.LedControl _i5Io;
        private BEMN.Forms.LedControl _i6;
        private System.Windows.Forms.Label label89;
        private BEMN.Forms.LedControl _i4Io;
        private BEMN.Forms.LedControl _i5;
        private System.Windows.Forms.Label label90;
        private BEMN.Forms.LedControl _i3Io;
        private BEMN.Forms.LedControl _i4;
        private System.Windows.Forms.Label label91;
        private BEMN.Forms.LedControl _i2Io;
        private BEMN.Forms.LedControl _i3;
        private System.Windows.Forms.Label label92;
        private BEMN.Forms.LedControl _i1Io;
        private BEMN.Forms.LedControl _i2;
        private System.Windows.Forms.Label label93;
        private BEMN.Forms.LedControl _i1;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.GroupBox groupBox10;
        private BEMN.Forms.LedControl _vls16;
        private System.Windows.Forms.Label label63;
        private BEMN.Forms.LedControl _vls15;
        private System.Windows.Forms.Label label64;
        private BEMN.Forms.LedControl _vls14;
        private System.Windows.Forms.Label label65;
        private BEMN.Forms.LedControl _vls13;
        private System.Windows.Forms.Label label66;
        private BEMN.Forms.LedControl _vls12;
        private System.Windows.Forms.Label label67;
        private BEMN.Forms.LedControl _vls11;
        private System.Windows.Forms.Label label68;
        private BEMN.Forms.LedControl _vls10;
        private System.Windows.Forms.Label label69;
        private BEMN.Forms.LedControl _vls9;
        private System.Windows.Forms.Label label70;
        private BEMN.Forms.LedControl _vls8;
        private System.Windows.Forms.Label label71;
        private BEMN.Forms.LedControl _vls7;
        private System.Windows.Forms.Label label72;
        private BEMN.Forms.LedControl _vls6;
        private System.Windows.Forms.Label label73;
        private BEMN.Forms.LedControl _vls5;
        private System.Windows.Forms.Label label74;
        private BEMN.Forms.LedControl _vls4;
        private System.Windows.Forms.Label label75;
        private BEMN.Forms.LedControl _vls3;
        private System.Windows.Forms.Label label76;
        private BEMN.Forms.LedControl _vls2;
        private System.Windows.Forms.Label label77;
        private BEMN.Forms.LedControl _vls1;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox9;
        private BEMN.Forms.LedControl _ls16;
        private System.Windows.Forms.Label label47;
        private BEMN.Forms.LedControl _ls15;
        private System.Windows.Forms.Label label48;
        private BEMN.Forms.LedControl _ls14;
        private System.Windows.Forms.Label label49;
        private BEMN.Forms.LedControl _ls13;
        private System.Windows.Forms.Label label50;
        private BEMN.Forms.LedControl _ls12;
        private System.Windows.Forms.Label label51;
        private BEMN.Forms.LedControl _ls11;
        private System.Windows.Forms.Label label52;
        private BEMN.Forms.LedControl _ls10;
        private System.Windows.Forms.Label label53;
        private BEMN.Forms.LedControl _ls9;
        private System.Windows.Forms.Label label54;
        private BEMN.Forms.LedControl _ls8;
        private System.Windows.Forms.Label label55;
        private BEMN.Forms.LedControl _ls7;
        private System.Windows.Forms.Label label56;
        private BEMN.Forms.LedControl _ls6;
        private System.Windows.Forms.Label label57;
        private BEMN.Forms.LedControl _ls5;
        private System.Windows.Forms.Label label58;
        private BEMN.Forms.LedControl _ls4;
        private System.Windows.Forms.Label label59;
        private BEMN.Forms.LedControl _ls3;
        private System.Windows.Forms.Label label60;
        private BEMN.Forms.LedControl _ls2;
        private System.Windows.Forms.Label label61;
        private BEMN.Forms.LedControl _ls1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TabPage _controlSignalsTabPage;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Button _switchGroupButton;
        private System.Windows.Forms.GroupBox groupBox27;
        private BEMN.Forms.LedControl _availabilityFaultSystemJournal;
        private BEMN.Forms.LedControl _newRecordOscJournal;
        private BEMN.Forms.LedControl _newRecordAlarmJournal;
        private BEMN.Forms.LedControl _newRecordSystemJournal;
        private System.Windows.Forms.Button _resetAvailabilityFaultSystemJournalButton;
        private System.Windows.Forms.Button _resetOscJournalButton;
        private System.Windows.Forms.Button _resetAlarmJournalButton;
        private System.Windows.Forms.Button _resetSystemJournalButton;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox _cosfTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox _qTextBox;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox _pTextBox;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Button _breakerOffBut;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button _breakerOnBut;
        private System.Windows.Forms.Label label101;
        private BEMN.Forms.LedControl _switchOn;
        private BEMN.Forms.LedControl _switchOff;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox _za1Box;
        private System.Windows.Forms.TextBox _zc1Box;
        private System.Windows.Forms.TextBox _zb1Box;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox _zabBox;
        private System.Windows.Forms.TextBox _zcaBox;
        private System.Windows.Forms.TextBox _zbcBox;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.ComboBox _groupCombo;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label _currenGroupLabel;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label rxcaLabel;
        private System.Windows.Forms.Label rxbcLabel;
        private System.Windows.Forms.Label rxabLabel;
        private System.Windows.Forms.Button _startOscBtn;
        private System.Windows.Forms.GroupBox groupBox40;
        private BEMN.Forms.LedControl _logicState;
        private System.Windows.Forms.Button stopLogic;
        private System.Windows.Forms.Button startLogic;
        private System.Windows.Forms.Label label314;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.TextBox _iZa1Box;
        private System.Windows.Forms.TextBox _iZc1Box;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox _iZb1Box;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.TextBox _iZabBox;
        private System.Windows.Forms.TextBox _iZcaBox;
        private System.Windows.Forms.TextBox _iZbcBox;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label330;
        private System.Windows.Forms.Label label331;
        private System.Windows.Forms.Label label332;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.Label label340;
        private System.Windows.Forms.Label label339;
        private System.Windows.Forms.Label label338;
        private System.Windows.Forms.Label label337;
        private System.Windows.Forms.Label label336;
        private System.Windows.Forms.Label label335;
        private System.Windows.Forms.TextBox CN;
        private System.Windows.Forms.TextBox CA;
        private System.Windows.Forms.TextBox BN;
        private System.Windows.Forms.TextBox AN;
        private System.Windows.Forms.TextBox BC;
        private System.Windows.Forms.TextBox AB;
        private System.Windows.Forms.TabPage _discretTabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private BEMN.Forms.LedControl _q2Led;
        private System.Windows.Forms.Label label102;
        private BEMN.Forms.LedControl _q1Led;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.Label label301;
        private System.Windows.Forms.Label label302;
        private BEMN.Forms.LedControl _f4IoLessLed;
        private BEMN.Forms.LedControl _f3IoLessLed;
        private BEMN.Forms.LedControl _f2IoLessLed;
        private BEMN.Forms.LedControl _f4LessLed;
        private System.Windows.Forms.Label label303;
        private BEMN.Forms.LedControl _f1IoLessLed;
        private BEMN.Forms.LedControl _f3LessLed;
        private System.Windows.Forms.Label label304;
        private BEMN.Forms.LedControl _f2LessLed;
        private System.Windows.Forms.Label label305;
        private BEMN.Forms.LedControl _f1LessLed;
        private System.Windows.Forms.Label label306;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.Label label295;
        private System.Windows.Forms.Label label296;
        private BEMN.Forms.LedControl _f4IoMoreLed;
        private BEMN.Forms.LedControl _f3IoMoreLed;
        private BEMN.Forms.LedControl _f2IoMoreLed;
        private BEMN.Forms.LedControl _f4MoreLed;
        private System.Windows.Forms.Label label297;
        private BEMN.Forms.LedControl _f1IoMoreLed;
        private BEMN.Forms.LedControl _f3MoreLed;
        private System.Windows.Forms.Label label298;
        private BEMN.Forms.LedControl _f2MoreLed;
        private System.Windows.Forms.Label label299;
        private BEMN.Forms.LedControl _f1MoreLed;
        private System.Windows.Forms.Label label300;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.Label label289;
        private System.Windows.Forms.Label label290;
        private BEMN.Forms.LedControl _u4IoLessLed;
        private BEMN.Forms.LedControl _u3IoLessLed;
        private BEMN.Forms.LedControl _u2IoLessLed;
        private BEMN.Forms.LedControl _u4LessLed;
        private System.Windows.Forms.Label label291;
        private BEMN.Forms.LedControl _u1IoLessLed;
        private BEMN.Forms.LedControl _u3LessLed;
        private System.Windows.Forms.Label label292;
        private BEMN.Forms.LedControl _u2LessLed;
        private System.Windows.Forms.Label label293;
        private BEMN.Forms.LedControl _u1LessLed;
        private System.Windows.Forms.Label label294;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.Label label283;
        private System.Windows.Forms.Label label284;
        private BEMN.Forms.LedControl _u4IoMoreLed;
        private BEMN.Forms.LedControl _u3IoMoreLed;
        private BEMN.Forms.LedControl _u2IoMoreLed;
        private BEMN.Forms.LedControl _u4MoreLed;
        private System.Windows.Forms.Label label285;
        private BEMN.Forms.LedControl _u1IoMoreLed;
        private BEMN.Forms.LedControl _u3MoreLed;
        private System.Windows.Forms.Label label286;
        private BEMN.Forms.LedControl _u2MoreLed;
        private System.Windows.Forms.Label label287;
        private BEMN.Forms.LedControl _u1MoreLed;
        private System.Windows.Forms.Label label288;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label344;
        private BEMN.Forms.LedControl _faultSwitchOff;
        private BEMN.Forms.LedControl _faultMeasuringf;
        private System.Windows.Forms.Label label96;
        private BEMN.Forms.LedControl _faultAlarmJournal;
        private System.Windows.Forms.Label label192;
        private BEMN.Forms.LedControl _faultOsc;
        private System.Windows.Forms.Label label193;
        private BEMN.Forms.LedControl _faultModule5;
        private System.Windows.Forms.Label label194;
        private BEMN.Forms.LedControl _faultModule4;
        private System.Windows.Forms.Label label195;
        private BEMN.Forms.LedControl _faultModule3;
        private System.Windows.Forms.Label label196;
        private BEMN.Forms.LedControl _faultModule2;
        private System.Windows.Forms.Label label197;
        private BEMN.Forms.LedControl _faultModule1;
        private System.Windows.Forms.Label label198;
        private BEMN.Forms.LedControl _faultSystemJournal;
        private System.Windows.Forms.Label label200;
        private BEMN.Forms.LedControl _faultGroupsOfSetpoints;
        private System.Windows.Forms.Label label201;
        private BEMN.Forms.LedControl _faultSetpoints;
        private System.Windows.Forms.Label label202;
        private BEMN.Forms.LedControl _faultPass;
        private System.Windows.Forms.Label label203;
        private BEMN.Forms.LedControl _faultMeasuringU;
        private System.Windows.Forms.Label label204;
        private BEMN.Forms.LedControl _faultSoftware;
        private System.Windows.Forms.Label label205;
        private BEMN.Forms.LedControl _faultHardware;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.GroupBox groupBox38;
        private System.Windows.Forms.GroupBox groupBox39;
        private BEMN.Forms.LedControl _fSpl1;
        private System.Windows.Forms.Label label315;
        private System.Windows.Forms.Label label316;
        private BEMN.Forms.LedControl _fSpl2;
        private System.Windows.Forms.Label label317;
        private BEMN.Forms.LedControl _fSpl3;
        private BEMN.Forms.LedControl _fSpl4;
        private System.Windows.Forms.Label label320;
        private System.Windows.Forms.Label label319;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.Label label347;
        private BEMN.Forms.LedControl _faultDisable2;
        private System.Windows.Forms.Label label346;
        private System.Windows.Forms.Label label345;
        private System.Windows.Forms.Label label311;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label22;
        private BEMN.Forms.LedControl _faultDisable1;
        private BEMN.Forms.LedControl _faultSwithON;
        private BEMN.Forms.LedControl _faultManage;
        private BEMN.Forms.LedControl _faultOtkaz;
        private BEMN.Forms.LedControl _faultBlockCon;
        private BEMN.Forms.LedControl _faultOut;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.Label label228;
        private BEMN.Forms.LedControl _UabcLow10;
        private System.Windows.Forms.Label label313;
        private BEMN.Forms.LedControl _freqLow40;
        private BEMN.Forms.LedControl _freqHiger60;
        private System.Windows.Forms.Label label312;
        private System.Windows.Forms.GroupBox groupBox12;
        private BEMN.Forms.LedControl _vz16;
        private System.Windows.Forms.Label label111;
        private BEMN.Forms.LedControl _vz15;
        private System.Windows.Forms.Label label112;
        private BEMN.Forms.LedControl _vz14;
        private System.Windows.Forms.Label label113;
        private BEMN.Forms.LedControl _vz13;
        private System.Windows.Forms.Label label114;
        private BEMN.Forms.LedControl _vz12;
        private System.Windows.Forms.Label label115;
        private BEMN.Forms.LedControl _vz11;
        private System.Windows.Forms.Label label116;
        private BEMN.Forms.LedControl _vz10;
        private System.Windows.Forms.Label label117;
        private BEMN.Forms.LedControl _vz9;
        private System.Windows.Forms.Label label118;
        private BEMN.Forms.LedControl _vz8;
        private System.Windows.Forms.Label label119;
        private BEMN.Forms.LedControl _vz7;
        private System.Windows.Forms.Label label120;
        private BEMN.Forms.LedControl _vz6;
        private System.Windows.Forms.Label label121;
        private BEMN.Forms.LedControl _vz5;
        private System.Windows.Forms.Label label122;
        private BEMN.Forms.LedControl _vz4;
        private System.Windows.Forms.Label label123;
        private BEMN.Forms.LedControl _vz3;
        private System.Windows.Forms.Label label124;
        private BEMN.Forms.LedControl _vz2;
        private System.Windows.Forms.Label label125;
        private BEMN.Forms.LedControl _vz1;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox61;
        private BEMN.Forms.LedControl _OnKsAndYppn1;
        private System.Windows.Forms.Label label423;
        private BEMN.Forms.LedControl _UnoUno1;
        private System.Windows.Forms.Label label424;
        private BEMN.Forms.LedControl _UyUno1;
        private System.Windows.Forms.Label label425;
        private BEMN.Forms.LedControl _US1;
        private BEMN.Forms.LedControl _U1noU2y1;
        private System.Windows.Forms.Label label426;
        private System.Windows.Forms.Label label427;
        private System.Windows.Forms.Label label428;
        private BEMN.Forms.LedControl _OS1;
        private System.Windows.Forms.Label label429;
        private BEMN.Forms.LedControl _autoSinchr1;
        private System.Windows.Forms.GroupBox groupBox62;
        private BEMN.Forms.LedControl _readyApv1;
        private System.Windows.Forms.Label label430;
        private BEMN.Forms.LedControl _blockApv1;
        private System.Windows.Forms.Label label431;
        private System.Windows.Forms.Label label432;
        private BEMN.Forms.LedControl _puskApv1;
        private BEMN.Forms.LedControl _krat41;
        private System.Windows.Forms.Label label433;
        private System.Windows.Forms.Label label434;
        private BEMN.Forms.LedControl _turnOnApv1;
        private BEMN.Forms.LedControl _krat31;
        private System.Windows.Forms.Label label435;
        private System.Windows.Forms.Label label436;
        private BEMN.Forms.LedControl _krat11;
        private BEMN.Forms.LedControl _zapretApv1;
        private System.Windows.Forms.Label label437;
        private BEMN.Forms.LedControl _krat21;
        private System.Windows.Forms.Label label438;
        private System.Windows.Forms.GroupBox groupBox58;
        private BEMN.Forms.LedControl _damageA1;
        private System.Windows.Forms.Label label409;
        private System.Windows.Forms.Label label410;
        private BEMN.Forms.LedControl _damageB1;
        private BEMN.Forms.LedControl _damageC1;
        private System.Windows.Forms.Label label411;
        private System.Windows.Forms.GroupBox groupBox59;
        private BEMN.Forms.LedControl _tnObivFaz;
        private System.Windows.Forms.Label l3;
        private BEMN.Forms.LedControl _faultTn3U0;
        private System.Windows.Forms.Label l4;
        private BEMN.Forms.LedControl _externalTnUn;
        private BEMN.Forms.LedControl _faultTnU2;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label label416;
        private System.Windows.Forms.Label l1;
        private BEMN.Forms.LedControl _faultTNmgn1;
        private System.Windows.Forms.Label l6;
        private BEMN.Forms.LedControl _externalTn;
        private System.Windows.Forms.Label l2;
        private BEMN.Forms.LedControl _faultTN1;
        private System.Windows.Forms.GroupBox groupBox60;
        private BEMN.Forms.LedControl _kachanie1;
        private System.Windows.Forms.Label label420;
        private BEMN.Forms.LedControl _inZone1;
        private System.Windows.Forms.Label label421;
        private BEMN.Forms.LedControl _outZone1;
        private System.Windows.Forms.Label label422;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.Label label379;
        private System.Windows.Forms.Label label380;
        private System.Windows.Forms.Label label381;
        private System.Windows.Forms.Label label382;
        private BEMN.Forms.LedControl _r4IoLed1;
        private BEMN.Forms.LedControl _r5Led1;
        private BEMN.Forms.LedControl _r1Led1;
        private BEMN.Forms.LedControl _r3IoLed1;
        private System.Windows.Forms.Label label383;
        private System.Windows.Forms.Label label384;
        private BEMN.Forms.LedControl _r6IoLed1;
        private BEMN.Forms.LedControl _r2IoLed1;
        private BEMN.Forms.LedControl _r6Led1;
        private BEMN.Forms.LedControl _r2Led1;
        private BEMN.Forms.LedControl _r4Led1;
        private System.Windows.Forms.Label label388;
        private System.Windows.Forms.Label label390;
        private BEMN.Forms.LedControl _r3Led1;
        private BEMN.Forms.LedControl _r5IoLed1;
        private BEMN.Forms.LedControl _r1IoLed1;
        private System.Windows.Forms.GroupBox groupBox56;
        private System.Windows.Forms.Label label391;
        private System.Windows.Forms.Label label392;
        private BEMN.Forms.LedControl _iS11;
        private System.Windows.Forms.Label label393;
        private BEMN.Forms.LedControl _iS21;
        private BEMN.Forms.LedControl _iS1Io1;
        private System.Windows.Forms.Label label394;
        private BEMN.Forms.LedControl _iS31;
        private BEMN.Forms.LedControl _iS2Io1;
        private System.Windows.Forms.Label label395;
        private System.Windows.Forms.Label label396;
        private BEMN.Forms.LedControl _iS41;
        private BEMN.Forms.LedControl _iS3Io1;
        private System.Windows.Forms.Label label397;
        private BEMN.Forms.LedControl _iS51;
        private BEMN.Forms.LedControl _iS4Io1;
        private System.Windows.Forms.Label label398;
        private BEMN.Forms.LedControl _iS61;
        private BEMN.Forms.LedControl _iS5Io1;
        private BEMN.Forms.LedControl _iS6Io1;
        private System.Windows.Forms.GroupBox groupBox57;
        private System.Windows.Forms.Label label399;
        private System.Windows.Forms.Label label400;
        private BEMN.Forms.LedControl _i8Io1;
        private BEMN.Forms.LedControl _i81;
        private BEMN.Forms.LedControl _i6Io1;
        private BEMN.Forms.LedControl _i5Io1;
        private BEMN.Forms.LedControl _i61;
        private System.Windows.Forms.Label label403;
        private BEMN.Forms.LedControl _i4Io1;
        private BEMN.Forms.LedControl _i51;
        private System.Windows.Forms.Label label404;
        private BEMN.Forms.LedControl _i3Io1;
        private BEMN.Forms.LedControl _i41;
        private System.Windows.Forms.Label label405;
        private BEMN.Forms.LedControl _i2Io1;
        private BEMN.Forms.LedControl _i31;
        private System.Windows.Forms.Label label406;
        private BEMN.Forms.LedControl _i1Io1;
        private BEMN.Forms.LedControl _i21;
        private System.Windows.Forms.Label label407;
        private BEMN.Forms.LedControl _i11;
        private System.Windows.Forms.Label label408;
        private BEMN.Forms.LedControl _faultLogic;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label88;
        private BEMN.Forms.LedControl _iS7;
        private BEMN.Forms.LedControl _iS7Io;
        private System.Windows.Forms.Label label413;
        private BEMN.Forms.LedControl _iS8;
        private BEMN.Forms.LedControl _iS8Io;
        private System.Windows.Forms.Label label414;
        private BEMN.Forms.LedControl _iS81;
        private BEMN.Forms.LedControl _iS8Io1;
        private System.Windows.Forms.Label label402;
        private BEMN.Forms.LedControl _iS71;
        private BEMN.Forms.LedControl _iS7Io1;
        private System.Windows.Forms.GroupBox urovGroup1;
        private BEMN.Forms.LedControl urov1Led1;
        private System.Windows.Forms.Label label417;
        private BEMN.Forms.LedControl blockUrovLed1;
        private System.Windows.Forms.Label label418;
        private BEMN.Forms.LedControl urov2Led1;
        private System.Windows.Forms.Label label419;
        private System.Windows.Forms.GroupBox groupBox64;
        private BEMN.Forms.LedControl _avrOn1;
        private System.Windows.Forms.Label label385;
        private BEMN.Forms.LedControl _avrBlock1;
        private System.Windows.Forms.Label label386;
        private BEMN.Forms.LedControl _avrOff1;
        private System.Windows.Forms.Label label387;
        private System.Windows.Forms.GroupBox groupBox66;
        private BEMN.Forms.LedControl dvPusk1;
        private System.Windows.Forms.Label label442;
        private BEMN.Forms.LedControl dvBlockN1;
        private BEMN.Forms.LedControl dvBlockQ1;
        private System.Windows.Forms.Label label443;
        private System.Windows.Forms.Label label444;
        private BEMN.Forms.LedControl dvWork1;
        private System.Windows.Forms.Label label445;
        private System.Windows.Forms.GroupBox dugGroup;
        private BEMN.Forms.LedControl dugPusk;
        private System.Windows.Forms.Label label446;
        private System.Windows.Forms.GroupBox dugGroup1;
        private BEMN.Forms.LedControl dugPusk1;
        private System.Windows.Forms.Label label447;
        private System.Windows.Forms.GroupBox reversGroup1;
        private System.Windows.Forms.Label label450;
        private System.Windows.Forms.Label label451;
        private BEMN.Forms.LedControl _P2Io1;
        private BEMN.Forms.LedControl _P1Io1;
        private BEMN.Forms.LedControl _P21;
        private System.Windows.Forms.Label label452;
        private BEMN.Forms.LedControl _P11;
        private System.Windows.Forms.Label label453;
        private System.Windows.Forms.GroupBox BGS_Group;
        private Forms.LedControl _bgs16;
        private System.Windows.Forms.Label label352;
        private Forms.LedControl _bgs15;
        private System.Windows.Forms.Label label412;
        private Forms.LedControl _bgs14;
        private System.Windows.Forms.Label label454;
        private Forms.LedControl _bgs13;
        private System.Windows.Forms.Label label457;
        private Forms.LedControl _bgs12;
        private System.Windows.Forms.Label label458;
        private Forms.LedControl _bgs11;
        private System.Windows.Forms.Label label459;
        private Forms.LedControl _bgs10;
        private System.Windows.Forms.Label label460;
        private Forms.LedControl _bgs9;
        private System.Windows.Forms.Label label461;
        private Forms.LedControl _bgs8;
        private System.Windows.Forms.Label label462;
        private Forms.LedControl _bgs7;
        private System.Windows.Forms.Label label463;
        private Forms.LedControl _bgs6;
        private System.Windows.Forms.Label label464;
        private Forms.LedControl _bgs5;
        private System.Windows.Forms.Label label465;
        private Forms.LedControl _bgs4;
        private System.Windows.Forms.Label label466;
        private Forms.LedControl _bgs3;
        private System.Windows.Forms.Label label467;
        private Forms.LedControl _bgs2;
        private System.Windows.Forms.Label label468;
        private Forms.LedControl _bgs1;
        private System.Windows.Forms.Label label469;
        private System.Windows.Forms.GroupBox groupBox54;
        private Forms.LedControl _faultHardware1;
        private System.Windows.Forms.Label label372;
        private Forms.LedControl _faultLogic1;
        private System.Windows.Forms.Label label12;
        private Forms.LedControl _faultSwitchOff1;
        private System.Windows.Forms.Label label378;
        private Forms.LedControl _faultMeasuringF1;
        private System.Windows.Forms.Label label375;
        private Forms.LedControl _faultMeasuringU1;
        private System.Windows.Forms.Label label376;
        private Forms.LedControl _faultSoftware1;
        private System.Windows.Forms.Label label377;
        private System.Windows.Forms.GroupBox groupBox14;
        private Forms.LedControl _faultMain;
        private System.Windows.Forms.Label label210;
        private Forms.LedControl _faultOffMain;
        private System.Windows.Forms.Label label21;
        private Forms.LedControl _accelerationMain;
        private System.Windows.Forms.Label _auto4Led;
        private System.Windows.Forms.GroupBox splGroupBox;
        private Forms.LedControl _ssl48;
        private Forms.LedControl _ssl40;
        private Forms.LedControl _ssl32;
        private System.Windows.Forms.Label _sslLabel48;
        private System.Windows.Forms.Label _sslLabel40;
        private System.Windows.Forms.Label label237;
        private Forms.LedControl _ssl47;
        private Forms.LedControl _ssl39;
        private Forms.LedControl _ssl31;
        private System.Windows.Forms.Label _sslLabel47;
        private System.Windows.Forms.Label _sslLabel39;
        private System.Windows.Forms.Label label238;
        private Forms.LedControl _ssl46;
        private Forms.LedControl _ssl38;
        private Forms.LedControl _ssl30;
        private System.Windows.Forms.Label _sslLabel46;
        private System.Windows.Forms.Label _sslLabel38;
        private System.Windows.Forms.Label label239;
        private Forms.LedControl _ssl45;
        private Forms.LedControl _ssl37;
        private Forms.LedControl _ssl29;
        private System.Windows.Forms.Label _sslLabel45;
        private System.Windows.Forms.Label _sslLabel37;
        private System.Windows.Forms.Label label240;
        private Forms.LedControl _ssl44;
        private Forms.LedControl _ssl36;
        private Forms.LedControl _ssl28;
        private System.Windows.Forms.Label _sslLabel44;
        private System.Windows.Forms.Label _sslLabel36;
        private System.Windows.Forms.Label label241;
        private Forms.LedControl _ssl43;
        private Forms.LedControl _ssl35;
        private Forms.LedControl _ssl27;
        private System.Windows.Forms.Label _sslLabel43;
        private System.Windows.Forms.Label _sslLabel35;
        private System.Windows.Forms.Label label242;
        private Forms.LedControl _ssl42;
        private Forms.LedControl _ssl34;
        private Forms.LedControl _ssl26;
        private System.Windows.Forms.Label _sslLabel42;
        private System.Windows.Forms.Label _sslLabel34;
        private System.Windows.Forms.Label label243;
        private Forms.LedControl _ssl41;
        private System.Windows.Forms.Label _sslLabel41;
        private Forms.LedControl _ssl33;
        private System.Windows.Forms.Label _sslLabel33;
        private Forms.LedControl _ssl25;
        private System.Windows.Forms.Label label244;
        private Forms.LedControl _ssl24;
        private System.Windows.Forms.Label label245;
        private Forms.LedControl _ssl23;
        private System.Windows.Forms.Label label246;
        private Forms.LedControl _ssl22;
        private System.Windows.Forms.Label label247;
        private Forms.LedControl _ssl21;
        private System.Windows.Forms.Label label248;
        private Forms.LedControl _ssl20;
        private System.Windows.Forms.Label label249;
        private Forms.LedControl _ssl19;
        private System.Windows.Forms.Label label250;
        private Forms.LedControl _ssl18;
        private System.Windows.Forms.Label label251;
        private Forms.LedControl _ssl17;
        private System.Windows.Forms.Label label252;
        private Forms.LedControl _ssl16;
        private System.Windows.Forms.Label label253;
        private Forms.LedControl _ssl15;
        private System.Windows.Forms.Label label254;
        private Forms.LedControl _ssl14;
        private System.Windows.Forms.Label label255;
        private Forms.LedControl _ssl13;
        private System.Windows.Forms.Label label256;
        private Forms.LedControl _ssl12;
        private System.Windows.Forms.Label label257;
        private Forms.LedControl _ssl11;
        private System.Windows.Forms.Label label258;
        private Forms.LedControl _ssl10;
        private System.Windows.Forms.Label label259;
        private Forms.LedControl _ssl9;
        private System.Windows.Forms.Label label260;
        private Forms.LedControl _ssl8;
        private System.Windows.Forms.Label label261;
        private Forms.LedControl _ssl7;
        private System.Windows.Forms.Label label262;
        private Forms.LedControl _ssl6;
        private System.Windows.Forms.Label label263;
        private Forms.LedControl _ssl5;
        private System.Windows.Forms.Label label264;
        private Forms.LedControl _ssl4;
        private System.Windows.Forms.Label label265;
        private Forms.LedControl _ssl3;
        private System.Windows.Forms.Label label266;
        private Forms.LedControl _ssl2;
        private System.Windows.Forms.Label label267;
        private Forms.LedControl _ssl1;
        private System.Windows.Forms.Label label268;
        private Forms.LedControl _externalTnUn1;
        private System.Windows.Forms.Label _externalTnUn1Label;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox _percentIdc;
        private System.Windows.Forms.TextBox _percentIdb;
        private System.Windows.Forms.TextBox _percentIda;
        private System.Windows.Forms.TextBox _Idc;
        private System.Windows.Forms.TextBox _Idb;
        private System.Windows.Forms.TextBox _Ida;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox _percentIbc;
        private System.Windows.Forms.TextBox _percentIbb;
        private System.Windows.Forms.TextBox _percentIba;
        private System.Windows.Forms.TextBox _Ibc;
        private System.Windows.Forms.TextBox _Ibb;
        private System.Windows.Forms.TextBox _Iba;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.GroupBox groupBox67;
        private System.Windows.Forms.Label label504;
        private System.Windows.Forms.TextBox _U2abcC;
        private System.Windows.Forms.Label label502;
        private System.Windows.Forms.TextBox _U2abcB;
        private System.Windows.Forms.TextBox _U2abcA;
        private System.Windows.Forms.TextBox _U2ca;
        private System.Windows.Forms.TextBox _U2bc;
        private System.Windows.Forms.TextBox _U2ab;
        private System.Windows.Forms.Label label500;
        private System.Windows.Forms.Label label494;
        private System.Windows.Forms.Label label496;
        private System.Windows.Forms.Label label498;
        private System.Windows.Forms.GroupBox groupBox44;
        private System.Windows.Forms.Label label495;
        private System.Windows.Forms.Label label497;
        private System.Windows.Forms.TextBox _U1abcC;
        private System.Windows.Forms.TextBox _U1abcB;
        private System.Windows.Forms.TextBox _U1abcA;
        private System.Windows.Forms.TextBox _U1ca;
        private System.Windows.Forms.TextBox _U1bc;
        private System.Windows.Forms.TextBox _U1ab;
        private System.Windows.Forms.Label _U10;
        private System.Windows.Forms.Label label499;
        private System.Windows.Forms.Label label501;
        private System.Windows.Forms.Label label503;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.Label label482;
        private System.Windows.Forms.TextBox _Is42;
        private System.Windows.Forms.TextBox _Is40;
        private System.Windows.Forms.TextBox _Is4n;
        private System.Windows.Forms.TextBox _Is4c;
        private System.Windows.Forms.TextBox _Is4b;
        private System.Windows.Forms.TextBox _Is4a;
        private System.Windows.Forms.Label label483;
        private System.Windows.Forms.Label label484;
        private System.Windows.Forms.Label label485;
        private System.Windows.Forms.Label label486;
        private System.Windows.Forms.Label label488;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.Label label470;
        private System.Windows.Forms.TextBox _Is32;
        private System.Windows.Forms.TextBox _Is30;
        private System.Windows.Forms.TextBox _Is3n;
        private System.Windows.Forms.TextBox _Is3c;
        private System.Windows.Forms.TextBox _Is3b;
        private System.Windows.Forms.TextBox _Is3a;
        private System.Windows.Forms.Label label471;
        private System.Windows.Forms.Label label472;
        private System.Windows.Forms.Label label473;
        private System.Windows.Forms.Label label474;
        private System.Windows.Forms.Label label476;
        private System.Windows.Forms.GroupBox groupBox37;
        private System.Windows.Forms.Label label323;
        private System.Windows.Forms.TextBox _Is22;
        private System.Windows.Forms.TextBox _Is20;
        private System.Windows.Forms.TextBox _Is2n;
        private System.Windows.Forms.TextBox _Is2c;
        private System.Windows.Forms.TextBox _Is2b;
        private System.Windows.Forms.TextBox _Is2a;
        private System.Windows.Forms.Label label324;
        private System.Windows.Forms.Label label325;
        private System.Windows.Forms.Label label326;
        private System.Windows.Forms.Label label327;
        private System.Windows.Forms.Label label329;
        private System.Windows.Forms.GroupBox groupBox36;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox _Is12;
        private System.Windows.Forms.TextBox _Is10;
        private System.Windows.Forms.TextBox _Is1n;
        private System.Windows.Forms.TextBox _Is1c;
        private System.Windows.Forms.TextBox _Is1b;
        private System.Windows.Forms.TextBox _Is1a;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox18;
        private Forms.Diod diod12;
        private Forms.Diod diod11;
        private Forms.Diod diod10;
        private Forms.Diod diod9;
        private Forms.Diod diod8;
        private Forms.Diod diod7;
        private Forms.Diod diod6;
        private Forms.Diod diod5;
        private Forms.Diod diod4;
        private Forms.Diod diod3;
        private Forms.Diod diod2;
        private Forms.Diod diod1;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private Forms.LedControl _module40;
        private Forms.LedControl _module51;
        private Forms.LedControl _module39;
        private System.Windows.Forms.Label _r40Label;
        private Forms.LedControl _module46;
        private Forms.LedControl _module34;
        private System.Windows.Forms.Label _r51Label;
        private System.Windows.Forms.Label _r39Label;
        private System.Windows.Forms.Label _r46Label;
        private System.Windows.Forms.Label _r34Label;
        private Forms.LedControl _module50;
        private Forms.LedControl _module38;
        private Forms.LedControl _module45;
        private Forms.LedControl _module33;
        private System.Windows.Forms.Label _r50Label;
        private System.Windows.Forms.Label _r38Label;
        private System.Windows.Forms.Label _r45Label;
        private System.Windows.Forms.Label _r33Label;
        private Forms.LedControl _module18;
        private Forms.LedControl _module10;
        private System.Windows.Forms.Label _r18Label;
        private Forms.LedControl _module49;
        private Forms.LedControl _module37;
        private Forms.LedControl _module44;
        private Forms.LedControl _module32;
        private Forms.LedControl _module17;
        private System.Windows.Forms.Label _r49Label;
        private System.Windows.Forms.Label _r37Label;
        private System.Windows.Forms.Label _r44Label;
        private System.Windows.Forms.Label _r32Label;
        private System.Windows.Forms.Label label162;
        private Forms.LedControl _module48;
        private Forms.LedControl _module36;
        private Forms.LedControl _module43;
        private Forms.LedControl _module31;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label _r48Label;
        private System.Windows.Forms.Label _r36Label;
        private System.Windows.Forms.Label _r43Label;
        private System.Windows.Forms.Label _r31Label;
        private Forms.LedControl _module9;
        private Forms.LedControl _module47;
        private Forms.LedControl _module35;
        private Forms.LedControl _module42;
        private Forms.LedControl _module30;
        private System.Windows.Forms.Label _r47Label;
        private System.Windows.Forms.Label _r35Label;
        private Forms.LedControl _module16;
        private System.Windows.Forms.Label _r42Label;
        private System.Windows.Forms.Label _r30Label;
        private System.Windows.Forms.Label label165;
        private Forms.LedControl _module41;
        private Forms.LedControl _module29;
        private System.Windows.Forms.Label _r41Label;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label _r29Label;
        private Forms.LedControl _module15;
        private Forms.LedControl _module28;
        private Forms.LedControl _module5;
        private System.Windows.Forms.Label _r28Label;
        private System.Windows.Forms.Label label169;
        private Forms.LedControl _module27;
        private Forms.LedControl _module8;
        private System.Windows.Forms.Label _r27Label;
        private Forms.LedControl _module14;
        private Forms.LedControl _module26;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label _r26Label;
        private System.Windows.Forms.Label label170;
        private Forms.LedControl _module25;
        private System.Windows.Forms.Label _r25Label;
        private System.Windows.Forms.Label label166;
        private Forms.LedControl _module13;
        private Forms.LedControl _module24;
        private Forms.LedControl _module1;
        private System.Windows.Forms.Label _r24Label;
        private System.Windows.Forms.Label label171;
        private Forms.LedControl _module23;
        private Forms.LedControl _module7;
        private System.Windows.Forms.Label _r23Label;
        private Forms.LedControl _module12;
        private Forms.LedControl _module22;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label _r22Label;
        private System.Windows.Forms.Label label177;
        private Forms.LedControl _module21;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label _r21Label;
        private Forms.LedControl _module11;
        private Forms.LedControl _module20;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label _r20Label;
        private Forms.LedControl _module2;
        private Forms.LedControl _module19;
        private System.Windows.Forms.Label _r19Label;
        private Forms.LedControl _module6;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label174;
        private Forms.LedControl _module3;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label172;
        private Forms.LedControl _module4;
        private System.Windows.Forms.GroupBox _diskretInputGB;
        private Forms.LedControl _d48;
        private Forms.LedControl _d40;
        private Forms.LedControl _d24;
        private System.Windows.Forms.Label _d48Label;
        private System.Windows.Forms.Label _d40Label;
        private Forms.LedControl _d8;
        private Forms.LedControl _d47;
        private Forms.LedControl _d39;
        private System.Windows.Forms.Label _d24Label;
        private System.Windows.Forms.Label _d47Label;
        private System.Windows.Forms.Label _d39Label;
        private Forms.LedControl _d54;
        private Forms.LedControl _d46;
        private Forms.LedControl _d38;
        private Forms.LedControl _d23;
        private System.Windows.Forms.Label _d54Label;
        private System.Windows.Forms.Label _d46Label;
        private System.Windows.Forms.Label _d38Label;
        private System.Windows.Forms.Label _d8Label;
        private Forms.LedControl _d53;
        private Forms.LedControl _d45;
        private Forms.LedControl _d37;
        private System.Windows.Forms.Label _d23Label;
        private System.Windows.Forms.Label _d53Label;
        private System.Windows.Forms.Label _d45Label;
        private System.Windows.Forms.Label _d37Label;
        private Forms.LedControl _d22;
        private Forms.LedControl _d52;
        private Forms.LedControl _d44;
        private Forms.LedControl _d36;
        private Forms.LedControl _d7;
        private System.Windows.Forms.Label _d52Label;
        private System.Windows.Forms.Label _d44Label;
        private System.Windows.Forms.Label _d36Label;
        private System.Windows.Forms.Label _d22Label;
        private Forms.LedControl _d51;
        private Forms.LedControl _d43;
        private Forms.LedControl _d35;
        private System.Windows.Forms.Label _d7Label;
        private System.Windows.Forms.Label _d51Label;
        private System.Windows.Forms.Label _d43Label;
        private System.Windows.Forms.Label _d35Label;
        private Forms.LedControl _d21;
        private Forms.LedControl _d50;
        private Forms.LedControl _d42;
        private Forms.LedControl _d34;
        private Forms.LedControl _d1;
        private System.Windows.Forms.Label _d50Label;
        private System.Windows.Forms.Label _d42Label;
        private System.Windows.Forms.Label _d34Label;
        private System.Windows.Forms.Label _d21Label;
        private Forms.LedControl _k2;
        private System.Windows.Forms.Label _k2Label;
        private Forms.LedControl _k1;
        private System.Windows.Forms.Label _k1Label;
        private Forms.LedControl _d49;
        private System.Windows.Forms.Label _d49Label;
        private Forms.LedControl _d41;
        private System.Windows.Forms.Label _d41Label;
        private Forms.LedControl _d33;
        private System.Windows.Forms.Label _d33Label;
        private Forms.LedControl _d6;
        private Forms.LedControl _d20;
        private Forms.LedControl _d32;
        private System.Windows.Forms.Label _d1Label;
        private System.Windows.Forms.Label _d32Label;
        private System.Windows.Forms.Label _d20Label;
        private Forms.LedControl _d31;
        private System.Windows.Forms.Label _d6Label;
        private System.Windows.Forms.Label _d31Label;
        private Forms.LedControl _d19;
        private Forms.LedControl _d30;
        private System.Windows.Forms.Label _d2Label;
        private System.Windows.Forms.Label _d30Label;
        private System.Windows.Forms.Label _d19Label;
        private Forms.LedControl _d29;
        private Forms.LedControl _d5;
        private System.Windows.Forms.Label _d29Label;
        private Forms.LedControl _d18;
        private Forms.LedControl _d28;
        private Forms.LedControl _d2;
        private System.Windows.Forms.Label _d28Label;
        private System.Windows.Forms.Label _d18Label;
        private Forms.LedControl _d27;
        private System.Windows.Forms.Label _d5Label;
        private System.Windows.Forms.Label _d27Label;
        private Forms.LedControl _d17;
        private Forms.LedControl _d26;
        private System.Windows.Forms.Label _d17Label;
        private System.Windows.Forms.Label _d26Label;
        private System.Windows.Forms.Label _d3Label;
        private Forms.LedControl _d25;
        private System.Windows.Forms.Label _d25Label;
        private Forms.LedControl _d4;
        private Forms.LedControl _d16;
        private Forms.LedControl _d3;
        private System.Windows.Forms.Label _d16Label;
        private System.Windows.Forms.Label _d4Label;
        private Forms.LedControl _d15;
        private Forms.LedControl _d9;
        private System.Windows.Forms.Label _d15Label;
        private System.Windows.Forms.Label _d9Label;
        private Forms.LedControl _d14;
        private System.Windows.Forms.Label _d10Label;
        private System.Windows.Forms.Label _d14Label;
        private Forms.LedControl _d10;
        private Forms.LedControl _d13;
        private System.Windows.Forms.Label _d11Label;
        private System.Windows.Forms.Label _d13Label;
        private Forms.LedControl _d11;
        private Forms.LedControl _d12;
        private System.Windows.Forms.Label _d12Label;
        private Forms.LedControl _module69;
        private Forms.LedControl _module57;
        private Forms.LedControl _module80;
        private Forms.LedControl _module68;
        private System.Windows.Forms.Label _r69Label;
        private Forms.LedControl _module75;
        private Forms.LedControl _module63;
        private System.Windows.Forms.Label _r57Label;
        private System.Windows.Forms.Label _r80Label;
        private System.Windows.Forms.Label _r68Label;
        private System.Windows.Forms.Label _r75Label;
        private System.Windows.Forms.Label _r63Label;
        private Forms.LedControl _module55;
        private Forms.LedControl _module79;
        private Forms.LedControl _module67;
        private Forms.LedControl _module74;
        private Forms.LedControl _module62;
        private System.Windows.Forms.Label _r55Label;
        private System.Windows.Forms.Label _r79Label;
        private System.Windows.Forms.Label _r67Label;
        private System.Windows.Forms.Label _r74Label;
        private System.Windows.Forms.Label _r62Label;
        private Forms.LedControl _module54;
        private Forms.LedControl _module78;
        private Forms.LedControl _module66;
        private Forms.LedControl _module73;
        private Forms.LedControl _module61;
        private System.Windows.Forms.Label _r54Label;
        private System.Windows.Forms.Label _r78Label;
        private System.Windows.Forms.Label _r66Label;
        private System.Windows.Forms.Label _r73Label;
        private System.Windows.Forms.Label _r61Label;
        private Forms.LedControl _module53;
        private Forms.LedControl _module77;
        private Forms.LedControl _module65;
        private Forms.LedControl _module72;
        private Forms.LedControl _module60;
        private System.Windows.Forms.Label _r53Label;
        private System.Windows.Forms.Label _r77Label;
        private System.Windows.Forms.Label _r65Label;
        private System.Windows.Forms.Label _r72Label;
        private System.Windows.Forms.Label _r60Label;
        private Forms.LedControl _module52;
        private Forms.LedControl _module76;
        private Forms.LedControl _module64;
        private Forms.LedControl _module71;
        private System.Windows.Forms.Label _r52Label;
        private Forms.LedControl _module59;
        private System.Windows.Forms.Label _r76Label;
        private System.Windows.Forms.Label _r64Label;
        private System.Windows.Forms.Label _r71Label;
        private System.Windows.Forms.Label _r59Label;
        private Forms.LedControl _module70;
        private Forms.LedControl _module58;
        private System.Windows.Forms.Label _r70Label;
        private System.Windows.Forms.Label _r58Label;
        private Forms.LedControl _module56;
        private System.Windows.Forms.Label _r56Label;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox _U2abc0;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox _U2abc2;
        private System.Windows.Forms.TextBox _U2abc1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _U1abc0;
        private System.Windows.Forms.TextBox _U1abc2;
        private System.Windows.Forms.TextBox _U1abc1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button ResetInd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _voltageTB;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _frequency;
        private System.Windows.Forms.TabPage _analogTabPage;
        private Forms.LedControl _faultModule6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ResetSamopodBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button _reserveGroupButton;
        private System.Windows.Forms.Button _mainGroupButton;
        private System.Windows.Forms.Label label490;
        private System.Windows.Forms.Label label491;
        private Forms.LedControl _reservedGroup;
        private Forms.LedControl _mainGroup;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button _commandBtn;
        private System.Windows.Forms.ComboBox _commandComboBox;
        private System.Windows.Forms.GroupBox _virtualReleGB;
        private System.Windows.Forms.GroupBox _releGB;
        private System.Windows.Forms.GroupBox groupBox6;
        private Forms.LedControl _IdMax1Main;
        private Forms.LedControl _IdMax1IOMain;
        private Forms.LedControl _IdMax2Main;
        private Forms.LedControl _IdMax2IOMain;
        private Forms.LedControl _IdMax2MgnMain;
        private System.Windows.Forms.Label _IdMax1L;
        private System.Windows.Forms.Label _IdMax1IOL;
        private System.Windows.Forms.Label _IdMax2L;
        private System.Windows.Forms.Label _IdMax2IOL;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox _rsTriggersGB;
        private Forms.LedControl _rst8;
        private System.Windows.Forms.Label label32;
        private Forms.LedControl _rst7;
        private System.Windows.Forms.Label label40;
        private Forms.LedControl _rst1;
        private Forms.LedControl _rst6;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label140;
        private Forms.LedControl _rst5;
        private Forms.LedControl _rst2;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label161;
        private Forms.LedControl _rst4;
        private Forms.LedControl _rst16;
        private Forms.LedControl _rst3;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.Label label208;
        private Forms.LedControl _rst15;
        private Forms.LedControl _rst9;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.Label label211;
        private Forms.LedControl _rst14;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.Label label215;
        private Forms.LedControl _rst10;
        private Forms.LedControl _rst13;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.Label label217;
        private Forms.LedControl _rst11;
        private Forms.LedControl _rst12;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TextBox _Is4I1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox _Is3I1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox _Is2I1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox _Is1I1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.TextBox _dU;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox _dFi;
        private System.Windows.Forms.TextBox _dF;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.GroupBox groupBox17;
        private Forms.LedControl _vz16m;
        private System.Windows.Forms.Label label4;
        private Forms.LedControl _vz15m;
        private System.Windows.Forms.Label label15;
        private Forms.LedControl _vz14m;
        private System.Windows.Forms.Label label16;
        private Forms.LedControl _vz13m;
        private System.Windows.Forms.Label label17;
        private Forms.LedControl _vz12m;
        private System.Windows.Forms.Label label18;
        private Forms.LedControl _vz11m;
        private System.Windows.Forms.Label label20;
        private Forms.LedControl _vz10m;
        private System.Windows.Forms.Label label31;
        private Forms.LedControl _vz9m;
        private System.Windows.Forms.Label label33;
        private Forms.LedControl _vz8m;
        private System.Windows.Forms.Label label34;
        private Forms.LedControl _vz7m;
        private System.Windows.Forms.Label label35;
        private Forms.LedControl _vz6m;
        private System.Windows.Forms.Label label36;
        private Forms.LedControl _vz5m;
        private System.Windows.Forms.Label label37;
        private Forms.LedControl _vz4m;
        private System.Windows.Forms.Label label38;
        private Forms.LedControl _vz3m;
        private System.Windows.Forms.Label label39;
        private Forms.LedControl _vz2m;
        private System.Windows.Forms.Label label41;
        private Forms.LedControl _vz1m;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.GroupBox groupBox20;
        private Forms.LedControl _fault;
        private System.Windows.Forms.Label label43;
        private Forms.LedControl _faultOff;
        private System.Windows.Forms.Label label44;
        private Forms.LedControl _acceleration;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox22;
        private Forms.LedControl _IdMax1;
        private Forms.LedControl _IdMax2;
        private Forms.LedControl _IdMax2IO;
        private Forms.LedControl _IdMax1IO;
        private Forms.LedControl _IdMax2Mgn;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private Forms.LedControl _Id0Max2;
        private Forms.LedControl _Id0Max2IO;
        private Forms.LedControl _Id0Max3;
        private Forms.LedControl _Id0Max3IO;
        private Forms.LedControl _Id0Max1;
        private Forms.LedControl _Id0Max1IO;
        private System.Windows.Forms.Label label130;
    }
}