﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR7.Measuring.Structures
{
    public class AnalogDataBaseStruct : StructBase
    {
        #region [Private fields]
        //дифф. ток основная гармоника
        [Layout(0)] private int _ida; // ток A
        [Layout(1)] private int _idb; // ток B
        [Layout(2)] private int _idc; // ток C
        //дифф. ток 2-я гармоника
        [Layout(3)] private int _id2a; // ток A
        [Layout(4)] private int _id2b; // ток B
        [Layout(5)] private int _id2c; // ток C
        //дифф. ток 5-я гармоника
        [Layout(6)] private int _id5a; // ток A
        [Layout(7)] private int _id5b; // ток B
        [Layout(8)] private int _id5c; // ток C
        //тормозной ток
        [Layout(9)] private int _iba; // ток A
        [Layout(10)] private int _ibb; // ток B
        [Layout(11)] private int _ibc; // ток C
        //ток стороны 1
        [Layout(12)] private int _is1a2; // ток A 2 гармоника ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(13)] private int _is1b2; // ток B 2 гармоника
        [Layout(14)] private int _is1c2; // ток C 2 гармоника
        [Layout(15)] private int _is1a; // ток A
        [Layout(16)] private int _is1b; // ток B
        [Layout(17)] private int _is1c; // ток C
        [Layout(18)] private int _is1n; // ток N
        [Layout(19)] private int _is10; // ток 0
        [Layout(20)] private int _is12; // ток 2
        [Layout(21)] private int _is11; // ток 1
        [Layout(22)] private int _is1d0; // дифф. ток 0 стороны 1
        [Layout(23)] private int _is1b0; // тормозной ток 0 стороны 1
        //ток стороны 2
        [Layout(24)] private int _is2a2; // ток A 2 гармоника ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(25)] private int _is2b2; // ток B 2 гармоника
        [Layout(26)] private int _is2c2; // ток C 2 гармоника
        [Layout(27)] private int _is2a; // ток A
        [Layout(28)] private int _is2b; // ток B
        [Layout(29)] private int _is2c; // ток C
        [Layout(30)] private int _is2n; // ток N
        [Layout(31)] private int _is20; // ток 0
        [Layout(32)] private int _is22; // ток 2
        [Layout(33)] private int _is21; // ток 1
        [Layout(34)] private int _is2d0; // дифф. ток 0 стороны 1
        [Layout(35)] private int _is2b0; // тормозной ток 0 стороны 1
        //ток стороны 3
        [Layout(36)] private int _is3a2; // ток A 2 гармоника ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(37)] private int _is3b2; // ток B 2 гармоника
        [Layout(38)] private int _is3c2; // ток C 2 гармоника
        [Layout(39)] private int _is3a; // ток A
        [Layout(40)] private int _is3b; // ток B
        [Layout(41)] private int _is3c; // ток C
        [Layout(42)] private int _is3n; // ток N
        [Layout(43)] private int _is30; // ток 0
        [Layout(44)] private int _is32; // ток 2
        [Layout(45)] private int _is31; // ток 1
        [Layout(46)] private int _is3d0; // дифф. ток 0 стороны 1
        [Layout(47)] private int _is3b0; // тормозной ток 0 стороны 1
        //ток стороны 4
        [Layout(48)] private int _is4a2; // ток A 2 гармоника ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(49)] private int _is4b2; // ток B 2 гармоника
        [Layout(50)] private int _is4c2; // ток C 2 гармоника
        [Layout(51)] private int _is4a; // ток A
        [Layout(52)] private int _is4b; // ток B
        [Layout(53)] private int _is4c; // ток C
        [Layout(54)] private int _is4n; // ток N
        [Layout(55)] private int _is40; // ток 0
        [Layout(56)] private int _is42; // ток 2
        [Layout(57)] private int _is41; // ток 1
        [Layout(58)] private int _is4d0; // дифф. ток 0 стороны 1
        [Layout(59)] private int _is4b0; // тормозной ток 0 стороны 1
        //канал U1
        [Layout(60)] private int _u1a; // напряжение A ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(61)] private int _u1b; // напряжение B
        [Layout(62)] private int _u1c; // напряжение C
        //напряжение 1
        [Layout(63)] private int _u1ab; // напряжение ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(64)] private int _u1bc; // напряжение B
        [Layout(65)] private int _u1ca; // напряжение C
        [Layout(66)] private int _u10; // напряжение 0
        [Layout(67)] private int _u12; // напряжение 2
        [Layout(68)] private int _u11; // напряжение 1
        //канал U2
        [Layout(69)] private int _u2a; // напряжение A ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(70)] private int _u2b; // напряжение B
        [Layout(71)] private int _u2c; // напряжение C
        //напряжение 2
        [Layout(72)] private int _u2ab; // напряжение ПОСОСЛЕДОВАТЕЛЬНОСТЬ ВНУТРИ СТОРОНЫ НЕ МЕНЯТЬ
        [Layout(73)] private int _u2bc; // напряжение B
        [Layout(74)] private int _u2ca; // напряжение C
        [Layout(75)] private int _u20; // напряжение 0
        [Layout(76)] private int _u22; // напряжение 2
        [Layout(77)] private int _u21; // напряжение 1
        //канал Un
        [Layout(78)] private int _un; // напряжение 
        //физические каналы
        [Layout(79)] private int _k1; 
        [Layout(80)] private int _k2;
        [Layout(81)] private int _k3;
        [Layout(82)] private int _k4;
        [Layout(83)] private int _k5;
        [Layout(84)] private int _k6;
        [Layout(85)] private int _k7;
        [Layout(86)] private int _k8;
        [Layout(87)] private int _k9;
        [Layout(88)] private int _k10;
        [Layout(89)] private int _k11;
        [Layout(90)] private int _k12;
        [Layout(91)] private int _k13;
        [Layout(92)] private int _k14;
        [Layout(93)] private int _k15;
        [Layout(94)] private int _k16;
        [Layout(95)] private int _k17;
        [Layout(96)] private int _k18;
        //дополнительные лин. токи для сопротивлений
        [Layout(97)] private int _iabd;
        [Layout(98)] private int _ibcd;
        [Layout(99)] private int _icad;
        // Сопротивления
        [Layout(100)] private int _zab;
        [Layout(101)] private int _zbc;
        [Layout(102)] private int _zca;
        [Layout(103)] private int _zal;
        [Layout(104)] private int _zbl;
        [Layout(105)] private int _zcl;
        [Layout(106)] private int _za;
        [Layout(107)] private int _zb;
        [Layout(108)] private int _zc;
        [Layout(109)] private int _rab;
        [Layout(110)] private int _xab;
        [Layout(111)] private int _rbc;
        [Layout(112)] private int _xbc;
        [Layout(113)] private int _rca;
        [Layout(114)] private int _xca;
        [Layout(115)] private int _ral;
        [Layout(116)] private int _xal;
        [Layout(117)] private int _rbl;
        [Layout(118)] private int _xbl;
        [Layout(119)] private int _rcl;
        [Layout(120)] private int _xcl;
        [Layout(121)] private int _ra;
        [Layout(122)] private int _xa;
        [Layout(123)] private int _rb;
        [Layout(124)] private int _xb;
        [Layout(125)] private int _rc;
        [Layout(126)] private int _xc;
        //разное
        [Layout(127)] private int _f;		
        [Layout(128)] private int _qt;
        [Layout(129)] private int _qn;
        [Layout(130)] private int _qnhot;
        [Layout(131)] private int _dU;
        [Layout(132)] private int _dFi;
        [Layout(133)] private int _dF;
        [Layout(134)] private int _dFdt;
        [Layout(135)] private int _dGroopUst;
        [Layout(136)] private int _powCos;
        [Layout(137)] private int _powP;
        [Layout(138)] private int _powQ;
        #endregion [Private fields]

        #region [Public members]

        private ushort GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        private short GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, short> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (short)(sum / (double)count);
        }

        private int GetMean(List<AnalogDataBaseStruct> list, Func<AnalogDataBaseStruct, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Sum(func);
            return (int)(sum / (double)count);
        }

        #region Side1
        public string GetIaS1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is1a);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetIbS1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            int value = this.GetMean(list, o => o._is1b);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetIcS1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is1c);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI0S1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is10);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI2S1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is12);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetInS1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is1n);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittx * 40);
        }

        public string GetI1S1(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is11);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        #endregion

        #region Side2

        public string GetIaS2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is2a);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetIbS2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is2b);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        public string GetIcS2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is2c);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI0S2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is20);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI2S2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is22);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetInS2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is2n);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittx * 40);
        }

        public string GetI1S2(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is21);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        #endregion

        #region Side3

        public string GetIaS3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is3a);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetIbS3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is3b);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        public string GetIcS3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is3c);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI0S3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is30);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI2S3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is32);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetInS3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is3n);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittx * 40);
        }

        public string GetI1S3(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is31);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        #endregion

        #region Side4

        public string GetIaS4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is4a);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetIbS4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is4b);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        public string GetIcS4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is4c);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI0S4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is40);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetI2S4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is42);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }

        public string GetInS4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is4n);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittx * 40);
        }

        public string GetI1S4(List<AnalogDataBaseStruct> list, SideStruct side, int iMin)
        {
            var value = this.GetMean(list, o => o._is41);
            return ValuesConverterCommon.Analog.GetI801(value, side.Ittl * 40);
        }
        #endregion

        #region Uabc1
        public string GetUa(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1a);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetUb(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1b);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetUc(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1c);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetUab(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1ab);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetUbc(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1bc);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetUca(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u1ca);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU1(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u11);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU2(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u12);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetU0(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u10);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        #endregion

        #region Uabc2

        public string GetU2a(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2a);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU2b(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2b);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetU2c(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2c);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU2ab(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2ab);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU2bc(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2bc);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetU2ca(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u2ca);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU21(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u21);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        public string GetU22(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u22);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        public string GetU20(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._u20);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }
        #endregion 

        public string GetF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF801(value);
        }

        public string GetQ(List<AnalogDataBaseStruct> list, SideStruct side, KanalTransU kanal)
        {
            double value = this.GetMean(list, o => o._powQ);
            return ValuesConverterCommon.Analog.GetQ((int)value, side.Ittl * kanal.KthValue);
        }

        public string GetP(List<AnalogDataBaseStruct> list, SideStruct side, KanalTransU kanal)
        {
            double value = this.GetMean(list, o => o._powP);
            return ValuesConverterCommon.Analog.GetP((int)value, side.Ittl * kanal.KthValue);
        }

        public string GetCosF(List<AnalogDataBaseStruct> list)
        {
            var value = this.GetMean(list, o => o._powCos);
            return ValuesConverterCommon.Analog.GetCosF801(value);
        }

        public string GetUn(List<AnalogDataBaseStruct> list, KanalTransU kanal)
        {
            var value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU801(value, kanal.KthValue);
        }

        //public string GetQt(List<AnalogDataBaseStruct> list)
        //{
        //    var value = this.GetMean(list, o => o._qt);
        //    return ValuesConverterCommon.Analog.GetQ(value);
        //}
        //public string Npusk
        //{
        //    get { return this._qn.ToString(); }
        //}

        //public string Nwarm
        //{
        //    get { return this._qnhot.ToString(); }
        //}



        public string GetdU(List<AnalogDataBaseStruct> list)
        {
            int value = this.GetMean(list, o => o._dU);
            return ValuesConverterCommon.Analog.GetUdouble(value);
        }

        public string GetdFi(List<AnalogDataBaseStruct> list)
        {
            int value = this.GetMean(list, o => o._dFi);
            return string.Format("{0} град.", Math.Round((double)value * 360 / ushort.MaxValue, 2));
        }

        public string GetdF(List<AnalogDataBaseStruct> list)
        {
            int value = this.GetMean(list, o => o._dF);
            return ValuesConverterCommon.Analog.GetF801(value);
        }


        //ДИФФ. ТОК ОСН. ГАРМОНИКИ
        public string GetIda(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ida);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }
        public string GetIdb(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._idb);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }
        public string GetIdc(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._idc);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }

        public string GetIdaPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ida);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        public string GetIdbPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._idb);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        public string GetIdcPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._idc);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        //ТОРМОЗНОЙ ТОК
        public string GetIba(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._iba);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }
        public string GetIbb(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ibb);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }
        public string GetIbc(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ibc);
            return ValuesConverterCommon.Analog.GetI801Diff(value, side.S, side.U);
        }

        public string GetIbaPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._iba);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        public string GetIbbPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ibb);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        public string GetIbcPercent(List<AnalogDataBaseStruct> list, SideStruct side)
        {
            int value = this.GetMean(list, o => o._ibc);
            return ValuesConverterCommon.Analog.ShowPercent(value);
        }

        ///*===========ВТОРИЧНЫЕ СОПРОТИВЛЕНИЯ============*/
        public string GetVtorZab(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rab);
            var x = this.GetMean(list, o => o._xab);
            var ia = this.GetMean(list, o => o._iabd);
            var ib = this.GetMean(list, o => o._ibcd);
            return (ia <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        public string GetVtorZbc(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rbc);
            var x = this.GetMean(list, o => o._xbc);
            var ic = this.GetMean(list, o => o._icad);
            var ib = this.GetMean(list, o => o._ibcd);
            return (ic <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        public string GetVtorZca(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rca);
            var x = this.GetMean(list, o => o._xca);
            var ia = this.GetMean(list, o => o._iabd);
            var ic = this.GetMean(list, o => o._icad);
            return (ia <= iMin || ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        public string GetVtorZa1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ral);
            var x = this.GetMean(list, o => o._xal);
            var ia = this.GetMean(list, o => o._iabd);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        public string GetVtorZb1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rbl);
            var x = this.GetMean(list, o => o._xbl);
            var ib = this.GetMean(list, o => o._ibcd);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        public string GetVtorZc1(List<AnalogDataBaseStruct> list, double iInp, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rcl);
            var x = this.GetMean(list, o => o._xcl);
            var ic = this.GetMean(list, o => o._icad);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ801(r, x, 1, iInp);
        }

        //===========ПЕРВИЧНЫЕ СОПРОТИВЛЕНИЯ============
        public string GetZab(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rab);
            var x = this.GetMean(list, o => o._xab);
            var ia = this.GetMean(list, o => o._iabd);
            var ib = this.GetMean(list, o => o._ibcd);

            return (ia <= iMin || ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }

        public string GetZbc(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rbc);
            var x = this.GetMean(list, o => o._xbc);
            var ib = this.GetMean(list, o => o._ibcd);
            var ic = this.GetMean(list, o => o._icad);
            return (ic <= iMin || ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }
        public string GetZca(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._rca);
            var x = this.GetMean(list, o => o._xca);
            var ia = this.GetMean(list, o => o._iabd);
            var ic = this.GetMean(list, o => o._icad);
            return (ia <= iMin || ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }
        public string GetZa1(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ral);
            var x = this.GetMean(list, o => o._xal);
            var ia = this.GetMean(list, o => o._iabd);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }

        public string GetZb1(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ral);
            var x = this.GetMean(list, o => o._xal);
            var ib = this.GetMean(list, o => o._ibcd);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }
        public string GetZc1(List<AnalogDataBaseStruct> list, KanalTransU kanal, SideStruct side, ushort iMin)
        {
            var r = this.GetMean(list, o => o._ral);
            var x = this.GetMean(list, o => o._xal);
            var ic = this.GetMean(list, o => o._icad);
            return (ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ801(r, x, kanal.KthValue, side.Ittl);
        }

        #endregion [Public members]
    }
}
