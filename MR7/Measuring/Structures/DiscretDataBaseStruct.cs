﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.MR7.Measuring.Structures
{
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 32;
        private const int ALARM_SIZE = 16;
        private const int PARAM_SIZE = 16;
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private ushort[] _base;      //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private ushort[] _alarm;    //БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private ushort[] _param;    //БД параметров
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                List<bool> retDiscr = new List<bool>();
                retDiscr.AddRange(Common.GetBitsArray(this._base, 0, 53));
                retDiscr.AddRange(Common.GetBitsArray(this._base, 323, 324));
                return retDiscr.ToArray();
            }
        }

        public bool[] State => Common.GetBitsArray(this._base, 506, 507);

        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals => Common.GetBitsArray(this._base, 96, 111);

        public bool[] Bgs => Common.GetBitsArray(this._base, 112, 127);

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals => Common.GetBitsArray(this._base, 128, 143);

        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic => Common.GetBitsArray(this._base, 144, 191);

        /// <summary>
        /// Защиты Z
        /// </summary>
        public bool[] ResistDef => Common.GetBitsArray(this._base, 203, 214);
        /// <summary>
        /// Защиты  P
        /// </summary>
        public bool[] ReversPower => Common.GetBitsArray(this._base, 217, 220);

        /// <summary>
        /// Защиты I, I*, I2/I1, Ig
        /// </summary>
        public bool[] MaximumCurrent => Common.GetBitsArray(this._base, 223, 254);

        /// <summary>
        /// Защиты U, F, Q
        /// </summary>
        public bool[] VoltageFeaq => Common.GetBitsArray(this._base, 255, 288);

        /// <summary>
        /// Двигатель
        /// </summary>
        public bool[] Motor
        {
            get
            {
                List<bool> ret = new List<bool>();
                ret.AddRange(Common.GetBitsArray(this._base, 289, 291));
                ret.Add(!ret[2]);
                return ret.ToArray();
            }
        }

        /// <summary>
        /// Состояния и АПВ
        /// </summary>
        public bool[] IdImaxSignals => Common.GetBitsArray(this._base, 192, 202);

        /// <summary>
        /// Состояния и АПВ
        /// </summary>
        public bool[] StateAndApv => Common.GetBitsArray(this._base, 292, 307);

        /// <summary>
        /// Контроль синхронизма
        /// </summary>
        public bool[] ContrSinchr => Common.GetBitsArray(this._base, 308, 314);

        /// <summary>
        /// Повр. фаз и качание
        /// </summary>
        public bool[] PhaseAndSw => Common.GetBitsArray(this._base, 315, 320);

        /// <summary>
        /// УРОВ
        /// </summary>
        public bool[] Urov => Common.GetBitsArray(this._base, 325, 327);

        /// <summary>
        /// АВР и дуговая защита
        /// </summary>
        public bool[] AvrAndDug => Common.GetBitsArray(this._base, 328, 331);

        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses => Common.GetBitsArray(this._base, 359, 374);

        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relays => Common.GetBitsArray(this._base, 375, 470);

        /// <summary>
        /// RS-Тригеры
        /// </summary>
        public bool[] RSTriggers => Common.GetBitsArray(this._base, 80, 95);

        /// <summary>
        /// Индикаторы
        /// </summary>
        public List<bool[]> Indicators
        {
            get
            {
                bool[] allIndArray = Common.GetBitsArray(this._base, 471, 494); // все подряд биты
                List<bool[]> retList = new List<bool[]>();
                for (int i = 0; i < allIndArray.Length; i = i + 2)    // выделяем попарно состояния одного диода
                {
                    retList.Add(new[] { allIndArray[i], allIndArray[i + 1] });// (bool зеленый, bool красный)
                }
                return retList;
            }
        }
        /// <summary>
        /// Контроль
        /// </summary>
        public bool[] ControlSignals => Common.GetBitsArray(this._base, 497, 511);

        /// <summary>
        /// Неисправности общие
        /// </summary>
        public bool[] Faults1 => Common.GetBitsArray(this._alarm, 0, 17);

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        public bool[] Faults2 => Common.GetBitsArray(this._alarm, 18, 24);

        /// <summary>
        /// Нисправности ТН и частоты
        /// </summary>
        public bool[] Faults3
        {
            get
            {
                List<bool> retList = new List<bool>(Common.GetBitsArray(this._base, 321, 322));
                retList.AddRange(Common.GetBitsArray(this._alarm, 26, 37));
                return retList.ToArray();
            }
        }



        /// <summary>
        /// Направления токов
        /// </summary>
        public virtual string[] CurrentsSymbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(this._param, 0, 65);
                string[] result = new string[33];
                for (int i = 0; i < 66; i += 2)
                {
                    result[i / 2] = this.GetCurrentSymbol(i, booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(int i, bool symbol, bool error)
        {
            string res = "";
            //if (i >= 33) res = "нет";
            return error ? res : (symbol ? "-" : "+");
        }
        List<bool> _logicList = new List<bool>();
        /// <summary>
        /// Неисправность логики
        /// </summary>
        public bool[] FaultLogicErr
        {
            get
            {
                this._logicList.Clear();
                this._logicList.AddRange(Common.GetBitsArray(this._alarm, 39, 43));
                this._logicList.RemoveAt(3);
                return this._logicList.ToArray();
            }
        }
        #endregion [Properties]
    }
}
