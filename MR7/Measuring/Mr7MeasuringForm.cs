﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR7.Configuration;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;
using BEMN.MR7.Measuring.Structures;
using BEMN.MR7.Properties;

namespace BEMN.MR7.Measuring
{
    public partial class Mr7MeasuringForm : Form, IFormView
    {
        #region Const
        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string CURRENT_GROUP = "Группа №{0}";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string RESET_HOT_STATE = "Сбросить состояние тепловой";
        private const string RESET_TN_STATE = "Сбросить неисправности ТН";
        private const string START_OSC = "Запустить осциллограф";
        private const string MEASURE_TRANS_READ_FAIL = "Параметры измерений не были загружены";
        #endregion

        #region [Private fields]

        private Mr7 _device;
        private readonly MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MemoryEntity<OneWordStruct> _iMinStruct; 
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<MeasureTransStruct> _measureTrans;
        private readonly int _measTransSize;
        private SideStruct _currentSideStruct;
        private SideStruct _currentSideStruct1;
        private SideStruct _currentSideStruct2;
        private SideStruct _currentSideStruct3;
        private KanalTransU _currentKanalTransU;
        private KanalTransU _currentKanalTransU2;
        private KanalTransU _currentKanalTransU3;
        private readonly AveragerTime<AnalogDataBaseStruct> _averagerTime;
        private string[] _symbols;
        private bool _loaded;
        private ushort? _numGroup;
        private ushort _iMin;
        private bool _activatefailmes;
        private Label label;
        private bool checkGroup;
        private int ind;
        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Защиты I, I*, I2/I1, Ig)
        /// </summary>
        private LedControl[] _currents;
        private LedControl[] _currents1;
        /// <summary>
        /// Защиты U,F
        /// </summary>
        private LedControl[] _voltage;
        /// <summary>
        /// Защиты по сопр.
        /// </summary>
        private LedControl[] _resist;
        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        private LedControl[] _externalDefensesMain;
        /// <summary>
        /// Защиты по мощности
        /// </summary>
        private LedControl[] _reversPow;
        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _stateAndApv;
        private LedControl[] _stateAndApvMain;
        private LedControl[] _stateAndApv1;
        private LedControl[] _state;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Энергонезависимые RS-Тригеры
        /// </summary>
        private LedControl[] _rsTriggers;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faultsMain;
        private LedControl[] _faultsMain1;
        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        private LedControl[] _faultsSwitch;

        /// <summary>
        /// Неисправности ТН
        /// </summary>
        private LedControl[] _faultsTn;
        private LedControl[] _faultsTn1;
        /// <summary>
        /// Синхронизм
        /// </summary>
        private LedControl[] _sinchronizm;
        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;
        /// <summary>
        /// Повреждение фаз и качание
        /// </summary>
        private LedControl[] _phaseAndSw;
        /// <summary>
        /// УРОВ
        /// </summary>
        private LedControl[] _urov;
        private LedControl[] _urov1;
        // АВР
        private LedControl[] _avr;
        private LedControl[] _avr1;
        // Двигатель
        private LedControl[] _motor;
        private LedControl[] _motor1;
        // Id
        private LedControl[] _idImaxSignalsLedsMain;
        private LedControl[] _idImaxSignalsLeds;

        private LedControl[] _bgs;
        #endregion [Private fields]

        #region Constructor
        public Mr7MeasuringForm()
        {
            this.InitializeComponent();
        }

        public Mr7MeasuringForm(Mr7 device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this._dateTime = device.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            if (!_device.IsConnect)
            {
                StringsConfig.DeviceType = this._device.Info.Plant;
            }
            else
            {
                StringsConfig.DeviceType = this._device.Info.DeviceConfiguration;
            }

            if (this._device.Configuration != null)
            {
                this._device.Configuration.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                {
                    this._numGroup = null;
                    this._groupUstavki.LoadStruct();
                });
            }

            this._groupUstavki = device.GroupUstavki;
            this._groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.GroupUstavkiLoaded);
            //this._groupUstavki.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("Невозможно прочитать группу уставок"));
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this._groupUstavki.LoadStruct();
            });
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                 MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK,
                 MessageBoxIcon.Error));
            
            this._discretDataBase = device.Discret;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);


            this._iMinStruct = device.Imin;
            this._iMinStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._iMin = this._iMinStruct.Value.Word;
            });
            this._iMinStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => { this._iMin = 0; });

            this._analogDataBase = device.Analog;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);
            
            this._measureTrans = device.MeasureTrans;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);
            this._measTransSize = this._measureTrans.Value.GetStructInfo().FullSize;

            this._averagerTime = new AveragerTime<AnalogDataBaseStruct>(1000);
            this._averagerTime.Tick += this.AveragerTimeTick;
            

            this.Init();
        }

        private void Init()
        {
            //todo для отладки убрал пока
            //this._dataBaseTabControl.TabPages.Remove(_analogTabPage);

            this._commandComboBox.SelectedIndex = 0;
            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32,
                this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39, this._ssl40,
                this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47, this._ssl48
            };

            this._idImaxSignalsLedsMain = new LedControl[]
            {
                this._IdMax2MgnMain, this._IdMax2IOMain, this._IdMax2Main, this._IdMax1IOMain, this._IdMax1Main, this._Id0Max1IO,
                this._Id0Max1, this._Id0Max2IO, this._Id0Max2, this._Id0Max3IO, this._Id0Max3
            };

            this._idImaxSignalsLeds = new LedControl[]
            {
                this._IdMax2Mgn, this._IdMax2IO, this._IdMax2, this._IdMax1IO, this._IdMax1, new LedControl() ,
                new LedControl() , new LedControl() , new LedControl() , new LedControl() , new LedControl()
            };

            this._phaseAndSw = new[]
            {
                this._damageA1, this._damageB1, this._damageC1, this._kachanie1, this._outZone1, this._inZone1
            };
            
            this._sinchronizm = new[]
            {
                this._autoSinchr1, this._U1noU2y1, this._UyUno1, this._UnoUno1, this._OS1, this._US1, this._OnKsAndYppn1
            };

            this._bgs = new[]
            {
                this._bgs1, this._bgs2, this._bgs3, this._bgs4, this._bgs5, this._bgs6, this._bgs7, this._bgs8,
                this._bgs9, this._bgs10, this._bgs11, this._bgs12, this._bgs13, this._bgs14, this._bgs15, this._bgs16
            };

            this._voltage = new[]
            {
                this._u1IoMoreLed, this._u1MoreLed, this._u2IoMoreLed, this._u2MoreLed, this._u3IoMoreLed,
                this._u3MoreLed, this._u4IoMoreLed, this._u4MoreLed, this._u1IoLessLed, this._u1LessLed,
                this._u2IoLessLed, this._u2LessLed, this._u3IoLessLed, this._u3LessLed, this._u4IoLessLed,
                this._u4LessLed, this._f1IoMoreLed, this._f1MoreLed, this._f2IoMoreLed, this._f2MoreLed,
                this._f3IoMoreLed, this._f3MoreLed, this._f4IoMoreLed, this._f4MoreLed, this._f1IoLessLed,
                this._f1LessLed, this._f2IoLessLed, this._f2LessLed, this._f3IoLessLed, this._f3LessLed,
                this._f4IoLessLed, this._f4LessLed, this._q1Led, this._q2Led
            };

           
            this._resist = new[]
            {
                this._r1IoLed1, this._r1Led1, this._r2IoLed1, this._r2Led1, this._r3IoLed1, this._r3Led1, this._r4IoLed1,
                this._r4Led1, this._r5IoLed1, this._r5Led1, this._r6IoLed1, this._r6Led1
            };
            this._reversPow = new[]
            {
                this._P1Io1, this._P11, this._P2Io1, this._P21
            };
            this._currents = new[]
            {
                this._i1Io, this._i1, this._i2Io, this._i2, this._i3Io, this._i3, this._i4Io, this._i4, this._i5Io, // I
                this._i5,this._i6Io, this._i6,  this._i8Io, this._i8, 
                this._iS1Io, this._iS1, this._iS2Io,this._iS2,this._iS3Io, this._iS3, this._iS4Io, this._iS4,       // I*
                this._iS5Io, this._iS5, this._iS6Io, this._iS6,this._iS7Io,this._iS7,this._iS8Io,this._iS8,
                /*this._i2i1IoLed, this._i2i1Led*/new LedControl(), new LedControl()                               // I2/I1                
            };
            this._currents1 = new[]
            {
                this._i1Io1, this._i11, this._i2Io1, this._i21, this._i3Io1, this._i31, this._i4Io1, this._i41, this._i5Io1,// I
                this._i51,this._i6Io1, this._i61, this._i8Io1, this._i81, 
                this._iS1Io1, this._iS11, this._iS2Io1,this._iS21,this._iS3Io1, this._iS31, this._iS4Io1, this._iS41,       // I*
                this._iS5Io1, this._iS51, this._iS6Io1, this._iS61,this._iS7Io1,this._iS71,this._iS8Io1,this._iS81
            };

            this._stateAndApvMain = new[]
            {
                this._faultMain, new LedControl(), this._accelerationMain, new LedControl(), this._faultOffMain,
                new LedControl(), new LedControl()
            };

            this._stateAndApv = new[]
            {
                this._fault, new LedControl(), this._acceleration, new LedControl(), this._faultOff,
                new LedControl(), new LedControl()
            };

            this._stateAndApv1 = new[]
            {
                this._puskApv1, this._krat11, this._krat21, this._krat31, this._krat41,
                this._turnOnApv1, this._zapretApv1, this._blockApv1, this._readyApv1
            };

            this._indicators = new[]
            {
                this.diod1, this.diod2, this.diod3, this.diod4, this.diod5, this.diod6, this.diod7, this.diod8, this.diod9, this.diod10, this.diod11, this.diod12
            };

            
            this._relays = new[]
            {
                this._module1, this._module2, this._module3, this._module4, this._module5, this._module6,
                this._module7, this._module8, this._module9, this._module10, this._module11, this._module12,
                this._module13, this._module14, this._module15, this._module16, this._module17, this._module18,
                this._module19, this._module20, this._module21, this._module22, this._module23, this._module24,
                this._module25, this._module26, this._module27, this._module28, this._module29, this._module30,
                this._module31, this._module32, this._module33, this._module34, this._module35, this._module36,
                this._module37, this._module38, this._module39, this._module40, this._module41, this._module42,
                this._module43, this._module44, this._module45, this._module46, this._module47, this._module48,
                this._module49, this._module50, this._module51, this._module52, this._module53, this._module54,
                this._module55, this._module56, this._module57, this._module58, this._module59, this._module60,
                this._module61, this._module62, this._module63, this._module64, this._module65, this._module66,
                this._module67, this._module68, this._module69, this._module70, this._module71, this._module72,
                this._module73, this._module74, this._module75, this._module76, this._module77, this._module78,
                this._module79, this._module80
            };

            this._rsTriggers = new[]
            {
                this._rst1, this._rst2, this._rst3, this._rst4, this._rst5, this._rst6, this._rst7, this._rst8,
                this._rst9, this._rst10, this._rst11, this._rst12, this._rst13, this._rst14, this._rst15, this._rst16
            };

            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16
            };

            this._externalDefensesMain = new[]
            {
                this._vz1m, this._vz2m, this._vz3m, this._vz4m, this._vz5m, this._vz6m, this._vz7m, this._vz8m,
                this._vz9m, this._vz10m, this._vz11m, this._vz12m, this._vz13m, this._vz14m, this._vz15m, this._vz16m
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal,
                this._availabilityFaultSystemJournal, new LedControl(), this._switchOff, this._switchOn
            };
            this._faultsMain = new[]
            {
                this._faultHardware, this._faultSoftware, this._faultMeasuringU, this._faultMeasuringf,
                this._faultSwitchOff, this._faultLogic, this._faultModule1, this._faultModule2,
                this._faultModule3, this._faultModule4, this._faultModule5, this._faultModule6, this._faultSetpoints,
                this._faultGroupsOfSetpoints, this._faultPass, this._faultSystemJournal, this._faultAlarmJournal,
                this._faultOsc
            };
            this._faultsMain1 = new[]
            {
                this._faultHardware1, this._faultSoftware1, this._faultMeasuringU1, this._faultMeasuringF1,
                this._faultSwitchOff1, this._faultLogic1
            };

            this._faultsTn = new[]
            {
                /*this._faultTNmgn*/new LedControl(), new LedControl(), /*this._faultUn1*/new LedControl(), /*this._faultTH3u0*/new LedControl(),                                      
                /*this._faultTHU2*/new LedControl(),/*this._faultObruvFaz*/new LedControl(), /*this._faultUabc*/ new LedControl(), new LedControl(),                                          
                /*this._faultTN*/ new LedControl(), /*this._faultUn*/new LedControl(), new LedControl(), this._UabcLow10, this._freqHiger60, this._freqLow40
            };

            this._faultsTn1 = new[]
            {
                this._faultTNmgn1, new LedControl(), this._externalTnUn1, this._faultTn3U0, this._faultTnU2, this._tnObivFaz, 
                this._externalTn, new LedControl(), this._faultTN1, this._externalTnUn
            };

            
            this._discretInputs = new[]
            {
                this._d1, this._d2, this._d3, this._d4, this._d5, this._d6, this._d7, this._d8, this._d9, this._d10,
                this._d11, this._d12, this._d13, this._d14, this._d15, this._d16, this._d17, this._d18, this._d19,
                this._d20, this._d21, this._d22, this._d23, this._d24, this._d25, this._d26, this._d27, this._d28, 
                this._d29, this._d30, this._d31, this._d32, this._d33, this._d34, this._d35, this._d36, this._d37, 
                this._d38, this._d39,this._d40, this._d41, this._d42, this._d43, this._d44, this._d45, this._d46,
                this._d47, this._d48,this._d49, this._d50, this._d51, this._d52, this._d53, this._d54, /*this._d55,
                this._d56, this._d57,this._d58, this._d59, this._d60, this._d61, this._d62, this._d63, this._d64,
                this._d65, this._d66,this._d67, this._d68, this._d69, this._d70, this._d71, this._d72, this._d73,
                this._d74, this._d75,this._d76, this._d77, this._d78, this._d79, this._d80, this._d81, this._d82,
                this._d83, this._d84,this._d85, this._d86, this._d87, this._d88, this._d89, this._d90, this._d91,
                this._d92, this._d93,this._d94, this._d95, this._d96, */this._k1, this._k2
            };

            this._state = new[]
            {
                this._mainGroup,this._reservedGroup
            };

            this._splErr = new []
            {
                this._fSpl1, this._fSpl2, this._fSpl3, this._fSpl4
            };
            this._groupCombo.SelectedIndex = 0;

            this._faultsSwitch = new[]
            {
                this._faultOut, this._faultBlockCon, this._faultManage, this._faultOtkaz, this._faultSwithON, this._faultDisable1,
                this._faultDisable2
            };
            this._urov = new[] /*{this.urov1Led, this.urov2Led, this.blockUrovLed}
            */ { new LedControl() ,new LedControl() ,new LedControl() };
            this._urov1 = new[] { this.urov1Led1, this.urov2Led1, this.blockUrovLed1 };
            this._avr = new[] { /*this.avrOn, this.avrOff, this.avrBlock,*/new LedControl(), new LedControl(), new LedControl(), this.dugPusk};
            this._avr1 = new[] {this._avrOn1, this._avrOff1, this._avrBlock1, this.dugPusk1};
            this._motor = new[] /*{this.dvBlockQ, this.dvBlockN, this.dvPusk, this.dvWork}*/ {new LedControl(), new LedControl(), new LedControl() };
            this._motor1 = new[] {this.dvBlockQ1, this.dvBlockN1, this.dvPusk1, this.dvWork1};

            this.HideDiskret();

        }

        private void HideDiskret()
        {
            switch (StringsConfig.DeviceType)
            {
                case Mr7.T12N5D58R51:
                case Mr7.T12N6D58R51:
                case Mr7.T9N8D58R51:
                    return;
                case Mr7.T12N4D26R19:
                    #region [D26R19]
                    _releGB.Size = new Size(52, 365);
                    _virtualReleGB.Size = new Size(182, 365);
                    _virtualReleGB.Location = new Point(120, 3);
                    _diskretInputGB.Location = new Point(308, 3);
                    _diskretInputGB.Size = new Size(121, 365);
                    _rsTriggersGB.Location = new Point(470, 3);

                    _module18.Location = new Point(65, 345);
                    _r18Label.Location = new Point(84, 345);

                    //First column
                    _module19.Location = new Point(126, 22);
                    _r19Label.Location = new Point(145, 22);

                    _module20.Location = new Point(126, 41);
                    _r20Label.Location = new Point(145, 38);

                    _module21.Location = new Point(126, 57);
                    _r21Label.Location = new Point(145, 57);

                    _module22.Location = new Point(126, 78);
                    _r22Label.Location = new Point(145, 78);

                    _module23.Location = new Point(126, 97);
                    _r23Label.Location = new Point(145, 97);

                    _module24.Location = new Point(126, 117);
                    _r24Label.Location = new Point(145, 117);

                    _module25.Location = new Point(126, 136);
                    _r25Label.Location = new Point(145, 136);

                    _module26.Location = new Point(126, 155);
                    _r26Label.Location = new Point(145, 155);

                    _module27.Location = new Point(126, 174);
                    _r27Label.Location = new Point(145, 174);

                    _module28.Location = new Point(126, 193);
                    _r28Label.Location = new Point(145, 193);

                    _module29.Location = new Point(126, 212);
                    _r29Label.Location = new Point(145, 212);

                    _module30.Location = new Point(126, 231);
                    _r30Label.Location = new Point(145, 231);

                    _module31.Location = new Point(126, 250);
                    _r31Label.Location = new Point(145, 250);

                    _module32.Location = new Point(126, 269);
                    _r32Label.Location = new Point(145, 269);

                    _module33.Location = new Point(126, 288);
                    _r33Label.Location = new Point(145, 288);

                    _module34.Location = new Point(126, 307);
                    _r34Label.Location = new Point(145, 307);

                    _module35.Location = new Point(126, 326);
                    _r35Label.Location = new Point(145, 326);

                    _module36.Location = new Point(126, 345);
                    _r36Label.Location = new Point(145, 345);

                    //Second column

                    _module37.Location = new Point(170, 22);
                    _r37Label.Location = new Point(189, 22);

                    _module38.Location = new Point(170, 41);
                    _r38Label.Location = new Point(189, 41);

                    _module39.Location = new Point(170, 60);
                    _r39Label.Location = new Point(189, 60);

                    _module40.Location = new Point(170, 79);
                    _r40Label.Location = new Point(189, 79);

                    _module41.Location = new Point(170, 98);
                    _r41Label.Location = new Point(189, 98);

                    _module42.Location = new Point(170, 117);
                    _r42Label.Location = new Point(189, 117);

                    _module43.Location = new Point(170, 136);
                    _r43Label.Location = new Point(189, 136);

                    _module44.Location = new Point(170, 155);
                    _r44Label.Location = new Point(189, 155);

                    _module45.Location = new Point(170, 174);
                    _r45Label.Location = new Point(189, 174);

                    _module46.Location = new Point(170, 193);
                    _r46Label.Location = new Point(189, 193);

                    _module47.Location = new Point(170, 212);
                    _r47Label.Location = new Point(189, 212);

                    _module48.Location = new Point(170, 231);
                    _r48Label.Location = new Point(189, 231);

                    _module49.Location = new Point(170, 250);
                    _r49Label.Location = new Point(189, 250);

                    _module50.Location = new Point(170, 269);
                    _r50Label.Location = new Point(189, 269);

                    _module51.Location = new Point(170, 288);
                    _r51Label.Location = new Point(189, 288);

                    _module52.Location = new Point(170, 307);
                    _r52Label.Location = new Point(189, 307);

                    _module53.Location = new Point(170, 326);
                    _r53Label.Location = new Point(189, 326);

                    _module54.Location = new Point(170, 345);
                    _r54Label.Location = new Point(189, 345);

                    //Third column

                    _module55.Location = new Point(214, 22);
                    _r55Label.Location = new Point(233, 22);
                    
                    _module56.Location = new Point(214, 41);
                    _r56Label.Location = new Point(233, 41);

                    _module57.Location = new Point(214, 60);
                    _r57Label.Location = new Point(233, 60);

                    _module58.Location = new Point(214, 79);
                    _r58Label.Location = new Point(233, 79);

                    _module59.Location = new Point(214, 98);
                    _r59Label.Location = new Point(233, 98);

                    _module60.Location = new Point(214, 117);
                    _r60Label.Location = new Point(233, 117);

                    _module61.Location = new Point(214, 136);
                    _r61Label.Location = new Point(233, 136);

                    _module62.Location = new Point(214, 155);
                    _r62Label.Location = new Point(233, 155);

                    _module63.Location = new Point(214, 174);
                    _r63Label.Location = new Point(233, 174);

                    _module64.Location = new Point(214, 193);
                    _r64Label.Location = new Point(233, 193);

                    _module65.Location = new Point(214, 212);
                    _r65Label.Location = new Point(233, 212);

                    _module66.Location = new Point(214, 231);
                    _r66Label.Location = new Point(233, 231);

                    _module67.Location = new Point(214, 250);
                    _r67Label.Location = new Point(233, 250);

                    _module68.Location = new Point(214, 269);
                    _r68Label.Location = new Point(233, 269);

                    _module69.Location = new Point(214, 288);
                    _r69Label.Location = new Point(233, 288);

                    _module70.Location = new Point(214, 307);
                    _r70Label.Location = new Point(233, 307);

                    _module71.Location = new Point(214, 326);
                    _r71Label.Location = new Point(233, 326);

                    _module72.Location = new Point(214, 345);
                    _r72Label.Location = new Point(233, 345);

                    //Fourth column

                    _module73.Location = new Point(258, 22);
                    _r73Label.Location = new Point(277, 22);

                    _module74.Location = new Point(258, 41);
                    _r74Label.Location = new Point(277, 41);

                    _module75.Location = new Point(258, 60);
                    _r75Label.Location = new Point(277, 60);

                    _module76.Location = new Point(258, 79);
                    _r76Label.Location = new Point(277, 79);

                    _module77.Location = new Point(258, 98);
                    _r77Label.Location = new Point(277, 98);

                    _module78.Location = new Point(258, 117);
                    _r78Label.Location = new Point(277, 117);

                    _module79.Location = new Point(258, 136);
                    _r79Label.Location = new Point(277, 136);

                    _module80.Location = new Point(258, 155);
                    _r80Label.Location = new Point(277, 155);
                    
                    //Change location led and label of DiscretInputGB First Column

                    _d1.Location = new Point(6, 19);
                    _d1Label.Location = new Point(22, 19);

                    _d2.Location = new Point(6, 38);
                    _d2Label.Location = new Point(22, 38);

                    _d3.Location = new Point(6, 57);
                    _d3Label.Location = new Point(22, 57);

                    _d4.Location = new Point(6, 76);
                    _d4Label.Location = new Point(22, 76);

                    _d5.Location = new Point(6, 95);
                    _d5Label.Location = new Point(22, 95);

                    _d6.Location = new Point(6, 114);
                    _d6Label.Location = new Point(22, 114);

                    _d7.Location = new Point(6, 133);
                    _d7Label.Location = new Point(22, 133);

                    _d8.Location = new Point(6, 152);
                    _d8Label.Location = new Point(22, 152);

                    _d9.Location = new Point(6, 171);
                    _d9Label.Location = new Point(22, 171);

                    _d10.Location = new Point(6, 190);
                    _d10Label.Location = new Point(22, 190);

                    _d11.Location = new Point(6, 209);
                    _d11Label.Location = new Point(22, 209);

                    _d12.Location = new Point(6, 228);
                    _d12Label.Location = new Point(22, 228);

                    _d13.Location = new Point(6, 247);
                    _d13Label.Location = new Point(22, 247);

                    _d14.Location = new Point(6, 266);
                    _d14Label.Location = new Point(22, 266);

                    _d15.Location = new Point(6, 285);
                    _d15Label.Location = new Point(22, 285);

                    _d16.Location = new Point(6, 304);
                    _d16Label.Location = new Point(22, 304);

                    _d17.Location = new Point(6, 323);
                    _d17Label.Location = new Point(22, 323);

                    _d18.Location = new Point(6, 342);
                    _d18Label.Location = new Point(22, 342);

                    //Second column

                    _d19.Location = new Point(65, 19);
                    _d19Label.Location = new Point(81, 19);

                    _d20.Location = new Point(65, 38);
                    _d20Label.Location = new Point(81, 38);

                    _d21.Location = new Point(65, 57);
                    _d21Label.Location = new Point(81, 57);

                    _d22.Location = new Point(65, 76);
                    _d22Label.Location = new Point(81, 76);

                    _d23.Location = new Point(65, 95);
                    _d23Label.Location = new Point(81, 95);

                    _d24.Location = new Point(65, 114);
                    _d24Label.Location = new Point(81, 114);

                    //Outgoing hide

                    _d25.Visible = false;
                    _d26.Visible = false;
                    _d27.Visible = false;
                    _d28.Visible = false;
                    _d29.Visible = false;
                    _d30.Visible = false;
                    _d31.Visible = false;
                    _d32.Visible = false;
                    _d33.Visible = false;
                    _d34.Visible = false;
                    _d35.Visible = false;
                    _d36.Visible = false;
                    _d37.Visible = false;
                    _d38.Visible = false;
                    _d39.Visible = false;
                    _d40.Visible = false;
                    _d41.Visible = false;
                    _d42.Visible = false;
                    _d43.Visible = false;
                    _d44.Visible = false;
                    _d45.Visible = false;
                    _d46.Visible = false;
                    _d47.Visible = false;
                    _d48.Visible = false;
                    _d49.Visible = false;
                    _d50.Visible = false;
                    _d51.Visible = false;
                    _d52.Visible = false;
                    _d53.Visible = false;
                    _d54.Visible = false;
                    _d25Label.Visible = false;
                    _d26Label.Visible = false;
                    _d27Label.Visible = false;
                    _d28Label.Visible = false;
                    _d29Label.Visible = false;
                    _d30Label.Visible = false;
                    _d31Label.Visible = false;
                    _d32Label.Visible = false;
                    _d33Label.Visible = false;
                    _d34Label.Visible = false;
                    _d35Label.Visible = false;
                    _d36Label.Visible = false;
                    _d37Label.Visible = false;
                    _d38Label.Visible = false;
                    _d39Label.Visible = false;
                    _d40Label.Visible = false;
                    _d41Label.Visible = false;
                    _d42Label.Visible = false;
                    _d43Label.Visible = false;
                    _d44Label.Visible = false;
                    _d45Label.Visible = false;
                    _d46Label.Visible = false;
                    _d47Label.Visible = false;
                    _d48Label.Visible = false;
                    _d49Label.Visible = false;
                    _d50Label.Visible = false;
                    _d51Label.Visible = false;
                    _d52Label.Visible = false;
                    _d53Label.Visible = false;
                    _d54Label.Visible = false;
                    
                    //New location of k1 and k2 

                    _k1.Location = new Point(65, 133);
                    _k2.Location = new Point(65, 152);
                    _k1Label.Location = new Point(81, 133);
                    _k2Label.Location = new Point(81, 152);

                    #endregion
                    return;
                case Mr7.T6N3D42R35:
                    #region [D42R35]
                    _releGB.Size = new Size(91, 356);
                    _virtualReleGB.Size = new Size(143, 356);
                    _virtualReleGB.Location = new Point(159, 3);
                    _diskretInputGB.Size = new Size(158, 356);
                    _diskretInputGB.Location = new Point(308, 3);
                    _rsTriggersGB.Location = new Point(470, 3);

                    _module18.Location = new Point(111, 22);
                    _r18Label.Location = new Point(130, 22);
                   
                    _module19.Location = new Point(111, 41);
                    _r19Label.Location = new Point(130, 41);

                    _module20.Location = new Point(111, 60);
                    _r20Label.Location = new Point(130, 60);

                    _module21.Location = new Point(111, 79);
                    _r21Label.Location = new Point(130, 79);

                    _module22.Location = new Point(111, 98);
                    _r22Label.Location = new Point(130, 98);

                    _module23.Location = new Point(111, 117);
                    _r23Label.Location = new Point(130, 117);

                    _module24.Location = new Point(111, 136);
                    _r24Label.Location = new Point(130, 136);

                    _module25.Location = new Point(111, 155);
                    _r25Label.Location = new Point(130, 155);

                    _module26.Location = new Point(111, 174);
                    _r26Label.Location = new Point(130, 174);

                    _module27.Location = new Point(111, 193);
                    _r27Label.Location = new Point(130, 193);

                    _module28.Location = new Point(111, 212);
                    _r28Label.Location = new Point(130, 212);

                    _module29.Location = new Point(111, 231);
                    _r29Label.Location = new Point(130, 231);

                    _module30.Location = new Point(111, 250);
                    _r30Label.Location = new Point(130, 250);

                    _module31.Location = new Point(111, 269);
                    _r31Label.Location = new Point(130, 269);

                    _module32.Location = new Point(111, 288);
                    _r32Label.Location = new Point(130, 288);

                    _module33.Location = new Point(111, 307);
                    _r33Label.Location = new Point(130, 307);

                    _module34.Location = new Point(111, 326);
                    _r34Label.Location = new Point(130, 326);


                    //Virtual rele First Column

                    _module35.Location = new Point(166, 22);
                    _r35Label.Location = new Point(185, 22);

                    _module36.Location = new Point(166, 41);
                    _r36Label.Location = new Point(185, 41);

                    _module37.Location = new Point(166, 60);
                    _r37Label.Location = new Point(185, 60);
                    
                    _module38.Location = new Point(166, 79);
                    _r38Label.Location = new Point(185, 79);

                    _module39.Location = new Point(166, 98);
                    _r39Label.Location = new Point(185, 98);

                    _module40.Location = new Point(166, 117);
                    _r40Label.Location = new Point(185, 117);

                    _module41.Location = new Point(166, 136);
                    _r41Label.Location = new Point(185, 136);

                    _module42.Location = new Point(166, 155);
                    _r42Label.Location = new Point(185, 155);

                    _module43.Location = new Point(166, 174);
                    _r43Label.Location = new Point(185, 174);

                    _module44.Location = new Point(166, 193);
                    _r44Label.Location = new Point(185, 193);

                    _module45.Location = new Point(166, 212);
                    _r45Label.Location = new Point(185, 212);

                    _module46.Location = new Point(166, 231);
                    _r46Label.Location = new Point(185, 231);

                    _module47.Location = new Point(166, 250);
                    _r47Label.Location = new Point(185, 250);

                    _module48.Location = new Point(166, 269);
                    _r48Label.Location = new Point(185, 269);

                    _module49.Location = new Point(166, 288);
                    _r49Label.Location = new Point(185, 288);

                    _module50.Location = new Point(166, 307);
                    _r50Label.Location = new Point(185, 307);

                    _module51.Location = new Point(166, 326);
                    _r51Label.Location = new Point(185, 326);


                    //Second Column
                    
                    _module52.Location = new Point(211, 22);
                    _r52Label.Location = new Point(230, 22);

                    _module53.Location = new Point(211, 41);
                    _r53Label.Location = new Point(230, 41);

                    _module54.Location = new Point(211, 60);
                    _r54Label.Location = new Point(230, 60);

                    _module55.Location = new Point(211, 79);
                    _r55Label.Location = new Point(230, 79);

                    _module56.Location = new Point(211, 98);
                    _r56Label.Location = new Point(230, 98);

                    _module57.Location = new Point(211, 117);
                    _r57Label.Location = new Point(230, 117);

                    _module58.Location = new Point(211, 136);
                    _r58Label.Location = new Point(230, 136);

                    _module59.Location = new Point(211, 155);
                    _r59Label.Location = new Point(230, 155);

                    _module60.Location = new Point(211, 174);
                    _r60Label.Location = new Point(230, 174);

                    _module61.Location = new Point(211, 193);
                    _r61Label.Location = new Point(230, 193);

                    _module62.Location = new Point(211, 212);
                    _r62Label.Location = new Point(230, 212);

                    _module63.Location = new Point(211, 231);
                    _r63Label.Location = new Point(230, 231);

                    _module64.Location = new Point(211, 250);
                    _r64Label.Location = new Point(230, 250);

                    _module65.Location = new Point(211, 269);
                    _r65Label.Location = new Point(230, 269);

                    _module66.Location = new Point(211, 288);
                    _r66Label.Location = new Point(230, 288);

                    _module67.Location = new Point(211, 307);
                    _r67Label.Location = new Point(230, 307);

                    _module68.Location = new Point(211, 326);
                    _r68Label.Location = new Point(230, 326);

                    //Third column
                    
                    _module69.Location = new Point(255, 22);
                    _r69Label.Location = new Point(274, 22);

                    _module70.Location = new Point(255, 41);
                    _r70Label.Location = new Point(274, 41);

                    _module71.Location = new Point(255, 60);
                    _r71Label.Location = new Point(274, 60);

                    _module72.Location = new Point(255, 79);
                    _r72Label.Location = new Point(274, 79);

                    _module73.Location = new Point(255, 98);
                    _r73Label.Location = new Point(274, 98);
                    
                    _module74.Location = new Point(255, 117);
                    _r74Label.Location = new Point(274, 117);

                    _module75.Location = new Point(255, 136);
                    _r75Label.Location = new Point(274, 136);

                    _module76.Location = new Point(255, 155);
                    _r76Label.Location = new Point(274, 155);

                    _module77.Location = new Point(255, 174);
                    _r77Label.Location = new Point(274, 174);

                    _module78.Location = new Point(255, 193);
                    _r78Label.Location = new Point(274, 193);

                    _module79.Location = new Point(255, 212);
                    _r79Label.Location = new Point(274, 212);

                    _module80.Location = new Point(255, 231);
                    _r80Label.Location = new Point(274, 231);

                    //Change location led and label of DiscretInputGB First Column

                    _d1.Location = new Point(6, 19);
                    _d1Label.Location = new Point(22, 19);

                    _d2.Location = new Point(6, 38);
                    _d2Label.Location = new Point(22, 38);

                    _d3.Location = new Point(6, 57);
                    _d3Label.Location = new Point(22, 57);

                    _d4.Location = new Point(6, 76);
                    _d4Label.Location = new Point(22, 76);

                    _d5.Location = new Point(6, 95);
                    _d5Label.Location = new Point(22, 95);

                    _d6.Location = new Point(6, 114);
                    _d6Label.Location = new Point(22, 114);

                    _d7.Location = new Point(6, 133);
                    _d7Label.Location = new Point(22, 133);

                    _d8.Location = new Point(6, 152);
                    _d8Label.Location = new Point(22, 152);

                    _d9.Location = new Point(6, 171);
                    _d9Label.Location = new Point(22, 171);

                    _d10.Location = new Point(6, 190);
                    _d10Label.Location = new Point(22, 190);

                    _d11.Location = new Point(6, 209);
                    _d11Label.Location = new Point(22, 209);

                    _d12.Location = new Point(6, 228);
                    _d12Label.Location = new Point(22, 228);

                    _d13.Location = new Point(6, 247);
                    _d13Label.Location = new Point(22, 247);

                    _d14.Location = new Point(6, 266);
                    _d14Label.Location = new Point(22, 266);

                    _d15.Location = new Point(6, 285);
                    _d15Label.Location = new Point(22, 285);

                    _d16.Location = new Point(6, 304);
                    _d16Label.Location = new Point(22, 304);

                    _d17.Location = new Point(6, 323);
                    _d17Label.Location = new Point(22, 323);

                    //Second column

                    _d18.Location = new Point(59, 19);
                    _d18Label.Location = new Point(75, 19);

                    _d19.Location = new Point(59, 38);
                    _d19Label.Location = new Point(75, 38);

                    _d20.Location = new Point(59, 57);
                    _d20Label.Location = new Point(75, 57);

                    _d21.Location = new Point(59, 76);
                    _d21Label.Location = new Point(75, 76);

                    _d22.Location = new Point(59, 95);
                    _d22Label.Location = new Point(75, 95);

                    _d23.Location = new Point(59, 114);
                    _d23Label.Location = new Point(75, 114);

                    _d24.Location = new Point(59, 133);
                    _d24Label.Location = new Point(75, 133);

                    _d25.Location = new Point(59, 152);
                    _d25Label.Location = new Point(75, 152);

                    _d26.Location = new Point(59, 171);
                    _d26Label.Location = new Point(75, 171);

                    _d27.Location = new Point(59, 190);
                    _d27Label.Location = new Point(75, 190);

                    _d28.Location = new Point(59, 209);
                    _d28Label.Location = new Point(75, 209);

                    _d29.Location = new Point(59, 228);
                    _d29Label.Location = new Point(75, 228);

                    _d30.Location = new Point(59, 247);
                    _d30Label.Location = new Point(75, 247);

                    _d31.Location = new Point(59, 266);
                    _d31Label.Location = new Point(75, 266);

                    _d32.Location = new Point(59, 285);
                    _d32Label.Location = new Point(75, 285);

                    _d33.Location = new Point(59, 304);
                    _d33Label.Location = new Point(75, 304);

                    _d34.Location = new Point(59, 323);
                    _d34Label.Location = new Point(75, 323);

                    //Third column

                    _d35.Location = new Point(109, 19);
                    _d35Label.Location = new Point(125, 19);

                    _d36.Location = new Point(109, 38);
                    _d36Label.Location = new Point(125, 38);

                    _d37.Location = new Point(109, 57);
                    _d37Label.Location = new Point(125, 57);

                    _d38.Location = new Point(109, 76);
                    _d38Label.Location = new Point(125, 76);

                    _d39.Location = new Point(109, 95);
                    _d39Label.Location = new Point(125, 95);

                    _d40.Location = new Point(109, 114);
                    _d40Label.Location = new Point(125, 114);

                    //Outgoing hide
                    
                    _d41.Visible = false;
                    _d42.Visible = false;
                    _d43.Visible = false;
                    _d44.Visible = false;
                    _d45.Visible = false;
                    _d46.Visible = false;
                    _d47.Visible = false;
                    _d48.Visible = false;
                    _d49.Visible = false;
                    _d50.Visible = false;
                    _d51.Visible = false;
                    _d52.Visible = false;
                    _d53.Visible = false;
                    _d54.Visible = false;
                    _d41Label.Visible = false;
                    _d42Label.Visible = false;
                    _d43Label.Visible = false;
                    _d44Label.Visible = false;
                    _d45Label.Visible = false;
                    _d46Label.Visible = false;
                    _d47Label.Visible = false;
                    _d48Label.Visible = false;
                    _d49Label.Visible = false;
                    _d50Label.Visible = false;
                    _d51Label.Visible = false;
                    _d52Label.Visible = false;
                    _d53Label.Visible = false;
                    _d54Label.Visible = false;

                    //New location of k1 and k2 

                    _k1.Location = new Point(109, 133);
                    _k2.Location = new Point(109, 152);
                    _k1Label.Location = new Point(125, 133);
                    _k2Label.Location = new Point(125, 152);

                    #endregion
                    return;
                default: return;

            }
        }


        #endregion Constructor

        #region MemoryEntity Members
        private void GroupUstavkiLoaded()
        {
            if (this._numGroup != this._groupUstavki.Value.Word)
            {
                this._numGroup = this._groupUstavki.Value.Word;
                this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
                ushort measureTransAddr = this._device.GetStartAddrMeasTrans((int) this._numGroup);
              
                this._measureTrans.RemoveStructQueries();
                this._measureTrans.Slots[0] = new Device.slot(measureTransAddr, (ushort) (measureTransAddr + this._measTransSize));
                this._measureTrans.LoadStruct();
                
            }
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        private void AnalogBdReadFail()
        {
            const string errorValue = "0";

            this._U1abcA.Text = errorValue;
            this._U1abcB.Text = errorValue;
            this._U1abcC.Text = errorValue;
            this._U1ab.Text = errorValue;
            this._U1bc.Text = errorValue;
            this._U1ca.Text = errorValue;
            this._U1abc1.Text = errorValue;
            this._U1abc2.Text = errorValue;
            this._U1abc0.Text = errorValue;

            this._U2abcA.Text = errorValue;
            this._U2abcB.Text = errorValue;
            this._U2abcC.Text = errorValue;
            this._U2ab.Text = errorValue;
            this._U2bc.Text = errorValue;
            this._U2ca.Text = errorValue;
            this._U2abc1.Text = errorValue;
            this._U2abc2.Text = errorValue;
            this._U2abc0.Text = errorValue;

            this._Is1a.Text = errorValue;
            this._Is1b.Text = errorValue;
            this._Is1c.Text = errorValue;
            this._Is10.Text = errorValue;
            this._Is12.Text = errorValue;
            this._Is1n.Text = errorValue;
            this._Is1I1.Text = errorValue;

            this._Is2a.Text = errorValue;
            this._Is2b.Text = errorValue;
            this._Is2c.Text = errorValue;
            this._Is20.Text = errorValue;
            this._Is22.Text = errorValue;
            this._Is2n.Text = errorValue;
            this._Is2I1.Text = errorValue;

            this._Is3a.Text = errorValue;
            this._Is3b.Text = errorValue;
            this._Is3c.Text = errorValue;
            this._Is30.Text = errorValue;
            this._Is32.Text = errorValue;
            this._Is3n.Text = errorValue;
            this._Is3I1.Text = errorValue;

            this._Is4a.Text = errorValue;
            this._Is4b.Text = errorValue;
            this._Is4c.Text = errorValue;
            this._Is40.Text = errorValue;
            this._Is42.Text = errorValue;
            this._Is4n.Text = errorValue;
            this._Is4I1.Text = errorValue;

            this._frequency.Text = errorValue;
            this._voltageTB.Text = errorValue;

            this._pTextBox.Text = errorValue; 
            this._qTextBox.Text = errorValue; 
            this._cosfTextBox.Text = errorValue; 

            this._Ida.Text = errorValue;
            this._Idb.Text = errorValue;
            this._Idc.Text = errorValue;
            this._Iba.Text = errorValue;
            this._Ibb.Text = errorValue;
            this._Ibc.Text = errorValue;
            this._percentIda.Text = errorValue;
            this._percentIdb.Text = errorValue;
            this._percentIdc.Text = errorValue;
            this._percentIba.Text = errorValue;
            this._percentIbb.Text = errorValue;
            this._percentIbc.Text = errorValue;

            this._zabBox.Text = errorValue;
            this._zbcBox.Text = errorValue;
            this._zcaBox.Text = errorValue;
            this._za1Box.Text = errorValue;
            this._zb1Box.Text = errorValue;
            this._zc1Box.Text = errorValue;

            this._iZabBox.Text = errorValue;
            this._iZbcBox.Text = errorValue;
            this._iZcaBox.Text = errorValue;
            this._iZa1Box.Text = errorValue;
            this._iZb1Box.Text = errorValue;
            this._iZc1Box.Text = errorValue;

            this._dU.Text = errorValue;
            this._dFi.Text = errorValue;
            this._dF.Text = errorValue;

            this.AB.Text = "нет";
            this.BC.Text = "нет";
            this.CA.Text = "нет";
            this.AN.Text = "нет";
            this.BN.Text = "нет";
            this.CN.Text = "нет";
        }

        private void AveragerTimeTick()
        {
            try
            {
                string[] symbols;

                if (this._symbols != null || this._symbols.Length < 34)
                {
                    symbols = this._symbols;
                }
                else
                {
                    symbols = new string[33];
                    for (int i = 0; i < 34; i++)
                    {
                        symbols[i] = "!";
                    }
                }

                #region Токи, напряжения и частота


                string _Un1 = _currentKanalTransU.KanalUSide;
                string _Un2 = _currentKanalTransU2.KanalUSide;
                string _Un3 = _currentKanalTransU3.KanalUSide;

                switch (_Un3)
                {
                    case "C1":
                        this._frequency.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                        this._voltageTB.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, _currentKanalTransU3);
                        break;
                    case "C2":
                        this._frequency.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                        this._voltageTB.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, _currentKanalTransU3);
                        break;
                    case "C3":
                        this._frequency.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                        this._voltageTB.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, _currentKanalTransU3);
                        break;
                    case "C4":
                        this._frequency.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                        this._voltageTB.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, _currentKanalTransU3);
                        break;
                }


                
                this._U1abcA.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1abcB.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1abcC.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1ab.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1bc.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1ca.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1abc1.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1abc2.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, _currentKanalTransU);
                this._U1abc0.Text = this._analogDataBase.Value.GetU0(this._averagerTime.ValueList, _currentKanalTransU);

                this._U2abcA.Text = this._analogDataBase.Value.GetU2a(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2abcB.Text = this._analogDataBase.Value.GetU2b(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2abcC.Text = this._analogDataBase.Value.GetU2c(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2ab.Text = this._analogDataBase.Value.GetU2ab(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2bc.Text = this._analogDataBase.Value.GetU2bc(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2ca.Text = this._analogDataBase.Value.GetU2ca(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2abc1.Text = this._analogDataBase.Value.GetU21(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2abc2.Text = this._analogDataBase.Value.GetU22(this._averagerTime.ValueList, _currentKanalTransU2);
                this._U2abc0.Text = this._analogDataBase.Value.GetU20(this._averagerTime.ValueList, _currentKanalTransU2);

                
                #region Стороны
                
                this._Is1a.Text = this._Is1a.Text == "+0 А" || this._Is1a.Text == "-0 А" || this._Is1a.Text == "0 А" ? this._analogDataBase.Value.GetIaS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[0] + this._analogDataBase.Value.GetIaS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is1b.Text = this._Is1b.Text == "+0 А" || this._Is1b.Text == "-0 А" || this._Is1b.Text == "0 А" ? this._analogDataBase.Value.GetIbS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[1] + this._analogDataBase.Value.GetIbS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is1c.Text = this._Is1c.Text == "+0 А" || this._Is1c.Text == "-0 А" || this._Is1c.Text == "0 А" ? this._analogDataBase.Value.GetIcS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[2] + this._analogDataBase.Value.GetIcS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is10.Text = this._Is10.Text == "+0 А" || this._Is10.Text == "-0 А" || this._Is10.Text == "0 А" ? this._analogDataBase.Value.GetI0S1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[4] + this._analogDataBase.Value.GetI0S1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is12.Text = this._Is12.Text == "+0 А" || this._Is12.Text == "-0 А" || this._Is12.Text == "0 А" ? this._analogDataBase.Value.GetI2S1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[5] + this._analogDataBase.Value.GetI2S1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is1n.Text = this._Is1n.Text == "+0 А" || this._Is1n.Text == "-0 А" || this._Is1n.Text == "0 А" ? this._analogDataBase.Value.GetInS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin) : symbols[3] + this._analogDataBase.Value.GetInS1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);
                this._Is1I1.Text = this._analogDataBase.Value.GetI1S1(this._averagerTime.ValueList, _currentSideStruct, this._iMin);

                this._Is2a.Text = this._Is2a.Text == "+0 А" || this._Is2a.Text == "-0 А" || this._Is2a.Text == "0 А" ? this._analogDataBase.Value.GetIaS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[6] + this._analogDataBase.Value.GetIaS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is2b.Text = this._Is2b.Text == "+0 А" || this._Is2b.Text == "-0 А" || this._Is2b.Text == "0 А" ? this._analogDataBase.Value.GetIbS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[7] + this._analogDataBase.Value.GetIbS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is2c.Text = this._Is2c.Text == "+0 А" || this._Is2c.Text == "-0 А" || this._Is2c.Text == "0 А" ? this._analogDataBase.Value.GetIcS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[8] + this._analogDataBase.Value.GetIcS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is20.Text = this._Is20.Text == "+0 А" || this._Is20.Text == "-0 А" || this._Is20.Text == "0 А" ? this._analogDataBase.Value.GetI0S2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[10] + this._analogDataBase.Value.GetI0S2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is22.Text = this._Is22.Text == "+0 А" || this._Is22.Text == "-0 А" || this._Is22.Text == "0 А" ? this._analogDataBase.Value.GetI2S2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[11] + this._analogDataBase.Value.GetI2S2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is2n.Text = this._Is2n.Text == "+0 А" || this._Is2n.Text == "-0 А" || this._Is2n.Text == "0 А" ? this._analogDataBase.Value.GetInS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin) : symbols[9] + this._analogDataBase.Value.GetInS2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);
                this._Is2I1.Text = this._analogDataBase.Value.GetI1S2(this._averagerTime.ValueList, _currentSideStruct1, this._iMin);

                this._Is3a.Text = this._Is3a.Text == "+0 А" || this._Is3a.Text == "-0 А" || this._Is3a.Text == "0 А" ? this._analogDataBase.Value.GetIaS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[12] + this._analogDataBase.Value.GetIaS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is3b.Text = this._Is3b.Text == "+0 А" || this._Is3b.Text == "-0 А" || this._Is3b.Text == "0 А" ? this._analogDataBase.Value.GetIbS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[13] + this._analogDataBase.Value.GetIbS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is3c.Text = this._Is3c.Text == "+0 А" || this._Is3c.Text == "-0 А" || this._Is3c.Text == "0 А" ? this._analogDataBase.Value.GetIcS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[14] + this._analogDataBase.Value.GetIcS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is30.Text = this._Is30.Text == "+0 А" || this._Is30.Text == "-0 А" || this._Is30.Text == "0 А" ? this._analogDataBase.Value.GetI0S3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[16] + this._analogDataBase.Value.GetI0S3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is32.Text = this._Is32.Text == "+0 А" || this._Is32.Text == "-0 А" || this._Is32.Text == "0 А" ? this._analogDataBase.Value.GetI2S3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[17] + this._analogDataBase.Value.GetI2S3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is3n.Text = this._Is3n.Text == "+0 А" || this._Is3n.Text == "-0 А" || this._Is3n.Text == "0 А" ? this._analogDataBase.Value.GetInS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin) : symbols[15] + this._analogDataBase.Value.GetInS3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                this._Is3I1.Text = this._analogDataBase.Value.GetI1S3(this._averagerTime.ValueList, _currentSideStruct2, this._iMin);
                
                this._Is4a.Text = this._Is4a.Text == "+0 А" || this._Is4a.Text == "-0 А" || this._Is4a.Text == "0 А" ? this._analogDataBase.Value.GetIaS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[18] + this._analogDataBase.Value.GetIaS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is4b.Text = this._Is4b.Text == "+0 А" || this._Is4b.Text == "-0 А" || this._Is4b.Text == "0 А" ? this._analogDataBase.Value.GetIbS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[19] + this._analogDataBase.Value.GetIbS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is4c.Text = this._Is4c.Text == "+0 А" || this._Is4c.Text == "-0 А" || this._Is4c.Text == "0 А" ? this._analogDataBase.Value.GetIcS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[20] + this._analogDataBase.Value.GetIcS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is40.Text = this._Is40.Text == "+0 А" || this._Is40.Text == "-0 А" || this._Is40.Text == "0 А" ? this._analogDataBase.Value.GetI0S4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[22] + this._analogDataBase.Value.GetI0S4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is42.Text = this._Is42.Text == "+0 А" || this._Is42.Text == "-0 А" || this._Is42.Text == "0 А" ? this._analogDataBase.Value.GetI2S4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[23] + this._analogDataBase.Value.GetI2S4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is4n.Text = this._Is4n.Text == "+0 А" || this._Is4n.Text == "-0 А" || this._Is4n.Text == "0 А" ? this._analogDataBase.Value.GetInS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin) : symbols[21] + this._analogDataBase.Value.GetInS4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                this._Is4I1.Text = this._analogDataBase.Value.GetI1S4(this._averagerTime.ValueList, _currentSideStruct3, this._iMin);
                
                #endregion
                

                #region Дифф. токи
                this._Ida.Text = this._analogDataBase.Value.GetIda(this._averagerTime.ValueList, _currentSideStruct);
                this._Idb.Text = this._analogDataBase.Value.GetIdb(this._averagerTime.ValueList, _currentSideStruct);
                this._Idc.Text = this._analogDataBase.Value.GetIdc(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIda.Text = this._analogDataBase.Value.GetIdaPercent(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIdb.Text = this._analogDataBase.Value.GetIdbPercent(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIdc.Text = this._analogDataBase.Value.GetIdcPercent(this._averagerTime.ValueList, _currentSideStruct);
                #endregion

                #region Тормозные токи
                this._Iba.Text = this._analogDataBase.Value.GetIba(this._averagerTime.ValueList, _currentSideStruct);
                this._Ibb.Text = this._analogDataBase.Value.GetIbb(this._averagerTime.ValueList, _currentSideStruct);
                this._Ibc.Text = this._analogDataBase.Value.GetIbc(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIba.Text = this._analogDataBase.Value.GetIbaPercent(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIbb.Text = this._analogDataBase.Value.GetIbbPercent(this._averagerTime.ValueList, _currentSideStruct);
                this._percentIbc.Text = this._analogDataBase.Value.GetIbcPercent(this._averagerTime.ValueList, _currentSideStruct);
                #endregion


                #endregion

                this._pTextBox.Text = this._analogDataBase.Value.GetP(this._averagerTime.ValueList, _currentSideStruct, _currentKanalTransU);
                this._qTextBox.Text = this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, _currentSideStruct, _currentKanalTransU);
                this._cosfTextBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);

                #region Сопротивления и знаки направлений

                // вторичные значения

                this._zabBox.Text = this._analogDataBase.Value.GetVtorZab(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);
                this._zbcBox.Text = this._analogDataBase.Value.GetVtorZbc(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);
                this._zcaBox.Text = this._analogDataBase.Value.GetVtorZca(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);
                this._za1Box.Text = this._analogDataBase.Value.GetVtorZa1(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);
                this._zb1Box.Text = this._analogDataBase.Value.GetVtorZb1(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);
                this._zc1Box.Text = this._analogDataBase.Value.GetVtorZc1(this._averagerTime.ValueList, _currentSideStruct.InputCurrent, this._iMin);

                //// первичные значения
                //this._iZabBox.Text = this._analogDataBase.Value.GetZab(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);
                //this._iZbcBox.Text = this._analogDataBase.Value.GetZbc(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);
                //this._iZcaBox.Text = this._analogDataBase.Value.GetZca(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);
                //this._iZa1Box.Text = this._analogDataBase.Value.GetZa1(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);
                //this._iZb1Box.Text = this._analogDataBase.Value.GetZb1(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);
                //this._iZc1Box.Text = this._analogDataBase.Value.GetZc1(this._averagerTime.ValueList, _currentKanalTransU, _currentSideStruct, this._iMin);


                this.AB.Text = symbols[27] == "" ? "нет" : symbols[27];
                this.BC.Text = symbols[28] == "" ? "нет" : symbols[28];
                this.CA.Text = symbols[29] == "" ? "нет" : symbols[29];
                this.AN.Text = symbols[30] == "" ? "нет" : symbols[30];
                this.BN.Text = symbols[31] == "" ? "нет" : symbols[31];
                this.CN.Text = symbols[32] == "" ? "нет" : symbols[32];
                #endregion

                this._dU.Text = this._analogDataBase.Value.GetdU(this._averagerTime.ValueList);
                this._dFi.Text = this._analogDataBase.Value.GetdFi(this._averagerTime.ValueList);
                this._dF.Text = this._analogDataBase.Value.GetdF(this._averagerTime.ValueList);

    
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void MeasureTransReadFail()
        {
            if (this._activatefailmes)
            {
                this._activatefailmes = false;
                MessageBox.Show(MEASURE_TRANS_READ_FAIL);
            }
        }

        private void MeasureTransReadOk()
        {
            if (!this._activatefailmes) this._activatefailmes = true;

            this._currentSideStruct = this._measureTrans.Value.Sides[0];
            this._currentSideStruct1 = this._measureTrans.Value.Sides[1];
            this._currentSideStruct2 = this._measureTrans.Value.Sides[2];
            this._currentSideStruct3 = this._measureTrans.Value.Sides[3];

            this._currentKanalTransU = this._measureTrans.Value.Uabc[0];
            this._currentKanalTransU2 = this._measureTrans.Value.Uabc[1];
            this._currentKanalTransU3 = this._measureTrans.Value.Uabc[2];

            if (this._loaded) return;
            this._loaded = true;
            this._analogDataBase.LoadStructCycle(new TimeSpan(300));
        }
        
        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);    
            LedManager.TurnOffLeds(this._currents);
            LedManager.TurnOffLeds(this._currents1);
            LedManager.TurnOffLeds(this._resist);
            LedManager.TurnOffLeds(this._reversPow);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._externalDefensesMain);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._stateAndApv);
            LedManager.TurnOffLeds(this._stateAndApvMain);
            LedManager.TurnOffLeds(this._stateAndApv1);
            LedManager.TurnOffLeds(this._state);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._rsTriggers);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faultsMain);
            LedManager.TurnOffLeds(this._faultsMain1);
            LedManager.TurnOffLeds(this._faultsSwitch);
            LedManager.TurnOffLeds(this._faultsTn);
            LedManager.TurnOffLeds(this._faultsTn1);
            LedManager.TurnOffLeds(this._voltage);
            LedManager.TurnOffLeds(this._sinchronizm);
            LedManager.TurnOffLeds(this._splErr);
            LedManager.TurnOffLeds(this._phaseAndSw);
            LedManager.TurnOffLeds(this._urov);
            LedManager.TurnOffLeds(this._urov1);
            LedManager.TurnOffLeds(this._avr);
            LedManager.TurnOffLeds(this._avr1);
            LedManager.TurnOffLeds(this._motor);
            LedManager.TurnOffLeds(this._motor1);
            LedManager.TurnOffLeds(this._idImaxSignalsLedsMain);
            LedManager.TurnOffLeds(this._idImaxSignalsLeds);
            LedManager.TurnOffLeds(this._bgs);
            this._logicState.State = LedState.Off;
            this._mainGroup.State = LedState.Off;
            this._reservedGroup.State = LedState.Off;
            foreach (var indicator in this._indicators)
            {
                indicator.TurnOff();
            }
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            DiscretDataBaseStruct discrets = this._discretDataBase.Value;

            this._symbols = discrets.CurrentsSymbols;
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, discrets.DiscretInputs);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, discrets.InputsLogicSignals);
            //Выходные ВЛС
            LedManager.SetLeds(this._outputLogicSignals, discrets.OutputLogicSignals);
            //Защиты U,F, Q
            LedManager.SetLeds(this._voltage, discrets.VoltageFeaq);
            //Защиты I, I*, I2/I1, Ig
            LedManager.SetLeds(this._currents, discrets.MaximumCurrent);
            LedManager.SetLeds(this._currents1, discrets.MaximumCurrent);
            //Защиты по сопротивлению
            LedManager.SetLeds(this._resist, discrets.ResistDef);
            //Защиты по мощности
            LedManager.SetLeds(this._reversPow, discrets.ReversPower);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, discrets.ExternalDefenses);
            LedManager.SetLeds(this._externalDefensesMain, discrets.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, discrets.FreeLogic);
            //Состояния и АПВ
            LedManager.SetLeds(this._stateAndApv, discrets.StateAndApv);
            LedManager.SetLeds(this._stateAndApvMain, discrets.StateAndApv);
            LedManager.SetLeds(this._stateAndApv1, discrets.StateAndApv.Skip(7).ToArray());
            LedManager.SetLeds(this._state, this._discretDataBase.Value.State);
            //Реле
            LedManager.SetLeds(this._relays, discrets.Relays);
            //RS-Тригеры 
            LedManager.SetLeds(this._rsTriggers, discrets.RSTriggers);
            //Индикаторы
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(discrets.Indicators[i]);
            }
            //Контроль синхронизма
            LedManager.SetLeds(this._sinchronizm, discrets.ContrSinchr);
            //Повреждение фаз и качание
            LedManager.SetLeds(this._phaseAndSw, discrets.PhaseAndSw);
            //Контроль
            LedManager.SetLeds(this._controlSignals, discrets.ControlSignals);
            ////Неисправности основные
            LedManager.SetLeds(this._faultsMain, discrets.Faults1);
            LedManager.SetLeds(this._faultsMain1, discrets.Faults1);
            //Неисправности выключателя
            LedManager.SetLeds(this._faultsSwitch, discrets.Faults2);
            //Неисправности ТН и частоты
            LedManager.SetLeds(this._faultsTn, discrets.Faults3);
            LedManager.SetLeds(this._faultsTn1, discrets.Faults3);
            //Ошибки СПЛ
            LedManager.SetLeds(this._splErr, discrets.FaultLogicErr);
            //УРОВ
            LedManager.SetLeds(this._urov, discrets.Urov);
            LedManager.SetLeds(this._urov1, discrets.Urov);
            //АВР
            LedManager.SetLeds(this._avr, discrets.AvrAndDug);
            LedManager.SetLeds(this._avr1, discrets.AvrAndDug);
            //Двигатель
            LedManager.SetLeds(this._motor, discrets.Motor);
            LedManager.SetLeds(this._motor1, discrets.Motor);
            //Id
            LedManager.SetLeds(this._idImaxSignalsLeds, discrets.IdImaxSignals);
            LedManager.SetLeds(this._idImaxSignalsLedsMain, discrets.IdImaxSignals);

            bool res = discrets.ControlSignals[7] &&
                       !(discrets.FaultLogicErr[0] || discrets.FaultLogicErr[1] ||
                         discrets.FaultLogicErr[2] || discrets.FaultLogicErr[3]);
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
            DiscretDataBaseStruct d = (DiscretDataBaseStruct)discrets;
            LedManager.SetLeds(this._bgs, d.Bgs);
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        #endregion 

        #region [Help members]

        private void dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartStopRead();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase?.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();  
            this._measureTrans?.RemoveStructQueries();
            this._groupUstavki.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._iMinStruct.LoadStruct();
                this._groupUstavki.LoadStructCycle();
                this._discretDataBase?.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._discretDataBase?.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._groupUstavki.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();  
                this._measureTrans.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D04, RESET_FAULT_SJ);
        }
        
        private void _switchGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._groupCombo.SelectedIndex == this._groupUstavki.Value.Word) return;
            int ind = 0;
            if (this._groupCombo.SelectedIndex != -1) ind = this._groupCombo.SelectedIndex;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _mainGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 0;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _reserveGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            ind = 1;
            if (ind == this._groupUstavki.Value.Word) return;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _startOscBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D11, START_OSC);
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D09, "Включить выключатель");
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D08, "Отключить выключатель");
        }

        private void StartLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0D, true, "Запуск СПЛ", this._device);
        }

        private void StopLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if (res == DialogResult.No) return;
            this._discretDataBase.SetBitByAdress(address, command);
        }

        private void ResetInd_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D05, "Сбросить блинкеры");
        }

        private void ResetSamopodBtn_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D10, "Сбросить самоподхват");
        }

        private void CommandRunClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            int i = _commandComboBox.SelectedIndex;
            this.ConfirmCommand((ushort)(0x0D20 + i), "Выполнить команду " + (i + 1));
        }

        #endregion [Help members]

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr7); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr7MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion [INodeView Members]

       private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._activatefailmes = true;
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._activatefailmes = false;
        }
    }
}
