<?xml version="1.0" encoding="windows-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
 <script type="text/javascript">
   function translateBoolean(value, elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "��";
   else if(value.toString().toLowerCase() == "false")
   result = "���";
   else
   result = "������������ ��������"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeText(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "�� ����."
   else if(value.toString().toLowerCase() == "false")
   result = "�� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR1(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX1(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextRp(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X1�.��. �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X1�.��. �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR1line(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R1, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R1, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextR2line(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R2, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R2, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextRS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "R, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "R, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextXS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "X, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextdzS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "dz, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "dz, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextFS(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "f, ����/�, �� ���."
   else if(value.toString().toLowerCase() == "false")
   result = "f, ����/�, �� ����."
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   
   function changeTextX1Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��1, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��1, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX2Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��2, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��2, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX3Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��3, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��3, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX4Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��4, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��4, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
   function changeTextX5Y(value,elementId){
   var result = "";
   if(value.toString().toLowerCase() == "true")
   result = "X��5, �� ����./��"
   else if(value.toString().toLowerCase() == "false")
   result = "X��5, �� ����./��"
   else
   result = "xxx"
   document.getElementById(elementId).innerHTML = result;
   }
 </script>

</head>
<body>
  <xsl:variable name="vers" select="��801/������"></xsl:variable>
  <xsl:variable name="config" select="��801/������������"></xsl:variable>
  <xsl:variable name="isPrimary" select="��801/���������_�������"/>
  
  <h3>
    ���������� ��801. ����� ���������� <xsl:value-of select="��801/�����_����������"/>. ������ �� <xsl:value-of select="��801/������"/></h3>
	<p></p>
	
	<div name="�������">
	<h2>�������</h2>
	
	<table border="1" cellspacing="0">
		<tr>
			<th bgcolor="c1ced5">���� �������� �������</th>
			<th><xsl:value-of select="��801/����_��������_������/InputAdd"/></th>
		</tr>
	</table>
	<p></p>
	<table border="1" cellspacing="0">
		<tr>
			<th bgcolor="c1ced5">���� ������������� � ��������� ���������</th>
			<xsl:element name="th">
			<xsl:attribute name="id">InputParam_<xsl:value-of select="position()"/></xsl:attribute>            
			<script>translateBoolean(<xsl:value-of select="$isPrimary"/>,"InputParam_<xsl:value-of select="position()"/>");</script>
			</xsl:element>
		</tr>
	</table>
	
	<div name="groups">
    <xsl:for-each select="��801/������������_����_�����_�������/������">
      <xsl:variable name="index" select="position()"/>
      <h2 style="color:#FF0000FF">������ ������� <xsl:value-of select="position()"/></h2>

        <h3>��������� ���������</h3>
        <p></p>
        <h4>��������� ������</h4>
        <p></p>
        <table border="1" cellspacing="0">
          <tr align ="center">
            <th bgcolor="#c1ced5">����� ������</th>
            <th>
              <xsl:value-of select="MeasureTrans/CountWinding"/>
            </th>
          </tr>
        </table>
        <h5>������� 1</h5>
        <table border="1" cellspacing="0">
          <tr bgcolor="#c1ced5">
            <th>��. Ia</th>
            <th>��. Ib</th>
            <th>��. Ic</th>
            <th>��. In</th>
            <th>S�, ���</th>
            <th>U�, ��</th>
            <th>Uabc �������.</th>
            <th>Un �������.</th>
            <th>I��, A</th>
            <th>I����, A</th>
            <th>����. �����</th>
            <th>������� ����</th>
            <th>��� ��</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="MeasureTrans/Sides/���_�������">
            <xsl:if test="position() = 1">
              <tr align="center">
                <td>
                  <xsl:value-of select="InpIaStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpIbStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpIcStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpInStr"/>
                </td>
                <td>
                  <xsl:value-of select="S"/>
                </td>
                <td>
                  <xsl:value-of select="U"/>
                </td>
                <td>
                  <xsl:value-of select="PolarisUabc"/>
                </td>
                <td>
                  <xsl:value-of select="PolarisUn"/>
                </td>
                <td>
                  <xsl:value-of select="SignTT"/><xsl:value-of select="������������_��"/>
                </td>
                <td>
                  <xsl:value-of select="SignTTNP"/><xsl:value-of select="������������_����"/>
                </td>
                <td>
                  <xsl:value-of select="CoeffTT"/>
                </td>
                <td>
                  <xsl:value-of select="InputCurrentStr"/>
                </td>
                <td>
                  <xsl:value-of select="TypeTT"/>
                </td>
                <td>
                  <xsl:value-of select="SideType"/>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
      <p></p>

      <h5>������� 2</h5>
        <table border="1" cellspacing="0">
          <tr bgcolor="#c1ced5">
            <th>��. Ia</th>
            <th>��. Ib</th>
            <th>��. Ic</th>
            <th>��. In</th>
            <th>U�, ��</th>
            <th>Uabc �������.</th>
            <th>Un �������.</th>
            <th>I��, A</th>
            <th>I����, A</th>
            <th>����. �����</th>
            <th>���</th>
            <th>������</th>
          </tr>
          <xsl:for-each select="MeasureTrans/Sides/���_�������">
            <xsl:if test="position() = 2">
              <tr align="center">
                <td>
                  <xsl:value-of select="InpIaStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpIbStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpIcStr"/>
                </td>
                <td>
                  <xsl:value-of select="InpInStr"/>
                </td>
                <td>
                  <xsl:value-of select="U"/>
                </td>
                <td>
                  <xsl:value-of select="PolarisUabc"/>
                </td>
                <td>
                  <xsl:value-of select="PolarisUn"/>
                </td>
                <td>
                  <xsl:value-of select="SignTT"/><xsl:value-of select="������������_��"/>
                </td>
                <td>
                  <xsl:value-of select="SignTTNP"/><xsl:value-of select="������������_����"/>
                </td>
                <td>
                  <xsl:value-of select="CoeffTT"/>
                </td>
                <td>
                  <xsl:value-of select="SideType"/>
                </td>
                <td>
                  <xsl:value-of select="Group"/>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
      
      <p></p>

      <h5>������� 3</h5>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>��. Ia</th>
          <th>��. Ib</th>
          <th>��. Ic</th>
          <th>��. In</th>
          <th>U�, ��</th>
          <th>Uabc �������.</th>
          <th>Un �������.</th>
          <th>I��, A</th>
          <th>I����, A</th>
          <th>����. �����</th>
          <th>���</th>
          <th>������</th>
        </tr>
        <xsl:for-each select="MeasureTrans/Sides/���_�������">
          <xsl:if test="position() = 3">
            <tr align="center">
              <td>
                <xsl:value-of select="InpIaStr"/>
              </td>
              <td>
                <xsl:value-of select="InpIbStr"/>
              </td>
              <td>
                <xsl:value-of select="InpIcStr"/>
              </td>
              <td>
                <xsl:value-of select="InpInStr"/>
              </td>
              <td>
                <xsl:value-of select="U"/>
              </td>
              <td>
                <xsl:value-of select="PolarisUabc"/>
              </td>
              <td>
                <xsl:value-of select="PolarisUn"/>
              </td>
              <td>
                <xsl:value-of select="SignTT"/><xsl:value-of select="������������_��"/>
              </td>
              <td>
                <xsl:value-of select="SignTTNP"/><xsl:value-of select="������������_����"/>
              </td>
              <td>
                <xsl:value-of select="CoeffTT"/>
              </td>
              <td>
                <xsl:value-of select="SideType"/>
              </td>
              <td>
                <xsl:value-of select="Group"/>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>
      
      <p></p>

      <h5>������� 4</h5>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>��. Ia</th>
          <th>��. Ib</th>
          <th>��. Ic</th>
          <th>��. In</th>
          <th>U�, ��</th>
          <th>Uabc �������.</th>
          <th>Un �������.</th>
          <th>I��, A</th>
          <th>I����, A</th>
          <th>����. �����</th>
          <th>���</th>
          <th>������</th>
        </tr>
        <xsl:for-each select="MeasureTrans/Sides/���_�������">
          <xsl:if test="position() = 4">
            <tr align="center">
              <td>
                <xsl:value-of select="InpIaStr"/>
              </td>
              <td>
                <xsl:value-of select="InpIbStr"/>
              </td>
              <td>
                <xsl:value-of select="InpIcStr"/>
              </td>
              <td>
                <xsl:value-of select="InpInStr"/>
              </td>
              <td>
                <xsl:value-of select="U"/>
              </td>
              <td>
                <xsl:value-of select="PolarisUabc"/>
              </td>
              <td>
                <xsl:value-of select="PolarisUn"/>
              </td>
              <td>
                <xsl:value-of select="SignTT"/><xsl:value-of select="������������_��"/>
              </td>
              <td>
                <xsl:value-of select="SignTTNP"/><xsl:value-of select="������������_����"/>
              </td>
              <td>
                <xsl:value-of select="CoeffTT"/>
              </td>
              <td>
                <xsl:value-of select="SideType"/>
              </td>
              <td>
                <xsl:value-of select="Group"/>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <p></p>

      <h5>������ Uabc1</h5>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>���������</th>
          <th>�����������</th>
          <th>��. Ua</th>
          <th>��. Ub</th>
          <th>��. Uc</th>
          <th>���</th>
          <th>���� ������. ��</th>
        </tr>
        <xsl:for-each select="MeasureTrans/Uabc/���_������">
          <xsl:if test="position() = 1">
            <tr align="center">
              <td>
                <xsl:value-of select="KanalUType"/>
              </td>
              <td>
                <xsl:value-of select="KanalUSide"/>
              </td>
              <td>
                <xsl:value-of select="InpUaStr"/>
              </td>
              <td>
                <xsl:value-of select="InpUbStr"/>
              </td>
              <td>
                <xsl:value-of select="InpUcStr"/>
              </td>
              <td>
                <xsl:value-of select="KTH"/>
              </td>
              <td>
                <xsl:value-of select="AlmInp"/>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <p></p>

      <h5>������ Uabc2</h5>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>���������</th>
          <th>�����������</th>
          <th>��. Ua</th>
          <th>��. Ub</th>
          <th>��. Uc</th>
          <th>���</th>
          <th>���� ������. ��</th>
        </tr>
        <xsl:for-each select="MeasureTrans/Uabc/���_������">
          <xsl:if test="position() = 2">
            <tr align="center">
              <td>
                <xsl:value-of select="KanalUType"/>
              </td>
              <td>
                <xsl:value-of select="KanalUSide"/>
              </td>
              <td>
                <xsl:value-of select="InpUaStr"/>
              </td>
              <td>
                <xsl:value-of select="InpUbStr"/>
              </td>
              <td>
                <xsl:value-of select="InpUcStr"/>
              </td>
              <td>
                <xsl:value-of select="KTH"/>
              </td>
              <td>
                <xsl:value-of select="AlmInp"/>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <p></p>

      <h5>����� Un</h5>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>�����������</th>
          <th>��. Un</th>
          <th>���</th>
          <th>���� ������. ��</th>
        </tr>
        <xsl:for-each select="MeasureTrans/Uabc/���_������">
          <xsl:if test="position() = 3">
            <tr align="center">
              <td>
                <xsl:value-of select="KanalUSide"/>
              </td>
              <td>
                <xsl:value-of select="InpUaStr"/>
              </td>
              <td>
                <xsl:value-of select="KTH"/>
              </td>
              <td>
                <xsl:value-of select="AlmInp"/>
              </td>
            </tr>
          </xsl:if>
        </xsl:for-each>
      </table>

      <p></p>

      <h4>���������</h4>
      <table border="1" cellspacing="0">
        <tr bgcolor="#c1ced5">
          <th>T �������,c</th>
          <th>� ����������, �</th>
          <th>I��, I�</th>
          <th>I����, I�</th>
          <th>T����, ��</th>
          <th>T����, �</th>
          <th>Q���, %</th>
          <th>���� Q �����</th>
          <th>���� N �����</th>
        </tr>
        <xsl:for-each select="TermConfig">
            <tr align="center">
              <td>
                <xsl:value-of select="T���"/>
              </td>
              <td>
                <xsl:value-of select="T���"/>
              </td>
              <td>
                <xsl:value-of select="I��"/>
              </td>
              <td>
                <xsl:value-of select="I����"/>
              </td>
              <td>
                <xsl:value-of select="T����"/>
              </td>
              <td>
                <xsl:value-of select="T����"/>
              </td>
              <td>
                <xsl:value-of select="Q"/>
              </td>
              <td>
                <xsl:value-of select="Q_�����"/>
              </td>
              <td>
                <xsl:value-of select="N_�����"/>
              </td>
            </tr>
        </xsl:for-each>
      </table>

      <p></p>

        <h4>�������� ����� ��</h4>

            <table border="1" cellpadding="4" cellspacing="0">
              <tr bgcolor="c1ced5">
                <th>������������� I2</th>
                <th>I2, I�</th>
                <th>U2, �</th>
                <th>������������� 3I0</th>
                <th>3I0, I�</th>
                <th>3U0, �</th>
                <th>Umax, �</th>
                <th>Umin, �</th>
                <th>Imax, I�</th>
                <th>Imin, I�</th>
                <th>Td, ��</th>
                <th>Ts, ��</th>
                <th>�����</th>
                <th>���. 3-� ���</th>
                <th>dI, %</th>
                <th>dU, %</th>
              </tr>
              <xsl:for-each select="CheckTn">
              <tr align="center">
                <td><xsl:for-each select="U2I2"><xsl:if test="current() = 'true'">��</xsl:if><xsl:if test="current() = 'false'">���</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="I2"/></td>
                <td><xsl:value-of select="U2"/></td>
                <td><xsl:for-each select="U0I0"><xsl:if test="current() = 'true'">��</xsl:if><xsl:if test="current() = 'false'">���</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="I0"/></td>
                <td><xsl:value-of select="U0"/></td>
                <td><xsl:value-of select="Umax"/></td>
                <td><xsl:value-of select="Umin"/></td>
                <td><xsl:value-of select="Imax"/></td>
                <td><xsl:value-of select="Imin"/></td>
                <td><xsl:value-of select="Td"/></td>
                <td><xsl:value-of select="Ts"/></td>
                <td><xsl:value-of select="Reset"/></td>
                <td><xsl:for-each select="Phase3"><xsl:if test="current() = 'true'">��</xsl:if><xsl:if test="current() = 'false'">���</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="Idelta"/></td>
                <td><xsl:value-of select="Udelta"/></td>
              </tr>
              </xsl:for-each>
            </table>

        <p></p>
      
      <h3>���. ������</h3>

      <p></p>

      <h4>���. ������� ������</h4>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>���������</th>
          <th>����������</th>
          <th>������� I��</th>
          <th>�������� ������� T��</th>
          <th>�����������</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <tr align="center">
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/����������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/�������_I��"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/��������_�������_t��"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/�����������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/����"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���"/></td>
        </tr>
      </table>
      <p></p>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>���������� I2/I1</th>
          <th>������. ����</th>
          <th>I2/I1 </th>
        </tr>
        <tr align="center">
          <td><xsl:for-each select="DefensesSetpoints/���_�������/����������_I2_x002F_I1"><xsl:if test="current() = 'true'">��</xsl:if><xsl:if test="current() = 'false'">���</xsl:if></xsl:for-each></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/������_����_I2I1"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/I2I1"/></td>
        </tr>
      </table>
      <p></p>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>���������� I2/I1</th>
          <th>������. ����</th>
          <th>I5/I1 </th>
        </tr>
        <tr align="center">
          <td><xsl:for-each select="DefensesSetpoints/���_�������/����������_I5_x002F_I1"><xsl:if test="current() = 'true'">��</xsl:if><xsl:if test="current() = 'false'">���</xsl:if></xsl:for-each></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/������_����_I5I1"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/I5I1"/></td>
        </tr>
      </table>
      
      <p></p>

      <h4>�������������� ����������</h4>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>I�1 - ������</th>
          <th>f1 - ���� �������</th>
          <th>I�2 - ������</th>
          <th>f2 - ���� �������</th>
        </tr>
        <tr align="center">
          <td><xsl:value-of select="DefensesSetpoints/���_�������/I�1_������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/f1_����_�������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/I�2_������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/f2_����_�������"/></td>
        </tr>
      </table>
      
      <p></p>

      <h4>���. �������</h4>
      <p></p>
      <h5>���. ������� ������� ��� ����������</h5>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>���������</th>
          <th>����������</th>
          <th>������� I��</th>
          <th>�������� ������� T��</th>
          <th>������� �� ���������� ���������</th>
          <th>�����������</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <tr align="center">
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/����������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/�������_I��"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/��������_�������_t��"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/�������_��_����������_���������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/�����������"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/����"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���"/></td>
          <td><xsl:value-of select="DefensesSetpoints/���_�������/���"/></td>
        </tr>
      </table>
      <p></p>

      <h4>���. ���. ������������������</h4>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>�������</th>
          <th>�����</th>
          <th>����������</th>
          <th>������� I��, I�</th>
          <th>�������</th>
          <th>t��, ��</th>
          <th>l�1, l� �������</th>
          <th>���� f1</th>
          <th>l�2, l� �������</th>
          <th>���� f2</th>
          <th>�����������</th>
          <th>����</th>
          <th>���</th>
          <th>���</th>
        </tr>
        <xsl:for-each select="DefensesSetpoints/���_�������_������������������/���_I�_x002A_/DiffIzeroStruct">
          <tr align="center">
            <td>������� Id0&gt; <xsl:value-of select="position()"/></td>
            <td><xsl:value-of select="�����"/></td>
            <td><xsl:value-of select="����������"/></td>
            <td><xsl:value-of select="�������_I��"/></td>
            <td><xsl:value-of select="�������"/></td>
            <td><xsl:value-of select="t��"/></td>
            <td><xsl:value-of select="I�1_������"/></td>
            <td><xsl:value-of select="f1_����_�������"/></td>
            <td><xsl:value-of select="I�2_������"/></td>
            <td><xsl:value-of select="f2_����_�������"/></td>
            <td><xsl:value-of select="�����������"/></td>
            <td><xsl:value-of select="����"/></td>
            <td><xsl:value-of select="���"/></td>
            <td><xsl:value-of select="���"/></td>
          </tr>
        </xsl:for-each>
      </table>
        <p></p>

        <h3>�������.���.</h3>
        <p></p>

        <h4>����� ���������</h4>
        <p></p>

        <h4>�����. ����������� ��</h4>
        <table border="1" cellspacing="0">
          <th>
            <table>
              <tr>������ �-N1</tr>
              <tr>
                <th bgcolor="c1ced5">Z0=</th>
                <th><xsl:value-of select="Resistance/R0Step1"/></th>
                <th>+j</th>
                <th><xsl:value-of select="Resistance/X0Step1"/></th>
                <xsl:element name="th">
                  <xsl:attribute name="id">Change_<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeText(<xsl:value-of select="$isPrimary"/>,"Change_<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
              <tr>
                <th bgcolor="c1ced5">Z1=</th>
                <th><xsl:value-of select="Resistance/R1Step1"/></th>
                <th>+j</th>
                <th><xsl:value-of select="Resistance/X1Step1"/></th>
                <xsl:element name="th">
                  <xsl:attribute name="id">_Change1<xsl:value-of select="position()"/></xsl:attribute>
                  <script>changeText(<xsl:value-of select="$isPrimary"/>,"_Change1<xsl:value-of select="position()"/>");</script>
                </xsl:element>
              </tr>
            </table>
          </th>
        </table>
        <p></p>

        <h4>���� ��������</h4>

        <table border="1" cellspacing="0">
          <tr>��� ������� �-�</tr>
          <tr bgcolor="c1ced5" align="center">
            <th>y1, ����</th>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeLine<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextR1line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeLine1<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextR2line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine1<xsl:value-of select="position()"/>");</script>
            </xsl:element>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="AcCountLoad/CornerLine"/></td>
            <td><xsl:value-of select="AcCountLoad/R1Line"/></td>
            <td><xsl:value-of select="AcCountLoad/R2Line"/></td>
          </tr>
        </table>
        <table border="1" cellspacing="0">
          <tr>��� ������� �-N</tr>
          <tr bgcolor="c1ced5" align="center">
            <th>y1, ����</th>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeLine3<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextR1line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine3<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeLine4<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextR2line(<xsl:value-of select="$isPrimary"/>,"_ChangeLine4<xsl:value-of select="position()"/>");</script>
            </xsl:element>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="AcCountLoad/CornerFaza"/></td>
            <td><xsl:value-of select="AcCountLoad/R1Faza"/></td>
            <td><xsl:value-of select="AcCountLoad/R2Faza"/></td>
          </tr>
        </table>

        <h4>�������</h4>

        <table border="1" cellspacing="0" cellpadding="4">
          <tr bgcolor="c1ced5" align="center">
            <th>���</th>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeRS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextRS(<xsl:value-of select="$isPrimary"/>,"_ChangeRS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeXS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextXS(<xsl:value-of select="$isPrimary"/>,"_ChangeXS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangedzS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextdzS(<xsl:value-of select="$isPrimary"/>,"_ChangedzS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <th>f/r</th>
            <th>Tdz, ��</th>
            <th>3I0�, I�</th>
            <th>I�, I�</th>
            <th>����� ���������� �� �������</th>
            <th>T�, ��</th>
          </tr>
          <tr align="center">
            <td><xsl:value-of select="Swing/Type"/></td>
            <td><xsl:value-of select="Swing/R1"/></td>
            <td><xsl:value-of select="Swing/X1"/></td>
            <td><xsl:value-of select="Swing/Dr"/></td>
            <td><xsl:value-of select="Swing/C1"/></td>
            <td><xsl:value-of select="Swing/Tsrab"/></td>
            <td><xsl:value-of select="Swing/I0"/></td>
            <td><xsl:value-of select="Swing/Imin"/></td>
            <xsl:element name="td">
              <xsl:attribute name="id">_Reset<xsl:value-of select="position()"/></xsl:attribute>
              <script>translateBoolean(<xsl:value-of select="Swing/Reset"/>,"_Reset<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <td><xsl:value-of select="Swing/ResetTime"/></td>
          </tr>
        </table>
        <p></p>

        <h4>���� ������������ �������������� ��� �������� Z</h4>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>
            <th bgcolor="c1ced5">y1, ����.</th>
            <th><xsl:value-of select="Resistance/C1"/></th>
          </tr>
          <tr>
            <th bgcolor="c1ced5">y1, ����.</th>
            <th><xsl:value-of select="Resistance/C2"/></th>
          </tr>
        </table>

        <h4>�������</h4>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="c1ced5" align="center">
            <th>������� �</th>
            <th>�����</th>
            <th>���</th>
            <th>����������</th>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeRSS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextRS(<xsl:value-of select="$isPrimary"/>,"_ChangeRSS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangeXSS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextXS(<xsl:value-of select="$isPrimary"/>,"_ChangeXSS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <xsl:element name="th">
              <xsl:attribute name="id">_ChangefS<xsl:value-of select="position()"/></xsl:attribute>
              <script>changeTextFS(<xsl:value-of select="$isPrimary"/>,"_ChangefS<xsl:value-of select="position()"/>");</script>
            </xsl:element>
            <th>tcp, ��</th>
            <th>Icp, In</th>
            <th>���� ���������</th>
            <th>ty, ��</th>
            <th>�����������</th>
            <th>U����</th>
            <th>U����, �</th>
            <th>������</th>
            <th>����. ������. ��</th>
            <th>����. �� ��������</th>
            <th>����. �� �������</th>
            <th>��������. ��� �����.</th>
            <th>���� �� ���</th>
            <th>����� 1��� ��� ����*</th>
            <th>�����������</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/��_�������������/���_�������������_������">
            <tr align="center">
              <td><xsl:value-of select="position()"/></td>
              <td><xsl:value-of select="Mode"/></td>
              <td><xsl:value-of select="Type"/></td>
              <td><xsl:value-of select="BlocInp"/></td>
              <td><xsl:value-of select="UstR"/></td>
              <td><xsl:value-of select="UstX"/></td>
              <td><xsl:value-of select="Ustr"/></td>
              <td><xsl:value-of select="Tsr"/></td>
              <td><xsl:value-of select="Isr"/></td>
              <td><xsl:value-of select="Acceleration"/></td>
              <td><xsl:value-of select="Tu"/></td>
              <td><xsl:value-of select="Direction"/></td>
              <td><xsl:for-each select="StartOnU"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:value-of select="Ustart"/></td>
              <td><xsl:value-of select="Conture"/></td>
              <td><xsl:value-of select="BlockFromTn"/></td>
              <td><xsl:for-each select="BlockFromLoad"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="BlockFromSwing"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="SteeredModeAcceler"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="DamageFaza"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="ResetStep"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:value-of select="Oscilloscope"/></td>
              <td><xsl:for-each select="Urov"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:value-of select="���_�����"/></td>
              <td><xsl:value-of select="���_�����"/></td>
            </tr>
          </xsl:for-each>
        </table>

      <h3>������ �� ����</h3>

        <h4>���� ����� ��� ������� ������������ �����</h4>
        <table border="1" cellspacing="0" cellpadding="4">
          <tr>
            <th bgcolor="c1ced5">fi1</th>
            <th><xsl:value-of select="DefensesSetpoints/����/I"/></th>
            <th bgcolor="c1ced5">fi0</th>
            <th><xsl:value-of select="DefensesSetpoints/����/I0"/></th>
          </tr>
          <tr>
            <th bgcolor="c1ced5">fin</th>
            <th><xsl:value-of select="DefensesSetpoints/����/In"/></th>
            <th bgcolor="c1ced5">fi2</th>
            <th><xsl:value-of select="DefensesSetpoints/����/I2"/></th>
          </tr>
        </table>
        <p></p>

        <h4>���. I</h4>
        <table border="1" cellspacing="0" cellpadding="4">
          <tr>������ I&gt;1 - I&gt;4 ������������� ����</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>I��, I� ��</th>
            <th>�������</th>
            <th>���� �� U</th>
            <th>U����, �</th>
            <th>���. �� ������. ��</th>
            <th>�����������</th>
            <th>������. ����.</th>
            <th>������</th>
            <th>������-��</th>
            <th>t��., ��/����</th>
            <th>k �����. ���-��</th>
            <th>���� ���������</th>
            <th>Ty, ��</th>
            <th>����������</th>
            <th>I2�/I1�, %</th>
            <th>����. �� I2�/I1�</th>
            <th>������. ����.</th>
            <th>������. ��� �����.</th>
            <th>�����������</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
            <tr align="center">
              <xsl:if test="position() &lt;5">
                <td>������� I><xsl:value-of select="position()"/></td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="�������"/></td>
                <td><xsl:value-of select="��������"/></td>
                <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="U_����"/></td>
                <td><xsl:value-of select="����_��_������_��"/></td>
                <td><xsl:value-of select="�����������"/></td>
                <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                <td><xsl:value-of select="������"/></td>
                <td><xsl:value-of select="��������������"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td><xsl:value-of select="K"/></td>
                <td><xsl:value-of select="Uskor"/></td>
                <td><xsl:value-of select="ty"/></td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="�������_2�_1�"/></td>
                <td><xsl:for-each select="����_2�_1�"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="������_����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���_�����"/></td>
                <td><xsl:value-of select="���_�����"/></td>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr>������ I&gt;5, I&gt;5 ������������� ����</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>I��, I� ��</th>
            <th>�������</th>
            <th>�������</th>
            <th>���� �� U</th>
            <th>U����, �</th>
            <th>���. �� ������. ��</th>
            <th>�����������</th>
            <th>������. ����.</th>
            <th>������</th>
            <th>������-��</th>
            <th>t��., ��/����</th>
            <th>k �����. ���-��</th>
            <th>���� ���������</th>
            <th>Ty, ��</th>
            <th>����������</th>
            <th>I2�/I1�, %</th>
            <th>����. �� I2�/I1�</th>
            <th>������. ����.</th>
            <th>������. ��� �����.</th>
            <th>�����������</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
            <tr align="center">
              <xsl:if test="position() &gt; 4">
                <xsl:if test="position() &lt;7">
                  <td>������� I> <xsl:value-of select="position()"/></td>
                  <td><xsl:value-of select="�����"/></td>
                  <td><xsl:value-of select="�������"/></td>
                  <td><xsl:value-of select="�������"/></td>
                  <td><xsl:value-of select="��������"/></td>
                  <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="U_����"/></td>
                  <td><xsl:value-of select="����_��_������_��"/></td>
                  <td><xsl:value-of select="�����������"/></td>
                  <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                  <td><xsl:value-of select="������"/></td>
                  <td><xsl:value-of select="��������������"/></td>
                  <td><xsl:value-of select="t��"/></td>
                  <td><xsl:value-of select="K"/></td>
                  <td><xsl:value-of select="Uskor"/></td>
                  <td><xsl:value-of select="ty"/></td>
                  <td><xsl:value-of select="����������"/></td>
                  <td><xsl:value-of select="�������_2�_1�"/></td>
                  <td><xsl:for-each select="����_2�_1�"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������_����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���"/></td>
                  <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                  <td><xsl:value-of select="���_�����"/></td>
                  <td><xsl:value-of select="���_�����"/></td>
                </xsl:if>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ I&#60; ������������ ����</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>I��, I� ��</th>
            <th>�������</th>
            <th>������</th>
            <th>t, ��</th>
            <th>����������</th>
            <th>�����������</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/I/���_���/MtzMainStruct">
            <tr align="center">
              <xsl:if test="position() =7">
                <td>������� I&#60;</td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="�������"/></td>
                <td><xsl:value-of select="��������"/></td>
                <td><xsl:value-of select="������"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���_�����"/></td>
                <td><xsl:value-of select="���_�����"/></td>
              </xsl:if>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <h4>������ I*</h4>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="c1ced5" align="center">
            <th>�������</th>
            <th>���������</th>
            <th>I, I� ��</th>
            <th>�������</th>
            <th>���� �� U</th>
            <th>U���� ,�</th>
            <th>�����������</th>
            <th>������. ������.</th>
            <th>I*</th>
            <th>��������������</th>
            <th>t, ��</th>
            <th>k �����. ���-��</th>
            <th>����������</th>
            <th>�����������</th>
            <th>���� ���������</th>
            <th>Ty ,��</th>
            <th>������. ��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/I_x002A_/���_I_x002A_/DefenseStarStruct">
              <tr align="center">
                <td>������� I*><xsl:value-of select="position()"/></td>
                <td><xsl:value-of select="�����"/></td>
                <td><xsl:value-of select="�������"/></td>
                <td><xsl:value-of select="��������"/></td>
                <td><xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="U_����"/></td>
                <td><xsl:value-of select="�����������"/></td>
                <td><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></td>
                <td><xsl:value-of select="I_x002A_"/></td>
                <td><xsl:value-of select="��������������"/></td>
                <td><xsl:value-of select="t��"/></td>
                <td><xsl:value-of select="K"/></td>
                <td><xsl:value-of select="����������"/></td>
                <td><xsl:value-of select="���"/></td>
                <td><xsl:value-of select="Uskor"/></td>
                <td><xsl:value-of select="ty"/></td>
                <td><xsl:for-each select="������.��������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:value-of select="���_�����"/></td>
                <td><xsl:value-of select="���_�����"/></td>
              </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <h4>I2I1</h4>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr align="center">
            <th bgcolor="c1ced5">�����</th>
            <td><xsl:value-of select="DefensesSetpoints/I2I1/�����"/></td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">����������</th>
            <td><xsl:value-of select="DefensesSetpoints/I2I1/����������"/></td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">I2/I1</th>
            <td>
              <xsl:value-of select="DefensesSetpoints/I2I1/�������_I2_I1"/>%|������� -
              <xsl:value-of select="DefensesSetpoints/I2I1/��������"/>
            </td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">tcp, ��</th>
            <td>
              <xsl:value-of select="DefensesSetpoints/I2I1/t��"/>
            </td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">���.</th>
            <td>
              <xsl:value-of select="DefensesSetpoints/I2I1/���"/>
            </td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">����</th>
            <td><xsl:for-each select="DefensesSetpoints/I2I1/����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">���</th>
            <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
          </tr>
          <tr align="center">
            <th bgcolor="c1ced5">���</th>
            <td><xsl:value-of select="DefensesSetpoints/I2I1/���"/></td>
          </tr>
        </table>
        <p></p>

      <h4>���� ���. ���.</h4>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr align="center">
          <th bgcolor="c1ced5">�����</th>
          <td><xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/�����"/></td>
        </tr>
        <tr align="center">
          <th bgcolor="c1ced5">����������</th>
          <td><xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/����������"/></td>
        </tr>
        <tr align="center">
          <th bgcolor="c1ced5">I2/I1</th>
          <td>
            <xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/�������"/>%|������� -
            <xsl:value-of select="DefensesSetpoints/������������_�����_�������_������/��������"/>
          </td>
        </tr>
        <tr align="center">
          <th bgcolor="c1ced5">���.</th>
          <td><xsl:for-each select="DefensesSetpoints/������������_�����_�������_������/�����������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
        </tr>
      </table>
      <p></p>

        <h4>���. U</h4>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ U></tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>U��, �</th>
            <th>t��, ��</th>
            <th>t��, ��</th>
            <th>U���, �</th>
            <th>�������</th>
            <th>����������</th>
            <th>�����������</th>
            <th>��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>����� �������</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/Ub/���_������_U/DefenceUStruct">
            <xsl:if test="position()&lt;5">
              <tr align="center">
                <td>
                  ������� U><xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="���_Umax"/>
                </td>
                <td>
                  <xsl:value-of select="��������"/>
                </td>
                <td>
                  <xsl:value-of select="�������"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="U��"/>
                </td>
                <td><xsl:for-each select="U��_����_x002F_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
                <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ U&#60;</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>���</th>
            <th>������</th>
            <th>U��, �</th>
            <th>t��, ��</th>
            <th>t��, ��</th>
            <th>U���, �</th>
            <th>�������</th>
            <th>����-�� U&#60;5�</th>
            <th>����. �� ������. ��</th>
            <th>����������</th>
            <th>�����������</th>
            <th>��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>����� �������</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/Ub/���_������_U/DefenceUStruct">
            <xsl:if test="position() &gt; 4">
              <tr align="center">
                <td>������� U&#60;1</td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="���_Umin"/>
                </td>
                <td>
                  <xsl:value-of select="��������"/>
                </td>
                <td>
                  <xsl:value-of select="�������"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="U��"/>
                </td>
                <td><xsl:for-each select="U��_����_x002F_���"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="����������_U_5V"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="����_��_������_��"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
                <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        
        <p></p>

        <h4>���. F</h4>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ F&#62;</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>���</th>
            <th>F��, ��</th>
            <th>U1, �</th>
            <th>tcp, ��</th>
            <th>t��, ��</th>
            <th>F��, ��</th>
            <th>�������</th>
            <th>����������</th>
            <th>�����������</th>
            <th>��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>����� �������</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/Fb/���_������_F/DefenseFStruct">
            <tr align="center">
              <td>������� F&#62;<xsl:value-of select="position()"/></td>
              <td>
                <xsl:value-of select="�����"/>
              </td>
              <td>
                <xsl:value-of select="���"/>
              </td>
              <td>
                <xsl:value-of select="�������"/>
              </td>
              <td>
                <xsl:value-of select="U1"/>
              </td>
              <td>
                <xsl:value-of select="t��"/>
              </td>
              <td>
                <xsl:value-of select="t��"/>
              </td>
              <td>
                <xsl:value-of select="F��"/>
              </td>
              <td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="����������"/>
              </td>
              <td>
                <xsl:value-of select="���"/>
              </td>
              <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="���_�����"/>
              </td>
              <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="���"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ F&#60;</tr>
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>���</th>
            <th>F��, ��</th>
            <th>U1, �</th>
            <th>tcp, ��</th>
            <th>t��, ��</th>
            <th>F��, ��</th>
            <th>�������</th>
            <th>����������</th>
            <th>�����������</th>
            <th>��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>����� �������</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/Fm/���_������_F/DefenseFStruct">
            <tr align="center">
              <td>
                ������� F&#60;<xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select="�����"/>
              </td>
              <td>
                <xsl:value-of select="���"/>
              </td>
              <td>
                <xsl:value-of select="�������"/>
              </td>
              <td>
                <xsl:value-of select="U1"/>
              </td>
              <td>
                <xsl:value-of select="t��"/>
              </td>
              <td>
                <xsl:value-of select="t��"/>
              </td>
              <td>
                <xsl:value-of select="F��"/>
              </td>
              <td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="����������"/>
              </td>
              <td>
                <xsl:value-of select="���"/>
              </td>
              <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="���_�����"/>
              </td>
              <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
              <td>
                <xsl:value-of select="���"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

        <p></p>

        <h4>������ Q</h4>
        <table border="1" cellpadding="4" cellspacing="0">
         <tr>���������� �� ��������� ��������� Q</tr>
          <tr bgcolor="c1ced5">
            <th>�����</th>
            <th>������� Q���, %</th>
            <th>����� t���., �</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="DefensesSetpoints/��������/�����"/>
            </td>
            <td>
              <xsl:value-of select="DefensesSetpoints/��������/�������_������������"/>
            </td>
            <td>
              <xsl:value-of select="DefensesSetpoints/��������/�����_������������"/>
            </td>
          </tr>
        </table>
        <table border="1" cellpadding="4" cellspacing="0">
        <tr>���������� �� ����� �����</tr>
        <tr bgcolor="c1ced5">
          <th>����� ����� ������ N����</th>
          <th>����� ������� ������ N���.</th>
          <th>����� t���, c</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="DefensesSetpoints/��������/�����_�������_������"/>
          </td>
          <td>
            <xsl:value-of select="DefensesSetpoints/��������/�����_��������_������"/>
          </td>
          <td>
            <xsl:value-of select="DefensesSetpoints/��������/�����_����������"/>
          </td>
        </tr>
        </table>
        <p></p>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>�����</th>
            <th>������� Q, %</th>
            <th>����������</th>
            <th>���.</th>
            <th>����</th>
            <th>���</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/Q/���_������_Q/DefenseQStruct">
            <xsl:if test="position()=1">
              <tr align="center">
                <td>Q&#62;</td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="Ustavka"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td> 
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
              </tr>
            </xsl:if>
            <xsl:if test="position()=2">
              <tr align="center">
                <td>Q&#62;&#62;</td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="Ustavka"/>
                </td>
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td> 
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>

        <h4>�������</h4>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�������</th>
            <th>���������</th>
            <th>������ ������������</th>
            <th>t��, ��</th>
            <th>t��, ��</th>
            <th>������ �������</th>
            <th>�������</th>
            <th>������ ����������</th>
            <th>�����������</th>
            <th>��� �����.</th>
            <th>����</th>
            <th>���</th>
            <th>����� �������</th>
            <th>���</th>
          </tr>
          <xsl:for-each select="DefensesSetpoints/�������/���_�������/DefenseExternalStruct">
              <tr align="center">
                <td>
                  ������� <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:value-of select="�����"/>
                </td>
                <td>
                  <xsl:value-of select="����_���_�������_�����"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="t��"/>
                </td>
                <td>
                  <xsl:value-of select="������_��������"/>
                </td>
                <td><xsl:for-each select="�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td> 
                <td>
                  <xsl:value-of select="����������"/>
                </td>
                <td>
                  <xsl:value-of select="���"/>
                </td>
                <td><xsl:for-each select="���_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td><xsl:for-each select="����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
                <td><xsl:for-each select="�����_�������"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td>
                <td>
                  <xsl:value-of select="���_�����"/>
                </td>
              </tr>
          </xsl:for-each>
        </table>
        <p></p>

      <h4>���. P</h4>
      <table border="1" cellpadding="4" cellspacing="0">
        <tr bgcolor="c1ced5">
          <th>�������</th>
          <th>�����</th>
          <th>S��[S�]</th>
          <th>���[']</th>
          <th>t��[��]</th>
          <th>t��[��]</th>
          <th>S��[S�]</th>
          <th>S��[����, ���]</th>
          <th>I��[�]</th>
          <th>����������</th>
          <th>���. �� ������. ��</th>
          <th>���.</th>
          <th>��� �����.</th>
          <th>����</th>
          <th>���</th>
          <th>����� �������</th>
        </tr>
        <xsl:for-each select="DefensesSetpoints/��_��������/���_P/ReversePowerStruct">
          <tr align="center">
            <td>
              ������� P <xsl:value-of select="position()"/>
            </td>
            <td>
              <xsl:value-of select="�����"/>
            </td>
            <td>
              <xsl:value-of select="�������_������������"/>
            </td>
            <td>
              <xsl:value-of select="����_������������"/>
            </td>
            <td>
              <xsl:value-of select="t��"/>
            </td>
            <td>
              <xsl:value-of select="t��"/>
            </td>
            <td>
              <xsl:value-of select="�������_��������"/>
            </td>
            <td>
              <xsl:for-each select="�������_��������_����_���">
                <xsl:if test="current() ='false'">���	</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
            <td>
              <xsl:value-of select="���_������������"/>
            </td>
            <td>
              <xsl:value-of select="����������"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
            <td>
              <xsl:value-of select="���_�������"/>
            </td>
            <td>
              <xsl:value-of select="����"/>
            </td>
            <td>
              <xsl:value-of select="���"/>
            </td>
            <td>
              <xsl:for-each select="�����_�������">
                <xsl:if test="current() ='false'">���	</xsl:if>
                <xsl:if test="current() ='true'">��</xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </xsl:for-each>
      </table>

      <p></p>
      

        <h4>���������� �������</h4>
        <table border="1" cellspacing="0">
          <td>
            <b>���������� ������� �</b>
            <table border="1" cellspacing="0">
              <tr bgcolor="c1ced5">
                <th>����� ��</th>
                <th>������������</th>
              </tr>
              <xsl:for-each select="InputLogicSignal/��">
                <xsl:if test="position() &lt; 9 ">
                  <tr>
                    <td>
                      <xsl:value-of  select="position()"/>
                    </td>

                    <td>
                      <xsl:for-each select="�������">
                        <xsl:if test="current() !='���'">
                          <xsl:if test="current() ='��'">
                            �<xsl:value-of select="position()"/>
                          </xsl:if>
                          <xsl:if test="current() ='������'">
                            ^�<xsl:value-of select="position()"/>
                          </xsl:if>
                        </xsl:if>
                      </xsl:for-each>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </table>

            <b>���������� ������� ���</b>
            <table border="1" cellspacing="0">

              <tr bgcolor="c1ced5">
                <th>����� ��</th>
                <th>������������</th>
              </tr>

              <xsl:for-each select="InputLogicSignal/��">
                <xsl:if test="position() &gt; 8 ">
                  <xsl:if test="position() &lt; 17">
                    <tr>
                      <td>
                        <xsl:value-of  select="position()"/>
                      </td>

                      <td>
                        <xsl:for-each select="�������">
                          <xsl:if test="current() !='���'">
                            <xsl:if test="current() ='��'">
                              �<xsl:value-of select="position()"/>
                            </xsl:if>
                            <xsl:if test="current() ='������'">
                              ^�<xsl:value-of select="position()"/>
                            </xsl:if>
                          </xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </table>
          </td>
        </table>

        <b>���</b>
        <table border="1" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�����</th>
            <th>������������</th>
          </tr>


          <xsl:for-each select="OutputLogicSignal/���">
            <xsl:if test="position() &lt; 17">
              <tr>
                <td>
                  <xsl:value-of select="position()"/>
                </td>
                <td>
                  <xsl:for-each select="�������">
                    <xsl:value-of select="current()"/>|
                  </xsl:for-each>
                </td>
              </tr>
            </xsl:if>
          </xsl:for-each>
        </table>
        <p></p>

        <h4>���</h4>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr bgcolor="c1ced5">
            <th>�����</th>
            <th>���������� �� ����</th>
            <th>������ ���</th>
            <th>t �������, ��</th>
            <th>��� �������</th>
            <th>����������</th>
            <th>t ����., ��</th>
            <th>t �����., ��</th>
            <th>1 ����, ��</th>
            <th>2 ����, ��</th>
            <th>3 ����, ��</th>
            <th>4 ����, ��</th>
            <th>����������.</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="Apv/�����"/>
            </td>
            <td><xsl:for-each select="Apv/����������_��_����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">��</xsl:if></xsl:for-each></td> 
            <td>
              <xsl:value-of select="Apv/����_�������_���"/>
            </td>
            <td>
              <xsl:value-of select="Apv/�����_�������_���"/>
            </td>
            <td>
              <xsl:value-of select="Apv/���_�������"/>
            </td>
            <td>
              <xsl:value-of select="Apv/����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="Apv/�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="Apv/�����_����������_���"/>
            </td>
            <td>
              <xsl:value-of select="Apv/����1"/>
            </td>
            <td>
              <xsl:value-of select="Apv/����2"/>
            </td>
            <td>
              <xsl:value-of select="Apv/����3"/>
            </td>
            <td>
              <xsl:value-of select="Apv/����4"/>
            </td>
            <td>
              <xsl:value-of select="Apv/������_���_��_�����������������_����������_�����������"/>
            </td>
          </tr>
        </table>
        <p></p>

         <h4>���</h4>
            <table border="1" cellpadding="4" cellspacing="0">
              <tr align="center">
                <th bgcolor="c1ced5">�� ������� (�� �������)</th>
                <td><xsl:value-of select="Avr/��_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ����������</th>
                <td><xsl:value-of select="Avr/��_����������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ��������������</th>
                <td><xsl:value-of select="Avr/��_��������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�� ������</th>
                <td><xsl:value-of select="Avr/��_������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">������ �����</th>
                <td><xsl:value-of select="Avr/����"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">����������</th>
                <td><xsl:value-of select="Avr/����_����������_���"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�����</th>
                <td><xsl:value-of select="Avr/����_�����_����������_���"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">��� ���������</th>
                <td><xsl:value-of select="Avr/����_���_������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t��, ��</th>
                <td><xsl:value-of select="Avr/�����_���_������������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�������</th>
                <td><xsl:value-of select="Avr/����_���_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t���, ��</th>
                <td><xsl:value-of select="Avr/�����_���_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">t����, ��</th>
                <td><xsl:value-of select="Avr/��������_����������_�������"/></td>
              </tr>
              <tr align="center">
                <th bgcolor="c1ced5">�����</th>
                <td><xsl:value-of select="Avr/�����"/></td>
              </tr>
            </table>
            <p></p>

      <h4>�������� ����������� � ������� �������� ��� ���������� (�� � ����)</h4>

      <table border="1" cellpadding="4" cellspacing="0">
        <tr>����� �������</tr>
        <tr bgcolor="c1ced5">
          <th>U1, �</th>
          <th>U2, �</th>
          <th>Umin. ���*, �</th>
          <th>Umin. ���*, �</th>
          <th>Umax. ���*, �</th>
          <th>t��, ��</th>
          <th>t�����, ��</th>
          <th>t ���, ��</th>
          <th>����, %</th>
          <th>f(U1;U2), ����</th>
          <th>���� ���������� ��</th>
          <th>���� ����� U1 ���, U2 ����</th>
          <th>���� ����� U1 ����, U2 ���</th>
          <th>���� ����� U1 ���, U2 ���</th>
        </tr>
        <tr align="center">
          <td>
            <xsl:value-of select="Sinhronizm/U1"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/U2"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/Umin"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/Umin_���"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/Umax_���"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/t��"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/t�����"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/t���"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/����"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/f"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/����������"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/�������_U1_���_U2_����"/>
          </td>
          <td>
            <xsl:value-of select="Sinhronizm/�������_U1_����_U2_���"/>
          </td>
          <td>
            <xsl:value-of select="DefensesSetpoints/I2I1/�������_U1_���_U2_���"/>
          </td>
        </tr>
      </table>
      <b>* - ������ ������� ������������ ��� ��������� � ������������ U1 � U2*����/100</b>
      <p></p>


      <table border="1" cellspacing="0">
        <tr>������� ������� ���������</tr>
        <td>
          <table border="1" cellpadding="4" cellspacing="0">
            <tr bgcolor="c1ced5">
              <th>�����</th>
              <th>���������� �� ������������� ��</th>
              <th>dUmax., �</th>
              <th>�������� ����������� (�����. �����)</th>
              <th>dF, ��</th>
              <th>dFi, ����</th>
              <th>����������� ����������� (�������. �����)</th>
              <th>dF, ��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/�����"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_�������_���������/����.��_������.��">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/dUmax"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_�������_���������/��������_�����������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/����������_���������"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/dFi"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_�������_���������/�����������_�����������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/������������_���������"/>
              </td>
            </tr>
          </table>
          <table border="1" cellpadding="4" cellspacing="0">
            <tr>���������� ���������</tr>
            <tr bgcolor="c1ced5">
              <th>U1 ���, U2 ����</th>
              <th>U1 ����, U2 ���</th>
              <th>U1 ���, U2 ���</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2����"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/U1����U2���"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/U1���U2���"/>
              </td>
            </tr>
          </table>
        </td>
      </table>

      <p></p>

      <table border="1" cellspacing="0">
        <tr>������� ��������������� ���������</tr>
        <td>
          <table border="1" cellpadding="4" cellspacing="0">
            <tr bgcolor="c1ced5">
              <th>�����</th>
              <th>���������� �� ������������� ��</th>
              <th>dUmax., �</th>
              <th>�������� ����������� (�����. �����)</th>
              <th>dF, ��</th>
              <th>dFi, ����</th>
              <th>����������� ����������� (�������. �����)</th>
              <th>dF, ��</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="Sinhronizm/������_���_�������_���������/�����"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_���������������_���������/����.��_������.��">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/dUmax"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_���������������_���������/��������_�����������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/����������_���������"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/dFi"/>
              </td>
              <td>
                <xsl:for-each select="Sinhronizm/������_���_���������������_���������/�����������_�����������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/������������_���������"/>
              </td>
            </tr>
          </table>
          <table border="1" cellpadding="4" cellspacing="0">
            <tr>���������� ���������</tr>
            <tr bgcolor="c1ced5">
              <th>U1 ���, U2 ����</th>
              <th>U1 ����, U2 ���</th>
              <th>U1 ���, U2 ���</th>
            </tr>
            <tr align="center">
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2����"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1����U2���"/>
              </td>
              <td>
                <xsl:value-of select="Sinhronizm/������_���_���������������_���������/U1���U2���"/>
              </td>
            </tr>
          </table>
        </td>
      </table>
    </xsl:for-each>
	</div>
    <p></p>
	</div>
	
	<h2>������� �������</h2>
	
	<table border="1" cellpadding="4" cellspacing="0">
	<tr>������ �������</tr>	
		<tr>
			<th bgcolor="FF8C00">������ 1</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_1"/></td>
		</tr>
		<tr>
			<th bgcolor="FF8C00">������ 2</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_2"/></td>
		</tr>
		<!--<tr>
			<th bgcolor="FF8C00">������ 3</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_3"/></td>
		</tr>
		<tr>
			<th bgcolor="FF8C00">������ 4</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_4"/></td>
		</tr>
		<tr>
			<th bgcolor="FF8C00">������ 5</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_5"/></td>
		</tr>
		<tr>
			<th bgcolor="FF8C00">������ 6</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_���������_������_�������_x002B_6"/></td>
		</tr>-->
	</table>
	
	<p></p>

	<table border="1" cellpadding="4" cellspacing="0">
		<tr>
			<th bgcolor="FF8C00">����� ���������</th>
			<td><xsl:value-of select="��801/������������_�������_��������/����_�����_���������"/></td>
		</tr>
	</table>
	<p></p>

  <h2>�������� �������</h2>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>�������� ����</tr>
    <tr bgcolor="90EE90">
      <th>�����</th>
      <th>���</th>
      <th>������</th>
      <th>T�����, ��</th>
    </tr>
    <xsl:if test="$config = 'T12N4D26R19'">
    <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
		  <xsl:if test="position() &lt; 19">
		    <tr align="center">
			    <td><xsl:value-of select="position()"/></td>
			    <td><xsl:value-of select="@���"/></td>
			    <td><xsl:value-of select="@������"/></td>
			    <td><xsl:value-of select="@�����"/></td>
		    </tr>
      </xsl:if>
    </xsl:for-each>
   </xsl:if>
  <xsl:if test="$config != 'T12N4D26R19'">
    <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
		  <xsl:if test="position() &lt; 51">
		    <tr align="center">
			    <td><xsl:value-of select="position()"/></td>
			    <td><xsl:value-of select="@���"/></td>
			    <td><xsl:value-of select="@������"/></td>
			    <td><xsl:value-of select="@�����"/></td>
		    </tr>
      </xsl:if>
    </xsl:for-each>
  </xsl:if>
  </table>
  <p></p>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>����������</tr>
    <tr bgcolor="90EE90">
      <th>�</th>
      <th>���</th>
      <th>������ "�������"</th>
      <th>������ "�������"</th>
      <th>����� ������</th>
    </tr>
    <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����������/���_����������/����_���������">
      <tr align="center">
        <td>
          <xsl:value-of select="position()"/>
        </td>
        <td>
          <xsl:value-of select="@���"/>
        </td>
        <td>
          <xsl:value-of select="@������_�������"/>
        </td>
        <td>
          <xsl:value-of select="@������_�������"/>
        </td>
        <td>
          <xsl:value-of select="@�����_������"/>
        </td>
      </tr>
    </xsl:for-each>
  </table>
  <p></p>

  <table border="1" cellspacing="0" cellpadding="4">
    <tr>����� �����������</tr>
    <xsl:for-each select="��801/����_��������_������">
      <tr align="left">
        <th bgcolor="90EE90">1. �� ����� � ������ �������</th>
        <xsl:element name="td">
          <xsl:attribute name="id">
            ResetBySys_<xsl:value-of select="position()"/>
          </xsl:attribute>
          <script>
            translateBoolean(<xsl:value-of select="@�����_��_�����_�_������_�������"/>,"ResetBySys_<xsl:value-of select="position()"/>");
          </script>
        </xsl:element>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">2. �� ����� � ������ ������</th>
        <xsl:element name="td">
          <xsl:attribute name="id">
            ResetByAlrm_<xsl:value-of select="position()"/>
          </xsl:attribute>
          <script>
            translateBoolean(<xsl:value-of select="@�����_��_�����_�_������_������"/>,"ResetByAlrm_<xsl:value-of select="position()"/>");
          </script>
        </xsl:element>
      </tr>
    </xsl:for-each>
  </table>
  <p></p>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>������ �������</tr>
    <th bgcolor="90EE90">����. ������ ������� F</th>
    <td>
      <xsl:for-each select="��801/����_��������_������/@������_�������">
        <xsl:if test="current() = 'true'">��</xsl:if>
        <xsl:if test="current() = 'false'">���</xsl:if>
      </xsl:for-each>
    </td>
  </table>
  <p></p>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>���� �������������</tr>
    <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����_�������������">
      <tr align="left">
        <th bgcolor="90EE90">1. ���������� �������������</th>
        <td>
          <xsl:for-each select="@�������������_1">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">2. ����������� �������������</th>
        <td>
          <xsl:for-each select="@�������������_2">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">3. ������������� ��������� U</th>
        <td>
          <xsl:for-each select="@�������������_3">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">4. ������������� ��������� F</th>
        <td>
          <xsl:for-each select="@�������������_4">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">5. ������������� �����������</th>
        <td>
          <xsl:for-each select="@�������������_5">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">6. ������������� ������</th>
        <td>
          <xsl:for-each select="@�������������_6">
            <xsl:if test="current() = 'true'">��</xsl:if>
            <xsl:if test="current() = 'false'">���</xsl:if>
          </xsl:for-each>
        </td>
      </tr>
      <tr align="left">
        <th bgcolor="90EE90">T�����., ��</th>
        <td>
          <xsl:value-of select="@�������_����_�������������"/>
        </td>
      </tr>
    </xsl:for-each>
  </table>
  <p></p>

  <h2>���������� ��������</h2>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>����������� ����</tr>
    <tr bgcolor="FF8C00">
      <th>�</th>
      <th>���</th>
      <th>������</th>
      <th>T�����., ��</th>
    </tr>
   <xsl:if test="$config = 'T12N4D26R19'">
     <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
      <xsl:if test="position() &gt; 18">
       <xsl:if test="position() &lt; 81">
        <tr align="center">
          <td><xsl:value-of select="position()"/></td>
          <td><xsl:value-of select="@���"/></td>
          <td><xsl:value-of select="@������"/></td>
          <td><xsl:value-of select="@�����"/></td>
        </tr>
       </xsl:if>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
  <xsl:if test="$config != 'T12N4D26R19'">
     <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
      <xsl:if test="position() &gt; 50">
       <xsl:if test="position() &lt; 81">
        <tr align="center">
          <td><xsl:value-of select="position()"/></td>
          <td><xsl:value-of select="@���"/></td>
          <td><xsl:value-of select="@������"/></td>
          <td><xsl:value-of select="@�����"/></td>
        </tr>
       </xsl:if>
      </xsl:if>
     </xsl:for-each>
    </xsl:if>
  </table>
  <p></p>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>��������������� RS-��������</tr>
    <tr bgcolor="FF8C00">
      <th>�</th>
      <th>���</th>
      <th>���� R</th>
      <th>���� S</th>
    </tr>
    <xsl:for-each select="��801/������������_����_x002C_�����������_x002C_��������������/RS_��������/���_rs_��������/����_�������">
      <tr align="center">
        <td>
          <xsl:value-of select="position()"/>
        </td>
        <td>
          <xsl:value-of select="@���"/>
        </td>
        <td>
          <xsl:value-of select="@����_R"/>
        </td>
        <td>
          <xsl:value-of select="@����_S"/>
        </td>
      </tr>
    </xsl:for-each>
  </table>
  <p></p>
  
	
	<h2>�����������</h2>
	<table border="1" cellpadding="4" cellspacing="0">
		<tr>
			<th bgcolor="FF69B4">���������� ������������</th>
			<td><xsl:value-of select="��801/������������_�����������/������������_���/����������_�����������"/></td>
		</tr>
		<tr>
			<th bgcolor="FF69B4">������, ��</th>
			<td><xsl:value-of select="��801/������_������������"/></td>
		</tr>
		<tr>
			<th bgcolor="FF69B4">����. ����������, %</th>
			<td><xsl:value-of select="��801/������������_�����������/������������_���/����������"/></td>
		</tr>
		<tr>
			<th bgcolor="FF69B4">������. ��</th>
			<td><xsl:value-of select="��801/������������_�����������/������������_���/��������"/></td>
		</tr>
		<tr>
			<th bgcolor="FF69B4">���� ����� ���.</th>
			<td><xsl:value-of select="��801/������������_�����������/����_�������_������������/@�����"/></td>
		</tr>
	</table>
	<p></p>

  <table border="1" cellpadding="4" cellspacing="0">
    <tr>��������������� ���������� ������</tr>
    <tr bgcolor="FF69B4">
      <th>�����</th>
      <th>����</th>
      <th>�����</th>
    </tr>
    <xsl:for-each select="��801/������������_�����������/������������_�������/���_������/ChannelWithBase">
      <tr align="center">
        <td><xsl:value-of select="position()"/></td>
        <td><xsl:value-of select="@����"/></td>
        <td><xsl:value-of select="@�����"/></td>
      </tr>
    </xsl:for-each>
  </table>
  
	<p></p>
	
	<h2>�����������</h2>
	<table border="1" cellpadding="4" cellspacing="0">
		<tr>�����������</tr>
			<tr bgcolor="FFC0CB">
				<th>��������� "���������"</th>
				<th>��������� "��������"</th>
				<th>���� �������������</th>
				<th>���� ����������</th>
        <xsl:if test="$vers &lt; 1.05">
          <th>t����, ��</th>
          <th>I����, I�</th>
        </xsl:if>
				<th>������� ������� ���., ��</th>
				<th>������� ����������</th>
				<th>t�����, ��</th>
				<th>�������� ����� ����������</th>
				<th>�������� ������� ��������� ����������</th>
			  <th>�������</th>
			</tr>
			<tr align="center">
				<td><xsl:value-of select="��801/������������_�����������/���������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/��������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����������"/></td>
        <xsl:if test="$vers &lt; 1.05">
          <td><xsl:value-of select="��801/������������_�����������/t����"/></td>
          <td><xsl:value-of select="��801/������������_�����������/���_����"/></td>
        </xsl:if>
				<td><xsl:value-of select="��801/������������_�����������/�������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/�������_����������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/���������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/��������_�����_���������_����������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/��������_�����_�������_���������_����������"/></td>
			  <td><xsl:value-of select="��801/������������_�����������/�������"/></td>
			</tr>
	</table>

  <xsl:if test="$vers &gt; 1.04">
    <p></p>
    <table border="1" cellpadding="4" cellspacing="0">
      <tr>����</tr>
      <tr bgcolor="FFC0CB">
        <th>�� ����</th>
        <th>�� ��</th>
        <th>�� ����</th>
        <th>t����1, ��</th>
        <th>t����2, ��</th>
        <th>I����, I�</th>
        <th>���� �����</th>
        <th>���� ����������</th>
      </tr>
      <tr align="center">
        <xsl:element name="td">
          <xsl:attribute name="id">����_��_����</xsl:attribute>
          <script>
            translateBoolean(<xsl:value-of select="��801/����/��_����"/>,"����_��_����");
          </script>
        </xsl:element>
        <xsl:element name="td">
          <xsl:attribute name="id">����_��_��</xsl:attribute>
          <script>
            translateBoolean(<xsl:value-of select="��801/����/��_��"/>,"����_��_��");
          </script>
        </xsl:element>
        <xsl:element name="td">
          <xsl:attribute name="id">����_��_����</xsl:attribute>
          <script>
            translateBoolean(<xsl:value-of select="��801/����/��_����"/>,"����_��_����");
          </script>
        </xsl:element>
        <td><xsl:value-of select="��801/����/t����1"/></td>
        <td><xsl:value-of select="��801/����/t����2"/></td>
        <td><xsl:value-of select="��801/����/���_����"/></td>
        <td><xsl:value-of select="��801/����/����"/></td>
        <td><xsl:value-of select="��801/����/����������"/></td>
      </tr>
    </table>
  </xsl:if>
  
	<p></p>
	
	<table border="1" cellpadding="4" cellspacing="0">
		<tr>����������</tr>
			<tr bgcolor="FFC0CB">
				<th>���� ��������</th>
				<th>���� ���������</th>
				<th>������� ��������</th>
				<th>������� ���������</th>
				<th>�� ������ ������</th>
				<th>�� �����</th>
				<th>�������</th>
				<th>�� ����</th>
			  <th>���������� ����</th>
			</tr>
			<tr align="center">
				<td><xsl:value-of select="��801/������������_�����������/����_���"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����_����"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����_����_��������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����_����_���������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����"/></td>
				<td><xsl:value-of select="��801/������������_�����������/�������"/></td>
				<td><xsl:value-of select="��801/������������_�����������/����"/></td>
			  <td><xsl:value-of select="��801/������������_�����������/����������_����"/></td>
			</tr>
	</table>
	
</body>
</html>
</xsl:template>
</xsl:stylesheet>
