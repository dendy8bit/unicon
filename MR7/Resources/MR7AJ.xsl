<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 
</head>
<body>
 <h3>���������� ��801. ������ ������</h3>
	<p></p>
    <table border="1">
      <tr bgcolor="#c1ced5">
         <th>�����</th>
         <th>�����</th>
         <th>���������</th>
		     <th>����������� ������</th>
		     <th>�������� ������������</th>
		     <th>�������� ��������� ������������</th>
		     <th>������ �������</th>

         <th>Ia ���., A</th>
         <th>Ib ���., A</th>
         <th>Ic ���., A</th>

         <th>Ia ����., A</th>
         <th>Ib ����., A</th>
         <th>Ic ����., A</th>

         <th>Ia s1, A</th>
         <th>Ib s1, A</th>
         <th>Ic s1, A</th>
         <th>3I0 s1, A</th>
         <th>I2 s1, A</th>
         <th>In s1, A</th>

         <th>Ia s2, A</th>
         <th>Ib s2, A</th>
         <th>Ic s2, A</th>
         <th>3I0 s2, A</th>
         <th>I2 s2, A</th>
         <th>In s2, A</th>

         <th>Ia s3, A</th>
         <th>Ib s3, A</th>
         <th>Ic s3, A</th>
         <th>3I0 s3, A</th>
         <th>I2 s3, A</th>
         <th>In s3, A</th>

         <th>Ia s4, A</th>
         <th>Ib s4, A</th>
         <th>Ic s4, A</th>
         <th>3I0 s4, A</th>
         <th>I2 s4, A</th>
         <th>In s4, A</th>

         <th>Ua1, B</th>
		     <th>Ub1, B</th>
		     <th>Uc1, B</th>
		     <th>Uab1, B</th>
		     <th>Ubc1, B</th>
		     <th>Uca1, B</th>
		     <th>U2-1, B</th>
		     <th>3U0-1, B</th>

         <th>Ua2, B</th>
         <th>Ub2, B</th>
         <th>Uc2, B</th>
         <th>Uab2, B</th>
         <th>Ubc2, B</th>
         <th>Uca2, B</th>
         <th>U2-2, B</th>
         <th>3U0-2, B</th>
          
		     <th>F,��</th>
		     <th>Un, B</th>
		     <th>�1-�8</th>
		     <th>�9-�16</th>
		     <th>�17-�24</th>
		     <th>�25-�32</th>
		     <th>�33-�40</th>
         <th>�41-�48</th>
         <th>�49-�56</th>

      </tr>
      
      
	  <xsl:for-each select="DocumentElement/��801_������_������">
	  <tr align="center">
     <td><xsl:value-of select="_indexCol"/></td>
     <td><xsl:value-of select="_timeCol"/></td>
     <td><xsl:value-of select="_msg1Col"/></td>
		 <td><xsl:value-of select="_msgCol"/></td>
		 <td><xsl:value-of select="_codeCol"/></td>
		 <td><xsl:value-of select="_typeCol"/></td>
		 <td><xsl:value-of select="_groupCol"/></td>

     <td><xsl:value-of select="_IdaCol"/></td>
		 <td><xsl:value-of select="_IdbCol"/></td>
		 <td><xsl:value-of select="_IdcCol"/></td>
		 <td><xsl:value-of select="_ItaCol"/></td>
		 <td><xsl:value-of select="_ItbCol"/></td>
		 <td><xsl:value-of select="_ItcCol"/></td>   

     <td><xsl:value-of select="_Is1aCol"/></td>
     <td><xsl:value-of select="_Is1bCol"/></td>
		 <td><xsl:value-of select="_Is1cCol"/></td>
		 <td><xsl:value-of select="_Is13I0Col"/></td>
		 <td><xsl:value-of select="_Is1I2Col"/></td>
		 <td><xsl:value-of select="_Is1InCol"/></td>

		 <td><xsl:value-of select="_Is2aCol"/></td>
     <td><xsl:value-of select="_Is2bCol"/></td>
     <td><xsl:value-of select="_Is2cCol"/></td>
		 <td><xsl:value-of select="_s13I0Col"/></td>
		 <td><xsl:value-of select="_s2I2Col"/></td>
		 <td><xsl:value-of select="_s2InCol"/></td>

		 <td><xsl:value-of select="_Is3aCol"/></td>
		 <td><xsl:value-of select="_Is3bCol"/></td>
		 <td><xsl:value-of select="_Is3cCol"/></td>
		 <td><xsl:value-of select="_s33I0Col"/></td>
		 <td><xsl:value-of select="_s3I2Col"/></td>
		 <td><xsl:value-of select="_s3InCol"/></td>

		 <td><xsl:value-of select="_Is4aCol"/></td>
		 <td><xsl:value-of select="_Is4bCol"/></td>
		 <td><xsl:value-of select="_Is4cCol"/></td>
		 <td><xsl:value-of select="_s43I0Col"/></td>
		 <td><xsl:value-of select="_s4I2Col"/></td>
		 <td><xsl:value-of select="_s4InCol"/></td>

		 <td><xsl:value-of select="_Ua1Col"/></td>
		 <td><xsl:value-of select="_Ub1Col"/></td>
		 <td><xsl:value-of select="_Uc1Col"/></td>
		 <td><xsl:value-of select="_Uab1Col"/></td>
		 <td><xsl:value-of select="_Ubc1Col"/></td>
		 <td><xsl:value-of select="_Uca1Col"/></td>
		 <td><xsl:value-of select="_U12Col"/></td>
		 <td><xsl:value-of select="_U130Col"/></td>

     <td><xsl:value-of select="_U2aCol"/></td>
		 <td><xsl:value-of select="_U2bCol"/></td>
		 <td><xsl:value-of select="_U2cCol"/></td>
		 <td><xsl:value-of select="_Uab2Col"/></td>
		 <td><xsl:value-of select="_Ubc2Col"/></td>
		 <td><xsl:value-of select="_Ucb2Col"/></td>
		 <td><xsl:value-of select="_U22Col"/></td>
		 <td><xsl:value-of select="_U230Col"/></td>
      
     <td><xsl:value-of select="_FCol"/></td>
		 <td><xsl:value-of select="_Un"/></td>
		 <td><xsl:value-of select="_D0Col"/></td>
		 <td><xsl:value-of select="_D1Col"/></td>
		 <td><xsl:value-of select="_D2Col"/></td>
		 <td><xsl:value-of select="_D3Col"/></td>
		 <td><xsl:value-of select="_D4Col"/></td>
		 <td><xsl:value-of select="_d5Col"/></td>
		 <td><xsl:value-of select="_D6Col"/></td>
		 </tr>
		 
    </xsl:for-each>
    </table>	
</body>
  </html>
</xsl:template>
</xsl:stylesheet>
