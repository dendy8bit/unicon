﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.MR7.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR7.Osc.Loaders
{
    /// <summary>
    /// Загружает уставки токов(Iтт) и напряжений (Ктн)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 2;

        private  MemoryEntity<MeasureTransStruct>[] _connections;
        private int _numberOfGroup;
        #endregion [Private fields]

        public MeasureTransStruct this[int index]
        {
            get { return this._connections[index].Value; }
        }

        #region [Events]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;
        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private Mr7 _device;

        #endregion [Events]


        #region [Ctor's]

        public CurrentOptionsLoader(Mr7 device)
        {
            this._device = device;
            this._connections = new MemoryEntity<MeasureTransStruct>[COUNT_GROUPS];
            this._numberOfGroup = 0;
            for (int i = 0; i < this._connections.Length; i++)
            {
                this._connections[i] = new MemoryEntity<MeasureTransStruct>(string.Format("Параметры измерительного трансформатора гр{0}", i + 1),
                    device, this._device.GetStartAddrMeasTrans(i));
                this._connections[i].AllReadOk += this._connections_AllReadOk;
                this._connections[i].AllReadFail += this._connections_AllReadFail;
            }
        }

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            this.LoadFail?.Invoke();
        }

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            this._numberOfGroup++;
            if (this._numberOfGroup < COUNT_GROUPS)
            {
                this._connections[this._numberOfGroup].LoadStruct();
            }
            else
            {
                this.LoadOk?.Invoke();
            }

        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this._connections[0].LoadStruct();
        }
        
        #endregion [Methods]
    }
}
