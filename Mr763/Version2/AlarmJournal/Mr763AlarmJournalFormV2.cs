﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms.Export;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr763.Properties;
using BEMN.Mr763.Version2.AlarmJournal.Structures;
using BEMN.Mr763.Version2.Configuration.Structures.MeasuringTransformer;

namespace BEMN.Mr763.Version2.AlarmJournal
{
    public partial class Mr763AlarmJournalFormV2 : Form, IFormView
    {
        #region [Constants]
        private const string RECORDS_IN_JOURNAL = "Аварий в журнале - {0}";
        private const string READ_AJ_FAIL = "Невозможно прочитать журнал аварий";
        private const string READ_AJ = "Чтение журнала аварий";
        private const string ALARM_JOURNAL = "Журнал аварий";
        private const string TABLE_NAME = "МР763_журнал_аварий";
        private const string JOURNAL_IS_EMPTY = "Журнал пуст";
        private const string JOURNAL_SAVED = "Журнал сохранён";
        #endregion [Constants]


        #region [Private fields]
        private readonly MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private readonly MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        private DataTable _table;
        private int _recordNumber;
        private Mr763Device _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr763AlarmJournalFormV2()
        {
            InitializeComponent();
        }

        public Mr763AlarmJournalFormV2(Mr763Device device)
        {
            InitializeComponent();
            this._device = device;
            
            this._alarmJournal = device.Mr763DeviceV2.AlarmJournal;
            this._refreshAlarmJournal = device.Mr763DeviceV2.RefreshAlarmJournal;
            this._currentOptionsLoader = device.Mr763DeviceV2.CurrentOptionsLoaderAj;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._refreshAlarmJournal.SaveStruct);
            this._currentOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, FailReadJournal);

            this._refreshAlarmJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, StartReadJournal);
            this._refreshAlarmJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, FailReadJournal);
            this._alarmJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, ReadRecord);
            this._alarmJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, FailReadJournal);
        } 
        #endregion [Ctor's]


        #region [Help members]
        private void FailReadJournal()
        {
            this._statusLabel.Text = READ_AJ_FAIL;
        }

        private void StartReadJournal()
        {
            this._recordNumber = 0;
            this._table.Clear();
            this._alarmJournal.LoadStructCycleWhile(a => a.IsEmpty);
        }

        private void ReadRecord()
        {
            if (!this._alarmJournal.Value.IsEmpty)
            {
                this._recordNumber++;
                MeasureTransStruct measure = this._currentOptionsLoader.MeasureStruct;
                AlarmJournalRecordStruct record = this._alarmJournal.Value;
                string parametrValue = this.GetParametr(record.NumberOfTriggeredParametr, record.ValueOfTriggeredParametr, measure);
                this._table.Rows.Add
                    (
                        this._recordNumber,
                        record.GetTime,
                        AjStringsV2.Message[record.Message],
                        AjStringsV2.Stage[record.Stage],
                        AjStringsV2.Parametr[record.NumberOfTriggeredParametr],
                        parametrValue,
                        AjStringsV2.AlarmJournalSetpointsGroup[record.GroupOfSetpoints],
                        ValuesConverterCommon.Analog.GetI(record.Ia, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ib, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.Ic, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I1, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I2, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetI(record.I0, measure.I1.Ittl * 40),
                        ValuesConverterCommon.Analog.GetU(record.Ua, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ub, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uc, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uab, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Ubc, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Uca, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U1, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U2, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.U0, measure.U1.KthlValue),
                        ValuesConverterCommon.Analog.GetU(record.Un, measure.U1.KthxValue),
                        ValuesConverterCommon.Analog.GetU(record.Un1, measure.U1.Kthx1Value),
                        ValuesConverterCommon.Analog.GetF(record.F),
                        ValuesConverterCommon.Analog.GetQt(record.Q),
                        record.D1To8,
                        record.D9To16,
                        record.D17To24,
                        record.D25To32,
                        record.D33To40
                    );
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._recordNumber);
                statusStrip1.Update();
            }
            else
            {
                if (this._table.Rows.Count == 0)
                {
                    this._statusLabel.Text = "Журнал пуст";
                }
                
            }
        }

        private string GetParametr(int parametr, ushort value, MeasureTransStruct measure)
        {
            List<int> i = new List<int> {0, 1, 2, 3, 4, 6, 9, 10, 11};
            if (i.Contains(parametr))
            {
                return ValuesConverterCommon.Analog.GetI(value, measure.I1.Ittl*40);
            }
            
            if ((parametr >= 12) & (parametr <= 20))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthlValue);
            }

            if ((parametr >= 21) & (parametr <= 22))
            {
                return ValuesConverterCommon.Analog.GetU(value, measure.U1.KthxValue);
            }
            if (parametr == 22)
            {
                ValuesConverterCommon.Analog.GetF(value);
            }
            if (parametr >= 24 && parametr <= 26)
            {
                return string.Empty;
            }
            return value.ToString();
        }

        private DataTable GetJournalDataTable()
         {
             DataTable table = new DataTable(TABLE_NAME);
             for (int j = 0; j < this._alarmJournalGrid.Columns.Count; j++)
             {
                 table.Columns.Add(this._alarmJournalGrid.Columns[j].HeaderText);
             }
             return table;
         }
        #endregion [Help members]


        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr763Device); }
        }

        public bool Multishow { get; private set; }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public Type ClassType
        {
            get { return typeof(Mr763AlarmJournalFormV2); }
        }

        public bool Deletable
        {
            get { return false; }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.ja; }
        }

        public string NodeName
        {
            get { return ALARM_JOURNAL; }
        }
        #endregion [IFormView Members]


        #region [Event Handlers]
        private void AlarmJournalForm_Load(object sender, EventArgs e)
        {
            this._table = this.GetJournalDataTable();
            this._alarmJournalGrid.DataSource = this._table;
            this.StartRead();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._table.Rows.Clear();
            this._statusLabel.Text = READ_AJ;
            this._currentOptionsLoader.StartRead();
        }
        
        private void _loadAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._openAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.Clear();
                this._table.ReadXml(this._openAlarmJournalDialog.FileName);
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, this._table.Rows.Count);
            }
        }

        private void _saveAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (this._table.Columns.Count == 0)
            {
                MessageBox.Show(JOURNAL_IS_EMPTY);
                return;
            }
            if (this._saveAlarmJournalDialog.ShowDialog() == DialogResult.OK)
            {
                this._table.WriteXml(this._saveAlarmJournalDialog.FileName);
            }
        }

        #endregion [Event Handlers]

        private void _exportButton_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == this._saveJournalHtmlDialog.ShowDialog())
            {
                HtmlExport.Export(this._table, this._saveJournalHtmlDialog.FileName, Resources.MR731AJ);
                this._statusLabel.Text = JOURNAL_SAVED;
            }
        }
    }
}
