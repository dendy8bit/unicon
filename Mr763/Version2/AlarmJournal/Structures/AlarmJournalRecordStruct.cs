﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr763.Version2.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct :StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;
        [Layout(12)] private ushort _ia;
        [Layout(13)] private ushort _ib;
        [Layout(14)] private ushort _ic;
        [Layout(15)] private ushort _i0;
        [Layout(16)] private ushort _i2;
        [Layout(17)] private ushort _ig;
        [Layout(18)] private ushort _i1;
        [Layout(19)] private ushort _in;
        [Layout(20)] private ushort _in1;
        [Layout(21)] private ushort _i2A;
        [Layout(22)] private ushort _i2B;
        [Layout(23)] private ushort _i2C;
        [Layout(24)] private ushort _ua;
        [Layout(25)] private ushort _ub;
        [Layout(26)] private ushort _uc;
        [Layout(27)] private ushort _uab;
        [Layout(28)] private ushort _ubc;
        [Layout(29)] private ushort _uca;
        [Layout(30)] private ushort _u0;
        [Layout(31)] private ushort _u2;
        [Layout(32)] private ushort _u1;
        [Layout(33)] private ushort _un;
        [Layout(34)] private ushort _un1;
        [Layout(35)] private ushort _f;
        [Layout(36)] private ushort _d1;
        [Layout(37)] private ushort _d2;
        [Layout(38)] private ushort _d3;
        [Layout(39)] private ushort _omp;
        [Layout(40)] private ushort _spl;
        [Layout(41)] private ushort _q;

        #endregion [Private fields]


        #region [Properties]

        public int Stage
        {
            get { return Common.GetBits(this.StageValue, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1),true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return   Common.ByteToMask(Common.LOBYTE(this.D2),true); }
        }

        public string D25To32
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D2), true); }
        }

        public string D33To40
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D3), true); }
        }

        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                          this.Month +
                          this.Date +
                          this.Hour +
                          this.Minute +
                          this.Second +
                          this.Millisecond;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );
            }
        }

        public ushort Message
        {
            get { return this._message; }
            set { this._message = value; }
        }

        public ushort NumberOfTriggeredParametr
        {
            get { return this._numberOfTriggeredParametr; }
            set { this._numberOfTriggeredParametr = value; }
        }

        public ushort ValueOfTriggeredParametr
        {
            get { return this._valueOfTriggeredParametr; }
            set { this._valueOfTriggeredParametr = value; }
        }

        public ushort GroupOfSetpoints
        {
            get { return this._groupOfSetpoints; }
            set { this._groupOfSetpoints = value; }
        }

        public ushort Ia
        {
            get { return this._ia; }
            set { this._ia = value; }
        }

        public ushort Ib
        {
            get { return this._ib; }
            set { this._ib = value; }
        }

        public ushort Ic
        {
            get { return this._ic; }
            set { this._ic = value; }
        }

        public ushort I0
        {
            get { return this._i0; }
            set { this._i0 = value; }
        }

        public ushort I2
        {
            get { return this._i2; }
            set { this._i2 = value; }
        }

        public ushort Ig
        {
            get { return this._ig; }
            set { this._ig = value; }
        }

        public ushort I1
        {
            get { return this._i1; }
            set { this._i1 = value; }
        }

        public ushort In
        {
            get { return this._in; }
            set { this._in = value; }
        }

        public ushort In1
        {
            get { return this._in1; }
            set { this._in1 = value; }
        }

        public ushort I2A
        {
            get { return this._i2A; }
            set { this._i2A = value; }
        }

        public ushort I2B
        {
            get { return this._i2B; }
            set { this._i2B = value; }
        }

        public ushort I2C
        {
            get { return this._i2C; }
            set { this._i2C = value; }
        }

        public ushort Ua
        {
            get { return this._ua; }
            set { this._ua = value; }
        }

        public ushort Ub
        {
            get { return this._ub; }
            set { this._ub = value; }
        }

        public ushort Uc
        {
            get { return this._uc; }
            set { this._uc = value; }
        }

        public ushort Uab
        {
            get { return this._uab; }
            set { this._uab = value; }
        }

        public ushort Ubc
        {
            get { return this._ubc; }
            set { this._ubc = value; }
        }

        public ushort Uca
        {
            get { return this._uca; }
            set { this._uca = value; }
        }

        public ushort U0
        {
            get { return this._u0; }
            set { this._u0 = value; }
        }

        public ushort U2
        {
            get { return this._u2; }
            set { this._u2 = value; }
        }

        public ushort U1
        {
            get { return this._u1; }
            set { this._u1 = value; }
        }

        public ushort Un
        {
            get { return this._un; }
            set { this._un = value; }
        }

        public ushort Un1
        {
            get { return this._un1; }
            set { this._un1 = value; }
        }

        public ushort F
        {
            get { return this._f; }
            set { this._f = value; }
        }

        public ushort Omp
        {
            get { return this._omp; }
            set { this._omp = value; }
        }

        public ushort Spl
        {
            get { return this._spl; }
            set { this._spl = value; }
        }

        public ushort Q
        {
            get { return this._q; }
            set { this._q = value; }
        }

        public ushort Year
        {
            get { return this._year; }
            set { this._year = value; }
        }

        public ushort Month
        {
            get { return this._month; }
            set { this._month = value; }
        }

        public ushort Date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        public ushort Hour
        {
            get { return this._hour; }
            set { this._hour = value; }
        }

        public ushort Minute
        {
            get { return this._minute; }
            set { this._minute = value; }
        }

        public ushort Second
        {
            get { return this._second; }
            set { this._second = value; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
            set { this._millisecond = value; }
        }

        public ushort StageValue
        {
            get { return this._stage; }
            set { this._stage = value; }
        }

        public ushort D1
        {
            get { return this._d1; }
            set { this._d1 = value; }
        }

        public ushort D2
        {
            get { return this._d2; }
            set { this._d2 = value; }
        }

        public ushort D3
        {
            get { return this._d3; }
            set { this._d3 = value; }
        }

        #endregion [Properties]





    }
}
