﻿namespace BEMN.Mr763.Version2.Osc.ShowOsc
{
    partial class MR763OscilloscopeSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._yTickerTextBox = new System.Windows.Forms.MaskedTextBox();
            this._xTickerTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(198, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._yTickerTextBox);
            this.groupBox1.Controls.Add(this._xTickerTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(198, 74);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сетка";
            // 
            // _yTickerTextBox
            // 
            this._yTickerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._yTickerTextBox.Location = new System.Drawing.Point(140, 44);
            this._yTickerTextBox.Name = "_yTickerTextBox";
            this._yTickerTextBox.Size = new System.Drawing.Size(51, 20);
            this._yTickerTextBox.TabIndex = 5;
            this._yTickerTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _xTickerTextBox
            // 
            this._xTickerTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._xTickerTextBox.Location = new System.Drawing.Point(140, 20);
            this._xTickerTextBox.Name = "_xTickerTextBox";
            this._xTickerTextBox.Size = new System.Drawing.Size(51, 20);
            this._xTickerTextBox.TabIndex = 4;
            this._xTickerTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._xTickerTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._xTickerTextBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Плотность сетки по Y :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Плотность сетки по X :";
            // 
            // Mr901OscilloscopeSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(216, 121);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MR763OscilloscopeSettingsForm";
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.Mr901OscilloscopeSettingsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox _yTickerTextBox;
        private System.Windows.Forms.MaskedTextBox _xTickerTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}