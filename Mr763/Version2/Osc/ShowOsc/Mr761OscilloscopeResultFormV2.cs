using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.MR763.Version2.Configuration;
using BEMN.MR763.Version2.Configuration.Structures;
using BEMN.MR763.Version2.Osc.HelpClasses;
using BEMN.MR763.Version2.Osc.ShowOsc.UI;
using BEMN_XY_Chart;

namespace BEMN.MR763.Version2.Osc.ShowOsc
{
    public partial class MR763OscilloscopeResultFormV2 : Form
    {
        #region [Constants]
        private const string GROUP_CHANNELS = "Channels";
        private const string TIME_PATTERN = "{0} ��";
        private const string CHANNELS_LABEL_PATTERN = "K{0} = {1}";
        private const string GROUP_DISCRETS = "Discrets";
        private const string CURRENTS_NAME_PATTERN = "Currents {0}";
        private const string VOLTAGES_NAME_PATTERN = "Voltages {0}";
        private const string DISCRET_LABEL_PATTERN = "�{0} = {1}";
        #endregion [Constants]



        #region [Private fields]
        /// <summary>
        /// ������
        /// </summary>
        private readonly CountingListV2 _listV2;
        /// <summary>
        /// ��������� �����
        /// </summary>
        private readonly List<ToolTip> _toolTips = new List<ToolTip>();
        /// <summary>
        /// ������ Label ��� ������� ������� �������
        /// </summary>
        private Label[] _marker1Channels;
        /// <summary>
        /// ������ Label ��� ������� ������� �������
        /// </summary>
        private Label[] _marker2Channels;
        /// <summary>
        /// ������ Label ��� ������� ������� �������
        /// </summary>
        private Label[] _marker1Discrets;
        /// <summary>
        /// ������ Label ��� ������� ������� �������
        /// </summary>
        private Label[] _marker2Discrets;
        /// <summary>
        /// ������ Label ��� ����� ������� �������
        /// </summary>
        private Label[] _marker1Currents;
        /// <summary>
        /// ������ Label ��� ����� ������� �������
        /// </summary>
        private Label[] _marker2Currents;
        /// <summary>
        /// ������ Label ��� ���������� ������� �������
        /// </summary>
        private Label[] _marker1Voltages;
        /// <summary>
        /// ������ Label ��� ���������� ������� �������
        /// </summary>
        private Label[] _marker2Voltages;
        /// <summary>
        /// ������ Label ��� �������� �������
        /// </summary>
        private Button[] _channelsSignalsButtons;
        /// <summary>
        /// ����� ��������
        /// </summary>
        private MR763OscilloscopeSettingsForm _settingsForm;
        /// <summary>
        /// ������� ������������
        /// </summary>
        private OscOptionsStruct _oscSettings;
        /// <summary>
        /// ����� �������
        /// </summary>
        readonly List<DiscretLine> _discretLines = new List<DiscretLine>();
        /// <summary>
        /// ����� �������
        /// </summary>
        readonly List<DiscretLine> _channelsLines = new List<DiscretLine>();
        /// <summary>
        /// �������
        /// </summary>
        private TrackBar[] _markers;
        /// <summary>
        /// ��� �������
        /// </summary>
        private DAS_Net_XYChart[] _charts;
        /// <summary>
        /// ��������������� �� X
        /// </summary>
// ReSharper disable NotAccessedField.Local
        private readonly ChartsXZoomer _zoomer;
// ReSharper restore NotAccessedField.Local
        /// <summary>
        /// ��������������� �� Y ��� �����
        /// </summary>
// ReSharper disable NotAccessedField.Local
        private ChartYZoomer _uYZoomer;
        private ChartYZoomer _iYZoomer;
// ReSharper restore NotAccessedField.Local
        #endregion [Private fields]


        #region [Ctor's]
        public MR763OscilloscopeResultFormV2()
        {
            InitializeComponent();
        }

        public MR763OscilloscopeResultFormV2(CountingListV2 listV2, OscOptionsStruct oscSettings)
        {
            InitializeComponent();
            this.Text = string.Format("�� 763 {0} ������� - {1}", listV2.DateAndTime,listV2.Stage);
            MAINTABLE.ColumnStyles[1].Width = 0;
            this._listV2 = listV2;
            this._oscSettings = oscSettings;
            this.Init();
            this.PrepareCharts();
            this.DrawAll();
            this._zoomer = new ChartsXZoomer(this._charts, hScrollBar4, this._markers, this._xIncreaseButton, this._xDecreaseButton);
            this._uYZoomer = new ChartYZoomer(this._uChart, this._voltageChartIncreaseButton, this._voltageChartDecreaseButton, this._uScroll);
            this._iYZoomer = new ChartYZoomer(this._iChart, this._currentChartIncreaseButton, this._currentChartDecreaseButton, this._iScroll);
        } 
        /// <summary>
        /// ������������� ��������
        /// </summary>
        private void Init()
        {
            this._charts = new[]
                {
                    this._iChart,
                    this._uChart,
                    this._discrestsChart,
                    this._channelsChart,
                    this._measuringChart
                };

            this._markers = new[]
                {
                    this._marker1TrackBar,
                    this._marker2TrackBar
                };

            this._marker1Currents = new[]
                {
                    this._marker1I1,
                    this._marker1I2,
                    this._marker1I3,
                    this._marker1I4
                };
            this._marker2Currents = new[]
                {
                    this._marker2I1,
                    this._marker2I2,
                    this._marker2I3,
                    this._marker2I4
                };

            this._marker1Voltages = new[]
                {
                    this._marker1U1,
                    this._marker1U2,
                    this._marker1U3,
                    this._marker1U4
                };

            this._marker2Voltages = new[]
                {
                    this._marker2U1,
                    this._marker2U2,
                    this._marker2U3,
                    this._marker2U4
                };

            this._marker1Channels = new[]
                {
                    this._marker1K1,
                    this._marker1K2,
                    this._marker1K3,
                    this._marker1K4,
                    this._marker1K5,
                    this._marker1K6,
                    this._marker1K7,
                    this._marker1K8,
                    this._marker1K9,
                    this._marker1K10,
                    this._marker1K11,
                    this._marker1K12,
                    this._marker1K13,
                    this._marker1K14,
                    this._marker1K15,
                    this._marker1K16,
                    this._marker1K17,
                    this._marker1K18,
                    this._marker1K19,
                    this._marker1K20,
                    this._marker1K21,
                    this._marker1K22,
                    this._marker1K23,
                    this._marker1K24
                };
            this._marker2Channels = new[]
                {
                    this._marker2K1,
                    this._marker2K2,
                    this._marker2K3,
                    this._marker2K4,
                    this._marker2K5,
                    this._marker2K6,
                    this._marker2K7,
                    this._marker2K8,
                    this._marker2K9,
                    this._marker2K10,
                    this._marker2K11,
                    this._marker2K12,
                    this._marker2K13,
                    this._marker2K14,
                    this._marker2K15,
                    this._marker2K16,
                    this._marker2K17,
                    this._marker2K18,
                    this._marker2K19,
                    this._marker2K20,
                    this._marker2K21,
                    this._marker2K22,
                    this._marker2K23,
                    this._marker2K24
                };

            this._marker1Discrets = new[]
                {
                    this._marker1D1,
                    this._marker1D2,
                    this._marker1D3,
                    this._marker1D4,
                    this._marker1D5,
                    this._marker1D6,
                    this._marker1D7,
                    this._marker1D8,
                    this._marker1D9,
                    this._marker1D10,
                    this._marker1D11,
                    this._marker1D12,
                    this._marker1D13,
                    this._marker1D14,
                    this._marker1D15,
                    this._marker1D16,
                    this._marker1D17,
                    this._marker1D18,
                    this._marker1D19,
                    this._marker1D20,
                    this._marker1D21,
                    this._marker1D22,
                    this._marker1D23,
                    this._marker1D24,
                    this._marker1D25,
                    this._marker1D26,
                    this._marker1D27,
                    this._marker1D28,
                    this._marker1D29,
                    this._marker1D30,
                    this._marker1D31,
                    this._marker1D32,
                    this._marker1D33,
                    this._marker1D34,
                    this._marker1D35,
                    this._marker1D36,
                    this._marker1D37,
                    this._marker1D38,
                    this._marker1D39,
                    this._marker1D40
                };

            this._marker2Discrets = new[]
                {
                    this._marker2D1,
                    this._marker2D2,
                    this._marker2D3,
                    this._marker2D4,
                    this._marker2D5,
                    this._marker2D6,
                    this._marker2D7,
                    this._marker2D8,
                    this._marker2D9,
                    this._marker2D10,
                    this._marker2D11,
                    this._marker2D12,
                    this._marker2D13,
                    this._marker2D14,
                    this._marker2D15,
                    this._marker2D16,
                    this._marker2D17,
                    this._marker2D18,
                    this._marker2D19,
                    this._marker2D20,
                    this._marker2D21,
                    this._marker2D22,
                    this._marker2D23,
                    this._marker2D24,
                    this._marker2D25,
                    this._marker2D26,
                    this._marker2D27,
                    this._marker2D28,
                    this._marker2D29,
                    this._marker2D30,
                    this._marker2D31,
                    this._marker2D32,
                    this._marker2D33,
                    this._marker2D34,
                    this._marker2D35,
                    this._marker2D36,
                    this._marker2D37,
                    this._marker2D38,
                    this._marker2D39,
                    this._marker2D40
                };
            this._channelsSignalsButtons = new[]
                {
                    this._channel1Button,
                    this._channel2Button,
                    this._channel3Button,
                    this._channel4Button,
                    this._channel5Button,
                    this._channel6Button,
                    this._channel7Button,
                    this._channel8Button,
                    this._channel9Button,
                    this._channel10Button,
                    this._channel11Button,
                    this._channel12Button,
                    this._channel13Button,
                    this._channel14Button,
                    this._channel15Button,
                    this._channel16Button,
                    this._channel17Button,
                    this._channel18Button,
                    this._channel19Button,
                    this._channel20Button,
                    this._channel21Button,
                    this._channel22Button,
                    this._channel23Button,
                    this._channel24Button,
                };
        }
        /// <summary>
        /// ���������� ��������
        /// </summary>
        private void PrepareCharts()
        {
            var xMin = 0;
            var xMax = this._listV2.Count - 1;

            if (this._listV2.MinU < this._listV2.MaxU)
            {
                var height = Math.Max(Math.Abs(this._listV2.MinU), Math.Abs(this._listV2.MaxU)) * 1.1;
                this._uChart.CoordinateYMin = (-1)*height;
                this._uChart.CoordinateYMax = height;
            }
            else
            {
                this._uChart.CoordinateYMin = -1;
                this._uChart.CoordinateYMax = 1;
            }

            if (this._listV2.MinI < this._listV2.MaxI)
            {
                var height = Math.Max(Math.Abs(this._listV2.MinI), Math.Abs(this._listV2.MaxI)) * 1.1;
                this._iChart.CoordinateYMin = (-1) * height;
                this._iChart.CoordinateYMax = height;
            }
            else
            {
                this._iChart.CoordinateYMin = -1;
                this._iChart.CoordinateYMax = 1;
            }


            foreach (var chart in this._charts)
            {
                chart.XMin = xMin;
                chart.XMax = xMax;
            }

            this._marker1TrackBar.Maximum = xMax;
            this._marker2TrackBar.Maximum = xMax;


            this._channelsChart.CoordinateYMin = (-1) * DiscretLine.DISCRET_LINE_INTERVAL * (CountingListV2.CHANNELS_COUNT);
            this._channelsChart.Height = DiscretLine.DISCRET_LINE_INTERVAL * (CountingListV2.CHANNELS_COUNT + 1);
            this._channelsChart.CoordinateYMax = 0;

            this._discrestsChart.CoordinateYMin = (-1) * DiscretLine.DISCRET_LINE_INTERVAL * (CountingListV2.DISCRETS_COUNT);
            this._discrestsChart.Height = DiscretLine.DISCRET_LINE_INTERVAL * (CountingListV2.DISCRETS_COUNT + 1);
            this._discrestsChart.CoordinateYMax = 0;


            //������������� ����� ��������
            this._settingsForm = new MR763OscilloscopeSettingsForm(this._charts ,this._uChart);

        }
        /// <summary>
        /// ����� ���� ��������
        /// </summary>
        private void DrawAll()
        {
            var maxWidth = 0;
            //������ �������
            for (int i = 0; i < CountingListV2.DISCRETS_COUNT; i++)
            {
                var newLine = new DiscretLine(this._discrestsChart, GROUP_DISCRETS, i, this._listV2.Discrets[i]);
                this._discretLines.Add(newLine);
            }
            //������ �������
            for (int i = 0; i < CountingListV2.CHANNELS_COUNT; i++)
            {
                var newLine = new DiscretLine(this._channelsChart, GROUP_CHANNELS, i, this._listV2.Channels[i]);
                this._channelsLines.Add(newLine);
                //������� �������

                var channelName = string.Empty;
                try
                {
                    channelName = StringsConfig.RelaySignals[this._listV2.ChannelsNames[i]];
                }
                catch (Exception)
                {
                    channelName = "������";
                }

                this._channelsSignalsButtons[i].Text = string.Format("K{0} ({1})", i + 1,channelName );
                this._channelsSignalsButtons[i].Invalidate();

                maxWidth = Math.Max(this._channelsSignalsButtons[i].Width, maxWidth);

            }
            //������������

            tableLayoutPanel1.Width = maxWidth;
            tableLayoutPanel5.Width = maxWidth;
            tableLayoutPanel8.Width = maxWidth;
            tableLayoutPanel9.Width = maxWidth;
            panel4.Width = maxWidth - 15;

            //������ �����
            for (int i = 0; i < CountingListV2.CURRENTS_COUNT; i++)
            {
                var curveName = string.Format(CURRENTS_NAME_PATTERN, i);
                ChartHelper.AddLineCurves(this._iChart, curveName, this.Colors[i], 1,
                                          this._listV2.Currents[i]);
            }

            //������ ����������
            for (int i = 0; i < CountingListV2.VOLTAGES_COUNT; i++)
            {
                var curveName = string.Format(VOLTAGES_NAME_PATTERN, i);
                ChartHelper.AddLineCurves(this._uChart, curveName, this.Colors[i], 1,
                                          this._listV2.Voltages[i]);
            }
        }
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// ����� ������ �����
        /// </summary>
        private List<Color> Colors
        {
            get
            {
                return new List<Color>
                    {
                        Color.Yellow,
                        Color.Green,
                        Color.Red,
                        Color.Indigo,
                        Color.DarkOliveGreen,
                        Color.CadetBlue,
                        Color.LimeGreen,
                        Color.Gold,
                        Color.SaddleBrown,
                        Color.Tomato,
                        Color.Brown,
                        Color.SteelBlue,
                        Color.Turquoise,
                        Color.OrangeRed,
                        Color.MidnightBlue,
                        Color.Magenta,
                    };
            }
        } 
        #endregion [Properties]


        #region [Event Handlers]
        /// <summary>
        /// ���� ������������
        /// </summary>
        private void _oscRun�heckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (this._oscRun�heckBox.Checked)
            {
                foreach (var chart in this._charts)
                {
                    ChartHelper.AddMarker(chart, 2, this._listV2.Alarm);
                }
            }
            else
            {
                foreach (var chart in this._charts)
                {
                    ChartHelper.RemoveMarker(chart, 2);
                }
            }


        }
        /// <summary>
        /// combo box "����"
        /// </summary>
        private void _current�onnections�heckBox_CheckedChanged(object sender, EventArgs e)
        {
          var check = this._current�heckBox.Checked;
          this.splitter3.Visible = check;
          this._currentsLayoutPanel.Visible = check;
        }

        private void _voltage�heckBox_CheckedChanged(object sender, EventArgs e)
        {
            var check = this._voltage�heckBox.Checked;
            this.splitter1.Visible = check;
            this._voltageLayoutPanel.Visible = check;
        }

        /// <summary>
        /// combo box "��������"
        /// </summary>
        private void _discrests�heckBox_CheckedChanged(object sender, EventArgs e)
        {
            var check = this._discrests�heckBox.Checked;
            this.splitter2.Visible = check;
            this._discretsLayoutPanel.Visible = check;
        }
        /// <summary>
        /// combo box "������"
        /// </summary>
        private void _channels�heckBox_CheckedChanged(object sender, EventArgs e)
        {
            var check = this._channels�heckBox.Checked;
            this._channelLayoutPanel.Visible = check;
        }

        /// <summary>
        /// combo box "�������"
        /// </summary>
        private void _markerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            MarkersTable.Visible = this._markerCheckBox.Checked;
            MarkersTable.Update();

            if (this._markerCheckBox.Checked)
            {
                MAINTABLE.ColumnStyles[1].Width = 310;
                this.SetMarker(true, 0, this._marker1TrackBar.Value);
                this.SetMarker(true, 1, this._marker2TrackBar.Value);
            }
            else
            {
                MAINTABLE.ColumnStyles[1].Width = 0;
                this.SetMarker(false, 0, 0);
                this.SetMarker(false, 1, 0);
            }
            MAINTABLE.Update();
            this._markerScrollPanel.Update();
        }
        /// <summary>
        /// ���������� ������� �� ������. �������������/������� �����
        /// </summary>
          private void Chart_MouseClick(object sender, MouseEventArgs e)
          {
              if (!this._markCheckBox.Checked)
                  return;
              
                  try
                  {
                      var chart = sender as DAS_Net_XYChart;
                      if (chart == null)
                          return;

                      if (e.Button == MouseButtons.Left)
                      {
                          this._newMarkToolTip = new ToolTip
                              {
                                  ShowAlways = true
                              };
                          double length = Math.Abs(chart.XMax) - Math.Abs(chart.XMin);
                          double x = (length/chart.Width*e.X) + chart.XMin;

                          if (e.X < Math.Abs(chart.XMin) && chart.XMin < 0)
                          {
                              x = (Math.Round(length/chart.Width*(e.X + 1), 2) + chart.XMin);
                          }
                       //   var markText = string.Format(MARK_TEXT_PATTERN, Math.Round(x), Math.Round(length/chart.Width));
                          string markText = Math.Round(x).ToString(CultureInfo.InvariantCulture);
                          this._newMarkToolTip.Show(markText,chart, e.X, e.Y);
                          this._toolTips.Add(this._newMarkToolTip);
                      }
                      else
                      {
                          foreach (var toolTip in this._toolTips)
                          {
                              toolTip.RemoveAll();
                          }

                      }
                  }
                  catch
                  {
                      
                  }
              
          }

          private void CurrentButton_Click(object sender, EventArgs e)
          {
              var button = sender as Button;

              if (button == null) return;


              var numberRegex = new Regex(@"_i(?<number>\d+)Button");
              var numberMatch = numberRegex.Match(button.Name);
              string numberInString = numberMatch.Groups["number"].Value;
              var buttonNumber = int.Parse(numberInString);

              var newState = this.ChangeButtonColor(button, buttonNumber);
              var curveNumber = buttonNumber - 1;
              var name = string.Format(CURRENTS_NAME_PATTERN, curveNumber);

              if (newState)
              {
                  ChartHelper.AddLineCurves(this._iChart, name, this.Colors[curveNumber], 1, this._listV2.Currents[curveNumber]);
              }
              else
              {
                  ChartHelper.RemoveLineCurves(this._iChart, name);
              }
          }

        /// <summary>
        /// ���������� ������� ������ ������ "����������"
        /// </summary>
        private void VoltageButton_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button == null) return;

          
                var numberRegex = new Regex(@"_u(?<number>\d+)Button");
            var numberMatch = numberRegex.Match(button.Name);
            string numberInString = numberMatch.Groups["number"].Value;
            var buttonNumber = int.Parse(numberInString);

            var newState = this.ChangeButtonColor(button, buttonNumber);
            var curveNumber = buttonNumber - 1;
            var name = string.Format(VOLTAGES_NAME_PATTERN, curveNumber);

            if (newState)
            {
                ChartHelper.AddLineCurves(this._uChart, name, this.Colors[curveNumber], 1, this._listV2.Voltages[curveNumber]);
            }
            else
            {
                ChartHelper.RemoveLineCurves(this._uChart, name);
            }


        }

        /// <summary>
        /// ���������� ������� ������ ������ "��������"
        /// </summary>
        private void DiscretesButton_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button == null) return;
            var buttonNumber = int.Parse(button.Text.Remove(0,1));
            var newState = this.ChangeDiscretButtonColor(button);
            var lineNumber = buttonNumber - 1;
            if (newState)
            {
                this._discretLines[lineNumber].Add();
            }
            else
            {
                this._discretLines[lineNumber].Remove();
            }

        }

        /// <summary>
        /// ���������� ������� ������ ������ "������"
        /// </summary>
        private void ChannelsButton_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button == null) return;

            
         //   var numberRegex = new Regex(@"K(?<number>\d+)");
         //   var numberMatch = numberRegex.Match(button.Text);
         //   string numberInString = numberMatch.Groups["number"].Value;
            var buttonNumber = int.Parse(button.Name.Replace("_channel",string.Empty).Replace("Button",string.Empty));

            var newState = this.ChangeDiscretButtonColor(button);
            var lineNumber = buttonNumber - 1;
            if (newState)
            {
                this._channelsLines[lineNumber].Add();
            }
            else
            {
                this._channelsLines[lineNumber].Remove();
            }
         
        }

        /// <summary>
        /// ����������� ������� 1
        /// </summary>
        private void _marker1TrackBar_Scroll(object sender, EventArgs e)
        {
            var x = this._marker1TrackBar.Value;
            this._marker1Time.Text = string.Format(TIME_PATTERN, x);
            this.SetMarker(true, 0, x);
            this.CalculationDeltaTime();
            this.SetLabelText(this._marker1Channels, this._listV2.Channels, x, CHANNELS_LABEL_PATTERN);
            this.SetLabelText(this._marker1Discrets, this._listV2.Discrets, x, DISCRET_LABEL_PATTERN);
            this.SetLabelText(this._marker1Currents, this._listV2.Currents, x, CountingListV2.INames , "�");
            this.SetLabelText(this._marker1Voltages, this._listV2.Voltages, x, CountingListV2.UNames, "�");

        }

        /// <summary>
        /// ����������� ������� 2
        /// </summary>
        private void _marker2TrackBar_Scroll(object sender, EventArgs e)
        {
            var x = this._marker2TrackBar.Value;
            this._marker2Time.Text = string.Format(TIME_PATTERN, x);
            this.SetMarker(true, 1, x);
            this.CalculationDeltaTime();
            this.SetLabelText(this._marker2Channels, this._listV2.Channels, x, CHANNELS_LABEL_PATTERN);
            this.SetLabelText(this._marker2Discrets, this._listV2.Discrets, x, DISCRET_LABEL_PATTERN);
            this.SetLabelText(this._marker2Currents, this._listV2.Currents, x, CountingListV2.INames, "�");
            this.SetLabelText(this._marker2Voltages, this._listV2.Voltages, x, CountingListV2.UNames, "�");
        }

        /// <summary>
        /// ���������
        /// </summary>
        private void ���������ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._settingsForm.ShowDialog();
        }
        #endregion [Event Handlers]


        #region [Help members]

        /// <summary>
        /// �������� ��������� ������ ������� �������
        /// </summary>
        /// <param name="button">������</param>
        /// <returns>����� ���������</returns>
        private bool ChangeDiscretButtonColor(Button button)
        {
            bool result = button.BackColor.Equals(Color.White);
            button.BackColor = result ? Color.LightSlateGray : Color.White;

            return result;
        } 

        /// <summary>
        /// �������� ��������� ������
        /// </summary>
        /// <param name="button">������</param>
        /// <param name="buttonNumber">����� ������</param>
        /// <returns>����� ���������</returns>
        private bool ChangeButtonColor(Button button, int buttonNumber)
        {
            bool result;
            if (button.BackColor.Equals(Color.White))
            {

                button.BackColor = Colors[buttonNumber - 1];
                result = true;
            }
            else
            {
                button.BackColor = Color.White;
                result = false;

            }

            button.ForeColor = button.BackColor.Equals(Color.MidnightBlue) ? Color.White : Color.Black;
            return result;
        }

        private void SetLabelText(Label[] labels, double[][] values, int index, string[] names, string measure)
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Text = string.Format("{0} = {1} {2}", names[i],ValuesConverterCommon.Analog.DoubleToString3(values[i][index]),measure );
            }
        }

        private void SetLabelText(Label[] labels, ushort[][] values, int index, string pattern)
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Text = string.Format(pattern, i + 1, values[i][index]);
            }
        }
        /// <summary>
        /// ������������ ��������� ����� ��������� � ������� �� �����
        /// </summary>
        private void CalculationDeltaTime()
        {
        //    var delta = this._zoomer.Delta;
            var delta =
                Math.Abs(int.Parse(this._marker1Time.Text.Replace("��",string.Empty)) -
                         int.Parse(this._marker2Time.Text.Replace("��", string.Empty)));
                
            this._markerTimeDelta.Text = string.Format(TIME_PATTERN, delta);
        }

        /// <summary>
        /// ������������� ��� ��������� ������
        /// </summary>
        /// <param name="state">����</param>
        /// <param name="markerNumber">����� �������</param>
        /// <param name="x">������� �� X</param>
        private void SetMarker(bool state, int markerNumber, int x)
        {
            if (state)
            {
                foreach (var chart in this._charts)
                {
                    ChartHelper.AddMarker(chart, markerNumber, x);
                }
            }
            else
            {
                foreach (var chart in this._charts)
                {
                    ChartHelper.RemoveMarker(chart, markerNumber);
                }
            }
        }


        #endregion [Help members]




    }
}
