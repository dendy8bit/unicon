﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr763.Version2.Configuration.Structures.Defenses.Q
{
    public class AllDefenseQStruct : StructBase, IDgvRowsContainer<DefenseQStruct>
    {
        [XmlArray(ElementName = "Q")]
        [Layout(0, Count = 2)]
        private DefenseQStruct[] _u; //мтз U>

        [XmlArray(ElementName = "Все")]
        public DefenseQStruct[] Rows
        {
            get { return this._u; }
            set { this._u = value; }
        }
    }
}
