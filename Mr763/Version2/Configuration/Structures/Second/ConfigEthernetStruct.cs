﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version2.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурация по Ethernet
    /// </summary>
    public class ConfigEthernetStruct :StructBase// IStruct, IStructInit
    {
        [Layout(0)] ushort ip_lo;		//сетевой адрес устройства
        [Layout(1)] ushort ip_hi;
        [Layout(2)] ushort port;	//порт
        [Layout(3)] ushort rez;
    }
}
