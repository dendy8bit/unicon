﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version2.Configuration.Structures.Second
{
    /// <summary>
    /// для протокола MODBUS для 485 порта на пульте
    /// </summary>
    public class ConfigNetStruct : StructBase // IStruct, IStructInit
    {
        [Layout(0)] private ushort adr; //сетевой адрес устройства (1-247)
        [Layout(1)] private ushort spd; //скорость работы (1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200)
        [Layout(2)] private ushort pause; //пауза ответа (мс)
        [Layout(3)] private ushort rez;
    }
}
