﻿using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.Mr763.Version2.Configuration.Structures.RelayInd
{
    /// <summary>
    /// параметры индикаторов
    /// </summary>
    [XmlType(TypeName = "Один_индикатор")]
    public class IndicatorsStruct : StructBase
    {
        #region [Private fields]

        [Layout(0)] private ushort _signal;
        [Layout(1)] private ushort _type;

        private StringsConfig _stringsConfig = new StringsConfig(2.0);
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// Тип
        /// </summary>
        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Тип")]
        public string Type
        {
            get
            {
                var index = Common.GetBits(this._type, 0);
                return StringsConfig.ReleyType[index];
            }
            set
            {
                var index = (ushort) StringsConfig.ReleyType.IndexOf(value);
                this._type = Common.SetBits(this._type, index, 0);
            }
        }

        /// <summary>
        /// Сигнал
        /// </summary>
        [BindingProperty(1)]
        [XmlAttribute(AttributeName = "Сигнал")]
        public string Signal
        {
            get { return Validator.Get(this._signal, this._stringsConfig.RelaySignals); }
            set { this._signal = Validator.Set(value, this._stringsConfig.RelaySignals); }
        }

        /// <summary>
        /// Цвет
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(Type = typeof(XmlColor), ElementName = "Цвет_индикатора")]
        public Color Color
        {
            get { return Common.GetBit(this._type, 8) ? Color.Green : Color.Red; }
            set
            {
                var bit = value == Color.Green;
                this._type = Common.SetBit(this._type, 8, bit);
            }
        }
        #endregion [Properties]

        public void InitStringConfig(StringsConfig config)
        {
            this._stringsConfig = config;
        }
    }
}
