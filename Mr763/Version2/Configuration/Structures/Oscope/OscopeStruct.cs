﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version2.Configuration.Structures.Oscope
{
    /// <summary>
    /// Конфигурация осцилографа
    /// </summary>
    public class OscopeStruct : StructBase 
    {
        #region [Constants]

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private OscopeConfigStruct _oscopeConfig;
        [Layout(1)] private OscopeAllChannelsStruct _oscopeAllChannels;
      

        #endregion [Private fields]


        #region [Properties]







        #endregion [Properties]
        /// <summary>
        /// Конфигурация_осц
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Конфигурация_осц")]
        public OscopeConfigStruct OscopeConfig
        {
            get { return this._oscopeConfig; }
            set { this._oscopeConfig = value; }
        }
        /// <summary>
        /// Конфигурация каналов
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "Конфигурация_каналов")]
        public OscopeAllChannelsStruct OscopeAllChannels
        {
            get { return this._oscopeAllChannels; }
            set { this._oscopeAllChannels = value; }
        }

        public void InitStringsConfig(StringsConfig config)
        {
            this._oscopeAllChannels.InitStringsConfig(config);
        }
    }
}
