﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr763.Version2.Configuration.Structures.Oscope
{
    [XmlType(TypeName = "Один_канал")]
    public class ChannelStruct : StructBase
    {
        [Layout(0)] private ushort _channel;
        private StringsConfig _stringsConfig = new StringsConfig(2.0);

        [BindingProperty(0)]
        [XmlAttribute(AttributeName = "Канал")]
        public string Channel
        {
            get { return Validator.Get(this._channel, this._stringsConfig.RelaySignals); }
            set { this._channel = Validator.Set(value, this._stringsConfig.RelaySignals); }
        }

        public void InitStringsConfig(StringsConfig config)
        {
            this._stringsConfig = config;
        }
    }
}
