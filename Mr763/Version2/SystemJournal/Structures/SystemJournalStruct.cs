﻿using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version2.SystemJournal.Structures
{
  
    public class SystemJournalStruct : StructBase
    {

        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        private const string MESSAGE_WITH_CODE_PATTERN = "{0} ({1})";
        private const string MESSAGE_SPL_PATTERN = "СООБЩЕНИЕ ЖС СПЛ N{0}";

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _moduleErrorCode;
        [Layout(8)] private ushort _message;

        #endregion [Private fields]


        #region [Properties]
        public List<string> Messages { get; set; }
        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                          this.Month +
                          this.Date +
                          this.Hour +
                          this.Minute +
                          this.Second +
                          this.Millisecond +
                          this.ModuleErrorCode +
                          this.Message;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );

            }
        }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string GetRecordMessage
        {
            get
            {
                if (this.Messages == null)
                {
                    this.Messages = StringsSj.Message;
                }
                if ((this.Message == 7) | (this.Message == 9) | (this.Message == 11) | (this.Message == 13) |
                    (this.Message == 15))
                    return string.Format(MESSAGE_WITH_CODE_PATTERN, this.Messages[this.Message],
                                         this.ModuleErrorCode);
                if (this.Message < this.Messages.Count)
                    return this.Messages[this.Message];
                if (this.Message >= 500)
                    return string.Format(MESSAGE_SPL_PATTERN, this.Message - 499);
                return string.Empty;
            }
        }

        public ushort Year
        {
            get { return this._year; }
            set { this._year = value; }
        }

        public ushort Month
        {
            get { return this._month; }
            set { this._month = value; }
        }

        public ushort Date
        {
            get { return this._date; }
            set { this._date = value; }
        }

        public ushort Hour
        {
            get { return this._hour; }
            set { this._hour = value; }
        }

        public ushort Minute
        {
            get { return this._minute; }
            set { this._minute = value; }
        }

        public ushort Second
        {
            get { return this._second; }
            set { this._second = value; }
        }

        public ushort Millisecond
        {
            get { return this._millisecond; }
            set { this._millisecond = value; }
        }

        public ushort ModuleErrorCode
        {
            get { return this._moduleErrorCode; }
            set { this._moduleErrorCode = value; }
        }

        public ushort Message
        {
            get { return this._message; }
            set { this._message = value; }
        }

        #endregion [Properties]
    }
}
