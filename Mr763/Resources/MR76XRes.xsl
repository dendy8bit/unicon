<?xml version="1.0" encoding="WINDOWS-1251"?>
<!-- Edited by XMLSpy� -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
<head>
 <script type="text/javascript">
	function translateBoolean(value, elementId){
 		var result = "";
 		if(value.toString().toLowerCase() == "true")
  			result = "��";
 		else if(value.toString().toLowerCase() == "false")
  			result = "���";
		else 
			result = "������������ ��������"
 		document.getElementById(elementId).innerHTML = result; 
}
</script>
</head>
  <body>
    <h2>���������� <xsl:value-of select="��76x/���_����������"/>. ������ �� <xsl:value-of select="��76x/������"/>. ����� <xsl:value-of select="��76x/�����_����������"/> </h2>
  
  <xsl:if test="��76x/���_���������� = '��761'"> 
   <h3><b>��������� ��������� </b></h3>
   <b>����</b>
    <table border="1">
      <tr bgcolor="FFFFCC">
         <th>��� ��</th>
         <th>Im, In</th>
         <th>ITT�, A</th>
         <th>ITTn, A</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/���_��"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/I�"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_��"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_����"/></td>
      </tr>

    </table>
 <b>����������</b>
    <table border="1">

      <tr bgcolor="FFFFCC">
         <th>U0</th>
         <th>KTH�</th>
         <th>������.���</th>
         <th>KTHn</th>
		 <th>������.��n</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/���_Uo"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHL"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/�����_L"/></td>
		 <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHX"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/�����_X"/></td>
	  </tr>

    </table>
  </xsl:if>
 
   <xsl:if test="��76x/���_���������� = '��762'"> 
   <h3><b>��������� ��������� </b></h3>
   <b>����</b>
    <table border="1">
      <tr bgcolor="FFFFCC">
         <th>��� ��</th>
         <th>Im, In</th>
         <th>ITT�, A</th>
         <th>ITTn, A</th>
		 <th>ITTn1, A</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/���_��"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/I�"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_��"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_����"/></td>
		 <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_����1"/></td>
      </tr>

    </table>
 <b>����������</b>
    <table border="1">

      <tr bgcolor="FFFFCC">
         <th>Uo</th>
         <th>KTH�</th>
         <th>������.���</th>
         </tr>
     
      <tr align="center">
		<td>U0</td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHL"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/�����_L"/></td>

	  </tr>

    </table>
  </xsl:if>
 
   <xsl:if test="��76x/���_���������� = '��763'"> 
   <h3><b>��������� ��������� </b></h3>
   <b>����</b>
    <table border="1">
      <tr bgcolor="FFFFCC">
         <th>��� ��</th>
         <th>Im, In</th>
         <th>ITT�, A</th>

      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/���_��"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/I�"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_I/������������_��"/></td>

      </tr>

    </table>
 <b>����������</b>
    <table border="1">

      <tr bgcolor="FFFFCC">
         <th>Uo</th>
         <th>KTH�</th>
         <th>������.���</th>
         <th>KTHn</th>
		 <th>������.��n</th>
		 <th>KTHn1</th>

      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/���_Uo"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHL"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/�����_L"/></td>
		 <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHX"/></td>
         <td><xsl:value-of select="��76x/�������������_�������������/�����_U/�����_X"/></td>
		 <td><xsl:value-of select="��76x/�������������_�������������/�����_U/KTHX1"/></td>
	  </tr>

    </table>
  </xsl:if>
 
 
 <b>���</b>
    <table border="1">
      <tr bgcolor="FFFFCC">
         <th>�����</th>
         <th>X �����, ��/��</th>
      </tr>
     
      <tr align="center">
         <td><xsl:value-of select="��76x/���/�����"/></td>
         <td><xsl:value-of select="��76x/���/Xyd"/></td>
       </tr>

    </table>
 
<!--|||||||||||||||||||||||||������� �������|||||||||||||||||||||||||||||||||||||||||-->
    <h3><b>������� �������</b></h3>
	<b>���������� ������� �</b>
	<table border="1">

      <tr bgcolor="CCFF99">
         <th>����� ��</th>
         <th>������������</th>
	  </tr>
 
   <xsl:for-each select="��76x/�������_����������_�������/��">  

     <xsl:if test="position() &lt; 9 ">
   	  <tr>	
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
			
  </tr>
</xsl:if>	


    </xsl:for-each>
	 
    </table>

	<b>���������� ������� ���</b>
	<table border="1">

      <tr bgcolor="CCFF99">
         <th>����� ��</th>
         <th>������������</th>
      </tr>
	  
   <xsl:for-each select="��76x/�������_����������_�������/��">  
     <xsl:if test="position() &gt; 8 ">
	 <tr>
         <td><xsl:value-of  select="position()"/></td>
	
			 <td>
			 <xsl:for-each select="�������">
				<xsl:if test="current() !='���'">
					<xsl:if test="current() ='��'">
					�<xsl:value-of select="position()"/>
					</xsl:if>
					<xsl:if test="current() ='������'">
					^�<xsl:value-of select="position()"/>
					</xsl:if>
				</xsl:if>
			 </xsl:for-each>
			 </td>
			
	  </tr>
</xsl:if>	  
    </xsl:for-each>
    </table>
<br />
��������� ������ ������� <b><xsl:value-of select="��76x/������������_�������_��������/����_���������_������_�������"/> </b><br /> 
����� ��������� <b><xsl:value-of select="��76x/������������_�������_��������/����_�����_���������"/>	</b>

<!-- |||||||||||||||||||||||||||��������� ���������||||||||||||||||||||||||||||||||||||||||||| -->
	<h3><b>��������� ���������</b></h3>
	 <table border="1">
      <tr bgcolor="CCCCCC">
         <th>�����, � </th>
         <th>����, �</th>
         <th>I��, In</th>
         <th>I����, In</th>
		 <th>� �����, ��</th>
		 <th>� ����, ��</th>
		 <th>Q���, %</th>
         <th>���� Q�����</th>
		 <th>���� N�����</th>
      </tr>
     
      <tr>
         <td><xsl:value-of select="��76x/���������/T���"/></td>
         <td><xsl:value-of select="��76x/���������/T���"/></td>
         <td><xsl:value-of select="��76x/���������/I��"/></td>
         <td><xsl:value-of select="��76x/���������/I����"/></td>
		 <td><xsl:value-of select="��76x/���������/�����"/></td>
		 <td><xsl:value-of select="��76x/���������/T����"/></td>
		 <td><xsl:value-of select="��76x/���������/Q���"/></td>
		 <td><xsl:value-of select="��76x/���������/Q_�����"/></td>
		 <td><xsl:value-of select="��76x/���������/N_�����"/></td>
      </tr>

    </table>
	
<!-- |||||||||||||||||||||||||||�������� �������||||||||||||||||||||||||||||||||||||||||||| -->	
   <h3><b>�������� ������� </b></h3>
   <b>�������� ����</b>
    <table border="1">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
         <th>�����, ��</th>
      </tr>
     
      
		 <xsl:for-each select="��76x/����_�_����������/����/���_����/����_����">
		 <tr>
         <td><xsl:value-of select="position()+2"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="@�����"/></td>
		 </tr>
		 </xsl:for-each>
	  

    </table>   

	   <b>����������</b>
    <table border="1">
      <tr bgcolor="CCFFCC">
         <th>�����</th>
         <th>���</th>
         <th>������</th>
         <th>����</th>
      </tr>
     
      
		 <xsl:for-each select="��76x/����_�_����������/����������/���_����������/����_���������">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
         <td><xsl:value-of select="@���"/></td>
         <td><xsl:value-of select="@������"/></td>
         <td><xsl:value-of select="����_����������/@����"/></td>
		 </tr>
		 </xsl:for-each>

    </table>   

		   <b>���� �������������</b>
    <table border="1">
      <tr bgcolor="CCFFCC">
         <th>����������</th>
         <th>�����������</th>
         <th>���������</th>
         <th>�����������</th>
		 <th>�������, ��</th>
      </tr>
     
 		  <tr>
         <td><xsl:variable  name = "v1" select = "��76x/����_�_����������/����_�������������/@�������������_1" />
		 <xsl:if test="$v1 = 'false' " >��� </xsl:if> <xsl:if test="$v1 = 'true' " > ���� </xsl:if></td>
		 
         <td><xsl:variable  name = "v2" select= "��76x/����_�_����������/����_�������������/@�������������_2" />
		 <xsl:if test="$v2 = 'false' " >��� </xsl:if> <xsl:if test="$v2 = 'true' " > ���� </xsl:if></td>
		 
         <td><xsl:variable  name = "v3" select= "��76x/����_�_����������/����_�������������/@�������������_3" />
		 <xsl:if test="$v3 = 'false' " >��� </xsl:if> <xsl:if test="$v3 = 'true' " > ���� </xsl:if></td>
		 
         <td><xsl:variable  name = "v4" select= "��76x/����_�_����������/����_�������������/@�������������_4" />
		 <xsl:if test="$v4 = 'false' " >��� </xsl:if> <xsl:if test="$v4 = 'true' " > ���� </xsl:if></td>
		 
		 <td><xsl:value-of  select= "��76x/����_�_����������/����_�������������/@�������_����_�������������" /></td>
		 </tr>
    </table>   

    <b>���</b>
    <table border="1">
      <tr bgcolor="CCFFCC">
         <th>����� ���</th>
         <th>������������</th>
         
      </tr>
     
      
		 <xsl:for-each select="��76x/���_���/���">
		 <tr>
         <td><xsl:value-of select="position()"/></td>
		 <td>
		 <xsl:for-each select="�������">
         <xsl:value-of select="current()"/>|
         </xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
	  

    </table>   

	<!-- |||||||||||||||||||||||||||�����������||||||||||||||||||||||||||||||||||||||||||| -->	
   <h3><b>�������������</b></h3>
���������� ������������ <b><xsl:value-of select="��76x/������������_�����������/������������_���/����������_�����������"/></b><br />
������������ ���������� <b><xsl:value-of select="��76x/������������_�����������/������������_���/����������"/>%</b><br />  
�������� �� <b><xsl:value-of select="��76x/������������_�����������/������������_���/��������"/></b>
<br /><br />
<b>������</b> 
   <table border="1">
      <tr>
         <th bgcolor="66FFCC">�����</th>
         
		 <xsl:for-each select="��76x/������������_�����������/������������_�������/���_������/����_�����">
			<th><xsl:value-of select="position()"/></th>
		 </xsl:for-each>
		 
      </tr>
     <tr>
         <th bgcolor="66FFCC">������</th>
         
		 <xsl:for-each select="��76x/������������_�����������/������������_�������/���_������/����_�����/@�����">
			<th><xsl:value-of select="current()"/></th>
		 </xsl:for-each>
		 
      </tr>

    </table>   
 
 <!-- |||||||||||||||||||||||||||���������� � ����������||||||||||||||||||||||||||||||||||||||||||| -->	
 <h3><b>���������� � ����������</b></h3>
   <b>�����������</b>
		    <table border="1">
      <tr bgcolor="9966CC">
         <th>���������</th>
         <th>��������</th>
         <th>�������������</th>
         <th>����-��</th>
		 <th>t ����, ��</th>
		 <th>I ����, In</th>
		 <th>�������, ��</th>
		 <th>t �����, ��</th>
		 <th>�������� �����</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/������������_�����������/���������"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/��������"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/������"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/����������"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/t����"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/���_����"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/�������"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/���������"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/��������_�����_���������_����������"/></td>
		 </tr>
    </table> 
 
 <b>����������</b>
		    <table border="1">
      <tr bgcolor="9966CC">
         <th>���� ��������</th>
         <th>���� ���������</th>
         <th>������� ��������</th>
         <th>������� ���������</th>
		 <th>������</th>
		 <th>����</th>
		 <th>�������</th>
		 <th>����</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/������������_�����������/����_���"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/����_����"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/����_����_��������"/></td>
         <td><xsl:value-of select="��76x/������������_�����������/����_����_���������"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/����"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/����"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/�������"/></td>
		 <td><xsl:value-of select="��76x/������������_�����������/����"/></td>

		 </tr>
    </table> 
 
  <b>���</b>
		    <table border="1">
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>�������</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/���/�����"/></td>
         <td><xsl:value-of select="��76x/���/�������_���"/></td>
		 </tr>
    </table> 
	
  <b>���</b>
		    <table border="1">
      <tr bgcolor="9966CC">
         <th>�����</th>
         <th>����������</th>
		 <th>t ����, ��</th>
		 <th>t �����, ��</th>
		 <th>1 ����</th>
		 <th>2 ����</th>
		 <th>3 ����</th>
		 <th>4 ����</th>
		 <th>��������������</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/���/�����"/></td>
         <td><xsl:value-of select="��76x/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��76x/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��76x/���/�����_����������_���"/></td>
		 <td><xsl:value-of select="��76x/���/����1"/></td>
		 <td><xsl:value-of select="��76x/���/����2"/></td>
		 <td><xsl:value-of select="��76x/���/����3"/></td>
		 <td><xsl:value-of select="��76x/���/����4"/></td>
		 <td><xsl:value-of select="��76x/���/������_���_��_�����������������_����������_�����������"/></td>
		 
		 </tr>
    </table> 	
    <b>���</b>
		    <table border="1">			
			
      <tr bgcolor="9966CC">
         <th>�� �������</th>
         <th>�� ����������</th>
		 <th>�� ��������.</th>
		 <th>�� ������</th>
		 <th>����. ����</th>
		 <th>����������</th>
		 <th>�����</th>
		 <th>��� ������.</th>
		 <th>t ��, ��</th>
		 <th>�������</th>
		 <th>t ���, ��</th>
		 <th>t ����, ��</th>
		 <th>�����</th>
      </tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/���/��_�������"/></td>
         <td><xsl:value-of select="��76x/���/��_����������"/></td>
		 <td><xsl:value-of select="��76x/���/��_��������������"/></td>
		 <td><xsl:value-of select="��76x/���/��_������"/></td>
		 <td><xsl:value-of select="��76x/���/����"/></td>
		 <td><xsl:value-of select="��76x/���/����_����������_���"/></td>
		 <td><xsl:value-of select="��76x/���/����_�����_����������_���"/></td>
		 <td><xsl:value-of select="��76x/���/����_���_������������"/></td>
		 <td><xsl:value-of select="��76x/���/�����_���_������������"/></td>
		 <td><xsl:value-of select="��76x/���/����_���_�������"/></td>
		 <td><xsl:value-of select="��76x/���/�����_���_�������"/></td>
		 <td><xsl:value-of select="��76x/���/��������_����������_�������"/></td>
		 <td><xsl:value-of select="��76x/���/�����"/></td>
		 </tr>
    </table>
 <!-- |||||||||||||||||||||||||||�������� �����������||||||||||||||||||||||||||||||||||||||||||| -->
    <h3>
      <b>�������� �����������</b>
    </h3>
    <b>����� �������</b>
    <table border="1">
      <tr bgcolor="CC9999">
        <th>U1</th>
        <th>U2</th>
        <th>Umin.���, �</th>
        <th>Umin.���, �</th>
        <th>Umax.���, �</th>
        <th>t��, ��</th>
        <th>t�����, ��</th>
        <th>t ���, ��</th>
        <xsl:if test="��76x/������ &gt; 2.00">
          <th>����, %</th>
          <th>f, ����.</th>
        </xsl:if>
      </tr>
      <tr>
        <td>
          <xsl:value-of select="��76x/������������_�����������/U1"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/U2"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/Umin"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/Umin_���"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/Umax_���"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/t��"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/t�����"/>
        </td>
        <td>
          <xsl:value-of select="��76x/������������_�����������/t���"/>
        </td>
        <xsl:if test="��76x/������ &gt; 2.00">
          <td>
            <xsl:value-of select="��76x/������������_�����������/����"/>
          </td>
          <td>
            <xsl:value-of select="��76x/������������_�����������/f"/>
          </td>
        </xsl:if>
      </tr>
    </table>

    <br />
    <b>������� ������� ���������</b>
    <table border="1">
      <td>
        ����� <b>
          <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/�����"/>
        </b><br />
        dUmax <b>
          <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/dUmax"/>
        </b>� <br /><br />
        <b>���������� ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>U1 ���, U2 ����</th>
            <th>U1 ����, U2 ���</th>
            <th>U1 ���, U2 ���</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/U1���U2����"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/U1����U2���"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/U1���U2���"/>
            </td>
          </tr>
        </table>
        <b>���������� ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>dF, ��</th>
            <th>dFi, ����</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/����������_���������"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/dFi"/>
            </td>
          </tr>
        </table>
        <b>������������ ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>dF, ��</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_�������_���������/������������_���������"/>
            </td>
          </tr>
        </table>
      </td>
    </table>

    <br />
    <b>������� ��������������� ���������</b>
    <table border="1">
      <td>
        ����� <b>
          <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/�����"/>
        </b><br />
        dUmax <b>
          <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/dUmax"/>
        </b>� <br />
        <xsl:if test="��76x/������ &gt; 2.00">
          ���������� <b>
            <xsl:value-of select="��76x/������������_�����������/����������"/>
          </b><br />
        </xsl:if>
        <br />
        <b>���������� ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>U1 ���, U2 ����</th>
            <th>U1 ����, U2 ���</th>
            <th>U1 ���, U2 ���</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/U1���U2����"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/U1����U2���"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/U1���U2���"/>
            </td>
          </tr>
          <xsl:if test="��76x/������ &gt; 2.00">
            <tr>
              <td>
                <xsl:value-of select="��76x/������������_�����������/�������_U1_���_U2_����"/>
              </td>
              <td>
                <xsl:value-of select="��76x/������������_�����������/�������_U1_����_U2_���"/>
              </td>
              <td> </td>
            </tr>
          </xsl:if>
        </table>
        <b>���������� ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>dF, ��</th>
            <th>dFi, ����</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/����������_���������"/>
            </td>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/dFi"/>
            </td>
          </tr>
        </table>
        <b>������������ ���������</b>
        <table border="1">
          <tr bgcolor="CC9999">
            <th>dF, ��</th>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="��76x/������������_�����������/������_���_���������������_���������/������������_���������"/>
            </td>
          </tr>
        </table>
      </td>
    </table>
  
<!-- |||||||||||||||||||||||||||������||||||||||||||||||||||||||||||||||||||||||| -->  
 <h2><b>������. �������� ������ �������</b></h2>
<h3><b>���� ��</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>I</th>
         <th>In</th>
		 <th>I0</th>
         <th>I2</th>
		</tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/������/��������_������_�������/����/I"/></td>
         <td><xsl:value-of select="��76x/������/��������_������_�������/����/In"/></td>
		 <td><xsl:value-of select="��76x/������/��������_������_�������/����/I0"/></td>
		 <td><xsl:value-of select="��76x/������/��������_������_�������/����/I2"/></td>
		 </tr>
		</table> 
  
<h3><b>������ I</b></h3>
		<table border="1">
		<td>
		<b>������ I> ������������� ����</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>������</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() &lt; 6">
		 <tr align="center">
			<td><center>I&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			 	 <xsl:element name="td">
             <xsl:attribute name="id">Upusk1<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="U����"/>,"Upusk1<xsl:value-of select="position()"/>");</script>
         </xsl:element>

			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
				<xsl:element name="td">
				<xsl:attribute name="id">Ty1<xsl:value-of select="position()"/></xsl:attribute>            
				<script>translateBoolean(<xsl:value-of select="Ty"/>,"Ty1<xsl:value-of select="position()"/>");</script>
				</xsl:element>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		<br /><b>������ I �� ���������� ����� � ���������� ������</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
		 <th>����� Ip</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>������</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() &gt; 5">
		 <xsl:if test="position() &lt; 8">
		 <tr>
			<td><center>I&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="�����_Ip"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><xsl:for-each select="Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		<br /><b>������ I&#60; ������������ ����</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
		 <th>������</th>

		 <th>t, ��</th>

		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() =8">
		 <tr>
			<td><center>I&#60;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
				
		
		</td>
		</table>

<h3><b>������ I*</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>I��, In</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>I*</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/I_��_�������/���/DefenseStarStruct">
		 <tr align="center">
			<td><center>I*&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="I_x002A_"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><xsl:for-each select="Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		
		 </tr>
		 </xsl:for-each>
		</table>

<h3><b>������ I2I1</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>����������</th>
		 <th>I2/I1</th>
         <th>t��, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 </tr>
     
 	
		 <tr>
	
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/I2I1/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/I2I1/����������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/I2I1/�������_I2_I1"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/I2I1/t��"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/I2I1/���"/></center></td>
			
			<td><xsl:for-each select="��76x/������/��������_������_�������/I2I1/����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/��������_������_�������/I2I1/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/��������_������_�������/I2I1/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>

		</table>
 <xsl:if test="��76x/���_���������� != '��763'">
<h3><b>������ I�</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>����������</th>
		 <th>U����, �</th>
		 <th>I��</th>
         <th>t��, ��</th>
		 <th>t�, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 </tr>
     
 	
		 <tr align="center">
	
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/����������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/U_����"/>&#32;<xsl:for-each select="��76x/������/��������_������_�������/Ig/U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/�������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/t��"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/ty"/>&#32; <xsl:for-each select="��76x/������/��������_������_�������/Ig/Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/Ig/���"/></center></td>
			<td><xsl:for-each select="��76x/������/��������_������_�������/Ig/����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/��������_������_�������/Ig/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/��������_������_�������/Ig/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>

		</table>
</xsl:if>
<h3><b>������ U></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>���</th>
         <th>U��, �</th>
		 <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>U��, �</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/U/���/DefenceUStruct">
		<xsl:if test="position() &lt; 5">
		<tr align="center">
		    
			<td><center>U>  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="���_Umax"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><center><xsl:value-of select="U_����"/>&#32; <xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		
		<h3><b>������ U&#60;</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>���</th>
         <th>U��, �</th>
		 <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>U��, �</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>���������� U&#60;5�</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/U/���/DefenceUStruct">
		 <xsl:if test="position() &gt; 4">
		 <tr align="center">
		    <td><center>U&#60;  <xsl:value-of select="position()-4"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="���_Umin"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><center><xsl:value-of select="U_����"/>&#32; <xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><xsl:for-each select="����������_U_5V"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		<h3><b>������ F></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>F��, ��</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>F��, ��</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/F/���/DefenseFStruct">
		<xsl:if test="position() &lt; 5">
		<tr align="center">
		    
			<td><center>F>  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		
		<h3><b>������ F&#60;</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>F��, ��</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>F��, ��</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/F/���/DefenseFStruct">
		 <xsl:if test="position() &gt; 4">
		 <tr align="center">
		    <td><center>F&#60;  <xsl:value-of select="position()-4"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		
		
<h3><b>������ ���������</b></h3>
		<table border="1">
		<td>
		<b>���������� �� ����� ������ N</b>
		<table border="1">
		
		<tr bgcolor="FFCC66">
         <th>����. �� N����</th>
         <th>����. �� N���</th>
		 <th>����� ���������� �, � </th>
         <th>���� �� �����, � </th>
		</tr>
     
 		  
		 <tr>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��_�����_������/ColdStarts"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��_�����_������/HotStarts"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��_�����_������/BlockTime"/></center></td>
			<td><center><xsl:value-of select="��76x/���������/T����"/></center></td>
		 </tr>
		 
		</table>
  
  		<b>���������� �� ��������� ���������� Q</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>������� Q���, %</th>
		 <th>����� ���������� �, �</th>
		</tr>
     
 		 <tr>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��������/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��������/�������_������������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/��������_������_�������/��������/�����_������������"/></center></td>
		 </tr>
		 
		</table>
  
 <h3><b>������ Q></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>������� Q, %</th>
         <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/��������_������_�������/Q/���/DefenseQStruct">
		 <tr>
			<td><center>Q&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="Ustavka"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			
		 </tr>
		 </xsl:for-each>
		</table>
		</td>
		</table>
 
 <h3><b>�������</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>����.</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>�����.</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>����� �������</th>
		</tr>
		<xsl:for-each select="��76x/������/��������_������_�������/�������/���/DefenseExternalStruct">
		<tr align="center">
		    
			<td><center><xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="����_���_�������_�����"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="�������_���_�������_�����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
		 
		</table>
      
	<!--  //////////////////////////////////////////////////--> 
	  
	  <h2><b>������. ��������� ������ �������</b></h2>
<h3><b>���� ��</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>I</th>
         <th>In</th>
		 <th>I0</th>
         <th>I2</th>
		</tr>
     
 		  <tr>
         <td><xsl:value-of select="��76x/������/���������_������_�������/����/I"/></td>
         <td><xsl:value-of select="��76x/������/���������_������_�������/����/In"/></td>
		 <td><xsl:value-of select="��76x/������/���������_������_�������/����/I0"/></td>
		 <td><xsl:value-of select="��76x/������/���������_������_�������/����/I2"/></td>
		 </tr>
		</table> 
  
<h3><b>������ I</b></h3>
		<table border="1">
		<td>
		<b>������ I> ������������� ����</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>������</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() &lt; 6">
		 <tr align="center">
			<td><center>I&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			 	 <xsl:element name="td">
             <xsl:attribute name="id">Upusk1<xsl:value-of select="position()"/></xsl:attribute>            
             <script>translateBoolean(<xsl:value-of select="U����"/>,"Upusk1<xsl:value-of select="position()"/>");</script>
         </xsl:element>

			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
				<xsl:element name="td">
				<xsl:attribute name="id">Ty1<xsl:value-of select="position()"/></xsl:attribute>            
				<script>translateBoolean(<xsl:value-of select="Ty"/>,"Ty1<xsl:value-of select="position()"/>");</script>
				</xsl:element>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		<br /><b>������ I �� ���������� ����� � ���������� ������</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
		 <th>����� Ip</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>������</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() &gt; 5">
		 <xsl:if test="position() &lt; 8">
		 <tr>
			<td><center>I&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="�����_Ip"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><xsl:for-each select="Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		<br /><b>������ I&#60; ������������ ����</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>I��, In</th>
		 <th>������</th>

		 <th>t, ��</th>

		 <th>����������</th>
		 <th>I2�/I1�, %</th>
		 <th>���� �� I2�/I1�</th>
		 <th>������. ����.</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/I/���/MtzMainStruct">
		 <xsl:if test="position() =8">
		 <tr>
			<td><center>I&#60;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="�������_2�_1�"/></center></td>
			<td><center><xsl:value-of select="����_2�_1�"/></center></td>
			<td><center><xsl:value-of select="������_����"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
				
		
		</td>
		</table>

<h3><b>������ I*</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>I��, In</th>
         <th>U����, �</th>
		 <th>���� �� U</th>
		 <th>�����������</th>
		 <th>������.����</th>
		 <th>I*</th>
		 <th>������-��</th>
		 <th>t, ��</th>
		 <th>k �����. �-��</th>
		 <th>��, ��</th>
		 <th>���� �� ��</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/I_��_�������/���/DefenseStarStruct">
		 <tr align="center">
			<td><center>I*&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="�����������"/></center></td>
			<td><center><xsl:value-of select="������_����_x0028_I_x002A__x0029_"/></center></td>
			<td><center><xsl:value-of select="I_x002A_"/></center></td>
			<td><center><xsl:value-of select="��������������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="K"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><xsl:for-each select="Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		
		 </tr>
		 </xsl:for-each>
		</table>

<h3><b>������ I2I1</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>����������</th>
		 <th>I2/I1</th>
         <th>t��, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 </tr>
     
 	
		 <tr>
	
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/I2I1/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/I2I1/����������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/I2I1/�������_I2_I1"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/I2I1/t��"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/I2I1/���"/></center></td>
			
			<td><xsl:for-each select="��76x/������/���������_������_�������/I2I1/����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/���������_������_�������/I2I1/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/���������_������_�������/I2I1/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>

		</table>

		 <xsl:if test="��76x/���_���������� != '��763'">
<h3><b>������ I�</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>����������</th>
		 <th>U����, �</th>
		 <th>I��</th>
         <th>t��, ��</th>
		 <th>t�, ��</th>
		 <th>���</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 </tr>
     
 	
		 <tr align="center">
	
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/����������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/U_����"/>&#32;<xsl:for-each select="��76x/������/���������_������_�������/Ig/U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/�������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/t��"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/ty"/>&#32; <xsl:for-each select="��76x/������/���������_������_�������/Ig/Ty"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/Ig/���"/></center></td>
			<td><xsl:for-each select="��76x/������/���������_������_�������/Ig/����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/���������_������_�������/Ig/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="��76x/������/���������_������_�������/Ig/���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>

		</table>
</xsl:if>
<h3><b>������ U></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>���</th>
         <th>U��, �</th>
		 <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>U��, �</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/U/���/DefenceUStruct">
		<xsl:if test="position() &lt; 5">
		<tr align="center">
		    
			<td><center>U>  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="���_Umax"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><center><xsl:value-of select="U_����"/>&#32; <xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		
		<h3><b>������ U&#60;</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>���</th>
         <th>U��, �</th>
		 <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>U��, �</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>���������� U&#60;5�</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/U/���/DefenceUStruct">
		 <xsl:if test="position() &gt; 4">
		 <tr align="center">
		    <td><center>U>  <xsl:value-of select="position()-4"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="���_Umax"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><center><xsl:value-of select="U_����"/>&#32; <xsl:for-each select="U����"> <xsl:if test="current() ='false'">���	</xsl:if><xsl:if test="current() ='true'">����</xsl:if></xsl:for-each></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><xsl:for-each select="����������_U_5V"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		<h3><b>������ F></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>F��, ��</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>F��, ��</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/F/���/DefenseFStruct">
		<xsl:if test="position() &lt; 5">
		<tr align="center">
		    
			<td><center>F>  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>
		
		<h3><b>������ F&#60;</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>F��, ��</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>F��, ��</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>�����</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/F/���/DefenseFStruct">
		 <xsl:if test="position() &gt; 4">
		 <tr align="center">
		    <td><center>F&#60;  <xsl:value-of select="position()-4"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="�������"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="U_����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:if>
		 </xsl:for-each>
		 
		</table>

		
		
<h3><b>������ ���������</b></h3>
		<table border="1">
		<td>
		<b>���������� �� ����� ������ N</b>
		<table border="1">
		
		<tr bgcolor="FFCC66">
         <th>����. �� N����</th>
         <th>����. �� N���</th>
		 <th>����� ���������� �, � </th>
         <th>���� �� �����, � </th>
		</tr>
     
 		  
		 <tr>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��_�����_������/ColdStarts"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��_�����_������/HotStarts"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��_�����_������/BlockTime"/></center></td>
			<td><center><xsl:value-of select="��76x/���������/T����"/></center></td>
		 </tr>
		 
		</table>
  
  		<b>���������� �� ��������� ���������� Q</b>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�����</th>
         <th>������� Q���, %</th>
		 <th>����� ���������� �, �</th>
		</tr>
     
 		 <tr>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��������/�����"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��������/�������_������������"/></center></td>
			<td><center><xsl:value-of select="��76x/������/���������_������_�������/��������/�����_������������"/></center></td>
		 </tr>
		 
		</table>
  
 <h3><b>������ Q></b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>�����</th>
		 <th>������� Q, %</th>
         <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		</tr>
     
 		  
		 <xsl:for-each select="��76x/������/���������_������_�������/Q/���/DefenseQStruct">
		 <tr>
			<td><center>Q&#62;  <xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="Ustavka"/></center></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			
		 </tr>
		 </xsl:for-each>
		</table>
		</td>
		</table>
 
 <h3><b>�������</b></h3>
		<table border="1">
		<tr bgcolor="FFCC66">
         <th>�������</th>
         <th>���������</th>
		 <th>����.</th>
         <th>t��, ��</th>
		 <th>t��, ��</th>
		 <th>�����.</th>
		 <th>����/���</th>
		 <th>����������</th>
		 <th>�����������</th>
		 <th>����</th>
		 <th>���</th>
		 <th>���</th>
		 <th>��� ����.</th>
		 <th>����� �������</th>
		</tr>
		<xsl:for-each select="��76x/������/���������_������_�������/�������/���/DefenseExternalStruct">
		<tr align="center">
		    
			<td><center><xsl:value-of select="position()"/></center></td>
			<td><center><xsl:value-of select="�����"/></center></td>
			<td><center><xsl:value-of select="����_���_�������_�����"/></center></td>
			<td><center><xsl:value-of select="t��"/></center></td>
			<td><center><xsl:value-of select="ty"/></center></td>
			<td><center><xsl:value-of select="�������_���_�������_�����"/></center></td>
			<td><xsl:for-each select="U����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><center><xsl:value-of select="����������"/></center></td>
			<td><center><xsl:value-of select="���"/></center></td>
			<td><xsl:for-each select="����"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="���_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
			<td><xsl:for-each select="�����_�������"><xsl:if test="current() ='false'">	���	</xsl:if><xsl:if test="current() ='true'">	����	</xsl:if></xsl:for-each></td>
		 </tr>
		 </xsl:for-each>
		 
		</table>
      
  
    </body>
  </html>
</xsl:template>
</xsl:stylesheet>
