﻿namespace BEMN.Mr763.Version300.Emulation
{
    partial class EmulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startTime = new System.Windows.Forms.CheckBox();
            this._signal = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label31 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this._status = new System.Windows.Forms.MaskedTextBox();
            this._step = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._timeSignal = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._startFazaUc = new System.Windows.Forms.CheckBox();
            this._startUa = new System.Windows.Forms.CheckBox();
            this._startFUn1 = new System.Windows.Forms.CheckBox();
            this._startUb = new System.Windows.Forms.CheckBox();
            this._startUc = new System.Windows.Forms.CheckBox();
            this._startFUn = new System.Windows.Forms.CheckBox();
            this._startUn = new System.Windows.Forms.CheckBox();
            this._startUn1 = new System.Windows.Forms.CheckBox();
            this._startFUc = new System.Windows.Forms.CheckBox();
            this._startFazaUa = new System.Windows.Forms.CheckBox();
            this._startFazaUb = new System.Windows.Forms.CheckBox();
            this._startFUb = new System.Windows.Forms.CheckBox();
            this._startFazaUn = new System.Windows.Forms.CheckBox();
            this._startFazaUn1 = new System.Windows.Forms.CheckBox();
            this._startFUa = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._startFazaN1 = new System.Windows.Forms.CheckBox();
            this._startIn1 = new System.Windows.Forms.CheckBox();
            this._startFn1 = new System.Windows.Forms.CheckBox();
            this._startFazaB = new System.Windows.Forms.CheckBox();
            this._startFazaN = new System.Windows.Forms.CheckBox();
            this._startFazaC = new System.Windows.Forms.CheckBox();
            this._startFazaA = new System.Windows.Forms.CheckBox();
            this._startIn = new System.Windows.Forms.CheckBox();
            this._startIc = new System.Windows.Forms.CheckBox();
            this._startFa = new System.Windows.Forms.CheckBox();
            this._startIa = new System.Windows.Forms.CheckBox();
            this._startIb = new System.Windows.Forms.CheckBox();
            this._startFb = new System.Windows.Forms.CheckBox();
            this._startFc = new System.Windows.Forms.CheckBox();
            this._startFn = new System.Windows.Forms.CheckBox();
            this._writeEmulButton = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.FUn1 = new System.Windows.Forms.MaskedTextBox();
            this.Ua = new System.Windows.Forms.MaskedTextBox();
            this._fazaUabutUp = new System.Windows.Forms.Button();
            this._FUn1butDown = new System.Windows.Forms.Button();
            this.Un1 = new System.Windows.Forms.MaskedTextBox();
            this._fazaUabutDown = new System.Windows.Forms.Button();
            this._FUn1butUp = new System.Windows.Forms.Button();
            this._Un1butDown = new System.Windows.Forms.Button();
            this.fazaUa = new System.Windows.Forms.MaskedTextBox();
            this.FUn = new System.Windows.Forms.MaskedTextBox();
            this._Un1butUp = new System.Windows.Forms.Button();
            this._fazaUbbutUp = new System.Windows.Forms.Button();
            this._FUnbutDown = new System.Windows.Forms.Button();
            this.Un = new System.Windows.Forms.MaskedTextBox();
            this._fazaUbbutDown = new System.Windows.Forms.Button();
            this._FUnbutUp = new System.Windows.Forms.Button();
            this._UnbutDown = new System.Windows.Forms.Button();
            this.fazaUb = new System.Windows.Forms.MaskedTextBox();
            this.FUc = new System.Windows.Forms.MaskedTextBox();
            this._UnbutUp = new System.Windows.Forms.Button();
            this._fazaUcbutUp = new System.Windows.Forms.Button();
            this._FUcbutDown = new System.Windows.Forms.Button();
            this.Uc = new System.Windows.Forms.MaskedTextBox();
            this._fazaUcbutDown = new System.Windows.Forms.Button();
            this._FUcbutUp = new System.Windows.Forms.Button();
            this._UcbutDown = new System.Windows.Forms.Button();
            this.fazaUc = new System.Windows.Forms.MaskedTextBox();
            this.FUb = new System.Windows.Forms.MaskedTextBox();
            this._UcbutUp = new System.Windows.Forms.Button();
            this._fazaUnbutUp = new System.Windows.Forms.Button();
            this._FUbbutDown = new System.Windows.Forms.Button();
            this.Ub = new System.Windows.Forms.MaskedTextBox();
            this._fazaUnbutDown = new System.Windows.Forms.Button();
            this._FUbbutUp = new System.Windows.Forms.Button();
            this._UbbutDown = new System.Windows.Forms.Button();
            this.fazaUn = new System.Windows.Forms.MaskedTextBox();
            this.FUa = new System.Windows.Forms.MaskedTextBox();
            this._UbbutUp = new System.Windows.Forms.Button();
            this._fazaUn1butUp = new System.Windows.Forms.Button();
            this._FUabutDown = new System.Windows.Forms.Button();
            this._UabutDown = new System.Windows.Forms.Button();
            this._fazaUn1butDown = new System.Windows.Forms.Button();
            this._FUabutUp = new System.Windows.Forms.Button();
            this._UabutUp = new System.Windows.Forms.Button();
            this.fazaUn1 = new System.Windows.Forms.MaskedTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._k2 = new System.Windows.Forms.CheckBox();
            this._k1 = new System.Windows.Forms.CheckBox();
            this._d40 = new System.Windows.Forms.CheckBox();
            this._d39 = new System.Windows.Forms.CheckBox();
            this._d38 = new System.Windows.Forms.CheckBox();
            this._d37 = new System.Windows.Forms.CheckBox();
            this._d36 = new System.Windows.Forms.CheckBox();
            this._d35 = new System.Windows.Forms.CheckBox();
            this._d34 = new System.Windows.Forms.CheckBox();
            this._d33 = new System.Windows.Forms.CheckBox();
            this._d32 = new System.Windows.Forms.CheckBox();
            this._d31 = new System.Windows.Forms.CheckBox();
            this._d30 = new System.Windows.Forms.CheckBox();
            this._d29 = new System.Windows.Forms.CheckBox();
            this._d28 = new System.Windows.Forms.CheckBox();
            this._d27 = new System.Windows.Forms.CheckBox();
            this._d26 = new System.Windows.Forms.CheckBox();
            this._d25 = new System.Windows.Forms.CheckBox();
            this._d24 = new System.Windows.Forms.CheckBox();
            this._d23 = new System.Windows.Forms.CheckBox();
            this._d22 = new System.Windows.Forms.CheckBox();
            this._d21 = new System.Windows.Forms.CheckBox();
            this._d20 = new System.Windows.Forms.CheckBox();
            this._d19 = new System.Windows.Forms.CheckBox();
            this._d18 = new System.Windows.Forms.CheckBox();
            this._d17 = new System.Windows.Forms.CheckBox();
            this._d16 = new System.Windows.Forms.CheckBox();
            this._d15 = new System.Windows.Forms.CheckBox();
            this._d14 = new System.Windows.Forms.CheckBox();
            this._d13 = new System.Windows.Forms.CheckBox();
            this._d12 = new System.Windows.Forms.CheckBox();
            this._d11 = new System.Windows.Forms.CheckBox();
            this._d10 = new System.Windows.Forms.CheckBox();
            this._d9 = new System.Windows.Forms.CheckBox();
            this._d8 = new System.Windows.Forms.CheckBox();
            this._d7 = new System.Windows.Forms.CheckBox();
            this._d6 = new System.Windows.Forms.CheckBox();
            this._d5 = new System.Windows.Forms.CheckBox();
            this._d4 = new System.Windows.Forms.CheckBox();
            this._d3 = new System.Windows.Forms.CheckBox();
            this._d2 = new System.Windows.Forms.CheckBox();
            this._d1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Ia = new System.Windows.Forms.MaskedTextBox();
            this.Fn1 = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._Fn1butDown = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._Fn1butUp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Fn = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._FnbutDown = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this._FnbutUp = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Fc = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._FcbutDown = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this._FcbutUp = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.Fb = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._FbbutDown = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this._FbbutUp = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.Fa = new System.Windows.Forms.MaskedTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._FabutDown = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this._FabutUp = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.fazan1 = new System.Windows.Forms.MaskedTextBox();
            this._IabutUp = new System.Windows.Forms.Button();
            this._FazaN1butDown = new System.Windows.Forms.Button();
            this._IabutDown = new System.Windows.Forms.Button();
            this._FazaN1butUp = new System.Windows.Forms.Button();
            this._IbbutUp = new System.Windows.Forms.Button();
            this.fazaIn = new System.Windows.Forms.MaskedTextBox();
            this._IbbutDown = new System.Windows.Forms.Button();
            this._fazaNbutDown = new System.Windows.Forms.Button();
            this.Ib = new System.Windows.Forms.MaskedTextBox();
            this._fazaNbutUp = new System.Windows.Forms.Button();
            this._IcbutUp = new System.Windows.Forms.Button();
            this.fazaIc = new System.Windows.Forms.MaskedTextBox();
            this._IcbutDown = new System.Windows.Forms.Button();
            this._fazaCbutDown = new System.Windows.Forms.Button();
            this.Ic = new System.Windows.Forms.MaskedTextBox();
            this._fazaCbutUp = new System.Windows.Forms.Button();
            this._InbutUp = new System.Windows.Forms.Button();
            this.fazaIb = new System.Windows.Forms.MaskedTextBox();
            this._InbutDown = new System.Windows.Forms.Button();
            this._fazaBbutDown = new System.Windows.Forms.Button();
            this.In = new System.Windows.Forms.MaskedTextBox();
            this._fazaBbutUp = new System.Windows.Forms.Button();
            this._In1butUp = new System.Windows.Forms.Button();
            this.fazaIa = new System.Windows.Forms.MaskedTextBox();
            this._In1butDown = new System.Windows.Forms.Button();
            this._fazaAbutDown = new System.Windows.Forms.Button();
            this.In1 = new System.Windows.Forms.MaskedTextBox();
            this._fazaAbutUp = new System.Windows.Forms.Button();
            this._time = new System.Windows.Forms.Label();
            this._statusLedControl = new BEMN.Forms.LedControl();
            this._labelStatus = new System.Windows.Forms.Label();
            this._kvit = new System.Windows.Forms.Button();
            this.kvitTooltip = new System.Windows.Forms.ToolTip(this.components);
            this._exitEmulation = new System.Windows.Forms.CheckBox();
            this._startK1 = new System.Windows.Forms.CheckBox();
            this._startK2 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this._step)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // _startTime
            // 
            this._startTime.AutoSize = true;
            this._startTime.Location = new System.Drawing.Point(16, 55);
            this._startTime.Name = "_startTime";
            this._startTime.Size = new System.Drawing.Size(173, 17);
            this._startTime.TabIndex = 0;
            this._startTime.Text = "Старт по любому изменению";
            this._startTime.UseVisualStyleBackColor = true;
            this._startTime.CheckedChanged += new System.EventHandler(this._startTime_CheckedChanged);
            // 
            // _signal
            // 
            this._signal.FormattingEnabled = true;
            this._signal.Location = new System.Drawing.Point(117, 27);
            this._signal.Name = "_signal";
            this._signal.Size = new System.Drawing.Size(96, 21);
            this._signal.TabIndex = 1;
            this._signal.SelectedIndexChanged += new System.EventHandler(this._signal_SelectedIndexChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(34, 404);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "Шаг  :";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(175, 442);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 33);
            this.button2.TabIndex = 67;
            this.button2.Text = "Прочитать эмуляцию";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.readEmulation_Click);
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(3, 488);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(118, 35);
            this.label32.TabIndex = 68;
            this.label32.Text = "Время с момента подачи воздействия";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(161, 404);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(135, 13);
            this.label33.TabIndex = 70;
            this.label33.Text = "Время режима эмуляции";
            // 
            // _status
            // 
            this._status.Location = new System.Drawing.Point(675, 513);
            this._status.Name = "_status";
            this._status.Size = new System.Drawing.Size(29, 20);
            this._status.TabIndex = 73;
            this._status.Visible = false;
            this._status.TextChanged += new System.EventHandler(this._status_TextChanged);
            // 
            // _step
            // 
            this._step.DecimalPlaces = 2;
            this._step.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this._step.Location = new System.Drawing.Point(77, 404);
            this._step.Name = "_step";
            this._step.Size = new System.Drawing.Size(58, 20);
            this._step.TabIndex = 74;
            this._step.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._timeSignal);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this._startTime);
            this.groupBox5.Controls.Add(this._signal);
            this.groupBox5.Location = new System.Drawing.Point(514, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(230, 530);
            this.groupBox5.TabIndex = 75;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Расчёт времени";
            // 
            // _timeSignal
            // 
            this._timeSignal.AutoSize = true;
            this._timeSignal.Location = new System.Drawing.Point(154, 488);
            this._timeSignal.Name = "_timeSignal";
            this._timeSignal.Size = new System.Drawing.Size(0, 13);
            this._timeSignal.TabIndex = 78;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(13, 23);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(104, 29);
            this.label35.TabIndex = 77;
            this.label35.Text = "Сигнал окончания расчёта времени";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._startK1);
            this.groupBox8.Controls.Add(this._startK2);
            this.groupBox8.Controls.Add(this.checkBox38);
            this.groupBox8.Controls.Add(this.checkBox35);
            this.groupBox8.Controls.Add(this.checkBox1);
            this.groupBox8.Controls.Add(this.checkBox34);
            this.groupBox8.Controls.Add(this.checkBox2);
            this.groupBox8.Controls.Add(this.checkBox36);
            this.groupBox8.Controls.Add(this.checkBox3);
            this.groupBox8.Controls.Add(this.checkBox33);
            this.groupBox8.Controls.Add(this.checkBox4);
            this.groupBox8.Controls.Add(this.checkBox37);
            this.groupBox8.Controls.Add(this.checkBox5);
            this.groupBox8.Controls.Add(this.checkBox32);
            this.groupBox8.Controls.Add(this.checkBox6);
            this.groupBox8.Controls.Add(this.checkBox31);
            this.groupBox8.Controls.Add(this.checkBox7);
            this.groupBox8.Controls.Add(this.checkBox39);
            this.groupBox8.Controls.Add(this.checkBox8);
            this.groupBox8.Controls.Add(this.checkBox30);
            this.groupBox8.Controls.Add(this.checkBox9);
            this.groupBox8.Controls.Add(this.checkBox40);
            this.groupBox8.Controls.Add(this.checkBox10);
            this.groupBox8.Controls.Add(this.checkBox29);
            this.groupBox8.Controls.Add(this.checkBox11);
            this.groupBox8.Controls.Add(this.checkBox28);
            this.groupBox8.Controls.Add(this.checkBox12);
            this.groupBox8.Controls.Add(this.checkBox27);
            this.groupBox8.Controls.Add(this.checkBox13);
            this.groupBox8.Controls.Add(this.checkBox26);
            this.groupBox8.Controls.Add(this.checkBox14);
            this.groupBox8.Controls.Add(this.checkBox25);
            this.groupBox8.Controls.Add(this.checkBox15);
            this.groupBox8.Controls.Add(this.checkBox24);
            this.groupBox8.Controls.Add(this.checkBox16);
            this.groupBox8.Controls.Add(this.checkBox23);
            this.groupBox8.Controls.Add(this.checkBox17);
            this.groupBox8.Controls.Add(this.checkBox22);
            this.groupBox8.Controls.Add(this.checkBox18);
            this.groupBox8.Controls.Add(this.checkBox21);
            this.groupBox8.Controls.Add(this.checkBox19);
            this.groupBox8.Controls.Add(this.checkBox20);
            this.groupBox8.Location = new System.Drawing.Point(5, 284);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(205, 201);
            this.groupBox8.TabIndex = 76;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Дискреты";
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.Location = new System.Drawing.Point(13, 51);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(41, 17);
            this.checkBox38.TabIndex = 42;
            this.checkBox38.Text = "Д3";
            this.checkBox38.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Location = new System.Drawing.Point(13, 100);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(41, 17);
            this.checkBox35.TabIndex = 45;
            this.checkBox35.Text = "Д6";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(151, 163);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(47, 17);
            this.checkBox1.TabIndex = 79;
            this.checkBox1.Text = "Д40";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Location = new System.Drawing.Point(13, 116);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(41, 17);
            this.checkBox34.TabIndex = 46;
            this.checkBox34.Text = "Д7";
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(151, 147);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(47, 17);
            this.checkBox2.TabIndex = 78;
            this.checkBox2.Text = "Д39";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.Location = new System.Drawing.Point(13, 84);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(41, 17);
            this.checkBox36.TabIndex = 44;
            this.checkBox36.Text = "Д5";
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(151, 131);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(47, 17);
            this.checkBox3.TabIndex = 77;
            this.checkBox3.Text = "Д38";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Location = new System.Drawing.Point(13, 132);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(41, 17);
            this.checkBox33.TabIndex = 47;
            this.checkBox33.Text = "Д8";
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(151, 115);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(47, 17);
            this.checkBox4.TabIndex = 76;
            this.checkBox4.Text = "Д37";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.Location = new System.Drawing.Point(13, 68);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(41, 17);
            this.checkBox37.TabIndex = 43;
            this.checkBox37.Text = "Д4";
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(151, 99);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(47, 17);
            this.checkBox5.TabIndex = 75;
            this.checkBox5.Text = "Д36";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Location = new System.Drawing.Point(13, 148);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(41, 17);
            this.checkBox32.TabIndex = 48;
            this.checkBox32.Text = "Д9";
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(151, 83);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(47, 17);
            this.checkBox6.TabIndex = 74;
            this.checkBox6.Text = "Д35";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Location = new System.Drawing.Point(13, 164);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(47, 17);
            this.checkBox31.TabIndex = 49;
            this.checkBox31.Text = "Д10";
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(151, 67);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(47, 17);
            this.checkBox7.TabIndex = 73;
            this.checkBox7.Text = "Д34";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.Location = new System.Drawing.Point(13, 35);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(41, 17);
            this.checkBox39.TabIndex = 41;
            this.checkBox39.Text = "Д2";
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(151, 51);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(47, 17);
            this.checkBox8.TabIndex = 72;
            this.checkBox8.Text = "Д33";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(61, 19);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(47, 17);
            this.checkBox30.TabIndex = 50;
            this.checkBox30.Text = "Д11";
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(152, 35);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(47, 17);
            this.checkBox9.TabIndex = 71;
            this.checkBox9.Text = "Д32";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.Location = new System.Drawing.Point(13, 19);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(41, 17);
            this.checkBox40.TabIndex = 40;
            this.checkBox40.Text = "Д1";
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(152, 19);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(47, 17);
            this.checkBox10.TabIndex = 70;
            this.checkBox10.Text = "Д31";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(61, 35);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(47, 17);
            this.checkBox29.TabIndex = 51;
            this.checkBox29.Text = "Д12";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(107, 163);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(47, 17);
            this.checkBox11.TabIndex = 69;
            this.checkBox11.Text = "Д30";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(60, 51);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(47, 17);
            this.checkBox28.TabIndex = 52;
            this.checkBox28.Text = "Д13";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(107, 147);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(47, 17);
            this.checkBox12.TabIndex = 68;
            this.checkBox12.Text = "Д29";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(60, 67);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(47, 17);
            this.checkBox27.TabIndex = 53;
            this.checkBox27.Text = "Д14";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(107, 131);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(47, 17);
            this.checkBox13.TabIndex = 67;
            this.checkBox13.Text = "Д28";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(60, 83);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(47, 17);
            this.checkBox26.TabIndex = 54;
            this.checkBox26.Text = "Д15";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(107, 115);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(47, 17);
            this.checkBox14.TabIndex = 66;
            this.checkBox14.Text = "Д27";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(60, 99);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(47, 17);
            this.checkBox25.TabIndex = 55;
            this.checkBox25.Text = "Д16";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(107, 99);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(47, 17);
            this.checkBox15.TabIndex = 65;
            this.checkBox15.Text = "Д26";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(60, 115);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(47, 17);
            this.checkBox24.TabIndex = 56;
            this.checkBox24.Text = "Д17";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(107, 83);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(47, 17);
            this.checkBox16.TabIndex = 64;
            this.checkBox16.Text = "Д25";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(60, 131);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(47, 17);
            this.checkBox23.TabIndex = 57;
            this.checkBox23.Text = "Д18";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(107, 67);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(47, 17);
            this.checkBox17.TabIndex = 63;
            this.checkBox17.Text = "Д24";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(60, 147);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(47, 17);
            this.checkBox22.TabIndex = 58;
            this.checkBox22.Text = "Д19";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(107, 51);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(47, 17);
            this.checkBox18.TabIndex = 62;
            this.checkBox18.Text = "Д23";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(60, 164);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(47, 17);
            this.checkBox21.TabIndex = 59;
            this.checkBox21.Text = "Д20";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(108, 35);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(47, 17);
            this.checkBox19.TabIndex = 61;
            this.checkBox19.Text = "Д22";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(108, 19);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(47, 17);
            this.checkBox20.TabIndex = 60;
            this.checkBox20.Text = "Д21";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._startFazaUc);
            this.groupBox7.Controls.Add(this._startUa);
            this.groupBox7.Controls.Add(this._startFUn1);
            this.groupBox7.Controls.Add(this._startUb);
            this.groupBox7.Controls.Add(this._startUc);
            this.groupBox7.Controls.Add(this._startFUn);
            this.groupBox7.Controls.Add(this._startUn);
            this.groupBox7.Controls.Add(this._startUn1);
            this.groupBox7.Controls.Add(this._startFUc);
            this.groupBox7.Controls.Add(this._startFazaUa);
            this.groupBox7.Controls.Add(this._startFazaUb);
            this.groupBox7.Controls.Add(this._startFUb);
            this.groupBox7.Controls.Add(this._startFazaUn);
            this.groupBox7.Controls.Add(this._startFazaUn1);
            this.groupBox7.Controls.Add(this._startFUa);
            this.groupBox7.Location = new System.Drawing.Point(6, 165);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(161, 104);
            this.groupBox7.TabIndex = 76;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Напряжения ";
            // 
            // _startFazaUc
            // 
            this._startFazaUc.AutoSize = true;
            this._startFazaUc.Location = new System.Drawing.Point(65, 51);
            this._startFazaUc.Name = "_startFazaUc";
            this._startFazaUc.Size = new System.Drawing.Size(35, 17);
            this._startFazaUc.TabIndex = 89;
            this._startFazaUc.Text = "fc";
            this._startFazaUc.UseVisualStyleBackColor = true;
            // 
            // _startUa
            // 
            this._startUa.AutoSize = true;
            this._startUa.Location = new System.Drawing.Point(11, 17);
            this._startUa.Name = "_startUa";
            this._startUa.Size = new System.Drawing.Size(40, 17);
            this._startUa.TabIndex = 78;
            this._startUa.Text = "Ua";
            this._startUa.UseVisualStyleBackColor = true;
            // 
            // _startFUn1
            // 
            this._startFUn1.AutoSize = true;
            this._startFUn1.Location = new System.Drawing.Point(112, 84);
            this._startFUn1.Name = "_startFUn1";
            this._startFUn1.Size = new System.Drawing.Size(44, 17);
            this._startFUn1.TabIndex = 100;
            this._startFUn1.Text = "Fn1";
            this._startFUn1.UseVisualStyleBackColor = true;
            // 
            // _startUb
            // 
            this._startUb.AutoSize = true;
            this._startUb.Location = new System.Drawing.Point(11, 34);
            this._startUb.Name = "_startUb";
            this._startUb.Size = new System.Drawing.Size(40, 17);
            this._startUb.TabIndex = 79;
            this._startUb.Text = "Ub";
            this._startUb.UseVisualStyleBackColor = true;
            // 
            // _startUc
            // 
            this._startUc.AutoSize = true;
            this._startUc.Location = new System.Drawing.Point(11, 51);
            this._startUc.Name = "_startUc";
            this._startUc.Size = new System.Drawing.Size(40, 17);
            this._startUc.TabIndex = 80;
            this._startUc.Text = "Uc";
            this._startUc.UseVisualStyleBackColor = true;
            // 
            // _startFUn
            // 
            this._startFUn.AutoSize = true;
            this._startFUn.Location = new System.Drawing.Point(112, 67);
            this._startFUn.Name = "_startFUn";
            this._startFUn.Size = new System.Drawing.Size(38, 17);
            this._startFUn.TabIndex = 99;
            this._startFUn.Text = "Fn";
            this._startFUn.UseVisualStyleBackColor = true;
            // 
            // _startUn
            // 
            this._startUn.AutoSize = true;
            this._startUn.Location = new System.Drawing.Point(11, 67);
            this._startUn.Name = "_startUn";
            this._startUn.Size = new System.Drawing.Size(40, 17);
            this._startUn.TabIndex = 81;
            this._startUn.Text = "Un";
            this._startUn.UseVisualStyleBackColor = true;
            // 
            // _startUn1
            // 
            this._startUn1.AutoSize = true;
            this._startUn1.Location = new System.Drawing.Point(11, 84);
            this._startUn1.Name = "_startUn1";
            this._startUn1.Size = new System.Drawing.Size(46, 17);
            this._startUn1.TabIndex = 82;
            this._startUn1.Text = "Un1";
            this._startUn1.UseVisualStyleBackColor = true;
            // 
            // _startFUc
            // 
            this._startFUc.AutoSize = true;
            this._startFUc.Location = new System.Drawing.Point(112, 51);
            this._startFUc.Name = "_startFUc";
            this._startFUc.Size = new System.Drawing.Size(38, 17);
            this._startFUc.TabIndex = 98;
            this._startFUc.Text = "Fc";
            this._startFUc.UseVisualStyleBackColor = true;
            // 
            // _startFazaUa
            // 
            this._startFazaUa.AutoSize = true;
            this._startFazaUa.Location = new System.Drawing.Point(65, 17);
            this._startFazaUa.Name = "_startFazaUa";
            this._startFazaUa.Size = new System.Drawing.Size(35, 17);
            this._startFazaUa.TabIndex = 87;
            this._startFazaUa.Text = "fa";
            this._startFazaUa.UseVisualStyleBackColor = true;
            // 
            // _startFazaUb
            // 
            this._startFazaUb.AutoSize = true;
            this._startFazaUb.Location = new System.Drawing.Point(65, 34);
            this._startFazaUb.Name = "_startFazaUb";
            this._startFazaUb.Size = new System.Drawing.Size(35, 17);
            this._startFazaUb.TabIndex = 88;
            this._startFazaUb.Text = "fb";
            this._startFazaUb.UseVisualStyleBackColor = true;
            // 
            // _startFUb
            // 
            this._startFUb.AutoSize = true;
            this._startFUb.Location = new System.Drawing.Point(112, 34);
            this._startFUb.Name = "_startFUb";
            this._startFUb.Size = new System.Drawing.Size(38, 17);
            this._startFUb.TabIndex = 97;
            this._startFUb.Text = "Fb";
            this._startFUb.UseVisualStyleBackColor = true;
            // 
            // _startFazaUn
            // 
            this._startFazaUn.AutoSize = true;
            this._startFazaUn.Location = new System.Drawing.Point(65, 67);
            this._startFazaUn.Name = "_startFazaUn";
            this._startFazaUn.Size = new System.Drawing.Size(35, 17);
            this._startFazaUn.TabIndex = 90;
            this._startFazaUn.Text = "fn";
            this._startFazaUn.UseVisualStyleBackColor = true;
            // 
            // _startFazaUn1
            // 
            this._startFazaUn1.AutoSize = true;
            this._startFazaUn1.Location = new System.Drawing.Point(65, 84);
            this._startFazaUn1.Name = "_startFazaUn1";
            this._startFazaUn1.Size = new System.Drawing.Size(41, 17);
            this._startFazaUn1.TabIndex = 91;
            this._startFazaUn1.Text = "fn1";
            this._startFazaUn1.UseVisualStyleBackColor = true;
            // 
            // _startFUa
            // 
            this._startFUa.AutoSize = true;
            this._startFUa.Location = new System.Drawing.Point(112, 17);
            this._startFUa.Name = "_startFUa";
            this._startFUa.Size = new System.Drawing.Size(38, 17);
            this._startFUa.TabIndex = 96;
            this._startFUa.Text = "Fa";
            this._startFUa.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this._startFazaN1);
            this.groupBox6.Controls.Add(this._startIn1);
            this.groupBox6.Controls.Add(this._startFn1);
            this.groupBox6.Controls.Add(this._startFazaB);
            this.groupBox6.Controls.Add(this._startFazaN);
            this.groupBox6.Controls.Add(this._startFazaC);
            this.groupBox6.Controls.Add(this._startFazaA);
            this.groupBox6.Controls.Add(this._startIn);
            this.groupBox6.Controls.Add(this._startIc);
            this.groupBox6.Controls.Add(this._startFa);
            this.groupBox6.Controls.Add(this._startIa);
            this.groupBox6.Controls.Add(this._startIb);
            this.groupBox6.Controls.Add(this._startFb);
            this.groupBox6.Controls.Add(this._startFc);
            this.groupBox6.Controls.Add(this._startFn);
            this.groupBox6.Location = new System.Drawing.Point(5, 78);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(162, 81);
            this.groupBox6.TabIndex = 76;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Токи";
            // 
            // _startFazaN1
            // 
            this._startFazaN1.AutoSize = true;
            this._startFazaN1.Location = new System.Drawing.Point(65, 87);
            this._startFazaN1.Name = "_startFazaN1";
            this._startFazaN1.Size = new System.Drawing.Size(41, 17);
            this._startFazaN1.TabIndex = 97;
            this._startFazaN1.Text = "fn1";
            this._startFazaN1.UseVisualStyleBackColor = true;
            // 
            // _startIn1
            // 
            this._startIn1.AutoSize = true;
            this._startIn1.Location = new System.Drawing.Point(11, 87);
            this._startIn1.Name = "_startIn1";
            this._startIn1.Size = new System.Drawing.Size(41, 17);
            this._startIn1.TabIndex = 96;
            this._startIn1.Text = "In1";
            this._startIn1.UseVisualStyleBackColor = true;
            // 
            // _startFn1
            // 
            this._startFn1.AutoSize = true;
            this._startFn1.Location = new System.Drawing.Point(112, 87);
            this._startFn1.Name = "_startFn1";
            this._startFn1.Size = new System.Drawing.Size(44, 17);
            this._startFn1.TabIndex = 98;
            this._startFn1.Text = "Fn1";
            this._startFn1.UseVisualStyleBackColor = true;
            // 
            // _startFazaB
            // 
            this._startFazaB.AutoSize = true;
            this._startFazaB.Location = new System.Drawing.Point(65, 37);
            this._startFazaB.Name = "_startFazaB";
            this._startFazaB.Size = new System.Drawing.Size(35, 17);
            this._startFazaB.TabIndex = 84;
            this._startFazaB.Text = "fb";
            this._startFazaB.UseVisualStyleBackColor = true;
            // 
            // _startFazaN
            // 
            this._startFazaN.AutoSize = true;
            this._startFazaN.Location = new System.Drawing.Point(65, 71);
            this._startFazaN.Name = "_startFazaN";
            this._startFazaN.Size = new System.Drawing.Size(35, 17);
            this._startFazaN.TabIndex = 86;
            this._startFazaN.Text = "fn";
            this._startFazaN.UseVisualStyleBackColor = true;
            this._startFazaN.Visible = false;
            // 
            // _startFazaC
            // 
            this._startFazaC.AutoSize = true;
            this._startFazaC.Location = new System.Drawing.Point(65, 54);
            this._startFazaC.Name = "_startFazaC";
            this._startFazaC.Size = new System.Drawing.Size(35, 17);
            this._startFazaC.TabIndex = 85;
            this._startFazaC.Text = "fc";
            this._startFazaC.UseVisualStyleBackColor = true;
            // 
            // _startFazaA
            // 
            this._startFazaA.AutoSize = true;
            this._startFazaA.Location = new System.Drawing.Point(65, 20);
            this._startFazaA.Name = "_startFazaA";
            this._startFazaA.Size = new System.Drawing.Size(35, 17);
            this._startFazaA.TabIndex = 83;
            this._startFazaA.Text = "fa";
            this._startFazaA.UseVisualStyleBackColor = true;
            // 
            // _startIn
            // 
            this._startIn.AutoSize = true;
            this._startIn.Location = new System.Drawing.Point(11, 71);
            this._startIn.Name = "_startIn";
            this._startIn.Size = new System.Drawing.Size(35, 17);
            this._startIn.TabIndex = 77;
            this._startIn.Text = "In";
            this._startIn.UseVisualStyleBackColor = true;
            this._startIn.Visible = false;
            // 
            // _startIc
            // 
            this._startIc.AutoSize = true;
            this._startIc.Location = new System.Drawing.Point(11, 54);
            this._startIc.Name = "_startIc";
            this._startIc.Size = new System.Drawing.Size(35, 17);
            this._startIc.TabIndex = 76;
            this._startIc.Text = "Ic";
            this._startIc.UseVisualStyleBackColor = true;
            // 
            // _startFa
            // 
            this._startFa.AutoSize = true;
            this._startFa.Location = new System.Drawing.Point(112, 20);
            this._startFa.Name = "_startFa";
            this._startFa.Size = new System.Drawing.Size(38, 17);
            this._startFa.TabIndex = 92;
            this._startFa.Text = "Fa";
            this._startFa.UseVisualStyleBackColor = true;
            // 
            // _startIa
            // 
            this._startIa.AutoSize = true;
            this._startIa.Location = new System.Drawing.Point(11, 20);
            this._startIa.Name = "_startIa";
            this._startIa.Size = new System.Drawing.Size(35, 17);
            this._startIa.TabIndex = 0;
            this._startIa.Text = "Ia";
            this._startIa.UseVisualStyleBackColor = true;
            // 
            // _startIb
            // 
            this._startIb.AutoSize = true;
            this._startIb.Location = new System.Drawing.Point(11, 37);
            this._startIb.Name = "_startIb";
            this._startIb.Size = new System.Drawing.Size(35, 17);
            this._startIb.TabIndex = 1;
            this._startIb.Text = "Ib";
            this._startIb.UseVisualStyleBackColor = true;
            // 
            // _startFb
            // 
            this._startFb.AutoSize = true;
            this._startFb.Location = new System.Drawing.Point(112, 37);
            this._startFb.Name = "_startFb";
            this._startFb.Size = new System.Drawing.Size(38, 17);
            this._startFb.TabIndex = 93;
            this._startFb.Text = "Fb";
            this._startFb.UseVisualStyleBackColor = true;
            // 
            // _startFc
            // 
            this._startFc.AutoSize = true;
            this._startFc.Location = new System.Drawing.Point(112, 54);
            this._startFc.Name = "_startFc";
            this._startFc.Size = new System.Drawing.Size(38, 17);
            this._startFc.TabIndex = 94;
            this._startFc.Text = "Fc";
            this._startFc.UseVisualStyleBackColor = true;
            // 
            // _startFn
            // 
            this._startFn.AutoSize = true;
            this._startFn.Location = new System.Drawing.Point(112, 71);
            this._startFn.Name = "_startFn";
            this._startFn.Size = new System.Drawing.Size(38, 17);
            this._startFn.TabIndex = 95;
            this._startFn.Text = "Fn";
            this._startFn.UseVisualStyleBackColor = true;
            this._startFn.Visible = false;
            // 
            // _writeEmulButton
            // 
            this._writeEmulButton.Appearance = System.Windows.Forms.Appearance.Button;
            this._writeEmulButton.BackColor = System.Drawing.SystemColors.Control;
            this._writeEmulButton.Location = new System.Drawing.Point(36, 442);
            this._writeEmulButton.Name = "_writeEmulButton";
            this._writeEmulButton.Size = new System.Drawing.Size(121, 33);
            this._writeEmulButton.TabIndex = 77;
            this._writeEmulButton.Text = "Записать эмуляцию ";
            this._writeEmulButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._writeEmulButton.UseVisualStyleBackColor = false;
            this._writeEmulButton.CheckedChanged += new System.EventHandler(this._writeEmulButton_CheckedChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(295, 404);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 13);
            this.label34.TabIndex = 74;
            this.label34.Text = "(мин:сек,мс)";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.groupBox3);
            this.groupBox9.Controls.Add(this.groupBox2);
            this.groupBox9.Controls.Add(this.groupBox4);
            this.groupBox9.Location = new System.Drawing.Point(12, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(496, 398);
            this.groupBox9.TabIndex = 78;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Входные сигналы";
            this.groupBox9.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.FUn1);
            this.groupBox3.Controls.Add(this.Ua);
            this.groupBox3.Controls.Add(this._fazaUabutUp);
            this.groupBox3.Controls.Add(this._FUn1butDown);
            this.groupBox3.Controls.Add(this.Un1);
            this.groupBox3.Controls.Add(this._fazaUabutDown);
            this.groupBox3.Controls.Add(this._FUn1butUp);
            this.groupBox3.Controls.Add(this._Un1butDown);
            this.groupBox3.Controls.Add(this.fazaUa);
            this.groupBox3.Controls.Add(this.FUn);
            this.groupBox3.Controls.Add(this._Un1butUp);
            this.groupBox3.Controls.Add(this._fazaUbbutUp);
            this.groupBox3.Controls.Add(this._FUnbutDown);
            this.groupBox3.Controls.Add(this.Un);
            this.groupBox3.Controls.Add(this._fazaUbbutDown);
            this.groupBox3.Controls.Add(this._FUnbutUp);
            this.groupBox3.Controls.Add(this._UnbutDown);
            this.groupBox3.Controls.Add(this.fazaUb);
            this.groupBox3.Controls.Add(this.FUc);
            this.groupBox3.Controls.Add(this._UnbutUp);
            this.groupBox3.Controls.Add(this._fazaUcbutUp);
            this.groupBox3.Controls.Add(this._FUcbutDown);
            this.groupBox3.Controls.Add(this.Uc);
            this.groupBox3.Controls.Add(this._fazaUcbutDown);
            this.groupBox3.Controls.Add(this._FUcbutUp);
            this.groupBox3.Controls.Add(this._UcbutDown);
            this.groupBox3.Controls.Add(this.fazaUc);
            this.groupBox3.Controls.Add(this.FUb);
            this.groupBox3.Controls.Add(this._UcbutUp);
            this.groupBox3.Controls.Add(this._fazaUnbutUp);
            this.groupBox3.Controls.Add(this._FUbbutDown);
            this.groupBox3.Controls.Add(this.Ub);
            this.groupBox3.Controls.Add(this._fazaUnbutDown);
            this.groupBox3.Controls.Add(this._FUbbutUp);
            this.groupBox3.Controls.Add(this._UbbutDown);
            this.groupBox3.Controls.Add(this.fazaUn);
            this.groupBox3.Controls.Add(this.FUa);
            this.groupBox3.Controls.Add(this._UbbutUp);
            this.groupBox3.Controls.Add(this._fazaUn1butUp);
            this.groupBox3.Controls.Add(this._FUabutDown);
            this.groupBox3.Controls.Add(this._UabutDown);
            this.groupBox3.Controls.Add(this._fazaUn1butDown);
            this.groupBox3.Controls.Add(this._FUabutUp);
            this.groupBox3.Controls.Add(this._UabutUp);
            this.groupBox3.Controls.Add(this.fazaUn1);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Location = new System.Drawing.Point(12, 151);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(356, 197);
            this.groupBox3.TabIndex = 197;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Напряжения";
            // 
            // FUn1
            // 
            this.FUn1.Location = new System.Drawing.Point(274, 161);
            this.FUn1.Name = "FUn1";
            this.FUn1.Size = new System.Drawing.Size(53, 20);
            this.FUn1.TabIndex = 210;
            this.FUn1.Text = "50";
            // 
            // Ua
            // 
            this.Ua.Location = new System.Drawing.Point(53, 19);
            this.Ua.Name = "Ua";
            this.Ua.Size = new System.Drawing.Size(53, 20);
            this.Ua.TabIndex = 138;
            this.Ua.Tag = "U";
            // 
            // _fazaUabutUp
            // 
            this._fazaUabutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUabutUp.Location = new System.Drawing.Point(210, 17);
            this._fazaUabutUp.Name = "_fazaUabutUp";
            this._fazaUabutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUabutUp.TabIndex = 166;
            this._fazaUabutUp.Text = "▲";
            this._fazaUabutUp.UseMnemonic = false;
            this._fazaUabutUp.UseVisualStyleBackColor = true;
            this._fazaUabutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FUn1butDown
            // 
            this._FUn1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._FUn1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUn1butDown.Location = new System.Drawing.Point(325, 170);
            this._FUn1butDown.Name = "_FUn1butDown";
            this._FUn1butDown.Size = new System.Drawing.Size(22, 14);
            this._FUn1butDown.TabIndex = 209;
            this._FUn1butDown.Text = "\t▼";
            this._FUn1butDown.UseVisualStyleBackColor = false;
            this._FUn1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Un1
            // 
            this.Un1.Location = new System.Drawing.Point(53, 161);
            this.Un1.Name = "Un1";
            this.Un1.Size = new System.Drawing.Size(53, 20);
            this.Un1.TabIndex = 150;
            this.Un1.Tag = "U";
            // 
            // _fazaUabutDown
            // 
            this._fazaUabutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaUabutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUabutDown.Location = new System.Drawing.Point(210, 29);
            this._fazaUabutDown.Name = "_fazaUabutDown";
            this._fazaUabutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaUabutDown.TabIndex = 167;
            this._fazaUabutDown.Text = "\t▼";
            this._fazaUabutDown.UseVisualStyleBackColor = false;
            this._fazaUabutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FUn1butUp
            // 
            this._FUn1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUn1butUp.Location = new System.Drawing.Point(325, 159);
            this._FUn1butUp.Name = "_FUn1butUp";
            this._FUn1butUp.Size = new System.Drawing.Size(22, 14);
            this._FUn1butUp.TabIndex = 208;
            this._FUn1butUp.Text = "▲";
            this._FUn1butUp.UseMnemonic = false;
            this._FUn1butUp.UseVisualStyleBackColor = true;
            this._FUn1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _Un1butDown
            // 
            this._Un1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._Un1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._Un1butDown.Location = new System.Drawing.Point(104, 171);
            this._Un1butDown.Name = "_Un1butDown";
            this._Un1butDown.Size = new System.Drawing.Size(22, 14);
            this._Un1butDown.TabIndex = 149;
            this._Un1butDown.Text = "\t▼";
            this._Un1butDown.UseVisualStyleBackColor = false;
            this._Un1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // fazaUa
            // 
            this.fazaUa.Location = new System.Drawing.Point(159, 19);
            this.fazaUa.Name = "fazaUa";
            this.fazaUa.Size = new System.Drawing.Size(53, 20);
            this.fazaUa.TabIndex = 168;
            this.fazaUa.Tag = "";
            // 
            // FUn
            // 
            this.FUn.Location = new System.Drawing.Point(274, 129);
            this.FUn.Name = "FUn";
            this.FUn.Size = new System.Drawing.Size(53, 20);
            this.FUn.TabIndex = 207;
            this.FUn.Text = "50";
            // 
            // _Un1butUp
            // 
            this._Un1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._Un1butUp.Location = new System.Drawing.Point(104, 159);
            this._Un1butUp.Name = "_Un1butUp";
            this._Un1butUp.Size = new System.Drawing.Size(22, 14);
            this._Un1butUp.TabIndex = 148;
            this._Un1butUp.Text = "▲";
            this._Un1butUp.UseMnemonic = false;
            this._Un1butUp.UseVisualStyleBackColor = true;
            this._Un1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _fazaUbbutUp
            // 
            this._fazaUbbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUbbutUp.Location = new System.Drawing.Point(210, 55);
            this._fazaUbbutUp.Name = "_fazaUbbutUp";
            this._fazaUbbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUbbutUp.TabIndex = 169;
            this._fazaUbbutUp.Text = "▲";
            this._fazaUbbutUp.UseMnemonic = false;
            this._fazaUbbutUp.UseVisualStyleBackColor = true;
            this._fazaUbbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FUnbutDown
            // 
            this._FUnbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FUnbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUnbutDown.Location = new System.Drawing.Point(325, 139);
            this._FUnbutDown.Name = "_FUnbutDown";
            this._FUnbutDown.Size = new System.Drawing.Size(22, 14);
            this._FUnbutDown.TabIndex = 206;
            this._FUnbutDown.Text = "\t▼";
            this._FUnbutDown.UseVisualStyleBackColor = false;
            this._FUnbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Un
            // 
            this.Un.Location = new System.Drawing.Point(53, 129);
            this.Un.Name = "Un";
            this.Un.Size = new System.Drawing.Size(53, 20);
            this.Un.TabIndex = 147;
            this.Un.Tag = "U";
            // 
            // _fazaUbbutDown
            // 
            this._fazaUbbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaUbbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUbbutDown.Location = new System.Drawing.Point(210, 67);
            this._fazaUbbutDown.Name = "_fazaUbbutDown";
            this._fazaUbbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaUbbutDown.TabIndex = 170;
            this._fazaUbbutDown.Text = "\t▼";
            this._fazaUbbutDown.UseVisualStyleBackColor = false;
            this._fazaUbbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FUnbutUp
            // 
            this._FUnbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUnbutUp.Location = new System.Drawing.Point(325, 127);
            this._FUnbutUp.Name = "_FUnbutUp";
            this._FUnbutUp.Size = new System.Drawing.Size(22, 14);
            this._FUnbutUp.TabIndex = 205;
            this._FUnbutUp.Text = "▲";
            this._FUnbutUp.UseMnemonic = false;
            this._FUnbutUp.UseVisualStyleBackColor = true;
            this._FUnbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _UnbutDown
            // 
            this._UnbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._UnbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UnbutDown.Location = new System.Drawing.Point(104, 139);
            this._UnbutDown.Name = "_UnbutDown";
            this._UnbutDown.Size = new System.Drawing.Size(22, 14);
            this._UnbutDown.TabIndex = 146;
            this._UnbutDown.Text = "\t▼";
            this._UnbutDown.UseVisualStyleBackColor = false;
            this._UnbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // fazaUb
            // 
            this.fazaUb.Location = new System.Drawing.Point(159, 57);
            this.fazaUb.Name = "fazaUb";
            this.fazaUb.Size = new System.Drawing.Size(53, 20);
            this.fazaUb.TabIndex = 171;
            this.fazaUb.Tag = "";
            // 
            // FUc
            // 
            this.FUc.Location = new System.Drawing.Point(274, 94);
            this.FUc.Name = "FUc";
            this.FUc.Size = new System.Drawing.Size(53, 20);
            this.FUc.TabIndex = 204;
            this.FUc.Text = "50";
            // 
            // _UnbutUp
            // 
            this._UnbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UnbutUp.Location = new System.Drawing.Point(104, 127);
            this._UnbutUp.Name = "_UnbutUp";
            this._UnbutUp.Size = new System.Drawing.Size(22, 14);
            this._UnbutUp.TabIndex = 145;
            this._UnbutUp.Text = "▲";
            this._UnbutUp.UseMnemonic = false;
            this._UnbutUp.UseVisualStyleBackColor = true;
            this._UnbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _fazaUcbutUp
            // 
            this._fazaUcbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUcbutUp.Location = new System.Drawing.Point(210, 92);
            this._fazaUcbutUp.Name = "_fazaUcbutUp";
            this._fazaUcbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUcbutUp.TabIndex = 172;
            this._fazaUcbutUp.Text = "▲";
            this._fazaUcbutUp.UseMnemonic = false;
            this._fazaUcbutUp.UseVisualStyleBackColor = true;
            this._fazaUcbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FUcbutDown
            // 
            this._FUcbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FUcbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUcbutDown.Location = new System.Drawing.Point(325, 104);
            this._FUcbutDown.Name = "_FUcbutDown";
            this._FUcbutDown.Size = new System.Drawing.Size(22, 14);
            this._FUcbutDown.TabIndex = 203;
            this._FUcbutDown.Text = "\t▼";
            this._FUcbutDown.UseVisualStyleBackColor = false;
            this._FUcbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Uc
            // 
            this.Uc.Location = new System.Drawing.Point(53, 94);
            this.Uc.Name = "Uc";
            this.Uc.Size = new System.Drawing.Size(53, 20);
            this.Uc.TabIndex = 144;
            this.Uc.Tag = "U";
            // 
            // _fazaUcbutDown
            // 
            this._fazaUcbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaUcbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUcbutDown.Location = new System.Drawing.Point(210, 104);
            this._fazaUcbutDown.Name = "_fazaUcbutDown";
            this._fazaUcbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaUcbutDown.TabIndex = 173;
            this._fazaUcbutDown.Text = "\t▼";
            this._fazaUcbutDown.UseVisualStyleBackColor = false;
            this._fazaUcbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FUcbutUp
            // 
            this._FUcbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUcbutUp.Location = new System.Drawing.Point(325, 92);
            this._FUcbutUp.Name = "_FUcbutUp";
            this._FUcbutUp.Size = new System.Drawing.Size(22, 14);
            this._FUcbutUp.TabIndex = 202;
            this._FUcbutUp.Text = "▲";
            this._FUcbutUp.UseMnemonic = false;
            this._FUcbutUp.UseVisualStyleBackColor = true;
            this._FUcbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _UcbutDown
            // 
            this._UcbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._UcbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UcbutDown.Location = new System.Drawing.Point(104, 104);
            this._UcbutDown.Name = "_UcbutDown";
            this._UcbutDown.Size = new System.Drawing.Size(22, 14);
            this._UcbutDown.TabIndex = 143;
            this._UcbutDown.Text = "\t▼";
            this._UcbutDown.UseVisualStyleBackColor = false;
            this._UcbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // fazaUc
            // 
            this.fazaUc.Location = new System.Drawing.Point(159, 94);
            this.fazaUc.Name = "fazaUc";
            this.fazaUc.Size = new System.Drawing.Size(53, 20);
            this.fazaUc.TabIndex = 174;
            this.fazaUc.Tag = "";
            // 
            // FUb
            // 
            this.FUb.Location = new System.Drawing.Point(274, 57);
            this.FUb.Name = "FUb";
            this.FUb.Size = new System.Drawing.Size(53, 20);
            this.FUb.TabIndex = 201;
            this.FUb.Text = "50";
            // 
            // _UcbutUp
            // 
            this._UcbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UcbutUp.Location = new System.Drawing.Point(104, 92);
            this._UcbutUp.Name = "_UcbutUp";
            this._UcbutUp.Size = new System.Drawing.Size(22, 14);
            this._UcbutUp.TabIndex = 142;
            this._UcbutUp.Text = "▲";
            this._UcbutUp.UseMnemonic = false;
            this._UcbutUp.UseVisualStyleBackColor = true;
            this._UcbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _fazaUnbutUp
            // 
            this._fazaUnbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUnbutUp.Location = new System.Drawing.Point(210, 127);
            this._fazaUnbutUp.Name = "_fazaUnbutUp";
            this._fazaUnbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUnbutUp.TabIndex = 175;
            this._fazaUnbutUp.Text = "▲";
            this._fazaUnbutUp.UseMnemonic = false;
            this._fazaUnbutUp.UseVisualStyleBackColor = true;
            this._fazaUnbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FUbbutDown
            // 
            this._FUbbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FUbbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUbbutDown.Location = new System.Drawing.Point(325, 67);
            this._FUbbutDown.Name = "_FUbbutDown";
            this._FUbbutDown.Size = new System.Drawing.Size(22, 14);
            this._FUbbutDown.TabIndex = 200;
            this._FUbbutDown.Text = "\t▼";
            this._FUbbutDown.UseVisualStyleBackColor = false;
            this._FUbbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Ub
            // 
            this.Ub.Location = new System.Drawing.Point(53, 57);
            this.Ub.Name = "Ub";
            this.Ub.Size = new System.Drawing.Size(53, 20);
            this.Ub.TabIndex = 141;
            this.Ub.Tag = "U";
            // 
            // _fazaUnbutDown
            // 
            this._fazaUnbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaUnbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUnbutDown.Location = new System.Drawing.Point(210, 139);
            this._fazaUnbutDown.Name = "_fazaUnbutDown";
            this._fazaUnbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaUnbutDown.TabIndex = 176;
            this._fazaUnbutDown.Text = "\t▼";
            this._fazaUnbutDown.UseVisualStyleBackColor = false;
            this._fazaUnbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FUbbutUp
            // 
            this._FUbbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUbbutUp.Location = new System.Drawing.Point(325, 55);
            this._FUbbutUp.Name = "_FUbbutUp";
            this._FUbbutUp.Size = new System.Drawing.Size(22, 14);
            this._FUbbutUp.TabIndex = 199;
            this._FUbbutUp.Text = "▲";
            this._FUbbutUp.UseMnemonic = false;
            this._FUbbutUp.UseVisualStyleBackColor = true;
            this._FUbbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _UbbutDown
            // 
            this._UbbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._UbbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UbbutDown.Location = new System.Drawing.Point(104, 67);
            this._UbbutDown.Name = "_UbbutDown";
            this._UbbutDown.Size = new System.Drawing.Size(22, 14);
            this._UbbutDown.TabIndex = 140;
            this._UbbutDown.Text = "\t▼";
            this._UbbutDown.UseVisualStyleBackColor = false;
            this._UbbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // fazaUn
            // 
            this.fazaUn.Location = new System.Drawing.Point(159, 129);
            this.fazaUn.Name = "fazaUn";
            this.fazaUn.Size = new System.Drawing.Size(53, 20);
            this.fazaUn.TabIndex = 177;
            this.fazaUn.Tag = "";
            // 
            // FUa
            // 
            this.FUa.Location = new System.Drawing.Point(274, 19);
            this.FUa.Name = "FUa";
            this.FUa.Size = new System.Drawing.Size(53, 20);
            this.FUa.TabIndex = 198;
            this.FUa.Text = "50";
            // 
            // _UbbutUp
            // 
            this._UbbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UbbutUp.Location = new System.Drawing.Point(104, 55);
            this._UbbutUp.Name = "_UbbutUp";
            this._UbbutUp.Size = new System.Drawing.Size(22, 14);
            this._UbbutUp.TabIndex = 139;
            this._UbbutUp.Text = "▲";
            this._UbbutUp.UseMnemonic = false;
            this._UbbutUp.UseVisualStyleBackColor = true;
            this._UbbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _fazaUn1butUp
            // 
            this._fazaUn1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUn1butUp.Location = new System.Drawing.Point(210, 159);
            this._fazaUn1butUp.Name = "_fazaUn1butUp";
            this._fazaUn1butUp.Size = new System.Drawing.Size(22, 14);
            this._fazaUn1butUp.TabIndex = 178;
            this._fazaUn1butUp.Text = "▲";
            this._fazaUn1butUp.UseMnemonic = false;
            this._fazaUn1butUp.UseVisualStyleBackColor = true;
            this._fazaUn1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FUabutDown
            // 
            this._FUabutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FUabutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUabutDown.Location = new System.Drawing.Point(325, 29);
            this._FUabutDown.Name = "_FUabutDown";
            this._FUabutDown.Size = new System.Drawing.Size(22, 14);
            this._FUabutDown.TabIndex = 197;
            this._FUabutDown.Text = "\t▼";
            this._FUabutDown.UseVisualStyleBackColor = false;
            this._FUabutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _UabutDown
            // 
            this._UabutDown.BackColor = System.Drawing.SystemColors.Control;
            this._UabutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UabutDown.Location = new System.Drawing.Point(104, 29);
            this._UabutDown.Name = "_UabutDown";
            this._UabutDown.Size = new System.Drawing.Size(22, 14);
            this._UabutDown.TabIndex = 137;
            this._UabutDown.Text = "\t▼";
            this._UabutDown.UseVisualStyleBackColor = false;
            this._UabutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _fazaUn1butDown
            // 
            this._fazaUn1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaUn1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaUn1butDown.Location = new System.Drawing.Point(210, 171);
            this._fazaUn1butDown.Name = "_fazaUn1butDown";
            this._fazaUn1butDown.Size = new System.Drawing.Size(22, 14);
            this._fazaUn1butDown.TabIndex = 179;
            this._fazaUn1butDown.Text = "\t▼";
            this._fazaUn1butDown.UseVisualStyleBackColor = false;
            this._fazaUn1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FUabutUp
            // 
            this._FUabutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FUabutUp.Location = new System.Drawing.Point(325, 17);
            this._FUabutUp.Name = "_FUabutUp";
            this._FUabutUp.Size = new System.Drawing.Size(22, 14);
            this._FUabutUp.TabIndex = 196;
            this._FUabutUp.Text = "▲";
            this._FUabutUp.UseMnemonic = false;
            this._FUabutUp.UseVisualStyleBackColor = true;
            this._FUabutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _UabutUp
            // 
            this._UabutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._UabutUp.Location = new System.Drawing.Point(104, 17);
            this._UabutUp.Name = "_UabutUp";
            this._UabutUp.Size = new System.Drawing.Size(22, 14);
            this._UabutUp.TabIndex = 136;
            this._UabutUp.Text = "▲";
            this._UabutUp.UseMnemonic = false;
            this._UabutUp.UseVisualStyleBackColor = true;
            this._UabutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // fazaUn1
            // 
            this.fazaUn1.Location = new System.Drawing.Point(159, 161);
            this.fazaUn1.Name = "fazaUn1";
            this.fazaUn1.Size = new System.Drawing.Size(53, 20);
            this.fazaUn1.TabIndex = 180;
            this.fazaUn1.Tag = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 13);
            this.label18.TabIndex = 92;
            this.label18.Text = "Ua,, В";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(230, 164);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 120;
            this.label28.Text = "Fn1, Гц";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(132, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 13);
            this.label17.TabIndex = 94;
            this.label17.Text = "fa, °";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(127, 167);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 13);
            this.label29.TabIndex = 118;
            this.label29.Text = "fn1, °";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(236, 22);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 96;
            this.label16.Text = "Fa, Гц";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 164);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 13);
            this.label30.TabIndex = 116;
            this.label30.Text = "Un1, В";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 13);
            this.label21.TabIndex = 98;
            this.label21.Text = "Ub, В";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(236, 132);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 114;
            this.label25.Text = "Fn, Гц";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(132, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(26, 13);
            this.label20.TabIndex = 100;
            this.label20.Text = "fb, °";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(132, 132);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(26, 13);
            this.label26.TabIndex = 112;
            this.label26.Text = "fn, °";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(236, 60);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 102;
            this.label19.Text = "Fb, Гц";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 132);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 110;
            this.label27.Text = "Un, В";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(34, 13);
            this.label24.TabIndex = 104;
            this.label24.Text = "Uc, В";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(235, 97);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 13);
            this.label22.TabIndex = 108;
            this.label22.Text = "Fc, Гц";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(132, 97);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(26, 13);
            this.label23.TabIndex = 106;
            this.label23.Text = "fc, °";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._k2);
            this.groupBox2.Controls.Add(this._k1);
            this.groupBox2.Controls.Add(this._d40);
            this.groupBox2.Controls.Add(this._d39);
            this.groupBox2.Controls.Add(this._d38);
            this.groupBox2.Controls.Add(this._d37);
            this.groupBox2.Controls.Add(this._d36);
            this.groupBox2.Controls.Add(this._d35);
            this.groupBox2.Controls.Add(this._d34);
            this.groupBox2.Controls.Add(this._d33);
            this.groupBox2.Controls.Add(this._d32);
            this.groupBox2.Controls.Add(this._d31);
            this.groupBox2.Controls.Add(this._d30);
            this.groupBox2.Controls.Add(this._d29);
            this.groupBox2.Controls.Add(this._d28);
            this.groupBox2.Controls.Add(this._d27);
            this.groupBox2.Controls.Add(this._d26);
            this.groupBox2.Controls.Add(this._d25);
            this.groupBox2.Controls.Add(this._d24);
            this.groupBox2.Controls.Add(this._d23);
            this.groupBox2.Controls.Add(this._d22);
            this.groupBox2.Controls.Add(this._d21);
            this.groupBox2.Controls.Add(this._d20);
            this.groupBox2.Controls.Add(this._d19);
            this.groupBox2.Controls.Add(this._d18);
            this.groupBox2.Controls.Add(this._d17);
            this.groupBox2.Controls.Add(this._d16);
            this.groupBox2.Controls.Add(this._d15);
            this.groupBox2.Controls.Add(this._d14);
            this.groupBox2.Controls.Add(this._d13);
            this.groupBox2.Controls.Add(this._d12);
            this.groupBox2.Controls.Add(this._d11);
            this.groupBox2.Controls.Add(this._d10);
            this.groupBox2.Controls.Add(this._d9);
            this.groupBox2.Controls.Add(this._d8);
            this.groupBox2.Controls.Add(this._d7);
            this.groupBox2.Controls.Add(this._d6);
            this.groupBox2.Controls.Add(this._d5);
            this.groupBox2.Controls.Add(this._d4);
            this.groupBox2.Controls.Add(this._d3);
            this.groupBox2.Controls.Add(this._d2);
            this.groupBox2.Controls.Add(this._d1);
            this.groupBox2.Location = new System.Drawing.Point(385, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(103, 373);
            this.groupBox2.TabIndex = 65;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Дискреты";
            // 
            // _k2
            // 
            this._k2.AutoSize = true;
            this._k2.Location = new System.Drawing.Point(55, 352);
            this._k2.Name = "_k2";
            this._k2.Size = new System.Drawing.Size(39, 17);
            this._k2.TabIndex = 41;
            this._k2.Text = "K2";
            this._k2.UseVisualStyleBackColor = true;
            // 
            // _k1
            // 
            this._k1.AutoSize = true;
            this._k1.Location = new System.Drawing.Point(6, 352);
            this._k1.Name = "_k1";
            this._k1.Size = new System.Drawing.Size(39, 17);
            this._k1.TabIndex = 40;
            this._k1.Text = "K1";
            this._k1.UseVisualStyleBackColor = true;
            // 
            // _d40
            // 
            this._d40.AutoSize = true;
            this._d40.Location = new System.Drawing.Point(55, 335);
            this._d40.Name = "_d40";
            this._d40.Size = new System.Drawing.Size(47, 17);
            this._d40.TabIndex = 39;
            this._d40.Text = "Д40";
            this._d40.UseVisualStyleBackColor = true;
            // 
            // _d39
            // 
            this._d39.AutoSize = true;
            this._d39.Location = new System.Drawing.Point(55, 318);
            this._d39.Name = "_d39";
            this._d39.Size = new System.Drawing.Size(47, 17);
            this._d39.TabIndex = 38;
            this._d39.Text = "Д39";
            this._d39.UseVisualStyleBackColor = true;
            // 
            // _d38
            // 
            this._d38.AutoSize = true;
            this._d38.Location = new System.Drawing.Point(55, 301);
            this._d38.Name = "_d38";
            this._d38.Size = new System.Drawing.Size(47, 17);
            this._d38.TabIndex = 37;
            this._d38.Text = "Д38";
            this._d38.UseVisualStyleBackColor = true;
            // 
            // _d37
            // 
            this._d37.AutoSize = true;
            this._d37.Location = new System.Drawing.Point(55, 284);
            this._d37.Name = "_d37";
            this._d37.Size = new System.Drawing.Size(47, 17);
            this._d37.TabIndex = 36;
            this._d37.Text = "Д37";
            this._d37.UseVisualStyleBackColor = true;
            // 
            // _d36
            // 
            this._d36.AutoSize = true;
            this._d36.Location = new System.Drawing.Point(55, 267);
            this._d36.Name = "_d36";
            this._d36.Size = new System.Drawing.Size(47, 17);
            this._d36.TabIndex = 35;
            this._d36.Text = "Д36";
            this._d36.UseVisualStyleBackColor = true;
            // 
            // _d35
            // 
            this._d35.AutoSize = true;
            this._d35.Location = new System.Drawing.Point(55, 249);
            this._d35.Name = "_d35";
            this._d35.Size = new System.Drawing.Size(47, 17);
            this._d35.TabIndex = 34;
            this._d35.Text = "Д35";
            this._d35.UseVisualStyleBackColor = true;
            // 
            // _d34
            // 
            this._d34.AutoSize = true;
            this._d34.Location = new System.Drawing.Point(55, 232);
            this._d34.Name = "_d34";
            this._d34.Size = new System.Drawing.Size(47, 17);
            this._d34.TabIndex = 33;
            this._d34.Text = "Д34";
            this._d34.UseVisualStyleBackColor = true;
            // 
            // _d33
            // 
            this._d33.AutoSize = true;
            this._d33.Location = new System.Drawing.Point(54, 215);
            this._d33.Name = "_d33";
            this._d33.Size = new System.Drawing.Size(47, 17);
            this._d33.TabIndex = 32;
            this._d33.Text = "Д33";
            this._d33.UseVisualStyleBackColor = true;
            // 
            // _d32
            // 
            this._d32.AutoSize = true;
            this._d32.Location = new System.Drawing.Point(54, 199);
            this._d32.Name = "_d32";
            this._d32.Size = new System.Drawing.Size(47, 17);
            this._d32.TabIndex = 31;
            this._d32.Text = "Д32";
            this._d32.UseVisualStyleBackColor = true;
            // 
            // _d31
            // 
            this._d31.AutoSize = true;
            this._d31.Location = new System.Drawing.Point(54, 182);
            this._d31.Name = "_d31";
            this._d31.Size = new System.Drawing.Size(47, 17);
            this._d31.TabIndex = 30;
            this._d31.Text = "Д31";
            this._d31.UseVisualStyleBackColor = true;
            // 
            // _d30
            // 
            this._d30.AutoSize = true;
            this._d30.Location = new System.Drawing.Point(53, 165);
            this._d30.Name = "_d30";
            this._d30.Size = new System.Drawing.Size(47, 17);
            this._d30.TabIndex = 29;
            this._d30.Text = "Д30";
            this._d30.UseVisualStyleBackColor = true;
            // 
            // _d29
            // 
            this._d29.AutoSize = true;
            this._d29.Location = new System.Drawing.Point(53, 148);
            this._d29.Name = "_d29";
            this._d29.Size = new System.Drawing.Size(47, 17);
            this._d29.TabIndex = 28;
            this._d29.Text = "Д29";
            this._d29.UseVisualStyleBackColor = true;
            // 
            // _d28
            // 
            this._d28.AutoSize = true;
            this._d28.Location = new System.Drawing.Point(53, 131);
            this._d28.Name = "_d28";
            this._d28.Size = new System.Drawing.Size(47, 17);
            this._d28.TabIndex = 27;
            this._d28.Text = "Д28";
            this._d28.UseVisualStyleBackColor = true;
            // 
            // _d27
            // 
            this._d27.AutoSize = true;
            this._d27.Location = new System.Drawing.Point(53, 114);
            this._d27.Name = "_d27";
            this._d27.Size = new System.Drawing.Size(47, 17);
            this._d27.TabIndex = 26;
            this._d27.Text = "Д27";
            this._d27.UseVisualStyleBackColor = true;
            // 
            // _d26
            // 
            this._d26.AutoSize = true;
            this._d26.Location = new System.Drawing.Point(53, 98);
            this._d26.Name = "_d26";
            this._d26.Size = new System.Drawing.Size(47, 17);
            this._d26.TabIndex = 25;
            this._d26.Text = "Д26";
            this._d26.UseVisualStyleBackColor = true;
            // 
            // _d25
            // 
            this._d25.AutoSize = true;
            this._d25.Location = new System.Drawing.Point(53, 81);
            this._d25.Name = "_d25";
            this._d25.Size = new System.Drawing.Size(47, 17);
            this._d25.TabIndex = 24;
            this._d25.Text = "Д25";
            this._d25.UseVisualStyleBackColor = true;
            // 
            // _d24
            // 
            this._d24.AutoSize = true;
            this._d24.Location = new System.Drawing.Point(53, 64);
            this._d24.Name = "_d24";
            this._d24.Size = new System.Drawing.Size(47, 17);
            this._d24.TabIndex = 23;
            this._d24.Text = "Д24";
            this._d24.UseVisualStyleBackColor = true;
            // 
            // _d23
            // 
            this._d23.AutoSize = true;
            this._d23.Location = new System.Drawing.Point(53, 47);
            this._d23.Name = "_d23";
            this._d23.Size = new System.Drawing.Size(47, 17);
            this._d23.TabIndex = 22;
            this._d23.Text = "Д23";
            this._d23.UseVisualStyleBackColor = true;
            // 
            // _d22
            // 
            this._d22.AutoSize = true;
            this._d22.Location = new System.Drawing.Point(53, 30);
            this._d22.Name = "_d22";
            this._d22.Size = new System.Drawing.Size(47, 17);
            this._d22.TabIndex = 21;
            this._d22.Text = "Д22";
            this._d22.UseVisualStyleBackColor = true;
            // 
            // _d21
            // 
            this._d21.AutoSize = true;
            this._d21.Location = new System.Drawing.Point(53, 13);
            this._d21.Name = "_d21";
            this._d21.Size = new System.Drawing.Size(47, 17);
            this._d21.TabIndex = 20;
            this._d21.Text = "Д21";
            this._d21.UseVisualStyleBackColor = true;
            // 
            // _d20
            // 
            this._d20.AutoSize = true;
            this._d20.Location = new System.Drawing.Point(6, 335);
            this._d20.Name = "_d20";
            this._d20.Size = new System.Drawing.Size(47, 17);
            this._d20.TabIndex = 19;
            this._d20.Text = "Д20";
            this._d20.UseVisualStyleBackColor = true;
            // 
            // _d19
            // 
            this._d19.AutoSize = true;
            this._d19.Location = new System.Drawing.Point(6, 318);
            this._d19.Name = "_d19";
            this._d19.Size = new System.Drawing.Size(47, 17);
            this._d19.TabIndex = 18;
            this._d19.Text = "Д19";
            this._d19.UseVisualStyleBackColor = true;
            // 
            // _d18
            // 
            this._d18.AutoSize = true;
            this._d18.Location = new System.Drawing.Point(6, 301);
            this._d18.Name = "_d18";
            this._d18.Size = new System.Drawing.Size(47, 17);
            this._d18.TabIndex = 17;
            this._d18.Text = "Д18";
            this._d18.UseVisualStyleBackColor = true;
            // 
            // _d17
            // 
            this._d17.AutoSize = true;
            this._d17.Location = new System.Drawing.Point(6, 284);
            this._d17.Name = "_d17";
            this._d17.Size = new System.Drawing.Size(47, 17);
            this._d17.TabIndex = 16;
            this._d17.Text = "Д17";
            this._d17.UseVisualStyleBackColor = true;
            // 
            // _d16
            // 
            this._d16.AutoSize = true;
            this._d16.Location = new System.Drawing.Point(6, 267);
            this._d16.Name = "_d16";
            this._d16.Size = new System.Drawing.Size(47, 17);
            this._d16.TabIndex = 15;
            this._d16.Text = "Д16";
            this._d16.UseVisualStyleBackColor = true;
            // 
            // _d15
            // 
            this._d15.AutoSize = true;
            this._d15.Location = new System.Drawing.Point(6, 250);
            this._d15.Name = "_d15";
            this._d15.Size = new System.Drawing.Size(47, 17);
            this._d15.TabIndex = 14;
            this._d15.Text = "Д15";
            this._d15.UseVisualStyleBackColor = true;
            // 
            // _d14
            // 
            this._d14.AutoSize = true;
            this._d14.Location = new System.Drawing.Point(6, 233);
            this._d14.Name = "_d14";
            this._d14.Size = new System.Drawing.Size(47, 17);
            this._d14.TabIndex = 13;
            this._d14.Text = "Д14";
            this._d14.UseVisualStyleBackColor = true;
            // 
            // _d13
            // 
            this._d13.AutoSize = true;
            this._d13.Location = new System.Drawing.Point(6, 216);
            this._d13.Name = "_d13";
            this._d13.Size = new System.Drawing.Size(47, 17);
            this._d13.TabIndex = 12;
            this._d13.Text = "Д13";
            this._d13.UseVisualStyleBackColor = true;
            // 
            // _d12
            // 
            this._d12.AutoSize = true;
            this._d12.Location = new System.Drawing.Point(6, 199);
            this._d12.Name = "_d12";
            this._d12.Size = new System.Drawing.Size(47, 17);
            this._d12.TabIndex = 11;
            this._d12.Text = "Д12";
            this._d12.UseVisualStyleBackColor = true;
            // 
            // _d11
            // 
            this._d11.AutoSize = true;
            this._d11.Location = new System.Drawing.Point(6, 182);
            this._d11.Name = "_d11";
            this._d11.Size = new System.Drawing.Size(47, 17);
            this._d11.TabIndex = 10;
            this._d11.Text = "Д11";
            this._d11.UseVisualStyleBackColor = true;
            // 
            // _d10
            // 
            this._d10.AutoSize = true;
            this._d10.Location = new System.Drawing.Point(6, 166);
            this._d10.Name = "_d10";
            this._d10.Size = new System.Drawing.Size(47, 17);
            this._d10.TabIndex = 9;
            this._d10.Text = "Д10";
            this._d10.UseVisualStyleBackColor = true;
            // 
            // _d9
            // 
            this._d9.AutoSize = true;
            this._d9.Location = new System.Drawing.Point(6, 149);
            this._d9.Name = "_d9";
            this._d9.Size = new System.Drawing.Size(41, 17);
            this._d9.TabIndex = 8;
            this._d9.Text = "Д9";
            this._d9.UseVisualStyleBackColor = true;
            // 
            // _d8
            // 
            this._d8.AutoSize = true;
            this._d8.Location = new System.Drawing.Point(6, 132);
            this._d8.Name = "_d8";
            this._d8.Size = new System.Drawing.Size(41, 17);
            this._d8.TabIndex = 7;
            this._d8.Text = "Д8";
            this._d8.UseVisualStyleBackColor = true;
            this._d8.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d7
            // 
            this._d7.AutoSize = true;
            this._d7.Location = new System.Drawing.Point(6, 115);
            this._d7.Name = "_d7";
            this._d7.Size = new System.Drawing.Size(41, 17);
            this._d7.TabIndex = 6;
            this._d7.Text = "Д7";
            this._d7.UseVisualStyleBackColor = true;
            this._d7.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d6
            // 
            this._d6.AutoSize = true;
            this._d6.Location = new System.Drawing.Point(6, 98);
            this._d6.Name = "_d6";
            this._d6.Size = new System.Drawing.Size(41, 17);
            this._d6.TabIndex = 5;
            this._d6.Text = "Д6";
            this._d6.UseVisualStyleBackColor = true;
            this._d6.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d5
            // 
            this._d5.AutoSize = true;
            this._d5.Location = new System.Drawing.Point(6, 81);
            this._d5.Name = "_d5";
            this._d5.Size = new System.Drawing.Size(41, 17);
            this._d5.TabIndex = 4;
            this._d5.Text = "Д5";
            this._d5.UseVisualStyleBackColor = true;
            this._d5.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d4
            // 
            this._d4.AutoSize = true;
            this._d4.Location = new System.Drawing.Point(6, 64);
            this._d4.Name = "_d4";
            this._d4.Size = new System.Drawing.Size(41, 17);
            this._d4.TabIndex = 3;
            this._d4.Text = "Д4";
            this._d4.UseVisualStyleBackColor = true;
            this._d4.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d3
            // 
            this._d3.AutoSize = true;
            this._d3.Location = new System.Drawing.Point(6, 47);
            this._d3.Name = "_d3";
            this._d3.Size = new System.Drawing.Size(41, 17);
            this._d3.TabIndex = 2;
            this._d3.Text = "Д3";
            this._d3.UseVisualStyleBackColor = true;
            this._d3.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d2
            // 
            this._d2.AutoSize = true;
            this._d2.Location = new System.Drawing.Point(6, 29);
            this._d2.Name = "_d2";
            this._d2.Size = new System.Drawing.Size(41, 17);
            this._d2.TabIndex = 1;
            this._d2.Text = "Д2";
            this._d2.UseVisualStyleBackColor = true;
            this._d2.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // _d1
            // 
            this._d1.AutoSize = true;
            this._d1.Location = new System.Drawing.Point(6, 12);
            this._d1.Name = "_d1";
            this._d1.Size = new System.Drawing.Size(41, 17);
            this._d1.TabIndex = 0;
            this._d1.Text = "Д1";
            this._d1.UseVisualStyleBackColor = true;
            this._d1.CheckedChanged += new System.EventHandler(this._discret_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Ia);
            this.groupBox4.Controls.Add(this.Fn1);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this._Fn1butDown);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this._Fn1butUp);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.Fn);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this._FnbutDown);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this._FnbutUp);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.Fc);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this._FcbutDown);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this._FcbutUp);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.Fb);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this._FbbutDown);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this._FbbutUp);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.Fa);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this._FabutDown);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this._FabutUp);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.fazan1);
            this.groupBox4.Controls.Add(this._IabutUp);
            this.groupBox4.Controls.Add(this._FazaN1butDown);
            this.groupBox4.Controls.Add(this._IabutDown);
            this.groupBox4.Controls.Add(this._FazaN1butUp);
            this.groupBox4.Controls.Add(this._IbbutUp);
            this.groupBox4.Controls.Add(this.fazaIn);
            this.groupBox4.Controls.Add(this._IbbutDown);
            this.groupBox4.Controls.Add(this._fazaNbutDown);
            this.groupBox4.Controls.Add(this.Ib);
            this.groupBox4.Controls.Add(this._fazaNbutUp);
            this.groupBox4.Controls.Add(this._IcbutUp);
            this.groupBox4.Controls.Add(this.fazaIc);
            this.groupBox4.Controls.Add(this._IcbutDown);
            this.groupBox4.Controls.Add(this._fazaCbutDown);
            this.groupBox4.Controls.Add(this.Ic);
            this.groupBox4.Controls.Add(this._fazaCbutUp);
            this.groupBox4.Controls.Add(this._InbutUp);
            this.groupBox4.Controls.Add(this.fazaIb);
            this.groupBox4.Controls.Add(this._InbutDown);
            this.groupBox4.Controls.Add(this._fazaBbutDown);
            this.groupBox4.Controls.Add(this.In);
            this.groupBox4.Controls.Add(this._fazaBbutUp);
            this.groupBox4.Controls.Add(this._In1butUp);
            this.groupBox4.Controls.Add(this.fazaIa);
            this.groupBox4.Controls.Add(this._In1butDown);
            this.groupBox4.Controls.Add(this._fazaAbutDown);
            this.groupBox4.Controls.Add(this.In1);
            this.groupBox4.Controls.Add(this._fazaAbutUp);
            this.groupBox4.Location = new System.Drawing.Point(12, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(356, 126);
            this.groupBox4.TabIndex = 198;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Токи";
            // 
            // Ia
            // 
            this.Ia.Location = new System.Drawing.Point(53, 22);
            this.Ia.Name = "Ia";
            this.Ia.Size = new System.Drawing.Size(53, 20);
            this.Ia.TabIndex = 123;
            this.Ia.Tag = "I";
            // 
            // Fn1
            // 
            this.Fn1.Location = new System.Drawing.Point(237, 164);
            this.Fn1.Name = "Fn1";
            this.Fn1.Size = new System.Drawing.Size(53, 20);
            this.Fn1.TabIndex = 195;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 62;
            this.label1.Text = "Ia, А";
            // 
            // _Fn1butDown
            // 
            this._Fn1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._Fn1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._Fn1butDown.Location = new System.Drawing.Point(288, 174);
            this._Fn1butDown.Name = "_Fn1butDown";
            this._Fn1butDown.Size = new System.Drawing.Size(22, 14);
            this._Fn1butDown.TabIndex = 194;
            this._Fn1butDown.Text = "\t▼";
            this._Fn1butDown.UseVisualStyleBackColor = false;
            this._Fn1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = "fa, °";
            // 
            // _Fn1butUp
            // 
            this._Fn1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._Fn1butUp.Location = new System.Drawing.Point(288, 162);
            this._Fn1butUp.Name = "_Fn1butUp";
            this._Fn1butUp.Size = new System.Drawing.Size(22, 14);
            this._Fn1butUp.TabIndex = 193;
            this._Fn1butUp.Text = "▲";
            this._Fn1butUp.UseMnemonic = false;
            this._Fn1butUp.UseVisualStyleBackColor = true;
            this._Fn1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 66;
            this.label3.Text = "Fa, Гц";
            // 
            // Fn
            // 
            this.Fn.Location = new System.Drawing.Point(237, 128);
            this.Fn.Name = "Fn";
            this.Fn.Size = new System.Drawing.Size(53, 20);
            this.Fn.TabIndex = 192;
            this.Fn.Text = "50";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 68;
            this.label6.Text = "Ib, А";
            // 
            // _FnbutDown
            // 
            this._FnbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FnbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FnbutDown.Location = new System.Drawing.Point(288, 138);
            this._FnbutDown.Name = "_FnbutDown";
            this._FnbutDown.Size = new System.Drawing.Size(22, 14);
            this._FnbutDown.TabIndex = 191;
            this._FnbutDown.Text = "\t▼";
            this._FnbutDown.UseVisualStyleBackColor = false;
            this._FnbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(132, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 70;
            this.label5.Text = "fb, °";
            // 
            // _FnbutUp
            // 
            this._FnbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FnbutUp.Location = new System.Drawing.Point(288, 126);
            this._FnbutUp.Name = "_FnbutUp";
            this._FnbutUp.Size = new System.Drawing.Size(22, 14);
            this._FnbutUp.TabIndex = 190;
            this._FnbutUp.Text = "▲";
            this._FnbutUp.UseMnemonic = false;
            this._FnbutUp.UseVisualStyleBackColor = true;
            this._FnbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 72;
            this.label4.Text = "Fb, Гц";
            // 
            // Fc
            // 
            this.Fc.Location = new System.Drawing.Point(274, 89);
            this.Fc.Name = "Fc";
            this.Fc.Size = new System.Drawing.Size(53, 20);
            this.Fc.TabIndex = 189;
            this.Fc.Text = "50";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 74;
            this.label9.Text = "Ic, А";
            // 
            // _FcbutDown
            // 
            this._FcbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FcbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FcbutDown.Location = new System.Drawing.Point(325, 99);
            this._FcbutDown.Name = "_FcbutDown";
            this._FcbutDown.Size = new System.Drawing.Size(22, 14);
            this._FcbutDown.TabIndex = 188;
            this._FcbutDown.Tag = "Fc";
            this._FcbutDown.Text = "\t▼";
            this._FcbutDown.UseVisualStyleBackColor = false;
            this._FcbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(132, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 76;
            this.label8.Text = "fc, °";
            // 
            // _FcbutUp
            // 
            this._FcbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FcbutUp.Location = new System.Drawing.Point(325, 87);
            this._FcbutUp.Name = "_FcbutUp";
            this._FcbutUp.Size = new System.Drawing.Size(22, 14);
            this._FcbutUp.TabIndex = 187;
            this._FcbutUp.Tag = "Fc";
            this._FcbutUp.Text = "▲";
            this._FcbutUp.UseMnemonic = false;
            this._FcbutUp.UseVisualStyleBackColor = true;
            this._FcbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(236, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 78;
            this.label7.Text = "Fc, Гц";
            // 
            // Fb
            // 
            this.Fb.Location = new System.Drawing.Point(274, 54);
            this.Fb.Name = "Fb";
            this.Fb.Size = new System.Drawing.Size(53, 20);
            this.Fb.TabIndex = 186;
            this.Fb.Text = "50";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 80;
            this.label12.Text = "In";
            // 
            // _FbbutDown
            // 
            this._FbbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FbbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FbbutDown.Location = new System.Drawing.Point(325, 64);
            this._FbbutDown.Name = "_FbbutDown";
            this._FbbutDown.Size = new System.Drawing.Size(22, 14);
            this._FbbutDown.TabIndex = 185;
            this._FbbutDown.Tag = "Fb";
            this._FbbutDown.Text = "\t▼";
            this._FbbutDown.UseVisualStyleBackColor = false;
            this._FbbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(117, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 13);
            this.label11.TabIndex = 82;
            this.label11.Text = "fn";
            // 
            // _FbbutUp
            // 
            this._FbbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FbbutUp.Location = new System.Drawing.Point(325, 52);
            this._FbbutUp.Name = "_FbbutUp";
            this._FbbutUp.Size = new System.Drawing.Size(22, 14);
            this._FbbutUp.TabIndex = 184;
            this._FbbutUp.Tag = "Fb";
            this._FbbutUp.Text = "▲";
            this._FbbutUp.UseMnemonic = false;
            this._FbbutUp.UseVisualStyleBackColor = true;
            this._FbbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(212, 131);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 84;
            this.label10.Text = "Fn";
            // 
            // Fa
            // 
            this.Fa.Location = new System.Drawing.Point(274, 22);
            this.Fa.Name = "Fa";
            this.Fa.Size = new System.Drawing.Size(53, 20);
            this.Fa.TabIndex = 183;
            this.Fa.Text = "50";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 167);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(22, 13);
            this.label15.TabIndex = 86;
            this.label15.Text = "In1";
            // 
            // _FabutDown
            // 
            this._FabutDown.BackColor = System.Drawing.SystemColors.Control;
            this._FabutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FabutDown.Location = new System.Drawing.Point(325, 32);
            this._FabutDown.Name = "_FabutDown";
            this._FabutDown.Size = new System.Drawing.Size(22, 14);
            this._FabutDown.TabIndex = 182;
            this._FabutDown.Tag = "Fa";
            this._FabutDown.Text = "\t▼";
            this._FabutDown.UseVisualStyleBackColor = false;
            this._FabutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(117, 167);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 13);
            this.label14.TabIndex = 88;
            this.label14.Text = "fn1";
            // 
            // _FabutUp
            // 
            this._FabutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FabutUp.Location = new System.Drawing.Point(325, 20);
            this._FabutUp.Name = "_FabutUp";
            this._FabutUp.Size = new System.Drawing.Size(22, 14);
            this._FabutUp.TabIndex = 181;
            this._FabutUp.Tag = "Fa";
            this._FabutUp.Text = "▲";
            this._FabutUp.UseMnemonic = false;
            this._FabutUp.UseVisualStyleBackColor = true;
            this._FabutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(212, 167);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(25, 13);
            this.label13.TabIndex = 90;
            this.label13.Text = "Fn1";
            // 
            // fazan1
            // 
            this.fazan1.Location = new System.Drawing.Point(139, 164);
            this.fazan1.Name = "fazan1";
            this.fazan1.Size = new System.Drawing.Size(53, 20);
            this.fazan1.TabIndex = 165;
            // 
            // _IabutUp
            // 
            this._IabutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IabutUp.Location = new System.Drawing.Point(104, 20);
            this._IabutUp.Name = "_IabutUp";
            this._IabutUp.Size = new System.Drawing.Size(22, 14);
            this._IabutUp.TabIndex = 121;
            this._IabutUp.Tag = "Ia";
            this._IabutUp.Text = "▲";
            this._IabutUp.UseMnemonic = false;
            this._IabutUp.UseVisualStyleBackColor = true;
            this._IabutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _FazaN1butDown
            // 
            this._FazaN1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._FazaN1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FazaN1butDown.Location = new System.Drawing.Point(190, 174);
            this._FazaN1butDown.Name = "_FazaN1butDown";
            this._FazaN1butDown.Size = new System.Drawing.Size(22, 14);
            this._FazaN1butDown.TabIndex = 164;
            this._FazaN1butDown.Text = "\t▼";
            this._FazaN1butDown.UseVisualStyleBackColor = false;
            this._FazaN1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _IabutDown
            // 
            this._IabutDown.BackColor = System.Drawing.SystemColors.Control;
            this._IabutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IabutDown.Location = new System.Drawing.Point(104, 32);
            this._IabutDown.Name = "_IabutDown";
            this._IabutDown.Size = new System.Drawing.Size(22, 14);
            this._IabutDown.TabIndex = 122;
            this._IabutDown.Tag = "Ia";
            this._IabutDown.Text = "\t▼";
            this._IabutDown.UseVisualStyleBackColor = false;
            this._IabutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _FazaN1butUp
            // 
            this._FazaN1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._FazaN1butUp.Location = new System.Drawing.Point(190, 162);
            this._FazaN1butUp.Name = "_FazaN1butUp";
            this._FazaN1butUp.Size = new System.Drawing.Size(22, 14);
            this._FazaN1butUp.TabIndex = 163;
            this._FazaN1butUp.Text = "▲";
            this._FazaN1butUp.UseMnemonic = false;
            this._FazaN1butUp.UseVisualStyleBackColor = true;
            this._FazaN1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _IbbutUp
            // 
            this._IbbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IbbutUp.Location = new System.Drawing.Point(104, 52);
            this._IbbutUp.Name = "_IbbutUp";
            this._IbbutUp.Size = new System.Drawing.Size(22, 14);
            this._IbbutUp.TabIndex = 124;
            this._IbbutUp.Tag = "Ib";
            this._IbbutUp.Text = "▲";
            this._IbbutUp.UseMnemonic = false;
            this._IbbutUp.UseVisualStyleBackColor = true;
            this._IbbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // fazaIn
            // 
            this.fazaIn.Location = new System.Drawing.Point(139, 128);
            this.fazaIn.Name = "fazaIn";
            this.fazaIn.Size = new System.Drawing.Size(53, 20);
            this.fazaIn.TabIndex = 162;
            this.fazaIn.Tag = "";
            // 
            // _IbbutDown
            // 
            this._IbbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._IbbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IbbutDown.Location = new System.Drawing.Point(104, 64);
            this._IbbutDown.Name = "_IbbutDown";
            this._IbbutDown.Size = new System.Drawing.Size(22, 14);
            this._IbbutDown.TabIndex = 125;
            this._IbbutDown.Tag = "Ib";
            this._IbbutDown.Text = "\t▼";
            this._IbbutDown.UseVisualStyleBackColor = false;
            this._IbbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _fazaNbutDown
            // 
            this._fazaNbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaNbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaNbutDown.Location = new System.Drawing.Point(190, 138);
            this._fazaNbutDown.Name = "_fazaNbutDown";
            this._fazaNbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaNbutDown.TabIndex = 161;
            this._fazaNbutDown.Text = "\t▼";
            this._fazaNbutDown.UseVisualStyleBackColor = false;
            this._fazaNbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Ib
            // 
            this.Ib.Location = new System.Drawing.Point(53, 54);
            this.Ib.Name = "Ib";
            this.Ib.Size = new System.Drawing.Size(53, 20);
            this.Ib.TabIndex = 126;
            this.Ib.Tag = "I";
            // 
            // _fazaNbutUp
            // 
            this._fazaNbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaNbutUp.Location = new System.Drawing.Point(190, 126);
            this._fazaNbutUp.Name = "_fazaNbutUp";
            this._fazaNbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaNbutUp.TabIndex = 160;
            this._fazaNbutUp.Text = "▲";
            this._fazaNbutUp.UseMnemonic = false;
            this._fazaNbutUp.UseVisualStyleBackColor = true;
            this._fazaNbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _IcbutUp
            // 
            this._IcbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IcbutUp.Location = new System.Drawing.Point(104, 87);
            this._IcbutUp.Name = "_IcbutUp";
            this._IcbutUp.Size = new System.Drawing.Size(22, 14);
            this._IcbutUp.TabIndex = 127;
            this._IcbutUp.Tag = "Ic";
            this._IcbutUp.Text = "▲";
            this._IcbutUp.UseMnemonic = false;
            this._IcbutUp.UseVisualStyleBackColor = true;
            this._IcbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // fazaIc
            // 
            this.fazaIc.Location = new System.Drawing.Point(159, 89);
            this.fazaIc.Name = "fazaIc";
            this.fazaIc.Size = new System.Drawing.Size(53, 20);
            this.fazaIc.TabIndex = 159;
            this.fazaIc.Tag = "";
            // 
            // _IcbutDown
            // 
            this._IcbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._IcbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._IcbutDown.Location = new System.Drawing.Point(104, 99);
            this._IcbutDown.Name = "_IcbutDown";
            this._IcbutDown.Size = new System.Drawing.Size(22, 14);
            this._IcbutDown.TabIndex = 128;
            this._IcbutDown.Tag = "Ic";
            this._IcbutDown.Text = "\t▼";
            this._IcbutDown.UseVisualStyleBackColor = false;
            this._IcbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _fazaCbutDown
            // 
            this._fazaCbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaCbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaCbutDown.Location = new System.Drawing.Point(210, 99);
            this._fazaCbutDown.Name = "_fazaCbutDown";
            this._fazaCbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaCbutDown.TabIndex = 158;
            this._fazaCbutDown.Tag = "fazaC";
            this._fazaCbutDown.Text = "\t▼";
            this._fazaCbutDown.UseVisualStyleBackColor = false;
            this._fazaCbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // Ic
            // 
            this.Ic.Location = new System.Drawing.Point(53, 89);
            this.Ic.Name = "Ic";
            this.Ic.Size = new System.Drawing.Size(53, 20);
            this.Ic.TabIndex = 129;
            this.Ic.Tag = "I";
            // 
            // _fazaCbutUp
            // 
            this._fazaCbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaCbutUp.Location = new System.Drawing.Point(210, 87);
            this._fazaCbutUp.Name = "_fazaCbutUp";
            this._fazaCbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaCbutUp.TabIndex = 157;
            this._fazaCbutUp.Tag = "fazaC";
            this._fazaCbutUp.Text = "▲";
            this._fazaCbutUp.UseMnemonic = false;
            this._fazaCbutUp.UseVisualStyleBackColor = true;
            this._fazaCbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _InbutUp
            // 
            this._InbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._InbutUp.Location = new System.Drawing.Point(87, 126);
            this._InbutUp.Name = "_InbutUp";
            this._InbutUp.Size = new System.Drawing.Size(22, 14);
            this._InbutUp.TabIndex = 130;
            this._InbutUp.Text = "▲";
            this._InbutUp.UseMnemonic = false;
            this._InbutUp.UseVisualStyleBackColor = true;
            this._InbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // fazaIb
            // 
            this.fazaIb.Location = new System.Drawing.Point(159, 54);
            this.fazaIb.Name = "fazaIb";
            this.fazaIb.Size = new System.Drawing.Size(53, 20);
            this.fazaIb.TabIndex = 156;
            this.fazaIb.Tag = "";
            // 
            // _InbutDown
            // 
            this._InbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._InbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._InbutDown.Location = new System.Drawing.Point(87, 138);
            this._InbutDown.Name = "_InbutDown";
            this._InbutDown.Size = new System.Drawing.Size(22, 14);
            this._InbutDown.TabIndex = 131;
            this._InbutDown.Text = "\t▼";
            this._InbutDown.UseVisualStyleBackColor = false;
            this._InbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _fazaBbutDown
            // 
            this._fazaBbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaBbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaBbutDown.Location = new System.Drawing.Point(210, 64);
            this._fazaBbutDown.Name = "_fazaBbutDown";
            this._fazaBbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaBbutDown.TabIndex = 155;
            this._fazaBbutDown.Tag = "fazaB";
            this._fazaBbutDown.Text = "\t▼";
            this._fazaBbutDown.UseVisualStyleBackColor = false;
            this._fazaBbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // In
            // 
            this.In.Location = new System.Drawing.Point(36, 128);
            this.In.Name = "In";
            this.In.Size = new System.Drawing.Size(53, 20);
            this.In.TabIndex = 132;
            this.In.Tag = "I";
            // 
            // _fazaBbutUp
            // 
            this._fazaBbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaBbutUp.Location = new System.Drawing.Point(210, 52);
            this._fazaBbutUp.Name = "_fazaBbutUp";
            this._fazaBbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaBbutUp.TabIndex = 154;
            this._fazaBbutUp.Tag = "fazaB";
            this._fazaBbutUp.Text = "▲";
            this._fazaBbutUp.UseMnemonic = false;
            this._fazaBbutUp.UseVisualStyleBackColor = true;
            this._fazaBbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _In1butUp
            // 
            this._In1butUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._In1butUp.Location = new System.Drawing.Point(87, 162);
            this._In1butUp.Name = "_In1butUp";
            this._In1butUp.Size = new System.Drawing.Size(22, 14);
            this._In1butUp.TabIndex = 133;
            this._In1butUp.Text = "▲";
            this._In1butUp.UseMnemonic = false;
            this._In1butUp.UseVisualStyleBackColor = true;
            this._In1butUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // fazaIa
            // 
            this.fazaIa.Location = new System.Drawing.Point(159, 22);
            this.fazaIa.Name = "fazaIa";
            this.fazaIa.Size = new System.Drawing.Size(53, 20);
            this.fazaIa.TabIndex = 153;
            this.fazaIa.Tag = "";
            // 
            // _In1butDown
            // 
            this._In1butDown.BackColor = System.Drawing.SystemColors.Control;
            this._In1butDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._In1butDown.Location = new System.Drawing.Point(87, 174);
            this._In1butDown.Name = "_In1butDown";
            this._In1butDown.Size = new System.Drawing.Size(22, 14);
            this._In1butDown.TabIndex = 134;
            this._In1butDown.Text = "\t▼";
            this._In1butDown.UseVisualStyleBackColor = false;
            this._In1butDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // _fazaAbutDown
            // 
            this._fazaAbutDown.BackColor = System.Drawing.SystemColors.Control;
            this._fazaAbutDown.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaAbutDown.Location = new System.Drawing.Point(210, 32);
            this._fazaAbutDown.Name = "_fazaAbutDown";
            this._fazaAbutDown.Size = new System.Drawing.Size(22, 14);
            this._fazaAbutDown.TabIndex = 152;
            this._fazaAbutDown.Tag = "fazaA";
            this._fazaAbutDown.Text = "\t▼";
            this._fazaAbutDown.UseVisualStyleBackColor = false;
            this._fazaAbutDown.Click += new System.EventHandler(this.Down_Click);
            // 
            // In1
            // 
            this.In1.Location = new System.Drawing.Point(36, 164);
            this.In1.Name = "In1";
            this.In1.Size = new System.Drawing.Size(53, 20);
            this.In1.TabIndex = 135;
            // 
            // _fazaAbutUp
            // 
            this._fazaAbutUp.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._fazaAbutUp.Location = new System.Drawing.Point(210, 20);
            this._fazaAbutUp.Name = "_fazaAbutUp";
            this._fazaAbutUp.Size = new System.Drawing.Size(22, 14);
            this._fazaAbutUp.TabIndex = 151;
            this._fazaAbutUp.Tag = "fazaA";
            this._fazaAbutUp.Text = "▲";
            this._fazaAbutUp.UseMnemonic = false;
            this._fazaAbutUp.UseVisualStyleBackColor = true;
            this._fazaAbutUp.Click += new System.EventHandler(this.Up_Click);
            // 
            // _time
            // 
            this._time.AutoSize = true;
            this._time.Location = new System.Drawing.Point(394, 404);
            this._time.Name = "_time";
            this._time.Size = new System.Drawing.Size(0, 13);
            this._time.TabIndex = 79;
            this._time.TextChanged += new System.EventHandler(this._time_TextChanged);
            // 
            // _statusLedControl
            // 
            this._statusLedControl.Location = new System.Drawing.Point(36, 491);
            this._statusLedControl.Name = "_statusLedControl";
            this._statusLedControl.Size = new System.Drawing.Size(13, 13);
            this._statusLedControl.State = BEMN.Forms.LedState.Off;
            this._statusLedControl.TabIndex = 80;
            // 
            // _labelStatus
            // 
            this._labelStatus.AutoSize = true;
            this._labelStatus.Location = new System.Drawing.Point(55, 491);
            this._labelStatus.Name = "_labelStatus";
            this._labelStatus.Size = new System.Drawing.Size(136, 13);
            this._labelStatus.TabIndex = 81;
            this._labelStatus.Text = "Нет связи с устройством";
            // 
            // _kvit
            // 
            this._kvit.Location = new System.Drawing.Point(318, 441);
            this._kvit.Name = "_kvit";
            this._kvit.Size = new System.Drawing.Size(140, 33);
            this._kvit.TabIndex = 82;
            this._kvit.Text = "Квитировать";
            this._kvit.UseVisualStyleBackColor = true;
            this._kvit.Click += new System.EventHandler(this._kvit_Click);
            // 
            // _exitEmulation
            // 
            this._exitEmulation.AutoSize = true;
            this._exitEmulation.Location = new System.Drawing.Point(163, 420);
            this._exitEmulation.Name = "_exitEmulation";
            this._exitEmulation.Size = new System.Drawing.Size(201, 17);
            this._exitEmulation.TabIndex = 85;
            this._exitEmulation.Text = "Не выходить из режима эмуляции";
            this._exitEmulation.UseVisualStyleBackColor = true;
            this._exitEmulation.CheckedChanged += new System.EventHandler(this._exitEmulation_CheckedChanged);
            // 
            // _startK1
            // 
            this._startK1.AutoSize = true;
            this._startK1.Location = new System.Drawing.Point(13, 181);
            this._startK1.Name = "_startK1";
            this._startK1.Size = new System.Drawing.Size(39, 17);
            this._startK1.TabIndex = 80;
            this._startK1.Text = "K1";
            this._startK1.UseVisualStyleBackColor = true;
            // 
            // _startK2
            // 
            this._startK2.AutoSize = true;
            this._startK2.Location = new System.Drawing.Point(60, 181);
            this._startK2.Name = "_startK2";
            this._startK2.Size = new System.Drawing.Size(39, 17);
            this._startK2.TabIndex = 81;
            this._startK2.Text = "K2";
            this._startK2.UseVisualStyleBackColor = true;
            // 
            // EmulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 541);
            this.Controls.Add(this._exitEmulation);
            this.Controls.Add(this._kvit);
            this.Controls.Add(this._labelStatus);
            this.Controls.Add(this._statusLedControl);
            this.Controls.Add(this._time);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.label34);
            this.Controls.Add(this._writeEmulButton);
            this.Controls.Add(this.label33);
            this.Controls.Add(this._status);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this._step);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label31);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "EmulationForm";
            this.Text = "EmulationForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EmulationForm_FormClosed);
            this.Load += new System.EventHandler(this.EmulationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._step)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _startTime;
        private System.Windows.Forms.ComboBox _signal;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.MaskedTextBox _status;
        private System.Windows.Forms.NumericUpDown _step;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox _startFUn1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox _startFUn;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox _startFUc;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox _startFUb;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox _startFUa;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox _startFn;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox _startFc;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox _startFb;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox _startFa;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox _startFazaUn1;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox _startFazaUn;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox _startFazaUc;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox _startFazaUb;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox _startFazaUa;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox _startFazaN;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox _startFazaC;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox _startFazaB;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox _startFazaA;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox _startUn1;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox _startUn;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox _startUc;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox _startUb;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox _startUa;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox _startIn;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox _startIc;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox _startIb;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox _startIa;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox _writeEmulButton;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox FUn1;
        private System.Windows.Forms.MaskedTextBox Ua;
        private System.Windows.Forms.Button _fazaUabutUp;
        private System.Windows.Forms.Button _FUn1butDown;
        private System.Windows.Forms.MaskedTextBox Un1;
        private System.Windows.Forms.Button _fazaUabutDown;
        private System.Windows.Forms.Button _FUn1butUp;
        private System.Windows.Forms.Button _Un1butDown;
        private System.Windows.Forms.MaskedTextBox fazaUa;
        private System.Windows.Forms.MaskedTextBox FUn;
        private System.Windows.Forms.Button _Un1butUp;
        private System.Windows.Forms.Button _fazaUbbutUp;
        private System.Windows.Forms.Button _FUnbutDown;
        private System.Windows.Forms.MaskedTextBox Un;
        private System.Windows.Forms.Button _fazaUbbutDown;
        private System.Windows.Forms.Button _FUnbutUp;
        private System.Windows.Forms.Button _UnbutDown;
        private System.Windows.Forms.MaskedTextBox fazaUb;
        private System.Windows.Forms.MaskedTextBox FUc;
        private System.Windows.Forms.Button _UnbutUp;
        private System.Windows.Forms.Button _fazaUcbutUp;
        private System.Windows.Forms.Button _FUcbutDown;
        private System.Windows.Forms.MaskedTextBox Uc;
        private System.Windows.Forms.Button _fazaUcbutDown;
        private System.Windows.Forms.Button _FUcbutUp;
        private System.Windows.Forms.Button _UcbutDown;
        private System.Windows.Forms.MaskedTextBox fazaUc;
        private System.Windows.Forms.MaskedTextBox FUb;
        private System.Windows.Forms.Button _UcbutUp;
        private System.Windows.Forms.Button _fazaUnbutUp;
        private System.Windows.Forms.Button _FUbbutDown;
        private System.Windows.Forms.MaskedTextBox Ub;
        private System.Windows.Forms.Button _fazaUnbutDown;
        private System.Windows.Forms.Button _FUbbutUp;
        private System.Windows.Forms.Button _UbbutDown;
        private System.Windows.Forms.MaskedTextBox fazaUn;
        private System.Windows.Forms.MaskedTextBox FUa;
        private System.Windows.Forms.Button _UbbutUp;
        private System.Windows.Forms.Button _fazaUn1butUp;
        private System.Windows.Forms.Button _FUabutDown;
        private System.Windows.Forms.Button _UabutDown;
        private System.Windows.Forms.Button _fazaUn1butDown;
        private System.Windows.Forms.Button _FUabutUp;
        private System.Windows.Forms.Button _UabutUp;
        private System.Windows.Forms.MaskedTextBox fazaUn1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox _d40;
        private System.Windows.Forms.CheckBox _d39;
        private System.Windows.Forms.CheckBox _d38;
        private System.Windows.Forms.CheckBox _d37;
        private System.Windows.Forms.CheckBox _d36;
        private System.Windows.Forms.CheckBox _d35;
        private System.Windows.Forms.CheckBox _d34;
        private System.Windows.Forms.CheckBox _d33;
        private System.Windows.Forms.CheckBox _d32;
        private System.Windows.Forms.CheckBox _d31;
        private System.Windows.Forms.CheckBox _d30;
        private System.Windows.Forms.CheckBox _d29;
        private System.Windows.Forms.CheckBox _d28;
        private System.Windows.Forms.CheckBox _d27;
        private System.Windows.Forms.CheckBox _d26;
        private System.Windows.Forms.CheckBox _d25;
        private System.Windows.Forms.CheckBox _d24;
        private System.Windows.Forms.CheckBox _d23;
        private System.Windows.Forms.CheckBox _d22;
        private System.Windows.Forms.CheckBox _d21;
        private System.Windows.Forms.CheckBox _d20;
        private System.Windows.Forms.CheckBox _d19;
        private System.Windows.Forms.CheckBox _d18;
        private System.Windows.Forms.CheckBox _d17;
        private System.Windows.Forms.CheckBox _d16;
        private System.Windows.Forms.CheckBox _d15;
        private System.Windows.Forms.CheckBox _d14;
        private System.Windows.Forms.CheckBox _d13;
        private System.Windows.Forms.CheckBox _d12;
        private System.Windows.Forms.CheckBox _d11;
        private System.Windows.Forms.CheckBox _d10;
        private System.Windows.Forms.CheckBox _d9;
        private System.Windows.Forms.CheckBox _d8;
        private System.Windows.Forms.CheckBox _d7;
        private System.Windows.Forms.CheckBox _d6;
        private System.Windows.Forms.CheckBox _d5;
        private System.Windows.Forms.CheckBox _d4;
        private System.Windows.Forms.CheckBox _d3;
        private System.Windows.Forms.CheckBox _d2;
        private System.Windows.Forms.CheckBox _d1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox Ia;
        private System.Windows.Forms.MaskedTextBox Fn1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _Fn1butDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button _Fn1butUp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox Fn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button _FnbutDown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button _FnbutUp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox Fc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button _FcbutDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button _FcbutUp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox Fb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button _FbbutDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button _FbbutUp;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox Fa;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button _FabutDown;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button _FabutUp;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox fazan1;
        private System.Windows.Forms.Button _IabutUp;
        private System.Windows.Forms.Button _FazaN1butDown;
        private System.Windows.Forms.Button _IabutDown;
        private System.Windows.Forms.Button _FazaN1butUp;
        private System.Windows.Forms.Button _IbbutUp;
        private System.Windows.Forms.MaskedTextBox fazaIn;
        private System.Windows.Forms.Button _IbbutDown;
        private System.Windows.Forms.Button _fazaNbutDown;
        private System.Windows.Forms.MaskedTextBox Ib;
        private System.Windows.Forms.Button _fazaNbutUp;
        private System.Windows.Forms.Button _IcbutUp;
        private System.Windows.Forms.MaskedTextBox fazaIc;
        private System.Windows.Forms.Button _IcbutDown;
        private System.Windows.Forms.Button _fazaCbutDown;
        private System.Windows.Forms.MaskedTextBox Ic;
        private System.Windows.Forms.Button _fazaCbutUp;
        private System.Windows.Forms.Button _InbutUp;
        private System.Windows.Forms.MaskedTextBox fazaIb;
        private System.Windows.Forms.Button _InbutDown;
        private System.Windows.Forms.Button _fazaBbutDown;
        private System.Windows.Forms.MaskedTextBox In;
        private System.Windows.Forms.Button _fazaBbutUp;
        private System.Windows.Forms.Button _In1butUp;
        private System.Windows.Forms.MaskedTextBox fazaIa;
        private System.Windows.Forms.Button _In1butDown;
        private System.Windows.Forms.Button _fazaAbutDown;
        private System.Windows.Forms.MaskedTextBox In1;
        private System.Windows.Forms.Button _fazaAbutUp;
        private System.Windows.Forms.Label _timeSignal;
        private System.Windows.Forms.Label _time;
        private BEMN.Forms.LedControl _statusLedControl;
        private System.Windows.Forms.Label _labelStatus;
        private System.Windows.Forms.Button _kvit;
        private System.Windows.Forms.ToolTip kvitTooltip;
        private System.Windows.Forms.CheckBox _startFazaN1;
        private System.Windows.Forms.CheckBox _startIn1;
        private System.Windows.Forms.CheckBox _startFn1;
        private System.Windows.Forms.CheckBox _exitEmulation;
        private System.Windows.Forms.CheckBox _k2;
        private System.Windows.Forms.CheckBox _k1;
        private System.Windows.Forms.CheckBox _startK1;
        private System.Windows.Forms.CheckBox _startK2;
    }
}