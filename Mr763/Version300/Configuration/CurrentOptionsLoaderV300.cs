﻿using System;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer;

namespace BEMN.Mr763.Version300.Configuration
{
    public class CurrentOptionsLoaderV300
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 6;
        

        private  MemoryEntity<MeasureTransStructV3>[] _connections;
        private int _numberOfGroup;
        #endregion [Private fields]

        public MeasureTransStructV3 this[int index]
        {
            get { return this._connections[index].Value; }
        }

        #region [Events]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private Mr763Device _device;

        #endregion [Events]


        #region [Ctor's]

        public CurrentOptionsLoaderV300(Mr763Device device, int slotLen)
        {
            this._device = device;
            this._connections = new MemoryEntity<MeasureTransStructV3>[COUNT_GROUPS];
            this._numberOfGroup = 0;
            for (int i = 0; i < this._connections.Length; i++)
            {
                this._connections[i] =
                    new MemoryEntity<MeasureTransStructV3>(string.Format("Параметры измерительного трансформатора гр{0} V3", i + 1),
                        this._device, device.GetStartAddrMeasTrans(i, this._device.DeviceVersion), slotLen);
                this._connections[i].AllReadOk += this._connections_AllReadOk;
                this._connections[i].AllReadFail += this._connections_AllReadFail;
            }
        }
        

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            switch (this._numberOfGroup)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    this._connections[++this._numberOfGroup].LoadStruct();
                    break;
                case 5:
                    if (this.LoadOk != null)
                    {
                        this.LoadOk.Invoke();
                    }
                    break;
            }
        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this._connections[0].LoadStruct();
        }
        
        #endregion [Methods]
    }
}
