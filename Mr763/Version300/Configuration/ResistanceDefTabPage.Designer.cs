﻿namespace BEMN.Mr763.Version300.Configuration
{
    partial class ResistanceDefTabPage
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uStartMaskedText = new System.Windows.Forms.MaskedTextBox();
            this.tyTextBox = new System.Windows.Forms.MaskedTextBox();
            this.icpTextBox = new System.Windows.Forms.MaskedTextBox();
            this.tcpTextBox = new System.Windows.Forms.MaskedTextBox();
            this.oscilloscopeCombo = new System.Windows.Forms.ComboBox();
            this.logicCombo = new System.Windows.Forms.ComboBox();
            this.directionCombo = new System.Windows.Forms.ComboBox();
            this.inpAccelerationCombo = new System.Windows.Forms.ComboBox();
            this.blockCombo = new System.Windows.Forms.ComboBox();
            this.typeCombo = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._frLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.modeCombo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.rTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.xTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this._xl1 = new System.Windows.Forms.Label();
            this.rTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.xTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this._rl1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cornerRadTextBox2 = new System.Windows.Forms.MaskedTextBox();
            this.cornerRadTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.uStartCheck = new System.Windows.Forms.CheckBox();
            this.blockFromLoadCheck = new System.Windows.Forms.CheckBox();
            this.blockFromSwingCheck = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this._steeredModeAccelerChBox = new System.Windows.Forms.CheckBox();
            this.urovCheck = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this._damageFaza = new System.Windows.Forms.CheckBox();
            this._resetStep = new System.Windows.Forms.CheckBox();
            this.blockFromTnCmb = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.apvCombo = new System.Windows.Forms.ComboBox();
            this.avrCombo = new System.Windows.Forms.ComboBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // uStartMaskedText
            // 
            this.uStartMaskedText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uStartMaskedText.Location = new System.Drawing.Point(403, 3);
            this.uStartMaskedText.Name = "uStartMaskedText";
            this.uStartMaskedText.Size = new System.Drawing.Size(145, 20);
            this.uStartMaskedText.TabIndex = 3;
            // 
            // tyTextBox
            // 
            this.tyTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tyTextBox.Location = new System.Drawing.Point(113, 248);
            this.tyTextBox.Name = "tyTextBox";
            this.tyTextBox.Size = new System.Drawing.Size(144, 20);
            this.tyTextBox.TabIndex = 3;
            // 
            // icpTextBox
            // 
            this.icpTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.icpTextBox.Location = new System.Drawing.Point(113, 197);
            this.icpTextBox.Name = "icpTextBox";
            this.icpTextBox.Size = new System.Drawing.Size(144, 20);
            this.icpTextBox.TabIndex = 3;
            // 
            // tcpTextBox
            // 
            this.tcpTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcpTextBox.Location = new System.Drawing.Point(113, 171);
            this.tcpTextBox.Name = "tcpTextBox";
            this.tcpTextBox.Size = new System.Drawing.Size(144, 20);
            this.tcpTextBox.TabIndex = 3;
            // 
            // oscilloscopeCombo
            // 
            this.oscilloscopeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.oscilloscopeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscilloscopeCombo.FormattingEnabled = true;
            this.oscilloscopeCombo.Location = new System.Drawing.Point(403, 223);
            this.oscilloscopeCombo.Name = "oscilloscopeCombo";
            this.oscilloscopeCombo.Size = new System.Drawing.Size(145, 21);
            this.oscilloscopeCombo.TabIndex = 2;
            // 
            // logicCombo
            // 
            this.logicCombo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logicCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.logicCombo.FormattingEnabled = true;
            this.logicCombo.Location = new System.Drawing.Point(403, 28);
            this.logicCombo.Name = "logicCombo";
            this.logicCombo.Size = new System.Drawing.Size(145, 21);
            this.logicCombo.TabIndex = 2;
            // 
            // directionCombo
            // 
            this.directionCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.directionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.directionCombo.FormattingEnabled = true;
            this.directionCombo.Location = new System.Drawing.Point(113, 274);
            this.directionCombo.Name = "directionCombo";
            this.directionCombo.Size = new System.Drawing.Size(144, 21);
            this.directionCombo.TabIndex = 2;
            // 
            // inpAccelerationCombo
            // 
            this.inpAccelerationCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.inpAccelerationCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.inpAccelerationCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.inpAccelerationCombo.FormattingEnabled = true;
            this.inpAccelerationCombo.Location = new System.Drawing.Point(113, 223);
            this.inpAccelerationCombo.Name = "inpAccelerationCombo";
            this.inpAccelerationCombo.Size = new System.Drawing.Size(144, 21);
            this.inpAccelerationCombo.TabIndex = 2;
            // 
            // blockCombo
            // 
            this.blockCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blockCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.blockCombo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.blockCombo.FormattingEnabled = true;
            this.blockCombo.Location = new System.Drawing.Point(113, 53);
            this.blockCombo.Name = "blockCombo";
            this.blockCombo.Size = new System.Drawing.Size(144, 21);
            this.blockCombo.TabIndex = 2;
            // 
            // typeCombo
            // 
            this.typeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeCombo.FormattingEnabled = true;
            this.typeCombo.Location = new System.Drawing.Point(113, 28);
            this.typeCombo.Name = "typeCombo";
            this.typeCombo.Size = new System.Drawing.Size(144, 21);
            this.typeCombo.TabIndex = 2;
            this.typeCombo.SelectedIndexChanged += new System.EventHandler(this.typeCombo_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label20.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label20, 2);
            this.label20.Location = new System.Drawing.Point(368, 277);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "АПВ";
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label19.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label19, 2);
            this.label19.Location = new System.Drawing.Point(360, 251);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(37, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "УРОВ";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label18, 2);
            this.label18.Location = new System.Drawing.Point(321, 226);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Осциллограф";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label17, 2);
            this.label17.Location = new System.Drawing.Point(304, 116);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Блок. от качания";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label16.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label16, 2);
            this.label16.Location = new System.Drawing.Point(299, 83);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Блок. от нагрузки";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label15.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label15, 2);
            this.label15.Location = new System.Drawing.Point(288, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Блок. от неиспр. ТН";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label14.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label14, 2);
            this.label14.Location = new System.Drawing.Point(355, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Контур";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(326, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Uпуск, В";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 277);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Направление";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(72, 251);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "ty, мс";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Вход ускорения";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(70, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Icp, In";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(65, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "tcp, мс";
            // 
            // _frLabel
            // 
            this._frLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._frLabel.AutoSize = true;
            this._frLabel.Location = new System.Drawing.Point(68, 148);
            this._frLabel.Name = "_frLabel";
            this._frLabel.Size = new System.Drawing.Size(39, 13);
            this._frLabel.TabIndex = 1;
            this._frLabel.Text = "f, град";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Блокировка";
            // 
            // modeCombo
            // 
            this.modeCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.modeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modeCombo.FormattingEnabled = true;
            this.modeCombo.Location = new System.Drawing.Point(113, 3);
            this.modeCombo.Name = "modeCombo";
            this.modeCombo.Size = new System.Drawing.Size(144, 21);
            this.modeCombo.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Тип";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Режим";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.modeCombo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.typeCombo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.blockCombo, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tcpTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.icpTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.inpAccelerationCombo, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.tyTextBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.panel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.directionCombo, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.uStartCheck, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label13, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.uStartMaskedText, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.logicCombo, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label15, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.blockFromLoadCheck, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label16, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.blockFromSwingCheck, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.label17, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label21, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this._steeredModeAccelerChBox, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.label20, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.urovCheck, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.label19, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label18, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label22, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label23, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.oscilloscopeCombo, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this._damageFaza, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this._resetStep, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.blockFromTnCmb, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this._frLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label24, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 11);
            this.tableLayoutPanel1.Controls.Add(this.apvCombo, 4, 10);
            this.tableLayoutPanel1.Controls.Add(this.avrCombo, 4, 11);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(551, 356);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel, 2);
            this.panel.Controls.Add(this.panel1);
            this.panel.Controls.Add(this.panel2);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(3, 78);
            this.panel.Name = "panel";
            this.tableLayoutPanel1.SetRowSpan(this.panel, 2);
            this.panel.Size = new System.Drawing.Size(254, 61);
            this.panel.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.rTextBox1);
            this.panel1.Controls.Add(this.xTextBox1);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(252, 59);
            this.panel1.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "X, Ом втор.";
            // 
            // rTextBox1
            // 
            this.rTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rTextBox1.Location = new System.Drawing.Point(107, 6);
            this.rTextBox1.Name = "rTextBox1";
            this.rTextBox1.Size = new System.Drawing.Size(144, 20);
            this.rTextBox1.TabIndex = 6;
            // 
            // xTextBox1
            // 
            this.xTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xTextBox1.Location = new System.Drawing.Point(107, 35);
            this.xTextBox1.Name = "xTextBox1";
            this.xTextBox1.Size = new System.Drawing.Size(144, 20);
            this.xTextBox1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "R, Ом втор.";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this._xl1);
            this.panel2.Controls.Add(this.rTextBox2);
            this.panel2.Controls.Add(this.xTextBox2);
            this.panel2.Controls.Add(this._rl1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(255, 61);
            this.panel2.TabIndex = 1;
            // 
            // _xl1
            // 
            this._xl1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._xl1.AutoSize = true;
            this._xl1.Location = new System.Drawing.Point(26, 38);
            this._xl1.Name = "_xl1";
            this._xl1.Size = new System.Drawing.Size(66, 13);
            this._xl1.TabIndex = 4;
            this._xl1.Text = "X, Ом перв.";
            // 
            // rTextBox2
            // 
            this.rTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rTextBox2.Location = new System.Drawing.Point(100, 6);
            this.rTextBox2.Name = "rTextBox2";
            this.rTextBox2.Size = new System.Drawing.Size(152, 20);
            this.rTextBox2.TabIndex = 6;
            // 
            // xTextBox2
            // 
            this.xTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xTextBox2.Location = new System.Drawing.Point(100, 35);
            this.xTextBox2.Name = "xTextBox2";
            this.xTextBox2.Size = new System.Drawing.Size(152, 20);
            this.xTextBox2.TabIndex = 7;
            // 
            // _rl1
            // 
            this._rl1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._rl1.AutoSize = true;
            this._rl1.Location = new System.Drawing.Point(26, 9);
            this._rl1.Name = "_rl1";
            this._rl1.Size = new System.Drawing.Size(67, 13);
            this._rl1.TabIndex = 5;
            this._rl1.Text = "R, Ом перв.";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cornerRadTextBox2);
            this.panel3.Controls.Add(this.cornerRadTextBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(113, 145);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(144, 20);
            this.panel3.TabIndex = 7;
            // 
            // cornerRadTextBox2
            // 
            this.cornerRadTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cornerRadTextBox2.Location = new System.Drawing.Point(0, 0);
            this.cornerRadTextBox2.Name = "cornerRadTextBox2";
            this.cornerRadTextBox2.Size = new System.Drawing.Size(144, 20);
            this.cornerRadTextBox2.TabIndex = 5;
            this.cornerRadTextBox2.Visible = false;
            // 
            // cornerRadTextBox1
            // 
            this.cornerRadTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cornerRadTextBox1.Location = new System.Drawing.Point(0, 0);
            this.cornerRadTextBox1.Name = "cornerRadTextBox1";
            this.cornerRadTextBox1.Size = new System.Drawing.Size(144, 20);
            this.cornerRadTextBox1.TabIndex = 4;
            // 
            // uStartCheck
            // 
            this.uStartCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uStartCheck.AutoSize = true;
            this.uStartCheck.Location = new System.Drawing.Point(383, 3);
            this.uStartCheck.Name = "uStartCheck";
            this.uStartCheck.Size = new System.Drawing.Size(14, 19);
            this.uStartCheck.TabIndex = 4;
            this.uStartCheck.UseVisualStyleBackColor = true;
            // 
            // blockFromLoadCheck
            // 
            this.blockFromLoadCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blockFromLoadCheck.AutoSize = true;
            this.blockFromLoadCheck.Location = new System.Drawing.Point(403, 78);
            this.blockFromLoadCheck.Name = "blockFromLoadCheck";
            this.blockFromLoadCheck.Size = new System.Drawing.Size(145, 23);
            this.blockFromLoadCheck.TabIndex = 4;
            this.blockFromLoadCheck.UseVisualStyleBackColor = true;
            // 
            // blockFromSwingCheck
            // 
            this.blockFromSwingCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blockFromSwingCheck.AutoSize = true;
            this.blockFromSwingCheck.Location = new System.Drawing.Point(403, 107);
            this.blockFromSwingCheck.Name = "blockFromSwingCheck";
            this.blockFromSwingCheck.Size = new System.Drawing.Size(145, 32);
            this.blockFromSwingCheck.TabIndex = 4;
            this.blockFromSwingCheck.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label21.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label21, 2);
            this.label21.Location = new System.Drawing.Point(281, 148);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(116, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Ненаправ. при ускор.";
            // 
            // _steeredModeAccelerChBox
            // 
            this._steeredModeAccelerChBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._steeredModeAccelerChBox.AutoSize = true;
            this._steeredModeAccelerChBox.Location = new System.Drawing.Point(403, 145);
            this._steeredModeAccelerChBox.Name = "_steeredModeAccelerChBox";
            this._steeredModeAccelerChBox.Size = new System.Drawing.Size(145, 20);
            this._steeredModeAccelerChBox.TabIndex = 8;
            this._steeredModeAccelerChBox.UseVisualStyleBackColor = true;
            // 
            // urovCheck
            // 
            this.urovCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.urovCheck.AutoSize = true;
            this.urovCheck.Location = new System.Drawing.Point(403, 248);
            this.urovCheck.Name = "urovCheck";
            this.urovCheck.Size = new System.Drawing.Size(145, 20);
            this.urovCheck.TabIndex = 4;
            this.urovCheck.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label22.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label22, 2);
            this.label22.Location = new System.Drawing.Point(321, 174);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(76, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Пуск от ОПФ";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label23.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label23, 2);
            this.label23.Location = new System.Drawing.Point(270, 200);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(127, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Сброс 1фКЗ при мфКЗ*";
            // 
            // _damageFaza
            // 
            this._damageFaza.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._damageFaza.AutoSize = true;
            this._damageFaza.Location = new System.Drawing.Point(403, 171);
            this._damageFaza.Name = "_damageFaza";
            this._damageFaza.Size = new System.Drawing.Size(145, 20);
            this._damageFaza.TabIndex = 8;
            this._damageFaza.UseVisualStyleBackColor = true;
            // 
            // _resetStep
            // 
            this._resetStep.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._resetStep.AutoSize = true;
            this._resetStep.Location = new System.Drawing.Point(403, 197);
            this._resetStep.Name = "_resetStep";
            this._resetStep.Size = new System.Drawing.Size(145, 20);
            this._resetStep.TabIndex = 8;
            this._resetStep.UseVisualStyleBackColor = true;
            // 
            // blockFromTnCmb
            // 
            this.blockFromTnCmb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.blockFromTnCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.blockFromTnCmb.FormattingEnabled = true;
            this.blockFromTnCmb.Location = new System.Drawing.Point(403, 53);
            this.blockFromTnCmb.Name = "blockFromTnCmb";
            this.blockFromTnCmb.Size = new System.Drawing.Size(145, 21);
            this.blockFromTnCmb.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label24, 3);
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(263, 325);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(285, 31);
            this.label24.TabIndex = 1;
            this.label24.Text = "* - сброс ступеней в режиме Ф-N при переходе однофазного КЗ в междуфазное.";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label12.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label12, 2);
            this.label12.Location = new System.Drawing.Point(369, 304);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "АВР";
            // 
            // apvCombo
            // 
            this.apvCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apvCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.apvCombo.FormattingEnabled = true;
            this.apvCombo.Location = new System.Drawing.Point(403, 274);
            this.apvCombo.Name = "apvCombo";
            this.apvCombo.Size = new System.Drawing.Size(145, 21);
            this.apvCombo.TabIndex = 2;
            // 
            // avrCombo
            // 
            this.avrCombo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.avrCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.avrCombo.FormattingEnabled = true;
            this.avrCombo.Location = new System.Drawing.Point(403, 299);
            this.avrCombo.Name = "avrCombo";
            this.avrCombo.Size = new System.Drawing.Size(145, 21);
            this.avrCombo.TabIndex = 2;
            // 
            // ResistanceDefTabPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ResistanceDefTabPage";
            this.Size = new System.Drawing.Size(557, 362);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox uStartMaskedText;
        private System.Windows.Forms.MaskedTextBox tyTextBox;
        private System.Windows.Forms.MaskedTextBox icpTextBox;
        private System.Windows.Forms.MaskedTextBox tcpTextBox;
        private System.Windows.Forms.ComboBox oscilloscopeCombo;
        private System.Windows.Forms.ComboBox logicCombo;
        private System.Windows.Forms.ComboBox directionCombo;
        private System.Windows.Forms.ComboBox inpAccelerationCombo;
        private System.Windows.Forms.ComboBox blockCombo;
        private System.Windows.Forms.ComboBox typeCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label _frLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox modeCombo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox uStartCheck;
        private System.Windows.Forms.CheckBox blockFromLoadCheck;
        private System.Windows.Forms.CheckBox blockFromSwingCheck;
        private System.Windows.Forms.CheckBox urovCheck;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label _xl1;
        private System.Windows.Forms.MaskedTextBox rTextBox2;
        private System.Windows.Forms.MaskedTextBox xTextBox2;
        private System.Windows.Forms.Label _rl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox rTextBox1;
        private System.Windows.Forms.MaskedTextBox xTextBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.MaskedTextBox cornerRadTextBox2;
        private System.Windows.Forms.MaskedTextBox cornerRadTextBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox _steeredModeAccelerChBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox _damageFaza;
        private System.Windows.Forms.CheckBox _resetStep;
        private System.Windows.Forms.ComboBox blockFromTnCmb;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox apvCombo;
        private System.Windows.Forms.ComboBox avrCombo;

    }
}
