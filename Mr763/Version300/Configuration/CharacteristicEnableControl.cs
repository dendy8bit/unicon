﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace BEMN.Mr763.Version300.Configuration
{
    public partial class CharacteristicEnableControl : UserControl
    {
        private ICharacteristic _characteristic;

        
        public ICharacteristic CurrentCharacterictic
        {
            get { return this._characteristic; }
            set { this._characteristic = value;
                if (this._characteristic == null)
                {
                    this.Visible = false;
                    return;
                }
                this.Visible = true;
                if (this._characteristic.Enable)
                {
                    this.panel.BackColor = this._characteristic.Color; 
                }
                else
                {
                    this.panel.BackColor = Color.Gray;
                }
               
                this.labelZ1.Text = this._characteristic.Name;
            }
        }
        public CharacteristicEnableControl()
        {
            this.InitializeComponent();
        }

        public event Action Change;
        private void panel_Click(object sender, EventArgs e)
        {
            this._characteristic.Enable = !this._characteristic.Enable;
            this.CurrentCharacterictic = this._characteristic;
            if (this.Change!= null)
            {
                this.Change.Invoke();
            }

        }

        public event Action<int> SelectStage;

        private void labelZ1_Click(object sender, EventArgs e)
        {
            try
            {
                int res =  int.Parse(this._characteristic.Name.Split(' ').Last());
                if (this.SelectStage != null)
                {
                    this.SelectStage.Invoke(res);
                }
            }
            catch (Exception)
            {
                

            }
        }
    }
}
