﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using BEMN.Devices.Structures;
using BEMN.Forms.ValidatingClasses;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Forms.ValidatingClasses.Rules.Ushort;
using BEMN.Mr763.Version300.Configuration.Structures.AcCountLoad;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Z;
using BEMN.Mr763.Version300.Configuration.Structures.Swing;

namespace BEMN.Mr763.Version300.Configuration
{
    public partial class ResistanceDefTabCtr : UserControl
    {
        #region Const
        private const string PRIMARY_R = "R, Ом перв.";
        private const string PRIMARY_X = "X, Ом перв.";
        private const string PRIMORY_FR = "f, град/r, Ом перв";
        private const string SECOND_R = "R, Ом втор.";
        private const string SECOND_X = "X, Ом втор.";
        private const string SECOND_FR = "f, град/r, Ом втор";
        #endregion

        #region Fields

        private CalcKoefForm _calcForm;
        private MaskedTextBox[] _primary;
        private MaskedTextBox[] _second;
        private GroupBox[] _primaryGrBox;
        private GroupBox[] _secondGrBox;
        private double _koef;
        private bool _isKryg;
        private bool _isPrimary;
        /// <summary>
        /// Валидатор всех защит по сопротивлению
        /// </summary>
        private StructUnion<AllResistanceDefensesStruct> _resistanceUnion;
        private StructUnion<AllResistanceDefensesStructNew> _resistanceUnionNew;
        /// <summary>
        /// Валидатор сопротивлений для определения коэфициэнтов компенсации тока нулевой последоватеоьности
        /// и углов для определения направления мощности для защит по сопротивлению
        /// </summary>
        private NewStructValidator<ResistanceStruct> _resistValidatorGr1;
        /// <summary>
        /// Валидатор по учету нагрузки
        /// </summary>
        private NewStructValidator<AcCountLoadStruct> _loadValidatorGr1;
        private NewStructValidator<AcCountLoadStructNew> _loadValidatorGr1New;
        /// <summary>
        /// Качание
        /// </summary>
        private NewStructValidator<SwingStruct> _swingValidatorGr1;
        private ResistanceDefTabPage[] _resistPages;

        #endregion

        #region Constructor
        public ResistanceDefTabCtr()
        {
            this.InitializeComponent();

            if (StringsConfig.CurrentVersion >= 3.07)
            {
                this.settingsOpfGroupBox.Visible = true;
                this.loadFaz2.Visible = false;

                this._loadedGroup.Size = new Size(444, 61);
                this._infoGrid.Columns.Remove(Column6);

                this.label24.Visible = false;
            }

            this._second = new[]
            {
                this._rZ0Step1Gr1, this._xZ0Step1Gr1, this._rZ1Step1Gr1, this._xZ1Step1Gr1, 
                this._rZ0Step2Gr1, this._xZ0Step2Gr1, this._rZ1Step2Gr1, this._xZ1Step2Gr1,
                this._rZ0Step3Gr1, this._xZ0Step3Gr1, this._rZ1Step3Gr1, this._xZ1Step3Gr1,
                this._rZ0Step4Gr1, this._xZ0Step4Gr1, this._rZ1Step4Gr1, this._xZ1Step4Gr1,
                this._rZ0Step5Gr1, this._xZ0Step5Gr1, this._rZ1Step5Gr1, this._xZ1Step5Gr1,
                this._r1LinGr1, this._r2LinGr1, this._r1FazGr1, this._r2FazGr1, this._swingRGr1, this._swingXGr1, this._swingdzGr1
            };
            this._primary = new[]
            {
                this._rZ0Step1Gr2, this._xZ0Step1Gr2, this._rZ1Step1Gr2, this._xZ1Step1Gr2, 
                this._rZ0Step2Gr2, this._xZ0Step2Gr2, this._rZ1Step2Gr2, this._xZ1Step2Gr2,
                this._rZ0Step3Gr2, this._xZ0Step3Gr2, this._rZ1Step3Gr2, this._xZ1Step3Gr2,
                this._rZ0Step4Gr2, this._xZ0Step4Gr2, this._rZ1Step4Gr2, this._xZ1Step4Gr2,
                this._rZ0Step5Gr2, this._xZ0Step5Gr2, this._rZ1Step5Gr2, this._xZ1Step5Gr2,
                this._r1LinGr2, this._r2LinGr2, this._r1FazGr2, this._r2FazGr2, this._swingRGr2, this._swingXGr2, this._swingdzGr2
            };

            this._secondGrBox = new[]
            {
                this.fn1groupBox1, this.fn2groupBox1, this.fn3groupBox1, this.fn4groupBox1, this.fn5groupBox1, this.loadFaz1, this.loadLin1
            };
            this._primaryGrBox = new[]
            {
                this.fn1groupBox2, this.fn2groupBox2, this.fn3groupBox2, this.fn4groupBox2, this.fn5groupBox2, this.loadFaz2, this.loadLin2
            };
        }

        public void Initialization()
        {
            var currentDefenseFunc1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new SignValidatingRule(-256.00, 256.00);
                    }
                    return RulesContainer.Ustavka256;
                }
                catch (Exception)
                {
                    return RulesContainer.Ustavka256;
                }
            });

            var currentDefenseFunc2 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return RulesContainer.Ustavka256;
                    }
                    return new CustomUshortRule(0, 89);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(0, 89);
                }
            });
            var currentFuncPrimary = new Func<IValidatingRule>(() => new CustomDoubleRule(0, 256 * this._koef));
            var currentFuncResistPrimary = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new SignValidatingRule(-256 * this._koef, 256 * this._koef);
                    }
                    else
                    {
                        return new CustomDoubleRule(0, 256 * this._koef);
                    }
                }
                catch (Exception)
                {
                    return new CustomDoubleRule(0, 256 * this._koef);
                }
            });

            var currentFuncResistPrimary1 = new Func<IValidatingRule>(() =>
            {
                try
                {
                    if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
                    {
                        return new CustomDoubleRule(0, 256 * this._koef);
                    }
                    return new CustomUshortRule(0, 89);
                }
                catch (Exception)
                {
                    return new CustomUshortRule(0, 89);
                }
            });

            ToolTip toolTip = new ToolTip();
            // Сопротивления для определения коэффициента
            this._resistValidatorGr1 = new NewStructValidator<ResistanceStruct>
                (
                toolTip,
                new ControlInfoText(this._rZ0Step1Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ0Step1Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ1Step1Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ1Step1Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ0Step2Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ0Step2Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ1Step2Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ1Step2Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ0Step3Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ0Step3Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ1Step3Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ1Step3Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ0Step4Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ0Step4Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ1Step4Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ1Step4Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ0Step5Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ0Step5Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._rZ1Step5Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._xZ1Step5Gr1, RulesContainer.Ustavka256),
                new ControlInfoText(this._corner1ForRGr1, new CustomUshortRule(0, 45)),
                new ControlInfoText(this._corner2ForRGr1, new CustomUshortRule(0, 45))
                );

            // Контроль нагрузки
            if (StringsConfig.CurrentVersion >= 3.07)
            {
                this._loadValidatorGr1New = new NewStructValidator<AcCountLoadStructNew>
                (
                    toolTip,
                    new ControlInfoCheck(this._settingsResetCB),
                    new ControlInfoText(this._settingsUminTB, RulesContainer.Ustavka256),
                    new ControlInfoText(this._settingsImaxTB, RulesContainer.Ustavka40),
                    new ControlInfoText(this._r1LinGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._r2LinGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._cornerLinGr1, new CustomUshortRule(0, 89))
                );
            }
            else
            {
                this._loadValidatorGr1 = new NewStructValidator<AcCountLoadStruct>
                (
                    toolTip,
                    new ControlInfoText(this._r1FazGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._r2FazGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._cornerFazGr1, new CustomUshortRule(0, 89)),
                    new ControlInfoText(this._r1LinGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._r2LinGr1, RulesContainer.Ustavka256),
                    new ControlInfoText(this._cornerLinGr1, new CustomUshortRule(0, 89))
                );
            }
            // Качание
            this._swingValidatorGr1 = new NewStructValidator<SwingStruct>(
                toolTip,
                new ControlInfoCombo(this._swingTypeGr1, StringsConfig.ResistDefType),
                new ControlInfoTextDependent(this._swingRGr1, currentDefenseFunc1),
                new ControlInfoTextDependent(this._swingXGr1, currentDefenseFunc1),
                new ControlInfoText(this._swingdzGr1, RulesContainer.Ustavka256),
                new ControlInfoTextDependent(this._swingfGr1, currentDefenseFunc2),
                new ControlInfoText(this._swingTGr1, new CustomIntRule(20, 3276700)),
                new ControlInfoText(this._swingI0Gr1, RulesContainer.Ustavka40),
                new ControlInfoCheck(this._swingYesNoTrGr1),
                new ControlInfoText(this._swingTrGr1, RulesContainer.TimeRule),
                new ControlInfoText(this._swingIGr1, RulesContainer.Ustavka40)
            );
            NewStructValidator<OneWordStruct> resisitValidSecond = new NewStructValidator<OneWordStruct>
                (
                toolTip,
                new ControlInfoTextDependent(this._rZ0Step1Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ0Step1Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ1Step1Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ1Step1Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ0Step2Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ0Step2Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ1Step2Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ1Step2Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ0Step3Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ0Step3Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ1Step3Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ1Step3Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ0Step4Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ0Step4Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ1Step4Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ1Step4Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ0Step5Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ0Step5Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._rZ1Step5Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._xZ1Step5Gr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._r1LinGr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._r2LinGr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._r1FazGr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._r1FazGr2, currentFuncPrimary),
                new ControlInfoTextDependent(this._swingRGr2, currentFuncResistPrimary),
                new ControlInfoTextDependent(this._swingXGr2, currentFuncResistPrimary),
                new ControlInfoTextDependent(this._swingdzGr2, currentFuncResistPrimary),
                new ControlInfoTextDependent(this._swingfGr2, currentFuncResistPrimary1)
                );

            #region [Resist Deffence]
            this._resistPages = new[]
            {
                this.resistanceDefTabPage1, this.resistanceDefTabPage2, this.resistanceDefTabPage3, this.resistanceDefTabPage4,
                this.resistanceDefTabPage5, this.resistanceDefTabPage6
            };
            foreach (ResistanceDefTabPage page in this._resistPages)
            {
                page.Initialization();
            }
            if (StringsConfig.CurrentVersion >= 3.07)
            {
                IValidator[] resistanceValidatorsNew = new[]
                {
                    this.resistanceDefTabPage1.ResistDefValidatorNew, this.resistanceDefTabPage2.ResistDefValidatorNew,
                    this.resistanceDefTabPage3.ResistDefValidatorNew, this.resistanceDefTabPage4.ResistDefValidatorNew,
                    this.resistanceDefTabPage5.ResistDefValidatorNew, this.resistanceDefTabPage6.ResistDefValidatorNew
                };
                this._resistanceUnionNew = new StructUnion<AllResistanceDefensesStructNew>(resistanceValidatorsNew);
            }
            else
            {
                IValidator[] resistanceValidators = new[]
                {
                    this.resistanceDefTabPage1.ResistDefValidator, this.resistanceDefTabPage2.ResistDefValidator,
                    this.resistanceDefTabPage3.ResistDefValidator, this.resistanceDefTabPage4.ResistDefValidator,
                    this.resistanceDefTabPage5.ResistDefValidator, this.resistanceDefTabPage6.ResistDefValidator
                };
                this._resistanceUnion = new StructUnion<AllResistanceDefensesStruct>(resistanceValidators);
            }
            #endregion
        }
        #endregion

        #region Properties

        public StructUnion<AllResistanceDefensesStructNew> ResistanceUnionNew
        {
            get { return this._resistanceUnionNew; }
        }

        public StructUnion<AllResistanceDefensesStruct> ResistanceUnion
        {
            get { return this._resistanceUnion; }
        }

        /// <summary>
        /// Валидатор сопротивлений для определения коэфициэнтов компенсации тока нулевой последоватеоьности
        /// и углов для определения направления мощности для защит по сопротивлению
        /// </summary>
        public NewStructValidator<ResistanceStruct> ResistValidator
        {
            get { return this._resistValidatorGr1; }
        }

        /// <summary>
        /// Валидатор по учету нагрузки
        /// </summary>
        public NewStructValidator<AcCountLoadStruct> LoadValidator
        {
            get { return this._loadValidatorGr1; }
        }

        public NewStructValidator<AcCountLoadStructNew> LoadValidatorNew
        {
            get { return this._loadValidatorGr1New; }
        }

        /// <summary>
        /// Качание
        /// </summary>
        public NewStructValidator<SwingStruct> SwingValidator
        {
            get { return this._swingValidatorGr1; }
        }

        public StructUnion<ConfigResistDiagramStruct> ResistConfigUnion { get; set; }

        public StructUnion<ConfigResistDiagramStructNew> ResistConfigUnionNew { get; set; }

        [Browsable(false)]
        public bool IsPrimary
        {
            get { return this._isPrimary; }
            set
            {
                this._isPrimary = value;
                if(this._resistPages == null) return; 
                for (int i = 0; i < this._secondGrBox.Length; i++)
                {
                    this._secondGrBox[i].Visible = !value;
                    this._primaryGrBox[i].Visible = value;
                }
                this.swPanel1.Visible = this._swingfGr1.Visible = !value;
                this.swPanel2.Visible = this._swingfGr2.Visible = value;
                foreach (var tabPage in this._resistPages)
                {
                    tabPage.IsPrimary = value;
                }
            }
        }

        #endregion

        #region Methods and event handlers
        /// <summary>
        /// Если пользователь находится на вкладке таблицы защит по сопротивлению, то при считывании
        /// с устройства новых значених, следует обновить данные таблицы
        /// </summary>
        public void RefreshResistInfoTable()
        {
            this.ResistTabControl_SelectedIndexChanged(this.ResistTabControl, new EventArgs());
        }

        private void _isMash_CheckedChanged(object sender, EventArgs e)
        {
            this.DrawPanel.Invalidate();
        }

        private void ResistTabControl_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            TabControl tabCtrl = sender as TabControl;
            if (tabCtrl == null || tabCtrl.SelectedIndex == tabCtrl.TabCount-2) return;
            this._infoGrid.Rows.Clear();
            for (int i = 0; i < this._resistPages.Length; i++)
            {
                if (this._resistPages[i].Mode != StringsConfig.DefenseModes[0])
                {
                    this.SetDgvRow(this._resistPages[i], i);
                }
            }
        }

        private void SetDgvRow(ResistanceDefTabPage page, int i)
        {
            if (StringsConfig.CurrentVersion >= 3.07)
            {
                this._infoGrid.Rows.Add
                (
                    string.Format("Z {0}", i + 1),
                    page.Mode,
                    page.Type,
                    page.Block,
                    page.R,
                    page.X,
                    page.CornerRadius,
                    page.Tcp,
                    page.Icp,
                    page.Accleration,
                    page.Tu,
                    page.Direction,
                    page.StartOnU,
                    page.Ustart,
                    page.Logic,
                    page.BlockFromTn,
                    page.BlockFromLoad,
                    page.BlockFromSwing,
                    page.SteeredModeAcceler,
                    page.DamageFaza,
                    page.Oscilloscope,
                    page.Urov,
                    page.Apv,
                    page.Avr
                );
            }
            else
            {
                this._infoGrid.Rows.Add
                (
                    string.Format("Z {0}", i + 1),
                    page.Mode,
                    page.Type,
                    page.Block,
                    page.R,
                    page.X,
                    page.CornerRadius,
                    page.Tcp,
                    page.Icp,
                    page.Accleration,
                    page.Tu,
                    page.Direction,
                    page.StartOnU,
                    page.Ustart,
                    page.Logic,
                    page.BlockFromTn,
                    page.BlockFromLoad,
                    page.BlockFromSwing, 
                    page.SteeredModeAcceler,
                    page.DamageFaza,
                    page.ResetStep,
                    page.Oscilloscope,
                    page.Urov,
                    page.Apv,
                    page.Avr
                );
            }
        }

        private void _infoGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var grid = sender as DataGridView;
            if (grid == null) return;
            var row = grid.Rows[e.RowIndex];
            int num = (int)row.Tag;
            this.ResistTabControl.SelectedIndex = num;
        }

        public void GetSecondValue(double koef)
        {
            this._header1.HeaderText = SECOND_R;
            this._header2.HeaderText = SECOND_X;
            this.Column7.HeaderText = SECOND_FR;
            if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this.label115.Text = "f, град.";
            }
            else
            {
                this.label115.Text = "r, Ом втор.";
            }
            double val;
            for (int i = 0; i < this._primary.Length; i++)
            {
                val = Math.Round(Convert.ToDouble(this._primary[i].Text) * koef, 2);
                this._second[i].Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this._swingfGr2.Text)*koef, 2);
                this._swingfGr1.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this._swingfGr1.Text = this._swingfGr2.Text;
            }
            foreach (var page in this._resistPages)
            {
                page.GetSecondValue(koef);
            }
        }

        public void GetPrimaryValue(double koef)
        {
            this._koef = koef;
            this._header1.HeaderText = PRIMARY_R;
            this._header2.HeaderText = PRIMARY_X;
            this.Column7.HeaderText = PRIMORY_FR;
            if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this.label115.Text = "f, град.";
            }
            else
            {
                this.label115.Text = "r, Ом перв.";
            }
            double val;
            for (int i = 0; i < this._second.Length; i++)
            {
                val = Math.Round(Convert.ToDouble(this._second[i].Text) * koef, 2);
                this._primary[i].Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[1])
            {
                val = Math.Round(Convert.ToDouble(this._swingfGr1.Text)*koef, 2);
                this._swingfGr2.Text = val.ToString("F", CultureInfo.CurrentCulture);
            }
            else
            {
                this._swingfGr2.Text = this._swingfGr1.Text;
            }
            foreach (var page in this._resistPages)
            {
                page.GetPrimaryValue(koef);
            }
        }

        private void _swingTypeGr1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (this._swingTypeGr1.SelectedItem.ToString() == StringsConfig.ResistDefType[0])
            {
                this.label115.Text = "f, град.";
            }
            else
            {
                if (this.IsPrimary)
                {
                    this.label115.Text = "r, Ом перв.";
                }
                else
                {
                    this.label115.Text = "r, Ом втор.";
                }
            }
        }

        private void CalcKoefClick(object sender, EventArgs e)
        {
            if (this._isPrimary)
            {
                double koef = 1/this._koef;
                double val;
                for (int i = 0; i < this._primary.Length; i++)
                {
                    val = Math.Round(Convert.ToDouble(this._primary[i].Text) * koef, 2);
                    this._second[i].Text = val.ToString("F", CultureInfo.CurrentCulture);
                }
            }
            ResistanceStruct resist = this._resistValidatorGr1.Get();
            if (this._calcForm == null || !this._calcForm.IsHandleCreated)
            {
                this._calcForm = new CalcKoefForm(resist) {TopMost = true};
            }
            this._calcForm.ShowDialog();
        }

        #endregion

        #region Draw Characteristic

        private void DrawPanel_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                CharacteristicEnableControl[] characteristicEnableControls = new CharacteristicEnableControl[]
                {
                    this.characteristicEnableControl1,
                    this.characteristicEnableControl2,
                    this.characteristicEnableControl3,
                    this.characteristicEnableControl4,
                    this.characteristicEnableControl5,
                    this.characteristicEnableControl6
                };

                if (StringsConfig.CurrentVersion >= 3.07)
                {
                    ConfigResistDiagramStructNew config = this.ResistConfigUnionNew.Get();
                    PieChartOptions options = config.GetPieChartOptions();

                    var r = (this.DrawPanel.Width - 100) / 2;
                    var p = options.Kachanie2.MaxPoint;
                    var mid = new Point(r + 50, r + 15);
                    for (int i = 0; i < characteristicEnableControls.Length; i++)
                    {
                        var a = options.Characteristics.FirstOrDefault(o =>
                        {
                            if (characteristicEnableControls[i].CurrentCharacterictic != null)
                            {
                                return o.Name == characteristicEnableControls[i].CurrentCharacterictic.Name;
                            }
                            else
                            {
                                return false;
                            }

                        });
                        if (a != null)
                        {
                            a.Enable = characteristicEnableControls[i].CurrentCharacterictic.Enable;
                        }
                    }

                    var g = this.DrawPanel.CreateGraphics();
                    g.Clear(Color.White);

                    for (int i = 0; i < characteristicEnableControls.Length; i++)
                    {
                        characteristicEnableControls[i].CurrentCharacterictic = i < options.Characteristics.Count
                            ? options.Characteristics[i]
                            : null;
                    }
                    double xMax = 0.0;
                    double yMax = 0.0;
                    foreach (var characteristic in options.Characteristics)
                    {
                        try
                        {
                            if (!characteristic.Enable)
                            {
                                continue;
                            }
                            string[] type = characteristic.ToString().Split(' ');
                            if (type[0] == StringsConfig.ResistDefType[1])
                            {
                                this._isKryg = true;
                            }
                            if (characteristic.MaxPoint.X > xMax && characteristic.MaxPoint.X > p.X)
                            {
                                xMax = characteristic.MaxPoint.X;
                            }
                            else
                            {
                                if (p.X > xMax)
                                {
                                    xMax = p.X;
                                }
                            }
                            if (characteristic.MaxPoint.Y > yMax && characteristic.MaxPoint.Y > p.Y)
                            {
                                yMax = characteristic.MaxPoint.Y;
                            }
                            else
                            {
                                if (p.Y > yMax)
                                {
                                    yMax = p.Y;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Неверная конфигурация " + characteristic.Name);
                            throw;
                        }
                    }
                    if (this._isMash.Checked && !this._isKryg)
                    {

                        if (yMax < 1.5)
                        {
                            yMax = 1;
                        }
                        else
                        {
                            yMax = this.mod3(yMax);
                        }
                        if (xMax == 0)
                        {
                            xMax = 1;
                        }
                        else
                        {
                            xMax = this.mod3(xMax);
                        }
                    }
                    else
                    {

                        yMax = this.mod3(yMax);

                        xMax = this.mod3(xMax);
                        yMax = Math.Max(xMax, yMax);
                        xMax = yMax;

                    }
                    double factorX1 = r / (double)xMax / 1.2;
                    double factorY1 = r / (double)yMax / 1.2;
                    this.Drowsetka(g, xMax, yMax, factorX1, factorY1, mid);

                    if (options.Kachanie.IsValid)
                    {
                        options.Kachanie.Draw(g, mid, factorX1, factorY1, r, false);
                        options.Kachanie2.Draw(g, mid, factorX1, factorY1, r, false);
                    }

                    foreach (ICharacteristic characteristic in options.Characteristics)
                    {
                        try
                        {
                            if (!characteristic.Enable)
                            {
                                continue;
                            }
                            g.ResetClip();
                            options.Corners.SetClip(g, mid, r, factorX1, factorY1, characteristic.MaxPointForCorner.X, characteristic.MaxPointForCorner.Y, characteristic.Direction);
                            if (characteristic.BlockFromLoad == Block.LINE)
                            {
                                options.Linear.SetClip(g, mid, factorX1, factorY1, r);
                            }
                            if (characteristic.BlockFromLoad == Block.PHASE)
                            {
                                options.Phase.SetClip(g, mid, factorX1, factorY1, r);
                            }

                            characteristic.Draw(g, mid, factorX1, factorY1, r, true);
                        }
                        catch (Exception)
                        {

                            MessageBox.Show("Неверная конфигурация " + characteristic.Name);
                            throw;
                        }

                    }
                }
                else
                {
                    ConfigResistDiagramStruct config = this.ResistConfigUnion.Get();
                    PieChartOptions options = config.GetPieChartOptions();
                    var r = (this.DrawPanel.Width - 100) / 2;
                    var p = options.Kachanie2.MaxPoint;
                    var mid = new Point(r + 50, r + 15);
                    for (int i = 0; i < characteristicEnableControls.Length; i++)
                    {
                        var a = options.Characteristics.FirstOrDefault(o =>
                        {
                            if (characteristicEnableControls[i].CurrentCharacterictic != null)
                            {
                                return o.Name == characteristicEnableControls[i].CurrentCharacterictic.Name;
                            }
                            else
                            {
                                return false;
                            }

                        });
                        if (a != null)
                        {
                            a.Enable = characteristicEnableControls[i].CurrentCharacterictic.Enable;
                        }
                    }

                    var g = this.DrawPanel.CreateGraphics();
                    g.Clear(Color.White);

                    for (int i = 0; i < characteristicEnableControls.Length; i++)
                    {
                        characteristicEnableControls[i].CurrentCharacterictic = i < options.Characteristics.Count
                            ? options.Characteristics[i]
                            : null;
                    }
                    double xMax = 0.0;
                    double yMax = 0.0;
                    foreach (var characteristic in options.Characteristics)
                    {
                        try
                        {
                            if (!characteristic.Enable)
                            {
                                continue;
                            }
                            string[] type = characteristic.ToString().Split(' ');
                            if (type[0] == StringsConfig.ResistDefType[1])
                            {
                                this._isKryg = true;
                            }
                            if (characteristic.MaxPoint.X > xMax && characteristic.MaxPoint.X > p.X)
                            {
                                xMax = characteristic.MaxPoint.X;
                            }
                            else
                            {
                                if (p.X > xMax)
                                {
                                    xMax = p.X;
                                }
                            }
                            if (characteristic.MaxPoint.Y > yMax && characteristic.MaxPoint.Y > p.Y)
                            {
                                yMax = characteristic.MaxPoint.Y;
                            }
                            else
                            {
                                if (p.Y > yMax)
                                {
                                    yMax = p.Y;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Неверная конфигурация " + characteristic.Name);
                            throw;
                        }
                    }
                    if (this._isMash.Checked && !this._isKryg)
                    {

                        if (yMax < 1.5)
                        {
                            yMax = 1;
                        }
                        else
                        {
                            yMax = this.mod3(yMax);
                        }
                        if (xMax == 0)
                        {
                            xMax = 1;
                        }
                        else
                        {
                            xMax = this.mod3(xMax);
                        }
                    }
                    else
                    {

                        yMax = this.mod3(yMax);

                        xMax = this.mod3(xMax);
                        yMax = Math.Max(xMax, yMax);
                        xMax = yMax;

                    }
                    double factorX1 = r / (double)xMax / 1.2;
                    double factorY1 = r / (double)yMax / 1.2;
                    this.Drowsetka(g, xMax, yMax, factorX1, factorY1, mid);

                    if (options.Kachanie.IsValid)
                    {
                        options.Kachanie.Draw(g, mid, factorX1, factorY1, r, false);
                        options.Kachanie2.Draw(g, mid, factorX1, factorY1, r, false);
                    }

                    foreach (ICharacteristic characteristic in options.Characteristics)
                    {
                        try
                        {
                            if (!characteristic.Enable)
                            {
                                continue;
                            }
                            g.ResetClip();
                            options.Corners.SetClip(g, mid, r, factorX1, factorY1, characteristic.MaxPointForCorner.X, characteristic.MaxPointForCorner.Y, characteristic.Direction);
                            if (characteristic.BlockFromLoad == Block.LINE)
                            {
                                options.Linear.SetClip(g, mid, factorX1, factorY1, r);
                            }
                            if (characteristic.BlockFromLoad == Block.PHASE)
                            {
                                options.Phase.SetClip(g, mid, factorX1, factorY1, r);
                            }

                            characteristic.Draw(g, mid, factorX1, factorY1, r, true);
                        }
                        catch (Exception)
                        {

                            MessageBox.Show("Неверная конфигурация " + characteristic.Name);
                            throw;
                        }

                    }
                }
                
               
            }
            catch (Exception)
            {
                // MessageBox.Show("Ошибка");
            }
            this._isKryg = false;
        }

        /// <summary>
        /// метод для корректного вывода максимального значения оси(%3=0)
        /// </summary>
        private double mod3(double x)
        {
            if (x < 1) return 1;
            var z = Math.Round(x, 0);
            while (z % 3 != 0)
            {
                z++;
            }
            return z;
        }

        public void Drowsetka(Graphics g,double xMax,double yMax,double factorX1,double factorY1,Point mid)
        {
            int baseCount = 3;
            Font f = new Font("Arial", 9);
            for (int i = -baseCount; i <= baseCount; i++)
            {
                if (i == 0)
                {
                    continue;
                }
                //по оси Х

                double a = Math.Round(xMax / (double)(baseCount), 1);
                double b = Math.Round(yMax / (double)(baseCount), 1);
                var valueX = a * i;
                var offsetX = factorX1 * valueX;
                var valueY = b * i;
                var offsetY = factorY1 * valueY;
                var thisWidth = g.MeasureString(valueX.ToString(), f);
                g.DrawString(valueX.ToString(), f, Brushes.Black, (int)offsetX + mid.X - thisWidth.Width / 2, (int)this.DrawPanel.Height / 2 + 7);
                // g.DrawLine(Pens.Black, (int)offset + mid.X, (int)DrawPanel.Height / 2 - 5, (int)offset + mid.X, (int)DrawPanel.Height / 2+5);

                Pen myPen = new Pen(Color.LightGray);
                //x
                g.DrawLine(myPen, new Point((int)offsetX + mid.X, (int)35), new Point((int)offsetX + mid.X, (int)this.DrawPanel.Height - 35));
                g.DrawLine(myPen, new Point((int)(35), (int)offsetY + mid.Y), new Point((int)(this.DrawPanel.Width - 35), (int)offsetY + mid.Y));
                //по оси У
                thisWidth = g.MeasureString((valueY * -1).ToString(), f);
                g.DrawString((valueY * -1).ToString(), f, Brushes.Black, mid.X - thisWidth.Width - 5, (int)offsetY + mid.Y - thisWidth.Height / 2);
            }

            g.DrawLine(Pens.Black, 35, this.DrawPanel.Height / 2, this.DrawPanel.Width - 35, this.DrawPanel.Height / 2);
            g.DrawLine(Pens.Black, this.DrawPanel.Width - 35, this.DrawPanel.Height / 2, this.DrawPanel.Width - 35 - 10, this.DrawPanel.Height / 2 + 5);
            g.DrawLine(Pens.Black, this.DrawPanel.Width - 35, this.DrawPanel.Height / 2, this.DrawPanel.Width - 35 - 10, this.DrawPanel.Height / 2 - 5);

            g.DrawString("+R, Ом втор.", f, Brushes.Black, this.DrawPanel.Width - 80, this.DrawPanel.Height / 2 - 20);

            g.DrawLine(Pens.Black, this.DrawPanel.Width / 2, 25, this.DrawPanel.Width / 2, this.DrawPanel.Height - 35);
            g.DrawLine(Pens.Black, this.DrawPanel.Width / 2, 25, this.DrawPanel.Width / 2 - 5, 35);
            g.DrawLine(Pens.Black, this.DrawPanel.Width / 2, 25, this.DrawPanel.Width / 2 + 5, 35);
            g.DrawString("+jX, Ом втор.", f, Brushes.Black, this.DrawPanel.Width / 2 + 10, 20);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.DrawPanel.Invalidate();
        }

        private void characteristicEnableControl1_Change()
        {
            this.DrawPanel.Invalidate();
        }

        private void characteristicEnableControl1_SelectStage(int ind)
        {
            this.ResistTabControl.SelectedIndex = ind;
        }

        private void saveResistParamsBtn_Click(object sender, EventArgs e)
        {
            string mes;
            if (StringsConfig.CurrentVersion >= 3.07 ? this.ResistConfigUnionNew.Check(out mes, false) : this.ResistConfigUnion.Check(out mes, false))
            {
                if (StringsConfig.CurrentVersion >= 3.07)
                {
                    var config = this.ResistConfigUnionNew.Get();
                    if (config.Ittl == 0)
                    {
                        MessageBox.Show(
                            "Файл не будет сохранён: значение ITTф не может быть равным 0, исправте на корректное");
                        return;
                    }
                    this.saveFileResistCharacteristic.FileName =
                        "ResistanseConfig"; // string.Format("{0}\\ResistanseConfig", Environment.CurrentDirectory);
                    if (this.saveFileResistCharacteristic.ShowDialog() != DialogResult.OK) return;
                    PieChartOptions options = config.GetPieChartOptions();
                    if (options.Characteristics != null)
                    {
                        foreach (var har in options.Characteristics)
                        {
                            har.ToFirst(config.KoefMisha);
                        }
                    }

                    options.Linear?.ToFirst(config.KoefMisha);
                    options.Phase?.ToFirst(config.KoefMisha);
                    options.Kachanie?.ToFirst(config.KoefMisha);
                    options.Kachanie2?.ToFirst(config.KoefMisha);

                    options.Save(this.saveFileResistCharacteristic.FileName);
                    MessageBox.Show("Конфигурация сохранена успешно", "Сохранение конфигурации",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var config = this.ResistConfigUnion.Get();
                    if (config.Ittl == 0)
                    {
                        MessageBox.Show(
                            "Файл не будет сохранён: значение ITTф не может быть равным 0, исправте на корректное");
                        return;
                    }
                    this.saveFileResistCharacteristic.FileName =
                        "ResistanseConfig"; // string.Format("{0}\\ResistanseConfig", Environment.CurrentDirectory);
                    if (this.saveFileResistCharacteristic.ShowDialog() != DialogResult.OK) return;
                    PieChartOptions options = config.GetPieChartOptions();
                    if (options.Characteristics != null)
                    {
                        foreach (var har in options.Characteristics)
                        {
                            har.ToFirst(config.KoefMisha);
                        }
                    }

                    options.Linear?.ToFirst(config.KoefMisha);
                    options.Phase?.ToFirst(config.KoefMisha);
                    options.Kachanie?.ToFirst(config.KoefMisha);
                    options.Kachanie2?.ToFirst(config.KoefMisha);

                    options.Save(this.saveFileResistCharacteristic.FileName);
                    MessageBox.Show("Конфигурация сохранена успешно", "Сохранение конфигурации",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Ошибка сохранения конфигурации. Проверьте правильность уставок", "Ошибка сохранения",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion
    }
}
