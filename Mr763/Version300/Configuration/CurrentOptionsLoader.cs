﻿using System;
using BEMN.Devices;
using BEMN.MR761.Version300.Configuration.Structures.MeasuringTransformer;

namespace BEMN.MR761.Version300.Configuration
{
    /// <summary>
    /// Загружает уставки токов(Iтт) и напряжений (Ктн)
    /// </summary>
    public class CurrentOptionsLoader
    {
        #region [Private fields]
        private const int COUNT_GROUPS = 6;
        private const ushort START_ADDR_MEAS_TRANS = 0x1278;
        private const ushort GROUP_SETPOINT_SIZE = 0x0428;

        private  MemoryEntity<MeasureTransStruct>[] _connections;
        private int _numberOfGroup;
        #endregion [Private fields]

        public MeasureTransStruct this[int index]
        {
            get { return this._connections[index].Value; }
        }

        #region [Events]

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        public event Action LoadOk;

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        public event Action LoadFail;
        private MR761 _device;

        #endregion [Events]


        #region [Ctor's]

        public CurrentOptionsLoader(MR761 device)
        {
            this._device = device;
            this._connections = new MemoryEntity<MeasureTransStruct>[COUNT_GROUPS];
            this._numberOfGroup = 0;
            for (int i = 0; i < this._connections.Length; i++)
            {
                this._connections[i] =
                    new MemoryEntity<MeasureTransStruct>(string.Format("Параметры измерительного трансформатора гр{0}", i + 1),
                        this._device.MB, this.GetStartAddrMeasTrans(i));
                this._connections[i].AllReadOk += this._connections_AllReadOk;
                this._connections[i].AllReadFail += this._connections_AllReadFail;
                this._connections[i].DeviceNum = device.DeviceNumber;
            }
            device.DeviceNumberChanged += this.OnDeviceNumberChanged;
        }

        public ushort GetStartAddrMeasTrans(int group)
        {
            if (string.IsNullOrEmpty(this._device.DeviceVersion)) return START_ADDR_MEAS_TRANS;
            return (ushort)(START_ADDR_MEAS_TRANS + GROUP_SETPOINT_SIZE * group);
        }

        #endregion [Ctor's]


        #region [Memory Entity Events Handlers]

        /// <summary>
        /// Загрузка прошла успешно
        /// </summary>
        private void _connections_AllReadFail(object sender)
        {
            if (this.LoadFail != null)
            {
                this.LoadFail.Invoke();
            }
        }

        /// <summary>
        /// Невозможно загрузить
        /// </summary>
        private void _connections_AllReadOk(object sender)
        {
            switch (this._numberOfGroup)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    this._connections[++this._numberOfGroup].LoadStruct();
                    break;
                case 5:
                    if (this.LoadOk != null)
                    {
                        this.LoadOk.Invoke();
                    }
                    break;
            }
        }

        #endregion [Memory Entity Events Handlers]


        #region [Methods]

        /// <summary>
        /// Запускает загрузку уставок токов(Iтт) и напряжений(Ктн), начиная с первой группы
        /// </summary>
        public void StartRead()
        {
            this._numberOfGroup = 0;
            this._connections[0].LoadStruct();
        }
        
        private void OnDeviceNumberChanged(object sender, byte old, byte newNum)
        {
            if (old == newNum) return;
            foreach (var entity in this._connections)
            {
                entity.DeviceNum = newNum;
            }
        }
        #endregion [Methods]
    }
}
