﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Mr763.Version300.Configuration.Structures.AcCountLoad;
using BEMN.Mr763.Version300.Configuration.Structures.Apv;
using BEMN.Mr763.Version300.Configuration.Structures.Avr;
using BEMN.Mr763.Version300.Configuration.Structures.CheckTn;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Z;
using BEMN.Mr763.Version300.Configuration.Structures.Engine;
using BEMN.Mr763.Version300.Configuration.Structures.Ls;
using BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr763.Version300.Configuration.Structures.Omp;
using BEMN.Mr763.Version300.Configuration.Structures.Sihronizm;
using BEMN.Mr763.Version300.Configuration.Structures.Swing;
using BEMN.Mr763.Version300.Configuration.Structures.Vls;

namespace BEMN.Mr763.Version300.Configuration.Structures
{
    public class GroupSetpointStruct : StructBase
    {
        #region Fields
        [Layout(0)] private DefensesSetpointsStruct _defensesSetpoints;
        [Layout(1)] private AvrStruct _avr;
        [Layout(2)] private ResistanceStruct _resistanceParam;
        [Layout(3)] private AcCountLoadStruct _acCountLoad;
        [Layout(4)] private CheckTnStruct _checkTn;
        [Layout(5)] private SwingStruct _swing;
        [Layout(6)] private ApvStruct _apv;
        [Layout(7)] private TermConfigStruct _termConfig;
        [Layout(8)] private MeasureTransStructV3 _measureTrans;
        [Layout(9)] private AllInputLogicStruct _inputLogicSignal;
        [Layout(10)] private AllOutputLogicSignalStruct _outputLogicSignal;
        [Layout(11)] private SinhronizmStruct _sinhronizm;
        [Layout(12)] private ConfigurationOpmStruct _omp;
        #endregion

        #region Property
        [BindingProperty(0)]
        public DefensesSetpointsStruct DefensesSetpoints
        {
            get { return this._defensesSetpoints; }
            set { this._defensesSetpoints = value; }
        }
        
        [BindingProperty(1)]
        public AvrStruct Avr
        {
            get { return this._avr; } 
            set { this._avr = value; }
        }

        [BindingProperty(2)]
        public ResistanceStruct ResistanceParam
        {
            get { return this._resistanceParam; }
            set { this._resistanceParam = value; }
        }

        [BindingProperty(3)]
        public AcCountLoadStruct AcCountLoad
        {
            get { return this._acCountLoad; }
            set { this._acCountLoad = value; }
        }

        [BindingProperty(4)]
        public CheckTnStruct CheckTn
        {
            get { return this._checkTn; }
            set { this._checkTn = value; }
        }

        [BindingProperty(5)]
        public SwingStruct Swing
        {
            get { return this._swing; }
            set { this._swing = value; }
        }

        [BindingProperty(6)]
        public ApvStruct Apv
        {
            get { return this._apv; }
            set { this._apv = value; }
        }

        [BindingProperty(7)]
        public TermConfigStruct TermConfig
        {
            get { return this._termConfig; }
            set { this._termConfig = value; }
        }

        [BindingProperty(8)]
        public MeasureTransStructV3 MeasureTrans
        {
            get { return this._measureTrans; }
            set { this._measureTrans = value; }
        }

        [BindingProperty(9)]
        public AllInputLogicStruct InputLogicSignal
        {
            get { return this._inputLogicSignal; }
            set { this._inputLogicSignal = value; }
        }

        [BindingProperty(10)]
        public AllOutputLogicSignalStruct OutputLogicSignal
        {
            get { return this._outputLogicSignal; }
            set { this._outputLogicSignal = value; }
        }

        [BindingProperty(11)]
        public SinhronizmStruct Sinhronizm
        {
            get { return this._sinhronizm; }
            set { this._sinhronizm = value; }
        }

        [BindingProperty(12)]
        public ConfigurationOpmStruct Omp
        {
            get { return this._omp; }
            set { this._omp = value; }
        }
        #endregion Property
    }
}
