﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Dugovaya;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.External;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.F;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.I;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.I2I1;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Istar;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.P;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Q;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.U;
using BEMN.Mr763.Version300.Configuration.Structures.Defenses.Z;

namespace BEMN.Mr763.Version300.Configuration.Structures.Defenses
{
    /// <summary>
    /// список защит
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class DefensesSetpointsStruct : StructBase
    {
        [Layout(0)] private CornerStruct _corner;                           //углы
        [Layout(1)] private AllMtzMainStruct _mtzmain;                      //мтз основная
        [Layout(2)] private AllDefenseStarStruct _mtzmaini0;                //мтз I*
        [Layout(3)] private DefenseI2I1Struct _mtzi2I1;                     //обрыв провода
        [Layout(4)] private DugovDefense _dugDef;                           //дуговая
        [Layout(5)] private AllDefenceUStruct _uDefences;                   //защиты по напряжению
        [Layout(6)] private AllDefenseFStruct _fB;                          //защиты по частоте >
        [Layout(7)] private AllDefenseFStruct _fM;                          //защиты по частоте <
        [Layout(8)] private AllDefenseQStruct _qDefenses;                   //мтз Q>
        [Layout(9)] private DefenseTermBlockStruct _termblock;              //блокировка по тепловой модели
        [Layout(10)] private AllDefenseExternalStruct _allExternalDefenses; //Внешние защиты
        [Layout(11)] private AllResistanceDefensesStruct _allResistDefensec;//Защиты по сопротивлению
        [Layout(12)] private AllDefenseReversPower _pDefense;               //Защиты по обратной мощности

        [BindingProperty(0)]
        [XmlElement(ElementName = "Углы")]
        public CornerStruct Corner
        {
            get { return this._corner; }
            set { this._corner = value; }
        }
        [BindingProperty(1)]
        [XmlElement(ElementName = "I")]
        public AllMtzMainStruct Mtzmain
        {
            get { return this._mtzmain; }
            set { this._mtzmain = value; }
        }
        [BindingProperty(2)]
        [XmlElement(ElementName = "I*")]
        public AllDefenseStarStruct Mtzmaini0
        {
            get { return this._mtzmaini0; }
            set { this._mtzmaini0 = value; }
        }
        [BindingProperty(3)]
        [XmlElement(ElementName = "I2I1")]
        public DefenseI2I1Struct Mtzi2I1
        {
            get { return this._mtzi2I1; }
            set { this._mtzi2I1 = value; }
        }

        [BindingProperty(4)]
        [XmlElement(ElementName = "Дуговая")]
        public DugovDefense DugDef
        {
            get { return this._dugDef; }
            set { this._dugDef = value; }
        }

        [BindingProperty(5)]
        [XmlElement(ElementName = "U")]
        public AllDefenceUStruct UDefences
        {
            get { return this._uDefences; }
            set { this._uDefences = value; }
        }
        [BindingProperty(6)]
        [XmlElement(ElementName = "Fb")]
        public AllDefenseFStruct Fb
        {
            get { return this._fB; }
            set { this._fB = value; }
        }
        [BindingProperty(7)]
        [XmlElement(ElementName = "Fm")]
        public AllDefenseFStruct Fm
        {
            get { return this._fM; }
            set { this._fM = value; }
        }
        [BindingProperty(8)]
        [XmlElement(ElementName = "Q")]
        public AllDefenseQStruct QDefenses
        {
            get { return this._qDefenses; }
            set { this._qDefenses = value; }
        }
        [BindingProperty(9)]
        [XmlElement(ElementName = "Тепловая")]
        public DefenseTermBlockStruct Termblock
        {
            get { return this._termblock; }
            set { this._termblock = value; }
        }
        [BindingProperty(10)]
        [XmlElement(ElementName = "Внешние")]
        public AllDefenseExternalStruct ExternalDefenses
        {
            get { return this._allExternalDefenses; }
            set { this._allExternalDefenses = value; }
        }

        [BindingProperty(11)]
        [XmlElement(ElementName = "По_сопротивлению")]
        public AllResistanceDefensesStruct ResistanceDefenses
        {
            get { return this._allResistDefensec; }
            set { this._allResistDefensec = value; }
        }
        [BindingProperty(12)]
        [XmlElement(ElementName = "P")]
        public AllDefenseReversPower DefenseReversPower
        {
            get { return this._pDefense; }
            set { this._pDefense = value; }
        }
    }
}
