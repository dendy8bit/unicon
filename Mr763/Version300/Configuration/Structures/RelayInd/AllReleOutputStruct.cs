using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr763.Version300.Configuration.Structures.RelayInd
{
    /// <summary>
    /// ��� ����
    /// </summary>
    public class AllReleOutputStruct : StructBase, IDgvRowsContainer<ReleOutputStruct>
    {
        public const int RELAY_COUNT = 32;
        /// <summary>
        /// ����
        /// </summary>
        [Layout(0, Count = RELAY_COUNT)]
        private ReleOutputStruct[] _relays;

        /// <summary>
        /// ����
        /// </summary>
        [XmlArray(ElementName = "���_����")]
    
        public ReleOutputStruct[] Rows
        {
            get
            {
                return this._relays;
            }
            set
            {
                this._relays = value;
            }
        }
    }
}