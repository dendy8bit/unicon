﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer
{
    /// <summary>
    /// Конфигурация измерительного трансформатора U
    /// </summary>
    public class KanalUN5TransStruct : StructBase
    {
        #region [Private fields]
        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx; //конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1; //конфигурация ТНn1 - коэфициэнт Un1
        [Layout(3)] private ushort _res1; //резерв
        [Layout(4)] private ushort _res2; //резерв
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _neisprTn1; // для неисправности цепей ТН

        #endregion [Private fields]

        #region [U (ТН)]

        /// <summary>
        /// тип_Uo
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "тип_Uo")]
        public string UtypeXml
        {
            get { return Validator.Get(this._binding, StringsConfig.UoType); }
            set { this._binding = Validator.Set(value, StringsConfig.UoType); }
        }

        /// <summary>
        /// KTHL
        /// </summary>
        [BindingProperty(1)]
        [XmlIgnore]
        public double Kthl
        {
            get { return ValuesConverterCommon.GetKth(this._ittl); }
            set { this._ittl = ValuesConverterCommon.SetKthRound(value); }
        }

        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthlValue
        {
            get
            {
                double ktn = Common.SetBit(this._ittl, 15, false);
                return Common.GetBit(this._ittl, 15)
                    ? ktn * 1000 / 256
                    : ktn / 256;
            }
        }
        /// <summary>
        /// KTHL Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHL")]
        public double KthlXml
        {
            get
            {
                double ktn = ValuesConverterCommon.GetKth(this._ittl);
                return Common.GetBit(this._ittl, 15)
                    ? ktn * 1000
                    : ktn;
            }
            set { }
        }
        /// <summary>
        /// KTHX
        /// </summary>
        [BindingProperty(2)]
        [XmlIgnore]
        public double Kthx
        {
            get { return ValuesConverterCommon.GetKth(this._ittx); }
            set { this._ittx = ValuesConverterCommon.SetKthRound(value); }
        }

        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        [XmlIgnore]
        public double KthxValue
        {
            get
            {
                double ktx = Common.SetBit(this._ittx, 15, false);
                return Common.GetBit(this._ittx, 15)
                    ? ktx * 1000 / 256
                    : ktx / 256;
            }
        }
        /// <summary>
        /// KTHX Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHX")]
        public double KthxXml
        {
            get
            {
                double ktx = ValuesConverterCommon.GetKth(this._ittx);
                return Common.GetBit(this._ittx, 15)
                    ? ktx * 1000
                    : ktx;
            }
            set { }
        }

        /// <summary>
        /// KTHX
        /// </summary>
        [BindingProperty(3)]
        [XmlIgnore]
        public double Kthx1
        {
            get { return ValuesConverterCommon.GetKth(this._ittx1); }
            set { this._ittx1 = ValuesConverterCommon.SetKthRound(value); }
        }

        /// <summary>
        /// KTHX1 Полное значение
        /// </summary>
        [XmlIgnore]
        public double Kthx1Value
        {
            get
            {
                double ktx = Common.SetBit(this._ittx1, 15, false);
                return Common.GetBit(this._ittx1, 15)
                    ? ktx * 1000 / 256
                    : ktx / 256;
            }
            set { }
        }

        /// <summary>
        /// KTHX1 Полное значение (XML)
        /// </summary>
        [XmlElement(ElementName = "KTHX1")]
        public double Kthx1Xml
        {
            get
            {
                double ktx = ValuesConverterCommon.GetKth(this._ittx1);
                return Common.GetBit(this._ittx1, 15)
                    ? ktx * 1000
                    : ktx;
            }
            set { }
        }

        /// <summary>
        /// KTHL коэффициент
        /// </summary>
        [BindingProperty(4)]
        [XmlIgnore]
        public string Lkoef
        {
            get { return Validator.Get(this._ittl, StringsConfig.KthKoefs, 15); }
            set { this._ittl = Validator.Set(value, StringsConfig.KthKoefs, this._ittl, 15); }
        }

        /// <summary>
        /// KTHX коэффициент
        /// </summary>
        [BindingProperty(5)]
        [XmlIgnore]
        public string Xkoef
        {
            get { return Validator.Get(this._ittx, StringsConfig.KthKoefs, 15); }
            set { this._ittx = Validator.Set(value, StringsConfig.KthKoefs, this._ittx, 15); }
        }

        /// <summary>
        /// KTHX коэффициент
        /// </summary>
        [BindingProperty(6)]
        [XmlIgnore]
        public string Xkoef1
        {
            get { return Validator.Get(this._ittx1, StringsConfig.KthKoefs, 15); }
            set { this._ittx1 = Validator.Set(value, StringsConfig.KthKoefs, this._ittx1, 15); }
        }

        /// <summary>
        /// Неиспр_ТНn
        /// </summary>
        [BindingProperty(7)]
        public string X1fault
        {
            get { return Validator.Get(this._neisprTn1, StringsConfig.SwitchSignals); }
            set { this._neisprTn1 = Validator.Set(value, StringsConfig.SwitchSignals); }
        }
        #endregion [U (ТН)] 
    }
}
