﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.Mr763.Version300.Configuration.Structures
{
    public class AllGroupSetpointStruct304 : StructBase, ISetpointContainer<GroupSetpoint304>
    {
        private const int GROUP_COUNT = 6;

        [Layout(0, Count = GROUP_COUNT)] private GroupSetpoint304[] _groupSetpoints;

        [XmlElement(ElementName = "Группы")]
        public GroupSetpoint304[] Setpoints
        {
            get
            {
                GroupSetpoint304[] res = new GroupSetpoint304[GROUP_COUNT];
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    res[i] = this._groupSetpoints[i].Clone<GroupSetpoint304>();
                }
                return res;
            }
            set
            {
                for (int i = 0; i < GROUP_COUNT; i++)
                {
                    this._groupSetpoints[i] = value[i];
                }
            }
        }
    }
}
