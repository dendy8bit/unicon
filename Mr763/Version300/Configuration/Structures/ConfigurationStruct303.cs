﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Mr763.Version2.Configuration.Structures.Oscope;
using BEMN.Mr763.Version300.Configuration.Structures.ConfigSystem;
using BEMN.Mr763.Version300.Configuration.Structures.InputSignals;
using BEMN.Mr763.Version300.Configuration.Structures.Oscope;
using BEMN.Mr763.Version300.Configuration.Structures.RelayInd;
using BEMN.Mr763.Version300.Configuration.Structures.Switch;
using BEMN.Mr763.Version300.Configuration.Structures.UROV;

namespace BEMN.Mr763.Version300.Configuration.Structures
{
    /// <summary>
    /// Вся конфигурация
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "МР763")]
    public class ConfigurationStruct303 : StructBase
    {

        [XmlElement(ElementName = "Версия")]
        public double DeviceVersion { get; set; }

        [XmlElement(ElementName = "Номер_устройства")]
        public string DeviceNumber { get; set; }

        [XmlElement(ElementName = "Тип_устройства")]
        public string DeviceType
        {
            get { return "МР763"; }
            set { }
        }

        [XmlElement(ElementName = "Первичные_уставки")]
        public bool Primary { get; set; }

        [XmlElement(ElementName = "Размер_осциллограмы")]
        public string SizeOsc { get; set; }

        [XmlElement(ElementName = "Группа")]
        public int Group { get; set; }

        #region [Private fields]

        [Layout(0)] private AllGroupSetpointStruct _allGroupSetpoints;
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [Layout(1)] private SwitchStruct _sw;
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [Layout(2)] private InputSignalStruct _impsg;
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [Layout(3)] private OscopeStructV300 _osc;
        /// <summary>
        /// Параметры автоматики
        /// </summary>
        [Layout(4)] private AutomaticsParametersStruct _automatics;
        /// <summary>
        /// конфигурациия RS485
        /// </summary>
        [Layout(5, Ignore = true)] private ConfigNetStruct _cnfRS485;
        /// <summary>
        /// конфигурациия Ethernet
        /// </summary>
        [Layout(6)] private ConfigIPAddress _ipAddress;
        [Layout(7, Count = 6, Ignore = true)] private ushort[] _mac;
        /// <summary>
        /// Вход опорного канала
        /// </summary>
        [Layout(8)] private ConfigAddStruct _cnfAdd;

        [Layout(9)] private UrovStruct _urov;
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Группы уставок
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_всех_групп_уставок")]
        [BindingProperty(0)]
        public AllGroupSetpointStruct AllGroupSetpoints
        {
            get { return this._allGroupSetpoints; }
            set { this._allGroupSetpoints = value; }
        }
        /// <summary>
        /// конфигурациия выключателя
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_выключателя")]
        [BindingProperty(1)]
        public SwitchStruct Sw
        {
            get { return this._sw; }
            set { this._sw = value; }
        }
        /// <summary>
        /// конфигурациия входных сигналов
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_входных_сигналов")]
        [BindingProperty(2)]
        public InputSignalStruct Impsg
        {
            get { return this._impsg; }
            set { this._impsg = value; }
        }
        /// <summary>
        /// конфигурациия осцилографа
        /// </summary>
        [XmlElement(ElementName = "Конфигурация_осцилографа")]
        [BindingProperty(3)]
        public OscopeStructV300 Osc
        {
            get { return this._osc; }
            set { this._osc = value; }
        }
        [XmlElement(ElementName = "Конфигурация_реле,индикаторов,неисправностей")]
        [BindingProperty(4)]
        public AutomaticsParametersStruct Automatics
        {
            get { return this._automatics; }
            set { this._automatics = value; }
        }

        [XmlElement(ElementName = "IP")]
        [BindingProperty(5)]
        public ConfigIPAddress IP
        {
            get { return this._ipAddress; }
            set { this._ipAddress = value; }
        }

        [XmlElement(ElementName = "Вход_опорного_канала")]
        [BindingProperty(6)]
        public ConfigAddStruct ConfigAdd
        {
            get { return this._cnfAdd; }
            set { this._cnfAdd = value; }
        }
        /// <summary>
        /// УРОВ
        /// </summary>
        [XmlElement(ElementName = "УРОВ")]
        [BindingProperty(7)]
        public UrovStruct Urov
        {
            get { return this._urov; }
            set { this._urov = value; }
        }
        #endregion [Properties]
    }
}
