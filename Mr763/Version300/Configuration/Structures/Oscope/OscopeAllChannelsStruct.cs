﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.MBServer;

namespace BEMN.Mr763.Version300.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStructV300 : StructBase, IDgvRowsContainer<ChannelWithBase>
    {
        public const int KANAL_COUNT = 56;
        public const int CFG_OSC_COUNT = 8;

        [Layout(0, Count = CFG_OSC_COUNT)] private ushort[] _cnfOscChannel; //конфигурация каналов, выборка списков сигналов из БД
        [Layout(1, Count = 2)] private ushort[] _rez;
        [Layout(2, Count = KANAL_COUNT)] private ushort[] _oscChannels;    //конфигурация канала осциллографирования
        
        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelWithBase[] Rows
        {
            get { return this.GetChannelsWithBase(); }
            set { this.SetChannelsWithBase(value); }
        }

        public ChannelWithBase[] GetChannelsWithBase()
        {
            BitArray array = new BitArray(Common.TOBYTES(this._cnfOscChannel, false));
            byte[] bases = new byte[KANAL_COUNT];
            for (int i = 0; i < KANAL_COUNT; i++)
            {
                bases[i] = (byte) (1*(array[i*2] ? 1 : 0) + 2*(array[i*2+1] ? 1 : 0));
            }
            List<ChannelWithBase> channelsList = new List<ChannelWithBase>();
            for (int i = 0; i < KANAL_COUNT; i++)
            {
                channelsList.Add(new ChannelWithBase { Base = bases[i], Channel = this._oscChannels[i] });
            }
            return channelsList.ToArray();
        }

        public void SetChannelsWithBase(ChannelWithBase[] value)
        {
            List<bool> boolList = new List<bool>();
            foreach (bool[] values in value.Select(channel => new[] { Common.GetBit(channel.Base, 0), Common.GetBit(channel.Base, 1) }))
            {
                boolList.AddRange(values);
            }
            do
            {
                boolList.Add(false);
            } while (boolList.Count < OscopeStructV300.CFG_OSC_COUNT * 16); // заполнение 0 битами до размера 8 ushort
            List<ushort> retUshorts = new List<ushort>();
            for (int i = 0; i < OscopeStructV300.CFG_OSC_COUNT; i++)
            {
                ushort cfg = 0;
                for (int j = 0; j < 16; j++)
                {
                    cfg += boolList[j+i*16] ? (ushort)Math.Pow(2, j) : (ushort)0;
                }
                retUshorts.Add(cfg);
            }
            this._cnfOscChannel = retUshorts.ToArray();
            for (int i = 0; i < value.Length; i++)
            {
                this._oscChannels[i] = value[i].Channel;
            }
        }
    }
}
