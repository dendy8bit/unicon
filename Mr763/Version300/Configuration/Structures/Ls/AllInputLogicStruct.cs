﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version300.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    public class AllInputLogicStruct : StructBase, IXmlSerializable
    {
        public const int LOGIC_COUNT = 16;

        [Layout(0, Count = LOGIC_COUNT)] private InputLogicStruct[] _logic;


        [BindingProperty(0)]
        [XmlIgnore]
        public InputLogicStruct this[int index]
        {
            get { return this._logic[index]; }
            set { this._logic[index] = value; }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {

        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            for (int i = 0; i < LOGIC_COUNT; i++)
            {

                writer.WriteStartElement("ЛС");
                this[i].WriteXml(writer);
                writer.WriteEndElement();
            }
        }
    }
}
