﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;

namespace BEMN.Mr763.Version300.Oscilloscope
{
    /// <summary>
    /// Загружает журнал осцилограммы
    /// </summary>
    public class OscJournalLoader
    {
        #region [Private fields]
        /// <summary>
        /// Записи журнала
        /// </summary>
        private readonly MemoryEntity<OscJournalStructV3> _oscJournal;
        /// <summary>
        /// Сброс журнала на нулевую запись
        /// </summary>
        private readonly MemoryEntity<OneWordStruct> _setPageJournal;
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        private readonly List<OscJournalStructV3> _oscRecords;
        /// <summary>
        /// Текущий номер записи журнала осциллографа
        /// </summary>
        private int _recordNumber;

        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Успешно прочитана одна запись журнала осциллографа
        /// </summary>
        public event Action ReadRecordOk;
        /// <summary>
        /// Успешно прочитаны все записи
        /// </summary>
        public event Action AllJournalReadOk;
        /// <summary>
        /// Возникла ошибка при чтении журнала осциллографа
        /// </summary>
        public event Action ReadJournalFail;
        #endregion [Events]


        #region [Ctor's]
        /// <summary>
        /// Создаёт загрузчик Журнала осциллографа
        /// </summary>
        /// <param name="oscJournal">Объект записи журнала</param>
        /// <param name="setPageJournal">Объект сброса журнала</param>
        public OscJournalLoader(MemoryEntity<OscJournalStructV3> oscJournal, MemoryEntity<OneWordStruct> setPageJournal)
        {
            this._oscRecords = new List<OscJournalStructV3>();
            //Записи журнала
            this._oscJournal = oscJournal;
            this._oscJournal.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
            //запись индекса ЖО
            this._setPageJournal = setPageJournal;
            this._setPageJournal.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._setPageJournal.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        } 
        #endregion [Ctor's]


        #region [Properties]
        /// <summary>
        /// Количество записей в журнале осциллографа
        /// </summary>
        public int RecordNumber
        {
            get { return this._oscRecords.Count; }
        }
        /// <summary>
        /// Список структур "Запись журнала осциллографа"
        /// </summary>
        public List<OscJournalStructV3> OscRecords
        {
            get { return this._oscRecords; }
        }

        public object[] GetRecord
        {
            get { return this._oscJournal.Value.GetRecord; }
        }
        #endregion [Properties]


        #region [Private MemoryEntity Events Handlers]
        /// <summary>
        /// Невозможно прочитать журнал
        /// </summary>
        private void FailReadOscJournal()
        {
            if (this.ReadJournalFail != null)
                this.ReadJournalFail.Invoke();
        }
        /// <summary>
        /// Прочитана одна запись журнала
        /// </summary>
        private void ReadRecord()
        {
            if (!this._oscJournal.Value.IsEmpty)
            {
                OscJournalStructV3.RecordIndex = this.RecordNumber;
                this.OscRecords.Add(this._oscJournal.Value.Clone<OscJournalStructV3>());
                this._recordNumber++;
                this.SaveIdex();
                if (this.ReadRecordOk == null) return;
                this.ReadRecordOk.Invoke();
            }
            else
            {
                if (this.AllJournalReadOk == null) return;
                this.AllJournalReadOk.Invoke();
            }
        }

        #endregion [Private MemoryEntity Events Handlers]


        #region [Public members]
        /// <summary>
        /// Запуск чтения журнала осциллографа
        /// </summary>
        public void StartReadJournal()
        {
            this._recordNumber = 0;
            this.SaveIdex();
        }

        public void ClearEvents()
        {
            this._oscJournal.AllReadOk -= HandlerHelper.CreateReadArrayHandler(this.ReadRecord);
            this._oscJournal.AllReadFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);

            this._setPageJournal.AllWriteOk -= HandlerHelper.CreateReadArrayHandler(this._oscJournal.LoadStruct);
            this._setPageJournal.AllWriteFail -= HandlerHelper.CreateReadArrayHandler(this.FailReadOscJournal);
        }
        #endregion [Public members]

        internal void Reset()
        {
            this._oscRecords.Clear();
        }

        private void SaveIdex()
        {
            OneWordStruct ind = this._setPageJournal.Value;
            ind.Word = (ushort)this._recordNumber;
            this._setPageJournal.Value = ind;
            this._setPageJournal.SaveStruct6();
        }
    }
}
