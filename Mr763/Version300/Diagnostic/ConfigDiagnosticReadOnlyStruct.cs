﻿using System;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version300.Diagnostic
{
    public class ConfigDiagnosticReadOnlyStruct : StructBase
    {
        [Layout(0)] private ushort _size;                   //размер структуры неисправности
        [Layout(1)] private ushort _pos;                    //текущая пустая позиция
        [Layout(2)] private ushort _max;                    //максимальное число сохраняемых неисправностей
        [Layout(3)] private ushort _res;                    
        [Layout(4)] private uint _err;                      //общее число ошибок модулей
        [Layout(5)] private uint _tim;                      //время в мс от вкл. устройства
        [Layout(6, Count = 8)] private ushort[] _dateTime;  //дата и время

        public uint Tim
        {
            get { return this._tim; }
            set { this._tim = value; }
        }

        public DateTime GetDataTime(uint tim)
        {
            //TODO сделать преобразование времени по текущей дате и времени от вклучения устройства
            return new DateTime();
        } 
    }
}
