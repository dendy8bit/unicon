﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr763.Version300.Diagnostic
{
    public class DiagnosticDataStruct : StructBase
    {
        [Layout(0, Count = 13)] private ushort[] _write;    //запрос к модулю
        [Layout(1, Count = 13)] private ushort[] _read;     //ответ от модуля
        [Layout(2)] private int _tim;          //время в мс от вкл. устройства
        [Layout(3)] private int _err;          //общее число ошибок модулей (номер текущей ошибки)
        [Layout(4)] private ushort _typefault;  //тип неисправности
        [Layout(5)] private ushort _pos;        //позиция модуля
        [Layout(6)] private ushort _lenPac;     //размер данных I/O
        [Layout(7)] private ushort _crc;        //CRC кадра


        public ushort[] Write
        {
            get { return this._write; }
        }

        public ushort[] Read
        {
            get { return this._read; }
        }

        public uint Tim
        {
            get { return (uint)this._tim; }
        }

        public uint Error
        {
            get { return (uint)this._err; }
        }

        public ushort TypeFault
        {
            get { return this._typefault; }
        }

        public ushort Pos
        {
            get { return this._pos; }
        }

        public ushort LenPac
        {
            get { return this._lenPac; }
        }

        public ushort CRC
        {
            get { return this._crc; }
        }
    }
}
