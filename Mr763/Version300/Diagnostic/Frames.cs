﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BEMN.MBServer;

namespace BEMN.Mr763.Version300.Diagnostic
{
    [Serializable]
    public class Frames
    {
        public Frames()
        {
            this.FramesList = new List<Frame>();
        }
        [XmlElement("Frame")]
        public List<Frame> FramesList { get; private set; }

        public void AddFrames(AllDiagnosticDataStruct allDiagnostic, uint tim)
        {
            foreach (DiagnosticDataStruct diagnostic in
                    allDiagnostic.AllDiagnostic.Where(diagnostic => diagnostic.Tim >= tim))
            {
                this.FramesList.Add(new Frame(diagnostic));
            }
        }
    }

    [Serializable]
    public class Frame
    {
        public Frame()
        {
            this.Write = this.Read = this.TypeFault = this.Pos = string.Empty;
            this.Tim = this.Err = this.LenPac = 0;
        }

        public Frame(DiagnosticDataStruct diagnostic)
        {
            this.Write = diagnostic.Write.Aggregate(string.Empty,
                (current, w) =>
                    current + Common.LOBYTE(w).ToString("X").PadLeft(2, '0') + " " +
                    Common.HIBYTE(w).ToString("X").PadLeft(2, '0') + " ");
            this.Read = diagnostic.Read.Aggregate(string.Empty,
                (current, w) =>
                    current + Common.LOBYTE(w).ToString("X").PadLeft(2, '0') + " " +
                    Common.HIBYTE(w).ToString("X").PadLeft(2, '0') + " ");

            this.Tim = diagnostic.Tim;
            this.Err = diagnostic.Error;
            this.TypeFault = Convert.ToString(diagnostic.TypeFault, 2).PadLeft(16, '0');
            switch (diagnostic.Pos)
            {
                case 0x1:
                    this.Pos = "Модуль 5";
                    break;
                case 0x2:
                    this.Pos = "Модуль 4";
                    break;
                case 0x4:
                    this.Pos = "Модуль 3";
                    break;
                case 0x8:
                    this.Pos = "Модуль 2";
                    break;
                case 0x10:
                    this.Pos = "Модуль 1";
                    break;
                default:
                    this.Pos = "Неизвестное значение";
                    break;
            }
            this.LenPac = diagnostic.LenPac;
        }

        [XmlElement]
        public string Write { get; set; }
        [XmlElement]
        public string Read { get; set; }
        [XmlElement]
        public uint Tim { get; set; }
        [XmlElement]
        public uint Err { get; set; }
        [XmlElement]
        public string TypeFault { get; set; }
        [XmlElement]
        public string Pos { get; set; }
        [XmlElement]
        public ushort LenPac { get; set; }
    }
}
