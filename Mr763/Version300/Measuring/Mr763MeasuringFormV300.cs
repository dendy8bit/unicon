﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Forms.MeasuringClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr763.Properties;
using BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr763.Version300.Measuring.Structures;

namespace BEMN.Mr763.Version300.Measuring
{
    public partial class Mr763MeasuringFormV300 : Form, IFormView
    {
        #region Const
        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string CURRENT_GROUP = "Группа №{0}";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string RESET_HOT_STATE = "Сбросить состояние тепловой";
        private const string RESET_TN_STATE = "Сбросить неисправности ТН";
        private const string START_OSC = "Запустить осциллограф";
        private const string MEASURE_TRANS_READ_FAIL = "Параметры измерений не были загружены";
        #endregion

        #region [Private fields]

        private Mr763Device _device;
        private readonly MemoryEntity<AnalogDataBaseStructV300> _analogDataBase;
        private readonly MemoryEntity<DiscretDataBaseStructV300> _discretDataBase;
        private readonly MemoryEntity<DiscretDataBaseStruct304> _discretDataBase304;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MemoryEntity<OneWordStruct> _iMinStruct; 
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private readonly MemoryEntity<MeasureTransStructV3> _measureTrans;
        private readonly int _measTransSize;
        private MeasureTransStructV3 _currentMeasureTrans;
        private readonly AveragerTime<AnalogDataBaseStructV300> _averagerTime;
        private string[] _symbols;
        private bool _loaded;
        private ushort? _numGroup;
        private ushort _iMin;
        private bool _activatefailmes;
        private double _version;
        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Защиты I, I*, I2/I1, Ig)
        /// </summary>
        private LedControl[] _currents;
        private LedControl[] _currents1;
        /// <summary>
        /// Защиты U,F
        /// </summary>
        private LedControl[] _voltage;
        /// <summary>
        /// Защиты по сопр.
        /// </summary>
        private LedControl[] _resist;
        private LedControl[] _resist1;
        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;
        /// <summary>
        /// Защиты по мощности
        /// </summary>
        private LedControl[] _reversPow;
        private LedControl[] _reversPow1;
        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _stateAndApv;
        private LedControl[] _stateAndApv1;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faultsMain;
        private LedControl[] _faultsMain1;
        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        private LedControl[] _faultsSwitch;

        /// <summary>
        /// Неисправности ТН
        /// </summary>
        private LedControl[] _faultsTn;
        private LedControl[] _faultsTn1;
        /// <summary>
        /// Синхронизм
        /// </summary>
        private LedControl[] _sinchronizm;
        private LedControl[] _sinchronizm1;
        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;
        /// <summary>
        /// Повреждение фаз и качание
        /// </summary>
        private LedControl[] _phaseAndSw;
        private LedControl[] _phaseAndSw1;
        /// <summary>
        /// УРОВ
        /// </summary>
        private LedControl[] _urov;
        private LedControl[] _urov1;
        // АВР
        private LedControl[] _avr;
        private LedControl[] _avr1;
        // Двигатель
        private LedControl[] _motor;
        private LedControl[] _motor1;

        private LedControl[] _bgs;
        #endregion [Private fields]

        #region Constructor
        public Mr763MeasuringFormV300()
        {
            this.InitializeComponent();
        }

        public Mr763MeasuringFormV300(Mr763Device device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopRead;
            this._version = Common.VersionConverter(this._device.DeviceVersion);
            this._dateTime = device.Mr763DeviceV2.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);

            this._groupUstavki = device.Mr763DeviceV2.GroupSetpoint;
            this._groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.GroupUstavkiLoaded);
            this._groupUstavki.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => MessageBox.Show("Невозможно прочитать группу уствок"));
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this._groupUstavki.LoadStruct();
            });
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                 MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK,
                 MessageBoxIcon.Error));

            if (this._version >= 3.04)
            {
                this._discretDataBase304 = device.Mr763DeviceV2.DiscretV304;
                this._discretDataBase304.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
                this._discretDataBase304.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);
                //this._device.Mr763DeviceV2.ConfigurationV304.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                //    () =>
                //    {
                //        this._numGroup = null;
                //        this._groupUstavki.LoadStruct();
                //    });
                if (this._version >= 3.07)
                {
                    this._device.Mr763DeviceV2.ConfigurationV307.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadGroup);
                }
                else
                {
                    this._device.Mr763DeviceV2.ConfigurationV304.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadGroup);
                }
            }
            else
            {
                this._discretDataBase = device.Mr763DeviceV2.DiscretV3;
                this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
                this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);
                if (this._version < 3.03)
                {
                    //this._device.Mr763DeviceV2.ConfigurationV3.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                    //    () =>
                    //    {
                    //        this._numGroup = null;
                    //        this._groupUstavki.LoadStruct();
                    //    });
                    this._device.Mr763DeviceV2.ConfigurationV3.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadGroup);
                }
                else
                {
                    //this._device.Mr763DeviceV2.ConfigurationV303.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this,
                    //    () =>
                    //    {
                    //        this._numGroup = null;
                    //        this._groupUstavki.LoadStruct();
                    //    });
                    this._device.Mr763DeviceV2.ConfigurationV303.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadGroup);
                }
            }

            this._iMinStruct = device.Mr763DeviceV2.Imin;
            this._iMinStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, () =>
            {
                this._iMin = this._iMinStruct.Value.Word;
            });
            this._iMinStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, () => { this._iMin = 0; });

            this._analogDataBase = device.Mr763DeviceV2.AnalogV3;
            this._analogDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadOk);
            this._analogDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.AnalogBdReadFail);

            this._measureTrans = device.Mr763DeviceV2.MeasuringV3;
            this._measureTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadOk);
            this._measureTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.MeasureTransReadFail);
            this._measTransSize = this._measureTrans.Value.GetStructInfo().FullSize;

            this._averagerTime = new AveragerTime<AnalogDataBaseStructV300>(1000);
            this._averagerTime.Tick += this.AveragerTimeTick;

            this.Init();
        }

        private void LoadGroup()
        {
            this._numGroup = null;
            this._groupUstavki.LoadStruct();
        }

        private void Init()
        {
            if (Common.VersionConverter(this._device.DeviceVersion) < 3.01)
            {
                this.dugGroup.Visible = this.dugGroup1.Visible = false;
            }

            if (Common.VersionConverter(this._device.DeviceVersion) < 3.02)
            {
                this.reversGroup.Visible = this.reversGroup1.Visible = false;
            }
            if (this._version < 3.04)
            {
                this.BGS_Group.Visible = false;
                this.splGroupBox.Size = new Size(250, 182);
                this._freeLogic = new[]
                {
                    this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                    this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                    this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                    this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32
                };
            }
            else
            {
                this._bgs = new[]
                {
                    this._bgs1, this._bgs2, this._bgs3, this._bgs4, this._bgs5, this._bgs6, this._bgs7, this._bgs8,
                    this._bgs9, this._bgs10, this._bgs11, this._bgs12, this._bgs13, this._bgs14, this._bgs15, this._bgs16
                };
                this._freeLogic = new[]
               {
                    this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                    this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                    this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                    this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32,
                    this._ssl33, this._ssl34, this._ssl35, this._ssl36, this._ssl37, this._ssl38, this._ssl39, this._ssl40,
                    this._ssl41, this._ssl42, this._ssl43, this._ssl44, this._ssl45, this._ssl46, this._ssl47, this._ssl48
                };
            }

            this._phaseAndSw = new[]
            {
                this._damageA, this._damageB, this._damageC, this._kachanie, this._outZone, this._inZone
            };
            this._phaseAndSw1 = new[]
            {
                this._damageA1, this._damageB1, this._damageC1, this._kachanie1, this._outZone1, this._inZone1
            };

            this._sinchronizm = new[]
            {
                this._autoSinchr, this._U1noU2y, this._UyUno, this._UnoUno, this._OS, this._US, this._OnKsAndYppN
            };
            this._sinchronizm1 = new[]
            {
                this._autoSinchr1, this._U1noU2y1, this._UyUno1, this._UnoUno1, this._OS1, this._US1, this._OnKsAndYppn1
            };

            this._voltage = new[]
            {
                this._u1IoMoreLed, this._u1MoreLed, this._u2IoMoreLed, this._u2MoreLed, this._u3IoMoreLed,
                this._u3MoreLed, this._u4IoMoreLed, this._u4MoreLed, this._u1IoLessLed, this._u1LessLed,
                this._u2IoLessLed, this._u2LessLed, this._u3IoLessLed, this._u3LessLed, this._u4IoLessLed,
                this._u4LessLed, this._f1IoMoreLed, this._f1MoreLed, this._f2IoMoreLed, this._f2MoreLed,
                this._f3IoMoreLed, this._f3MoreLed, this._f4IoMoreLed, this._f4MoreLed, this._f1IoLessLed,
                this._f1LessLed, this._f2IoLessLed, this._f2LessLed, this._f3IoLessLed, this._f3LessLed,
                this._f4IoLessLed, this._f4LessLed, this._q1Led, this._q2Led
            };

            this._resist = new[]
            {
                this._r1IoLed, this._r1Led, this._r2IoLed, this._r2Led, this._r3IoLed, this._r3Led, this._r4IoLed,
                this._r4Led, this._r5IoLed, this._r5Led, this._r6IoLed, this._r6Led
            };
            this._resist1 = new[]
            {
                this._r1IoLed1, this._r1Led1, this._r2IoLed1, this._r2Led1, this._r3IoLed1, this._r3Led1, this._r4IoLed1,
                this._r4Led1, this._r5IoLed1, this._r5Led1, this._r6IoLed1, this._r6Led1
            };
            this._reversPow = new[]
            {
                this._P1Io, this._P1, this._P2Io, this._P2
            };
            this._reversPow1 = new[]
            {
                this._P1Io1, this._P11, this._P2Io1, this._P21
            };
            this._currents = new[]
            {
                this._i1Io, this._i1, this._i2Io, this._i2, this._i3Io, this._i3, this._i4Io, this._i4, this._i5Io, // I
                this._i5,this._i6Io, this._i6,  this._i8Io, this._i8, 
                this._iS1Io, this._iS1, this._iS2Io,this._iS2,this._iS3Io, this._iS3, this._iS4Io, this._iS4,       // I*
                this._iS5Io, this._iS5, this._iS6Io, this._iS6,this._iS7Io,this._iS7,this._iS8Io,this._iS8,
                this._i2i1IoLed, this._i2i1Led,                                                                     // I2/I1                
            };
            this._currents1 = new[]
            {
                this._i1Io1, this._i11, this._i2Io1, this._i21, this._i3Io1, this._i31, this._i4Io1, this._i41, this._i5Io1,// I
                this._i51,this._i6Io1, this._i61, this._i8Io1, this._i81, 
                this._iS1Io1, this._iS11, this._iS2Io1,this._iS21,this._iS3Io1, this._iS31, this._iS4Io1, this._iS41,       // I*
                this._iS5Io1, this._iS51, this._iS6Io1, this._iS61,this._iS7Io1,this._iS71,this._iS8Io1,this._iS81
            };

            this._stateAndApv = new[]
            {
                this._fault, new LedControl(), this._acceleration, new LedControl(), this._faultOff,
                new LedControl(), new LedControl(), this._puskApv, this._krat1, this._krat2, this._krat3, this._krat4,
                this._turnOnApv, this._zapretApv, this._blockApv, this._readyApv
            };
            this._stateAndApv1 = new[]
            {
                this._puskApv1, this._krat11, this._krat21, this._krat31, this._krat41,
                this._turnOnApv1, this._zapretApv1, this._blockApv1, this._readyApv1
            };

            this._indicators = new[]
            {
                this.diod1, this.diod2, this.diod3, this.diod4, this.diod5, this.diod6, this.diod7, this.diod8, this.diod9, this.diod10, this.diod11, this.diod12
            };

            this._relays = new[]
            {
                this._module1, this._module2, this._module3, this._module4, this._module5, this._module6,
                this._module7, this._module8, this._module9, this._module10, this._module11, this._module12,
                this._module13, this._module14, this._module15, this._module16, this._module17, this._module18,
                this._module19, this._module20, this._module21, this._module22, this._module23, this._module24,
                this._module25, this._module26, this._module27, this._module28, this._module29, this._module30,
                this._module31, this._module32, this._module33, this._module34
            };
            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal,
                this._availabilityFaultSystemJournal, new LedControl(), this._switchOff, this._switchOn
            };
            this._faultsMain = new[]
            {
                this._faultHardware, this._faultSoftware, this._faultMeasuringU, this._faultMeasuringf,
                this._faultSwitchOff, this._faultLogic, new LedControl(), this._faultModule1, this._faultModule2,
                this._faultModule3, this._faultModule4, this._faultModule5, this._faultSetpoints,
                this._faultGroupsOfSetpoints, this._faultPass, this._faultSystemJournal, this._faultAlarmJournal,
                this._faultOsc
            };
            this._faultsMain1 = new[]
            {
                this._faultHardware1, this._faultSoftware1, this._faultMeasuringU1, this._faultMeasuringF1,
                this._faultSwitchOff1, this._faultLogic1
            };

            this._faultsTn = new[]
            {
                this._faultTNmgn, new LedControl(), this._faultUn1, this._faultTH3u0, this._faultTHU2, this._faultObruvFaz,
                this._faultUnExt, new LedControl(), this._faultTN, this._faultUn, new LedControl(), this._UabcLow10,
                this._freqHiger60, this._freqLow40
            };
            this._faultsTn1 = new[]
            {
                this._faultTNmgn1,new LedControl(), this._externalTnUn1, this._faultTn3U0, this._faultTnU2, this._tnObivFaz, 
                this._externalTn, new LedControl(), this._faultTN1, this._externalTnUn
            };

            this._discretInputs = new[]
            {
                this._d1, this._d2, this._d3, this._d4, this._d5, this._d6, this._d7, this._d8, this._d9, this._d10,
                this._d11, this._d12, this._d13, this._d14, this._d15, this._d16, this._d17, this._d18, this._d19,
                this._d20, this._d21, this._d22, this._d23, this._d24, this._d25, this._d26, this._d27, this._d28, 
                this._d29,this._d30, this._d31, this._d32, this._d33, this._d34, this._d35, this._d36, this._d37, 
                this._d38, this._d39,this._d40, this._k1, this._k2
            };

            this._splErr = new []
            {
                this._fSpl1, this._fSpl2, this._fSpl3, this._fSpl4
            };
            this._groupCombo.SelectedIndex = 0;

            this._faultsSwitch = new[]
            {
                this._faultOut, this._faultBlockCon, this._faultManage, this._faultOtkaz, this._faultSwithON, this._faultDisable1,
                this._faultDisable2
            };
            this._urov = new[] {this.urov1Led, this.urov2Led, this.blockUrovLed};
            this._urov1 = new[] { this.urov1Led1, this.urov2Led1, this.blockUrovLed1 };
            this._avr = new[] {this.avrOn, this.avrOff, this.avrBlock, this.dugPusk};
            this._avr1 = new[] {this._avrOn1, this._avrOff1, this._avrBlock1, this.dugPusk1};
            this._motor = new[] {this.dvBlockQ, this.dvBlockN, this.dvPusk, this.dvWork};
            this._motor1 = new[] {this.dvBlockQ1, this.dvBlockN1, this.dvPusk1, this.dvWork1};
        }

        #endregion Constructor

        #region MemoryEntity Members
        private void GroupUstavkiLoaded()
        {
            if (this._numGroup != this._groupUstavki.Value.Word)
            {
                this._numGroup = this._groupUstavki.Value.Word;
                this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
                ushort measureTransAddr = this._device.GetStartAddrMeasTrans((int) this._numGroup, this._device.DeviceVersion);
                this._measureTrans.RemoveStructQueries();
                this._measureTrans.Slots[0] = new Device.slot(measureTransAddr, (ushort) (measureTransAddr + this._measTransSize));
                this._measureTrans.LoadStruct();
            }
        }

        private void AnalogBdReadOk()
        {
            this._averagerTime.Add(this._analogDataBase.Value);
        }

        private void AnalogBdReadFail()
        {
            const string errorValue = "0";
            this._uaTextBox.Text = errorValue;
            this._ubTextBox.Text = errorValue;
            this._ucTextBox.Text = errorValue;
            this._uabTextBox.Text = errorValue;
            this._ubcTextBox.Text = errorValue;
            this._ucaTextBox.Text = errorValue;
            this._u1TextBox.Text = errorValue; 
            this._u2TextBox.Text = errorValue;
            this._iaTextBox.Text = errorValue;
            this._ibTextBox.Text = errorValue;
            this._icTextBox.Text = errorValue;
            this._i1TextBox.Text = errorValue; 
            this._i2TextBox.Text = errorValue;
            this._fTextBox.Text = errorValue; 
            this._pTextBox.Text = errorValue; 
            this._qTextBox.Text = errorValue; 
            this._cosfTextBox.Text = errorValue; 
            this._qpTextBox.Text = errorValue;
            this._i30.Text = errorValue;
            this._u30.Text = errorValue;

            this._zabBox.Text = errorValue;
            this._zbcBox.Text = errorValue;
            this._zcaBox.Text = errorValue;
            this._za1Box.Text = errorValue;
            this._zb1Box.Text = errorValue;
            this._zc1Box.Text = errorValue;
            this._za2Box.Text = errorValue;
            this._zb2Box.Text = errorValue;
            this._zc2Box.Text = errorValue;
            this._za3Box.Text = errorValue;
            this._zb3Box.Text = errorValue;
            this._zc3Box.Text = errorValue;
            this._za4Box.Text = errorValue;
            this._zb4Box.Text = errorValue;
            this._zc4Box.Text = errorValue;
            this._za5Box.Text = errorValue;
            this._zb5Box.Text = errorValue;
            this._zc5Box.Text = errorValue;

            this._iZabBox.Text = errorValue;
            this._iZbcBox.Text = errorValue;
            this._iZcaBox.Text = errorValue;
            this._iZa1Box.Text = errorValue;
            this._iZb1Box.Text = errorValue;
            this._iZc1Box.Text = errorValue;
            this._iZa2Box.Text = errorValue;
            this._iZb2Box.Text = errorValue;
            this._iZc2Box.Text = errorValue;
            this._iZa3Box.Text = errorValue;
            this._iZb3Box.Text = errorValue;
            this._iZc3Box.Text = errorValue;
            this._iZa4Box.Text = errorValue;
            this._iZb4Box.Text = errorValue;
            this._iZc4Box.Text = errorValue;
            this._iZa5Box.Text = errorValue;
            this._iZb5Box.Text = errorValue;
            this._iZc5Box.Text = errorValue;

            this._cIa.Text = errorValue;
            this._cIb.Text = errorValue;
            this._cIc.Text = errorValue;
            this._cUa.Text = errorValue;
            this._cUb.Text = errorValue;
            this._cUc.Text = errorValue;
            this._cI0.Text = errorValue;
            this._cI1.Text = errorValue;
            this._cI2.Text = errorValue;
            this._cUab.Text = errorValue;
            this._cUbc.Text = errorValue;
            this._cUca.Text = errorValue;
            this._cU0.Text = errorValue;
            this._cU1.Text = errorValue;
            this._cU2.Text = errorValue;
            this._cUn.Text = errorValue;
            this._cUn1.Text = errorValue;
            this._nPusk.Text = errorValue;
            this._nWarm.Text = errorValue;
        }

        private void AveragerTimeTick()
        {
            try
            {
                string[] symbols;
              
                if (this._symbols != null || this._symbols.Length < 14)
                {
                    symbols = this._symbols;
                }
                else
                {
                    symbols = new string[13];
                    for (int i = 0; i < 14; i++)
                    {
                        symbols[i] = "!";
                    }
                }

                #region Токи, напряжения и частота
                this._uaTextBox.Text = this._analogDataBase.Value.GetUa(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ubTextBox.Text = this._analogDataBase.Value.GetUb(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ucTextBox.Text = this._analogDataBase.Value.GetUc(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._uabTextBox.Text = this._analogDataBase.Value.GetUab(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ubcTextBox.Text = this._analogDataBase.Value.GetUbc(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._ucaTextBox.Text = this._analogDataBase.Value.GetUca(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u1TextBox.Text = this._analogDataBase.Value.GetU1(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u2TextBox.Text = this._analogDataBase.Value.GetU2(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._u30.Text = this._analogDataBase.Value.Get3U0(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._unTextBox.Text = this._analogDataBase.Value.GetUn(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._un1TextBox.Text = this._analogDataBase.Value.GetUn1(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._iaTextBox.Text = symbols[0] + this._analogDataBase.Value.GetIa(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._ibTextBox.Text = symbols[1] + this._analogDataBase.Value.GetIb(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._icTextBox.Text = symbols[2] + this._analogDataBase.Value.GetIc(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i1TextBox.Text = this._analogDataBase.Value.GetI1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i2TextBox.Text = symbols[4] + this._analogDataBase.Value.GetI2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._i30.Text = symbols[3] + this._analogDataBase.Value.Get3I0(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._fTextBox.Text = this._analogDataBase.Value.GetF(this._averagerTime.ValueList);
                this._pTextBox.Text = this._analogDataBase.Value.GetP(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._qTextBox.Text = this._analogDataBase.Value.GetQ(this._averagerTime.ValueList, this._currentMeasureTrans);
                this._cosfTextBox.Text = this._analogDataBase.Value.GetCosF(this._averagerTime.ValueList);
                this._qpTextBox.Text = this._analogDataBase.Value.GetQt(this._averagerTime.ValueList);
                this._dU.Text = this._analogDataBase.Value.GetdU(this._averagerTime.ValueList);
                this._dFi.Text = this._analogDataBase.Value.GetdFi(this._averagerTime.ValueList);
                this._dF.Text = this._analogDataBase.Value.GetdF(this._averagerTime.ValueList);
                #endregion

                #region Сопротивления и знаки направлений
                // вторичные значения
                this._zabBox.Text = this._analogDataBase.Value.GetVtorZab(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);   
                this._zbcBox.Text = this._analogDataBase.Value.GetVtorZbc(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zcaBox.Text = this._analogDataBase.Value.GetVtorZca(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._za1Box.Text = this._analogDataBase.Value.GetVtorZa1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb1Box.Text = this._analogDataBase.Value.GetVtorZb1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc1Box.Text = this._analogDataBase.Value.GetVtorZc1(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._za2Box.Text = this._analogDataBase.Value.GetVtorZa2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb2Box.Text = this._analogDataBase.Value.GetVtorZb2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc2Box.Text = this._analogDataBase.Value.GetVtorZc2(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._za3Box.Text = this._analogDataBase.Value.GetVtorZa3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb3Box.Text = this._analogDataBase.Value.GetVtorZb3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc3Box.Text = this._analogDataBase.Value.GetVtorZc3(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._za4Box.Text = this._analogDataBase.Value.GetVtorZa4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb4Box.Text = this._analogDataBase.Value.GetVtorZb4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc4Box.Text = this._analogDataBase.Value.GetVtorZc4(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._za5Box.Text = this._analogDataBase.Value.GetVtorZa5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zb5Box.Text = this._analogDataBase.Value.GetVtorZb5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                this._zc5Box.Text = this._analogDataBase.Value.GetVtorZc5(this._averagerTime.ValueList, this._currentMeasureTrans.ChannelI.InpInValue, this._iMin);
                // первичные значения
                this._iZabBox.Text = this._analogDataBase.Value.GetZab(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZbcBox.Text = this._analogDataBase.Value.GetZbc(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZcaBox.Text = this._analogDataBase.Value.GetZca(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZa1Box.Text = this._analogDataBase.Value.GetZa1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb1Box.Text = this._analogDataBase.Value.GetZb1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc1Box.Text = this._analogDataBase.Value.GetZc1(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZa2Box.Text = this._analogDataBase.Value.GetZa2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb2Box.Text = this._analogDataBase.Value.GetZb2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc2Box.Text = this._analogDataBase.Value.GetZc2(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZa3Box.Text = this._analogDataBase.Value.GetZa3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb3Box.Text = this._analogDataBase.Value.GetZb3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc3Box.Text = this._analogDataBase.Value.GetZc3(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZa4Box.Text = this._analogDataBase.Value.GetZa4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb4Box.Text = this._analogDataBase.Value.GetZb4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc4Box.Text = this._analogDataBase.Value.GetZc4(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZa5Box.Text = this._analogDataBase.Value.GetZa5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZb5Box.Text = this._analogDataBase.Value.GetZb5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this._iZc5Box.Text = this._analogDataBase.Value.GetZc5(this._averagerTime.ValueList, this._currentMeasureTrans, this._iMin);
                this.AN.Text = symbols[6];
                this.BN.Text = symbols[7];
                this.CN.Text = symbols[8];
                this.AB.Text = symbols[9];
                this.BC.Text = symbols[10];
                this.CA.Text = symbols[11];
                #endregion

                #region Углы
                this._cIa.Text = this._analogDataBase.Value.CornerIa;
                this._cIb.Text = this._analogDataBase.Value.CornerIb;
                this._cIc.Text = this._analogDataBase.Value.CornerIc;
                this._cUa.Text = this._analogDataBase.Value.CornerUa;
                this._cUb.Text = this._analogDataBase.Value.CornerUb;
                this._cUc.Text = this._analogDataBase.Value.CornerUc;
                this._cI0.Text = this._analogDataBase.Value.CornerI0;
                this._cI1.Text = this._analogDataBase.Value.CornerI1;
                this._cI2.Text = this._analogDataBase.Value.CornerI2;
                this._cUab.Text = this._analogDataBase.Value.CornerUab;
                this._cUbc.Text = this._analogDataBase.Value.CornerUbc;
                this._cUca.Text = this._analogDataBase.Value.CornerUca;
                this._cU0.Text = this._analogDataBase.Value.CornerU0;
                this._cU1.Text = this._analogDataBase.Value.CornerU1;
                this._cU2.Text = this._analogDataBase.Value.CornerU2;
                this._cUn.Text = this._analogDataBase.Value.CornerUn;
                this._cUn1.Text = this._analogDataBase.Value.CornerUn1;
                #endregion

                this._nPusk.Text = this._analogDataBase.Value.Npusk;
                this._nWarm.Text = this._analogDataBase.Value.Nwarm;
            }
            catch (Exception)
            {}
        }

        private void MeasureTransReadFail()
        {
            if (this._activatefailmes)
            {
                this._activatefailmes = false;
                MessageBox.Show(MEASURE_TRANS_READ_FAIL);
            }
        }

        private void MeasureTransReadOk()
        {
            if (!this._activatefailmes) this._activatefailmes = true;
            this._currentMeasureTrans = this._measureTrans.Value;
            if (this._loaded) return;
            this._loaded = true;
            this._analogDataBase.LoadStructCycle();
        }
        
        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);    
            LedManager.TurnOffLeds(this._currents);
            LedManager.TurnOffLeds(this._currents1);
            LedManager.TurnOffLeds(this._resist);
            LedManager.TurnOffLeds(this._resist1);
            LedManager.TurnOffLeds(this._reversPow);
            LedManager.TurnOffLeds(this._reversPow1);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._stateAndApv);
            LedManager.TurnOffLeds(this._stateAndApv1);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faultsMain);
            LedManager.TurnOffLeds(this._faultsMain1);
            LedManager.TurnOffLeds(this._faultsSwitch);
            LedManager.TurnOffLeds(this._faultsTn);
            LedManager.TurnOffLeds(this._faultsTn1);
            LedManager.TurnOffLeds(this._voltage);
            LedManager.TurnOffLeds(this._sinchronizm);
            LedManager.TurnOffLeds(this._sinchronizm1);
            LedManager.TurnOffLeds(this._splErr);
            LedManager.TurnOffLeds(this._phaseAndSw);     
            LedManager.TurnOffLeds(this._phaseAndSw1);
            LedManager.TurnOffLeds(this._urov);
            LedManager.TurnOffLeds(this._urov1);
            LedManager.TurnOffLeds(this._avr);
            LedManager.TurnOffLeds(this._avr1);
            LedManager.TurnOffLeds(this._motor);
            LedManager.TurnOffLeds(this._motor1);
            LedManager.TurnOffLeds(this._bgs);
            this._faultTnInd.State = this._faultTN.State;
            this._logicState.State = LedState.Off;
            foreach (var indicator in this._indicators)
            {
                indicator.TurnOff();
            }
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {
            DiscretDataBaseStructV300 discrets = this._version >= 3.04
               ? this._discretDataBase304.Value
               : this._discretDataBase.Value;

            this._symbols = discrets.CurrentsSymbols;
            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, discrets.DiscretInputs);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, discrets.InputsLogicSignals);
            //Выходные ВЛС
            LedManager.SetLeds(this._outputLogicSignals, discrets.OutputLogicSignals);
            //Защиты U,F, Q
            LedManager.SetLeds(this._voltage, discrets.VoltageFeaq);
            //Защиты I, I*, I2/I1, Ig
            LedManager.SetLeds(this._currents, discrets.MaximumCurrent);
            LedManager.SetLeds(this._currents1, discrets.MaximumCurrent);
            //Защиты по сопротивлению
            LedManager.SetLeds(this._resist, discrets.ResistDef);
            LedManager.SetLeds(this._resist1, discrets.ResistDef);
            //Защиты по мощности
            LedManager.SetLeds(this._reversPow, discrets.ReversPower);
            LedManager.SetLeds(this._reversPow1, discrets.ReversPower);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, discrets.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, discrets.FreeLogic);
            //Состояния и АПВ
            LedManager.SetLeds(this._stateAndApv, discrets.StateAndApv);
            LedManager.SetLeds(this._stateAndApv1, discrets.StateAndApv.Skip(7).ToArray());
            //Реле
            LedManager.SetLeds(this._relays, discrets.Relays);
            //Индикаторы
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(discrets.Indicators[i]);
            }
            //Контроль синхронизма
            LedManager.SetLeds(this._sinchronizm, discrets.ContrSinchr);
            LedManager.SetLeds(this._sinchronizm1, discrets.ContrSinchr);
            //Повреждение фаз и качание
            LedManager.SetLeds(this._phaseAndSw, discrets.PhaseAndSw);
            LedManager.SetLeds(this._phaseAndSw1, discrets.PhaseAndSw);
            //Контроль
            LedManager.SetLeds(this._controlSignals, discrets.ControlSignals);
            //Неисправности основные
            LedManager.SetLeds(this._faultsMain, discrets.Faults1);
            LedManager.SetLeds(this._faultsMain1, discrets.Faults1);
            //Неисправности выключателя
            LedManager.SetLeds(this._faultsSwitch, discrets.Faults2);
            //Неисправности ТН и частоты
            LedManager.SetLeds(this._faultsTn, discrets.Faults3);
            LedManager.SetLeds(this._faultsTn1, discrets.Faults3);
            //Ошибки СПЛ
            LedManager.SetLeds(this._splErr, discrets.FaultLogicErr);
            //УРОВ
            LedManager.SetLeds(this._urov, discrets.Urov);
            LedManager.SetLeds(this._urov1, discrets.Urov);
            //АВР
            LedManager.SetLeds(this._avr, discrets.AvrAndDug);
            LedManager.SetLeds(this._avr1, discrets.AvrAndDug);
            //Двигатель
            LedManager.SetLeds(this._motor, discrets.Motor);
            LedManager.SetLeds(this._motor1, discrets.Motor);
            this._faultTnInd.State = this._faultTN.State;
            bool res = discrets.ControlSignals[7] &&
                       !(discrets.FaultLogicErr[0] || discrets.FaultLogicErr[1] ||
                         discrets.FaultLogicErr[2] || discrets.FaultLogicErr[3]);
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
            if (this._version >= 3.04)
            {
                DiscretDataBaseStruct304 d = (DiscretDataBaseStruct304)discrets;
                LedManager.SetLeds(this._bgs, d.Bgs);
            }
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }
        #endregion MemoryEntity Members

        #region [Help members]

        private void dateTimeControl_TimeChanged()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.StartStopRead();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase?.RemoveStructQueries();
            this._discretDataBase304?.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._analogDataBase.RemoveStructQueries();  
            this._measureTrans.RemoveStructQueries();
            this._groupUstavki.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopRead;
        }

        private void StartStopRead()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._iMinStruct.LoadStruct();
                this._groupUstavki.LoadStructCycle();
                this._discretDataBase?.LoadStructCycle();
                this._discretDataBase304?.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {   
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this._analogDataBase.RemoveStructQueries();  
                this._measureTrans.RemoveStructQueries();
                this.AnalogBdReadFail();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D05, RESET_INDICATION);
        }

        private void _switchGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._groupCombo.SelectedIndex == this._groupUstavki.Value.Word) return;
            int ind = 0;
            if (this._groupCombo.SelectedIndex != -1)
                ind = this._groupCombo.SelectedIndex;

            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                           "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            //this._device.Mr763DeviceV2.ConfigurationV304.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.LoadGroup);
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
        }

        private void _resetTermStateButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D0E, RESET_HOT_STATE);
        }

        private void _resetFaultTnBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D10, RESET_TN_STATE);
        }

        private void _startOscBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D11, START_OSC);
        }

        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D09, "Включить выключатель");
        }

        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.ConfirmCommand(0x0D08, "Отключить выключатель");
        }

        private void StartLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0D, true, "Запуск СПЛ", this._device);
        }

        private void StopLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства", "Останов СПЛ",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if (res == DialogResult.No) return;
            if (this._version >= 3.04)
            {
                this._discretDataBase304.SetBitByAdress(address, command);
            }
            else
            {
                this._discretDataBase.SetBitByAdress(address, command);
            }
        }

        #endregion [Help members]

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof(Mr763Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof(Mr763MeasuringFormV300); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion [INodeView Members]

       private void MeasuringForm_Activated(object sender, EventArgs e)
        {
            this._activatefailmes = true;
        }

        private void MeasuringForm_Deactivate(object sender, EventArgs e)
        {
            this._activatefailmes = false;
        }
    }
}
