﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer;

namespace BEMN.Mr763.Version300.Measuring.Structures
{
    /// <summary>
    /// МР771 Аналоговая база данных
    /// </summary>
    public class AnalogDataBaseStructV300 : StructBase
    {
        #region [Private fields]
        //ток
        [Layout(0)] private ushort _ia; // ток A
        [Layout(1)] private ushort _ib; // ток B
        [Layout(2)] private ushort _ic; // ток C
        [Layout(3)] private ushort _i0; // ток 0
        [Layout(4)] private ushort _i1; // ток 2
        [Layout(5)] private ushort _i2; // ток 2
        [Layout(6)] private ushort _in; // ток N
        [Layout(7)] private ushort _ig; // ток Iг
        //ток вторая гармоника
        [Layout(8)] private ushort _i2A; // ток A
        [Layout(9)] private ushort _i2B; // ток B
        [Layout(10)] private ushort _i2C; // ток C
        //канал U
        [Layout(11)] private ushort _ua; // напряжение A
        [Layout(12)] private ushort _ub; // напряжение B
        [Layout(13)] private ushort _uc; // напряжение C
        [Layout(14)] private ushort _un; // напряжение N
        [Layout(15)] private ushort _un1; // напряжение N1
        //напряжения расчетные
        [Layout(16)] private ushort _uab;
        [Layout(17)] private ushort _ubc;
        [Layout(18)] private ushort _uca;
        [Layout(19)] private ushort _u0;
        [Layout(20)] private ushort _u1;
        [Layout(21)] private ushort _u2;
        [Layout(22)] private ushort _i30;
        [Layout(23)] private ushort _u30;
        //канал F
        [Layout(24)] private ushort _f; // частота
        [Layout(25)] private ushort _in1;// ток N1(резерв)
        [Layout(26)] private int _p;    //дб выровнено на 2 слова
        [Layout(27)] private int _q;    //дб выровнено на 2 слова
        [Layout(28)] private ushort _fi; // угол сдвига фаз
        [Layout(29)] private ushort _omp;
        // Сопротивления
        [Layout(30)] private short _rab;
        [Layout(31)] private short _xab;
        [Layout(32)] private ushort _zab;
        [Layout(33)] private short _rbc;
        [Layout(34)] private short _xbc;
        [Layout(35)] private ushort _zbc;
        [Layout(36)] private short _rca;
        [Layout(37)] private short _xca;
        [Layout(38)] private ushort _zca;
        [Layout(39)] private short _ra1;
        [Layout(40)] private short _xa1;
        [Layout(41)] private ushort _za1;
        [Layout(42)] private short _rb1;
        [Layout(43)] private short _xb1;
        [Layout(44)] private ushort _zb1;
        [Layout(45)] private short _rc1;
        [Layout(46)] private short _xc1;
        [Layout(47)] private ushort _zc1;
        [Layout(48)] private short _ra2;
        [Layout(49)] private short _xa2;
        [Layout(50)] private ushort _za2;
        [Layout(51)] private short _rb2;
        [Layout(52)] private short _xb2;
        [Layout(53)] private ushort _zb2;
        [Layout(54)] private short _rc2;
        [Layout(55)] private short _xc2;
        [Layout(56)] private ushort _zc2;
        [Layout(57)] private short _ra3;
        [Layout(58)] private short _xa3;
        [Layout(59)] private ushort _za3;
        [Layout(60)] private short _rb3;
        [Layout(61)] private short _xb3;
        [Layout(62)] private ushort _zb3;
        [Layout(63)] private short _rc3;
        [Layout(64)] private short _xc3;
        [Layout(65)] private ushort _zc3;
        [Layout(66)] private short _ra4;
        [Layout(67)] private short _xa4;
        [Layout(68)] private ushort _za4;
        [Layout(69)] private short _rb4;
        [Layout(70)] private short _xb4;
        [Layout(71)] private ushort _zb4;
        [Layout(72)] private short _rc4;
        [Layout(73)] private short _xc4;
        [Layout(74)] private ushort _zc4;
        [Layout(75)] private short _ra5;
        [Layout(76)] private short _xa5;
        [Layout(77)] private ushort _za5;
        [Layout(78)] private short _rb5;
        [Layout(79)] private short _xb5;
        [Layout(80)] private ushort _zb5;
        [Layout(81)] private short _rc5;
        [Layout(82)] private short _xc5;
        [Layout(83)] private ushort _zc5;
        //свободная логика(SIGNAL FREE LOGIC)
        [Layout(84)] private ushort _sfl1;
        [Layout(85)] private ushort _sfl2;
        [Layout(86)] private ushort _sfl3;
        [Layout(87)] private ushort _sfl4;
        [Layout(88)] private ushort _sfl5;
        [Layout(89)] private ushort _sfl6;
        [Layout(90)] private ushort _sfl7;
        [Layout(91)] private ushort _sfl8;
        //тепловая модель
        [Layout(92)] private ushort _qt;		//состояние тепловой модели
        [Layout(93)] private short _dU;
        [Layout(94)] private ushort _dFi;
        [Layout(95)] private short _dF;
        [Layout(96)] private ushort _res2;
        [Layout(97)] private ushort _cornerIa;
        [Layout(98)] private ushort _cornerIb;
        [Layout(99)] private ushort _cornerIc;
        [Layout(100)] private ushort _cornerIn;
        [Layout(101)] private ushort _cornerUa;
        [Layout(102)] private ushort _cornerUb;
        [Layout(103)] private ushort _cornerUc;
        [Layout(104)] private ushort _cornerUn;
        [Layout(105)] private ushort _cornerUn1;
        [Layout(106)] private ushort _cornerI0;
        [Layout(107)] private ushort _cornerI1;
        [Layout(108)] private ushort _cornerI2; 
        [Layout(109)] private ushort _cornerUab;
        [Layout(110)] private ushort _cornerUbc;
        [Layout(111)] private ushort _cornerUca;
        [Layout(112)] private ushort _cornerU0;
        [Layout(113)] private ushort _cornerU1;
        [Layout(114)] private ushort _cornerU2;
        [Layout(115)] private ushort _cornerIn1;
        [Layout(116, Count = 144)] private ushort[] _res3;
        [Layout(117)] private ushort _dFdt;
        [Layout(118)] private ushort _nPusk;
        [Layout(119)] private ushort _nWarm;
        #endregion [Private fields]

        #region [Public members]

        private ushort GetMean(List<AnalogDataBaseStructV300> list, Func<AnalogDataBaseStructV300, ushort> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (ushort)(sum / (double)count);
        }

        private short GetMean(List<AnalogDataBaseStructV300> list, Func<AnalogDataBaseStructV300, short> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Aggregate(0, (current, oneStruct) => current + func.Invoke(oneStruct));
            return (short)(sum / (double)count);
        }

        private double GetMean(List<AnalogDataBaseStructV300> list, Func<AnalogDataBaseStructV300, int> func)
        {
            int count = list.Count;
            if (count == 0)
            {
                return 0;
            }
            int sum = list.Sum(func);
            return (int)(sum / (double)count);
        }

        
        public string GetQt(List<AnalogDataBaseStructV300> list)
        {
            ushort value = this.GetMean(list, o => o._qt);
            return ValuesConverterCommon.Analog.GetQ(value);
        }

        public string GetCosF(List<AnalogDataBaseStructV300> list)
        {
            ushort value = this.GetMean(list, o => o._fi);
            return ValuesConverterCommon.Analog.GetCosF(value);
        }

        public string GetQ(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            double value = this.GetMean(list, o => o._q);
            return ValuesConverterCommon.Analog.GetQ((int)value, measure.ChannelI.Ittl * measure.ChannelU.KthlValue);
        }

        public string GetP(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            double value = this.GetMean(list, o => o._p);
            return ValuesConverterCommon.Analog.GetP(value, measure.ChannelI.Ittl * measure.ChannelU.KthlValue);
        }

        public string GetF(List<AnalogDataBaseStructV300> list)
        {
            ushort value = this.GetMean(list, o => o._f);
            return ValuesConverterCommon.Analog.GetF(value);
        }

        public string GetIa(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._ia);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }

        public string GetIb(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._ib);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }
        public string GetIc(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._ic);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }
        public string GetI1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._i1);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }
        public string GetI2(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._i2);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }
        
        public string Get3I0(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._i30);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }
        
        public string GetIg(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            ushort value = this.GetMean(list, o => o._ig);
            return value <= iMin
                ? "0"
                : ValuesConverterCommon.Analog.GetI(value, measure.ChannelI.Ittl * 40);
        }

        public string GetUa(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._ua);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUb(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._ub);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUc(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._uc);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUab(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._uab);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUbc(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._ubc);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUca(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._uca);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetU1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._u1);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetU2(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._u2);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }
        
        public string Get3U0(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._u30);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthlValue);
        }

        public string GetUn(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._un);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.KthxValue);
        }

        public string GetUn1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure)
        {
            ushort value = this.GetMean(list, o => o._un1);
            return ValuesConverterCommon.Analog.GetU(value, measure.ChannelU.Kthx1Value);
        }

        /*===========ВТОРИЧНЫЕ СОПРОТИВЛЕНИЯ============*/
        public string GetVtorZab(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rab);
            short x = this.GetMean(list, o => o._xab);
            ushort ia = this.GetMean(list, o => o._ia);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ia <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZbc(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rbc);
            short x = this.GetMean(list, o => o._xbc);
            ushort ic = this.GetMean(list, o => o._ic);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ic <= iMin || ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZca(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rca);
            short x = this.GetMean(list, o => o._xca);
            ushort ia = this.GetMean(list, o => o._ia);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ia <= iMin || ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZa1(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra1);
            short x = this.GetMean(list, o => o._xa1);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZb1(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb1);
            short x = this.GetMean(list, o => o._xb1);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZc1(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc1);
            short x = this.GetMean(list, o => o._xc1);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }
        public string GetVtorZa2(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra2);
            short x = this.GetMean(list, o => o._xa2);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZb2(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb2);
            short x = this.GetMean(list, o => o._xb2);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZc2(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc2);
            short x = this.GetMean(list, o => o._xc2);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZa3(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra3);
            short x = this.GetMean(list, o => o._xa3);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZb3(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb3);
            short x = this.GetMean(list, o => o._xb3);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZc3(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc3);
            short x = this.GetMean(list, o => o._xc3);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZa4(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra4);
            short x = this.GetMean(list, o => o._xa4);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZb4(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb4);
            short x = this.GetMean(list, o => o._xb4);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZc4(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc4);
            short x = this.GetMean(list, o => o._xc4);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZa5(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra5);
            short x = this.GetMean(list, o => o._xa5);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZb5(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb5);
            short x = this.GetMean(list, o => o._xb5);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }

        public string GetVtorZc5(List<AnalogDataBaseStructV300> list, double iInp, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc5);
            short x = this.GetMean(list, o => o._xc5);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin) ? "---------" : ValuesConverterCommon.Analog.GetZ(r, x, 1, iInp);
        }
        //===========ПЕРВИЧНЫЕ СОПРОТИВЛЕНИЯ============
        public string GetZab(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rab);
            short x = this.GetMean(list, o => o._xab);
            ushort ia = this.GetMean(list, o => o._ia);
            ushort ib = this.GetMean(list, o => o._ib);

            return (ia <= iMin || ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue, measure.ChannelI.Ittl);
        }

        public string GetZbc(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rbc);
            short x = this.GetMean(list, o => o._xbc);
            ushort ib = this.GetMean(list, o => o._ib);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin || ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZca(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rca);
            short x = this.GetMean(list, o => o._xca);
            ushort ia = this.GetMean(list, o => o._ia);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ia <= iMin || ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZa1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra1);
            short x = this.GetMean(list, o => o._xa1);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb1);
            short x = this.GetMean(list, o => o._xb1);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZc1(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc1);
            short x = this.GetMean(list, o => o._xc1);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin )
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZa2(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra2);
            short x = this.GetMean(list, o => o._xa2);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb2(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb2);
            short x = this.GetMean(list, o => o._xb2);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue,measure.ChannelI.Ittl);
        }
        public string GetZc2(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc2);
            short x = this.GetMean(list, o => o._xc2);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa3(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra3);
            short x = this.GetMean(list, o => o._xa3);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb3(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb3);
            short x = this.GetMean(list, o => o._xb3);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZc3(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc3);
            short x = this.GetMean(list, o => o._xc3);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa4(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra4);
            short x = this.GetMean(list, o => o._xa4);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb4(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb4);
            short x = this.GetMean(list, o => o._xb4);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue ,measure.ChannelI.Ittl);
        }
        public string GetZc4(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc4);
            short x = this.GetMean(list, o => o._xc4);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZa5(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._ra5);
            short x = this.GetMean(list, o => o._xa5);
            ushort ia = this.GetMean(list, o => o._ia);
            return (ia <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }

        public string GetZb5(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rb5);
            short x = this.GetMean(list, o => o._xb5);
            ushort ib = this.GetMean(list, o => o._ib);
            return (ib <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        public string GetZc5(List<AnalogDataBaseStructV300> list, MeasureTransStructV3 measure, ushort iMin)
        {
            short r = this.GetMean(list, o => o._rc5);
            short x = this.GetMean(list, o => o._xc5);
            ushort ic = this.GetMean(list, o => o._ic);
            return (ic <= iMin)
                ? "---------"
                : ValuesConverterCommon.Analog.GetZ(r, x, measure.ChannelU.KthlValue , measure.ChannelI.Ittl);
        }
        
             
        public string GetdU(List<AnalogDataBaseStructV300> list)
        {
            short value = this.GetMean(list, o => o._dU);
            return ValuesConverterCommon.Analog.GetUdouble(value);
        }

        public string GetdFi(List<AnalogDataBaseStructV300> list)
        {
            ushort value = this.GetMean(list, o => o._dFi);
            return string.Format("{0} град.", Math.Round((double) value*360/ushort.MaxValue, 2));
        }

        public string GetdF(List<AnalogDataBaseStructV300> list)
        {
            short value = this.GetMean(list, o => o._dF);
            return ValuesConverterCommon.Analog.GetF(value);
        }


        //======================УГЛЫ=======================
        public string CornerIa
        {
            get { return this._cornerIa == 1000 ? string.Empty : this._cornerIa.ToString(); }
        }

        public string CornerIb
        {
            get { return this._cornerIb == 1000 ? string.Empty : this._cornerIb.ToString(); }
        }

        public string CornerIc
        {
            get { return this._cornerIc == 1000 ? string.Empty : this._cornerIc.ToString(); }
        }

        public string CornerIn
        {
            get { return this._cornerIn == 1000 ? string.Empty : this._cornerIn.ToString(); }
        }

        public string CornerIn1
        {
            get { return this._cornerIn1 == 1000 ? string.Empty : this._cornerIn1.ToString(); }
        }

        public string CornerUa
        {
            get { return this._cornerUa == 1000 ? string.Empty : this._cornerUa.ToString(); }
        }

        public string CornerUb
        {
            get { return this._cornerUb == 1000 ? string.Empty : this._cornerUb.ToString(); }
        }

        public string CornerUc
        {
            get { return this._cornerUc == 1000 ? string.Empty : this._cornerUc.ToString(); }
        }

        public string CornerUn
        {
            get { return this._cornerUn == 1000 ? string.Empty : this._cornerUn.ToString(); }
        }

        public string CornerUn1
        {
            get { return this._cornerUn1 == 1000 ? string.Empty : this._cornerUn1.ToString(); }
        }

        public string CornerI0
        {
            get { return this._cornerI0 == 1000 ? string.Empty : this._cornerI0.ToString(); }
        }

        public string CornerI1
        {
            get { return this._cornerI1 == 1000 ? string.Empty : this._cornerI1.ToString(); }
        }

        public string CornerI2
        {
            get { return this._cornerI2 == 1000 ? string.Empty : this._cornerI2.ToString(); }
        }

        public string CornerUab
        {
            get { return this._cornerUab == 1000 ? string.Empty : this._cornerUab.ToString(); }
        }

        public string CornerUbc
        {
            get { return this._cornerUbc == 1000 ? string.Empty : this._cornerUbc.ToString(); }
        }

        public string CornerUca
        {
            get { return this._cornerUca == 1000 ? string.Empty : this._cornerUca.ToString(); }
        }

        public string CornerU0
        {
            get { return this._cornerU0 == 1000 ? string.Empty : this._cornerU0.ToString(); }
        }

        public string CornerU1
        {
            get { return this._cornerU1 == 1000 ? string.Empty : this._cornerU1.ToString(); }
        }

        public string CornerU2
        {
            get { return this._cornerU2 == 1000 ? string.Empty : this._cornerU2.ToString(); }
        }

        public string Npusk
        {
            get { return this._nPusk.ToString(); }
        }

        public string Nwarm
        {
            get { return this._nWarm.ToString(); }
        }
        #endregion [Public members]
    }
}
