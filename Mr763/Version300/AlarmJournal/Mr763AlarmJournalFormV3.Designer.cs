﻿namespace BEMN.Mr763.Version300.AlarmJournal
{
    partial class Mr763AlarmJournalFormV3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this._readAlarmJournalButton = new System.Windows.Forms.Button();
            this._alarmJournalGrid = new System.Windows.Forms.DataGridView();
            this._indexCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._timeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msg1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._msgCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._codeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._typeCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._groupCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._stepColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ra1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xa1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xb1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._rc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._xc1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._IcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._I2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3I0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UabCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UbcCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UcaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._U2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._3U0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._UnCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._Un1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._FCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._QCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D0Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D1Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D2Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D3Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._D4Col = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._saveAlarmJournalButton = new System.Windows.Forms.Button();
            this._loadAlarmJournalButton = new System.Windows.Forms.Button();
            this._openAlarmJournalDialog = new System.Windows.Forms.OpenFileDialog();
            this._saveAlarmJournalDialog = new System.Windows.Forms.SaveFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._exportButton = new System.Windows.Forms.Button();
            this._saveJournalHtmlDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _readAlarmJournalButton
            // 
            this._readAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._readAlarmJournalButton.Location = new System.Drawing.Point(12, 539);
            this._readAlarmJournalButton.Name = "_readAlarmJournalButton";
            this._readAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._readAlarmJournalButton.TabIndex = 1;
            this._readAlarmJournalButton.Text = "Прочитать журнал";
            this._readAlarmJournalButton.UseVisualStyleBackColor = true;
            this._readAlarmJournalButton.Click += new System.EventHandler(this._readAlarmJournalButtonClick);
            // 
            // _alarmJournalGrid
            // 
            this._alarmJournalGrid.AllowUserToAddRows = false;
            this._alarmJournalGrid.AllowUserToDeleteRows = false;
            this._alarmJournalGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._alarmJournalGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._alarmJournalGrid.BackgroundColor = System.Drawing.Color.White;
            this._alarmJournalGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._alarmJournalGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._indexCol,
            this._timeCol,
            this._msg1Col,
            this._msgCol,
            this._codeCol,
            this._typeCol,
            this._groupCol,
            this._rabCol,
            this._xabCol,
            this._rbcCol,
            this._xbcCol,
            this._rcaCol,
            this._xcaCol,
            this._stepColumn,
            this._ra1Col,
            this._xa1Col,
            this._rb1Col,
            this._xb1Col,
            this._rc1Col,
            this._xc1Col,
            this._IaCol,
            this._IbCol,
            this._IcCol,
            this._I1Col,
            this._I2Col,
            this._3I0Col,
            this._UaCol,
            this._UbCol,
            this._UcCol,
            this._UabCol,
            this._UbcCol,
            this._UcaCol,
            this._U1Col,
            this._U2Col,
            this._3U0Col,
            this._UnCol,
            this._Un1Col,
            this._FCol,
            this._QCol,
            this._D0Col,
            this._D1Col,
            this._D2Col,
            this._D3Col,
            this._D4Col});
            this._alarmJournalGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this._alarmJournalGrid.Location = new System.Drawing.Point(0, 0);
            this._alarmJournalGrid.Margin = new System.Windows.Forms.Padding(100, 3, 3, 100);
            this._alarmJournalGrid.Name = "_alarmJournalGrid";
            this._alarmJournalGrid.ReadOnly = true;
            this._alarmJournalGrid.RowHeadersVisible = false;
            this._alarmJournalGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._alarmJournalGrid.Size = new System.Drawing.Size(788, 528);
            this._alarmJournalGrid.TabIndex = 19;
            // 
            // _indexCol
            // 
            this._indexCol.DataPropertyName = "_indexCol";
            this._indexCol.Frozen = true;
            this._indexCol.HeaderText = "№";
            this._indexCol.Name = "_indexCol";
            this._indexCol.ReadOnly = true;
            this._indexCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._indexCol.Width = 24;
            // 
            // _timeCol
            // 
            this._timeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this._timeCol.DataPropertyName = "_timeCol";
            this._timeCol.HeaderText = "Дата/Время";
            this._timeCol.Name = "_timeCol";
            this._timeCol.ReadOnly = true;
            this._timeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._timeCol.Width = 77;
            // 
            // _msg1Col
            // 
            this._msg1Col.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msg1Col.DataPropertyName = "_msg1Col";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msg1Col.DefaultCellStyle = dataGridViewCellStyle1;
            this._msg1Col.HeaderText = "Сообщение";
            this._msg1Col.Name = "_msg1Col";
            this._msg1Col.ReadOnly = true;
            this._msg1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _msgCol
            // 
            this._msgCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._msgCol.DataPropertyName = "_msgCol";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._msgCol.DefaultCellStyle = dataGridViewCellStyle2;
            this._msgCol.HeaderText = "Сработавшая защита";
            this._msgCol.Name = "_msgCol";
            this._msgCol.ReadOnly = true;
            this._msgCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._msgCol.Width = 140;
            // 
            // _codeCol
            // 
            this._codeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._codeCol.DataPropertyName = "_codeCol";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._codeCol.DefaultCellStyle = dataGridViewCellStyle3;
            this._codeCol.HeaderText = "Параметр срабатывания";
            this._codeCol.Name = "_codeCol";
            this._codeCol.ReadOnly = true;
            this._codeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _typeCol
            // 
            this._typeCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._typeCol.DataPropertyName = "_typeCol";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._typeCol.DefaultCellStyle = dataGridViewCellStyle4;
            this._typeCol.HeaderText = "Значение параметра срабатывания";
            this._typeCol.Name = "_typeCol";
            this._typeCol.ReadOnly = true;
            this._typeCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _groupCol
            // 
            this._groupCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this._groupCol.DataPropertyName = "_groupCol";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this._groupCol.DefaultCellStyle = dataGridViewCellStyle5;
            this._groupCol.HeaderText = "Группа уставок";
            this._groupCol.Name = "_groupCol";
            this._groupCol.ReadOnly = true;
            this._groupCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _rabCol
            // 
            this._rabCol.DataPropertyName = "_rabCol";
            this._rabCol.HeaderText = "Rab";
            this._rabCol.Name = "_rabCol";
            this._rabCol.ReadOnly = true;
            this._rabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rabCol.Width = 33;
            // 
            // _xabCol
            // 
            this._xabCol.DataPropertyName = "_xabCol";
            this._xabCol.HeaderText = "Xab";
            this._xabCol.Name = "_xabCol";
            this._xabCol.ReadOnly = true;
            this._xabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xabCol.Width = 32;
            // 
            // _rbcCol
            // 
            this._rbcCol.DataPropertyName = "_rbcCol";
            this._rbcCol.HeaderText = "Rbc";
            this._rbcCol.Name = "_rbcCol";
            this._rbcCol.ReadOnly = true;
            this._rbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rbcCol.Width = 33;
            // 
            // _xbcCol
            // 
            this._xbcCol.DataPropertyName = "_xbcCol";
            this._xbcCol.HeaderText = "Xbc";
            this._xbcCol.Name = "_xbcCol";
            this._xbcCol.ReadOnly = true;
            this._xbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xbcCol.Width = 32;
            // 
            // _rcaCol
            // 
            this._rcaCol.DataPropertyName = "_rcaCol";
            this._rcaCol.HeaderText = "Rca";
            this._rcaCol.Name = "_rcaCol";
            this._rcaCol.ReadOnly = true;
            this._rcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rcaCol.Width = 33;
            // 
            // _xcaCol
            // 
            this._xcaCol.DataPropertyName = "_xcaCol";
            this._xcaCol.HeaderText = "Xca";
            this._xcaCol.Name = "_xcaCol";
            this._xcaCol.ReadOnly = true;
            this._xcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xcaCol.Width = 32;
            // 
            // _stepColumn
            // 
            this._stepColumn.DataPropertyName = "_stepColumn";
            this._stepColumn.HeaderText = "Контур Ф-N";
            this._stepColumn.Name = "_stepColumn";
            this._stepColumn.ReadOnly = true;
            this._stepColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._stepColumn.Width = 66;
            // 
            // _ra1Col
            // 
            this._ra1Col.DataPropertyName = "_ra1Col";
            this._ra1Col.HeaderText = "Ra";
            this._ra1Col.Name = "_ra1Col";
            this._ra1Col.ReadOnly = true;
            this._ra1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._ra1Col.Width = 27;
            // 
            // _xa1Col
            // 
            this._xa1Col.DataPropertyName = "_xa1Col";
            this._xa1Col.HeaderText = "Xa";
            this._xa1Col.Name = "_xa1Col";
            this._xa1Col.ReadOnly = true;
            this._xa1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xa1Col.Width = 26;
            // 
            // _rb1Col
            // 
            this._rb1Col.DataPropertyName = "_rb1Col";
            this._rb1Col.HeaderText = "Rb";
            this._rb1Col.Name = "_rb1Col";
            this._rb1Col.ReadOnly = true;
            this._rb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rb1Col.Width = 27;
            // 
            // _xb1Col
            // 
            this._xb1Col.DataPropertyName = "_xb1Col";
            this._xb1Col.HeaderText = "Xb";
            this._xb1Col.Name = "_xb1Col";
            this._xb1Col.ReadOnly = true;
            this._xb1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xb1Col.Width = 26;
            // 
            // _rc1Col
            // 
            this._rc1Col.DataPropertyName = "_rc1Col";
            this._rc1Col.HeaderText = "Rc";
            this._rc1Col.Name = "_rc1Col";
            this._rc1Col.ReadOnly = true;
            this._rc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._rc1Col.Width = 27;
            // 
            // _xc1Col
            // 
            this._xc1Col.DataPropertyName = "_xc1Col";
            this._xc1Col.HeaderText = "Xc";
            this._xc1Col.Name = "_xc1Col";
            this._xc1Col.ReadOnly = true;
            this._xc1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._xc1Col.Width = 26;
            // 
            // _IaCol
            // 
            this._IaCol.DataPropertyName = "_IaCol";
            this._IaCol.HeaderText = "Ia";
            this._IaCol.Name = "_IaCol";
            this._IaCol.ReadOnly = true;
            this._IaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IaCol.Width = 22;
            // 
            // _IbCol
            // 
            this._IbCol.DataPropertyName = "_IbCol";
            this._IbCol.HeaderText = "Ib";
            this._IbCol.Name = "_IbCol";
            this._IbCol.ReadOnly = true;
            this._IbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IbCol.Width = 22;
            // 
            // _IcCol
            // 
            this._IcCol.DataPropertyName = "_IcCol";
            this._IcCol.HeaderText = "Ic";
            this._IcCol.Name = "_IcCol";
            this._IcCol.ReadOnly = true;
            this._IcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._IcCol.Width = 22;
            // 
            // _I1Col
            // 
            this._I1Col.DataPropertyName = "_I1Col";
            this._I1Col.HeaderText = "I1";
            this._I1Col.Name = "_I1Col";
            this._I1Col.ReadOnly = true;
            this._I1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I1Col.Width = 22;
            // 
            // _I2Col
            // 
            this._I2Col.DataPropertyName = "_I2Col";
            this._I2Col.HeaderText = "I2";
            this._I2Col.Name = "_I2Col";
            this._I2Col.ReadOnly = true;
            this._I2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._I2Col.Width = 22;
            // 
            // _3I0Col
            // 
            this._3I0Col.DataPropertyName = "_3I0Col";
            this._3I0Col.HeaderText = "3I0";
            this._3I0Col.Name = "_3I0Col";
            this._3I0Col.ReadOnly = true;
            this._3I0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3I0Col.Width = 28;
            // 
            // _UaCol
            // 
            this._UaCol.DataPropertyName = "_UaCol";
            this._UaCol.HeaderText = "Ua";
            this._UaCol.Name = "_UaCol";
            this._UaCol.ReadOnly = true;
            this._UaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UaCol.Width = 27;
            // 
            // _UbCol
            // 
            this._UbCol.DataPropertyName = "_UbCol";
            this._UbCol.HeaderText = "Ub";
            this._UbCol.Name = "_UbCol";
            this._UbCol.ReadOnly = true;
            this._UbCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbCol.Width = 27;
            // 
            // _UcCol
            // 
            this._UcCol.DataPropertyName = "_UcCol";
            this._UcCol.HeaderText = "Uc";
            this._UcCol.Name = "_UcCol";
            this._UcCol.ReadOnly = true;
            this._UcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcCol.Width = 27;
            // 
            // _UabCol
            // 
            this._UabCol.DataPropertyName = "_UabCol";
            this._UabCol.HeaderText = "Uab";
            this._UabCol.Name = "_UabCol";
            this._UabCol.ReadOnly = true;
            this._UabCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UabCol.Width = 33;
            // 
            // _UbcCol
            // 
            this._UbcCol.DataPropertyName = "_UbcCol";
            this._UbcCol.HeaderText = "Ubc";
            this._UbcCol.Name = "_UbcCol";
            this._UbcCol.ReadOnly = true;
            this._UbcCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UbcCol.Width = 33;
            // 
            // _UcaCol
            // 
            this._UcaCol.DataPropertyName = "_UcaCol";
            this._UcaCol.HeaderText = "Uca";
            this._UcaCol.Name = "_UcaCol";
            this._UcaCol.ReadOnly = true;
            this._UcaCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._UcaCol.Width = 33;
            // 
            // _U1Col
            // 
            this._U1Col.DataPropertyName = "_U1Col";
            this._U1Col.HeaderText = "U1";
            this._U1Col.Name = "_U1Col";
            this._U1Col.ReadOnly = true;
            this._U1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U1Col.Width = 27;
            // 
            // _U2Col
            // 
            this._U2Col.DataPropertyName = "_U2Col";
            this._U2Col.HeaderText = "U2";
            this._U2Col.Name = "_U2Col";
            this._U2Col.ReadOnly = true;
            this._U2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._U2Col.Width = 27;
            // 
            // _3U0Col
            // 
            this._3U0Col.DataPropertyName = "_3U0Col";
            this._3U0Col.HeaderText = "3U0";
            this._3U0Col.Name = "_3U0Col";
            this._3U0Col.ReadOnly = true;
            this._3U0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._3U0Col.Width = 33;
            // 
            // _UnCol
            // 
            this._UnCol.DataPropertyName = "_UnCol";
            this._UnCol.HeaderText = "Un";
            this._UnCol.Name = "_UnCol";
            this._UnCol.ReadOnly = true;
            this._UnCol.Width = 46;
            // 
            // _Un1Col
            // 
            this._Un1Col.DataPropertyName = "_Un1Col";
            this._Un1Col.HeaderText = "Un1";
            this._Un1Col.Name = "_Un1Col";
            this._Un1Col.ReadOnly = true;
            this._Un1Col.Width = 52;
            // 
            // _FCol
            // 
            this._FCol.DataPropertyName = "_FCol";
            this._FCol.HeaderText = "F";
            this._FCol.Name = "_FCol";
            this._FCol.ReadOnly = true;
            this._FCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._FCol.Width = 19;
            // 
            // _QCol
            // 
            this._QCol.DataPropertyName = "_QCol";
            this._QCol.HeaderText = "Q";
            this._QCol.Name = "_QCol";
            this._QCol.ReadOnly = true;
            this._QCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._QCol.Width = 21;
            // 
            // _D0Col
            // 
            this._D0Col.DataPropertyName = "_D0Col";
            this._D0Col.HeaderText = "Д [1-8]";
            this._D0Col.Name = "_D0Col";
            this._D0Col.ReadOnly = true;
            this._D0Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D0Col.Width = 41;
            // 
            // _D1Col
            // 
            this._D1Col.DataPropertyName = "_D1Col";
            this._D1Col.HeaderText = "Д [9-16]";
            this._D1Col.Name = "_D1Col";
            this._D1Col.ReadOnly = true;
            this._D1Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D1Col.Width = 47;
            // 
            // _D2Col
            // 
            this._D2Col.DataPropertyName = "_D2Col";
            this._D2Col.HeaderText = "Д [17-24]";
            this._D2Col.Name = "_D2Col";
            this._D2Col.ReadOnly = true;
            this._D2Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D2Col.Width = 52;
            // 
            // _D3Col
            // 
            this._D3Col.DataPropertyName = "_D3Col";
            this._D3Col.HeaderText = "Д [25-32]";
            this._D3Col.Name = "_D3Col";
            this._D3Col.ReadOnly = true;
            this._D3Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D3Col.Width = 52;
            // 
            // _D4Col
            // 
            this._D4Col.DataPropertyName = "_D4Col";
            this._D4Col.HeaderText = "Д [33-40]";
            this._D4Col.Name = "_D4Col";
            this._D4Col.ReadOnly = true;
            this._D4Col.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._D4Col.Width = 52;
            // 
            // _saveAlarmJournalButton
            // 
            this._saveAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._saveAlarmJournalButton.Location = new System.Drawing.Point(536, 539);
            this._saveAlarmJournalButton.Name = "_saveAlarmJournalButton";
            this._saveAlarmJournalButton.Size = new System.Drawing.Size(117, 23);
            this._saveAlarmJournalButton.TabIndex = 20;
            this._saveAlarmJournalButton.Text = "Сохранить в файл";
            this._saveAlarmJournalButton.UseVisualStyleBackColor = true;
            this._saveAlarmJournalButton.Click += new System.EventHandler(this._saveAlarmJournalButton_Click);
            // 
            // _loadAlarmJournalButton
            // 
            this._loadAlarmJournalButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._loadAlarmJournalButton.Location = new System.Drawing.Point(401, 539);
            this._loadAlarmJournalButton.Name = "_loadAlarmJournalButton";
            this._loadAlarmJournalButton.Size = new System.Drawing.Size(129, 23);
            this._loadAlarmJournalButton.TabIndex = 21;
            this._loadAlarmJournalButton.Text = "Загрузить из файла";
            this._loadAlarmJournalButton.UseVisualStyleBackColor = true;
            this._loadAlarmJournalButton.Click += new System.EventHandler(this._loadAlarmJournalButton_Click);
            // 
            // _openAlarmJournalDialog
            // 
            this._openAlarmJournalDialog.DefaultExt = "xml";
            this._openAlarmJournalDialog.FileName = "Журнал аварий МР 763";
            this._openAlarmJournalDialog.Filter = "МР763 Журнал аварий(*.xml)|*.xml|МР763 Журнал аварий(*.bin)|*.bin";
            this._openAlarmJournalDialog.RestoreDirectory = true;
            this._openAlarmJournalDialog.Title = "Открыть журнал  аварий для МР 763";
            // 
            // _saveAlarmJournalDialog
            // 
            this._saveAlarmJournalDialog.DefaultExt = "xml";
            this._saveAlarmJournalDialog.FileName = "Журнал аварий МР 763";
            this._saveAlarmJournalDialog.Filter = "(Журнал аварий МР 763) | *.xml";
            this._saveAlarmJournalDialog.Title = "Сохранить  журнал аварий для МР 763";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 571);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(788, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(10, 17);
            this._statusLabel.Text = ".";
            // 
            // _exportButton
            // 
            this._exportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._exportButton.Location = new System.Drawing.Point(659, 539);
            this._exportButton.Name = "_exportButton";
            this._exportButton.Size = new System.Drawing.Size(117, 23);
            this._exportButton.TabIndex = 23;
            this._exportButton.Text = "Сохранить в HTML";
            this._exportButton.UseVisualStyleBackColor = true;
            this._exportButton.Click += new System.EventHandler(this._exportButton_Click);
            // 
            // _saveJournalHtmlDialog
            // 
            this._saveJournalHtmlDialog.DefaultExt = "xml";
            this._saveJournalHtmlDialog.FileName = "Журнал аварий МР 763";
            this._saveJournalHtmlDialog.Filter = "Журнал аварий МР 763 | *.html";
            this._saveJournalHtmlDialog.Title = "Сохранить  журнал аварий для МР763";
            // 
            // Mr763AlarmJournalFormV3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 593);
            this.Controls.Add(this._exportButton);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this._loadAlarmJournalButton);
            this.Controls.Add(this._alarmJournalGrid);
            this.Controls.Add(this._readAlarmJournalButton);
            this.Controls.Add(this._saveAlarmJournalButton);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Mr763AlarmJournalFormV3";
            this.Text = "AlarmJournalForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlarmJournalForm_FormClosing);
            this.Load += new System.EventHandler(this.AlarmJournalForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._alarmJournalGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _readAlarmJournalButton;
        private System.Windows.Forms.DataGridView _alarmJournalGrid;
        private System.Windows.Forms.Button _saveAlarmJournalButton;
        private System.Windows.Forms.Button _loadAlarmJournalButton;
        private System.Windows.Forms.OpenFileDialog _openAlarmJournalDialog;
        private System.Windows.Forms.SaveFileDialog _saveAlarmJournalDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        private System.Windows.Forms.Button _exportButton;
        private System.Windows.Forms.SaveFileDialog _saveJournalHtmlDialog;
        private System.Windows.Forms.DataGridViewTextBoxColumn _indexCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _timeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msg1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _msgCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _codeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _typeCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _groupCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _stepColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _ra1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xa1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xb1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _rc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _xc1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _IcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _I2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3I0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UabCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UbcCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UcaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _U2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _3U0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _UnCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _Un1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _FCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _QCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D0Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D1Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D2Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D3Col;
        private System.Windows.Forms.DataGridViewTextBoxColumn _D4Col;
    }
}