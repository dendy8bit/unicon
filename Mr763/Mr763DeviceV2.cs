﻿using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Forms;
using BEMN.MBServer;
using BEMN.Mr763.Version2.AlarmJournal;
using BEMN.Mr763.Version2.AlarmJournal.Structures;
using BEMN.Mr763.Version2.Configuration.Structures;
using BEMN.Mr763.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr763.Version2.Configuration.Structures.Oscope;
using BEMN.Mr763.Version2.Measuring.Structures;
using BEMN.Mr763.Version2.Osc.Loaders;
using BEMN.Mr763.Version2.Osc.Structures;
using BEMN.Mr763.Version2.SystemJournal.Structures;
using BEMN.Mr763.Version201.Configuration.Structures;
using BEMN.Mr763.Version201.Measuring.Structures;
using BEMN.Mr763.Version300.AlarmJournal.Structures;
using BEMN.Mr763.Version300.Configuration;
using BEMN.Mr763.Version300.Configuration.Structures;
using BEMN.Mr763.Version300.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr763.Version300.Configuration.Structures.Oscope;
using BEMN.Mr763.Version300.Diagnostic;
using BEMN.Mr763.Version300.Measuring.Structures;
using BEMN.Mr763.Version300.Oscilloscope;
using BEMN.Mr763.Version300.SystemJournal.Structures;

namespace BEMN.Mr763
{
    public class Mr763DeviceV2
    {
        #region [Constants]

        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;

        #endregion [Constants]

        #region [Private fields]

        private MemoryEntity<OscJournalStructV2> _oscJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;

        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<ConfigurationStructV201> _configurationV201;
        private MemoryEntity<JournalRefreshStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<AnalogDataBaseStructV201> _analogDataBaseV201;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private CurrentOptionsLoader _currentOptionsLoaderAj;
        private CurrentOptionsLoader _currentOptionsLoaderOsc;
        private OscOptionsLoaderV2 _oscOptionsLoaderV2;

        private MemoryEntity<ConfigurationStructV300> _configV300;
        private MemoryEntity<ProgramPageStruct> _programPageStructV3;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStructV3;
        private MemoryEntity<StartStruct> _programStartStructV3;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStructV3;
        private MemoryEntity<DiscretDataBaseStructV300> _discretsV300;
        private MemoryEntity<DiscretDataBaseStruct304> _discretsV304;
        private MemoryEntity<AnalogDataBaseStructV300> _analogV300;
        private MemoryEntity<DateTimeStruct> _dateTimeV300;
        private MemoryEntity<OneWordStruct> _iMin;
        private MemoryEntity<OneWordStruct> _groupSetpoint;
        private MemoryEntity<MeasureTransStructV3> _measuringTrans;
        private MemoryEntity<OneWordStruct> _setPageJa;
        private MemoryEntity<AlarmJournalRecordStructV3> _alarmJournalV3;
        private CurrentOptionsLoaderV300 _currentOptionsLoaderV3Ja;
        private MemoryEntity<OneWordStruct> _setPageOscJournal;
        private MemoryEntity<OscJournalStructV3> _oscJournalV3;
        private MemoryEntity<OscopeAllChannelsStructV300> _oscopeChannelsStruct;
        private CurrentOptionsLoaderV300 _currentOptionsLoader;
        private MemoryEntity<SystemJournalStructV3> _systemJournalV3;
        private MemoryEntity<OneWordStruct> _indexSystemJournal;
        private MemoryEntity<ConfigDiagnosticStruct> _configDiagnostic;
        private MemoryEntity<ConfigDiagnosticReadOnlyStruct> _configDiagnosticReadOnly;

        #endregion [Private fields]

        #region [Ctor's]



        public Mr763DeviceV2(Mr763Device device)
        {
            double version = Common.VersionConverter(device.DeviceVersion);
            if (version < 3.0)
            {
                this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", device, 0x1000);
                this._configurationV201 = new MemoryEntity<ConfigurationStructV201>("Конфигурация v2.01", device, 0x1000);
                this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", device, SYSTEM_JOURNAL_START_ADRESS);
                this._refreshSystemJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала системы", device, SYSTEM_JOURNAL_START_ADRESS);
                this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", device, 0x200);
                this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", device, 0x5A0);
                this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", device, DISCRET_DATABASE_START_ADRESS);
                this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", device, ANALOG_DATABASE_START_ADRESS);
                this._analogDataBaseV201 = new MemoryEntity<AnalogDataBaseStructV201>("Аналоговая БД v2.01", device, ANALOG_DATABASE_START_ADRESS);
                this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений", device, 0x104E);
                this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", device, 0x700);
                this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", device, 0x700);
                this._currentOptionsLoaderAj = new CurrentOptionsLoader(new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА", device, 0x104E));
                this._currentOptionsLoaderOsc = new CurrentOptionsLoader(new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖО", device, 0x104E));
                this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", device, OSC_JOURNAL_START_ADRESS);
                this._oscJournal = new MemoryEntity<OscJournalStructV2>("Журнал осциллографа", device, OSC_JOURNAL_START_ADRESS);

                this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", device, 0x900);
                this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", device, 0x900);
                MemoryEntity<OscopeStruct> oscopeStruct = new MemoryEntity<OscopeStruct>("Уставки осциллографа", device, 0x102E /*3A*/);
                this._oscOptionsLoaderV2 = new OscOptionsLoaderV2(oscopeStruct);
            }
            else
            {
                this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", device, 0x5A0);
                int slotLen = device.MB.BaudeRate == 921600 ? 1024 : 64;

                if (version >= 3.04)
                {
                    this.StateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логики v3.xx", device, 0x0D17);
                    ushort[] values = new ushort[4];
                    this.StateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D17);
                    this.StateSpl.Values = values;

                    this._oscopeChannelsStruct = new MemoryEntity<OscopeAllChannelsStructV300>("Конфигурация каналов осциллографа V3", device, 0x2A14, slotLen);
                    
                    this._discretsV304 = new MemoryEntity<DiscretDataBaseStruct304>("Дискретная БД v3.04", device, DISCRET_DATABASE_START_ADRESS, slotLen);

                    if (version >= 3.07)
                    {
                        this.ConfigurationV307 = new MemoryEntity<ConfigurationStruct307>("Конфигурация V3.хх", device, 0x1000, slotLen);
                    }
                    else
                    {
                        this.ConfigurationV304 = new MemoryEntity<ConfigurationStruct304>("Конфигурация V3.хх", device, 0x1000, slotLen);
                    }
                }
                else
                {
                    this._oscopeChannelsStruct = new MemoryEntity<OscopeAllChannelsStructV300>("Конфигурация каналов осциллографа V3", device, 0x290C, slotLen);

                    this.StateSpl = new MemoryEntity<SomeStruct>("Состояние ошибок логикиV3", device, 0x0D14, slotLen);
                    ushort[] values = new ushort[4];
                    this.StateSpl.Slots = HelperFunctions.SetSlots(values, 0x0D14);
                    this.StateSpl.Values = values;

                    this._discretsV300 = new MemoryEntity<DiscretDataBaseStructV300>("Дискретная БД v3", device, DISCRET_DATABASE_START_ADRESS, slotLen);
                    if (version == 3.03)
                    {
                        this.ConfigurationV303 = new MemoryEntity<ConfigurationStruct303>("Конфигурация V3.03", device, 0x1000, slotLen);
                    }
                    else
                    {
                        this._configV300 = new MemoryEntity<ConfigurationStructV300>("КонфигурацияV3", device, 0x1000, slotLen);
                    }
                }
                this._sourceProgramStructV3 = new MemoryEntity<SourceProgramStruct>("SaveProgramV3", device, 0x4300, slotLen);
                this._programStartStructV3 = new MemoryEntity<StartStruct>("SaveProgramStartV3", device, 0x0E00, slotLen);
                this._programSignalsStructV3 = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_V3", device, 0x4100, slotLen);
                this._programPageStructV3 = new MemoryEntity<ProgramPageStruct>("SaveProgrammPageV3", device, 0x4000, slotLen);
                this.StopSpl = new MemoryEntity<OneWordStruct>("Останов логической программыV3", device, 0x0D0C, slotLen);
                this.StartSpl = new MemoryEntity<OneWordStruct>("Старт логической программыV3", device, 0x0D0D, slotLen);
                
                this._analogV300 = new MemoryEntity<AnalogDataBaseStructV300>("Аналоговая БД v3.00", device, ANALOG_DATABASE_START_ADRESS, slotLen);
                this._dateTimeV300 = new MemoryEntity<DateTimeStruct>("Дата и время v3", device, 0x200, slotLen);
                this._iMin = new MemoryEntity<OneWordStruct>("Минимально отображаемый ток V3", device, 0xFC05, slotLen);
                this._groupSetpoint = new MemoryEntity<OneWordStruct>("Группа уставок V3", device, 0x0400, slotLen);
                this._measuringTrans = new MemoryEntity<MeasureTransStructV3>("Измерительный трансформатор V3", device, 0x1278, slotLen);
                this.MeasuringN5 = new MemoryEntity<MeasureTransN5Struct>("Измерительный трансформатор V3.xx", device, 0x1278, slotLen);

                this._setPageJa = new MemoryEntity<OneWordStruct>("Страница ЖА V3", device, 0x700, slotLen);
                this._alarmJournalV3 = new MemoryEntity<AlarmJournalRecordStructV3>("ЖА V3", device, 0x700, slotLen);
                this._currentOptionsLoaderV3Ja = new CurrentOptionsLoaderV300(device, slotLen);

                this._setPageOscJournal = new MemoryEntity<OneWordStruct>("Установка страницы ЖО V3", device, OSC_JOURNAL_START_ADRESS, slotLen);
                this._oscJournalV3 = new MemoryEntity<OscJournalStructV3>("ЖО V3", device, OSC_JOURNAL_START_ADRESS, slotLen);
                this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы V3", device, 0x900, slotLen);
                this._currentOptionsLoader = new CurrentOptionsLoaderV300(device, slotLen);
                this._systemJournalV3 = new MemoryEntity<SystemJournalStructV3>("Журнал системы v3", device, SYSTEM_JOURNAL_START_ADRESS, slotLen);
                this._indexSystemJournal = new MemoryEntity<OneWordStruct>("Номер журнала системы v3", device, SYSTEM_JOURNAL_START_ADRESS, slotLen);
                this._oscPage = Common.VersionConverter(device.DeviceVersion) >= 3.02
                    ? new MemoryEntity<OscPage>("Страница осциллографа V3", device, 0x900, slotLen)
                    : new MemoryEntity<OscPage>("Страница осциллографа V3", device, 0x900);
                this._configDiagnostic = new MemoryEntity<ConfigDiagnosticStruct>("Перезаписываемая конфигурация диагностики", device, 0x5D00, slotLen);
                this._configDiagnosticReadOnly = new MemoryEntity<ConfigDiagnosticReadOnlyStruct>("Конфигурация диагностики только для чтения", device, 0x5D04, slotLen);
            }
        }

        #endregion [Ctor's]

        #region [Properties]

        public MemoryEntity<MeasureTransN5Struct> MeasuringN5 { get; private set; }

        public OscOptionsLoaderV2 OscopeOptionsLoaderV2
        {
            get { return this._oscOptionsLoaderV2; }
        }

        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return _setOscStartPage; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }

        public MemoryEntity<OscJournalStructV2> OscJournal
        {
            get { return _oscJournal; }
        }

        public CurrentOptionsLoader CurrentOptionsLoaderAj
        {
            get { return _currentOptionsLoaderAj; }
        }

        public CurrentOptionsLoader CurrentOptionsLoaderOsc
        {
            get { return _currentOptionsLoaderOsc; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return _refreshAlarmJournal; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return _alarmJournal; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return _analogDataBase; }
        }

        public MemoryEntity<AnalogDataBaseStructV201> AnalogDataBaseV201
        {
            get { return _analogDataBaseV201; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return _dateTime; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return _configuration; }
        }

        public MemoryEntity<ConfigurationStructV201> ConfigurationV201
        {
            get { return _configurationV201; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshSystemJournal
        {
            get { return _refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return _systemJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return _discretDataBase; }
        }

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return _measureTrans; }
        }

        // ========================== V 3.XX ===========================

        public MemoryEntity<OneWordStruct> SetPageOscJournal
        {
            get { return this._setPageOscJournal; }
        }

        public MemoryEntity<OscJournalStructV3> OscJournalV3
        {
            get { return this._oscJournalV3; }
        }

        public MemoryEntity<OscopeAllChannelsStructV300> OscChannels
        {
            get { return this._oscopeChannelsStruct; }
        }

        public CurrentOptionsLoaderV300 CurrentOptionsLoader
        {
            get { return this._currentOptionsLoader; }
        }

        public MemoryEntity<ConfigurationStructV300> ConfigurationV3
        {
            get { return this._configV300; }
        }

        public MemoryEntity<ConfigurationStruct303> ConfigurationV303 { get; private set; }
        public MemoryEntity<ConfigurationStruct304> ConfigurationV304 { get; private set; }
        public MemoryEntity<ConfigurationStruct307> ConfigurationV307 { get; private set; }

        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return this._programPageStructV3; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return this._programStartStructV3; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return this._programSignalsStructV3; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return this._sourceProgramStructV3; }
        }

        public MemoryEntity<OneWordStruct> StopSpl { get; }

        public MemoryEntity<OneWordStruct> StartSpl { get; }

        public MemoryEntity<SomeStruct> StateSpl { get; }

        public MemoryEntity<AnalogDataBaseStructV300> AnalogV3
        {
            get { return this._analogV300; }
        }

        public MemoryEntity<DiscretDataBaseStruct304> DiscretV304
        {
            get { return this._discretsV304; }
        }
        public MemoryEntity<DiscretDataBaseStructV300> DiscretV3
        {
            get { return this._discretsV300; }
        }

        public MemoryEntity<OneWordStruct> Imin
        {
            get { return this._iMin; }
        }

        public MemoryEntity<OneWordStruct> GroupSetpoint
        {
            get { return this._groupSetpoint; }
        }

        public MemoryEntity<DateTimeStruct> DateTime
        {
            get { return this._dateTimeV300; }
        }

        public MemoryEntity<MeasureTransStructV3> MeasuringV3
        {
            get { return this._measuringTrans; }
        }

        public MemoryEntity<OneWordStruct> SetPageJa
        {
            get { return this._setPageJa; }
        }

        public MemoryEntity<AlarmJournalRecordStructV3> AlarmJournalV3
        {
            get { return this._alarmJournalV3; }
        }

        public CurrentOptionsLoaderV300 CurrentOptionsLoaderJa
        {
            get { return this._currentOptionsLoaderV3Ja; }
        }

        public MemoryEntity<SystemJournalStructV3> SystemJournalV3
        {
            get { return this._systemJournalV3; }
        }

        public MemoryEntity<OneWordStruct> IndexSystemJourna
        {
            get { return this._indexSystemJournal; }
        }

        public MemoryEntity<ConfigDiagnosticStruct> ConfigDiagnostic
        {
            get { return this._configDiagnostic; }
        }

        public MemoryEntity<ConfigDiagnosticReadOnlyStruct> ConfigDiagnosticReadOnly
        {
            get { return this._configDiagnosticReadOnly; }
        }
        #endregion [Properties]
        
    }
}
