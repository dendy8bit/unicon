﻿using System;
using BEMN.Devices;
using BEMN.Forms.ValidatingClasses.New;
using BEMN.Forms.ValidatingClasses.New.ColumnsInfos;
using BEMN.Forms.ValidatingClasses.New.ControlInfos;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Forms.ValidatingClasses.New.Validators;
using BEMN.Forms.ValidatingClasses.New.Validators.TurnOff;
using BEMN.Forms.ValidatingClasses.Rules;
using BEMN.Forms.ValidatingClasses.Rules.Double;
using BEMN.Forms.ValidatingClasses.Rules.Int;
using BEMN.Mr763.Version201.Configuration.Structures;
using BEMN.Mr763.Version201.Configuration.Structures.Oscope;
using BEMN.Mr763.Version201.Configuration.Structures.Sihronizm;
using BEMN.MR763.Version2.Configuration;
using BEMN.MR763.Version2.Configuration.Structures.Apv;
using BEMN.MR763.Version2.Configuration.Structures.Avr;
using BEMN.MR763.Version2.Configuration.Structures.Defenses;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.Block;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.External;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.F;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.I;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.I2I1;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.Ig;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.Istar;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.Q;
using BEMN.MR763.Version2.Configuration.Structures.Defenses.U;
using BEMN.MR763.Version2.Configuration.Structures.Engine;
using BEMN.MR763.Version2.Configuration.Structures.InputSignals;
using BEMN.MR763.Version2.Configuration.Structures.Ls;
using BEMN.MR763.Version2.Configuration.Structures.Lzsh;
using BEMN.MR763.Version2.Configuration.Structures.MeasuringTransformer;
using BEMN.MR763.Version2.Configuration.Structures.Opm;
using BEMN.MR763.Version2.Configuration.Structures.Oscope;
using BEMN.MR763.Version2.Configuration.Structures.RelayInd;
using BEMN.MR763.Version2.Configuration.Structures.Sihronizm;
using BEMN.MR763.Version2.Configuration.Structures.Switch;
using BEMN.MR763.Version2.Configuration.Structures.Vls;

namespace BEMN.Mr763.Version201.Configuration
{
    public partial class Mr763ConfigurationFormV202 : Mr763ConfigurationFormV201
    {
        public Mr763ConfigurationFormV202()
        {
            this.InitializeComponent();
        }

        public Mr763ConfigurationFormV202(Mr763Device device) : base(device)
        {
            Load -= base.Mr763ConfigurationForm_Load;
            Load += this.Mr763ConfigurationForm_Load;
        }

        private new void Mr763ConfigurationForm_Load(object sender, EventArgs e)
        {
            this.Init();
            if (Device.AutoloadConfig)
            {
                this._oscOptions.LoadStruct();
                this.StartRead();
            }
        }

        private void Init()
        {
            this._currentSetpointsStruct = new ConfigurationStructV201();
            this._lsBoxes = new[]
                {
                    this._inputSignals1,this._inputSignals2,this._inputSignals3,this._inputSignals4,
                    this._inputSignals5,this._inputSignals6,this._inputSignals7,this._inputSignals8,
                    this._inputSignals9,this._inputSignals10,this._inputSignals11,this._inputSignals12,
                    this._inputSignals13,this._inputSignals14,this._inputSignals15,this._inputSignals16
                };
            
            this._vlsBoxes = new[]
                {
                    VLScheckedListBox1,VLScheckedListBox2,VLScheckedListBox3,VLScheckedListBox4,
                    VLScheckedListBox5,VLScheckedListBox6,VLScheckedListBox7,VLScheckedListBox8,
                    VLScheckedListBox9,VLScheckedListBox10,VLScheckedListBox11,VLScheckedListBox12,
                    VLScheckedListBox13,VLScheckedListBox14,VLScheckedListBox15,VLScheckedListBox16
                };

            #region [Выключатель]
            this._switchValidator = new NewStructValidator<SwitchStruct>
                (
                this._toolTip,
                   new ControlInfoCombo(this._switchOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchError, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchBlock, StringsConfig.SwitchSignals),
                   new ControlInfoText(this._switchTUrov, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchIUrov, RulesContainer.Ustavka40),
                   new ControlInfoText(this._switchImp, RulesContainer.TimeRule),
                   new ControlInfoText(this._switchTUskor, RulesContainer.TimeRule),
                   new ControlInfoCombo(this._switchKontCep, StringsConfig.OffOn),
                   new ControlInfoCombo(this._switchKeyOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchKeyOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOn, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchVneshOff, StringsConfig.SwitchSignals),
                   new ControlInfoCombo(this._switchButtons, StringsConfig.ForbiddenAllowed),
                   new ControlInfoCombo(this._switchKey, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchVnesh, StringsConfig.ControlForbidden),
                   new ControlInfoCombo(this._switchSDTU, StringsConfig.ForbiddenAllowed)
                   );
            #endregion [Выключатель]

            #region [Апв]

            this._apvValidator = new NewStructValidator<ApvStruct>
                (
                this._toolTip,
                 new ControlInfoCombo(this._apvModes, StringsConfig.ApvModes),
                 new ControlInfoCombo(this._apvBlocking, StringsConfig.SwitchSignals),
                 new ControlInfoText(this._apvTBlock, RulesContainer.TimeRule),
                 new ControlInfoText(this._apvTReady, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv1Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv2Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv3Krat, RulesContainer.TimeRule),
                 new ControlInfoText(this._apv4Krat, RulesContainer.TimeRule),
                 new ControlInfoCombo(this._apvOff, StringsConfig.BeNo)
                );
            #endregion [Апв]

            #region [Авр]

            this._avrValidator = new NewStructValidator<AvrStruct>
                (
                this._toolTip,
                 new ControlInfoCombo(this._avrBySignal, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrBySelfOff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrByDiff, StringsConfig.BeNo),
                 new ControlInfoCombo(this._avrSIGNOn, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlocking, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBlockClear, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrResolve, StringsConfig.SwitchSignals),
                 new ControlInfoCombo(this._avrBack, StringsConfig.SwitchSignals),
                 new ControlInfoText(this._avrTSr, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTBack, RulesContainer.TimeRule),
                 new ControlInfoText(this._avrTOff, RulesContainer.TimeRule),
                 new ControlInfoCombo(this._avrClear, StringsConfig.ForbiddenAllowed)
                );
            #endregion [Авр]

            #region [ЛЗШ]
            this._lpbValidator = new NewStructValidator<LpbStruct>
                    (
                    this._toolTip,
                    new ControlInfoCombo(this._lzhModes, StringsConfig.LzhModes),
                    new ControlInfoText(this._lzhVal, RulesContainer.Ustavka40)
                    );
            #endregion [ЛЗШ]

            #region [Двигатель]

            this._termValidator = new NewStructValidator<TermConfigStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineHeatingTimeBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineCoolingTimeBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineInBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._engineIpConstraintBox, RulesContainer.Ustavka40),
                new ControlInfoText(this._enginePuskTimeBox, RulesContainer.TimeRule),
                new ControlInfoText(this._engineBlockDurationBox, RulesContainer.UshortTo65534),
                new ControlInfoText(this._engineHeatPuskConstraintBox, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._engineQresetCombo, StringsConfig.ExternalDafenseSrab),
                new ControlInfoCombo(this._engineNpuskCombo, StringsConfig.ExternalDafenseSrab)
                );
            #endregion [Двигатель]

            #region [Конфигурация входных сигналов]

            this._inputSignalValidator = new NewStructValidator<InputSignalStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._grUstComboBox, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._indComboBox, StringsConfig.SwitchSignals)
                );
            #endregion [Конфигурация входных сигналов]

            #region [Реле и Индикаторы]

            this._releyValidator = new NewDgwValidatior<AllReleOutputStruct, ReleOutputStruct>
                (
                _outputReleGrid,
                AllReleOutputStruct.RELAY_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.RelayNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoText(RulesContainer.TimeRule)
                );

            this._indicatorValidator = new NewDgwValidatior<AllIndicatorsStruct, IndicatorsStruct>
                (
                _outputIndicatorsGrid,
                AllIndicatorsStruct.INDICATORS_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.IndNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.ReleyType),
                new ColumnInfoCombo(StringsConfig.RelaySignals),
                new ColumnInfoColor()
                );

            this._faultValidator = new NewStructValidator<FaultStruct>
                (
                this._toolTip,
                new ControlInfoCheack(this._fault1CheckBox),
                new ControlInfoCheack(this._fault2CheckBox),
                new ControlInfoCheack(this._fault3CheckBox),
                new ControlInfoCheack(this._fault4CheckBox),
                new ControlInfoText(this._impTB, RulesContainer.TimeRule)
                );

            _automaticsParametersUnion = new StructUnion<AutomaticsParametersStruct>(_releyValidator, _indicatorValidator, _faultValidator);

            #endregion [Реле и Индикаторы]

            #region [Осц]
            this._oscopeConfigValidator = new NewStructValidator<OscopeConfigStruct>
                     (
                     this._toolTip,
                     new ControlInfoText(_oscWriteLength, RulesContainer.UshortTo100),
                     new ControlInfoCombo(this._oscFix, StringsConfig.OscFixation),
                     new ControlInfoCombo(this._oscLength, StringsConfig.OscSize)
                     );

            this._channelsValidator = new NewDgwValidatior<OscopeAllChannelsStructV201, ChannelStruct>
                (
                this._oscChannels,
                OscopeAllChannelsStructV201.KANAL_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ChannelNames, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.RelaySignals)
                );

            this._inpOscValidator = new NewStructValidator<ChannelStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.oscStartCb, StringsConfig.RelaySignals)
                );

            this._oscopeUnion = new StructUnion<OscopeStructV201>(_oscopeConfigValidator, _channelsValidator, _inpOscValidator);

            #endregion [Осц]

            #region [Конфигурациия_измерительных_трансформаторов]

            this._iTransValidator = new NewStructValidator<KanalITransStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._TT_typeCombo, StringsConfig.TtType),
                new ControlInfoText(this._Im_Box, RulesContainer.Ustavka40),
                new ControlInfoText(this._ITTL_Box, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX_Box, RulesContainer.UshortRule),
                new ControlInfoText(this._ITTX1_Box, RulesContainer.UshortRule)
                );

            this._uTransValidator = new NewStructValidator<KanalUTransStruct>
                (
                this._toolTip,
                new ControlInfoText(this._KTHL_Box, RulesContainer.Ustavka128),
                new ControlInfoCombo(this._errorL_combo, StringsConfig.SwitchSignals),
                new ControlInfoCombo(this._KTHLkoef_combo, StringsConfig.KthKoefs)
                );

            this._measureTransUnion = new StructUnion<MeasureTransStruct>(this._iTransValidator, this._uTransValidator);

            _opmValidator = new NewStructValidator<ConfigurationOpmStruct>
            (
            this._toolTip,
            new ControlInfoCombo(this._OMPmode_combo, StringsConfig.OffOn),
            new ControlInfoText(this._Xline_Box, RulesContainer.DoubleTo1)
            );


            #endregion [Конфигурациия_измерительных_трансформаторов]

            #region [ЛС]
            _inputLogicValidator = new NewDgwValidatior<InputLogicStruct>[InputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < InputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _inputLogicValidator[i] = new NewDgwValidatior<InputLogicStruct>
              (
              this._lsBoxes[i],
             InputLogicStruct.DISCRETS_COUNT,
              this._toolTip,
              new ColumnInfoCombo(StringsConfig.LsSignals, ColumnsType.NAME),
              new ColumnInfoCombo(StringsConfig.LsState)
              );
            }
            _inputLogicUnion = new StructUnion<InputLogicSignalStruct>(_inputLogicValidator);

            #endregion [ЛС]

            #region [ВЛС]
            _vlsValidator = new NewCheckedListBoxValidator<OutputLogicStruct>[OutputLogicSignalStruct.LOGIC_COUNT];
            for (int i = 0; i < OutputLogicSignalStruct.LOGIC_COUNT; i++)
            {
                _vlsValidator[i] = new NewCheckedListBoxValidator<OutputLogicStruct>(this._vlsBoxes[i], StringsConfig.VlsSignals);
            }
            _vlsUnion = new StructUnion<OutputLogicSignalStruct>(_vlsValidator);
            #endregion [ВЛС]

            #region [защиты]
            //углы
            _cornerValidator = new NewStructValidator<CornerStruct>
                      (
                      this._toolTip,
                      new ControlInfoText(this._iCorner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._i0Corner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._inCorner, RulesContainer.UshortTo360),
                      new ControlInfoText(this._i2Corner, RulesContainer.UshortTo360)
                      );

            //I*
            _iStarValidator = new NewDgwValidatior<AllDefenseStarStruct, DefenseStarStruct>
                (
                this._difensesI0DataGrid,
                AllDefenseStarStruct.DEF_COUNT,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesIStar, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoText(RulesContainer.Ustavka256),
                new ColumnInfoCheack(),
                new ColumnInfoCombo(StringsConfig.Direction),
                new ColumnInfoCombo(StringsConfig.Undir),
                new ColumnInfoCombo(StringsConfig.I),
                new ColumnInfoCombo(StringsConfig.Characteristic),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesI0DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true, 
                                    2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12, 13, 14, 15, 16, 17)
                                )
                        }
            };

            //I2I1
            _i2I1Validator = new NewStructValidator<DefenseI2I1Struct>
               (
               this._toolTip,
               new ControlInfoCombo(this.I2I1ModeCombo, StringsConfig.DefenseModes),
               new ControlInfoCombo(this.I2I1BlockingCombo, StringsConfig.SwitchSignals),
               new ControlInfoText(this.I2I1TB, RulesContainer.Ustavka100),
                new ControlInfoText(this.I2I1tcp, new CustomIntRule(20, 3276700)),
               new ControlInfoCombo(this.I2I1OSCCombo, StringsConfig.OscModes),
               new ControlInfoCheack(this.I2I1UROVCheck),
               new ControlInfoCheack(this.I2I1APVCheck),
               new ControlInfoCheack(this.I2I1AVRCheck)
               );
            //Ig
            _igValidator = new NewStructValidator<DefenseIgStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this.IrModesCombo, StringsConfig.DefenseModes),
                new ControlInfoCombo(this.IrBlockingCombo, StringsConfig.SwitchSignals),
                new ControlInfoText(this.IrUpuskTB, RulesContainer.Ustavka256),
                new ControlInfoText(this.IrIcpTB, RulesContainer.Ustavka40),
                new ControlInfoText(this.IrtcpTB, RulesContainer.TimeRule),
                new ControlInfoText(this.IrtyTB, RulesContainer.TimeRule),
                new ControlInfoCheack(this.IrUpuskCheck),
                new ControlInfoCheack(this.IrtyCheck),
                new ControlInfoCombo(this.IrOSCCombo, StringsConfig.OscModes),
                new ControlInfoCheack(this.IrUROVCheck),
                new ControlInfoCheack(this.IrAPVCheck),
                new ControlInfoCheack(this.IrAVRCheck)
                );

            #region [I]

            _i1To5Validator = new NewDgwValidatior<AllMtzMainStruct, MtzMainStruct>
                (
                new[] { this._difensesIDataGrid, this._difensesI67DataGrid, this._difensesI8DataGrid },
                new[] { 5, 2, 1 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.NamesI, ColumnsType.NAME),
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40),
                new ColumnInfoCombo(StringsConfig.I67Modes, ColumnsType.COMBO, false, true, false),
                new ColumnInfoText(RulesContainer.Ustavka256, true, true, false), //4
                new ColumnInfoCheack(true, true, false),
                new ColumnInfoCombo(StringsConfig.Direction, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Undir, ColumnsType.COMBO, true, true, false),
                new ColumnInfoCombo(StringsConfig.Logic),
                new ColumnInfoCombo(StringsConfig.Characteristic, ColumnsType.COMBO, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule),
                new ColumnInfoText(RulesContainer.Ushort100To4000, true, true, false),
                new ColumnInfoText(RulesContainer.TimeRule, true, true, false),
                new ColumnInfoCheack(true, true, false),
                new ColumnInfoCombo(StringsConfig.SwitchSignals),
                new ColumnInfoText(RulesContainer.Ustavka100),
                new ColumnInfoCombo(StringsConfig.BeNo),
                new ColumnInfoCombo(StringsConfig.BeNo),
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesIDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesI67DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesI8DataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
                                )
                        }
            };
            #endregion [I]

            //U
            _uValidator = new NewDgwValidatior<AllDefenceUStruct, DefenceUStruct>
                (
                new[] { _difensesUBDataGrid, _difensesUMDataGrid },
                new[] { 4, 4 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.UStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.UmaxDefenseMode, ColumnsType.COMBO, true, false), //1
                new ColumnInfoCombo(StringsConfig.UminDefenseMode, ColumnsType.COMBO, false, true), //2
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka256), //6
                new ColumnInfoCheack(), //7
                new ColumnInfoCheack(false, true), //8
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesUBDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesUMDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
                                )
                        }
            };

            //F
            _fValidator = new NewDgwValidatior<AllDefenseFStruct, DefenseFStruct>
                (
                new[] { _difensesFBDataGrid, _difensesFMDataGrid },
                new[] { 4, 4 },
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.FStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka40To60), //3
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoText(RulesContainer.Ustavka40To60), //6
                new ColumnInfoCheack(), //7
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._difensesFBDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                ),
                            new TurnOffDgv
                                (
                                this._difensesFMDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                )
                        }
            };

            _externalValidator = new NewDgwValidatior<AllDefenseExternalStruct, DefenseExternalStruct>
                (
                _externalDifensesDataGrid,
                16,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.ExternalStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoText(RulesContainer.TimeRule), //4
                new ColumnInfoText(RulesContainer.TimeRule), //5
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab),
                new ColumnInfoCheack(), //7
                new ColumnInfoCombo(StringsConfig.ExternalDafenseSrab), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._externalDifensesDataGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13)
                                )
                        }
            };

            _qValidator = new NewDgwValidatior<AllDefenseQStruct, DefenseQStruct>(
                _engineDefensesGrid,
                2,
                this._toolTip,
                new ColumnInfoCombo(StringsConfig.QStages, ColumnsType.NAME), //0
                new ColumnInfoCombo(StringsConfig.DefenseModes),
                new ColumnInfoText(RulesContainer.Ustavka256), //3
                new ColumnInfoCombo(StringsConfig.SwitchSignals), //9
                new ColumnInfoCombo(StringsConfig.OscModes),
                new ColumnInfoCheack(),
                new ColumnInfoCheack(),
                new ColumnInfoCheack()
                )
            {
                TurnOff = new[]
                        {
                            new TurnOffDgv
                                (
                                this._engineDefensesGrid,
                                new TurnOffRule(1, StringsConfig.DefenseModes[0], true,
                                                2, 3, 4, 5, 6, 7)
                                )
                        }
            };

            _termBlockValidator = new NewStructValidator<DefenseTermBlockStruct>
              (
              this._toolTip,
              new ControlInfoCombo(this._engineQmodeCombo, StringsConfig.OffOn),
              new ControlInfoText(this._engineQconstraintBox, RulesContainer.Ustavka256),
              new ControlInfoText(this._engineQtimeBox, RulesContainer.UshortRule)
              );

            _nBlockValidator = new NewStructValidator<DefenseNBlockStruct>
                (
                this._toolTip,
                new ControlInfoText(this._engineBlockHeatBox, RulesContainer.UshortTo10),
                new ControlInfoText(this._engineBlockPuskBox, RulesContainer.UshortTo10),
                new ControlInfoText(this._engineBlockTimeBox, RulesContainer.UshortRule)
                );

            _defensesUnion = new StructUnion<DefensesSetpointsStruct>
                (
                _cornerValidator,
                _i1To5Validator,
                _iStarValidator,
                _i2I1Validator,
                _igValidator,
                _uValidator,
                _fValidator,
                _qValidator,
                _termBlockValidator,
                _nBlockValidator,
                _externalValidator
                );

            #endregion [защиты]

            _groupSelector = new RadioButtonSelector(_mainRadioButton, _reserveRadioButton, _groupChangeButton);

            _defensesValidator = new SetpointsValidator<AllDefensesSetpointsStruct, DefensesSetpointsStruct>
                (
                _groupSelector,
                _defensesUnion
                );

            #region [Синхронизм]

            _manualSinhronizmValidator = new NewStructValidator<SinhronizmAddStruct>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrManualMode, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrManualUmax, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrManualNoYes, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrManualYesNo, StringsConfig.YesNo),
                new ControlInfoCombo(this._sinhrManualNoNo, StringsConfig.YesNo),
                new ControlInfoText(this._sinhrManualdF, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrManualdFi, new CustomUshortRule(0, 50)),
                new ControlInfoText(this._sinhrManualdFno, new CustomDoubleRule(0, 0.5))
                );
            _autoSinhronizmValidator = new NewStructValidator<SinhronizmAddStructV201>
                (
                this._toolTip,
                new ControlInfoCombo(this._sinhrAutoMode, StringsConfig.OffOn),
                new ControlInfoText(this._sinhrAutoUmax, RulesContainer.Ustavka256),
                new ControlInfoCombo(this._sinhrAutoNoYes, StringsConfig.DiscretOrYes),
                new ControlInfoCombo(this._sinhrAutoYesNo, StringsConfig.DiscretOrYes),
                new ControlInfoCombo(this._sinhrAutoNoNo, StringsConfig.YesNo),
                new ControlInfoText(this._sinhrAutodF, new CustomDoubleRule(0, 0.5)),
                new ControlInfoText(this._sinhrAutodFi, new CustomUshortRule(0, 50)),
                new ControlInfoText(this._sinhrAutodFno, new CustomDoubleRule(0, 0.5))
                );
            _sinhronizmValidator = new NewStructValidator<SinhronizmStructV201>
                (
                this._toolTip,
                new ControlInfoText(this._sinhrUminOts, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUminNal, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrUmaxNal, RulesContainer.Ustavka256),
                new ControlInfoText(this._sinhrTwait, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTsinhr, RulesContainer.TimeRule),
                new ControlInfoText(this._sinhrTon, RulesContainer.UshortTo600),
                new ControlInfoCombo(this._sinhrU1, StringsConfig.Usinhr),
                new ControlInfoCombo(this._sinhrU2, StringsConfig.Usinhr),
                new ControlInfoValidator(this._manualSinhronizmValidator),
                new ControlInfoValidator(this._autoSinhronizmValidator),
                new ControlInfoCombo(_blockSinhCmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(_discretIn1Cmb, StringsConfig.SwitchSignals),
                new ControlInfoCombo(_discretIn2Cmb, StringsConfig.SwitchSignals),
                new ControlInfoText(_sinhrKamp, RulesContainer.Ustavka256),
                new ControlInfoText(_sinhrF, new CustomUshortRule(0, 360))
                );
            #endregion [Синхронизм]

            _configurationValidator = new StructUnion<ConfigurationStructV201>
                (
                _switchValidator,
                _apvValidator,
                _avrValidator,
                _lpbValidator,
                _termValidator,
                _inputSignalValidator,
                _oscopeUnion,
                _measureTransUnion,
                _inputLogicUnion,
                _vlsUnion,
                _defensesValidator,
                _automaticsParametersUnion,
                _opmValidator,
                _sinhronizmValidator
                );
        }
    }
}
