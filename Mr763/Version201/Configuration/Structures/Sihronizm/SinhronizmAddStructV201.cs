﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Mr763.Version2.Configuration;

namespace BEMN.Mr763.Version201.Configuration.Structures.Sihronizm
{
    /// <summary>
    /// дополнительная конфигурациия улавливания синхронизма (УС) для ручного, автоматического включения
    /// </summary>
    [XmlRoot(ElementName = "дополнительная_конфигурациия_улавливания_синхронизма")]
    public class SinhronizmAddStructV201 : StructBase
    {
        #region [Private fields]

        [Layout(0)]
        private ushort _config;
        [Layout(1)]
        private ushort _ustUdelta; //уставка max разность напряжения
        //уставки синхронного включения (режима)
        [Layout(2)]
        private ushort _fs; //допустимая разность частот
        [Layout(3)]
        private ushort _cs; //допустимая разность фаз
        //уставки несинхронного включения (режима)
        [Layout(4)]
        private ushort _fa; //допустимая разность частот
        [Layout(5)]
        private ushort _param;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string ModeXml
        {
            get { return Validator.Get(this._config, StringsConfig.OffOn, 0); }
            set { this._config = Validator.Set(value, StringsConfig.OffOn, this._config, 0); }
        }

        /// <summary>
        /// dUmax., В
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "dUmax")]
        public double DUmax
        {
            get { return ValuesConverterCommon.GetUstavka256(this._ustUdelta); }
            set { this._ustUdelta = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// U1 нет, U2 есть
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "U1нетU2есть")]
        public string NoYesXml
        {
            get { return Validator.Get(this._config, StringsConfig.DiscretOrYes, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DiscretOrYes, this._config, 1); }
        }

        /// <summary>
        /// U1 есть, U2 нет
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "U1естьU2нет")]
        public string YesNoXml
        {
            get { return Validator.Get(this._config, StringsConfig.DiscretOrYes, 2); }
            set { this._config = Validator.Set(value, StringsConfig.DiscretOrYes, this._config, 2); }
        }


        /// <summary>
        /// U1 нет, U2 нет
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "U1нетU2нет")]
        public string NoNoXml
        {
            get { return Validator.Get(this._config, StringsConfig.YesNo, 3); }
            set { this._config = Validator.Set(value, StringsConfig.YesNo, this._config, 3); }
        }

        /// <summary>
        /// Синхронное включение dF, Гц
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "Синхронное_включение")]
        public double DfSinhr
        {
            get { return ValuesConverterCommon.GetUstavka256(this._fs); }
            set { this._fs = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// dFi, град
        /// </summary>
        [BindingProperty(6)]
        [XmlElement(ElementName = "dFi")]
        public ushort Dfi
        {
            get { return this._cs; }
            set { this._cs = value; }
        }

        /// <summary>
        /// Несинхронное включение dF, Гц
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Несинхронное_включение")]
        public double DfNosinhr
        {
            get { return ValuesConverterCommon.GetUstavka256(this._fa); }
            set { this._fa = ValuesConverterCommon.SetUstavka256(value); }
        }

        public ushort Param
        {
            get { return _param; }
            set { _param = value; }
        }
        #endregion [Properties]
    }
}
