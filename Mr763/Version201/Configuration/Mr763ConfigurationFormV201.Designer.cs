﻿namespace BEMN.Mr763.Version201.Configuration
{
    partial class Mr763ConfigurationFormV201
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle67 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle68 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle69 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle70 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle71 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle72 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle73 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle74 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle75 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle109 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle110 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle111 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle112 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle113 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle114 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle115 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle116 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle117 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle118 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle119 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle120 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle121 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle122 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle123 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle124 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle125 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle126 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle127 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle128 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle129 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle130 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle131 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle132 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle133 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle134 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle135 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle136 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle137 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle138 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle139 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle140 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle141 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle142 = new System.Windows.Forms.DataGridViewCellStyle();
            this._configurationTabControl = new System.Windows.Forms.TabControl();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.readFromDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToDeviceItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readFromFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToFileItem = new System.Windows.Forms.ToolStripMenuItem();
            this.writeToHtmlItem = new System.Windows.Forms.ToolStripMenuItem();
            this._measureTransPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this._Xline_Box = new System.Windows.Forms.MaskedTextBox();
            this._OMPmode_combo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._KTHX1koef_combo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this._KTHX1_Box = new System.Windows.Forms.MaskedTextBox();
            this._errorX_combo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this._KTHXkoef_combo = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._errorL_combo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this._KTHLkoef_combo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this._Uo_typeCombo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._KTHX_Box = new System.Windows.Forms.MaskedTextBox();
            this._KTHL_Box = new System.Windows.Forms.MaskedTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._TT_typeCombo = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this._ITTL_Box = new System.Windows.Forms.MaskedTextBox();
            this._Im_Box = new System.Windows.Forms.MaskedTextBox();
            this._inputSygnalsPage = new System.Windows.Forms.TabPage();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsOR = new System.Windows.Forms.TreeView();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.treeViewForLsAND = new System.Windows.Forms.TreeView();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this._indComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this._grUstComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this._inputSignals9 = new System.Windows.Forms.DataGridView();
            this._signalValueNumILI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueColILI = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this._inputSignals10 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn8 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this._inputSignals11 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn9 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this._inputSignals12 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn10 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this._inputSignals13 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn11 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this._inputSignals14 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn12 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this._inputSignals15 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn13 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this._inputSignals16 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn14 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._inputSignals1 = new System.Windows.Forms.DataGridView();
            this._lsChannelCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._signalValueCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._inputSignals2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._inputSignals3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn2 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._inputSignals4 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._inputSignals5 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this._inputSignals6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this._inputSignals7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn6 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this._inputSignals8 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewComboBoxColumn7 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tabPage28 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this._engineNpuskCombo = new System.Windows.Forms.ComboBox();
            this._engineQresetCombo = new System.Windows.Forms.ComboBox();
            this._engineHeatPuskConstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this._enginePuskTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label45 = new System.Windows.Forms.Label();
            this._engineIpConstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this._engineInBox = new System.Windows.Forms.MaskedTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this._engineCoolingTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this._engineHeatingTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this._allDefensesPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._groupChangeButton = new System.Windows.Forms.Button();
            this._mainRadioButton = new System.Windows.Forms.RadioButton();
            this._reserveRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this._difensesTC = new System.Windows.Forms.TabControl();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label134 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this._i2Corner = new System.Windows.Forms.MaskedTextBox();
            this._inCorner = new System.Windows.Forms.MaskedTextBox();
            this._i0Corner = new System.Windows.Forms.MaskedTextBox();
            this._iCorner = new System.Windows.Forms.MaskedTextBox();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._difensesI8DataGrid = new System.Windows.Forms.DataGridView();
            this._i8StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8ModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8IColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8UStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8UstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8DirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8UnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8LogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8CharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8TColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8KColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8TyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8TyYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8I2I1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i8I2I1YNColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8BlockingDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8OscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i8UROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8APVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i8AVRModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._difensesI67DataGrid = new System.Windows.Forms.DataGridView();
            this._i67StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67ModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67IColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67IModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67UStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67UstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i67DirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67UnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67LogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67CharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67TColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67KColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67TyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67TyYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i67BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67I2I1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i67I2I1YNColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67BlockingDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67OscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i67UROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i67APVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i67AVRModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this._difensesIDataGrid = new System.Windows.Forms.DataGridView();
            this._iStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iIColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iUStartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iUstartYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUnDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iLogicColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iCharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iTColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iKColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iTyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iTyYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iI2I1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._iI2I1YNColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iBlockingDirectColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iOscModeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._iUROVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iAPVModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._iAVRModeColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this._difensesI0DataGrid = new System.Windows.Forms.DataGridView();
            this._i0StageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0ModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0IColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0UstartColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0UsYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0DirColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0UndirColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0I0Column = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0CharColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0TColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0kColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0BlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0OscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._i0TyColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._i0TyYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0UROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0APVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._i0AVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.I2I1AVRCheck = new System.Windows.Forms.CheckBox();
            this.I2I1APVCheck = new System.Windows.Forms.CheckBox();
            this.I2I1UROVCheck = new System.Windows.Forms.CheckBox();
            this.I2I1OSCCombo = new System.Windows.Forms.ComboBox();
            this.I2I1tcp = new System.Windows.Forms.MaskedTextBox();
            this.I2I1BlockingCombo = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.I2I1TB = new System.Windows.Forms.MaskedTextBox();
            this.I2I1ModeCombo = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.IrAVRCheck = new System.Windows.Forms.CheckBox();
            this.IrAPVCheck = new System.Windows.Forms.CheckBox();
            this.IrUROVCheck = new System.Windows.Forms.CheckBox();
            this.IrtyCheck = new System.Windows.Forms.CheckBox();
            this.IrUpuskCheck = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.IrtyTB = new System.Windows.Forms.MaskedTextBox();
            this.IrtcpTB = new System.Windows.Forms.MaskedTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.IrOSCCombo = new System.Windows.Forms.ComboBox();
            this.IrIcpTB = new System.Windows.Forms.MaskedTextBox();
            this.IrBlockingCombo = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.IrUpuskTB = new System.Windows.Forms.MaskedTextBox();
            this.IrModesCombo = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this._difensesUBDataGrid = new System.Windows.Forms.DataGridView();
            this._uBStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uBUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uBUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uBSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this._difensesUMDataGrid = new System.Windows.Forms.DataGridView();
            this._uMStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMTypeColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._uMUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMBlockingUMColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._uMUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._uMSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this._difensesFBDataGrid = new System.Windows.Forms.DataGridView();
            this._fBStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fBUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fBUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fBSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage27 = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this._difensesFMDataGrid = new System.Windows.Forms.DataGridView();
            this._fMStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fMModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fMUsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fMTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fMTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fMUvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._fMUvzYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fMOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._fMUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._fMSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this._externalDifensesDataGrid = new System.Windows.Forms.DataGridView();
            this._externalDifStageColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifModesColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifSrabColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifTsrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifTvzColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._externalDifVozvrColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifVozvrYNColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifBlockingColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifOscColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._externalDifUROVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAVRColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifAPVRetColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._externalDifSbrosColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this._engineDefensesGrid = new System.Windows.Forms.DataGridView();
            this._engineDefenseNameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseModeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseConstraintCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._engineDefenseBlockCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseOscCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._engineDefenseUROVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAVRcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this._engineDefenseAPVcol = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.groupBox43 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this._engineQconstraintBox = new System.Windows.Forms.MaskedTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this._engineQtimeBox = new System.Windows.Forms.MaskedTextBox();
            this._engineQmodeCombo = new System.Windows.Forms.ComboBox();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this._engineBlockDurationBox = new System.Windows.Forms.MaskedTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this._engineBlockTimeBox = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this._engineBlockPuskBox = new System.Windows.Forms.MaskedTextBox();
            this._engineBlockHeatBox = new System.Windows.Forms.MaskedTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this._outputSignalsPage = new System.Windows.Forms.TabPage();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this.treeViewForVLS = new System.Windows.Forms.TreeView();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this._fault4CheckBox = new System.Windows.Forms.CheckBox();
            this._fault3CheckBox = new System.Windows.Forms.CheckBox();
            this._fault2CheckBox = new System.Windows.Forms.CheckBox();
            this._fault1CheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this._impTB = new System.Windows.Forms.MaskedTextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.VLSTabControl = new System.Windows.Forms.TabControl();
            this.VLS1 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.VLS2 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.VLS3 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox3 = new System.Windows.Forms.CheckedListBox();
            this.VLS4 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox4 = new System.Windows.Forms.CheckedListBox();
            this.VLS5 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox5 = new System.Windows.Forms.CheckedListBox();
            this.VLS6 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox6 = new System.Windows.Forms.CheckedListBox();
            this.VLS7 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox7 = new System.Windows.Forms.CheckedListBox();
            this.VLS8 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox8 = new System.Windows.Forms.CheckedListBox();
            this.VLS9 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox9 = new System.Windows.Forms.CheckedListBox();
            this.VLS10 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox10 = new System.Windows.Forms.CheckedListBox();
            this.VLS11 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox11 = new System.Windows.Forms.CheckedListBox();
            this.VLS12 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox12 = new System.Windows.Forms.CheckedListBox();
            this.VLS13 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox13 = new System.Windows.Forms.CheckedListBox();
            this.VLS14 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox14 = new System.Windows.Forms.CheckedListBox();
            this.VLS15 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox15 = new System.Windows.Forms.CheckedListBox();
            this.VLS16 = new System.Windows.Forms.TabPage();
            this.VLScheckedListBox16 = new System.Windows.Forms.CheckedListBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this._outputIndicatorsGrid = new System.Windows.Forms.DataGridView();
            this._outIndNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._outIndTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._outIndColorCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this._outputReleGrid = new System.Windows.Forms.DataGridView();
            this._systemPage = new System.Windows.Forms.TabPage();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this._oscChannels = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._oscSygnal = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.oscStartCb = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this._oscSizeTextBox = new System.Windows.Forms.MaskedTextBox();
            this._oscWriteLength = new System.Windows.Forms.MaskedTextBox();
            this._oscFix = new System.Windows.Forms.ComboBox();
            this._oscLength = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this._automatPage = new System.Windows.Forms.TabPage();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label127 = new System.Windows.Forms.Label();
            this._lzhVal = new System.Windows.Forms.MaskedTextBox();
            this._lzhModes = new System.Windows.Forms.ComboBox();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this._avrClear = new System.Windows.Forms.ComboBox();
            this.label124 = new System.Windows.Forms.Label();
            this._avrTOff = new System.Windows.Forms.MaskedTextBox();
            this.label123 = new System.Windows.Forms.Label();
            this._avrTBack = new System.Windows.Forms.MaskedTextBox();
            this.label122 = new System.Windows.Forms.Label();
            this._avrBack = new System.Windows.Forms.ComboBox();
            this.label121 = new System.Windows.Forms.Label();
            this._avrTSr = new System.Windows.Forms.MaskedTextBox();
            this.label120 = new System.Windows.Forms.Label();
            this._avrResolve = new System.Windows.Forms.ComboBox();
            this.label119 = new System.Windows.Forms.Label();
            this._avrBlockClear = new System.Windows.Forms.ComboBox();
            this.label118 = new System.Windows.Forms.Label();
            this._avrBlocking = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this._avrSIGNOn = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this._avrByDiff = new System.Windows.Forms.ComboBox();
            this.label115 = new System.Windows.Forms.Label();
            this._avrBySelfOff = new System.Windows.Forms.ComboBox();
            this.label114 = new System.Windows.Forms.Label();
            this._avrByOff = new System.Windows.Forms.ComboBox();
            this.label113 = new System.Windows.Forms.Label();
            this._avrBySignal = new System.Windows.Forms.ComboBox();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this._apv4Krat = new System.Windows.Forms.MaskedTextBox();
            this._apv3Krat = new System.Windows.Forms.MaskedTextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this._apvOff = new System.Windows.Forms.ComboBox();
            this._apv2Krat = new System.Windows.Forms.MaskedTextBox();
            this._apv1Krat = new System.Windows.Forms.MaskedTextBox();
            this._apvTReady = new System.Windows.Forms.MaskedTextBox();
            this._apvTBlock = new System.Windows.Forms.MaskedTextBox();
            this._apvBlocking = new System.Windows.Forms.ComboBox();
            this.label95 = new System.Windows.Forms.Label();
            this._apvModes = new System.Windows.Forms.ComboBox();
            this.label94 = new System.Windows.Forms.Label();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this._switchKontCep = new System.Windows.Forms.ComboBox();
            this._switchTUskor = new System.Windows.Forms.MaskedTextBox();
            this._switchImp = new System.Windows.Forms.MaskedTextBox();
            this._switchIUrov = new System.Windows.Forms.MaskedTextBox();
            this._switchTUrov = new System.Windows.Forms.MaskedTextBox();
            this._switchBlock = new System.Windows.Forms.ComboBox();
            this._switchError = new System.Windows.Forms.ComboBox();
            this._switchOn = new System.Windows.Forms.ComboBox();
            this._switchOff = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this._switchSDTU = new System.Windows.Forms.ComboBox();
            this._switchVnesh = new System.Windows.Forms.ComboBox();
            this._switchKey = new System.Windows.Forms.ComboBox();
            this._switchButtons = new System.Windows.Forms.ComboBox();
            this._switchVneshOff = new System.Windows.Forms.ComboBox();
            this._switchVneshOn = new System.Windows.Forms.ComboBox();
            this._switchKeyOff = new System.Windows.Forms.ComboBox();
            this._switchKeyOn = new System.Windows.Forms.ComboBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.tabPage29 = new System.Windows.Forms.TabPage();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this._sinhrAutodFno = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this._sinhrAutodF = new System.Windows.Forms.MaskedTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this._sinhrAutodFi = new System.Windows.Forms.MaskedTextBox();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this._sinhrAutoNoNo = new System.Windows.Forms.ComboBox();
            this._discretIn2Cmb = new System.Windows.Forms.ComboBox();
            this._sinhrAutoYesNo = new System.Windows.Forms.ComboBox();
            this._discretIn1Cmb = new System.Windows.Forms.ComboBox();
            this._sinhrAutoNoYes = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this._sinhrAutoUmax = new System.Windows.Forms.MaskedTextBox();
            this._blockSinhCmb = new System.Windows.Forms.ComboBox();
            this._sinhrAutoMode = new System.Windows.Forms.ComboBox();
            this._blockSinhLabel = new System.Windows.Forms.Label();
            this._dUmaxAutoLabel = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this._sinhrManualdFno = new System.Windows.Forms.MaskedTextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this._sinhrManualdF = new System.Windows.Forms.MaskedTextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this._sinhrManualdFi = new System.Windows.Forms.MaskedTextBox();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this._sinhrManualNoNo = new System.Windows.Forms.ComboBox();
            this._sinhrManualYesNo = new System.Windows.Forms.ComboBox();
            this._sinhrManualNoYes = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this._sinhrManualUmax = new System.Windows.Forms.MaskedTextBox();
            this._sinhrManualMode = new System.Windows.Forms.ComboBox();
            this._dUmaxManualLabel = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this._sinhrF = new System.Windows.Forms.MaskedTextBox();
            this._sinhrTon = new System.Windows.Forms.MaskedTextBox();
            this._sinhrKamp = new System.Windows.Forms.MaskedTextBox();
            this._sinhrTsinhr = new System.Windows.Forms.MaskedTextBox();
            this._fLabel = new System.Windows.Forms.Label();
            this._sinhrTwait = new System.Windows.Forms.MaskedTextBox();
            this._KampLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this._sinhrUmaxNal = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminNal = new System.Windows.Forms.MaskedTextBox();
            this._sinhrUminOts = new System.Windows.Forms.MaskedTextBox();
            this._sinhrU2 = new System.Windows.Forms.ComboBox();
            this._sinhrU1 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this._saveToXmlButton = new System.Windows.Forms.Button();
            this._resetSetpointsButton = new System.Windows.Forms.Button();
            this._writeConfigBut = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this._statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._readConfigBut = new System.Windows.Forms.Button();
            this._saveConfigBut = new System.Windows.Forms.Button();
            this._loadConfigBut = new System.Windows.Forms.Button();
            this._saveConfigurationDlg = new System.Windows.Forms.SaveFileDialog();
            this._openConfigurationDlg = new System.Windows.Forms.OpenFileDialog();
            this._toolTip = new System.Windows.Forms.ToolTip(this.components);
            this._saveXmlDialog = new System.Windows.Forms.SaveFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this._releNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._releTypeCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releSignalCol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this._releWaitCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._configurationTabControl.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this._measureTransPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this._inputSygnalsPage.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).BeginInit();
            this.tabPage28.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this._allDefensesPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this._difensesTC.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI8DataGrid)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI67DataGrid)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesIDataGrid)).BeginInit();
            this.tabPage22.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).BeginInit();
            this.tabPage19.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage20.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.tabPage23.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).BeginInit();
            this.tabPage24.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).BeginInit();
            this.tabPage26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).BeginInit();
            this.tabPage27.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).BeginInit();
            this.tabPage25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).BeginInit();
            this.groupBox25.SuspendLayout();
            this.groupBox43.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this._outputSignalsPage.SuspendLayout();
            this.groupBox46.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.VLSTabControl.SuspendLayout();
            this.VLS1.SuspendLayout();
            this.VLS2.SuspendLayout();
            this.VLS3.SuspendLayout();
            this.VLS4.SuspendLayout();
            this.VLS5.SuspendLayout();
            this.VLS6.SuspendLayout();
            this.VLS7.SuspendLayout();
            this.VLS8.SuspendLayout();
            this.VLS9.SuspendLayout();
            this.VLS10.SuspendLayout();
            this.VLS11.SuspendLayout();
            this.VLS12.SuspendLayout();
            this.VLS13.SuspendLayout();
            this.VLS14.SuspendLayout();
            this.VLS15.SuspendLayout();
            this.VLS16.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).BeginInit();
            this._systemPage.SuspendLayout();
            this.groupBox45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._oscChannels)).BeginInit();
            this.groupBox3.SuspendLayout();
            this._automatPage.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.tabPage29.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.groupBox38.SuspendLayout();
            this.groupBox37.SuspendLayout();
            this.groupBox36.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _configurationTabControl
            // 
            this._configurationTabControl.ContextMenuStrip = this.contextMenu;
            this._configurationTabControl.Controls.Add(this._measureTransPage);
            this._configurationTabControl.Controls.Add(this._inputSygnalsPage);
            this._configurationTabControl.Controls.Add(this.tabPage28);
            this._configurationTabControl.Controls.Add(this._allDefensesPage);
            this._configurationTabControl.Controls.Add(this._outputSignalsPage);
            this._configurationTabControl.Controls.Add(this._systemPage);
            this._configurationTabControl.Controls.Add(this._automatPage);
            this._configurationTabControl.Controls.Add(this.tabPage29);
            this._configurationTabControl.Dock = System.Windows.Forms.DockStyle.Top;
            this._configurationTabControl.Location = new System.Drawing.Point(0, 0);
            this._configurationTabControl.MinimumSize = new System.Drawing.Size(820, 579);
            this._configurationTabControl.Name = "_configurationTabControl";
            this._configurationTabControl.SelectedIndex = 0;
            this._configurationTabControl.Size = new System.Drawing.Size(1029, 579);
            this._configurationTabControl.TabIndex = 31;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.readFromDeviceItem,
            this.writeToDeviceItem,
            this.readFromFileItem,
            this.writeToFileItem,
            this.writeToHtmlItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(213, 114);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            this.contextMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenu_ItemClicked);
            // 
            // readFromDeviceItem
            // 
            this.readFromDeviceItem.Name = "readFromDeviceItem";
            this.readFromDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.readFromDeviceItem.Text = "Прочитать из устройства";
            // 
            // writeToDeviceItem
            // 
            this.writeToDeviceItem.Name = "writeToDeviceItem";
            this.writeToDeviceItem.Size = new System.Drawing.Size(212, 22);
            this.writeToDeviceItem.Text = "Записать в устройство";
            // 
            // readFromFileItem
            // 
            this.readFromFileItem.Name = "readFromFileItem";
            this.readFromFileItem.Size = new System.Drawing.Size(212, 22);
            this.readFromFileItem.Text = "Загрузить из файла";
            // 
            // writeToFileItem
            // 
            this.writeToFileItem.Name = "writeToFileItem";
            this.writeToFileItem.Size = new System.Drawing.Size(212, 22);
            this.writeToFileItem.Text = "Сохранить в файл";
            // 
            // writeToHtmlItem
            // 
            this.writeToHtmlItem.Name = "writeToHtmlItem";
            this.writeToHtmlItem.Size = new System.Drawing.Size(212, 22);
            this.writeToHtmlItem.Text = "Сохранить в HTML";
            // 
            // _measureTransPage
            // 
            this._measureTransPage.Controls.Add(this.groupBox2);
            this._measureTransPage.Controls.Add(this.groupBox1);
            this._measureTransPage.Controls.Add(this.groupBox4);
            this._measureTransPage.Location = new System.Drawing.Point(4, 22);
            this._measureTransPage.Name = "_measureTransPage";
            this._measureTransPage.Size = new System.Drawing.Size(1021, 553);
            this._measureTransPage.TabIndex = 2;
            this._measureTransPage.Text = "Параметры измерений";
            this._measureTransPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this._Xline_Box);
            this.groupBox2.Controls.Add(this._OMPmode_combo);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(8, 139);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(207, 89);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ОМП";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Х линии, Ом/км";
            // 
            // _Xline_Box
            // 
            this._Xline_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Xline_Box.Location = new System.Drawing.Point(135, 55);
            this._Xline_Box.Name = "_Xline_Box";
            this._Xline_Box.Size = new System.Drawing.Size(60, 20);
            this._Xline_Box.TabIndex = 25;
            this._Xline_Box.Tag = "1500";
            this._Xline_Box.Text = "0";
            this._Xline_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _OMPmode_combo
            // 
            this._OMPmode_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._OMPmode_combo.FormattingEnabled = true;
            this._OMPmode_combo.Location = new System.Drawing.Point(75, 24);
            this._OMPmode_combo.Name = "_OMPmode_combo";
            this._OMPmode_combo.Size = new System.Drawing.Size(120, 21);
            this._OMPmode_combo.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Режим";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._KTHX1koef_combo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label76);
            this.groupBox1.Controls.Add(this._KTHX1_Box);
            this.groupBox1.Controls.Add(this._errorX_combo);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this._KTHXkoef_combo);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._errorL_combo);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this._KTHLkoef_combo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this._Uo_typeCombo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this._KTHX_Box);
            this.groupBox1.Controls.Add(this._KTHL_Box);
            this.groupBox1.Location = new System.Drawing.Point(221, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(303, 199);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Напряжения";
            // 
            // _KTHX1koef_combo
            // 
            this._KTHX1koef_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHX1koef_combo.FormattingEnabled = true;
            this._KTHX1koef_combo.Location = new System.Drawing.Point(229, 163);
            this._KTHX1koef_combo.Name = "_KTHX1koef_combo";
            this._KTHX1koef_combo.Size = new System.Drawing.Size(66, 21);
            this._KTHX1koef_combo.TabIndex = 44;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(211, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(12, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "x";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(16, 166);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(41, 13);
            this.label76.TabIndex = 42;
            this.label76.Text = "КТНn1";
            // 
            // _KTHX1_Box
            // 
            this._KTHX1_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHX1_Box.Location = new System.Drawing.Point(145, 163);
            this._KTHX1_Box.Name = "_KTHX1_Box";
            this._KTHX1_Box.Size = new System.Drawing.Size(60, 20);
            this._KTHX1_Box.TabIndex = 41;
            this._KTHX1_Box.Tag = "100";
            this._KTHX1_Box.Text = "0";
            this._KTHX1_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _errorX_combo
            // 
            this._errorX_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._errorX_combo.FormattingEnabled = true;
            this._errorX_combo.Location = new System.Drawing.Point(129, 136);
            this._errorX_combo.Name = "_errorX_combo";
            this._errorX_combo.Size = new System.Drawing.Size(76, 21);
            this._errorX_combo.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Неиспр. ТНn";
            // 
            // _KTHXkoef_combo
            // 
            this._KTHXkoef_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHXkoef_combo.FormattingEnabled = true;
            this._KTHXkoef_combo.Location = new System.Drawing.Point(229, 109);
            this._KTHXkoef_combo.Name = "_KTHXkoef_combo";
            this._KTHXkoef_combo.Size = new System.Drawing.Size(66, 21);
            this._KTHXkoef_combo.TabIndex = 38;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(211, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "x";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 36;
            this.label2.Text = "КТНn";
            // 
            // _errorL_combo
            // 
            this._errorL_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._errorL_combo.FormattingEnabled = true;
            this._errorL_combo.Location = new System.Drawing.Point(129, 81);
            this._errorL_combo.Name = "_errorL_combo";
            this._errorL_combo.Size = new System.Drawing.Size(76, 21);
            this._errorL_combo.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Неиспр. ТНф";
            // 
            // _KTHLkoef_combo
            // 
            this._KTHLkoef_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._KTHLkoef_combo.FormattingEnabled = true;
            this._KTHLkoef_combo.Location = new System.Drawing.Point(229, 54);
            this._KTHLkoef_combo.Name = "_KTHLkoef_combo";
            this._KTHLkoef_combo.Size = new System.Drawing.Size(66, 21);
            this._KTHLkoef_combo.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "x";
            // 
            // _Uo_typeCombo
            // 
            this._Uo_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._Uo_typeCombo.FormattingEnabled = true;
            this._Uo_typeCombo.Location = new System.Drawing.Point(66, 26);
            this._Uo_typeCombo.Name = "_Uo_typeCombo";
            this._Uo_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._Uo_typeCombo.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Uo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "КТНф";
            // 
            // _KTHX_Box
            // 
            this._KTHX_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHX_Box.Location = new System.Drawing.Point(145, 109);
            this._KTHX_Box.Name = "_KTHX_Box";
            this._KTHX_Box.Size = new System.Drawing.Size(60, 20);
            this._KTHX_Box.TabIndex = 24;
            this._KTHX_Box.Tag = "100";
            this._KTHX_Box.Text = "0";
            this._KTHX_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _KTHL_Box
            // 
            this._KTHL_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._KTHL_Box.Location = new System.Drawing.Point(145, 54);
            this._KTHL_Box.Name = "_KTHL_Box";
            this._KTHL_Box.Size = new System.Drawing.Size(60, 20);
            this._KTHL_Box.TabIndex = 23;
            this._KTHL_Box.Tag = "1500";
            this._KTHL_Box.Text = "0";
            this._KTHL_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._TT_typeCombo);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label65);
            this.groupBox4.Controls.Add(this._ITTL_Box);
            this.groupBox4.Controls.Add(this._Im_Box);
            this.groupBox4.Location = new System.Drawing.Point(8, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(207, 130);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Тока";
            // 
            // _TT_typeCombo
            // 
            this._TT_typeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._TT_typeCombo.FormattingEnabled = true;
            this._TT_typeCombo.Location = new System.Drawing.Point(56, 26);
            this._TT_typeCombo.Name = "_TT_typeCombo";
            this._TT_typeCombo.Size = new System.Drawing.Size(139, 21);
            this._TT_typeCombo.TabIndex = 22;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 29);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "Тип ТT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Iм";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 101);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(45, 13);
            this.label65.TabIndex = 17;
            this.label65.Text = "ITTф, A";
            // 
            // _ITTL_Box
            // 
            this._ITTL_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ITTL_Box.Location = new System.Drawing.Point(135, 98);
            this._ITTL_Box.Name = "_ITTL_Box";
            this._ITTL_Box.Size = new System.Drawing.Size(60, 20);
            this._ITTL_Box.TabIndex = 16;
            this._ITTL_Box.Tag = "100";
            this._ITTL_Box.Text = "0";
            this._ITTL_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _Im_Box
            // 
            this._Im_Box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Im_Box.Location = new System.Drawing.Point(135, 62);
            this._Im_Box.Name = "_Im_Box";
            this._Im_Box.Size = new System.Drawing.Size(60, 20);
            this._Im_Box.TabIndex = 15;
            this._Im_Box.Tag = "1500";
            this._Im_Box.Text = "0";
            this._Im_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inputSygnalsPage
            // 
            this._inputSygnalsPage.Controls.Add(this.groupBox47);
            this._inputSygnalsPage.Controls.Add(this.groupBox48);
            this._inputSygnalsPage.Controls.Add(this.groupBox18);
            this._inputSygnalsPage.Controls.Add(this.groupBox15);
            this._inputSygnalsPage.Controls.Add(this.groupBox17);
            this._inputSygnalsPage.Controls.Add(this.groupBox14);
            this._inputSygnalsPage.Location = new System.Drawing.Point(4, 22);
            this._inputSygnalsPage.Name = "_inputSygnalsPage";
            this._inputSygnalsPage.Size = new System.Drawing.Size(1021, 553);
            this._inputSygnalsPage.TabIndex = 7;
            this._inputSygnalsPage.Text = "Входные сигналы";
            this._inputSygnalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox47
            // 
            this.groupBox47.Controls.Add(this.treeViewForLsOR);
            this.groupBox47.Location = new System.Drawing.Point(797, 3);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(200, 546);
            this.groupBox47.TabIndex = 16;
            this.groupBox47.TabStop = false;
            this.groupBox47.Text = "Логические сигналы ИЛИ";
            // 
            // treeViewForLsOR
            // 
            this.treeViewForLsOR.Location = new System.Drawing.Point(6, 19);
            this.treeViewForLsOR.Name = "treeViewForLsOR";
            this.treeViewForLsOR.Size = new System.Drawing.Size(188, 520);
            this.treeViewForLsOR.TabIndex = 0;
            this.treeViewForLsOR.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsOR_NodeMouseClick);
            // 
            // groupBox48
            // 
            this.groupBox48.Controls.Add(this.treeViewForLsAND);
            this.groupBox48.Location = new System.Drawing.Point(591, 3);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(200, 546);
            this.groupBox48.TabIndex = 15;
            this.groupBox48.TabStop = false;
            this.groupBox48.Text = "Логические сигналы И";
            // 
            // treeViewForLsAND
            // 
            this.treeViewForLsAND.Location = new System.Drawing.Point(6, 19);
            this.treeViewForLsAND.Name = "treeViewForLsAND";
            this.treeViewForLsAND.Size = new System.Drawing.Size(188, 520);
            this.treeViewForLsAND.TabIndex = 0;
            this.treeViewForLsAND.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForLsAND_NodeMouseClick);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this._indComboBox);
            this.groupBox18.Location = new System.Drawing.Point(406, 61);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(179, 52);
            this.groupBox18.TabIndex = 4;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Сброс индикации";
            // 
            // _indComboBox
            // 
            this._indComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._indComboBox.FormattingEnabled = true;
            this._indComboBox.Location = new System.Drawing.Point(6, 19);
            this._indComboBox.Name = "_indComboBox";
            this._indComboBox.Size = new System.Drawing.Size(167, 21);
            this._indComboBox.TabIndex = 0;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this._grUstComboBox);
            this.groupBox15.Location = new System.Drawing.Point(406, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(179, 52);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Аварийная группа уставок";
            // 
            // _grUstComboBox
            // 
            this._grUstComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._grUstComboBox.FormattingEnabled = true;
            this._grUstComboBox.Location = new System.Drawing.Point(6, 19);
            this._grUstComboBox.Name = "_grUstComboBox";
            this._grUstComboBox.Size = new System.Drawing.Size(167, 21);
            this._grUstComboBox.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.tabControl2);
            this.groupBox17.Location = new System.Drawing.Point(207, 3);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(193, 547);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Логические сигналы ИЛИ";
            // 
            // tabControl2
            // 
            this.tabControl2.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Controls.Add(this.tabPage15);
            this.tabControl2.Controls.Add(this.tabPage16);
            this.tabControl2.Location = new System.Drawing.Point(6, 18);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(181, 528);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this._inputSignals9);
            this.tabPage9.Location = new System.Drawing.Point(4, 49);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(173, 475);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "ЛС9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // _inputSignals9
            // 
            this._inputSignals9.AllowUserToAddRows = false;
            this._inputSignals9.AllowUserToDeleteRows = false;
            this._inputSignals9.AllowUserToResizeColumns = false;
            this._inputSignals9.AllowUserToResizeRows = false;
            this._inputSignals9.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals9.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals9.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._signalValueNumILI,
            this._signalValueColILI});
            this._inputSignals9.Location = new System.Drawing.Point(3, 3);
            this._inputSignals9.MultiSelect = false;
            this._inputSignals9.Name = "_inputSignals9";
            this._inputSignals9.RowHeadersVisible = false;
            this._inputSignals9.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals9.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this._inputSignals9.RowTemplate.Height = 20;
            this._inputSignals9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals9.ShowCellErrors = false;
            this._inputSignals9.ShowCellToolTips = false;
            this._inputSignals9.ShowEditingIcon = false;
            this._inputSignals9.ShowRowErrors = false;
            this._inputSignals9.Size = new System.Drawing.Size(167, 469);
            this._inputSignals9.TabIndex = 2;
            this._inputSignals9.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // _signalValueNumILI
            // 
            this._signalValueNumILI.HeaderText = "№";
            this._signalValueNumILI.Name = "_signalValueNumILI";
            this._signalValueNumILI.ReadOnly = true;
            this._signalValueNumILI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._signalValueNumILI.Width = 24;
            // 
            // _signalValueColILI
            // 
            this._signalValueColILI.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueColILI.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueColILI.HeaderText = "Значение";
            this._signalValueColILI.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueColILI.Name = "_signalValueColILI";
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this._inputSignals10);
            this.tabPage10.Location = new System.Drawing.Point(4, 49);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(173, 475);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "ЛС10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // _inputSignals10
            // 
            this._inputSignals10.AllowUserToAddRows = false;
            this._inputSignals10.AllowUserToDeleteRows = false;
            this._inputSignals10.AllowUserToResizeColumns = false;
            this._inputSignals10.AllowUserToResizeRows = false;
            this._inputSignals10.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals10.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewComboBoxColumn8});
            this._inputSignals10.Location = new System.Drawing.Point(3, 3);
            this._inputSignals10.MultiSelect = false;
            this._inputSignals10.Name = "_inputSignals10";
            this._inputSignals10.RowHeadersVisible = false;
            this._inputSignals10.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals10.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this._inputSignals10.RowTemplate.Height = 20;
            this._inputSignals10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals10.ShowCellErrors = false;
            this._inputSignals10.ShowCellToolTips = false;
            this._inputSignals10.ShowEditingIcon = false;
            this._inputSignals10.ShowRowErrors = false;
            this._inputSignals10.Size = new System.Drawing.Size(167, 469);
            this._inputSignals10.TabIndex = 4;
            this._inputSignals10.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "№";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Width = 24;
            // 
            // dataGridViewComboBoxColumn8
            // 
            this.dataGridViewComboBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn8.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn8.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn8.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn8.Name = "dataGridViewComboBoxColumn8";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this._inputSignals11);
            this.tabPage11.Location = new System.Drawing.Point(4, 49);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(173, 475);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "ЛС11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // _inputSignals11
            // 
            this._inputSignals11.AllowUserToAddRows = false;
            this._inputSignals11.AllowUserToDeleteRows = false;
            this._inputSignals11.AllowUserToResizeColumns = false;
            this._inputSignals11.AllowUserToResizeRows = false;
            this._inputSignals11.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals11.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals11.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals11.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewComboBoxColumn9});
            this._inputSignals11.Location = new System.Drawing.Point(3, 3);
            this._inputSignals11.MultiSelect = false;
            this._inputSignals11.Name = "_inputSignals11";
            this._inputSignals11.RowHeadersVisible = false;
            this._inputSignals11.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals11.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this._inputSignals11.RowTemplate.Height = 20;
            this._inputSignals11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals11.ShowCellErrors = false;
            this._inputSignals11.ShowCellToolTips = false;
            this._inputSignals11.ShowEditingIcon = false;
            this._inputSignals11.ShowRowErrors = false;
            this._inputSignals11.Size = new System.Drawing.Size(167, 469);
            this._inputSignals11.TabIndex = 4;
            this._inputSignals11.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "№";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Width = 24;
            // 
            // dataGridViewComboBoxColumn9
            // 
            this.dataGridViewComboBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn9.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn9.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn9.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn9.Name = "dataGridViewComboBoxColumn9";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this._inputSignals12);
            this.tabPage12.Location = new System.Drawing.Point(4, 49);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(173, 475);
            this.tabPage12.TabIndex = 3;
            this.tabPage12.Text = "ЛС12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // _inputSignals12
            // 
            this._inputSignals12.AllowUserToAddRows = false;
            this._inputSignals12.AllowUserToDeleteRows = false;
            this._inputSignals12.AllowUserToResizeColumns = false;
            this._inputSignals12.AllowUserToResizeRows = false;
            this._inputSignals12.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals12.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals12.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals12.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewComboBoxColumn10});
            this._inputSignals12.Location = new System.Drawing.Point(3, 3);
            this._inputSignals12.MultiSelect = false;
            this._inputSignals12.Name = "_inputSignals12";
            this._inputSignals12.RowHeadersVisible = false;
            this._inputSignals12.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals12.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this._inputSignals12.RowTemplate.Height = 20;
            this._inputSignals12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals12.ShowCellErrors = false;
            this._inputSignals12.ShowCellToolTips = false;
            this._inputSignals12.ShowEditingIcon = false;
            this._inputSignals12.ShowRowErrors = false;
            this._inputSignals12.Size = new System.Drawing.Size(167, 469);
            this._inputSignals12.TabIndex = 4;
            this._inputSignals12.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "№";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 24;
            // 
            // dataGridViewComboBoxColumn10
            // 
            this.dataGridViewComboBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn10.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn10.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn10.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn10.Name = "dataGridViewComboBoxColumn10";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this._inputSignals13);
            this.tabPage13.Location = new System.Drawing.Point(4, 49);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(173, 475);
            this.tabPage13.TabIndex = 4;
            this.tabPage13.Text = "ЛС13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // _inputSignals13
            // 
            this._inputSignals13.AllowUserToAddRows = false;
            this._inputSignals13.AllowUserToDeleteRows = false;
            this._inputSignals13.AllowUserToResizeColumns = false;
            this._inputSignals13.AllowUserToResizeRows = false;
            this._inputSignals13.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals13.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals13.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals13.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewComboBoxColumn11});
            this._inputSignals13.Location = new System.Drawing.Point(3, 3);
            this._inputSignals13.MultiSelect = false;
            this._inputSignals13.Name = "_inputSignals13";
            this._inputSignals13.RowHeadersVisible = false;
            this._inputSignals13.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals13.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._inputSignals13.RowTemplate.Height = 20;
            this._inputSignals13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals13.ShowCellErrors = false;
            this._inputSignals13.ShowCellToolTips = false;
            this._inputSignals13.ShowEditingIcon = false;
            this._inputSignals13.ShowRowErrors = false;
            this._inputSignals13.Size = new System.Drawing.Size(167, 469);
            this._inputSignals13.TabIndex = 4;
            this._inputSignals13.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "№";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 24;
            // 
            // dataGridViewComboBoxColumn11
            // 
            this.dataGridViewComboBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn11.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn11.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn11.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn11.Name = "dataGridViewComboBoxColumn11";
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this._inputSignals14);
            this.tabPage14.Location = new System.Drawing.Point(4, 49);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(173, 475);
            this.tabPage14.TabIndex = 5;
            this.tabPage14.Text = "ЛС14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // _inputSignals14
            // 
            this._inputSignals14.AllowUserToAddRows = false;
            this._inputSignals14.AllowUserToDeleteRows = false;
            this._inputSignals14.AllowUserToResizeColumns = false;
            this._inputSignals14.AllowUserToResizeRows = false;
            this._inputSignals14.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals14.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals14.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals14.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewComboBoxColumn12});
            this._inputSignals14.Location = new System.Drawing.Point(3, 3);
            this._inputSignals14.MultiSelect = false;
            this._inputSignals14.Name = "_inputSignals14";
            this._inputSignals14.RowHeadersVisible = false;
            this._inputSignals14.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals14.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this._inputSignals14.RowTemplate.Height = 20;
            this._inputSignals14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals14.ShowCellErrors = false;
            this._inputSignals14.ShowCellToolTips = false;
            this._inputSignals14.ShowEditingIcon = false;
            this._inputSignals14.ShowRowErrors = false;
            this._inputSignals14.Size = new System.Drawing.Size(167, 469);
            this._inputSignals14.TabIndex = 4;
            this._inputSignals14.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "№";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 24;
            // 
            // dataGridViewComboBoxColumn12
            // 
            this.dataGridViewComboBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn12.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn12.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn12.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn12.Name = "dataGridViewComboBoxColumn12";
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this._inputSignals15);
            this.tabPage15.Location = new System.Drawing.Point(4, 49);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(173, 475);
            this.tabPage15.TabIndex = 6;
            this.tabPage15.Text = "ЛС15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // _inputSignals15
            // 
            this._inputSignals15.AllowUserToAddRows = false;
            this._inputSignals15.AllowUserToDeleteRows = false;
            this._inputSignals15.AllowUserToResizeColumns = false;
            this._inputSignals15.AllowUserToResizeRows = false;
            this._inputSignals15.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals15.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals15.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals15.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewComboBoxColumn13});
            this._inputSignals15.Location = new System.Drawing.Point(3, 3);
            this._inputSignals15.MultiSelect = false;
            this._inputSignals15.Name = "_inputSignals15";
            this._inputSignals15.RowHeadersVisible = false;
            this._inputSignals15.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals15.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this._inputSignals15.RowTemplate.Height = 20;
            this._inputSignals15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals15.ShowCellErrors = false;
            this._inputSignals15.ShowCellToolTips = false;
            this._inputSignals15.ShowEditingIcon = false;
            this._inputSignals15.ShowRowErrors = false;
            this._inputSignals15.Size = new System.Drawing.Size(167, 469);
            this._inputSignals15.TabIndex = 4;
            this._inputSignals15.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "№";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 24;
            // 
            // dataGridViewComboBoxColumn13
            // 
            this.dataGridViewComboBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn13.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn13.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn13.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn13.Name = "dataGridViewComboBoxColumn13";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this._inputSignals16);
            this.tabPage16.Location = new System.Drawing.Point(4, 49);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Size = new System.Drawing.Size(173, 475);
            this.tabPage16.TabIndex = 7;
            this.tabPage16.Text = "ЛС16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // _inputSignals16
            // 
            this._inputSignals16.AllowUserToAddRows = false;
            this._inputSignals16.AllowUserToDeleteRows = false;
            this._inputSignals16.AllowUserToResizeColumns = false;
            this._inputSignals16.AllowUserToResizeRows = false;
            this._inputSignals16.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals16.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals16.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals16.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewComboBoxColumn14});
            this._inputSignals16.Location = new System.Drawing.Point(3, 3);
            this._inputSignals16.MultiSelect = false;
            this._inputSignals16.Name = "_inputSignals16";
            this._inputSignals16.RowHeadersVisible = false;
            this._inputSignals16.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals16.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this._inputSignals16.RowTemplate.Height = 20;
            this._inputSignals16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals16.ShowCellErrors = false;
            this._inputSignals16.ShowCellToolTips = false;
            this._inputSignals16.ShowEditingIcon = false;
            this._inputSignals16.ShowRowErrors = false;
            this._inputSignals16.Size = new System.Drawing.Size(167, 469);
            this._inputSignals16.TabIndex = 4;
            this._inputSignals16.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals9_16CellEndEdit);
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "№";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 24;
            // 
            // dataGridViewComboBoxColumn14
            // 
            this.dataGridViewComboBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn14.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn14.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn14.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn14.Name = "dataGridViewComboBoxColumn14";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.tabControl1);
            this.groupBox14.Location = new System.Drawing.Point(8, 3);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(193, 547);
            this.groupBox14.TabIndex = 0;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Логические сигналы И";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(6, 18);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(181, 528);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._inputSignals1);
            this.tabPage1.Location = new System.Drawing.Point(4, 49);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(173, 475);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ЛС1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _inputSignals1
            // 
            this._inputSignals1.AllowUserToAddRows = false;
            this._inputSignals1.AllowUserToDeleteRows = false;
            this._inputSignals1.AllowUserToResizeColumns = false;
            this._inputSignals1.AllowUserToResizeRows = false;
            this._inputSignals1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals1.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._lsChannelCol,
            this._signalValueCol});
            this._inputSignals1.Location = new System.Drawing.Point(3, 3);
            this._inputSignals1.MultiSelect = false;
            this._inputSignals1.Name = "_inputSignals1";
            this._inputSignals1.RowHeadersVisible = false;
            this._inputSignals1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals1.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this._inputSignals1.RowTemplate.Height = 20;
            this._inputSignals1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals1.ShowCellErrors = false;
            this._inputSignals1.ShowCellToolTips = false;
            this._inputSignals1.ShowEditingIcon = false;
            this._inputSignals1.ShowRowErrors = false;
            this._inputSignals1.Size = new System.Drawing.Size(167, 469);
            this._inputSignals1.TabIndex = 2;
            this._inputSignals1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // _lsChannelCol
            // 
            this._lsChannelCol.HeaderText = "№";
            this._lsChannelCol.Name = "_lsChannelCol";
            this._lsChannelCol.ReadOnly = true;
            this._lsChannelCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._lsChannelCol.Width = 24;
            // 
            // _signalValueCol
            // 
            this._signalValueCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this._signalValueCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._signalValueCol.HeaderText = "Значение";
            this._signalValueCol.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this._signalValueCol.Name = "_signalValueCol";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._inputSignals2);
            this.tabPage2.Location = new System.Drawing.Point(4, 49);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(173, 475);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ЛС2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _inputSignals2
            // 
            this._inputSignals2.AllowUserToAddRows = false;
            this._inputSignals2.AllowUserToDeleteRows = false;
            this._inputSignals2.AllowUserToResizeColumns = false;
            this._inputSignals2.AllowUserToResizeRows = false;
            this._inputSignals2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals2.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewComboBoxColumn1});
            this._inputSignals2.Location = new System.Drawing.Point(3, 3);
            this._inputSignals2.MultiSelect = false;
            this._inputSignals2.Name = "_inputSignals2";
            this._inputSignals2.RowHeadersVisible = false;
            this._inputSignals2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals2.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this._inputSignals2.RowTemplate.Height = 20;
            this._inputSignals2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals2.ShowCellErrors = false;
            this._inputSignals2.ShowCellToolTips = false;
            this._inputSignals2.ShowEditingIcon = false;
            this._inputSignals2.ShowRowErrors = false;
            this._inputSignals2.Size = new System.Drawing.Size(167, 469);
            this._inputSignals2.TabIndex = 3;
            this._inputSignals2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "№";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 24;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn1.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn1.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._inputSignals3);
            this.tabPage3.Location = new System.Drawing.Point(4, 49);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(173, 475);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ЛС3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _inputSignals3
            // 
            this._inputSignals3.AllowUserToAddRows = false;
            this._inputSignals3.AllowUserToDeleteRows = false;
            this._inputSignals3.AllowUserToResizeColumns = false;
            this._inputSignals3.AllowUserToResizeRows = false;
            this._inputSignals3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals3.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewComboBoxColumn2});
            this._inputSignals3.Location = new System.Drawing.Point(3, 3);
            this._inputSignals3.MultiSelect = false;
            this._inputSignals3.Name = "_inputSignals3";
            this._inputSignals3.RowHeadersVisible = false;
            this._inputSignals3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals3.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this._inputSignals3.RowTemplate.Height = 20;
            this._inputSignals3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals3.ShowCellErrors = false;
            this._inputSignals3.ShowCellToolTips = false;
            this._inputSignals3.ShowEditingIcon = false;
            this._inputSignals3.ShowRowErrors = false;
            this._inputSignals3.Size = new System.Drawing.Size(167, 469);
            this._inputSignals3.TabIndex = 3;
            this._inputSignals3.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "№";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 24;
            // 
            // dataGridViewComboBoxColumn2
            // 
            this.dataGridViewComboBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn2.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn2.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn2.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn2.Name = "dataGridViewComboBoxColumn2";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._inputSignals4);
            this.tabPage4.Location = new System.Drawing.Point(4, 49);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(173, 475);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ЛС4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _inputSignals4
            // 
            this._inputSignals4.AllowUserToAddRows = false;
            this._inputSignals4.AllowUserToDeleteRows = false;
            this._inputSignals4.AllowUserToResizeColumns = false;
            this._inputSignals4.AllowUserToResizeRows = false;
            this._inputSignals4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals4.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewComboBoxColumn3});
            this._inputSignals4.Location = new System.Drawing.Point(3, 3);
            this._inputSignals4.MultiSelect = false;
            this._inputSignals4.Name = "_inputSignals4";
            this._inputSignals4.RowHeadersVisible = false;
            this._inputSignals4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals4.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this._inputSignals4.RowTemplate.Height = 20;
            this._inputSignals4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals4.ShowCellErrors = false;
            this._inputSignals4.ShowCellToolTips = false;
            this._inputSignals4.ShowEditingIcon = false;
            this._inputSignals4.ShowRowErrors = false;
            this._inputSignals4.Size = new System.Drawing.Size(167, 469);
            this._inputSignals4.TabIndex = 3;
            this._inputSignals4.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "№";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 24;
            // 
            // dataGridViewComboBoxColumn3
            // 
            this.dataGridViewComboBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn3.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn3.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn3.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn3.Name = "dataGridViewComboBoxColumn3";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._inputSignals5);
            this.tabPage5.Location = new System.Drawing.Point(4, 49);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(173, 475);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ЛС5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _inputSignals5
            // 
            this._inputSignals5.AllowUserToAddRows = false;
            this._inputSignals5.AllowUserToDeleteRows = false;
            this._inputSignals5.AllowUserToResizeColumns = false;
            this._inputSignals5.AllowUserToResizeRows = false;
            this._inputSignals5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals5.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewComboBoxColumn4});
            this._inputSignals5.Location = new System.Drawing.Point(3, 3);
            this._inputSignals5.MultiSelect = false;
            this._inputSignals5.Name = "_inputSignals5";
            this._inputSignals5.RowHeadersVisible = false;
            this._inputSignals5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals5.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this._inputSignals5.RowTemplate.Height = 20;
            this._inputSignals5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals5.ShowCellErrors = false;
            this._inputSignals5.ShowCellToolTips = false;
            this._inputSignals5.ShowEditingIcon = false;
            this._inputSignals5.ShowRowErrors = false;
            this._inputSignals5.Size = new System.Drawing.Size(167, 469);
            this._inputSignals5.TabIndex = 3;
            this._inputSignals5.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "№";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 24;
            // 
            // dataGridViewComboBoxColumn4
            // 
            this.dataGridViewComboBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn4.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn4.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn4.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn4.Name = "dataGridViewComboBoxColumn4";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this._inputSignals6);
            this.tabPage6.Location = new System.Drawing.Point(4, 49);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(173, 475);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ЛС6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // _inputSignals6
            // 
            this._inputSignals6.AllowUserToAddRows = false;
            this._inputSignals6.AllowUserToDeleteRows = false;
            this._inputSignals6.AllowUserToResizeColumns = false;
            this._inputSignals6.AllowUserToResizeRows = false;
            this._inputSignals6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals6.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewComboBoxColumn5});
            this._inputSignals6.Location = new System.Drawing.Point(3, 3);
            this._inputSignals6.MultiSelect = false;
            this._inputSignals6.Name = "_inputSignals6";
            this._inputSignals6.RowHeadersVisible = false;
            this._inputSignals6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals6.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this._inputSignals6.RowTemplate.Height = 20;
            this._inputSignals6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals6.ShowCellErrors = false;
            this._inputSignals6.ShowCellToolTips = false;
            this._inputSignals6.ShowEditingIcon = false;
            this._inputSignals6.ShowRowErrors = false;
            this._inputSignals6.Size = new System.Drawing.Size(167, 469);
            this._inputSignals6.TabIndex = 3;
            this._inputSignals6.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "№";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 24;
            // 
            // dataGridViewComboBoxColumn5
            // 
            this.dataGridViewComboBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn5.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn5.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn5.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn5.Name = "dataGridViewComboBoxColumn5";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this._inputSignals7);
            this.tabPage7.Location = new System.Drawing.Point(4, 49);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(173, 475);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "ЛС7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // _inputSignals7
            // 
            this._inputSignals7.AllowUserToAddRows = false;
            this._inputSignals7.AllowUserToDeleteRows = false;
            this._inputSignals7.AllowUserToResizeColumns = false;
            this._inputSignals7.AllowUserToResizeRows = false;
            this._inputSignals7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals7.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewComboBoxColumn6});
            this._inputSignals7.Location = new System.Drawing.Point(3, 3);
            this._inputSignals7.MultiSelect = false;
            this._inputSignals7.Name = "_inputSignals7";
            this._inputSignals7.RowHeadersVisible = false;
            this._inputSignals7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals7.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this._inputSignals7.RowTemplate.Height = 20;
            this._inputSignals7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals7.ShowCellErrors = false;
            this._inputSignals7.ShowCellToolTips = false;
            this._inputSignals7.ShowEditingIcon = false;
            this._inputSignals7.ShowRowErrors = false;
            this._inputSignals7.Size = new System.Drawing.Size(167, 469);
            this._inputSignals7.TabIndex = 3;
            this._inputSignals7.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "№";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 24;
            // 
            // dataGridViewComboBoxColumn6
            // 
            this.dataGridViewComboBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn6.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn6.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn6.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn6.Name = "dataGridViewComboBoxColumn6";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this._inputSignals8);
            this.tabPage8.Location = new System.Drawing.Point(4, 49);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(173, 475);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "ЛС8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // _inputSignals8
            // 
            this._inputSignals8.AllowUserToAddRows = false;
            this._inputSignals8.AllowUserToDeleteRows = false;
            this._inputSignals8.AllowUserToResizeColumns = false;
            this._inputSignals8.AllowUserToResizeRows = false;
            this._inputSignals8.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._inputSignals8.BackgroundColor = System.Drawing.Color.White;
            this._inputSignals8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._inputSignals8.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewComboBoxColumn7});
            this._inputSignals8.Location = new System.Drawing.Point(3, 3);
            this._inputSignals8.MultiSelect = false;
            this._inputSignals8.Name = "_inputSignals8";
            this._inputSignals8.RowHeadersVisible = false;
            this._inputSignals8.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._inputSignals8.RowsDefaultCellStyle = dataGridViewCellStyle16;
            this._inputSignals8.RowTemplate.Height = 20;
            this._inputSignals8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._inputSignals8.ShowCellErrors = false;
            this._inputSignals8.ShowCellToolTips = false;
            this._inputSignals8.ShowEditingIcon = false;
            this._inputSignals8.ShowRowErrors = false;
            this._inputSignals8.Size = new System.Drawing.Size(167, 469);
            this._inputSignals8.TabIndex = 3;
            this._inputSignals8.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._inputSignals1_8CellEndEdit);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "№";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Width = 24;
            // 
            // dataGridViewComboBoxColumn7
            // 
            this.dataGridViewComboBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewComboBoxColumn7.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.dataGridViewComboBoxColumn7.HeaderText = "Значение";
            this.dataGridViewComboBoxColumn7.Items.AddRange(new object[] {
            "Нет",
            "Да",
            "Инверс"});
            this.dataGridViewComboBoxColumn7.Name = "dataGridViewComboBoxColumn7";
            // 
            // tabPage28
            // 
            this.tabPage28.Controls.Add(this.groupBox9);
            this.tabPage28.Location = new System.Drawing.Point(4, 22);
            this.tabPage28.Name = "tabPage28";
            this.tabPage28.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage28.Size = new System.Drawing.Size(1021, 553);
            this.tabPage28.TabIndex = 10;
            this.tabPage28.Text = "Параметры двигателя";
            this.tabPage28.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this._engineNpuskCombo);
            this.groupBox9.Controls.Add(this._engineQresetCombo);
            this.groupBox9.Controls.Add(this._engineHeatPuskConstraintBox);
            this.groupBox9.Controls.Add(this.label44);
            this.groupBox9.Controls.Add(this._enginePuskTimeBox);
            this.groupBox9.Controls.Add(this.label45);
            this.groupBox9.Controls.Add(this._engineIpConstraintBox);
            this.groupBox9.Controls.Add(this.label46);
            this.groupBox9.Controls.Add(this._engineInBox);
            this.groupBox9.Controls.Add(this.label47);
            this.groupBox9.Controls.Add(this._engineCoolingTimeBox);
            this.groupBox9.Controls.Add(this.label48);
            this.groupBox9.Controls.Add(this._engineHeatingTimeBox);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Location = new System.Drawing.Point(8, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(408, 138);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Защиты двигателя";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(210, 110);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(78, 13);
            this.label23.TabIndex = 15;
            this.label23.Text = "Вход N сброс ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 110);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 13);
            this.label24.TabIndex = 14;
            this.label24.Text = "Вход Q сброс";
            // 
            // _engineNpuskCombo
            // 
            this._engineNpuskCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineNpuskCombo.FormattingEnabled = true;
            this._engineNpuskCombo.Location = new System.Drawing.Point(297, 106);
            this._engineNpuskCombo.Name = "_engineNpuskCombo";
            this._engineNpuskCombo.Size = new System.Drawing.Size(101, 21);
            this._engineNpuskCombo.TabIndex = 13;
            // 
            // _engineQresetCombo
            // 
            this._engineQresetCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQresetCombo.FormattingEnabled = true;
            this._engineQresetCombo.Location = new System.Drawing.Point(111, 107);
            this._engineQresetCombo.Name = "_engineQresetCombo";
            this._engineQresetCombo.Size = new System.Drawing.Size(90, 21);
            this._engineQresetCombo.TabIndex = 12;
            // 
            // _engineHeatPuskConstraintBox
            // 
            this._engineHeatPuskConstraintBox.Location = new System.Drawing.Point(329, 80);
            this._engineHeatPuskConstraintBox.Name = "_engineHeatPuskConstraintBox";
            this._engineHeatPuskConstraintBox.Size = new System.Drawing.Size(69, 20);
            this._engineHeatPuskConstraintBox.TabIndex = 11;
            this._engineHeatPuskConstraintBox.Tag = "256";
            this._engineHeatPuskConstraintBox.Text = "0";
            this._engineHeatPuskConstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(206, 83);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(117, 13);
            this.label44.TabIndex = 10;
            this.label44.Text = "Гор.состяние Qгор, %";
            // 
            // _enginePuskTimeBox
            // 
            this._enginePuskTimeBox.Location = new System.Drawing.Point(329, 39);
            this._enginePuskTimeBox.Name = "_enginePuskTimeBox";
            this._enginePuskTimeBox.Size = new System.Drawing.Size(69, 20);
            this._enginePuskTimeBox.TabIndex = 9;
            this._enginePuskTimeBox.Tag = "3000000";
            this._enginePuskTimeBox.Text = "0";
            this._enginePuskTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(210, 42);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(66, 13);
            this.label45.TabIndex = 8;
            this.label45.Text = "T пуска, мс";
            // 
            // _engineIpConstraintBox
            // 
            this._engineIpConstraintBox.Location = new System.Drawing.Point(329, 19);
            this._engineIpConstraintBox.Name = "_engineIpConstraintBox";
            this._engineIpConstraintBox.Size = new System.Drawing.Size(69, 20);
            this._engineIpConstraintBox.TabIndex = 7;
            this._engineIpConstraintBox.Tag = "40";
            this._engineIpConstraintBox.Text = "0";
            this._engineIpConstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(210, 22);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 13);
            this.label46.TabIndex = 6;
            this.label46.Text = "Iпуск, In";
            // 
            // _engineInBox
            // 
            this._engineInBox.Location = new System.Drawing.Point(132, 59);
            this._engineInBox.Name = "_engineInBox";
            this._engineInBox.Size = new System.Drawing.Size(69, 20);
            this._engineInBox.TabIndex = 5;
            this._engineInBox.Tag = "40";
            this._engineInBox.Text = "0";
            this._engineInBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 62);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(114, 13);
            this.label47.TabIndex = 4;
            this.label47.Text = "Ток двигателя Iдв, In";
            // 
            // _engineCoolingTimeBox
            // 
            this._engineCoolingTimeBox.Location = new System.Drawing.Point(132, 39);
            this._engineCoolingTimeBox.Name = "_engineCoolingTimeBox";
            this._engineCoolingTimeBox.Size = new System.Drawing.Size(69, 20);
            this._engineCoolingTimeBox.TabIndex = 3;
            this._engineCoolingTimeBox.Tag = "65000";
            this._engineCoolingTimeBox.Text = "0";
            this._engineCoolingTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 42);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(90, 13);
            this.label48.TabIndex = 2;
            this.label48.Text = "Т охлаждения, с";
            // 
            // _engineHeatingTimeBox
            // 
            this._engineHeatingTimeBox.Location = new System.Drawing.Point(132, 19);
            this._engineHeatingTimeBox.Name = "_engineHeatingTimeBox";
            this._engineHeatingTimeBox.Size = new System.Drawing.Size(69, 20);
            this._engineHeatingTimeBox.TabIndex = 1;
            this._engineHeatingTimeBox.Tag = "65000";
            this._engineHeatingTimeBox.Text = "0";
            this._engineHeatingTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(12, 22);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(67, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "T нагрева,c";
            // 
            // _allDefensesPage
            // 
            this._allDefensesPage.Controls.Add(this.groupBox5);
            this._allDefensesPage.Controls.Add(this.groupBox10);
            this._allDefensesPage.Location = new System.Drawing.Point(4, 22);
            this._allDefensesPage.Name = "_allDefensesPage";
            this._allDefensesPage.Size = new System.Drawing.Size(1021, 553);
            this._allDefensesPage.TabIndex = 4;
            this._allDefensesPage.Text = "Защиты";
            this._allDefensesPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._groupChangeButton);
            this.groupBox5.Controls.Add(this._mainRadioButton);
            this.groupBox5.Controls.Add(this._reserveRadioButton);
            this.groupBox5.Location = new System.Drawing.Point(8, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(417, 35);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Группа уставок";
            // 
            // _groupChangeButton
            // 
            this._groupChangeButton.Location = new System.Drawing.Point(184, 8);
            this._groupChangeButton.Name = "_groupChangeButton";
            this._groupChangeButton.Size = new System.Drawing.Size(226, 23);
            this._groupChangeButton.TabIndex = 8;
            this._groupChangeButton.Text = "Основные -> Резервные";
            this._toolTip.SetToolTip(this._groupChangeButton, "Копировать уставки");
            this._groupChangeButton.UseVisualStyleBackColor = true;
            // 
            // _mainRadioButton
            // 
            this._mainRadioButton.AutoSize = true;
            this._mainRadioButton.Checked = true;
            this._mainRadioButton.Location = new System.Drawing.Point(17, 13);
            this._mainRadioButton.Name = "_mainRadioButton";
            this._mainRadioButton.Size = new System.Drawing.Size(75, 17);
            this._mainRadioButton.TabIndex = 6;
            this._mainRadioButton.TabStop = true;
            this._mainRadioButton.Text = "Основная";
            this._mainRadioButton.UseVisualStyleBackColor = true;
            // 
            // _reserveRadioButton
            // 
            this._reserveRadioButton.AutoSize = true;
            this._reserveRadioButton.Location = new System.Drawing.Point(98, 14);
            this._reserveRadioButton.Name = "_reserveRadioButton";
            this._reserveRadioButton.Size = new System.Drawing.Size(80, 17);
            this._reserveRadioButton.TabIndex = 7;
            this._reserveRadioButton.Text = "Резервная";
            this._reserveRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this._difensesTC);
            this.groupBox10.Location = new System.Drawing.Point(8, 40);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(965, 510);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Защиты";
            // 
            // _difensesTC
            // 
            this._difensesTC.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this._difensesTC.Controls.Add(this.tabPage17);
            this._difensesTC.Controls.Add(this.tabPage21);
            this._difensesTC.Controls.Add(this.tabPage22);
            this._difensesTC.Controls.Add(this.tabPage19);
            this._difensesTC.Controls.Add(this.tabPage20);
            this._difensesTC.Controls.Add(this.tabPage23);
            this._difensesTC.Controls.Add(this.tabPage24);
            this._difensesTC.Controls.Add(this.tabPage26);
            this._difensesTC.Controls.Add(this.tabPage27);
            this._difensesTC.Controls.Add(this.tabPage25);
            this._difensesTC.Controls.Add(this.tabPage18);
            this._difensesTC.Dock = System.Windows.Forms.DockStyle.Top;
            this._difensesTC.Location = new System.Drawing.Point(3, 16);
            this._difensesTC.Name = "_difensesTC";
            this._difensesTC.SelectedIndex = 0;
            this._difensesTC.Size = new System.Drawing.Size(959, 494);
            this._difensesTC.TabIndex = 0;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.groupBox16);
            this.tabPage17.Location = new System.Drawing.Point(4, 25);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(951, 465);
            this.tabPage17.TabIndex = 0;
            this.tabPage17.Text = "Углы МЧ";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label134);
            this.groupBox16.Controls.Add(this.label133);
            this.groupBox16.Controls.Add(this.label132);
            this.groupBox16.Controls.Add(this.label100);
            this.groupBox16.Controls.Add(this._i2Corner);
            this.groupBox16.Controls.Add(this._inCorner);
            this.groupBox16.Controls.Add(this._i0Corner);
            this.groupBox16.Controls.Add(this._iCorner);
            this.groupBox16.Location = new System.Drawing.Point(6, 6);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(194, 80);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Угол МЧ";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(11, 47);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(16, 13);
            this.label134.TabIndex = 9;
            this.label134.Text = "In";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(111, 47);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(16, 13);
            this.label133.TabIndex = 8;
            this.label133.Text = "I2";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(111, 21);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(16, 13);
            this.label132.TabIndex = 7;
            this.label132.Text = "I0";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(11, 21);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(10, 13);
            this.label100.TabIndex = 6;
            this.label100.Text = "I";
            // 
            // _i2Corner
            // 
            this._i2Corner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i2Corner.Location = new System.Drawing.Point(127, 45);
            this._i2Corner.Name = "_i2Corner";
            this._i2Corner.Size = new System.Drawing.Size(38, 20);
            this._i2Corner.TabIndex = 5;
            this._i2Corner.Tag = "360";
            this._i2Corner.Text = "0";
            this._i2Corner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _inCorner
            // 
            this._inCorner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._inCorner.Location = new System.Drawing.Point(39, 45);
            this._inCorner.Name = "_inCorner";
            this._inCorner.Size = new System.Drawing.Size(38, 20);
            this._inCorner.TabIndex = 4;
            this._inCorner.Tag = "360";
            this._inCorner.Text = "0";
            this._inCorner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _i0Corner
            // 
            this._i0Corner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._i0Corner.Location = new System.Drawing.Point(127, 19);
            this._i0Corner.Name = "_i0Corner";
            this._i0Corner.Size = new System.Drawing.Size(38, 20);
            this._i0Corner.TabIndex = 3;
            this._i0Corner.Tag = "360";
            this._i0Corner.Text = "0";
            this._i0Corner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _iCorner
            // 
            this._iCorner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._iCorner.Location = new System.Drawing.Point(39, 19);
            this._iCorner.Name = "_iCorner";
            this._iCorner.Size = new System.Drawing.Size(38, 20);
            this._iCorner.TabIndex = 2;
            this._iCorner.Tag = "360";
            this._iCorner.Text = "0";
            this._iCorner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.groupBox8);
            this.tabPage21.Controls.Add(this.groupBox7);
            this.tabPage21.Controls.Add(this.groupBox20);
            this.tabPage21.Location = new System.Drawing.Point(4, 25);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Size = new System.Drawing.Size(951, 465);
            this.tabPage21.TabIndex = 4;
            this.tabPage21.Text = "Защ. I";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this._difensesI8DataGrid);
            this.groupBox8.Location = new System.Drawing.Point(3, 331);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(945, 104);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Защита I< минимального тока";
            // 
            // _difensesI8DataGrid
            // 
            this._difensesI8DataGrid.AllowUserToAddRows = false;
            this._difensesI8DataGrid.AllowUserToDeleteRows = false;
            this._difensesI8DataGrid.AllowUserToResizeColumns = false;
            this._difensesI8DataGrid.AllowUserToResizeRows = false;
            this._difensesI8DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesI8DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI8DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI8DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._i8StageColumn,
            this._i8ModesColumn,
            this._i8IColumn,
            this.Column2,
            this._i8UStartColumn,
            this._i8UstartYNColumn,
            this._i8DirectColumn,
            this._i8UnDirectColumn,
            this._i8LogicColumn,
            this._i8CharColumn,
            this._i8TColumn,
            this._i8KColumn,
            this._i8TyColumn,
            this._i8TyYNColumn,
            this._i8BlockingColumn,
            this._i8I2I1Column,
            this._i8I2I1YNColumn,
            this._i8BlockingDirectColumn,
            this._i8OscModeColumn,
            this._i8UROVModeColumn,
            this._i8APVModeColumn,
            this._i8AVRModeColumn});
            this._difensesI8DataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesI8DataGrid.Name = "_difensesI8DataGrid";
            this._difensesI8DataGrid.RowHeadersVisible = false;
            this._difensesI8DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI8DataGrid.RowTemplate.Height = 24;
            this._difensesI8DataGrid.ShowCellErrors = false;
            this._difensesI8DataGrid.ShowRowErrors = false;
            this._difensesI8DataGrid.Size = new System.Drawing.Size(933, 77);
            this._difensesI8DataGrid.TabIndex = 3;
            // 
            // _i8StageColumn
            // 
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.White;
            this._i8StageColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this._i8StageColumn.Frozen = true;
            this._i8StageColumn.HeaderText = "Ступень";
            this._i8StageColumn.Name = "_i8StageColumn";
            this._i8StageColumn.ReadOnly = true;
            this._i8StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _i8ModesColumn
            // 
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8ModesColumn.DefaultCellStyle = dataGridViewCellStyle18;
            this._i8ModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._i8ModesColumn.HeaderText = "Режим";
            this._i8ModesColumn.Name = "_i8ModesColumn";
            this._i8ModesColumn.Width = 80;
            // 
            // _i8IColumn
            // 
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8IColumn.DefaultCellStyle = dataGridViewCellStyle19;
            this._i8IColumn.HeaderText = "Iср, Iн тт";
            this._i8IColumn.Name = "_i8IColumn";
            this._i8IColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8IColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8IColumn.Width = 65;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // _i8UStartColumn
            // 
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8UStartColumn.DefaultCellStyle = dataGridViewCellStyle20;
            this._i8UStartColumn.HeaderText = "Uпуск [В]";
            this._i8UStartColumn.MaxInputLength = 6;
            this._i8UStartColumn.Name = "_i8UStartColumn";
            this._i8UStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8UStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8UStartColumn.Visible = false;
            this._i8UStartColumn.Width = 80;
            // 
            // _i8UstartYNColumn
            // 
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.NullValue = false;
            this._i8UstartYNColumn.DefaultCellStyle = dataGridViewCellStyle21;
            this._i8UstartYNColumn.HeaderText = "Пуск по U";
            this._i8UstartYNColumn.Name = "_i8UstartYNColumn";
            this._i8UstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8UstartYNColumn.Visible = false;
            this._i8UstartYNColumn.Width = 90;
            // 
            // _i8DirectColumn
            // 
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8DirectColumn.DefaultCellStyle = dataGridViewCellStyle22;
            this._i8DirectColumn.HeaderText = "Направление";
            this._i8DirectColumn.Name = "_i8DirectColumn";
            this._i8DirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8DirectColumn.Visible = false;
            this._i8DirectColumn.Width = 85;
            // 
            // _i8UnDirectColumn
            // 
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8UnDirectColumn.DefaultCellStyle = dataGridViewCellStyle23;
            this._i8UnDirectColumn.HeaderText = "Недост.напр";
            this._i8UnDirectColumn.Name = "_i8UnDirectColumn";
            this._i8UnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8UnDirectColumn.Visible = false;
            this._i8UnDirectColumn.Width = 80;
            // 
            // _i8LogicColumn
            // 
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8LogicColumn.DefaultCellStyle = dataGridViewCellStyle24;
            this._i8LogicColumn.HeaderText = "Логика";
            this._i8LogicColumn.Name = "_i8LogicColumn";
            this._i8LogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8LogicColumn.Width = 90;
            // 
            // _i8CharColumn
            // 
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8CharColumn.DefaultCellStyle = dataGridViewCellStyle25;
            this._i8CharColumn.HeaderText = "Характ-ка";
            this._i8CharColumn.Name = "_i8CharColumn";
            this._i8CharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8CharColumn.Visible = false;
            // 
            // _i8TColumn
            // 
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8TColumn.DefaultCellStyle = dataGridViewCellStyle26;
            this._i8TColumn.HeaderText = "t [мс]";
            this._i8TColumn.Name = "_i8TColumn";
            this._i8TColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8TColumn.Width = 65;
            // 
            // _i8KColumn
            // 
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8KColumn.DefaultCellStyle = dataGridViewCellStyle27;
            this._i8KColumn.HeaderText = "k завис. хар-ки";
            this._i8KColumn.Name = "_i8KColumn";
            this._i8KColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8KColumn.Visible = false;
            this._i8KColumn.Width = 110;
            // 
            // _i8TyColumn
            // 
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8TyColumn.DefaultCellStyle = dataGridViewCellStyle28;
            this._i8TyColumn.HeaderText = "Ty [мс]";
            this._i8TyColumn.Name = "_i8TyColumn";
            this._i8TyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8TyColumn.Visible = false;
            this._i8TyColumn.Width = 65;
            // 
            // _i8TyYNColumn
            // 
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle29.NullValue = false;
            this._i8TyYNColumn.DefaultCellStyle = dataGridViewCellStyle29;
            this._i8TyYNColumn.HeaderText = "Пуск по Ty";
            this._i8TyYNColumn.Name = "_i8TyYNColumn";
            this._i8TyYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i8TyYNColumn.Visible = false;
            this._i8TyYNColumn.Width = 90;
            // 
            // _i8BlockingColumn
            // 
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8BlockingColumn.DefaultCellStyle = dataGridViewCellStyle30;
            this._i8BlockingColumn.HeaderText = "Блокировка";
            this._i8BlockingColumn.Name = "_i8BlockingColumn";
            this._i8BlockingColumn.Width = 90;
            // 
            // _i8I2I1Column
            // 
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8I2I1Column.DefaultCellStyle = dataGridViewCellStyle31;
            this._i8I2I1Column.HeaderText = "I2г/I1г [%]";
            this._i8I2I1Column.Name = "_i8I2I1Column";
            this._i8I2I1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i8I2I1Column.Width = 65;
            // 
            // _i8I2I1YNColumn
            // 
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8I2I1YNColumn.DefaultCellStyle = dataGridViewCellStyle32;
            this._i8I2I1YNColumn.HeaderText = "Блок. по I2г/I1г";
            this._i8I2I1YNColumn.Name = "_i8I2I1YNColumn";
            this._i8I2I1YNColumn.Width = 93;
            // 
            // _i8BlockingDirectColumn
            // 
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8BlockingDirectColumn.DefaultCellStyle = dataGridViewCellStyle33;
            this._i8BlockingDirectColumn.HeaderText = "Перекр. Блок.";
            this._i8BlockingDirectColumn.Name = "_i8BlockingDirectColumn";
            this._i8BlockingDirectColumn.Width = 90;
            // 
            // _i8OscModeColumn
            // 
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i8OscModeColumn.DefaultCellStyle = dataGridViewCellStyle34;
            this._i8OscModeColumn.HeaderText = "Осциллограф";
            this._i8OscModeColumn.Name = "_i8OscModeColumn";
            this._i8OscModeColumn.Width = 90;
            // 
            // _i8UROVModeColumn
            // 
            this._i8UROVModeColumn.HeaderText = "УРОВ";
            this._i8UROVModeColumn.Name = "_i8UROVModeColumn";
            this._i8UROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i8APVModeColumn
            // 
            this._i8APVModeColumn.HeaderText = "АПВ";
            this._i8APVModeColumn.Name = "_i8APVModeColumn";
            this._i8APVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i8AVRModeColumn
            // 
            this._i8AVRModeColumn.HeaderText = "АВР";
            this._i8AVRModeColumn.Name = "_i8AVRModeColumn";
            this._i8AVRModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this._difensesI67DataGrid);
            this.groupBox7.Location = new System.Drawing.Point(3, 199);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(945, 126);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Защиты I от затянутого пуска и блокировки ротора";
            // 
            // _difensesI67DataGrid
            // 
            this._difensesI67DataGrid.AllowUserToAddRows = false;
            this._difensesI67DataGrid.AllowUserToDeleteRows = false;
            this._difensesI67DataGrid.AllowUserToResizeColumns = false;
            this._difensesI67DataGrid.AllowUserToResizeRows = false;
            this._difensesI67DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesI67DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI67DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI67DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._i67StageColumn,
            this._i67ModesColumn,
            this._i67IColumn,
            this._i67IModeColumn,
            this._i67UStartColumn,
            this._i67UstartYNColumn,
            this._i67DirectColumn,
            this._i67UnDirectColumn,
            this._i67LogicColumn,
            this._i67CharColumn,
            this._i67TColumn,
            this._i67KColumn,
            this._i67TyColumn,
            this._i67TyYNColumn,
            this._i67BlockingColumn,
            this._i67I2I1Column,
            this._i67I2I1YNColumn,
            this._i67BlockingDirectColumn,
            this._i67OscModeColumn,
            this._i67UROVModeColumn,
            this._i67APVModeColumn,
            this._i67AVRModeColumn});
            this._difensesI67DataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesI67DataGrid.Name = "_difensesI67DataGrid";
            this._difensesI67DataGrid.RowHeadersVisible = false;
            this._difensesI67DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI67DataGrid.RowTemplate.Height = 24;
            this._difensesI67DataGrid.ShowCellErrors = false;
            this._difensesI67DataGrid.ShowRowErrors = false;
            this._difensesI67DataGrid.Size = new System.Drawing.Size(933, 99);
            this._difensesI67DataGrid.TabIndex = 3;
            // 
            // _i67StageColumn
            // 
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.White;
            this._i67StageColumn.DefaultCellStyle = dataGridViewCellStyle35;
            this._i67StageColumn.Frozen = true;
            this._i67StageColumn.HeaderText = "Ступень";
            this._i67StageColumn.Name = "_i67StageColumn";
            this._i67StageColumn.ReadOnly = true;
            this._i67StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _i67ModesColumn
            // 
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67ModesColumn.DefaultCellStyle = dataGridViewCellStyle36;
            this._i67ModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._i67ModesColumn.HeaderText = "Режим";
            this._i67ModesColumn.Name = "_i67ModesColumn";
            this._i67ModesColumn.Width = 80;
            // 
            // _i67IColumn
            // 
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67IColumn.DefaultCellStyle = dataGridViewCellStyle37;
            this._i67IColumn.HeaderText = "Iср, Iн тт";
            this._i67IColumn.Name = "_i67IColumn";
            this._i67IColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67IColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67IColumn.Width = 65;
            // 
            // _i67IModeColumn
            // 
            this._i67IModeColumn.HeaderText = "Режим Ip";
            this._i67IModeColumn.Name = "_i67IModeColumn";
            // 
            // _i67UStartColumn
            // 
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67UStartColumn.DefaultCellStyle = dataGridViewCellStyle38;
            this._i67UStartColumn.HeaderText = "Uпуск [В]";
            this._i67UStartColumn.MaxInputLength = 6;
            this._i67UStartColumn.Name = "_i67UStartColumn";
            this._i67UStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67UStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67UStartColumn.Width = 80;
            // 
            // _i67UstartYNColumn
            // 
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle39.NullValue = false;
            this._i67UstartYNColumn.DefaultCellStyle = dataGridViewCellStyle39;
            this._i67UstartYNColumn.HeaderText = "Пуск по U";
            this._i67UstartYNColumn.Name = "_i67UstartYNColumn";
            this._i67UstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67UstartYNColumn.Width = 90;
            // 
            // _i67DirectColumn
            // 
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67DirectColumn.DefaultCellStyle = dataGridViewCellStyle40;
            this._i67DirectColumn.HeaderText = "Направление";
            this._i67DirectColumn.Name = "_i67DirectColumn";
            this._i67DirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67DirectColumn.Width = 85;
            // 
            // _i67UnDirectColumn
            // 
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67UnDirectColumn.DefaultCellStyle = dataGridViewCellStyle41;
            this._i67UnDirectColumn.HeaderText = "Недост.напр";
            this._i67UnDirectColumn.Name = "_i67UnDirectColumn";
            this._i67UnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67UnDirectColumn.Width = 80;
            // 
            // _i67LogicColumn
            // 
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67LogicColumn.DefaultCellStyle = dataGridViewCellStyle42;
            this._i67LogicColumn.HeaderText = "Логика";
            this._i67LogicColumn.Name = "_i67LogicColumn";
            this._i67LogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67LogicColumn.Width = 90;
            // 
            // _i67CharColumn
            // 
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67CharColumn.DefaultCellStyle = dataGridViewCellStyle43;
            this._i67CharColumn.HeaderText = "Характ-ка";
            this._i67CharColumn.Name = "_i67CharColumn";
            this._i67CharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i67TColumn
            // 
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67TColumn.DefaultCellStyle = dataGridViewCellStyle44;
            this._i67TColumn.HeaderText = "t [мс]";
            this._i67TColumn.Name = "_i67TColumn";
            this._i67TColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67TColumn.Width = 65;
            // 
            // _i67KColumn
            // 
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67KColumn.DefaultCellStyle = dataGridViewCellStyle45;
            this._i67KColumn.HeaderText = "k завис. хар-ки";
            this._i67KColumn.Name = "_i67KColumn";
            this._i67KColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67KColumn.Width = 110;
            // 
            // _i67TyColumn
            // 
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67TyColumn.DefaultCellStyle = dataGridViewCellStyle46;
            this._i67TyColumn.HeaderText = "Ty [мс]";
            this._i67TyColumn.Name = "_i67TyColumn";
            this._i67TyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67TyColumn.Width = 65;
            // 
            // _i67TyYNColumn
            // 
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle47.NullValue = false;
            this._i67TyYNColumn.DefaultCellStyle = dataGridViewCellStyle47;
            this._i67TyYNColumn.HeaderText = "Пуск по Ty";
            this._i67TyYNColumn.Name = "_i67TyYNColumn";
            this._i67TyYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i67TyYNColumn.Width = 90;
            // 
            // _i67BlockingColumn
            // 
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67BlockingColumn.DefaultCellStyle = dataGridViewCellStyle48;
            this._i67BlockingColumn.HeaderText = "Блокировка";
            this._i67BlockingColumn.Name = "_i67BlockingColumn";
            this._i67BlockingColumn.Width = 90;
            // 
            // _i67I2I1Column
            // 
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67I2I1Column.DefaultCellStyle = dataGridViewCellStyle49;
            this._i67I2I1Column.HeaderText = "I2г/I1г [%]";
            this._i67I2I1Column.Name = "_i67I2I1Column";
            this._i67I2I1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i67I2I1Column.Width = 65;
            // 
            // _i67I2I1YNColumn
            // 
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67I2I1YNColumn.DefaultCellStyle = dataGridViewCellStyle50;
            this._i67I2I1YNColumn.HeaderText = "Блок. по I2г/I1г";
            this._i67I2I1YNColumn.Name = "_i67I2I1YNColumn";
            this._i67I2I1YNColumn.Width = 93;
            // 
            // _i67BlockingDirectColumn
            // 
            dataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67BlockingDirectColumn.DefaultCellStyle = dataGridViewCellStyle51;
            this._i67BlockingDirectColumn.HeaderText = "Перекр. Блок.";
            this._i67BlockingDirectColumn.Name = "_i67BlockingDirectColumn";
            this._i67BlockingDirectColumn.Width = 90;
            // 
            // _i67OscModeColumn
            // 
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i67OscModeColumn.DefaultCellStyle = dataGridViewCellStyle52;
            this._i67OscModeColumn.HeaderText = "Осциллограф";
            this._i67OscModeColumn.Name = "_i67OscModeColumn";
            this._i67OscModeColumn.Width = 90;
            // 
            // _i67UROVModeColumn
            // 
            this._i67UROVModeColumn.HeaderText = "УРОВ";
            this._i67UROVModeColumn.Name = "_i67UROVModeColumn";
            this._i67UROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i67APVModeColumn
            // 
            this._i67APVModeColumn.HeaderText = "АПВ";
            this._i67APVModeColumn.Name = "_i67APVModeColumn";
            this._i67APVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i67AVRModeColumn
            // 
            this._i67AVRModeColumn.HeaderText = "АВР";
            this._i67AVRModeColumn.Name = "_i67AVRModeColumn";
            this._i67AVRModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // groupBox20
            // 
            this.groupBox20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox20.Controls.Add(this._difensesIDataGrid);
            this.groupBox20.Location = new System.Drawing.Point(3, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(945, 190);
            this.groupBox20.TabIndex = 4;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Защиты I> максимального тока";
            // 
            // _difensesIDataGrid
            // 
            this._difensesIDataGrid.AllowUserToAddRows = false;
            this._difensesIDataGrid.AllowUserToDeleteRows = false;
            this._difensesIDataGrid.AllowUserToResizeColumns = false;
            this._difensesIDataGrid.AllowUserToResizeRows = false;
            this._difensesIDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesIDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesIDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesIDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._iStageColumn,
            this._iModesColumn,
            this._iIColumn,
            this.Column1,
            this._iUStartColumn,
            this._iUstartYNColumn,
            this._iDirectColumn,
            this._iUnDirectColumn,
            this._iLogicColumn,
            this._iCharColumn,
            this._iTColumn,
            this._iKColumn,
            this._iTyColumn,
            this._iTyYNColumn,
            this._iBlockingColumn,
            this._iI2I1Column,
            this._iI2I1YNColumn,
            this._iBlockingDirectColumn,
            this._iOscModeColumn,
            this._iUROVModeColumn,
            this._iAPVModeColumn,
            this._iAVRModeColumn});
            this._difensesIDataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesIDataGrid.Name = "_difensesIDataGrid";
            this._difensesIDataGrid.RowHeadersVisible = false;
            this._difensesIDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesIDataGrid.RowTemplate.Height = 24;
            this._difensesIDataGrid.ShowCellErrors = false;
            this._difensesIDataGrid.ShowRowErrors = false;
            this._difensesIDataGrid.Size = new System.Drawing.Size(933, 162);
            this._difensesIDataGrid.TabIndex = 3;
            // 
            // _iStageColumn
            // 
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle53.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.Color.White;
            this._iStageColumn.DefaultCellStyle = dataGridViewCellStyle53;
            this._iStageColumn.Frozen = true;
            this._iStageColumn.HeaderText = "Ступень";
            this._iStageColumn.Name = "_iStageColumn";
            this._iStageColumn.ReadOnly = true;
            this._iStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _iModesColumn
            // 
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iModesColumn.DefaultCellStyle = dataGridViewCellStyle54;
            this._iModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._iModesColumn.HeaderText = "Режим";
            this._iModesColumn.Name = "_iModesColumn";
            this._iModesColumn.Width = 80;
            // 
            // _iIColumn
            // 
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iIColumn.DefaultCellStyle = dataGridViewCellStyle55;
            this._iIColumn.HeaderText = "Iср, Iн тт";
            this._iIColumn.Name = "_iIColumn";
            this._iIColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iIColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iIColumn.Width = 65;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // _iUStartColumn
            // 
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUStartColumn.DefaultCellStyle = dataGridViewCellStyle56;
            this._iUStartColumn.HeaderText = "Uпуск [В]";
            this._iUStartColumn.MaxInputLength = 6;
            this._iUStartColumn.Name = "_iUStartColumn";
            this._iUStartColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iUStartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iUStartColumn.Width = 80;
            // 
            // _iUstartYNColumn
            // 
            dataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle57.NullValue = false;
            this._iUstartYNColumn.DefaultCellStyle = dataGridViewCellStyle57;
            this._iUstartYNColumn.HeaderText = "Пуск по U";
            this._iUstartYNColumn.Name = "_iUstartYNColumn";
            this._iUstartYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iUstartYNColumn.Width = 90;
            // 
            // _iDirectColumn
            // 
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iDirectColumn.DefaultCellStyle = dataGridViewCellStyle58;
            this._iDirectColumn.HeaderText = "Направление";
            this._iDirectColumn.Name = "_iDirectColumn";
            this._iDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iDirectColumn.Width = 85;
            // 
            // _iUnDirectColumn
            // 
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iUnDirectColumn.DefaultCellStyle = dataGridViewCellStyle59;
            this._iUnDirectColumn.HeaderText = "Недост.напр";
            this._iUnDirectColumn.Name = "_iUnDirectColumn";
            this._iUnDirectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iUnDirectColumn.Width = 80;
            // 
            // _iLogicColumn
            // 
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iLogicColumn.DefaultCellStyle = dataGridViewCellStyle60;
            this._iLogicColumn.HeaderText = "Логика";
            this._iLogicColumn.Name = "_iLogicColumn";
            this._iLogicColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iLogicColumn.Width = 90;
            // 
            // _iCharColumn
            // 
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iCharColumn.DefaultCellStyle = dataGridViewCellStyle61;
            this._iCharColumn.HeaderText = "Характ-ка";
            this._iCharColumn.Name = "_iCharColumn";
            this._iCharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _iTColumn
            // 
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTColumn.DefaultCellStyle = dataGridViewCellStyle62;
            this._iTColumn.HeaderText = "t [мс]";
            this._iTColumn.Name = "_iTColumn";
            this._iTColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTColumn.Width = 65;
            // 
            // _iKColumn
            // 
            dataGridViewCellStyle63.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iKColumn.DefaultCellStyle = dataGridViewCellStyle63;
            this._iKColumn.HeaderText = "k завис. хар-ки";
            this._iKColumn.Name = "_iKColumn";
            this._iKColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iKColumn.Width = 110;
            // 
            // _iTyColumn
            // 
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iTyColumn.DefaultCellStyle = dataGridViewCellStyle64;
            this._iTyColumn.HeaderText = "Ty [мс]";
            this._iTyColumn.Name = "_iTyColumn";
            this._iTyColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iTyColumn.Width = 65;
            // 
            // _iTyYNColumn
            // 
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle65.NullValue = false;
            this._iTyYNColumn.DefaultCellStyle = dataGridViewCellStyle65;
            this._iTyYNColumn.HeaderText = "Пуск по Ty";
            this._iTyYNColumn.Name = "_iTyYNColumn";
            this._iTyYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._iTyYNColumn.Width = 90;
            // 
            // _iBlockingColumn
            // 
            dataGridViewCellStyle66.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iBlockingColumn.DefaultCellStyle = dataGridViewCellStyle66;
            this._iBlockingColumn.HeaderText = "Блокировка";
            this._iBlockingColumn.Name = "_iBlockingColumn";
            this._iBlockingColumn.Width = 90;
            // 
            // _iI2I1Column
            // 
            dataGridViewCellStyle67.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iI2I1Column.DefaultCellStyle = dataGridViewCellStyle67;
            this._iI2I1Column.HeaderText = "I2г/I1г [%]";
            this._iI2I1Column.Name = "_iI2I1Column";
            this._iI2I1Column.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._iI2I1Column.Width = 65;
            // 
            // _iI2I1YNColumn
            // 
            dataGridViewCellStyle68.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iI2I1YNColumn.DefaultCellStyle = dataGridViewCellStyle68;
            this._iI2I1YNColumn.HeaderText = "Блок. по I2г/I1г";
            this._iI2I1YNColumn.Name = "_iI2I1YNColumn";
            this._iI2I1YNColumn.Width = 93;
            // 
            // _iBlockingDirectColumn
            // 
            dataGridViewCellStyle69.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iBlockingDirectColumn.DefaultCellStyle = dataGridViewCellStyle69;
            this._iBlockingDirectColumn.HeaderText = "Перекр. Блок.";
            this._iBlockingDirectColumn.Name = "_iBlockingDirectColumn";
            this._iBlockingDirectColumn.Width = 90;
            // 
            // _iOscModeColumn
            // 
            dataGridViewCellStyle70.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._iOscModeColumn.DefaultCellStyle = dataGridViewCellStyle70;
            this._iOscModeColumn.HeaderText = "Осциллограф";
            this._iOscModeColumn.Name = "_iOscModeColumn";
            this._iOscModeColumn.Width = 90;
            // 
            // _iUROVModeColumn
            // 
            this._iUROVModeColumn.HeaderText = "УРОВ";
            this._iUROVModeColumn.Name = "_iUROVModeColumn";
            this._iUROVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _iAPVModeColumn
            // 
            this._iAPVModeColumn.HeaderText = "АПВ";
            this._iAPVModeColumn.Name = "_iAPVModeColumn";
            this._iAPVModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _iAVRModeColumn
            // 
            this._iAVRModeColumn.HeaderText = "АВР";
            this._iAVRModeColumn.Name = "_iAVRModeColumn";
            this._iAVRModeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.groupBox21);
            this.tabPage22.Location = new System.Drawing.Point(4, 25);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Size = new System.Drawing.Size(951, 465);
            this.tabPage22.TabIndex = 5;
            this.tabPage22.Text = "Защ. I*";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox21.Controls.Add(this._difensesI0DataGrid);
            this.groupBox21.Location = new System.Drawing.Point(3, 3);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(945, 211);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Защиты I*";
            // 
            // _difensesI0DataGrid
            // 
            this._difensesI0DataGrid.AllowUserToAddRows = false;
            this._difensesI0DataGrid.AllowUserToDeleteRows = false;
            this._difensesI0DataGrid.AllowUserToResizeColumns = false;
            this._difensesI0DataGrid.AllowUserToResizeRows = false;
            this._difensesI0DataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesI0DataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesI0DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesI0DataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._i0StageColumn,
            this._i0ModesColumn,
            this._i0IColumn,
            this._i0UstartColumn,
            this._i0UsYNColumn,
            this._i0DirColumn,
            this._i0UndirColumn,
            this._i0I0Column,
            this._i0CharColumn,
            this._i0TColumn,
            this._i0kColumn,
            this._i0BlockingColumn,
            this._i0OscColumn,
            this._i0TyColumn,
            this._i0TyYNColumn,
            this._i0UROVColumn,
            this._i0APVColumn,
            this._i0AVRColumn});
            this._difensesI0DataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesI0DataGrid.Name = "_difensesI0DataGrid";
            this._difensesI0DataGrid.RowHeadersVisible = false;
            this._difensesI0DataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesI0DataGrid.RowTemplate.Height = 24;
            this._difensesI0DataGrid.ShowCellErrors = false;
            this._difensesI0DataGrid.ShowRowErrors = false;
            this._difensesI0DataGrid.Size = new System.Drawing.Size(933, 184);
            this._difensesI0DataGrid.TabIndex = 3;
            // 
            // _i0StageColumn
            // 
            dataGridViewCellStyle71.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle71.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle71.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle71.SelectionForeColor = System.Drawing.Color.White;
            this._i0StageColumn.DefaultCellStyle = dataGridViewCellStyle71;
            this._i0StageColumn.Frozen = true;
            this._i0StageColumn.HeaderText = "Ступень";
            this._i0StageColumn.Name = "_i0StageColumn";
            this._i0StageColumn.ReadOnly = true;
            this._i0StageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _i0ModesColumn
            // 
            dataGridViewCellStyle72.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0ModesColumn.DefaultCellStyle = dataGridViewCellStyle72;
            this._i0ModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._i0ModesColumn.HeaderText = "Состояние";
            this._i0ModesColumn.Name = "_i0ModesColumn";
            this._i0ModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0ModesColumn.Width = 80;
            // 
            // _i0IColumn
            // 
            dataGridViewCellStyle73.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0IColumn.DefaultCellStyle = dataGridViewCellStyle73;
            this._i0IColumn.HeaderText = "I, Iн тт";
            this._i0IColumn.Name = "_i0IColumn";
            this._i0IColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0IColumn.Width = 65;
            // 
            // _i0UstartColumn
            // 
            dataGridViewCellStyle74.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0UstartColumn.DefaultCellStyle = dataGridViewCellStyle74;
            this._i0UstartColumn.HeaderText = "Uпуск [В]";
            this._i0UstartColumn.Name = "_i0UstartColumn";
            this._i0UstartColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0UstartColumn.Width = 80;
            // 
            // _i0UsYNColumn
            // 
            dataGridViewCellStyle75.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle75.NullValue = false;
            this._i0UsYNColumn.DefaultCellStyle = dataGridViewCellStyle75;
            this._i0UsYNColumn.HeaderText = "Пуск по U";
            this._i0UsYNColumn.Name = "_i0UsYNColumn";
            this._i0UsYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0UsYNColumn.Width = 90;
            // 
            // _i0DirColumn
            // 
            dataGridViewCellStyle76.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0DirColumn.DefaultCellStyle = dataGridViewCellStyle76;
            this._i0DirColumn.HeaderText = "Направление";
            this._i0DirColumn.Name = "_i0DirColumn";
            this._i0DirColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0DirColumn.Width = 85;
            // 
            // _i0UndirColumn
            // 
            dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0UndirColumn.DefaultCellStyle = dataGridViewCellStyle77;
            this._i0UndirColumn.HeaderText = "Недост.напр.";
            this._i0UndirColumn.Name = "_i0UndirColumn";
            this._i0UndirColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0UndirColumn.Width = 80;
            // 
            // _i0I0Column
            // 
            dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0I0Column.DefaultCellStyle = dataGridViewCellStyle78;
            this._i0I0Column.HeaderText = "I*";
            this._i0I0Column.Name = "_i0I0Column";
            this._i0I0Column.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0I0Column.Width = 90;
            // 
            // _i0CharColumn
            // 
            dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0CharColumn.DefaultCellStyle = dataGridViewCellStyle79;
            this._i0CharColumn.HeaderText = "Характ-ка";
            this._i0CharColumn.Name = "_i0CharColumn";
            this._i0CharColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0CharColumn.Width = 90;
            // 
            // _i0TColumn
            // 
            dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0TColumn.DefaultCellStyle = dataGridViewCellStyle80;
            this._i0TColumn.HeaderText = "t [мс]";
            this._i0TColumn.Name = "_i0TColumn";
            this._i0TColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0TColumn.Width = 65;
            // 
            // _i0kColumn
            // 
            dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0kColumn.DefaultCellStyle = dataGridViewCellStyle81;
            this._i0kColumn.HeaderText = "k завис. хар-ки";
            this._i0kColumn.Name = "_i0kColumn";
            this._i0kColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._i0kColumn.Width = 110;
            // 
            // _i0BlockingColumn
            // 
            this._i0BlockingColumn.HeaderText = "Блокировка";
            this._i0BlockingColumn.Name = "_i0BlockingColumn";
            this._i0BlockingColumn.Width = 90;
            // 
            // _i0OscColumn
            // 
            dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0OscColumn.DefaultCellStyle = dataGridViewCellStyle82;
            this._i0OscColumn.HeaderText = "Осциллограф";
            this._i0OscColumn.Name = "_i0OscColumn";
            this._i0OscColumn.Width = 80;
            // 
            // _i0TyColumn
            // 
            dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._i0TyColumn.DefaultCellStyle = dataGridViewCellStyle83;
            this._i0TyColumn.HeaderText = "ty [мс]";
            this._i0TyColumn.Name = "_i0TyColumn";
            this._i0TyColumn.Width = 65;
            // 
            // _i0TyYNColumn
            // 
            dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle84.NullValue = false;
            this._i0TyYNColumn.DefaultCellStyle = dataGridViewCellStyle84;
            this._i0TyYNColumn.HeaderText = "Пуск по Ту";
            this._i0TyYNColumn.Name = "_i0TyYNColumn";
            this._i0TyYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._i0TyYNColumn.Width = 90;
            // 
            // _i0UROVColumn
            // 
            this._i0UROVColumn.HeaderText = "УРОВ";
            this._i0UROVColumn.Name = "_i0UROVColumn";
            this._i0UROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i0APVColumn
            // 
            this._i0APVColumn.HeaderText = "АПВ";
            this._i0APVColumn.Name = "_i0APVColumn";
            this._i0APVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _i0AVRColumn
            // 
            this._i0AVRColumn.HeaderText = "АВР";
            this._i0AVRColumn.Name = "_i0AVRColumn";
            this._i0AVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.groupBox6);
            this.tabPage19.Location = new System.Drawing.Point(4, 25);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Size = new System.Drawing.Size(951, 465);
            this.tabPage19.TabIndex = 2;
            this.tabPage19.Text = "I2I1";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.I2I1AVRCheck);
            this.groupBox6.Controls.Add(this.I2I1APVCheck);
            this.groupBox6.Controls.Add(this.I2I1UROVCheck);
            this.groupBox6.Controls.Add(this.I2I1OSCCombo);
            this.groupBox6.Controls.Add(this.I2I1tcp);
            this.groupBox6.Controls.Add(this.I2I1BlockingCombo);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.I2I1TB);
            this.groupBox6.Controls.Add(this.I2I1ModeCombo);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(319, 182);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            // 
            // I2I1AVRCheck
            // 
            this.I2I1AVRCheck.AutoSize = true;
            this.I2I1AVRCheck.Location = new System.Drawing.Point(125, 157);
            this.I2I1AVRCheck.Name = "I2I1AVRCheck";
            this.I2I1AVRCheck.Size = new System.Drawing.Size(15, 14);
            this.I2I1AVRCheck.TabIndex = 40;
            this.I2I1AVRCheck.UseVisualStyleBackColor = true;
            // 
            // I2I1APVCheck
            // 
            this.I2I1APVCheck.AutoSize = true;
            this.I2I1APVCheck.Location = new System.Drawing.Point(125, 137);
            this.I2I1APVCheck.Name = "I2I1APVCheck";
            this.I2I1APVCheck.Size = new System.Drawing.Size(15, 14);
            this.I2I1APVCheck.TabIndex = 39;
            this.I2I1APVCheck.UseVisualStyleBackColor = true;
            // 
            // I2I1UROVCheck
            // 
            this.I2I1UROVCheck.AutoSize = true;
            this.I2I1UROVCheck.Location = new System.Drawing.Point(125, 117);
            this.I2I1UROVCheck.Name = "I2I1UROVCheck";
            this.I2I1UROVCheck.Size = new System.Drawing.Size(15, 14);
            this.I2I1UROVCheck.TabIndex = 38;
            this.I2I1UROVCheck.UseVisualStyleBackColor = true;
            // 
            // I2I1OSCCombo
            // 
            this.I2I1OSCCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1OSCCombo.FormattingEnabled = true;
            this.I2I1OSCCombo.Location = new System.Drawing.Point(125, 94);
            this.I2I1OSCCombo.Name = "I2I1OSCCombo";
            this.I2I1OSCCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1OSCCombo.TabIndex = 34;
            // 
            // I2I1tcp
            // 
            this.I2I1tcp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1tcp.Location = new System.Drawing.Point(125, 74);
            this.I2I1tcp.Name = "I2I1tcp";
            this.I2I1tcp.Size = new System.Drawing.Size(119, 20);
            this.I2I1tcp.TabIndex = 33;
            this.I2I1tcp.Tag = "1500";
            this.I2I1tcp.Text = "20";
            this.I2I1tcp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // I2I1BlockingCombo
            // 
            this.I2I1BlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1BlockingCombo.FormattingEnabled = true;
            this.I2I1BlockingCombo.Location = new System.Drawing.Point(125, 33);
            this.I2I1BlockingCombo.Name = "I2I1BlockingCombo";
            this.I2I1BlockingCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1BlockingCombo.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 157);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 13);
            this.label28.TabIndex = 31;
            this.label28.Text = "АВР";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 137);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 13);
            this.label29.TabIndex = 30;
            this.label29.Text = "АПВ";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 117);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 13);
            this.label22.TabIndex = 29;
            this.label22.Text = "УРОВ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 97);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 28;
            this.label26.Text = "ОСЦ.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 76);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "tcp, мс";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 56);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 26;
            this.label21.Text = "I2/I1, %";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 36);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Блокировка";
            // 
            // I2I1TB
            // 
            this.I2I1TB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.I2I1TB.Location = new System.Drawing.Point(125, 54);
            this.I2I1TB.Name = "I2I1TB";
            this.I2I1TB.Size = new System.Drawing.Size(119, 20);
            this.I2I1TB.TabIndex = 24;
            this.I2I1TB.Tag = "1500";
            this.I2I1TB.Text = "0";
            this.I2I1TB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // I2I1ModeCombo
            // 
            this.I2I1ModeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.I2I1ModeCombo.FormattingEnabled = true;
            this.I2I1ModeCombo.Location = new System.Drawing.Point(125, 13);
            this.I2I1ModeCombo.Name = "I2I1ModeCombo";
            this.I2I1ModeCombo.Size = new System.Drawing.Size(119, 21);
            this.I2I1ModeCombo.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Режим";
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.groupBox19);
            this.tabPage20.Location = new System.Drawing.Point(4, 25);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Size = new System.Drawing.Size(951, 465);
            this.tabPage20.TabIndex = 3;
            this.tabPage20.Text = "Iг";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.IrAVRCheck);
            this.groupBox19.Controls.Add(this.IrAPVCheck);
            this.groupBox19.Controls.Add(this.IrUROVCheck);
            this.groupBox19.Controls.Add(this.IrtyCheck);
            this.groupBox19.Controls.Add(this.IrUpuskCheck);
            this.groupBox19.Controls.Add(this.label39);
            this.groupBox19.Controls.Add(this.IrtyTB);
            this.groupBox19.Controls.Add(this.IrtcpTB);
            this.groupBox19.Controls.Add(this.label38);
            this.groupBox19.Controls.Add(this.IrOSCCombo);
            this.groupBox19.Controls.Add(this.IrIcpTB);
            this.groupBox19.Controls.Add(this.IrBlockingCombo);
            this.groupBox19.Controls.Add(this.label30);
            this.groupBox19.Controls.Add(this.label31);
            this.groupBox19.Controls.Add(this.label32);
            this.groupBox19.Controls.Add(this.label33);
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.label35);
            this.groupBox19.Controls.Add(this.label36);
            this.groupBox19.Controls.Add(this.IrUpuskTB);
            this.groupBox19.Controls.Add(this.IrModesCombo);
            this.groupBox19.Controls.Add(this.label37);
            this.groupBox19.Location = new System.Drawing.Point(3, 3);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(395, 223);
            this.groupBox19.TabIndex = 3;
            this.groupBox19.TabStop = false;
            // 
            // IrAVRCheck
            // 
            this.IrAVRCheck.AutoSize = true;
            this.IrAVRCheck.Location = new System.Drawing.Point(125, 197);
            this.IrAVRCheck.Name = "IrAVRCheck";
            this.IrAVRCheck.Size = new System.Drawing.Size(15, 14);
            this.IrAVRCheck.TabIndex = 64;
            this.IrAVRCheck.UseVisualStyleBackColor = true;
            // 
            // IrAPVCheck
            // 
            this.IrAPVCheck.AutoSize = true;
            this.IrAPVCheck.Location = new System.Drawing.Point(125, 177);
            this.IrAPVCheck.Name = "IrAPVCheck";
            this.IrAPVCheck.Size = new System.Drawing.Size(15, 14);
            this.IrAPVCheck.TabIndex = 63;
            this.IrAPVCheck.UseVisualStyleBackColor = true;
            // 
            // IrUROVCheck
            // 
            this.IrUROVCheck.AutoSize = true;
            this.IrUROVCheck.Location = new System.Drawing.Point(125, 157);
            this.IrUROVCheck.Name = "IrUROVCheck";
            this.IrUROVCheck.Size = new System.Drawing.Size(15, 14);
            this.IrUROVCheck.TabIndex = 62;
            this.IrUROVCheck.UseVisualStyleBackColor = true;
            // 
            // IrtyCheck
            // 
            this.IrtyCheck.AutoSize = true;
            this.IrtyCheck.Location = new System.Drawing.Point(104, 117);
            this.IrtyCheck.Name = "IrtyCheck";
            this.IrtyCheck.Size = new System.Drawing.Size(15, 14);
            this.IrtyCheck.TabIndex = 61;
            this.IrtyCheck.UseVisualStyleBackColor = true;
            // 
            // IrUpuskCheck
            // 
            this.IrUpuskCheck.AutoSize = true;
            this.IrUpuskCheck.Location = new System.Drawing.Point(104, 57);
            this.IrUpuskCheck.Name = "IrUpuskCheck";
            this.IrUpuskCheck.Size = new System.Drawing.Size(15, 14);
            this.IrUpuskCheck.TabIndex = 60;
            this.IrUpuskCheck.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 116);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(15, 13);
            this.label39.TabIndex = 58;
            this.label39.Text = "ty";
            // 
            // IrtyTB
            // 
            this.IrtyTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IrtyTB.Location = new System.Drawing.Point(125, 114);
            this.IrtyTB.Name = "IrtyTB";
            this.IrtyTB.Size = new System.Drawing.Size(119, 20);
            this.IrtyTB.TabIndex = 57;
            this.IrtyTB.Tag = "1500";
            this.IrtyTB.Text = "0";
            this.IrtyTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IrtcpTB
            // 
            this.IrtcpTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IrtcpTB.Location = new System.Drawing.Point(125, 94);
            this.IrtcpTB.Name = "IrtcpTB";
            this.IrtcpTB.Size = new System.Drawing.Size(119, 20);
            this.IrtcpTB.TabIndex = 56;
            this.IrtcpTB.Tag = "1500";
            this.IrtcpTB.Text = "0";
            this.IrtcpTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 96);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(22, 13);
            this.label38.TabIndex = 55;
            this.label38.Text = "tcp";
            // 
            // IrOSCCombo
            // 
            this.IrOSCCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IrOSCCombo.FormattingEnabled = true;
            this.IrOSCCombo.Location = new System.Drawing.Point(125, 134);
            this.IrOSCCombo.Name = "IrOSCCombo";
            this.IrOSCCombo.Size = new System.Drawing.Size(119, 21);
            this.IrOSCCombo.TabIndex = 50;
            // 
            // IrIcpTB
            // 
            this.IrIcpTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IrIcpTB.Location = new System.Drawing.Point(125, 74);
            this.IrIcpTB.Name = "IrIcpTB";
            this.IrIcpTB.Size = new System.Drawing.Size(119, 20);
            this.IrIcpTB.TabIndex = 49;
            this.IrIcpTB.Tag = "1500";
            this.IrIcpTB.Text = "0";
            this.IrIcpTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IrBlockingCombo
            // 
            this.IrBlockingCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IrBlockingCombo.FormattingEnabled = true;
            this.IrBlockingCombo.Location = new System.Drawing.Point(125, 33);
            this.IrBlockingCombo.Name = "IrBlockingCombo";
            this.IrBlockingCombo.Size = new System.Drawing.Size(119, 21);
            this.IrBlockingCombo.TabIndex = 48;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 197);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(28, 13);
            this.label30.TabIndex = 47;
            this.label30.Text = "АВР";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 177);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 13);
            this.label31.TabIndex = 46;
            this.label31.Text = "АПВ";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 157);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 13);
            this.label32.TabIndex = 45;
            this.label32.Text = "УРОВ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 137);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(33, 13);
            this.label33.TabIndex = 44;
            this.label33.Text = "ОСЦ.";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 76);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(22, 13);
            this.label34.TabIndex = 43;
            this.label34.Text = "Icp";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(38, 13);
            this.label35.TabIndex = 42;
            this.label35.Text = "Uпуск";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 36);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 13);
            this.label36.TabIndex = 41;
            this.label36.Text = "Блокировка";
            // 
            // IrUpuskTB
            // 
            this.IrUpuskTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.IrUpuskTB.Location = new System.Drawing.Point(125, 54);
            this.IrUpuskTB.Name = "IrUpuskTB";
            this.IrUpuskTB.Size = new System.Drawing.Size(119, 20);
            this.IrUpuskTB.TabIndex = 40;
            this.IrUpuskTB.Tag = "1500";
            this.IrUpuskTB.Text = "0";
            this.IrUpuskTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // IrModesCombo
            // 
            this.IrModesCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IrModesCombo.FormattingEnabled = true;
            this.IrModesCombo.Location = new System.Drawing.Point(125, 13);
            this.IrModesCombo.Name = "IrModesCombo";
            this.IrModesCombo.Size = new System.Drawing.Size(119, 21);
            this.IrModesCombo.TabIndex = 39;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 13);
            this.label37.TabIndex = 38;
            this.label37.Text = "Режим";
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.groupBox22);
            this.tabPage23.Location = new System.Drawing.Point(4, 25);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Size = new System.Drawing.Size(951, 465);
            this.tabPage23.TabIndex = 6;
            this.tabPage23.Text = "Защ. U>";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox22.Controls.Add(this._difensesUBDataGrid);
            this.groupBox22.Location = new System.Drawing.Point(3, 3);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(945, 167);
            this.groupBox22.TabIndex = 4;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Защиты U>";
            // 
            // _difensesUBDataGrid
            // 
            this._difensesUBDataGrid.AllowUserToAddRows = false;
            this._difensesUBDataGrid.AllowUserToDeleteRows = false;
            this._difensesUBDataGrid.AllowUserToResizeColumns = false;
            this._difensesUBDataGrid.AllowUserToResizeRows = false;
            this._difensesUBDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesUBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._uBStageColumn,
            this._uBModesColumn,
            this._uBTypeColumn,
            this.Column3,
            this._uBUsrColumn,
            this._uBTsrColumn,
            this._uBTvzColumn,
            this._uBUvzColumn,
            this._uBUvzYNColumn,
            this.Column4,
            this._uBBlockingColumn,
            this._uBOscColumn,
            this._uBUROVColumn,
            this._uBAPVColumn,
            this._uBAVRColumn,
            this._uBAPVRetColumn,
            this._uBSbrosColumn});
            this._difensesUBDataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesUBDataGrid.Name = "_difensesUBDataGrid";
            this._difensesUBDataGrid.RowHeadersVisible = false;
            this._difensesUBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUBDataGrid.RowTemplate.Height = 24;
            this._difensesUBDataGrid.ShowCellErrors = false;
            this._difensesUBDataGrid.ShowRowErrors = false;
            this._difensesUBDataGrid.Size = new System.Drawing.Size(933, 136);
            this._difensesUBDataGrid.TabIndex = 3;
            // 
            // _uBStageColumn
            // 
            dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle85.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle85.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle85.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle85.SelectionForeColor = System.Drawing.Color.White;
            this._uBStageColumn.DefaultCellStyle = dataGridViewCellStyle85;
            this._uBStageColumn.HeaderText = "Ступень";
            this._uBStageColumn.Name = "_uBStageColumn";
            this._uBStageColumn.ReadOnly = true;
            this._uBStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBStageColumn.Width = 80;
            // 
            // _uBModesColumn
            // 
            dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBModesColumn.DefaultCellStyle = dataGridViewCellStyle86;
            this._uBModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._uBModesColumn.HeaderText = "Режим";
            this._uBModesColumn.Name = "_uBModesColumn";
            this._uBModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBModesColumn.Width = 80;
            // 
            // _uBTypeColumn
            // 
            dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTypeColumn.DefaultCellStyle = dataGridViewCellStyle87;
            this._uBTypeColumn.HeaderText = "Тип";
            this._uBTypeColumn.Name = "_uBTypeColumn";
            this._uBTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBTypeColumn.Width = 90;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column3.Visible = false;
            // 
            // _uBUsrColumn
            // 
            dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUsrColumn.DefaultCellStyle = dataGridViewCellStyle88;
            this._uBUsrColumn.HeaderText = "Uср [В]";
            this._uBUsrColumn.Name = "_uBUsrColumn";
            this._uBUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUsrColumn.Width = 70;
            // 
            // _uBTsrColumn
            // 
            dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTsrColumn.DefaultCellStyle = dataGridViewCellStyle89;
            this._uBTsrColumn.HeaderText = "tср [мс]";
            this._uBTsrColumn.Name = "_uBTsrColumn";
            this._uBTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTsrColumn.Width = 70;
            // 
            // _uBTvzColumn
            // 
            dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBTvzColumn.DefaultCellStyle = dataGridViewCellStyle90;
            this._uBTvzColumn.HeaderText = "tвз [мс]";
            this._uBTvzColumn.Name = "_uBTvzColumn";
            this._uBTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBTvzColumn.Width = 70;
            // 
            // _uBUvzColumn
            // 
            dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBUvzColumn.DefaultCellStyle = dataGridViewCellStyle91;
            this._uBUvzColumn.HeaderText = "Uвз [В]";
            this._uBUvzColumn.Name = "_uBUvzColumn";
            this._uBUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uBUvzColumn.Width = 75;
            // 
            // _uBUvzYNColumn
            // 
            dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle92.NullValue = false;
            this._uBUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle92;
            this._uBUvzYNColumn.HeaderText = "Возврат";
            this._uBUvzYNColumn.Name = "_uBUvzYNColumn";
            this._uBUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBUvzYNColumn.Width = 80;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.Visible = false;
            // 
            // _uBBlockingColumn
            // 
            dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBBlockingColumn.DefaultCellStyle = dataGridViewCellStyle93;
            this._uBBlockingColumn.HeaderText = "Блокировка";
            this._uBBlockingColumn.Name = "_uBBlockingColumn";
            this._uBBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBBlockingColumn.Width = 90;
            // 
            // _uBOscColumn
            // 
            dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uBOscColumn.DefaultCellStyle = dataGridViewCellStyle94;
            this._uBOscColumn.HeaderText = "Осциллограф";
            this._uBOscColumn.Name = "_uBOscColumn";
            this._uBOscColumn.Width = 90;
            // 
            // _uBUROVColumn
            // 
            this._uBUROVColumn.HeaderText = "УРОВ";
            this._uBUROVColumn.Name = "_uBUROVColumn";
            this._uBUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uBUROVColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // _uBAPVColumn
            // 
            this._uBAPVColumn.HeaderText = "АПВ";
            this._uBAPVColumn.Name = "_uBAPVColumn";
            this._uBAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uBAVRColumn
            // 
            this._uBAVRColumn.HeaderText = "АВР";
            this._uBAVRColumn.Name = "_uBAVRColumn";
            this._uBAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uBAPVRetColumn
            // 
            this._uBAPVRetColumn.HeaderText = "АПВ возвр.";
            this._uBAPVRetColumn.Name = "_uBAPVRetColumn";
            this._uBAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uBSbrosColumn
            // 
            this._uBSbrosColumn.HeaderText = "Сброс";
            this._uBSbrosColumn.Name = "_uBSbrosColumn";
            this._uBSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage24
            // 
            this.tabPage24.Controls.Add(this.groupBox23);
            this.tabPage24.Location = new System.Drawing.Point(4, 25);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Size = new System.Drawing.Size(951, 465);
            this.tabPage24.TabIndex = 7;
            this.tabPage24.Text = "Защ. U<";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox23.Controls.Add(this._difensesUMDataGrid);
            this.groupBox23.Location = new System.Drawing.Point(3, 3);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(945, 166);
            this.groupBox23.TabIndex = 4;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Защиты U<";
            // 
            // _difensesUMDataGrid
            // 
            this._difensesUMDataGrid.AllowUserToAddRows = false;
            this._difensesUMDataGrid.AllowUserToDeleteRows = false;
            this._difensesUMDataGrid.AllowUserToResizeColumns = false;
            this._difensesUMDataGrid.AllowUserToResizeRows = false;
            this._difensesUMDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesUMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesUMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesUMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._uMStageColumn,
            this._uMModesColumn,
            this.Column5,
            this._uMTypeColumn,
            this._uMUsrColumn,
            this._uMTsrColumn,
            this._uMTvzColumn,
            this._uMUvzColumn,
            this._uMUvzYNColumn,
            this._uMBlockingUMColumn,
            this._uMBlockingColumn,
            this._uMOscColumn,
            this._uMUROVColumn,
            this._uMAPVColumn,
            this._uMAVRColumn,
            this._uMAPVRetColumn,
            this._uMSbrosColumn});
            this._difensesUMDataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesUMDataGrid.Name = "_difensesUMDataGrid";
            this._difensesUMDataGrid.RowHeadersVisible = false;
            this._difensesUMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesUMDataGrid.RowTemplate.Height = 24;
            this._difensesUMDataGrid.ShowCellErrors = false;
            this._difensesUMDataGrid.ShowRowErrors = false;
            this._difensesUMDataGrid.Size = new System.Drawing.Size(933, 136);
            this._difensesUMDataGrid.TabIndex = 4;
            // 
            // _uMStageColumn
            // 
            dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle95.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle95.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle95.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle95.SelectionForeColor = System.Drawing.Color.White;
            this._uMStageColumn.DefaultCellStyle = dataGridViewCellStyle95;
            this._uMStageColumn.Frozen = true;
            this._uMStageColumn.HeaderText = "Ступень";
            this._uMStageColumn.Name = "_uMStageColumn";
            this._uMStageColumn.ReadOnly = true;
            this._uMStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMStageColumn.Width = 80;
            // 
            // _uMModesColumn
            // 
            dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMModesColumn.DefaultCellStyle = dataGridViewCellStyle96;
            this._uMModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._uMModesColumn.HeaderText = "Режим";
            this._uMModesColumn.Name = "_uMModesColumn";
            this._uMModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMModesColumn.Width = 80;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Column5";
            this.Column5.Name = "Column5";
            this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column5.Visible = false;
            // 
            // _uMTypeColumn
            // 
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTypeColumn.DefaultCellStyle = dataGridViewCellStyle97;
            this._uMTypeColumn.HeaderText = "Тип";
            this._uMTypeColumn.Name = "_uMTypeColumn";
            this._uMTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMTypeColumn.Width = 90;
            // 
            // _uMUsrColumn
            // 
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMUsrColumn.DefaultCellStyle = dataGridViewCellStyle98;
            this._uMUsrColumn.HeaderText = "Uср [В]";
            this._uMUsrColumn.Name = "_uMUsrColumn";
            this._uMUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMUsrColumn.Width = 70;
            // 
            // _uMTsrColumn
            // 
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTsrColumn.DefaultCellStyle = dataGridViewCellStyle99;
            this._uMTsrColumn.HeaderText = "tср [мс]";
            this._uMTsrColumn.Name = "_uMTsrColumn";
            this._uMTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMTsrColumn.Width = 70;
            // 
            // _uMTvzColumn
            // 
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMTvzColumn.DefaultCellStyle = dataGridViewCellStyle100;
            this._uMTvzColumn.HeaderText = "tвз [мс]";
            this._uMTvzColumn.Name = "_uMTvzColumn";
            this._uMTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMTvzColumn.Width = 70;
            // 
            // _uMUvzColumn
            // 
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMUvzColumn.DefaultCellStyle = dataGridViewCellStyle101;
            this._uMUvzColumn.HeaderText = "Uвз [В]";
            this._uMUvzColumn.Name = "_uMUvzColumn";
            this._uMUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._uMUvzColumn.Width = 75;
            // 
            // _uMUvzYNColumn
            // 
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle102.NullValue = false;
            this._uMUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle102;
            this._uMUvzYNColumn.HeaderText = "Возврат";
            this._uMUvzYNColumn.Name = "_uMUvzYNColumn";
            this._uMUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMUvzYNColumn.Width = 80;
            // 
            // _uMBlockingUMColumn
            // 
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle103.NullValue = false;
            this._uMBlockingUMColumn.DefaultCellStyle = dataGridViewCellStyle103;
            this._uMBlockingUMColumn.HeaderText = "Блокировка U<5В";
            this._uMBlockingUMColumn.Name = "_uMBlockingUMColumn";
            this._uMBlockingUMColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMBlockingUMColumn.Width = 140;
            // 
            // _uMBlockingColumn
            // 
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMBlockingColumn.DefaultCellStyle = dataGridViewCellStyle104;
            this._uMBlockingColumn.HeaderText = "Блокировка";
            this._uMBlockingColumn.Name = "_uMBlockingColumn";
            this._uMBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._uMBlockingColumn.Width = 90;
            // 
            // _uMOscColumn
            // 
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._uMOscColumn.DefaultCellStyle = dataGridViewCellStyle105;
            this._uMOscColumn.HeaderText = "Осциллограф";
            this._uMOscColumn.Name = "_uMOscColumn";
            this._uMOscColumn.Width = 90;
            // 
            // _uMUROVColumn
            // 
            this._uMUROVColumn.HeaderText = "УРОВ";
            this._uMUROVColumn.Name = "_uMUROVColumn";
            this._uMUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uMAPVColumn
            // 
            this._uMAPVColumn.HeaderText = "АПВ";
            this._uMAPVColumn.Name = "_uMAPVColumn";
            this._uMAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uMAVRColumn
            // 
            this._uMAVRColumn.HeaderText = "АВР";
            this._uMAVRColumn.Name = "_uMAVRColumn";
            this._uMAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uMAPVRetColumn
            // 
            this._uMAPVRetColumn.HeaderText = "АПВ возвр.";
            this._uMAPVRetColumn.Name = "_uMAPVRetColumn";
            this._uMAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _uMSbrosColumn
            // 
            this._uMSbrosColumn.HeaderText = "Сброс";
            this._uMSbrosColumn.Name = "_uMSbrosColumn";
            this._uMSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage26
            // 
            this.tabPage26.Controls.Add(this.groupBox27);
            this.tabPage26.Location = new System.Drawing.Point(4, 25);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Size = new System.Drawing.Size(951, 465);
            this.tabPage26.TabIndex = 9;
            this.tabPage26.Text = "Защ. F>";
            this.tabPage26.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox27.Controls.Add(this._difensesFBDataGrid);
            this.groupBox27.Location = new System.Drawing.Point(3, 3);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(945, 167);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Защиты F>";
            // 
            // _difensesFBDataGrid
            // 
            this._difensesFBDataGrid.AllowUserToAddRows = false;
            this._difensesFBDataGrid.AllowUserToDeleteRows = false;
            this._difensesFBDataGrid.AllowUserToResizeColumns = false;
            this._difensesFBDataGrid.AllowUserToResizeRows = false;
            this._difensesFBDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesFBDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFBDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFBDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._fBStageColumn,
            this._fBModesColumn,
            this._fBUsrColumn,
            this._fBTsrColumn,
            this._fBTvzColumn,
            this._fBUvzColumn,
            this._fBUvzYNColumn,
            this._fBBlockingColumn,
            this._fBOscColumn,
            this._fBUROVColumn,
            this._fBAPVColumn,
            this._fBAVRColumn,
            this._fBAPVRetColumn,
            this._fBSbrosColumn});
            this._difensesFBDataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesFBDataGrid.Name = "_difensesFBDataGrid";
            this._difensesFBDataGrid.RowHeadersVisible = false;
            this._difensesFBDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFBDataGrid.RowTemplate.Height = 24;
            this._difensesFBDataGrid.ShowCellErrors = false;
            this._difensesFBDataGrid.ShowRowErrors = false;
            this._difensesFBDataGrid.Size = new System.Drawing.Size(933, 136);
            this._difensesFBDataGrid.TabIndex = 3;
            // 
            // _fBStageColumn
            // 
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle106.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle106.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.Color.White;
            this._fBStageColumn.DefaultCellStyle = dataGridViewCellStyle106;
            this._fBStageColumn.Frozen = true;
            this._fBStageColumn.HeaderText = "Ступень";
            this._fBStageColumn.Name = "_fBStageColumn";
            this._fBStageColumn.ReadOnly = true;
            this._fBStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBStageColumn.Width = 80;
            // 
            // _fBModesColumn
            // 
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBModesColumn.DefaultCellStyle = dataGridViewCellStyle107;
            this._fBModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._fBModesColumn.HeaderText = "Состояние";
            this._fBModesColumn.Name = "_fBModesColumn";
            this._fBModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fBModesColumn.Width = 80;
            // 
            // _fBUsrColumn
            // 
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBUsrColumn.DefaultCellStyle = dataGridViewCellStyle108;
            this._fBUsrColumn.HeaderText = "Fср [Гц]";
            this._fBUsrColumn.Name = "_fBUsrColumn";
            this._fBUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBUsrColumn.Width = 70;
            // 
            // _fBTsrColumn
            // 
            dataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBTsrColumn.DefaultCellStyle = dataGridViewCellStyle109;
            this._fBTsrColumn.HeaderText = "tср [мс]";
            this._fBTsrColumn.Name = "_fBTsrColumn";
            this._fBTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fBTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBTsrColumn.Width = 70;
            // 
            // _fBTvzColumn
            // 
            dataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBTvzColumn.DefaultCellStyle = dataGridViewCellStyle110;
            this._fBTvzColumn.HeaderText = "tвз [мс]";
            this._fBTvzColumn.Name = "_fBTvzColumn";
            this._fBTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBTvzColumn.Width = 70;
            // 
            // _fBUvzColumn
            // 
            dataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBUvzColumn.DefaultCellStyle = dataGridViewCellStyle111;
            this._fBUvzColumn.HeaderText = "Fвз [Гц]";
            this._fBUvzColumn.Name = "_fBUvzColumn";
            this._fBUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fBUvzColumn.Width = 75;
            // 
            // _fBUvzYNColumn
            // 
            dataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle112.NullValue = false;
            this._fBUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle112;
            this._fBUvzYNColumn.HeaderText = "Возврат";
            this._fBUvzYNColumn.Name = "_fBUvzYNColumn";
            this._fBUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fBUvzYNColumn.Width = 80;
            // 
            // _fBBlockingColumn
            // 
            dataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBBlockingColumn.DefaultCellStyle = dataGridViewCellStyle113;
            this._fBBlockingColumn.HeaderText = "Блокировка";
            this._fBBlockingColumn.Name = "_fBBlockingColumn";
            this._fBBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fBBlockingColumn.Width = 90;
            // 
            // _fBOscColumn
            // 
            dataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fBOscColumn.DefaultCellStyle = dataGridViewCellStyle114;
            this._fBOscColumn.HeaderText = "Осциллограф";
            this._fBOscColumn.Name = "_fBOscColumn";
            this._fBOscColumn.Width = 90;
            // 
            // _fBUROVColumn
            // 
            this._fBUROVColumn.HeaderText = "УРОВ";
            this._fBUROVColumn.Name = "_fBUROVColumn";
            this._fBUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fBAPVColumn
            // 
            this._fBAPVColumn.HeaderText = "АПВ";
            this._fBAPVColumn.Name = "_fBAPVColumn";
            this._fBAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fBAVRColumn
            // 
            this._fBAVRColumn.HeaderText = "АВР";
            this._fBAVRColumn.Name = "_fBAVRColumn";
            this._fBAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fBAPVRetColumn
            // 
            this._fBAPVRetColumn.HeaderText = "АПВ возвр.";
            this._fBAPVRetColumn.Name = "_fBAPVRetColumn";
            this._fBAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fBSbrosColumn
            // 
            this._fBSbrosColumn.HeaderText = "Сброс";
            this._fBSbrosColumn.Name = "_fBSbrosColumn";
            this._fBSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage27
            // 
            this.tabPage27.Controls.Add(this.groupBox28);
            this.tabPage27.Location = new System.Drawing.Point(4, 25);
            this.tabPage27.Name = "tabPage27";
            this.tabPage27.Size = new System.Drawing.Size(951, 465);
            this.tabPage27.TabIndex = 10;
            this.tabPage27.Text = "Защ. F<";
            this.tabPage27.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox28.Controls.Add(this._difensesFMDataGrid);
            this.groupBox28.Location = new System.Drawing.Point(3, 3);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(945, 167);
            this.groupBox28.TabIndex = 5;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Защиты F<";
            // 
            // _difensesFMDataGrid
            // 
            this._difensesFMDataGrid.AllowUserToAddRows = false;
            this._difensesFMDataGrid.AllowUserToDeleteRows = false;
            this._difensesFMDataGrid.AllowUserToResizeColumns = false;
            this._difensesFMDataGrid.AllowUserToResizeRows = false;
            this._difensesFMDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._difensesFMDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._difensesFMDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._difensesFMDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._fMStageColumn,
            this._fMModesColumn,
            this._fMUsrColumn,
            this._fMTsrColumn,
            this._fMTvzColumn,
            this._fMUvzColumn,
            this._fMUvzYNColumn,
            this._fMBlockingColumn,
            this._fMOscColumn,
            this._fMUROVColumn,
            this._fMAPVColumn,
            this._fMAVRColumn,
            this._fMAPVRetColumn,
            this._fMSbrosColumn});
            this._difensesFMDataGrid.Location = new System.Drawing.Point(6, 21);
            this._difensesFMDataGrid.Name = "_difensesFMDataGrid";
            this._difensesFMDataGrid.RowHeadersVisible = false;
            this._difensesFMDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._difensesFMDataGrid.RowTemplate.Height = 24;
            this._difensesFMDataGrid.ShowCellErrors = false;
            this._difensesFMDataGrid.ShowRowErrors = false;
            this._difensesFMDataGrid.Size = new System.Drawing.Size(933, 136);
            this._difensesFMDataGrid.TabIndex = 3;
            // 
            // _fMStageColumn
            // 
            dataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle115.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle115.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle115.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle115.SelectionForeColor = System.Drawing.Color.White;
            this._fMStageColumn.DefaultCellStyle = dataGridViewCellStyle115;
            this._fMStageColumn.Frozen = true;
            this._fMStageColumn.HeaderText = "Ступень";
            this._fMStageColumn.Name = "_fMStageColumn";
            this._fMStageColumn.ReadOnly = true;
            this._fMStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fMStageColumn.Width = 80;
            // 
            // _fMModesColumn
            // 
            dataGridViewCellStyle116.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMModesColumn.DefaultCellStyle = dataGridViewCellStyle116;
            this._fMModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._fMModesColumn.HeaderText = "Состояние";
            this._fMModesColumn.Name = "_fMModesColumn";
            this._fMModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fMModesColumn.Width = 80;
            // 
            // _fMUsrColumn
            // 
            dataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMUsrColumn.DefaultCellStyle = dataGridViewCellStyle117;
            this._fMUsrColumn.HeaderText = "Fср [Гц]";
            this._fMUsrColumn.Name = "_fMUsrColumn";
            this._fMUsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fMUsrColumn.Width = 70;
            // 
            // _fMTsrColumn
            // 
            dataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMTsrColumn.DefaultCellStyle = dataGridViewCellStyle118;
            this._fMTsrColumn.HeaderText = "tср [мс]";
            this._fMTsrColumn.Name = "_fMTsrColumn";
            this._fMTsrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fMTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fMTsrColumn.Width = 70;
            // 
            // _fMTvzColumn
            // 
            dataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMTvzColumn.DefaultCellStyle = dataGridViewCellStyle119;
            this._fMTvzColumn.HeaderText = "tвз [мс]";
            this._fMTvzColumn.Name = "_fMTvzColumn";
            this._fMTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fMTvzColumn.Width = 70;
            // 
            // _fMUvzColumn
            // 
            dataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMUvzColumn.DefaultCellStyle = dataGridViewCellStyle120;
            this._fMUvzColumn.HeaderText = "Fвз [Гц]";
            this._fMUvzColumn.Name = "_fMUvzColumn";
            this._fMUvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._fMUvzColumn.Width = 75;
            // 
            // _fMUvzYNColumn
            // 
            dataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle121.NullValue = false;
            this._fMUvzYNColumn.DefaultCellStyle = dataGridViewCellStyle121;
            this._fMUvzYNColumn.HeaderText = "Возврат";
            this._fMUvzYNColumn.Name = "_fMUvzYNColumn";
            this._fMUvzYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fMUvzYNColumn.Width = 80;
            // 
            // _fMBlockingColumn
            // 
            dataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMBlockingColumn.DefaultCellStyle = dataGridViewCellStyle122;
            this._fMBlockingColumn.HeaderText = "Блокировка";
            this._fMBlockingColumn.Name = "_fMBlockingColumn";
            this._fMBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._fMBlockingColumn.Width = 90;
            // 
            // _fMOscColumn
            // 
            dataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._fMOscColumn.DefaultCellStyle = dataGridViewCellStyle123;
            this._fMOscColumn.HeaderText = "Осциллограф";
            this._fMOscColumn.Name = "_fMOscColumn";
            this._fMOscColumn.Width = 90;
            // 
            // _fMUROVColumn
            // 
            this._fMUROVColumn.HeaderText = "УРОВ";
            this._fMUROVColumn.Name = "_fMUROVColumn";
            this._fMUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fMAPVColumn
            // 
            this._fMAPVColumn.HeaderText = "АПВ";
            this._fMAPVColumn.Name = "_fMAPVColumn";
            this._fMAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fMAVRColumn
            // 
            this._fMAVRColumn.HeaderText = "АВР";
            this._fMAVRColumn.Name = "_fMAVRColumn";
            this._fMAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fMAPVRetColumn
            // 
            this._fMAPVRetColumn.HeaderText = "АПВ возвр.";
            this._fMAPVRetColumn.Name = "_fMAPVRetColumn";
            this._fMAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _fMSbrosColumn
            // 
            this._fMSbrosColumn.HeaderText = "Сброс";
            this._fMSbrosColumn.Name = "_fMSbrosColumn";
            this._fMSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.groupBox24);
            this.tabPage25.Location = new System.Drawing.Point(4, 25);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Size = new System.Drawing.Size(951, 465);
            this.tabPage25.TabIndex = 8;
            this.tabPage25.Text = "Внешние";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox24.Controls.Add(this._externalDifensesDataGrid);
            this.groupBox24.Location = new System.Drawing.Point(3, 3);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(948, 450);
            this.groupBox24.TabIndex = 4;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Внешние защиты";
            // 
            // _externalDifensesDataGrid
            // 
            this._externalDifensesDataGrid.AllowUserToAddRows = false;
            this._externalDifensesDataGrid.AllowUserToDeleteRows = false;
            this._externalDifensesDataGrid.AllowUserToResizeColumns = false;
            this._externalDifensesDataGrid.AllowUserToResizeRows = false;
            this._externalDifensesDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._externalDifensesDataGrid.BackgroundColor = System.Drawing.Color.White;
            this._externalDifensesDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._externalDifensesDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._externalDifStageColumn,
            this._externalDifModesColumn,
            this._externalDifSrabColumn,
            this._externalDifTsrColumn,
            this._externalDifTvzColumn,
            this._externalDifVozvrColumn,
            this._externalDifVozvrYNColumn,
            this._externalDifBlockingColumn,
            this._externalDifOscColumn,
            this._externalDifUROVColumn,
            this._externalDifAPVColumn,
            this._externalDifAVRColumn,
            this._externalDifAPVRetColumn,
            this._externalDifSbrosColumn});
            this._externalDifensesDataGrid.Location = new System.Drawing.Point(6, 21);
            this._externalDifensesDataGrid.Name = "_externalDifensesDataGrid";
            this._externalDifensesDataGrid.RowHeadersVisible = false;
            this._externalDifensesDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._externalDifensesDataGrid.RowTemplate.Height = 24;
            this._externalDifensesDataGrid.ShowCellErrors = false;
            this._externalDifensesDataGrid.ShowRowErrors = false;
            this._externalDifensesDataGrid.Size = new System.Drawing.Size(936, 425);
            this._externalDifensesDataGrid.TabIndex = 3;
            // 
            // _externalDifStageColumn
            // 
            dataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle124.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle124.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle124.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle124.SelectionForeColor = System.Drawing.Color.White;
            this._externalDifStageColumn.DefaultCellStyle = dataGridViewCellStyle124;
            this._externalDifStageColumn.Frozen = true;
            this._externalDifStageColumn.HeaderText = "Ступень";
            this._externalDifStageColumn.Name = "_externalDifStageColumn";
            this._externalDifStageColumn.ReadOnly = true;
            this._externalDifStageColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _externalDifModesColumn
            // 
            dataGridViewCellStyle125.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifModesColumn.DefaultCellStyle = dataGridViewCellStyle125;
            this._externalDifModesColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._externalDifModesColumn.HeaderText = "Состояние";
            this._externalDifModesColumn.Name = "_externalDifModesColumn";
            this._externalDifModesColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifModesColumn.Width = 95;
            // 
            // _externalDifSrabColumn
            // 
            dataGridViewCellStyle126.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifSrabColumn.DefaultCellStyle = dataGridViewCellStyle126;
            this._externalDifSrabColumn.HeaderText = "Сигнал срабатывания";
            this._externalDifSrabColumn.Name = "_externalDifSrabColumn";
            this._externalDifSrabColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifSrabColumn.Width = 130;
            // 
            // _externalDifTsrColumn
            // 
            dataGridViewCellStyle127.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTsrColumn.DefaultCellStyle = dataGridViewCellStyle127;
            this._externalDifTsrColumn.HeaderText = "tср [мс]";
            this._externalDifTsrColumn.Name = "_externalDifTsrColumn";
            this._externalDifTsrColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTsrColumn.Width = 70;
            // 
            // _externalDifTvzColumn
            // 
            dataGridViewCellStyle128.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifTvzColumn.DefaultCellStyle = dataGridViewCellStyle128;
            this._externalDifTvzColumn.HeaderText = "tвз [мс]";
            this._externalDifTvzColumn.Name = "_externalDifTvzColumn";
            this._externalDifTvzColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._externalDifTvzColumn.Width = 70;
            // 
            // _externalDifVozvrColumn
            // 
            dataGridViewCellStyle129.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifVozvrColumn.DefaultCellStyle = dataGridViewCellStyle129;
            this._externalDifVozvrColumn.HeaderText = "Сигнал возврата";
            this._externalDifVozvrColumn.Name = "_externalDifVozvrColumn";
            this._externalDifVozvrColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrColumn.Width = 110;
            // 
            // _externalDifVozvrYNColumn
            // 
            dataGridViewCellStyle130.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle130.NullValue = false;
            this._externalDifVozvrYNColumn.DefaultCellStyle = dataGridViewCellStyle130;
            this._externalDifVozvrYNColumn.HeaderText = "Возврат";
            this._externalDifVozvrYNColumn.Name = "_externalDifVozvrYNColumn";
            this._externalDifVozvrYNColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifVozvrYNColumn.Width = 80;
            // 
            // _externalDifBlockingColumn
            // 
            dataGridViewCellStyle131.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifBlockingColumn.DefaultCellStyle = dataGridViewCellStyle131;
            this._externalDifBlockingColumn.HeaderText = "Сигнал блокировки";
            this._externalDifBlockingColumn.Name = "_externalDifBlockingColumn";
            this._externalDifBlockingColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._externalDifBlockingColumn.Width = 120;
            // 
            // _externalDifOscColumn
            // 
            dataGridViewCellStyle132.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._externalDifOscColumn.DefaultCellStyle = dataGridViewCellStyle132;
            this._externalDifOscColumn.HeaderText = "Осциллограф";
            this._externalDifOscColumn.Name = "_externalDifOscColumn";
            this._externalDifOscColumn.Width = 110;
            // 
            // _externalDifUROVColumn
            // 
            dataGridViewCellStyle133.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle133.NullValue = false;
            this._externalDifUROVColumn.DefaultCellStyle = dataGridViewCellStyle133;
            this._externalDifUROVColumn.HeaderText = "УРОВ";
            this._externalDifUROVColumn.Name = "_externalDifUROVColumn";
            this._externalDifUROVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _externalDifAPVColumn
            // 
            dataGridViewCellStyle134.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle134.NullValue = false;
            this._externalDifAPVColumn.DefaultCellStyle = dataGridViewCellStyle134;
            this._externalDifAPVColumn.HeaderText = "АПВ";
            this._externalDifAPVColumn.Name = "_externalDifAPVColumn";
            this._externalDifAPVColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _externalDifAVRColumn
            // 
            dataGridViewCellStyle135.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle135.NullValue = false;
            this._externalDifAVRColumn.DefaultCellStyle = dataGridViewCellStyle135;
            this._externalDifAVRColumn.HeaderText = "АВР";
            this._externalDifAVRColumn.Name = "_externalDifAVRColumn";
            this._externalDifAVRColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _externalDifAPVRetColumn
            // 
            dataGridViewCellStyle136.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle136.NullValue = false;
            this._externalDifAPVRetColumn.DefaultCellStyle = dataGridViewCellStyle136;
            this._externalDifAPVRetColumn.HeaderText = "АПВ возвр.";
            this._externalDifAPVRetColumn.Name = "_externalDifAPVRetColumn";
            this._externalDifAPVRetColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // _externalDifSbrosColumn
            // 
            dataGridViewCellStyle137.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle137.NullValue = false;
            this._externalDifSbrosColumn.DefaultCellStyle = dataGridViewCellStyle137;
            this._externalDifSbrosColumn.HeaderText = "Сброс";
            this._externalDifSbrosColumn.Name = "_externalDifSbrosColumn";
            this._externalDifSbrosColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this._engineDefensesGrid);
            this.tabPage18.Controls.Add(this.groupBox25);
            this.tabPage18.Location = new System.Drawing.Point(4, 25);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(951, 465);
            this.tabPage18.TabIndex = 11;
            this.tabPage18.Text = "Защиты двигателя";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // _engineDefensesGrid
            // 
            this._engineDefensesGrid.AllowUserToAddRows = false;
            this._engineDefensesGrid.AllowUserToDeleteRows = false;
            this._engineDefensesGrid.AllowUserToResizeColumns = false;
            this._engineDefensesGrid.AllowUserToResizeRows = false;
            this._engineDefensesGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._engineDefensesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._engineDefensesGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            this._engineDefensesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._engineDefensesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._engineDefenseNameCol,
            this._engineDefenseModeCol,
            this._engineDefenseConstraintCol,
            this._engineDefenseBlockCol,
            this._engineDefenseOscCol,
            this._engineDefenseUROVcol,
            this._engineDefenseAVRcol,
            this._engineDefenseAPVcol});
            this._engineDefensesGrid.Location = new System.Drawing.Point(6, 144);
            this._engineDefensesGrid.Name = "_engineDefensesGrid";
            this._engineDefensesGrid.RowHeadersVisible = false;
            this._engineDefensesGrid.RowTemplate.Height = 24;
            this._engineDefensesGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._engineDefensesGrid.Size = new System.Drawing.Size(945, 74);
            this._engineDefensesGrid.TabIndex = 3;
            // 
            // _engineDefenseNameCol
            // 
            this._engineDefenseNameCol.HeaderText = "";
            this._engineDefenseNameCol.MinimumWidth = 40;
            this._engineDefenseNameCol.Name = "_engineDefenseNameCol";
            this._engineDefenseNameCol.ReadOnly = true;
            this._engineDefenseNameCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseNameCol.Width = 40;
            // 
            // _engineDefenseModeCol
            // 
            this._engineDefenseModeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._engineDefenseModeCol.HeaderText = "Режим";
            this._engineDefenseModeCol.MinimumWidth = 120;
            this._engineDefenseModeCol.Name = "_engineDefenseModeCol";
            this._engineDefenseModeCol.Width = 120;
            // 
            // _engineDefenseConstraintCol
            // 
            dataGridViewCellStyle138.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle138.NullValue = null;
            this._engineDefenseConstraintCol.DefaultCellStyle = dataGridViewCellStyle138;
            this._engineDefenseConstraintCol.HeaderText = "Уставка";
            this._engineDefenseConstraintCol.MinimumWidth = 50;
            this._engineDefenseConstraintCol.Name = "_engineDefenseConstraintCol";
            this._engineDefenseConstraintCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._engineDefenseConstraintCol.Width = 56;
            // 
            // _engineDefenseBlockCol
            // 
            this._engineDefenseBlockCol.HeaderText = "Блокировка";
            this._engineDefenseBlockCol.MinimumWidth = 100;
            this._engineDefenseBlockCol.Name = "_engineDefenseBlockCol";
            // 
            // _engineDefenseOscCol
            // 
            this._engineDefenseOscCol.HeaderText = "Осц.";
            this._engineDefenseOscCol.MinimumWidth = 150;
            this._engineDefenseOscCol.Name = "_engineDefenseOscCol";
            this._engineDefenseOscCol.Width = 150;
            // 
            // _engineDefenseUROVcol
            // 
            this._engineDefenseUROVcol.HeaderText = "УРОВ";
            this._engineDefenseUROVcol.MinimumWidth = 50;
            this._engineDefenseUROVcol.Name = "_engineDefenseUROVcol";
            this._engineDefenseUROVcol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._engineDefenseUROVcol.Width = 50;
            // 
            // _engineDefenseAVRcol
            // 
            this._engineDefenseAVRcol.HeaderText = "АВР";
            this._engineDefenseAVRcol.MinimumWidth = 50;
            this._engineDefenseAVRcol.Name = "_engineDefenseAVRcol";
            this._engineDefenseAVRcol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._engineDefenseAVRcol.Width = 50;
            // 
            // _engineDefenseAPVcol
            // 
            this._engineDefenseAPVcol.HeaderText = "АПВ";
            this._engineDefenseAPVcol.MinimumWidth = 50;
            this._engineDefenseAPVcol.Name = "_engineDefenseAPVcol";
            this._engineDefenseAPVcol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._engineDefenseAPVcol.Width = 50;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.groupBox43);
            this.groupBox25.Controls.Add(this.groupBox44);
            this.groupBox25.Location = new System.Drawing.Point(6, 6);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(434, 132);
            this.groupBox25.TabIndex = 2;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Блокировки пусков";
            // 
            // groupBox43
            // 
            this.groupBox43.Controls.Add(this.label54);
            this.groupBox43.Controls.Add(this.label56);
            this.groupBox43.Controls.Add(this._engineQconstraintBox);
            this.groupBox43.Controls.Add(this.label55);
            this.groupBox43.Controls.Add(this._engineQtimeBox);
            this.groupBox43.Controls.Add(this._engineQmodeCombo);
            this.groupBox43.Location = new System.Drawing.Point(238, 19);
            this.groupBox43.Name = "groupBox43";
            this.groupBox43.Size = new System.Drawing.Size(186, 108);
            this.groupBox43.TabIndex = 4;
            this.groupBox43.TabStop = false;
            this.groupBox43.Text = "Блокировка по тепловому состоянию Q";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(9, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(42, 13);
            this.label54.TabIndex = 14;
            this.label54.Text = "Режим";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(9, 68);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(82, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Уставка, Qблк";
            // 
            // _engineQconstraintBox
            // 
            this._engineQconstraintBox.Location = new System.Drawing.Point(111, 61);
            this._engineQconstraintBox.Name = "_engineQconstraintBox";
            this._engineQconstraintBox.Size = new System.Drawing.Size(69, 20);
            this._engineQconstraintBox.TabIndex = 1;
            this._engineQconstraintBox.Tag = "256";
            this._engineQconstraintBox.Text = "0";
            this._engineQconstraintBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(9, 87);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(80, 13);
            this.label55.TabIndex = 6;
            this.label55.Text = "Время Tблк, c";
            // 
            // _engineQtimeBox
            // 
            this._engineQtimeBox.Location = new System.Drawing.Point(111, 81);
            this._engineQtimeBox.Name = "_engineQtimeBox";
            this._engineQtimeBox.Size = new System.Drawing.Size(69, 20);
            this._engineQtimeBox.TabIndex = 7;
            this._engineQtimeBox.Tag = "65000";
            this._engineQtimeBox.Text = "0";
            this._engineQtimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineQmodeCombo
            // 
            this._engineQmodeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._engineQmodeCombo.FormattingEnabled = true;
            this._engineQmodeCombo.Location = new System.Drawing.Point(90, 36);
            this._engineQmodeCombo.Name = "_engineQmodeCombo";
            this._engineQmodeCombo.Size = new System.Drawing.Size(90, 21);
            this._engineQmodeCombo.TabIndex = 12;
            // 
            // groupBox44
            // 
            this.groupBox44.Controls.Add(this._engineBlockDurationBox);
            this.groupBox44.Controls.Add(this.label51);
            this.groupBox44.Controls.Add(this._engineBlockTimeBox);
            this.groupBox44.Controls.Add(this.label53);
            this.groupBox44.Controls.Add(this.label50);
            this.groupBox44.Controls.Add(this._engineBlockPuskBox);
            this.groupBox44.Controls.Add(this._engineBlockHeatBox);
            this.groupBox44.Controls.Add(this.label52);
            this.groupBox44.Location = new System.Drawing.Point(6, 19);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(226, 108);
            this.groupBox44.TabIndex = 5;
            this.groupBox44.TabStop = false;
            this.groupBox44.Text = "Блокировка по числу пусков N";
            // 
            // _engineBlockDurationBox
            // 
            this._engineBlockDurationBox.Location = new System.Drawing.Point(151, 81);
            this._engineBlockDurationBox.Name = "_engineBlockDurationBox";
            this._engineBlockDurationBox.Size = new System.Drawing.Size(69, 20);
            this._engineBlockDurationBox.TabIndex = 24;
            this._engineBlockDurationBox.Tag = "65000";
            this._engineBlockDurationBox.Text = "0";
            this._engineBlockDurationBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(14, 84);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(92, 13);
            this.label51.TabIndex = 23;
            this.label51.Text = "Блок по Tдлит, с";
            // 
            // _engineBlockTimeBox
            // 
            this._engineBlockTimeBox.Location = new System.Drawing.Point(151, 60);
            this._engineBlockTimeBox.Name = "_engineBlockTimeBox";
            this._engineBlockTimeBox.Size = new System.Drawing.Size(69, 20);
            this._engineBlockTimeBox.TabIndex = 22;
            this._engineBlockTimeBox.Tag = "65000";
            this._engineBlockTimeBox.Text = "0";
            this._engineBlockTimeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(14, 26);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(84, 13);
            this.label53.TabIndex = 15;
            this.label53.Text = "Блок. по Nпуск";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(15, 65);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(125, 13);
            this.label50.TabIndex = 21;
            this.label50.Text = "Время блокировки T, c";
            // 
            // _engineBlockPuskBox
            // 
            this._engineBlockPuskBox.Location = new System.Drawing.Point(151, 19);
            this._engineBlockPuskBox.Name = "_engineBlockPuskBox";
            this._engineBlockPuskBox.Size = new System.Drawing.Size(69, 20);
            this._engineBlockPuskBox.TabIndex = 16;
            this._engineBlockPuskBox.Tag = "10";
            this._engineBlockPuskBox.Text = "0";
            this._engineBlockPuskBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _engineBlockHeatBox
            // 
            this._engineBlockHeatBox.Location = new System.Drawing.Point(151, 39);
            this._engineBlockHeatBox.Name = "_engineBlockHeatBox";
            this._engineBlockHeatBox.Size = new System.Drawing.Size(69, 20);
            this._engineBlockHeatBox.TabIndex = 18;
            this._engineBlockHeatBox.Tag = "10";
            this._engineBlockHeatBox.Text = "0";
            this._engineBlockHeatBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(14, 46);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(75, 13);
            this.label52.TabIndex = 17;
            this.label52.Text = "Блок по Nгор";
            // 
            // _outputSignalsPage
            // 
            this._outputSignalsPage.Controls.Add(this.groupBox46);
            this._outputSignalsPage.Controls.Add(this.groupBox26);
            this._outputSignalsPage.Controls.Add(this.groupBox13);
            this._outputSignalsPage.Controls.Add(this.groupBox11);
            this._outputSignalsPage.Controls.Add(this.groupBox12);
            this._outputSignalsPage.Location = new System.Drawing.Point(4, 22);
            this._outputSignalsPage.Name = "_outputSignalsPage";
            this._outputSignalsPage.Size = new System.Drawing.Size(1021, 553);
            this._outputSignalsPage.TabIndex = 6;
            this._outputSignalsPage.Text = "Выходные сигналы";
            this._outputSignalsPage.UseVisualStyleBackColor = true;
            // 
            // groupBox46
            // 
            this.groupBox46.Controls.Add(this.treeViewForVLS);
            this.groupBox46.Location = new System.Drawing.Point(815, 4);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(200, 546);
            this.groupBox46.TabIndex = 13;
            this.groupBox46.TabStop = false;
            this.groupBox46.Text = "Список всех ВЛС";
            // 
            // treeViewForVLS
            // 
            this.treeViewForVLS.Location = new System.Drawing.Point(6, 19);
            this.treeViewForVLS.Name = "treeViewForVLS";
            this.treeViewForVLS.Size = new System.Drawing.Size(188, 521);
            this.treeViewForVLS.TabIndex = 0;
            this.treeViewForVLS.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeViewForVLS_NodeMouseClick);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this._fault4CheckBox);
            this.groupBox26.Controls.Add(this._fault3CheckBox);
            this.groupBox26.Controls.Add(this._fault2CheckBox);
            this.groupBox26.Controls.Add(this._fault1CheckBox);
            this.groupBox26.Controls.Add(this.label1);
            this.groupBox26.Controls.Add(this.label83);
            this.groupBox26.Controls.Add(this.label82);
            this.groupBox26.Controls.Add(this.label81);
            this.groupBox26.Controls.Add(this._impTB);
            this.groupBox26.Controls.Add(this.label84);
            this.groupBox26.Location = new System.Drawing.Point(8, 394);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(393, 156);
            this.groupBox26.TabIndex = 6;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Реле неисправность";
            // 
            // _fault4CheckBox
            // 
            this._fault4CheckBox.AutoSize = true;
            this._fault4CheckBox.Location = new System.Drawing.Point(181, 93);
            this._fault4CheckBox.Name = "_fault4CheckBox";
            this._fault4CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault4CheckBox.TabIndex = 18;
            this._fault4CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault3CheckBox
            // 
            this._fault3CheckBox.AutoSize = true;
            this._fault3CheckBox.Location = new System.Drawing.Point(181, 69);
            this._fault3CheckBox.Name = "_fault3CheckBox";
            this._fault3CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault3CheckBox.TabIndex = 17;
            this._fault3CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault2CheckBox
            // 
            this._fault2CheckBox.AutoSize = true;
            this._fault2CheckBox.Location = new System.Drawing.Point(181, 45);
            this._fault2CheckBox.Name = "_fault2CheckBox";
            this._fault2CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault2CheckBox.TabIndex = 16;
            this._fault2CheckBox.UseVisualStyleBackColor = true;
            // 
            // _fault1CheckBox
            // 
            this._fault1CheckBox.AutoSize = true;
            this._fault1CheckBox.Location = new System.Drawing.Point(181, 21);
            this._fault1CheckBox.Name = "_fault1CheckBox";
            this._fault1CheckBox.Size = new System.Drawing.Size(15, 14);
            this._fault1CheckBox.TabIndex = 15;
            this._fault1CheckBox.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "4. Неисправность выключателя";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 70);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(157, 13);
            this.label83.TabIndex = 13;
            this.label83.Text = "3. Неисправность измерений";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 46);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(170, 13);
            this.label82.TabIndex = 12;
            this.label82.Text = "2. Программная неисправность";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 22);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(159, 13);
            this.label81.TabIndex = 11;
            this.label81.Text = "1. Аппаратная неисправность";
            // 
            // _impTB
            // 
            this._impTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._impTB.Location = new System.Drawing.Point(182, 115);
            this._impTB.Name = "_impTB";
            this._impTB.Size = new System.Drawing.Size(121, 20);
            this._impTB.TabIndex = 7;
            this._impTB.Text = "0";
            this._impTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 117);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(64, 13);
            this.label84.TabIndex = 3;
            this.label84.Text = "Твозвр, мс";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.VLSTabControl);
            this.groupBox13.Location = new System.Drawing.Point(407, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(402, 546);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "ВЛС";
            // 
            // VLSTabControl
            // 
            this.VLSTabControl.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.VLSTabControl.Controls.Add(this.VLS1);
            this.VLSTabControl.Controls.Add(this.VLS2);
            this.VLSTabControl.Controls.Add(this.VLS3);
            this.VLSTabControl.Controls.Add(this.VLS4);
            this.VLSTabControl.Controls.Add(this.VLS5);
            this.VLSTabControl.Controls.Add(this.VLS6);
            this.VLSTabControl.Controls.Add(this.VLS7);
            this.VLSTabControl.Controls.Add(this.VLS8);
            this.VLSTabControl.Controls.Add(this.VLS9);
            this.VLSTabControl.Controls.Add(this.VLS10);
            this.VLSTabControl.Controls.Add(this.VLS11);
            this.VLSTabControl.Controls.Add(this.VLS12);
            this.VLSTabControl.Controls.Add(this.VLS13);
            this.VLSTabControl.Controls.Add(this.VLS14);
            this.VLSTabControl.Controls.Add(this.VLS15);
            this.VLSTabControl.Controls.Add(this.VLS16);
            this.VLSTabControl.Location = new System.Drawing.Point(6, 19);
            this.VLSTabControl.Multiline = true;
            this.VLSTabControl.Name = "VLSTabControl";
            this.VLSTabControl.SelectedIndex = 0;
            this.VLSTabControl.Size = new System.Drawing.Size(390, 521);
            this.VLSTabControl.TabIndex = 0;
            // 
            // VLS1
            // 
            this.VLS1.Controls.Add(this.VLScheckedListBox1);
            this.VLS1.Location = new System.Drawing.Point(4, 49);
            this.VLS1.Name = "VLS1";
            this.VLS1.Padding = new System.Windows.Forms.Padding(3);
            this.VLS1.Size = new System.Drawing.Size(382, 468);
            this.VLS1.TabIndex = 0;
            this.VLS1.Text = "ВЛС 1";
            this.VLS1.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox1
            // 
            this.VLScheckedListBox1.CheckOnClick = true;
            this.VLScheckedListBox1.FormattingEnabled = true;
            this.VLScheckedListBox1.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox1.Name = "VLScheckedListBox1";
            this.VLScheckedListBox1.ScrollAlwaysVisible = true;
            this.VLScheckedListBox1.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox1.TabIndex = 6;
            this.VLScheckedListBox1.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS2
            // 
            this.VLS2.Controls.Add(this.VLScheckedListBox2);
            this.VLS2.Location = new System.Drawing.Point(4, 49);
            this.VLS2.Name = "VLS2";
            this.VLS2.Padding = new System.Windows.Forms.Padding(3);
            this.VLS2.Size = new System.Drawing.Size(382, 468);
            this.VLS2.TabIndex = 1;
            this.VLS2.Text = "ВЛС 2";
            this.VLS2.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox2
            // 
            this.VLScheckedListBox2.CheckOnClick = true;
            this.VLScheckedListBox2.FormattingEnabled = true;
            this.VLScheckedListBox2.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox2.Name = "VLScheckedListBox2";
            this.VLScheckedListBox2.ScrollAlwaysVisible = true;
            this.VLScheckedListBox2.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox2.TabIndex = 6;
            this.VLScheckedListBox2.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS3
            // 
            this.VLS3.Controls.Add(this.VLScheckedListBox3);
            this.VLS3.Location = new System.Drawing.Point(4, 49);
            this.VLS3.Name = "VLS3";
            this.VLS3.Size = new System.Drawing.Size(382, 468);
            this.VLS3.TabIndex = 2;
            this.VLS3.Text = "ВЛС   3";
            this.VLS3.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox3
            // 
            this.VLScheckedListBox3.CheckOnClick = true;
            this.VLScheckedListBox3.FormattingEnabled = true;
            this.VLScheckedListBox3.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox3.Name = "VLScheckedListBox3";
            this.VLScheckedListBox3.ScrollAlwaysVisible = true;
            this.VLScheckedListBox3.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox3.TabIndex = 6;
            this.VLScheckedListBox3.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS4
            // 
            this.VLS4.Controls.Add(this.VLScheckedListBox4);
            this.VLS4.Location = new System.Drawing.Point(4, 49);
            this.VLS4.Name = "VLS4";
            this.VLS4.Size = new System.Drawing.Size(382, 468);
            this.VLS4.TabIndex = 3;
            this.VLS4.Text = "ВЛС 4";
            this.VLS4.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox4
            // 
            this.VLScheckedListBox4.CheckOnClick = true;
            this.VLScheckedListBox4.FormattingEnabled = true;
            this.VLScheckedListBox4.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox4.Name = "VLScheckedListBox4";
            this.VLScheckedListBox4.ScrollAlwaysVisible = true;
            this.VLScheckedListBox4.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox4.TabIndex = 6;
            this.VLScheckedListBox4.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS5
            // 
            this.VLS5.Controls.Add(this.VLScheckedListBox5);
            this.VLS5.Location = new System.Drawing.Point(4, 49);
            this.VLS5.Name = "VLS5";
            this.VLS5.Size = new System.Drawing.Size(382, 468);
            this.VLS5.TabIndex = 4;
            this.VLS5.Text = "ВЛС   5";
            this.VLS5.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox5
            // 
            this.VLScheckedListBox5.CheckOnClick = true;
            this.VLScheckedListBox5.FormattingEnabled = true;
            this.VLScheckedListBox5.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox5.Name = "VLScheckedListBox5";
            this.VLScheckedListBox5.ScrollAlwaysVisible = true;
            this.VLScheckedListBox5.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox5.TabIndex = 6;
            this.VLScheckedListBox5.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS6
            // 
            this.VLS6.Controls.Add(this.VLScheckedListBox6);
            this.VLS6.Location = new System.Drawing.Point(4, 49);
            this.VLS6.Name = "VLS6";
            this.VLS6.Size = new System.Drawing.Size(382, 468);
            this.VLS6.TabIndex = 5;
            this.VLS6.Text = "ВЛС  6";
            this.VLS6.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox6
            // 
            this.VLScheckedListBox6.CheckOnClick = true;
            this.VLScheckedListBox6.FormattingEnabled = true;
            this.VLScheckedListBox6.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox6.Name = "VLScheckedListBox6";
            this.VLScheckedListBox6.ScrollAlwaysVisible = true;
            this.VLScheckedListBox6.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox6.TabIndex = 6;
            this.VLScheckedListBox6.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS7
            // 
            this.VLS7.Controls.Add(this.VLScheckedListBox7);
            this.VLS7.Location = new System.Drawing.Point(4, 49);
            this.VLS7.Name = "VLS7";
            this.VLS7.Size = new System.Drawing.Size(382, 468);
            this.VLS7.TabIndex = 6;
            this.VLS7.Text = "ВЛС 7";
            this.VLS7.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox7
            // 
            this.VLScheckedListBox7.CheckOnClick = true;
            this.VLScheckedListBox7.FormattingEnabled = true;
            this.VLScheckedListBox7.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox7.Name = "VLScheckedListBox7";
            this.VLScheckedListBox7.ScrollAlwaysVisible = true;
            this.VLScheckedListBox7.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox7.TabIndex = 6;
            this.VLScheckedListBox7.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS8
            // 
            this.VLS8.Controls.Add(this.VLScheckedListBox8);
            this.VLS8.Location = new System.Drawing.Point(4, 49);
            this.VLS8.Name = "VLS8";
            this.VLS8.Size = new System.Drawing.Size(382, 468);
            this.VLS8.TabIndex = 7;
            this.VLS8.Text = "ВЛС   8";
            this.VLS8.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox8
            // 
            this.VLScheckedListBox8.CheckOnClick = true;
            this.VLScheckedListBox8.FormattingEnabled = true;
            this.VLScheckedListBox8.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox8.Name = "VLScheckedListBox8";
            this.VLScheckedListBox8.ScrollAlwaysVisible = true;
            this.VLScheckedListBox8.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox8.TabIndex = 6;
            this.VLScheckedListBox8.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS9
            // 
            this.VLS9.Controls.Add(this.VLScheckedListBox9);
            this.VLS9.Location = new System.Drawing.Point(4, 49);
            this.VLS9.Name = "VLS9";
            this.VLS9.Size = new System.Drawing.Size(382, 468);
            this.VLS9.TabIndex = 8;
            this.VLS9.Text = "ВЛС9";
            this.VLS9.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox9
            // 
            this.VLScheckedListBox9.CheckOnClick = true;
            this.VLScheckedListBox9.FormattingEnabled = true;
            this.VLScheckedListBox9.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox9.Name = "VLScheckedListBox9";
            this.VLScheckedListBox9.ScrollAlwaysVisible = true;
            this.VLScheckedListBox9.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox9.TabIndex = 6;
            this.VLScheckedListBox9.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS10
            // 
            this.VLS10.Controls.Add(this.VLScheckedListBox10);
            this.VLS10.Location = new System.Drawing.Point(4, 49);
            this.VLS10.Name = "VLS10";
            this.VLS10.Size = new System.Drawing.Size(382, 468);
            this.VLS10.TabIndex = 9;
            this.VLS10.Text = "ВЛС10";
            this.VLS10.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox10
            // 
            this.VLScheckedListBox10.CheckOnClick = true;
            this.VLScheckedListBox10.FormattingEnabled = true;
            this.VLScheckedListBox10.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox10.Name = "VLScheckedListBox10";
            this.VLScheckedListBox10.ScrollAlwaysVisible = true;
            this.VLScheckedListBox10.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox10.TabIndex = 6;
            this.VLScheckedListBox10.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS11
            // 
            this.VLS11.Controls.Add(this.VLScheckedListBox11);
            this.VLS11.Location = new System.Drawing.Point(4, 49);
            this.VLS11.Name = "VLS11";
            this.VLS11.Size = new System.Drawing.Size(382, 468);
            this.VLS11.TabIndex = 10;
            this.VLS11.Text = "ВЛС11";
            this.VLS11.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox11
            // 
            this.VLScheckedListBox11.CheckOnClick = true;
            this.VLScheckedListBox11.FormattingEnabled = true;
            this.VLScheckedListBox11.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox11.Name = "VLScheckedListBox11";
            this.VLScheckedListBox11.ScrollAlwaysVisible = true;
            this.VLScheckedListBox11.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox11.TabIndex = 6;
            this.VLScheckedListBox11.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS12
            // 
            this.VLS12.Controls.Add(this.VLScheckedListBox12);
            this.VLS12.Location = new System.Drawing.Point(4, 49);
            this.VLS12.Name = "VLS12";
            this.VLS12.Size = new System.Drawing.Size(382, 468);
            this.VLS12.TabIndex = 11;
            this.VLS12.Text = "ВЛС12";
            this.VLS12.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox12
            // 
            this.VLScheckedListBox12.CheckOnClick = true;
            this.VLScheckedListBox12.FormattingEnabled = true;
            this.VLScheckedListBox12.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox12.Name = "VLScheckedListBox12";
            this.VLScheckedListBox12.ScrollAlwaysVisible = true;
            this.VLScheckedListBox12.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox12.TabIndex = 6;
            this.VLScheckedListBox12.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS13
            // 
            this.VLS13.Controls.Add(this.VLScheckedListBox13);
            this.VLS13.Location = new System.Drawing.Point(4, 49);
            this.VLS13.Name = "VLS13";
            this.VLS13.Size = new System.Drawing.Size(382, 468);
            this.VLS13.TabIndex = 12;
            this.VLS13.Text = "ВЛС13";
            this.VLS13.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox13
            // 
            this.VLScheckedListBox13.CheckOnClick = true;
            this.VLScheckedListBox13.FormattingEnabled = true;
            this.VLScheckedListBox13.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox13.Name = "VLScheckedListBox13";
            this.VLScheckedListBox13.ScrollAlwaysVisible = true;
            this.VLScheckedListBox13.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox13.TabIndex = 6;
            this.VLScheckedListBox13.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS14
            // 
            this.VLS14.Controls.Add(this.VLScheckedListBox14);
            this.VLS14.Location = new System.Drawing.Point(4, 49);
            this.VLS14.Name = "VLS14";
            this.VLS14.Size = new System.Drawing.Size(382, 468);
            this.VLS14.TabIndex = 13;
            this.VLS14.Text = "ВЛС14";
            this.VLS14.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox14
            // 
            this.VLScheckedListBox14.CheckOnClick = true;
            this.VLScheckedListBox14.FormattingEnabled = true;
            this.VLScheckedListBox14.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox14.Name = "VLScheckedListBox14";
            this.VLScheckedListBox14.ScrollAlwaysVisible = true;
            this.VLScheckedListBox14.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox14.TabIndex = 6;
            this.VLScheckedListBox14.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS15
            // 
            this.VLS15.Controls.Add(this.VLScheckedListBox15);
            this.VLS15.Location = new System.Drawing.Point(4, 49);
            this.VLS15.Name = "VLS15";
            this.VLS15.Size = new System.Drawing.Size(382, 468);
            this.VLS15.TabIndex = 14;
            this.VLS15.Text = "ВЛС15";
            this.VLS15.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox15
            // 
            this.VLScheckedListBox15.CheckOnClick = true;
            this.VLScheckedListBox15.FormattingEnabled = true;
            this.VLScheckedListBox15.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox15.Name = "VLScheckedListBox15";
            this.VLScheckedListBox15.ScrollAlwaysVisible = true;
            this.VLScheckedListBox15.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox15.TabIndex = 6;
            this.VLScheckedListBox15.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // VLS16
            // 
            this.VLS16.Controls.Add(this.VLScheckedListBox16);
            this.VLS16.Location = new System.Drawing.Point(4, 49);
            this.VLS16.Name = "VLS16";
            this.VLS16.Size = new System.Drawing.Size(382, 468);
            this.VLS16.TabIndex = 15;
            this.VLS16.Text = "ВЛС16";
            this.VLS16.UseVisualStyleBackColor = true;
            // 
            // VLScheckedListBox16
            // 
            this.VLScheckedListBox16.CheckOnClick = true;
            this.VLScheckedListBox16.FormattingEnabled = true;
            this.VLScheckedListBox16.Location = new System.Drawing.Point(110, 3);
            this.VLScheckedListBox16.Name = "VLScheckedListBox16";
            this.VLScheckedListBox16.ScrollAlwaysVisible = true;
            this.VLScheckedListBox16.Size = new System.Drawing.Size(184, 439);
            this.VLScheckedListBox16.TabIndex = 6;
            this.VLScheckedListBox16.SelectedValueChanged += new System.EventHandler(this.VLScheckedListBox_SelectedValueChanged);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this._outputIndicatorsGrid);
            this.groupBox11.Location = new System.Drawing.Point(8, 198);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(393, 190);
            this.groupBox11.TabIndex = 4;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Индикаторы";
            // 
            // _outputIndicatorsGrid
            // 
            this._outputIndicatorsGrid.AllowUserToAddRows = false;
            this._outputIndicatorsGrid.AllowUserToDeleteRows = false;
            this._outputIndicatorsGrid.AllowUserToResizeColumns = false;
            this._outputIndicatorsGrid.AllowUserToResizeRows = false;
            this._outputIndicatorsGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputIndicatorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputIndicatorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._outIndNumberCol,
            this._outIndTypeCol,
            this._outIndSignalCol,
            this._outIndColorCol});
            this._outputIndicatorsGrid.Location = new System.Drawing.Point(9, 14);
            this._outputIndicatorsGrid.Name = "_outputIndicatorsGrid";
            this._outputIndicatorsGrid.RowHeadersVisible = false;
            this._outputIndicatorsGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle139.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputIndicatorsGrid.RowsDefaultCellStyle = dataGridViewCellStyle139;
            this._outputIndicatorsGrid.RowTemplate.Height = 24;
            this._outputIndicatorsGrid.ShowCellErrors = false;
            this._outputIndicatorsGrid.ShowRowErrors = false;
            this._outputIndicatorsGrid.Size = new System.Drawing.Size(376, 167);
            this._outputIndicatorsGrid.TabIndex = 0;
            // 
            // _outIndNumberCol
            // 
            this._outIndNumberCol.HeaderText = "№";
            this._outIndNumberCol.Name = "_outIndNumberCol";
            this._outIndNumberCol.ReadOnly = true;
            this._outIndNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndNumberCol.Width = 25;
            // 
            // _outIndTypeCol
            // 
            this._outIndTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndTypeCol.HeaderText = "Тип";
            this._outIndTypeCol.Name = "_outIndTypeCol";
            this._outIndTypeCol.Width = 120;
            // 
            // _outIndSignalCol
            // 
            this._outIndSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._outIndSignalCol.HeaderText = "Сигнал";
            this._outIndSignalCol.Name = "_outIndSignalCol";
            this._outIndSignalCol.Width = 140;
            // 
            // _outIndColorCol
            // 
            this._outIndColorCol.HeaderText = "Цвет";
            this._outIndColorCol.Name = "_outIndColorCol";
            this._outIndColorCol.ReadOnly = true;
            this._outIndColorCol.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._outIndColorCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._outIndColorCol.Width = 70;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this._outputReleGrid);
            this.groupBox12.Location = new System.Drawing.Point(8, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(393, 188);
            this.groupBox12.TabIndex = 3;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Выходные реле";
            // 
            // _outputReleGrid
            // 
            this._outputReleGrid.AllowUserToAddRows = false;
            this._outputReleGrid.AllowUserToDeleteRows = false;
            this._outputReleGrid.AllowUserToResizeColumns = false;
            this._outputReleGrid.AllowUserToResizeRows = false;
            this._outputReleGrid.BackgroundColor = System.Drawing.Color.White;
            this._outputReleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._outputReleGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._releNumberCol,
            this._releTypeCol,
            this._releSignalCol,
            this._releWaitCol});
            this._outputReleGrid.Location = new System.Drawing.Point(9, 15);
            this._outputReleGrid.Name = "_outputReleGrid";
            this._outputReleGrid.RowHeadersVisible = false;
            this._outputReleGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle140.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._outputReleGrid.RowsDefaultCellStyle = dataGridViewCellStyle140;
            this._outputReleGrid.RowTemplate.Height = 24;
            this._outputReleGrid.ShowCellErrors = false;
            this._outputReleGrid.ShowRowErrors = false;
            this._outputReleGrid.Size = new System.Drawing.Size(376, 167);
            this._outputReleGrid.TabIndex = 0;
            // 
            // _systemPage
            // 
            this._systemPage.Controls.Add(this.groupBox45);
            this._systemPage.Controls.Add(this.groupBox3);
            this._systemPage.Location = new System.Drawing.Point(4, 22);
            this._systemPage.Name = "_systemPage";
            this._systemPage.Size = new System.Drawing.Size(1021, 553);
            this._systemPage.TabIndex = 8;
            this._systemPage.Text = "Осциллограф";
            this._systemPage.UseVisualStyleBackColor = true;
            // 
            // groupBox45
            // 
            this.groupBox45.Controls.Add(this._oscChannels);
            this.groupBox45.Location = new System.Drawing.Point(281, 3);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(245, 474);
            this.groupBox45.TabIndex = 28;
            this.groupBox45.TabStop = false;
            this.groupBox45.Text = "Программируемые дискретные каналы";
            // 
            // _oscChannels
            // 
            this._oscChannels.AllowUserToAddRows = false;
            this._oscChannels.AllowUserToDeleteRows = false;
            this._oscChannels.AllowUserToResizeColumns = false;
            this._oscChannels.AllowUserToResizeRows = false;
            this._oscChannels.BackgroundColor = System.Drawing.Color.White;
            this._oscChannels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._oscChannels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this._oscSygnal});
            this._oscChannels.Location = new System.Drawing.Point(6, 19);
            this._oscChannels.Name = "_oscChannels";
            this._oscChannels.RowHeadersVisible = false;
            this._oscChannels.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this._oscChannels.RowTemplate.Height = 24;
            this._oscChannels.ShowCellErrors = false;
            this._oscChannels.ShowRowErrors = false;
            this._oscChannels.Size = new System.Drawing.Size(234, 450);
            this._oscChannels.TabIndex = 27;
            // 
            // dataGridViewTextBoxColumn17
            // 
            dataGridViewCellStyle141.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle141.BackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle141.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle141.SelectionBackColor = System.Drawing.Color.Black;
            dataGridViewCellStyle141.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle141;
            this.dataGridViewTextBoxColumn17.Frozen = true;
            this.dataGridViewTextBoxColumn17.HeaderText = "Канал";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 50;
            // 
            // _oscSygnal
            // 
            dataGridViewCellStyle142.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this._oscSygnal.DefaultCellStyle = dataGridViewCellStyle142;
            this._oscSygnal.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._oscSygnal.HeaderText = "Сигнал";
            this._oscSygnal.Name = "_oscSygnal";
            this._oscSygnal.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._oscSygnal.Width = 150;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.oscStartCb);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this._oscSizeTextBox);
            this.groupBox3.Controls.Add(this._oscWriteLength);
            this.groupBox3.Controls.Add(this._oscFix);
            this.groupBox3.Controls.Add(this._oscLength);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Location = new System.Drawing.Point(8, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(267, 122);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Осциллограф";
            // 
            // oscStartCb
            // 
            this.oscStartCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.oscStartCb.FormattingEnabled = true;
            this.oscStartCb.Location = new System.Drawing.Point(134, 91);
            this.oscStartCb.Name = "oscStartCb";
            this.oscStartCb.Size = new System.Drawing.Size(121, 21);
            this.oscStartCb.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Вход пуска осц.";
            // 
            // _oscSizeTextBox
            // 
            this._oscSizeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscSizeTextBox.Location = new System.Drawing.Point(184, 23);
            this._oscSizeTextBox.Name = "_oscSizeTextBox";
            this._oscSizeTextBox.ReadOnly = true;
            this._oscSizeTextBox.Size = new System.Drawing.Size(71, 20);
            this._oscSizeTextBox.TabIndex = 28;
            this._oscSizeTextBox.Tag = "3000000";
            this._oscSizeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscWriteLength
            // 
            this._oscWriteLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._oscWriteLength.Location = new System.Drawing.Point(134, 44);
            this._oscWriteLength.Name = "_oscWriteLength";
            this._oscWriteLength.Size = new System.Drawing.Size(121, 20);
            this._oscWriteLength.TabIndex = 23;
            this._oscWriteLength.Tag = "3000000";
            this._oscWriteLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _oscFix
            // 
            this._oscFix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscFix.FormattingEnabled = true;
            this._oscFix.Location = new System.Drawing.Point(134, 64);
            this._oscFix.Name = "_oscFix";
            this._oscFix.Size = new System.Drawing.Size(121, 21);
            this._oscFix.TabIndex = 13;
            // 
            // _oscLength
            // 
            this._oscLength.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._oscLength.FormattingEnabled = true;
            this._oscLength.Location = new System.Drawing.Point(134, 23);
            this._oscLength.Name = "_oscLength";
            this._oscLength.Size = new System.Drawing.Size(44, 21);
            this._oscLength.TabIndex = 12;
            this._oscLength.SelectedIndexChanged += new System.EventHandler(this._oscLength_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(18, 67);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(66, 13);
            this.label41.TabIndex = 2;
            this.label41.Text = "Фиксац. по";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(18, 46);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Длит. предзаписи, %";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(18, 26);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(66, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Размер, мс";
            // 
            // _automatPage
            // 
            this._automatPage.Controls.Add(this.groupBox31);
            this._automatPage.Controls.Add(this.groupBox30);
            this._automatPage.Controls.Add(this.groupBox29);
            this._automatPage.Controls.Add(this.groupBox32);
            this._automatPage.Controls.Add(this.groupBox33);
            this._automatPage.Location = new System.Drawing.Point(4, 22);
            this._automatPage.Name = "_automatPage";
            this._automatPage.Size = new System.Drawing.Size(1021, 553);
            this._automatPage.TabIndex = 9;
            this._automatPage.Text = "Автоматика и упр.";
            this._automatPage.UseVisualStyleBackColor = true;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label127);
            this.groupBox31.Controls.Add(this._lzhVal);
            this.groupBox31.Controls.Add(this._lzhModes);
            this.groupBox31.Controls.Add(this.label126);
            this.groupBox31.Location = new System.Drawing.Point(8, 207);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(410, 136);
            this.groupBox31.TabIndex = 1;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "ЛЗШ";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 42);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(65, 13);
            this.label127.TabIndex = 11;
            this.label127.Text = "Уставка, Iн";
            // 
            // _lzhVal
            // 
            this._lzhVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._lzhVal.Location = new System.Drawing.Point(95, 39);
            this._lzhVal.Name = "_lzhVal";
            this._lzhVal.Size = new System.Drawing.Size(121, 20);
            this._lzhVal.TabIndex = 10;
            this._lzhVal.Tag = "40";
            this._lzhVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _lzhModes
            // 
            this._lzhModes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._lzhModes.FormattingEnabled = true;
            this._lzhModes.Location = new System.Drawing.Point(95, 19);
            this._lzhModes.Name = "_lzhModes";
            this._lzhModes.Size = new System.Drawing.Size(121, 21);
            this._lzhModes.TabIndex = 3;
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 22);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(42, 13);
            this.label126.TabIndex = 2;
            this.label126.Text = "Режим";
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label18);
            this.groupBox30.Controls.Add(this.label125);
            this.groupBox30.Controls.Add(this._avrClear);
            this.groupBox30.Controls.Add(this.label124);
            this.groupBox30.Controls.Add(this._avrTOff);
            this.groupBox30.Controls.Add(this.label123);
            this.groupBox30.Controls.Add(this._avrTBack);
            this.groupBox30.Controls.Add(this.label122);
            this.groupBox30.Controls.Add(this._avrBack);
            this.groupBox30.Controls.Add(this.label121);
            this.groupBox30.Controls.Add(this._avrTSr);
            this.groupBox30.Controls.Add(this.label120);
            this.groupBox30.Controls.Add(this._avrResolve);
            this.groupBox30.Controls.Add(this.label119);
            this.groupBox30.Controls.Add(this._avrBlockClear);
            this.groupBox30.Controls.Add(this.label118);
            this.groupBox30.Controls.Add(this._avrBlocking);
            this.groupBox30.Controls.Add(this.label117);
            this.groupBox30.Controls.Add(this._avrSIGNOn);
            this.groupBox30.Controls.Add(this.label116);
            this.groupBox30.Controls.Add(this._avrByDiff);
            this.groupBox30.Controls.Add(this.label115);
            this.groupBox30.Controls.Add(this._avrBySelfOff);
            this.groupBox30.Controls.Add(this.label114);
            this.groupBox30.Controls.Add(this._avrByOff);
            this.groupBox30.Controls.Add(this.label113);
            this.groupBox30.Controls.Add(this._avrBySignal);
            this.groupBox30.Location = new System.Drawing.Point(424, 174);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(397, 185);
            this.groupBox30.TabIndex = 0;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "АВР";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "(по питанию)";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(202, 118);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(38, 13);
            this.label125.TabIndex = 25;
            this.label125.Text = "Сброс";
            // 
            // _avrClear
            // 
            this._avrClear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrClear.FormattingEnabled = true;
            this._avrClear.Location = new System.Drawing.Point(279, 116);
            this._avrClear.Name = "_avrClear";
            this._avrClear.Size = new System.Drawing.Size(113, 21);
            this._avrClear.TabIndex = 24;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(202, 99);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(56, 13);
            this.label124.TabIndex = 23;
            this.label124.Text = "t откл, мс";
            // 
            // _avrTOff
            // 
            this._avrTOff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTOff.Location = new System.Drawing.Point(279, 96);
            this._avrTOff.Name = "_avrTOff";
            this._avrTOff.Size = new System.Drawing.Size(113, 20);
            this._avrTOff.TabIndex = 22;
            this._avrTOff.Tag = "3276700";
            this._avrTOff.Text = "0";
            this._avrTOff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(202, 80);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(51, 13);
            this.label123.TabIndex = 21;
            this.label123.Text = "t воз, мс";
            // 
            // _avrTBack
            // 
            this._avrTBack.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTBack.Location = new System.Drawing.Point(279, 77);
            this._avrTBack.Name = "_avrTBack";
            this._avrTBack.Size = new System.Drawing.Size(113, 20);
            this._avrTBack.TabIndex = 20;
            this._avrTBack.Tag = "3276700";
            this._avrTBack.Text = "0";
            this._avrTBack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(202, 60);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(49, 13);
            this.label122.TabIndex = 19;
            this.label122.Text = "Возврат";
            // 
            // _avrBack
            // 
            this._avrBack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBack.FormattingEnabled = true;
            this._avrBack.Location = new System.Drawing.Point(279, 58);
            this._avrBack.Name = "_avrBack";
            this._avrBack.Size = new System.Drawing.Size(113, 21);
            this._avrBack.TabIndex = 18;
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(202, 41);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(45, 13);
            this.label121.TabIndex = 17;
            this.label121.Text = "t ср, мс";
            // 
            // _avrTSr
            // 
            this._avrTSr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._avrTSr.Location = new System.Drawing.Point(279, 38);
            this._avrTSr.Name = "_avrTSr";
            this._avrTSr.Size = new System.Drawing.Size(113, 20);
            this._avrTSr.TabIndex = 16;
            this._avrTSr.Tag = "3276700";
            this._avrTSr.Text = "0";
            this._avrTSr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(202, 21);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(72, 13);
            this.label120.TabIndex = 15;
            this.label120.Text = "АВР разреш.";
            // 
            // _avrResolve
            // 
            this._avrResolve.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrResolve.FormattingEnabled = true;
            this._avrResolve.Location = new System.Drawing.Point(279, 18);
            this._avrResolve.Name = "_avrResolve";
            this._avrResolve.Size = new System.Drawing.Size(113, 21);
            this._avrResolve.TabIndex = 14;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(6, 158);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(38, 13);
            this.label119.TabIndex = 13;
            this.label119.Text = "Сброс";
            // 
            // _avrBlockClear
            // 
            this._avrBlockClear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBlockClear.FormattingEnabled = true;
            this._avrBlockClear.Location = new System.Drawing.Point(83, 155);
            this._avrBlockClear.Name = "_avrBlockClear";
            this._avrBlockClear.Size = new System.Drawing.Size(113, 21);
            this._avrBlockClear.TabIndex = 12;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(6, 138);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(47, 13);
            this.label118.TabIndex = 11;
            this.label118.Text = "Блок-ка";
            // 
            // _avrBlocking
            // 
            this._avrBlocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBlocking.FormattingEnabled = true;
            this._avrBlocking.Location = new System.Drawing.Point(83, 135);
            this._avrBlocking.Name = "_avrBlocking";
            this._avrBlocking.Size = new System.Drawing.Size(113, 21);
            this._avrBlocking.TabIndex = 10;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(6, 118);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(62, 13);
            this.label117.TabIndex = 9;
            this.label117.Text = "СИГН пуск";
            // 
            // _avrSIGNOn
            // 
            this._avrSIGNOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrSIGNOn.FormattingEnabled = true;
            this._avrSIGNOn.Location = new System.Drawing.Point(83, 115);
            this._avrSIGNOn.Name = "_avrSIGNOn";
            this._avrSIGNOn.Size = new System.Drawing.Size(113, 21);
            this._avrSIGNOn.TabIndex = 8;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(6, 98);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(62, 13);
            this.label116.TabIndex = 7;
            this.label116.Text = "По защите";
            // 
            // _avrByDiff
            // 
            this._avrByDiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByDiff.FormattingEnabled = true;
            this._avrByDiff.Location = new System.Drawing.Point(83, 95);
            this._avrByDiff.Name = "_avrByDiff";
            this._avrByDiff.Size = new System.Drawing.Size(113, 21);
            this._avrByDiff.TabIndex = 6;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(6, 78);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(76, 13);
            this.label115.TabIndex = 5;
            this.label115.Text = "По самооткл.";
            // 
            // _avrBySelfOff
            // 
            this._avrBySelfOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySelfOff.FormattingEnabled = true;
            this._avrBySelfOff.Location = new System.Drawing.Point(83, 75);
            this._avrBySelfOff.Name = "_avrBySelfOff";
            this._avrBySelfOff.Size = new System.Drawing.Size(113, 21);
            this._avrBySelfOff.TabIndex = 4;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(6, 58);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(63, 13);
            this.label114.TabIndex = 3;
            this.label114.Text = "По отключ.";
            // 
            // _avrByOff
            // 
            this._avrByOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrByOff.FormattingEnabled = true;
            this._avrByOff.Location = new System.Drawing.Point(83, 55);
            this._avrByOff.Name = "_avrByOff";
            this._avrByOff.Size = new System.Drawing.Size(113, 21);
            this._avrByOff.TabIndex = 2;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 21);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(64, 13);
            this.label113.TabIndex = 1;
            this.label113.Text = "От сигнала";
            // 
            // _avrBySignal
            // 
            this._avrBySignal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._avrBySignal.FormattingEnabled = true;
            this._avrBySignal.Location = new System.Drawing.Point(83, 25);
            this._avrBySignal.Name = "_avrBySignal";
            this._avrBySignal.Size = new System.Drawing.Size(113, 21);
            this._avrBySignal.TabIndex = 0;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.label135);
            this.groupBox29.Controls.Add(this.label136);
            this.groupBox29.Controls.Add(this._apv4Krat);
            this.groupBox29.Controls.Add(this._apv3Krat);
            this.groupBox29.Controls.Add(this.label112);
            this.groupBox29.Controls.Add(this.label111);
            this.groupBox29.Controls.Add(this.label110);
            this.groupBox29.Controls.Add(this.label109);
            this.groupBox29.Controls.Add(this.label96);
            this.groupBox29.Controls.Add(this._apvOff);
            this.groupBox29.Controls.Add(this._apv2Krat);
            this.groupBox29.Controls.Add(this._apv1Krat);
            this.groupBox29.Controls.Add(this._apvTReady);
            this.groupBox29.Controls.Add(this._apvTBlock);
            this.groupBox29.Controls.Add(this._apvBlocking);
            this.groupBox29.Controls.Add(this.label95);
            this.groupBox29.Controls.Add(this._apvModes);
            this.groupBox29.Controls.Add(this.label94);
            this.groupBox29.Location = new System.Drawing.Point(424, 3);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(396, 167);
            this.groupBox29.TabIndex = 0;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "АПВ";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(220, 119);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(60, 13);
            this.label135.TabIndex = 17;
            this.label135.Text = "4 Крат, мс";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(220, 100);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(60, 13);
            this.label136.TabIndex = 16;
            this.label136.Text = "3 Крат, мс";
            // 
            // _apv4Krat
            // 
            this._apv4Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv4Krat.Location = new System.Drawing.Point(308, 116);
            this._apv4Krat.Name = "_apv4Krat";
            this._apv4Krat.Size = new System.Drawing.Size(82, 20);
            this._apv4Krat.TabIndex = 15;
            this._apv4Krat.Tag = "3276700";
            this._apv4Krat.Text = "0";
            this._apv4Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apv3Krat
            // 
            this._apv3Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv3Krat.Location = new System.Drawing.Point(308, 97);
            this._apv3Krat.Name = "_apv3Krat";
            this._apv3Krat.Size = new System.Drawing.Size(82, 20);
            this._apv3Krat.TabIndex = 14;
            this._apv3Krat.Tag = "3276700";
            this._apv3Krat.Text = "0";
            this._apv3Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 138);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(73, 13);
            this.label112.TabIndex = 13;
            this.label112.Text = "Самоотключ.";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 119);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(60, 13);
            this.label111.TabIndex = 12;
            this.label111.Text = "2 Крат, мс";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 100);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(60, 13);
            this.label110.TabIndex = 11;
            this.label110.Text = "1 Крат, мс";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(6, 81);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(64, 13);
            this.label109.TabIndex = 10;
            this.label109.Text = "t готов., мс";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 62);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(60, 13);
            this.label96.TabIndex = 9;
            this.label96.Text = "t блок., мс";
            // 
            // _apvOff
            // 
            this._apvOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvOff.FormattingEnabled = true;
            this._apvOff.Location = new System.Drawing.Point(95, 136);
            this._apvOff.Name = "_apvOff";
            this._apvOff.Size = new System.Drawing.Size(121, 21);
            this._apvOff.TabIndex = 8;
            // 
            // _apv2Krat
            // 
            this._apv2Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv2Krat.Location = new System.Drawing.Point(95, 116);
            this._apv2Krat.Name = "_apv2Krat";
            this._apv2Krat.Size = new System.Drawing.Size(82, 20);
            this._apv2Krat.TabIndex = 7;
            this._apv2Krat.Tag = "3276700";
            this._apv2Krat.Text = "0";
            this._apv2Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apv1Krat
            // 
            this._apv1Krat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apv1Krat.Location = new System.Drawing.Point(95, 97);
            this._apv1Krat.Name = "_apv1Krat";
            this._apv1Krat.Size = new System.Drawing.Size(82, 20);
            this._apv1Krat.TabIndex = 6;
            this._apv1Krat.Tag = "3276700";
            this._apv1Krat.Text = "0";
            this._apv1Krat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTReady
            // 
            this._apvTReady.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTReady.Location = new System.Drawing.Point(95, 78);
            this._apvTReady.Name = "_apvTReady";
            this._apvTReady.Size = new System.Drawing.Size(121, 20);
            this._apvTReady.TabIndex = 5;
            this._apvTReady.Tag = "3276700";
            this._apvTReady.Text = "0";
            this._apvTReady.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvTBlock
            // 
            this._apvTBlock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._apvTBlock.Location = new System.Drawing.Point(95, 59);
            this._apvTBlock.Name = "_apvTBlock";
            this._apvTBlock.Size = new System.Drawing.Size(121, 20);
            this._apvTBlock.TabIndex = 4;
            this._apvTBlock.Tag = "3276700";
            this._apvTBlock.Text = "0";
            this._apvTBlock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _apvBlocking
            // 
            this._apvBlocking.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvBlocking.FormattingEnabled = true;
            this._apvBlocking.Location = new System.Drawing.Point(95, 39);
            this._apvBlocking.Name = "_apvBlocking";
            this._apvBlocking.Size = new System.Drawing.Size(121, 21);
            this._apvBlocking.TabIndex = 3;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(6, 42);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(68, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Блокировка";
            // 
            // _apvModes
            // 
            this._apvModes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._apvModes.FormattingEnabled = true;
            this._apvModes.Location = new System.Drawing.Point(95, 19);
            this._apvModes.Name = "_apvModes";
            this._apvModes.Size = new System.Drawing.Size(121, 21);
            this._apvModes.TabIndex = 1;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(6, 22);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(42, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "Режим";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this._switchKontCep);
            this.groupBox32.Controls.Add(this._switchTUskor);
            this.groupBox32.Controls.Add(this._switchImp);
            this.groupBox32.Controls.Add(this._switchIUrov);
            this.groupBox32.Controls.Add(this._switchTUrov);
            this.groupBox32.Controls.Add(this._switchBlock);
            this.groupBox32.Controls.Add(this._switchError);
            this.groupBox32.Controls.Add(this._switchOn);
            this.groupBox32.Controls.Add(this._switchOff);
            this.groupBox32.Controls.Add(this.label97);
            this.groupBox32.Controls.Add(this.label98);
            this.groupBox32.Controls.Add(this.label99);
            this.groupBox32.Controls.Add(this.label91);
            this.groupBox32.Controls.Add(this.label92);
            this.groupBox32.Controls.Add(this.label93);
            this.groupBox32.Controls.Add(this.label90);
            this.groupBox32.Controls.Add(this.label89);
            this.groupBox32.Controls.Add(this.label88);
            this.groupBox32.Location = new System.Drawing.Point(8, 3);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(204, 197);
            this.groupBox32.TabIndex = 14;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "Выключатель";
            // 
            // _switchKontCep
            // 
            this._switchKontCep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKontCep.FormattingEnabled = true;
            this._switchKontCep.Location = new System.Drawing.Point(93, 171);
            this._switchKontCep.Name = "_switchKontCep";
            this._switchKontCep.Size = new System.Drawing.Size(105, 21);
            this._switchKontCep.TabIndex = 26;
            // 
            // _switchTUskor
            // 
            this._switchTUskor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUskor.Location = new System.Drawing.Point(93, 151);
            this._switchTUskor.Name = "_switchTUskor";
            this._switchTUskor.Size = new System.Drawing.Size(105, 20);
            this._switchTUskor.TabIndex = 25;
            this._switchTUskor.Tag = "3276700";
            this._switchTUskor.Text = "0";
            this._switchTUskor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchImp
            // 
            this._switchImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchImp.Location = new System.Drawing.Point(93, 132);
            this._switchImp.Name = "_switchImp";
            this._switchImp.Size = new System.Drawing.Size(105, 20);
            this._switchImp.TabIndex = 24;
            this._switchImp.Tag = "3276700";
            this._switchImp.Text = "0";
            this._switchImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchIUrov
            // 
            this._switchIUrov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchIUrov.Location = new System.Drawing.Point(93, 113);
            this._switchIUrov.Name = "_switchIUrov";
            this._switchIUrov.Size = new System.Drawing.Size(105, 20);
            this._switchIUrov.TabIndex = 23;
            this._switchIUrov.Tag = "40";
            this._switchIUrov.Text = "0";
            this._switchIUrov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchTUrov
            // 
            this._switchTUrov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._switchTUrov.Location = new System.Drawing.Point(93, 94);
            this._switchTUrov.Name = "_switchTUrov";
            this._switchTUrov.Size = new System.Drawing.Size(105, 20);
            this._switchTUrov.TabIndex = 22;
            this._switchTUrov.Tag = "3276700";
            this._switchTUrov.Text = "0";
            this._switchTUrov.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _switchBlock
            // 
            this._switchBlock.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchBlock.FormattingEnabled = true;
            this._switchBlock.Location = new System.Drawing.Point(93, 75);
            this._switchBlock.Name = "_switchBlock";
            this._switchBlock.Size = new System.Drawing.Size(105, 21);
            this._switchBlock.TabIndex = 21;
            // 
            // _switchError
            // 
            this._switchError.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchError.FormattingEnabled = true;
            this._switchError.Location = new System.Drawing.Point(93, 55);
            this._switchError.Name = "_switchError";
            this._switchError.Size = new System.Drawing.Size(105, 21);
            this._switchError.TabIndex = 20;
            // 
            // _switchOn
            // 
            this._switchOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchOn.FormattingEnabled = true;
            this._switchOn.Location = new System.Drawing.Point(93, 35);
            this._switchOn.Name = "_switchOn";
            this._switchOn.Size = new System.Drawing.Size(105, 21);
            this._switchOn.TabIndex = 19;
            // 
            // _switchOff
            // 
            this._switchOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchOff.FormattingEnabled = true;
            this._switchOff.Location = new System.Drawing.Point(93, 15);
            this._switchOff.Name = "_switchOff";
            this._switchOff.Size = new System.Drawing.Size(105, 21);
            this._switchOff.TabIndex = 18;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(12, 174);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(58, 13);
            this.label97.TabIndex = 17;
            this.label97.Text = "Конт. цеп.";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(12, 154);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(65, 13);
            this.label98.TabIndex = 16;
            this.label98.Text = "t ускор., мс";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(12, 135);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(72, 13);
            this.label99.TabIndex = 15;
            this.label99.Text = "Импульс, мс";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(12, 116);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(54, 13);
            this.label91.TabIndex = 14;
            this.label91.Text = "I уров., Iн";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(12, 97);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(59, 13);
            this.label92.TabIndex = 13;
            this.label92.Text = "t уров., мс";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(12, 78);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(50, 13);
            this.label93.TabIndex = 12;
            this.label93.Text = "Блок-ка.";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(12, 58);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(48, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Неиспр.";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(12, 38);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(42, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Включ.";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(12, 18);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Отключ.";
            // 
            // groupBox33
            // 
            this.groupBox33.Controls.Add(this._switchSDTU);
            this.groupBox33.Controls.Add(this._switchVnesh);
            this.groupBox33.Controls.Add(this._switchKey);
            this.groupBox33.Controls.Add(this._switchButtons);
            this.groupBox33.Controls.Add(this._switchVneshOff);
            this.groupBox33.Controls.Add(this._switchVneshOn);
            this.groupBox33.Controls.Add(this._switchKeyOff);
            this.groupBox33.Controls.Add(this._switchKeyOn);
            this.groupBox33.Controls.Add(this.label101);
            this.groupBox33.Controls.Add(this.label102);
            this.groupBox33.Controls.Add(this.label103);
            this.groupBox33.Controls.Add(this.label104);
            this.groupBox33.Controls.Add(this.label105);
            this.groupBox33.Controls.Add(this.label106);
            this.groupBox33.Controls.Add(this.label107);
            this.groupBox33.Controls.Add(this.label108);
            this.groupBox33.Location = new System.Drawing.Point(218, 3);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(200, 197);
            this.groupBox33.TabIndex = 15;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "Управление";
            // 
            // _switchSDTU
            // 
            this._switchSDTU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchSDTU.FormattingEnabled = true;
            this._switchSDTU.Location = new System.Drawing.Point(89, 154);
            this._switchSDTU.Name = "_switchSDTU";
            this._switchSDTU.Size = new System.Drawing.Size(105, 21);
            this._switchSDTU.TabIndex = 34;
            // 
            // _switchVnesh
            // 
            this._switchVnesh.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVnesh.FormattingEnabled = true;
            this._switchVnesh.Location = new System.Drawing.Point(89, 134);
            this._switchVnesh.Name = "_switchVnesh";
            this._switchVnesh.Size = new System.Drawing.Size(105, 21);
            this._switchVnesh.TabIndex = 33;
            // 
            // _switchKey
            // 
            this._switchKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKey.FormattingEnabled = true;
            this._switchKey.Location = new System.Drawing.Point(89, 115);
            this._switchKey.Name = "_switchKey";
            this._switchKey.Size = new System.Drawing.Size(105, 21);
            this._switchKey.TabIndex = 32;
            // 
            // _switchButtons
            // 
            this._switchButtons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchButtons.FormattingEnabled = true;
            this._switchButtons.Location = new System.Drawing.Point(89, 95);
            this._switchButtons.Name = "_switchButtons";
            this._switchButtons.Size = new System.Drawing.Size(105, 21);
            this._switchButtons.TabIndex = 31;
            // 
            // _switchVneshOff
            // 
            this._switchVneshOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVneshOff.FormattingEnabled = true;
            this._switchVneshOff.Location = new System.Drawing.Point(89, 75);
            this._switchVneshOff.Name = "_switchVneshOff";
            this._switchVneshOff.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOff.TabIndex = 30;
            // 
            // _switchVneshOn
            // 
            this._switchVneshOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchVneshOn.FormattingEnabled = true;
            this._switchVneshOn.Location = new System.Drawing.Point(89, 55);
            this._switchVneshOn.Name = "_switchVneshOn";
            this._switchVneshOn.Size = new System.Drawing.Size(105, 21);
            this._switchVneshOn.TabIndex = 29;
            // 
            // _switchKeyOff
            // 
            this._switchKeyOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKeyOff.FormattingEnabled = true;
            this._switchKeyOff.Location = new System.Drawing.Point(89, 35);
            this._switchKeyOff.Name = "_switchKeyOff";
            this._switchKeyOff.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOff.TabIndex = 28;
            // 
            // _switchKeyOn
            // 
            this._switchKeyOn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._switchKeyOn.FormattingEnabled = true;
            this._switchKeyOn.Location = new System.Drawing.Point(89, 15);
            this._switchKeyOn.Name = "_switchKeyOn";
            this._switchKeyOn.Size = new System.Drawing.Size(105, 21);
            this._switchKeyOn.TabIndex = 27;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 157);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(38, 13);
            this.label101.TabIndex = 25;
            this.label101.Text = "СДТУ";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 137);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 24;
            this.label102.Text = "Внешнее";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(6, 118);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(33, 13);
            this.label103.TabIndex = 23;
            this.label103.Text = "Ключ";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 98);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(44, 13);
            this.label104.TabIndex = 22;
            this.label104.Text = "Кнопки";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(66, 13);
            this.label105.TabIndex = 21;
            this.label105.Text = "Внеш. откл.";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(6, 58);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(61, 13);
            this.label106.TabIndex = 20;
            this.label106.Text = "Внеш. вкл.";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(6, 38);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(62, 13);
            this.label107.TabIndex = 19;
            this.label107.Text = "Ключ откл.";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 18);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(57, 13);
            this.label108.TabIndex = 18;
            this.label108.Text = "Ключ вкл.";
            // 
            // tabPage29
            // 
            this.tabPage29.Controls.Add(this.groupBox39);
            this.tabPage29.Controls.Add(this.groupBox35);
            this.tabPage29.Controls.Add(this.groupBox34);
            this.tabPage29.Location = new System.Drawing.Point(4, 22);
            this.tabPage29.Name = "tabPage29";
            this.tabPage29.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage29.Size = new System.Drawing.Size(1021, 553);
            this.tabPage29.TabIndex = 11;
            this.tabPage29.Text = "Контроль синхронизма";
            this.tabPage29.UseVisualStyleBackColor = true;
            // 
            // groupBox39
            // 
            this.groupBox39.Controls.Add(this.groupBox40);
            this.groupBox39.Controls.Add(this.groupBox41);
            this.groupBox39.Controls.Add(this.groupBox42);
            this.groupBox39.Controls.Add(this._sinhrAutoUmax);
            this.groupBox39.Controls.Add(this._blockSinhCmb);
            this.groupBox39.Controls.Add(this._sinhrAutoMode);
            this.groupBox39.Controls.Add(this._blockSinhLabel);
            this.groupBox39.Controls.Add(this._dUmaxAutoLabel);
            this.groupBox39.Controls.Add(this.label75);
            this.groupBox39.Location = new System.Drawing.Point(477, 6);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(335, 288);
            this.groupBox39.TabIndex = 37;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Уставки автоматического включения";
            // 
            // groupBox40
            // 
            this.groupBox40.Controls.Add(this._sinhrAutodFno);
            this.groupBox40.Controls.Add(this.label17);
            this.groupBox40.Location = new System.Drawing.Point(6, 241);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(246, 45);
            this.groupBox40.TabIndex = 33;
            this.groupBox40.TabStop = false;
            this.groupBox40.Text = "Улавливание синхронизма(несинхр. режим)";
            // 
            // _sinhrAutodFno
            // 
            this._sinhrAutodFno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFno.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodFno.Name = "_sinhrAutodFno";
            this._sinhrAutodFno.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFno.TabIndex = 23;
            this._sinhrAutodFno.Tag = "40";
            this._sinhrAutodFno.Text = "0";
            this._sinhrAutodFno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(43, 21);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(37, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "dF, Гц";
            // 
            // groupBox41
            // 
            this.groupBox41.Controls.Add(this._sinhrAutodF);
            this.groupBox41.Controls.Add(this.label61);
            this.groupBox41.Controls.Add(this.label62);
            this.groupBox41.Controls.Add(this._sinhrAutodFi);
            this.groupBox41.Location = new System.Drawing.Point(6, 171);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(246, 64);
            this.groupBox41.TabIndex = 32;
            this.groupBox41.TabStop = false;
            this.groupBox41.Text = "Ожидание синхронизма(синхр. режим)";
            // 
            // _sinhrAutodF
            // 
            this._sinhrAutodF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodF.Location = new System.Drawing.Point(99, 19);
            this._sinhrAutodF.Name = "_sinhrAutodF";
            this._sinhrAutodF.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodF.TabIndex = 23;
            this._sinhrAutodF.Tag = "40";
            this._sinhrAutodF.Text = "0";
            this._sinhrAutodF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(43, 21);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(37, 13);
            this.label61.TabIndex = 12;
            this.label61.Text = "dF, Гц";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(43, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(50, 13);
            this.label62.TabIndex = 13;
            this.label62.Text = "dFi, град";
            // 
            // _sinhrAutodFi
            // 
            this._sinhrAutodFi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutodFi.Location = new System.Drawing.Point(99, 38);
            this._sinhrAutodFi.Name = "_sinhrAutodFi";
            this._sinhrAutodFi.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutodFi.TabIndex = 24;
            this._sinhrAutodFi.Tag = "3276700";
            this._sinhrAutodFi.Text = "0";
            this._sinhrAutodFi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox42
            // 
            this.groupBox42.Controls.Add(this._sinhrAutoNoNo);
            this.groupBox42.Controls.Add(this._discretIn2Cmb);
            this.groupBox42.Controls.Add(this._sinhrAutoYesNo);
            this.groupBox42.Controls.Add(this._discretIn1Cmb);
            this.groupBox42.Controls.Add(this._sinhrAutoNoYes);
            this.groupBox42.Controls.Add(this.label66);
            this.groupBox42.Controls.Add(this.label72);
            this.groupBox42.Controls.Add(this.label73);
            this.groupBox42.Location = new System.Drawing.Point(6, 83);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(323, 82);
            this.groupBox42.TabIndex = 31;
            this.groupBox42.TabStop = false;
            this.groupBox42.Text = "Разрешение включения";
            // 
            // _sinhrAutoNoNo
            // 
            this._sinhrAutoNoNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoNo.FormattingEnabled = true;
            this._sinhrAutoNoNo.Location = new System.Drawing.Point(99, 54);
            this._sinhrAutoNoNo.Name = "_sinhrAutoNoNo";
            this._sinhrAutoNoNo.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoNo.TabIndex = 35;
            // 
            // _discretIn2Cmb
            // 
            this._discretIn2Cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._discretIn2Cmb.FormattingEnabled = true;
            this._discretIn2Cmb.Location = new System.Drawing.Point(210, 34);
            this._discretIn2Cmb.Name = "_discretIn2Cmb";
            this._discretIn2Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn2Cmb.TabIndex = 34;
            // 
            // _sinhrAutoYesNo
            // 
            this._sinhrAutoYesNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoYesNo.FormattingEnabled = true;
            this._sinhrAutoYesNo.Location = new System.Drawing.Point(99, 34);
            this._sinhrAutoYesNo.Name = "_sinhrAutoYesNo";
            this._sinhrAutoYesNo.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoYesNo.TabIndex = 34;
            // 
            // _discretIn1Cmb
            // 
            this._discretIn1Cmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._discretIn1Cmb.FormattingEnabled = true;
            this._discretIn1Cmb.Location = new System.Drawing.Point(210, 14);
            this._discretIn1Cmb.Name = "_discretIn1Cmb";
            this._discretIn1Cmb.Size = new System.Drawing.Size(105, 21);
            this._discretIn1Cmb.TabIndex = 33;
            // 
            // _sinhrAutoNoYes
            // 
            this._sinhrAutoNoYes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoNoYes.FormattingEnabled = true;
            this._sinhrAutoNoYes.Location = new System.Drawing.Point(99, 14);
            this._sinhrAutoNoYes.Name = "_sinhrAutoNoYes";
            this._sinhrAutoNoYes.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoNoYes.TabIndex = 33;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 57);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(81, 13);
            this.label66.TabIndex = 32;
            this.label66.Text = "U1 нет, U2 нет";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(6, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(87, 13);
            this.label72.TabIndex = 31;
            this.label72.Text = "U1 есть, U2 нет";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 17);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(87, 13);
            this.label73.TabIndex = 30;
            this.label73.Text = "U1 нет, U2 есть";
            // 
            // _sinhrAutoUmax
            // 
            this._sinhrAutoUmax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrAutoUmax.Location = new System.Drawing.Point(89, 57);
            this._sinhrAutoUmax.Name = "_sinhrAutoUmax";
            this._sinhrAutoUmax.Size = new System.Drawing.Size(105, 20);
            this._sinhrAutoUmax.TabIndex = 22;
            this._sinhrAutoUmax.Tag = "3276700";
            this._sinhrAutoUmax.Text = "0";
            this._sinhrAutoUmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _blockSinhCmb
            // 
            this._blockSinhCmb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._blockSinhCmb.FormattingEnabled = true;
            this._blockSinhCmb.Location = new System.Drawing.Point(89, 36);
            this._blockSinhCmb.Name = "_blockSinhCmb";
            this._blockSinhCmb.Size = new System.Drawing.Size(105, 21);
            this._blockSinhCmb.TabIndex = 18;
            // 
            // _sinhrAutoMode
            // 
            this._sinhrAutoMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrAutoMode.FormattingEnabled = true;
            this._sinhrAutoMode.Location = new System.Drawing.Point(89, 16);
            this._sinhrAutoMode.Name = "_sinhrAutoMode";
            this._sinhrAutoMode.Size = new System.Drawing.Size(105, 21);
            this._sinhrAutoMode.TabIndex = 18;
            // 
            // _blockSinhLabel
            // 
            this._blockSinhLabel.AutoSize = true;
            this._blockSinhLabel.Location = new System.Drawing.Point(12, 38);
            this._blockSinhLabel.Name = "_blockSinhLabel";
            this._blockSinhLabel.Size = new System.Drawing.Size(68, 13);
            this._blockSinhLabel.TabIndex = 9;
            this._blockSinhLabel.Text = "Блокировка";
            // 
            // _dUmaxAutoLabel
            // 
            this._dUmaxAutoLabel.AutoSize = true;
            this._dUmaxAutoLabel.Location = new System.Drawing.Point(12, 58);
            this._dUmaxAutoLabel.Name = "_dUmaxAutoLabel";
            this._dUmaxAutoLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxAutoLabel.TabIndex = 11;
            this._dUmaxAutoLabel.Text = "dUmax., В";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(12, 18);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Режим";
            // 
            // groupBox35
            // 
            this.groupBox35.Controls.Add(this.groupBox38);
            this.groupBox35.Controls.Add(this.groupBox37);
            this.groupBox35.Controls.Add(this.groupBox36);
            this.groupBox35.Controls.Add(this._sinhrManualUmax);
            this.groupBox35.Controls.Add(this._sinhrManualMode);
            this.groupBox35.Controls.Add(this._dUmaxManualLabel);
            this.groupBox35.Controls.Add(this.label67);
            this.groupBox35.Location = new System.Drawing.Point(212, 6);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(259, 268);
            this.groupBox35.TabIndex = 36;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "Уставки ручного включения";
            // 
            // groupBox38
            // 
            this.groupBox38.Controls.Add(this._sinhrManualdFno);
            this.groupBox38.Controls.Add(this.label71);
            this.groupBox38.Location = new System.Drawing.Point(6, 221);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(246, 45);
            this.groupBox38.TabIndex = 33;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "Улавливание синхронизма(несинхр. режим)";
            // 
            // _sinhrManualdFno
            // 
            this._sinhrManualdFno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFno.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdFno.Name = "_sinhrManualdFno";
            this._sinhrManualdFno.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFno.TabIndex = 23;
            this._sinhrManualdFno.Tag = "40";
            this._sinhrManualdFno.Text = "0";
            this._sinhrManualdFno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(43, 21);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(37, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "dF, Гц";
            // 
            // groupBox37
            // 
            this.groupBox37.Controls.Add(this._sinhrManualdF);
            this.groupBox37.Controls.Add(this.label64);
            this.groupBox37.Controls.Add(this.label63);
            this.groupBox37.Controls.Add(this._sinhrManualdFi);
            this.groupBox37.Location = new System.Drawing.Point(6, 151);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(246, 64);
            this.groupBox37.TabIndex = 32;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "Ожидание синхронизма(синхр. режим)";
            // 
            // _sinhrManualdF
            // 
            this._sinhrManualdF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdF.Location = new System.Drawing.Point(99, 19);
            this._sinhrManualdF.Name = "_sinhrManualdF";
            this._sinhrManualdF.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdF.TabIndex = 23;
            this._sinhrManualdF.Tag = "40";
            this._sinhrManualdF.Text = "0";
            this._sinhrManualdF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(43, 21);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(37, 13);
            this.label64.TabIndex = 12;
            this.label64.Text = "dF, Гц";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(43, 40);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(50, 13);
            this.label63.TabIndex = 13;
            this.label63.Text = "dFi, град";
            // 
            // _sinhrManualdFi
            // 
            this._sinhrManualdFi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualdFi.Location = new System.Drawing.Point(99, 38);
            this._sinhrManualdFi.Name = "_sinhrManualdFi";
            this._sinhrManualdFi.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualdFi.TabIndex = 24;
            this._sinhrManualdFi.Tag = "3276700";
            this._sinhrManualdFi.Text = "0";
            this._sinhrManualdFi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox36
            // 
            this.groupBox36.Controls.Add(this._sinhrManualNoNo);
            this.groupBox36.Controls.Add(this._sinhrManualYesNo);
            this.groupBox36.Controls.Add(this._sinhrManualNoYes);
            this.groupBox36.Controls.Add(this.label68);
            this.groupBox36.Controls.Add(this.label69);
            this.groupBox36.Controls.Add(this.label70);
            this.groupBox36.Location = new System.Drawing.Point(6, 63);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(246, 82);
            this.groupBox36.TabIndex = 31;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "Разрешение включения";
            // 
            // _sinhrManualNoNo
            // 
            this._sinhrManualNoNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoNo.FormattingEnabled = true;
            this._sinhrManualNoNo.Location = new System.Drawing.Point(99, 54);
            this._sinhrManualNoNo.Name = "_sinhrManualNoNo";
            this._sinhrManualNoNo.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoNo.TabIndex = 35;
            // 
            // _sinhrManualYesNo
            // 
            this._sinhrManualYesNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualYesNo.FormattingEnabled = true;
            this._sinhrManualYesNo.Location = new System.Drawing.Point(99, 34);
            this._sinhrManualYesNo.Name = "_sinhrManualYesNo";
            this._sinhrManualYesNo.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualYesNo.TabIndex = 34;
            // 
            // _sinhrManualNoYes
            // 
            this._sinhrManualNoYes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualNoYes.FormattingEnabled = true;
            this._sinhrManualNoYes.Location = new System.Drawing.Point(99, 14);
            this._sinhrManualNoYes.Name = "_sinhrManualNoYes";
            this._sinhrManualNoYes.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualNoYes.TabIndex = 33;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 57);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(81, 13);
            this.label68.TabIndex = 32;
            this.label68.Text = "U1 нет, U2 нет";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 37);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(87, 13);
            this.label69.TabIndex = 31;
            this.label69.Text = "U1 есть, U2 нет";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 17);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(87, 13);
            this.label70.TabIndex = 30;
            this.label70.Text = "U1 нет, U2 есть";
            // 
            // _sinhrManualUmax
            // 
            this._sinhrManualUmax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrManualUmax.Location = new System.Drawing.Point(89, 37);
            this._sinhrManualUmax.Name = "_sinhrManualUmax";
            this._sinhrManualUmax.Size = new System.Drawing.Size(105, 20);
            this._sinhrManualUmax.TabIndex = 22;
            this._sinhrManualUmax.Tag = "3276700";
            this._sinhrManualUmax.Text = "0";
            this._sinhrManualUmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrManualMode
            // 
            this._sinhrManualMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrManualMode.FormattingEnabled = true;
            this._sinhrManualMode.Location = new System.Drawing.Point(89, 16);
            this._sinhrManualMode.Name = "_sinhrManualMode";
            this._sinhrManualMode.Size = new System.Drawing.Size(105, 21);
            this._sinhrManualMode.TabIndex = 18;
            // 
            // _dUmaxManualLabel
            // 
            this._dUmaxManualLabel.AutoSize = true;
            this._dUmaxManualLabel.Location = new System.Drawing.Point(12, 38);
            this._dUmaxManualLabel.Name = "_dUmaxManualLabel";
            this._dUmaxManualLabel.Size = new System.Drawing.Size(56, 13);
            this._dUmaxManualLabel.TabIndex = 11;
            this._dUmaxManualLabel.Text = "dUmax., В";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(12, 18);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(42, 13);
            this.label67.TabIndex = 9;
            this.label67.Text = "Режим";
            // 
            // groupBox34
            // 
            this.groupBox34.Controls.Add(this._sinhrF);
            this.groupBox34.Controls.Add(this._sinhrTon);
            this.groupBox34.Controls.Add(this._sinhrKamp);
            this.groupBox34.Controls.Add(this._sinhrTsinhr);
            this.groupBox34.Controls.Add(this._fLabel);
            this.groupBox34.Controls.Add(this._sinhrTwait);
            this.groupBox34.Controls.Add(this._KampLabel);
            this.groupBox34.Controls.Add(this.label14);
            this.groupBox34.Controls.Add(this.label15);
            this.groupBox34.Controls.Add(this.label16);
            this.groupBox34.Controls.Add(this._sinhrUmaxNal);
            this.groupBox34.Controls.Add(this._sinhrUminNal);
            this.groupBox34.Controls.Add(this._sinhrUminOts);
            this.groupBox34.Controls.Add(this._sinhrU2);
            this.groupBox34.Controls.Add(this._sinhrU1);
            this.groupBox34.Controls.Add(this.label40);
            this.groupBox34.Controls.Add(this.label57);
            this.groupBox34.Controls.Add(this.label58);
            this.groupBox34.Controls.Add(this.label59);
            this.groupBox34.Controls.Add(this.label60);
            this.groupBox34.Location = new System.Drawing.Point(8, 6);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(198, 215);
            this.groupBox34.TabIndex = 35;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "Общие уставки";
            // 
            // _sinhrF
            // 
            this._sinhrF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrF.Location = new System.Drawing.Point(89, 187);
            this._sinhrF.Name = "_sinhrF";
            this._sinhrF.Size = new System.Drawing.Size(105, 20);
            this._sinhrF.TabIndex = 30;
            this._sinhrF.Tag = "3276700";
            this._sinhrF.Text = "0";
            this._sinhrF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrTon
            // 
            this._sinhrTon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTon.Location = new System.Drawing.Point(89, 149);
            this._sinhrTon.Name = "_sinhrTon";
            this._sinhrTon.Size = new System.Drawing.Size(105, 20);
            this._sinhrTon.TabIndex = 30;
            this._sinhrTon.Tag = "3276700";
            this._sinhrTon.Text = "0";
            this._sinhrTon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrKamp
            // 
            this._sinhrKamp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrKamp.Location = new System.Drawing.Point(89, 168);
            this._sinhrKamp.Name = "_sinhrKamp";
            this._sinhrKamp.Size = new System.Drawing.Size(105, 20);
            this._sinhrKamp.TabIndex = 29;
            this._sinhrKamp.Tag = "40";
            this._sinhrKamp.Text = "0";
            this._sinhrKamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrTsinhr
            // 
            this._sinhrTsinhr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTsinhr.Location = new System.Drawing.Point(89, 130);
            this._sinhrTsinhr.Name = "_sinhrTsinhr";
            this._sinhrTsinhr.Size = new System.Drawing.Size(105, 20);
            this._sinhrTsinhr.TabIndex = 29;
            this._sinhrTsinhr.Tag = "40";
            this._sinhrTsinhr.Text = "0";
            this._sinhrTsinhr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _fLabel
            // 
            this._fLabel.AutoSize = true;
            this._fLabel.Location = new System.Drawing.Point(12, 189);
            this._fLabel.Name = "_fLabel";
            this._fLabel.Size = new System.Drawing.Size(42, 13);
            this._fLabel.TabIndex = 27;
            this._fLabel.Text = "f, град.";
            // 
            // _sinhrTwait
            // 
            this._sinhrTwait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrTwait.Location = new System.Drawing.Point(89, 111);
            this._sinhrTwait.Name = "_sinhrTwait";
            this._sinhrTwait.Size = new System.Drawing.Size(105, 20);
            this._sinhrTwait.TabIndex = 28;
            this._sinhrTwait.Tag = "3276700";
            this._sinhrTwait.Text = "0";
            this._sinhrTwait.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _KampLabel
            // 
            this._KampLabel.AutoSize = true;
            this._KampLabel.Location = new System.Drawing.Point(12, 170);
            this._KampLabel.Name = "_KampLabel";
            this._KampLabel.Size = new System.Drawing.Size(48, 13);
            this._KampLabel.TabIndex = 26;
            this._KampLabel.Text = "Камп, %";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 151);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "t вкл, мс";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 132);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(59, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "tсинхр, мс";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "tож, мс";
            // 
            // _sinhrUmaxNal
            // 
            this._sinhrUmaxNal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUmaxNal.Location = new System.Drawing.Point(89, 92);
            this._sinhrUmaxNal.Name = "_sinhrUmaxNal";
            this._sinhrUmaxNal.Size = new System.Drawing.Size(105, 20);
            this._sinhrUmaxNal.TabIndex = 24;
            this._sinhrUmaxNal.Tag = "3276700";
            this._sinhrUmaxNal.Text = "0";
            this._sinhrUmaxNal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminNal
            // 
            this._sinhrUminNal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminNal.Location = new System.Drawing.Point(89, 73);
            this._sinhrUminNal.Name = "_sinhrUminNal";
            this._sinhrUminNal.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminNal.TabIndex = 23;
            this._sinhrUminNal.Tag = "40";
            this._sinhrUminNal.Text = "0";
            this._sinhrUminNal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrUminOts
            // 
            this._sinhrUminOts.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._sinhrUminOts.Location = new System.Drawing.Point(89, 54);
            this._sinhrUminOts.Name = "_sinhrUminOts";
            this._sinhrUminOts.Size = new System.Drawing.Size(105, 20);
            this._sinhrUminOts.TabIndex = 22;
            this._sinhrUminOts.Tag = "3276700";
            this._sinhrUminOts.Text = "0";
            this._sinhrUminOts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _sinhrU2
            // 
            this._sinhrU2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU2.FormattingEnabled = true;
            this._sinhrU2.Location = new System.Drawing.Point(89, 36);
            this._sinhrU2.Name = "_sinhrU2";
            this._sinhrU2.Size = new System.Drawing.Size(105, 21);
            this._sinhrU2.TabIndex = 19;
            // 
            // _sinhrU1
            // 
            this._sinhrU1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._sinhrU1.FormattingEnabled = true;
            this._sinhrU1.Location = new System.Drawing.Point(89, 16);
            this._sinhrU1.Name = "_sinhrU1";
            this._sinhrU1.Size = new System.Drawing.Size(105, 21);
            this._sinhrU1.TabIndex = 18;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 94);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(71, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Umax. нал, В";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(12, 75);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(68, 13);
            this.label57.TabIndex = 12;
            this.label57.Text = "Umin. нал, В";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(12, 55);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(67, 13);
            this.label58.TabIndex = 11;
            this.label58.Text = "Umin. отс, В";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(12, 38);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(21, 13);
            this.label59.TabIndex = 10;
            this.label59.Text = "U2";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(12, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(21, 13);
            this.label60.TabIndex = 9;
            this.label60.Text = "U1";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this._saveToXmlButton);
            this.panel1.Controls.Add(this._resetSetpointsButton);
            this.panel1.Controls.Add(this._writeConfigBut);
            this.panel1.Controls.Add(this.statusStrip1);
            this.panel1.Controls.Add(this._readConfigBut);
            this.panel1.Controls.Add(this._saveConfigBut);
            this.panel1.Controls.Add(this._loadConfigBut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1029, 52);
            this.panel1.TabIndex = 33;
            // 
            // _saveToXmlButton
            // 
            this._saveToXmlButton.AllowDrop = true;
            this._saveToXmlButton.AutoSize = true;
            this._saveToXmlButton.Location = new System.Drawing.Point(889, 5);
            this._saveToXmlButton.Name = "_saveToXmlButton";
            this._saveToXmlButton.Size = new System.Drawing.Size(136, 23);
            this._saveToXmlButton.TabIndex = 31;
            this._saveToXmlButton.Text = "Сохранить в HTML";
            this._saveToXmlButton.UseVisualStyleBackColor = true;
            this._saveToXmlButton.Click += new System.EventHandler(this._saveToXmlButton_Click);
            // 
            // _resetSetpointsButton
            // 
            this._resetSetpointsButton.AutoSize = true;
            this._resetSetpointsButton.Location = new System.Drawing.Point(315, 5);
            this._resetSetpointsButton.Name = "_resetSetpointsButton";
            this._resetSetpointsButton.Size = new System.Drawing.Size(149, 23);
            this._resetSetpointsButton.TabIndex = 30;
            this._resetSetpointsButton.Text = "Обнулить уставки";
            this._resetSetpointsButton.UseVisualStyleBackColor = true;
            this._resetSetpointsButton.Click += new System.EventHandler(this._resetSetpointsButton_Click);
            // 
            // _writeConfigBut
            // 
            this._writeConfigBut.AutoSize = true;
            this._writeConfigBut.Location = new System.Drawing.Point(160, 5);
            this._writeConfigBut.Name = "_writeConfigBut";
            this._writeConfigBut.Size = new System.Drawing.Size(149, 23);
            this._writeConfigBut.TabIndex = 27;
            this._writeConfigBut.Text = "Записать в устройство";
            this.toolTip1.SetToolTip(this._writeConfigBut, "Записать конфигурацию в устройство (CTRL+W)");
            this._writeConfigBut.UseVisualStyleBackColor = true;
            this._writeConfigBut.Click += new System.EventHandler(this._writeConfigBut_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this._progressBar,
            this._statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 30);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip1.Size = new System.Drawing.Size(1029, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // _progressBar
            // 
            this._progressBar.Maximum = 90;
            this._progressBar.Name = "_progressBar";
            this._progressBar.Size = new System.Drawing.Size(100, 16);
            this._progressBar.Step = 1;
            // 
            // _statusLabel
            // 
            this._statusLabel.Name = "_statusLabel";
            this._statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // _readConfigBut
            // 
            this._readConfigBut.AutoSize = true;
            this._readConfigBut.Location = new System.Drawing.Point(3, 5);
            this._readConfigBut.Name = "_readConfigBut";
            this._readConfigBut.Size = new System.Drawing.Size(151, 23);
            this._readConfigBut.TabIndex = 26;
            this._readConfigBut.Text = "Прочитать из устройства";
            this.toolTip1.SetToolTip(this._readConfigBut, "Прочитать конфигурацию из устройства (CTRL+R)");
            this._readConfigBut.UseVisualStyleBackColor = true;
            this._readConfigBut.Click += new System.EventHandler(this._readConfigBut_Click);
            // 
            // _saveConfigBut
            // 
            this._saveConfigBut.AllowDrop = true;
            this._saveConfigBut.AutoSize = true;
            this._saveConfigBut.Location = new System.Drawing.Point(747, 5);
            this._saveConfigBut.Name = "_saveConfigBut";
            this._saveConfigBut.Size = new System.Drawing.Size(136, 23);
            this._saveConfigBut.TabIndex = 29;
            this._saveConfigBut.Text = "Сохранить в файл";
            this.toolTip1.SetToolTip(this._saveConfigBut, "Сохранить конфигурацию в файл (CTRL+S)");
            this._saveConfigBut.UseVisualStyleBackColor = true;
            this._saveConfigBut.Click += new System.EventHandler(this._saveConfigBut_Click);
            // 
            // _loadConfigBut
            // 
            this._loadConfigBut.AutoSize = true;
            this._loadConfigBut.Location = new System.Drawing.Point(592, 5);
            this._loadConfigBut.Name = "_loadConfigBut";
            this._loadConfigBut.Size = new System.Drawing.Size(149, 23);
            this._loadConfigBut.TabIndex = 28;
            this._loadConfigBut.Text = "Загрузить из файла";
            this.toolTip1.SetToolTip(this._loadConfigBut, "Загрузить конфигурацию из файла (CTRL+O)");
            this._loadConfigBut.UseVisualStyleBackColor = true;
            this._loadConfigBut.Click += new System.EventHandler(this._loadConfigBut_Click);
            // 
            // _saveConfigurationDlg
            // 
            this._saveConfigurationDlg.DefaultExt = "xml";
            this._saveConfigurationDlg.FileName = "МР762";
            this._saveConfigurationDlg.Filter = "Уставки МР762 (*.xml) | *.xml";
            this._saveConfigurationDlg.Title = "Сохранить  уставки для МР762";
            // 
            // _openConfigurationDlg
            // 
            this._openConfigurationDlg.DefaultExt = "xml";
            this._openConfigurationDlg.FileName = "МР762";
            this._openConfigurationDlg.Filter = "Уставки МР762 (*.xml) | *.xml";
            this._openConfigurationDlg.RestoreDirectory = true;
            this._openConfigurationDlg.Title = "Открыть уставки для МР762";
            // 
            // _saveXmlDialog
            // 
            this._saveXmlDialog.Filter = "(*.html) | *.html";
            // 
            // _releNumberCol
            // 
            this._releNumberCol.HeaderText = "№";
            this._releNumberCol.Name = "_releNumberCol";
            this._releNumberCol.ReadOnly = true;
            this._releNumberCol.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this._releNumberCol.Width = 25;
            // 
            // _releTypeCol
            // 
            this._releTypeCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releTypeCol.HeaderText = "Тип";
            this._releTypeCol.Name = "_releTypeCol";
            this._releTypeCol.Width = 120;
            // 
            // _releSignalCol
            // 
            this._releSignalCol.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this._releSignalCol.HeaderText = "Сигнал";
            this._releSignalCol.Name = "_releSignalCol";
            this._releSignalCol.Width = 120;
            // 
            // _releWaitCol
            // 
            this._releWaitCol.HeaderText = "Твозвр, мс";
            this._releWaitCol.Name = "_releWaitCol";
            this._releWaitCol.Width = 90;
            // 
            // Mr763ConfigurationFormV201
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 634);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this._configurationTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 670);
            this.Name = "Mr763ConfigurationFormV201";
            this.Text = "Mr763ConfigurationForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mr763ConfigurationForm_FormClosing);
            this.Load += new System.EventHandler(this.Mr763ConfigurationForm_Load);
            this.Shown += new System.EventHandler(this.Mr763ConfigurationForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Mr763ConfigurationForm_KeyUp);
            this._configurationTabControl.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this._measureTransPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this._inputSygnalsPage.ResumeLayout(false);
            this.groupBox47.ResumeLayout(false);
            this.groupBox48.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox17.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals9)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals10)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals11)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals12)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals13)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals14)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals15)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals16)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals4)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals5)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals6)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals7)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._inputSignals8)).EndInit();
            this.tabPage28.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this._allDefensesPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this._difensesTC.ResumeLayout(false);
            this.tabPage17.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.tabPage21.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI8DataGrid)).EndInit();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI67DataGrid)).EndInit();
            this.groupBox20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesIDataGrid)).EndInit();
            this.tabPage22.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesI0DataGrid)).EndInit();
            this.tabPage19.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.tabPage23.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUBDataGrid)).EndInit();
            this.tabPage24.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesUMDataGrid)).EndInit();
            this.tabPage26.ResumeLayout(false);
            this.groupBox27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFBDataGrid)).EndInit();
            this.tabPage27.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._difensesFMDataGrid)).EndInit();
            this.tabPage25.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._externalDifensesDataGrid)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._engineDefensesGrid)).EndInit();
            this.groupBox25.ResumeLayout(false);
            this.groupBox43.ResumeLayout(false);
            this.groupBox43.PerformLayout();
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this._outputSignalsPage.ResumeLayout(false);
            this.groupBox46.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.VLSTabControl.ResumeLayout(false);
            this.VLS1.ResumeLayout(false);
            this.VLS2.ResumeLayout(false);
            this.VLS3.ResumeLayout(false);
            this.VLS4.ResumeLayout(false);
            this.VLS5.ResumeLayout(false);
            this.VLS6.ResumeLayout(false);
            this.VLS7.ResumeLayout(false);
            this.VLS8.ResumeLayout(false);
            this.VLS9.ResumeLayout(false);
            this.VLS10.ResumeLayout(false);
            this.VLS11.ResumeLayout(false);
            this.VLS12.ResumeLayout(false);
            this.VLS13.ResumeLayout(false);
            this.VLS14.ResumeLayout(false);
            this.VLS15.ResumeLayout(false);
            this.VLS16.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputIndicatorsGrid)).EndInit();
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._outputReleGrid)).EndInit();
            this._systemPage.ResumeLayout(false);
            this.groupBox45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._oscChannels)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._automatPage.ResumeLayout(false);
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.tabPage29.ResumeLayout(false);
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TabControl _configurationTabControl;
        protected System.Windows.Forms.TabPage _measureTransPage;
        protected System.Windows.Forms.TabPage _inputSygnalsPage;
        protected System.Windows.Forms.GroupBox groupBox18;
        protected System.Windows.Forms.ComboBox _indComboBox;
        protected System.Windows.Forms.GroupBox groupBox15;
        protected System.Windows.Forms.ComboBox _grUstComboBox;
        protected System.Windows.Forms.GroupBox groupBox17;
        protected System.Windows.Forms.TabControl tabControl2;
        protected System.Windows.Forms.TabPage tabPage9;
        protected System.Windows.Forms.DataGridView _inputSignals9;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _signalValueNumILI;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _signalValueColILI;
        protected System.Windows.Forms.TabPage tabPage10;
        protected System.Windows.Forms.DataGridView _inputSignals10;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn8;
        protected System.Windows.Forms.TabPage tabPage11;
        protected System.Windows.Forms.DataGridView _inputSignals11;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn9;
        protected System.Windows.Forms.TabPage tabPage12;
        protected System.Windows.Forms.DataGridView _inputSignals12;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn10;
        protected System.Windows.Forms.TabPage tabPage13;
        protected System.Windows.Forms.DataGridView _inputSignals13;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn11;
        protected System.Windows.Forms.TabPage tabPage14;
        protected System.Windows.Forms.DataGridView _inputSignals14;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn12;
        protected System.Windows.Forms.TabPage tabPage15;
        protected System.Windows.Forms.DataGridView _inputSignals15;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn13;
        protected System.Windows.Forms.TabPage tabPage16;
        protected System.Windows.Forms.DataGridView _inputSignals16;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn14;
        protected System.Windows.Forms.GroupBox groupBox14;
        protected System.Windows.Forms.TabControl tabControl1;
        protected System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.DataGridView _inputSignals1;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _lsChannelCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _signalValueCol;
        protected System.Windows.Forms.TabPage tabPage2;
        protected System.Windows.Forms.DataGridView _inputSignals2;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        protected System.Windows.Forms.TabPage tabPage3;
        protected System.Windows.Forms.DataGridView _inputSignals3;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn2;
        protected System.Windows.Forms.TabPage tabPage4;
        protected System.Windows.Forms.DataGridView _inputSignals4;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn3;
        protected System.Windows.Forms.TabPage tabPage5;
        protected System.Windows.Forms.DataGridView _inputSignals5;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn4;
        protected System.Windows.Forms.TabPage tabPage6;
        protected System.Windows.Forms.DataGridView _inputSignals6;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn5;
        protected System.Windows.Forms.TabPage tabPage7;
        protected System.Windows.Forms.DataGridView _inputSignals7;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn6;
        protected System.Windows.Forms.TabPage tabPage8;
        protected System.Windows.Forms.DataGridView _inputSignals8;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        protected System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn7;
        protected System.Windows.Forms.TabPage _allDefensesPage;
        protected System.Windows.Forms.GroupBox groupBox5;
        protected System.Windows.Forms.RadioButton _mainRadioButton;
        protected System.Windows.Forms.RadioButton _reserveRadioButton;
        protected System.Windows.Forms.GroupBox groupBox10;
        protected System.Windows.Forms.TabControl _difensesTC;
        protected System.Windows.Forms.TabPage tabPage17;
        protected System.Windows.Forms.GroupBox groupBox16;
        protected System.Windows.Forms.Label label134;
        protected System.Windows.Forms.Label label133;
        protected System.Windows.Forms.Label label132;
        protected System.Windows.Forms.Label label100;
        protected System.Windows.Forms.MaskedTextBox _i2Corner;
        protected System.Windows.Forms.MaskedTextBox _inCorner;
        protected System.Windows.Forms.MaskedTextBox _i0Corner;
        protected System.Windows.Forms.MaskedTextBox _iCorner;
        protected System.Windows.Forms.TabPage tabPage21;
        protected System.Windows.Forms.GroupBox groupBox20;
        protected System.Windows.Forms.DataGridView _difensesIDataGrid;
        protected System.Windows.Forms.TabPage tabPage22;
        protected System.Windows.Forms.GroupBox groupBox21;
        protected System.Windows.Forms.DataGridView _difensesI0DataGrid;
        protected System.Windows.Forms.TabPage tabPage19;
        protected System.Windows.Forms.GroupBox groupBox6;
        protected System.Windows.Forms.ComboBox I2I1OSCCombo;
        protected System.Windows.Forms.MaskedTextBox I2I1tcp;
        protected System.Windows.Forms.ComboBox I2I1BlockingCombo;
        protected System.Windows.Forms.Label label28;
        protected System.Windows.Forms.Label label29;
        protected System.Windows.Forms.Label label22;
        protected System.Windows.Forms.Label label26;
        protected System.Windows.Forms.Label label27;
        protected System.Windows.Forms.Label label21;
        protected System.Windows.Forms.Label label20;
        protected System.Windows.Forms.MaskedTextBox I2I1TB;
        protected System.Windows.Forms.ComboBox I2I1ModeCombo;
        protected System.Windows.Forms.Label label19;
        protected System.Windows.Forms.TabPage tabPage20;
        protected System.Windows.Forms.GroupBox groupBox19;
        protected System.Windows.Forms.Label label39;
        protected System.Windows.Forms.MaskedTextBox IrtyTB;
        protected System.Windows.Forms.MaskedTextBox IrtcpTB;
        protected System.Windows.Forms.Label label38;
        protected System.Windows.Forms.ComboBox IrOSCCombo;
        protected System.Windows.Forms.MaskedTextBox IrIcpTB;
        protected System.Windows.Forms.ComboBox IrBlockingCombo;
        protected System.Windows.Forms.Label label30;
        protected System.Windows.Forms.Label label31;
        protected System.Windows.Forms.Label label32;
        protected System.Windows.Forms.Label label33;
        protected System.Windows.Forms.Label label34;
        protected System.Windows.Forms.Label label35;
        protected System.Windows.Forms.Label label36;
        protected System.Windows.Forms.MaskedTextBox IrUpuskTB;
        protected System.Windows.Forms.ComboBox IrModesCombo;
        protected System.Windows.Forms.Label label37;
        protected System.Windows.Forms.TabPage tabPage23;
        protected System.Windows.Forms.GroupBox groupBox22;
        protected System.Windows.Forms.DataGridView _difensesUBDataGrid;
        protected System.Windows.Forms.TabPage tabPage24;
        protected System.Windows.Forms.GroupBox groupBox23;
        protected System.Windows.Forms.DataGridView _difensesUMDataGrid;
        protected System.Windows.Forms.TabPage tabPage26;
        protected System.Windows.Forms.GroupBox groupBox27;
        protected System.Windows.Forms.DataGridView _difensesFBDataGrid;
        protected System.Windows.Forms.TabPage tabPage27;
        protected System.Windows.Forms.GroupBox groupBox28;
        protected System.Windows.Forms.DataGridView _difensesFMDataGrid;
        protected System.Windows.Forms.TabPage tabPage25;
        protected System.Windows.Forms.GroupBox groupBox24;
        protected System.Windows.Forms.DataGridView _externalDifensesDataGrid;
        protected System.Windows.Forms.TabPage _outputSignalsPage;
        protected System.Windows.Forms.GroupBox groupBox26;
        protected System.Windows.Forms.MaskedTextBox _impTB;
        protected System.Windows.Forms.Label label84;
        protected System.Windows.Forms.GroupBox groupBox13;
        protected System.Windows.Forms.TabControl VLSTabControl;
        protected System.Windows.Forms.TabPage VLS1;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox1;
        protected System.Windows.Forms.TabPage VLS2;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox2;
        protected System.Windows.Forms.TabPage VLS3;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox3;
        protected System.Windows.Forms.TabPage VLS4;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox4;
        protected System.Windows.Forms.TabPage VLS5;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox5;
        protected System.Windows.Forms.TabPage VLS6;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox6;
        protected System.Windows.Forms.TabPage VLS7;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox7;
        protected System.Windows.Forms.TabPage VLS8;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox8;
        protected System.Windows.Forms.TabPage VLS9;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox9;
        protected System.Windows.Forms.TabPage VLS10;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox10;
        protected System.Windows.Forms.TabPage VLS11;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox11;
        protected System.Windows.Forms.TabPage VLS12;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox12;
        protected System.Windows.Forms.TabPage VLS13;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox13;
        protected System.Windows.Forms.TabPage VLS14;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox14;
        protected System.Windows.Forms.TabPage VLS15;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox15;
        protected System.Windows.Forms.TabPage VLS16;
        protected System.Windows.Forms.CheckedListBox VLScheckedListBox16;
        protected System.Windows.Forms.GroupBox groupBox11;
        protected System.Windows.Forms.DataGridView _outputIndicatorsGrid;
        protected System.Windows.Forms.GroupBox groupBox12;
        protected System.Windows.Forms.DataGridView _outputReleGrid;
        protected System.Windows.Forms.TabPage _systemPage;
        protected System.Windows.Forms.GroupBox groupBox3;
        protected System.Windows.Forms.DataGridView _oscChannels;
        protected System.Windows.Forms.MaskedTextBox _oscWriteLength;
        protected System.Windows.Forms.ComboBox _oscFix;
        protected System.Windows.Forms.ComboBox _oscLength;
        protected System.Windows.Forms.Label label41;
        protected System.Windows.Forms.Label label42;
        protected System.Windows.Forms.Label label43;
        protected System.Windows.Forms.TabPage _automatPage;
        protected System.Windows.Forms.GroupBox groupBox31;
        protected System.Windows.Forms.Label label127;
        protected System.Windows.Forms.MaskedTextBox _lzhVal;
        protected System.Windows.Forms.ComboBox _lzhModes;
        protected System.Windows.Forms.Label label126;
        protected System.Windows.Forms.GroupBox groupBox30;
        protected System.Windows.Forms.Label label18;
        protected System.Windows.Forms.Label label125;
        protected System.Windows.Forms.ComboBox _avrClear;
        protected System.Windows.Forms.Label label124;
        protected System.Windows.Forms.MaskedTextBox _avrTOff;
        protected System.Windows.Forms.Label label123;
        protected System.Windows.Forms.MaskedTextBox _avrTBack;
        protected System.Windows.Forms.Label label122;
        protected System.Windows.Forms.ComboBox _avrBack;
        protected System.Windows.Forms.Label label121;
        protected System.Windows.Forms.MaskedTextBox _avrTSr;
        protected System.Windows.Forms.Label label120;
        protected System.Windows.Forms.ComboBox _avrResolve;
        protected System.Windows.Forms.Label label119;
        protected System.Windows.Forms.ComboBox _avrBlockClear;
        protected System.Windows.Forms.Label label118;
        protected System.Windows.Forms.ComboBox _avrBlocking;
        protected System.Windows.Forms.Label label117;
        protected System.Windows.Forms.ComboBox _avrSIGNOn;
        protected System.Windows.Forms.Label label116;
        protected System.Windows.Forms.ComboBox _avrByDiff;
        protected System.Windows.Forms.Label label115;
        protected System.Windows.Forms.ComboBox _avrBySelfOff;
        protected System.Windows.Forms.Label label114;
        protected System.Windows.Forms.ComboBox _avrByOff;
        protected System.Windows.Forms.Label label113;
        protected System.Windows.Forms.ComboBox _avrBySignal;
        protected System.Windows.Forms.GroupBox groupBox29;
        protected System.Windows.Forms.Label label135;
        protected System.Windows.Forms.Label label136;
        protected System.Windows.Forms.MaskedTextBox _apv4Krat;
        protected System.Windows.Forms.MaskedTextBox _apv3Krat;
        protected System.Windows.Forms.Label label112;
        protected System.Windows.Forms.Label label111;
        protected System.Windows.Forms.Label label110;
        protected System.Windows.Forms.Label label109;
        protected System.Windows.Forms.Label label96;
        protected System.Windows.Forms.ComboBox _apvOff;
        protected System.Windows.Forms.MaskedTextBox _apv2Krat;
        protected System.Windows.Forms.MaskedTextBox _apv1Krat;
        protected System.Windows.Forms.MaskedTextBox _apvTReady;
        protected System.Windows.Forms.MaskedTextBox _apvTBlock;
        protected System.Windows.Forms.ComboBox _apvBlocking;
        protected System.Windows.Forms.Label label95;
        protected System.Windows.Forms.ComboBox _apvModes;
        protected System.Windows.Forms.Label label94;
        protected System.Windows.Forms.GroupBox groupBox32;
        protected System.Windows.Forms.ComboBox _switchKontCep;
        protected System.Windows.Forms.MaskedTextBox _switchTUskor;
        protected System.Windows.Forms.MaskedTextBox _switchImp;
        protected System.Windows.Forms.MaskedTextBox _switchIUrov;
        protected System.Windows.Forms.MaskedTextBox _switchTUrov;
        protected System.Windows.Forms.ComboBox _switchBlock;
        protected System.Windows.Forms.ComboBox _switchError;
        protected System.Windows.Forms.ComboBox _switchOn;
        protected System.Windows.Forms.ComboBox _switchOff;
        protected System.Windows.Forms.Label label97;
        protected System.Windows.Forms.Label label98;
        protected System.Windows.Forms.Label label99;
        protected System.Windows.Forms.Label label91;
        protected System.Windows.Forms.Label label92;
        protected System.Windows.Forms.Label label93;
        protected System.Windows.Forms.Label label90;
        protected System.Windows.Forms.Label label89;
        protected System.Windows.Forms.Label label88;
        protected System.Windows.Forms.GroupBox groupBox33;
        protected System.Windows.Forms.ComboBox _switchSDTU;
        protected System.Windows.Forms.ComboBox _switchVnesh;
        protected System.Windows.Forms.ComboBox _switchKey;
        protected System.Windows.Forms.ComboBox _switchButtons;
        protected System.Windows.Forms.ComboBox _switchVneshOff;
        protected System.Windows.Forms.ComboBox _switchVneshOn;
        protected System.Windows.Forms.ComboBox _switchKeyOff;
        protected System.Windows.Forms.ComboBox _switchKeyOn;
        protected System.Windows.Forms.Label label101;
        protected System.Windows.Forms.Label label102;
        protected System.Windows.Forms.Label label103;
        protected System.Windows.Forms.Label label104;
        protected System.Windows.Forms.Label label105;
        protected System.Windows.Forms.Label label106;
        protected System.Windows.Forms.Label label107;
        protected System.Windows.Forms.Label label108;
        protected System.Windows.Forms.Panel panel1;
        protected System.Windows.Forms.Button _writeConfigBut;
        protected System.Windows.Forms.StatusStrip statusStrip1;
        protected System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        protected System.Windows.Forms.ToolStripProgressBar _progressBar;
        protected System.Windows.Forms.Button _readConfigBut;
        protected System.Windows.Forms.Button _saveConfigBut;
        protected System.Windows.Forms.Button _loadConfigBut;
        protected System.Windows.Forms.SaveFileDialog _saveConfigurationDlg;
        protected System.Windows.Forms.OpenFileDialog _openConfigurationDlg;
        protected System.Windows.Forms.ToolStripStatusLabel _statusLabel;
        protected System.Windows.Forms.GroupBox groupBox8;
        protected System.Windows.Forms.DataGridView _difensesI8DataGrid;
        protected System.Windows.Forms.GroupBox groupBox7;
        protected System.Windows.Forms.DataGridView _difensesI67DataGrid;
        protected System.Windows.Forms.TabPage tabPage18;
        protected System.Windows.Forms.GroupBox groupBox25;
        protected System.Windows.Forms.MaskedTextBox _engineBlockTimeBox;
        protected System.Windows.Forms.Label label50;
        protected System.Windows.Forms.MaskedTextBox _engineBlockHeatBox;
        protected System.Windows.Forms.Label label52;
        protected System.Windows.Forms.MaskedTextBox _engineBlockPuskBox;
        protected System.Windows.Forms.Label label53;
        protected System.Windows.Forms.Label label54;
        protected System.Windows.Forms.ComboBox _engineQmodeCombo;
        protected System.Windows.Forms.MaskedTextBox _engineQtimeBox;
        protected System.Windows.Forms.Label label55;
        protected System.Windows.Forms.MaskedTextBox _engineQconstraintBox;
        protected System.Windows.Forms.Label label56;
        protected System.Windows.Forms.DataGridView _engineDefensesGrid;
        protected System.Windows.Forms.TabPage tabPage28;
        protected System.Windows.Forms.GroupBox groupBox9;
        protected System.Windows.Forms.Label label23;
        protected System.Windows.Forms.Label label24;
        protected System.Windows.Forms.ComboBox _engineNpuskCombo;
        protected System.Windows.Forms.ComboBox _engineQresetCombo;
        protected System.Windows.Forms.MaskedTextBox _engineHeatPuskConstraintBox;
        protected System.Windows.Forms.Label label44;
        protected System.Windows.Forms.MaskedTextBox _enginePuskTimeBox;
        protected System.Windows.Forms.Label label45;
        protected System.Windows.Forms.MaskedTextBox _engineIpConstraintBox;
        protected System.Windows.Forms.Label label46;
        protected System.Windows.Forms.MaskedTextBox _engineInBox;
        protected System.Windows.Forms.Label label47;
        protected System.Windows.Forms.MaskedTextBox _engineCoolingTimeBox;
        protected System.Windows.Forms.Label label48;
        protected System.Windows.Forms.MaskedTextBox _engineHeatingTimeBox;
        protected System.Windows.Forms.Label label49;
        protected System.Windows.Forms.MaskedTextBox _oscSizeTextBox;
        protected System.Windows.Forms.TabPage tabPage29;
        protected System.Windows.Forms.GroupBox groupBox43;
        protected System.Windows.Forms.GroupBox groupBox44;
        protected System.Windows.Forms.MaskedTextBox _engineBlockDurationBox;
        protected System.Windows.Forms.Label label51;
        protected System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Label label83;
        protected System.Windows.Forms.Label label82;
        protected System.Windows.Forms.Label label81;
        protected System.Windows.Forms.ToolTip _toolTip;
        protected System.Windows.Forms.Button _resetSetpointsButton;
        protected System.Windows.Forms.Button _saveToXmlButton;
        protected System.Windows.Forms.Button _groupChangeButton;
        protected System.Windows.Forms.CheckBox _fault4CheckBox;
        protected System.Windows.Forms.CheckBox _fault3CheckBox;
        protected System.Windows.Forms.CheckBox _fault2CheckBox;
        protected System.Windows.Forms.CheckBox _fault1CheckBox;
        protected System.Windows.Forms.CheckBox I2I1AVRCheck;
        protected System.Windows.Forms.CheckBox I2I1APVCheck;
        protected System.Windows.Forms.CheckBox I2I1UROVCheck;
        protected System.Windows.Forms.CheckBox IrUpuskCheck;
        protected System.Windows.Forms.CheckBox IrtyCheck;
        protected System.Windows.Forms.CheckBox IrAVRCheck;
        protected System.Windows.Forms.CheckBox IrAPVCheck;
        protected System.Windows.Forms.CheckBox IrUROVCheck;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseNameCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseModeCol;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _engineDefenseConstraintCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseBlockCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _engineDefenseOscCol;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseUROVcol;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseAVRcol;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _engineDefenseAPVcol;
        protected System.Windows.Forms.SaveFileDialog _saveXmlDialog;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _outIndNumberCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _outIndTypeCol;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _outIndSignalCol;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _outIndColorCol;
        protected System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _oscSygnal;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0StageColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0ModesColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0IColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0UstartColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i0UsYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0DirColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0UndirColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0I0Column;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0CharColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0TColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0kColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0BlockingColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i0OscColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i0TyColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i0TyYNColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i0UROVColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i0APVColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i0AVRColumn;
        protected System.Windows.Forms.GroupBox groupBox45;
        protected System.Windows.Forms.ComboBox oscStartCb;
        protected System.Windows.Forms.Label label8;
        protected System.Windows.Forms.GroupBox groupBox39;
        protected System.Windows.Forms.GroupBox groupBox40;
        protected System.Windows.Forms.MaskedTextBox _sinhrAutodFno;
        protected System.Windows.Forms.Label label17;
        protected System.Windows.Forms.GroupBox groupBox41;
        protected System.Windows.Forms.MaskedTextBox _sinhrAutodF;
        protected System.Windows.Forms.Label label61;
        protected System.Windows.Forms.Label label62;
        protected System.Windows.Forms.MaskedTextBox _sinhrAutodFi;
        protected System.Windows.Forms.GroupBox groupBox42;
        protected System.Windows.Forms.ComboBox _sinhrAutoNoNo;
        protected System.Windows.Forms.ComboBox _discretIn2Cmb;
        protected System.Windows.Forms.ComboBox _sinhrAutoYesNo;
        protected System.Windows.Forms.ComboBox _discretIn1Cmb;
        protected System.Windows.Forms.ComboBox _sinhrAutoNoYes;
        protected System.Windows.Forms.Label label66;
        protected System.Windows.Forms.Label label72;
        protected System.Windows.Forms.Label label73;
        protected System.Windows.Forms.MaskedTextBox _sinhrAutoUmax;
        protected System.Windows.Forms.ComboBox _blockSinhCmb;
        protected System.Windows.Forms.ComboBox _sinhrAutoMode;
        protected System.Windows.Forms.Label _blockSinhLabel;
        protected System.Windows.Forms.Label _dUmaxAutoLabel;
        protected System.Windows.Forms.Label label75;
        protected System.Windows.Forms.GroupBox groupBox35;
        protected System.Windows.Forms.GroupBox groupBox38;
        protected System.Windows.Forms.MaskedTextBox _sinhrManualdFno;
        protected System.Windows.Forms.Label label71;
        protected System.Windows.Forms.GroupBox groupBox37;
        protected System.Windows.Forms.MaskedTextBox _sinhrManualdF;
        protected System.Windows.Forms.Label label64;
        protected System.Windows.Forms.Label label63;
        protected System.Windows.Forms.MaskedTextBox _sinhrManualdFi;
        protected System.Windows.Forms.GroupBox groupBox36;
        protected System.Windows.Forms.ComboBox _sinhrManualNoNo;
        protected System.Windows.Forms.ComboBox _sinhrManualYesNo;
        protected System.Windows.Forms.ComboBox _sinhrManualNoYes;
        protected System.Windows.Forms.Label label68;
        protected System.Windows.Forms.Label label69;
        protected System.Windows.Forms.Label label70;
        protected System.Windows.Forms.MaskedTextBox _sinhrManualUmax;
        protected System.Windows.Forms.ComboBox _sinhrManualMode;
        protected System.Windows.Forms.Label _dUmaxManualLabel;
        protected System.Windows.Forms.Label label67;
        protected System.Windows.Forms.GroupBox groupBox34;
        protected System.Windows.Forms.MaskedTextBox _sinhrF;
        protected System.Windows.Forms.MaskedTextBox _sinhrTon;
        protected System.Windows.Forms.MaskedTextBox _sinhrKamp;
        protected System.Windows.Forms.MaskedTextBox _sinhrTsinhr;
        protected System.Windows.Forms.Label _fLabel;
        protected System.Windows.Forms.MaskedTextBox _sinhrTwait;
        protected System.Windows.Forms.Label _KampLabel;
        protected System.Windows.Forms.Label label14;
        protected System.Windows.Forms.Label label15;
        protected System.Windows.Forms.Label label16;
        protected System.Windows.Forms.MaskedTextBox _sinhrUmaxNal;
        protected System.Windows.Forms.MaskedTextBox _sinhrUminNal;
        protected System.Windows.Forms.MaskedTextBox _sinhrUminOts;
        protected System.Windows.Forms.ComboBox _sinhrU2;
        protected System.Windows.Forms.ComboBox _sinhrU1;
        protected System.Windows.Forms.Label label40;
        protected System.Windows.Forms.Label label57;
        protected System.Windows.Forms.Label label58;
        protected System.Windows.Forms.Label label59;
        protected System.Windows.Forms.Label label60;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8StageColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8ModesColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8IColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8UStartColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i8UstartYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8DirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8UnDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8LogicColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8CharColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8TColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8KColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8TyColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i8TyYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8BlockingColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i8I2I1Column;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8I2I1YNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8BlockingDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i8OscModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i8UROVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i8APVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i8AVRModeColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67StageColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67ModesColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67IColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67IModeColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67UStartColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i67UstartYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67DirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67UnDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67LogicColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67CharColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67TColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67KColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67TyColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i67TyYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67BlockingColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _i67I2I1Column;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67I2I1YNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67BlockingDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _i67OscModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i67UROVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i67APVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _i67AVRModeColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iStageColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iModesColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iIColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iUStartColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _iUstartYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iUnDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iLogicColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iCharColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iTColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iKColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iTyColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _iTyYNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iBlockingColumn;
        protected System.Windows.Forms.DataGridViewTextBoxColumn _iI2I1Column;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iI2I1YNColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iBlockingDirectColumn;
        protected System.Windows.Forms.DataGridViewComboBoxColumn _iOscModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _iUROVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _iAPVModeColumn;
        protected System.Windows.Forms.DataGridViewCheckBoxColumn _iAVRModeColumn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox _Xline_Box;
        private System.Windows.Forms.ComboBox _OMPmode_combo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _KTHX1koef_combo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.MaskedTextBox _KTHX1_Box;
        private System.Windows.Forms.ComboBox _errorX_combo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox _KTHXkoef_combo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox _errorL_combo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox _KTHLkoef_combo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox _Uo_typeCombo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox _KTHX_Box;
        private System.Windows.Forms.MaskedTextBox _KTHL_Box;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox _TT_typeCombo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.MaskedTextBox _ITTL_Box;
        private System.Windows.Forms.MaskedTextBox _Im_Box;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBTypeColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uBUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUvzYNColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column4;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uBOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uBSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column5;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMTypeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _uMUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMUvzYNColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMBlockingUMColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _uMOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _uMSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fBUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBUvzYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fBOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fBSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fMStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fMModesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fMUsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fMTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fMTvzColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _fMUvzColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMUvzYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fMBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _fMOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _fMSbrosColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifStageColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifModesColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifSrabColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTsrColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn _externalDifTvzColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifVozvrColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifVozvrYNColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifBlockingColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn _externalDifOscColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifUROVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAVRColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifAPVRetColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn _externalDifSbrosColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem readFromDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem writeToDeviceItem;
        private System.Windows.Forms.ToolStripMenuItem readFromFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToFileItem;
        private System.Windows.Forms.ToolStripMenuItem writeToHtmlItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.TreeView treeViewForVLS;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.TreeView treeViewForLsOR;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.TreeView treeViewForLsAND;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releNumberCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releTypeCol;
        private System.Windows.Forms.DataGridViewComboBoxColumn _releSignalCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn _releWaitCol;
    }
}