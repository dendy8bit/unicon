﻿using System;
using System.Windows.Forms;
using BEMN_XY_Chart;

namespace BEMN.Mr763.Version1.Osc.ShowOsc.UI
{
    /// <summary>
    /// Изменение масштаба графиков по Х
    /// </summary>
    public class ChartsXZoomer
    {
        #region [Constants]
        /// <summary>
        /// Минимальное увеличение
        /// </summary>
        private const int MIN_ZOOM = 1;
        /// <summary>
        /// Максимальное увеличение
        /// </summary>
        private const int MAX_ZOOM = 64; 
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// Графики
        /// </summary>
        private readonly DAS_Net_XYChart[] _charts;
        /// <summary>
        /// Маркеры
        /// </summary>
        private readonly TrackBar[] _markers;
        /// <summary>
        /// Полоска прокрутки
        /// </summary>
        private readonly HScrollBar _scrollBar;
        /// <summary>
        /// Кнопка увеличения
        /// </summary>
        private readonly Button _increaseButton;
        /// <summary>
        /// Кнопка уменьшения
        /// </summary>
        private readonly Button _decreaseButton;
        /// <summary>
        /// Увеличение
        /// </summary>
        private int _zoom = 1;
        /// <summary>
        /// Начальная ширина графика(в координатах) 
        /// </summary>
        private readonly int _maxX;
        /// <summary>
        /// Значение 1-ого маркера
        /// </summary>
        private int _marker1Value;
        /// <summary>
        /// Значение 2-ого маркера
        /// </summary>
        private int _marker2Value;
        #endregion [Private fields]


        #region [Ctor's]
        /// <summary>
        /// Создаёт объект ChartZoomer
        /// </summary>
        /// <param name="charts">Массив графиков</param>
        /// <param name="scrollBar">Полоса прокрутки</param>
        /// <param name="markers">Маркеры</param>
        /// <param name="increaseButton">Кнопка увеличения</param>
        /// <param name="decreaseButton">Кнопка уменьшения</param>
        public ChartsXZoomer(DAS_Net_XYChart[] charts, HScrollBar scrollBar, TrackBar[] markers, Button increaseButton, Button decreaseButton)
        {
            this._charts = charts;
            this._scrollBar = scrollBar;
            this._markers = markers;
            this._increaseButton = increaseButton;
            this._decreaseButton = decreaseButton;

            this._maxX = (int)charts[0].XMax;

            this._scrollBar.Scroll += this._scrollBar_Scroll;
            this._increaseButton.Click += this._increaseButton_Click;
            this._decreaseButton.Click += this._decreaseButton_Click;

            foreach (var marker in this._markers)
            {
                marker.Scroll += this.marker_Scroll;
            }

            this._decreaseButton.Enabled = false;
        }
        #endregion [Ctor's]


        #region [Events Handlers]
        /// <summary>
        /// Перемещение маркера
        /// </summary>
        void marker_Scroll(object sender, EventArgs e)
        {
            var trackBar = sender as TrackBar;
            if (trackBar != null)
            {
                if (trackBar.Equals(this._markers[0]))
                {
                    this._marker1Value = trackBar.Value;
                }
                else
                {
                    this._marker2Value = trackBar.Value;
                }
            }
        }
        /// <summary>
        /// Изменение положения полосы прокрутки
        /// </summary>
        private void _scrollBar_Scroll(object sender, ScrollEventArgs e)
        {
           this.ApplyChanges();
        } 
        
        /// <summary>
        /// Увеличивает графики
        /// </summary>
        void _increaseButton_Click(object sender, EventArgs e)
        {
            this._decreaseButton.Enabled = true;
            this._scrollBar.Visible = true;
 
            this._zoom *= 2;
            this.SetScrollBarValues();
            this.ApplyChanges();

            if (this._zoom == ChartsXZoomer.MAX_ZOOM)
            {
                this._increaseButton.Enabled = false;
            }
        }

        /// <summary>
        /// Уменьшает графики
        /// </summary>
        private void _decreaseButton_Click(object sender, System.EventArgs e)
        {
            this._increaseButton.Enabled = true;
            this._zoom /= 2;    

            this.SetScrollBarValues();
            this.ApplyChanges();     

            if (this._zoom == ChartsXZoomer.MIN_ZOOM)
            {
                this._decreaseButton.Enabled = false;
                this._scrollBar.Visible = false;
            }
        }
        #endregion [Events Handlers]


        #region [Help members]
        /// <summary>
        /// Устанавливает значения полосы прокрутки
        /// </summary>
        private void SetScrollBarValues()
        {
            this._scrollBar.Minimum = 0;
            this._scrollBar.Maximum = (this._zoom-1)*2;
            this._scrollBar.Value = 0;
        }

        /// <summary>
        /// Присваивает всем элементам текущие значения xMax , xMin
        /// </summary>
        private void ApplyChanges()
        {
            //размер видимого куска
            var currentLenght = this._maxX/this._zoom;
            var currentXMin = this._scrollBar.Value*currentLenght/2;
            var currentXMax = currentXMin + currentLenght;

            foreach (var chart in this._charts)
            {
                chart.XMax = currentXMax;
                chart.CoordinateXOrigin = currentXMin;
            
                chart.XMin = currentXMin;
                chart.Update();
            }

            foreach (var marker in this._markers)
            {
                marker.Maximum = currentXMax;
                marker.Minimum = currentXMin;
            }
            this.SetTrackBarValue(this._markers[0],this._marker1Value);
            this.SetTrackBarValue(this._markers[1], this._marker2Value);
        }
        /// <summary>
        /// Восстанавливает положение ползунка
        /// </summary>
        private void SetTrackBarValue(TrackBar trackBar, int value)
        {
            if((value >= trackBar.Minimum)&(value <= trackBar.Maximum))
            {
                trackBar.Value = value;
                return;
            }
            if (value > trackBar.Maximum)
            {
                trackBar.Value = trackBar.Maximum;
            }
        }
        #endregion [Help members]


        public int Delta
        {
            get { return Math.Abs(this._marker1Value - this._marker2Value); }
        }
    }
}
