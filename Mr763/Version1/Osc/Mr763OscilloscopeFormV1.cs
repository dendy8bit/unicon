using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Interfaces;
using BEMN.Mr763.Version1.Measuring.Structures;
using BEMN.Mr763.Version1.Osc.HelpClasses;
using BEMN.Mr763.Version1.Osc.Loaders;
using BEMN.Mr763.Version1.Osc.ShowOsc;
using BEMN.Mr763.Version1.Osc.Structures;

namespace BEMN.Mr763.Version1.Osc
{
    public partial class Mr763OscilloscopeFormV1 : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]
        
        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoaderV1 _pageLoaderV1;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoaderV1 _oscJournalLoaderV1;

        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly MemoryEntity<MeasureTransStruct> _measuringTrans;
        
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingListV1 _countingListV1;

        private Mr763Device _device;
        private OscJournalStructV1 _journalStructV1;
        private readonly DataTable _table;
        private OscOptionsLoader _oscopeOptionsLoader;
        #endregion [Private fields]
        
        #region [Ctor's]
        public Mr763OscilloscopeFormV1()
        {
            InitializeComponent();
        }

        public Mr763OscilloscopeFormV1(Mr763Device device)
        {
            InitializeComponent();
            this._device = device;
            //��������� �������
            this._pageLoaderV1 = new OscPageLoaderV1(device.SetOscStartPage, device.OscPage);
            this._pageLoaderV1.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoaderV1.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, OscReadOk);
            this._pageLoaderV1.OscReadStopped += HandlerHelper.CreateActionHandler(this, ReadStop);
          
            //��������� �������
            this._oscJournalLoaderV1 = new OscJournalLoaderV1(device.OscJournal, device.RefreshOscJournal, this._device.OscOptions);
            this._oscJournalLoaderV1.ReadRecordOk += HandlerHelper.CreateActionHandler(this, ReadRecord);
            this._oscJournalLoaderV1.ReadJournalFail += HandlerHelper.CreateActionHandler(this, FailReadOscJournal);
            this._oscJournalLoaderV1.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);

            //������������ �����������
            this._oscopeOptionsLoader = device.OscopeOptionsLoader;
            this._oscopeOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscJournalLoaderV1.StartReadJournal);
            this._oscopeOptionsLoader.LoadFail += HandlerHelper.CreateActionHandler(this, FailReadOscJournal);

            //��������� ������� �����
            this._measuringTrans = device.MeasureTransOsc;
            this._measuringTrans.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this._oscopeOptionsLoader.StartRead);
            this._measuringTrans.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.FailReadOscJournal);
            
            this._table = this.GetJournalDataTable();

        }
        #endregion [Ctor's]
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr763Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr763OscilloscopeFormV1); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr763OscilloscopeFormV1.OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]

        #region [Help Classes Events Handlers]
        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            _stopReadOsc.Enabled = false;
            _oscProgressBar.Value = 0;
            this.EnableButtons = true;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoaderV1.RecordNumber == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }

            this.EnableButtons = true;
        }

        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }

        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
            int number = this._oscJournalLoaderV1.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
            if (!this.CanSelectOsc)
            {
                this.CanSelectOsc = true;
            }
            this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
            this._table.Rows.Add(this._oscJournalLoaderV1.GetRecord);
            this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            try
            {
                this.CountingListV1 = new CountingListV1(this._pageLoaderV1.ResultArray, this._journalStructV1, 
                    this._measuringTrans.Value, this._oscopeOptionsLoader.OscOptions.ChannelsInWords, this._device.DeviceVersion);
            }
            catch (Exception)
            {
                MessageBox.Show("������ ������������� ���������� ��� �������", "��������", MessageBoxButtons.OK,
                      MessageBoxIcon.Error);
                this.EnableButtons = true;
            }

            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._stopReadOsc.Enabled = false;
            this._oscSaveButton.Enabled = true;
        }

        #endregion [Help Classes Events Handlers]
        
        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingListV1 CountingListV1
        {
            get { return _countingListV1; }
            set
            {
                this._countingListV1 = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]
        
        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��763_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]
        
        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingListV1 == null)
            {
                this.CountingListV1 = new CountingListV1(new ushort[12000],new OscJournalStructV1(), new MeasureTransStruct(), new ushort[24], this._device.DeviceVersion);
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this.CountingListV1.IsLoad)
                {
                    fileName = this.CountingListV1.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��763 v{this._device.DeviceVersion} �������������");
                    this._countingListV1.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
               var resForm = new MR763OscilloscopeResultForm(this.CountingListV1, this._oscJournalLoaderV1.OscSizeOptions);
            resForm.Show();
            }
            
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }
        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoaderV1.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._measuringTrans.LoadStruct();
        }
      

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStructV1 = this._oscJournalLoaderV1.OscRecords[selectedOsc];
            this._pageLoaderV1.StartRead(this._journalStructV1, this._oscJournalLoaderV1.OscSizeOptions);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoaderV1.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        {
            this._saveOscilloscopeDlg.FileName = "������������� ��763";
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = this._saveOscilloscopeDlg.FileName.Replace(".hdr", $"[v{this._device.DeviceVersion}].hdr");
                this._countingListV1.Save(fileName);
                this._statusLabel.Text = "������������ ���������";
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                this.CountingListV1 = CountingListV1.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoaderV1.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            _oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            _oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }

        #endregion [Event Handlers]
        
    }
}