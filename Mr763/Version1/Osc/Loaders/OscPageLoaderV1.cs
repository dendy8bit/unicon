﻿using System;
using System.Collections.Generic;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Mr763.Version1.Osc.Structures;

namespace BEMN.Mr763.Version1.Osc.Loaders
{
    /// <summary>
    /// Класс загрузки страниц осциллограммы
    /// </summary>
    public class OscPageLoaderV1
    {
        #region [Private fields]
        /// <summary>
        /// Установка начальной страницы осциллограммы
        /// </summary>
        private readonly MemoryEntity<SetOscStartPageStruct> _setStartPage;
        /// <summary>
        /// Страница осциллограммы
        /// </summary>
        private readonly MemoryEntity<OscPage> _oscPage;
        /// <summary>
        /// Номер начальной страницы осцилограммы
        /// </summary>
        private int _startPage;
        /// <summary>
        /// Номер конечной страницы осцилограммы
        /// </summary>
        private int _endPage;
        /// <summary>
        /// Список страниц
        /// </summary>
        private List<OscPage> _pages;
        /// <summary>
        /// Запись о текущей осцилограмме
        /// </summary>
        private OscJournalStructV1 _journalStructV1;
        /// <summary>
        /// Параматры осцилографа
        /// </summary>
        private OscOptionsStruct _oscOptions;
        /// <summary>
        /// Флаг остановки загрузки
        /// </summary>
        private bool _needStop;
        /// <summary>
        /// Индекс начала осцилограммы в первой странице
        /// </summary>
        private int _startWordIndex;
        /// <summary>
        /// Количество страниц которые нужно прочитать
        /// </summary>
        private int _pageCount;
        /// <summary>
        /// Конечный размер осцилограммы в словах
        /// </summary>
        private int _resultLenInWords;
        #endregion [Private fields]


        #region [Events]
        /// <summary>
        /// Прочитана одна страница
        /// </summary>
        public event Action PageRead;
        /// <summary>
        /// Осцилограмма загружена успешно
        /// </summary>
        public event Action OscReadSuccessful;
        /// <summary>
        /// Чтение осцилограммы прекращено
        /// </summary>
        public event Action OscReadStopped;
        #endregion [Events]
        

        #region [Ctor's]
        public OscPageLoaderV1(MemoryEntity<SetOscStartPageStruct> setStartPage, MemoryEntity<OscPage> oscPage)
        {
            this._setStartPage = setStartPage;
            this._oscPage = oscPage;

            //Установка начальной страницы осциллограммы
            this._setStartPage.AllWriteOk += o => this._oscPage.LoadStruct();

            //Страницы осциллограммы
            this._oscPage.AllReadOk += HandlerHelper.CreateReadArrayHandler(this.PageReadOk);
        } 
        #endregion [Ctor's]


        #region [Public members]

        /// <summary>
        /// Запускает чтение указанной осцилограммы
        /// </summary>
        /// <param name="journalStructV1">Запись журнала о осцилограмме</param>
        /// <param name="oscOptions">Параматры осцилографа</param>
        public void StartRead(OscJournalStructV1 journalStructV1, OscOptionsStruct oscOptions)
        {
            
            this._needStop = false;
            this._oscOptions = oscOptions;
            this._journalStructV1 = journalStructV1;
            this._startPage = journalStructV1.OscStartIndex;

            this._startWordIndex = this._journalStructV1.Point % this._oscOptions.PageSize;
            //Конечный размер осцилограммы в словах
            this._resultLenInWords = this._journalStructV1.Len * this._journalStructV1.SizeReference;
            //Количесто слов которые нужно прочитать
            double lenOscInWords = this._resultLenInWords + this._startWordIndex ;
            //Количество страниц которые нужно прочитать
            this._pageCount = (int)Math.Ceiling(lenOscInWords / this._oscOptions.PageSize);


            this._endPage = this._startPage + this._pageCount;
            this._pages = new List<OscPage>();
            this.WritePageNumber((ushort) this._startPage);
        }
        /// <summary>
        /// Останавливает чтение осцилограммы
        /// </summary>
        public void StopRead()
        {
            this._needStop = true;
        }
        #endregion [Public members]


        #region [Event Raise Members]
        /// <summary>
        /// Вызывает событие "Чтение осцилограммы прекращено"
        /// </summary>
        private void OnRaiseOscReadStopped()
        {
            if (this.OscReadStopped != null)
            {
                this.OscReadStopped.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Осцилограмма загружена успешно"
        /// </summary>
        private void OnRaiseOscReadSuccessful()
        {
            if (this.OscReadSuccessful != null)
            {
                this.OscReadSuccessful.Invoke();
            }
        }
        /// <summary>
        /// Вызывает событие "Прочитана одна страница"
        /// </summary>
        private void OnRaisePageRead()
        {
            if (this.PageRead != null)
            {
                this.PageRead.Invoke();
            }
        } 
        #endregion [Event Raise Members]


        #region [Help members]
        /// <summary>
        /// Страница прочитана
        /// </summary>
        private void PageReadOk()
        {
            if (this._needStop)
            {
                this.OnRaiseOscReadStopped();        
                return;
            }
            this._pages.Add(this._oscPage.Value.Clone<OscPage>());
            this._startPage++;
            //Читаем пока не дойдём до последней страницы.
            if (this._startPage < this._endPage)
            {
                //Если вылазим за пределы размера осцилографа - начинаем читать с нулевой страницы
                if (this._startPage < this._oscOptions.FullOscSizeInPages)
                {
                    this.WritePageNumber((ushort) this._startPage);
                }
                else
                {
                    this.WritePageNumber((ushort) (this._startPage - this._oscOptions.FullOscSizeInPages));
                }
            }
            else
            {
                this.OscReadComplite();
                this.OnRaiseOscReadSuccessful();

            }
            this.OnRaisePageRead();

        }

        /*
        private void UshortArrayAddCountingList(List<ushort[]> destanationList, ushort[] sourseArray)
        {
            int count = sourseArray.Length/18;
            for (int i = 0; i < count; i++)
            {
                var newCounting = new ushort[18];
                Array.ConstrainedCopy(sourseArray,i*18,newCounting,0,18);
                destanationList.Add(newCounting);
            }
        }*/

        /// <summary>
        /// Осцилограмма прочитана
        /// </summary>
        private void OscReadComplite()
        {

            //Результирующий массив
            var resultMassiv = new ushort[this._resultLenInWords];
            //Если осцилограмма начинается с начала страницы
    /*        if (this._startWordIndex == 0)
            {
                int destanationIndex = 0;
                for (int i = 0; i < this._pages.Count-1; i++)
                {
                    var pageValue = this._pages[i].Words;
                    Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                    destanationIndex += pageValue.Length;
                }
                var endPage = this._pages[this._pages.Count - 1];
                Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex, this._resultLenInWords -destanationIndex);
            }*/
         //   else
        //    {
                var startPageValueArray = this._pages[0].Words;
                int destanationIndex = 0;


                //Копируем часть первой страницы
                Array.ConstrainedCopy(startPageValueArray, this._startWordIndex, resultMassiv, destanationIndex, startPageValueArray.Length - this._startWordIndex);
                destanationIndex += startPageValueArray.Length - this._startWordIndex;

                for (int i = 1; i < this._pages.Count - 1; i++)
                {
                    var pageValue = this._pages[i].Words;
                    Array.ConstrainedCopy(pageValue, 0, resultMassiv, destanationIndex, pageValue.Length);
                    destanationIndex += pageValue.Length;
                }
                var endPage = this._pages[this._pages.Count - 1];
                Array.ConstrainedCopy(endPage.Words, 0, resultMassiv, destanationIndex, this._resultLenInWords - destanationIndex);
         //   }

            //----------------------------------ПЕРЕВОРОТ---------------------------------//
            int c;
            var b = (this._journalStructV1.Len - this._journalStructV1.After) * this._journalStructV1.SizeReference;            //b = LEN – AFTER (рассчитывается в отсчётах, далее в словах, переведём в слова)
            if (this._journalStructV1.Begin < this._journalStructV1.Point)              // Если BEGIN меньше POINT, то:
            {
                                                                                    //c = BEGIN + OSCSIZE - POINT  
                c = this._journalStructV1.Begin + this._oscOptions.LoadedFullOscSizeInWords - this._journalStructV1.Point;      
            }
            else                                                                    //Если BEGIN больше POINT, то:
            {
                c = this._journalStructV1.Begin - this._journalStructV1.Point;          //c = BEGIN – POINT
            }
            var start = c - b;                                                      //START = c – b
            if (start < 0)                                                          //Если START меньше 0, то:
            {
                start += this._journalStructV1.Len*this._journalStructV1.SizeReference; //START = START + LEN•REZ
            }
            //-----------------------------------------------------------------------------//
            
            //Перевёрнутый массив
            var invertedMass = new ushort[this._resultLenInWords];
            Array.ConstrainedCopy(resultMassiv, start, invertedMass, 0, resultMassiv.Length - start);
            Array.ConstrainedCopy(resultMassiv, 0, invertedMass, invertedMass.Length - start, start);
            this.ResultArray = invertedMass;
        }
        /// <summary>
        /// Записывает номер желаемой страницы
        /// </summary>
        private void WritePageNumber(ushort pageNumber)
        {
            this._setStartPage.Value.PageIndex = pageNumber;
            this._setStartPage.SaveStruct();
        } 
        #endregion [Help members]


        #region [Properties]
        /// <summary>
        /// Количество страниц осцилограммы
        /// </summary>
        public int PagesCount
        {
            get { return this._pageCount; }
        }
        /// <summary>
        /// Готовый массив осц
        /// </summary>
        public ushort[] ResultArray { get; private set; }
        #endregion [Properties]
    }
}
