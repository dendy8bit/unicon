﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.Mr763.Version1.Measuring.Structures
{
    /// <summary>
    /// Конфигурациия измерительного трансформатора
    /// </summary>
    public class KanalTransStruct : StructBase
    {
       #region [Private fields]
        [Layout(0)] private ushort _ittl; //конфигурация ТТ - номинальный первичный ток, конфигурация ТН - коэфициэнт
        [Layout(1)] private ushort _ittx;//конфигурация ТТНП - номинальный первичный ток нулувой последовательности, конфигурация ТННП - коэфициэнт
        [Layout(2)] private ushort _ittx1; //конфигурация - номинальный первичный ток In1, конфигурация - коэфициэнт Un1
        [Layout(3)] private ushort _polarityL; //резерв, вход внешней неисправности тн (трансформатора напряжения)
        [Layout(4)] private ushort _polarityX;//резерв, вход внешней неисправности тн (трансформатора напряжения нулевой последовательности)
        [Layout(5)] private ushort _binding; //(для трансформатора тока тип ТТ, для трансформатора напряжения тип ТН)
        [Layout(6)] private ushort _imax; //max ток нагрузки,резерв
        [Layout(7)] private ushort _rez; //резерв 
        #endregion [Private fields]


        #region [Properties]
        /// <summary>
        /// конфигурация ТТ
        /// </summary>
        public ushort Ittl
        {
            get { return this._ittl; }
            set { this._ittl = value; }
        }

        /// <summary>
        /// конфигурация ТТНП
        /// </summary>
        public ushort Ittx
        {
            get { return this._ittx; }
            set { this._ittx = value; }
        }
        
        /// <summary>
        /// KTHL Полное значение
        /// </summary>
        public double KthlValue
        {
            get
            {
                double value = ValuesConverterCommon.GetKth(this._ittl);
                return Common.GetBit(this._ittl, 15)
                           ? value * 1000
                           : value;
            }
        }

        /// <summary>
        /// KTHX Полное значение
        /// </summary>
        public double KthxValue
        {
            get
            {
                double value = ValuesConverterCommon.GetKth(this._ittx);
                return Common.GetBit(this._ittx, 15)
                           ? value * 1000
                           : value;
            }
        }

        /// <summary>
        /// KTHX1 Полное значение
        /// </summary>
        public double Kthx1Value
        {
            get
            {
                double value = ValuesConverterCommon.GetKth(this._ittx1);
                return Common.GetBit(this._ittx1, 15)
                           ? value * 1000
                           : value;
            }
        }
        #endregion [Properties]
    }
}
