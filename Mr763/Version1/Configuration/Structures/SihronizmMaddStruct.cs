﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.MBServer;

namespace BEMN.Mr763.Version1.Configuration.Structures
{
    /// <summary>
    /// дополнительная конфигурациия улавливания синхронизма (УС) для ручного, автоматического включения
    /// </summary>
    public class SihronizmMaddStruct : StructBase
    {
        [Layout(0)] ushort _config;
        [Layout(1)] ushort _ustUdelta;				//уставка max разность напряжения
        //уставки синхронного включения (режима)
        [Layout(2)] ushort _fs;						//допустимая разность частот
        [Layout(3)] ushort _cs;						//допустимая разность фаз
        //уставки несинхронного включения (режима)
        [Layout(4)] ushort _fa;						//допустимая разность частот
        [Layout(5)] ushort _rez1;

        /// <summary>
        /// Синхронное включение dF, Гц
        /// </summary>
        public double DfSinhr
        {
            get { return ValuesConverterCommon.GetU(this._fs); }
            set { this._fs = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Несинхронное включение dF, Гц
        /// </summary>
        public double DfNosinhr
        {
            get { return ValuesConverterCommon.GetU(this._fa); }
            set { this._fa = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// dFi, град
        /// </summary>
        public ushort Dfi
        {
            get { return this._cs; }
            set { this._cs = value; }
        }

        /// <summary>
        /// U1 нет, U2 нет
        /// </summary>
        public string NoNo
        {
            get
            {
                var index = Common.GetBits(this._config, 3) >> 3;
                return StringsConfig.YesNo[index];
            }
            set
            {
                var index = (ushort) StringsConfig.YesNo.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 3);
            }
        }


        /// <summary>
        /// U1 есть, U2 нет
        /// </summary>
        public string YesNo
        {
            get
            {
                var index = Common.GetBits(this._config, 2) >> 2;
                return StringsConfig.YesNo[index];
            }
            set
            {
                var index = (ushort) StringsConfig.YesNo.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 2);
            }
        }

        /// <summary>
        /// U1 нет, U2 есть
        /// </summary>
        public string NoYes
        {
            get
            {
                var index = Common.GetBits(this._config, 1) >> 1;
                return StringsConfig.YesNo[index];
            }
            set
            {
                var index = (ushort) StringsConfig.YesNo.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 1);
            }
        }


        /// <summary>
        /// dUmax., В
        /// </summary>
        public double DUmax
        {
            get { return ValuesConverterCommon.GetU(this._ustUdelta); }
            set { this._ustUdelta = ValuesConverterCommon.SetU(value); }
        }

        /// <summary>
        /// Режим
        /// </summary>
        public string Mode
        {
            get
            {
                var index = Common.GetBits(this._config, 0) >> 0;
                return StringsConfig.OffOn[index];
            }
            set
            {
                var index = (ushort) StringsConfig.OffOn.IndexOf(value);
                this._config = Common.SetBits(this._config, index, 0);
            }
        }
    }
}
