﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;
using BEMN.Devices;
using System.Collections;
using AssemblyResources;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.StructHelperClasses.Interfaces;
using BEMN.Forms.TreeView;
using BEMN.Forms.ValidatingClasses.ControlsSupportClasses;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr763.Properties;
using BEMN.Mr763.Version1.Configuration.Structures;
using BEMN.Mr763.Version1.OldClasses;
using BEMN.Mr763.Version1.Osc.Structures;
using System.Collections.Generic;

namespace BEMN.Mr763.Version1.Configuration
{
    public partial class Mr763ConfigurationForm : Form, IFormView
    {
        #region Поля
        private Mr763Device _device;
        private ComboBox[] _switchCombos,
                            _paramMeasureCombos,
                           _apvCombos,
                           _avrCombos,
                           _lzhCombos,
                           _paramI2I1Combos,

                           _OSCCombos;
        private MaskedTextBox[] _ULONGSwitchMasketTextBoxes,
                                _ULONGapvTransMasketTextBoxes,
                                _ULONGavrTransMasketTextBoxes,

                                _ULONGparamMeasureMasketTextBoxes,
                                _ULONGparamI2I1MasketTextBoxes,

                                _DOUBLESwitchMasketTextBoxes,

                                _DOUBLEparamMeasureMasketTextBoxes,
                                _DOUBLElzhTransMasketTextBoxes,
                                _DOUBLEparamI2I1MasketTextBoxes,

                                _ULONGOSCMasketTextBoxes;
        CheckedListBox[] _VLSChectboxlistArray;
        DataGridView[] _inpSygnalsArray;

        private bool _validatingOk = true;
        private bool _validatingGB = true;

        private int[] ErrorCoord;
        private TabPage ErrorPage;
        private TabPage ErrorMainPage;
        private DataGridView ErrorGrid;
        private string ErrorMes;

        private RadioButtonSelector _groupSelector;

        private List<CheckedListBox> allVlsCheckedListBoxs = new List<CheckedListBox>();
        private List<DataGridView> dataGridsViewLsAND = new List<DataGridView>();
        private List<DataGridView> dataGridsViewLsOR = new List<DataGridView>();

        #endregion

        #region Конструкторы
        public Mr763ConfigurationForm()
        {
            this.InitializeComponent();
        }

        public Mr763ConfigurationForm(Mr763Device device)
        {
            this.InitializeComponent();
            this._device = device;
            if (Common.VersionConverter(this._device.DeviceVersion) < 1.02)
            {
                this._configurationTabControl.TabPages.RemoveByKey("tabPage18");
            }


            this._device.ConfigWriteFail += this._device_ConfigWriteFail;
            this._device.ConfigWriteOk += this._device_ConfigWriteOk;
            this._groupSelector = new RadioButtonSelector(this._mainRadioButton, this._reserveRadioButton, this._groupChangeButton);
            this._groupSelector.NeedCopyGroup += this._groupSelector_NeedCopyGroup;
        }

        void _groupSelector_NeedCopyGroup()
        {
            if (this._groupSelector.SelectedGroup == 0)
            {
                var res = new CURRENTPROTRESERVE(64);
                res.InitStruct(Common.TOBYTES(this._device.sProtmain.GetValues(), false));
                this._device.sProtreserve = res;
                WriteCurrentProtReserve();
            }
            else
            {
                var res = new CURRENTPROTMAIN(64);
                res.InitStruct(Common.TOBYTES(this._device.sProtreserve.GetValues(), false));
                this._device.sProtmain = res;
                WriteCurrentProtMain();
            }
        }

        private bool writeComplite = false;
        void _device_ConfigWriteOk()
        {
            this._processLabel.Text = "Конфигурация записана";
            if (this.writeComplite)
            {
                this.writeComplite = false;
                MessageBox.Show("Конфигурация записана");
            }

        }

        void _device_ConfigWriteFail()
        {
            this._processLabel.Text = "Ошибка записи конфигурации";
            MessageBox.Show("Ошибка записи конфигурации");
        }


        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr763Device); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(Mr763Device); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.config.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Конфигурация"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion

        #region Функции чтения конфигурации
        #region SWITCH
        private void PrepareSwich()
        {
            this._switchCombos = new ComboBox[] {
                this._switchOff, this._switchOn, this._switchError, this._switchBlock , this._switchKontCep, this._switchKeyOn, this._switchKeyOff, this._switchVneshOn, this._switchVneshOff, this._switchButtons, this._switchKey, this._switchVnesh, this._switchSDTU};

            this._ULONGSwitchMasketTextBoxes = new MaskedTextBox[] {this._switchTUrov, this._switchImp, this._switchTUskor, this._sinhrTwait, this._sinhrTsinhr, this._sinhrTon, this._sinhrManualdFi, this._sinhrAutodFi };
            this._DOUBLESwitchMasketTextBoxes = new MaskedTextBox[] {this._switchIUrov, this._sinhrUminOts, this._sinhrUminNal, this._sinhrUmaxNal, this._sinhrManualUmax, this._sinhrManualdF, this._sinhrManualdFno, this._sinhrAutoUmax, this._sinhrAutodF, this._sinhrAutodFno };

            this.ClearCombos(this._switchCombos);

            this.FillSwitchCombos();

            this.SubscriptCombos(this._switchCombos);

            this.PrepareMaskedBoxes(this._ULONGSwitchMasketTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLESwitchMasketTextBoxes, typeof(double));
        }

        private void FillSwitchCombos()
        {
            this._switchOff.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchOn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchError.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchBlock.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchKontCep.Items.AddRange(Strings.ModesLight.ToArray());

            this._switchKeyOn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchKeyOff.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchVneshOn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchVneshOff.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._switchButtons.Items.AddRange(Strings.Forbidden.ToArray());
            this._switchKey.Items.AddRange(Strings.Forbidden2.ToArray());
            this._switchVnesh.Items.AddRange(Strings.Forbidden2.ToArray());
            this._switchSDTU.Items.AddRange(Strings.Forbidden.ToArray());
        }

        private void ReadSwich()
        {
            this._switchOff.SelectedItem = this._device.SwitchOff;
            this._switchOn.SelectedItem = this._device.SwitchOn;
            this._switchError.SelectedItem = this._device.SwitchError;
            this._switchBlock.SelectedItem = this._device.SwitchBlock;
            this._switchTUrov.Text = this._device.SwitchTUrov.ToString();
            this._switchIUrov.Text = this._device.SwitchIUrov.ToString();
            this._switchImp.Text = this._device.SwitchImpuls.ToString();
            this._switchTUskor.Text = this._device.SwitchTUskor.ToString();
            this._switchKontCep.SelectedItem = this._device.SwitchKontCep;

            this._switchKeyOn.SelectedItem = this._device.SwitchKeyOn;
            this._switchKeyOff.SelectedItem = this._device.SwitchKeyOff;
            this._switchVneshOn.SelectedItem = this._device.SwitchVneshOn;
            this._switchVneshOff.SelectedItem = this._device.SwitchVneshOff;
            this._switchButtons.SelectedItem = this._device.SwitchButtons;
            this._switchKey.SelectedItem = this._device.SwitchKey;
            this._switchVnesh.SelectedItem = this._device.SwitchVnesh;
            this._switchSDTU.SelectedItem = this._device.SwitchSDTU;
        }

        public bool WriteSwich()
        {
            bool ret = true;
            try
            {
                this._device.SwitchOff = this._switchOff.SelectedItem.ToString();
                this._device.SwitchOn = this._switchOn.SelectedItem.ToString();
                this._device.SwitchError = this._switchError.SelectedItem.ToString();
                this._device.SwitchBlock = this._switchBlock.SelectedItem.ToString();
                this._device.SwitchTUrov = Convert.ToInt32(this._switchTUrov.Text);
                this._device.SwitchIUrov = Convert.ToDouble(this._switchIUrov.Text);
                this._device.SwitchImpuls = Convert.ToInt32(this._switchImp.Text);
                this._device.SwitchTUskor = Convert.ToInt32(this._switchTUskor.Text);
                this._device.SwitchKontCep = this._switchKontCep.SelectedItem.ToString();

                this._device.SwitchKeyOn = this._switchKeyOn.SelectedItem.ToString();
                this._device.SwitchKeyOff = this._switchKeyOff.SelectedItem.ToString();
                this._device.SwitchVneshOn = this._switchVneshOn.SelectedItem.ToString();
                this._device.SwitchVneshOff = this._switchVneshOff.SelectedItem.ToString();
                this._device.SwitchButtons = this._switchButtons.SelectedItem.ToString();
                this._device.SwitchKey = this._switchKey.SelectedItem.ToString();
                this._device.SwitchVnesh = this._switchVnesh.SelectedItem.ToString();
                this._device.SwitchSDTU = this._switchSDTU.SelectedItem.ToString();
                this._device.MemoryMap["Выключатель"].StructObj = this._device.sSwitch;
            }
            catch (Exception ee)
            {
                ret = false;
                MessageBox.Show("Произошла ошибка во время записи блока Выключатель.");
            }
            return ret;
        }
        #endregion

        #region APV
        public void PrepareApv()
        {
            this._apvCombos = new ComboBox[] {
                this._apvModes, this._apvBlocking, this._apvOff };

            this._ULONGapvTransMasketTextBoxes = new MaskedTextBox[] {this._apvTBlock, this._apvTReady, this._apv1Krat, this._apv2Krat, this._apv3Krat, this._apv4Krat };

            this.ClearCombos(this._apvCombos);

            this.FillApvCombos();

            this.SubscriptCombos(this._apvCombos);

            this.PrepareMaskedBoxes(this._ULONGapvTransMasketTextBoxes, typeof(ulong));
        }

        public void FillApvCombos()
        {
            this._apvModes.Items.AddRange(Strings.APVMode.ToArray());
            this._apvBlocking.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._apvOff.Items.AddRange(Strings.YesNo.ToArray());
        }

        private void ReadAPV()
        {
            this._apvModes.SelectedItem = this._device.APVModes;
            this._apvBlocking.SelectedItem = this._device.APVBlocking;
            this._apvTBlock.Text = this._device.APVTBlock.ToString();
            this._apvTReady.Text = this._device.APVTReady.ToString();
            this._apv1Krat.Text = this._device.APV1Krat.ToString();
            this._apv2Krat.Text = this._device.APV2Krat.ToString();
            this._apv3Krat.Text = this._device.APV3Krat.ToString();
            this._apv4Krat.Text = this._device.APV4Krat.ToString();
            this._apvOff.SelectedItem = this._device.APVOff;
        }

        public bool WriteApv()
        {
            bool ret = true;
            try
            {
                this._device.APVModes = this._apvModes.SelectedItem.ToString();
                this._device.APVBlocking = this._apvBlocking.SelectedItem.ToString();
                this._device.APVTBlock = Convert.ToInt32(this._apvTBlock.Text);
                this._device.APVTReady = Convert.ToInt32(this._apvTReady.Text);
                this._device.APV1Krat = Convert.ToInt32(this._apv1Krat.Text);
                this._device.APV2Krat = Convert.ToInt32(this._apv2Krat.Text);
                this._device.APV3Krat = Convert.ToInt32(this._apv3Krat.Text);
                this._device.APV4Krat = Convert.ToInt32(this._apv4Krat.Text);
                this._device.APVOff = this._apvOff.SelectedItem.ToString();
                this._device.MemoryMap["АПВ"].StructObj = this._device.sApv;
            }
            catch (Exception ee)
            {
                ret = false;
                MessageBox.Show("Произошла ошибка во время записи блока АПВ.");
            }
            return ret;
        }
        #endregion

        #region AVR
        public void PrepareAvr()
        {
            this._avrCombos = new ComboBox[] {
                this._avrBySignal, this._avrByOff, this._avrBySelfOff, this._avrByDiff, this._avrSIGNOn, this._avrBlocking, this._avrBlockClear, this._avrResolve, this._avrBack, this._avrClear
                                          };

            this._ULONGavrTransMasketTextBoxes = new MaskedTextBox[] { };

            this.ClearCombos(this._avrCombos);

            this.FillAvrCombos();

            this.SubscriptCombos(this._avrCombos);

            this.PrepareMaskedBoxes(this._ULONGavrTransMasketTextBoxes, typeof(ulong));
        }

        public void FillAvrCombos()
        {
            this._avrBySignal.Items.AddRange(Strings.YesNo.ToArray());
            this._avrByOff.Items.AddRange(Strings.YesNo.ToArray());
            this._avrBySelfOff.Items.AddRange(Strings.YesNo.ToArray());
            this._avrByDiff.Items.AddRange(Strings.YesNo.ToArray());
            this._avrSIGNOn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._avrBlocking.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._avrBlockClear.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._avrResolve.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._avrBack.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._avrClear.Items.AddRange(Strings.Forbidden.ToArray());
        }

        private void ReadAVR()
        {
            this._avrBySignal.SelectedItem = this._device.AVRBySignal;
            this._avrByOff.SelectedItem = this._device.AVRByOff;
            this._avrBySelfOff.SelectedItem = this._device.AVRBySelfOff;
            this._avrByDiff.SelectedItem = this._device.AVRByDiff;
            this._avrSIGNOn.SelectedItem = this._device.AVRSIGNOn;
            this._avrBlocking.SelectedItem = this._device.AVRBlocking;
            this._avrBlockClear.SelectedItem = this._device.AVRBlockClear;
            this._avrResolve.SelectedItem = this._device.AVRResolve;
            this._avrTSr.Text = this._device.AVRTimeOn.ToString();
            this._avrBack.SelectedItem = this._device.AVRBack;
            this._avrTBack.Text = this._device.AVRTimeBack.ToString();
            this._avrTOff.Text = this._device.AVRTimeOtkl.ToString();
            this._avrClear.SelectedItem = this._device.AVRClear;
        }

        public bool WriteAvr()
        {
            bool ret = true;
            try
            {
                this._device.AVRBySignal = this._avrBySignal.SelectedItem.ToString();
                this._device.AVRByOff = this._avrByOff.SelectedItem.ToString();
                this._device.AVRBySelfOff = this._avrBySelfOff.SelectedItem.ToString();
                this._device.AVRByDiff = this._avrByDiff.SelectedItem.ToString();
                this._device.AVRSIGNOn = this._avrSIGNOn.SelectedItem.ToString();
                this._device.AVRBlocking = this._avrBlocking.SelectedItem.ToString();
                this._device.AVRBlockClear = this._avrBlockClear.SelectedItem.ToString();
                this._device.AVRResolve = this._avrResolve.SelectedItem.ToString();
                this._device.AVRTimeOn = Convert.ToInt32(this._avrTSr.Text);
                this._device.AVRBack = this._avrBack.SelectedItem.ToString();
                this._device.AVRTimeBack = Convert.ToInt32(this._avrTBack.Text);
                this._device.AVRTimeOtkl = Convert.ToInt32(this._avrTOff.Text);
                this._device.AVRClear = this._avrClear.SelectedItem.ToString();
                this._device.MemoryMap["АВР"].StructObj = this._device.sAvr;
            }
            catch (Exception ee)
            {
                ret = false;
                MessageBox.Show("Произошла ошибка во время записи блока АВР.");
            }
            return ret;
        }
        #endregion

        #region LPB
        public void PrepareLZH()
        {
            this._lzhCombos = new ComboBox[] {
                this._lzhModes
                                          };

            this._DOUBLElzhTransMasketTextBoxes = new MaskedTextBox[] {this._lzhVal };

            this.ClearCombos(this._lzhCombos);

            this.FillLZHCombos();

            this.SubscriptCombos(this._lzhCombos);

            this.PrepareMaskedBoxes(this._DOUBLElzhTransMasketTextBoxes, typeof(double));
        }

        public void FillLZHCombos()
        {
            this._lzhModes.Items.AddRange(Strings.LZHModes.ToArray());
        }

        private void ReadLPB()
        {
            this._lzhModes.SelectedItem = this._device.LZHMode;
            this._lzhVal.Text = this._device.LZHUstavka.ToString();
        }

        public bool WriteLzh()
        {
            bool ret = true;
            try
            {
                this._device.LZHMode = this._lzhModes.SelectedItem.ToString();
                this._device.LZHUstavka = Convert.ToDouble(this._lzhVal.Text);
                this._device.MemoryMap["ЛПБ"].StructObj = this._device.sLpb;
            }
            catch (Exception ee)
            {
                ret = false;
                MessageBox.Show("Произошла ошибка во время записи блока ЛЗШ.");
            }
            return ret;
        }
        #endregion

        #endregion

        #region Параметры измерений
        public void PreparePARAMMEASURE()
        {
            this._paramMeasureCombos = new ComboBox[] {
                this._TT_typeCombo, this._OMPmode_combo, this._Uo_typeCombo, this._KTHLkoef_combo , this._errorL_combo, this._KTHXkoef_combo, this._errorX_combo};

            this._ULONGparamMeasureMasketTextBoxes = new MaskedTextBox[] {this._ITTL_Box };
            this._DOUBLEparamMeasureMasketTextBoxes = new MaskedTextBox[] {this._Im_Box, this._KTHL_Box, this._KTHX_Box, this._Xline_Box };

            this.ClearCombos(this._paramMeasureCombos);

            this.FillParamMeasureCombos();

            this.SubscriptCombos(this._paramMeasureCombos);

            this.PrepareMaskedBoxes(this._ULONGparamMeasureMasketTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLEparamMeasureMasketTextBoxes, typeof(double));
        }

        private void FillParamMeasureCombos()
        {
            this._TT_typeCombo.Items.AddRange(Strings.TT_Type.ToArray());
            this._OMPmode_combo.Items.AddRange(Strings.ModesLight.ToArray());
            this._Uo_typeCombo.Items.AddRange(Strings.Uo_Type.ToArray());
            this._KTHLkoef_combo.Items.AddRange(Strings.Measure_Koef.ToArray());
            this._errorL_combo.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._KTHXkoef_combo.Items.AddRange(Strings.Measure_Koef.ToArray());
            this._KTHX1koef_combo.Items.AddRange(Strings.Measure_Koef.ToArray());
            this._errorX_combo.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            this._ITTL_Box.Text = "0";
            this._KTHX1_Box.Text = "0";
            this._Im_Box.Text = "0,0";
            this._KTHL_Box.Text = "0,0";
            this._KTHX_Box.Text = "0,0";
            this._Xline_Box.Text = "0,0";
        }

        public void Read_TT_TTNP()
        {
            this._TT_typeCombo.SelectedItem = this._device.TTtype;
            this._Im_Box.Text = this._device.Imax.ToString();
            this._ITTL_Box.Text = this._device.ITTL.ToString();


            this._Uo_typeCombo.SelectedItem = this._device.TNtype;
            this._KTHL_Box.Text = this._device.KTHL.ToString();
            this._KTHLkoef_combo.SelectedItem = this._device.KTHLKoeff;
            this._KTHX_Box.Text = this._device.KTHX.ToString();
            this._KTHXkoef_combo.SelectedItem = this._device.KTHXKoeff;

            this._KTHX1_Box.Text = this._device.KTHX1.ToString();
            this._KTHX1koef_combo.SelectedItem = this._device.KTHX1Koeff;

            this._errorL_combo.SelectedItem = this._device.PolarityL;
            this._errorX_combo.SelectedItem = this._device.PolarityX;
        }

        public void Read_OMP()
        {
            this._OMPmode_combo.SelectedItem = this._device.OMPtype;
            this._Xline_Box.Text = this._device.Hud.ToString();
        }

        public bool WritePARAMMEASURE()
        {
            bool ret = true;
            try
            {
                this._device.TTtype = this._TT_typeCombo.SelectedItem.ToString();
                this._device.Imax = Convert.ToDouble(this._Im_Box.Text);
                this._device.ITTL = Convert.ToUInt16(this._ITTL_Box.Text);

                this._device.TNtype = this._Uo_typeCombo.SelectedItem.ToString();
                this._device.KTHL = Convert.ToDouble(this._KTHL_Box.Text);
                this._device.KTHLKoeff = this._KTHLkoef_combo.SelectedItem.ToString();
                this._device.KTHX = Convert.ToDouble(this._KTHX_Box.Text);
                this._device.KTHX1 = Convert.ToDouble(this._KTHX1_Box.Text);
                this._device.KTHXKoeff = this._KTHXkoef_combo.SelectedItem.ToString();
                this._device.KTHX1Koeff = this._KTHX1koef_combo.SelectedItem.ToString();
                this._device.PolarityL = this._errorL_combo.SelectedItem.ToString();
                this._device.PolarityX = this._errorX_combo.SelectedItem.ToString();

                this._device.OMPtype = this._OMPmode_combo.SelectedItem.ToString();
                this._device.Hud = Convert.ToDouble(this._Xline_Box.Text);

                this._device.MemoryMap["ОМП"].StructObj = this._device.sConfOMP;
                this._device.MemoryMap["Измерительный_Трансформатор"].StructObj = this._device.sMeasuretrans;
            }
            catch (Exception ee)
            {
                ret = false;
            }
            return ret;
        }
        #endregion

        #region Реле и индикаторы
        private void PreparePARAMAUTOMAT()
        {
            DataGridViewComboBoxColumn[] _ar = new DataGridViewComboBoxColumn[] {
                this._releSignalCol, this._releTypeCol, this._outIndSignalCol, this._outIndTypeCol
                                                                                };

            this._outputReleGrid.Rows.Clear();
            this._outputIndicatorsGrid.Rows.Clear();
            this._releSignalCol.Items.Clear();
            this._releTypeCol.Items.Clear();
            this._outIndSignalCol.Items.Clear();
            this._outIndTypeCol.Items.Clear();

            this._releSignalCol.Items.AddRange(Strings.SygnalSrab.ToArray());
            this._releTypeCol.Items.AddRange(Strings.SygnalType.ToArray());
            this._outIndSignalCol.Items.AddRange(Strings.SygnalSrab.ToArray());
            this._outIndTypeCol.Items.AddRange(Strings.SygnalType.ToArray());

            for (int i = 0; i < Mr763Device.RELE_COUNT; i++)
            {
                this._outputReleGrid.Rows.Add(new object[] { i + 3,
                                                        "Повторитель",
                                                        "Нет",
                                                        0
                                                      });
            }
            this._outputIndicatorsGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.DataGrid_EditingControlShowing);
            this._outputReleGrid.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.DataGrid_EditingControlShowing);
            this._outputIndicatorsGrid.Rows.Clear();
            for (int i = 0; i < Mr763Device.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid.Rows.Add(new object[]{ i + 1,
                                                             "Повторитель",
                                                             "Нет",
                                                             "",
                                                            });
                this._outputIndicatorsGrid.Rows[i].Cells["_outIndColorCol"].Style.BackColor = Color.Red;

            }

            foreach (var _rowBox in _ar)
            {
                _rowBox.Items.Remove("ХХХХХ");
            }

            this._neispr1CB.SelectedIndex = 0;
            this._neispr2CB.SelectedIndex = 0;
            this._neispr3CB.SelectedIndex = 0;
            this._neispr4CB.SelectedIndex = 0;
            this._impTB.Text = "0";
        }

        private void ReadPARAMAUTOMAT()
        {
            try
            {
                for (int i = 0; i < Mr763Device.RELE_COUNT; i++)
                {
                    this._outputReleGrid[2, i].Value = Strings.SygnalSrab[this._device.OutputReleValues[i * 4]];
                    this._outputReleGrid[1, i].Value = Strings.SygnalType[this._device.OutputReleValues[i * 4 + 1]];

                    int val = Common.GetBits(this._device.OutputReleValues[i * 4 + 2], 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14) >> 0;
                    int ind = Common.GetBits(this._device.OutputReleValues[i * 4 + 2], 15);

                    if (ind == 0)
                    {
                        val *= 10;
                    }
                    else
                    {
                        val *= 100;
                    }

                    this._outputReleGrid[3, i].Value = val;
                }
            }
            catch { }

            int index = 0;
            for (int i = 0; i < Mr763Device.INDICATOR_COUNT; i++)
            {
                this._outputIndicatorsGrid[2, i].Value = Strings.SygnalSrab[this._device.OutputIndicatorValues[index]];
                bool type = BEMN.MBServer.Common.GetBit(this._device.OutputIndicatorValues[index + 1], 0);
                int typeInt;
                bool color = BEMN.MBServer.Common.GetBit(this._device.OutputIndicatorValues[index + 1], 8);
                if (type)
                {
                    typeInt = 1;
                }
                else
                {
                    typeInt = 0;
                }
                if (color)
                {
                    this._outputIndicatorsGrid[3, i].Style.BackColor = Color.Green;
                    this._outputIndicatorsGrid[3, i].Style.SelectionBackColor = this._outputIndicatorsGrid[3, i].Style.BackColor;
                }
                else
                {
                    this._outputIndicatorsGrid[3, i].Style.BackColor = Color.Red;
                    this._outputIndicatorsGrid[3, i].Style.SelectionBackColor = this._outputIndicatorsGrid[3, i].Style.BackColor;
                }
                this._outputIndicatorsGrid[1, i].Value = Strings.SygnalType[typeInt];
                index += 2;
            }

            this._neispr1CB.SelectedItem = this._device.OutputN1;
            this._neispr2CB.SelectedItem = this._device.OutputN2;
            this._neispr3CB.SelectedItem = this._device.OutputN3;
            this._neispr4CB.SelectedItem = this._device.OutputN4;
            this._impTB.Text = this._device.OutputImp.ToString();
        }

        private void WriteRele()
        {
            ushort[] res = new ushort[this._device.OutputReleValues.Length];
            for (int i = 0; i < Mr763Device.RELE_COUNT; i++)
            {
                res[i * 4] = (ushort)Strings.SygnalSrab.IndexOf(this._outputReleGrid[2, i].Value.ToString());
                res[i * 4 + 1] = (ushort)Strings.SygnalType.IndexOf(this._outputReleGrid[1, i].Value.ToString());

                ushort ind = 0;
                ushort val = (ushort)Convert.ToInt32(this._outputReleGrid[3, i].Value);
                if (Convert.ToInt32(this._outputReleGrid[3, i].Value) > 32767 * 10)
                {
                    ind = 1;
                    val = (ushort)(Convert.ToInt32(this._outputReleGrid[3, i].Value) / 100);
                }
                else
                {
                    val = (ushort)(val / 10);
                }
                if (Convert.ToInt32(this._outputReleGrid[3, i].Value) > 32767 && Convert.ToInt32(this._outputReleGrid[3, i].Value) < 32767 * 10)
                {
                    val = (ushort)(Convert.ToInt32(this._outputReleGrid[3, i].Value) / 10);
                }
                if (val > 32767)
                {
                    val = 32767;
                }
                res[i * 4 + 2] = Common.SetBits(this._device.OutputReleValues[i * 4 + 2], ind, 15);
                res[i * 4 + 2] = Common.SetBits(this._device.OutputReleValues[i * 4 + 2], val, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            }
            this._device.OutputReleValues = res;
        }

        public void WriteNeispr()
        {
            try
            {
                this._device.OutputN1 = this._neispr1CB.SelectedItem.ToString();
                this._device.OutputN2 = this._neispr2CB.SelectedItem.ToString();
                this._device.OutputN3 = this._neispr3CB.SelectedItem.ToString();
                this._device.OutputN4 = this._neispr4CB.SelectedItem.ToString();
                this._device.OutputImp = Convert.ToUInt16(this._impTB.Text);
            }
            catch
            {

            }
        }

        private void WriteIndicator()
        {
            ushort[] res = new ushort[this._device.OutputIndicatorValues.Length];
            int index = 0;
            for (int i = 0; i < Mr763Device.INDICATOR_COUNT; i++)
            {
                res[index] = (ushort)Strings.SygnalSrab.IndexOf(this._outputIndicatorsGrid[2, i].Value.ToString());

                if (this._outputIndicatorsGrid[1, i].Value.ToString() == "Блинкер")
                {
                    res[index + 1] = BEMN.MBServer.Common.SetBit(res[index + 1], 0, true);
                }
                else
                {
                    res[index + 1] = BEMN.MBServer.Common.SetBit(res[index + 1], 0, false);
                }

                if (this._outputIndicatorsGrid[3, i].Style.BackColor == Color.Green)
                {
                    res[index + 1] = BEMN.MBServer.Common.SetBit(res[index + 1], 8, true);
                }
                else
                {
                    res[index + 1] = BEMN.MBServer.Common.SetBit(res[index + 1], 8, false);
                }
                index += 2;
            }
            this._device.OutputIndicatorValues = res;
        }

        private bool WriteParamAutomat()
        {
            bool ret = true;
            try
            {
                this.WriteNeispr();
                this.WriteRele();
                this.WriteIndicator();
                this._device.MemoryMap["Автоматика"].StructObj = this._device.sParamautomat;
            }
            catch (Exception rr)
            {
                ret = false;
            }
            return ret;
        }

        void _outputIndicatorsGrid_Click(object sender, EventArgs e)
        {
            if (this._outputIndicatorsGrid.CurrentCell.ColumnIndex == 3)
            {
                if (this._outputIndicatorsGrid.CurrentCell.Style.BackColor == Color.Red)
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Green;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
                else
                {
                    this._outputIndicatorsGrid.CurrentCell.Style.BackColor = Color.Red;
                    this._outputIndicatorsGrid.CurrentCell.Style.SelectionBackColor = this._outputIndicatorsGrid.CurrentCell.Style.BackColor;
                }
            }
        }
        #endregion

        #region ВЛС
        public void PrepareELSSYGNAL()
        {
            this._VLSChectboxlistArray = new CheckedListBox[]{
                this.VLScheckedListBox1, this.VLScheckedListBox2, this.VLScheckedListBox3, this.VLScheckedListBox4, this.VLScheckedListBox5, this.VLScheckedListBox6, this.VLScheckedListBox7, this.VLScheckedListBox8, this.VLScheckedListBox9, this.VLScheckedListBox10, this.VLScheckedListBox11, this.VLScheckedListBox12, this.VLScheckedListBox13, this.VLScheckedListBox14, this.VLScheckedListBox15, this.VLScheckedListBox16};

            for (int i = 0; i < this._VLSChectboxlistArray.Length; i++)
            {
                if (this._VLSChectboxlistArray[i].Items.Count == 0)
                {
                    this._VLSChectboxlistArray[i].Items.AddRange(Strings.VLSSygnals.ToArray());
                    if (this._VLSChectboxlistArray[i].Items[this._VLSChectboxlistArray[i].Items.Count - 1].ToString() == "ХХХХХ")
                    {
                        this._VLSChectboxlistArray[i].Items.Remove(this._VLSChectboxlistArray[i].Items[this._VLSChectboxlistArray[i].Items.Count - 1]);
                    }
                }
            }

            allVlsCheckedListBoxs.Add(this.VLScheckedListBox1);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox2);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox3);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox4);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox5);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox6);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox7);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox8);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox9);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox10);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox11);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox12);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox13);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox14);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox15);
            allVlsCheckedListBoxs.Add(this.VLScheckedListBox16);
        }

        /// <summary>
        /// Запись внешних логических сигналов
        /// </summary>
        private BitArray WriteVls(CheckedListBox box)
        {
            var count = box.Items.Count;
            var bits = new bool[count];
            for (int i = 0; i < count; i++)
            {
                bits[i] = box.GetItemChecked(i);
            }
            return new BitArray(bits);
        }

        public bool WriteELSSYGNAL()
        {
            bool res = true;
            try
            {
                ELSSYGNAL signalStruct = new ELSSYGNAL(64);
                for (int i = 0; i < 16; i++)
                {
                    signalStruct.i[i].Bits = this.WriteVls(this._VLSChectboxlistArray[i]);
                }
                this._device.sElssignal = signalStruct;
                this._device.MemoryMap["Выходные_ЛС"].StructObj = this._device.sElssignal;
            }
            catch (Exception)
            {
                res = false;
            }
            return res;
        }

        /// <summary>
        /// Выводит значение одного ВЛС
        /// </summary>
        /// <param name="box">Контрол</param>
        /// <param name="bits">Данные</param>
        private void ShowVls(CheckedListBox box, BitArray bits)
        {
            for (int i = 0; i < box.Items.Count; i++)
            {
                box.SetItemChecked(i, bits[i]);
            }
        }

        public void ReadELSSYGNAL()
        {
            for (int i = 0; i < 16; i++)
            {
                this.ShowVls(this._VLSChectboxlistArray[i], this._device.sElssignal.i[i].Bits);
            }

            /*
            ushort[] _vls = _device.VLS;

            int byteIndex = 0;

            for (int i = 0; i < _device.sElssignal.i.Length; i++) //Счетчик закладок ВЛС 1-16
            {
                byteIndex = 16 * i; //индекс массива 1ВЛС = 16 байт

                for (int j = 0; j < Strings.SygnalSrab.Count; j += _device.sElssignal.i.Length) //Счетчик элементов одного ВЛС 1-125
                {
                    int itemIndex = j; //индекс элемента списка
                    for (int bitIndex = 0; bitIndex < 16; bitIndex++)//перебираем биты слова
                    {
                        if (itemIndex < (Strings.VLSSygnals.Count - 1))//Чтоб индекс элемента списка не зашкалил за длинну списка
                        {
                            _VLSChectboxlistArray[i].SetItemChecked(itemIndex, BEMN.MBServer.Common.GetBit(_vls[byteIndex], bitIndex));
                            itemIndex++;//переходим к сл. элементу списка
                        }
                    }
                    byteIndex++;//переходим к следующему байту массива данного ВЛС
                }
            }*/
        }
        #endregion

        #region И Или
        public void PrepareINPSIGNAL()
        {
            this._inpSygnalsArray = new DataGridView[]{
                this._inputSignals1, this._inputSignals2, this._inputSignals3, this._inputSignals4, this._inputSignals5, this._inputSignals6, this._inputSignals7, this._inputSignals8, this._inputSignals9, this._inputSignals10, this._inputSignals11, this._inputSignals12, this._inputSignals13, this._inputSignals14, this._inputSignals15, this._inputSignals16};

            for (int j = 0; j < this._inpSygnalsArray.Length; j++)
            {
                if (this._inpSygnalsArray[j].Rows.Count == 0)
                {
                    for (int i = 0; i < Strings.LogycSygnals.Count; i++)
                    {

                        if (this._signalValueCol.Items.Count == 0)
                        {
                            this._signalValueCol.Items.AddRange(Strings.LogycValues.ToArray());
                        }
                        this._inpSygnalsArray[j].Rows.Add("Д" + (i + 1).ToString(), Strings.LogycValues[0].ToString());
                    }
                }
            }

            dataGridsViewLsAND.Add(this._inputSignals1);
            dataGridsViewLsAND.Add(this._inputSignals2);
            dataGridsViewLsAND.Add(this._inputSignals3);
            dataGridsViewLsAND.Add(this._inputSignals4);
            dataGridsViewLsAND.Add(this._inputSignals5);
            dataGridsViewLsAND.Add(this._inputSignals6);
            dataGridsViewLsAND.Add(this._inputSignals7);
            dataGridsViewLsAND.Add(this._inputSignals8);

            dataGridsViewLsOR.Add(this._inputSignals9);
            dataGridsViewLsOR.Add(this._inputSignals10);
            dataGridsViewLsOR.Add(this._inputSignals11);
            dataGridsViewLsOR.Add(this._inputSignals12);
            dataGridsViewLsOR.Add(this._inputSignals13);
            dataGridsViewLsOR.Add(this._inputSignals14);
            dataGridsViewLsOR.Add(this._inputSignals15);
            dataGridsViewLsOR.Add(this._inputSignals16);
        }

        public void ReadINPSIGNAL()
        {
            int _inpSygnalsIndex = 0;
            ushort[] _inpSygnals = this._device.InpSygnals;
            for (int j = 0; j < this._inpSygnalsArray.Length; j++)
            {
                _inpSygnalsIndex = j * 6;
                int indexLs = 0;
                do
                {
                    for (int i = 0; i < 16; i += 2)
                    {
                        int IND = BEMN.MBServer.Common.GetBits(_inpSygnals[_inpSygnalsIndex], i, i + 1) >> i;
                        this._inpSygnalsArray[j][1, indexLs].Value = Strings.LogycValues[IND];
                        indexLs++;
                    }
                    _inpSygnalsIndex++;
                } while (indexLs < 40);
            }
        }

        public bool WriteINPSIGNAL()
        {
            bool ret = true;
            try
            {
                int _inpSygnalsIndex = 0;
                ushort[] _inpSygnals = new ushort[12 * 16 / 2];
                for (int j = 0; j < this._inpSygnalsArray.Length; j++)
                {
                    _inpSygnalsIndex = j * 6;
                    int indexLs = 0;
                    do
                    {
                        for (int i = 0; i < 16; i += 2)
                        {
                            _inpSygnals[_inpSygnalsIndex] = BEMN.MBServer.Common.SetBits(_inpSygnals[_inpSygnalsIndex], (ushort)Strings.LogycValues.IndexOf(this._inpSygnalsArray[j][1, indexLs].Value.ToString()), i, i + 1);
                            indexLs++;
                        }
                        _inpSygnalsIndex++;
                    } while (indexLs < 40);
                }
                this._device.InpSygnals = _inpSygnals;
                this._device.MemoryMap["Входные_ЛС"].StructObj = this._device.sInpsignal;
            }
            catch (Exception ee)
            {
                ret = false;
            }
            return ret;
        }
        #endregion

        #region Группа уставок и Сброс индикации
        public void PrepareINPUTSYGNAL()
        {
            if (this._grUstComboBox.Items.Count == 0)
            {
                this._grUstComboBox.Items.AddRange(Strings.SygnalInputSignals.ToArray());
                this._grUstComboBox.SelectedIndex = 0;
            }
            else
            {
                this._grUstComboBox.SelectedIndex = 0;
            }
            if (this._indComboBox.Items.Count == 0)
            {
                this._indComboBox.Items.AddRange(Strings.SygnalInputSignals.ToArray());
                this._indComboBox.SelectedIndex = 0;
            }
            else
            {
                this._indComboBox.SelectedIndex = 0;
            }
        }

        public void ReadINPUTSYGNAL()
        {
            this._grUstComboBox.Text = this._device.GrUst;
            this._indComboBox.Text = this._device.SbInd;
        }

        public bool WriteINPUTSYGNAL()
        {
            bool ret = true;
            try
            {
                this._device.GrUst = this._grUstComboBox.SelectedItem.ToString();
                this._device.SbInd = this._indComboBox.SelectedItem.ToString();
                this._device.MemoryMap["Входные_Сигналы"].StructObj = this._device.sInputsignal;
            }
            catch (Exception ee)
            {
                ret = false;
            }
            return ret;
        }
        #endregion

        #region Углы
        public void PrepareCORNERS()
        {
            this._iCorner.Text = "0";
            this._i0Corner.Text = "0";
            this._inCorner.Text = "0";
            this._i2Corner.Text = "0";
        }

        public void ReadCORNERS()
        {
            this._iCorner.Text = this._device.sProtmain.currentprotlist.corner.S1.c.ToString();
            this._i0Corner.Text = this._device.sProtmain.currentprotlist.corner.S1.c0.ToString();
            this._inCorner.Text = this._device.sProtmain.currentprotlist.corner.S1.cn.ToString();
            this._i2Corner.Text = this._device.sProtmain.currentprotlist.corner.S1.c2.ToString();
        }

        public void ReadCORNERSReserve()
        {
            this._iCorner.Text = this._device.sProtreserve.currentprotlist.corner.S1.c.ToString();
            this._i0Corner.Text = this._device.sProtreserve.currentprotlist.corner.S1.c0.ToString();
            this._inCorner.Text = this._device.sProtreserve.currentprotlist.corner.S1.cn.ToString();
            this._i2Corner.Text = this._device.sProtreserve.currentprotlist.corner.S1.c2.ToString();
        }

        public void WriteCORNERS()
        {
            CURRENTPROTMAIN temp = this._device.sProtmain;

            temp.currentprotlist.corner.S1.c = Convert.ToUInt16(this._iCorner.Text);
            temp.currentprotlist.corner.S1.c0 = Convert.ToUInt16(this._i0Corner.Text);
            temp.currentprotlist.corner.S1.cn = Convert.ToUInt16(this._inCorner.Text);
            temp.currentprotlist.corner.S1.c2 = Convert.ToUInt16(this._i2Corner.Text);

            this._device.sProtmain = temp;
        }

        public void WriteCORNERSReserve()
        {
            CURRENTPROTRESERVE temp = this._device.sProtreserve;

            temp.currentprotlist.corner.S1.c = Convert.ToUInt16(this._iCorner.Text);
            temp.currentprotlist.corner.S1.c0 = Convert.ToUInt16(this._i0Corner.Text);
            temp.currentprotlist.corner.S1.cn = Convert.ToUInt16(this._inCorner.Text);
            temp.currentprotlist.corner.S1.c2 = Convert.ToUInt16(this._i2Corner.Text);

            this._device.sProtreserve = temp;
        }
        #endregion

        #region Защиты I
        public void PrepareIGrid()
        {
            this.FillIGrid();

            if (this._difensesIDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 8; i++)
                {
                    this._difensesIDataGrid.Rows.Add(new object[]{"Ступень I> " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.BusDirection[0],
                                                              Strings.UnDirection[0],
                                                              Strings.TokParameter[0],
                                                              Strings.Characteristic[0],
                                                              0,
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.SygnalInputSignals[0],
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.YesNo[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0]
                    });
                }
            }

        }

        public void FillIGrid()
        {
            if (this._iModesColumn.Items.Count == 0)
            {
                this._iModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._iUstartYNColumn.Items.Count == 0)
            {
                this._iUstartYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._iDirectColumn.Items.Count == 0)
            {
                this._iDirectColumn.Items.AddRange(Strings.BusDirection.ToArray());
            }
            if (this._iUnDirectColumn.Items.Count == 0)
            {
                this._iUnDirectColumn.Items.AddRange(Strings.UnDirection.ToArray());
            }
            if (this._iLogicColumn.Items.Count == 0)
            {
                this._iLogicColumn.Items.AddRange(Strings.TokParameter.ToArray());
            }
            if (this._iCharColumn.Items.Count == 0)
            {
                this._iCharColumn.Items.AddRange(Strings.Characteristic.ToArray());
            }
            if (this._iTyYNColumn.Items.Count == 0)
            {
                this._iTyYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._iBlockingColumn.Items.Count == 0)
            {
                this._iBlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._iI2I1YNColumn.Items.Count == 0)
            {
                this._iI2I1YNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._iBlockingDirectColumn.Items.Count == 0)
            {
                this._iBlockingDirectColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._iOscModeColumn.Items.Count == 0)
            {
                this._iOscModeColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._iUROVModeColumn.Items.Count == 0)
            {
                this._iUROVModeColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._iAPVModeColumn.Items.Count == 0)
            {
                this._iAPVModeColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._iAVRModeColumn.Items.Count == 0)
            {
                this._iAVRModeColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
        }

        public void ReadIGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzmain.Length; i++)
            {
                this._device.sMtzMain = this._device.sProtmain.currentprotlist.mtzmain[i];
                this._difensesIDataGrid.Rows[i].Cells["_iModesColumn"].Value = this._device.I_MODE;
                this._difensesIDataGrid.Rows[i].Cells["_iIColumn"].Value = this._device.I_ICP;
                this._difensesIDataGrid.Rows[i].Cells["_iUStartColumn"].Value = this._device.I_UPUSK;
                this._difensesIDataGrid.Rows[i].Cells["_iUstartYNColumn"].Value = this._device.I_UPUSK_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iDirectColumn"].Value = this._device.I_DIRECTION;
                this._difensesIDataGrid.Rows[i].Cells["_iUnDirectColumn"].Value = this._device.I_UNDIRECTION;
                this._difensesIDataGrid.Rows[i].Cells["_iLogicColumn"].Value = this._device.I_LOGIC;
                this._difensesIDataGrid.Rows[i].Cells["_iCharColumn"].Value = this._device.I_CHAR;
                this._difensesIDataGrid.Rows[i].Cells["_iTColumn"].Value = this._device.I_T;
                this._difensesIDataGrid.Rows[i].Cells["_iKColumn"].Value = this._device.I_K;
                this._difensesIDataGrid.Rows[i].Cells["_iTyColumn"].Value = this._device.I_TY;
                this._difensesIDataGrid.Rows[i].Cells["_iTyYNColumn"].Value = this._device.I_TY_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iBlockingColumn"].Value = this._device.I_BLOCK;
                this._difensesIDataGrid.Rows[i].Cells["_iI2I1Column"].Value = this._device.I_2r1r;
                this._difensesIDataGrid.Rows[i].Cells["_iI2I1YNColumn"].Value = this._device.I_2r1r_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iBlockingDirectColumn"].Value = this._device.I_BLOCK_DIRECT;
                this._difensesIDataGrid.Rows[i].Cells["_iOscModeColumn"].Value = this._device.I_OSC;
                this._difensesIDataGrid.Rows[i].Cells["_iUROVModeColumn"].Value = this._device.I_UROV;
                this._difensesIDataGrid.Rows[i].Cells["_iAPVModeColumn"].Value = this._device.I_APV;
                this._difensesIDataGrid.Rows[i].Cells["_iAVRModeColumn"].Value = this._device.I_AVR;
            }
        }

        public void ReadIGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzmain.Length; i++)
            {
                this._device.sMtzMain = this._device.sProtreserve.currentprotlist.mtzmain[i];
                this._difensesIDataGrid.Rows[i].Cells["_iModesColumn"].Value = this._device.I_MODE;
                this._difensesIDataGrid.Rows[i].Cells["_iIColumn"].Value = this._device.I_ICP;
                this._difensesIDataGrid.Rows[i].Cells["_iUStartColumn"].Value = this._device.I_UPUSK;
                this._difensesIDataGrid.Rows[i].Cells["_iUstartYNColumn"].Value = this._device.I_UPUSK_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iDirectColumn"].Value = this._device.I_DIRECTION;
                this._difensesIDataGrid.Rows[i].Cells["_iUnDirectColumn"].Value = this._device.I_UNDIRECTION;
                this._difensesIDataGrid.Rows[i].Cells["_iLogicColumn"].Value = this._device.I_LOGIC;
                this._difensesIDataGrid.Rows[i].Cells["_iCharColumn"].Value = this._device.I_CHAR;
                this._difensesIDataGrid.Rows[i].Cells["_iTColumn"].Value = this._device.I_T;
                this._difensesIDataGrid.Rows[i].Cells["_iKColumn"].Value = this._device.I_K;
                this._difensesIDataGrid.Rows[i].Cells["_iTyColumn"].Value = this._device.I_TY;
                this._difensesIDataGrid.Rows[i].Cells["_iTyYNColumn"].Value = this._device.I_TY_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iBlockingColumn"].Value = this._device.I_BLOCK;
                this._difensesIDataGrid.Rows[i].Cells["_iI2I1Column"].Value = this._device.I_2r1r;
                this._difensesIDataGrid.Rows[i].Cells["_iI2I1YNColumn"].Value = this._device.I_2r1r_YN;
                this._difensesIDataGrid.Rows[i].Cells["_iBlockingDirectColumn"].Value = this._device.I_BLOCK_DIRECT;
                this._difensesIDataGrid.Rows[i].Cells["_iOscModeColumn"].Value = this._device.I_OSC;
                this._difensesIDataGrid.Rows[i].Cells["_iUROVModeColumn"].Value = this._device.I_UROV;
                this._difensesIDataGrid.Rows[i].Cells["_iAPVModeColumn"].Value = this._device.I_APV;
                this._difensesIDataGrid.Rows[i].Cells["_iAVRModeColumn"].Value = this._device.I_AVR;
            }
        }

        public void WriteIGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzmain.Length; i++)
            {
                this._device.I_MODE = this._difensesIDataGrid.Rows[i].Cells["_iModesColumn"].Value.ToString();
                this._device.I_ICP = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iIColumn"].Value);
                this._device.I_UPUSK = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iUStartColumn"].Value);
                this._device.I_UPUSK_YN = this._difensesIDataGrid.Rows[i].Cells["_iUstartYNColumn"].Value.ToString();
                this._device.I_DIRECTION = this._difensesIDataGrid.Rows[i].Cells["_iDirectColumn"].Value.ToString();
                this._device.I_UNDIRECTION = this._difensesIDataGrid.Rows[i].Cells["_iUnDirectColumn"].Value.ToString();
                this._device.I_LOGIC = this._difensesIDataGrid.Rows[i].Cells["_iLogicColumn"].Value.ToString();
                this._device.I_CHAR = this._difensesIDataGrid.Rows[i].Cells["_iCharColumn"].Value.ToString();
                this._device.I_T = Convert.ToInt32(this._difensesIDataGrid.Rows[i].Cells["_iTColumn"].Value);
                this._device.I_K = Convert.ToUInt16(this._difensesIDataGrid.Rows[i].Cells["_iKColumn"].Value);
                this._device.I_TY = Convert.ToInt32(this._difensesIDataGrid.Rows[i].Cells["_iTyColumn"].Value);
                this._device.I_TY_YN = this._difensesIDataGrid.Rows[i].Cells["_iTyYNColumn"].Value.ToString();
                this._device.I_BLOCK = this._difensesIDataGrid.Rows[i].Cells["_iBlockingColumn"].Value.ToString();
                this._device.I_2r1r = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iI2I1Column"].Value);
                this._device.I_2r1r_YN = this._difensesIDataGrid.Rows[i].Cells["_iI2I1YNColumn"].Value.ToString();
                this._device.I_BLOCK_DIRECT = this._difensesIDataGrid.Rows[i].Cells["_iBlockingDirectColumn"].Value.ToString();
                this._device.I_OSC = this._difensesIDataGrid.Rows[i].Cells["_iOscModeColumn"].Value.ToString();
                this._device.I_UROV = this._difensesIDataGrid.Rows[i].Cells["_iUROVModeColumn"].Value.ToString();
                this._device.I_APV = this._difensesIDataGrid.Rows[i].Cells["_iAPVModeColumn"].Value.ToString();
                this._device.I_AVR = this._difensesIDataGrid.Rows[i].Cells["_iAVRModeColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzmain[i] = this._device.sMtzMain;
            }
        }

        public void WriteIGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzmain.Length; i++)
            {
                this._device.I_MODE = this._difensesIDataGrid.Rows[i].Cells["_iModesColumn"].Value.ToString();
                this._device.I_ICP = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iIColumn"].Value);
                this._device.I_UPUSK = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iUStartColumn"].Value);
                this._device.I_UPUSK_YN = this._difensesIDataGrid.Rows[i].Cells["_iUstartYNColumn"].Value.ToString();
                this._device.I_DIRECTION = this._difensesIDataGrid.Rows[i].Cells["_iDirectColumn"].Value.ToString();
                this._device.I_UNDIRECTION = this._difensesIDataGrid.Rows[i].Cells["_iUnDirectColumn"].Value.ToString();
                this._device.I_LOGIC = this._difensesIDataGrid.Rows[i].Cells["_iLogicColumn"].Value.ToString();
                this._device.I_CHAR = this._difensesIDataGrid.Rows[i].Cells["_iCharColumn"].Value.ToString();
                this._device.I_T = Convert.ToInt32(this._difensesIDataGrid.Rows[i].Cells["_iTColumn"].Value);
                this._device.I_K = Convert.ToUInt16(this._difensesIDataGrid.Rows[i].Cells["_iKColumn"].Value);
                this._device.I_TY = Convert.ToInt32(this._difensesIDataGrid.Rows[i].Cells["_iTyColumn"].Value);
                this._device.I_TY_YN = this._difensesIDataGrid.Rows[i].Cells["_iTyYNColumn"].Value.ToString();
                this._device.I_BLOCK = this._difensesIDataGrid.Rows[i].Cells["_iBlockingColumn"].Value.ToString();
                this._device.I_2r1r = Convert.ToDouble(this._difensesIDataGrid.Rows[i].Cells["_iI2I1Column"].Value);
                this._device.I_2r1r_YN = this._difensesIDataGrid.Rows[i].Cells["_iI2I1YNColumn"].Value.ToString();
                this._device.I_BLOCK_DIRECT = this._difensesIDataGrid.Rows[i].Cells["_iBlockingDirectColumn"].Value.ToString();
                this._device.I_OSC = this._difensesIDataGrid.Rows[i].Cells["_iOscModeColumn"].Value.ToString();
                this._device.I_UROV = this._difensesIDataGrid.Rows[i].Cells["_iUROVModeColumn"].Value.ToString();
                this._device.I_APV = this._difensesIDataGrid.Rows[i].Cells["_iAPVModeColumn"].Value.ToString();
                this._device.I_AVR = this._difensesIDataGrid.Rows[i].Cells["_iAVRModeColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzmain[i] = this._device.sMtzMain;
            }
        }
        #endregion

        #region Защиты I0
        public void PrepareI0Grid()
        {
            this.FillI0Grid();
            if (this._difensesI0DataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 6; i++)
                {
                    this._difensesI0DataGrid.Rows.Add(new object[]{"Ступень I*>" + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.BusDirection[0],
                                                              Strings.UnDirection[0],
                                                              Strings.I0Modes[0],
                                                              Strings.Characteristic[0],
                                                              0,
                                                              0,
                                                              Strings.SygnalInputSignals[0],
                                                              Strings.ModesLightOsc[0],
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0]

                    });
                }
            }
        }

        DataGridViewComboBoxColumn[] _cBI0Temp;
        public void FillI0Grid()
        {
            if (this._i0ModesColumn.Items.Count == 0)
            {
                this._i0ModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._i0UsYNColumn.Items.Count == 0)
            {
                this._i0UsYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._i0DirColumn.Items.Count == 0)
            {
                this._i0DirColumn.Items.AddRange(Strings.BusDirection.ToArray());
            }
            if (this._i0UndirColumn.Items.Count == 0)
            {
                this._i0UndirColumn.Items.AddRange(Strings.UnDirection.ToArray());
            }
            if (this._i0I0Column.Items.Count == 0)
            {
                this._i0I0Column.Items.AddRange(Strings.I0Modes.ToArray());
            }
            if (this._i0CharColumn.Items.Count == 0)
            {
                this._i0CharColumn.Items.AddRange(Strings.Characteristic.ToArray());
            }
            if (this._i0BlockingColumn.Items.Count == 0)
            {
                this._i0BlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._i0OscColumn.Items.Count == 0)
            {
                this._i0OscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._i0TyYNColumn.Items.Count == 0)
            {
                this._i0TyYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._i0UROVColumn.Items.Count == 0)
            {
                this._i0UROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._i0APVColumn.Items.Count == 0)
            {
                this._i0APVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._i0AVRColumn.Items.Count == 0)
            {
                this._i0AVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
        }

        public void ReadI0Grid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzmaini0.Length; i++)
            {
                this._device.sMtzMainI0 = this._device.sProtmain.currentprotlist.mtzmaini0[i];
                this._difensesI0DataGrid.Rows[i].Cells["_i0ModesColumn"].Value = this._device.I0_MODE;
                this._difensesI0DataGrid.Rows[i].Cells["_i0IColumn"].Value = this._device.I0_ICP;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UStartColumn"].Value = this._device.I0_UPUSK;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UsYNColumn"].Value = this._device.I0_UPUSK_YN;
                this._difensesI0DataGrid.Rows[i].Cells["_i0DirColumn"].Value = this._device.I0_DIRECTION;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UndirColumn"].Value = this._device.I0_UNDIRECTION;

                this._difensesI0DataGrid.Rows[i].Cells["_i0I0Column"].Value = this._device.I0_I0;
                this._difensesI0DataGrid.Rows[i].Cells["_i0CharColumn"].Value = this._device.I0_CHAR;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TColumn"].Value = this._device.I0_T;
                this._difensesI0DataGrid.Rows[i].Cells["_i0KColumn"].Value = this._device.I0_K;

                this._difensesI0DataGrid.Rows[i].Cells["_i0BlockingColumn"].Value = this._device.I0_BLOCK;
                this._difensesI0DataGrid.Rows[i].Cells["_i0OscColumn"].Value = this._device.I0_OSC;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TyColumn"].Value = this._device.I0_TY;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TyYNColumn"].Value = this._device.I0_TY_YN;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UROVColumn"].Value = this._device.I0_UROV;
                this._difensesI0DataGrid.Rows[i].Cells["_i0APVColumn"].Value = this._device.I0_APV;
                this._difensesI0DataGrid.Rows[i].Cells["_i0AVRColumn"].Value = this._device.I0_AVR;
            }
        }

        public void ReadI0GridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzmaini0.Length; i++)
            {
                this._device.sMtzMainI0 = this._device.sProtreserve.currentprotlist.mtzmaini0[i];
                this._difensesI0DataGrid.Rows[i].Cells["_i0ModesColumn"].Value = this._device.I0_MODE;
                this._difensesI0DataGrid.Rows[i].Cells["_i0IColumn"].Value = this._device.I0_ICP;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UStartColumn"].Value = this._device.I0_UPUSK;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UsYNColumn"].Value = this._device.I0_UPUSK_YN;
                this._difensesI0DataGrid.Rows[i].Cells["_i0DirColumn"].Value = this._device.I0_DIRECTION;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UndirColumn"].Value = this._device.I0_UNDIRECTION;

                this._difensesI0DataGrid.Rows[i].Cells["_i0I0Column"].Value = this._device.I0_I0;
                this._difensesI0DataGrid.Rows[i].Cells["_i0CharColumn"].Value = this._device.I0_CHAR;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TColumn"].Value = this._device.I0_T;
                this._difensesI0DataGrid.Rows[i].Cells["_i0KColumn"].Value = this._device.I0_K;

                this._difensesI0DataGrid.Rows[i].Cells["_i0BlockingColumn"].Value = this._device.I0_BLOCK;
                this._difensesI0DataGrid.Rows[i].Cells["_i0OscColumn"].Value = this._device.I0_OSC;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TyColumn"].Value = this._device.I0_TY;
                this._difensesI0DataGrid.Rows[i].Cells["_i0TyYNColumn"].Value = this._device.I0_TY_YN;
                this._difensesI0DataGrid.Rows[i].Cells["_i0UROVColumn"].Value = this._device.I0_UROV;
                this._difensesI0DataGrid.Rows[i].Cells["_i0APVColumn"].Value = this._device.I0_APV;
                this._difensesI0DataGrid.Rows[i].Cells["_i0AVRColumn"].Value = this._device.I0_AVR;
            }
        }

        public void WriteI0Grid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzmaini0.Length; i++)
            {
                this._device.I0_MODE = this._difensesI0DataGrid.Rows[i].Cells["_i0ModesColumn"].Value.ToString();
                this._device.I0_ICP = Convert.ToDouble(this._difensesI0DataGrid.Rows[i].Cells["_i0IColumn"].Value);
                this._device.I0_UPUSK = Convert.ToDouble(this._difensesI0DataGrid.Rows[i].Cells["_i0UStartColumn"].Value);
                this._device.I0_UPUSK_YN = this._difensesI0DataGrid.Rows[i].Cells["_i0UsYNColumn"].Value.ToString();
                this._device.I0_DIRECTION = this._difensesI0DataGrid.Rows[i].Cells["_i0DirColumn"].Value.ToString();
                this._device.I0_UNDIRECTION = this._difensesI0DataGrid.Rows[i].Cells["_i0UndirColumn"].Value.ToString();

                this._device.I0_I0 = this._difensesI0DataGrid.Rows[i].Cells["_i0I0Column"].Value.ToString();
                this._device.I0_CHAR = this._difensesI0DataGrid.Rows[i].Cells["_i0CharColumn"].Value.ToString();
                this._device.I0_T = Convert.ToInt32(this._difensesI0DataGrid.Rows[i].Cells["_i0TColumn"].Value);
                this._device.I0_K = Convert.ToUInt16(this._difensesI0DataGrid.Rows[i].Cells["_i0KColumn"].Value);

                this._device.I0_BLOCK = this._difensesI0DataGrid.Rows[i].Cells["_i0BlockingColumn"].Value.ToString();
                this._device.I0_OSC = this._difensesI0DataGrid.Rows[i].Cells["_i0OscColumn"].Value.ToString();
                this._device.I0_TY = Convert.ToInt32(this._difensesI0DataGrid.Rows[i].Cells["_i0TyColumn"].Value);
                this._device.I0_TY_YN = this._difensesI0DataGrid.Rows[i].Cells["_i0TyYNColumn"].Value.ToString();
                this._device.I0_UROV = this._difensesI0DataGrid.Rows[i].Cells["_i0UROVColumn"].Value.ToString();
                this._device.I0_APV = this._difensesI0DataGrid.Rows[i].Cells["_i0APVColumn"].Value.ToString();
                this._device.I0_AVR = this._difensesI0DataGrid.Rows[i].Cells["_i0AVRColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzmaini0[i] = this._device.sMtzMainI0;
            }
        }

        public void WriteI0GridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzmaini0.Length; i++)
            {
                this._device.I0_MODE = this._difensesI0DataGrid.Rows[i].Cells["_i0ModesColumn"].Value.ToString();
                this._device.I0_ICP = Convert.ToDouble(this._difensesI0DataGrid.Rows[i].Cells["_i0IColumn"].Value);
                this._device.I0_UPUSK = Convert.ToDouble(this._difensesI0DataGrid.Rows[i].Cells["_i0UStartColumn"].Value);
                this._device.I0_UPUSK_YN = this._difensesI0DataGrid.Rows[i].Cells["_i0UsYNColumn"].Value.ToString();
                this._device.I0_DIRECTION = this._difensesI0DataGrid.Rows[i].Cells["_i0DirColumn"].Value.ToString();
                this._device.I0_UNDIRECTION = this._difensesI0DataGrid.Rows[i].Cells["_i0UndirColumn"].Value.ToString();

                this._device.I0_I0 = this._difensesI0DataGrid.Rows[i].Cells["_i0I0Column"].Value.ToString();
                this._device.I0_CHAR = this._difensesI0DataGrid.Rows[i].Cells["_i0CharColumn"].Value.ToString();
                this._device.I0_T = Convert.ToInt32(this._difensesI0DataGrid.Rows[i].Cells["_i0TColumn"].Value);
                this._device.I0_K = Convert.ToUInt16(this._difensesI0DataGrid.Rows[i].Cells["_i0KColumn"].Value);

                this._device.I0_BLOCK = this._difensesI0DataGrid.Rows[i].Cells["_i0BlockingColumn"].Value.ToString();
                this._device.I0_OSC = this._difensesI0DataGrid.Rows[i].Cells["_i0OscColumn"].Value.ToString();
                this._device.I0_TY = Convert.ToInt32(this._difensesI0DataGrid.Rows[i].Cells["_i0TyColumn"].Value);
                this._device.I0_TY_YN = this._difensesI0DataGrid.Rows[i].Cells["_i0TyYNColumn"].Value.ToString();
                this._device.I0_UROV = this._difensesI0DataGrid.Rows[i].Cells["_i0UROVColumn"].Value.ToString();
                this._device.I0_APV = this._difensesI0DataGrid.Rows[i].Cells["_i0APVColumn"].Value.ToString();
                this._device.I0_AVR = this._difensesI0DataGrid.Rows[i].Cells["_i0AVRColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzmaini0[i] = this._device.sMtzMainI0;
            }
        }
        #endregion

        #region Защиты I2I1
        public void PrepareI2I1()
        {
            this._paramI2I1Combos = new ComboBox[] {
                this.I2I1ModeCombo, this.I2I1BlockingCombo, this.I2I1OSCCombo, this.I2I1UROVCombo, this.I2I1APVCombo, this.I2I1AVRCombo};

            this._ULONGparamI2I1MasketTextBoxes = new MaskedTextBox[] {this.I2I1tcp };
            this._DOUBLEparamI2I1MasketTextBoxes = new MaskedTextBox[] {this.I2I1TB };

            this.ClearCombos(this._paramI2I1Combos);

            this.FillI2I1();

            this.SubscriptCombos(this._paramI2I1Combos);

            this.PrepareMaskedBoxes(this._ULONGparamI2I1MasketTextBoxes, typeof(ulong));
            this.PrepareMaskedBoxes(this._DOUBLEparamI2I1MasketTextBoxes, typeof(double));

        }

        public void FillI2I1()
        {
            if (this.I2I1ModeCombo.Items.Count == 0)
            {
                this.I2I1ModeCombo.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this.I2I1BlockingCombo.Items.Count == 0)
            {
                this.I2I1BlockingCombo.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this.I2I1OSCCombo.Items.Count == 0)
            {
                this.I2I1OSCCombo.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this.I2I1UROVCombo.Items.Count == 0)
            {
                this.I2I1UROVCombo.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this.I2I1APVCombo.Items.Count == 0)
            {
                this.I2I1APVCombo.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this.I2I1AVRCombo.Items.Count == 0)
            {
                this.I2I1AVRCombo.Items.AddRange(Strings.ModesLight.ToArray());
            }
        }

        public void ReadI2I1()
        {
            this._device.sMtzMainI2I1 = this._device.sProtmain.currentprotlist.mtzi2i1;
            this.I2I1ModeCombo.SelectedItem = this._device.I2I1_MODE;
            this.I2I1BlockingCombo.SelectedItem = this._device.I2I1_BLOCK;
            this.I2I1tcp.Text = this._device.I2I1_TCP.ToString();
            this.I2I1TB.Text = this._device.I2I1_I2I1.ToString();
            this.I2I1OSCCombo.SelectedItem = this._device.I2I1_OSC;
            this.I2I1UROVCombo.SelectedItem = this._device.I2I1_UROV;
            this.I2I1APVCombo.SelectedItem = this._device.I2I1_APV;
            this.I2I1AVRCombo.SelectedItem = this._device.I2I1_AVR;
        }

        public void ReadI2I1Reserve()
        {
            this._device.sMtzMainI2I1 = this._device.sProtreserve.currentprotlist.mtzi2i1;
            this.I2I1ModeCombo.SelectedItem = this._device.I2I1_MODE;
            this.I2I1BlockingCombo.SelectedItem = this._device.I2I1_BLOCK;
            this.I2I1tcp.Text = this._device.I2I1_TCP.ToString();
            this.I2I1TB.Text = this._device.I2I1_I2I1.ToString();
            this.I2I1OSCCombo.SelectedItem = this._device.I2I1_OSC;
            this.I2I1UROVCombo.SelectedItem = this._device.I2I1_UROV;
            this.I2I1APVCombo.SelectedItem = this._device.I2I1_APV;
            this.I2I1AVRCombo.SelectedItem = this._device.I2I1_AVR;
        }

        public void WriteI2I1()
        {
            CURRENTPROTMAIN mainTemp = this._device.sProtmain;

            this._device.I2I1_MODE = this.I2I1ModeCombo.SelectedItem.ToString();
            this._device.I2I1_BLOCK = this.I2I1BlockingCombo.SelectedItem.ToString();
            this._device.I2I1_TCP = Convert.ToInt32(this.I2I1tcp.Text);
            this._device.I2I1_I2I1 = Convert.ToDouble(this.I2I1TB.Text);
            this._device.I2I1_OSC = this.I2I1OSCCombo.SelectedItem.ToString();
            this._device.I2I1_UROV = this.I2I1UROVCombo.SelectedItem.ToString();
            this._device.I2I1_APV = this.I2I1APVCombo.SelectedItem.ToString();
            this._device.I2I1_AVR = this.I2I1AVRCombo.SelectedItem.ToString();

            mainTemp.currentprotlist.mtzi2i1 = this._device.sMtzMainI2I1;
            this._device.sProtmain = mainTemp;
        }

        public void WriteI2I1Reserve()
        {
            CURRENTPROTRESERVE reserveTemp = this._device.sProtreserve;

            this._device.I2I1_MODE = this.I2I1ModeCombo.SelectedItem.ToString();
            this._device.I2I1_BLOCK = this.I2I1BlockingCombo.SelectedItem.ToString();
            this._device.I2I1_TCP = Convert.ToInt32(this.I2I1tcp.Text);
            this._device.I2I1_I2I1 = Convert.ToDouble(this.I2I1TB.Text);
            this._device.I2I1_OSC = this.I2I1OSCCombo.SelectedItem.ToString();
            this._device.I2I1_UROV = this.I2I1UROVCombo.SelectedItem.ToString();
            this._device.I2I1_APV = this.I2I1APVCombo.SelectedItem.ToString();
            this._device.I2I1_AVR = this.I2I1AVRCombo.SelectedItem.ToString();

            reserveTemp.currentprotlist.mtzi2i1 = this._device.sMtzMainI2I1;
            this._device.sProtreserve = reserveTemp;
        }
        #endregion


        #region Защиты U>
        public void PrepareUBGrid()
        {
            this.FillUBGrid();
            if (this._difensesUBDataGrid.Rows.Count == 0)
            {
                string a = ">";
                for (int i = 0; i < 4; i++)
                {
                    this._difensesUBDataGrid.Rows.Add(new object[]{"Ступень U> " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              Strings.UType[0],
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.SygnalInputSignals[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.YesNo[0]
                    });
                    a += ">";
                }
            }
        }

        public void FillUBGrid()
        {
            if (this._uBModesColumn.Items.Count == 0)
            {
                this._uBModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._uBTypeColumn.Items.Count == 0)
            {
                this._uBTypeColumn.Items.AddRange(Strings.UType.ToArray());
            }
            if (this._uBUvzYNColumn.Items.Count == 0)
            {
                this._uBUvzYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._uBBlockingColumn.Items.Count == 0)
            {
                this._uBBlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._uBOscColumn.Items.Count == 0)
            {
                this._uBOscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._uBUROVColumn.Items.Count == 0)
            {
                this._uBUROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uBAPVColumn.Items.Count == 0)
            {
                this._uBAPVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uBAVRColumn.Items.Count == 0)
            {
                this._uBAVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uBAPVRetColumn.Items.Count == 0)
            {
                this._uBAPVRetColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uBSbrosColumn.Items.Count == 0)
            {
                this._uBSbrosColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
        }

        public void ReadUMaxGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzumax.Length; i++)
            {
                this._device.sMtzUMax = this._device.sProtmain.currentprotlist.mtzumax[i];
                this._difensesUBDataGrid.Rows[i].Cells["_uBModesColumn"].Value = this._device.UB_MODE;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTypeColumn"].Value = this._device.UB_TYPE;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUsrColumn"].Value = this._device.UB_UCP;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTsrColumn"].Value = this._device.UB_TCP;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTvzColumn"].Value = this._device.UB_TVZ;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUvzColumn"].Value = this._device.UB_UVZ;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUvzYNColumn"].Value = this._device.UB_UVZ_YN;
                this._difensesUBDataGrid.Rows[i].Cells["_uBBlockingColumn"].Value = this._device.UB_BLOCK;
                this._difensesUBDataGrid.Rows[i].Cells["_uBOscColumn"].Value = this._device.UB_OSC;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAPVRetColumn"].Value = this._device.UB_APV_VOZVR;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUROVColumn"].Value = this._device.UB_UROV;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAPVColumn"].Value = this._device.UB_APV;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAVRColumn"].Value = this._device.UB_AVR;
                this._difensesUBDataGrid.Rows[i].Cells["_uBSbrosColumn"].Value = this._device.UB_DROP;
            }
        }

        public void ReadUMaxGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzumax.Length; i++)
            {
                this._device.sMtzUMax = this._device.sProtreserve.currentprotlist.mtzumax[i];
                this._difensesUBDataGrid.Rows[i].Cells["_uBModesColumn"].Value = this._device.UB_MODE;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTypeColumn"].Value = this._device.UB_TYPE;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUsrColumn"].Value = this._device.UB_UCP;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTsrColumn"].Value = this._device.UB_TCP;
                this._difensesUBDataGrid.Rows[i].Cells["_uBTvzColumn"].Value = this._device.UB_TVZ;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUvzColumn"].Value = this._device.UB_UVZ;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUvzYNColumn"].Value = this._device.UB_UVZ_YN;
                this._difensesUBDataGrid.Rows[i].Cells["_uBBlockingColumn"].Value = this._device.UB_BLOCK;
                this._difensesUBDataGrid.Rows[i].Cells["_uBOscColumn"].Value = this._device.UB_OSC;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAPVRetColumn"].Value = this._device.UB_APV_VOZVR;
                this._difensesUBDataGrid.Rows[i].Cells["_uBUROVColumn"].Value = this._device.UB_UROV;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAPVColumn"].Value = this._device.UB_APV;
                this._difensesUBDataGrid.Rows[i].Cells["_uBAVRColumn"].Value = this._device.UB_AVR;
                this._difensesUBDataGrid.Rows[i].Cells["_uBSbrosColumn"].Value = this._device.UB_DROP;
            }
        }

        public void WriteUMaxGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzumax.Length; i++)
            {
                this._device.UB_MODE = this._difensesUBDataGrid.Rows[i].Cells["_uBModesColumn"].Value.ToString();
                this._device.UB_TYPE = this._difensesUBDataGrid.Rows[i].Cells["_uBTypeColumn"].Value.ToString();
                this._device.UB_UCP = Convert.ToDouble(this._difensesUBDataGrid.Rows[i].Cells["_uBUsrColumn"].Value);
                this._device.UB_TCP = Convert.ToInt32(this._difensesUBDataGrid.Rows[i].Cells["_uBTsrColumn"].Value);
                this._device.UB_TVZ = Convert.ToInt32(this._difensesUBDataGrid.Rows[i].Cells["_uBTvzColumn"].Value);
                this._device.UB_UVZ = Convert.ToDouble(this._difensesUBDataGrid.Rows[i].Cells["_uBUvzColumn"].Value);
                this._device.UB_UVZ_YN = this._difensesUBDataGrid.Rows[i].Cells["_uBUvzYNColumn"].Value.ToString();
                this._device.UB_BLOCK = this._difensesUBDataGrid.Rows[i].Cells["_uBBlockingColumn"].Value.ToString();
                this._device.UB_OSC = this._difensesUBDataGrid.Rows[i].Cells["_uBOscColumn"].Value.ToString();
                this._device.UB_APV_VOZVR = this._difensesUBDataGrid.Rows[i].Cells["_uBAPVRetColumn"].Value.ToString();
                this._device.UB_UROV = this._difensesUBDataGrid.Rows[i].Cells["_uBUROVColumn"].Value.ToString();
                this._device.UB_APV = this._difensesUBDataGrid.Rows[i].Cells["_uBAPVColumn"].Value.ToString();
                this._device.UB_AVR = this._difensesUBDataGrid.Rows[i].Cells["_uBAVRColumn"].Value.ToString();
                this._device.UB_DROP = this._difensesUBDataGrid.Rows[i].Cells["_uBSbrosColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzumax[i] = this._device.sMtzUMax;
            }
        }

        public void WriteUMaxGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzumax.Length; i++)
            {
                this._device.UB_MODE = this._difensesUBDataGrid.Rows[i].Cells["_uBModesColumn"].Value.ToString();
                this._device.UB_TYPE = this._difensesUBDataGrid.Rows[i].Cells["_uBTypeColumn"].Value.ToString();
                this._device.UB_UCP = Convert.ToDouble(this._difensesUBDataGrid.Rows[i].Cells["_uBUsrColumn"].Value);
                this._device.UB_TCP = Convert.ToInt32(this._difensesUBDataGrid.Rows[i].Cells["_uBTsrColumn"].Value);
                this._device.UB_TVZ = Convert.ToInt32(this._difensesUBDataGrid.Rows[i].Cells["_uBTvzColumn"].Value);
                this._device.UB_UVZ = Convert.ToDouble(this._difensesUBDataGrid.Rows[i].Cells["_uBUvzColumn"].Value);
                this._device.UB_UVZ_YN = this._difensesUBDataGrid.Rows[i].Cells["_uBUvzYNColumn"].Value.ToString();
                this._device.UB_BLOCK = this._difensesUBDataGrid.Rows[i].Cells["_uBBlockingColumn"].Value.ToString();
                this._device.UB_OSC = this._difensesUBDataGrid.Rows[i].Cells["_uBOscColumn"].Value.ToString();
                this._device.UB_APV_VOZVR = this._difensesUBDataGrid.Rows[i].Cells["_uBAPVRetColumn"].Value.ToString();
                this._device.UB_UROV = this._difensesUBDataGrid.Rows[i].Cells["_uBUROVColumn"].Value.ToString();
                this._device.UB_APV = this._difensesUBDataGrid.Rows[i].Cells["_uBAPVColumn"].Value.ToString();
                this._device.UB_AVR = this._difensesUBDataGrid.Rows[i].Cells["_uBAVRColumn"].Value.ToString();
                this._device.UB_DROP = this._difensesUBDataGrid.Rows[i].Cells["_uBSbrosColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzumax[i] = this._device.sMtzUMax;
            }
        }
        #endregion

        #region Защиты U<
        public void PrepareUMGrid()
        {
            this.FillUMGrid();
            if (this._difensesUMDataGrid.Rows.Count == 0)
            {
                string a = ">";
                for (int i = 0; i < 4; i++)
                {
                    this._difensesUMDataGrid.Rows.Add(new object[]{"Ступень U< " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              Strings.UMType[0],
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.YesNo[0],
                                                              Strings.SygnalInputSignals[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.YesNo[0]
                        });
                    a += ">";
                }
            }
        }

        public void FillUMGrid()
        {
            if (this._uMModesColumn.Items.Count == 0)
            {
                this._uMModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._uMTypeColumn.Items.Count == 0)
            {
                this._uMTypeColumn.Items.AddRange(Strings.UMType.ToArray());
            }
            if (this._uMUvzYNColumn.Items.Count == 0)
            {
                this._uMUvzYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._uMBlockingUMColumn.Items.Count == 0)
            {
                this._uMBlockingUMColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._uMBlockingColumn.Items.Count == 0)
            {
                this._uMBlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._uMOscColumn.Items.Count == 0)
            {
                this._uMOscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._uMUROVColumn.Items.Count == 0)
            {
                this._uMUROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uMAPVColumn.Items.Count == 0)
            {
                this._uMAPVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uMAVRColumn.Items.Count == 0)
            {
                this._uMAVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uMAPVRetColumn.Items.Count == 0)
            {
                this._uMAPVRetColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._uMSbrosColumn.Items.Count == 0)
            {
                this._uMSbrosColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
        }

        public void ReadUMinGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzumin.Length; i++)
            {
                this._device.sMtzUMin = this._device.sProtmain.currentprotlist.mtzumin[i];
                this._difensesUMDataGrid.Rows[i].Cells["_uMModesColumn"].Value = this._device.UM_MODE;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTypeColumn"].Value = this._device.UM_TYPE;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUsrColumn"].Value = this._device.UM_UCP;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTsrColumn"].Value = this._device.UM_TCP;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTvzColumn"].Value = this._device.UM_TVZ;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUvzColumn"].Value = this._device.UM_UVZ;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUvzYNColumn"].Value = this._device.UM_UVZ_YN;
                this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingUMColumn"].Value = this._device.UM_BLOCK_M5;
                this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingColumn"].Value = this._device.UM_BLOCK;
                this._difensesUMDataGrid.Rows[i].Cells["_uMOscColumn"].Value = this._device.UM_OSC;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAPVRetColumn"].Value = this._device.UM_APV_VOZVR;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUROVColumn"].Value = this._device.UM_UROV;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAPVColumn"].Value = this._device.UM_APV;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAVRColumn"].Value = this._device.UM_AVR;
                this._difensesUMDataGrid.Rows[i].Cells["_uMSbrosColumn"].Value = this._device.UM_DROP;
            }
        }

        public void ReadUMinGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzumin.Length; i++)
            {
                this._device.sMtzUMin = this._device.sProtreserve.currentprotlist.mtzumin[i];
                this._difensesUMDataGrid.Rows[i].Cells["_uMModesColumn"].Value = this._device.UM_MODE;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTypeColumn"].Value = this._device.UM_TYPE;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUsrColumn"].Value = this._device.UM_UCP;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTsrColumn"].Value = this._device.UM_TCP;
                this._difensesUMDataGrid.Rows[i].Cells["_uMTvzColumn"].Value = this._device.UM_TVZ;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUvzColumn"].Value = this._device.UM_UVZ;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUvzYNColumn"].Value = this._device.UM_UVZ_YN;
                this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingUMColumn"].Value = this._device.UM_BLOCK_M5;
                this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingColumn"].Value = this._device.UM_BLOCK;
                this._difensesUMDataGrid.Rows[i].Cells["_uMOscColumn"].Value = this._device.UM_OSC;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAPVRetColumn"].Value = this._device.UM_APV_VOZVR;
                this._difensesUMDataGrid.Rows[i].Cells["_uMUROVColumn"].Value = this._device.UM_UROV;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAPVColumn"].Value = this._device.UM_APV;
                this._difensesUMDataGrid.Rows[i].Cells["_uMAVRColumn"].Value = this._device.UM_AVR;
                this._difensesUMDataGrid.Rows[i].Cells["_uMSbrosColumn"].Value = this._device.UM_DROP;
            }
        }

        public void WriteUMinGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzumin.Length; i++)
            {
                this._device.UM_MODE = this._difensesUMDataGrid.Rows[i].Cells["_uMModesColumn"].Value.ToString();
                this._device.UM_TYPE = this._difensesUMDataGrid.Rows[i].Cells["_uMTypeColumn"].Value.ToString();
                this._device.UM_UCP = Convert.ToDouble(this._difensesUMDataGrid.Rows[i].Cells["_uMUsrColumn"].Value);
                this._device.UM_TCP = Convert.ToUInt16(this._difensesUMDataGrid.Rows[i].Cells["_uMTsrColumn"].Value);
                this._device.UM_TVZ = Convert.ToInt32(this._difensesUMDataGrid.Rows[i].Cells["_uMTvzColumn"].Value);
                this._device.UM_UVZ = Convert.ToDouble(this._difensesUMDataGrid.Rows[i].Cells["_uMUvzColumn"].Value);
                this._device.UM_UVZ_YN = this._difensesUMDataGrid.Rows[i].Cells["_uMUvzYNColumn"].Value.ToString();
                this._device.UM_BLOCK_M5 = this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingUMColumn"].Value.ToString();
                this._device.UM_BLOCK = this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingColumn"].Value.ToString();
                this._device.UM_OSC = this._difensesUMDataGrid.Rows[i].Cells["_uMOscColumn"].Value.ToString();
                this._device.UM_APV_VOZVR = this._difensesUMDataGrid.Rows[i].Cells["_uMAPVRetColumn"].Value.ToString();
                this._device.UM_UROV = this._difensesUMDataGrid.Rows[i].Cells["_uMUROVColumn"].Value.ToString();
                this._device.UM_APV = this._difensesUMDataGrid.Rows[i].Cells["_uMAPVColumn"].Value.ToString();
                this._device.UM_AVR = this._difensesUMDataGrid.Rows[i].Cells["_uMAVRColumn"].Value.ToString();
                this._device.UM_DROP = this._difensesUMDataGrid.Rows[i].Cells["_uMSbrosColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzumin[i] = this._device.sMtzUMin;
            }
        }

        public void WriteUMinGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzumin.Length; i++)
            {
                this._device.UM_MODE = this._difensesUMDataGrid.Rows[i].Cells["_uMModesColumn"].Value.ToString();
                this._device.UM_TYPE = this._difensesUMDataGrid.Rows[i].Cells["_uMTypeColumn"].Value.ToString();
                this._device.UM_UCP = Convert.ToDouble(this._difensesUMDataGrid.Rows[i].Cells["_uMUsrColumn"].Value);
                this._device.UM_TCP = Convert.ToUInt16(this._difensesUMDataGrid.Rows[i].Cells["_uMTsrColumn"].Value);
                this._device.UM_TVZ = Convert.ToInt32(this._difensesUMDataGrid.Rows[i].Cells["_uMTvzColumn"].Value);
                this._device.UM_UVZ = Convert.ToDouble(this._difensesUMDataGrid.Rows[i].Cells["_uMUvzColumn"].Value);
                this._device.UM_UVZ_YN = this._difensesUMDataGrid.Rows[i].Cells["_uMUvzYNColumn"].Value.ToString();
                this._device.UM_BLOCK_M5 = this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingUMColumn"].Value.ToString();
                this._device.UM_BLOCK = this._difensesUMDataGrid.Rows[i].Cells["_uMBlockingColumn"].Value.ToString();
                this._device.UM_OSC = this._difensesUMDataGrid.Rows[i].Cells["_uMOscColumn"].Value.ToString();
                this._device.UM_APV_VOZVR = this._difensesUMDataGrid.Rows[i].Cells["_uMAPVRetColumn"].Value.ToString();
                this._device.UM_UROV = this._difensesUMDataGrid.Rows[i].Cells["_uMUROVColumn"].Value.ToString();
                this._device.UM_APV = this._difensesUMDataGrid.Rows[i].Cells["_uMAPVColumn"].Value.ToString();
                this._device.UM_AVR = this._difensesUMDataGrid.Rows[i].Cells["_uMAVRColumn"].Value.ToString();
                this._device.UM_DROP = this._difensesUMDataGrid.Rows[i].Cells["_uMSbrosColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzumin[i] = this._device.sMtzUMin;
            }
        }
        #endregion

        #region Защиты F>
        public void PrepareFBGrid()
        {
            this.FillFBGrid();
            if (this._difensesFBDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    this._difensesFBDataGrid.Rows.Add(new object[]{"Ступень F> " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.SygnalInputSignals[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.YesNo[0]
                    });
                }
            }
        }

        public void FillFBGrid()
        {
            if (this._fBModesColumn.Items.Count == 0)
            {
                this._fBModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._fBUvzYNColumn.Items.Count == 0)
            {
                this._fBUvzYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._fBBlockingColumn.Items.Count == 0)
            {
                this._fBBlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._fBOscColumn.Items.Count == 0)
            {
                this._fBOscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._fBUROVColumn.Items.Count == 0)
            {
                this._fBUROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fBAPVColumn.Items.Count == 0)
            {
                this._fBAPVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fBAVRColumn.Items.Count == 0)
            {
                this._fBAVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fBAPVRetColumn.Items.Count == 0)
            {
                this._fBAPVRetColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fBSbrosColumn.Items.Count == 0)
            {
                this._fBSbrosColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
        }

        public void ReadFMaxGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzfmax.Length; i++)
            {
                this._device.sMtzFMax = this._device.sProtmain.currentprotlist.mtzfmax[i];
                this._difensesFBDataGrid.Rows[i].Cells["_fBModesColumn"].Value = this._device.FB_MODE;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUsrColumn"].Value = this._device.FB_FCP;
                this._difensesFBDataGrid.Rows[i].Cells["_fBTsrColumn"].Value = this._device.FB_TCP;
                this._difensesFBDataGrid.Rows[i].Cells["_fBTvzColumn"].Value = this._device.FB_TVZ;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUvzColumn"].Value = this._device.FB_FVZ;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUvzYNColumn"].Value = this._device.FB_FVZ_YN;
                this._difensesFBDataGrid.Rows[i].Cells["_fBBlockingColumn"].Value = this._device.FB_BLOCK;
                this._difensesFBDataGrid.Rows[i].Cells["_fBOscColumn"].Value = this._device.FB_OSC;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAPVRetColumn"].Value = this._device.FB_APV_VOZVR;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUROVColumn"].Value = this._device.FB_UROV;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAPVColumn"].Value = this._device.FB_APV;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAVRColumn"].Value = this._device.FB_AVR;
                this._difensesFBDataGrid.Rows[i].Cells["_fBSbrosColumn"].Value = this._device.FB_DROP;
            }
        }

        public void ReadFMaxGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzfmax.Length; i++)
            {
                this._device.sMtzFMax = this._device.sProtreserve.currentprotlist.mtzfmax[i];
                this._difensesFBDataGrid.Rows[i].Cells["_fBModesColumn"].Value = this._device.FB_MODE;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUsrColumn"].Value = this._device.FB_FCP;
                this._difensesFBDataGrid.Rows[i].Cells["_fBTsrColumn"].Value = this._device.FB_TCP;
                this._difensesFBDataGrid.Rows[i].Cells["_fBTvzColumn"].Value = this._device.FB_TVZ;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUvzColumn"].Value = this._device.FB_FVZ;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUvzYNColumn"].Value = this._device.FB_FVZ_YN;
                this._difensesFBDataGrid.Rows[i].Cells["_fBBlockingColumn"].Value = this._device.FB_BLOCK;
                this._difensesFBDataGrid.Rows[i].Cells["_fBOscColumn"].Value = this._device.FB_OSC;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAPVRetColumn"].Value = this._device.FB_APV_VOZVR;
                this._difensesFBDataGrid.Rows[i].Cells["_fBUROVColumn"].Value = this._device.FB_UROV;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAPVColumn"].Value = this._device.FB_APV;
                this._difensesFBDataGrid.Rows[i].Cells["_fBAVRColumn"].Value = this._device.FB_AVR;
                this._difensesFBDataGrid.Rows[i].Cells["_fBSbrosColumn"].Value = this._device.FB_DROP;
            }
        }

        public void WriteFMaxGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzfmax.Length; i++)
            {
                this._device.FB_MODE = this._difensesFBDataGrid.Rows[i].Cells["_fBModesColumn"].Value.ToString();
                this._device.FB_FCP = Convert.ToDouble(this._difensesFBDataGrid.Rows[i].Cells["_fBUsrColumn"].Value);
                this._device.FB_TCP = Convert.ToInt32(this._difensesFBDataGrid.Rows[i].Cells["_fBTsrColumn"].Value);
                this._device.FB_TVZ = Convert.ToInt32(this._difensesFBDataGrid.Rows[i].Cells["_fBTvzColumn"].Value);
                this._device.FB_FVZ = Convert.ToDouble(this._difensesFBDataGrid.Rows[i].Cells["_fBUvzColumn"].Value);
                this._device.FB_FVZ_YN = this._difensesFBDataGrid.Rows[i].Cells["_fBUvzYNColumn"].Value.ToString();
                this._device.FB_BLOCK = this._difensesFBDataGrid.Rows[i].Cells["_fBBlockingColumn"].Value.ToString();
                this._device.FB_OSC = this._difensesFBDataGrid.Rows[i].Cells["_fBOscColumn"].Value.ToString();
                this._device.FB_APV_VOZVR = this._difensesFBDataGrid.Rows[i].Cells["_fBAPVRetColumn"].Value.ToString();
                this._device.FB_UROV = this._difensesFBDataGrid.Rows[i].Cells["_fBUROVColumn"].Value.ToString();
                this._device.FB_APV = this._difensesFBDataGrid.Rows[i].Cells["_fBAPVColumn"].Value.ToString();
                this._device.FB_AVR = this._difensesFBDataGrid.Rows[i].Cells["_fBAVRColumn"].Value.ToString();
                this._device.FB_DROP = this._difensesFBDataGrid.Rows[i].Cells["_fBSbrosColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzfmax[i] = this._device.sMtzFMax;
            }
        }

        public void WriteFMaxGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzfmax.Length; i++)
            {
                this._device.FB_MODE = this._difensesFBDataGrid.Rows[i].Cells["_fBModesColumn"].Value.ToString();
                this._device.FB_FCP = Convert.ToDouble(this._difensesFBDataGrid.Rows[i].Cells["_fBUsrColumn"].Value);
                this._device.FB_TCP = Convert.ToInt32(this._difensesFBDataGrid.Rows[i].Cells["_fBTsrColumn"].Value);
                this._device.FB_TVZ = Convert.ToInt32(this._difensesFBDataGrid.Rows[i].Cells["_fBTvzColumn"].Value);
                this._device.FB_FVZ = Convert.ToDouble(this._difensesFBDataGrid.Rows[i].Cells["_fBUvzColumn"].Value);
                this._device.FB_FVZ_YN = this._difensesFBDataGrid.Rows[i].Cells["_fBUvzYNColumn"].Value.ToString();
                this._device.FB_BLOCK = this._difensesFBDataGrid.Rows[i].Cells["_fBBlockingColumn"].Value.ToString();
                this._device.FB_OSC = this._difensesFBDataGrid.Rows[i].Cells["_fBOscColumn"].Value.ToString();
                this._device.FB_APV_VOZVR = this._difensesFBDataGrid.Rows[i].Cells["_fBAPVRetColumn"].Value.ToString();
                this._device.FB_UROV = this._difensesFBDataGrid.Rows[i].Cells["_fBUROVColumn"].Value.ToString();
                this._device.FB_APV = this._difensesFBDataGrid.Rows[i].Cells["_fBAPVColumn"].Value.ToString();
                this._device.FB_AVR = this._difensesFBDataGrid.Rows[i].Cells["_fBAVRColumn"].Value.ToString();
                this._device.FB_DROP = this._difensesFBDataGrid.Rows[i].Cells["_fBSbrosColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzfmax[i] = this._device.sMtzFMax;
            }
        }

        #endregion

        #region Защиты F<
        public void PrepareFMGrid()
        {
            this.FillFMGrid();
            if (this._difensesFMDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 4; i++)
                {
                    this._difensesFMDataGrid.Rows.Add(new object[]{"Ступень F< " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              0,
                                                              0,
                                                              0,
                                                              0,
                                                              Strings.YesNo[0],
                                                              Strings.SygnalInputSignals[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.YesNo[0]

                    });
                }
            }
        }

        public void FillFMGrid()
        {
            if (this._fMModesColumn.Items.Count == 0)
            {
                this._fMModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._fMUvzYNColumn.Items.Count == 0)
            {
                this._fMUvzYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._fMBlockingColumn.Items.Count == 0)
            {
                this._fMBlockingColumn.Items.AddRange(Strings.SygnalInputSignals.ToArray());
            }
            if (this._fMOscColumn.Items.Count == 0)
            {
                this._fMOscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._fMUROVColumn.Items.Count == 0)
            {
                this._fMUROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fMAPVColumn.Items.Count == 0)
            {
                this._fMAPVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fMAVRColumn.Items.Count == 0)
            {
                this._fMAVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fMAPVRetColumn.Items.Count == 0)
            {
                this._fMAPVRetColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._fMSbrosColumn.Items.Count == 0)
            {
                this._fMSbrosColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
        }

        public void ReadFMinGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzfmin.Length; i++)
            {
                this._device.sMtzFMin = this._device.sProtmain.currentprotlist.mtzfmin[i];
                this._difensesFMDataGrid.Rows[i].Cells["_fMModesColumn"].Value = this._device.FM_MODE;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUsrColumn"].Value = this._device.FM_FCP;
                this._difensesFMDataGrid.Rows[i].Cells["_fMTsrColumn"].Value = this._device.FM_TCP;
                this._difensesFMDataGrid.Rows[i].Cells["_fMTvzColumn"].Value = this._device.FM_TVZ;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUvzColumn"].Value = this._device.FM_FVZ;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUvzYNColumn"].Value = this._device.FM_FVZ_YN;
                this._difensesFMDataGrid.Rows[i].Cells["_fMBlockingColumn"].Value = this._device.FM_BLOCK;
                this._difensesFMDataGrid.Rows[i].Cells["_fMOscColumn"].Value = this._device.FM_OSC;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAPVRetColumn"].Value = this._device.FM_APV_VOZVR;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUROVColumn"].Value = this._device.FM_UROV;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAPVColumn"].Value = this._device.FM_APV;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAVRColumn"].Value = this._device.FM_AVR;
                this._difensesFMDataGrid.Rows[i].Cells["_fMSbrosColumn"].Value = this._device.FM_DROP;
            }
        }

        public void ReadFMinGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzumin.Length; i++)
            {
                this._device.sMtzFMin = this._device.sProtreserve.currentprotlist.mtzfmin[i];
                this._difensesFMDataGrid.Rows[i].Cells["_fMModesColumn"].Value = this._device.FM_MODE;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUsrColumn"].Value = this._device.FM_FCP;
                this._difensesFMDataGrid.Rows[i].Cells["_fMTsrColumn"].Value = this._device.FM_TCP;
                this._difensesFMDataGrid.Rows[i].Cells["_fMTvzColumn"].Value = this._device.FM_TVZ;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUvzColumn"].Value = this._device.FM_FVZ;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUvzYNColumn"].Value = this._device.FM_FVZ_YN;
                this._difensesFMDataGrid.Rows[i].Cells["_fMBlockingColumn"].Value = this._device.FM_BLOCK;
                this._difensesFMDataGrid.Rows[i].Cells["_fMOscColumn"].Value = this._device.FM_OSC;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAPVRetColumn"].Value = this._device.FM_APV_VOZVR;
                this._difensesFMDataGrid.Rows[i].Cells["_fMUROVColumn"].Value = this._device.FM_UROV;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAPVColumn"].Value = this._device.FM_APV;
                this._difensesFMDataGrid.Rows[i].Cells["_fMAVRColumn"].Value = this._device.FM_AVR;
                this._difensesFMDataGrid.Rows[i].Cells["_fMSbrosColumn"].Value = this._device.FM_DROP;
            }
        }

        public void WriteFMinGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzfmin.Length; i++)
            {
                this._device.FM_MODE = this._difensesFMDataGrid.Rows[i].Cells["_fMModesColumn"].Value.ToString();
                this._device.FM_FCP = Convert.ToDouble(this._difensesFMDataGrid.Rows[i].Cells["_fMUsrColumn"].Value);
                this._device.FM_TCP = Convert.ToUInt16(this._difensesFMDataGrid.Rows[i].Cells["_fMTsrColumn"].Value);
                this._device.FM_TVZ = Convert.ToInt32(this._difensesFMDataGrid.Rows[i].Cells["_fMTvzColumn"].Value);
                this._device.FM_FVZ = Convert.ToDouble(this._difensesFMDataGrid.Rows[i].Cells["_fMUvzColumn"].Value);
                this._device.FM_FVZ_YN = this._difensesFMDataGrid.Rows[i].Cells["_fMUvzYNColumn"].Value.ToString();
                this._device.FM_BLOCK = this._difensesFMDataGrid.Rows[i].Cells["_fMBlockingColumn"].Value.ToString();
                this._device.FM_OSC = this._difensesFMDataGrid.Rows[i].Cells["_fMOscColumn"].Value.ToString();
                this._device.FM_APV_VOZVR = this._difensesFMDataGrid.Rows[i].Cells["_fMAPVRetColumn"].Value.ToString();
                this._device.FM_UROV = this._difensesFMDataGrid.Rows[i].Cells["_fMUROVColumn"].Value.ToString();
                this._device.FM_APV = this._difensesFMDataGrid.Rows[i].Cells["_fMAPVColumn"].Value.ToString();
                this._device.FM_AVR = this._difensesFMDataGrid.Rows[i].Cells["_fMAVRColumn"].Value.ToString();
                this._device.FM_DROP = this._difensesFMDataGrid.Rows[i].Cells["_fMSbrosColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzfmin[i] = this._device.sMtzFMin;
            }
        }

        public void WriteFMinGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzfmin.Length; i++)
            {
                this._device.FM_MODE = this._difensesFMDataGrid.Rows[i].Cells["_fMModesColumn"].Value.ToString();
                this._device.FM_FCP = Convert.ToDouble(this._difensesFMDataGrid.Rows[i].Cells["_fMUsrColumn"].Value);
                this._device.FM_TCP = Convert.ToUInt16(this._difensesFMDataGrid.Rows[i].Cells["_fMTsrColumn"].Value);
                this._device.FM_TVZ = Convert.ToInt32(this._difensesFMDataGrid.Rows[i].Cells["_fMTvzColumn"].Value);
                this._device.FM_FVZ = Convert.ToDouble(this._difensesFMDataGrid.Rows[i].Cells["_fMUvzColumn"].Value);
                this._device.FM_FVZ_YN = this._difensesFMDataGrid.Rows[i].Cells["_fMUvzYNColumn"].Value.ToString();
                this._device.FM_BLOCK = this._difensesFMDataGrid.Rows[i].Cells["_fMBlockingColumn"].Value.ToString();
                this._device.FM_OSC = this._difensesFMDataGrid.Rows[i].Cells["_fMOscColumn"].Value.ToString();
                this._device.FM_APV_VOZVR = this._difensesFMDataGrid.Rows[i].Cells["_fMAPVRetColumn"].Value.ToString();
                this._device.FM_UROV = this._difensesFMDataGrid.Rows[i].Cells["_fMUROVColumn"].Value.ToString();
                this._device.FM_APV = this._difensesFMDataGrid.Rows[i].Cells["_fMAPVColumn"].Value.ToString();
                this._device.FM_AVR = this._difensesFMDataGrid.Rows[i].Cells["_fMAVRColumn"].Value.ToString();
                this._device.FM_DROP = this._difensesFMDataGrid.Rows[i].Cells["_fMSbrosColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzfmin[i] = this._device.sMtzFMin;
            }
        }
        #endregion

        #region Внешние
        public void PrepareExternalDifensesGrid()
        {
            this.FillExternalDifensesGrid();
            if (this._externalDifensesDataGrid.Rows.Count == 0)
            {
                for (int i = 0; i < 16; i++)
                {
                    this._externalDifensesDataGrid.Rows.Add(new object[]{"Внешняя " + (i+1).ToString(),
                                                              Strings.ModesLightMode[0],
                                                              Strings.SygnalSrabExternal[0],
                                                              0,
                                                              0,
                                                              Strings.SygnalSrabExternal[0],
                                                              Strings.YesNo[0],
                                                              Strings.SygnalSrabExternal[0],
                                                              Strings.ModesLightOsc[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.ModesLight[0],
                                                              Strings.YesNo[0]

                    });
                }
            }
        }

        public void FillExternalDifensesGrid()
        {
            if (this._externalDifModesColumn.Items.Count == 0)
            {
                this._externalDifModesColumn.Items.AddRange(Strings.ModesLightMode.ToArray());
            }
            if (this._externalDifSrabColumn.Items.Count == 0)
            {
                this._externalDifSrabColumn.Items.AddRange(Strings.SygnalSrabExternal.ToArray());
            }
            if (this._externalDifVozvrColumn.Items.Count == 0)
            {
                this._externalDifVozvrColumn.Items.AddRange(Strings.SygnalSrabExternal.ToArray());
            }
            if (this._externalDifVozvrYNColumn.Items.Count == 0)
            {
                this._externalDifVozvrYNColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
            if (this._externalDifBlockingColumn.Items.Count == 0)
            {
                this._externalDifBlockingColumn.Items.AddRange(Strings.SygnalSrabExternal.ToArray());
            }
            if (this._externalDifOscColumn.Items.Count == 0)
            {
                this._externalDifOscColumn.Items.AddRange(Strings.ModesLightOsc.ToArray());
            }
            if (this._externalDifUROVColumn.Items.Count == 0)
            {
                this._externalDifUROVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._externalDifAPVColumn.Items.Count == 0)
            {
                this._externalDifAPVColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._externalDifAVRColumn.Items.Count == 0)
            {
                this._externalDifAVRColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._externalDifAPVRetColumn.Items.Count == 0)
            {
                this._externalDifAPVRetColumn.Items.AddRange(Strings.ModesLight.ToArray());
            }
            if (this._externalDifSbrosColumn.Items.Count == 0)
            {
                this._externalDifSbrosColumn.Items.AddRange(Strings.YesNo.ToArray());
            }
        }


        public void ReadExternalGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzext.Length; i++)
            {
                this._device.sMtzExt = this._device.sProtmain.currentprotlist.mtzext[i];
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value = this._device.EXTERNAL_MODE;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value = this._device.EXTERNAL_SRAB;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value = this._device.EXTERNAL_TSR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value = this._device.EXTERNAL_TVZ;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value = this._device.EXTERNAL_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value = this._device.EXTERNAL_VOZVR_YN;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value = this._device.EXTERNAL_BLOCK;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value = this._device.EXTERNAL_OSC;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVRetColumn"].Value = this._device.EXTERNAL_APV_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value = this._device.EXTERNAL_UROV;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVColumn"].Value = this._device.EXTERNAL_APV;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAVRColumn"].Value = this._device.EXTERNAL_AVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSbrosColumn"].Value = this._device.EXTERNAL_DROP;
            }
        }

        public void ReadExternalGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzext.Length; i++)
            {
                this._device.sMtzExt = this._device.sProtreserve.currentprotlist.mtzext[i];
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value = this._device.EXTERNAL_MODE;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value = this._device.EXTERNAL_SRAB;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value = this._device.EXTERNAL_TSR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value = this._device.EXTERNAL_TVZ;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value = this._device.EXTERNAL_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value = this._device.EXTERNAL_VOZVR_YN;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value = this._device.EXTERNAL_BLOCK;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value = this._device.EXTERNAL_OSC;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVRetColumn"].Value = this._device.EXTERNAL_APV_VOZVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value = this._device.EXTERNAL_UROV;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVColumn"].Value = this._device.EXTERNAL_APV;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAVRColumn"].Value = this._device.EXTERNAL_AVR;
                this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSbrosColumn"].Value = this._device.EXTERNAL_DROP;
            }
        }

        public void WriteExternalGrid()
        {
            for (int i = 0; i < this._device.sProtmain.currentprotlist.mtzext.Length; i++)
            {
                this._device.EXTERNAL_MODE = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value.ToString();
                this._device.EXTERNAL_SRAB = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value.ToString();
                this._device.EXTERNAL_TSR = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value);
                this._device.EXTERNAL_TVZ = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value);
                this._device.EXTERNAL_VOZVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value.ToString();
                this._device.EXTERNAL_VOZVR_YN = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value.ToString();
                this._device.EXTERNAL_BLOCK = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value.ToString();
                this._device.EXTERNAL_OSC = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value.ToString();
                this._device.EXTERNAL_APV_VOZVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVRetColumn"].Value.ToString();
                this._device.EXTERNAL_UROV = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value.ToString();
                this._device.EXTERNAL_APV = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVColumn"].Value.ToString();
                this._device.EXTERNAL_AVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAVRColumn"].Value.ToString();
                this._device.EXTERNAL_DROP = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSbrosColumn"].Value.ToString();
                this._device.sProtmain.currentprotlist.mtzext[i] = this._device.sMtzExt;
            }
        }

        public void WriteExternalGridReserve()
        {
            for (int i = 0; i < this._device.sProtreserve.currentprotlist.mtzext.Length; i++)
            {
                this._device.EXTERNAL_MODE = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifModesColumn"].Value.ToString();
                this._device.EXTERNAL_SRAB = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSrabColumn"].Value.ToString();
                this._device.EXTERNAL_TSR = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTsrColumn"].Value);
                this._device.EXTERNAL_TVZ = Convert.ToInt32(this._externalDifensesDataGrid.Rows[i].Cells["_externalDifTvzColumn"].Value);
                this._device.EXTERNAL_VOZVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrColumn"].Value.ToString();
                this._device.EXTERNAL_VOZVR_YN = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifVozvrYNColumn"].Value.ToString();
                this._device.EXTERNAL_BLOCK = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifBlockingColumn"].Value.ToString();
                this._device.EXTERNAL_OSC = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifOscColumn"].Value.ToString();
                this._device.EXTERNAL_APV_VOZVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVRetColumn"].Value.ToString();
                this._device.EXTERNAL_UROV = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifUROVColumn"].Value.ToString();
                this._device.EXTERNAL_APV = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAPVColumn"].Value.ToString();
                this._device.EXTERNAL_AVR = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifAVRColumn"].Value.ToString();
                this._device.EXTERNAL_DROP = this._externalDifensesDataGrid.Rows[i].Cells["_externalDifSbrosColumn"].Value.ToString();
                this._device.sProtreserve.currentprotlist.mtzext[i] = this._device.sMtzExt;
            }
        }
        #endregion

        #region Осциллограф
        public void PrepareOSC()
        {
            this._OSCCombos = new ComboBox[] {
                this._oscLength, this._oscFix};

            this._ULONGOSCMasketTextBoxes = new MaskedTextBox[] {this._oscWriteLength };

            this.ClearCombos(this._OSCCombos);

            this.FillOSC();

            this.SubscriptCombos(this._OSCCombos);

            this.PrepareMaskedBoxes(this._ULONGOSCMasketTextBoxes, typeof(ulong));

        }

        public void FillOSC()
        {
            this._oscWriteLength.Text = "0";
            if (this._oscLength.Items.Count == 0)
            {
                this._oscLength.Items.AddRange(Strings.OscLength.ToArray());
            }
            if (this._oscFix.Items.Count == 0)
            {
                this._oscFix.Items.AddRange(Strings.OscFix.ToArray());
            }
        }

        public void ReadOSC()
        {
            this._oscLength.SelectedItem = this._device.OSC_LENGTH;
            this._oscWriteLength.Text = this._device.OSC_W_LENGTH.ToString();
            this._oscFix.SelectedItem = this._device.OSC_FIX;
        }

        public bool WriteOSC()
        {
            bool res = true;
            try
            {
                this._device.OSC_LENGTH = this._oscLength.SelectedItem.ToString();
                this._device.OSC_W_LENGTH = Convert.ToUInt16(this._oscWriteLength.Text);
                this._device.OSC_FIX = this._oscFix.SelectedItem.ToString();

            }
            catch
            {
                res = false;
            }
            return res;
        }
        #endregion

        #region Осциллограф Каналы
        public void PrepareOSCCHANNELS()
        {
            this.FillOSCCHANNELSGrid();
            if (this._oscChannels.Rows.Count == 0)
            {
                for (int i = 0; i < this._device.sOsc.kanal.Length; i++)
                {
                    this._oscChannels.Rows.Add(new object[]{(i+1).ToString(),
                                                       Strings.SygnalSrab[0]
                    });
                }
            }
        }


        public void FillOSCCHANNELSGrid()
        {
            if (this._oscSygnal.Items.Count == 0)
            {
                this._oscSygnal.Items.AddRange(Strings.SygnalSrab.ToArray());
            }
        }

        public void ReadOSCCHANNELS()
        {
            for (int i = 0; i < this._device.sOsc.kanal.Length; i++)
            {
                this._oscChannels.Rows[i].Cells["_oscSygnal"].Value = Strings.SygnalSrab[this._device.sOsc.kanal[i]];
            }
        }

        public bool WriteOSCCHANNELS()
        {
            bool res = true;
            try
            {
                for (int i = 0; i < this._device.sOsc.kanal.Length; i++)
                {
                    this._device.sOsc.kanal[i] = (ushort)Strings.SygnalSrab.IndexOf(this._oscChannels.Rows[i].Cells["_oscSygnal"].Value.ToString());
                }
            }
            catch
            {
                res = false;
            }
            return res;
        }
        #endregion

        public bool WriteALLOSC()
        {
            bool res = true;
            try
            {
                this.WriteOSC();
                this.WriteOSCCHANNELS();
                this._device.MemoryMap["Осциллограмма"].StructObj = this._device.sOsc;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        public void PrepareCorrentProtAll()
        {
            this.PrepareCORNERS();
            this.PrepareIGrid();
            this.PrepareI0Grid();
            this.PrepareI2I1();

            this.PrepareUBGrid();
            this.PrepareUMGrid();
            this.PrepareFBGrid();
            this.PrepareFMGrid();
            this.PrepareExternalDifensesGrid();
        }

        public void PrepareAll()
        {
            this.PreparePARAMMEASURE();
            this.PrepareSwich();
            this.PrepareOSC();
            this.PrepareOSCCHANNELS();
            this.PrepareApv();
            this.PrepareAvr();
            this.PrepareLZH();
            this.PrepareINPSIGNAL();
            this.PrepareINPUTSYGNAL();
            this.PrepareELSSYGNAL();
            this.PrepareCorrentProtAll();
            this.PreparePARAMAUTOMAT();

        }

        public void ReadCurrentProtMain()
        {
            this.ReadCORNERS();
            this.ReadIGrid();
            this.ReadI0Grid();
            this.ReadI2I1();

            this.ReadFMaxGrid();
            this.ReadFMinGrid();
            this.ReadUMaxGrid();
            this.ReadUMinGrid();
            this.ReadExternalGrid();
        }

        public void ReadCurrentProtReserve()
        {
            this.ReadCORNERSReserve();
            this.ReadIGridReserve();
            this.ReadI0GridReserve();
            this.ReadI2I1Reserve();

            this.ReadFMaxGridReserve();
            this.ReadFMinGridReserve();
            this.ReadUMaxGridReserve();
            this.ReadUMinGridReserve();
            this.ReadExternalGridReserve();
        }

        public void WriteCurrentProtMain()
        {
            this.WriteCORNERS();
            this.WriteIGrid();
            this.WriteI0Grid();
            this.WriteI2I1();

            this.WriteFMaxGrid();
            this.WriteFMinGrid();
            this.WriteUMaxGrid();
            this.WriteUMinGrid();
            this.WriteExternalGrid();
        }

        public void WriteCurrentProtReserve()
        {
            this.WriteCORNERSReserve();
            this.WriteIGridReserve();
            this.WriteI0GridReserve();
            this.WriteI2I1Reserve();

            this.WriteFMaxGridReserve();
            this.WriteFMinGridReserve();
            this.WriteUMaxGridReserve();
            this.WriteUMinGridReserve();
            this.WriteExternalGridReserve();
        }

        public bool WriteCurrentProtAll()
        {
            bool res = true;
            try
            {
                if (this._mainRadioButton.Checked)
                {
                    this.WriteCurrentProtMain();
                }
                else
                {
                    this.WriteCurrentProtReserve();
                }

                this._device.MemoryMap["Защиты_Основные"].StructObj = this._device.sProtmain;
                this._device.MemoryMap["Защиты_Резервные"].StructObj = this._device.sProtreserve;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        #region Дополнительные функции
        private void LoadConfigurationBlocks()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._configProgressBar.Value = 0;
            this._configProgressBar.Maximum = this._device.MemoryMap.Count;
            this._processLabel.Text = "Идет чтение";
            this.readFailFlag = true;
            this.structsIsRead = 0;
            this.PreparePARAMMEASURE();
            this.PrepareSwich();
            this.PrepareOSC();
            this.PrepareOSCCHANNELS();
            this.PrepareApv();
            this.PrepareAvr();
            this.PrepareLZH();
            this.PrepareINPSIGNAL();
            this.PrepareINPUTSYGNAL();
            this.PrepareELSSYGNAL();
            this.PrepareCorrentProtAll();
            this.PreparePARAMAUTOMAT();
            foreach (StObj _struct in this._device.MemoryMap.Values)
            {
                _struct.LoadStruckt(this._device);
            }
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
        }

        private void StructRead(object struckObj)
        {
            if (struckObj != null)
            {
                if (struckObj is SWITCH)
                {
                    this._device.sSwitch = (SWITCH)struckObj;

                    this.ReadSwich();
                    this.ProgressBarInc();
                }
                if (struckObj is SihronizmStruct)
                {
                    this._sinx = (SihronizmStruct)struckObj;
                    this.ShowSinx();
                    this.ProgressBarInc();
                }

                if (struckObj is APV)
                {
                    this._device.sApv = (APV)struckObj;
                    this.ReadAPV();
                    this.ProgressBarInc();
                }
                if (struckObj is AVR)
                {
                    this._device.sAvr = (AVR)struckObj;
                    this.ReadAVR();
                    this.ProgressBarInc();
                }
                if (struckObj is LPB)
                {
                    this._device.sLpb = (LPB)struckObj;
                    this.ReadLPB();
                    this.ProgressBarInc();
                }
                if (struckObj is AUTOBLOWER)
                {
                    this._device.sAutoblower = (AUTOBLOWER)struckObj;
                    this.ProgressBarInc();
                }
                if (struckObj is TERMAL)
                {
                    this._device.sTermall = (TERMAL)struckObj;

                    this.ProgressBarInc();
                }
                if (struckObj is INPUTSIGNAL)
                {
                    this._device.sInputsignal = (INPUTSIGNAL)struckObj;
                    this.ReadINPUTSYGNAL();
                    this.ProgressBarInc();
                }
                if (struckObj is OSCOPE)
                {
                    this._device.sOsc = (OSCOPE)struckObj;
                    this.ReadOSC();
                    this.ReadOSCCHANNELS();
                    this.ProgressBarInc();
                }
                if (struckObj is MEASURETRANS)
                {
                    this._device.sMeasuretrans = (MEASURETRANS)struckObj;
                    this.Read_TT_TTNP();
                    this.ProgressBarInc();
                }
                if (struckObj is INPSYGNAL)
                {
                    this._device.sInpsignal = (INPSYGNAL)struckObj;
                    this.ReadINPSIGNAL();
                    this.ProgressBarInc();
                }
                if (struckObj is ELSSYGNAL)
                {
                    this._device.sElssignal = (ELSSYGNAL)struckObj;
                    this.ReadELSSYGNAL();
                    this.ProgressBarInc();
                }
                if (struckObj is CURRENTPROTMAIN)
                {
                    this._device.sProtmain = (CURRENTPROTMAIN)struckObj;
                    if (this._mainRadioButton.Checked)
                    {
                        this.ReadCurrentProtMain();
                    }
                    this.ProgressBarInc();
                }
                if (struckObj is CURRENTPROTRESERVE)
                {
                    this._device.sProtreserve = (CURRENTPROTRESERVE)struckObj;
                    if (this._reserveRadioButton.Checked)
                    {
                        this.ReadCurrentProtReserve();
                    }
                    this.ProgressBarInc();
                }
                if (struckObj is PARAMAUTOMAT)
                {
                    this._device.sParamautomat = (PARAMAUTOMAT)struckObj;
                    this.ReadPARAMAUTOMAT();
                    this.ProgressBarInc();
                }
                if (struckObj is CONFIGSYSTEM)
                {
                    this._device.sConfigsystem = (CONFIGSYSTEM)struckObj;
                    this.ProgressBarInc();
                }
                if (struckObj is CONFOMP)
                {
                    this._device.sConfOMP = (CONFOMP)struckObj;
                    this.Read_OMP();
                    this.ProgressBarInc();
                }
            }
        }

        private void ShowSinx()
        {
            #region [Синхронизм]

            #region [Общие уставки]
            this._sinhrUminOts.Text = this._sinx.UminOts.ToString();
            this._sinhrUminNal.Text = this._sinx.UminNal.ToString();
            this._sinhrUmaxNal.Text = this._sinx.UmaxNal.ToString();
            this._sinhrTwait.Text = this._sinx.Twait.ToString();
            this._sinhrTsinhr.Text = this._sinx.Tsinhr.ToString();
            this._sinhrTon.Text = this._sinx.Ton.ToString();
            this._sinhrU1.SelectedItem = this._sinx.U1;
            this._sinhrU2.SelectedItem = this._sinx.U2;
            #endregion [Общие уставки]


            #region [Уставки ручного включения]

            this._sinhrManualMode.SelectedItem = this._sinx.Manual.Mode;
            this._sinhrManualUmax.Text = this._sinx.Manual.DUmax.ToString();
            this._sinhrManualNoYes.SelectedItem = this._sinx.Manual.NoYes;
            this._sinhrManualYesNo.SelectedItem = this._sinx.Manual.YesNo;
            this._sinhrManualNoNo.SelectedItem = this._sinx.Manual.NoNo;
            this._sinhrManualdF.Text = this._sinx.Manual.DfSinhr.ToString();
            this._sinhrManualdFi.Text = this._sinx.Manual.Dfi.ToString();
            this._sinhrManualdFno.Text = this._sinx.Manual.DfNosinhr.ToString();
            #endregion [Уставки ручного включения]


            #region [Уставки автоматического включения]

            this._sinhrAutoMode.SelectedItem = this._sinx.Automatic.Mode;
            this._sinhrAutoUmax.Text = this._sinx.Automatic.DUmax.ToString();
            this._sinhrAutoNoYes.SelectedItem = this._sinx.Automatic.NoYes;
            this._sinhrAutoYesNo.SelectedItem = this._sinx.Automatic.YesNo;
            this._sinhrAutoNoNo.SelectedItem = this._sinx.Automatic.NoNo;
            this._sinhrAutodF.Text = this._sinx.Automatic.DfSinhr.ToString();
            this._sinhrAutodFi.Text = this._sinx.Automatic.Dfi.ToString();
            this._sinhrAutodFno.Text = this._sinx.Automatic.DfNosinhr.ToString();
            #endregion [Уставки автоматического включения]

            #endregion [Синхронизм]
        }

        private int structsIsRead;
        private void ProgressBarInc()
        {
            this._configProgressBar.PerformStep();
            this.structsIsRead++;
            if (this.structsIsRead >= this._device.MemoryMap.Count)
            {
                this._processLabel.Text = "Конфигурация прочитана";
                MessageBox.Show("Конфигурация прочитана");
            }
        }
        #endregion

        #region Обработчики событий
        void MLK_Configuration_AllReadOk(object sender)
        {
            if (sender is StObj)
            {
                try
                {
                    Invoke(new Handler(this.StructRead), (sender as StObj).StructObj);
                }
                catch { }
            }
        }

        private void Configuration_Load(object sender, EventArgs e)
        {
            #region [Синхронизм]

             
            this._sinhrU1.DataSource = StringsConfig.Usinhr;
            this._sinhrU2.DataSource = StringsConfig.Usinhr;

            this._sinhrManualMode.DataSource = StringsConfig.OffOn;
            this._sinhrAutoMode.DataSource = StringsConfig.OffOn;

            this._sinhrManualNoYes.DataSource = StringsConfig.YesNo;
            this._sinhrManualYesNo.DataSource = StringsConfig.YesNo;
            this._sinhrManualNoNo.DataSource = StringsConfig.YesNo;
            this._sinhrAutoNoYes.DataSource = StringsConfig.YesNo;
            this._sinhrAutoYesNo.DataSource = StringsConfig.YesNo;
            this._sinhrAutoNoNo.DataSource = StringsConfig.YesNo;
            this.PrepareAll();
            #endregion [Синхронизм]

            foreach (StObj _struct in this._device.MemoryMap.Values)
            {
                _struct.AllReadOk += new StObj.ReadArray(this.MLK_Configuration_AllReadOk);
                _struct.AllReadFail += (a) =>
                    {
                        try
                        {
                            Invoke(new Action(this._struct_AllReadFail));
                        }
                        catch (Exception)
                        {
                        }
                    };
                _struct.AllWriteOk += (a) =>
                     {
                         try
                         {
                             Invoke(new Action(this._struct_AllWriteOk));
                         }
                         catch (Exception)
                         {
                         }
                     };
            }
            if (Device.AutoloadConfig)
            {
                this.LoadConfigurationBlocks();
            }
        }

        private void _struct_AllWriteOk()
        {
            this._configProgressBar.PerformStep();
        }

        private bool readFailFlag;
        private SihronizmStruct _sinx;
        void _struct_AllReadFail()
        {
            if (this.readFailFlag)
            {
                this._processLabel.Text = "Ошибка чтения конфигурации";
                MessageBox.Show("Ошибка чтения конфигурации");
                this.readFailFlag = false;
            }
        }

        private void _readConfigBut_Click(object sender, EventArgs e)
        {
            this.LoadConfigurationBlocks();
        }
        #endregion

        #region Дополнительные функции
        private void ClearCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].Items.Clear();
            }
        }

        private void SubscriptCombos(ComboBox[] combos)
        {
            for (int i = 0; i < combos.Length; i++)
            {
                combos[i].DropDown += new EventHandler(this.Combo_DropDown);
                combos[i].SelectedIndexChanged += new EventHandler(this.Combo_SelectedIndexChanged);
                combos[i].SelectedIndex = 0;
            }
        }

        private void PrepareMaskedBoxes(MaskedTextBox[] boxes, Type validateType)
        {
            for (int i = 0; i < boxes.Length; i++)
            {
                boxes[i].TypeValidationCompleted += new TypeValidationEventHandler(this.TypeValidation);
                boxes[i].ValidatingType = validateType;
            }
        }

        private void TypeValidation(object sender, TypeValidationEventArgs e)
        {
            if (sender is MaskedTextBox)
            {
                MaskedTextBox box = sender as MaskedTextBox;

                if (!e.IsValidInput)
                {
                    this.ShowToolTip(box);

                }
                else
                {
                    if (box.ValidatingType == typeof(ulong) && ulong.Parse(box.Text) > ulong.Parse(box.Tag.ToString()))
                    {
                        this.ShowToolTip(box);
                    }
                    if (box.ValidatingType == typeof(double) && (double.Parse(box.Text) > double.Parse(box.Tag.ToString())) || (double.Parse(box.Text) < 0))
                    {
                        this.ShowToolTip(box);
                    }
                }

            }
        }

        private void ShowToolTip(MaskedTextBox box)
        {
            if (this._validatingOk)
            {
                if (box.Parent.Parent is TabPage)
                {
                    if (box.Parent.Parent.Parent.Parent.Parent is TabPage)
                    {
                        this._configurationTabControl.SelectedTab = box.Parent.Parent.Parent.Parent.Parent as TabPage;
                        this._difensesTC.SelectedTab = box.Parent.Parent as TabPage;
                    }
                    else
                    {
                        this._configurationTabControl.SelectedTab = box.Parent.Parent as TabPage;
                    }
                }
                switch (box.ValidatingType.Name)
                {
                    case "Double":
                        this._toolTip.Show("Введите число в диапазоне [0-" + box.Tag + "]", box, 2000);
                        break;
                    case "UInt64":
                        this._toolTip.Show("Введите целое число в диапазоне [0-" + box.Tag + "]", box, 2000);
                        break;
                    default: break;
                }
                box.Focus();
                box.SelectAll();
                //  _validatingOk = false;
            }
        }

        private void Combo_DropDown(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.DropDown -= new EventHandler(this.Combo_DropDown);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }

        }

        private void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            combo.SelectedIndexChanged -= new EventHandler(this.Combo_SelectedIndexChanged);
            if (combo.Items.Contains("ХХХХХ"))
            {
                combo.Items.Remove("ХХХХХ");
            }
        }
        #endregion

        private void _writeConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.WriteConfig();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        private void WriteConfig()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.writeComplite = true;

            this._configProgressBar.Maximum = Common.VersionConverter(this._device.DeviceVersion) >= 1.02 ? 14 : 13;
            this._configProgressBar.Value = 0;
            this._validatingOk = true;

            this.ValidateAll();

            if (!this._validatingGB)
            {
                this._validatingOk = false;
                this.ShowError();
            }

            if (this._validatingOk)
            {
                if (DialogResult.Yes == MessageBox.Show("Записать конфигурацию МР763 №" + this._device.DeviceNumber + " ?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                {
                    this._processLabel.Text = "Идет запись";
                    this._device.MemoryMap["Измерительный_Трансформатор"].SaveStruckt();
                    this._device.MemoryMap["ОМП"].SaveStruckt();
                    this._device.MemoryMap["Выключатель"].SaveStruckt();
                    this._device.MemoryMap["АПВ"].SaveStruckt();
                    this._device.MemoryMap["АВР"].SaveStruckt();
                    this._device.MemoryMap["ЛПБ"].SaveStruckt();
                    this._device.MemoryMap["Входные_Сигналы"].SaveStruckt();
                    this._device.MemoryMap["Входные_ЛС"].SaveStruckt();
                    this._device.MemoryMap["Защиты_Основные"].SaveStruckt();
                    this._device.MemoryMap["Защиты_Резервные"].SaveStruckt();
                    this._device.MemoryMap["Осциллограмма"].SaveStruckt();
                    this._device.MemoryMap["Выходные_ЛС"].SaveStruckt();
                    this._device.MemoryMap["Автоматика"].SaveStruckt();
                    if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                    {
                        this._device.MemoryMap["Синхронизм"].StructObj = this._sinx;
                        this._device.MemoryMap["Синхронизм"].SaveStruckt();
                    }

                    this._device.ConfirmConstraint();
                }
            }
        }



        private void ValidateAll()
        {
            this._validatingOk = true;

            if (this._validatingOk)
            {
                this._validatingOk = this._validatingOk && this.WritePARAMMEASURE()
                                              && this.WriteSwich()
                                              && this.WriteApv()
                                              && this.WriteALLOSC()
                                              && this.WriteAvr()
                                              && this.WriteLzh()
                                              && this.WriteINPSIGNAL()
                                              && this.WriteELSSYGNAL()
                                              && this.WriteINPUTSYGNAL()
                                              && this.WriteParamAutomat()
                                              && this.WriteCurrentProtAll()
                                              && this.WriteSinx();
            }
        }

        private bool WriteSinx()
        {
            try
            {
                #region [Синхронизм]

                #region [Общие уставки]

                this._sinx.UminOts = Convert.ToDouble(this._sinhrUminOts.Text);
                this._sinx.UminNal = Convert.ToDouble(this._sinhrUminNal.Text);
                this._sinx.UmaxNal = Convert.ToDouble(this._sinhrUmaxNal.Text);
                this._sinx.Twait = Convert.ToInt32(this._sinhrTwait.Text);
                this._sinx.Tsinhr = Convert.ToInt32(this._sinhrTsinhr.Text);
                this._sinx.Ton = Convert.ToUInt16(this._sinhrTon.Text);
                this._sinx.U1 = this._sinhrU1.SelectedItem.ToString();
                this._sinx.U2 = this._sinhrU2.SelectedItem.ToString();

                #endregion [Общие уставки]


                #region [Уставки ручного включения]

                this._sinx.Manual.Mode = this._sinhrManualMode.SelectedItem.ToString();
                this._sinx.Manual.DUmax = Convert.ToDouble(this._sinhrManualUmax.Text);
                this._sinx.Manual.NoYes = this._sinhrManualNoYes.SelectedItem.ToString();
                this._sinx.Manual.YesNo = this._sinhrManualYesNo.SelectedItem.ToString();
                this._sinx.Manual.NoNo = this._sinhrManualNoNo.SelectedItem.ToString();
                this._sinx.Manual.DfSinhr = Convert.ToDouble(this._sinhrManualdF.Text);
                this._sinx.Manual.Dfi = Convert.ToUInt16(this._sinhrManualdFi.Text);
                this._sinx.Manual.DfNosinhr = Convert.ToDouble(this._sinhrManualdFno.Text);

                #endregion [Уставки ручного включения]


                #region [Уставки автоматического включения]

                this._sinx.Automatic.Mode = this._sinhrAutoMode.SelectedItem.ToString();
                this._sinx.Automatic.DUmax = Convert.ToDouble(this._sinhrAutoUmax.Text);
                this._sinx.Automatic.NoYes = this._sinhrAutoNoYes.SelectedItem.ToString();
                this._sinx.Automatic.YesNo = this._sinhrAutoYesNo.SelectedItem.ToString();
                this._sinx.Automatic.NoNo = this._sinhrAutoNoNo.SelectedItem.ToString();
                this._sinx.Automatic.DfSinhr = Convert.ToDouble(this._sinhrAutodF.Text);
                this._sinx.Automatic.Dfi = Convert.ToUInt16(this._sinhrAutodFi.Text);
                this._sinx.Automatic.DfNosinhr = Convert.ToDouble(this._sinhrAutodFno.Text);

                #endregion [Уставки автоматического включения]

                #endregion [Синхронизм]

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        
        public void ShowError()
        {
            try
            {
                this._difensesTC.SelectedTab = this.ErrorPage;
                this._configurationTabControl.SelectedTab = this.ErrorMainPage;
                this._toolTip.Show("Введите " + this.ErrorMes + " число в диапазоне [0-" + this.ErrorCoord[2] + "]", this, this.ErrorGrid.Left + this.ErrorGrid.Parent.Left + this.ErrorGrid.Parent.Parent.Left + this.ErrorGrid.Parent.Parent.Parent.Left + this.ErrorGrid.Parent.Parent.Parent.Parent.Left + this.ErrorGrid.Parent.Parent.Parent.Parent.Parent.Left, this.ErrorGrid.Top + this.ErrorGrid.Parent.Top + this.ErrorGrid.Parent.Parent.Top + this.ErrorGrid.Parent.Parent.Parent.Top + this.ErrorGrid.Parent.Parent.Parent.Parent.Top + this.ErrorGrid.Parent.Parent.Parent.Parent.Parent.Top, 2000);
                this.ErrorGrid.Rows[this.ErrorCoord[0]].Cells[this.ErrorCoord[1]].Selected = true;
                this.ErrorGrid.CurrentCell = this.ErrorGrid.Rows[this.ErrorCoord[0]].Cells[this.ErrorCoord[1]];
            }
            catch { }
        }

        private void DataGrid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is DataGridViewComboBoxEditingControl)
            {
                DataGridViewComboBoxEditingControl combo = e.Control as DataGridViewComboBoxEditingControl;
                if (combo.Items[combo.Items.Count - 1].ToString() == "ХХХХХ")
                {
                    combo.Items.RemoveAt(combo.Items.Count - 1);
                }
            }
        }

        private void _mainRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._mainRadioButton.Checked)
            {
                this.WriteCurrentProtReserve();
                this.ReadCurrentProtMain();
            }
        }

        private void _reserveRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (this._reserveRadioButton.Checked)
            {
                this.WriteCurrentProtMain();
                this.ReadCurrentProtReserve();
            }
        }

        private void _saveConfigBut_Click(object sender, EventArgs e)
        {
            if (TextboxSupport.PassedValidation)
            {
                this.SaveinFile();
            }
            else
            {
                MessageBox.Show("На одной из открытых форм поле(я) для ввода заполнено(ы) неверно. Действие не будет выполнено.", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void SaveinFile()
        {
            this._saveConfigurationDlg.FileName = string.Format("МР763_Уставки_версия {0:F1}.bin", this._device.DeviceVersion);
            if (DialogResult.OK == this._saveConfigurationDlg.ShowDialog())
            {
                this.ValidateAll();
                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                {
                    this._device.MemoryMap["Синхронизм"].StructObj = this._sinx;
                }
                if (!this._validatingGB)
                {
                    this._validatingOk = false;
                    this.ShowError();
                }

                if (this._validatingOk)
                {
                    this.Serialize(this._saveConfigurationDlg.FileName);
                }
            }
        }
        public void Serialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.AppendChild(doc.CreateElement("MR600"));


                this.AddXmlNode(doc, "Измерительный_Трансформатор", this._device);
                this.AddXmlNode(doc, "ОМП", this._device);
                this.AddXmlNode(doc, "Выключатель", this._device);
                this.AddXmlNode(doc, "АПВ", this._device);
                this.AddXmlNode(doc, "АВР", this._device);
                this.AddXmlNode(doc, "ЛПБ", this._device);
                this.AddXmlNode(doc, "Входные_Сигналы", this._device);
                this.AddXmlNode(doc, "Входные_ЛС", this._device);
                this.AddXmlNode(doc, "Защиты_Основные", this._device);
                this.AddXmlNode(doc, "Защиты_Резервные", this._device);
                this.AddXmlNode(doc, "Осциллограмма", this._device);
                this.AddXmlNode(doc, "Выходные_ЛС", this._device);
                this.AddXmlNode(doc, "Автоматика", this._device);

                if (Common.VersionConverter(this._device.DeviceVersion) >= 1.02)
                {
                    this.AddXmlNode(doc, "Синхронизм", this._device);
                }

                doc.Save(binFileName);
            }
            catch
            {
            }
        }

        private void AddXmlNode(XmlDocument doc, string nodeName, Mr763Device device)
        {
            var values = ((IStructInit)device.MemoryMap[nodeName].StructObj).GetValues();
            XmlElement element = doc.CreateElement(nodeName);
            element.InnerText = Convert.ToBase64String(Common.TOBYTES(values, false));
            if (doc.DocumentElement == null)
            {
                throw new NullReferenceException();
            }
            doc.DocumentElement.AppendChild(element);
        }

        private void _loadConfigBut_Click(object sender, EventArgs e)
        {
            this.ReadFromFile();
        }


        private void ReadFromFile()
        {
            if (DialogResult.OK == this._openConfigurationDlg.ShowDialog())
            {
                try
                {
                    this.Deserialize(this._openConfigurationDlg.FileName);
                    TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
                    TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
                    TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
                    this._processLabel.Text = "Конфигурация загружена из файла";
                    MessageBox.Show("Конфигурация загружена из файла");

                }
                catch (FileLoadException exc)
                {
                    this._processLabel.Text = "Ошибка загрузки конфигурации из файла";
                    MessageBox.Show("Ошибка загрузки конфигурации из файла");
                    return;
                }
            }
        }

        /// <summary>
        /// Загрузка конфигурации из файла
        /// </summary>
        /// <param name="binFileName">Имя файла</param>
        public void Deserialize(string binFileName)
        {
            try
            {
                var doc = new XmlDocument();
                doc.Load(binFileName);
                this._device.MemoryMap["Синхронизм"].StructObj = new SihronizmStruct();
                foreach (XmlNode node in ((XmlNode)doc.ChildNodes[0]).ChildNodes)
                {
                    this.ReadXmlNode(node, this._device);
                }

                this._device.sSwitch = (SWITCH) this._device.MemoryMap["Выключатель"].StructObj;
                this._device.sApv = (APV) this._device.MemoryMap["АПВ"].StructObj;
                this._device.sAvr = (AVR) this._device.MemoryMap["АВР"].StructObj;
                this._device.sLpb = (LPB) this._device.MemoryMap["ЛПБ"].StructObj;
                this._device.sConfOMP = (CONFOMP) this._device.MemoryMap["ОМП"].StructObj;
                this._device.sMeasuretrans = (MEASURETRANS) this._device.MemoryMap["Измерительный_Трансформатор"].StructObj;
                this._device.sParamautomat = (PARAMAUTOMAT) this._device.MemoryMap["Автоматика"].StructObj;
                this._device.sElssignal = (ELSSYGNAL) this._device.MemoryMap["Выходные_ЛС"].StructObj;
                this._device.sInpsignal = (INPSYGNAL) this._device.MemoryMap["Входные_ЛС"].StructObj;
                this._device.sInputsignal = (INPUTSIGNAL) this._device.MemoryMap["Входные_Сигналы"].StructObj;
                this._device.sOsc = (OSCOPE) this._device.MemoryMap["Осциллограмма"].StructObj;
                this._device.sProtmain = (CURRENTPROTMAIN) this._device.MemoryMap["Защиты_Основные"].StructObj;
                this._device.sProtreserve = (CURRENTPROTRESERVE) this._device.MemoryMap["Защиты_Резервные"].StructObj;

                this._sinx = (SihronizmStruct) this._device.MemoryMap["Синхронизм"].StructObj;
                this.ShowSinx();
                this.ReadSwich();
                this.ReadAPV();
                this.ReadAVR();
                this.ReadLPB();
                this.ReadINPUTSYGNAL();
                this.ReadOSC();
                this.ReadOSCCHANNELS();
                this.Read_TT_TTNP();
                this.ReadINPSIGNAL();
                this.ReadELSSYGNAL();
                if (this._mainRadioButton.Checked)
                {
                    this.ReadCurrentProtMain();
                }
                else
                {
                    this.ReadCurrentProtReserve();
                }

                this.ReadPARAMAUTOMAT();
                this.Read_OMP();
            }
            catch
            {

            }
        }

        private void ReadXmlNode(XmlNode doc, Mr763Device device)
        {
            var values = Convert.FromBase64String(doc.InnerText);
            ((IStructInit)device.MemoryMap[doc.Name].StructObj).InitStruct(values);
        }


        private void _oscLength_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = this._oscLength.SelectedIndex;
            this._oscSizeTextBox.Text = (39253 * 2 / (index + 2)).ToString();
        }

        private void Mr763ConfigurationForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Modifiers != Keys.Control) return;
            switch (e.KeyCode)
            {
                case Keys.W:
                    this.WriteConfig();
                    break;
                case Keys.R:
                    this.LoadConfigurationBlocks();
                    break;
                case Keys.S:
                    this.SaveinFile();
                    break;
                case Keys.O:
                    this.ReadFromFile();
                    break;
            }
            e.Handled = true;
        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ((ContextMenuStrip)sender).Close();
            if (e.ClickedItem == this.readFromDeviceItem)
            {
                this.LoadConfigurationBlocks();
                return;
            }
            if (e.ClickedItem == this.writeToDeviceItem)
            {
                this.WriteConfig();
                return;
            }
            if (e.ClickedItem == this.readFromFileItem)
            {
                this.ReadFromFile();
                return;
            }
            if (e.ClickedItem == this.writeToFileItem)
            {
                this.SaveinFile();
                return;
            }
        }

        #region [Events CreateTree]
        private void VLScheckedListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandCurrentTreeNode(this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void treeViewForVLS_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewVLS.DeleteNode(sender, e, this.contextMenu, this.allVlsCheckedListBoxs);
            TreeViewVLS.StateNodes(this.treeViewForVLS);
            TreeViewVLS.CreateTree(this.allVlsCheckedListBoxs, this.VLSTabControl, this.treeViewForVLS);
            TreeViewVLS.ExpandTreeNodes(this.treeViewForVLS);
        }

        private void _inputSignals1_8CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void _inputSignals9_16CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandCurrentTreeNode(this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void treeViewForLsAND_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsAND);
            TreeViewLS.StateNodesAND(this.treeViewForLsAND);
            TreeViewLS.CreateTree(this.dataGridsViewLsAND, this.tabControl1, this.treeViewForLsAND);
            TreeViewLS.ExpandTreeNodesAND(this.treeViewForLsAND);
        }

        private void treeViewForLsOR_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeViewLS.DeleteNode(sender, e, this.contextMenu, this.dataGridsViewLsOR);
            TreeViewLS.StateNodesOR(this.treeViewForLsOR);
            TreeViewLS.CreateTree(this.dataGridsViewLsOR, this.tabControl2, this.treeViewForLsOR);
            TreeViewLS.ExpandTreeNodesOR(this.treeViewForLsOR);
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.contextMenu.Items.Clear();
            this.contextMenu.Items.AddRange(new ToolStripItem[]
            {
                this.readFromDeviceItem,
                this.writeToDeviceItem,
                this.readFromFileItem,
                this.writeToFileItem
            });
        }
        #endregion [Events CreateTree]
    }
}