﻿using System.Collections.Generic;

namespace BEMN.Mr763.Version1.AlarmJournal
{
    public static class AjStringsV1
    {
        /// <summary>
        /// Группа уставок(Журнал аварий)
        /// </summary>
        public static List<string> AlarmJournalSetpointsGroup
        {
            get
            {
                return new List<string>
                    {
                        "Основная",
                        "Резервная"
                    };
            }
        }
        /// <summary>
        /// Сработавшая ступень
        /// </summary>
        public static List<string> Stage
        {
            get
            {
                return new List<string>
                    {
                        "I> 1",
                        "I> 2",
                        "I> 3",
                        "I> 4",
                        "I> 5",
                        "I> 6",
                        "I> 7",
                        "I< 8",
                        "I*> 1",
                        "I*> 2",
                        "I*> 3",
                        "I*> 4",
                        "I*> 5",
                        "I*> 6",
                        "I2/I1",
                        "Iг>",
                        "U> 1",
                        "U> 2",
                        "U> 3",
                        "U> 4",
                        "U< 1",
                        "U< 2",
                        "U< 3",
                        "U< 4",
                        "F> 1",
                        "F> 2",
                        "F> 3",
                        "F> 4",
                        "F< 1",
                        "F< 2",
                        "F< 3",
                        "F< 4",
                        "Резерв",
                        "Резерв",
                        "ВНЕШ. 1",
                        "ВНЕШ. 2",
                        "ВНЕШ. 3",
                        "ВНЕШ. 4",
                        "ВНЕШ. 5",
                        "ВНЕШ. 6",
                        "ВНЕШ. 7",
                        "ВНЕШ. 8",
                        "ВНЕШ. 9",
                        "ВНЕШ. 10",
                        "ВНЕШ. 11",
                        "ВНЕШ. 12",
                        "ВНЕШ. 13",
                        "ВНЕШ. 14",
                        "ВНЕШ. 15",
                        "ВНЕШ. 16",
                        "ОМП",
                        "ЖА СПЛ"
                    };
            }
        }
        /// <summary>
        /// Сообщение
        /// </summary>
        public static List<string> Message
        {
            get
            {
                return new List<string>
                    {
                        "АВАРИЯ ERR",
                        "СИГНАЛ-ЦИЯ",
                        "РАБОТА",
                        "ОТКЛЮЧЕНИЕ",
                        "НЕУСП. АПВ",
                        "АВАРИЯ",
                        "ЛОГИКА",
                        "ОМП",
                        "СООБЩЕНИЕ"
                    };
            }
        }

        /// <summary>
        /// Параметр срабатывания
        /// </summary>
        public static List<string> Parametr
        {
           get
           {
               return new List<string>
                   {
                       "Ia", // 0
                       "Ib", // 1
                       "Ic", // 2
                       "I0", // 3
                       "I2", // 4
                       "Iг", // 5
                       "I1", // 6
                       "In", // 7
                       "In1", // 8
                       "Ia2", // 9
                       "Ib2", // 10
                       "Ic2", // 11
                       "Ua", // 12
                       "Ub", // 13
                       "Uc", // 14
                       "Uab", // 15
                       "Ubc", // 16
                       "Uca", // 17
                       "U0 ", // 18
                       "U2 ", // 19
                       "U1 ", // 20
                       "Un ", // 21
                       "Un1", // 22
                       "F", // 23
                       "", // 24
                       "", // 25
                       "", // 26
                       "Lкз", // 27
                       ""
                   };
           }
        }

    }
}
