﻿using System.Collections;
using System.Collections.Generic;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr731.Measuring.Structures
{
    /// <summary>
    /// МР 731 Дискретная база данных
    /// </summary>
    public class DiscretDataBaseStruct : StructBase
    {
        #region [Constants]
        private const int BASE_SIZE = 18 * 2;
        private const int ALARM_SIZE = 16 * 2;
        private const int PARAM_SIZE = 16 * 2;
        private const int CONTROL_SIZE = 1 * 2; 
        #endregion [Constants]


        #region [Private fields]
        [Layout(0, Count = BASE_SIZE)] private byte[] _base; //бд общаяя
        [Layout(1, Count = ALARM_SIZE)] private byte[] _alarm; //БД неисправностей
        [Layout(2, Count = PARAM_SIZE)] private byte[] _param; //БД параметров
        [Layout(3, Count = CONTROL_SIZE)] private byte[] _control; //БД управления 
        #endregion [Private fields]

        #region [Properties]
        /// <summary>
        /// Дискретные входы
        /// </summary>
        public bool[] DiscretInputs
        {
            get
            {
                return Common.GetBitsArray(new BitArray(this._base), 0, 39);
            }
        }
        /// <summary>
        /// Входные ЛС
        /// </summary>
        public bool[] InputsLogicSignals
        {
            get
            {
                return Common.GetBitsArray(new BitArray(this._base), 40, 55);
            }
        }
        /// <summary>
        /// Выходные ЛС
        /// </summary>
        public bool[] OutputLogicSignals
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 56, 71); }
        }

        /// <summary>
        /// Защиты I
        /// </summary>
        public bool[] MaximumCurrent
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 72, 103); }
        }
        /// <summary>
        /// U,F,Q
        /// </summary>
        public bool[] Voltage
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 104, 137); }
        }
        /// <summary>
        /// Внешние защиты
        /// </summary>
        public bool[] ExternalDefenses
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 143, 158); }
        }
        /// <summary>
        /// Свободная логика
        /// </summary>
        public bool[] FreeLogic
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 159, 190); }
        } 
        /// <summary>
        /// УРОВ
        /// </summary>
        public bool[] Urov
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 186, 204); }
        }
        /// <summary>
        /// Состояния
        /// </summary>
        public bool[] State
        {
            get
            {
                List<bool> res = new List<bool>();
                res.AddRange(Common.GetBitsArray(new BitArray(this._base), 191, 195));
                res.AddRange(Common.GetBitsArray(new BitArray(this._base), 259, 260));
                return res.ToArray();
            }
        }
        /// <summary>
        /// Реле
        /// </summary>
        public bool[] Relays
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 206, 239); }
        }
        /// <summary>
        /// Индикаторы
        /// </summary>
        public bool[] Indicators
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 240, 251); }
        }
        /// <summary>
        /// Контроль
        /// </summary>
        public bool[] ControlSignals
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 254, 257); }
        }
        /// <summary>
        /// Автоматика
        /// </summary>
        public bool[] Automatics
        {
            get { return Common.GetBitsArray(new BitArray(this._base), 200, 204); }
        }
        /// <summary>
        /// Неисправности
        /// </summary>
        public bool[] Faults
        {
            get
            {
                return new[]
                    {
                        Common.GetBit(this._alarm[0], 0), //аппаратная
                        Common.GetBit(this._alarm[0], 1), //программная
                        Common.GetBit(this._alarm[0], 2), //измерений
                        Common.GetBit(this._alarm[0], 3), //выключателя
                        Common.GetBit(this._alarm[0], 6), //цепей управления
                        Common.GetBit(this._alarm[0], 7), //модуль 1
                        Common.GetBit(this._alarm[1], 0), //модуль 2
                        Common.GetBit(this._alarm[1], 1), //модуль 3
                        Common.GetBit(this._alarm[1], 2), //модуль 4

                        Common.GetBit(this._alarm[1], 3), //модуль 5
                        Common.GetBit(this._alarm[1], 4), //уставок
                        Common.GetBit(this._alarm[1], 5), //группы уставок
                        Common.GetBit(this._alarm[1], 6), //пароля уставок
                        Common.GetBit(this._alarm[1], 7), //журнала системы
                        Common.GetBit(this._alarm[2], 0), //журнала аварий
                        Common.GetBit(this._alarm[2], 1), //осциллографа
                    };
            }
        }

        /// <summary>
        /// Направления токов
        /// </summary>
        public string[] CurrentsSymbols
        {
            get
            {
                bool[] booleanArray = Common.GetBitsArray(new BitArray(this._param), 0, 13);
                string[] result = new string[7];
                for (int i = 0; i < 14; i += 2)
                {
                    result[i/2] = this.GetCurrentSymbol(booleanArray[i], booleanArray[i + 1]);
                }
                return result;
            }
        }

        private string GetCurrentSymbol(bool symbol, bool error)
        {
            return error ? string.Empty : (symbol ? "-" : "+");
        }
        
        #endregion [Properties]
    }
}
