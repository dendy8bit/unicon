﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr731.Configuration.Structures.Second
{
    /// <summary>
    /// конфигурация по Ethernet
    /// </summary>
    public class ConfigEthernetStruct : StructBase
    {
        [Layout(0)] ushort ip_lo;		//сетевой адрес устройства
        [Layout(1)] ushort ip_hi;
        [Layout(2)] ushort port;	 	//порт
        [Layout(3)] ushort rez;
    }
}
