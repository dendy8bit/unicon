﻿using System.Collections.Generic;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr731.Configuration.Structures.Ls
{
    /// <summary>
    /// Конфигурациия входных логических сигналов
    /// </summary>
    [XmlRoot(ElementName = "Конфигурация_одного_ЛС")]
    public class InputLogicStruct : StructBase, IXmlSerializable
    {
        public  const int DISCRETS_COUNT =40;
        #region [Private fields]
        [Layout(0)]
        ushort _a1;
        [Layout(1)]
        ushort _a2;
        [Layout(2)]
        ushort _a3;
        [Layout(3)]
        ushort _a4;
        [Layout(4)]
        ushort _a5;
        [Layout(5)]
        ushort _a6; 
        #endregion [Private fields]
           [XmlIgnore]
        private ushort[] Mass
        {
            get
            {
                return new[]
                    {
                        this._a1,
                        this._a2,
                        this._a3,
                        this._a4,
                        this._a5,
                       // this._a6
                    };
            }

            set
            {
                this._a1 = value[0];
                this._a2 = value[1];
                this._a3 = value[2];
                this._a4 = value[3];
                this._a5 = value[4];
               // this._a6 = value[5];
            }
        }
           [XmlIgnore]
        public ushort[] LogicSygnal
        {
            get
            {
                var result = new ushort[40];

                var sourse = this.Mass;
                var enumerator = sourse.GetEnumerator();

                int j = 0;
                while (j < 40)
                {
                    enumerator.MoveNext();
                    var temp = (ushort)(enumerator.Current);

                    for (int i = 0; i < 16; i += 2)
                    {
                        result[j] = (ushort) (Common.GetBits(temp, i, i + 1) >> i);
                     
                        j++;
                    }
                }
                return result;
            }

            set
            {
                int j = 0;
                var result = new List<ushort>();
                while (j < 40)
                {
                    ushort newValue = 0;

                    for (int i = 0; i < 16; i += 2)
                    {
                        var bits =value[j] ;
                        newValue = Common.SetBits(newValue, bits, i, i + 1);
                        j++;
                    }
                    result.Add(newValue);
                }
                this.Mass = result.ToArray();
            }
        }

        [BindingProperty(0)]
        [XmlIgnore]
        public string this[int ls]
        {
            get
            {
                var sourse = this.Mass[ls/8];
                var pos = (ls*2)%16;
                return Validator.Get(sourse, StringsConfig.LsState, pos, pos + 1);
            }

            set
            {
                var sourse = this.Mass[ls/8];
                var pos = (ls*2)%16;
                sourse = Validator.Set(value, StringsConfig.LsState, sourse, pos, pos + 1);
                var mass = this.Mass;
                mass[ls/8] = sourse;
                this.Mass = mass;
            }
        }

        [XmlIgnore]
      //  [XmlArray(ElementName = "ЛС")]
           public string[] LogicSygnalXml
           {
               get
               {
                   var result = new string[40];

                   var sourse = this.Mass;
                   var enumerator = sourse.GetEnumerator();

                   int j = 0;
                   while (j < 40)
                   {
                       enumerator.MoveNext();
                       var temp = (ushort)(enumerator.Current);

                       for (int i = 0; i < 16; i += 2)
                       {
                           var number = Common.GetBits(temp, i, i + 1) >> i;
                           result[j] = StringsConfig.LsState[number];
                           j++;
                       }
                   }
                   return result;
               }

               set
               {
                   int j = 0;
                   var result = new List<ushort>();
                   while (j < 40)
                   {
                       ushort newValue = 0;

                       for (int i = 0; i < 16; i += 2)
                       {
                           var bits = (ushort)StringsConfig.LsState.IndexOf(value[j]);
                           newValue = Common.SetBits(newValue, bits, i, i + 1);
                           j++;
                       }
                       result.Add(newValue);
                   }
                   this.Mass = result.ToArray();
               }
           }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
       
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
           
            for (int i = 0; i < DISCRETS_COUNT; i++)
            {
                writer.WriteElementString("Дискрет", this[i]);
            }
        }
    }
}
