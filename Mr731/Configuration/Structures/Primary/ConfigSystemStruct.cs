﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Mr731.Configuration.Structures.Second;

namespace BEMN.Mr731.Configuration.Structures.Primary
{
    /// <summary>
    /// конфигурациия системы
    /// </summary>
     public class ConfigSystemStruct :StructBase
    {
        [Layout(0)]
        ConfigNetStruct confnet;
        [Layout(1)]
        ConfigEthernetStruct confethernet;
      
        [Layout(2,Count = 122)]
        ushort[] rez;

  
    }
}
