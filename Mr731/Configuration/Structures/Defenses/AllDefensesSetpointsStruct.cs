﻿using System;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New.GroupOfSetpoints;

namespace BEMN.Mr731.Configuration.Structures.Defenses
{
    /// <summary>
    /// защиты по двум группам уставок
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "Все_защиты")]
    public class AllDefensesSetpointsStruct : StructBase, ISetpointContainer<DefensesSetpointsStruct>
    {
        #region [Public fields]
        /// <summary>
        /// Основная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Основная_группа_уставок")]
        [Layout(0)]
        public DefensesSetpointsStruct MainSetpoints;
        /// <summary>
        /// Резервная группа уставок
        /// </summary>
        [XmlElement(ElementName = "Резервная_группа_уставок")]
        [Layout(1)]
        public DefensesSetpointsStruct ReserveSetpoints;
        #endregion [Public fields]

        [XmlIgnore]
        public DefensesSetpointsStruct[] Setpoints
        {
            get
            {
                return new[]
                    {
                        MainSetpoints.Clone<DefensesSetpointsStruct>(),
                        ReserveSetpoints.Clone<DefensesSetpointsStruct>()
                    };
            }
            set
            {
                MainSetpoints = value[0].Clone<DefensesSetpointsStruct>();
                ReserveSetpoints = value[1].Clone<DefensesSetpointsStruct>();
            }
        }
    }
}
