﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr731.Configuration.Structures.Defenses.U
{
    public class AllDefenceUStruct : StructBase, IDgvRowsContainer<DefenceUStruct>
    {
        [XmlArray(ElementName = "U")]
        [Layout(0, Count =8)]
        private DefenceUStruct[] _u; //мтз U>

        [XmlArray(ElementName = "Все")]
        public DefenceUStruct[] Rows
        {
            get { return _u; }
            set { _u = value; }
        }
    }
}
