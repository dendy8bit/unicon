﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.MBServer;

namespace BEMN.Mr731.Configuration.Structures.Defenses.Ig
{
    /// <summary>
    /// Ig
    /// </summary>
    public class DefenseIgStruct: StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _config; //конфигурация выведено/введено (УРОВ - выведено/введено)...
        [Layout(1)]
        private ushort _config1; //конфигурация дополнительная (АПВ - выведено/введено, АВР - выведено/введено)
        [Layout(2)]
        private ushort _block; //вход блокировки
        [Layout(3)]
        private ushort _ust; //уставка срабатывания_
        [Layout(4)]
        private ushort _time; //время срабатывания_
        [Layout(5)]
        private ushort _k; //коэфиц. зависимой хар-ки
        [Layout(6)]
        private ushort _u; //уставка пуска по напряжению
        [Layout(7)]
        private ushort _tu; //время ускорения_
        [Layout(8)]
        private ushort _i21; // уставка в %
        [Layout(9)]
        private ushort _rez; //резерв 
        #endregion [Private fields]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.DefenseModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.DefenseModes, this._config, 0, 1); }
        }

        [BindingProperty(1)]
        [XmlElement(ElementName = "Блокировка")]
        public string BlockXml
        {
            get { return StringsConfig.SwitchSignals[this._block]; }
            set { this._block = (ushort)StringsConfig.SwitchSignals.IndexOf(value); }
        }

        /// <summary>
        /// U пуск
        /// </summary>
        [BindingProperty(2)]
        [XmlElement(ElementName = "U_пуск")]
        public double UStart
        {
            get { return ValuesConverterCommon.GetUstavka256(this._u); }
            set { this._u = ValuesConverterCommon.SetUstavka256(value); }
        }

        /// <summary>
        /// Уставка
        /// </summary>
        [BindingProperty(3)]
        [XmlElement(ElementName = "Уставка")]
        public double Ustavka
        {
            get { return ValuesConverterCommon.GetIn(this._ust); }
            set { this._ust = ValuesConverterCommon.SetIn(value); }
        }

        /// <summary>
        /// tср, время срабатывания
        /// </summary>
        [BindingProperty(4)]
        [XmlElement(ElementName = "tср")]
        public int TimeSrab
        {
            get { return ValuesConverterCommon.GetWaitTime(this._time); }
            set { this._time = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// ty
        /// </summary>
        [BindingProperty(5)]
        [XmlElement(ElementName = "ty")]
        public int TimeY
        {
            get { return ValuesConverterCommon.GetWaitTime(this._tu); }
            set { this._tu = ValuesConverterCommon.SetWaitTime(value); }
        }

        /// <summary>
        /// Uпуск (есть/нет)
        /// </summary>
        [XmlElement(ElementName = "Uпуск")]
        [BindingProperty(6)]
        public bool UStartBool
        {
            get { return Common.GetBit(this._config, 3); }
            set { this._config = Common.SetBit(this._config, 3, value); }
        }

        /// <summary>
        /// Ty (есть/нет)
        /// </summary>
        [BindingProperty(7)]
        [XmlElement(ElementName = "Ty")]
        public bool TyBool
        {
            get { return Common.GetBit(this._config, 5); }
            set { this._config = Common.SetBit(this._config, 5, value); }
        }

        /// <summary>
        /// Осц
        /// </summary>
        [BindingProperty(8)]
        [XmlElement(ElementName = "Осц")]
        public string OscXml
        {
            get { return Validator.Get(this._config1, StringsConfig.OscModes, 4, 5); }
            set { this._config1 = Validator.Set(value, StringsConfig.OscModes, this._config1, 4, 5); }
        }

        /// <summary>
        /// Уров
        /// </summary>
        [BindingProperty(9)]
        [XmlElement(ElementName = "Уров")]
        public bool Urov
        {
            get { return Common.GetBit(this._config, 2); }
            set { this._config = Common.SetBit(this._config, 2, value); }
        }


        /// <summary>
        /// АПВ
        /// </summary>
        [BindingProperty(10)]
        [XmlElement(ElementName = "АПВ")]
        public bool Apv
        {
            get { return Common.GetBit(this._config1, 0); }
            set { this._config1 = Common.SetBit(this._config1, 0, value); }
        }



        /// <summary>
        /// АВР
        /// </summary>
        [BindingProperty(11)]
        [XmlElement(ElementName = "АВР")]
        public bool Avr
        {
            get { return Common.GetBit(this._config1, 1); }
            set { this._config1 = Common.SetBit(this._config1, 1, value); }
        }
    }
}
