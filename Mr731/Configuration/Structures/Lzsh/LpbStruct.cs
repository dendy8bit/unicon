﻿using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.MeasuringClasses;
using BEMN.Forms.ValidatingClasses;

namespace BEMN.Mr731.Configuration.Structures.Lzsh
{
    /// <summary>
    /// конфигурациия ЛЗШ
    /// </summary>
    public class LpbStruct :StructBase
    {
        #region [Private fields]
        [Layout(0)]
        private ushort _config; //конфигурация ЛЗШ
        [Layout(1)]
        private ushort _val; //уставка ЛЗШ 
        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// Режим
        /// </summary>
        [BindingProperty(0)]
        [XmlElement(ElementName = "Режим")]
        public string Mode
        {
            get { return Validator.Get(this._config, StringsConfig.LzhModes, 0, 1); }
            set { this._config = Validator.Set(value, StringsConfig.LzhModes, this._config, 0, 1); }
        }

        /// <summary>
        /// уставка ЛЗШ
        /// </summary>
        [BindingProperty(1)]
        [XmlElement(ElementName = "уставка_ЛЗШ")]
        public double LzhSetpoint
        {
            get { return ValuesConverterCommon.GetIn(this._val); }
            set { this._val = ValuesConverterCommon.SetIn(value); }
        } 
        #endregion [Properties]


    }
}
