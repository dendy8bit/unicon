﻿using System.Linq;
using System.Xml.Serialization;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.Forms.ValidatingClasses.New;

namespace BEMN.Mr731.Configuration.Structures.Oscope
{
    public class OscopeAllChannelsStruct : StructBase, IDgvRowsContainer<ChannelStruct>
    {
        #region [Constants]

        public const int KANAL_COUNT = 24;
        private const int REZ_COUNT = 5;

        #endregion [Constants]

        [Layout(0, Count = KANAL_COUNT)]
        private ChannelStruct[] _kanal; //конфигурация канала осциллографирования
        [Layout(1, Count = REZ_COUNT)]
        private ushort[] rez;

        
        [XmlIgnore]
        public ushort[] ChannelsInWords
        {
            get { return this._kanal.Select(o => (ushort)StringsConfig.RelaySignals.IndexOf(o.Channel)).ToArray(); }
        }

        /// <summary>
        /// Каналы
        /// </summary>
        [XmlArray(ElementName = "Все_каналы")]
        public ChannelStruct[] Rows
        {
            get { return this._kanal; }
            set { this._kanal = value; }
        }
    }
}
