﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;

namespace BEMN.Mr731.SystemJournal.Structures
{
  
    public class SystemJournalStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
        private const string MESSAGE_WITH_CODE_PATTERN = "{0} ({1})";
        private const string MESSAGE_SPL_PATTERN = "СООБЩЕНИЕ ЖС СПЛ N{0}";

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _moduleErrorCode;
        [Layout(8)] private ushort _message;

        #endregion [Private fields]


        #region [Properties]

        /// <summary>
        /// true если во всех полях 0, условие конца ЖС
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                          this.Month +
                          this.Date +
                          this.Hour +
                          this.Minute +
                          this.Second +
                          this.Millisecond +
                          this.ModuleErrorCode +
                          this.Message;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetRecordTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );

            }
        }

        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string GetRecordMessage
        {
            get
            {
                if ((this.Message == 7) | (this.Message == 9) | (this.Message == 11) | (this.Message == 13) |
                    (this.Message == 15))
                    return string.Format(MESSAGE_WITH_CODE_PATTERN, StringsSj.Message[this.Message],
                                         this.ModuleErrorCode);
                if (this.Message < StringsSj.Message.Count)
                    return StringsSj.Message[this.Message];
                if (this.Message >= 500)
                    return string.Format(MESSAGE_SPL_PATTERN, this.Message - 499);
                return string.Empty;
            }
        }

        public ushort Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public ushort Month
        {
            get { return _month; }
            set { _month = value; }
        }

        public ushort Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public ushort Hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public ushort Minute
        {
            get { return _minute; }
            set { _minute = value; }
        }

        public ushort Second
        {
            get { return _second; }
            set { _second = value; }
        }

        public ushort Millisecond
        {
            get { return _millisecond; }
            set { _millisecond = value; }
        }

        public ushort ModuleErrorCode
        {
            get { return _moduleErrorCode; }
            set { _moduleErrorCode = value; }
        }

        public ushort Message
        {
            get { return _message; }
            set { _message = value; }
        }

        #endregion [Properties]



    }
}
