﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml.Serialization;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.Mr731.AlarmJournal;
using BEMN.Mr731.AlarmJournal.Structures;
using BEMN.Mr731.Configuration.Structures;
using BEMN.Mr731.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr731.Configuration.Structures.Oscope;
using BEMN.Mr731.Measuring.Structures;
using BEMN.Mr731.Osc.Loaders;
using BEMN.Mr731.Osc.Structures;
using BEMN.Mr731.SystemJournal.Structures;


namespace BEMN.Mr731
{
    public class Mr731Device : Device, IDeviceView
    {
        #region [Constants]

        private const int SYSTEM_JOURNAL_START_ADRESS = 0x600;
        private const int DISCRET_DATABASE_START_ADRESS = 0xD00;
        private const int ANALOG_DATABASE_START_ADRESS = 0x0E00;
        private const int OSC_JOURNAL_START_ADRESS = 0x800;

        #endregion [Constants]

        #region [Ctor's]

        public Mr731Device()
        {
            Init();
        }

        public Mr731Device(Modbus mb)
        {
            MB = mb;
            Init();
        }
        private void Init()
        {
            HaveVersion = true;
        }

        public override Modbus MB
        {
            get {return mb; }
            set
            {
                base.MB = value;

                this._configuration = new MemoryEntity<ConfigurationStruct>("Конфигурация", this, 0x1000);
                this._systemJournal = new MemoryEntity<SystemJournalStruct>("Журнал системы", this, SYSTEM_JOURNAL_START_ADRESS);
                this._refreshSystemJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала системы", this, SYSTEM_JOURNAL_START_ADRESS);
                this._dateTime = new MemoryEntity<DateTimeStruct>("DateAndTime", this, 0x200);
                this._oscOptions = new MemoryEntity<OscOptionsStruct>("Параметры осциллографа", this, 0x5A0);
                this._discretDataBase = new MemoryEntity<DiscretDataBaseStruct>("Дискретная БД", this, DISCRET_DATABASE_START_ADRESS);
                this._analogDataBase = new MemoryEntity<AnalogDataBaseStruct>("Аналоговая БД", this, ANALOG_DATABASE_START_ADRESS);
                this._measureTrans = new MemoryEntity<MeasureTransStruct>("Параметры измерений ЖА", this, 0x104E);
                this._measureTransAj = new MemoryEntity<MeasureTransStruct>("Параметры измерений", this, 0x104E);
                this._refreshAlarmJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала аварий", this, 0x700);
                this._alarmJournal = new MemoryEntity<AlarmJournalRecordStruct>("Журнал аварий", this, 0x700);
                this._currentOptionsLoader = new CurrentOptionsLoader(this._measureTransAj);

                this._refreshOscJournal = new MemoryEntity<JournalRefreshStruct>("Обновление журнала осциллографа", this, OSC_JOURNAL_START_ADRESS);
                this._oscJournal = new MemoryEntity<OscJournalStruct>("Журнал осциллографа", this, OSC_JOURNAL_START_ADRESS);
                this._oscPage = new MemoryEntity<OscPage>("Страница осциллографа", this, 0x900);
                this._setOscStartPage = new MemoryEntity<SetOscStartPageStruct>("Установка стартовой страницы осциллограммы", this, 0x900);

                this._oscopeStruct = new MemoryEntity<OscopeStruct>("Уставки осциллографа", this, 0x102E /*3A*/);

                this._oscOptionsLoader = new OscOptionsLoader(this._oscopeStruct);

                this._sourceProgramStruct = new MemoryEntity<SourceProgramStruct>("SaveProgram", this, 0x4300);
                this._programStartStruct = new MemoryEntity<StartStruct>("SaveProgramStart", this, 0x0E00);
                this._programSignalsStruct = new MemoryEntity<ProgramSignalsStruct>("LoadProgramSignals_", this, 0x4100);
                this._programPageStruct = new MemoryEntity<ProgramPageStruct>("SaveProgrammPage", this, 0x4000);
            }
        }

        #endregion [Ctor's]
        
        #region [Private fields]

        private MemoryEntity<OscJournalStruct> _oscJournal;
        private MemoryEntity<JournalRefreshStruct> _refreshOscJournal;
        private MemoryEntity<SetOscStartPageStruct> _setOscStartPage;
        private MemoryEntity<OscPage> _oscPage;

        private MemoryEntity<ConfigurationStruct> _configuration;
        private MemoryEntity<JournalRefreshStruct> _refreshSystemJournal;
        private MemoryEntity<SystemJournalStruct> _systemJournal;
        private MemoryEntity<DateTimeStruct> _dateTime;
        private MemoryEntity<OscOptionsStruct> _oscOptions;
        private MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private MemoryEntity<AnalogDataBaseStruct> _analogDataBase;
        private MemoryEntity<MeasureTransStruct> _measureTrans;
        private MemoryEntity<MeasureTransStruct> _measureTransAj;
        private MemoryEntity<JournalRefreshStruct> _refreshAlarmJournal;
        private MemoryEntity<AlarmJournalRecordStruct> _alarmJournal;
        private MemoryEntity<OscopeStruct> _oscopeStruct;
        private CurrentOptionsLoader _currentOptionsLoader;
        private OscOptionsLoader _oscOptionsLoader;

        #endregion [Private fields]

        #region Programming

        private MemoryEntity<ProgramPageStruct> _programPageStruct;
        private MemoryEntity<SourceProgramStruct> _sourceProgramStruct;
        private MemoryEntity<StartStruct> _programStartStruct;
        private MemoryEntity<ProgramSignalsStruct> _programSignalsStruct;
        public MemoryEntity<ProgramPageStruct> ProgramPage
        {
            get { return _programPageStruct; }
        }
        public MemoryEntity<StartStruct> ProgramStartStruct
        {
            get { return _programStartStruct; }
        }
        public MemoryEntity<ProgramSignalsStruct> ProgramSignalsStruct
        {
            get { return _programSignalsStruct; }
        }

        public MemoryEntity<SourceProgramStruct> SourceProgramStruct
        {
            get { return _sourceProgramStruct; }
        }
        #endregion

        #region [Properties]

        public MemoryEntity<MeasureTransStruct> MeasureTrans
        {
            get { return _measureTrans; }
        }

        public OscOptionsLoader OscopeOptionsLoader
        {
            get { return this._oscOptionsLoader; }
        }


        public MemoryEntity<OscPage> OscPage
        {
            get { return _oscPage; }
        }

        public MemoryEntity<SetOscStartPageStruct> SetOscStartPage
        {
            get { return _setOscStartPage; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshOscJournal
        {
            get { return _refreshOscJournal; }
        }

        public MemoryEntity<OscJournalStruct> OscJournal
        {
            get { return _oscJournal; }
        }

        public CurrentOptionsLoader CurrentOptionsLoader
        {
            get { return _currentOptionsLoader; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshAlarmJournal
        {
            get { return _refreshAlarmJournal; }
        }

        public MemoryEntity<AlarmJournalRecordStruct> AlarmJournal
        {
            get { return _alarmJournal; }
        }

        public MemoryEntity<AnalogDataBaseStruct> AnalogDataBase
        {
            get { return _analogDataBase; }
        }

        public MemoryEntity<DateTimeStruct> DateAndTime
        {
            get { return _dateTime; }
        }

        public MemoryEntity<ConfigurationStruct> Configuration
        {
            get { return _configuration; }
        }

        public MemoryEntity<JournalRefreshStruct> RefreshSystemJournal
        {
            get { return _refreshSystemJournal; }
        }

        public MemoryEntity<SystemJournalStruct> SystemJournal
        {
            get { return _systemJournal; }
        }

        public MemoryEntity<OscOptionsStruct> OscOptions
        {
            get { return _oscOptions; }
        }

        public MemoryEntity<DiscretDataBaseStruct> DiscretDataBase
        {
            get { return _discretDataBase; }
        }

        #endregion [Properties]
        
        #region [Interface Members]

        [XmlIgnore]
        [Browsable(false)]
        public Type ClassType
        {
            get { return typeof (Mr731Device); }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool ForceShow
        {
            get { return false; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public Image NodeImage
        {
            get { return Framework.Properties.Resources.mr741; }
        }

        [Browsable(false)]
        public string NodeName
        {
            get { return "МР731"; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        [XmlIgnore]
        [Browsable(false)]
        public bool Deletable
        {
            get { return true; }
        }
        
        #endregion [Interface Members]

        #region события

        public event Handler ProgramSaveOk;
        public event Handler ProgramSaveFail;
        public event Handler ProgramStartSaveOk;
        public event Handler ProgramStartSaveFail;
        public event Handler ProgramStorageSaveOk;
        public event Handler ProgramStorageSaveFail;
        public event Handler ProgramStorageLoadOk;
        public event Handler ProgramStorageLoadFail;
        public event Handler ProgramSignalsLoadOk;
        public event Handler ProgramSignalsLoadFail;
        public event Handler ProgramPageSaveOk;
        public event Handler ProgramPageSaveFail;

        #endregion
    }
}
