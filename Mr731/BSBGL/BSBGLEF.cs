﻿using BEMN.MBServer;
using BMTCD.BMTCDMr731;
using BMTCD.HelperClasses;
using SchemeEditorSystem;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using AssemblyResources;
using BEMN.Compressor;
using Crownwood.Magic.Common;
using Crownwood.Magic.Docking;
using Crownwood.Magic.Menus;
using BEMN.Devices;
using BEMN.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures.FreeLogicStructures;
using BEMN.Interfaces;

namespace BEMN.Mr731.BSBGL
{
    public partial class BSBGLEF : Form, IFormView
    {
        #region Variables
        private const int COUNT_EXCHANGES_PROGRAM = 64; //Для аналоговых сигналов защит
        private MemoryEntity<StartStruct> _currentStartProgramStruct;
        private MemoryEntity<SourceProgramStruct> _currentSourceProgramStruct;
        private MemoryEntity<ProgramSignalsStruct> _currentSignalsStruct;
        private MemoryEntity<ProgramPageStruct> _currentProgramPage;
        private MessageBoxForm _formCheck;
        private Mr731Device _device;
        private NewSchematicForm _newForm;
        private DockingManager _manager;
        private MR731CD _compiller;
        private bool _isOnSimulateMode;

        protected OutputWindow _outputWindow;
        protected LibraryBox _libraryWindow;
        protected ToolStrip _toolBarWindow;
        protected ToolStrip _mainToolStrip;

        protected Content _outputContent;
        protected Content _libraryContent;

        ushort[] _binFile;
        ushort _pageIndex;
        protected byte[] _slot1;
        protected byte[] _slot2;
        protected bool _captionBars = true;
        protected bool _closeButtons = true;
        protected VisualStyle _style;
        protected MenuCommand _placeHolder;

        protected ImageList _internalImages;
        protected StatusBar _statusBar;
        protected MenuControl _topMenu;
        protected Crownwood.Magic.Controls.TabControl _filler;

        protected Crownwood.Magic.Controls.TabControl.VisualAppearance _tabAppearance =
            Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiForm;

        private ToolStrip MainToolStrip;
        private ToolStripButton newToolStripButton;
        private ToolStripButton openToolStripButton;
        private ToolStripButton openProjToolStripButton;
        private ToolStripButton saveToolStripButton;
        private ToolStripButton saveProjToolStripButton;
        private ToolStripButton printToolStripButton;

        private ToolStripButton undoToolStripButton;
        private ToolStripButton redoToolStripButton;

        private ToolStripButton cutToolStripButton;
        private ToolStripButton copyToolStripButton;
        private ToolStripButton pasteToolStripButton;

        private ToolStripButton CompilToolStripButton;
        private ToolStripButton StopToolStripButton;

        #endregion
        
        #region Constructor
        public BSBGLEF()
        {
            this.InitForm();
        }
        public BSBGLEF(Mr731Device device)
        {
            this._device = device;
            this.InitForm();
            this._currentSourceProgramStruct = this._device.SourceProgramStruct;
            this._currentStartProgramStruct = this._device.ProgramStartStruct;
            this._currentSignalsStruct = this._device.ProgramSignalsStruct;
            this._currentProgramPage = this._device.ProgramPage;
            //Страница программа
            this._device.ProgramPage.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveOk);
            this._device.ProgramPage.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramPageSaveFail);
            //Программа
            this._device.SourceProgramStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteOk);
            this._device.SourceProgramStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStructWriteFail);
            this._device.SourceProgramStruct.WriteOk += HandlerHelper.CreateHandler(this, this.ExchangeOk);
            this._device.SourceProgramStruct.WriteFail += HandlerHelper.CreateHandler(this, () =>
            {
                this._device.SourceProgramStruct.RemoveStructQueries();
                this.ProgramStructWriteFail();
            });
            //Старт программы
            this._device.ProgramStartStruct.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveOk);
            this._device.ProgramStartStruct.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramStartSaveFail);
            //Загрузка сигналов
            this._device.ProgramSignalsStruct.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.SignalsLoadOk);
            this._device.ProgramSignalsStruct.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.ProgramSignalsLoadFail);
        }
        
        void InitForm()
        {
            this.InitializeComponent();
            this._style = VisualStyle.IDE;
            this._manager = new DockingManager(this, this._style);
            this._newForm = new NewSchematicForm();
            this._compiller = new MR731CD();
            this._formCheck = new MessageBoxForm();
        }
        #endregion

        #region DeviceEventHandlers
        private void ProgramPageSaveFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (Exception){}
        }
        private void ProgramPageSaveOk()
        {
            try
            {
                if (this._pageIndex < 4)
                {
                    SourceProgramStruct ps = new SourceProgramStruct();
                    ushort[] _program = new ushort[1024];
                    Array.ConstrainedCopy(this._binFile, this._pageIndex * 1024, _program, 0, 1024);
                    ps.InitStruct(Common.TOBYTES(_program, false));
                    this._currentSourceProgramStruct.Value = ps;
                    this._currentSourceProgramStruct.SaveStruct();
                }
            }
            catch
            { }
        }
        private void ExchangeOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(() => this._formCheck.ProgramExchangeOk()));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramStructWriteFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSaveFail));
            }
            catch (InvalidOperationException)
            { }
        }

        private void ProgramSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_LOADING_PROGRAM_IN_DEVICE);
            this.OnStop();
        }
        private void ProgramStructWriteOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.WriteOk));
            }
            catch (InvalidOperationException)
            { }
        }

        private void WriteOk()
        {
            this._pageIndex++;
            if (this._pageIndex < 4)
            {
                ushort[] values = new ushort[1];
                values[0] = this._pageIndex;
                ProgramPageStruct pp = new ProgramPageStruct();
                pp.InitStruct(Common.TOBYTES(values, false));
                this._currentProgramPage.Value = pp;
                this._currentProgramPage.SaveStruct();
            }
            else
            {
                this._formCheck.ShowMessage(InformationMessages.PROGRAM_SAVE_OK);
                var values = new ushort[1];
                values[0] = 0x00FF;
                StartStruct ss = new StartStruct();
                ss.InitStruct(Common.TOBYTES(values, false));
                this._currentStartProgramStruct.Value = ss;
                this._currentStartProgramStruct.SaveStruct5();
            }
        }

        private void SignalsLoadOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.ProgramSignalsLoadOk));
            }
            catch (InvalidOperationException)
            { }
        }
        private void ProgramSignalsLoadOk()
        {
            if (this._isOnSimulateMode)
            {
                this._compiller.UpdateVol(this._currentSignalsStruct.Values);
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    src.Schematic.RedrawSchematic();
                    src.Schematic.RedrawBack();
                }
                this.OutMessage(InformationMessages.VARIABLES_UPDATED);
            }

        }

        private void ProgramStartSaveFail()
        {
            this.OutMessage(InformationMessages.ERROR_PROGRAM_START);
            this._formCheck.Fail = true;
            this._formCheck.ShowResultMessage(InformationMessages.ERROR_PROGRAM_START);
            this.OnStop();
        }
        private void ProgramSignalsLoadFail()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.SignalsLoadFail));
            }
            catch (InvalidOperationException)
            { }
        }
        void SignalsLoadFail()
        {
            //Останавливаем отрисовку
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
            if (this._isOnSimulateMode)
            {
                this.OutMessage(InformationMessages.ERROR_VARIABLES_UPDATED);
            }
        }
        private void ProgramStartSaveOk()
        {
            try
            {
                Invoke(new OnDeviceEventHandler(this.StartSaveOk));
            }
            catch (InvalidOperationException) { }
        }

        private void StartSaveOk()
        {
            this.OutMessage(InformationMessages.PROGRAM_START_OK);
            this._formCheck.ShowResultMessage(InformationMessages.PROGRAM_SAVE_OK_EMULATOR_RUNNING);
            ProgramSignalsStruct ps = new ProgramSignalsStruct(this._compiller.GetRamRequired());
            ushort[] values = new ushort[this._compiller.GetRamRequired()];
            this._currentSignalsStruct.Value = ps;
            this._currentSignalsStruct.Values = values;
            this._currentSignalsStruct.Slots = HelperFunctions.SetSlots(values, 0xA000);
            this._currentSignalsStruct.LoadStruct();
        }

        #endregion

        private void BSBGLEF_Load(object sender, EventArgs e)
        {
            this._filler = new Crownwood.Magic.Controls.TabControl
            {
                Appearance = Crownwood.Magic.Controls.TabControl.VisualAppearance.MultiDocument,
                Dock = DockStyle.Fill,
                Style = this._style,
                IDEPixelBorder = true
            };
            Controls.Add(this._filler);
            this._filler.ClosePressed += new EventHandler(this.OnFileClose);
            // Reduce the amount of flicker that occurs when windows are redocked within
            // the container. As this prevents unsightly backcolors being drawn in the
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            // Create the object that manages the docking state
            this._manager = new DockingManager(this, this._style);
            // Ensure that the RichTextBox is always the innermost control
            this._manager.InnerControl = this._filler;
            // Create and setup the StatusBar object
            this._statusBar = new StatusBar();
            this._statusBar.Dock = DockStyle.Bottom;
            this._statusBar.ShowPanels = true;
            // Create and setup a single panel for the StatusBar
            StatusBarPanel statusBarPanel = new StatusBarPanel();
            statusBarPanel.AutoSize = StatusBarPanelAutoSize.Spring;
            this._statusBar.Panels.Add(statusBarPanel);
            Controls.Add(this._statusBar);
            this._mainToolStrip = this.CreateToolStrip();
            Controls.Add(this._mainToolStrip);
            this._topMenu = this.CreateMenus();
            // Ensure that docking occurs after the menu control and status bar controls
            this._manager.OuterControl = this._statusBar;
            // Create Content which contains a RichTextBox
            this.CreateOutputWindow();
            this.CreateLibraryWindow();
            Width = 800;
            Height = 600;
        }

        private void BSBGLEF_FormClosed(object sender, FormClosedEventArgs e)
        {
            this._currentSignalsStruct.RemoveStructQueries();
            switch (MessageBox.Show("Сохранить текущий проект на диске ?", "Закрытие проекта", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    if (this.SaveProjectDoc())
                    {
                        this._filler.TabPages.Clear();
                    }
                    break;
            }
        }

        private void OutMessage(String str)
        {
            this._outputWindow.AddMessage(str + "\r\n");
        }

        protected void DefineContentState(Content c)
        {
            c.CaptionBar = this._captionBars;
            c.CloseButton = this._closeButtons;
        }

        #region Docking FOrms Code
        
        private void CreateOutputWindow()
        {
            this._outputWindow = new OutputWindow();
            this._outputContent = this._manager.Contents.Add(this._outputWindow, "Сообщения");
            this.DefineContentState(this._outputContent);
            this._manager.AddContentWithState(this._outputContent, State.DockBottom);
        }
        
        private void CreateLibraryWindow()
        {
            this._libraryWindow = new LibraryBox(Resources.BlockLib);
            this._libraryContent = this._manager.Contents.Add(this._libraryWindow, "Библиотека");
            this.DefineContentState(this._libraryContent);
            this._manager.AddContentWithState(this._libraryContent, State.DockRight);
        }

        #endregion

        #region Toolbox
        protected ToolStrip CreateToolStrip()
        {
            List<ToolStripSeparator> toolStripSepList = new List<ToolStripSeparator>();
            for (int i = 0; i < 6; i++)
            {
                ToolStripSeparator tls = new ToolStripSeparator();
                toolStripSepList.Add(tls);
                tls.Name = "toolStripSeparator" + i;
                tls.Size = new Size(6, 25);
            }

            this.MainToolStrip = new ToolStrip();
            this.newToolStripButton = new ToolStripButton();
            this.openToolStripButton = new ToolStripButton();
            this.openProjToolStripButton = new ToolStripButton();
            this.saveToolStripButton = new ToolStripButton();
            this.saveProjToolStripButton = new ToolStripButton();
            this.printToolStripButton = new ToolStripButton();

            this.undoToolStripButton = new ToolStripButton();
            this.redoToolStripButton = new ToolStripButton();
            this.cutToolStripButton = new ToolStripButton();
            this.copyToolStripButton = new ToolStripButton();
            this.pasteToolStripButton = new ToolStripButton();

            this.CompilToolStripButton = new ToolStripButton();
            this.StopToolStripButton = new ToolStripButton();
            
            this.MainToolStrip.Items.Add(this.newToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[0]);
            this.MainToolStrip.Items.Add(this.openToolStripButton);
            this.MainToolStrip.Items.Add(this.openProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[1]);
            this.MainToolStrip.Items.Add(this.saveToolStripButton);
            this.MainToolStrip.Items.Add(this.saveProjToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[2]);
            this.MainToolStrip.Items.Add(this.printToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[3]);
            this.MainToolStrip.Items.Add(this.undoToolStripButton);
            this.MainToolStrip.Items.Add(this.redoToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[4]);
            this.MainToolStrip.Items.Add(this.cutToolStripButton);
            this.MainToolStrip.Items.Add(this.copyToolStripButton);
            this.MainToolStrip.Items.Add(this.pasteToolStripButton);
            this.MainToolStrip.Items.Add(toolStripSepList[5]);
            this.MainToolStrip.Items.Add(this.CompilToolStripButton);
            this.MainToolStrip.Items.Add(new ToolStripSeparator { Margin = new Padding(20, 0, 0, 0) });
            this.MainToolStrip.Items.Add(this.StopToolStripButton);

            this.MainToolStrip.Location = new Point(0, 0);
            this.MainToolStrip.Name = "MainToolStrip";
            this.MainToolStrip.Size = new Size(816, 25);
            this.MainToolStrip.TabIndex = 0;
            this.MainToolStrip.Text = "toolStrip1";

            this.newToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.newToolStripButton.Image = Resources.filenew;
            this.newToolStripButton.ImageTransparentColor = Color.Magenta;
            this.newToolStripButton.Name = "newToolStripButton";
            this.newToolStripButton.Size = new Size(23, 22);
            this.newToolStripButton.Text = "Новая схема";
            this.newToolStripButton.Click += new EventHandler(this.OnFileNew);

            this.openToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = Resources.fileopen;
            this.openToolStripButton.ImageTransparentColor = Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new Size(23, 22);
            this.openToolStripButton.Text = "Открыть документ";
            this.openToolStripButton.Click += new EventHandler(this.OnFileOpen);

            this.openProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.openProjToolStripButton.Image = Resources.fileopenproject;
            this.openProjToolStripButton.ImageTransparentColor = Color.Magenta;
            this.openProjToolStripButton.Name = "openProjToolStripButton";
            this.openProjToolStripButton.Size = new Size(23, 22);
            this.openProjToolStripButton.Text = "Открыть проект";
            this.openProjToolStripButton.Click += new EventHandler(this.OnFileOpenProject);

            this.saveToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Image = Resources.filesave;
            this.saveToolStripButton.ImageTransparentColor = Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new Size(23, 22);
            this.saveToolStripButton.Text = "Сохранить документ";
            this.saveToolStripButton.Click += new EventHandler(this.OnFileSave);

            this.saveProjToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.saveProjToolStripButton.Image = Resources.filesaveproject;
            this.saveProjToolStripButton.ImageTransparentColor = Color.Magenta;
            this.saveProjToolStripButton.Name = "saveProjToolStripButton";
            this.saveProjToolStripButton.Size = new Size(23, 22);
            this.saveProjToolStripButton.Text = "Сохранить проект";
            this.saveProjToolStripButton.Click += new EventHandler(this.OnFileSaveProject);

            this.printToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.printToolStripButton.Image = Resources.print;
            this.printToolStripButton.ImageTransparentColor = Color.Magenta;
            this.printToolStripButton.Name = "printToolStripButton";
            this.printToolStripButton.Size = new Size(23, 22);
            this.printToolStripButton.Text = "Печать";
            this.printToolStripButton.Click += new EventHandler(this.printToolStripButton_Click);
            
            this.undoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.undoToolStripButton.Image = Resources.undo;
            this.undoToolStripButton.ImageTransparentColor = Color.Magenta;
            this.undoToolStripButton.Name = "undoToolStripButton";
            this.undoToolStripButton.Size = new Size(23, 22);
            this.undoToolStripButton.Text = "Отмена";
            this.undoToolStripButton.Click += new EventHandler(this.OnUndo);

            this.redoToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.redoToolStripButton.Image = Resources.redo;
            this.redoToolStripButton.ImageTransparentColor = Color.Magenta;
            this.redoToolStripButton.Name = "redoToolStripButton";
            this.redoToolStripButton.Size = new Size(23, 22);
            this.redoToolStripButton.Text = "Восстановить отмененое";
            this.redoToolStripButton.Click += new EventHandler(this.OnRedo);

            this.cutToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.cutToolStripButton.Image = Resources.cut;
            this.cutToolStripButton.ImageTransparentColor = Color.Magenta;
            this.cutToolStripButton.Name = "cutToolStripButton";
            this.cutToolStripButton.Size = new Size(23, 22);
            this.cutToolStripButton.Text = "Вырезать";
            this.cutToolStripButton.Click += new EventHandler(this.OnEditCut);

            this.copyToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.copyToolStripButton.Image = Resources.copy;
            this.copyToolStripButton.ImageTransparentColor = Color.Magenta;
            this.copyToolStripButton.Name = "copyToolStripButton";
            this.copyToolStripButton.Size = new Size(23, 22);
            this.copyToolStripButton.Text = "Копировать";
            this.copyToolStripButton.Click += new EventHandler(this.OnEditCopy);

            this.pasteToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.pasteToolStripButton.Image = Resources.paste;
            this.pasteToolStripButton.ImageTransparentColor = Color.Magenta;
            this.pasteToolStripButton.Name = "pasteToolStripButton";
            this.pasteToolStripButton.Size = new Size(23, 22);
            this.pasteToolStripButton.Text = "Вставить";
            this.pasteToolStripButton.Click += new EventHandler(this.OnEditPaste);

            this.CompilToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.CompilToolStripButton.Image = Resources.go;
            this.CompilToolStripButton.ImageTransparentColor = Color.Magenta;
            this.CompilToolStripButton.Name = "CompilToolStripButton";
            this.CompilToolStripButton.Size = new Size(23, 22);
            this.CompilToolStripButton.Text = "Загрузить программу СПЛ в устройство, запустить эмулятор";
            this.CompilToolStripButton.Click += new EventHandler(this.OnCompile);

            this.StopToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.StopToolStripButton.Image = Resources.stop;
            this.StopToolStripButton.ImageTransparentColor = Color.Magenta;
            this.StopToolStripButton.Name = "CompilToDeviceToolStripButton";
            this.StopToolStripButton.Size = new Size(23, 22);
            this.StopToolStripButton.Text = "Остановка эмуляции";
            this.StopToolStripButton.Click += new EventHandler(this.OnStop);
            
            return this.MainToolStrip;
        }
        
        #endregion

        #region MAIN MENU
        protected MenuControl CreateMenus()
        {
            MenuControl topMenu = new MenuControl
            {
                Style = this._style,
                MultiLine = false
            };

            MenuCommand topFile = new MenuCommand("&Файл");
            MenuCommand topEdit = new MenuCommand("Правка");
            MenuCommand topView = new MenuCommand("Вид");

            topMenu.MenuCommands.AddRange(new MenuCommand[] { topFile, topEdit, topView/*, topSettings,*/ });

            //File
            MenuCommand topFileNew = new MenuCommand("Новый", new EventHandler(this.OnFileNew));
            MenuCommand topFileOpen = new MenuCommand("Открыть документ", new EventHandler(this.OnFileOpen));
            MenuCommand topFileOpenProject = new MenuCommand("Открыть проект", new EventHandler(this.OnFileOpenProject));
            MenuCommand topFileClose = new MenuCommand("Закрыть документ", new EventHandler(this.OnFileClose));
            MenuCommand topFileCloseProject = new MenuCommand("Закрыть проект", new EventHandler(this.OnFileCloseProject));
            MenuCommand topFileSave = new MenuCommand("Сохранить документ", new EventHandler(this.OnFileSave));
            MenuCommand topFileSaveProject = new MenuCommand("Сохранить проект", new EventHandler(this.OnFileSaveProject));
            topFile.MenuCommands.AddRange(new MenuCommand[] { topFileNew, topFileOpen, topFileOpenProject,
                                                              topFileClose,topFileCloseProject,
                                                              topFileSave, topFileSaveProject });
            //Edit
            MenuCommand topEditCopy = new MenuCommand("Копировать", new EventHandler(this.OnEditCopy));
            MenuCommand topEditPaste = new MenuCommand("Вставить", new EventHandler(this.OnEditPaste));

            topEdit.MenuCommands.AddRange(new MenuCommand[] { topEditCopy, topEditPaste });
            //View
            MenuCommand topViewOutputWindow = new MenuCommand("Сообщения", new EventHandler(this.OnViewOutputWindow));
            MenuCommand topViewToolWindow = new MenuCommand("Библиотека", new EventHandler(this.OnViewToolWindow));
            topView.MenuCommands.AddRange(new MenuCommand[] { topViewOutputWindow,topViewToolWindow});
            topMenu.Dock = DockStyle.Top;
            Controls.Add(topMenu);
            return topMenu;
        }

        protected void OnFileNew(object sender, EventArgs e)
        {
            this._newForm.ShowDialog();
            if (DialogResult.OK == this._newForm.DialogResult)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this._newForm.NameOfSchema, this._newForm.sheetFormat, "MR731", this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);
                this.OutMessage("Создан новый лист схемы: " + this._newForm.NameOfSchema);
                myTab.Schematic.Focus();
            }
        }

        void printToolStripButton_Click(object sender, EventArgs e)
        {
            // Initialize the dialog's PrinterSettings property to hold user
            // defined printer settings.
            this.pageSetupDialog.PageSettings =
                new System.Drawing.Printing.PageSettings();
            // Initialize dialog's PrinterSettings property to hold user
            // set printer settings.
            this.pageSetupDialog.PrinterSettings =
                new System.Drawing.Printing.PrinterSettings();
            //Do not show the network in the printer dialog.
            this.pageSetupDialog.ShowNetwork = false;
            //Show the dialog storing the result.
            DialogResult result = this.pageSetupDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.printDocument.DefaultPageSettings = this.pageSetupDialog.PageSettings;
                this.printPreviewDialog.Document = this.printDocument;
                // Call the ShowDialog method. This will trigger the document's
                //  PrintPage event.
                this.printPreviewDialog.ShowDialog();
                DialogResult result1 = this.printDialog.ShowDialog();
                if (result1 == DialogResult.OK)
                {
                    this.printDocument.Print();
                }
            }
        }

        private void printDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;

                float scale = (float)e.PageBounds.Width /
                              (float)_sTab.Schematic.SizeX;
                _sTab.Schematic.DrawIntoGraphic(e.Graphics, scale);
            }
        }

        protected void OnUndo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Undo();
            }
        }
        protected void OnRedo(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.Redo();
            }
        }

        protected void OnEditCut(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
                _sTab.Schematic.DeleteEvent();
            }
        }

        protected void OnEditCopy(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.CopyFromXML();
            }
        }

        protected void OnEditPaste(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab is BSBGLTab)
            {
                BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                _sTab.Schematic.PasteFromXML();
            }

        }

        protected void OnFileOpen(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bsbgl файлы(*.bsbgl)|*.bsbgl|Все файлы (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                BSBGLTab myTab = new BSBGLTab();
                myTab.InitializeBSBGLSheet(this.openFileDialog.FileName, SheetFormat.A0_L, "MR731", this._manager);
                myTab.Selected = true;
                this._filler.TabPages.Add(myTab);

                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                myTab.Schematic.ReadXml(reader);
                myTab.UpdateTitle();
                reader.Close();
                this.OutMessage("Загружен лист схемы.");
                myTab.Schematic.Focus();
            }

        }
        protected void OnFileOpenProject(object sender, EventArgs e)
        {
            this.openFileDialog.Filter = "bprj файлы(*.bprj)|*.bprj|Все файлы (*.*)|*.*";
            if (this.openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (this._filler.SelectedTab != null)
                {
                    switch (MessageBox.Show("Сохранить текущий проект на диске ?",
                        "Закрытие проекта", MessageBoxButtons.YesNoCancel))
                    {
                        case DialogResult.Yes:
                            if (this.SaveProjectDoc())
                            {
                                this._filler.TabPages.Clear();
                            }
                            else
                            {
                                return;
                            }
                            break;
                        case DialogResult.No:
                            this._filler.TabPages.Clear();
                            break;
                        case DialogResult.Cancel:
                            return;
                    }
                }
                XmlTextReader reader = new XmlTextReader(this.openFileDialog.FileName);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                while (reader.Read())
                {
                    if ((reader.Name == "Source") && (reader.NodeType != XmlNodeType.EndElement))
                    {
                        BSBGLTab myTab = new BSBGLTab();
                        myTab.InitializeBSBGLSheet(reader.GetAttribute("name"), SheetFormat.A0_L, "MR731", this._manager);
                        myTab.Selected = true;
                        this._filler.TabPages.Add(myTab);
                        myTab.Schematic.ReadXml(reader);
                        myTab.UpdateTitle();
                    }
                }
                this.OutMessage("Загружен проект.");
                reader.Close();
            }
        }

        protected void OnFileClose(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("Сохранить документ на диске ?"
                                                , "Закрытие документа", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    if (this.SaveActiveDoc())
                    {
                        BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                        this._filler.TabPages.Remove(tab);
                        tab.Dispose();
                    }
                    break;
                case DialogResult.No:
                {
                    BSBGLTab tab = (BSBGLTab)this._filler.SelectedTab;
                    this._filler.TabPages.Remove(tab);
                    tab.Dispose();
                }
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        protected void OnFileCloseProject(object sender, EventArgs e)
        {
            if (this._filler.SelectedTab == null) return;
            switch (MessageBox.Show("Сохранить проект на диске ?"
                                                , "Закрытие проекта", MessageBoxButtons.YesNoCancel))
            {
                case DialogResult.Yes:
                    this.SaveProjectDoc();                    
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();                    
                    break;
                case DialogResult.No:
                    foreach (BSBGLTab tabPage in this._filler.TabPages)
                    {
                        tabPage.Dispose();
                    }
                    this._filler.TabPages.Clear();
                    break;
                case DialogResult.Cancel:
                    break;
            }
        }
        protected void OnFileSave(object sender, EventArgs e)
        {
            this.SaveActiveDoc();
        }
        protected void OnFileSaveProject(object sender, EventArgs e)
        {
            this.SaveProjectDoc();
        }
        protected void OnViewToolWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._libraryContent);
        }
        protected void OnViewOutputWindow(object sender, EventArgs e)
        {
            this._manager.ShowContent(this._outputContent);
        }

        protected void CompileProject()
        {
            this._compiller.ResetCompiller();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                this._compiller.AddSource(src.TabName, src.Schematic);
                this.OutMessage("Компиляция схемы :" + src.TabName);
            }
            this._binFile = this._compiller.Make(Common.VersionConverter(this._device.DeviceVersion));
            this.OutMessage("Пpоцент заполнения схемы:" + ((float)(((float) this._compiller.binarysize * 100) / 1024)).ToString() + "%.");
            if (this._compiller.binarysize > 4096)
            {
                MessageBox.Show("Программа слишком велика ! "
                                , "Ошибка компиляции", MessageBoxButtons.OK);
                this.OnStop();
                return;
            }
            if (this._binFile.Length == 0)
            {
                this.OnStop();
                return;
            }
            foreach (BSBGLTab tabPage in this._filler.TabPages)
            {
                tabPage.Schematic.StartDebugMode();
            }
            this._pageIndex = 0;
            ushort[] values = new ushort[1];
            values[0] = this._pageIndex;
            ProgramPageStruct pp = new ProgramPageStruct();
            pp.InitStruct(Common.TOBYTES(values, false));
            this._currentProgramPage.Value = pp;
            this._currentProgramPage.SaveStruct();
            this._isOnSimulateMode = true;
            this._formCheck = new MessageBoxForm();
            this._formCheck.SetMaxProgramBar(COUNT_EXCHANGES_PROGRAM);
            this._formCheck.ShowDialog(InformationMessages.LOADING_PROGRAM_IN_DEVICE);
        }
        
        protected void OnCompile(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this.CompileProject();
        }

        void OnStop(object sender, EventArgs e)
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        void OnStop()
        {
            this._isOnSimulateMode = false;
            this._currentSignalsStruct.RemoveStructQueries();
            foreach (BSBGLTab src in this._filler.TabPages)
            {
                src.Schematic.StopDebugEvent();
            }
        }
        private bool SaveActiveDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bsbgl файлы(*.bsbgl)|*.bsbgl|Все файлы (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter memwriter = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                memwriter.Formatting = Formatting.Indented;
                memwriter.WriteStartDocument();

                if (this._filler.SelectedTab is BSBGLTab)
                {
                    BSBGLTab _sTab = (BSBGLTab) this._filler.SelectedTab;
                    _sTab.Schematic.WriteXml(memwriter);
                }
                memwriter.WriteEndDocument();
                memwriter.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName,
                                FileMode.Create, FileAccess.Write, FileShare.None, uncompressed.Length,
                                false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();

                byte[] compressed = compr.Compress(uncompressed);
                FileStream fsa = new FileStream(this.saveFileDialog.FileName + ".Zip",
                                FileMode.Create, FileAccess.Write, FileShare.None, compressed.Length,
                                false);
                fsa.Write(compressed, 0, compressed.Length);
                fsa.Close();
                memstream.Close();
                return true;
            }
            return false;
        }
        private bool SaveProjectDoc()
        {
            ZIPCompressor compr = new ZIPCompressor();
            DialogResult SaveFileRzult;
            this.saveFileDialog = new SaveFileDialog();
            this.saveFileDialog.Filter = "bprj файлы(*.bprj)|*.bprj|Все файлы (*.*)|*.*";
            SaveFileRzult = this.saveFileDialog.ShowDialog();
            if (SaveFileRzult == DialogResult.OK)
            {
                MemoryStream memstream = new MemoryStream();
                XmlTextWriter writer = new XmlTextWriter(memstream, System.Text.Encoding.UTF8);
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("BSBGL_ProjectFile");
                foreach (BSBGLTab src in this._filler.TabPages)
                {
                    writer.WriteStartElement("Source");
                    writer.WriteAttributeString("name", src.TabName);
                    src.Schematic.WriteXml(writer);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
                byte[] uncompressed = memstream.ToArray();
                FileStream fs = new FileStream(this.saveFileDialog.FileName, FileMode.Create, FileAccess.Write,
                    FileShare.None, uncompressed.Length, false);
                fs.Write(uncompressed, 0, uncompressed.Length);
                fs.Close();
                byte[] compressed = compr.Compress(uncompressed);
                fs = new FileStream(this.saveFileDialog.FileName + ".Zip", FileMode.Create, FileAccess.Write, FileShare.None,
                    compressed.Length, false);
                fs.Write(compressed, 0, compressed.Length);
                fs.Close();
                memstream.Close();
                return true;
            }
            return false;
        }

        #endregion

        #region IFormView Members

        public Type FormDevice
        {
            get { return typeof(Mr731Device); }
        }

        public bool Multishow { get; private set; }

        #endregion

        #region INodeView Members

        public Type ClassType
        {
            get { return typeof(BSBGLEF); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get
            {
                return Resources.programming.ToBitmap();
                //(Image)BEMN.UDZT.Properties.Resources.programming1.ToBitmap(); 
            }
        }

        public string NodeName
        {
            get { return "Программирование"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }

        #endregion
    }
}
