﻿using BEMN.Devices.Structures;
using BEMN.Devices.Structures.Attributes;
using BEMN.MBServer;

namespace BEMN.Mr731.AlarmJournal.Structures
{
    public class AlarmJournalRecordStruct : StructBase
    {
        #region [Constants]

        private const string DATE_TIME_PATTERN = "{0:d2}.{1:d2}.{2:d2} {3:d2}:{4:d2}:{5:d2},{6:d2}";
   //     private const string SPL_PATTERN = "СПЛ {0}";

        #endregion [Constants]


        #region [Private fields]

        [Layout(0)] private ushort _year;
        [Layout(1)] private ushort _month;
        [Layout(2)] private ushort _date;
        [Layout(3)] private ushort _hour;
        [Layout(4)] private ushort _minute;
        [Layout(5)] private ushort _second;
        [Layout(6)] private ushort _millisecond;
        [Layout(7)] private ushort _message;
        [Layout(8)] private ushort _stage;
        [Layout(9)] private ushort _numberOfTriggeredParametr;
        [Layout(10)] private ushort _valueOfTriggeredParametr;
        [Layout(11)] private ushort _groupOfSetpoints;
        [Layout(12)] private ushort _ia;
        [Layout(13)] private ushort _ib;
        [Layout(14)] private ushort _ic;
        [Layout(15)] private ushort _i0;
        [Layout(16)] private ushort _i2;
        [Layout(17)] private ushort _ig;
        [Layout(18)] private ushort _i1;
        [Layout(19)] private ushort _in;
        [Layout(20)] private ushort _in1;
        [Layout(21)] private ushort _i2A;
        [Layout(22)] private ushort _i2B;
        [Layout(23)] private ushort _i2C;
        [Layout(24)] private ushort _ua;
        [Layout(25)] private ushort _ub;
        [Layout(26)] private ushort _uc;
        [Layout(27)] private ushort _uab;
        [Layout(28)] private ushort _ubc;
        [Layout(29)] private ushort _uca;
        [Layout(30)] private ushort _u0;
        [Layout(31)] private ushort _u2;
        [Layout(32)] private ushort _u1;
        [Layout(33)] private ushort _un;
        [Layout(34)] private ushort _un1;
        [Layout(35)] private ushort _f;
        [Layout(36)] private ushort _d1;
        [Layout(37)] private ushort _d2;
        [Layout(38)] private ushort _d3;
        [Layout(39)] private ushort _omp;
        [Layout(40)] private ushort _spl;
        [Layout(41)] private ushort _q;

        #endregion [Private fields]


        #region [Properties]

        public int Stage
        {
            get { return Common.GetBits(this.StageValue, 0, 1, 2, 3, 4, 5, 6, 7); }
        }

        public string D1To8
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D1),true); }
        }

        public string D9To16
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D1), true); }
        }

        public string D17To24
        {
            get { return   Common.ByteToMask(Common.LOBYTE(this.D2),true); }
        }

        public string D25To32
        {
            get { return Common.ByteToMask(Common.HIBYTE(this.D2), true); }
        }

        public string D33To40
        {
            get { return Common.ByteToMask(Common.LOBYTE(this.D3), true); }
        }

        /// <summary>
        /// true если во всех полях 0, условие конца ЖА
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                var sum = this.Year +
                          this.Month +
                          this.Date +
                          this.Hour +
                          this.Minute +
                          this.Second +
                          this.Millisecond;
                return sum == 0;
            }
        }

        /// <summary>
        /// Дата и время сообщения
        /// </summary>
        public string GetTime
        {
            get
            {
                return string.Format
                    (
                        DATE_TIME_PATTERN,
                        this.Date,
                        this.Month,
                        this.Year,
                        this.Hour,
                        this.Minute,
                        this.Second,
                        this.Millisecond
                    );
            }
        }

        public ushort Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public ushort NumberOfTriggeredParametr
        {
            get { return _numberOfTriggeredParametr; }
            set { _numberOfTriggeredParametr = value; }
        }

        public ushort ValueOfTriggeredParametr
        {
            get { return _valueOfTriggeredParametr; }
            set { _valueOfTriggeredParametr = value; }
        }

        public ushort GroupOfSetpoints
        {
            get { return _groupOfSetpoints; }
            set { _groupOfSetpoints = value; }
        }

        public ushort Ia
        {
            get { return _ia; }
            set { _ia = value; }
        }

        public ushort Ib
        {
            get { return _ib; }
            set { _ib = value; }
        }

        public ushort Ic
        {
            get { return _ic; }
            set { _ic = value; }
        }

        public ushort I0
        {
            get { return _i0; }
            set { _i0 = value; }
        }

        public ushort I2
        {
            get { return _i2; }
            set { _i2 = value; }
        }

        public ushort Ig
        {
            get { return _ig; }
            set { _ig = value; }
        }

        public ushort I1
        {
            get { return _i1; }
            set { _i1 = value; }
        }

        public ushort In
        {
            get { return _in; }
            set { _in = value; }
        }

        public ushort In1
        {
            get { return _in1; }
            set { _in1 = value; }
        }

        public ushort I2A
        {
            get { return _i2A; }
            set { _i2A = value; }
        }

        public ushort I2B
        {
            get { return _i2B; }
            set { _i2B = value; }
        }

        public ushort I2C
        {
            get { return _i2C; }
            set { _i2C = value; }
        }

        public ushort Ua
        {
            get { return _ua; }
            set { _ua = value; }
        }

        public ushort Ub
        {
            get { return _ub; }
            set { _ub = value; }
        }

        public ushort Uc
        {
            get { return _uc; }
            set { _uc = value; }
        }

        public ushort Uab
        {
            get { return _uab; }
            set { _uab = value; }
        }

        public ushort Ubc
        {
            get { return _ubc; }
            set { _ubc = value; }
        }

        public ushort Uca
        {
            get { return _uca; }
            set { _uca = value; }
        }

        public ushort U0
        {
            get { return _u0; }
            set { _u0 = value; }
        }

        public ushort U2
        {
            get { return _u2; }
            set { _u2 = value; }
        }

        public ushort U1
        {
            get { return _u1; }
            set { _u1 = value; }
        }

        public ushort Un
        {
            get { return _un; }
            set { _un = value; }
        }

        public ushort Un1
        {
            get { return _un1; }
            set { _un1 = value; }
        }

        public ushort F
        {
            get { return _f; }
            set { _f = value; }
        }

        public ushort Omp
        {
            get { return _omp; }
            set { _omp = value; }
        }

        public ushort Spl
        {
            get { return _spl; }
            set { _spl = value; }
        }

        public ushort Q
        {
            get { return _q; }
            set { _q = value; }
        }

        public ushort Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public ushort Month
        {
            get { return _month; }
            set { _month = value; }
        }

        public ushort Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public ushort Hour
        {
            get { return _hour; }
            set { _hour = value; }
        }

        public ushort Minute
        {
            get { return _minute; }
            set { _minute = value; }
        }

        public ushort Second
        {
            get { return _second; }
            set { _second = value; }
        }

        public ushort Millisecond
        {
            get { return _millisecond; }
            set { _millisecond = value; }
        }

        public ushort StageValue
        {
            get { return _stage; }
            set { _stage = value; }
        }

        public ushort D1
        {
            get { return _d1; }
            set { _d1 = value; }
        }

        public ushort D2
        {
            get { return _d2; }
            set { _d2 = value; }
        }

        public ushort D3
        {
            get { return _d3; }
            set { _d3 = value; }
        }

        #endregion [Properties]
    }
}
