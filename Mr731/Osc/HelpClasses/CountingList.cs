using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using BEMN.MBServer;
using System.Linq;
using System.Text;
using BEMN.Mr731.Configuration;
using BEMN.Mr731.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr731.Osc.Structures;

namespace BEMN.Mr731.Osc.HelpClasses
{
    /// <summary>
    /// ��������� �������� ������������
    /// </summary>
    public class CountingList
    {
        #region [Constants]
        /// <summary>
        /// ������ ������ �������(� ������)
        /// </summary>
        public const int COUNTING_SIZE = 12;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int DISCRETS_COUNT = 40;
        /// <summary>
        /// ���-�� �������
        /// </summary>
        public const int CHANNELS_COUNT = 24;
        /// <summary>
        /// ���-�� �����
        /// </summary>
        public const int CURRENTS_COUNT = 4;
        /// <summary>
        /// ���-�� ����������
        /// </summary>
        public const int VOLTAGES_COUNT = 4;

        public static readonly string[] INames = new[] { "Ia", "Ib", "Ic", "In" };
        public static readonly string[] UNames = new[] { "Ua", "Ub", "Uc", "Un" };
        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ������ �������� �������� �� �����
        /// </summary>
        private readonly ushort[][] _countingArray;
        /// <summary>
        /// ������ ������� 1-40
        /// </summary>
        private ushort[][] _discrets;
        /// <summary>
        /// ������ ������� 1-24
        /// </summary>
        private ushort[][] _channels;
        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        private int _count;

        #region [����]
        /// <summary>
        /// ����
        /// </summary>
        private double[][] _currents;
        /// <summary>
        /// ������������ �������� �����
        /// </summary>
        private short[][] _baseCurrents;

        /// <summary>
        /// ����������� ���
        /// </summary>
        private double _minI = 0;
        /// <summary>
        /// ������������ ���
        /// </summary>
        private double _maxI = 0;

        /// <summary>
        /// ����������� ���
        /// </summary>
        public double MinI
        {
            get { return this._minI; }
        }

        /// <summary>
        /// ������������ ���
        /// </summary>
        public double MaxI
        {
            get { return this._maxI; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Currents
        {
            get { return this._currents; }
            set { this._currents = value; }
        } 
        #endregion [����]


        #region [����������]
        /// <summary>
        /// ����������
        /// </summary>
        private double[][] _voltages;
        /// <summary>
        /// ������������ �������� ����������
        /// </summary>
        private short[][] _baseVoltages;

        /// <summary>
        /// ����������� ����������
        /// </summary>
        private double _minU = 0;
        /// <summary>
        /// ������������ ����������
        /// </summary>
        private double _maxU = 0;
        /// <summary>
        /// ����������� ����������
        /// </summary>
        public double MinU
        {
            get { return this._minU; }
        }
        /// <summary>
        /// ������������ ����������
        /// </summary>
        public double MaxU
        {
            get { return this._maxU; }
        }
        /// <summary>
        /// ������������
        /// </summary>
        public double[][] Voltages
        {
            get { return this._voltages; }
            set { this._voltages = value; }
        }  
        #endregion [����������]




        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        private int _alarm;


        private OscJournalStruct _oscJournalStruct;

   


        #endregion [Private fields]

        /// <summary>
        /// ������
        /// </summary>
        public ushort[][] Channels
        {
            get { return this._channels; }
            set { this._channels = value; }
        }

        /// <summary>
        /// ��������
        /// </summary>
        public ushort[][] Discrets
        {
            get { return this._discrets; }
            set { this._discrets = value; }
        }



        /// <summary>
        /// ����� ���������� ��������
        /// </summary>
        public int Count
        {
            get { return this._count; }
        }

        /// <summary>
        /// ������("���� �����������") � ��������
        /// </summary>
        public int Alarm
        {
            get { return this._alarm; }
        }
        
        private string _dateTime;
        public string DateAndTime
        {
            get { return this._dateTime; }
        }

        private string _stage;

        private IEnumerable<ushort> _channelsNames;
        public ushort[] ChannelsNames
        {
            get { return this._channelsNames.ToArray(); }
        }

        public string Stage
        {
            get { return this._stage; }
        }
        /// <summary>
        /// ������������ �������� ��� �����
        /// </summary>
        private double[] CurrentsKoefs { get; set; }
        /// <summary>
        /// ������������ �������� ��� ����������
        /// </summary>
        private double[] VoltageKoefs { get; set; }
        public string FilePath { get; private set; }
        public bool IsLoad { get; private set; }

        #region [Ctor's]
        public CountingList(ushort[] pageValue, OscJournalStruct oscJournalStruct, MeasureTransStruct measure, IEnumerable<ushort> channelsNames)
        {
            this._channelsNames = channelsNames;
            this._oscJournalStruct = oscJournalStruct;
            this._dateTime = oscJournalStruct.GetFormattedDateTime;
            this._alarm = this._oscJournalStruct.Len - this._oscJournalStruct.After;
            this._stage = oscJournalStruct.Stage;
            this._countingArray = new ushort[COUNTING_SIZE][];
            //����� ���������� ��������
            this._count = pageValue.Length/COUNTING_SIZE;
            //������������� �������
            for (int i = 0; i < COUNTING_SIZE; i++)
            {
                this._countingArray[i] = new ushort[this.Count];
            }
            int m = 0;
            int n = 0;
            foreach (ushort value in pageValue)
            {
                this._countingArray[n][m] = value;
                n++;
                if (n == COUNTING_SIZE)
                {
                    m++;
                    n = 0;
                }
            }
            ushort[][] d1To16 = this.DiscretArrayInit(this._countingArray[8]);
            ushort[][] d17To32 = this.DiscretArrayInit(this._countingArray[9]);
            ushort[][] d33To40AndC1To8 = this.DiscretArrayInit(this._countingArray[10]);
            ushort[][] c9To24 = this.DiscretArrayInit(this._countingArray[11]);

            List<ushort[]> dicrets = new List<ushort[]>(DISCRETS_COUNT);
            dicrets.AddRange(d33To40AndC1To8.Take(8));
            dicrets.AddRange(d1To16);
            dicrets.AddRange(d17To32);
            
            List<ushort[]> channels = new List<ushort[]>(CHANNELS_COUNT);

            channels.AddRange(d33To40AndC1To8.Skip(8));
            channels.AddRange(c9To24);
            this._channels = channels.ToArray();
            this._discrets = dicrets.ToArray();

            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];
            double[] currentsKoefs = new double[CURRENTS_COUNT];
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                this._baseCurrents[i] = this._countingArray[i].Select(a => (short) a).ToArray();

                double koef;
                if (i == CURRENTS_COUNT - 1)
                {
                    koef = measure.I1.Ittx;
                }
                else
                {
                    koef = measure.I1.Ittl;
                }

                currentsKoefs[i] = koef * 40 *2 * Math.Sqrt(2) / 65536.0;
                this._currents[i] = this._baseCurrents[i].Select(a => currentsKoefs[i] * a).ToArray();
                this._maxI = Math.Max(this.MaxI, this._currents[i].Max());
                this._minI = Math.Min(this.MinI, this._currents[i].Min());
            }
            this.CurrentsKoefs = currentsKoefs;


            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];
            double[] voltageKoefs = new double[VOLTAGES_COUNT];

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                this._baseVoltages[i] = this._countingArray[i+4].Select(a => (short)a).ToArray();

                double koef;
                if (i == VOLTAGES_COUNT-1)
                {
                    koef = measure.U1.KthxValue;
                }
                else
                {
                    koef = measure.U1.KthlValue;
                }

                voltageKoefs[i] = koef*2*Math.Sqrt(2)/256.0;
                this._voltages[i] = this._baseVoltages[i].Select(a => voltageKoefs[i] * (double)a).ToArray();
                this._maxU = Math.Max(this._maxU, this._voltages[i].Max());
                this._minU = Math.Min(this._minU, this._voltages[i].Min());
            }
            this.VoltageKoefs = voltageKoefs;

        }

        #endregion [Ctor's]

        #region [Help members]
        /// <summary>
        /// ���������� ������ ����� � ��������������� ������ ���(�������� 0/1) 
        /// </summary>
        /// <param name="sourseArray">������ �������� ���</param>
        /// <returns></returns>
        private ushort[][] DiscretArrayInit(ushort[] sourseArray)
        {
            ushort[][] result = new ushort[16][];
            for (int i = 0; i < 16; i++)
            {
                result[i] = new ushort[sourseArray.Length];
            }

            for (int i = 0; i < sourseArray.Length; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    result[j][i] = (ushort)(Common.GetBit(sourseArray[i], j) ? 1 : 0);
                }
            }

            return result;
        } 
        #endregion [Help members]
        
        public void Save(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            using (StreamWriter hdrFile = new StreamWriter(hdrPath))
            {
                hdrFile.WriteLine("�� 731 {0} {1} ������� - {2}", this._oscJournalStruct.GetDate,
                    this._oscJournalStruct.GetTime, this._oscJournalStruct.Stage);
                hdrFile.WriteLine("Size, ms = {0}", this._oscJournalStruct.Len);
                hdrFile.WriteLine("Alarm = {0}", this._alarm);
                for (int i = 0; i < this.ChannelsNames.Length; i++)
                {
                    hdrFile.WriteLine("K{0} = {1}", i + 1, this.ChannelsNames[i]);
                }
                hdrFile.WriteLine(this._stage);
                hdrFile.WriteLine(1251);

            }

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            using (StreamWriter cgfFile = new StreamWriter(cgfPath, false, Encoding.GetEncoding(1251)))
            {
                cgfFile.WriteLine("MP731,1");
                cgfFile.WriteLine("72,8A,64D");
                int index = 1;
                for (int i =0; i < this.Currents.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                    
                    cgfFile.WriteLine("{0},{1},,,A,{2},0,0,-32768,32767,1,1,P", index,INames[i], this.CurrentsKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Voltages.Length; i++)
                {
                    NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                    cgfFile.WriteLine("{0},{1},,,V,{2},0,0,-32768,32767,1,1,P", index,UNames[i], this.VoltageKoefs[i].ToString(format));
                    index++;
                }

                for (int i = 0; i < this.Discrets.Length; i++)
                {
                    cgfFile.WriteLine("{0},D{1},0", index, i+1);
                    index++;
                }

                for (int i = 0; i < this.Channels.Length; i++)
                {
                    cgfFile.WriteLine("{0},K{1} ({2}),0", index, i + 1, StringsConfig.RelaySignalsLat[this.ChannelsNames[i]]);
                    index++;
                }
                cgfFile.WriteLine("50");
                cgfFile.WriteLine("1");
                cgfFile.WriteLine("1000,{0}", this._oscJournalStruct.Len);

                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTime);
                cgfFile.WriteLine(this._oscJournalStruct.GetFormattedDateTimeAlarm(this.Alarm));
                cgfFile.WriteLine("ASCII");
            }

            string datPath = Path.ChangeExtension(filePath, "dat");
            using (StreamWriter datFile = new StreamWriter(datPath))
            {
                for (int i = 0; i < this._count; i++)
                {
                    datFile.Write("{0:D6},{1:D6}",i,i*1000);
                    foreach (short[] current  in this._baseCurrents)
                    {
                        datFile.Write(",{0}", current[i]);      
                    }
                    foreach (short[] voltage in this._baseVoltages)
                    {
                        datFile.Write(",{0}", voltage[i]);
                    }

                    foreach (ushort[] discret in this.Discrets)
                    {
                        datFile.Write(",{0}", discret[i]);   
                    }
                    foreach (ushort[] chanel in this.Channels)
                    {
                        datFile.Write(",{0}", chanel[i]);
                    }
                    datFile.WriteLine();
                }
            }
        }

        public static CountingList Load(string filePath)
        {
            string hdrPath = Path.ChangeExtension(filePath, "hdr");
            string[] hdrStrings = File.ReadAllLines(hdrPath);
            string timeString;
            if (hdrStrings[0].StartsWith("Fault date :"))
            {
                timeString = hdrStrings[0].Replace("Fault date : ", string.Empty);
            }
            else
            {
                string[] buff = hdrStrings[0].Split(' ');
                timeString = string.Format("{0} {1}", buff[2], buff[3]);
            }
            int alarm = int.Parse(hdrStrings[2].Replace("Alarm = ",string.Empty));

            ushort[] channelNames = new ushort[CHANNELS_COUNT];
            for (int i = 0; i < CHANNELS_COUNT; i++)
            {
                channelNames[i] = ushort.Parse(hdrStrings[3 + i].Replace(string.Format("K{0} = ",i+1),string.Empty));
            }
            string stage = hdrStrings[27];
      

            string cgfPath = Path.ChangeExtension(filePath, "cfg");
            string[] cfgStrings = File.ReadAllLines(cgfPath);
            double[] factors = new double[VOLTAGES_COUNT+ CURRENTS_COUNT];

                    Regex factorRegex = new Regex(@"\d+\,[IU]\w+\,\,\,[AV]\,(?<value>[0-9\.]+)");
            for (int i = 2; i < 2 + VOLTAGES_COUNT + CURRENTS_COUNT; i++)
            {
                string res = factorRegex.Match(cfgStrings[i]).Groups["value"].Value;
                NumberFormatInfo format = new NumberFormatInfo {NumberDecimalSeparator = "."};
                factors[i - 2] = double.Parse(res, format);
            }
            int counts = int.Parse(cfgStrings[76].Replace("1000,", string.Empty));

            CountingList result = new CountingList(counts) {_alarm = alarm};
            string datPath = Path.ChangeExtension(filePath, "dat");
            string[] datStrings = File.ReadAllLines(datPath);

            string dataPattern =
                @"^\d+\,\d+,(?<I1>\-?\d+),(?<I2>\-?\d+),(?<I3>\-?\d+),(?<I4>\-?\d+),(?<U1>\-?\d+),(?<U2>\-?\d+),(?<U3>\-?\d+),(?<U4>\-?\d+),(?<D1>\d+),(?<D2>\d+),(?<D3>\d+),(?<D4>\d+),(?<D5>\d+),(?<D6>\d+),(?<D7>\d+),(?<D8>\d+),(?<D9>\d+),(?<D10>\d+),(?<D11>\d+),(?<D12>\d+),(?<D13>\d+),(?<D14>\d+),(?<D15>\d+),(?<D16>\d+),(?<D17>\d+),(?<D18>\d+),(?<D19>\d+),(?<D20>\d+),(?<D21>\d+),(?<D22>\d+),(?<D23>\d+),(?<D24>\d+),(?<D25>\d+),(?<D26>\d+),(?<D27>\d+),(?<D28>\d+),(?<D29>\d+),(?<D30>\d+),(?<D31>\d+),(?<D32>\d+),(?<D33>\d+),(?<D34>\d+),(?<D35>\d+),(?<D36>\d+),(?<D37>\d+),(?<D38>\d+),(?<D39>\d+),(?<D40>\d+),(?<C1>\d+),(?<C2>\d+),(?<C3>\d+),(?<C4>\d+),(?<C5>\d+),(?<C6>\d+),(?<C7>\d+),(?<C8>\d+),(?<C9>\d+),(?<C10>\d+),(?<C11>\d+),(?<C12>\d+),(?<C13>\d+),(?<C14>\d+),(?<C15>\d+),(?<C16>\d+),(?<C17>\d+),(?<C18>\d+),(?<C19>\d+),(?<C20>\d+),(?<C21>\d+),(?<C22>\d+),(?<C23>\d+),(?<C24>\d+)";
            Regex dataRegex = new Regex(dataPattern);



            double[][] currents = new double[CURRENTS_COUNT][];
            double[][] voltages = new double[VOLTAGES_COUNT][];
            ushort[][] discrets = new ushort[DISCRETS_COUNT][];
            ushort[][] channels = new ushort[CHANNELS_COUNT][];
            
            for (int i = 0; i < currents.Length; i++)
            {
                currents[i] = new double[counts];         
            }

            for (int i = 0; i < voltages.Length; i++)
            {
                voltages[i] = new double[counts];
            }

            for (int i = 0; i < discrets.Length; i++)
            {
                discrets[i] = new ushort[counts];
            }

            for (int i = 0; i < channels.Length; i++)
            {
                channels[i] = new ushort[counts];
            }

            for (int i = 0; i < datStrings.Length; i++)
            {
                for (int j = 0; j < CURRENTS_COUNT; j++)
                {
                    currents[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["I" + (j + 1)].Value) * factors[j];
                }

                for (int j = 0; j < VOLTAGES_COUNT; j++)
                {
                    voltages[j][i] = double.Parse(dataRegex.Match(datStrings[i]).Groups["U" + (j + 1)].Value) * factors[j + CURRENTS_COUNT];
                }

                for (int j = 0; j < DISCRETS_COUNT; j++)
                {
                    discrets[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["D" + (j + 1)].Value);
                }

                for (int j = 0; j < CHANNELS_COUNT; j++)
                {
                    channels[j][i] = ushort.Parse(dataRegex.Match(datStrings[i]).Groups["C" + (j + 1)].Value);
                }      
            }
            for (int i = 0; i < CURRENTS_COUNT; i++)
            {
                result._maxI = Math.Max(result.MaxI, currents[i].Max());
                result._minI = Math.Min(result.MinI, currents[i].Min());                
            }

            for (int i = 0; i < VOLTAGES_COUNT; i++)
            {
                result._maxU = Math.Max(result.MaxU, voltages[i].Max());
                result._minU = Math.Min(result.MinU, voltages[i].Min());
            }

            result.Currents = currents;
            result.Channels = channels;
            result.Discrets = discrets;
            result.Voltages = voltages;
            result.FilePath = filePath;
            result.IsLoad = true;

            result._dateTime = timeString;
            result._stage = stage;
            result._channelsNames = channelNames;
            return result;
        }
        private CountingList(int count)
        {
            this._discrets = new ushort[DISCRETS_COUNT][];
            this._channels = new ushort[CHANNELS_COUNT][];
            this._currents = new double[CURRENTS_COUNT][];
            this._baseCurrents = new short[CURRENTS_COUNT][];

            this._voltages = new double[VOLTAGES_COUNT][];
            this._baseVoltages = new short[VOLTAGES_COUNT][];

            this._count = count;
        }

    }
}
