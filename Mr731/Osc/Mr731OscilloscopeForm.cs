using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Forms.ValidatingClasses;
using BEMN.Framework.Properties;
using BEMN.Interfaces;
using BEMN.Mr731.AlarmJournal;
using BEMN.Mr731.Configuration.Structures.MeasuringTransformer;
using BEMN.Mr731.Osc.HelpClasses;
using BEMN.Mr731.Osc.Loaders;
using BEMN.Mr731.Osc.ShowOsc;
using BEMN.Mr731.Osc.Structures;

namespace BEMN.Mr731.Osc
{
    public partial class Mr731OscilloscopeForm : Form, IFormView
    {
        #region [Constants]
        private const string OSC = "�������������";
        private const string READ_OSC_FAIL = "���������� ��������� ������ ������������";
        private const string RECORDS_IN_JOURNAL = "������������ � ������� - {0}";
        private const string OSC_LOAD_SUCCESSFUL = "������������ ������� ���������";
        private const string READ_OSC_STOPPED = "������ ������������� ����������";
        private const string JOURNAL_IS_EMPTY = "������ ������������ ����";

        #endregion [Constants]


        #region [Private fields]
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscPageLoader _pageLoader;
        /// <summary>
        /// ��������� �������
        /// </summary>
        private readonly OscJournalLoader _oscJournalLoader;
        /// <summary>
        /// ��������� ������� �����
        /// </summary>
        private readonly CurrentOptionsLoader _currentOptionsLoader;
        /// <summary>
        /// ������ ���
        /// </summary>
        private CountingList _countingList;
        private OscJournalStruct _journalStruct;
        private readonly DataTable _table;
        private OscOptionsLoader _oscopeOptionsLoader;
        private Mr731Device _device;
        #endregion [Private fields]


        #region [Ctor's]
        public Mr731OscilloscopeForm()
        {
            InitializeComponent();
        }

        public Mr731OscilloscopeForm(Mr731Device device)
        {
            InitializeComponent();
            this._device = device;
            //��������� �������
            this._pageLoader = new OscPageLoader(device.SetOscStartPage, device.OscPage);
            this._pageLoader.PageRead += HandlerHelper.CreateActionHandler(this, this._oscProgressBar.PerformStep);
            this._pageLoader.OscReadSuccessful += HandlerHelper.CreateActionHandler(this, OscReadOk);
            this._pageLoader.OscReadStopped += HandlerHelper.CreateActionHandler(this, ReadStop);
            //��������� �������
            this._oscJournalLoader = new OscJournalLoader(device.OscJournal, device.RefreshOscJournal, device.OscOptions);
            this._oscJournalLoader.ReadRecordOk += HandlerHelper.CreateActionHandler(this, ReadRecord);
            this._oscJournalLoader.AllJournalReadOk += HandlerHelper.CreateActionHandler(this, this.OnAllJournalReadOk);
            var failReadOscJournalDelegate = HandlerHelper.CreateActionHandler(this, FailReadOscJournal);
            this._oscJournalLoader.ReadJournalFail += failReadOscJournalDelegate;
            //������������ �����������
            this._oscopeOptionsLoader = device.OscopeOptionsLoader;
            this._oscopeOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscJournalLoader.StartReadJournal);
            this._oscopeOptionsLoader.LoadFail += failReadOscJournalDelegate;
            //��������� ������� �����
            this._currentOptionsLoader = device.CurrentOptionsLoader;
            this._currentOptionsLoader.LoadOk += HandlerHelper.CreateActionHandler(this, this._oscopeOptionsLoader.StartRead);
            this._currentOptionsLoader.LoadFail += failReadOscJournalDelegate;

            this._table = this.GetJournalDataTable();

        }
        #endregion [Ctor's]

        private void ReadStop()
        {
            this._statusLabel.Text = READ_OSC_STOPPED;
            _stopReadOsc.Enabled = false;
            _oscProgressBar.Value = 0;
        }

        /// <summary>
        /// �������� ���� ������
        /// </summary>
        private void OnAllJournalReadOk()
        {
            if (this._oscJournalLoader.RecordNumber == 0)
            {
                this._statusLabel.Text = JOURNAL_IS_EMPTY;
            }
            this.EnableButtons = true;
        }
        
        #region [IFormView Members]
        public Type FormDevice
        {
            get { return typeof(Mr731Device); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (Mr731OscilloscopeForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Resources.oscilloscope.ToBitmap(); }
        }

        public string NodeName
        {
            get { return Mr731OscilloscopeForm.OSC; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] { }; }
        }

        public bool Deletable
        {
            get { return false; }
        }
        #endregion [IFormView Members]


        #region [Help Classes Events Handlers]
        /// <summary>
        /// ���������� ��������� ������ - ������� ��������� �� ������
        /// </summary>
        private void FailReadOscJournal()
        {
            this._statusLabel.Text = READ_OSC_FAIL;
            this.EnableButtons = true;
        }


        /// <summary>
        /// ��������� ���� ������ �������
        /// </summary>
        private void ReadRecord()
        {
          
            var number = this._oscJournalLoader.RecordNumber;
            this._oscilloscopeCountCb.Items.Add(number);
                if (!this.CanSelectOsc)
                {
                    this.CanSelectOsc = true;
                }
                this._statusLabel.Text = string.Format(RECORDS_IN_JOURNAL, number);
                this._table.Rows.Add(this._oscJournalLoader.GetRecord);
                this._oscJournalDataGrid.Refresh();
        }

        /// <summary>
        /// ������������ ������� ��������� �� ����������
        /// </summary>
        private void OscReadOk()
        {
            this._statusLabel.Text = OSC_LOAD_SUCCESSFUL;
            this.CountingList = new CountingList(this._pageLoader.ResultArray, this._journalStruct, this._currentOptionsLoader.MeasureStruct, this._oscopeOptionsLoader.OscOptions.OscopeAllChannels.ChannelsInWords);
            this._stopReadOsc.Enabled = false;
            this.EnableButtons = true;
            this._oscReadButton.Enabled = true;
            this._oscShowButton.Enabled = true;
            this._oscSaveButton.Enabled = true;
            _oscProgressBar.Value = _oscProgressBar.Maximum;
        }

        #endregion [Help Classes Events Handlers]


        #region [Properties]
        /// <summary>
        /// ���������� ����������� ������� ������������ ��� ������
        /// </summary>
        private bool CanSelectOsc
        {
            set
            {
                this._oscilloscopeCountCb.Enabled = value;
                this._oscilloscopeCountLabel.Enabled = value;
                this._oscReadButton.Enabled = value;
                this._oscilloscopeCountCb.SelectedIndex = value ? 0 : -1;
            }
            get { return this._oscilloscopeCountCb.Enabled; }
        }

        /// <summary>
        /// ������ ���
        /// </summary>
        public CountingList CountingList
        {
            get { return _countingList; }
            set
            {
                this._countingList = value;
                this._oscShowButton.Enabled = true;
            }
        }

        #endregion [Properties]


        #region [Help members]
        private DataTable GetJournalDataTable()
        {
            var table = new DataTable("��731_������_������������");
            for (int j = 0; j < this._oscJournalDataGrid.Columns.Count; j++)
            {
                table.Columns.Add(this._oscJournalDataGrid.Columns[j].Name);
            }
            return table;
        }
        #endregion [Help members]


        #region [Event Handlers]
        /// <summary>
        /// �������� �����
        /// </summary>
        private void OscilloscopeForm_Load(object sender, EventArgs e)
        {
            this._oscJournalDataGrid.DataSource = this._table;
            this.StartRead();
        }
        /// <summary>
        /// �������� �������������
        /// </summary>
        private void _oscShowButton_Click(object sender, EventArgs e)
        {
            this.OscShow();
        }

        private void OscShow()
        {
            if (this.CountingList == null)
            {
                this.CountingList = new CountingList(new ushort[12000],new OscJournalStruct(), new MeasureTransStruct(), new ushort[24] );
            }
            if (Validator.GetVersionFromRegistry())
            {
                string fileName;
                if (this.CountingList.IsLoad)
                {
                    fileName = this.CountingList.FilePath;
                }
                else
                {
                    fileName = Validator.CreateOscFileNameCfg($"��731 v{this._device.DeviceVersion} �������������");
                    this._countingList.Save(fileName);
                }
                System.Diagnostics.Process.Start(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Oscilloscope.exe"),
                    fileName);
            }
            else
            {
                var resForm = new Mr731OscilloscopeResultForm(this.CountingList);
                resForm.Show();
            }

            
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        private void _oscJournalReadButton_Click(object sender, EventArgs e)
        {
            this.StartRead();
        }

        private void StartRead()
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._oscilloscopeCountCb.Items.Clear();
            this._oscilloscopeCountCb.SelectedIndex = -1;
            this._table.Clear();
            this._oscJournalLoader.Reset();
            this._oscJournalDataGrid.Refresh();
            this.CanSelectOsc = false;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._currentOptionsLoader.StartRead();
        }

        private bool EnableButtons
        {
            set
            {
                this._oscJournalReadButton.Enabled =
                        this._oscLoadButton.Enabled = value;
            }
        }

        /// <summary>
        /// ��������� �������������
        /// </summary>
        private void _oscReadButton_Click(object sender, EventArgs e)
        {
            int selectedOsc = this._oscilloscopeCountCb.SelectedIndex;
            this._journalStruct = this._oscJournalLoader.OscRecords[selectedOsc];
            this._pageLoader.StartRead(this._journalStruct, this._oscJournalLoader.OscSizeOptions);
            this._oscProgressBar.Value = 0;
            this._oscProgressBar.Maximum = this._pageLoader.PagesCount;
            //�������� ����������� ���������� ������ ������������
            this._stopReadOsc.Enabled = true;
            this.EnableButtons = false;
            this._oscReadButton.Enabled = false;
            this._oscShowButton.Enabled = false;
            this._oscSaveButton.Enabled = false;
        }

        /// <summary>
        /// ��������� ������������� � ����
        /// </summary>
        private void _oscSaveButton_Click(object sender, EventArgs e)
        { 
            if (this._saveOscilloscopeDlg.ShowDialog() == DialogResult.OK)
            {
                this._countingList.Save(this._saveOscilloscopeDlg.FileName);
            }
        }

        /// <summary>
        /// ��������� ������������� �� �����
        /// </summary>
        private void _oscLoadButton_Click(object sender, EventArgs e)
        {
            if (this._openOscilloscopeDlg.ShowDialog() != DialogResult.OK)
                return;
            try
            {
                this.CountingList = CountingList.Load(this._openOscilloscopeDlg.FileName);
                this._statusLabel.Text = string.Format("������������ ��������� �� ����� {0}",
                                                       this._openOscilloscopeDlg.FileName);
                this._oscSaveButton.Enabled = false;
                this._stopReadOsc.Enabled = false;
            }
            catch
            {
                this._statusLabel.Text = "���������� ��������� ������������";
            }

        }

        #endregion [Event Handlers]

        /// <summary>
        /// ���������� ������ ������������
        /// </summary>
        private void _stopReadOsc_Click(object sender, EventArgs e)
        {
            this._pageLoader.StopRead();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {   
            _oscJournalDataGrid.Columns["_oscReadyColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscStartColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscEndColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscBeginColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscLengthColumn"].Visible = this.checkBox1.Checked;
            _oscJournalDataGrid.Columns["_oscOtschLengthColumn"].Visible = this.checkBox1.Checked;
        }

        private void _oscJournalDataGrid_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            _oscilloscopeCountCb.SelectedIndex = e.RowIndex;
        }
    }
}