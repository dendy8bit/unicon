<?xml version="1.0" encoding="windows-1251"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
      
      </head>
      <body>
        <xsl:variable name="vers" select="��761���/������"></xsl:variable>
        <xsl:variable name="isPrimary" select="��761���/������������_����_�����_�������"/>

        <h3>
          ���������� ��761���. ����� ���������� <xsl:value-of select="��761���/�����_����������"/>. ������ �� <xsl:value-of select="��761���/������"/>
        </h3>
        <p></p>

        <div name="�������">
          <h2>�������</h2>

          <div name="groups">
            <xsl:for-each select="��761���/������������_����_�����_�������/Setpoints/GroupSetpointStruct">
              <xsl:variable name="index" select="position()"/>
              <h2 style="color:#FF0000FF">
                ������ ������� <xsl:value-of select="position()"/>
              </h2>

              <h4>�������</h4>

              <table border="1" cellpadding="4" cellspacing="0">
                <tr bgcolor="c1ced5">
                  <th>�������</th>
                  <th>���������</th>
                  <th>������ ������������</th>
                  <th>t��[��]</th>
                  <th>t��[��]</th>
                  <th>������ �������</th>
                  <th>�������</th>
                  <th>������ ����������</th>
                  <th>�����������</th>
                  <th>��� �����.</th>
                  <th>�����</th>
                </tr>
                <xsl:for-each select="DefensesSetpoints/���_�������/DefenseExternalStruct">
                  <xsl:if test="position() &lt; 17">
                    <tr align="center">
                      <td>
                        ������� <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:value-of select="�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="����_���_�������_�����"/>
                      </td>
                      <td>
                        <xsl:value-of select="t��"/>
                      </td>
                      <td>
                        <xsl:value-of select="ty"/>
                      </td>
                      <td>
                        <xsl:value-of select="�������_���_�������_�����"/>
                      </td>
                      <td>
                        <xsl:for-each select="U����">
                          <xsl:if test="current() ='false'">���	</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:value-of select="����������"/>
                      </td>
                      <td>
                        <xsl:value-of select="���"/>
                      </td>
                      <td>
                        <xsl:for-each select="���_�������">
                          <xsl:if test="current() ='false'">���	</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                      <td>
                        <xsl:for-each select="�����_�������">
                          <xsl:if test="current() ='false'">���	</xsl:if>
                          <xsl:if test="current() ='true'">��</xsl:if>
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>

              <h4>���������� �������</h4>
              <table border="1" cellspacing="0">
                <td>
                  <b>���������� ������� �</b>
                  <table border="1" cellspacing="0">
                    <tr bgcolor="c1ced5">
                      <th>����� ��</th>
                      <th>������������</th>
                    </tr>
                    <xsl:for-each select="InputLogicSignal/��">
                      <xsl:if test="position() &lt; 9 ">
                        <tr>
                          <td>
                            <xsl:value-of  select="position()"/>
                          </td>

                          <td>
                            <xsl:for-each select="�������">
                              <xsl:if test="current() !='���'">
                                <xsl:if test="current() ='��'">
                                  �<xsl:value-of select="position()"/>
                                </xsl:if>
                                <xsl:if test="current() ='������'">
                                  ^�<xsl:value-of select="position()"/>
                                </xsl:if>
                              </xsl:if>
                            </xsl:for-each>
                          </td>
                        </tr>
                      </xsl:if>
                    </xsl:for-each>
                  </table>

                  <b>���������� ������� ���</b>
                  <table border="1" cellspacing="0">

                    <tr bgcolor="c1ced5">
                      <th>����� ��</th>
                      <th>������������</th>
                    </tr>

                    <xsl:for-each select="InputLogicSignal/��">
                      <xsl:if test="position() &gt; 8 ">
                        <xsl:if test="position() &lt; 17">
                          <tr>
                            <td>
                              <xsl:value-of  select="position()"/>
                            </td>

                            <td>
                              <xsl:for-each select="�������">
                                <xsl:if test="current() !='���'">
                                  <xsl:if test="current() ='��'">
                                    �<xsl:value-of select="position()"/>
                                  </xsl:if>
                                  <xsl:if test="current() ='������'">
                                    ^�<xsl:value-of select="position()"/>
                                  </xsl:if>
                                </xsl:if>
                              </xsl:for-each>
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:if>
                    </xsl:for-each>
                  </table>
                </td>
              </table>

              <b>���</b>
              <table border="1" cellspacing="0">
                <tr bgcolor="c1ced5">
                  <th>�����</th>
                  <th>������������</th>
                </tr>


                <xsl:for-each select="OutputLogicSignal/���">
                  <xsl:if test="position() &lt; 17">
                    <tr>
                      <td>
                        <xsl:value-of select="position()"/>
                      </td>
                      <td>
                        <xsl:for-each select="�������">
                          <xsl:value-of select="current()"/>|
                        </xsl:for-each>
                      </td>
                    </tr>
                  </xsl:if>
                </xsl:for-each>
              </table>
              <p></p>

            </xsl:for-each>
          </div>
        </div>

        <h2>������� �������</h2>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>������ �������</tr>
          <tr>
            <th bgcolor="FF8C00">������ 1</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_1"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF8C00">������ 2</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_2"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF8C00">������ 3</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_3"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF8C00">������ 4</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_4"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF8C00">������ 5</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_5"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF8C00">������ 6</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_���������_������_�������_x002B_6"/>
            </td>
          </tr>
        </table>

        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>
            <th bgcolor="FF8C00">����� ���������</th>
            <td>
              <xsl:value-of select="��761���/������������_�������_��������/����_�����_���������"/>
            </td>
          </tr>
        </table>
        <p></p>

        <h2>�������� �������</h2>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>�������� ����</tr>
          <tr bgcolor="90EE90">
            <th>�</th>
            <th>���</th>
            <th>������</th>
            <th>T�����., ��</th>
          </tr>
          <xsl:for-each select="��761���/������������_����_x002C_�����������_x002C_��������������/����/���_����/����_����">
            <tr align="center">
              <td>
                <xsl:value-of select="position() + 2"/>
              </td>
              <td>
                <xsl:value-of select="@���"/>
              </td>
              <td>
                <xsl:value-of select="@������"/>
              </td>
              <td>
                <xsl:value-of select="@�����"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>����������</tr>
          <tr bgcolor="90EE90">
            <th>�</th>
            <th>���</th>
            <th>������ "�������"</th>
            <th>������ "�������"</th>
            <th>����� ������</th>
          </tr>
          <xsl:for-each select="��761���/������������_����_x002C_�����������_x002C_��������������/����������/���_����������/����_���������">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select="@���"/>
              </td>
              <td>
                <xsl:value-of select="@������_�������"/>
              </td>
              <td>
                <xsl:value-of select="@������_�������"/>
              </td>
              <td>
                <xsl:value-of select="@�����_������"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellspacing="0" cellpadding="4">
          <tr>����� �����������</tr>
          <xsl:for-each select="��761���/��������������_������������">
            <tr align="left">
              <th bgcolor="90EE90">1. �� ����� � ������ �������</th>
              <td>
                <xsl:for-each select="@�����_��_�����_�_������_�������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
            <tr align="left">
              <th bgcolor="90EE90">2. �� ����� � ������ ������</th>
              <td>
                <xsl:for-each select="@�����_��_�����_�_������_������">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>���� �������������</tr>
          <xsl:for-each select="��761���/������������_����_x002C_�����������_x002C_��������������/����_�������������">
            <tr align="left">
              <th bgcolor="90EE90">1. ���������� �������������</th>
              <td>
                <xsl:for-each select="@�������������_1">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
            </tr>
              <tr align="left">
                <th bgcolor="90EE90">2. ����������� �������������</th>
              <td>
                <xsl:for-each select="@�������������_2">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
              </tr>
                <tr align="left">
                  <th bgcolor="90EE90">3. ������������� �����������</th>
              <td>
                <xsl:for-each select="@�������������_3">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
                </tr>
                  <tr align="left">
                    <th bgcolor="90EE90">4. ������������� ������</th>
              <td>
                <xsl:for-each select="@�������������_4">
                  <xsl:if test="current() ='false'">���	</xsl:if>
                  <xsl:if test="current() ='true'">��</xsl:if>
                </xsl:for-each>
              </td>
                  </tr>
            <tr align="left">
              <th bgcolor="90EE90">�������, ��</th>
              <td>
                <xsl:value-of select="@�������_����_�������������"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p></p>

        <h2>�����������</h2>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr>
            <th bgcolor="FF69B4">���������� �����������</th>
            <td>
              <xsl:value-of select="��761���/������������_�����������/������������_���/����������_�����������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF69B4">������, ��</th>
            <td>
              <xsl:value-of select="��761���/������_������������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF69B4">����. ����������, %</th>
            <td>
              <xsl:value-of select="��761���/������������_�����������/������������_���/����������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF69B4">������. ��</th>
            <td>
              <xsl:value-of select="��761���/������������_�����������/������������_���/��������"/>
            </td>
          </tr>
          <tr>
            <th bgcolor="FF69B4">���� ����� ���.</th>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����_�������_������������/@�����"/>
            </td>
          </tr>
        </table>
        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>��������������� ���������� ������</tr>
          <tr bgcolor="FF69B4">
            <th>�����</th>
            <th>����</th>
            <th>�����</th>
          </tr>
          <xsl:for-each select="��761���/������������_�����������/������������_�������/���_������/ChannelWithBase">
            <tr align="center">
              <td>
                <xsl:value-of select="position()"/>
              </td>
              <td>
                <xsl:value-of select="@����"/>
              </td>
              <td>
                <xsl:value-of select="@�����"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>

        <p></p>

        <h2>�����������</h2>
        <table border="1" cellpadding="4" cellspacing="0">
          <tr>�����������</tr>
          <tr bgcolor="FFC0CB">
            <th>��������� "���������"</th>
            <th>��������� "��������"</th>
            <th>���� �������������</th>
            <th>���� ����������</th>
            <th>������� ������� ���., ��</th>
            <th>������� ����������</th>
            <th>t�����, ��</th>
            <th>�������� ����� ����������</th>
            <th>�������� ������� ��������� ����������</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="��761���/������������_�����������/���������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/��������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/�������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/�������_����������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/���������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/��������_�����_���������_����������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/��������_�����_�������_���������_����������"/>
            </td>
          </tr>
        </table>

        <p></p>

        <table border="1" cellpadding="4" cellspacing="0">
          <tr>����������</tr>
          <tr bgcolor="FFC0CB">
            <th>���� ��������</th>
            <th>���� ���������</th>
            <th>������� ��������</th>
            <th>������� ���������</th>
            <th>�� ������ ������</th>
            <th>�� �����</th>
            <th>�������</th>
            <th>�� ����</th>
          </tr>
          <tr align="center">
            <td>
              <xsl:value-of select="��761���/������������_�����������/����_���"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����_����"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����_����_��������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����_����_���������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/�������"/>
            </td>
            <td>
              <xsl:value-of select="��761���/������������_�����������/����"/>
            </td>
          </tr>
        </table>

      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
