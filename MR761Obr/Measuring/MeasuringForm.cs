﻿using System;
using System.Drawing;
using System.Windows.Forms;
using BEMN.Devices;
using BEMN.Devices.MemoryEntityClasses;
using BEMN.Devices.Structures;
using BEMN.Forms;
using BEMN.Interfaces;
using BEMN.MBServer;
using BEMN.MR761Obr.Measuring.Structures;

namespace BEMN.MR761Obr.Measuring
{
    public partial class MeasuringForm : Form, IFormView
    {
        #region Const

        private const string RESET_SJ = "Сбросить новую запись в журнале системы";
        private const string RESET_AJ = "Сбросить новую запись в журнале аварий";
        private const string RESET_OSC = "Сбросить новую запись журнала осциллографа";
        private const string RESET_FAULT_SJ = "Сбросить наличие неисправности по ЖС";
        private const string RESET_INDICATION = "Сбросить блинкеры";
        private const string CURRENT_GROUP = "Группа №{0}";
        private const string SWITCH_GROUP_USTAVKI = "Переключить на группу уставок №{0}?";
        private const string START_OSC = "Запустить осциллограф";
        private const string SWITCH_OFF = "Отключить выключатель";
        private const string SWITCH_ON = "Включить выключатель";
        #endregion

        #region [Private fields]

        private Mr761ObrDevice _device;
        private readonly MemoryEntity<DiscretDataBaseStruct> _discretDataBase;
        private readonly MemoryEntity<OneWordStruct> _groupUstavki;
        private readonly MemoryEntity<DateTimeStruct> _dateTime;
        private ushort? _numGroup;

        /// <summary>
        /// Дискретные входы
        /// </summary>
        private LedControl[] _discretInputs;

        /// <summary>
        /// Входные ЛС
        /// </summary>
        private LedControl[] _inputsLogicSignals;

        /// <summary>
        /// Выходные ЛС
        /// </summary>
        private LedControl[] _outputLogicSignals;

        /// <summary>
        /// Внешние защиты
        /// </summary>
        private LedControl[] _externalDefenses;

        /// <summary>
        /// Свободная логика
        /// </summary>
        private LedControl[] _freeLogic;

        /// <summary>
        /// Состояния
        /// </summary>
        private LedControl[] _stateAndApv;

        /// <summary>
        /// Реле
        /// </summary>
        private LedControl[] _relays;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private Diod[] _indicators;

        /// <summary>
        /// Индикаторы
        /// </summary>
        private LedControl[] _controlSignals;

        /// <summary>
        /// Неисправности
        /// </summary>
        private LedControl[] _faultsMain;

        /// <summary>
        /// Неисправности выключателя
        /// </summary>
        private LedControl[] _faultsSwitch;

        /// <summary>
        /// Ошибки СПЛ
        /// </summary>
        private LedControl[] _splErr;

        #endregion [Private fields]

        #region Constructor

        public MeasuringForm()
        {
            this.InitializeComponent();
        }

        public MeasuringForm(Mr761ObrDevice device)
        {
            this.InitializeComponent();
            this._device = device;
            this._device.ConnectionModeChanged += this.StartStopLoad;

            this._dateTime = device.DateTime;
            this._dateTime.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DateTimeLoad);
            
            this._discretDataBase = device.DiscretDataBase;
            this._discretDataBase.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadOk);
            this._discretDataBase.AllReadFail += HandlerHelper.CreateReadArrayHandler(this, this.DiscretBdReadFail);
            
            this._groupUstavki = device.GroupUstavki;
            this._groupUstavki.AllReadOk += HandlerHelper.CreateReadArrayHandler(this, this.GroupUstavkiLoaded);
            this._groupUstavki.AllWriteOk += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Группа уставок успешно изменена", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Information));
            this._groupUstavki.AllWriteFail += HandlerHelper.CreateReadArrayHandler(this, () =>
                MessageBox.Show("Невозможно изменить группу уставок", "Запись группы уставок", MessageBoxButtons.OK,
                    MessageBoxIcon.Error));

            this.Init();
        }

        private void Init()
        {
            this._commandComboBox.SelectedIndex = 0;
            this._freeLogic = new[]
            {
                this._ssl1, this._ssl2, this._ssl3, this._ssl4, this._ssl5, this._ssl6, this._ssl7, this._ssl8,
                this._ssl9, this._ssl10, this._ssl11, this._ssl12, this._ssl13, this._ssl14, this._ssl15, this._ssl16,
                this._ssl17, this._ssl18, this._ssl19, this._ssl20, this._ssl21, this._ssl22, this._ssl23, this._ssl24,
                this._ssl25, this._ssl26, this._ssl27, this._ssl28, this._ssl29, this._ssl30, this._ssl31, this._ssl32
            };


            this._stateAndApv = new[]
            {
                this._fault, this._acceleration, this._signaling, this._faultOff
            };

            this._indicators = new[]
            {
                this.diod1, this.diod2, this.diod3, this.diod4, this.diod5, this.diod6, this.diod7, this.diod8,
                this.diod9, this.diod10, this.diod11, this.diod12
            };

            this._relays = new[]
            {
                this._module1, this._module2, this._module3, this._module4, this._module5, this._module6,
                this._module7, this._module8, this._module9, this._module10, this._module11, this._module12,
                this._module13, this._module14, this._module15, this._module16, this._module17, this._module18,
                this._module19, this._module20, this._module21, this._module22, this._module23, this._module24,
                this._module25, this._module26, this._module27, this._module28, this._module29, this._module30,
                this._module31, this._module32, this._module33, this._module34
            };
            this._externalDefenses = new[]
            {
                this._vz1, this._vz2, this._vz3, this._vz4, this._vz5, this._vz6, this._vz7, this._vz8,
                this._vz9, this._vz10, this._vz11, this._vz12, this._vz13, this._vz14, this._vz15, this._vz16
            };

            this._outputLogicSignals = new[]
            {
                this._vls1, this._vls2, this._vls3, this._vls4, this._vls5, this._vls6, this._vls7, this._vls8,
                this._vls9, this._vls10, this._vls11, this._vls12, this._vls13, this._vls14, this._vls15, this._vls16
            };
            this._inputsLogicSignals = new[]
            {
                this._ls1, this._ls2, this._ls3, this._ls4, this._ls5, this._ls6, this._ls7, this._ls8,
                this._ls9, this._ls10, this._ls11, this._ls12, this._ls13, this._ls14, this._ls15, this._ls16
            };

            this._controlSignals = new[]
            {
                this._newRecordSystemJournal, this._newRecordAlarmJournal, this._newRecordOscJournal,
                this._availabilityFaultSystemJournal, new LedControl(), this._switchOff, this._switchOn
            };
            this._faultsMain = new[]
            {
                this._faultHardware, this._faultSoftware,
                this._faultSwitchOff, this._faultLogic, new LedControl(), new LedControl(), new LedControl(),
                this._faultModule1, this._faultModule2,
                this._faultModule3, this._faultModule4, this._faultModule5, this._faultSetpoints,
                this._faultGroupsOfSetpoints, this._faultPass, this._faultSystemJournal, this._faultAlarmJournal,
                this._faultOsc
            };
            this._discretInputs = new[]
            {
                this._d1, this._d2, this._d3, this._d4, this._d5, this._d6, this._d7, this._d8, this._d9, this._d10,
                this._d11, this._d12, this._d13, this._d14, this._d15, this._d16, this._d17, this._d18, this._d19,
                this._d20, this._d21, this._d22, this._d23, this._d24, this._d25, this._d26, this._d27, this._d28,
                this._d29, this._d30, this._d31, this._d32, this._d33, this._d34, this._d35, this._d36, this._d37,
                this._d38, this._d39, this._d40, this._d41, this._d42, this._d43, this._d44, this._d45, this._d46,
                this._d47, this._d48, this._d49, this._d50, this._d51, this._d52, this._d53, this._d54, this._d55,
                this._d56, this._d57, this._d58, this._d59, this._d60, this._d61, this._d62, this._d63, this._d64,
                this._d65, this._d66, this._d67, this._d68, this._d69, this._d70, this._d71, this._d72, this._k1,
                this._k2
            };

            this._splErr = new[]
            {
                this._fSpl5, this._fSpl1, this._fSpl2, this._fSpl3
            };
            this._groupCombo.SelectedIndex = 0;

            this._faultsSwitch = new[]
            {
                this._faultOut, this._faultBlockCon, this._faultManage, new LedControl(), this._faultSwithON,
                this._faultDisable1, this._faultDisable2
            };

        }

        #endregion Constructor

        #region MemoryEntity Members

        private void GroupUstavkiLoaded()
        {
            if (this._numGroup == this._groupUstavki.Value.Word) return;
            this._numGroup = this._groupUstavki.Value.Word;
            this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
        }
        
        /// <summary>
        /// Ошибка чтения дискретной базы данных
        /// </summary>
        private void DiscretBdReadFail()
        {
            LedManager.TurnOffLeds(this._discretInputs);
            LedManager.TurnOffLeds(this._inputsLogicSignals);
            LedManager.TurnOffLeds(this._outputLogicSignals);
            LedManager.TurnOffLeds(this._externalDefenses);
            LedManager.TurnOffLeds(this._freeLogic);
            LedManager.TurnOffLeds(this._stateAndApv);
            LedManager.TurnOffLeds(this._relays);
            LedManager.TurnOffLeds(this._controlSignals);
            LedManager.TurnOffLeds(this._faultsMain);
            LedManager.TurnOffLeds(this._faultsSwitch);
            LedManager.TurnOffLeds(this._splErr);
            this._logicState.State = LedState.Off;
            foreach (var indicator in this._indicators)
            {
                indicator.TurnOff();
            }
        }

        /// <summary>
        /// Прочитана дискретная база данных
        /// </summary>
        private void DiscretBdReadOk()
        {

            //Дискретные входы
            LedManager.SetLeds(this._discretInputs, this._discretDataBase.Value.DiscretInputs);
            //Входные ЛС
            LedManager.SetLeds(this._inputsLogicSignals, this._discretDataBase.Value.InputsLogicSignals);
            //Выходные ВЛС
            LedManager.SetLeds(this._outputLogicSignals, this._discretDataBase.Value.OutputLogicSignals);
            //Внешние защиты
            LedManager.SetLeds(this._externalDefenses, this._discretDataBase.Value.ExternalDefenses);
            //Свободная логика
            LedManager.SetLeds(this._freeLogic, this._discretDataBase.Value.FreeLogic);
            //Состояния и АПВ
            LedManager.SetLeds(this._stateAndApv, this._discretDataBase.Value.StateAndApv);
            //Реле
            LedManager.SetLeds(this._relays, this._discretDataBase.Value.Relays);
            //Индикаторы
            for (int i = 0; i < this._indicators.Length; i++)
            {
                this._indicators[i].SetState(this._discretDataBase.Value.Indicators[i]);
            }
            //Контроль
            LedManager.SetLeds(this._controlSignals, this._discretDataBase.Value.ControlSignals);
            //Неисправности основные
            LedManager.SetLeds(this._faultsMain, this._discretDataBase.Value.Faults1);
            //Неисправности выключателя
            LedManager.SetLeds(this._faultsSwitch, this._discretDataBase.Value.Faults2);
            //Ошибки СПЛ
            LedManager.SetLeds(this._splErr, this._discretDataBase.Value.FaultLogicErr);

            bool res = this._discretDataBase.Value.ControlSignals[7] &&
                       !(this._discretDataBase.Value.FaultLogicErr[0] || this._discretDataBase.Value.FaultLogicErr[1] ||
                         this._discretDataBase.Value.FaultLogicErr[2] || this._discretDataBase.Value.FaultLogicErr[3]);
            this._logicState.State = res ? LedState.NoSignaled : LedState.Signaled;
        }

        /// <summary>
        /// Прочитанно время
        /// </summary>
        private void DateTimeLoad()
        {
            this._dateTimeControl.DateTime = this._dateTime.Value;
        }

        #endregion MemoryEntity Members

        #region [Help members]

        private void dateTimeControl_TimeChanged()
        {
            this._dateTime.Value = this._dateTimeControl.DateTime;
            this._dateTime.SaveStruct();
        }

        private void MeasuringForm_Load(object sender, EventArgs e)
        {
            this.StartStopLoad();
        }

        private void MeasuringForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._groupUstavki.RemoveStructQueries();
            this._discretDataBase.RemoveStructQueries();
            this._dateTime.RemoveStructQueries();
            this._device.ConnectionModeChanged -= this.StartStopLoad;
        }

        private void StartStopLoad()
        {
            if (this._device.IsConnect && this._device.DeviceDlgInfo.IsConnectionMode)
            {
                this._groupUstavki.LoadStructCycle();
                this._discretDataBase.LoadStructCycle();
                this._dateTime.LoadStructCycle();
            }
            else
            {
                this._groupUstavki.RemoveStructQueries();
                this._discretDataBase.RemoveStructQueries();
                this._dateTime.RemoveStructQueries();
                this.DiscretBdReadFail();
            }
        }

        private void _resetSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D01, RESET_SJ);
        }

        private void _resetAlarmJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D02, RESET_AJ);
        }

        private void _resetOscJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D03, RESET_OSC);
        }

        private void _resetAvailabilityFaultSystemJournalButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D04, RESET_FAULT_SJ);
        }

        private void _resetAnButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D05, RESET_INDICATION);
        }
        
        private void _breakerOffBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D08, SWITCH_OFF);
        }
        
        private void _breakerOnBut_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D09, SWITCH_ON);
        }
        
        private void StopLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show(
                "Остановить свободно программируемую логику в устройстве? ВНИМАНИЕ! Это может привести к выводу из работы важных функций устройства",
                "Останов СПЛ",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0C, true, "Останов СПЛ", this._device);
        }

        private void StartLogicBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (MessageBox.Show("Запустить свободно программируемую логику в устройстве?", "Запуск СПЛ",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            this._device.SetBit(this._device.DeviceNumber, 0x0D0D, true, "Запуск СПЛ", this._device);
        }
        
        private void _startOscBtnClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            this._discretDataBase.SetBitByAdress(0x0D11, START_OSC);
        }

        private void _switchGroupButton_Click(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            if (this._groupCombo.SelectedIndex == this._groupUstavki.Value.Word) return;
            int ind = 0;
            if (this._groupCombo.SelectedIndex != -1) ind = this._groupCombo.SelectedIndex;
            bool res = MessageBox.Show(string.Format(SWITCH_GROUP_USTAVKI, ind + 1),
                "Группы уставок", MessageBoxButtons.YesNo) != DialogResult.Yes;
            if (res) return;
            this._groupUstavki.Value.Word = (ushort)ind;
            this._groupUstavki.SaveStruct();
            this._numGroup = this._groupUstavki.Value.Word;
            this._currenGroupLabel.Text = string.Format(CURRENT_GROUP, this._numGroup + 1);
        }
        
        private void CommandRunClick(object sender, EventArgs e)
        {
            if (!this._device.IsConnect && !this._device.DeviceDlgInfo.IsConnectionMode) return;
            int i = _commandComboBox.SelectedIndex;
            ConfirmCommand((ushort)(0x0D12 + i), "Выполнить команду " + (i + 1));
            //this._discretDataBase.SetBitByAdress((ushort) (0x0D12 + i), "Выполнить команду " + (i+1));
        }

        private void ConfirmCommand(ushort address, string command)
        {
            DialogResult res = MessageBox.Show(command + "?", "Подтверждение комманды", MessageBoxButtons.YesNo);
            if (res == DialogResult.No) return;
            this._discretDataBase.SetBitByAdress(address, command);
        }

        #endregion [Help members]

        #region [IFormView Members]

        public Type FormDevice
        {
            get { return typeof (Mr761ObrDevice); }
        }

        public bool Multishow { get; private set; }

        public Type ClassType
        {
            get { return typeof (MeasuringForm); }
        }

        public bool ForceShow
        {
            get { return false; }
        }

        public Image NodeImage
        {
            get { return Properties.Resources.measuring.ToBitmap(); }
        }

        public string NodeName
        {
            get { return "Измерения"; }
        }

        public INodeView[] ChildNodes
        {
            get { return new INodeView[] {}; }
        }

        public bool Deletable
        {
            get { return false; }
        }


        #endregion [INodeView Members]

    }
}
